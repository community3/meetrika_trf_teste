/*
               File: ContratadaGeneral
        Description: Contratada General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:27:39.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratadageneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratadageneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratadageneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_Codigo )
      {
         this.A39Contratada_Codigo = aP0_Contratada_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContratada_TipoFabrica = new GXCombobox();
         cmbContratada_UsaOSistema = new GXCombobox();
         cmbContratada_OSPreferencial = new GXCombobox();
         chkContratada_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A39Contratada_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA372( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "ContratadaGeneral";
               context.Gx_err = 0;
               WS372( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contratada General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202053021273940");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratadageneral.aspx") + "?" + UrlEncode("" +A39Contratada_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA39Contratada_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_TIPOFABRICA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A516Contratada_TipoFabrica, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A438Contratada_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_BANCONOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A342Contratada_BancoNome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_BANCONRO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A343Contratada_BancoNro, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_AGENCIANOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A344Contratada_AgenciaNome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_AGENCIANRO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A345Contratada_AgenciaNro, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_CONTACORRENTE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A51Contratada_ContaCorrente, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_SS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1451Contratada_SS), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_OS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A524Contratada_OS), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_LOTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A530Contratada_Lote), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_USAOSISTEMA", GetSecureSignedToken( sPrefix, A1481Contratada_UsaOSistema));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_OSPREFERENCIAL", GetSecureSignedToken( sPrefix, A1867Contratada_OSPreferencial));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_ATIVO", GetSecureSignedToken( sPrefix, A43Contratada_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_PESSOACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A40Contratada_PessoaCod), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContratadaGeneral";
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1867Contratada_OSPreferencial);
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratadageneral:[SendSecurityCheck value for]"+"Contratada_OSPreferencial:"+StringUtil.BoolToStr( A1867Contratada_OSPreferencial));
      }

      protected void RenderHtmlCloseForm372( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratadageneral.js", "?202053021273946");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratadaGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contratada General" ;
      }

      protected void WB370( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratadageneral.aspx");
            }
            wb_table1_2_372( true) ;
         }
         else
         {
            wb_table1_2_372( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_372e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratada_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratadaGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A40Contratada_PessoaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtContratada_PessoaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratadaGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START372( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contratada General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP370( ) ;
            }
         }
      }

      protected void WS372( )
      {
         START372( ) ;
         EVT372( ) ;
      }

      protected void EVT372( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP370( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP370( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11372 */
                                    E11372 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP370( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12372 */
                                    E12372 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP370( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13372 */
                                    E13372 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP370( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14372 */
                                    E14372 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP370( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15372 */
                                    E15372 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP370( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP370( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE372( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm372( ) ;
            }
         }
      }

      protected void PA372( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbContratada_TipoFabrica.Name = "CONTRATADA_TIPOFABRICA";
            cmbContratada_TipoFabrica.WebTags = "";
            cmbContratada_TipoFabrica.addItem("", "(Nenhuma)", 0);
            cmbContratada_TipoFabrica.addItem("S", "F�brica de Software", 0);
            cmbContratada_TipoFabrica.addItem("M", "F�brica de M�trica", 0);
            cmbContratada_TipoFabrica.addItem("I", "Departamento / Setor", 0);
            cmbContratada_TipoFabrica.addItem("O", "Outras", 0);
            if ( cmbContratada_TipoFabrica.ItemCount > 0 )
            {
               A516Contratada_TipoFabrica = cmbContratada_TipoFabrica.getValidValue(A516Contratada_TipoFabrica);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_TIPOFABRICA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A516Contratada_TipoFabrica, ""))));
            }
            cmbContratada_UsaOSistema.Name = "CONTRATADA_USAOSISTEMA";
            cmbContratada_UsaOSistema.WebTags = "";
            cmbContratada_UsaOSistema.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratada_UsaOSistema.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratada_UsaOSistema.ItemCount > 0 )
            {
               A1481Contratada_UsaOSistema = StringUtil.StrToBool( cmbContratada_UsaOSistema.getValidValue(StringUtil.BoolToStr( A1481Contratada_UsaOSistema)));
               n1481Contratada_UsaOSistema = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1481Contratada_UsaOSistema", A1481Contratada_UsaOSistema);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_USAOSISTEMA", GetSecureSignedToken( sPrefix, A1481Contratada_UsaOSistema));
            }
            cmbContratada_OSPreferencial.Name = "CONTRATADA_OSPREFERENCIAL";
            cmbContratada_OSPreferencial.WebTags = "";
            cmbContratada_OSPreferencial.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratada_OSPreferencial.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratada_OSPreferencial.ItemCount > 0 )
            {
               A1867Contratada_OSPreferencial = StringUtil.StrToBool( cmbContratada_OSPreferencial.getValidValue(StringUtil.BoolToStr( A1867Contratada_OSPreferencial)));
               n1867Contratada_OSPreferencial = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1867Contratada_OSPreferencial", A1867Contratada_OSPreferencial);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_OSPREFERENCIAL", GetSecureSignedToken( sPrefix, A1867Contratada_OSPreferencial));
            }
            chkContratada_Ativo.Name = "CONTRATADA_ATIVO";
            chkContratada_Ativo.WebTags = "";
            chkContratada_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratada_Ativo_Internalname, "TitleCaption", chkContratada_Ativo.Caption);
            chkContratada_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratada_TipoFabrica.ItemCount > 0 )
         {
            A516Contratada_TipoFabrica = cmbContratada_TipoFabrica.getValidValue(A516Contratada_TipoFabrica);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_TIPOFABRICA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A516Contratada_TipoFabrica, ""))));
         }
         if ( cmbContratada_UsaOSistema.ItemCount > 0 )
         {
            A1481Contratada_UsaOSistema = StringUtil.StrToBool( cmbContratada_UsaOSistema.getValidValue(StringUtil.BoolToStr( A1481Contratada_UsaOSistema)));
            n1481Contratada_UsaOSistema = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1481Contratada_UsaOSistema", A1481Contratada_UsaOSistema);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_USAOSISTEMA", GetSecureSignedToken( sPrefix, A1481Contratada_UsaOSistema));
         }
         if ( cmbContratada_OSPreferencial.ItemCount > 0 )
         {
            A1867Contratada_OSPreferencial = StringUtil.StrToBool( cmbContratada_OSPreferencial.getValidValue(StringUtil.BoolToStr( A1867Contratada_OSPreferencial)));
            n1867Contratada_OSPreferencial = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1867Contratada_OSPreferencial", A1867Contratada_OSPreferencial);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_OSPREFERENCIAL", GetSecureSignedToken( sPrefix, A1867Contratada_OSPreferencial));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF372( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "ContratadaGeneral";
         context.Gx_err = 0;
      }

      protected void RF372( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00372 */
            pr_default.execute(0, new Object[] {A39Contratada_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A503Pessoa_MunicipioCod = H00372_A503Pessoa_MunicipioCod[0];
               n503Pessoa_MunicipioCod = H00372_n503Pessoa_MunicipioCod[0];
               A52Contratada_AreaTrabalhoCod = H00372_A52Contratada_AreaTrabalhoCod[0];
               A349Contratada_MunicipioCod = H00372_A349Contratada_MunicipioCod[0];
               n349Contratada_MunicipioCod = H00372_n349Contratada_MunicipioCod[0];
               A29Contratante_Codigo = H00372_A29Contratante_Codigo[0];
               n29Contratante_Codigo = H00372_n29Contratante_Codigo[0];
               A25Municipio_Codigo = H00372_A25Municipio_Codigo[0];
               n25Municipio_Codigo = H00372_n25Municipio_Codigo[0];
               A40Contratada_PessoaCod = H00372_A40Contratada_PessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_PESSOACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A40Contratada_PessoaCod), "ZZZZZ9")));
               A43Contratada_Ativo = H00372_A43Contratada_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A43Contratada_Ativo", A43Contratada_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_ATIVO", GetSecureSignedToken( sPrefix, A43Contratada_Ativo));
               A1867Contratada_OSPreferencial = H00372_A1867Contratada_OSPreferencial[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1867Contratada_OSPreferencial", A1867Contratada_OSPreferencial);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_OSPREFERENCIAL", GetSecureSignedToken( sPrefix, A1867Contratada_OSPreferencial));
               n1867Contratada_OSPreferencial = H00372_n1867Contratada_OSPreferencial[0];
               A1481Contratada_UsaOSistema = H00372_A1481Contratada_UsaOSistema[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1481Contratada_UsaOSistema", A1481Contratada_UsaOSistema);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_USAOSISTEMA", GetSecureSignedToken( sPrefix, A1481Contratada_UsaOSistema));
               n1481Contratada_UsaOSistema = H00372_n1481Contratada_UsaOSistema[0];
               A530Contratada_Lote = H00372_A530Contratada_Lote[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A530Contratada_Lote", StringUtil.LTrim( StringUtil.Str( (decimal)(A530Contratada_Lote), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_LOTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A530Contratada_Lote), "ZZZ9")));
               n530Contratada_Lote = H00372_n530Contratada_Lote[0];
               A524Contratada_OS = H00372_A524Contratada_OS[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A524Contratada_OS", StringUtil.LTrim( StringUtil.Str( (decimal)(A524Contratada_OS), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_OS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A524Contratada_OS), "ZZZZZZZ9")));
               n524Contratada_OS = H00372_n524Contratada_OS[0];
               A1451Contratada_SS = H00372_A1451Contratada_SS[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1451Contratada_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1451Contratada_SS), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_SS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1451Contratada_SS), "ZZZZZZZ9")));
               n1451Contratada_SS = H00372_n1451Contratada_SS[0];
               A523Pessoa_Fax = H00372_A523Pessoa_Fax[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A523Pessoa_Fax", A523Pessoa_Fax);
               n523Pessoa_Fax = H00372_n523Pessoa_Fax[0];
               A522Pessoa_Telefone = H00372_A522Pessoa_Telefone[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A522Pessoa_Telefone", A522Pessoa_Telefone);
               n522Pessoa_Telefone = H00372_n522Pessoa_Telefone[0];
               A350Contratada_UF = H00372_A350Contratada_UF[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A350Contratada_UF", A350Contratada_UF);
               n350Contratada_UF = H00372_n350Contratada_UF[0];
               A26Municipio_Nome = H00372_A26Municipio_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A26Municipio_Nome", A26Municipio_Nome);
               n26Municipio_Nome = H00372_n26Municipio_Nome[0];
               A521Pessoa_CEP = H00372_A521Pessoa_CEP[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A521Pessoa_CEP", A521Pessoa_CEP);
               n521Pessoa_CEP = H00372_n521Pessoa_CEP[0];
               A519Pessoa_Endereco = H00372_A519Pessoa_Endereco[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A519Pessoa_Endereco", A519Pessoa_Endereco);
               n519Pessoa_Endereco = H00372_n519Pessoa_Endereco[0];
               A51Contratada_ContaCorrente = H00372_A51Contratada_ContaCorrente[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A51Contratada_ContaCorrente", A51Contratada_ContaCorrente);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_CONTACORRENTE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A51Contratada_ContaCorrente, ""))));
               n51Contratada_ContaCorrente = H00372_n51Contratada_ContaCorrente[0];
               A345Contratada_AgenciaNro = H00372_A345Contratada_AgenciaNro[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A345Contratada_AgenciaNro", A345Contratada_AgenciaNro);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_AGENCIANRO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A345Contratada_AgenciaNro, ""))));
               n345Contratada_AgenciaNro = H00372_n345Contratada_AgenciaNro[0];
               A344Contratada_AgenciaNome = H00372_A344Contratada_AgenciaNome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A344Contratada_AgenciaNome", A344Contratada_AgenciaNome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_AGENCIANOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A344Contratada_AgenciaNome, "@!"))));
               n344Contratada_AgenciaNome = H00372_n344Contratada_AgenciaNome[0];
               A343Contratada_BancoNro = H00372_A343Contratada_BancoNro[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A343Contratada_BancoNro", A343Contratada_BancoNro);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_BANCONRO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A343Contratada_BancoNro, ""))));
               n343Contratada_BancoNro = H00372_n343Contratada_BancoNro[0];
               A342Contratada_BancoNome = H00372_A342Contratada_BancoNome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A342Contratada_BancoNome", A342Contratada_BancoNome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_BANCONOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A342Contratada_BancoNome, "@!"))));
               n342Contratada_BancoNome = H00372_n342Contratada_BancoNome[0];
               A438Contratada_Sigla = H00372_A438Contratada_Sigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A438Contratada_Sigla", A438Contratada_Sigla);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A438Contratada_Sigla, "@!"))));
               A40000Contratada_Logo_GXI = H00372_A40000Contratada_Logo_GXI[0];
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgContratada_Logo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? A40000Contratada_Logo_GXI : context.convertURL( context.PathToRelativeUrl( A1664Contratada_Logo))));
               n40000Contratada_Logo_GXI = H00372_n40000Contratada_Logo_GXI[0];
               A518Pessoa_IE = H00372_A518Pessoa_IE[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A518Pessoa_IE", A518Pessoa_IE);
               n518Pessoa_IE = H00372_n518Pessoa_IE[0];
               A42Contratada_PessoaCNPJ = H00372_A42Contratada_PessoaCNPJ[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               n42Contratada_PessoaCNPJ = H00372_n42Contratada_PessoaCNPJ[0];
               A516Contratada_TipoFabrica = H00372_A516Contratada_TipoFabrica[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_TIPOFABRICA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A516Contratada_TipoFabrica, ""))));
               A1664Contratada_Logo = H00372_A1664Contratada_Logo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1664Contratada_Logo", A1664Contratada_Logo);
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgContratada_Logo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? A40000Contratada_Logo_GXI : context.convertURL( context.PathToRelativeUrl( A1664Contratada_Logo))));
               n1664Contratada_Logo = H00372_n1664Contratada_Logo[0];
               A29Contratante_Codigo = H00372_A29Contratante_Codigo[0];
               n29Contratante_Codigo = H00372_n29Contratante_Codigo[0];
               A25Municipio_Codigo = H00372_A25Municipio_Codigo[0];
               n25Municipio_Codigo = H00372_n25Municipio_Codigo[0];
               A350Contratada_UF = H00372_A350Contratada_UF[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A350Contratada_UF", A350Contratada_UF);
               n350Contratada_UF = H00372_n350Contratada_UF[0];
               A503Pessoa_MunicipioCod = H00372_A503Pessoa_MunicipioCod[0];
               n503Pessoa_MunicipioCod = H00372_n503Pessoa_MunicipioCod[0];
               A523Pessoa_Fax = H00372_A523Pessoa_Fax[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A523Pessoa_Fax", A523Pessoa_Fax);
               n523Pessoa_Fax = H00372_n523Pessoa_Fax[0];
               A522Pessoa_Telefone = H00372_A522Pessoa_Telefone[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A522Pessoa_Telefone", A522Pessoa_Telefone);
               n522Pessoa_Telefone = H00372_n522Pessoa_Telefone[0];
               A521Pessoa_CEP = H00372_A521Pessoa_CEP[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A521Pessoa_CEP", A521Pessoa_CEP);
               n521Pessoa_CEP = H00372_n521Pessoa_CEP[0];
               A519Pessoa_Endereco = H00372_A519Pessoa_Endereco[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A519Pessoa_Endereco", A519Pessoa_Endereco);
               n519Pessoa_Endereco = H00372_n519Pessoa_Endereco[0];
               A518Pessoa_IE = H00372_A518Pessoa_IE[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A518Pessoa_IE", A518Pessoa_IE);
               n518Pessoa_IE = H00372_n518Pessoa_IE[0];
               A42Contratada_PessoaCNPJ = H00372_A42Contratada_PessoaCNPJ[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               n42Contratada_PessoaCNPJ = H00372_n42Contratada_PessoaCNPJ[0];
               A26Municipio_Nome = H00372_A26Municipio_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A26Municipio_Nome", A26Municipio_Nome);
               n26Municipio_Nome = H00372_n26Municipio_Nome[0];
               /* Execute user event: E12372 */
               E12372 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB370( ) ;
         }
      }

      protected void STRUP370( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "ContratadaGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11372 */
         E11372 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbContratada_TipoFabrica.CurrentValue = cgiGet( cmbContratada_TipoFabrica_Internalname);
            A516Contratada_TipoFabrica = cgiGet( cmbContratada_TipoFabrica_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_TIPOFABRICA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A516Contratada_TipoFabrica, ""))));
            A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
            n42Contratada_PessoaCNPJ = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            A518Pessoa_IE = cgiGet( edtPessoa_IE_Internalname);
            n518Pessoa_IE = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A518Pessoa_IE", A518Pessoa_IE);
            A1664Contratada_Logo = cgiGet( imgContratada_Logo_Internalname);
            n1664Contratada_Logo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1664Contratada_Logo", A1664Contratada_Logo);
            A438Contratada_Sigla = StringUtil.Upper( cgiGet( edtContratada_Sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A438Contratada_Sigla", A438Contratada_Sigla);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A438Contratada_Sigla, "@!"))));
            A342Contratada_BancoNome = StringUtil.Upper( cgiGet( edtContratada_BancoNome_Internalname));
            n342Contratada_BancoNome = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A342Contratada_BancoNome", A342Contratada_BancoNome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_BANCONOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A342Contratada_BancoNome, "@!"))));
            A343Contratada_BancoNro = cgiGet( edtContratada_BancoNro_Internalname);
            n343Contratada_BancoNro = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A343Contratada_BancoNro", A343Contratada_BancoNro);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_BANCONRO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A343Contratada_BancoNro, ""))));
            A344Contratada_AgenciaNome = StringUtil.Upper( cgiGet( edtContratada_AgenciaNome_Internalname));
            n344Contratada_AgenciaNome = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A344Contratada_AgenciaNome", A344Contratada_AgenciaNome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_AGENCIANOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A344Contratada_AgenciaNome, "@!"))));
            A345Contratada_AgenciaNro = cgiGet( edtContratada_AgenciaNro_Internalname);
            n345Contratada_AgenciaNro = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A345Contratada_AgenciaNro", A345Contratada_AgenciaNro);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_AGENCIANRO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A345Contratada_AgenciaNro, ""))));
            A51Contratada_ContaCorrente = cgiGet( edtContratada_ContaCorrente_Internalname);
            n51Contratada_ContaCorrente = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A51Contratada_ContaCorrente", A51Contratada_ContaCorrente);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_CONTACORRENTE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A51Contratada_ContaCorrente, ""))));
            A519Pessoa_Endereco = cgiGet( edtPessoa_Endereco_Internalname);
            n519Pessoa_Endereco = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A519Pessoa_Endereco", A519Pessoa_Endereco);
            A521Pessoa_CEP = cgiGet( edtPessoa_CEP_Internalname);
            n521Pessoa_CEP = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A521Pessoa_CEP", A521Pessoa_CEP);
            A26Municipio_Nome = StringUtil.Upper( cgiGet( edtMunicipio_Nome_Internalname));
            n26Municipio_Nome = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A26Municipio_Nome", A26Municipio_Nome);
            A350Contratada_UF = StringUtil.Upper( cgiGet( edtContratada_UF_Internalname));
            n350Contratada_UF = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A350Contratada_UF", A350Contratada_UF);
            A522Pessoa_Telefone = cgiGet( edtPessoa_Telefone_Internalname);
            n522Pessoa_Telefone = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A522Pessoa_Telefone", A522Pessoa_Telefone);
            A523Pessoa_Fax = cgiGet( edtPessoa_Fax_Internalname);
            n523Pessoa_Fax = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A523Pessoa_Fax", A523Pessoa_Fax);
            A1451Contratada_SS = (int)(context.localUtil.CToN( cgiGet( edtContratada_SS_Internalname), ",", "."));
            n1451Contratada_SS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1451Contratada_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1451Contratada_SS), 8, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_SS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1451Contratada_SS), "ZZZZZZZ9")));
            A524Contratada_OS = (int)(context.localUtil.CToN( cgiGet( edtContratada_OS_Internalname), ",", "."));
            n524Contratada_OS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A524Contratada_OS", StringUtil.LTrim( StringUtil.Str( (decimal)(A524Contratada_OS), 8, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_OS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A524Contratada_OS), "ZZZZZZZ9")));
            A530Contratada_Lote = (short)(context.localUtil.CToN( cgiGet( edtContratada_Lote_Internalname), ",", "."));
            n530Contratada_Lote = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A530Contratada_Lote", StringUtil.LTrim( StringUtil.Str( (decimal)(A530Contratada_Lote), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_LOTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A530Contratada_Lote), "ZZZ9")));
            cmbContratada_UsaOSistema.CurrentValue = cgiGet( cmbContratada_UsaOSistema_Internalname);
            A1481Contratada_UsaOSistema = StringUtil.StrToBool( cgiGet( cmbContratada_UsaOSistema_Internalname));
            n1481Contratada_UsaOSistema = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1481Contratada_UsaOSistema", A1481Contratada_UsaOSistema);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_USAOSISTEMA", GetSecureSignedToken( sPrefix, A1481Contratada_UsaOSistema));
            cmbContratada_OSPreferencial.CurrentValue = cgiGet( cmbContratada_OSPreferencial_Internalname);
            A1867Contratada_OSPreferencial = StringUtil.StrToBool( cgiGet( cmbContratada_OSPreferencial_Internalname));
            n1867Contratada_OSPreferencial = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1867Contratada_OSPreferencial", A1867Contratada_OSPreferencial);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_OSPREFERENCIAL", GetSecureSignedToken( sPrefix, A1867Contratada_OSPreferencial));
            A43Contratada_Ativo = StringUtil.StrToBool( cgiGet( chkContratada_Ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A43Contratada_Ativo", A43Contratada_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_ATIVO", GetSecureSignedToken( sPrefix, A43Contratada_Ativo));
            A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratada_PessoaCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_PESSOACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A40Contratada_PessoaCod), "ZZZZZ9")));
            /* Read saved values. */
            wcpOA39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA39Contratada_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContratadaGeneral";
            A1867Contratada_OSPreferencial = StringUtil.StrToBool( cgiGet( cmbContratada_OSPreferencial_Internalname));
            n1867Contratada_OSPreferencial = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1867Contratada_OSPreferencial", A1867Contratada_OSPreferencial);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_OSPREFERENCIAL", GetSecureSignedToken( sPrefix, A1867Contratada_OSPreferencial));
            forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1867Contratada_OSPreferencial);
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contratadageneral:[SecurityCheckFailed value for]"+"Contratada_OSPreferencial:"+StringUtil.BoolToStr( A1867Contratada_OSPreferencial));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11372 */
         E11372 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11372( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.0 - Data: 09/04/2020 23:40", 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12372( )
      {
         /* Load Routine */
         edtMunicipio_Nome_Link = formatLink("viewmunicipio.aspx") + "?" + UrlEncode("" +A25Municipio_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtMunicipio_Nome_Internalname, "Link", edtMunicipio_Nome_Link);
         edtContratada_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_Codigo_Visible), 5, 0)));
         edtContratada_PessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_PessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaCod_Visible), 5, 0)));
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
         lblContratada_ospreferencial_righttext_Visible = (A1867Contratada_OSPreferencial ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblContratada_ospreferencial_righttext_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblContratada_ospreferencial_righttext_Visible), 5, 0)));
      }

      protected void E13372( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contratada.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A39Contratada_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14372( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contratada.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A39Contratada_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E15372( )
      {
         /* 'DoFechar' Routine */
         AV12Websession.Remove("Entidade");
         context.wjLoc = formatLink("wwcontratada.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Contratada";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Contratada_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_372( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_372( true) ;
         }
         else
         {
            wb_table2_8_372( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_372e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_134_372( true) ;
         }
         else
         {
            wb_table3_134_372( false) ;
         }
         return  ;
      }

      protected void wb_table3_134_372e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_372e( true) ;
         }
         else
         {
            wb_table1_2_372e( false) ;
         }
      }

      protected void wb_table3_134_372( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_134_372e( true) ;
         }
         else
         {
            wb_table3_134_372e( false) ;
         }
      }

      protected void wb_table2_8_372( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_tipofabrica_Internalname, "Tipo", "", "", lblTextblockcontratada_tipofabrica_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratada_TipoFabrica, cmbContratada_TipoFabrica_Internalname, StringUtil.RTrim( A516Contratada_TipoFabrica), 1, cmbContratada_TipoFabrica_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratadaGeneral.htm");
            cmbContratada_TipoFabrica.CurrentValue = StringUtil.RTrim( A516Contratada_TipoFabrica);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratada_TipoFabrica_Internalname, "Values", (String)(cmbContratada_TipoFabrica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoacnpj_Internalname, "CNPJ", "", "", lblTextblockcontratada_pessoacnpj_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaCNPJ_Internalname, A42Contratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( A42Contratada_PessoaCNPJ, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaCNPJ_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_ie_Internalname, "Insc. Estadual", "", "", lblTextblockpessoa_ie_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_IE_Internalname, StringUtil.RTrim( A518Pessoa_IE), StringUtil.RTrim( context.localUtil.Format( A518Pessoa_IE, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_IE_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "IE", "left", true, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"4\"  class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_logo_Internalname, "Logomarca", "", "", lblTextblockcontratada_logo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"4\"  class='DataContentCell'>") ;
            /* Static Bitmap Variable */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            A1664Contratada_Logo_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo))&&String.IsNullOrEmpty(StringUtil.RTrim( A40000Contratada_Logo_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)));
            GxWebStd.gx_bitmap( context, imgContratada_Logo_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? A40000Contratada_Logo_GXI : context.PathToRelativeUrl( A1664Contratada_Logo)), "", "", "", context.GetTheme( ), 1, 0, "", "", 1, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, A1664Contratada_Logo_IsBlob, true, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_sigla_Internalname, "Sigla", "", "", lblTextblockcontratada_sigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_Sigla_Internalname, StringUtil.RTrim( A438Contratada_Sigla), StringUtil.RTrim( context.localUtil.Format( A438Contratada_Sigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_Sigla_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_banconome_Internalname, "Banco", "", "", lblTextblockcontratada_banconome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_BancoNome_Internalname, StringUtil.RTrim( A342Contratada_BancoNome), StringUtil.RTrim( context.localUtil.Format( A342Contratada_BancoNome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_BancoNome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_banconro_Internalname, "N�mero", "", "", lblTextblockcontratada_banconro_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_BancoNro_Internalname, StringUtil.RTrim( A343Contratada_BancoNro), StringUtil.RTrim( context.localUtil.Format( A343Contratada_BancoNro, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_BancoNro_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, -1, true, "NumeroBanco", "left", true, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_agencianome_Internalname, "Ag�ncia", "", "", lblTextblockcontratada_agencianome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_AgenciaNome_Internalname, StringUtil.RTrim( A344Contratada_AgenciaNome), StringUtil.RTrim( context.localUtil.Format( A344Contratada_AgenciaNome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_AgenciaNome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_agencianro_Internalname, "N�mero", "", "", lblTextblockcontratada_agencianro_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_AgenciaNro_Internalname, StringUtil.RTrim( A345Contratada_AgenciaNro), StringUtil.RTrim( context.localUtil.Format( A345Contratada_AgenciaNro, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_AgenciaNro_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "Agencia", "left", true, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_contacorrente_Internalname, "Conta Corrente", "", "", lblTextblockcontratada_contacorrente_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_ContaCorrente_Internalname, StringUtil.RTrim( A51Contratada_ContaCorrente), StringUtil.RTrim( context.localUtil.Format( A51Contratada_ContaCorrente, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_ContaCorrente_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "ContaCorrente", "left", true, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_endereco_Internalname, "Endere�o", "", "", lblTextblockpessoa_endereco_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_Endereco_Internalname, A519Pessoa_Endereco, StringUtil.RTrim( context.localUtil.Format( A519Pessoa_Endereco, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_Endereco_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_cep_Internalname, "CEP", "", "", lblTextblockpessoa_cep_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_CEP_Internalname, StringUtil.RTrim( A521Pessoa_CEP), StringUtil.RTrim( context.localUtil.Format( A521Pessoa_CEP, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_CEP_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmunicipio_nome_Internalname, "Municipio", "", "", lblTextblockmunicipio_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMunicipio_Nome_Internalname, StringUtil.RTrim( A26Municipio_Nome), StringUtil.RTrim( context.localUtil.Format( A26Municipio_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtMunicipio_Nome_Link, "", "", "", edtMunicipio_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_uf_Internalname, "Estado", "", "", lblTextblockcontratada_uf_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_UF_Internalname, StringUtil.RTrim( A350Contratada_UF), StringUtil.RTrim( context.localUtil.Format( A350Contratada_UF, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_UF_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "UF", "left", true, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_telefone_Internalname, "Telefone", "", "", lblTextblockpessoa_telefone_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_Telefone_Internalname, StringUtil.RTrim( A522Pessoa_Telefone), StringUtil.RTrim( context.localUtil.Format( A522Pessoa_Telefone, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_Telefone_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_fax_Internalname, "Fax", "", "", lblTextblockpessoa_fax_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_Fax_Internalname, StringUtil.RTrim( A523Pessoa_Fax), StringUtil.RTrim( context.localUtil.Format( A523Pessoa_Fax, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_Fax_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_ss_Internalname, "�ltima SS", "", "", lblTextblockcontratada_ss_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_SS_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1451Contratada_SS), 8, 0, ",", "")), context.localUtil.Format( (decimal)(A1451Contratada_SS), "ZZZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_SS_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_os_Internalname, "�ltima OS", "", "", lblTextblockcontratada_os_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_OS_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A524Contratada_OS), 8, 0, ",", "")), context.localUtil.Format( (decimal)(A524Contratada_OS), "ZZZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_OS_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_lote_Internalname, "�ltimo Lote", "", "", lblTextblockcontratada_lote_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_Lote_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A530Contratada_Lote), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A530Contratada_Lote), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_Lote_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 45, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_usaosistema_Internalname, "Usa o Sistema", "", "", lblTextblockcontratada_usaosistema_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratada_UsaOSistema, cmbContratada_UsaOSistema_Internalname, StringUtil.BoolToStr( A1481Contratada_UsaOSistema), 1, cmbContratada_UsaOSistema_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratadaGeneral.htm");
            cmbContratada_UsaOSistema.CurrentValue = StringUtil.BoolToStr( A1481Contratada_UsaOSistema);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratada_UsaOSistema_Internalname, "Values", (String)(cmbContratada_UsaOSistema.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_ospreferencial_Internalname, "Preferencial", "", "", lblTextblockcontratada_ospreferencial_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table4_115_372( true) ;
         }
         else
         {
            wb_table4_115_372( false) ;
         }
         return  ;
      }

      protected void wb_table4_115_372e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_ativo_Internalname, "Ativo", "", "", lblTextblockcontratada_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='DataContentCell'>") ;
            /* Check box */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratada_Ativo_Internalname, StringUtil.BoolToStr( A43Contratada_Ativo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_372e( true) ;
         }
         else
         {
            wb_table2_8_372e( false) ;
         }
      }

      protected void wb_table4_115_372( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratada_ospreferencial_Internalname, tblTablemergedcontratada_ospreferencial_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratada_OSPreferencial, cmbContratada_OSPreferencial_Internalname, StringUtil.BoolToStr( A1867Contratada_OSPreferencial), 1, cmbContratada_OSPreferencial_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "Atendimento preferencial", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratadaGeneral.htm");
            cmbContratada_OSPreferencial.CurrentValue = StringUtil.BoolToStr( A1867Contratada_OSPreferencial);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratada_OSPreferencial_Internalname, "Values", (String)(cmbContratada_OSPreferencial.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratada_ospreferencial_righttext_Internalname, "(1 OS por vez)", "", "", lblContratada_ospreferencial_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblContratada_ospreferencial_righttext_Visible, 1, 0, "HLP_ContratadaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_115_372e( true) ;
         }
         else
         {
            wb_table4_115_372e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A39Contratada_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA372( ) ;
         WS372( ) ;
         WE372( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA39Contratada_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA372( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratadageneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA372( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A39Contratada_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         }
         wcpOA39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA39Contratada_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A39Contratada_Codigo != wcpOA39Contratada_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA39Contratada_Codigo = A39Contratada_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA39Contratada_Codigo = cgiGet( sPrefix+"A39Contratada_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA39Contratada_Codigo) > 0 )
         {
            A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA39Contratada_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         }
         else
         {
            A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A39Contratada_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA372( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS372( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS372( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A39Contratada_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA39Contratada_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A39Contratada_Codigo_CTRL", StringUtil.RTrim( sCtrlA39Contratada_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE372( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202053021274092");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratadageneral.js", "?202053021274093");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratada_tipofabrica_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_TIPOFABRICA";
         cmbContratada_TipoFabrica_Internalname = sPrefix+"CONTRATADA_TIPOFABRICA";
         lblTextblockcontratada_pessoacnpj_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_PESSOACNPJ";
         edtContratada_PessoaCNPJ_Internalname = sPrefix+"CONTRATADA_PESSOACNPJ";
         lblTextblockpessoa_ie_Internalname = sPrefix+"TEXTBLOCKPESSOA_IE";
         edtPessoa_IE_Internalname = sPrefix+"PESSOA_IE";
         lblTextblockcontratada_logo_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_LOGO";
         imgContratada_Logo_Internalname = sPrefix+"CONTRATADA_LOGO";
         lblTextblockcontratada_sigla_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_SIGLA";
         edtContratada_Sigla_Internalname = sPrefix+"CONTRATADA_SIGLA";
         lblTextblockcontratada_banconome_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_BANCONOME";
         edtContratada_BancoNome_Internalname = sPrefix+"CONTRATADA_BANCONOME";
         lblTextblockcontratada_banconro_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_BANCONRO";
         edtContratada_BancoNro_Internalname = sPrefix+"CONTRATADA_BANCONRO";
         lblTextblockcontratada_agencianome_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_AGENCIANOME";
         edtContratada_AgenciaNome_Internalname = sPrefix+"CONTRATADA_AGENCIANOME";
         lblTextblockcontratada_agencianro_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_AGENCIANRO";
         edtContratada_AgenciaNro_Internalname = sPrefix+"CONTRATADA_AGENCIANRO";
         lblTextblockcontratada_contacorrente_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_CONTACORRENTE";
         edtContratada_ContaCorrente_Internalname = sPrefix+"CONTRATADA_CONTACORRENTE";
         lblTextblockpessoa_endereco_Internalname = sPrefix+"TEXTBLOCKPESSOA_ENDERECO";
         edtPessoa_Endereco_Internalname = sPrefix+"PESSOA_ENDERECO";
         lblTextblockpessoa_cep_Internalname = sPrefix+"TEXTBLOCKPESSOA_CEP";
         edtPessoa_CEP_Internalname = sPrefix+"PESSOA_CEP";
         lblTextblockmunicipio_nome_Internalname = sPrefix+"TEXTBLOCKMUNICIPIO_NOME";
         edtMunicipio_Nome_Internalname = sPrefix+"MUNICIPIO_NOME";
         lblTextblockcontratada_uf_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_UF";
         edtContratada_UF_Internalname = sPrefix+"CONTRATADA_UF";
         lblTextblockpessoa_telefone_Internalname = sPrefix+"TEXTBLOCKPESSOA_TELEFONE";
         edtPessoa_Telefone_Internalname = sPrefix+"PESSOA_TELEFONE";
         lblTextblockpessoa_fax_Internalname = sPrefix+"TEXTBLOCKPESSOA_FAX";
         edtPessoa_Fax_Internalname = sPrefix+"PESSOA_FAX";
         lblTextblockcontratada_ss_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_SS";
         edtContratada_SS_Internalname = sPrefix+"CONTRATADA_SS";
         lblTextblockcontratada_os_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_OS";
         edtContratada_OS_Internalname = sPrefix+"CONTRATADA_OS";
         lblTextblockcontratada_lote_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_LOTE";
         edtContratada_Lote_Internalname = sPrefix+"CONTRATADA_LOTE";
         lblTextblockcontratada_usaosistema_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_USAOSISTEMA";
         cmbContratada_UsaOSistema_Internalname = sPrefix+"CONTRATADA_USAOSISTEMA";
         lblTextblockcontratada_ospreferencial_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_OSPREFERENCIAL";
         cmbContratada_OSPreferencial_Internalname = sPrefix+"CONTRATADA_OSPREFERENCIAL";
         lblContratada_ospreferencial_righttext_Internalname = sPrefix+"CONTRATADA_OSPREFERENCIAL_RIGHTTEXT";
         tblTablemergedcontratada_ospreferencial_Internalname = sPrefix+"TABLEMERGEDCONTRATADA_OSPREFERENCIAL";
         lblTextblockcontratada_ativo_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_ATIVO";
         chkContratada_Ativo_Internalname = sPrefix+"CONTRATADA_ATIVO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContratada_Codigo_Internalname = sPrefix+"CONTRATADA_CODIGO";
         edtContratada_PessoaCod_Internalname = sPrefix+"CONTRATADA_PESSOACOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         lblContratada_ospreferencial_righttext_Visible = 1;
         cmbContratada_OSPreferencial_Jsonclick = "";
         cmbContratada_UsaOSistema_Jsonclick = "";
         edtContratada_Lote_Jsonclick = "";
         edtContratada_OS_Jsonclick = "";
         edtContratada_SS_Jsonclick = "";
         edtPessoa_Fax_Jsonclick = "";
         edtPessoa_Telefone_Jsonclick = "";
         edtContratada_UF_Jsonclick = "";
         edtMunicipio_Nome_Jsonclick = "";
         edtPessoa_CEP_Jsonclick = "";
         edtPessoa_Endereco_Jsonclick = "";
         edtContratada_ContaCorrente_Jsonclick = "";
         edtContratada_AgenciaNro_Jsonclick = "";
         edtContratada_AgenciaNome_Jsonclick = "";
         edtContratada_BancoNro_Jsonclick = "";
         edtContratada_BancoNome_Jsonclick = "";
         edtContratada_Sigla_Jsonclick = "";
         edtPessoa_IE_Jsonclick = "";
         edtContratada_PessoaCNPJ_Jsonclick = "";
         cmbContratada_TipoFabrica_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         edtMunicipio_Nome_Link = "";
         chkContratada_Ativo.Caption = "";
         edtContratada_PessoaCod_Jsonclick = "";
         edtContratada_PessoaCod_Visible = 1;
         edtContratada_Codigo_Jsonclick = "";
         edtContratada_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13372',iparms:[{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14372',iparms:[{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15372',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A516Contratada_TipoFabrica = "";
         A438Contratada_Sigla = "";
         A342Contratada_BancoNome = "";
         A343Contratada_BancoNro = "";
         A344Contratada_AgenciaNome = "";
         A345Contratada_AgenciaNro = "";
         A51Contratada_ContaCorrente = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00372_A503Pessoa_MunicipioCod = new int[1] ;
         H00372_n503Pessoa_MunicipioCod = new bool[] {false} ;
         H00372_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00372_A349Contratada_MunicipioCod = new int[1] ;
         H00372_n349Contratada_MunicipioCod = new bool[] {false} ;
         H00372_A29Contratante_Codigo = new int[1] ;
         H00372_n29Contratante_Codigo = new bool[] {false} ;
         H00372_A39Contratada_Codigo = new int[1] ;
         H00372_A25Municipio_Codigo = new int[1] ;
         H00372_n25Municipio_Codigo = new bool[] {false} ;
         H00372_A40Contratada_PessoaCod = new int[1] ;
         H00372_A43Contratada_Ativo = new bool[] {false} ;
         H00372_A1867Contratada_OSPreferencial = new bool[] {false} ;
         H00372_n1867Contratada_OSPreferencial = new bool[] {false} ;
         H00372_A1481Contratada_UsaOSistema = new bool[] {false} ;
         H00372_n1481Contratada_UsaOSistema = new bool[] {false} ;
         H00372_A530Contratada_Lote = new short[1] ;
         H00372_n530Contratada_Lote = new bool[] {false} ;
         H00372_A524Contratada_OS = new int[1] ;
         H00372_n524Contratada_OS = new bool[] {false} ;
         H00372_A1451Contratada_SS = new int[1] ;
         H00372_n1451Contratada_SS = new bool[] {false} ;
         H00372_A523Pessoa_Fax = new String[] {""} ;
         H00372_n523Pessoa_Fax = new bool[] {false} ;
         H00372_A522Pessoa_Telefone = new String[] {""} ;
         H00372_n522Pessoa_Telefone = new bool[] {false} ;
         H00372_A350Contratada_UF = new String[] {""} ;
         H00372_n350Contratada_UF = new bool[] {false} ;
         H00372_A26Municipio_Nome = new String[] {""} ;
         H00372_n26Municipio_Nome = new bool[] {false} ;
         H00372_A521Pessoa_CEP = new String[] {""} ;
         H00372_n521Pessoa_CEP = new bool[] {false} ;
         H00372_A519Pessoa_Endereco = new String[] {""} ;
         H00372_n519Pessoa_Endereco = new bool[] {false} ;
         H00372_A51Contratada_ContaCorrente = new String[] {""} ;
         H00372_n51Contratada_ContaCorrente = new bool[] {false} ;
         H00372_A345Contratada_AgenciaNro = new String[] {""} ;
         H00372_n345Contratada_AgenciaNro = new bool[] {false} ;
         H00372_A344Contratada_AgenciaNome = new String[] {""} ;
         H00372_n344Contratada_AgenciaNome = new bool[] {false} ;
         H00372_A343Contratada_BancoNro = new String[] {""} ;
         H00372_n343Contratada_BancoNro = new bool[] {false} ;
         H00372_A342Contratada_BancoNome = new String[] {""} ;
         H00372_n342Contratada_BancoNome = new bool[] {false} ;
         H00372_A438Contratada_Sigla = new String[] {""} ;
         H00372_A40000Contratada_Logo_GXI = new String[] {""} ;
         H00372_n40000Contratada_Logo_GXI = new bool[] {false} ;
         H00372_A518Pessoa_IE = new String[] {""} ;
         H00372_n518Pessoa_IE = new bool[] {false} ;
         H00372_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H00372_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H00372_A516Contratada_TipoFabrica = new String[] {""} ;
         H00372_A1664Contratada_Logo = new String[] {""} ;
         H00372_n1664Contratada_Logo = new bool[] {false} ;
         A523Pessoa_Fax = "";
         A522Pessoa_Telefone = "";
         A350Contratada_UF = "";
         A26Municipio_Nome = "";
         A521Pessoa_CEP = "";
         A519Pessoa_Endereco = "";
         A40000Contratada_Logo_GXI = "";
         A1664Contratada_Logo = "";
         A518Pessoa_IE = "";
         A42Contratada_PessoaCNPJ = "";
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV12Websession = context.GetSession();
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockcontratada_tipofabrica_Jsonclick = "";
         lblTextblockcontratada_pessoacnpj_Jsonclick = "";
         lblTextblockpessoa_ie_Jsonclick = "";
         lblTextblockcontratada_logo_Jsonclick = "";
         lblTextblockcontratada_sigla_Jsonclick = "";
         lblTextblockcontratada_banconome_Jsonclick = "";
         lblTextblockcontratada_banconro_Jsonclick = "";
         lblTextblockcontratada_agencianome_Jsonclick = "";
         lblTextblockcontratada_agencianro_Jsonclick = "";
         lblTextblockcontratada_contacorrente_Jsonclick = "";
         lblTextblockpessoa_endereco_Jsonclick = "";
         lblTextblockpessoa_cep_Jsonclick = "";
         lblTextblockmunicipio_nome_Jsonclick = "";
         lblTextblockcontratada_uf_Jsonclick = "";
         lblTextblockpessoa_telefone_Jsonclick = "";
         lblTextblockpessoa_fax_Jsonclick = "";
         lblTextblockcontratada_ss_Jsonclick = "";
         lblTextblockcontratada_os_Jsonclick = "";
         lblTextblockcontratada_lote_Jsonclick = "";
         lblTextblockcontratada_usaosistema_Jsonclick = "";
         lblTextblockcontratada_ospreferencial_Jsonclick = "";
         lblTextblockcontratada_ativo_Jsonclick = "";
         lblContratada_ospreferencial_righttext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA39Contratada_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratadageneral__default(),
            new Object[][] {
                new Object[] {
               H00372_A503Pessoa_MunicipioCod, H00372_n503Pessoa_MunicipioCod, H00372_A52Contratada_AreaTrabalhoCod, H00372_A349Contratada_MunicipioCod, H00372_n349Contratada_MunicipioCod, H00372_A29Contratante_Codigo, H00372_n29Contratante_Codigo, H00372_A39Contratada_Codigo, H00372_A25Municipio_Codigo, H00372_n25Municipio_Codigo,
               H00372_A40Contratada_PessoaCod, H00372_A43Contratada_Ativo, H00372_A1867Contratada_OSPreferencial, H00372_n1867Contratada_OSPreferencial, H00372_A1481Contratada_UsaOSistema, H00372_n1481Contratada_UsaOSistema, H00372_A530Contratada_Lote, H00372_n530Contratada_Lote, H00372_A524Contratada_OS, H00372_n524Contratada_OS,
               H00372_A1451Contratada_SS, H00372_n1451Contratada_SS, H00372_A523Pessoa_Fax, H00372_n523Pessoa_Fax, H00372_A522Pessoa_Telefone, H00372_n522Pessoa_Telefone, H00372_A350Contratada_UF, H00372_n350Contratada_UF, H00372_A26Municipio_Nome, H00372_n26Municipio_Nome,
               H00372_A521Pessoa_CEP, H00372_n521Pessoa_CEP, H00372_A519Pessoa_Endereco, H00372_n519Pessoa_Endereco, H00372_A51Contratada_ContaCorrente, H00372_n51Contratada_ContaCorrente, H00372_A345Contratada_AgenciaNro, H00372_n345Contratada_AgenciaNro, H00372_A344Contratada_AgenciaNome, H00372_n344Contratada_AgenciaNome,
               H00372_A343Contratada_BancoNro, H00372_n343Contratada_BancoNro, H00372_A342Contratada_BancoNome, H00372_n342Contratada_BancoNome, H00372_A438Contratada_Sigla, H00372_A40000Contratada_Logo_GXI, H00372_n40000Contratada_Logo_GXI, H00372_A518Pessoa_IE, H00372_n518Pessoa_IE, H00372_A42Contratada_PessoaCNPJ,
               H00372_n42Contratada_PessoaCNPJ, H00372_A516Contratada_TipoFabrica, H00372_A1664Contratada_Logo, H00372_n1664Contratada_Logo
               }
            }
         );
         AV15Pgmname = "ContratadaGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "ContratadaGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A530Contratada_Lote ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A39Contratada_Codigo ;
      private int wcpOA39Contratada_Codigo ;
      private int A1451Contratada_SS ;
      private int A524Contratada_OS ;
      private int A40Contratada_PessoaCod ;
      private int edtContratada_Codigo_Visible ;
      private int edtContratada_PessoaCod_Visible ;
      private int A503Pessoa_MunicipioCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A349Contratada_MunicipioCod ;
      private int A29Contratante_Codigo ;
      private int A25Municipio_Codigo ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int lblContratada_ospreferencial_righttext_Visible ;
      private int AV7Contratada_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A516Contratada_TipoFabrica ;
      private String A438Contratada_Sigla ;
      private String A342Contratada_BancoNome ;
      private String A343Contratada_BancoNro ;
      private String A344Contratada_AgenciaNome ;
      private String A345Contratada_AgenciaNro ;
      private String A51Contratada_ContaCorrente ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String edtContratada_Codigo_Internalname ;
      private String edtContratada_Codigo_Jsonclick ;
      private String edtContratada_PessoaCod_Internalname ;
      private String edtContratada_PessoaCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkContratada_Ativo_Internalname ;
      private String scmdbuf ;
      private String A523Pessoa_Fax ;
      private String A522Pessoa_Telefone ;
      private String A350Contratada_UF ;
      private String A26Municipio_Nome ;
      private String A521Pessoa_CEP ;
      private String imgContratada_Logo_Internalname ;
      private String A518Pessoa_IE ;
      private String cmbContratada_TipoFabrica_Internalname ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtPessoa_IE_Internalname ;
      private String edtContratada_Sigla_Internalname ;
      private String edtContratada_BancoNome_Internalname ;
      private String edtContratada_BancoNro_Internalname ;
      private String edtContratada_AgenciaNome_Internalname ;
      private String edtContratada_AgenciaNro_Internalname ;
      private String edtContratada_ContaCorrente_Internalname ;
      private String edtPessoa_Endereco_Internalname ;
      private String edtPessoa_CEP_Internalname ;
      private String edtMunicipio_Nome_Internalname ;
      private String edtContratada_UF_Internalname ;
      private String edtPessoa_Telefone_Internalname ;
      private String edtPessoa_Fax_Internalname ;
      private String edtContratada_SS_Internalname ;
      private String edtContratada_OS_Internalname ;
      private String edtContratada_Lote_Internalname ;
      private String cmbContratada_UsaOSistema_Internalname ;
      private String cmbContratada_OSPreferencial_Internalname ;
      private String hsh ;
      private String edtMunicipio_Nome_Link ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String lblContratada_ospreferencial_righttext_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratada_tipofabrica_Internalname ;
      private String lblTextblockcontratada_tipofabrica_Jsonclick ;
      private String cmbContratada_TipoFabrica_Jsonclick ;
      private String lblTextblockcontratada_pessoacnpj_Internalname ;
      private String lblTextblockcontratada_pessoacnpj_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String lblTextblockpessoa_ie_Internalname ;
      private String lblTextblockpessoa_ie_Jsonclick ;
      private String edtPessoa_IE_Jsonclick ;
      private String lblTextblockcontratada_logo_Internalname ;
      private String lblTextblockcontratada_logo_Jsonclick ;
      private String lblTextblockcontratada_sigla_Internalname ;
      private String lblTextblockcontratada_sigla_Jsonclick ;
      private String edtContratada_Sigla_Jsonclick ;
      private String lblTextblockcontratada_banconome_Internalname ;
      private String lblTextblockcontratada_banconome_Jsonclick ;
      private String edtContratada_BancoNome_Jsonclick ;
      private String lblTextblockcontratada_banconro_Internalname ;
      private String lblTextblockcontratada_banconro_Jsonclick ;
      private String edtContratada_BancoNro_Jsonclick ;
      private String lblTextblockcontratada_agencianome_Internalname ;
      private String lblTextblockcontratada_agencianome_Jsonclick ;
      private String edtContratada_AgenciaNome_Jsonclick ;
      private String lblTextblockcontratada_agencianro_Internalname ;
      private String lblTextblockcontratada_agencianro_Jsonclick ;
      private String edtContratada_AgenciaNro_Jsonclick ;
      private String lblTextblockcontratada_contacorrente_Internalname ;
      private String lblTextblockcontratada_contacorrente_Jsonclick ;
      private String edtContratada_ContaCorrente_Jsonclick ;
      private String lblTextblockpessoa_endereco_Internalname ;
      private String lblTextblockpessoa_endereco_Jsonclick ;
      private String edtPessoa_Endereco_Jsonclick ;
      private String lblTextblockpessoa_cep_Internalname ;
      private String lblTextblockpessoa_cep_Jsonclick ;
      private String edtPessoa_CEP_Jsonclick ;
      private String lblTextblockmunicipio_nome_Internalname ;
      private String lblTextblockmunicipio_nome_Jsonclick ;
      private String edtMunicipio_Nome_Jsonclick ;
      private String lblTextblockcontratada_uf_Internalname ;
      private String lblTextblockcontratada_uf_Jsonclick ;
      private String edtContratada_UF_Jsonclick ;
      private String lblTextblockpessoa_telefone_Internalname ;
      private String lblTextblockpessoa_telefone_Jsonclick ;
      private String edtPessoa_Telefone_Jsonclick ;
      private String lblTextblockpessoa_fax_Internalname ;
      private String lblTextblockpessoa_fax_Jsonclick ;
      private String edtPessoa_Fax_Jsonclick ;
      private String lblTextblockcontratada_ss_Internalname ;
      private String lblTextblockcontratada_ss_Jsonclick ;
      private String edtContratada_SS_Jsonclick ;
      private String lblTextblockcontratada_os_Internalname ;
      private String lblTextblockcontratada_os_Jsonclick ;
      private String edtContratada_OS_Jsonclick ;
      private String lblTextblockcontratada_lote_Internalname ;
      private String lblTextblockcontratada_lote_Jsonclick ;
      private String edtContratada_Lote_Jsonclick ;
      private String lblTextblockcontratada_usaosistema_Internalname ;
      private String lblTextblockcontratada_usaosistema_Jsonclick ;
      private String cmbContratada_UsaOSistema_Jsonclick ;
      private String lblTextblockcontratada_ospreferencial_Internalname ;
      private String lblTextblockcontratada_ospreferencial_Jsonclick ;
      private String lblTextblockcontratada_ativo_Internalname ;
      private String lblTextblockcontratada_ativo_Jsonclick ;
      private String tblTablemergedcontratada_ospreferencial_Internalname ;
      private String cmbContratada_OSPreferencial_Jsonclick ;
      private String lblContratada_ospreferencial_righttext_Jsonclick ;
      private String sCtrlA39Contratada_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A1481Contratada_UsaOSistema ;
      private bool A1867Contratada_OSPreferencial ;
      private bool A43Contratada_Ativo ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1481Contratada_UsaOSistema ;
      private bool n1867Contratada_OSPreferencial ;
      private bool n503Pessoa_MunicipioCod ;
      private bool n349Contratada_MunicipioCod ;
      private bool n29Contratante_Codigo ;
      private bool n25Municipio_Codigo ;
      private bool n530Contratada_Lote ;
      private bool n524Contratada_OS ;
      private bool n1451Contratada_SS ;
      private bool n523Pessoa_Fax ;
      private bool n522Pessoa_Telefone ;
      private bool n350Contratada_UF ;
      private bool n26Municipio_Nome ;
      private bool n521Pessoa_CEP ;
      private bool n519Pessoa_Endereco ;
      private bool n51Contratada_ContaCorrente ;
      private bool n345Contratada_AgenciaNro ;
      private bool n344Contratada_AgenciaNome ;
      private bool n343Contratada_BancoNro ;
      private bool n342Contratada_BancoNome ;
      private bool n40000Contratada_Logo_GXI ;
      private bool n518Pessoa_IE ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n1664Contratada_Logo ;
      private bool returnInSub ;
      private bool A1664Contratada_Logo_IsBlob ;
      private String A519Pessoa_Endereco ;
      private String A40000Contratada_Logo_GXI ;
      private String A42Contratada_PessoaCNPJ ;
      private String A1664Contratada_Logo ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratada_TipoFabrica ;
      private GXCombobox cmbContratada_UsaOSistema ;
      private GXCombobox cmbContratada_OSPreferencial ;
      private GXCheckbox chkContratada_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H00372_A503Pessoa_MunicipioCod ;
      private bool[] H00372_n503Pessoa_MunicipioCod ;
      private int[] H00372_A52Contratada_AreaTrabalhoCod ;
      private int[] H00372_A349Contratada_MunicipioCod ;
      private bool[] H00372_n349Contratada_MunicipioCod ;
      private int[] H00372_A29Contratante_Codigo ;
      private bool[] H00372_n29Contratante_Codigo ;
      private int[] H00372_A39Contratada_Codigo ;
      private int[] H00372_A25Municipio_Codigo ;
      private bool[] H00372_n25Municipio_Codigo ;
      private int[] H00372_A40Contratada_PessoaCod ;
      private bool[] H00372_A43Contratada_Ativo ;
      private bool[] H00372_A1867Contratada_OSPreferencial ;
      private bool[] H00372_n1867Contratada_OSPreferencial ;
      private bool[] H00372_A1481Contratada_UsaOSistema ;
      private bool[] H00372_n1481Contratada_UsaOSistema ;
      private short[] H00372_A530Contratada_Lote ;
      private bool[] H00372_n530Contratada_Lote ;
      private int[] H00372_A524Contratada_OS ;
      private bool[] H00372_n524Contratada_OS ;
      private int[] H00372_A1451Contratada_SS ;
      private bool[] H00372_n1451Contratada_SS ;
      private String[] H00372_A523Pessoa_Fax ;
      private bool[] H00372_n523Pessoa_Fax ;
      private String[] H00372_A522Pessoa_Telefone ;
      private bool[] H00372_n522Pessoa_Telefone ;
      private String[] H00372_A350Contratada_UF ;
      private bool[] H00372_n350Contratada_UF ;
      private String[] H00372_A26Municipio_Nome ;
      private bool[] H00372_n26Municipio_Nome ;
      private String[] H00372_A521Pessoa_CEP ;
      private bool[] H00372_n521Pessoa_CEP ;
      private String[] H00372_A519Pessoa_Endereco ;
      private bool[] H00372_n519Pessoa_Endereco ;
      private String[] H00372_A51Contratada_ContaCorrente ;
      private bool[] H00372_n51Contratada_ContaCorrente ;
      private String[] H00372_A345Contratada_AgenciaNro ;
      private bool[] H00372_n345Contratada_AgenciaNro ;
      private String[] H00372_A344Contratada_AgenciaNome ;
      private bool[] H00372_n344Contratada_AgenciaNome ;
      private String[] H00372_A343Contratada_BancoNro ;
      private bool[] H00372_n343Contratada_BancoNro ;
      private String[] H00372_A342Contratada_BancoNome ;
      private bool[] H00372_n342Contratada_BancoNome ;
      private String[] H00372_A438Contratada_Sigla ;
      private String[] H00372_A40000Contratada_Logo_GXI ;
      private bool[] H00372_n40000Contratada_Logo_GXI ;
      private String[] H00372_A518Pessoa_IE ;
      private bool[] H00372_n518Pessoa_IE ;
      private String[] H00372_A42Contratada_PessoaCNPJ ;
      private bool[] H00372_n42Contratada_PessoaCNPJ ;
      private String[] H00372_A516Contratada_TipoFabrica ;
      private String[] H00372_A1664Contratada_Logo ;
      private bool[] H00372_n1664Contratada_Logo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV12Websession ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class contratadageneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00372 ;
          prmH00372 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00372", "SELECT T5.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[Contratada_MunicipioCod] AS Contratada_MunicipioCod, T2.[Contratante_Codigo], T1.[Contratada_Codigo], T3.[Municipio_Codigo], T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_Ativo], T1.[Contratada_OSPreferencial], T1.[Contratada_UsaOSistema], T1.[Contratada_Lote], T1.[Contratada_OS], T1.[Contratada_SS], T5.[Pessoa_Fax], T5.[Pessoa_Telefone], T4.[Estado_UF] AS Contratada_UF, T6.[Municipio_Nome], T5.[Pessoa_CEP], T5.[Pessoa_Endereco], T1.[Contratada_ContaCorrente], T1.[Contratada_AgenciaNro], T1.[Contratada_AgenciaNome], T1.[Contratada_BancoNro], T1.[Contratada_BancoNome], T1.[Contratada_Sigla], T1.[Contratada_Logo_GXI], T5.[Pessoa_IE], T5.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[Contratada_TipoFabrica], T1.[Contratada_Logo] FROM ((((([Contratada] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Contratante] T3 WITH (NOLOCK) ON T3.[Contratante_Codigo] = T2.[Contratante_Codigo]) LEFT JOIN [Municipio] T4 WITH (NOLOCK) ON T4.[Municipio_Codigo] = T1.[Contratada_MunicipioCod]) INNER JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) LEFT JOIN [Municipio] T6 WITH (NOLOCK) ON T6.[Municipio_Codigo] = T5.[Pessoa_MunicipioCod]) WHERE T1.[Contratada_Codigo] = @Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00372,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((bool[]) buf[12])[0] = rslt.getBool(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((short[]) buf[16])[0] = rslt.getShort(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((int[]) buf[20])[0] = rslt.getInt(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((String[]) buf[22])[0] = rslt.getString(14, 15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((String[]) buf[24])[0] = rslt.getString(15, 15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((String[]) buf[26])[0] = rslt.getString(16, 2) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((String[]) buf[28])[0] = rslt.getString(17, 50) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((String[]) buf[30])[0] = rslt.getString(18, 10) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((String[]) buf[32])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((String[]) buf[34])[0] = rslt.getString(20, 10) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                ((String[]) buf[36])[0] = rslt.getString(21, 10) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(21);
                ((String[]) buf[38])[0] = rslt.getString(22, 50) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(22);
                ((String[]) buf[40])[0] = rslt.getString(23, 6) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(23);
                ((String[]) buf[42])[0] = rslt.getString(24, 50) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(24);
                ((String[]) buf[44])[0] = rslt.getString(25, 15) ;
                ((String[]) buf[45])[0] = rslt.getMultimediaUri(26) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((String[]) buf[47])[0] = rslt.getString(27, 15) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(27);
                ((String[]) buf[49])[0] = rslt.getVarchar(28) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(28);
                ((String[]) buf[51])[0] = rslt.getString(29, 1) ;
                ((String[]) buf[52])[0] = rslt.getMultimediaFile(30, rslt.getVarchar(26)) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(30);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
