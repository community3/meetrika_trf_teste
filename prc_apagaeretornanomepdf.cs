/*
               File: PRC_ApagaERetornaNomePDF
        Description: PRC_Apaga ERetorna Nome PDF
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:36.38
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_apagaeretornanomepdf : GXProcedure
   {
      public prc_apagaeretornanomepdf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_apagaeretornanomepdf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_FileName ,
                           int aP1_Usuario_Codigo ,
                           String aP2_AbsolutePath ,
                           out String aP3_retFileNameFull ,
                           out String aP4_retFileNameFullPdf ,
                           out String aP5_retFileName )
      {
         this.AV10FileName = aP0_FileName;
         this.AV14Usuario_Codigo = aP1_Usuario_Codigo;
         this.AV8AbsolutePath = aP2_AbsolutePath;
         this.AV15retFileNameFull = "" ;
         this.AV13retFileNameFullPdf = "" ;
         this.AV12retFileName = "" ;
         initialize();
         executePrivate();
         aP3_retFileNameFull=this.AV15retFileNameFull;
         aP4_retFileNameFullPdf=this.AV13retFileNameFullPdf;
         aP5_retFileName=this.AV12retFileName;
      }

      public String executeUdp( String aP0_FileName ,
                                int aP1_Usuario_Codigo ,
                                String aP2_AbsolutePath ,
                                out String aP3_retFileNameFull ,
                                out String aP4_retFileNameFullPdf )
      {
         this.AV10FileName = aP0_FileName;
         this.AV14Usuario_Codigo = aP1_Usuario_Codigo;
         this.AV8AbsolutePath = aP2_AbsolutePath;
         this.AV15retFileNameFull = "" ;
         this.AV13retFileNameFullPdf = "" ;
         this.AV12retFileName = "" ;
         initialize();
         executePrivate();
         aP3_retFileNameFull=this.AV15retFileNameFull;
         aP4_retFileNameFullPdf=this.AV13retFileNameFullPdf;
         aP5_retFileName=this.AV12retFileName;
         return AV12retFileName ;
      }

      public void executeSubmit( String aP0_FileName ,
                                 int aP1_Usuario_Codigo ,
                                 String aP2_AbsolutePath ,
                                 out String aP3_retFileNameFull ,
                                 out String aP4_retFileNameFullPdf ,
                                 out String aP5_retFileName )
      {
         prc_apagaeretornanomepdf objprc_apagaeretornanomepdf;
         objprc_apagaeretornanomepdf = new prc_apagaeretornanomepdf();
         objprc_apagaeretornanomepdf.AV10FileName = aP0_FileName;
         objprc_apagaeretornanomepdf.AV14Usuario_Codigo = aP1_Usuario_Codigo;
         objprc_apagaeretornanomepdf.AV8AbsolutePath = aP2_AbsolutePath;
         objprc_apagaeretornanomepdf.AV15retFileNameFull = "" ;
         objprc_apagaeretornanomepdf.AV13retFileNameFullPdf = "" ;
         objprc_apagaeretornanomepdf.AV12retFileName = "" ;
         objprc_apagaeretornanomepdf.context.SetSubmitInitialConfig(context);
         objprc_apagaeretornanomepdf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_apagaeretornanomepdf);
         aP3_retFileNameFull=this.AV15retFileNameFull;
         aP4_retFileNameFullPdf=this.AV13retFileNameFullPdf;
         aP5_retFileName=this.AV12retFileName;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_apagaeretornanomepdf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV12retFileName = StringUtil.Trim( AV10FileName) + "_User" + StringUtil.Trim( StringUtil.Str( (decimal)(AV14Usuario_Codigo), 6, 0)) + ".pdf";
         AV11FileNameFull = StringUtil.Trim( AV8AbsolutePath) + AV12retFileName;
         AV13retFileNameFullPdf = "PDF\\" + AV12retFileName;
         AV9File.Source = AV11FileNameFull;
         if ( AV9File.Exists() )
         {
            AV9File.Delete();
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV11FileNameFull = "";
         AV9File = new GxFile(context.GetPhysicalPath());
         AV15retFileNameFull = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV14Usuario_Codigo ;
      private String AV10FileName ;
      private String AV12retFileName ;
      private String AV11FileNameFull ;
      private String AV13retFileNameFullPdf ;
      private String AV15retFileNameFull ;
      private String AV8AbsolutePath ;
      private String aP3_retFileNameFull ;
      private String aP4_retFileNameFullPdf ;
      private String aP5_retFileName ;
      private GxFile AV9File ;
   }

}
