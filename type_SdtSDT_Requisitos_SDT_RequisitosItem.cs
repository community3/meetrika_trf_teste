/*
               File: type_SdtSDT_Requisitos_SDT_RequisitosItem
        Description: SDT_Requisitos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:7.92
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Requisitos.SDT_RequisitosItem" )]
   [XmlType(TypeName =  "SDT_Requisitos.SDT_RequisitosItem" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSDT_Requisitos_SDT_RequisitosItem : GxUserType
   {
      public SdtSDT_Requisitos_SDT_RequisitosItem( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_identificador = "";
         gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_titulo = "";
         gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_descricao = "";
         gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_agrupador = "";
         gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_referenciatecnica = "";
         gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_restricao = "";
      }

      public SdtSDT_Requisitos_SDT_RequisitosItem( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Requisitos_SDT_RequisitosItem deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Requisitos_SDT_RequisitosItem)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Requisitos_SDT_RequisitosItem obj ;
         obj = this;
         obj.gxTpr_Requisito_codigo = deserialized.gxTpr_Requisito_codigo;
         obj.gxTpr_Requisito_identificador = deserialized.gxTpr_Requisito_identificador;
         obj.gxTpr_Requisito_titulo = deserialized.gxTpr_Requisito_titulo;
         obj.gxTpr_Requisito_descricao = deserialized.gxTpr_Requisito_descricao;
         obj.gxTpr_Requisito_prioridade = deserialized.gxTpr_Requisito_prioridade;
         obj.gxTpr_Requisito_agrupador = deserialized.gxTpr_Requisito_agrupador;
         obj.gxTpr_Requisito_pontuacao = deserialized.gxTpr_Requisito_pontuacao;
         obj.gxTpr_Requisito_status = deserialized.gxTpr_Requisito_status;
         obj.gxTpr_Requisito_ordem = deserialized.gxTpr_Requisito_ordem;
         obj.gxTpr_Requisito_sequencia = deserialized.gxTpr_Requisito_sequencia;
         obj.gxTpr_Requisito_editando = deserialized.gxTpr_Requisito_editando;
         obj.gxTpr_Requisito_tipo = deserialized.gxTpr_Requisito_tipo;
         obj.gxTpr_Requisito_tiporeqcod = deserialized.gxTpr_Requisito_tiporeqcod;
         obj.gxTpr_Requisito_referenciatecnica = deserialized.gxTpr_Requisito_referenciatecnica;
         obj.gxTpr_Requisito_restricao = deserialized.gxTpr_Requisito_restricao;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Codigo") )
               {
                  gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Identificador") )
               {
                  gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_identificador = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Titulo") )
               {
                  gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_titulo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Descricao") )
               {
                  gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Prioridade") )
               {
                  gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_prioridade = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Agrupador") )
               {
                  gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_agrupador = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Pontuacao") )
               {
                  gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_pontuacao = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Status") )
               {
                  gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_status = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Ordem") )
               {
                  gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_ordem = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Sequencia") )
               {
                  gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_sequencia = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Editando") )
               {
                  gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_editando = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Tipo") )
               {
                  gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_tipo = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_TipoReqCod") )
               {
                  gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_tiporeqcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_ReferenciaTecnica") )
               {
                  gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_referenciatecnica = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Restricao") )
               {
                  gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_restricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Requisitos.SDT_RequisitosItem";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Requisito_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Identificador", StringUtil.RTrim( gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_identificador));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Titulo", StringUtil.RTrim( gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_titulo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Descricao", StringUtil.RTrim( gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Prioridade", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_prioridade), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Agrupador", StringUtil.RTrim( gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_agrupador));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Pontuacao", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_pontuacao, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Status", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_status), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Ordem", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_ordem), 3, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Sequencia", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_sequencia), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Editando", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_editando)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Tipo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_tipo), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_TipoReqCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_tiporeqcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_ReferenciaTecnica", StringUtil.RTrim( gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_referenciatecnica));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Restricao", StringUtil.RTrim( gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_restricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Requisito_Codigo", gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_codigo, false);
         AddObjectProperty("Requisito_Identificador", gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_identificador, false);
         AddObjectProperty("Requisito_Titulo", gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_titulo, false);
         AddObjectProperty("Requisito_Descricao", gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_descricao, false);
         AddObjectProperty("Requisito_Prioridade", gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_prioridade, false);
         AddObjectProperty("Requisito_Agrupador", gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_agrupador, false);
         AddObjectProperty("Requisito_Pontuacao", gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_pontuacao, false);
         AddObjectProperty("Requisito_Status", gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_status, false);
         AddObjectProperty("Requisito_Ordem", gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_ordem, false);
         AddObjectProperty("Requisito_Sequencia", gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_sequencia, false);
         AddObjectProperty("Requisito_Editando", gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_editando, false);
         AddObjectProperty("Requisito_Tipo", gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_tipo, false);
         AddObjectProperty("Requisito_TipoReqCod", gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_tiporeqcod, false);
         AddObjectProperty("Requisito_ReferenciaTecnica", gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_referenciatecnica, false);
         AddObjectProperty("Requisito_Restricao", gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_restricao, false);
         return  ;
      }

      [  SoapElement( ElementName = "Requisito_Codigo" )]
      [  XmlElement( ElementName = "Requisito_Codigo"   )]
      public int gxTpr_Requisito_codigo
      {
         get {
            return gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_codigo ;
         }

         set {
            gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_Identificador" )]
      [  XmlElement( ElementName = "Requisito_Identificador"   )]
      public String gxTpr_Requisito_identificador
      {
         get {
            return gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_identificador ;
         }

         set {
            gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_identificador = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_Titulo" )]
      [  XmlElement( ElementName = "Requisito_Titulo"   )]
      public String gxTpr_Requisito_titulo
      {
         get {
            return gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_titulo ;
         }

         set {
            gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_titulo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_Descricao" )]
      [  XmlElement( ElementName = "Requisito_Descricao"   )]
      public String gxTpr_Requisito_descricao
      {
         get {
            return gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_descricao ;
         }

         set {
            gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_Prioridade" )]
      [  XmlElement( ElementName = "Requisito_Prioridade"   )]
      public short gxTpr_Requisito_prioridade
      {
         get {
            return gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_prioridade ;
         }

         set {
            gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_prioridade = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_Agrupador" )]
      [  XmlElement( ElementName = "Requisito_Agrupador"   )]
      public String gxTpr_Requisito_agrupador
      {
         get {
            return gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_agrupador ;
         }

         set {
            gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_agrupador = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_Pontuacao" )]
      [  XmlElement( ElementName = "Requisito_Pontuacao"   )]
      public double gxTpr_Requisito_pontuacao_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_pontuacao) ;
         }

         set {
            gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_pontuacao = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Requisito_pontuacao
      {
         get {
            return gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_pontuacao ;
         }

         set {
            gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_pontuacao = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_Status" )]
      [  XmlElement( ElementName = "Requisito_Status"   )]
      public short gxTpr_Requisito_status
      {
         get {
            return gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_status ;
         }

         set {
            gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_status = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_Ordem" )]
      [  XmlElement( ElementName = "Requisito_Ordem"   )]
      public short gxTpr_Requisito_ordem
      {
         get {
            return gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_ordem ;
         }

         set {
            gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_ordem = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_Sequencia" )]
      [  XmlElement( ElementName = "Requisito_Sequencia"   )]
      public short gxTpr_Requisito_sequencia
      {
         get {
            return gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_sequencia ;
         }

         set {
            gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_sequencia = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_Editando" )]
      [  XmlElement( ElementName = "Requisito_Editando"   )]
      public bool gxTpr_Requisito_editando
      {
         get {
            return gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_editando ;
         }

         set {
            gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_editando = value;
         }

      }

      [  SoapElement( ElementName = "Requisito_Tipo" )]
      [  XmlElement( ElementName = "Requisito_Tipo"   )]
      public short gxTpr_Requisito_tipo
      {
         get {
            return gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_tipo ;
         }

         set {
            gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_tipo = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_TipoReqCod" )]
      [  XmlElement( ElementName = "Requisito_TipoReqCod"   )]
      public int gxTpr_Requisito_tiporeqcod
      {
         get {
            return gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_tiporeqcod ;
         }

         set {
            gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_tiporeqcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_ReferenciaTecnica" )]
      [  XmlElement( ElementName = "Requisito_ReferenciaTecnica"   )]
      public String gxTpr_Requisito_referenciatecnica
      {
         get {
            return gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_referenciatecnica ;
         }

         set {
            gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_referenciatecnica = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_Restricao" )]
      [  XmlElement( ElementName = "Requisito_Restricao"   )]
      public String gxTpr_Requisito_restricao
      {
         get {
            return gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_restricao ;
         }

         set {
            gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_restricao = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_identificador = "";
         gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_titulo = "";
         gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_descricao = "";
         gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_agrupador = "";
         gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_referenciatecnica = "";
         gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_restricao = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_prioridade ;
      protected short gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_status ;
      protected short gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_ordem ;
      protected short gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_sequencia ;
      protected short gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_tipo ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_codigo ;
      protected int gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_tiporeqcod ;
      protected decimal gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_pontuacao ;
      protected String sTagName ;
      protected bool gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_editando ;
      protected String gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_descricao ;
      protected String gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_referenciatecnica ;
      protected String gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_restricao ;
      protected String gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_identificador ;
      protected String gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_titulo ;
      protected String gxTv_SdtSDT_Requisitos_SDT_RequisitosItem_Requisito_agrupador ;
   }

   [DataContract(Name = @"SDT_Requisitos.SDT_RequisitosItem", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_Requisitos_SDT_RequisitosItem_RESTInterface : GxGenericCollectionItem<SdtSDT_Requisitos_SDT_RequisitosItem>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Requisitos_SDT_RequisitosItem_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Requisitos_SDT_RequisitosItem_RESTInterface( SdtSDT_Requisitos_SDT_RequisitosItem psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Requisito_Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Requisito_codigo
      {
         get {
            return sdt.gxTpr_Requisito_codigo ;
         }

         set {
            sdt.gxTpr_Requisito_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Requisito_Identificador" , Order = 1 )]
      public String gxTpr_Requisito_identificador
      {
         get {
            return sdt.gxTpr_Requisito_identificador ;
         }

         set {
            sdt.gxTpr_Requisito_identificador = (String)(value);
         }

      }

      [DataMember( Name = "Requisito_Titulo" , Order = 2 )]
      public String gxTpr_Requisito_titulo
      {
         get {
            return sdt.gxTpr_Requisito_titulo ;
         }

         set {
            sdt.gxTpr_Requisito_titulo = (String)(value);
         }

      }

      [DataMember( Name = "Requisito_Descricao" , Order = 3 )]
      public String gxTpr_Requisito_descricao
      {
         get {
            return sdt.gxTpr_Requisito_descricao ;
         }

         set {
            sdt.gxTpr_Requisito_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Requisito_Prioridade" , Order = 4 )]
      public Nullable<short> gxTpr_Requisito_prioridade
      {
         get {
            return sdt.gxTpr_Requisito_prioridade ;
         }

         set {
            sdt.gxTpr_Requisito_prioridade = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Requisito_Agrupador" , Order = 5 )]
      public String gxTpr_Requisito_agrupador
      {
         get {
            return sdt.gxTpr_Requisito_agrupador ;
         }

         set {
            sdt.gxTpr_Requisito_agrupador = (String)(value);
         }

      }

      [DataMember( Name = "Requisito_Pontuacao" , Order = 6 )]
      public String gxTpr_Requisito_pontuacao
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Requisito_pontuacao, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Requisito_pontuacao = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Requisito_Status" , Order = 7 )]
      public Nullable<short> gxTpr_Requisito_status
      {
         get {
            return sdt.gxTpr_Requisito_status ;
         }

         set {
            sdt.gxTpr_Requisito_status = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Requisito_Ordem" , Order = 8 )]
      public Nullable<short> gxTpr_Requisito_ordem
      {
         get {
            return sdt.gxTpr_Requisito_ordem ;
         }

         set {
            sdt.gxTpr_Requisito_ordem = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Requisito_Sequencia" , Order = 9 )]
      public Nullable<short> gxTpr_Requisito_sequencia
      {
         get {
            return sdt.gxTpr_Requisito_sequencia ;
         }

         set {
            sdt.gxTpr_Requisito_sequencia = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Requisito_Editando" , Order = 10 )]
      public bool gxTpr_Requisito_editando
      {
         get {
            return sdt.gxTpr_Requisito_editando ;
         }

         set {
            sdt.gxTpr_Requisito_editando = value;
         }

      }

      [DataMember( Name = "Requisito_Tipo" , Order = 11 )]
      public Nullable<short> gxTpr_Requisito_tipo
      {
         get {
            return sdt.gxTpr_Requisito_tipo ;
         }

         set {
            sdt.gxTpr_Requisito_tipo = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Requisito_TipoReqCod" , Order = 12 )]
      public Nullable<int> gxTpr_Requisito_tiporeqcod
      {
         get {
            return sdt.gxTpr_Requisito_tiporeqcod ;
         }

         set {
            sdt.gxTpr_Requisito_tiporeqcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Requisito_ReferenciaTecnica" , Order = 13 )]
      public String gxTpr_Requisito_referenciatecnica
      {
         get {
            return sdt.gxTpr_Requisito_referenciatecnica ;
         }

         set {
            sdt.gxTpr_Requisito_referenciatecnica = (String)(value);
         }

      }

      [DataMember( Name = "Requisito_Restricao" , Order = 14 )]
      public String gxTpr_Requisito_restricao
      {
         get {
            return sdt.gxTpr_Requisito_restricao ;
         }

         set {
            sdt.gxTpr_Requisito_restricao = (String)(value);
         }

      }

      public SdtSDT_Requisitos_SDT_RequisitosItem sdt
      {
         get {
            return (SdtSDT_Requisitos_SDT_RequisitosItem)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Requisitos_SDT_RequisitosItem() ;
         }
      }

   }

}
