/*
               File: MigrarRequisitos
        Description: Migrar Requisitos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:14:13.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class migrarrequisitos : GXProcedure
   {
      public migrarrequisitos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public migrarrequisitos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         migrarrequisitos objmigrarrequisitos;
         objmigrarrequisitos = new migrarrequisitos();
         objmigrarrequisitos.context.SetSubmitInitialConfig(context);
         objmigrarrequisitos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objmigrarrequisitos);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((migrarrequisitos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00VK2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1947SolicServicoReqNeg_ReqNeqCod = P00VK2_A1947SolicServicoReqNeg_ReqNeqCod[0];
            n1947SolicServicoReqNeg_ReqNeqCod = P00VK2_n1947SolicServicoReqNeg_ReqNeqCod[0];
            A1895SolicServicoReqNeg_Codigo = P00VK2_A1895SolicServicoReqNeg_Codigo[0];
            A1912SolicServicoReqNeg_Agrupador = P00VK2_A1912SolicServicoReqNeg_Agrupador[0];
            A1910SolicServicoReqNeg_Descricao = P00VK2_A1910SolicServicoReqNeg_Descricao[0];
            A1350ContagemResultado_DataCadastro = P00VK2_A1350ContagemResultado_DataCadastro[0];
            n1350ContagemResultado_DataCadastro = P00VK2_n1350ContagemResultado_DataCadastro[0];
            A1915SolicServicoReqNeg_Identificador = P00VK2_A1915SolicServicoReqNeg_Identificador[0];
            A1909SolicServicoReqNeg_Nome = P00VK2_A1909SolicServicoReqNeg_Nome[0];
            A1911SolicServicoReqNeg_Prioridade = P00VK2_A1911SolicServicoReqNeg_Prioridade[0];
            A1914SolicServicoReqNeg_Situacao = P00VK2_A1914SolicServicoReqNeg_Situacao[0];
            A1913SolicServicoReqNeg_Tamanho = P00VK2_A1913SolicServicoReqNeg_Tamanho[0];
            A456ContagemResultado_Codigo = P00VK2_A456ContagemResultado_Codigo[0];
            A508ContagemResultado_Owner = P00VK2_A508ContagemResultado_Owner[0];
            A1350ContagemResultado_DataCadastro = P00VK2_A1350ContagemResultado_DataCadastro[0];
            n1350ContagemResultado_DataCadastro = P00VK2_n1350ContagemResultado_DataCadastro[0];
            A508ContagemResultado_Owner = P00VK2_A508ContagemResultado_Owner[0];
            OV11Requisito_Cadastro = AV11Requisito_Cadastro;
            AV28SolicServicoReqNeg_Codigo = A1895SolicServicoReqNeg_Codigo;
            AV8Requisito_Agrupador = A1912SolicServicoReqNeg_Agrupador;
            AV10Requisito_Descricao = A1910SolicServicoReqNeg_Descricao;
            AV11Requisito_Cadastro = A1350ContagemResultado_DataCadastro;
            AV12Requisito_Identificador = A1915SolicServicoReqNeg_Identificador;
            AV19Requisito_Titulo = A1909SolicServicoReqNeg_Nome;
            AV17Requisito_Prioridade = A1911SolicServicoReqNeg_Prioridade;
            if ( A1914SolicServicoReqNeg_Situacao == 1 )
            {
               AV21Requisito_Status = 1;
            }
            else if ( A1914SolicServicoReqNeg_Situacao == 2 )
            {
               AV21Requisito_Status = 2;
            }
            AV23Requisito_ReqCod = (int)(A1947SolicServicoReqNeg_ReqNeqCod);
            AV16Requisito_Pontuacao = A1913SolicServicoReqNeg_Tamanho;
            AV34Requisito_TipoReqCod = A2049Requisito_TipoReqCod;
            AV27OSCod = A456ContagemResultado_Codigo;
            AV26OwnerCod = A508ContagemResultado_Owner;
            /* Execute user subroutine: 'NEWREQUISITO' */
            S111 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               this.cleanup();
               if (true) return;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'NEWREQUISITO' Routine */
         /*
            INSERT RECORD ON TABLE Requisito

         */
         A1926Requisito_Agrupador = AV8Requisito_Agrupador;
         n1926Requisito_Agrupador = false;
         A1943Requisito_Cadastro = AV11Requisito_Cadastro;
         n1943Requisito_Cadastro = false;
         A1923Requisito_Descricao = AV10Requisito_Descricao;
         n1923Requisito_Descricao = false;
         A2001Requisito_Identificador = AV12Requisito_Identificador;
         n2001Requisito_Identificador = false;
         A1931Requisito_Ordem = 0;
         n1931Requisito_Ordem = false;
         A1932Requisito_Pontuacao = AV16Requisito_Pontuacao;
         n1932Requisito_Pontuacao = false;
         A2002Requisito_Prioridade = AV17Requisito_Prioridade;
         n2002Requisito_Prioridade = false;
         A1934Requisito_Status = AV21Requisito_Status;
         A1927Requisito_Titulo = AV19Requisito_Titulo;
         n1927Requisito_Titulo = false;
         if ( (0==AV34Requisito_TipoReqCod) )
         {
            A2049Requisito_TipoReqCod = 0;
            n2049Requisito_TipoReqCod = false;
            n2049Requisito_TipoReqCod = true;
         }
         else
         {
            A2049Requisito_TipoReqCod = AV34Requisito_TipoReqCod;
            n2049Requisito_TipoReqCod = false;
         }
         A1999Requisito_ReqCod = 0;
         n1999Requisito_ReqCod = false;
         n1999Requisito_ReqCod = true;
         A1935Requisito_Ativo = true;
         /* Using cursor P00VK3 */
         pr_default.execute(1, new Object[] {n1923Requisito_Descricao, A1923Requisito_Descricao, n1926Requisito_Agrupador, A1926Requisito_Agrupador, n1927Requisito_Titulo, A1927Requisito_Titulo, n1931Requisito_Ordem, A1931Requisito_Ordem, n1932Requisito_Pontuacao, A1932Requisito_Pontuacao, A1934Requisito_Status, A1935Requisito_Ativo, n1943Requisito_Cadastro, A1943Requisito_Cadastro, n1999Requisito_ReqCod, A1999Requisito_ReqCod, n2001Requisito_Identificador, A2001Requisito_Identificador, n2002Requisito_Prioridade, A2002Requisito_Prioridade, n2049Requisito_TipoReqCod, A2049Requisito_TipoReqCod});
         A1919Requisito_Codigo = P00VK3_A1919Requisito_Codigo[0];
         pr_default.close(1);
         dsDefault.SmartCacheProvider.SetUpdated("Requisito") ;
         if ( (pr_default.getStatus(1) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         AV31NewRequisito_Codigo = A1919Requisito_Codigo;
         AV24Requisito_Codigo = AV31NewRequisito_Codigo;
         /* Execute user subroutine: 'NEWREQUISITOOS' */
         S121 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'RELACIONADOS' */
         S131 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'CLONADO' */
         S141 ();
         if (returnInSub) return;
      }

      protected void S141( )
      {
         /* 'CLONADO' Routine */
         /* Using cursor P00VK4 */
         pr_default.execute(2, new Object[] {AV28SolicServicoReqNeg_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A1947SolicServicoReqNeg_ReqNeqCod = P00VK4_A1947SolicServicoReqNeg_ReqNeqCod[0];
            n1947SolicServicoReqNeg_ReqNeqCod = P00VK4_n1947SolicServicoReqNeg_ReqNeqCod[0];
            A1895SolicServicoReqNeg_Codigo = P00VK4_A1895SolicServicoReqNeg_Codigo[0];
            A456ContagemResultado_Codigo = P00VK4_A456ContagemResultado_Codigo[0];
            A508ContagemResultado_Owner = P00VK4_A508ContagemResultado_Owner[0];
            A508ContagemResultado_Owner = P00VK4_A508ContagemResultado_Owner[0];
            AV28SolicServicoReqNeg_Codigo = A1895SolicServicoReqNeg_Codigo;
            AV27OSCod = A456ContagemResultado_Codigo;
            AV26OwnerCod = A508ContagemResultado_Owner;
            /* Execute user subroutine: 'NEWREQUISITOOS' */
            S121 ();
            if ( returnInSub )
            {
               pr_default.close(2);
               returnInSub = true;
               if (true) return;
            }
            /* Execute user subroutine: 'RELACIONADOS' */
            S131 ();
            if ( returnInSub )
            {
               pr_default.close(2);
               returnInSub = true;
               if (true) return;
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void S131( )
      {
         /* 'RELACIONADOS' Routine */
         /* Using cursor P00VK5 */
         pr_default.execute(3, new Object[] {AV28SolicServicoReqNeg_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A456ContagemResultado_Codigo = P00VK5_A456ContagemResultado_Codigo[0];
            A1946ReqNegReqTec_OSCod = P00VK5_A1946ReqNegReqTec_OSCod[0];
            n1946ReqNegReqTec_OSCod = P00VK5_n1946ReqNegReqTec_OSCod[0];
            A890ContagemResultado_Responsavel = P00VK5_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00VK5_n890ContagemResultado_Responsavel[0];
            A1895SolicServicoReqNeg_Codigo = P00VK5_A1895SolicServicoReqNeg_Codigo[0];
            A1919Requisito_Codigo = P00VK5_A1919Requisito_Codigo[0];
            A456ContagemResultado_Codigo = P00VK5_A456ContagemResultado_Codigo[0];
            A890ContagemResultado_Responsavel = P00VK5_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00VK5_n890ContagemResultado_Responsavel[0];
            AV24Requisito_Codigo = A1919Requisito_Codigo;
            AV27OSCod = A1946ReqNegReqTec_OSCod;
            /* Using cursor P00VK6 */
            pr_default.execute(4, new Object[] {n1946ReqNegReqTec_OSCod, A1946ReqNegReqTec_OSCod});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A456ContagemResultado_Codigo = P00VK6_A456ContagemResultado_Codigo[0];
               AV26OwnerCod = A890ContagemResultado_Responsavel;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(4);
            /* Execute user subroutine: 'ATUALIZADEPENDENCIA' */
            S155 ();
            if ( returnInSub )
            {
               pr_default.close(3);
               returnInSub = true;
               if (true) return;
            }
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void S155( )
      {
         /* 'ATUALIZADEPENDENCIA' Routine */
         /* Using cursor P00VK7 */
         pr_default.execute(5, new Object[] {AV24Requisito_Codigo});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A1919Requisito_Codigo = P00VK7_A1919Requisito_Codigo[0];
            A1999Requisito_ReqCod = P00VK7_A1999Requisito_ReqCod[0];
            n1999Requisito_ReqCod = P00VK7_n1999Requisito_ReqCod[0];
            A1999Requisito_ReqCod = AV31NewRequisito_Codigo;
            n1999Requisito_ReqCod = false;
            /* Execute user subroutine: 'NEWREQUISITOOS' */
            S121 ();
            if ( returnInSub )
            {
               pr_default.close(5);
               returnInSub = true;
               if (true) return;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00VK8 */
            pr_default.execute(6, new Object[] {n1999Requisito_ReqCod, A1999Requisito_ReqCod, A1919Requisito_Codigo});
            pr_default.close(6);
            dsDefault.SmartCacheProvider.SetUpdated("Requisito") ;
            if (true) break;
            /* Using cursor P00VK9 */
            pr_default.execute(7, new Object[] {n1999Requisito_ReqCod, A1999Requisito_ReqCod, A1919Requisito_Codigo});
            pr_default.close(7);
            dsDefault.SmartCacheProvider.SetUpdated("Requisito") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(5);
      }

      protected void S121( )
      {
         /* 'NEWREQUISITOOS' Routine */
         /*
            INSERT RECORD ON TABLE ContagemResultadoRequisito

         */
         A2003ContagemResultadoRequisito_OSCod = AV27OSCod;
         A2004ContagemResultadoRequisito_ReqCod = AV24Requisito_Codigo;
         A2006ContagemResultadoRequisito_Owner = (bool)(Convert.ToBoolean(AV26OwnerCod));
         /* Using cursor P00VK10 */
         pr_default.execute(8, new Object[] {A2003ContagemResultadoRequisito_OSCod, A2004ContagemResultadoRequisito_ReqCod, A2006ContagemResultadoRequisito_Owner});
         A2005ContagemResultadoRequisito_Codigo = P00VK10_A2005ContagemResultadoRequisito_Codigo[0];
         pr_default.close(8);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoRequisito") ;
         if ( (pr_default.getStatus(8) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "MigrarRequisitos");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00VK2_A1947SolicServicoReqNeg_ReqNeqCod = new long[1] ;
         P00VK2_n1947SolicServicoReqNeg_ReqNeqCod = new bool[] {false} ;
         P00VK2_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         P00VK2_A1912SolicServicoReqNeg_Agrupador = new String[] {""} ;
         P00VK2_A1910SolicServicoReqNeg_Descricao = new String[] {""} ;
         P00VK2_A1350ContagemResultado_DataCadastro = new DateTime[] {DateTime.MinValue} ;
         P00VK2_n1350ContagemResultado_DataCadastro = new bool[] {false} ;
         P00VK2_A1915SolicServicoReqNeg_Identificador = new String[] {""} ;
         P00VK2_A1909SolicServicoReqNeg_Nome = new String[] {""} ;
         P00VK2_A1911SolicServicoReqNeg_Prioridade = new short[1] ;
         P00VK2_A1914SolicServicoReqNeg_Situacao = new short[1] ;
         P00VK2_A1913SolicServicoReqNeg_Tamanho = new decimal[1] ;
         P00VK2_A456ContagemResultado_Codigo = new int[1] ;
         P00VK2_A508ContagemResultado_Owner = new int[1] ;
         A1912SolicServicoReqNeg_Agrupador = "";
         A1910SolicServicoReqNeg_Descricao = "";
         A1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         A1915SolicServicoReqNeg_Identificador = "";
         A1909SolicServicoReqNeg_Nome = "";
         OV11Requisito_Cadastro = (DateTime)(DateTime.MinValue);
         AV11Requisito_Cadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV8Requisito_Agrupador = "";
         AV10Requisito_Descricao = "";
         AV12Requisito_Identificador = "";
         AV19Requisito_Titulo = "";
         A1926Requisito_Agrupador = "";
         A1943Requisito_Cadastro = (DateTime)(DateTime.MinValue);
         A1923Requisito_Descricao = "";
         A2001Requisito_Identificador = "";
         A1927Requisito_Titulo = "";
         P00VK3_A1919Requisito_Codigo = new int[1] ;
         Gx_emsg = "";
         P00VK4_A1947SolicServicoReqNeg_ReqNeqCod = new long[1] ;
         P00VK4_n1947SolicServicoReqNeg_ReqNeqCod = new bool[] {false} ;
         P00VK4_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         P00VK4_A456ContagemResultado_Codigo = new int[1] ;
         P00VK4_A508ContagemResultado_Owner = new int[1] ;
         P00VK5_A456ContagemResultado_Codigo = new int[1] ;
         P00VK5_A1946ReqNegReqTec_OSCod = new int[1] ;
         P00VK5_n1946ReqNegReqTec_OSCod = new bool[] {false} ;
         P00VK5_A890ContagemResultado_Responsavel = new int[1] ;
         P00VK5_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00VK5_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         P00VK5_A1919Requisito_Codigo = new int[1] ;
         P00VK6_A456ContagemResultado_Codigo = new int[1] ;
         P00VK7_A1919Requisito_Codigo = new int[1] ;
         P00VK7_A1999Requisito_ReqCod = new int[1] ;
         P00VK7_n1999Requisito_ReqCod = new bool[] {false} ;
         P00VK10_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.migrarrequisitos__default(),
            new Object[][] {
                new Object[] {
               P00VK2_A1947SolicServicoReqNeg_ReqNeqCod, P00VK2_n1947SolicServicoReqNeg_ReqNeqCod, P00VK2_A1895SolicServicoReqNeg_Codigo, P00VK2_A1912SolicServicoReqNeg_Agrupador, P00VK2_A1910SolicServicoReqNeg_Descricao, P00VK2_A1350ContagemResultado_DataCadastro, P00VK2_n1350ContagemResultado_DataCadastro, P00VK2_A1915SolicServicoReqNeg_Identificador, P00VK2_A1909SolicServicoReqNeg_Nome, P00VK2_A1911SolicServicoReqNeg_Prioridade,
               P00VK2_A1914SolicServicoReqNeg_Situacao, P00VK2_A1913SolicServicoReqNeg_Tamanho, P00VK2_A456ContagemResultado_Codigo, P00VK2_A508ContagemResultado_Owner
               }
               , new Object[] {
               P00VK3_A1919Requisito_Codigo
               }
               , new Object[] {
               P00VK4_A1947SolicServicoReqNeg_ReqNeqCod, P00VK4_n1947SolicServicoReqNeg_ReqNeqCod, P00VK4_A1895SolicServicoReqNeg_Codigo, P00VK4_A456ContagemResultado_Codigo, P00VK4_A508ContagemResultado_Owner
               }
               , new Object[] {
               P00VK5_A456ContagemResultado_Codigo, P00VK5_A1946ReqNegReqTec_OSCod, P00VK5_n1946ReqNegReqTec_OSCod, P00VK5_A890ContagemResultado_Responsavel, P00VK5_n890ContagemResultado_Responsavel, P00VK5_A1895SolicServicoReqNeg_Codigo, P00VK5_A1919Requisito_Codigo
               }
               , new Object[] {
               P00VK6_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00VK7_A1919Requisito_Codigo, P00VK7_A1999Requisito_ReqCod, P00VK7_n1999Requisito_ReqCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00VK10_A2005ContagemResultadoRequisito_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1911SolicServicoReqNeg_Prioridade ;
      private short A1914SolicServicoReqNeg_Situacao ;
      private short AV17Requisito_Prioridade ;
      private short AV21Requisito_Status ;
      private short A1931Requisito_Ordem ;
      private short A2002Requisito_Prioridade ;
      private short A1934Requisito_Status ;
      private int A456ContagemResultado_Codigo ;
      private int A508ContagemResultado_Owner ;
      private int AV23Requisito_ReqCod ;
      private int AV34Requisito_TipoReqCod ;
      private int A2049Requisito_TipoReqCod ;
      private int AV27OSCod ;
      private int AV26OwnerCod ;
      private int GX_INS215 ;
      private int A1999Requisito_ReqCod ;
      private int A1919Requisito_Codigo ;
      private int AV31NewRequisito_Codigo ;
      private int AV24Requisito_Codigo ;
      private int A1946ReqNegReqTec_OSCod ;
      private int A890ContagemResultado_Responsavel ;
      private int GX_INS221 ;
      private int A2003ContagemResultadoRequisito_OSCod ;
      private int A2004ContagemResultadoRequisito_ReqCod ;
      private int A2005ContagemResultadoRequisito_Codigo ;
      private long A1947SolicServicoReqNeg_ReqNeqCod ;
      private long A1895SolicServicoReqNeg_Codigo ;
      private long AV28SolicServicoReqNeg_Codigo ;
      private decimal A1913SolicServicoReqNeg_Tamanho ;
      private decimal AV16Requisito_Pontuacao ;
      private decimal A1932Requisito_Pontuacao ;
      private String scmdbuf ;
      private String Gx_emsg ;
      private DateTime A1350ContagemResultado_DataCadastro ;
      private DateTime OV11Requisito_Cadastro ;
      private DateTime AV11Requisito_Cadastro ;
      private DateTime A1943Requisito_Cadastro ;
      private bool n1947SolicServicoReqNeg_ReqNeqCod ;
      private bool n1350ContagemResultado_DataCadastro ;
      private bool returnInSub ;
      private bool n1926Requisito_Agrupador ;
      private bool n1943Requisito_Cadastro ;
      private bool n1923Requisito_Descricao ;
      private bool n2001Requisito_Identificador ;
      private bool n1931Requisito_Ordem ;
      private bool n1932Requisito_Pontuacao ;
      private bool n2002Requisito_Prioridade ;
      private bool n1927Requisito_Titulo ;
      private bool n2049Requisito_TipoReqCod ;
      private bool n1999Requisito_ReqCod ;
      private bool A1935Requisito_Ativo ;
      private bool n1946ReqNegReqTec_OSCod ;
      private bool n890ContagemResultado_Responsavel ;
      private bool A2006ContagemResultadoRequisito_Owner ;
      private String A1910SolicServicoReqNeg_Descricao ;
      private String AV10Requisito_Descricao ;
      private String A1923Requisito_Descricao ;
      private String A1912SolicServicoReqNeg_Agrupador ;
      private String A1915SolicServicoReqNeg_Identificador ;
      private String A1909SolicServicoReqNeg_Nome ;
      private String AV8Requisito_Agrupador ;
      private String AV12Requisito_Identificador ;
      private String AV19Requisito_Titulo ;
      private String A1926Requisito_Agrupador ;
      private String A2001Requisito_Identificador ;
      private String A1927Requisito_Titulo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private long[] P00VK2_A1947SolicServicoReqNeg_ReqNeqCod ;
      private bool[] P00VK2_n1947SolicServicoReqNeg_ReqNeqCod ;
      private long[] P00VK2_A1895SolicServicoReqNeg_Codigo ;
      private String[] P00VK2_A1912SolicServicoReqNeg_Agrupador ;
      private String[] P00VK2_A1910SolicServicoReqNeg_Descricao ;
      private DateTime[] P00VK2_A1350ContagemResultado_DataCadastro ;
      private bool[] P00VK2_n1350ContagemResultado_DataCadastro ;
      private String[] P00VK2_A1915SolicServicoReqNeg_Identificador ;
      private String[] P00VK2_A1909SolicServicoReqNeg_Nome ;
      private short[] P00VK2_A1911SolicServicoReqNeg_Prioridade ;
      private short[] P00VK2_A1914SolicServicoReqNeg_Situacao ;
      private decimal[] P00VK2_A1913SolicServicoReqNeg_Tamanho ;
      private int[] P00VK2_A456ContagemResultado_Codigo ;
      private int[] P00VK2_A508ContagemResultado_Owner ;
      private int[] P00VK3_A1919Requisito_Codigo ;
      private long[] P00VK4_A1947SolicServicoReqNeg_ReqNeqCod ;
      private bool[] P00VK4_n1947SolicServicoReqNeg_ReqNeqCod ;
      private long[] P00VK4_A1895SolicServicoReqNeg_Codigo ;
      private int[] P00VK4_A456ContagemResultado_Codigo ;
      private int[] P00VK4_A508ContagemResultado_Owner ;
      private int[] P00VK5_A456ContagemResultado_Codigo ;
      private int[] P00VK5_A1946ReqNegReqTec_OSCod ;
      private bool[] P00VK5_n1946ReqNegReqTec_OSCod ;
      private int[] P00VK5_A890ContagemResultado_Responsavel ;
      private bool[] P00VK5_n890ContagemResultado_Responsavel ;
      private long[] P00VK5_A1895SolicServicoReqNeg_Codigo ;
      private int[] P00VK5_A1919Requisito_Codigo ;
      private int[] P00VK6_A456ContagemResultado_Codigo ;
      private int[] P00VK7_A1919Requisito_Codigo ;
      private int[] P00VK7_A1999Requisito_ReqCod ;
      private bool[] P00VK7_n1999Requisito_ReqCod ;
      private int[] P00VK10_A2005ContagemResultadoRequisito_Codigo ;
   }

   public class migrarrequisitos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VK2 ;
          prmP00VK2 = new Object[] {
          } ;
          Object[] prmP00VK3 ;
          prmP00VK3 = new Object[] {
          new Object[] {"@Requisito_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Requisito_Agrupador",SqlDbType.VarChar,25,0} ,
          new Object[] {"@Requisito_Titulo",SqlDbType.VarChar,250,0} ,
          new Object[] {"@Requisito_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@Requisito_Pontuacao",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Requisito_Status",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Requisito_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Requisito_Cadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Requisito_ReqCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Requisito_Identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Requisito_Prioridade",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@Requisito_TipoReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VK4 ;
          prmP00VK4 = new Object[] {
          new Object[] {"@AV28SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmP00VK5 ;
          prmP00VK5 = new Object[] {
          new Object[] {"@AV28SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmP00VK6 ;
          prmP00VK6 = new Object[] {
          new Object[] {"@ReqNegReqTec_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VK7 ;
          prmP00VK7 = new Object[] {
          new Object[] {"@AV24Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VK8 ;
          prmP00VK8 = new Object[] {
          new Object[] {"@Requisito_ReqCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VK9 ;
          prmP00VK9 = new Object[] {
          new Object[] {"@Requisito_ReqCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VK10 ;
          prmP00VK10 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoRequisito_ReqCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoRequisito_Owner",SqlDbType.Bit,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VK2", "SELECT T1.[SolicServicoReqNeg_ReqNeqCod], T1.[SolicServicoReqNeg_Codigo], T1.[SolicServicoReqNeg_Agrupador], T1.[SolicServicoReqNeg_Descricao], T2.[ContagemResultado_DataCadastro], T1.[SolicServicoReqNeg_Identificador], T1.[SolicServicoReqNeg_Nome], T1.[SolicServicoReqNeg_Prioridade], T1.[SolicServicoReqNeg_Situacao], T1.[SolicServicoReqNeg_Tamanho], T1.[ContagemResultado_Codigo], T2.[ContagemResultado_Owner] FROM ([SolicitacaoServicoReqNegocio] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[SolicServicoReqNeg_ReqNeqCod] IS NULL ORDER BY T1.[SolicServicoReqNeg_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VK2,100,0,true,false )
             ,new CursorDef("P00VK3", "INSERT INTO [Requisito]([Requisito_Descricao], [Requisito_Agrupador], [Requisito_Titulo], [Requisito_Ordem], [Requisito_Pontuacao], [Requisito_Status], [Requisito_Ativo], [Requisito_Cadastro], [Requisito_ReqCod], [Requisito_Identificador], [Requisito_Prioridade], [Requisito_TipoReqCod], [Proposta_Codigo], [Requisito_ReferenciaTecnica], [Requisito_Restricao], [Requisito_DataHomologacao]) VALUES(@Requisito_Descricao, @Requisito_Agrupador, @Requisito_Titulo, @Requisito_Ordem, @Requisito_Pontuacao, @Requisito_Status, @Requisito_Ativo, @Requisito_Cadastro, @Requisito_ReqCod, @Requisito_Identificador, @Requisito_Prioridade, @Requisito_TipoReqCod, convert(int, 0), '', '', convert( DATETIME, '17530101', 112 )); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00VK3)
             ,new CursorDef("P00VK4", "SELECT T1.[SolicServicoReqNeg_ReqNeqCod], T1.[SolicServicoReqNeg_Codigo], T1.[ContagemResultado_Codigo], T2.[ContagemResultado_Owner] FROM ([SolicitacaoServicoReqNegocio] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[SolicServicoReqNeg_ReqNeqCod] = @AV28SolicServicoReqNeg_Codigo ORDER BY T1.[SolicServicoReqNeg_ReqNeqCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VK4,100,0,true,false )
             ,new CursorDef("P00VK5", "SELECT T2.[ContagemResultado_Codigo], T1.[ReqNegReqTec_OSCod], T3.[ContagemResultado_Responsavel], T1.[SolicServicoReqNeg_Codigo], T1.[Requisito_Codigo] FROM (([ReqNegReqTec] T1 WITH (NOLOCK) INNER JOIN [SolicitacaoServicoReqNegocio] T2 WITH (NOLOCK) ON T2.[SolicServicoReqNeg_Codigo] = T1.[SolicServicoReqNeg_Codigo]) INNER JOIN [ContagemResultado] T3 WITH (NOLOCK) ON T3.[ContagemResultado_Codigo] = T2.[ContagemResultado_Codigo]) WHERE T1.[SolicServicoReqNeg_Codigo] = @AV28SolicServicoReqNeg_Codigo ORDER BY T1.[SolicServicoReqNeg_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VK5,100,0,true,false )
             ,new CursorDef("P00VK6", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ReqNegReqTec_OSCod ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VK6,1,0,false,true )
             ,new CursorDef("P00VK7", "SELECT TOP 1 [Requisito_Codigo], [Requisito_ReqCod] FROM [Requisito] WITH (UPDLOCK) WHERE [Requisito_Codigo] = @AV24Requisito_Codigo ORDER BY [Requisito_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VK7,1,0,true,true )
             ,new CursorDef("P00VK8", "UPDATE [Requisito] SET [Requisito_ReqCod]=@Requisito_ReqCod  WHERE [Requisito_Codigo] = @Requisito_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VK8)
             ,new CursorDef("P00VK9", "UPDATE [Requisito] SET [Requisito_ReqCod]=@Requisito_ReqCod  WHERE [Requisito_Codigo] = @Requisito_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VK9)
             ,new CursorDef("P00VK10", "INSERT INTO [ContagemResultadoRequisito]([ContagemResultadoRequisito_OSCod], [ContagemResultadoRequisito_ReqCod], [ContagemResultadoRequisito_Owner]) VALUES(@ContagemResultadoRequisito_OSCod, @ContagemResultadoRequisito_ReqCod, @ContagemResultadoRequisito_Owner); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00VK10)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((long[]) buf[2])[0] = rslt.getLong(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(7) ;
                ((short[]) buf[9])[0] = rslt.getShort(8) ;
                ((short[]) buf[10])[0] = rslt.getShort(9) ;
                ((decimal[]) buf[11])[0] = rslt.getDecimal(10) ;
                ((int[]) buf[12])[0] = rslt.getInt(11) ;
                ((int[]) buf[13])[0] = rslt.getInt(12) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((long[]) buf[2])[0] = rslt.getLong(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((long[]) buf[5])[0] = rslt.getLong(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[9]);
                }
                stmt.SetParameter(6, (short)parms[10]);
                stmt.SetParameter(7, (bool)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(8, (DateTime)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[21]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                return;
       }
    }

 }

}
