/*
               File: PRC_PadronizaString
        Description: Padroniza String tirando espa�os e acentos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:50.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_padronizastring : GXProcedure
   {
      public prc_padronizastring( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_padronizastring( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Texto ,
                           out String aP1_TextoPadronizado )
      {
         this.AV10Texto = aP0_Texto;
         this.AV11TextoPadronizado = "" ;
         initialize();
         executePrivate();
         aP1_TextoPadronizado=this.AV11TextoPadronizado;
      }

      public String executeUdp( String aP0_Texto )
      {
         this.AV10Texto = aP0_Texto;
         this.AV11TextoPadronizado = "" ;
         initialize();
         executePrivate();
         aP1_TextoPadronizado=this.AV11TextoPadronizado;
         return AV11TextoPadronizado ;
      }

      public void executeSubmit( String aP0_Texto ,
                                 out String aP1_TextoPadronizado )
      {
         prc_padronizastring objprc_padronizastring;
         objprc_padronizastring = new prc_padronizastring();
         objprc_padronizastring.AV10Texto = aP0_Texto;
         objprc_padronizastring.AV11TextoPadronizado = "" ;
         objprc_padronizastring.context.SetSubmitInitialConfig(context);
         objprc_padronizastring.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_padronizastring);
         aP1_TextoPadronizado=this.AV11TextoPadronizado;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_padronizastring)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P002G2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A417ParametrosSistema_PadronizarStrings = P002G2_A417ParametrosSistema_PadronizarStrings[0];
            A330ParametrosSistema_Codigo = P002G2_A330ParametrosSistema_Codigo[0];
            AV11TextoPadronizado = StringUtil.Upper( AV10Texto);
            pr_default.close(0);
            this.cleanup();
            if (true) return;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         AV9i = 1;
         while ( AV9i <= StringUtil.Len( AV10Texto) )
         {
            AV8Caracter = StringUtil.Substring( AV10Texto, AV9i, 1);
            if ( ( StringUtil.StringSearch( " ~-_\"�`�^", AV8Caracter, 1) > 0 ) || ( StringUtil.StrCmp(AV8Caracter, "'") == 0 ) )
            {
               AV8Caracter = "";
            }
            else if ( StringUtil.StringSearch( "������������", AV8Caracter, 1) > 0 )
            {
               AV8Caracter = "a";
            }
            else if ( StringUtil.StringSearch( "��������", AV8Caracter, 1) > 0 )
            {
               AV8Caracter = "e";
            }
            else if ( StringUtil.StringSearch( "�������", AV8Caracter, 1) > 0 )
            {
               AV8Caracter = "i";
            }
            else if ( StringUtil.StringSearch( "�����������", AV8Caracter, 1) > 0 )
            {
               AV8Caracter = "o";
            }
            else if ( StringUtil.StringSearch( "��������", AV8Caracter, 1) > 0 )
            {
               AV8Caracter = "u";
            }
            else if ( StringUtil.StringSearch( "��", AV8Caracter, 1) > 0 )
            {
               AV8Caracter = "c";
            }
            else if ( StringUtil.StringSearch( "��", AV8Caracter, 1) > 0 )
            {
               AV8Caracter = "n";
            }
            AV11TextoPadronizado = AV11TextoPadronizado + AV8Caracter;
            AV9i = (short)(AV9i+1);
         }
         AV11TextoPadronizado = StringUtil.Upper( AV11TextoPadronizado);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P002G2_A417ParametrosSistema_PadronizarStrings = new bool[] {false} ;
         P002G2_A330ParametrosSistema_Codigo = new int[1] ;
         AV8Caracter = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_padronizastring__default(),
            new Object[][] {
                new Object[] {
               P002G2_A417ParametrosSistema_PadronizarStrings, P002G2_A330ParametrosSistema_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9i ;
      private int A330ParametrosSistema_Codigo ;
      private String scmdbuf ;
      private String AV8Caracter ;
      private bool A417ParametrosSistema_PadronizarStrings ;
      private String AV10Texto ;
      private String AV11TextoPadronizado ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private bool[] P002G2_A417ParametrosSistema_PadronizarStrings ;
      private int[] P002G2_A330ParametrosSistema_Codigo ;
      private String aP1_TextoPadronizado ;
   }

   public class prc_padronizastring__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP002G2 ;
          prmP002G2 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P002G2", "SELECT TOP 1 [ParametrosSistema_PadronizarStrings], [ParametrosSistema_Codigo] FROM [ParametrosSistema] WITH (NOLOCK) WHERE Not [ParametrosSistema_PadronizarStrings] = 1 ORDER BY [ParametrosSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002G2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
