/*
               File: type_SdtMenuPerfil
        Description: Menu Perfil
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:55:7.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "MenuPerfil" )]
   [XmlType(TypeName =  "MenuPerfil" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtMenuPerfil : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtMenuPerfil( )
      {
         /* Constructor for serialization */
         gxTv_SdtMenuPerfil_Menu_nome = "";
         gxTv_SdtMenuPerfil_Menu_imagem = "";
         gxTv_SdtMenuPerfil_Menu_imagem_gxi = "";
         gxTv_SdtMenuPerfil_Menu_link = "";
         gxTv_SdtMenuPerfil_Perfil_nome = "";
         gxTv_SdtMenuPerfil_Mode = "";
         gxTv_SdtMenuPerfil_Menu_nome_Z = "";
         gxTv_SdtMenuPerfil_Menu_link_Z = "";
         gxTv_SdtMenuPerfil_Perfil_nome_Z = "";
         gxTv_SdtMenuPerfil_Menu_imagem_gxi_Z = "";
      }

      public SdtMenuPerfil( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV277Menu_Codigo ,
                        int AV3Perfil_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV277Menu_Codigo,(int)AV3Perfil_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Menu_Codigo", typeof(int)}, new Object[]{"Perfil_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "MenuPerfil");
         metadata.Set("BT", "MenuPerfil");
         metadata.Set("PK", "[ \"Menu_Codigo\",\"Perfil_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Menu_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Perfil_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Menu_imagem_gxi" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_tipo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_ordem_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_link_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_imagem_gxi_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_imagem_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_link_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_imagem_gxi_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtMenuPerfil deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtMenuPerfil)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtMenuPerfil obj ;
         obj = this;
         obj.gxTpr_Menu_codigo = deserialized.gxTpr_Menu_codigo;
         obj.gxTpr_Menu_nome = deserialized.gxTpr_Menu_nome;
         obj.gxTpr_Menu_tipo = deserialized.gxTpr_Menu_tipo;
         obj.gxTpr_Menu_ativo = deserialized.gxTpr_Menu_ativo;
         obj.gxTpr_Menu_ordem = deserialized.gxTpr_Menu_ordem;
         obj.gxTpr_Menu_imagem = deserialized.gxTpr_Menu_imagem;
         obj.gxTpr_Menu_imagem_gxi = deserialized.gxTpr_Menu_imagem_gxi;
         obj.gxTpr_Menu_link = deserialized.gxTpr_Menu_link;
         obj.gxTpr_Perfil_codigo = deserialized.gxTpr_Perfil_codigo;
         obj.gxTpr_Perfil_nome = deserialized.gxTpr_Perfil_nome;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Menu_codigo_Z = deserialized.gxTpr_Menu_codigo_Z;
         obj.gxTpr_Menu_nome_Z = deserialized.gxTpr_Menu_nome_Z;
         obj.gxTpr_Menu_tipo_Z = deserialized.gxTpr_Menu_tipo_Z;
         obj.gxTpr_Menu_ativo_Z = deserialized.gxTpr_Menu_ativo_Z;
         obj.gxTpr_Menu_ordem_Z = deserialized.gxTpr_Menu_ordem_Z;
         obj.gxTpr_Menu_link_Z = deserialized.gxTpr_Menu_link_Z;
         obj.gxTpr_Perfil_codigo_Z = deserialized.gxTpr_Perfil_codigo_Z;
         obj.gxTpr_Perfil_nome_Z = deserialized.gxTpr_Perfil_nome_Z;
         obj.gxTpr_Menu_imagem_gxi_Z = deserialized.gxTpr_Menu_imagem_gxi_Z;
         obj.gxTpr_Menu_imagem_N = deserialized.gxTpr_Menu_imagem_N;
         obj.gxTpr_Menu_link_N = deserialized.gxTpr_Menu_link_N;
         obj.gxTpr_Menu_imagem_gxi_N = deserialized.gxTpr_Menu_imagem_gxi_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Codigo") )
               {
                  gxTv_SdtMenuPerfil_Menu_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Nome") )
               {
                  gxTv_SdtMenuPerfil_Menu_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Tipo") )
               {
                  gxTv_SdtMenuPerfil_Menu_tipo = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Ativo") )
               {
                  gxTv_SdtMenuPerfil_Menu_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Ordem") )
               {
                  gxTv_SdtMenuPerfil_Menu_ordem = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Imagem") )
               {
                  gxTv_SdtMenuPerfil_Menu_imagem = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Imagem_GXI") )
               {
                  gxTv_SdtMenuPerfil_Menu_imagem_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Link") )
               {
                  gxTv_SdtMenuPerfil_Menu_link = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Codigo") )
               {
                  gxTv_SdtMenuPerfil_Perfil_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Nome") )
               {
                  gxTv_SdtMenuPerfil_Perfil_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtMenuPerfil_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtMenuPerfil_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Codigo_Z") )
               {
                  gxTv_SdtMenuPerfil_Menu_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Nome_Z") )
               {
                  gxTv_SdtMenuPerfil_Menu_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Tipo_Z") )
               {
                  gxTv_SdtMenuPerfil_Menu_tipo_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Ativo_Z") )
               {
                  gxTv_SdtMenuPerfil_Menu_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Ordem_Z") )
               {
                  gxTv_SdtMenuPerfil_Menu_ordem_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Link_Z") )
               {
                  gxTv_SdtMenuPerfil_Menu_link_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Codigo_Z") )
               {
                  gxTv_SdtMenuPerfil_Perfil_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Nome_Z") )
               {
                  gxTv_SdtMenuPerfil_Perfil_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Imagem_GXI_Z") )
               {
                  gxTv_SdtMenuPerfil_Menu_imagem_gxi_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Imagem_N") )
               {
                  gxTv_SdtMenuPerfil_Menu_imagem_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Link_N") )
               {
                  gxTv_SdtMenuPerfil_Menu_link_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Imagem_GXI_N") )
               {
                  gxTv_SdtMenuPerfil_Menu_imagem_gxi_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "MenuPerfil";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Menu_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenuPerfil_Menu_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_Nome", StringUtil.RTrim( gxTv_SdtMenuPerfil_Menu_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_Tipo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenuPerfil_Menu_tipo), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtMenuPerfil_Menu_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_Ordem", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenuPerfil_Menu_ordem), 3, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_Imagem", StringUtil.RTrim( gxTv_SdtMenuPerfil_Menu_imagem));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_Link", StringUtil.RTrim( gxTv_SdtMenuPerfil_Menu_link));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenuPerfil_Perfil_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_Nome", StringUtil.RTrim( gxTv_SdtMenuPerfil_Perfil_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Menu_Imagem_GXI", StringUtil.RTrim( gxTv_SdtMenuPerfil_Menu_imagem_gxi));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtMenuPerfil_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenuPerfil_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenuPerfil_Menu_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Nome_Z", StringUtil.RTrim( gxTv_SdtMenuPerfil_Menu_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Tipo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenuPerfil_Menu_tipo_Z), 2, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtMenuPerfil_Menu_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Ordem_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenuPerfil_Menu_ordem_Z), 3, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Link_Z", StringUtil.RTrim( gxTv_SdtMenuPerfil_Menu_link_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenuPerfil_Perfil_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_Nome_Z", StringUtil.RTrim( gxTv_SdtMenuPerfil_Perfil_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Imagem_GXI_Z", StringUtil.RTrim( gxTv_SdtMenuPerfil_Menu_imagem_gxi_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Imagem_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenuPerfil_Menu_imagem_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Link_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenuPerfil_Menu_link_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Imagem_GXI_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenuPerfil_Menu_imagem_gxi_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Menu_Codigo", gxTv_SdtMenuPerfil_Menu_codigo, false);
         AddObjectProperty("Menu_Nome", gxTv_SdtMenuPerfil_Menu_nome, false);
         AddObjectProperty("Menu_Tipo", gxTv_SdtMenuPerfil_Menu_tipo, false);
         AddObjectProperty("Menu_Ativo", gxTv_SdtMenuPerfil_Menu_ativo, false);
         AddObjectProperty("Menu_Ordem", gxTv_SdtMenuPerfil_Menu_ordem, false);
         AddObjectProperty("Menu_Imagem", gxTv_SdtMenuPerfil_Menu_imagem, false);
         AddObjectProperty("Menu_Link", gxTv_SdtMenuPerfil_Menu_link, false);
         AddObjectProperty("Perfil_Codigo", gxTv_SdtMenuPerfil_Perfil_codigo, false);
         AddObjectProperty("Perfil_Nome", gxTv_SdtMenuPerfil_Perfil_nome, false);
         if ( includeState )
         {
            AddObjectProperty("Menu_Imagem_GXI", gxTv_SdtMenuPerfil_Menu_imagem_gxi, false);
            AddObjectProperty("Mode", gxTv_SdtMenuPerfil_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtMenuPerfil_Initialized, false);
            AddObjectProperty("Menu_Codigo_Z", gxTv_SdtMenuPerfil_Menu_codigo_Z, false);
            AddObjectProperty("Menu_Nome_Z", gxTv_SdtMenuPerfil_Menu_nome_Z, false);
            AddObjectProperty("Menu_Tipo_Z", gxTv_SdtMenuPerfil_Menu_tipo_Z, false);
            AddObjectProperty("Menu_Ativo_Z", gxTv_SdtMenuPerfil_Menu_ativo_Z, false);
            AddObjectProperty("Menu_Ordem_Z", gxTv_SdtMenuPerfil_Menu_ordem_Z, false);
            AddObjectProperty("Menu_Link_Z", gxTv_SdtMenuPerfil_Menu_link_Z, false);
            AddObjectProperty("Perfil_Codigo_Z", gxTv_SdtMenuPerfil_Perfil_codigo_Z, false);
            AddObjectProperty("Perfil_Nome_Z", gxTv_SdtMenuPerfil_Perfil_nome_Z, false);
            AddObjectProperty("Menu_Imagem_GXI_Z", gxTv_SdtMenuPerfil_Menu_imagem_gxi_Z, false);
            AddObjectProperty("Menu_Imagem_N", gxTv_SdtMenuPerfil_Menu_imagem_N, false);
            AddObjectProperty("Menu_Link_N", gxTv_SdtMenuPerfil_Menu_link_N, false);
            AddObjectProperty("Menu_Imagem_GXI_N", gxTv_SdtMenuPerfil_Menu_imagem_gxi_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Menu_Codigo" )]
      [  XmlElement( ElementName = "Menu_Codigo"   )]
      public int gxTpr_Menu_codigo
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_codigo ;
         }

         set {
            if ( gxTv_SdtMenuPerfil_Menu_codigo != value )
            {
               gxTv_SdtMenuPerfil_Mode = "INS";
               this.gxTv_SdtMenuPerfil_Menu_codigo_Z_SetNull( );
               this.gxTv_SdtMenuPerfil_Menu_nome_Z_SetNull( );
               this.gxTv_SdtMenuPerfil_Menu_tipo_Z_SetNull( );
               this.gxTv_SdtMenuPerfil_Menu_ativo_Z_SetNull( );
               this.gxTv_SdtMenuPerfil_Menu_ordem_Z_SetNull( );
               this.gxTv_SdtMenuPerfil_Menu_link_Z_SetNull( );
               this.gxTv_SdtMenuPerfil_Perfil_codigo_Z_SetNull( );
               this.gxTv_SdtMenuPerfil_Perfil_nome_Z_SetNull( );
               this.gxTv_SdtMenuPerfil_Menu_imagem_gxi_Z_SetNull( );
            }
            gxTv_SdtMenuPerfil_Menu_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Menu_Nome" )]
      [  XmlElement( ElementName = "Menu_Nome"   )]
      public String gxTpr_Menu_nome
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_nome ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Menu_Tipo" )]
      [  XmlElement( ElementName = "Menu_Tipo"   )]
      public short gxTpr_Menu_tipo
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_tipo ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_tipo = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Menu_Ativo" )]
      [  XmlElement( ElementName = "Menu_Ativo"   )]
      public bool gxTpr_Menu_ativo
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_ativo ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Menu_Ordem" )]
      [  XmlElement( ElementName = "Menu_Ordem"   )]
      public short gxTpr_Menu_ordem
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_ordem ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_ordem = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Menu_Imagem" )]
      [  XmlElement( ElementName = "Menu_Imagem"   )]
      [GxUpload()]
      public String gxTpr_Menu_imagem
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_imagem ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_imagem_N = 0;
            gxTv_SdtMenuPerfil_Menu_imagem = (String)(value);
         }

      }

      public void gxTv_SdtMenuPerfil_Menu_imagem_SetNull( )
      {
         gxTv_SdtMenuPerfil_Menu_imagem_N = 1;
         gxTv_SdtMenuPerfil_Menu_imagem = "";
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Menu_imagem_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Imagem_GXI" )]
      [  XmlElement( ElementName = "Menu_Imagem_GXI"   )]
      public String gxTpr_Menu_imagem_gxi
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_imagem_gxi ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_imagem_gxi_N = 0;
            gxTv_SdtMenuPerfil_Menu_imagem_gxi = (String)(value);
         }

      }

      public void gxTv_SdtMenuPerfil_Menu_imagem_gxi_SetNull( )
      {
         gxTv_SdtMenuPerfil_Menu_imagem_gxi_N = 1;
         gxTv_SdtMenuPerfil_Menu_imagem_gxi = "";
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Menu_imagem_gxi_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Link" )]
      [  XmlElement( ElementName = "Menu_Link"   )]
      public String gxTpr_Menu_link
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_link ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_link_N = 0;
            gxTv_SdtMenuPerfil_Menu_link = (String)(value);
         }

      }

      public void gxTv_SdtMenuPerfil_Menu_link_SetNull( )
      {
         gxTv_SdtMenuPerfil_Menu_link_N = 1;
         gxTv_SdtMenuPerfil_Menu_link = "";
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Menu_link_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_Codigo" )]
      [  XmlElement( ElementName = "Perfil_Codigo"   )]
      public int gxTpr_Perfil_codigo
      {
         get {
            return gxTv_SdtMenuPerfil_Perfil_codigo ;
         }

         set {
            if ( gxTv_SdtMenuPerfil_Perfil_codigo != value )
            {
               gxTv_SdtMenuPerfil_Mode = "INS";
               this.gxTv_SdtMenuPerfil_Menu_codigo_Z_SetNull( );
               this.gxTv_SdtMenuPerfil_Menu_nome_Z_SetNull( );
               this.gxTv_SdtMenuPerfil_Menu_tipo_Z_SetNull( );
               this.gxTv_SdtMenuPerfil_Menu_ativo_Z_SetNull( );
               this.gxTv_SdtMenuPerfil_Menu_ordem_Z_SetNull( );
               this.gxTv_SdtMenuPerfil_Menu_link_Z_SetNull( );
               this.gxTv_SdtMenuPerfil_Perfil_codigo_Z_SetNull( );
               this.gxTv_SdtMenuPerfil_Perfil_nome_Z_SetNull( );
               this.gxTv_SdtMenuPerfil_Menu_imagem_gxi_Z_SetNull( );
            }
            gxTv_SdtMenuPerfil_Perfil_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_Nome" )]
      [  XmlElement( ElementName = "Perfil_Nome"   )]
      public String gxTpr_Perfil_nome
      {
         get {
            return gxTv_SdtMenuPerfil_Perfil_nome ;
         }

         set {
            gxTv_SdtMenuPerfil_Perfil_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtMenuPerfil_Mode ;
         }

         set {
            gxTv_SdtMenuPerfil_Mode = (String)(value);
         }

      }

      public void gxTv_SdtMenuPerfil_Mode_SetNull( )
      {
         gxTv_SdtMenuPerfil_Mode = "";
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtMenuPerfil_Initialized ;
         }

         set {
            gxTv_SdtMenuPerfil_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtMenuPerfil_Initialized_SetNull( )
      {
         gxTv_SdtMenuPerfil_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Codigo_Z" )]
      [  XmlElement( ElementName = "Menu_Codigo_Z"   )]
      public int gxTpr_Menu_codigo_Z
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_codigo_Z ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtMenuPerfil_Menu_codigo_Z_SetNull( )
      {
         gxTv_SdtMenuPerfil_Menu_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Menu_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Nome_Z" )]
      [  XmlElement( ElementName = "Menu_Nome_Z"   )]
      public String gxTpr_Menu_nome_Z
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_nome_Z ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtMenuPerfil_Menu_nome_Z_SetNull( )
      {
         gxTv_SdtMenuPerfil_Menu_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Menu_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Tipo_Z" )]
      [  XmlElement( ElementName = "Menu_Tipo_Z"   )]
      public short gxTpr_Menu_tipo_Z
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_tipo_Z ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_tipo_Z = (short)(value);
         }

      }

      public void gxTv_SdtMenuPerfil_Menu_tipo_Z_SetNull( )
      {
         gxTv_SdtMenuPerfil_Menu_tipo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Menu_tipo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Ativo_Z" )]
      [  XmlElement( ElementName = "Menu_Ativo_Z"   )]
      public bool gxTpr_Menu_ativo_Z
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_ativo_Z ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_ativo_Z = value;
         }

      }

      public void gxTv_SdtMenuPerfil_Menu_ativo_Z_SetNull( )
      {
         gxTv_SdtMenuPerfil_Menu_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Menu_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Ordem_Z" )]
      [  XmlElement( ElementName = "Menu_Ordem_Z"   )]
      public short gxTpr_Menu_ordem_Z
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_ordem_Z ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_ordem_Z = (short)(value);
         }

      }

      public void gxTv_SdtMenuPerfil_Menu_ordem_Z_SetNull( )
      {
         gxTv_SdtMenuPerfil_Menu_ordem_Z = 0;
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Menu_ordem_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Link_Z" )]
      [  XmlElement( ElementName = "Menu_Link_Z"   )]
      public String gxTpr_Menu_link_Z
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_link_Z ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_link_Z = (String)(value);
         }

      }

      public void gxTv_SdtMenuPerfil_Menu_link_Z_SetNull( )
      {
         gxTv_SdtMenuPerfil_Menu_link_Z = "";
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Menu_link_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_Codigo_Z" )]
      [  XmlElement( ElementName = "Perfil_Codigo_Z"   )]
      public int gxTpr_Perfil_codigo_Z
      {
         get {
            return gxTv_SdtMenuPerfil_Perfil_codigo_Z ;
         }

         set {
            gxTv_SdtMenuPerfil_Perfil_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtMenuPerfil_Perfil_codigo_Z_SetNull( )
      {
         gxTv_SdtMenuPerfil_Perfil_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Perfil_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_Nome_Z" )]
      [  XmlElement( ElementName = "Perfil_Nome_Z"   )]
      public String gxTpr_Perfil_nome_Z
      {
         get {
            return gxTv_SdtMenuPerfil_Perfil_nome_Z ;
         }

         set {
            gxTv_SdtMenuPerfil_Perfil_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtMenuPerfil_Perfil_nome_Z_SetNull( )
      {
         gxTv_SdtMenuPerfil_Perfil_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Perfil_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Imagem_GXI_Z" )]
      [  XmlElement( ElementName = "Menu_Imagem_GXI_Z"   )]
      public String gxTpr_Menu_imagem_gxi_Z
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_imagem_gxi_Z ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_imagem_gxi_Z = (String)(value);
         }

      }

      public void gxTv_SdtMenuPerfil_Menu_imagem_gxi_Z_SetNull( )
      {
         gxTv_SdtMenuPerfil_Menu_imagem_gxi_Z = "";
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Menu_imagem_gxi_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Imagem_N" )]
      [  XmlElement( ElementName = "Menu_Imagem_N"   )]
      public short gxTpr_Menu_imagem_N
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_imagem_N ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_imagem_N = (short)(value);
         }

      }

      public void gxTv_SdtMenuPerfil_Menu_imagem_N_SetNull( )
      {
         gxTv_SdtMenuPerfil_Menu_imagem_N = 0;
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Menu_imagem_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Link_N" )]
      [  XmlElement( ElementName = "Menu_Link_N"   )]
      public short gxTpr_Menu_link_N
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_link_N ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_link_N = (short)(value);
         }

      }

      public void gxTv_SdtMenuPerfil_Menu_link_N_SetNull( )
      {
         gxTv_SdtMenuPerfil_Menu_link_N = 0;
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Menu_link_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Imagem_GXI_N" )]
      [  XmlElement( ElementName = "Menu_Imagem_GXI_N"   )]
      public short gxTpr_Menu_imagem_gxi_N
      {
         get {
            return gxTv_SdtMenuPerfil_Menu_imagem_gxi_N ;
         }

         set {
            gxTv_SdtMenuPerfil_Menu_imagem_gxi_N = (short)(value);
         }

      }

      public void gxTv_SdtMenuPerfil_Menu_imagem_gxi_N_SetNull( )
      {
         gxTv_SdtMenuPerfil_Menu_imagem_gxi_N = 0;
         return  ;
      }

      public bool gxTv_SdtMenuPerfil_Menu_imagem_gxi_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtMenuPerfil_Menu_nome = "";
         gxTv_SdtMenuPerfil_Menu_imagem = "";
         gxTv_SdtMenuPerfil_Menu_imagem_gxi = "";
         gxTv_SdtMenuPerfil_Menu_link = "";
         gxTv_SdtMenuPerfil_Perfil_nome = "";
         gxTv_SdtMenuPerfil_Mode = "";
         gxTv_SdtMenuPerfil_Menu_nome_Z = "";
         gxTv_SdtMenuPerfil_Menu_link_Z = "";
         gxTv_SdtMenuPerfil_Perfil_nome_Z = "";
         gxTv_SdtMenuPerfil_Menu_imagem_gxi_Z = "";
         gxTv_SdtMenuPerfil_Menu_ativo = true;
         gxTv_SdtMenuPerfil_Menu_ordem = 0;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "menuperfil", "GeneXus.Programs.menuperfil_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtMenuPerfil_Menu_tipo ;
      private short gxTv_SdtMenuPerfil_Menu_ordem ;
      private short gxTv_SdtMenuPerfil_Initialized ;
      private short gxTv_SdtMenuPerfil_Menu_tipo_Z ;
      private short gxTv_SdtMenuPerfil_Menu_ordem_Z ;
      private short gxTv_SdtMenuPerfil_Menu_imagem_N ;
      private short gxTv_SdtMenuPerfil_Menu_link_N ;
      private short gxTv_SdtMenuPerfil_Menu_imagem_gxi_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtMenuPerfil_Menu_codigo ;
      private int gxTv_SdtMenuPerfil_Perfil_codigo ;
      private int gxTv_SdtMenuPerfil_Menu_codigo_Z ;
      private int gxTv_SdtMenuPerfil_Perfil_codigo_Z ;
      private String gxTv_SdtMenuPerfil_Menu_nome ;
      private String gxTv_SdtMenuPerfil_Perfil_nome ;
      private String gxTv_SdtMenuPerfil_Mode ;
      private String gxTv_SdtMenuPerfil_Menu_nome_Z ;
      private String gxTv_SdtMenuPerfil_Perfil_nome_Z ;
      private String sTagName ;
      private bool gxTv_SdtMenuPerfil_Menu_ativo ;
      private bool gxTv_SdtMenuPerfil_Menu_ativo_Z ;
      private String gxTv_SdtMenuPerfil_Menu_imagem_gxi ;
      private String gxTv_SdtMenuPerfil_Menu_link ;
      private String gxTv_SdtMenuPerfil_Menu_link_Z ;
      private String gxTv_SdtMenuPerfil_Menu_imagem_gxi_Z ;
      private String gxTv_SdtMenuPerfil_Menu_imagem ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"MenuPerfil", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtMenuPerfil_RESTInterface : GxGenericCollectionItem<SdtMenuPerfil>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtMenuPerfil_RESTInterface( ) : base()
      {
      }

      public SdtMenuPerfil_RESTInterface( SdtMenuPerfil psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Menu_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Menu_codigo
      {
         get {
            return sdt.gxTpr_Menu_codigo ;
         }

         set {
            sdt.gxTpr_Menu_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Menu_Nome" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Menu_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Menu_nome) ;
         }

         set {
            sdt.gxTpr_Menu_nome = (String)(value);
         }

      }

      [DataMember( Name = "Menu_Tipo" , Order = 2 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Menu_tipo
      {
         get {
            return sdt.gxTpr_Menu_tipo ;
         }

         set {
            sdt.gxTpr_Menu_tipo = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Menu_Ativo" , Order = 3 )]
      [GxSeudo()]
      public bool gxTpr_Menu_ativo
      {
         get {
            return sdt.gxTpr_Menu_ativo ;
         }

         set {
            sdt.gxTpr_Menu_ativo = value;
         }

      }

      [DataMember( Name = "Menu_Ordem" , Order = 4 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Menu_ordem
      {
         get {
            return sdt.gxTpr_Menu_ordem ;
         }

         set {
            sdt.gxTpr_Menu_ordem = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Menu_Imagem" , Order = 5 )]
      [GxUpload()]
      public String gxTpr_Menu_imagem
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Menu_imagem)) ? PathUtil.RelativePath( sdt.gxTpr_Menu_imagem) : StringUtil.RTrim( sdt.gxTpr_Menu_imagem_gxi)) ;
         }

         set {
            sdt.gxTpr_Menu_imagem = (String)(value);
         }

      }

      [DataMember( Name = "Menu_Link" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Menu_link
      {
         get {
            return sdt.gxTpr_Menu_link ;
         }

         set {
            sdt.gxTpr_Menu_link = (String)(value);
         }

      }

      [DataMember( Name = "Perfil_Codigo" , Order = 8 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Perfil_codigo
      {
         get {
            return sdt.gxTpr_Perfil_codigo ;
         }

         set {
            sdt.gxTpr_Perfil_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Perfil_Nome" , Order = 9 )]
      [GxSeudo()]
      public String gxTpr_Perfil_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Perfil_nome) ;
         }

         set {
            sdt.gxTpr_Perfil_nome = (String)(value);
         }

      }

      public SdtMenuPerfil sdt
      {
         get {
            return (SdtMenuPerfil)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtMenuPerfil() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 24 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
