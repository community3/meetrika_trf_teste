/*
               File: ContratanteGeneral
        Description: Contratante General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:29:4.3
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratantegeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratantegeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratantegeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratante_Codigo )
      {
         this.A29Contratante_Codigo = aP0_Contratante_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContratante_UsaOSistema = new GXCombobox();
         chkContratante_Ativo = new GXCheckbox();
         cmbContratante_SSAutomatica = new GXCombobox();
         cmbContratante_SSPrestadoraUnica = new GXCombobox();
         cmbContratante_SSStatusPlanejamento = new GXCombobox();
         dynContratante_ServicoSSPadrao = new GXCombobox();
         cmbContratante_RetornaSS = new GXCombobox();
         cmbContratante_PropostaRqrSrv = new GXCombobox();
         cmbContratante_PropostaRqrPrz = new GXCombobox();
         cmbContratante_PropostaRqrEsf = new GXCombobox();
         cmbContratante_PropostaRqrCnt = new GXCombobox();
         cmbContratante_PropostaNvlCnt = new GXCombobox();
         cmbContratante_OSAutomatica = new GXCombobox();
         cmbContratante_OSGeraOS = new GXCombobox();
         cmbContratante_OSGeraTRP = new GXCombobox();
         cmbContratante_OSGeraTRD = new GXCombobox();
         cmbContratante_OSGeraTH = new GXCombobox();
         cmbContratante_OSGeraTR = new GXCombobox();
         cmbContratante_OSHmlgComPnd = new GXCombobox();
         cmbContratante_SelecionaResponsavelOS = new GXCombobox();
         cmbContratante_ExibePF = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A29Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A29Contratante_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTRATANTE_SERVICOSSPADRAO") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLACONTRATANTE_SERVICOSSPADRAO102( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA102( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV16Pgmname = "ContratanteGeneral";
               context.Gx_err = 0;
               GXACONTRATANTE_SERVICOSSPADRAO_html102( ) ;
               WS102( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contratante General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205302129419");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratantegeneral.aspx") + "?" + UrlEncode("" +A29Contratante_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA29Contratante_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA29Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2208Contratante_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_IE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A11Contratante_IE, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_TELEFONE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A31Contratante_Telefone, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_RAMAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A32Contratante_Ramal, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_FAX", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A33Contratante_Fax, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_WEBSITE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A13Contratante_WebSite, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_BANCONOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A338Contratante_BancoNome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_BANCONRO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A339Contratante_BancoNro, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_AGENCIANOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A336Contratante_AgenciaNome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_AGENCIANRO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A337Contratante_AgenciaNro, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_CONTACORRENTE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A16Contratante_ContaCorrente, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_INICIODOEXPEDIENTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1448Contratante_InicioDoExpediente, "99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_FIMDOEXPEDIENTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1192Contratante_FimDoExpediente, "99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_USAOSISTEMA", GetSecureSignedToken( sPrefix, A1822Contratante_UsaOSistema));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_ATIVO", GetSecureSignedToken( sPrefix, A30Contratante_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_SSAUTOMATICA", GetSecureSignedToken( sPrefix, A1652Contratante_SSAutomatica));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_SSPRESTADORAUNICA", GetSecureSignedToken( sPrefix, A1733Contratante_SSPrestadoraUnica));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_SSSTATUSPLANEJAMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1732Contratante_SSStatusPlanejamento, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_SERVICOSSPADRAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1727Contratante_ServicoSSPadrao), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_RETORNASS", GetSecureSignedToken( sPrefix, A1729Contratante_RetornaSS));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_PROPOSTARQRSRV", GetSecureSignedToken( sPrefix, A1738Contratante_PropostaRqrSrv));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_PROPOSTARQRPRZ", GetSecureSignedToken( sPrefix, A1739Contratante_PropostaRqrPrz));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_PROPOSTARQRESF", GetSecureSignedToken( sPrefix, A1740Contratante_PropostaRqrEsf));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_PROPOSTARQRCNT", GetSecureSignedToken( sPrefix, A1741Contratante_PropostaRqrCnt));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_PROPOSTANVLCNT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1742Contratante_PropostaNvlCnt), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_OSAUTOMATICA", GetSecureSignedToken( sPrefix, A593Contratante_OSAutomatica));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_OSGERAOS", GetSecureSignedToken( sPrefix, A1757Contratante_OSGeraOS));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_OSGERATRP", GetSecureSignedToken( sPrefix, A1758Contratante_OSGeraTRP));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_OSGERATRD", GetSecureSignedToken( sPrefix, A1760Contratante_OSGeraTRD));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_OSGERATH", GetSecureSignedToken( sPrefix, A1759Contratante_OSGeraTH));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_OSGERATR", GetSecureSignedToken( sPrefix, A1761Contratante_OSGeraTR));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_OSHMLGCOMPND", GetSecureSignedToken( sPrefix, A1803Contratante_OSHmlgComPnd));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_SELECIONARESPONSAVELOS", GetSecureSignedToken( sPrefix, A2089Contratante_SelecionaResponsavelOS));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EXIBEPF", GetSecureSignedToken( sPrefix, A2090Contratante_ExibePF));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_ATIVOCIRCULANTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1805Contratante_AtivoCirculante, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_PASSIVOCIRCULANTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1806Contratante_PassivoCirculante, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_PATRIMONIOLIQUIDO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1807Contratante_PatrimonioLiquido, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_RECEITABRUTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1808Contratante_ReceitaBruta, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_TTLRLTGERENCIAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2034Contratante_TtlRltGerencial, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_MUNICIPIO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXUITABSPANEL_TABS_Width", StringUtil.RTrim( Gxuitabspanel_tabs_Width));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXUITABSPANEL_TABS_Cls", StringUtil.RTrim( Gxuitabspanel_tabs_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXUITABSPANEL_TABS_Autowidth", StringUtil.BoolToStr( Gxuitabspanel_tabs_Autowidth));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXUITABSPANEL_TABS_Autoheight", StringUtil.BoolToStr( Gxuitabspanel_tabs_Autoheight));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXUITABSPANEL_TABS_Autoscroll", StringUtil.BoolToStr( Gxuitabspanel_tabs_Autoscroll));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXUITABSPANEL_TABS_Designtimetabs", StringUtil.RTrim( Gxuitabspanel_tabs_Designtimetabs));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContratanteGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratantegeneral:[SendSecurityCheck value for]"+"Municipio_Codigo:"+context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm102( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratantegeneral.js", "?20205302129440");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratanteGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contratante General" ;
      }

      protected void WB100( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratantegeneral.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
            }
            wb_table1_2_102( true) ;
         }
         else
         {
            wb_table1_2_102( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_102e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratante_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratanteGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMunicipio_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMunicipio_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtMunicipio_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratanteGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START102( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contratante General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP100( ) ;
            }
         }
      }

      protected void WS102( )
      {
         START102( ) ;
         EVT102( ) ;
      }

      protected void EVT102( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP100( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP100( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11102 */
                                    E11102 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP100( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12102 */
                                    E12102 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP100( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13102 */
                                    E13102 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP100( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14102 */
                                    E14102 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP100( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15102 */
                                    E15102 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP100( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP100( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE102( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm102( ) ;
            }
         }
      }

      protected void PA102( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbContratante_UsaOSistema.Name = "CONTRATANTE_USAOSISTEMA";
            cmbContratante_UsaOSistema.WebTags = "";
            cmbContratante_UsaOSistema.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_UsaOSistema.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_UsaOSistema.ItemCount > 0 )
            {
               A1822Contratante_UsaOSistema = StringUtil.StrToBool( cmbContratante_UsaOSistema.getValidValue(StringUtil.BoolToStr( A1822Contratante_UsaOSistema)));
               n1822Contratante_UsaOSistema = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1822Contratante_UsaOSistema", A1822Contratante_UsaOSistema);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_USAOSISTEMA", GetSecureSignedToken( sPrefix, A1822Contratante_UsaOSistema));
            }
            chkContratante_Ativo.Name = "CONTRATANTE_ATIVO";
            chkContratante_Ativo.WebTags = "";
            chkContratante_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratante_Ativo_Internalname, "TitleCaption", chkContratante_Ativo.Caption);
            chkContratante_Ativo.CheckedValue = "false";
            cmbContratante_SSAutomatica.Name = "CONTRATANTE_SSAUTOMATICA";
            cmbContratante_SSAutomatica.WebTags = "";
            cmbContratante_SSAutomatica.addItem(StringUtil.BoolToStr( false), "Manual", 0);
            cmbContratante_SSAutomatica.addItem(StringUtil.BoolToStr( true), "Autom�tica", 0);
            if ( cmbContratante_SSAutomatica.ItemCount > 0 )
            {
               A1652Contratante_SSAutomatica = StringUtil.StrToBool( cmbContratante_SSAutomatica.getValidValue(StringUtil.BoolToStr( A1652Contratante_SSAutomatica)));
               n1652Contratante_SSAutomatica = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1652Contratante_SSAutomatica", A1652Contratante_SSAutomatica);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SSAUTOMATICA", GetSecureSignedToken( sPrefix, A1652Contratante_SSAutomatica));
            }
            cmbContratante_SSPrestadoraUnica.Name = "CONTRATANTE_SSPRESTADORAUNICA";
            cmbContratante_SSPrestadoraUnica.WebTags = "";
            cmbContratante_SSPrestadoraUnica.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_SSPrestadoraUnica.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_SSPrestadoraUnica.ItemCount > 0 )
            {
               A1733Contratante_SSPrestadoraUnica = StringUtil.StrToBool( cmbContratante_SSPrestadoraUnica.getValidValue(StringUtil.BoolToStr( A1733Contratante_SSPrestadoraUnica)));
               n1733Contratante_SSPrestadoraUnica = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1733Contratante_SSPrestadoraUnica", A1733Contratante_SSPrestadoraUnica);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SSPRESTADORAUNICA", GetSecureSignedToken( sPrefix, A1733Contratante_SSPrestadoraUnica));
            }
            cmbContratante_SSStatusPlanejamento.Name = "CONTRATANTE_SSSTATUSPLANEJAMENTO";
            cmbContratante_SSStatusPlanejamento.WebTags = "";
            cmbContratante_SSStatusPlanejamento.addItem("", "Stand by", 0);
            cmbContratante_SSStatusPlanejamento.addItem("B", "Stand by", 0);
            cmbContratante_SSStatusPlanejamento.addItem("S", "Solicitada", 0);
            cmbContratante_SSStatusPlanejamento.addItem("E", "Em An�lise", 0);
            cmbContratante_SSStatusPlanejamento.addItem("A", "Em execu��o", 0);
            cmbContratante_SSStatusPlanejamento.addItem("R", "Resolvida", 0);
            cmbContratante_SSStatusPlanejamento.addItem("C", "Conferida", 0);
            cmbContratante_SSStatusPlanejamento.addItem("D", "Retornada", 0);
            cmbContratante_SSStatusPlanejamento.addItem("H", "Homologada", 0);
            cmbContratante_SSStatusPlanejamento.addItem("O", "Aceite", 0);
            cmbContratante_SSStatusPlanejamento.addItem("P", "A Pagar", 0);
            cmbContratante_SSStatusPlanejamento.addItem("L", "Liquidada", 0);
            cmbContratante_SSStatusPlanejamento.addItem("X", "Cancelada", 0);
            cmbContratante_SSStatusPlanejamento.addItem("N", "N�o Faturada", 0);
            cmbContratante_SSStatusPlanejamento.addItem("J", "Planejamento", 0);
            cmbContratante_SSStatusPlanejamento.addItem("I", "An�lise Planejamento", 0);
            cmbContratante_SSStatusPlanejamento.addItem("T", "Validacao T�cnica", 0);
            cmbContratante_SSStatusPlanejamento.addItem("Q", "Validacao Qualidade", 0);
            cmbContratante_SSStatusPlanejamento.addItem("G", "Em Homologa��o", 0);
            cmbContratante_SSStatusPlanejamento.addItem("M", "Valida��o Mensura��o", 0);
            cmbContratante_SSStatusPlanejamento.addItem("U", "Rascunho", 0);
            if ( cmbContratante_SSStatusPlanejamento.ItemCount > 0 )
            {
               A1732Contratante_SSStatusPlanejamento = cmbContratante_SSStatusPlanejamento.getValidValue(A1732Contratante_SSStatusPlanejamento);
               n1732Contratante_SSStatusPlanejamento = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1732Contratante_SSStatusPlanejamento", A1732Contratante_SSStatusPlanejamento);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SSSTATUSPLANEJAMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1732Contratante_SSStatusPlanejamento, ""))));
            }
            dynContratante_ServicoSSPadrao.Name = "CONTRATANTE_SERVICOSSPADRAO";
            dynContratante_ServicoSSPadrao.WebTags = "";
            cmbContratante_RetornaSS.Name = "CONTRATANTE_RETORNASS";
            cmbContratante_RetornaSS.WebTags = "";
            cmbContratante_RetornaSS.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_RetornaSS.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_RetornaSS.ItemCount > 0 )
            {
               A1729Contratante_RetornaSS = StringUtil.StrToBool( cmbContratante_RetornaSS.getValidValue(StringUtil.BoolToStr( A1729Contratante_RetornaSS)));
               n1729Contratante_RetornaSS = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1729Contratante_RetornaSS", A1729Contratante_RetornaSS);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_RETORNASS", GetSecureSignedToken( sPrefix, A1729Contratante_RetornaSS));
            }
            cmbContratante_PropostaRqrSrv.Name = "CONTRATANTE_PROPOSTARQRSRV";
            cmbContratante_PropostaRqrSrv.WebTags = "";
            cmbContratante_PropostaRqrSrv.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_PropostaRqrSrv.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_PropostaRqrSrv.ItemCount > 0 )
            {
               A1738Contratante_PropostaRqrSrv = StringUtil.StrToBool( cmbContratante_PropostaRqrSrv.getValidValue(StringUtil.BoolToStr( A1738Contratante_PropostaRqrSrv)));
               n1738Contratante_PropostaRqrSrv = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1738Contratante_PropostaRqrSrv", A1738Contratante_PropostaRqrSrv);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTARQRSRV", GetSecureSignedToken( sPrefix, A1738Contratante_PropostaRqrSrv));
            }
            cmbContratante_PropostaRqrPrz.Name = "CONTRATANTE_PROPOSTARQRPRZ";
            cmbContratante_PropostaRqrPrz.WebTags = "";
            cmbContratante_PropostaRqrPrz.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_PropostaRqrPrz.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_PropostaRqrPrz.ItemCount > 0 )
            {
               A1739Contratante_PropostaRqrPrz = StringUtil.StrToBool( cmbContratante_PropostaRqrPrz.getValidValue(StringUtil.BoolToStr( A1739Contratante_PropostaRqrPrz)));
               n1739Contratante_PropostaRqrPrz = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1739Contratante_PropostaRqrPrz", A1739Contratante_PropostaRqrPrz);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTARQRPRZ", GetSecureSignedToken( sPrefix, A1739Contratante_PropostaRqrPrz));
            }
            cmbContratante_PropostaRqrEsf.Name = "CONTRATANTE_PROPOSTARQRESF";
            cmbContratante_PropostaRqrEsf.WebTags = "";
            cmbContratante_PropostaRqrEsf.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_PropostaRqrEsf.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_PropostaRqrEsf.ItemCount > 0 )
            {
               A1740Contratante_PropostaRqrEsf = StringUtil.StrToBool( cmbContratante_PropostaRqrEsf.getValidValue(StringUtil.BoolToStr( A1740Contratante_PropostaRqrEsf)));
               n1740Contratante_PropostaRqrEsf = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1740Contratante_PropostaRqrEsf", A1740Contratante_PropostaRqrEsf);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTARQRESF", GetSecureSignedToken( sPrefix, A1740Contratante_PropostaRqrEsf));
            }
            cmbContratante_PropostaRqrCnt.Name = "CONTRATANTE_PROPOSTARQRCNT";
            cmbContratante_PropostaRqrCnt.WebTags = "";
            cmbContratante_PropostaRqrCnt.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_PropostaRqrCnt.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_PropostaRqrCnt.ItemCount > 0 )
            {
               A1741Contratante_PropostaRqrCnt = StringUtil.StrToBool( cmbContratante_PropostaRqrCnt.getValidValue(StringUtil.BoolToStr( A1741Contratante_PropostaRqrCnt)));
               n1741Contratante_PropostaRqrCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1741Contratante_PropostaRqrCnt", A1741Contratante_PropostaRqrCnt);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTARQRCNT", GetSecureSignedToken( sPrefix, A1741Contratante_PropostaRqrCnt));
            }
            cmbContratante_PropostaNvlCnt.Name = "CONTRATANTE_PROPOSTANVLCNT";
            cmbContratante_PropostaNvlCnt.WebTags = "";
            cmbContratante_PropostaNvlCnt.addItem("0", "Unica", 0);
            cmbContratante_PropostaNvlCnt.addItem("1", "Estimada", 0);
            cmbContratante_PropostaNvlCnt.addItem("2", "Detalhada", 0);
            if ( cmbContratante_PropostaNvlCnt.ItemCount > 0 )
            {
               A1742Contratante_PropostaNvlCnt = (short)(NumberUtil.Val( cmbContratante_PropostaNvlCnt.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0))), "."));
               n1742Contratante_PropostaNvlCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1742Contratante_PropostaNvlCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTANVLCNT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1742Contratante_PropostaNvlCnt), "ZZZ9")));
            }
            cmbContratante_OSAutomatica.Name = "CONTRATANTE_OSAUTOMATICA";
            cmbContratante_OSAutomatica.WebTags = "";
            cmbContratante_OSAutomatica.addItem(StringUtil.BoolToStr( true), "Autom�tica", 0);
            cmbContratante_OSAutomatica.addItem(StringUtil.BoolToStr( false), "Manual", 0);
            if ( cmbContratante_OSAutomatica.ItemCount > 0 )
            {
               A593Contratante_OSAutomatica = StringUtil.StrToBool( cmbContratante_OSAutomatica.getValidValue(StringUtil.BoolToStr( A593Contratante_OSAutomatica)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A593Contratante_OSAutomatica", A593Contratante_OSAutomatica);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSAUTOMATICA", GetSecureSignedToken( sPrefix, A593Contratante_OSAutomatica));
            }
            cmbContratante_OSGeraOS.Name = "CONTRATANTE_OSGERAOS";
            cmbContratante_OSGeraOS.WebTags = "";
            cmbContratante_OSGeraOS.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_OSGeraOS.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_OSGeraOS.ItemCount > 0 )
            {
               A1757Contratante_OSGeraOS = StringUtil.StrToBool( cmbContratante_OSGeraOS.getValidValue(StringUtil.BoolToStr( A1757Contratante_OSGeraOS)));
               n1757Contratante_OSGeraOS = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1757Contratante_OSGeraOS", A1757Contratante_OSGeraOS);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERAOS", GetSecureSignedToken( sPrefix, A1757Contratante_OSGeraOS));
            }
            cmbContratante_OSGeraTRP.Name = "CONTRATANTE_OSGERATRP";
            cmbContratante_OSGeraTRP.WebTags = "";
            cmbContratante_OSGeraTRP.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_OSGeraTRP.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_OSGeraTRP.ItemCount > 0 )
            {
               A1758Contratante_OSGeraTRP = StringUtil.StrToBool( cmbContratante_OSGeraTRP.getValidValue(StringUtil.BoolToStr( A1758Contratante_OSGeraTRP)));
               n1758Contratante_OSGeraTRP = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1758Contratante_OSGeraTRP", A1758Contratante_OSGeraTRP);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERATRP", GetSecureSignedToken( sPrefix, A1758Contratante_OSGeraTRP));
            }
            cmbContratante_OSGeraTRD.Name = "CONTRATANTE_OSGERATRD";
            cmbContratante_OSGeraTRD.WebTags = "";
            cmbContratante_OSGeraTRD.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_OSGeraTRD.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_OSGeraTRD.ItemCount > 0 )
            {
               A1760Contratante_OSGeraTRD = StringUtil.StrToBool( cmbContratante_OSGeraTRD.getValidValue(StringUtil.BoolToStr( A1760Contratante_OSGeraTRD)));
               n1760Contratante_OSGeraTRD = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1760Contratante_OSGeraTRD", A1760Contratante_OSGeraTRD);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERATRD", GetSecureSignedToken( sPrefix, A1760Contratante_OSGeraTRD));
            }
            cmbContratante_OSGeraTH.Name = "CONTRATANTE_OSGERATH";
            cmbContratante_OSGeraTH.WebTags = "";
            cmbContratante_OSGeraTH.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_OSGeraTH.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_OSGeraTH.ItemCount > 0 )
            {
               A1759Contratante_OSGeraTH = StringUtil.StrToBool( cmbContratante_OSGeraTH.getValidValue(StringUtil.BoolToStr( A1759Contratante_OSGeraTH)));
               n1759Contratante_OSGeraTH = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1759Contratante_OSGeraTH", A1759Contratante_OSGeraTH);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERATH", GetSecureSignedToken( sPrefix, A1759Contratante_OSGeraTH));
            }
            cmbContratante_OSGeraTR.Name = "CONTRATANTE_OSGERATR";
            cmbContratante_OSGeraTR.WebTags = "";
            cmbContratante_OSGeraTR.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_OSGeraTR.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_OSGeraTR.ItemCount > 0 )
            {
               A1761Contratante_OSGeraTR = StringUtil.StrToBool( cmbContratante_OSGeraTR.getValidValue(StringUtil.BoolToStr( A1761Contratante_OSGeraTR)));
               n1761Contratante_OSGeraTR = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1761Contratante_OSGeraTR", A1761Contratante_OSGeraTR);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERATR", GetSecureSignedToken( sPrefix, A1761Contratante_OSGeraTR));
            }
            cmbContratante_OSHmlgComPnd.Name = "CONTRATANTE_OSHMLGCOMPND";
            cmbContratante_OSHmlgComPnd.WebTags = "";
            cmbContratante_OSHmlgComPnd.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_OSHmlgComPnd.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_OSHmlgComPnd.ItemCount > 0 )
            {
               A1803Contratante_OSHmlgComPnd = StringUtil.StrToBool( cmbContratante_OSHmlgComPnd.getValidValue(StringUtil.BoolToStr( A1803Contratante_OSHmlgComPnd)));
               n1803Contratante_OSHmlgComPnd = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1803Contratante_OSHmlgComPnd", A1803Contratante_OSHmlgComPnd);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSHMLGCOMPND", GetSecureSignedToken( sPrefix, A1803Contratante_OSHmlgComPnd));
            }
            cmbContratante_SelecionaResponsavelOS.Name = "CONTRATANTE_SELECIONARESPONSAVELOS";
            cmbContratante_SelecionaResponsavelOS.WebTags = "";
            cmbContratante_SelecionaResponsavelOS.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_SelecionaResponsavelOS.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_SelecionaResponsavelOS.ItemCount > 0 )
            {
               A2089Contratante_SelecionaResponsavelOS = StringUtil.StrToBool( cmbContratante_SelecionaResponsavelOS.getValidValue(StringUtil.BoolToStr( A2089Contratante_SelecionaResponsavelOS)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2089Contratante_SelecionaResponsavelOS", A2089Contratante_SelecionaResponsavelOS);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SELECIONARESPONSAVELOS", GetSecureSignedToken( sPrefix, A2089Contratante_SelecionaResponsavelOS));
            }
            cmbContratante_ExibePF.Name = "CONTRATANTE_EXIBEPF";
            cmbContratante_ExibePF.WebTags = "";
            cmbContratante_ExibePF.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_ExibePF.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_ExibePF.ItemCount > 0 )
            {
               A2090Contratante_ExibePF = StringUtil.StrToBool( cmbContratante_ExibePF.getValidValue(StringUtil.BoolToStr( A2090Contratante_ExibePF)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2090Contratante_ExibePF", A2090Contratante_ExibePF);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_EXIBEPF", GetSecureSignedToken( sPrefix, A2090Contratante_ExibePF));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLACONTRATANTE_SERVICOSSPADRAO102( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTRATANTE_SERVICOSSPADRAO_data102( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTRATANTE_SERVICOSSPADRAO_html102( )
      {
         int gxdynajaxvalue ;
         GXDLACONTRATANTE_SERVICOSSPADRAO_data102( ) ;
         gxdynajaxindex = 1;
         dynContratante_ServicoSSPadrao.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContratante_ServicoSSPadrao.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynContratante_ServicoSSPadrao.ItemCount > 0 )
         {
            A1727Contratante_ServicoSSPadrao = (int)(NumberUtil.Val( dynContratante_ServicoSSPadrao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0))), "."));
            n1727Contratante_ServicoSSPadrao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1727Contratante_ServicoSSPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SERVICOSSPADRAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1727Contratante_ServicoSSPadrao), "ZZZZZ9")));
         }
      }

      protected void GXDLACONTRATANTE_SERVICOSSPADRAO_data102( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00102 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00102_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00102_A605Servico_Sigla[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratante_UsaOSistema.ItemCount > 0 )
         {
            A1822Contratante_UsaOSistema = StringUtil.StrToBool( cmbContratante_UsaOSistema.getValidValue(StringUtil.BoolToStr( A1822Contratante_UsaOSistema)));
            n1822Contratante_UsaOSistema = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1822Contratante_UsaOSistema", A1822Contratante_UsaOSistema);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_USAOSISTEMA", GetSecureSignedToken( sPrefix, A1822Contratante_UsaOSistema));
         }
         if ( cmbContratante_SSAutomatica.ItemCount > 0 )
         {
            A1652Contratante_SSAutomatica = StringUtil.StrToBool( cmbContratante_SSAutomatica.getValidValue(StringUtil.BoolToStr( A1652Contratante_SSAutomatica)));
            n1652Contratante_SSAutomatica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1652Contratante_SSAutomatica", A1652Contratante_SSAutomatica);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SSAUTOMATICA", GetSecureSignedToken( sPrefix, A1652Contratante_SSAutomatica));
         }
         if ( cmbContratante_SSPrestadoraUnica.ItemCount > 0 )
         {
            A1733Contratante_SSPrestadoraUnica = StringUtil.StrToBool( cmbContratante_SSPrestadoraUnica.getValidValue(StringUtil.BoolToStr( A1733Contratante_SSPrestadoraUnica)));
            n1733Contratante_SSPrestadoraUnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1733Contratante_SSPrestadoraUnica", A1733Contratante_SSPrestadoraUnica);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SSPRESTADORAUNICA", GetSecureSignedToken( sPrefix, A1733Contratante_SSPrestadoraUnica));
         }
         if ( cmbContratante_SSStatusPlanejamento.ItemCount > 0 )
         {
            A1732Contratante_SSStatusPlanejamento = cmbContratante_SSStatusPlanejamento.getValidValue(A1732Contratante_SSStatusPlanejamento);
            n1732Contratante_SSStatusPlanejamento = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1732Contratante_SSStatusPlanejamento", A1732Contratante_SSStatusPlanejamento);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SSSTATUSPLANEJAMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1732Contratante_SSStatusPlanejamento, ""))));
         }
         if ( dynContratante_ServicoSSPadrao.ItemCount > 0 )
         {
            A1727Contratante_ServicoSSPadrao = (int)(NumberUtil.Val( dynContratante_ServicoSSPadrao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0))), "."));
            n1727Contratante_ServicoSSPadrao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1727Contratante_ServicoSSPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SERVICOSSPADRAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1727Contratante_ServicoSSPadrao), "ZZZZZ9")));
         }
         if ( cmbContratante_RetornaSS.ItemCount > 0 )
         {
            A1729Contratante_RetornaSS = StringUtil.StrToBool( cmbContratante_RetornaSS.getValidValue(StringUtil.BoolToStr( A1729Contratante_RetornaSS)));
            n1729Contratante_RetornaSS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1729Contratante_RetornaSS", A1729Contratante_RetornaSS);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_RETORNASS", GetSecureSignedToken( sPrefix, A1729Contratante_RetornaSS));
         }
         if ( cmbContratante_PropostaRqrSrv.ItemCount > 0 )
         {
            A1738Contratante_PropostaRqrSrv = StringUtil.StrToBool( cmbContratante_PropostaRqrSrv.getValidValue(StringUtil.BoolToStr( A1738Contratante_PropostaRqrSrv)));
            n1738Contratante_PropostaRqrSrv = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1738Contratante_PropostaRqrSrv", A1738Contratante_PropostaRqrSrv);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTARQRSRV", GetSecureSignedToken( sPrefix, A1738Contratante_PropostaRqrSrv));
         }
         if ( cmbContratante_PropostaRqrPrz.ItemCount > 0 )
         {
            A1739Contratante_PropostaRqrPrz = StringUtil.StrToBool( cmbContratante_PropostaRqrPrz.getValidValue(StringUtil.BoolToStr( A1739Contratante_PropostaRqrPrz)));
            n1739Contratante_PropostaRqrPrz = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1739Contratante_PropostaRqrPrz", A1739Contratante_PropostaRqrPrz);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTARQRPRZ", GetSecureSignedToken( sPrefix, A1739Contratante_PropostaRqrPrz));
         }
         if ( cmbContratante_PropostaRqrEsf.ItemCount > 0 )
         {
            A1740Contratante_PropostaRqrEsf = StringUtil.StrToBool( cmbContratante_PropostaRqrEsf.getValidValue(StringUtil.BoolToStr( A1740Contratante_PropostaRqrEsf)));
            n1740Contratante_PropostaRqrEsf = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1740Contratante_PropostaRqrEsf", A1740Contratante_PropostaRqrEsf);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTARQRESF", GetSecureSignedToken( sPrefix, A1740Contratante_PropostaRqrEsf));
         }
         if ( cmbContratante_PropostaRqrCnt.ItemCount > 0 )
         {
            A1741Contratante_PropostaRqrCnt = StringUtil.StrToBool( cmbContratante_PropostaRqrCnt.getValidValue(StringUtil.BoolToStr( A1741Contratante_PropostaRqrCnt)));
            n1741Contratante_PropostaRqrCnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1741Contratante_PropostaRqrCnt", A1741Contratante_PropostaRqrCnt);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTARQRCNT", GetSecureSignedToken( sPrefix, A1741Contratante_PropostaRqrCnt));
         }
         if ( cmbContratante_PropostaNvlCnt.ItemCount > 0 )
         {
            A1742Contratante_PropostaNvlCnt = (short)(NumberUtil.Val( cmbContratante_PropostaNvlCnt.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0))), "."));
            n1742Contratante_PropostaNvlCnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1742Contratante_PropostaNvlCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTANVLCNT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1742Contratante_PropostaNvlCnt), "ZZZ9")));
         }
         if ( cmbContratante_OSAutomatica.ItemCount > 0 )
         {
            A593Contratante_OSAutomatica = StringUtil.StrToBool( cmbContratante_OSAutomatica.getValidValue(StringUtil.BoolToStr( A593Contratante_OSAutomatica)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A593Contratante_OSAutomatica", A593Contratante_OSAutomatica);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSAUTOMATICA", GetSecureSignedToken( sPrefix, A593Contratante_OSAutomatica));
         }
         if ( cmbContratante_OSGeraOS.ItemCount > 0 )
         {
            A1757Contratante_OSGeraOS = StringUtil.StrToBool( cmbContratante_OSGeraOS.getValidValue(StringUtil.BoolToStr( A1757Contratante_OSGeraOS)));
            n1757Contratante_OSGeraOS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1757Contratante_OSGeraOS", A1757Contratante_OSGeraOS);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERAOS", GetSecureSignedToken( sPrefix, A1757Contratante_OSGeraOS));
         }
         if ( cmbContratante_OSGeraTRP.ItemCount > 0 )
         {
            A1758Contratante_OSGeraTRP = StringUtil.StrToBool( cmbContratante_OSGeraTRP.getValidValue(StringUtil.BoolToStr( A1758Contratante_OSGeraTRP)));
            n1758Contratante_OSGeraTRP = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1758Contratante_OSGeraTRP", A1758Contratante_OSGeraTRP);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERATRP", GetSecureSignedToken( sPrefix, A1758Contratante_OSGeraTRP));
         }
         if ( cmbContratante_OSGeraTRD.ItemCount > 0 )
         {
            A1760Contratante_OSGeraTRD = StringUtil.StrToBool( cmbContratante_OSGeraTRD.getValidValue(StringUtil.BoolToStr( A1760Contratante_OSGeraTRD)));
            n1760Contratante_OSGeraTRD = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1760Contratante_OSGeraTRD", A1760Contratante_OSGeraTRD);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERATRD", GetSecureSignedToken( sPrefix, A1760Contratante_OSGeraTRD));
         }
         if ( cmbContratante_OSGeraTH.ItemCount > 0 )
         {
            A1759Contratante_OSGeraTH = StringUtil.StrToBool( cmbContratante_OSGeraTH.getValidValue(StringUtil.BoolToStr( A1759Contratante_OSGeraTH)));
            n1759Contratante_OSGeraTH = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1759Contratante_OSGeraTH", A1759Contratante_OSGeraTH);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERATH", GetSecureSignedToken( sPrefix, A1759Contratante_OSGeraTH));
         }
         if ( cmbContratante_OSGeraTR.ItemCount > 0 )
         {
            A1761Contratante_OSGeraTR = StringUtil.StrToBool( cmbContratante_OSGeraTR.getValidValue(StringUtil.BoolToStr( A1761Contratante_OSGeraTR)));
            n1761Contratante_OSGeraTR = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1761Contratante_OSGeraTR", A1761Contratante_OSGeraTR);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERATR", GetSecureSignedToken( sPrefix, A1761Contratante_OSGeraTR));
         }
         if ( cmbContratante_OSHmlgComPnd.ItemCount > 0 )
         {
            A1803Contratante_OSHmlgComPnd = StringUtil.StrToBool( cmbContratante_OSHmlgComPnd.getValidValue(StringUtil.BoolToStr( A1803Contratante_OSHmlgComPnd)));
            n1803Contratante_OSHmlgComPnd = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1803Contratante_OSHmlgComPnd", A1803Contratante_OSHmlgComPnd);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSHMLGCOMPND", GetSecureSignedToken( sPrefix, A1803Contratante_OSHmlgComPnd));
         }
         if ( cmbContratante_SelecionaResponsavelOS.ItemCount > 0 )
         {
            A2089Contratante_SelecionaResponsavelOS = StringUtil.StrToBool( cmbContratante_SelecionaResponsavelOS.getValidValue(StringUtil.BoolToStr( A2089Contratante_SelecionaResponsavelOS)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2089Contratante_SelecionaResponsavelOS", A2089Contratante_SelecionaResponsavelOS);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SELECIONARESPONSAVELOS", GetSecureSignedToken( sPrefix, A2089Contratante_SelecionaResponsavelOS));
         }
         if ( cmbContratante_ExibePF.ItemCount > 0 )
         {
            A2090Contratante_ExibePF = StringUtil.StrToBool( cmbContratante_ExibePF.getValidValue(StringUtil.BoolToStr( A2090Contratante_ExibePF)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2090Contratante_ExibePF", A2090Contratante_ExibePF);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_EXIBEPF", GetSecureSignedToken( sPrefix, A2090Contratante_ExibePF));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF102( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV16Pgmname = "ContratanteGeneral";
         context.Gx_err = 0;
      }

      protected void RF102( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00103 */
            pr_default.execute(1, new Object[] {A29Contratante_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A335Contratante_PessoaCod = H00103_A335Contratante_PessoaCod[0];
               A25Municipio_Codigo = H00103_A25Municipio_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MUNICIPIO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")));
               n25Municipio_Codigo = H00103_n25Municipio_Codigo[0];
               A2034Contratante_TtlRltGerencial = H00103_A2034Contratante_TtlRltGerencial[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2034Contratante_TtlRltGerencial", A2034Contratante_TtlRltGerencial);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_TTLRLTGERENCIAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2034Contratante_TtlRltGerencial, ""))));
               n2034Contratante_TtlRltGerencial = H00103_n2034Contratante_TtlRltGerencial[0];
               A1808Contratante_ReceitaBruta = H00103_A1808Contratante_ReceitaBruta[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1808Contratante_ReceitaBruta", StringUtil.LTrim( StringUtil.Str( A1808Contratante_ReceitaBruta, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_RECEITABRUTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1808Contratante_ReceitaBruta, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               n1808Contratante_ReceitaBruta = H00103_n1808Contratante_ReceitaBruta[0];
               A1807Contratante_PatrimonioLiquido = H00103_A1807Contratante_PatrimonioLiquido[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1807Contratante_PatrimonioLiquido", StringUtil.LTrim( StringUtil.Str( A1807Contratante_PatrimonioLiquido, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PATRIMONIOLIQUIDO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1807Contratante_PatrimonioLiquido, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               n1807Contratante_PatrimonioLiquido = H00103_n1807Contratante_PatrimonioLiquido[0];
               A1806Contratante_PassivoCirculante = H00103_A1806Contratante_PassivoCirculante[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1806Contratante_PassivoCirculante", StringUtil.LTrim( StringUtil.Str( A1806Contratante_PassivoCirculante, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PASSIVOCIRCULANTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1806Contratante_PassivoCirculante, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               n1806Contratante_PassivoCirculante = H00103_n1806Contratante_PassivoCirculante[0];
               A1805Contratante_AtivoCirculante = H00103_A1805Contratante_AtivoCirculante[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1805Contratante_AtivoCirculante", StringUtil.LTrim( StringUtil.Str( A1805Contratante_AtivoCirculante, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_ATIVOCIRCULANTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1805Contratante_AtivoCirculante, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               n1805Contratante_AtivoCirculante = H00103_n1805Contratante_AtivoCirculante[0];
               A2090Contratante_ExibePF = H00103_A2090Contratante_ExibePF[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2090Contratante_ExibePF", A2090Contratante_ExibePF);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_EXIBEPF", GetSecureSignedToken( sPrefix, A2090Contratante_ExibePF));
               A2089Contratante_SelecionaResponsavelOS = H00103_A2089Contratante_SelecionaResponsavelOS[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2089Contratante_SelecionaResponsavelOS", A2089Contratante_SelecionaResponsavelOS);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SELECIONARESPONSAVELOS", GetSecureSignedToken( sPrefix, A2089Contratante_SelecionaResponsavelOS));
               A1803Contratante_OSHmlgComPnd = H00103_A1803Contratante_OSHmlgComPnd[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1803Contratante_OSHmlgComPnd", A1803Contratante_OSHmlgComPnd);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSHMLGCOMPND", GetSecureSignedToken( sPrefix, A1803Contratante_OSHmlgComPnd));
               n1803Contratante_OSHmlgComPnd = H00103_n1803Contratante_OSHmlgComPnd[0];
               A1761Contratante_OSGeraTR = H00103_A1761Contratante_OSGeraTR[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1761Contratante_OSGeraTR", A1761Contratante_OSGeraTR);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERATR", GetSecureSignedToken( sPrefix, A1761Contratante_OSGeraTR));
               n1761Contratante_OSGeraTR = H00103_n1761Contratante_OSGeraTR[0];
               A1759Contratante_OSGeraTH = H00103_A1759Contratante_OSGeraTH[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1759Contratante_OSGeraTH", A1759Contratante_OSGeraTH);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERATH", GetSecureSignedToken( sPrefix, A1759Contratante_OSGeraTH));
               n1759Contratante_OSGeraTH = H00103_n1759Contratante_OSGeraTH[0];
               A1760Contratante_OSGeraTRD = H00103_A1760Contratante_OSGeraTRD[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1760Contratante_OSGeraTRD", A1760Contratante_OSGeraTRD);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERATRD", GetSecureSignedToken( sPrefix, A1760Contratante_OSGeraTRD));
               n1760Contratante_OSGeraTRD = H00103_n1760Contratante_OSGeraTRD[0];
               A1758Contratante_OSGeraTRP = H00103_A1758Contratante_OSGeraTRP[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1758Contratante_OSGeraTRP", A1758Contratante_OSGeraTRP);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERATRP", GetSecureSignedToken( sPrefix, A1758Contratante_OSGeraTRP));
               n1758Contratante_OSGeraTRP = H00103_n1758Contratante_OSGeraTRP[0];
               A1757Contratante_OSGeraOS = H00103_A1757Contratante_OSGeraOS[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1757Contratante_OSGeraOS", A1757Contratante_OSGeraOS);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERAOS", GetSecureSignedToken( sPrefix, A1757Contratante_OSGeraOS));
               n1757Contratante_OSGeraOS = H00103_n1757Contratante_OSGeraOS[0];
               A593Contratante_OSAutomatica = H00103_A593Contratante_OSAutomatica[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A593Contratante_OSAutomatica", A593Contratante_OSAutomatica);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSAUTOMATICA", GetSecureSignedToken( sPrefix, A593Contratante_OSAutomatica));
               A1742Contratante_PropostaNvlCnt = H00103_A1742Contratante_PropostaNvlCnt[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1742Contratante_PropostaNvlCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTANVLCNT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1742Contratante_PropostaNvlCnt), "ZZZ9")));
               n1742Contratante_PropostaNvlCnt = H00103_n1742Contratante_PropostaNvlCnt[0];
               A1741Contratante_PropostaRqrCnt = H00103_A1741Contratante_PropostaRqrCnt[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1741Contratante_PropostaRqrCnt", A1741Contratante_PropostaRqrCnt);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTARQRCNT", GetSecureSignedToken( sPrefix, A1741Contratante_PropostaRqrCnt));
               n1741Contratante_PropostaRqrCnt = H00103_n1741Contratante_PropostaRqrCnt[0];
               A1740Contratante_PropostaRqrEsf = H00103_A1740Contratante_PropostaRqrEsf[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1740Contratante_PropostaRqrEsf", A1740Contratante_PropostaRqrEsf);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTARQRESF", GetSecureSignedToken( sPrefix, A1740Contratante_PropostaRqrEsf));
               n1740Contratante_PropostaRqrEsf = H00103_n1740Contratante_PropostaRqrEsf[0];
               A1739Contratante_PropostaRqrPrz = H00103_A1739Contratante_PropostaRqrPrz[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1739Contratante_PropostaRqrPrz", A1739Contratante_PropostaRqrPrz);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTARQRPRZ", GetSecureSignedToken( sPrefix, A1739Contratante_PropostaRqrPrz));
               n1739Contratante_PropostaRqrPrz = H00103_n1739Contratante_PropostaRqrPrz[0];
               A1738Contratante_PropostaRqrSrv = H00103_A1738Contratante_PropostaRqrSrv[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1738Contratante_PropostaRqrSrv", A1738Contratante_PropostaRqrSrv);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTARQRSRV", GetSecureSignedToken( sPrefix, A1738Contratante_PropostaRqrSrv));
               n1738Contratante_PropostaRqrSrv = H00103_n1738Contratante_PropostaRqrSrv[0];
               A1729Contratante_RetornaSS = H00103_A1729Contratante_RetornaSS[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1729Contratante_RetornaSS", A1729Contratante_RetornaSS);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_RETORNASS", GetSecureSignedToken( sPrefix, A1729Contratante_RetornaSS));
               n1729Contratante_RetornaSS = H00103_n1729Contratante_RetornaSS[0];
               A1727Contratante_ServicoSSPadrao = H00103_A1727Contratante_ServicoSSPadrao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1727Contratante_ServicoSSPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SERVICOSSPADRAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1727Contratante_ServicoSSPadrao), "ZZZZZ9")));
               n1727Contratante_ServicoSSPadrao = H00103_n1727Contratante_ServicoSSPadrao[0];
               A1732Contratante_SSStatusPlanejamento = H00103_A1732Contratante_SSStatusPlanejamento[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1732Contratante_SSStatusPlanejamento", A1732Contratante_SSStatusPlanejamento);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SSSTATUSPLANEJAMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1732Contratante_SSStatusPlanejamento, ""))));
               n1732Contratante_SSStatusPlanejamento = H00103_n1732Contratante_SSStatusPlanejamento[0];
               A1733Contratante_SSPrestadoraUnica = H00103_A1733Contratante_SSPrestadoraUnica[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1733Contratante_SSPrestadoraUnica", A1733Contratante_SSPrestadoraUnica);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SSPRESTADORAUNICA", GetSecureSignedToken( sPrefix, A1733Contratante_SSPrestadoraUnica));
               n1733Contratante_SSPrestadoraUnica = H00103_n1733Contratante_SSPrestadoraUnica[0];
               A1652Contratante_SSAutomatica = H00103_A1652Contratante_SSAutomatica[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1652Contratante_SSAutomatica", A1652Contratante_SSAutomatica);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SSAUTOMATICA", GetSecureSignedToken( sPrefix, A1652Contratante_SSAutomatica));
               n1652Contratante_SSAutomatica = H00103_n1652Contratante_SSAutomatica[0];
               A30Contratante_Ativo = H00103_A30Contratante_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A30Contratante_Ativo", A30Contratante_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_ATIVO", GetSecureSignedToken( sPrefix, A30Contratante_Ativo));
               A1822Contratante_UsaOSistema = H00103_A1822Contratante_UsaOSistema[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1822Contratante_UsaOSistema", A1822Contratante_UsaOSistema);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_USAOSISTEMA", GetSecureSignedToken( sPrefix, A1822Contratante_UsaOSistema));
               n1822Contratante_UsaOSistema = H00103_n1822Contratante_UsaOSistema[0];
               A1192Contratante_FimDoExpediente = H00103_A1192Contratante_FimDoExpediente[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1192Contratante_FimDoExpediente", context.localUtil.TToC( A1192Contratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_FIMDOEXPEDIENTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1192Contratante_FimDoExpediente, "99:99")));
               n1192Contratante_FimDoExpediente = H00103_n1192Contratante_FimDoExpediente[0];
               A1448Contratante_InicioDoExpediente = H00103_A1448Contratante_InicioDoExpediente[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1448Contratante_InicioDoExpediente", context.localUtil.TToC( A1448Contratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_INICIODOEXPEDIENTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1448Contratante_InicioDoExpediente, "99:99")));
               n1448Contratante_InicioDoExpediente = H00103_n1448Contratante_InicioDoExpediente[0];
               A23Estado_UF = H00103_A23Estado_UF[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A23Estado_UF", A23Estado_UF);
               A26Municipio_Nome = H00103_A26Municipio_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A26Municipio_Nome", A26Municipio_Nome);
               A16Contratante_ContaCorrente = H00103_A16Contratante_ContaCorrente[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A16Contratante_ContaCorrente", A16Contratante_ContaCorrente);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_CONTACORRENTE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A16Contratante_ContaCorrente, ""))));
               n16Contratante_ContaCorrente = H00103_n16Contratante_ContaCorrente[0];
               A337Contratante_AgenciaNro = H00103_A337Contratante_AgenciaNro[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A337Contratante_AgenciaNro", A337Contratante_AgenciaNro);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_AGENCIANRO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A337Contratante_AgenciaNro, ""))));
               n337Contratante_AgenciaNro = H00103_n337Contratante_AgenciaNro[0];
               A336Contratante_AgenciaNome = H00103_A336Contratante_AgenciaNome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A336Contratante_AgenciaNome", A336Contratante_AgenciaNome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_AGENCIANOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A336Contratante_AgenciaNome, "@!"))));
               n336Contratante_AgenciaNome = H00103_n336Contratante_AgenciaNome[0];
               A339Contratante_BancoNro = H00103_A339Contratante_BancoNro[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A339Contratante_BancoNro", A339Contratante_BancoNro);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_BANCONRO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A339Contratante_BancoNro, ""))));
               n339Contratante_BancoNro = H00103_n339Contratante_BancoNro[0];
               A338Contratante_BancoNome = H00103_A338Contratante_BancoNome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A338Contratante_BancoNome", A338Contratante_BancoNome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_BANCONOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A338Contratante_BancoNome, "@!"))));
               n338Contratante_BancoNome = H00103_n338Contratante_BancoNome[0];
               A13Contratante_WebSite = H00103_A13Contratante_WebSite[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A13Contratante_WebSite", A13Contratante_WebSite);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_WEBSITE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A13Contratante_WebSite, ""))));
               n13Contratante_WebSite = H00103_n13Contratante_WebSite[0];
               A33Contratante_Fax = H00103_A33Contratante_Fax[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A33Contratante_Fax", A33Contratante_Fax);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_FAX", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A33Contratante_Fax, ""))));
               n33Contratante_Fax = H00103_n33Contratante_Fax[0];
               A32Contratante_Ramal = H00103_A32Contratante_Ramal[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A32Contratante_Ramal", A32Contratante_Ramal);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_RAMAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A32Contratante_Ramal, ""))));
               n32Contratante_Ramal = H00103_n32Contratante_Ramal[0];
               A31Contratante_Telefone = H00103_A31Contratante_Telefone[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A31Contratante_Telefone", A31Contratante_Telefone);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_TELEFONE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A31Contratante_Telefone, ""))));
               A11Contratante_IE = H00103_A11Contratante_IE[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A11Contratante_IE", A11Contratante_IE);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_IE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A11Contratante_IE, ""))));
               A12Contratante_CNPJ = H00103_A12Contratante_CNPJ[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A12Contratante_CNPJ", A12Contratante_CNPJ);
               n12Contratante_CNPJ = H00103_n12Contratante_CNPJ[0];
               A2208Contratante_Sigla = H00103_A2208Contratante_Sigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2208Contratante_Sigla", A2208Contratante_Sigla);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2208Contratante_Sigla, "@!"))));
               n2208Contratante_Sigla = H00103_n2208Contratante_Sigla[0];
               A9Contratante_RazaoSocial = H00103_A9Contratante_RazaoSocial[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A9Contratante_RazaoSocial", A9Contratante_RazaoSocial);
               n9Contratante_RazaoSocial = H00103_n9Contratante_RazaoSocial[0];
               A12Contratante_CNPJ = H00103_A12Contratante_CNPJ[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A12Contratante_CNPJ", A12Contratante_CNPJ);
               n12Contratante_CNPJ = H00103_n12Contratante_CNPJ[0];
               A9Contratante_RazaoSocial = H00103_A9Contratante_RazaoSocial[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A9Contratante_RazaoSocial", A9Contratante_RazaoSocial);
               n9Contratante_RazaoSocial = H00103_n9Contratante_RazaoSocial[0];
               A23Estado_UF = H00103_A23Estado_UF[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A23Estado_UF", A23Estado_UF);
               A26Municipio_Nome = H00103_A26Municipio_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A26Municipio_Nome", A26Municipio_Nome);
               GXACONTRATANTE_SERVICOSSPADRAO_html102( ) ;
               /* Execute user event: E12102 */
               E12102 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            WB100( ) ;
         }
      }

      protected void STRUP100( )
      {
         /* Before Start, stand alone formulas. */
         AV16Pgmname = "ContratanteGeneral";
         context.Gx_err = 0;
         GXACONTRATANTE_SERVICOSSPADRAO_html102( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11102 */
         E11102 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A9Contratante_RazaoSocial = StringUtil.Upper( cgiGet( edtContratante_RazaoSocial_Internalname));
            n9Contratante_RazaoSocial = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A9Contratante_RazaoSocial", A9Contratante_RazaoSocial);
            A2208Contratante_Sigla = StringUtil.Upper( cgiGet( edtContratante_Sigla_Internalname));
            n2208Contratante_Sigla = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2208Contratante_Sigla", A2208Contratante_Sigla);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2208Contratante_Sigla, "@!"))));
            A12Contratante_CNPJ = cgiGet( edtContratante_CNPJ_Internalname);
            n12Contratante_CNPJ = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A12Contratante_CNPJ", A12Contratante_CNPJ);
            A11Contratante_IE = cgiGet( edtContratante_IE_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A11Contratante_IE", A11Contratante_IE);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_IE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A11Contratante_IE, ""))));
            A31Contratante_Telefone = cgiGet( edtContratante_Telefone_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A31Contratante_Telefone", A31Contratante_Telefone);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_TELEFONE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A31Contratante_Telefone, ""))));
            A32Contratante_Ramal = cgiGet( edtContratante_Ramal_Internalname);
            n32Contratante_Ramal = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A32Contratante_Ramal", A32Contratante_Ramal);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_RAMAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A32Contratante_Ramal, ""))));
            A33Contratante_Fax = cgiGet( edtContratante_Fax_Internalname);
            n33Contratante_Fax = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A33Contratante_Fax", A33Contratante_Fax);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_FAX", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A33Contratante_Fax, ""))));
            A13Contratante_WebSite = cgiGet( edtContratante_WebSite_Internalname);
            n13Contratante_WebSite = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A13Contratante_WebSite", A13Contratante_WebSite);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_WEBSITE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A13Contratante_WebSite, ""))));
            A338Contratante_BancoNome = StringUtil.Upper( cgiGet( edtContratante_BancoNome_Internalname));
            n338Contratante_BancoNome = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A338Contratante_BancoNome", A338Contratante_BancoNome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_BANCONOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A338Contratante_BancoNome, "@!"))));
            A339Contratante_BancoNro = cgiGet( edtContratante_BancoNro_Internalname);
            n339Contratante_BancoNro = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A339Contratante_BancoNro", A339Contratante_BancoNro);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_BANCONRO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A339Contratante_BancoNro, ""))));
            A336Contratante_AgenciaNome = StringUtil.Upper( cgiGet( edtContratante_AgenciaNome_Internalname));
            n336Contratante_AgenciaNome = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A336Contratante_AgenciaNome", A336Contratante_AgenciaNome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_AGENCIANOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A336Contratante_AgenciaNome, "@!"))));
            A337Contratante_AgenciaNro = cgiGet( edtContratante_AgenciaNro_Internalname);
            n337Contratante_AgenciaNro = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A337Contratante_AgenciaNro", A337Contratante_AgenciaNro);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_AGENCIANRO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A337Contratante_AgenciaNro, ""))));
            A16Contratante_ContaCorrente = cgiGet( edtContratante_ContaCorrente_Internalname);
            n16Contratante_ContaCorrente = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A16Contratante_ContaCorrente", A16Contratante_ContaCorrente);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_CONTACORRENTE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A16Contratante_ContaCorrente, ""))));
            A26Municipio_Nome = StringUtil.Upper( cgiGet( edtMunicipio_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A26Municipio_Nome", A26Municipio_Nome);
            A23Estado_UF = StringUtil.Upper( cgiGet( edtEstado_UF_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A23Estado_UF", A23Estado_UF);
            A1448Contratante_InicioDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( edtContratante_InicioDoExpediente_Internalname), 0));
            n1448Contratante_InicioDoExpediente = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1448Contratante_InicioDoExpediente", context.localUtil.TToC( A1448Contratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_INICIODOEXPEDIENTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1448Contratante_InicioDoExpediente, "99:99")));
            A1192Contratante_FimDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( edtContratante_FimDoExpediente_Internalname), 0));
            n1192Contratante_FimDoExpediente = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1192Contratante_FimDoExpediente", context.localUtil.TToC( A1192Contratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_FIMDOEXPEDIENTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1192Contratante_FimDoExpediente, "99:99")));
            cmbContratante_UsaOSistema.CurrentValue = cgiGet( cmbContratante_UsaOSistema_Internalname);
            A1822Contratante_UsaOSistema = StringUtil.StrToBool( cgiGet( cmbContratante_UsaOSistema_Internalname));
            n1822Contratante_UsaOSistema = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1822Contratante_UsaOSistema", A1822Contratante_UsaOSistema);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_USAOSISTEMA", GetSecureSignedToken( sPrefix, A1822Contratante_UsaOSistema));
            A30Contratante_Ativo = StringUtil.StrToBool( cgiGet( chkContratante_Ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A30Contratante_Ativo", A30Contratante_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_ATIVO", GetSecureSignedToken( sPrefix, A30Contratante_Ativo));
            cmbContratante_SSAutomatica.CurrentValue = cgiGet( cmbContratante_SSAutomatica_Internalname);
            A1652Contratante_SSAutomatica = StringUtil.StrToBool( cgiGet( cmbContratante_SSAutomatica_Internalname));
            n1652Contratante_SSAutomatica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1652Contratante_SSAutomatica", A1652Contratante_SSAutomatica);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SSAUTOMATICA", GetSecureSignedToken( sPrefix, A1652Contratante_SSAutomatica));
            cmbContratante_SSPrestadoraUnica.CurrentValue = cgiGet( cmbContratante_SSPrestadoraUnica_Internalname);
            A1733Contratante_SSPrestadoraUnica = StringUtil.StrToBool( cgiGet( cmbContratante_SSPrestadoraUnica_Internalname));
            n1733Contratante_SSPrestadoraUnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1733Contratante_SSPrestadoraUnica", A1733Contratante_SSPrestadoraUnica);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SSPRESTADORAUNICA", GetSecureSignedToken( sPrefix, A1733Contratante_SSPrestadoraUnica));
            cmbContratante_SSStatusPlanejamento.CurrentValue = cgiGet( cmbContratante_SSStatusPlanejamento_Internalname);
            A1732Contratante_SSStatusPlanejamento = cgiGet( cmbContratante_SSStatusPlanejamento_Internalname);
            n1732Contratante_SSStatusPlanejamento = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1732Contratante_SSStatusPlanejamento", A1732Contratante_SSStatusPlanejamento);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SSSTATUSPLANEJAMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1732Contratante_SSStatusPlanejamento, ""))));
            dynContratante_ServicoSSPadrao.CurrentValue = cgiGet( dynContratante_ServicoSSPadrao_Internalname);
            A1727Contratante_ServicoSSPadrao = (int)(NumberUtil.Val( cgiGet( dynContratante_ServicoSSPadrao_Internalname), "."));
            n1727Contratante_ServicoSSPadrao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1727Contratante_ServicoSSPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SERVICOSSPADRAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1727Contratante_ServicoSSPadrao), "ZZZZZ9")));
            cmbContratante_RetornaSS.CurrentValue = cgiGet( cmbContratante_RetornaSS_Internalname);
            A1729Contratante_RetornaSS = StringUtil.StrToBool( cgiGet( cmbContratante_RetornaSS_Internalname));
            n1729Contratante_RetornaSS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1729Contratante_RetornaSS", A1729Contratante_RetornaSS);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_RETORNASS", GetSecureSignedToken( sPrefix, A1729Contratante_RetornaSS));
            cmbContratante_PropostaRqrSrv.CurrentValue = cgiGet( cmbContratante_PropostaRqrSrv_Internalname);
            A1738Contratante_PropostaRqrSrv = StringUtil.StrToBool( cgiGet( cmbContratante_PropostaRqrSrv_Internalname));
            n1738Contratante_PropostaRqrSrv = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1738Contratante_PropostaRqrSrv", A1738Contratante_PropostaRqrSrv);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTARQRSRV", GetSecureSignedToken( sPrefix, A1738Contratante_PropostaRqrSrv));
            cmbContratante_PropostaRqrPrz.CurrentValue = cgiGet( cmbContratante_PropostaRqrPrz_Internalname);
            A1739Contratante_PropostaRqrPrz = StringUtil.StrToBool( cgiGet( cmbContratante_PropostaRqrPrz_Internalname));
            n1739Contratante_PropostaRqrPrz = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1739Contratante_PropostaRqrPrz", A1739Contratante_PropostaRqrPrz);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTARQRPRZ", GetSecureSignedToken( sPrefix, A1739Contratante_PropostaRqrPrz));
            cmbContratante_PropostaRqrEsf.CurrentValue = cgiGet( cmbContratante_PropostaRqrEsf_Internalname);
            A1740Contratante_PropostaRqrEsf = StringUtil.StrToBool( cgiGet( cmbContratante_PropostaRqrEsf_Internalname));
            n1740Contratante_PropostaRqrEsf = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1740Contratante_PropostaRqrEsf", A1740Contratante_PropostaRqrEsf);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTARQRESF", GetSecureSignedToken( sPrefix, A1740Contratante_PropostaRqrEsf));
            cmbContratante_PropostaRqrCnt.CurrentValue = cgiGet( cmbContratante_PropostaRqrCnt_Internalname);
            A1741Contratante_PropostaRqrCnt = StringUtil.StrToBool( cgiGet( cmbContratante_PropostaRqrCnt_Internalname));
            n1741Contratante_PropostaRqrCnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1741Contratante_PropostaRqrCnt", A1741Contratante_PropostaRqrCnt);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTARQRCNT", GetSecureSignedToken( sPrefix, A1741Contratante_PropostaRqrCnt));
            cmbContratante_PropostaNvlCnt.CurrentValue = cgiGet( cmbContratante_PropostaNvlCnt_Internalname);
            A1742Contratante_PropostaNvlCnt = (short)(NumberUtil.Val( cgiGet( cmbContratante_PropostaNvlCnt_Internalname), "."));
            n1742Contratante_PropostaNvlCnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1742Contratante_PropostaNvlCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PROPOSTANVLCNT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1742Contratante_PropostaNvlCnt), "ZZZ9")));
            cmbContratante_OSAutomatica.CurrentValue = cgiGet( cmbContratante_OSAutomatica_Internalname);
            A593Contratante_OSAutomatica = StringUtil.StrToBool( cgiGet( cmbContratante_OSAutomatica_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A593Contratante_OSAutomatica", A593Contratante_OSAutomatica);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSAUTOMATICA", GetSecureSignedToken( sPrefix, A593Contratante_OSAutomatica));
            cmbContratante_OSGeraOS.CurrentValue = cgiGet( cmbContratante_OSGeraOS_Internalname);
            A1757Contratante_OSGeraOS = StringUtil.StrToBool( cgiGet( cmbContratante_OSGeraOS_Internalname));
            n1757Contratante_OSGeraOS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1757Contratante_OSGeraOS", A1757Contratante_OSGeraOS);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERAOS", GetSecureSignedToken( sPrefix, A1757Contratante_OSGeraOS));
            cmbContratante_OSGeraTRP.CurrentValue = cgiGet( cmbContratante_OSGeraTRP_Internalname);
            A1758Contratante_OSGeraTRP = StringUtil.StrToBool( cgiGet( cmbContratante_OSGeraTRP_Internalname));
            n1758Contratante_OSGeraTRP = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1758Contratante_OSGeraTRP", A1758Contratante_OSGeraTRP);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERATRP", GetSecureSignedToken( sPrefix, A1758Contratante_OSGeraTRP));
            cmbContratante_OSGeraTRD.CurrentValue = cgiGet( cmbContratante_OSGeraTRD_Internalname);
            A1760Contratante_OSGeraTRD = StringUtil.StrToBool( cgiGet( cmbContratante_OSGeraTRD_Internalname));
            n1760Contratante_OSGeraTRD = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1760Contratante_OSGeraTRD", A1760Contratante_OSGeraTRD);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERATRD", GetSecureSignedToken( sPrefix, A1760Contratante_OSGeraTRD));
            cmbContratante_OSGeraTH.CurrentValue = cgiGet( cmbContratante_OSGeraTH_Internalname);
            A1759Contratante_OSGeraTH = StringUtil.StrToBool( cgiGet( cmbContratante_OSGeraTH_Internalname));
            n1759Contratante_OSGeraTH = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1759Contratante_OSGeraTH", A1759Contratante_OSGeraTH);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERATH", GetSecureSignedToken( sPrefix, A1759Contratante_OSGeraTH));
            cmbContratante_OSGeraTR.CurrentValue = cgiGet( cmbContratante_OSGeraTR_Internalname);
            A1761Contratante_OSGeraTR = StringUtil.StrToBool( cgiGet( cmbContratante_OSGeraTR_Internalname));
            n1761Contratante_OSGeraTR = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1761Contratante_OSGeraTR", A1761Contratante_OSGeraTR);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSGERATR", GetSecureSignedToken( sPrefix, A1761Contratante_OSGeraTR));
            cmbContratante_OSHmlgComPnd.CurrentValue = cgiGet( cmbContratante_OSHmlgComPnd_Internalname);
            A1803Contratante_OSHmlgComPnd = StringUtil.StrToBool( cgiGet( cmbContratante_OSHmlgComPnd_Internalname));
            n1803Contratante_OSHmlgComPnd = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1803Contratante_OSHmlgComPnd", A1803Contratante_OSHmlgComPnd);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_OSHMLGCOMPND", GetSecureSignedToken( sPrefix, A1803Contratante_OSHmlgComPnd));
            cmbContratante_SelecionaResponsavelOS.CurrentValue = cgiGet( cmbContratante_SelecionaResponsavelOS_Internalname);
            A2089Contratante_SelecionaResponsavelOS = StringUtil.StrToBool( cgiGet( cmbContratante_SelecionaResponsavelOS_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2089Contratante_SelecionaResponsavelOS", A2089Contratante_SelecionaResponsavelOS);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_SELECIONARESPONSAVELOS", GetSecureSignedToken( sPrefix, A2089Contratante_SelecionaResponsavelOS));
            cmbContratante_ExibePF.CurrentValue = cgiGet( cmbContratante_ExibePF_Internalname);
            A2090Contratante_ExibePF = StringUtil.StrToBool( cgiGet( cmbContratante_ExibePF_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2090Contratante_ExibePF", A2090Contratante_ExibePF);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_EXIBEPF", GetSecureSignedToken( sPrefix, A2090Contratante_ExibePF));
            A1805Contratante_AtivoCirculante = context.localUtil.CToN( cgiGet( edtContratante_AtivoCirculante_Internalname), ",", ".");
            n1805Contratante_AtivoCirculante = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1805Contratante_AtivoCirculante", StringUtil.LTrim( StringUtil.Str( A1805Contratante_AtivoCirculante, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_ATIVOCIRCULANTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1805Contratante_AtivoCirculante, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1806Contratante_PassivoCirculante = context.localUtil.CToN( cgiGet( edtContratante_PassivoCirculante_Internalname), ",", ".");
            n1806Contratante_PassivoCirculante = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1806Contratante_PassivoCirculante", StringUtil.LTrim( StringUtil.Str( A1806Contratante_PassivoCirculante, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PASSIVOCIRCULANTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1806Contratante_PassivoCirculante, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1807Contratante_PatrimonioLiquido = context.localUtil.CToN( cgiGet( edtContratante_PatrimonioLiquido_Internalname), ",", ".");
            n1807Contratante_PatrimonioLiquido = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1807Contratante_PatrimonioLiquido", StringUtil.LTrim( StringUtil.Str( A1807Contratante_PatrimonioLiquido, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_PATRIMONIOLIQUIDO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1807Contratante_PatrimonioLiquido, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1808Contratante_ReceitaBruta = context.localUtil.CToN( cgiGet( edtContratante_ReceitaBruta_Internalname), ",", ".");
            n1808Contratante_ReceitaBruta = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1808Contratante_ReceitaBruta", StringUtil.LTrim( StringUtil.Str( A1808Contratante_ReceitaBruta, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_RECEITABRUTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1808Contratante_ReceitaBruta, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A2034Contratante_TtlRltGerencial = cgiGet( edtContratante_TtlRltGerencial_Internalname);
            n2034Contratante_TtlRltGerencial = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2034Contratante_TtlRltGerencial", A2034Contratante_TtlRltGerencial);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATANTE_TTLRLTGERENCIAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2034Contratante_TtlRltGerencial, ""))));
            A25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMunicipio_Codigo_Internalname), ",", "."));
            n25Municipio_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MUNICIPIO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")));
            /* Read saved values. */
            wcpOA29Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA29Contratante_Codigo"), ",", "."));
            Gxuitabspanel_tabs_Width = cgiGet( sPrefix+"GXUITABSPANEL_TABS_Width");
            Gxuitabspanel_tabs_Cls = cgiGet( sPrefix+"GXUITABSPANEL_TABS_Cls");
            Gxuitabspanel_tabs_Autowidth = StringUtil.StrToBool( cgiGet( sPrefix+"GXUITABSPANEL_TABS_Autowidth"));
            Gxuitabspanel_tabs_Autoheight = StringUtil.StrToBool( cgiGet( sPrefix+"GXUITABSPANEL_TABS_Autoheight"));
            Gxuitabspanel_tabs_Autoscroll = StringUtil.StrToBool( cgiGet( sPrefix+"GXUITABSPANEL_TABS_Autoscroll"));
            Gxuitabspanel_tabs_Designtimetabs = cgiGet( sPrefix+"GXUITABSPANEL_TABS_Designtimetabs");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContratanteGeneral";
            A25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMunicipio_Codigo_Internalname), ",", "."));
            n25Municipio_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MUNICIPIO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contratantegeneral:[SecurityCheckFailed value for]"+"Municipio_Codigo:"+context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            GXACONTRATANTE_SERVICOSSPADRAO_html102( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11102 */
         E11102 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11102( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.0 - Data: 09/04/2020 23:40", 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12102( )
      {
         /* Load Routine */
         edtContratante_RazaoSocial_Link = formatLink("viewpessoa.aspx") + "?" + UrlEncode("" +A335Contratante_PessoaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_RazaoSocial_Internalname, "Link", edtContratante_RazaoSocial_Link);
         edtMunicipio_Nome_Link = formatLink("viewmunicipio.aspx") + "?" + UrlEncode("" +A25Municipio_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtMunicipio_Nome_Internalname, "Link", edtMunicipio_Nome_Link);
         edtContratante_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_Codigo_Visible), 5, 0)));
         edtMunicipio_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtMunicipio_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMunicipio_Codigo_Visible), 5, 0)));
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
      }

      protected void E13102( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contratante.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A29Contratante_Codigo);
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("contratante.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A29Contratante_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14102( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contratante.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A29Contratante_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E15102( )
      {
         /* 'DoFechar' Routine */
         AV13Websession.Remove("Entidade");
         context.wjLoc = formatLink("wwcontratante.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV16Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Contratante";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Contratante_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Contratante_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_102( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GXUITABSPANEL_TABSContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"GXUITABSPANEL_TABSContainer"+"TitleDados"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Dados") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"GXUITABSPANEL_TABSContainer"+"Dados"+"\" style=\"display:none;\">") ;
            wb_table2_11_102( true) ;
         }
         else
         {
            wb_table2_11_102( false) ;
         }
         return  ;
      }

      protected void wb_table2_11_102e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"GXUITABSPANEL_TABSContainer"+"TitleSSOS"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "SS / OS") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"GXUITABSPANEL_TABSContainer"+"SSOS"+"\" style=\"display:none;\">") ;
            wb_table3_128_102( true) ;
         }
         else
         {
            wb_table3_128_102( false) ;
         }
         return  ;
      }

      protected void wb_table3_128_102e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"GXUITABSPANEL_TABSContainer"+"TitleFinanceiro"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Financeiro") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"GXUITABSPANEL_TABSContainer"+"Financeiro"+"\" style=\"display:none;\">") ;
            wb_table4_243_102( true) ;
         }
         else
         {
            wb_table4_243_102( false) ;
         }
         return  ;
      }

      protected void wb_table4_243_102e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_271_102( true) ;
         }
         else
         {
            wb_table5_271_102( false) ;
         }
         return  ;
      }

      protected void wb_table5_271_102e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_102e( true) ;
         }
         else
         {
            wb_table1_2_102e( false) ;
         }
      }

      protected void wb_table5_271_102( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 274,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 276,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 278,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_271_102e( true) ;
         }
         else
         {
            wb_table5_271_102e( false) ;
         }
      }

      protected void wb_table4_243_102( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ativocirculante_Internalname, "Ativo Circulante", "", "", lblTextblockcontratante_ativocirculante_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_AtivoCirculante_Internalname, StringUtil.LTrim( StringUtil.NToC( A1805Contratante_AtivoCirculante, 18, 5, ",", "")), context.localUtil.Format( A1805Contratante_AtivoCirculante, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_AtivoCirculante_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 100, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_passivocirculante_Internalname, "Passivo Circulante", "", "", lblTextblockcontratante_passivocirculante_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_PassivoCirculante_Internalname, StringUtil.LTrim( StringUtil.NToC( A1806Contratante_PassivoCirculante, 18, 5, ",", "")), context.localUtil.Format( A1806Contratante_PassivoCirculante, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_PassivoCirculante_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 100, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_patrimonioliquido_Internalname, "Patrimonio Liquido", "", "", lblTextblockcontratante_patrimonioliquido_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_PatrimonioLiquido_Internalname, StringUtil.LTrim( StringUtil.NToC( A1807Contratante_PatrimonioLiquido, 18, 5, ",", "")), context.localUtil.Format( A1807Contratante_PatrimonioLiquido, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_PatrimonioLiquido_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 100, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_receitabruta_Internalname, "Receita Bruta", "", "", lblTextblockcontratante_receitabruta_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_ReceitaBruta_Internalname, StringUtil.LTrim( StringUtil.NToC( A1808Contratante_ReceitaBruta, 18, 5, ",", "")), context.localUtil.Format( A1808Contratante_ReceitaBruta, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_ReceitaBruta_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 100, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ttlrltgerencial_Internalname, "Titulo relat�rio gerencial", "", "", lblTextblockcontratante_ttlrltgerencial_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_TtlRltGerencial_Internalname, A2034Contratante_TtlRltGerencial, StringUtil.RTrim( context.localUtil.Format( A2034Contratante_TtlRltGerencial, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_TtlRltGerencial_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_243_102e( true) ;
         }
         else
         {
            wb_table4_243_102e( false) ;
         }
      }

      protected void wb_table3_128_102( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup4_Internalname, "SS", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratanteGeneral.htm");
            wb_table6_132_102( true) ;
         }
         else
         {
            wb_table6_132_102( false) ;
         }
         return  ;
      }

      protected void wb_table6_132_102e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup6_Internalname, "Proposta", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratanteGeneral.htm");
            wb_table7_162_102( true) ;
         }
         else
         {
            wb_table7_162_102( false) ;
         }
         return  ;
      }

      protected void wb_table7_162_102e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup8_Internalname, "OS", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratanteGeneral.htm");
            wb_table8_192_102( true) ;
         }
         else
         {
            wb_table8_192_102( false) ;
         }
         return  ;
      }

      protected void wb_table8_192_102e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_128_102e( true) ;
         }
         else
         {
            wb_table3_128_102e( false) ;
         }
      }

      protected void wb_table8_192_102( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable7_Internalname, tblUnnamedtable7_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_osautomatica_Internalname, "Numera��o", "", "", lblTextblockcontratante_osautomatica_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_OSAutomatica, cmbContratante_OSAutomatica_Internalname, StringUtil.BoolToStr( A593Contratante_OSAutomatica), 1, cmbContratante_OSAutomatica_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_OSAutomatica.CurrentValue = StringUtil.BoolToStr( A593Contratante_OSAutomatica);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_OSAutomatica_Internalname, "Values", (String)(cmbContratante_OSAutomatica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_osgeraos_Internalname, "Gera OS?", "", "", lblTextblockcontratante_osgeraos_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_OSGeraOS, cmbContratante_OSGeraOS_Internalname, StringUtil.BoolToStr( A1757Contratante_OSGeraOS), 1, cmbContratante_OSGeraOS_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_OSGeraOS.CurrentValue = StringUtil.BoolToStr( A1757Contratante_OSGeraOS);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_OSGeraOS_Internalname, "Values", (String)(cmbContratante_OSGeraOS.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_osgeratrp_Internalname, "Termo de Recebimento Provis�rio?", "", "", lblTextblockcontratante_osgeratrp_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_OSGeraTRP, cmbContratante_OSGeraTRP_Internalname, StringUtil.BoolToStr( A1758Contratante_OSGeraTRP), 1, cmbContratante_OSGeraTRP_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_OSGeraTRP.CurrentValue = StringUtil.BoolToStr( A1758Contratante_OSGeraTRP);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_OSGeraTRP_Internalname, "Values", (String)(cmbContratante_OSGeraTRP.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_osgeratrd_Internalname, "Termo de Recebimento Definitivo?", "", "", lblTextblockcontratante_osgeratrd_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_OSGeraTRD, cmbContratante_OSGeraTRD_Internalname, StringUtil.BoolToStr( A1760Contratante_OSGeraTRD), 1, cmbContratante_OSGeraTRD_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_OSGeraTRD.CurrentValue = StringUtil.BoolToStr( A1760Contratante_OSGeraTRD);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_OSGeraTRD_Internalname, "Values", (String)(cmbContratante_OSGeraTRD.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_osgerath_Internalname, "Termo de Homologa��o?", "", "", lblTextblockcontratante_osgerath_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_OSGeraTH, cmbContratante_OSGeraTH_Internalname, StringUtil.BoolToStr( A1759Contratante_OSGeraTH), 1, cmbContratante_OSGeraTH_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_OSGeraTH.CurrentValue = StringUtil.BoolToStr( A1759Contratante_OSGeraTH);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_OSGeraTH_Internalname, "Values", (String)(cmbContratante_OSGeraTH.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_osgeratr_Internalname, "Termo de Recusa?", "", "", lblTextblockcontratante_osgeratr_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_OSGeraTR, cmbContratante_OSGeraTR_Internalname, StringUtil.BoolToStr( A1761Contratante_OSGeraTR), 1, cmbContratante_OSGeraTR_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_OSGeraTR.CurrentValue = StringUtil.BoolToStr( A1761Contratante_OSGeraTR);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_OSGeraTR_Internalname, "Values", (String)(cmbContratante_OSGeraTR.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_oshmlgcompnd_Internalname, "Homologa com pend�ncias", "", "", lblTextblockcontratante_oshmlgcompnd_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_OSHmlgComPnd, cmbContratante_OSHmlgComPnd_Internalname, StringUtil.BoolToStr( A1803Contratante_OSHmlgComPnd), 1, cmbContratante_OSHmlgComPnd_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_OSHmlgComPnd.CurrentValue = StringUtil.BoolToStr( A1803Contratante_OSHmlgComPnd);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_OSHmlgComPnd_Internalname, "Values", (String)(cmbContratante_OSHmlgComPnd.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_selecionaresponsavelos_Internalname, "Seleciona o Profissional respons�vel pela OS?", "", "", lblTextblockcontratante_selecionaresponsavelos_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_SelecionaResponsavelOS, cmbContratante_SelecionaResponsavelOS_Internalname, StringUtil.BoolToStr( A2089Contratante_SelecionaResponsavelOS), 1, cmbContratante_SelecionaResponsavelOS_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_SelecionaResponsavelOS.CurrentValue = StringUtil.BoolToStr( A2089Contratante_SelecionaResponsavelOS);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_SelecionaResponsavelOS_Internalname, "Values", (String)(cmbContratante_SelecionaResponsavelOS.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_exibepf_Internalname, "Exibir PF Bruto e L�quido?", "", "", lblTextblockcontratante_exibepf_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_ExibePF, cmbContratante_ExibePF_Internalname, StringUtil.BoolToStr( A2090Contratante_ExibePF), 1, cmbContratante_ExibePF_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_ExibePF.CurrentValue = StringUtil.BoolToStr( A2090Contratante_ExibePF);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_ExibePF_Internalname, "Values", (String)(cmbContratante_ExibePF.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_192_102e( true) ;
         }
         else
         {
            wb_table8_192_102e( false) ;
         }
      }

      protected void wb_table7_162_102( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable5_Internalname, tblUnnamedtable5_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_propostarqrsrv_Internalname, "Requer servi�o?", "", "", lblTextblockcontratante_propostarqrsrv_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_PropostaRqrSrv, cmbContratante_PropostaRqrSrv_Internalname, StringUtil.BoolToStr( A1738Contratante_PropostaRqrSrv), 1, cmbContratante_PropostaRqrSrv_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_PropostaRqrSrv.CurrentValue = StringUtil.BoolToStr( A1738Contratante_PropostaRqrSrv);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_PropostaRqrSrv_Internalname, "Values", (String)(cmbContratante_PropostaRqrSrv.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_propostarqrprz_Internalname, "Requer prazo?", "", "", lblTextblockcontratante_propostarqrprz_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_PropostaRqrPrz, cmbContratante_PropostaRqrPrz_Internalname, StringUtil.BoolToStr( A1739Contratante_PropostaRqrPrz), 1, cmbContratante_PropostaRqrPrz_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_PropostaRqrPrz.CurrentValue = StringUtil.BoolToStr( A1739Contratante_PropostaRqrPrz);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_PropostaRqrPrz_Internalname, "Values", (String)(cmbContratante_PropostaRqrPrz.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_propostarqresf_Internalname, "Requer esfor�o?", "", "", lblTextblockcontratante_propostarqresf_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_PropostaRqrEsf, cmbContratante_PropostaRqrEsf_Internalname, StringUtil.BoolToStr( A1740Contratante_PropostaRqrEsf), 1, cmbContratante_PropostaRqrEsf_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_PropostaRqrEsf.CurrentValue = StringUtil.BoolToStr( A1740Contratante_PropostaRqrEsf);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_PropostaRqrEsf_Internalname, "Values", (String)(cmbContratante_PropostaRqrEsf.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_propostarqrcnt_Internalname, "Requer valores?", "", "", lblTextblockcontratante_propostarqrcnt_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_PropostaRqrCnt, cmbContratante_PropostaRqrCnt_Internalname, StringUtil.BoolToStr( A1741Contratante_PropostaRqrCnt), 1, cmbContratante_PropostaRqrCnt_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_PropostaRqrCnt.CurrentValue = StringUtil.BoolToStr( A1741Contratante_PropostaRqrCnt);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_PropostaRqrCnt_Internalname, "Values", (String)(cmbContratante_PropostaRqrCnt.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_propostanvlcnt_Internalname, "Nivel dos valores?", "", "", lblTextblockcontratante_propostanvlcnt_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_PropostaNvlCnt, cmbContratante_PropostaNvlCnt_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0)), 1, cmbContratante_PropostaNvlCnt_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_PropostaNvlCnt.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_PropostaNvlCnt_Internalname, "Values", (String)(cmbContratante_PropostaNvlCnt.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_162_102e( true) ;
         }
         else
         {
            wb_table7_162_102e( false) ;
         }
      }

      protected void wb_table6_132_102( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ssautomatica_Internalname, "Numera��o", "", "", lblTextblockcontratante_ssautomatica_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_SSAutomatica, cmbContratante_SSAutomatica_Internalname, StringUtil.BoolToStr( A1652Contratante_SSAutomatica), 1, cmbContratante_SSAutomatica_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_SSAutomatica.CurrentValue = StringUtil.BoolToStr( A1652Contratante_SSAutomatica);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_SSAutomatica_Internalname, "Values", (String)(cmbContratante_SSAutomatica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ssprestadoraunica_Internalname, "Prestadora �nica?", "", "", lblTextblockcontratante_ssprestadoraunica_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_SSPrestadoraUnica, cmbContratante_SSPrestadoraUnica_Internalname, StringUtil.BoolToStr( A1733Contratante_SSPrestadoraUnica), 1, cmbContratante_SSPrestadoraUnica_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_SSPrestadoraUnica.CurrentValue = StringUtil.BoolToStr( A1733Contratante_SSPrestadoraUnica);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_SSPrestadoraUnica_Internalname, "Values", (String)(cmbContratante_SSPrestadoraUnica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ssstatusplanejamento_Internalname, "Status em planejamento", "", "", lblTextblockcontratante_ssstatusplanejamento_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_SSStatusPlanejamento, cmbContratante_SSStatusPlanejamento_Internalname, StringUtil.RTrim( A1732Contratante_SSStatusPlanejamento), 1, cmbContratante_SSStatusPlanejamento_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_SSStatusPlanejamento.CurrentValue = StringUtil.RTrim( A1732Contratante_SSStatusPlanejamento);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_SSStatusPlanejamento_Internalname, "Values", (String)(cmbContratante_SSStatusPlanejamento.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_servicosspadrao_Internalname, "Servico SS padr�o", "", "", lblTextblockcontratante_servicosspadrao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContratante_ServicoSSPadrao, dynContratante_ServicoSSPadrao_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0)), 1, dynContratante_ServicoSSPadrao_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            dynContratante_ServicoSSPadrao.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynContratante_ServicoSSPadrao_Internalname, "Values", (String)(dynContratante_ServicoSSPadrao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_retornass_Internalname, "Retorna SS?", "", "", lblTextblockcontratante_retornass_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_RetornaSS, cmbContratante_RetornaSS_Internalname, StringUtil.BoolToStr( A1729Contratante_RetornaSS), 1, cmbContratante_RetornaSS_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_RetornaSS.CurrentValue = StringUtil.BoolToStr( A1729Contratante_RetornaSS);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_RetornaSS_Internalname, "Values", (String)(cmbContratante_RetornaSS.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_132_102e( true) ;
         }
         else
         {
            wb_table6_132_102e( false) ;
         }
      }

      protected void wb_table2_11_102( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable9_Internalname, tblUnnamedtable9_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_razaosocial_Internalname, "Raz�o Social", "", "", lblTextblockcontratante_razaosocial_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_RazaoSocial_Internalname, StringUtil.RTrim( A9Contratante_RazaoSocial), StringUtil.RTrim( context.localUtil.Format( A9Contratante_RazaoSocial, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContratante_RazaoSocial_Link, "", "", "", edtContratante_RazaoSocial_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_sigla_Internalname, "Sigla", "", "", lblTextblockcontratante_sigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_Sigla_Internalname, StringUtil.RTrim( A2208Contratante_Sigla), StringUtil.RTrim( context.localUtil.Format( A2208Contratante_Sigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_Sigla_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_cnpj_Internalname, "CNPJ", "", "", lblTextblockcontratante_cnpj_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_CNPJ_Internalname, A12Contratante_CNPJ, StringUtil.RTrim( context.localUtil.Format( A12Contratante_CNPJ, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_CNPJ_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ie_Internalname, "Insc. Estadual", "", "", lblTextblockcontratante_ie_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_IE_Internalname, StringUtil.RTrim( A11Contratante_IE), StringUtil.RTrim( context.localUtil.Format( A11Contratante_IE, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_IE_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "IE", "left", true, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_telefone_Internalname, "Telefone", "", "", lblTextblockcontratante_telefone_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            if ( context.isSmartDevice( ) )
            {
               gxphoneLink = "tel:" + StringUtil.RTrim( A31Contratante_Telefone);
            }
            GxWebStd.gx_single_line_edit( context, edtContratante_Telefone_Internalname, StringUtil.RTrim( A31Contratante_Telefone), StringUtil.RTrim( context.localUtil.Format( A31Contratante_Telefone, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", gxphoneLink, "", "", "", edtContratante_Telefone_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "tel", "", 100, "px", 1, "row", 20, 0, 0, 0, 1, -1, 0, true, "Phone", "left", true, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ramal_Internalname, "Ramal", "", "", lblTextblockcontratante_ramal_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_Ramal_Internalname, StringUtil.RTrim( A32Contratante_Ramal), StringUtil.RTrim( context.localUtil.Format( A32Contratante_Ramal, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_Ramal_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 100, "px", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "Ramal", "left", true, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_fax_Internalname, "Fax", "", "", lblTextblockcontratante_fax_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            if ( context.isSmartDevice( ) )
            {
               gxphoneLink = "tel:" + StringUtil.RTrim( A33Contratante_Fax);
            }
            GxWebStd.gx_single_line_edit( context, edtContratante_Fax_Internalname, StringUtil.RTrim( A33Contratante_Fax), StringUtil.RTrim( context.localUtil.Format( A33Contratante_Fax, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", gxphoneLink, "", "", "", edtContratante_Fax_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "tel", "", 100, "px", 1, "row", 20, 0, 0, 0, 1, -1, 0, true, "Phone", "left", true, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_website_Internalname, "Site", "", "", lblTextblockcontratante_website_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_WebSite_Internalname, A13Contratante_WebSite, StringUtil.RTrim( context.localUtil.Format( A13Contratante_WebSite, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_WebSite_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_banconome_Internalname, "Banco", "", "", lblTextblockcontratante_banconome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_BancoNome_Internalname, StringUtil.RTrim( A338Contratante_BancoNome), StringUtil.RTrim( context.localUtil.Format( A338Contratante_BancoNome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_BancoNome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_banconro_Internalname, "N�mero", "", "", lblTextblockcontratante_banconro_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_BancoNro_Internalname, StringUtil.RTrim( A339Contratante_BancoNro), StringUtil.RTrim( context.localUtil.Format( A339Contratante_BancoNro, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_BancoNro_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 6, 0, 0, 0, 1, -1, -1, true, "NumeroBanco", "left", true, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_agencianome_Internalname, "Ag�ncia", "", "", lblTextblockcontratante_agencianome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_AgenciaNome_Internalname, StringUtil.RTrim( A336Contratante_AgenciaNome), StringUtil.RTrim( context.localUtil.Format( A336Contratante_AgenciaNome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_AgenciaNome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_agencianro_Internalname, "N�mero", "", "", lblTextblockcontratante_agencianro_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_AgenciaNro_Internalname, StringUtil.RTrim( A337Contratante_AgenciaNro), StringUtil.RTrim( context.localUtil.Format( A337Contratante_AgenciaNro, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_AgenciaNro_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "Agencia", "left", true, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_contacorrente_Internalname, "Conta", "", "", lblTextblockcontratante_contacorrente_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_ContaCorrente_Internalname, StringUtil.RTrim( A16Contratante_ContaCorrente), StringUtil.RTrim( context.localUtil.Format( A16Contratante_ContaCorrente, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_ContaCorrente_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "ContaCorrente", "left", true, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmunicipio_nome_Internalname, "Munic�pio", "", "", lblTextblockmunicipio_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMunicipio_Nome_Internalname, StringUtil.RTrim( A26Municipio_Nome), StringUtil.RTrim( context.localUtil.Format( A26Municipio_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtMunicipio_Nome_Link, "", "", "", edtMunicipio_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockestado_uf_Internalname, "UF", "", "", lblTextblockestado_uf_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtEstado_UF_Internalname, StringUtil.RTrim( A23Estado_UF), StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEstado_UF_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "UF", "left", true, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_iniciodoexpediente_Internalname, "Expediente:", "", "", lblTextblockcontratante_iniciodoexpediente_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table9_92_102( true) ;
         }
         else
         {
            wb_table9_92_102( false) ;
         }
         return  ;
      }

      protected void wb_table9_92_102e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table10_99_102( true) ;
         }
         else
         {
            wb_table10_99_102( false) ;
         }
         return  ;
      }

      protected void wb_table10_99_102e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_usaosistema_Internalname, "Usa o Sistema", "", "", lblTextblockcontratante_usaosistema_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_UsaOSistema, cmbContratante_UsaOSistema_Internalname, StringUtil.BoolToStr( A1822Contratante_UsaOSistema), 1, cmbContratante_UsaOSistema_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteGeneral.htm");
            cmbContratante_UsaOSistema.CurrentValue = StringUtil.BoolToStr( A1822Contratante_UsaOSistema);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_UsaOSistema_Internalname, "Values", (String)(cmbContratante_UsaOSistema.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ativo_Internalname, "Ativo?", "", "", lblTextblockcontratante_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratante_Ativo_Internalname, StringUtil.BoolToStr( A30Contratante_Ativo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_11_102e( true) ;
         }
         else
         {
            wb_table2_11_102e( false) ;
         }
      }

      protected void wb_table10_99_102( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratante_fimdoexpediente_Internalname, tblTablemergedcontratante_fimdoexpediente_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContratante_FimDoExpediente_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratante_FimDoExpediente_Internalname, context.localUtil.TToC( A1192Contratante_FimDoExpediente, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1192Contratante_FimDoExpediente, "99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_FimDoExpediente_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "Time", "right", false, "HLP_ContratanteGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContratante_FimDoExpediente_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratante_fimdoexpediente_righttext_Internalname, "��hs.", "", "", lblContratante_fimdoexpediente_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_99_102e( true) ;
         }
         else
         {
            wb_table10_99_102e( false) ;
         }
      }

      protected void wb_table9_92_102( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratante_iniciodoexpediente_Internalname, tblTablemergedcontratante_iniciodoexpediente_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContratante_InicioDoExpediente_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratante_InicioDoExpediente_Internalname, context.localUtil.TToC( A1448Contratante_InicioDoExpediente, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1448Contratante_InicioDoExpediente, "99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_InicioDoExpediente_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "Time", "right", false, "HLP_ContratanteGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContratante_InicioDoExpediente_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratante_iniciodoexpediente_righttext_Internalname, "��s", "", "", lblContratante_iniciodoexpediente_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_92_102e( true) ;
         }
         else
         {
            wb_table9_92_102e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A29Contratante_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA102( ) ;
         WS102( ) ;
         WE102( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA29Contratante_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA102( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratantegeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA102( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A29Contratante_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
         }
         wcpOA29Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA29Contratante_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A29Contratante_Codigo != wcpOA29Contratante_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA29Contratante_Codigo = A29Contratante_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA29Contratante_Codigo = cgiGet( sPrefix+"A29Contratante_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA29Contratante_Codigo) > 0 )
         {
            A29Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA29Contratante_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
         }
         else
         {
            A29Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A29Contratante_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA102( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS102( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS102( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A29Contratante_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA29Contratante_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A29Contratante_Codigo_CTRL", StringUtil.RTrim( sCtrlA29Contratante_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE102( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205302129771");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratantegeneral.js", "?20205302129771");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratante_razaosocial_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_RAZAOSOCIAL";
         edtContratante_RazaoSocial_Internalname = sPrefix+"CONTRATANTE_RAZAOSOCIAL";
         lblTextblockcontratante_sigla_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_SIGLA";
         edtContratante_Sigla_Internalname = sPrefix+"CONTRATANTE_SIGLA";
         lblTextblockcontratante_cnpj_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_CNPJ";
         edtContratante_CNPJ_Internalname = sPrefix+"CONTRATANTE_CNPJ";
         lblTextblockcontratante_ie_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_IE";
         edtContratante_IE_Internalname = sPrefix+"CONTRATANTE_IE";
         lblTextblockcontratante_telefone_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_TELEFONE";
         edtContratante_Telefone_Internalname = sPrefix+"CONTRATANTE_TELEFONE";
         lblTextblockcontratante_ramal_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_RAMAL";
         edtContratante_Ramal_Internalname = sPrefix+"CONTRATANTE_RAMAL";
         lblTextblockcontratante_fax_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_FAX";
         edtContratante_Fax_Internalname = sPrefix+"CONTRATANTE_FAX";
         lblTextblockcontratante_website_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_WEBSITE";
         edtContratante_WebSite_Internalname = sPrefix+"CONTRATANTE_WEBSITE";
         lblTextblockcontratante_banconome_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_BANCONOME";
         edtContratante_BancoNome_Internalname = sPrefix+"CONTRATANTE_BANCONOME";
         lblTextblockcontratante_banconro_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_BANCONRO";
         edtContratante_BancoNro_Internalname = sPrefix+"CONTRATANTE_BANCONRO";
         lblTextblockcontratante_agencianome_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_AGENCIANOME";
         edtContratante_AgenciaNome_Internalname = sPrefix+"CONTRATANTE_AGENCIANOME";
         lblTextblockcontratante_agencianro_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_AGENCIANRO";
         edtContratante_AgenciaNro_Internalname = sPrefix+"CONTRATANTE_AGENCIANRO";
         lblTextblockcontratante_contacorrente_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_CONTACORRENTE";
         edtContratante_ContaCorrente_Internalname = sPrefix+"CONTRATANTE_CONTACORRENTE";
         lblTextblockmunicipio_nome_Internalname = sPrefix+"TEXTBLOCKMUNICIPIO_NOME";
         edtMunicipio_Nome_Internalname = sPrefix+"MUNICIPIO_NOME";
         lblTextblockestado_uf_Internalname = sPrefix+"TEXTBLOCKESTADO_UF";
         edtEstado_UF_Internalname = sPrefix+"ESTADO_UF";
         lblTextblockcontratante_iniciodoexpediente_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_INICIODOEXPEDIENTE";
         edtContratante_InicioDoExpediente_Internalname = sPrefix+"CONTRATANTE_INICIODOEXPEDIENTE";
         lblContratante_iniciodoexpediente_righttext_Internalname = sPrefix+"CONTRATANTE_INICIODOEXPEDIENTE_RIGHTTEXT";
         tblTablemergedcontratante_iniciodoexpediente_Internalname = sPrefix+"TABLEMERGEDCONTRATANTE_INICIODOEXPEDIENTE";
         edtContratante_FimDoExpediente_Internalname = sPrefix+"CONTRATANTE_FIMDOEXPEDIENTE";
         lblContratante_fimdoexpediente_righttext_Internalname = sPrefix+"CONTRATANTE_FIMDOEXPEDIENTE_RIGHTTEXT";
         tblTablemergedcontratante_fimdoexpediente_Internalname = sPrefix+"TABLEMERGEDCONTRATANTE_FIMDOEXPEDIENTE";
         lblTextblockcontratante_usaosistema_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_USAOSISTEMA";
         cmbContratante_UsaOSistema_Internalname = sPrefix+"CONTRATANTE_USAOSISTEMA";
         lblTextblockcontratante_ativo_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_ATIVO";
         chkContratante_Ativo_Internalname = sPrefix+"CONTRATANTE_ATIVO";
         tblUnnamedtable9_Internalname = sPrefix+"UNNAMEDTABLE9";
         lblTextblockcontratante_ssautomatica_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_SSAUTOMATICA";
         cmbContratante_SSAutomatica_Internalname = sPrefix+"CONTRATANTE_SSAUTOMATICA";
         lblTextblockcontratante_ssprestadoraunica_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_SSPRESTADORAUNICA";
         cmbContratante_SSPrestadoraUnica_Internalname = sPrefix+"CONTRATANTE_SSPRESTADORAUNICA";
         lblTextblockcontratante_ssstatusplanejamento_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_SSSTATUSPLANEJAMENTO";
         cmbContratante_SSStatusPlanejamento_Internalname = sPrefix+"CONTRATANTE_SSSTATUSPLANEJAMENTO";
         lblTextblockcontratante_servicosspadrao_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_SERVICOSSPADRAO";
         dynContratante_ServicoSSPadrao_Internalname = sPrefix+"CONTRATANTE_SERVICOSSPADRAO";
         lblTextblockcontratante_retornass_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_RETORNASS";
         cmbContratante_RetornaSS_Internalname = sPrefix+"CONTRATANTE_RETORNASS";
         tblUnnamedtable3_Internalname = sPrefix+"UNNAMEDTABLE3";
         grpUnnamedgroup4_Internalname = sPrefix+"UNNAMEDGROUP4";
         lblTextblockcontratante_propostarqrsrv_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_PROPOSTARQRSRV";
         cmbContratante_PropostaRqrSrv_Internalname = sPrefix+"CONTRATANTE_PROPOSTARQRSRV";
         lblTextblockcontratante_propostarqrprz_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_PROPOSTARQRPRZ";
         cmbContratante_PropostaRqrPrz_Internalname = sPrefix+"CONTRATANTE_PROPOSTARQRPRZ";
         lblTextblockcontratante_propostarqresf_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_PROPOSTARQRESF";
         cmbContratante_PropostaRqrEsf_Internalname = sPrefix+"CONTRATANTE_PROPOSTARQRESF";
         lblTextblockcontratante_propostarqrcnt_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_PROPOSTARQRCNT";
         cmbContratante_PropostaRqrCnt_Internalname = sPrefix+"CONTRATANTE_PROPOSTARQRCNT";
         lblTextblockcontratante_propostanvlcnt_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_PROPOSTANVLCNT";
         cmbContratante_PropostaNvlCnt_Internalname = sPrefix+"CONTRATANTE_PROPOSTANVLCNT";
         tblUnnamedtable5_Internalname = sPrefix+"UNNAMEDTABLE5";
         grpUnnamedgroup6_Internalname = sPrefix+"UNNAMEDGROUP6";
         lblTextblockcontratante_osautomatica_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_OSAUTOMATICA";
         cmbContratante_OSAutomatica_Internalname = sPrefix+"CONTRATANTE_OSAUTOMATICA";
         lblTextblockcontratante_osgeraos_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_OSGERAOS";
         cmbContratante_OSGeraOS_Internalname = sPrefix+"CONTRATANTE_OSGERAOS";
         lblTextblockcontratante_osgeratrp_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_OSGERATRP";
         cmbContratante_OSGeraTRP_Internalname = sPrefix+"CONTRATANTE_OSGERATRP";
         lblTextblockcontratante_osgeratrd_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_OSGERATRD";
         cmbContratante_OSGeraTRD_Internalname = sPrefix+"CONTRATANTE_OSGERATRD";
         lblTextblockcontratante_osgerath_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_OSGERATH";
         cmbContratante_OSGeraTH_Internalname = sPrefix+"CONTRATANTE_OSGERATH";
         lblTextblockcontratante_osgeratr_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_OSGERATR";
         cmbContratante_OSGeraTR_Internalname = sPrefix+"CONTRATANTE_OSGERATR";
         lblTextblockcontratante_oshmlgcompnd_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_OSHMLGCOMPND";
         cmbContratante_OSHmlgComPnd_Internalname = sPrefix+"CONTRATANTE_OSHMLGCOMPND";
         lblTextblockcontratante_selecionaresponsavelos_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_SELECIONARESPONSAVELOS";
         cmbContratante_SelecionaResponsavelOS_Internalname = sPrefix+"CONTRATANTE_SELECIONARESPONSAVELOS";
         lblTextblockcontratante_exibepf_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_EXIBEPF";
         cmbContratante_ExibePF_Internalname = sPrefix+"CONTRATANTE_EXIBEPF";
         tblUnnamedtable7_Internalname = sPrefix+"UNNAMEDTABLE7";
         grpUnnamedgroup8_Internalname = sPrefix+"UNNAMEDGROUP8";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         lblTextblockcontratante_ativocirculante_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_ATIVOCIRCULANTE";
         edtContratante_AtivoCirculante_Internalname = sPrefix+"CONTRATANTE_ATIVOCIRCULANTE";
         lblTextblockcontratante_passivocirculante_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_PASSIVOCIRCULANTE";
         edtContratante_PassivoCirculante_Internalname = sPrefix+"CONTRATANTE_PASSIVOCIRCULANTE";
         lblTextblockcontratante_patrimonioliquido_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_PATRIMONIOLIQUIDO";
         edtContratante_PatrimonioLiquido_Internalname = sPrefix+"CONTRATANTE_PATRIMONIOLIQUIDO";
         lblTextblockcontratante_receitabruta_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_RECEITABRUTA";
         edtContratante_ReceitaBruta_Internalname = sPrefix+"CONTRATANTE_RECEITABRUTA";
         lblTextblockcontratante_ttlrltgerencial_Internalname = sPrefix+"TEXTBLOCKCONTRATANTE_TTLRLTGERENCIAL";
         edtContratante_TtlRltGerencial_Internalname = sPrefix+"CONTRATANTE_TTLRLTGERENCIAL";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Gxuitabspanel_tabs_Internalname = sPrefix+"GXUITABSPANEL_TABS";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContratante_Codigo_Internalname = sPrefix+"CONTRATANTE_CODIGO";
         edtMunicipio_Codigo_Internalname = sPrefix+"MUNICIPIO_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratante_InicioDoExpediente_Jsonclick = "";
         edtContratante_FimDoExpediente_Jsonclick = "";
         cmbContratante_UsaOSistema_Jsonclick = "";
         edtEstado_UF_Jsonclick = "";
         edtMunicipio_Nome_Jsonclick = "";
         edtContratante_ContaCorrente_Jsonclick = "";
         edtContratante_AgenciaNro_Jsonclick = "";
         edtContratante_AgenciaNome_Jsonclick = "";
         edtContratante_BancoNro_Jsonclick = "";
         edtContratante_BancoNome_Jsonclick = "";
         edtContratante_WebSite_Jsonclick = "";
         edtContratante_Fax_Jsonclick = "";
         edtContratante_Ramal_Jsonclick = "";
         edtContratante_Telefone_Jsonclick = "";
         edtContratante_IE_Jsonclick = "";
         edtContratante_CNPJ_Jsonclick = "";
         edtContratante_Sigla_Jsonclick = "";
         edtContratante_RazaoSocial_Jsonclick = "";
         cmbContratante_RetornaSS_Jsonclick = "";
         dynContratante_ServicoSSPadrao_Jsonclick = "";
         cmbContratante_SSStatusPlanejamento_Jsonclick = "";
         cmbContratante_SSPrestadoraUnica_Jsonclick = "";
         cmbContratante_SSAutomatica_Jsonclick = "";
         cmbContratante_PropostaNvlCnt_Jsonclick = "";
         cmbContratante_PropostaRqrCnt_Jsonclick = "";
         cmbContratante_PropostaRqrEsf_Jsonclick = "";
         cmbContratante_PropostaRqrPrz_Jsonclick = "";
         cmbContratante_PropostaRqrSrv_Jsonclick = "";
         cmbContratante_ExibePF_Jsonclick = "";
         cmbContratante_SelecionaResponsavelOS_Jsonclick = "";
         cmbContratante_OSHmlgComPnd_Jsonclick = "";
         cmbContratante_OSGeraTR_Jsonclick = "";
         cmbContratante_OSGeraTH_Jsonclick = "";
         cmbContratante_OSGeraTRD_Jsonclick = "";
         cmbContratante_OSGeraTRP_Jsonclick = "";
         cmbContratante_OSGeraOS_Jsonclick = "";
         cmbContratante_OSAutomatica_Jsonclick = "";
         edtContratante_TtlRltGerencial_Jsonclick = "";
         edtContratante_ReceitaBruta_Jsonclick = "";
         edtContratante_PatrimonioLiquido_Jsonclick = "";
         edtContratante_PassivoCirculante_Jsonclick = "";
         edtContratante_AtivoCirculante_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         edtMunicipio_Nome_Link = "";
         edtContratante_RazaoSocial_Link = "";
         chkContratante_Ativo.Caption = "";
         edtMunicipio_Codigo_Jsonclick = "";
         edtMunicipio_Codigo_Visible = 1;
         edtContratante_Codigo_Jsonclick = "";
         edtContratante_Codigo_Visible = 1;
         Gxuitabspanel_tabs_Designtimetabs = "[{\"id\":\"Dados\"},{\"id\":\"SSOS\"},{\"id\":\"Financeiro\"}]";
         Gxuitabspanel_tabs_Autoscroll = Convert.ToBoolean( -1);
         Gxuitabspanel_tabs_Autoheight = Convert.ToBoolean( -1);
         Gxuitabspanel_tabs_Autowidth = Convert.ToBoolean( 0);
         Gxuitabspanel_tabs_Cls = "GXUI-DVelop-Tabs";
         Gxuitabspanel_tabs_Width = "100%";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13102',iparms:[{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14102',iparms:[{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15102',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A2208Contratante_Sigla = "";
         A11Contratante_IE = "";
         A31Contratante_Telefone = "";
         A32Contratante_Ramal = "";
         A33Contratante_Fax = "";
         A13Contratante_WebSite = "";
         A338Contratante_BancoNome = "";
         A339Contratante_BancoNro = "";
         A336Contratante_AgenciaNome = "";
         A337Contratante_AgenciaNro = "";
         A16Contratante_ContaCorrente = "";
         A1448Contratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
         A1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         A1732Contratante_SSStatusPlanejamento = "";
         A2034Contratante_TtlRltGerencial = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00102_A155Servico_Codigo = new int[1] ;
         H00102_A605Servico_Sigla = new String[] {""} ;
         H00102_A1635Servico_IsPublico = new bool[] {false} ;
         H00103_A29Contratante_Codigo = new int[1] ;
         H00103_A335Contratante_PessoaCod = new int[1] ;
         H00103_A25Municipio_Codigo = new int[1] ;
         H00103_n25Municipio_Codigo = new bool[] {false} ;
         H00103_A2034Contratante_TtlRltGerencial = new String[] {""} ;
         H00103_n2034Contratante_TtlRltGerencial = new bool[] {false} ;
         H00103_A1808Contratante_ReceitaBruta = new decimal[1] ;
         H00103_n1808Contratante_ReceitaBruta = new bool[] {false} ;
         H00103_A1807Contratante_PatrimonioLiquido = new decimal[1] ;
         H00103_n1807Contratante_PatrimonioLiquido = new bool[] {false} ;
         H00103_A1806Contratante_PassivoCirculante = new decimal[1] ;
         H00103_n1806Contratante_PassivoCirculante = new bool[] {false} ;
         H00103_A1805Contratante_AtivoCirculante = new decimal[1] ;
         H00103_n1805Contratante_AtivoCirculante = new bool[] {false} ;
         H00103_A2090Contratante_ExibePF = new bool[] {false} ;
         H00103_A2089Contratante_SelecionaResponsavelOS = new bool[] {false} ;
         H00103_A1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         H00103_n1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         H00103_A1761Contratante_OSGeraTR = new bool[] {false} ;
         H00103_n1761Contratante_OSGeraTR = new bool[] {false} ;
         H00103_A1759Contratante_OSGeraTH = new bool[] {false} ;
         H00103_n1759Contratante_OSGeraTH = new bool[] {false} ;
         H00103_A1760Contratante_OSGeraTRD = new bool[] {false} ;
         H00103_n1760Contratante_OSGeraTRD = new bool[] {false} ;
         H00103_A1758Contratante_OSGeraTRP = new bool[] {false} ;
         H00103_n1758Contratante_OSGeraTRP = new bool[] {false} ;
         H00103_A1757Contratante_OSGeraOS = new bool[] {false} ;
         H00103_n1757Contratante_OSGeraOS = new bool[] {false} ;
         H00103_A593Contratante_OSAutomatica = new bool[] {false} ;
         H00103_A1742Contratante_PropostaNvlCnt = new short[1] ;
         H00103_n1742Contratante_PropostaNvlCnt = new bool[] {false} ;
         H00103_A1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         H00103_n1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         H00103_A1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         H00103_n1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         H00103_A1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         H00103_n1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         H00103_A1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         H00103_n1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         H00103_A1729Contratante_RetornaSS = new bool[] {false} ;
         H00103_n1729Contratante_RetornaSS = new bool[] {false} ;
         H00103_A1727Contratante_ServicoSSPadrao = new int[1] ;
         H00103_n1727Contratante_ServicoSSPadrao = new bool[] {false} ;
         H00103_A1732Contratante_SSStatusPlanejamento = new String[] {""} ;
         H00103_n1732Contratante_SSStatusPlanejamento = new bool[] {false} ;
         H00103_A1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         H00103_n1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         H00103_A1652Contratante_SSAutomatica = new bool[] {false} ;
         H00103_n1652Contratante_SSAutomatica = new bool[] {false} ;
         H00103_A30Contratante_Ativo = new bool[] {false} ;
         H00103_A1822Contratante_UsaOSistema = new bool[] {false} ;
         H00103_n1822Contratante_UsaOSistema = new bool[] {false} ;
         H00103_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         H00103_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         H00103_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         H00103_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         H00103_A23Estado_UF = new String[] {""} ;
         H00103_A26Municipio_Nome = new String[] {""} ;
         H00103_A16Contratante_ContaCorrente = new String[] {""} ;
         H00103_n16Contratante_ContaCorrente = new bool[] {false} ;
         H00103_A337Contratante_AgenciaNro = new String[] {""} ;
         H00103_n337Contratante_AgenciaNro = new bool[] {false} ;
         H00103_A336Contratante_AgenciaNome = new String[] {""} ;
         H00103_n336Contratante_AgenciaNome = new bool[] {false} ;
         H00103_A339Contratante_BancoNro = new String[] {""} ;
         H00103_n339Contratante_BancoNro = new bool[] {false} ;
         H00103_A338Contratante_BancoNome = new String[] {""} ;
         H00103_n338Contratante_BancoNome = new bool[] {false} ;
         H00103_A13Contratante_WebSite = new String[] {""} ;
         H00103_n13Contratante_WebSite = new bool[] {false} ;
         H00103_A33Contratante_Fax = new String[] {""} ;
         H00103_n33Contratante_Fax = new bool[] {false} ;
         H00103_A32Contratante_Ramal = new String[] {""} ;
         H00103_n32Contratante_Ramal = new bool[] {false} ;
         H00103_A31Contratante_Telefone = new String[] {""} ;
         H00103_A11Contratante_IE = new String[] {""} ;
         H00103_A12Contratante_CNPJ = new String[] {""} ;
         H00103_n12Contratante_CNPJ = new bool[] {false} ;
         H00103_A2208Contratante_Sigla = new String[] {""} ;
         H00103_n2208Contratante_Sigla = new bool[] {false} ;
         H00103_A9Contratante_RazaoSocial = new String[] {""} ;
         H00103_n9Contratante_RazaoSocial = new bool[] {false} ;
         A23Estado_UF = "";
         A26Municipio_Nome = "";
         A12Contratante_CNPJ = "";
         A9Contratante_RazaoSocial = "";
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV13Websession = context.GetSession();
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockcontratante_ativocirculante_Jsonclick = "";
         lblTextblockcontratante_passivocirculante_Jsonclick = "";
         lblTextblockcontratante_patrimonioliquido_Jsonclick = "";
         lblTextblockcontratante_receitabruta_Jsonclick = "";
         lblTextblockcontratante_ttlrltgerencial_Jsonclick = "";
         lblTextblockcontratante_osautomatica_Jsonclick = "";
         lblTextblockcontratante_osgeraos_Jsonclick = "";
         lblTextblockcontratante_osgeratrp_Jsonclick = "";
         lblTextblockcontratante_osgeratrd_Jsonclick = "";
         lblTextblockcontratante_osgerath_Jsonclick = "";
         lblTextblockcontratante_osgeratr_Jsonclick = "";
         lblTextblockcontratante_oshmlgcompnd_Jsonclick = "";
         lblTextblockcontratante_selecionaresponsavelos_Jsonclick = "";
         lblTextblockcontratante_exibepf_Jsonclick = "";
         lblTextblockcontratante_propostarqrsrv_Jsonclick = "";
         lblTextblockcontratante_propostarqrprz_Jsonclick = "";
         lblTextblockcontratante_propostarqresf_Jsonclick = "";
         lblTextblockcontratante_propostarqrcnt_Jsonclick = "";
         lblTextblockcontratante_propostanvlcnt_Jsonclick = "";
         lblTextblockcontratante_ssautomatica_Jsonclick = "";
         lblTextblockcontratante_ssprestadoraunica_Jsonclick = "";
         lblTextblockcontratante_ssstatusplanejamento_Jsonclick = "";
         lblTextblockcontratante_servicosspadrao_Jsonclick = "";
         lblTextblockcontratante_retornass_Jsonclick = "";
         lblTextblockcontratante_razaosocial_Jsonclick = "";
         lblTextblockcontratante_sigla_Jsonclick = "";
         lblTextblockcontratante_cnpj_Jsonclick = "";
         lblTextblockcontratante_ie_Jsonclick = "";
         lblTextblockcontratante_telefone_Jsonclick = "";
         gxphoneLink = "";
         lblTextblockcontratante_ramal_Jsonclick = "";
         lblTextblockcontratante_fax_Jsonclick = "";
         lblTextblockcontratante_website_Jsonclick = "";
         lblTextblockcontratante_banconome_Jsonclick = "";
         lblTextblockcontratante_banconro_Jsonclick = "";
         lblTextblockcontratante_agencianome_Jsonclick = "";
         lblTextblockcontratante_agencianro_Jsonclick = "";
         lblTextblockcontratante_contacorrente_Jsonclick = "";
         lblTextblockmunicipio_nome_Jsonclick = "";
         lblTextblockestado_uf_Jsonclick = "";
         lblTextblockcontratante_iniciodoexpediente_Jsonclick = "";
         lblTextblockcontratante_usaosistema_Jsonclick = "";
         lblTextblockcontratante_ativo_Jsonclick = "";
         lblContratante_fimdoexpediente_righttext_Jsonclick = "";
         lblContratante_iniciodoexpediente_righttext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA29Contratante_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratantegeneral__default(),
            new Object[][] {
                new Object[] {
               H00102_A155Servico_Codigo, H00102_A605Servico_Sigla, H00102_A1635Servico_IsPublico
               }
               , new Object[] {
               H00103_A29Contratante_Codigo, H00103_A335Contratante_PessoaCod, H00103_A25Municipio_Codigo, H00103_n25Municipio_Codigo, H00103_A2034Contratante_TtlRltGerencial, H00103_n2034Contratante_TtlRltGerencial, H00103_A1808Contratante_ReceitaBruta, H00103_n1808Contratante_ReceitaBruta, H00103_A1807Contratante_PatrimonioLiquido, H00103_n1807Contratante_PatrimonioLiquido,
               H00103_A1806Contratante_PassivoCirculante, H00103_n1806Contratante_PassivoCirculante, H00103_A1805Contratante_AtivoCirculante, H00103_n1805Contratante_AtivoCirculante, H00103_A2090Contratante_ExibePF, H00103_A2089Contratante_SelecionaResponsavelOS, H00103_A1803Contratante_OSHmlgComPnd, H00103_n1803Contratante_OSHmlgComPnd, H00103_A1761Contratante_OSGeraTR, H00103_n1761Contratante_OSGeraTR,
               H00103_A1759Contratante_OSGeraTH, H00103_n1759Contratante_OSGeraTH, H00103_A1760Contratante_OSGeraTRD, H00103_n1760Contratante_OSGeraTRD, H00103_A1758Contratante_OSGeraTRP, H00103_n1758Contratante_OSGeraTRP, H00103_A1757Contratante_OSGeraOS, H00103_n1757Contratante_OSGeraOS, H00103_A593Contratante_OSAutomatica, H00103_A1742Contratante_PropostaNvlCnt,
               H00103_n1742Contratante_PropostaNvlCnt, H00103_A1741Contratante_PropostaRqrCnt, H00103_n1741Contratante_PropostaRqrCnt, H00103_A1740Contratante_PropostaRqrEsf, H00103_n1740Contratante_PropostaRqrEsf, H00103_A1739Contratante_PropostaRqrPrz, H00103_n1739Contratante_PropostaRqrPrz, H00103_A1738Contratante_PropostaRqrSrv, H00103_n1738Contratante_PropostaRqrSrv, H00103_A1729Contratante_RetornaSS,
               H00103_n1729Contratante_RetornaSS, H00103_A1727Contratante_ServicoSSPadrao, H00103_n1727Contratante_ServicoSSPadrao, H00103_A1732Contratante_SSStatusPlanejamento, H00103_n1732Contratante_SSStatusPlanejamento, H00103_A1733Contratante_SSPrestadoraUnica, H00103_n1733Contratante_SSPrestadoraUnica, H00103_A1652Contratante_SSAutomatica, H00103_n1652Contratante_SSAutomatica, H00103_A30Contratante_Ativo,
               H00103_A1822Contratante_UsaOSistema, H00103_n1822Contratante_UsaOSistema, H00103_A1192Contratante_FimDoExpediente, H00103_n1192Contratante_FimDoExpediente, H00103_A1448Contratante_InicioDoExpediente, H00103_n1448Contratante_InicioDoExpediente, H00103_A23Estado_UF, H00103_A26Municipio_Nome, H00103_A16Contratante_ContaCorrente, H00103_n16Contratante_ContaCorrente,
               H00103_A337Contratante_AgenciaNro, H00103_n337Contratante_AgenciaNro, H00103_A336Contratante_AgenciaNome, H00103_n336Contratante_AgenciaNome, H00103_A339Contratante_BancoNro, H00103_n339Contratante_BancoNro, H00103_A338Contratante_BancoNome, H00103_n338Contratante_BancoNome, H00103_A13Contratante_WebSite, H00103_n13Contratante_WebSite,
               H00103_A33Contratante_Fax, H00103_n33Contratante_Fax, H00103_A32Contratante_Ramal, H00103_n32Contratante_Ramal, H00103_A31Contratante_Telefone, H00103_A11Contratante_IE, H00103_A12Contratante_CNPJ, H00103_n12Contratante_CNPJ, H00103_A2208Contratante_Sigla, H00103_n2208Contratante_Sigla,
               H00103_A9Contratante_RazaoSocial, H00103_n9Contratante_RazaoSocial
               }
            }
         );
         AV16Pgmname = "ContratanteGeneral";
         /* GeneXus formulas. */
         AV16Pgmname = "ContratanteGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A1742Contratante_PropostaNvlCnt ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A29Contratante_Codigo ;
      private int wcpOA29Contratante_Codigo ;
      private int A1727Contratante_ServicoSSPadrao ;
      private int A25Municipio_Codigo ;
      private int edtContratante_Codigo_Visible ;
      private int edtMunicipio_Codigo_Visible ;
      private int gxdynajaxindex ;
      private int A335Contratante_PessoaCod ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7Contratante_Codigo ;
      private int idxLst ;
      private decimal A1805Contratante_AtivoCirculante ;
      private decimal A1806Contratante_PassivoCirculante ;
      private decimal A1807Contratante_PatrimonioLiquido ;
      private decimal A1808Contratante_ReceitaBruta ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV16Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A2208Contratante_Sigla ;
      private String A11Contratante_IE ;
      private String A31Contratante_Telefone ;
      private String A32Contratante_Ramal ;
      private String A33Contratante_Fax ;
      private String A338Contratante_BancoNome ;
      private String A339Contratante_BancoNro ;
      private String A336Contratante_AgenciaNome ;
      private String A337Contratante_AgenciaNro ;
      private String A16Contratante_ContaCorrente ;
      private String A1732Contratante_SSStatusPlanejamento ;
      private String Gxuitabspanel_tabs_Width ;
      private String Gxuitabspanel_tabs_Cls ;
      private String Gxuitabspanel_tabs_Designtimetabs ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String edtContratante_Codigo_Internalname ;
      private String edtContratante_Codigo_Jsonclick ;
      private String edtMunicipio_Codigo_Internalname ;
      private String edtMunicipio_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkContratante_Ativo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String A23Estado_UF ;
      private String A26Municipio_Nome ;
      private String A9Contratante_RazaoSocial ;
      private String edtContratante_RazaoSocial_Internalname ;
      private String edtContratante_Sigla_Internalname ;
      private String edtContratante_CNPJ_Internalname ;
      private String edtContratante_IE_Internalname ;
      private String edtContratante_Telefone_Internalname ;
      private String edtContratante_Ramal_Internalname ;
      private String edtContratante_Fax_Internalname ;
      private String edtContratante_WebSite_Internalname ;
      private String edtContratante_BancoNome_Internalname ;
      private String edtContratante_BancoNro_Internalname ;
      private String edtContratante_AgenciaNome_Internalname ;
      private String edtContratante_AgenciaNro_Internalname ;
      private String edtContratante_ContaCorrente_Internalname ;
      private String edtMunicipio_Nome_Internalname ;
      private String edtEstado_UF_Internalname ;
      private String edtContratante_InicioDoExpediente_Internalname ;
      private String edtContratante_FimDoExpediente_Internalname ;
      private String cmbContratante_UsaOSistema_Internalname ;
      private String cmbContratante_SSAutomatica_Internalname ;
      private String cmbContratante_SSPrestadoraUnica_Internalname ;
      private String cmbContratante_SSStatusPlanejamento_Internalname ;
      private String dynContratante_ServicoSSPadrao_Internalname ;
      private String cmbContratante_RetornaSS_Internalname ;
      private String cmbContratante_PropostaRqrSrv_Internalname ;
      private String cmbContratante_PropostaRqrPrz_Internalname ;
      private String cmbContratante_PropostaRqrEsf_Internalname ;
      private String cmbContratante_PropostaRqrCnt_Internalname ;
      private String cmbContratante_PropostaNvlCnt_Internalname ;
      private String cmbContratante_OSAutomatica_Internalname ;
      private String cmbContratante_OSGeraOS_Internalname ;
      private String cmbContratante_OSGeraTRP_Internalname ;
      private String cmbContratante_OSGeraTRD_Internalname ;
      private String cmbContratante_OSGeraTH_Internalname ;
      private String cmbContratante_OSGeraTR_Internalname ;
      private String cmbContratante_OSHmlgComPnd_Internalname ;
      private String cmbContratante_SelecionaResponsavelOS_Internalname ;
      private String cmbContratante_ExibePF_Internalname ;
      private String edtContratante_AtivoCirculante_Internalname ;
      private String edtContratante_PassivoCirculante_Internalname ;
      private String edtContratante_PatrimonioLiquido_Internalname ;
      private String edtContratante_ReceitaBruta_Internalname ;
      private String edtContratante_TtlRltGerencial_Internalname ;
      private String hsh ;
      private String edtContratante_RazaoSocial_Link ;
      private String edtMunicipio_Nome_Link ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockcontratante_ativocirculante_Internalname ;
      private String lblTextblockcontratante_ativocirculante_Jsonclick ;
      private String edtContratante_AtivoCirculante_Jsonclick ;
      private String lblTextblockcontratante_passivocirculante_Internalname ;
      private String lblTextblockcontratante_passivocirculante_Jsonclick ;
      private String edtContratante_PassivoCirculante_Jsonclick ;
      private String lblTextblockcontratante_patrimonioliquido_Internalname ;
      private String lblTextblockcontratante_patrimonioliquido_Jsonclick ;
      private String edtContratante_PatrimonioLiquido_Jsonclick ;
      private String lblTextblockcontratante_receitabruta_Internalname ;
      private String lblTextblockcontratante_receitabruta_Jsonclick ;
      private String edtContratante_ReceitaBruta_Jsonclick ;
      private String lblTextblockcontratante_ttlrltgerencial_Internalname ;
      private String lblTextblockcontratante_ttlrltgerencial_Jsonclick ;
      private String edtContratante_TtlRltGerencial_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String grpUnnamedgroup4_Internalname ;
      private String grpUnnamedgroup6_Internalname ;
      private String grpUnnamedgroup8_Internalname ;
      private String tblUnnamedtable7_Internalname ;
      private String lblTextblockcontratante_osautomatica_Internalname ;
      private String lblTextblockcontratante_osautomatica_Jsonclick ;
      private String cmbContratante_OSAutomatica_Jsonclick ;
      private String lblTextblockcontratante_osgeraos_Internalname ;
      private String lblTextblockcontratante_osgeraos_Jsonclick ;
      private String cmbContratante_OSGeraOS_Jsonclick ;
      private String lblTextblockcontratante_osgeratrp_Internalname ;
      private String lblTextblockcontratante_osgeratrp_Jsonclick ;
      private String cmbContratante_OSGeraTRP_Jsonclick ;
      private String lblTextblockcontratante_osgeratrd_Internalname ;
      private String lblTextblockcontratante_osgeratrd_Jsonclick ;
      private String cmbContratante_OSGeraTRD_Jsonclick ;
      private String lblTextblockcontratante_osgerath_Internalname ;
      private String lblTextblockcontratante_osgerath_Jsonclick ;
      private String cmbContratante_OSGeraTH_Jsonclick ;
      private String lblTextblockcontratante_osgeratr_Internalname ;
      private String lblTextblockcontratante_osgeratr_Jsonclick ;
      private String cmbContratante_OSGeraTR_Jsonclick ;
      private String lblTextblockcontratante_oshmlgcompnd_Internalname ;
      private String lblTextblockcontratante_oshmlgcompnd_Jsonclick ;
      private String cmbContratante_OSHmlgComPnd_Jsonclick ;
      private String lblTextblockcontratante_selecionaresponsavelos_Internalname ;
      private String lblTextblockcontratante_selecionaresponsavelos_Jsonclick ;
      private String cmbContratante_SelecionaResponsavelOS_Jsonclick ;
      private String lblTextblockcontratante_exibepf_Internalname ;
      private String lblTextblockcontratante_exibepf_Jsonclick ;
      private String cmbContratante_ExibePF_Jsonclick ;
      private String tblUnnamedtable5_Internalname ;
      private String lblTextblockcontratante_propostarqrsrv_Internalname ;
      private String lblTextblockcontratante_propostarqrsrv_Jsonclick ;
      private String cmbContratante_PropostaRqrSrv_Jsonclick ;
      private String lblTextblockcontratante_propostarqrprz_Internalname ;
      private String lblTextblockcontratante_propostarqrprz_Jsonclick ;
      private String cmbContratante_PropostaRqrPrz_Jsonclick ;
      private String lblTextblockcontratante_propostarqresf_Internalname ;
      private String lblTextblockcontratante_propostarqresf_Jsonclick ;
      private String cmbContratante_PropostaRqrEsf_Jsonclick ;
      private String lblTextblockcontratante_propostarqrcnt_Internalname ;
      private String lblTextblockcontratante_propostarqrcnt_Jsonclick ;
      private String cmbContratante_PropostaRqrCnt_Jsonclick ;
      private String lblTextblockcontratante_propostanvlcnt_Internalname ;
      private String lblTextblockcontratante_propostanvlcnt_Jsonclick ;
      private String cmbContratante_PropostaNvlCnt_Jsonclick ;
      private String tblUnnamedtable3_Internalname ;
      private String lblTextblockcontratante_ssautomatica_Internalname ;
      private String lblTextblockcontratante_ssautomatica_Jsonclick ;
      private String cmbContratante_SSAutomatica_Jsonclick ;
      private String lblTextblockcontratante_ssprestadoraunica_Internalname ;
      private String lblTextblockcontratante_ssprestadoraunica_Jsonclick ;
      private String cmbContratante_SSPrestadoraUnica_Jsonclick ;
      private String lblTextblockcontratante_ssstatusplanejamento_Internalname ;
      private String lblTextblockcontratante_ssstatusplanejamento_Jsonclick ;
      private String cmbContratante_SSStatusPlanejamento_Jsonclick ;
      private String lblTextblockcontratante_servicosspadrao_Internalname ;
      private String lblTextblockcontratante_servicosspadrao_Jsonclick ;
      private String dynContratante_ServicoSSPadrao_Jsonclick ;
      private String lblTextblockcontratante_retornass_Internalname ;
      private String lblTextblockcontratante_retornass_Jsonclick ;
      private String cmbContratante_RetornaSS_Jsonclick ;
      private String tblUnnamedtable9_Internalname ;
      private String lblTextblockcontratante_razaosocial_Internalname ;
      private String lblTextblockcontratante_razaosocial_Jsonclick ;
      private String edtContratante_RazaoSocial_Jsonclick ;
      private String lblTextblockcontratante_sigla_Internalname ;
      private String lblTextblockcontratante_sigla_Jsonclick ;
      private String edtContratante_Sigla_Jsonclick ;
      private String lblTextblockcontratante_cnpj_Internalname ;
      private String lblTextblockcontratante_cnpj_Jsonclick ;
      private String edtContratante_CNPJ_Jsonclick ;
      private String lblTextblockcontratante_ie_Internalname ;
      private String lblTextblockcontratante_ie_Jsonclick ;
      private String edtContratante_IE_Jsonclick ;
      private String lblTextblockcontratante_telefone_Internalname ;
      private String lblTextblockcontratante_telefone_Jsonclick ;
      private String gxphoneLink ;
      private String edtContratante_Telefone_Jsonclick ;
      private String lblTextblockcontratante_ramal_Internalname ;
      private String lblTextblockcontratante_ramal_Jsonclick ;
      private String edtContratante_Ramal_Jsonclick ;
      private String lblTextblockcontratante_fax_Internalname ;
      private String lblTextblockcontratante_fax_Jsonclick ;
      private String edtContratante_Fax_Jsonclick ;
      private String lblTextblockcontratante_website_Internalname ;
      private String lblTextblockcontratante_website_Jsonclick ;
      private String edtContratante_WebSite_Jsonclick ;
      private String lblTextblockcontratante_banconome_Internalname ;
      private String lblTextblockcontratante_banconome_Jsonclick ;
      private String edtContratante_BancoNome_Jsonclick ;
      private String lblTextblockcontratante_banconro_Internalname ;
      private String lblTextblockcontratante_banconro_Jsonclick ;
      private String edtContratante_BancoNro_Jsonclick ;
      private String lblTextblockcontratante_agencianome_Internalname ;
      private String lblTextblockcontratante_agencianome_Jsonclick ;
      private String edtContratante_AgenciaNome_Jsonclick ;
      private String lblTextblockcontratante_agencianro_Internalname ;
      private String lblTextblockcontratante_agencianro_Jsonclick ;
      private String edtContratante_AgenciaNro_Jsonclick ;
      private String lblTextblockcontratante_contacorrente_Internalname ;
      private String lblTextblockcontratante_contacorrente_Jsonclick ;
      private String edtContratante_ContaCorrente_Jsonclick ;
      private String lblTextblockmunicipio_nome_Internalname ;
      private String lblTextblockmunicipio_nome_Jsonclick ;
      private String edtMunicipio_Nome_Jsonclick ;
      private String lblTextblockestado_uf_Internalname ;
      private String lblTextblockestado_uf_Jsonclick ;
      private String edtEstado_UF_Jsonclick ;
      private String lblTextblockcontratante_iniciodoexpediente_Internalname ;
      private String lblTextblockcontratante_iniciodoexpediente_Jsonclick ;
      private String lblTextblockcontratante_usaosistema_Internalname ;
      private String lblTextblockcontratante_usaosistema_Jsonclick ;
      private String cmbContratante_UsaOSistema_Jsonclick ;
      private String lblTextblockcontratante_ativo_Internalname ;
      private String lblTextblockcontratante_ativo_Jsonclick ;
      private String tblTablemergedcontratante_fimdoexpediente_Internalname ;
      private String edtContratante_FimDoExpediente_Jsonclick ;
      private String lblContratante_fimdoexpediente_righttext_Internalname ;
      private String lblContratante_fimdoexpediente_righttext_Jsonclick ;
      private String tblTablemergedcontratante_iniciodoexpediente_Internalname ;
      private String edtContratante_InicioDoExpediente_Jsonclick ;
      private String lblContratante_iniciodoexpediente_righttext_Internalname ;
      private String lblContratante_iniciodoexpediente_righttext_Jsonclick ;
      private String sCtrlA29Contratante_Codigo ;
      private String Gxuitabspanel_tabs_Internalname ;
      private DateTime A1448Contratante_InicioDoExpediente ;
      private DateTime A1192Contratante_FimDoExpediente ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A1822Contratante_UsaOSistema ;
      private bool A30Contratante_Ativo ;
      private bool A1652Contratante_SSAutomatica ;
      private bool A1733Contratante_SSPrestadoraUnica ;
      private bool A1729Contratante_RetornaSS ;
      private bool A1738Contratante_PropostaRqrSrv ;
      private bool A1739Contratante_PropostaRqrPrz ;
      private bool A1740Contratante_PropostaRqrEsf ;
      private bool A1741Contratante_PropostaRqrCnt ;
      private bool A593Contratante_OSAutomatica ;
      private bool A1757Contratante_OSGeraOS ;
      private bool A1758Contratante_OSGeraTRP ;
      private bool A1760Contratante_OSGeraTRD ;
      private bool A1759Contratante_OSGeraTH ;
      private bool A1761Contratante_OSGeraTR ;
      private bool A1803Contratante_OSHmlgComPnd ;
      private bool A2089Contratante_SelecionaResponsavelOS ;
      private bool A2090Contratante_ExibePF ;
      private bool Gxuitabspanel_tabs_Autowidth ;
      private bool Gxuitabspanel_tabs_Autoheight ;
      private bool Gxuitabspanel_tabs_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1822Contratante_UsaOSistema ;
      private bool n1652Contratante_SSAutomatica ;
      private bool n1733Contratante_SSPrestadoraUnica ;
      private bool n1732Contratante_SSStatusPlanejamento ;
      private bool n1729Contratante_RetornaSS ;
      private bool n1738Contratante_PropostaRqrSrv ;
      private bool n1739Contratante_PropostaRqrPrz ;
      private bool n1740Contratante_PropostaRqrEsf ;
      private bool n1741Contratante_PropostaRqrCnt ;
      private bool n1742Contratante_PropostaNvlCnt ;
      private bool n1757Contratante_OSGeraOS ;
      private bool n1758Contratante_OSGeraTRP ;
      private bool n1760Contratante_OSGeraTRD ;
      private bool n1759Contratante_OSGeraTH ;
      private bool n1761Contratante_OSGeraTR ;
      private bool n1803Contratante_OSHmlgComPnd ;
      private bool n1727Contratante_ServicoSSPadrao ;
      private bool n25Municipio_Codigo ;
      private bool n2034Contratante_TtlRltGerencial ;
      private bool n1808Contratante_ReceitaBruta ;
      private bool n1807Contratante_PatrimonioLiquido ;
      private bool n1806Contratante_PassivoCirculante ;
      private bool n1805Contratante_AtivoCirculante ;
      private bool n1192Contratante_FimDoExpediente ;
      private bool n1448Contratante_InicioDoExpediente ;
      private bool n16Contratante_ContaCorrente ;
      private bool n337Contratante_AgenciaNro ;
      private bool n336Contratante_AgenciaNome ;
      private bool n339Contratante_BancoNro ;
      private bool n338Contratante_BancoNome ;
      private bool n13Contratante_WebSite ;
      private bool n33Contratante_Fax ;
      private bool n32Contratante_Ramal ;
      private bool n12Contratante_CNPJ ;
      private bool n2208Contratante_Sigla ;
      private bool n9Contratante_RazaoSocial ;
      private bool returnInSub ;
      private String A13Contratante_WebSite ;
      private String A2034Contratante_TtlRltGerencial ;
      private String A12Contratante_CNPJ ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratante_UsaOSistema ;
      private GXCheckbox chkContratante_Ativo ;
      private GXCombobox cmbContratante_SSAutomatica ;
      private GXCombobox cmbContratante_SSPrestadoraUnica ;
      private GXCombobox cmbContratante_SSStatusPlanejamento ;
      private GXCombobox dynContratante_ServicoSSPadrao ;
      private GXCombobox cmbContratante_RetornaSS ;
      private GXCombobox cmbContratante_PropostaRqrSrv ;
      private GXCombobox cmbContratante_PropostaRqrPrz ;
      private GXCombobox cmbContratante_PropostaRqrEsf ;
      private GXCombobox cmbContratante_PropostaRqrCnt ;
      private GXCombobox cmbContratante_PropostaNvlCnt ;
      private GXCombobox cmbContratante_OSAutomatica ;
      private GXCombobox cmbContratante_OSGeraOS ;
      private GXCombobox cmbContratante_OSGeraTRP ;
      private GXCombobox cmbContratante_OSGeraTRD ;
      private GXCombobox cmbContratante_OSGeraTH ;
      private GXCombobox cmbContratante_OSGeraTR ;
      private GXCombobox cmbContratante_OSHmlgComPnd ;
      private GXCombobox cmbContratante_SelecionaResponsavelOS ;
      private GXCombobox cmbContratante_ExibePF ;
      private IDataStoreProvider pr_default ;
      private int[] H00102_A155Servico_Codigo ;
      private String[] H00102_A605Servico_Sigla ;
      private bool[] H00102_A1635Servico_IsPublico ;
      private int[] H00103_A29Contratante_Codigo ;
      private int[] H00103_A335Contratante_PessoaCod ;
      private int[] H00103_A25Municipio_Codigo ;
      private bool[] H00103_n25Municipio_Codigo ;
      private String[] H00103_A2034Contratante_TtlRltGerencial ;
      private bool[] H00103_n2034Contratante_TtlRltGerencial ;
      private decimal[] H00103_A1808Contratante_ReceitaBruta ;
      private bool[] H00103_n1808Contratante_ReceitaBruta ;
      private decimal[] H00103_A1807Contratante_PatrimonioLiquido ;
      private bool[] H00103_n1807Contratante_PatrimonioLiquido ;
      private decimal[] H00103_A1806Contratante_PassivoCirculante ;
      private bool[] H00103_n1806Contratante_PassivoCirculante ;
      private decimal[] H00103_A1805Contratante_AtivoCirculante ;
      private bool[] H00103_n1805Contratante_AtivoCirculante ;
      private bool[] H00103_A2090Contratante_ExibePF ;
      private bool[] H00103_A2089Contratante_SelecionaResponsavelOS ;
      private bool[] H00103_A1803Contratante_OSHmlgComPnd ;
      private bool[] H00103_n1803Contratante_OSHmlgComPnd ;
      private bool[] H00103_A1761Contratante_OSGeraTR ;
      private bool[] H00103_n1761Contratante_OSGeraTR ;
      private bool[] H00103_A1759Contratante_OSGeraTH ;
      private bool[] H00103_n1759Contratante_OSGeraTH ;
      private bool[] H00103_A1760Contratante_OSGeraTRD ;
      private bool[] H00103_n1760Contratante_OSGeraTRD ;
      private bool[] H00103_A1758Contratante_OSGeraTRP ;
      private bool[] H00103_n1758Contratante_OSGeraTRP ;
      private bool[] H00103_A1757Contratante_OSGeraOS ;
      private bool[] H00103_n1757Contratante_OSGeraOS ;
      private bool[] H00103_A593Contratante_OSAutomatica ;
      private short[] H00103_A1742Contratante_PropostaNvlCnt ;
      private bool[] H00103_n1742Contratante_PropostaNvlCnt ;
      private bool[] H00103_A1741Contratante_PropostaRqrCnt ;
      private bool[] H00103_n1741Contratante_PropostaRqrCnt ;
      private bool[] H00103_A1740Contratante_PropostaRqrEsf ;
      private bool[] H00103_n1740Contratante_PropostaRqrEsf ;
      private bool[] H00103_A1739Contratante_PropostaRqrPrz ;
      private bool[] H00103_n1739Contratante_PropostaRqrPrz ;
      private bool[] H00103_A1738Contratante_PropostaRqrSrv ;
      private bool[] H00103_n1738Contratante_PropostaRqrSrv ;
      private bool[] H00103_A1729Contratante_RetornaSS ;
      private bool[] H00103_n1729Contratante_RetornaSS ;
      private int[] H00103_A1727Contratante_ServicoSSPadrao ;
      private bool[] H00103_n1727Contratante_ServicoSSPadrao ;
      private String[] H00103_A1732Contratante_SSStatusPlanejamento ;
      private bool[] H00103_n1732Contratante_SSStatusPlanejamento ;
      private bool[] H00103_A1733Contratante_SSPrestadoraUnica ;
      private bool[] H00103_n1733Contratante_SSPrestadoraUnica ;
      private bool[] H00103_A1652Contratante_SSAutomatica ;
      private bool[] H00103_n1652Contratante_SSAutomatica ;
      private bool[] H00103_A30Contratante_Ativo ;
      private bool[] H00103_A1822Contratante_UsaOSistema ;
      private bool[] H00103_n1822Contratante_UsaOSistema ;
      private DateTime[] H00103_A1192Contratante_FimDoExpediente ;
      private bool[] H00103_n1192Contratante_FimDoExpediente ;
      private DateTime[] H00103_A1448Contratante_InicioDoExpediente ;
      private bool[] H00103_n1448Contratante_InicioDoExpediente ;
      private String[] H00103_A23Estado_UF ;
      private String[] H00103_A26Municipio_Nome ;
      private String[] H00103_A16Contratante_ContaCorrente ;
      private bool[] H00103_n16Contratante_ContaCorrente ;
      private String[] H00103_A337Contratante_AgenciaNro ;
      private bool[] H00103_n337Contratante_AgenciaNro ;
      private String[] H00103_A336Contratante_AgenciaNome ;
      private bool[] H00103_n336Contratante_AgenciaNome ;
      private String[] H00103_A339Contratante_BancoNro ;
      private bool[] H00103_n339Contratante_BancoNro ;
      private String[] H00103_A338Contratante_BancoNome ;
      private bool[] H00103_n338Contratante_BancoNome ;
      private String[] H00103_A13Contratante_WebSite ;
      private bool[] H00103_n13Contratante_WebSite ;
      private String[] H00103_A33Contratante_Fax ;
      private bool[] H00103_n33Contratante_Fax ;
      private String[] H00103_A32Contratante_Ramal ;
      private bool[] H00103_n32Contratante_Ramal ;
      private String[] H00103_A31Contratante_Telefone ;
      private String[] H00103_A11Contratante_IE ;
      private String[] H00103_A12Contratante_CNPJ ;
      private bool[] H00103_n12Contratante_CNPJ ;
      private String[] H00103_A2208Contratante_Sigla ;
      private bool[] H00103_n2208Contratante_Sigla ;
      private String[] H00103_A9Contratante_RazaoSocial ;
      private bool[] H00103_n9Contratante_RazaoSocial ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV13Websession ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class contratantegeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00102 ;
          prmH00102 = new Object[] {
          } ;
          Object[] prmH00103 ;
          prmH00103 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00102", "SELECT [Servico_Codigo], [Servico_Sigla], [Servico_IsPublico] FROM [Servico] WITH (NOLOCK) WHERE [Servico_IsPublico] = 1 ORDER BY [Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00102,0,0,true,false )
             ,new CursorDef("H00103", "SELECT T1.[Contratante_Codigo], T1.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[Municipio_Codigo], T1.[Contratante_TtlRltGerencial], T1.[Contratante_ReceitaBruta], T1.[Contratante_PatrimonioLiquido], T1.[Contratante_PassivoCirculante], T1.[Contratante_AtivoCirculante], T1.[Contratante_ExibePF], T1.[Contratante_SelecionaResponsavelOS], T1.[Contratante_OSHmlgComPnd], T1.[Contratante_OSGeraTR], T1.[Contratante_OSGeraTH], T1.[Contratante_OSGeraTRD], T1.[Contratante_OSGeraTRP], T1.[Contratante_OSGeraOS], T1.[Contratante_OSAutomatica], T1.[Contratante_PropostaNvlCnt], T1.[Contratante_PropostaRqrCnt], T1.[Contratante_PropostaRqrEsf], T1.[Contratante_PropostaRqrPrz], T1.[Contratante_PropostaRqrSrv], T1.[Contratante_RetornaSS], T1.[Contratante_ServicoSSPadrao], T1.[Contratante_SSStatusPlanejamento], T1.[Contratante_SSPrestadoraUnica], T1.[Contratante_SSAutomatica], T1.[Contratante_Ativo], T1.[Contratante_UsaOSistema], T1.[Contratante_FimDoExpediente], T1.[Contratante_InicioDoExpediente], T3.[Estado_UF], T3.[Municipio_Nome], T1.[Contratante_ContaCorrente], T1.[Contratante_AgenciaNro], T1.[Contratante_AgenciaNome], T1.[Contratante_BancoNro], T1.[Contratante_BancoNome], T1.[Contratante_WebSite], T1.[Contratante_Fax], T1.[Contratante_Ramal], T1.[Contratante_Telefone], T1.[Contratante_IE], T2.[Pessoa_Docto] AS Contratante_CNPJ, T1.[Contratante_Sigla], T2.[Pessoa_Nome] AS Contratante_RazaoSocial FROM (([Contratante] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratante_PessoaCod]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T1.[Municipio_Codigo]) WHERE T1.[Contratante_Codigo] = @Contratante_Codigo ORDER BY T1.[Contratante_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00103,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((bool[]) buf[14])[0] = rslt.getBool(9) ;
                ((bool[]) buf[15])[0] = rslt.getBool(10) ;
                ((bool[]) buf[16])[0] = rslt.getBool(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((bool[]) buf[18])[0] = rslt.getBool(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((bool[]) buf[20])[0] = rslt.getBool(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((bool[]) buf[22])[0] = rslt.getBool(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((bool[]) buf[24])[0] = rslt.getBool(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((bool[]) buf[26])[0] = rslt.getBool(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((bool[]) buf[28])[0] = rslt.getBool(17) ;
                ((short[]) buf[29])[0] = rslt.getShort(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((bool[]) buf[31])[0] = rslt.getBool(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((bool[]) buf[33])[0] = rslt.getBool(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((bool[]) buf[35])[0] = rslt.getBool(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((bool[]) buf[37])[0] = rslt.getBool(22) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((bool[]) buf[39])[0] = rslt.getBool(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((int[]) buf[41])[0] = rslt.getInt(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getString(25, 1) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((bool[]) buf[45])[0] = rslt.getBool(26) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((bool[]) buf[47])[0] = rslt.getBool(27) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(27);
                ((bool[]) buf[49])[0] = rslt.getBool(28) ;
                ((bool[]) buf[50])[0] = rslt.getBool(29) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(29);
                ((DateTime[]) buf[52])[0] = rslt.getGXDateTime(30) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(30);
                ((DateTime[]) buf[54])[0] = rslt.getGXDateTime(31) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(31);
                ((String[]) buf[56])[0] = rslt.getString(32, 2) ;
                ((String[]) buf[57])[0] = rslt.getString(33, 50) ;
                ((String[]) buf[58])[0] = rslt.getString(34, 10) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(34);
                ((String[]) buf[60])[0] = rslt.getString(35, 10) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(35);
                ((String[]) buf[62])[0] = rslt.getString(36, 50) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(36);
                ((String[]) buf[64])[0] = rslt.getString(37, 6) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(37);
                ((String[]) buf[66])[0] = rslt.getString(38, 50) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(38);
                ((String[]) buf[68])[0] = rslt.getVarchar(39) ;
                ((bool[]) buf[69])[0] = rslt.wasNull(39);
                ((String[]) buf[70])[0] = rslt.getString(40, 20) ;
                ((bool[]) buf[71])[0] = rslt.wasNull(40);
                ((String[]) buf[72])[0] = rslt.getString(41, 10) ;
                ((bool[]) buf[73])[0] = rslt.wasNull(41);
                ((String[]) buf[74])[0] = rslt.getString(42, 20) ;
                ((String[]) buf[75])[0] = rslt.getString(43, 15) ;
                ((String[]) buf[76])[0] = rslt.getVarchar(44) ;
                ((bool[]) buf[77])[0] = rslt.wasNull(44);
                ((String[]) buf[78])[0] = rslt.getString(45, 15) ;
                ((bool[]) buf[79])[0] = rslt.wasNull(45);
                ((String[]) buf[80])[0] = rslt.getString(46, 100) ;
                ((bool[]) buf[81])[0] = rslt.wasNull(46);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
