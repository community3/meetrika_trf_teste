/*
               File: WC_PerfisUsuario
        Description: WC_Perfis Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:39.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_perfisusuario : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_perfisusuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_perfisusuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo )
      {
         this.AV7Usuario_Codigo = aP0_Usuario_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Usuario_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_13 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_13_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_13_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
                  AV36TFPerfil_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFPerfil_Nome", AV36TFPerfil_Nome);
                  AV37TFPerfil_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFPerfil_Nome_Sel", AV37TFPerfil_Nome_Sel);
                  AV7Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
                  AV38ddo_Perfil_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_Perfil_NomeTitleControlIdToReplace", AV38ddo_Perfil_NomeTitleControlIdToReplace);
                  AV45Pgmname = GetNextPar( );
                  AV34Perfil_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV36TFPerfil_Nome, AV37TFPerfil_Nome_Sel, AV7Usuario_Codigo, AV38ddo_Perfil_NomeTitleControlIdToReplace, AV45Pgmname, AV34Perfil_AreaTrabalhoCod, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA0M2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV45Pgmname = "WC_PerfisUsuario";
               context.Gx_err = 0;
               WS0M2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "WC_Perfis Usuario") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282250404");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_perfisusuario.aspx") + "?" + UrlEncode("" +AV7Usuario_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV13OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFPERFIL_NOME", StringUtil.RTrim( AV36TFPerfil_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFPERFIL_NOME_SEL", StringUtil.RTrim( AV37TFPerfil_Nome_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_13", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_13), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV39DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV39DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vPERFIL_NOMETITLEFILTERDATA", AV35Perfil_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vPERFIL_NOMETITLEFILTERDATA", AV35Perfil_NomeTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Usuario_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV45Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Caption", StringUtil.RTrim( Ddo_perfil_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Tooltip", StringUtil.RTrim( Ddo_perfil_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Cls", StringUtil.RTrim( Ddo_perfil_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_perfil_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_perfil_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_perfil_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_perfil_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_perfil_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_perfil_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Sortedstatus", StringUtil.RTrim( Ddo_perfil_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Includefilter", StringUtil.BoolToStr( Ddo_perfil_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Filtertype", StringUtil.RTrim( Ddo_perfil_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_perfil_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_perfil_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Datalisttype", StringUtil.RTrim( Ddo_perfil_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Datalistfixedvalues", StringUtil.RTrim( Ddo_perfil_nome_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Datalistproc", StringUtil.RTrim( Ddo_perfil_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_perfil_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Sortasc", StringUtil.RTrim( Ddo_perfil_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Sortdsc", StringUtil.RTrim( Ddo_perfil_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Loadingdata", StringUtil.RTrim( Ddo_perfil_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Cleanfilter", StringUtil.RTrim( Ddo_perfil_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Rangefilterfrom", StringUtil.RTrim( Ddo_perfil_nome_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Rangefilterto", StringUtil.RTrim( Ddo_perfil_nome_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Noresultsfound", StringUtil.RTrim( Ddo_perfil_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_perfil_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Activeeventkey", StringUtil.RTrim( Ddo_perfil_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_perfil_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PERFIL_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_perfil_nome_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm0M2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_perfisusuario.js", "?202042822504063");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_PerfisUsuario" ;
      }

      public override String GetPgmdesc( )
      {
         return "WC_Perfis Usuario" ;
      }

      protected void WB0M0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_perfisusuario.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_0M2( true) ;
         }
         else
         {
            wb_table1_2_0M2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_0M2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'" + sPrefix + "',false,'" + sGXsfl_13_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV13OrderedDsc), StringUtil.BoolToStr( AV13OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WC_PerfisUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'" + sGXsfl_13_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfperfil_nome_Internalname, StringUtil.RTrim( AV36TFPerfil_Nome), StringUtil.RTrim( context.localUtil.Format( AV36TFPerfil_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,23);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfperfil_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfperfil_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WC_PerfisUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'" + sGXsfl_13_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfperfil_nome_sel_Internalname, StringUtil.RTrim( AV37TFPerfil_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV37TFPerfil_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,24);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfperfil_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfperfil_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WC_PerfisUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_PERFIL_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'" + sPrefix + "',false,'" + sGXsfl_13_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname, AV38ddo_Perfil_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", 0, edtavDdo_perfil_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WC_PerfisUsuario.htm");
         }
         wbLoad = true;
      }

      protected void START0M2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "WC_Perfis Usuario", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP0M0( ) ;
            }
         }
      }

      protected void WS0M2( )
      {
         START0M2( ) ;
         EVT0M2( ) ;
      }

      protected void EVT0M2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0M0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0M0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E110M2 */
                                    E110M2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PERFIL_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0M0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E120M2 */
                                    E120M2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0M0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavPerfil_areatrabalhocod_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0M0( ) ;
                              }
                              nGXsfl_13_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
                              SubsflControlProps_132( ) ;
                              A3Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPerfil_Codigo_Internalname), ",", "."));
                              A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", "."));
                              A4Perfil_Nome = StringUtil.Upper( cgiGet( edtPerfil_Nome_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPerfil_areatrabalhocod_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E130M2 */
                                          E130M2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPerfil_areatrabalhocod_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E140M2 */
                                          E140M2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPerfil_areatrabalhocod_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E150M2 */
                                          E150M2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV13OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfperfil_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFPERFIL_NOME"), AV36TFPerfil_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfperfil_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFPERFIL_NOME_SEL"), AV37TFPerfil_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPerfil_areatrabalhocod_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP0M0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPerfil_areatrabalhocod_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE0M2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm0M2( ) ;
            }
         }
      }

      protected void PA0M2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavPerfil_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_132( ) ;
         while ( nGXsfl_13_idx <= nRC_GXsfl_13 )
         {
            sendrow_132( ) ;
            nGXsfl_13_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_13_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_13_idx+1));
            sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
            SubsflControlProps_132( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       bool AV13OrderedDsc ,
                                       String AV36TFPerfil_Nome ,
                                       String AV37TFPerfil_Nome_Sel ,
                                       int AV7Usuario_Codigo ,
                                       String AV38ddo_Perfil_NomeTitleControlIdToReplace ,
                                       String AV45Pgmname ,
                                       int AV34Perfil_AreaTrabalhoCod ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF0M2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PERFIL_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"PERFIL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_USUARIO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0M2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV45Pgmname = "WC_PerfisUsuario";
         context.Gx_err = 0;
      }

      protected void RF0M2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 13;
         /* Execute user event: E140M2 */
         E140M2 ();
         nGXsfl_13_idx = 1;
         sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
         SubsflControlProps_132( ) ;
         nGXsfl_13_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_132( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV37TFPerfil_Nome_Sel ,
                                                 AV36TFPerfil_Nome ,
                                                 A4Perfil_Nome ,
                                                 AV13OrderedDsc ,
                                                 A1Usuario_Codigo ,
                                                 AV7Usuario_Codigo ,
                                                 A7Perfil_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV36TFPerfil_Nome = StringUtil.PadR( StringUtil.RTrim( AV36TFPerfil_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFPerfil_Nome", AV36TFPerfil_Nome);
            /* Using cursor H000M2 */
            pr_default.execute(0, new Object[] {AV7Usuario_Codigo, AV6WWPContext.gxTpr_Areatrabalho_codigo, lV36TFPerfil_Nome, AV37TFPerfil_Nome_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_13_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A7Perfil_AreaTrabalhoCod = H000M2_A7Perfil_AreaTrabalhoCod[0];
               A4Perfil_Nome = H000M2_A4Perfil_Nome[0];
               A1Usuario_Codigo = H000M2_A1Usuario_Codigo[0];
               A3Perfil_Codigo = H000M2_A3Perfil_Codigo[0];
               A7Perfil_AreaTrabalhoCod = H000M2_A7Perfil_AreaTrabalhoCod[0];
               A4Perfil_Nome = H000M2_A4Perfil_Nome[0];
               /* Execute user event: E150M2 */
               E150M2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 13;
            WB0M0( ) ;
         }
         nGXsfl_13_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV37TFPerfil_Nome_Sel ,
                                              AV36TFPerfil_Nome ,
                                              A4Perfil_Nome ,
                                              AV13OrderedDsc ,
                                              A1Usuario_Codigo ,
                                              AV7Usuario_Codigo ,
                                              A7Perfil_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV36TFPerfil_Nome = StringUtil.PadR( StringUtil.RTrim( AV36TFPerfil_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFPerfil_Nome", AV36TFPerfil_Nome);
         /* Using cursor H000M3 */
         pr_default.execute(1, new Object[] {AV7Usuario_Codigo, AV6WWPContext.gxTpr_Areatrabalho_codigo, lV36TFPerfil_Nome, AV37TFPerfil_Nome_Sel});
         GRID_nRecordCount = H000M3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV36TFPerfil_Nome, AV37TFPerfil_Nome_Sel, AV7Usuario_Codigo, AV38ddo_Perfil_NomeTitleControlIdToReplace, AV45Pgmname, AV34Perfil_AreaTrabalhoCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV36TFPerfil_Nome, AV37TFPerfil_Nome_Sel, AV7Usuario_Codigo, AV38ddo_Perfil_NomeTitleControlIdToReplace, AV45Pgmname, AV34Perfil_AreaTrabalhoCod, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV36TFPerfil_Nome, AV37TFPerfil_Nome_Sel, AV7Usuario_Codigo, AV38ddo_Perfil_NomeTitleControlIdToReplace, AV45Pgmname, AV34Perfil_AreaTrabalhoCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV36TFPerfil_Nome, AV37TFPerfil_Nome_Sel, AV7Usuario_Codigo, AV38ddo_Perfil_NomeTitleControlIdToReplace, AV45Pgmname, AV34Perfil_AreaTrabalhoCod, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV36TFPerfil_Nome, AV37TFPerfil_Nome_Sel, AV7Usuario_Codigo, AV38ddo_Perfil_NomeTitleControlIdToReplace, AV45Pgmname, AV34Perfil_AreaTrabalhoCod, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP0M0( )
      {
         /* Before Start, stand alone formulas. */
         AV45Pgmname = "WC_PerfisUsuario";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E130M2 */
         E130M2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV39DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vPERFIL_NOMETITLEFILTERDATA"), AV35Perfil_NomeTitleFilterData);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPerfil_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPerfil_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPERFIL_AREATRABALHOCOD");
               GX_FocusControl = edtavPerfil_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34Perfil_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV34Perfil_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavPerfil_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0)));
            }
            AV13OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            AV36TFPerfil_Nome = StringUtil.Upper( cgiGet( edtavTfperfil_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFPerfil_Nome", AV36TFPerfil_Nome);
            AV37TFPerfil_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfperfil_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFPerfil_Nome_Sel", AV37TFPerfil_Nome_Sel);
            AV38ddo_Perfil_NomeTitleControlIdToReplace = cgiGet( edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_Perfil_NomeTitleControlIdToReplace", AV38ddo_Perfil_NomeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_13 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_13"), ",", "."));
            AV41GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV42GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Usuario_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_perfil_nome_Caption = cgiGet( sPrefix+"DDO_PERFIL_NOME_Caption");
            Ddo_perfil_nome_Tooltip = cgiGet( sPrefix+"DDO_PERFIL_NOME_Tooltip");
            Ddo_perfil_nome_Cls = cgiGet( sPrefix+"DDO_PERFIL_NOME_Cls");
            Ddo_perfil_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_PERFIL_NOME_Filteredtext_set");
            Ddo_perfil_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_PERFIL_NOME_Selectedvalue_set");
            Ddo_perfil_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_PERFIL_NOME_Dropdownoptionstype");
            Ddo_perfil_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_PERFIL_NOME_Titlecontrolidtoreplace");
            Ddo_perfil_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PERFIL_NOME_Includesortasc"));
            Ddo_perfil_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PERFIL_NOME_Includesortdsc"));
            Ddo_perfil_nome_Sortedstatus = cgiGet( sPrefix+"DDO_PERFIL_NOME_Sortedstatus");
            Ddo_perfil_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PERFIL_NOME_Includefilter"));
            Ddo_perfil_nome_Filtertype = cgiGet( sPrefix+"DDO_PERFIL_NOME_Filtertype");
            Ddo_perfil_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PERFIL_NOME_Filterisrange"));
            Ddo_perfil_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PERFIL_NOME_Includedatalist"));
            Ddo_perfil_nome_Datalisttype = cgiGet( sPrefix+"DDO_PERFIL_NOME_Datalisttype");
            Ddo_perfil_nome_Datalistfixedvalues = cgiGet( sPrefix+"DDO_PERFIL_NOME_Datalistfixedvalues");
            Ddo_perfil_nome_Datalistproc = cgiGet( sPrefix+"DDO_PERFIL_NOME_Datalistproc");
            Ddo_perfil_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_PERFIL_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_perfil_nome_Sortasc = cgiGet( sPrefix+"DDO_PERFIL_NOME_Sortasc");
            Ddo_perfil_nome_Sortdsc = cgiGet( sPrefix+"DDO_PERFIL_NOME_Sortdsc");
            Ddo_perfil_nome_Loadingdata = cgiGet( sPrefix+"DDO_PERFIL_NOME_Loadingdata");
            Ddo_perfil_nome_Cleanfilter = cgiGet( sPrefix+"DDO_PERFIL_NOME_Cleanfilter");
            Ddo_perfil_nome_Rangefilterfrom = cgiGet( sPrefix+"DDO_PERFIL_NOME_Rangefilterfrom");
            Ddo_perfil_nome_Rangefilterto = cgiGet( sPrefix+"DDO_PERFIL_NOME_Rangefilterto");
            Ddo_perfil_nome_Noresultsfound = cgiGet( sPrefix+"DDO_PERFIL_NOME_Noresultsfound");
            Ddo_perfil_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_PERFIL_NOME_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_perfil_nome_Activeeventkey = cgiGet( sPrefix+"DDO_PERFIL_NOME_Activeeventkey");
            Ddo_perfil_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_PERFIL_NOME_Filteredtext_get");
            Ddo_perfil_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_PERFIL_NOME_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV13OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFPERFIL_NOME"), AV36TFPerfil_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFPERFIL_NOME_SEL"), AV37TFPerfil_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E130M2 */
         E130M2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E130M2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfperfil_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfperfil_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfperfil_nome_Visible), 5, 0)));
         edtavTfperfil_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfperfil_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfperfil_nome_sel_Visible), 5, 0)));
         Ddo_perfil_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Perfil_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_perfil_nome_Internalname, "TitleControlIdToReplace", Ddo_perfil_nome_Titlecontrolidtoreplace);
         AV38ddo_Perfil_NomeTitleControlIdToReplace = Ddo_perfil_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_Perfil_NomeTitleControlIdToReplace", AV38ddo_Perfil_NomeTitleControlIdToReplace);
         edtavDdo_perfil_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_perfil_nometitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV39DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV39DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E140M2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV35Perfil_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtPerfil_Nome_Titleformat = 2;
         edtPerfil_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Perfil", AV38ddo_Perfil_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPerfil_Nome_Internalname, "Title", edtPerfil_Nome_Title);
         AV41GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41GridCurrentPage), 10, 0)));
         AV42GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV35Perfil_NomeTitleFilterData", AV35Perfil_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
      }

      protected void E110M2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV40PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV40PageToGo) ;
         }
      }

      protected void E120M2( )
      {
         /* Ddo_perfil_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_perfil_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_perfil_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_perfil_nome_Internalname, "SortedStatus", Ddo_perfil_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_perfil_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_perfil_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_perfil_nome_Internalname, "SortedStatus", Ddo_perfil_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_perfil_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV36TFPerfil_Nome = Ddo_perfil_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFPerfil_Nome", AV36TFPerfil_Nome);
            AV37TFPerfil_Nome_Sel = Ddo_perfil_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFPerfil_Nome_Sel", AV37TFPerfil_Nome_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E150M2( )
      {
         /* Grid_Load Routine */
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 13;
         }
         sendrow_132( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_13_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(13, GridRow);
         }
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         Ddo_perfil_nome_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_perfil_nome_Internalname, "SortedStatus", Ddo_perfil_nome_Sortedstatus);
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV14Session.Get(AV45Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV45Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV14Session.Get(AV45Pgmname+"GridState"), "");
         }
         AV13OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV46GXV1 = 1;
         while ( AV46GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV46GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "PERFIL_AREATRABALHOCOD") == 0 )
            {
               AV34Perfil_AreaTrabalhoCod = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFPERFIL_NOME") == 0 )
            {
               AV36TFPerfil_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFPerfil_Nome", AV36TFPerfil_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFPerfil_Nome)) )
               {
                  Ddo_perfil_nome_Filteredtext_set = AV36TFPerfil_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_perfil_nome_Internalname, "FilteredText_set", Ddo_perfil_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFPERFIL_NOME_SEL") == 0 )
            {
               AV37TFPerfil_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFPerfil_Nome_Sel", AV37TFPerfil_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFPerfil_Nome_Sel)) )
               {
                  Ddo_perfil_nome_Selectedvalue_set = AV37TFPerfil_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_perfil_nome_Internalname, "SelectedValue_set", Ddo_perfil_nome_Selectedvalue_set);
               }
            }
            AV46GXV1 = (int)(AV46GXV1+1);
         }
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV14Session.Get(AV45Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Ordereddsc = AV13OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV34Perfil_AreaTrabalhoCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PERFIL_AREATRABALHOCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFPerfil_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFPERFIL_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV36TFPerfil_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFPerfil_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFPERFIL_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV37TFPerfil_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7Usuario_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&USUARIO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV45Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV45Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Usuario";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Usuario_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV14Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_0M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextperfil_areatrabalhocod_Internalname, "C�d. �rea", "", "", lblFiltertextperfil_areatrabalhocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WC_PerfisUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 7,'" + sPrefix + "',false,'" + sGXsfl_13_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPerfil_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV34Perfil_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,7);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPerfil_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WC_PerfisUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_10_0M2( true) ;
         }
         else
         {
            wb_table2_10_0M2( false) ;
         }
         return  ;
      }

      protected void wb_table2_10_0M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0M2e( true) ;
         }
         else
         {
            wb_table1_2_0M2e( false) ;
         }
      }

      protected void wb_table2_10_0M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"13\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPerfil_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtPerfil_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPerfil_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A4Perfil_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPerfil_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPerfil_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 13 )
         {
            wbEnd = 0;
            nRC_GXsfl_13 = (short)(nGXsfl_13_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_10_0M2e( true) ;
         }
         else
         {
            wb_table2_10_0M2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Usuario_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0M2( ) ;
         WS0M2( ) ;
         WE0M2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Usuario_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA0M2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_perfisusuario");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA0M2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Usuario_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
         }
         wcpOAV7Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Usuario_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Usuario_Codigo != wcpOAV7Usuario_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Usuario_Codigo = AV7Usuario_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Usuario_Codigo = cgiGet( sPrefix+"AV7Usuario_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Usuario_Codigo) > 0 )
         {
            AV7Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Usuario_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
         }
         else
         {
            AV7Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Usuario_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA0M2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS0M2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS0M2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Usuario_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Usuario_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Usuario_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Usuario_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Usuario_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE0M2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042822504219");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("wc_perfisusuario.js", "?202042822504219");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_132( )
      {
         edtPerfil_Codigo_Internalname = sPrefix+"PERFIL_CODIGO_"+sGXsfl_13_idx;
         edtUsuario_Codigo_Internalname = sPrefix+"USUARIO_CODIGO_"+sGXsfl_13_idx;
         edtPerfil_Nome_Internalname = sPrefix+"PERFIL_NOME_"+sGXsfl_13_idx;
      }

      protected void SubsflControlProps_fel_132( )
      {
         edtPerfil_Codigo_Internalname = sPrefix+"PERFIL_CODIGO_"+sGXsfl_13_fel_idx;
         edtUsuario_Codigo_Internalname = sPrefix+"USUARIO_CODIGO_"+sGXsfl_13_fel_idx;
         edtPerfil_Nome_Internalname = sPrefix+"PERFIL_NOME_"+sGXsfl_13_fel_idx;
      }

      protected void sendrow_132( )
      {
         SubsflControlProps_132( ) ;
         WB0M0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_13_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_13_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_13_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPerfil_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPerfil_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)13,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)13,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPerfil_Nome_Internalname,StringUtil.RTrim( A4Perfil_Nome),StringUtil.RTrim( context.localUtil.Format( A4Perfil_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPerfil_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)13,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PERFIL_CODIGO"+"_"+sGXsfl_13_idx, GetSecureSignedToken( sPrefix+sGXsfl_13_idx, context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_USUARIO_CODIGO"+"_"+sGXsfl_13_idx, GetSecureSignedToken( sPrefix+sGXsfl_13_idx, context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_13_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_13_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_13_idx+1));
            sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
            SubsflControlProps_132( ) ;
         }
         /* End function sendrow_132 */
      }

      protected void init_default_properties( )
      {
         lblFiltertextperfil_areatrabalhocod_Internalname = sPrefix+"FILTERTEXTPERFIL_AREATRABALHOCOD";
         edtavPerfil_areatrabalhocod_Internalname = sPrefix+"vPERFIL_AREATRABALHOCOD";
         edtPerfil_Codigo_Internalname = sPrefix+"PERFIL_CODIGO";
         edtUsuario_Codigo_Internalname = sPrefix+"USUARIO_CODIGO";
         edtPerfil_Nome_Internalname = sPrefix+"PERFIL_NOME";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfperfil_nome_Internalname = sPrefix+"vTFPERFIL_NOME";
         edtavTfperfil_nome_sel_Internalname = sPrefix+"vTFPERFIL_NOME_SEL";
         Ddo_perfil_nome_Internalname = sPrefix+"DDO_PERFIL_NOME";
         edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtPerfil_Nome_Jsonclick = "";
         edtUsuario_Codigo_Jsonclick = "";
         edtPerfil_Codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtPerfil_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavPerfil_areatrabalhocod_Jsonclick = "";
         edtPerfil_Nome_Title = "Perfil";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_perfil_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfperfil_nome_sel_Jsonclick = "";
         edtavTfperfil_nome_sel_Visible = 1;
         edtavTfperfil_nome_Jsonclick = "";
         edtavTfperfil_nome_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         Ddo_perfil_nome_Searchbuttontext = "Pesquisar";
         Ddo_perfil_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_perfil_nome_Rangefilterto = "At�";
         Ddo_perfil_nome_Rangefilterfrom = "Desde";
         Ddo_perfil_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_perfil_nome_Loadingdata = "Carregando dados...";
         Ddo_perfil_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_perfil_nome_Sortasc = "Ordenar de A � Z";
         Ddo_perfil_nome_Datalistupdateminimumcharacters = 0;
         Ddo_perfil_nome_Datalistproc = "GetWC_PerfisUsuarioFilterData";
         Ddo_perfil_nome_Datalistfixedvalues = "";
         Ddo_perfil_nome_Datalisttype = "Dynamic";
         Ddo_perfil_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_perfil_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_perfil_nome_Filtertype = "Character";
         Ddo_perfil_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_perfil_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_perfil_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_perfil_nome_Titlecontrolidtoreplace = "";
         Ddo_perfil_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_perfil_nome_Cls = "ColumnSettings";
         Ddo_perfil_nome_Tooltip = "Op��es";
         Ddo_perfil_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV38ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV34Perfil_AreaTrabalhoCod',fld:'vPERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV37TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV35Perfil_NomeTitleFilterData',fld:'vPERFIL_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtPerfil_Nome_Titleformat',ctrl:'PERFIL_NOME',prop:'Titleformat'},{av:'edtPerfil_Nome_Title',ctrl:'PERFIL_NOME',prop:'Title'},{av:'AV41GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV42GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E110M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV37TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV34Perfil_AreaTrabalhoCod',fld:'vPERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_PERFIL_NOME.ONOPTIONCLICKED","{handler:'E120M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV37TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV34Perfil_AreaTrabalhoCod',fld:'vPERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_perfil_nome_Activeeventkey',ctrl:'DDO_PERFIL_NOME',prop:'ActiveEventKey'},{av:'Ddo_perfil_nome_Filteredtext_get',ctrl:'DDO_PERFIL_NOME',prop:'FilteredText_get'},{av:'Ddo_perfil_nome_Selectedvalue_get',ctrl:'DDO_PERFIL_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_perfil_nome_Sortedstatus',ctrl:'DDO_PERFIL_NOME',prop:'SortedStatus'},{av:'AV36TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV37TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''}]}");
         setEventMetadata("GRID.LOAD","{handler:'E150M2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_perfil_nome_Activeeventkey = "";
         Ddo_perfil_nome_Filteredtext_get = "";
         Ddo_perfil_nome_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV36TFPerfil_Nome = "";
         AV37TFPerfil_Nome_Sel = "";
         AV38ddo_Perfil_NomeTitleControlIdToReplace = "";
         AV45Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV39DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV35Perfil_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_perfil_nome_Filteredtext_set = "";
         Ddo_perfil_nome_Selectedvalue_set = "";
         Ddo_perfil_nome_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A4Perfil_Nome = "";
         GridContainer = new GXWebGrid( context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         lV36TFPerfil_Nome = "";
         H000M2_A7Perfil_AreaTrabalhoCod = new int[1] ;
         H000M2_A4Perfil_Nome = new String[] {""} ;
         H000M2_A1Usuario_Codigo = new int[1] ;
         H000M2_A3Perfil_Codigo = new int[1] ;
         H000M3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV14Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         lblFiltertextperfil_areatrabalhocod_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Usuario_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wc_perfisusuario__default(),
            new Object[][] {
                new Object[] {
               H000M2_A7Perfil_AreaTrabalhoCod, H000M2_A4Perfil_Nome, H000M2_A1Usuario_Codigo, H000M2_A3Perfil_Codigo
               }
               , new Object[] {
               H000M3_AGRID_nRecordCount
               }
            }
         );
         AV45Pgmname = "WC_PerfisUsuario";
         /* GeneXus formulas. */
         AV45Pgmname = "WC_PerfisUsuario";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_13 ;
      private short nGXsfl_13_idx=1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_13_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtPerfil_Nome_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Usuario_Codigo ;
      private int wcpOAV7Usuario_Codigo ;
      private int subGrid_Rows ;
      private int AV34Perfil_AreaTrabalhoCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_perfil_nome_Datalistupdateminimumcharacters ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfperfil_nome_Visible ;
      private int edtavTfperfil_nome_sel_Visible ;
      private int edtavDdo_perfil_nometitlecontrolidtoreplace_Visible ;
      private int A3Perfil_Codigo ;
      private int A1Usuario_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A7Perfil_AreaTrabalhoCod ;
      private int AV40PageToGo ;
      private int AV46GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV41GridCurrentPage ;
      private long AV42GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_perfil_nome_Activeeventkey ;
      private String Ddo_perfil_nome_Filteredtext_get ;
      private String Ddo_perfil_nome_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_13_idx="0001" ;
      private String AV36TFPerfil_Nome ;
      private String AV37TFPerfil_Nome_Sel ;
      private String AV45Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_perfil_nome_Caption ;
      private String Ddo_perfil_nome_Tooltip ;
      private String Ddo_perfil_nome_Cls ;
      private String Ddo_perfil_nome_Filteredtext_set ;
      private String Ddo_perfil_nome_Selectedvalue_set ;
      private String Ddo_perfil_nome_Dropdownoptionstype ;
      private String Ddo_perfil_nome_Titlecontrolidtoreplace ;
      private String Ddo_perfil_nome_Sortedstatus ;
      private String Ddo_perfil_nome_Filtertype ;
      private String Ddo_perfil_nome_Datalisttype ;
      private String Ddo_perfil_nome_Datalistfixedvalues ;
      private String Ddo_perfil_nome_Datalistproc ;
      private String Ddo_perfil_nome_Sortasc ;
      private String Ddo_perfil_nome_Sortdsc ;
      private String Ddo_perfil_nome_Loadingdata ;
      private String Ddo_perfil_nome_Cleanfilter ;
      private String Ddo_perfil_nome_Rangefilterfrom ;
      private String Ddo_perfil_nome_Rangefilterto ;
      private String Ddo_perfil_nome_Noresultsfound ;
      private String Ddo_perfil_nome_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfperfil_nome_Internalname ;
      private String edtavTfperfil_nome_Jsonclick ;
      private String edtavTfperfil_nome_sel_Internalname ;
      private String edtavTfperfil_nome_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavPerfil_areatrabalhocod_Internalname ;
      private String edtPerfil_Codigo_Internalname ;
      private String edtUsuario_Codigo_Internalname ;
      private String A4Perfil_Nome ;
      private String edtPerfil_Nome_Internalname ;
      private String scmdbuf ;
      private String lV36TFPerfil_Nome ;
      private String subGrid_Internalname ;
      private String Ddo_perfil_nome_Internalname ;
      private String edtPerfil_Nome_Title ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String lblFiltertextperfil_areatrabalhocod_Internalname ;
      private String lblFiltertextperfil_areatrabalhocod_Jsonclick ;
      private String edtavPerfil_areatrabalhocod_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV7Usuario_Codigo ;
      private String sGXsfl_13_fel_idx="0001" ;
      private String ROClassString ;
      private String edtPerfil_Codigo_Jsonclick ;
      private String edtUsuario_Codigo_Jsonclick ;
      private String edtPerfil_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV13OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_perfil_nome_Includesortasc ;
      private bool Ddo_perfil_nome_Includesortdsc ;
      private bool Ddo_perfil_nome_Includefilter ;
      private bool Ddo_perfil_nome_Filterisrange ;
      private bool Ddo_perfil_nome_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV38ddo_Perfil_NomeTitleControlIdToReplace ;
      private IGxSession AV14Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H000M2_A7Perfil_AreaTrabalhoCod ;
      private String[] H000M2_A4Perfil_Nome ;
      private int[] H000M2_A1Usuario_Codigo ;
      private int[] H000M2_A3Perfil_Codigo ;
      private long[] H000M3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV35Perfil_NomeTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV39DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wc_perfisusuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H000M2( IGxContext context ,
                                             String AV37TFPerfil_Nome_Sel ,
                                             String AV36TFPerfil_Nome ,
                                             String A4Perfil_Nome ,
                                             bool AV13OrderedDsc ,
                                             int A1Usuario_Codigo ,
                                             int AV7Usuario_Codigo ,
                                             int A7Perfil_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [9] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Perfil_AreaTrabalhoCod], T2.[Perfil_Nome], T1.[Usuario_Codigo], T1.[Perfil_Codigo]";
         sFromString = " FROM ([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T2 WITH (NOLOCK) ON T2.[Perfil_Codigo] = T1.[Perfil_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Usuario_Codigo] = @AV7Usuario_Codigo)";
         sWhereString = sWhereString + " and (T2.[Perfil_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV37TFPerfil_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFPerfil_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Perfil_Nome] like @lV36TFPerfil_Nome)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFPerfil_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Perfil_Nome] = @AV37TFPerfil_Nome_Sel)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_Codigo], T2.[Perfil_Nome]";
         }
         else if ( AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_Codigo] DESC, T2.[Perfil_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_Codigo], T1.[Perfil_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H000M3( IGxContext context ,
                                             String AV37TFPerfil_Nome_Sel ,
                                             String AV36TFPerfil_Nome ,
                                             String A4Perfil_Nome ,
                                             bool AV13OrderedDsc ,
                                             int A1Usuario_Codigo ,
                                             int AV7Usuario_Codigo ,
                                             int A7Perfil_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [4] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T2 WITH (NOLOCK) ON T2.[Perfil_Codigo] = T1.[Perfil_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Usuario_Codigo] = @AV7Usuario_Codigo)";
         scmdbuf = scmdbuf + " and (T2.[Perfil_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV37TFPerfil_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFPerfil_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Perfil_Nome] like @lV36TFPerfil_Nome)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFPerfil_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Perfil_Nome] = @AV37TFPerfil_Nome_Sel)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H000M2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
               case 1 :
                     return conditional_H000M3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH000M2 ;
          prmH000M2 = new Object[] {
          new Object[] {"@AV7Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV36TFPerfil_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV37TFPerfil_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH000M3 ;
          prmH000M3 = new Object[] {
          new Object[] {"@AV7Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV36TFPerfil_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV37TFPerfil_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H000M2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000M2,11,0,true,false )
             ,new CursorDef("H000M3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000M3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
       }
    }

 }

}
