/*
               File: PRC_UsuarioEhContratada
        Description: PRC_Usuario Eh Contratada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:57:28.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_usuarioehcontratada : GXProcedure
   {
      public prc_usuarioehcontratada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_usuarioehcontratada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_Codigo ,
                           int aP1_Usuario_Codigo ,
                           out bool aP2_vrEhContratada )
      {
         this.AV11Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV8Usuario_Codigo = aP1_Usuario_Codigo;
         this.AV9vrEhContratada = false ;
         initialize();
         executePrivate();
         aP2_vrEhContratada=this.AV9vrEhContratada;
      }

      public bool executeUdp( int aP0_Contratada_Codigo ,
                              int aP1_Usuario_Codigo )
      {
         this.AV11Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV8Usuario_Codigo = aP1_Usuario_Codigo;
         this.AV9vrEhContratada = false ;
         initialize();
         executePrivate();
         aP2_vrEhContratada=this.AV9vrEhContratada;
         return AV9vrEhContratada ;
      }

      public void executeSubmit( int aP0_Contratada_Codigo ,
                                 int aP1_Usuario_Codigo ,
                                 out bool aP2_vrEhContratada )
      {
         prc_usuarioehcontratada objprc_usuarioehcontratada;
         objprc_usuarioehcontratada = new prc_usuarioehcontratada();
         objprc_usuarioehcontratada.AV11Contratada_Codigo = aP0_Contratada_Codigo;
         objprc_usuarioehcontratada.AV8Usuario_Codigo = aP1_Usuario_Codigo;
         objprc_usuarioehcontratada.AV9vrEhContratada = false ;
         objprc_usuarioehcontratada.context.SetSubmitInitialConfig(context);
         objprc_usuarioehcontratada.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_usuarioehcontratada);
         aP2_vrEhContratada=this.AV9vrEhContratada;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_usuarioehcontratada)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P001M2 */
         pr_default.execute(0, new Object[] {AV11Contratada_Codigo, AV8Usuario_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A66ContratadaUsuario_ContratadaCod = P001M2_A66ContratadaUsuario_ContratadaCod[0];
            A69ContratadaUsuario_UsuarioCod = P001M2_A69ContratadaUsuario_UsuarioCod[0];
            AV9vrEhContratada = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P001M2_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P001M2_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_usuarioehcontratada__default(),
            new Object[][] {
                new Object[] {
               P001M2_A66ContratadaUsuario_ContratadaCod, P001M2_A69ContratadaUsuario_UsuarioCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV11Contratada_Codigo ;
      private int AV8Usuario_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private String scmdbuf ;
      private bool AV9vrEhContratada ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P001M2_A66ContratadaUsuario_ContratadaCod ;
      private int[] P001M2_A69ContratadaUsuario_UsuarioCod ;
      private bool aP2_vrEhContratada ;
   }

   public class prc_usuarioehcontratada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP001M2 ;
          prmP001M2 = new Object[] {
          new Object[] {"@AV11Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P001M2", "SELECT TOP 1 [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @AV11Contratada_Codigo and [ContratadaUsuario_UsuarioCod] = @AV8Usuario_Codigo ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001M2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
