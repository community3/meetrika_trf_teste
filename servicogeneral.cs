/*
               File: ServicoGeneral
        Description: Servico General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:15:34.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public servicogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public servicogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Servico_Codigo )
      {
         this.A155Servico_Codigo = aP0_Servico_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         dynServico_UO = new GXCombobox();
         cmbServico_UORespExclusiva = new GXCombobox();
         dynServico_Anterior = new GXCombobox();
         dynServico_Posterior = new GXCombobox();
         cmbServico_Terceriza = new GXCombobox();
         cmbServico_Tela = new GXCombobox();
         cmbServico_Atende = new GXCombobox();
         cmbServico_ObrigaValores = new GXCombobox();
         cmbServico_ObjetoControle = new GXCombobox();
         cmbServico_IsPublico = new GXCombobox();
         cmbServico_IsOrigemReferencia = new GXCombobox();
         cmbServico_PausaSLA = new GXCombobox();
         cmbServico_Ativo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A155Servico_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SERVICO_UO") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLASERVICO_UO702( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SERVICO_ANTERIOR") == 0 )
               {
                  AV7Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLASERVICO_ANTERIOR702( AV7Servico_Codigo) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SERVICO_POSTERIOR") == 0 )
               {
                  AV7Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLASERVICO_POSTERIOR702( AV7Servico_Codigo) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA702( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "ServicoGeneral";
               context.Gx_err = 0;
               GXASERVICO_UO_html702( ) ;
               WS702( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Servico General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202052118153440");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servicogeneral.aspx") + "?" + UrlEncode("" +A155Servico_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA155Servico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_UO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A633Servico_UO), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_UORESPEXCLUSIVA", GetSecureSignedToken( sPrefix, A1077Servico_UORespExclusiva));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_VINCULADO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_DESCRICAO", GetSecureSignedToken( sPrefix, A156Servico_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_ANTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1545Servico_Anterior), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_POSTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1546Servico_Posterior), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_TERCERIZA", GetSecureSignedToken( sPrefix, A889Servico_Terceriza));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_TELA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1061Servico_Tela, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_ATENDE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1072Servico_Atende, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_OBRIGAVALORES", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1429Servico_ObrigaValores, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_OBJETOCONTROLE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1436Servico_ObjetoControle, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_PERCPGM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1535Servico_PercPgm), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_PERCCNC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1536Servico_PercCnc), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_PERCTMP", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1534Servico_PercTmp), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_ISPUBLICO", GetSecureSignedToken( sPrefix, A1635Servico_IsPublico));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_LINNEGCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2047Servico_LinNegCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_ISORIGEMREFERENCIA", GetSecureSignedToken( sPrefix, A2092Servico_IsOrigemReferencia));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_PAUSASLA", GetSecureSignedToken( sPrefix, A2131Servico_PausaSLA));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_ATIVO", GetSecureSignedToken( sPrefix, A632Servico_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOGRUPO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ServicoGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("servicogeneral:[SendSecurityCheck value for]"+"Servico_Vinculado:"+context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9"));
         GXUtil.WriteLog("servicogeneral:[SendSecurityCheck value for]"+"ServicoGrupo_Codigo:"+context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm702( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("servicogeneral.js", "?202052118153445");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ServicoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Servico General" ;
      }

      protected void WB700( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "servicogeneral.aspx");
            }
            wb_table1_2_702( true) ;
         }
         else
         {
            wb_table1_2_702( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_702e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtServico_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoGrupo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoGrupo_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtServicoGrupo_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START702( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Servico General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP700( ) ;
            }
         }
      }

      protected void WS702( )
      {
         START702( ) ;
         EVT702( ) ;
      }

      protected void EVT702( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP700( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP700( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11702 */
                                    E11702 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP700( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12702 */
                                    E12702 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP700( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13702 */
                                    E13702 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP700( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14702 */
                                    E14702 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP700( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15702 */
                                    E15702 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP700( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP700( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE702( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm702( ) ;
            }
         }
      }

      protected void PA702( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            dynServico_UO.Name = "SERVICO_UO";
            dynServico_UO.WebTags = "";
            cmbServico_UORespExclusiva.Name = "SERVICO_UORESPEXCLUSIVA";
            cmbServico_UORespExclusiva.WebTags = "";
            cmbServico_UORespExclusiva.addItem(StringUtil.BoolToStr( false), "Herdada pelas sub-unidades", 0);
            cmbServico_UORespExclusiva.addItem(StringUtil.BoolToStr( true), "Exclusiva", 0);
            if ( cmbServico_UORespExclusiva.ItemCount > 0 )
            {
               A1077Servico_UORespExclusiva = StringUtil.StrToBool( cmbServico_UORespExclusiva.getValidValue(StringUtil.BoolToStr( A1077Servico_UORespExclusiva)));
               n1077Servico_UORespExclusiva = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1077Servico_UORespExclusiva", A1077Servico_UORespExclusiva);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_UORESPEXCLUSIVA", GetSecureSignedToken( sPrefix, A1077Servico_UORespExclusiva));
            }
            dynServico_Anterior.Name = "SERVICO_ANTERIOR";
            dynServico_Anterior.WebTags = "";
            dynServico_Posterior.Name = "SERVICO_POSTERIOR";
            dynServico_Posterior.WebTags = "";
            cmbServico_Terceriza.Name = "SERVICO_TERCERIZA";
            cmbServico_Terceriza.WebTags = "";
            cmbServico_Terceriza.addItem(StringUtil.BoolToStr( false), "Desconhecido", 0);
            cmbServico_Terceriza.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbServico_Terceriza.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbServico_Terceriza.ItemCount > 0 )
            {
               A889Servico_Terceriza = StringUtil.StrToBool( cmbServico_Terceriza.getValidValue(StringUtil.BoolToStr( A889Servico_Terceriza)));
               n889Servico_Terceriza = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A889Servico_Terceriza", A889Servico_Terceriza);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TERCERIZA", GetSecureSignedToken( sPrefix, A889Servico_Terceriza));
            }
            cmbServico_Tela.Name = "SERVICO_TELA";
            cmbServico_Tela.WebTags = "";
            cmbServico_Tela.addItem("", "(Nenhuma)", 0);
            cmbServico_Tela.addItem("CNT", "Contagem", 0);
            cmbServico_Tela.addItem("CHK", "Check List", 0);
            if ( cmbServico_Tela.ItemCount > 0 )
            {
               A1061Servico_Tela = cmbServico_Tela.getValidValue(A1061Servico_Tela);
               n1061Servico_Tela = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1061Servico_Tela", A1061Servico_Tela);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TELA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1061Servico_Tela, ""))));
            }
            cmbServico_Atende.Name = "SERVICO_ATENDE";
            cmbServico_Atende.WebTags = "";
            cmbServico_Atende.addItem("S", "Software", 0);
            cmbServico_Atende.addItem("H", "Hardware", 0);
            if ( cmbServico_Atende.ItemCount > 0 )
            {
               A1072Servico_Atende = cmbServico_Atende.getValidValue(A1072Servico_Atende);
               n1072Servico_Atende = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1072Servico_Atende", A1072Servico_Atende);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ATENDE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1072Servico_Atende, ""))));
            }
            cmbServico_ObrigaValores.Name = "SERVICO_OBRIGAVALORES";
            cmbServico_ObrigaValores.WebTags = "";
            cmbServico_ObrigaValores.addItem("", "Nenhum", 0);
            cmbServico_ObrigaValores.addItem("L", "Valor L�quido", 0);
            cmbServico_ObrigaValores.addItem("B", "Valor Bruto", 0);
            cmbServico_ObrigaValores.addItem("A", "Ambos", 0);
            if ( cmbServico_ObrigaValores.ItemCount > 0 )
            {
               A1429Servico_ObrigaValores = cmbServico_ObrigaValores.getValidValue(A1429Servico_ObrigaValores);
               n1429Servico_ObrigaValores = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1429Servico_ObrigaValores", A1429Servico_ObrigaValores);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_OBRIGAVALORES", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1429Servico_ObrigaValores, ""))));
            }
            cmbServico_ObjetoControle.Name = "SERVICO_OBJETOCONTROLE";
            cmbServico_ObjetoControle.WebTags = "";
            cmbServico_ObjetoControle.addItem("SIS", "Sistema", 0);
            cmbServico_ObjetoControle.addItem("PRC", "Processo", 0);
            cmbServico_ObjetoControle.addItem("PRD", "Produto", 0);
            if ( cmbServico_ObjetoControle.ItemCount > 0 )
            {
               A1436Servico_ObjetoControle = cmbServico_ObjetoControle.getValidValue(A1436Servico_ObjetoControle);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1436Servico_ObjetoControle", A1436Servico_ObjetoControle);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_OBJETOCONTROLE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1436Servico_ObjetoControle, ""))));
            }
            cmbServico_IsPublico.Name = "SERVICO_ISPUBLICO";
            cmbServico_IsPublico.WebTags = "";
            cmbServico_IsPublico.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbServico_IsPublico.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbServico_IsPublico.ItemCount > 0 )
            {
               A1635Servico_IsPublico = StringUtil.StrToBool( cmbServico_IsPublico.getValidValue(StringUtil.BoolToStr( A1635Servico_IsPublico)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ISPUBLICO", GetSecureSignedToken( sPrefix, A1635Servico_IsPublico));
            }
            cmbServico_IsOrigemReferencia.Name = "SERVICO_ISORIGEMREFERENCIA";
            cmbServico_IsOrigemReferencia.WebTags = "";
            cmbServico_IsOrigemReferencia.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbServico_IsOrigemReferencia.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbServico_IsOrigemReferencia.ItemCount > 0 )
            {
               A2092Servico_IsOrigemReferencia = StringUtil.StrToBool( cmbServico_IsOrigemReferencia.getValidValue(StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2092Servico_IsOrigemReferencia", A2092Servico_IsOrigemReferencia);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ISORIGEMREFERENCIA", GetSecureSignedToken( sPrefix, A2092Servico_IsOrigemReferencia));
            }
            cmbServico_PausaSLA.Name = "SERVICO_PAUSASLA";
            cmbServico_PausaSLA.WebTags = "";
            cmbServico_PausaSLA.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbServico_PausaSLA.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbServico_PausaSLA.ItemCount > 0 )
            {
               A2131Servico_PausaSLA = StringUtil.StrToBool( cmbServico_PausaSLA.getValidValue(StringUtil.BoolToStr( A2131Servico_PausaSLA)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2131Servico_PausaSLA", A2131Servico_PausaSLA);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_PAUSASLA", GetSecureSignedToken( sPrefix, A2131Servico_PausaSLA));
            }
            cmbServico_Ativo.Name = "SERVICO_ATIVO";
            cmbServico_Ativo.WebTags = "";
            cmbServico_Ativo.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbServico_Ativo.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbServico_Ativo.ItemCount > 0 )
            {
               A632Servico_Ativo = StringUtil.StrToBool( cmbServico_Ativo.getValidValue(StringUtil.BoolToStr( A632Servico_Ativo)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A632Servico_Ativo", A632Servico_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ATIVO", GetSecureSignedToken( sPrefix, A632Servico_Ativo));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLASERVICO_UO702( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLASERVICO_UO_data702( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASERVICO_UO_html702( )
      {
         int gxdynajaxvalue ;
         GXDLASERVICO_UO_data702( ) ;
         gxdynajaxindex = 1;
         dynServico_UO.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynServico_UO.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynServico_UO.ItemCount > 0 )
         {
            A633Servico_UO = (int)(NumberUtil.Val( dynServico_UO.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0))), "."));
            n633Servico_UO = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_UO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A633Servico_UO), "ZZZZZ9")));
         }
      }

      protected void GXDLASERVICO_UO_data702( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00702 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00702_A633Servico_UO[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00702_A612UnidadeOrganizacional_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLASERVICO_ANTERIOR702( int AV7Servico_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLASERVICO_ANTERIOR_data702( AV7Servico_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASERVICO_ANTERIOR_html702( int AV7Servico_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLASERVICO_ANTERIOR_data702( AV7Servico_Codigo) ;
         gxdynajaxindex = 1;
         dynServico_Anterior.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynServico_Anterior.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynServico_Anterior.ItemCount > 0 )
         {
            A1545Servico_Anterior = (int)(NumberUtil.Val( dynServico_Anterior.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0))), "."));
            n1545Servico_Anterior = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1545Servico_Anterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ANTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1545Servico_Anterior), "ZZZZZ9")));
         }
      }

      protected void GXDLASERVICO_ANTERIOR_data702( int AV7Servico_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00703 */
         pr_default.execute(1, new Object[] {AV7Servico_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00703_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00703_A605Servico_Sigla[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLASERVICO_POSTERIOR702( int AV7Servico_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLASERVICO_POSTERIOR_data702( AV7Servico_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASERVICO_POSTERIOR_html702( int AV7Servico_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLASERVICO_POSTERIOR_data702( AV7Servico_Codigo) ;
         gxdynajaxindex = 1;
         dynServico_Posterior.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynServico_Posterior.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynServico_Posterior.ItemCount > 0 )
         {
            A1546Servico_Posterior = (int)(NumberUtil.Val( dynServico_Posterior.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0))), "."));
            n1546Servico_Posterior = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1546Servico_Posterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_POSTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1546Servico_Posterior), "ZZZZZ9")));
         }
      }

      protected void GXDLASERVICO_POSTERIOR_data702( int AV7Servico_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00704 */
         pr_default.execute(2, new Object[] {AV7Servico_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00704_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00704_A605Servico_Sigla[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynServico_UO.ItemCount > 0 )
         {
            A633Servico_UO = (int)(NumberUtil.Val( dynServico_UO.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0))), "."));
            n633Servico_UO = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_UO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A633Servico_UO), "ZZZZZ9")));
         }
         if ( cmbServico_UORespExclusiva.ItemCount > 0 )
         {
            A1077Servico_UORespExclusiva = StringUtil.StrToBool( cmbServico_UORespExclusiva.getValidValue(StringUtil.BoolToStr( A1077Servico_UORespExclusiva)));
            n1077Servico_UORespExclusiva = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1077Servico_UORespExclusiva", A1077Servico_UORespExclusiva);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_UORESPEXCLUSIVA", GetSecureSignedToken( sPrefix, A1077Servico_UORespExclusiva));
         }
         if ( dynServico_Anterior.ItemCount > 0 )
         {
            A1545Servico_Anterior = (int)(NumberUtil.Val( dynServico_Anterior.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0))), "."));
            n1545Servico_Anterior = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1545Servico_Anterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ANTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1545Servico_Anterior), "ZZZZZ9")));
         }
         if ( dynServico_Posterior.ItemCount > 0 )
         {
            A1546Servico_Posterior = (int)(NumberUtil.Val( dynServico_Posterior.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0))), "."));
            n1546Servico_Posterior = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1546Servico_Posterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_POSTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1546Servico_Posterior), "ZZZZZ9")));
         }
         if ( cmbServico_Terceriza.ItemCount > 0 )
         {
            A889Servico_Terceriza = StringUtil.StrToBool( cmbServico_Terceriza.getValidValue(StringUtil.BoolToStr( A889Servico_Terceriza)));
            n889Servico_Terceriza = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A889Servico_Terceriza", A889Servico_Terceriza);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TERCERIZA", GetSecureSignedToken( sPrefix, A889Servico_Terceriza));
         }
         if ( cmbServico_Tela.ItemCount > 0 )
         {
            A1061Servico_Tela = cmbServico_Tela.getValidValue(A1061Servico_Tela);
            n1061Servico_Tela = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1061Servico_Tela", A1061Servico_Tela);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TELA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1061Servico_Tela, ""))));
         }
         if ( cmbServico_Atende.ItemCount > 0 )
         {
            A1072Servico_Atende = cmbServico_Atende.getValidValue(A1072Servico_Atende);
            n1072Servico_Atende = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1072Servico_Atende", A1072Servico_Atende);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ATENDE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1072Servico_Atende, ""))));
         }
         if ( cmbServico_ObrigaValores.ItemCount > 0 )
         {
            A1429Servico_ObrigaValores = cmbServico_ObrigaValores.getValidValue(A1429Servico_ObrigaValores);
            n1429Servico_ObrigaValores = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1429Servico_ObrigaValores", A1429Servico_ObrigaValores);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_OBRIGAVALORES", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1429Servico_ObrigaValores, ""))));
         }
         if ( cmbServico_ObjetoControle.ItemCount > 0 )
         {
            A1436Servico_ObjetoControle = cmbServico_ObjetoControle.getValidValue(A1436Servico_ObjetoControle);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1436Servico_ObjetoControle", A1436Servico_ObjetoControle);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_OBJETOCONTROLE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1436Servico_ObjetoControle, ""))));
         }
         if ( cmbServico_IsPublico.ItemCount > 0 )
         {
            A1635Servico_IsPublico = StringUtil.StrToBool( cmbServico_IsPublico.getValidValue(StringUtil.BoolToStr( A1635Servico_IsPublico)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ISPUBLICO", GetSecureSignedToken( sPrefix, A1635Servico_IsPublico));
         }
         if ( cmbServico_IsOrigemReferencia.ItemCount > 0 )
         {
            A2092Servico_IsOrigemReferencia = StringUtil.StrToBool( cmbServico_IsOrigemReferencia.getValidValue(StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2092Servico_IsOrigemReferencia", A2092Servico_IsOrigemReferencia);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ISORIGEMREFERENCIA", GetSecureSignedToken( sPrefix, A2092Servico_IsOrigemReferencia));
         }
         if ( cmbServico_PausaSLA.ItemCount > 0 )
         {
            A2131Servico_PausaSLA = StringUtil.StrToBool( cmbServico_PausaSLA.getValidValue(StringUtil.BoolToStr( A2131Servico_PausaSLA)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2131Servico_PausaSLA", A2131Servico_PausaSLA);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_PAUSASLA", GetSecureSignedToken( sPrefix, A2131Servico_PausaSLA));
         }
         if ( cmbServico_Ativo.ItemCount > 0 )
         {
            A632Servico_Ativo = StringUtil.StrToBool( cmbServico_Ativo.getValidValue(StringUtil.BoolToStr( A632Servico_Ativo)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A632Servico_Ativo", A632Servico_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ATIVO", GetSecureSignedToken( sPrefix, A632Servico_Ativo));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF702( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "ServicoGeneral";
         context.Gx_err = 0;
      }

      protected void RF702( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00705 */
            pr_default.execute(3, new Object[] {A155Servico_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A157ServicoGrupo_Codigo = H00705_A157ServicoGrupo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICOGRUPO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")));
               A632Servico_Ativo = H00705_A632Servico_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A632Servico_Ativo", A632Servico_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ATIVO", GetSecureSignedToken( sPrefix, A632Servico_Ativo));
               A2131Servico_PausaSLA = H00705_A2131Servico_PausaSLA[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2131Servico_PausaSLA", A2131Servico_PausaSLA);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_PAUSASLA", GetSecureSignedToken( sPrefix, A2131Servico_PausaSLA));
               A2092Servico_IsOrigemReferencia = H00705_A2092Servico_IsOrigemReferencia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2092Servico_IsOrigemReferencia", A2092Servico_IsOrigemReferencia);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ISORIGEMREFERENCIA", GetSecureSignedToken( sPrefix, A2092Servico_IsOrigemReferencia));
               A2047Servico_LinNegCod = H00705_A2047Servico_LinNegCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2047Servico_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_LINNEGCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2047Servico_LinNegCod), "ZZZZZ9")));
               n2047Servico_LinNegCod = H00705_n2047Servico_LinNegCod[0];
               A1635Servico_IsPublico = H00705_A1635Servico_IsPublico[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ISPUBLICO", GetSecureSignedToken( sPrefix, A1635Servico_IsPublico));
               A1534Servico_PercTmp = H00705_A1534Servico_PercTmp[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1534Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_PERCTMP", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1534Servico_PercTmp), "ZZZ9")));
               n1534Servico_PercTmp = H00705_n1534Servico_PercTmp[0];
               A1536Servico_PercCnc = H00705_A1536Servico_PercCnc[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1536Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_PERCCNC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1536Servico_PercCnc), "ZZZ9")));
               n1536Servico_PercCnc = H00705_n1536Servico_PercCnc[0];
               A1535Servico_PercPgm = H00705_A1535Servico_PercPgm[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1535Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_PERCPGM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1535Servico_PercPgm), "ZZZ9")));
               n1535Servico_PercPgm = H00705_n1535Servico_PercPgm[0];
               A1436Servico_ObjetoControle = H00705_A1436Servico_ObjetoControle[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1436Servico_ObjetoControle", A1436Servico_ObjetoControle);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_OBJETOCONTROLE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1436Servico_ObjetoControle, ""))));
               A1429Servico_ObrigaValores = H00705_A1429Servico_ObrigaValores[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1429Servico_ObrigaValores", A1429Servico_ObrigaValores);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_OBRIGAVALORES", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1429Servico_ObrigaValores, ""))));
               n1429Servico_ObrigaValores = H00705_n1429Servico_ObrigaValores[0];
               A1072Servico_Atende = H00705_A1072Servico_Atende[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1072Servico_Atende", A1072Servico_Atende);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ATENDE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1072Servico_Atende, ""))));
               n1072Servico_Atende = H00705_n1072Servico_Atende[0];
               A1061Servico_Tela = H00705_A1061Servico_Tela[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1061Servico_Tela", A1061Servico_Tela);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TELA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1061Servico_Tela, ""))));
               n1061Servico_Tela = H00705_n1061Servico_Tela[0];
               A889Servico_Terceriza = H00705_A889Servico_Terceriza[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A889Servico_Terceriza", A889Servico_Terceriza);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TERCERIZA", GetSecureSignedToken( sPrefix, A889Servico_Terceriza));
               n889Servico_Terceriza = H00705_n889Servico_Terceriza[0];
               A1546Servico_Posterior = H00705_A1546Servico_Posterior[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1546Servico_Posterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_POSTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1546Servico_Posterior), "ZZZZZ9")));
               n1546Servico_Posterior = H00705_n1546Servico_Posterior[0];
               A1545Servico_Anterior = H00705_A1545Servico_Anterior[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1545Servico_Anterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ANTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1545Servico_Anterior), "ZZZZZ9")));
               n1545Servico_Anterior = H00705_n1545Servico_Anterior[0];
               A156Servico_Descricao = H00705_A156Servico_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A156Servico_Descricao", A156Servico_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_DESCRICAO", GetSecureSignedToken( sPrefix, A156Servico_Descricao));
               n156Servico_Descricao = H00705_n156Servico_Descricao[0];
               A631Servico_Vinculado = H00705_A631Servico_Vinculado[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_VINCULADO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9")));
               n631Servico_Vinculado = H00705_n631Servico_Vinculado[0];
               A605Servico_Sigla = H00705_A605Servico_Sigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A605Servico_Sigla", A605Servico_Sigla);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!"))));
               A608Servico_Nome = H00705_A608Servico_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A608Servico_Nome", A608Servico_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!"))));
               A158ServicoGrupo_Descricao = H00705_A158ServicoGrupo_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
               A1077Servico_UORespExclusiva = H00705_A1077Servico_UORespExclusiva[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1077Servico_UORespExclusiva", A1077Servico_UORespExclusiva);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_UORESPEXCLUSIVA", GetSecureSignedToken( sPrefix, A1077Servico_UORespExclusiva));
               n1077Servico_UORespExclusiva = H00705_n1077Servico_UORespExclusiva[0];
               A633Servico_UO = H00705_A633Servico_UO[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_UO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A633Servico_UO), "ZZZZZ9")));
               n633Servico_UO = H00705_n633Servico_UO[0];
               A158ServicoGrupo_Descricao = H00705_A158ServicoGrupo_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
               GXASERVICO_UO_html702( ) ;
               /* Execute user event: E12702 */
               E12702 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
            WB700( ) ;
         }
      }

      protected void STRUP700( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "ServicoGeneral";
         context.Gx_err = 0;
         GXASERVICO_UO_html702( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11702 */
         E11702 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynServico_UO.CurrentValue = cgiGet( dynServico_UO_Internalname);
            A633Servico_UO = (int)(NumberUtil.Val( cgiGet( dynServico_UO_Internalname), "."));
            n633Servico_UO = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_UO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A633Servico_UO), "ZZZZZ9")));
            cmbServico_UORespExclusiva.CurrentValue = cgiGet( cmbServico_UORespExclusiva_Internalname);
            A1077Servico_UORespExclusiva = StringUtil.StrToBool( cgiGet( cmbServico_UORespExclusiva_Internalname));
            n1077Servico_UORespExclusiva = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1077Servico_UORespExclusiva", A1077Servico_UORespExclusiva);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_UORESPEXCLUSIVA", GetSecureSignedToken( sPrefix, A1077Servico_UORespExclusiva));
            A158ServicoGrupo_Descricao = cgiGet( edtServicoGrupo_Descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
            A608Servico_Nome = StringUtil.Upper( cgiGet( edtServico_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A608Servico_Nome", A608Servico_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!"))));
            A605Servico_Sigla = StringUtil.Upper( cgiGet( edtServico_Sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A605Servico_Sigla", A605Servico_Sigla);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!"))));
            A631Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( edtServico_Vinculado_Internalname), ",", "."));
            n631Servico_Vinculado = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_VINCULADO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9")));
            A156Servico_Descricao = cgiGet( edtServico_Descricao_Internalname);
            n156Servico_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A156Servico_Descricao", A156Servico_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_DESCRICAO", GetSecureSignedToken( sPrefix, A156Servico_Descricao));
            dynServico_Anterior.CurrentValue = cgiGet( dynServico_Anterior_Internalname);
            A1545Servico_Anterior = (int)(NumberUtil.Val( cgiGet( dynServico_Anterior_Internalname), "."));
            n1545Servico_Anterior = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1545Servico_Anterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ANTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1545Servico_Anterior), "ZZZZZ9")));
            dynServico_Posterior.CurrentValue = cgiGet( dynServico_Posterior_Internalname);
            A1546Servico_Posterior = (int)(NumberUtil.Val( cgiGet( dynServico_Posterior_Internalname), "."));
            n1546Servico_Posterior = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1546Servico_Posterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_POSTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1546Servico_Posterior), "ZZZZZ9")));
            cmbServico_Terceriza.CurrentValue = cgiGet( cmbServico_Terceriza_Internalname);
            A889Servico_Terceriza = StringUtil.StrToBool( cgiGet( cmbServico_Terceriza_Internalname));
            n889Servico_Terceriza = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A889Servico_Terceriza", A889Servico_Terceriza);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TERCERIZA", GetSecureSignedToken( sPrefix, A889Servico_Terceriza));
            cmbServico_Tela.CurrentValue = cgiGet( cmbServico_Tela_Internalname);
            A1061Servico_Tela = cgiGet( cmbServico_Tela_Internalname);
            n1061Servico_Tela = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1061Servico_Tela", A1061Servico_Tela);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TELA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1061Servico_Tela, ""))));
            cmbServico_Atende.CurrentValue = cgiGet( cmbServico_Atende_Internalname);
            A1072Servico_Atende = cgiGet( cmbServico_Atende_Internalname);
            n1072Servico_Atende = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1072Servico_Atende", A1072Servico_Atende);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ATENDE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1072Servico_Atende, ""))));
            cmbServico_ObrigaValores.CurrentValue = cgiGet( cmbServico_ObrigaValores_Internalname);
            A1429Servico_ObrigaValores = cgiGet( cmbServico_ObrigaValores_Internalname);
            n1429Servico_ObrigaValores = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1429Servico_ObrigaValores", A1429Servico_ObrigaValores);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_OBRIGAVALORES", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1429Servico_ObrigaValores, ""))));
            cmbServico_ObjetoControle.CurrentValue = cgiGet( cmbServico_ObjetoControle_Internalname);
            A1436Servico_ObjetoControle = cgiGet( cmbServico_ObjetoControle_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1436Servico_ObjetoControle", A1436Servico_ObjetoControle);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_OBJETOCONTROLE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1436Servico_ObjetoControle, ""))));
            A1535Servico_PercPgm = (short)(context.localUtil.CToN( cgiGet( edtServico_PercPgm_Internalname), ",", "."));
            n1535Servico_PercPgm = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1535Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_PERCPGM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1535Servico_PercPgm), "ZZZ9")));
            A1536Servico_PercCnc = (short)(context.localUtil.CToN( cgiGet( edtServico_PercCnc_Internalname), ",", "."));
            n1536Servico_PercCnc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1536Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_PERCCNC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1536Servico_PercCnc), "ZZZ9")));
            A1534Servico_PercTmp = (short)(context.localUtil.CToN( cgiGet( edtServico_PercTmp_Internalname), ",", "."));
            n1534Servico_PercTmp = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1534Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_PERCTMP", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1534Servico_PercTmp), "ZZZ9")));
            cmbServico_IsPublico.CurrentValue = cgiGet( cmbServico_IsPublico_Internalname);
            A1635Servico_IsPublico = StringUtil.StrToBool( cgiGet( cmbServico_IsPublico_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ISPUBLICO", GetSecureSignedToken( sPrefix, A1635Servico_IsPublico));
            A2047Servico_LinNegCod = (int)(context.localUtil.CToN( cgiGet( edtServico_LinNegCod_Internalname), ",", "."));
            n2047Servico_LinNegCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2047Servico_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_LINNEGCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2047Servico_LinNegCod), "ZZZZZ9")));
            cmbServico_IsOrigemReferencia.CurrentValue = cgiGet( cmbServico_IsOrigemReferencia_Internalname);
            A2092Servico_IsOrigemReferencia = StringUtil.StrToBool( cgiGet( cmbServico_IsOrigemReferencia_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2092Servico_IsOrigemReferencia", A2092Servico_IsOrigemReferencia);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ISORIGEMREFERENCIA", GetSecureSignedToken( sPrefix, A2092Servico_IsOrigemReferencia));
            cmbServico_PausaSLA.CurrentValue = cgiGet( cmbServico_PausaSLA_Internalname);
            A2131Servico_PausaSLA = StringUtil.StrToBool( cgiGet( cmbServico_PausaSLA_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2131Servico_PausaSLA", A2131Servico_PausaSLA);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_PAUSASLA", GetSecureSignedToken( sPrefix, A2131Servico_PausaSLA));
            cmbServico_Ativo.CurrentValue = cgiGet( cmbServico_Ativo_Internalname);
            A632Servico_Ativo = StringUtil.StrToBool( cgiGet( cmbServico_Ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A632Servico_Ativo", A632Servico_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ATIVO", GetSecureSignedToken( sPrefix, A632Servico_Ativo));
            A157ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoGrupo_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICOGRUPO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")));
            /* Read saved values. */
            wcpOA155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA155Servico_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ServicoGeneral";
            A631Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( edtServico_Vinculado_Internalname), ",", "."));
            n631Servico_Vinculado = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_VINCULADO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9");
            A157ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoGrupo_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICOGRUPO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("servicogeneral:[SecurityCheckFailed value for]"+"Servico_Vinculado:"+context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9"));
               GXUtil.WriteLog("servicogeneral:[SecurityCheckFailed value for]"+"ServicoGrupo_Codigo:"+context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            GXASERVICO_UO_html702( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11702 */
         E11702 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11702( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12702( )
      {
         /* Load Routine */
         edtServicoGrupo_Descricao_Link = formatLink("viewservicogrupo.aspx") + "?" + UrlEncode("" +A157ServicoGrupo_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoGrupo_Descricao_Internalname, "Link", edtServicoGrupo_Descricao_Link);
         edtServico_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Visible), 5, 0)));
         edtServicoGrupo_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoGrupo_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoGrupo_Codigo_Visible), 5, 0)));
         if ( ! ( (0==A631Servico_Vinculado) ) )
         {
            edtServico_PercTmp_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_PercTmp_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_PercTmp_Visible), 5, 0)));
            cellServico_perctmp_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellServico_perctmp_cell_Internalname, "Class", cellServico_perctmp_cell_Class);
            cellTextblockservico_perctmp_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellTextblockservico_perctmp_cell_Internalname, "Class", cellTextblockservico_perctmp_cell_Class);
         }
         else
         {
            edtServico_PercTmp_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_PercTmp_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_PercTmp_Visible), 5, 0)));
            cellServico_perctmp_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellServico_perctmp_cell_Internalname, "Class", cellServico_perctmp_cell_Class);
            cellTextblockservico_perctmp_cell_Class = "DataDescriptionCell";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellTextblockservico_perctmp_cell_Internalname, "Class", cellTextblockservico_perctmp_cell_Class);
         }
      }

      protected void E13702( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("servico.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A155Servico_Codigo) + "," + UrlEncode("" +AV12ServicoGrupo_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14702( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("servico.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A155Servico_Codigo) + "," + UrlEncode("" +AV12ServicoGrupo_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E15702( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("viewservicogrupo.aspx") + "?" + UrlEncode("" +A157ServicoGrupo_Codigo) + "," + UrlEncode(StringUtil.RTrim("Servico"));
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Servico";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Servico_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Servico_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_702( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_702( true) ;
         }
         else
         {
            wb_table2_8_702( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_702e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_141_702( true) ;
         }
         else
         {
            wb_table3_141_702( false) ;
         }
         return  ;
      }

      protected void wb_table3_141_702e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_702e( true) ;
         }
         else
         {
            wb_table1_2_702e( false) ;
         }
      }

      protected void wb_table3_141_702( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 148,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_141_702e( true) ;
         }
         else
         {
            wb_table3_141_702e( false) ;
         }
      }

      protected void wb_table2_8_702( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_uo_Internalname, "Unidade Organizacional", "", "", lblTextblockservico_uo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynServico_UO, dynServico_UO_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)), 1, dynServico_UO_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoGeneral.htm");
            dynServico_UO.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynServico_UO_Internalname, "Values", (String)(dynServico_UO.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_uorespexclusiva_Internalname, "Visibilidade", "", "", lblTextblockservico_uorespexclusiva_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_UORespExclusiva, cmbServico_UORespExclusiva_Internalname, StringUtil.BoolToStr( A1077Servico_UORespExclusiva), 1, cmbServico_UORespExclusiva_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoGeneral.htm");
            cmbServico_UORespExclusiva.CurrentValue = StringUtil.BoolToStr( A1077Servico_UORespExclusiva);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_UORespExclusiva_Internalname, "Values", (String)(cmbServico_UORespExclusiva.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicogrupo_descricao_Internalname, "Grupo de Servi�os", "", "", lblTextblockservicogrupo_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoGrupo_Descricao_Internalname, A158ServicoGrupo_Descricao, StringUtil.RTrim( context.localUtil.Format( A158ServicoGrupo_Descricao, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtServicoGrupo_Descricao_Link, "", "", "", edtServicoGrupo_Descricao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_nome_Internalname, "Nome", "", "", lblTextblockservico_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Nome_Internalname, StringUtil.RTrim( A608Servico_Nome), StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_sigla_Internalname, "Sigla", "", "", lblTextblockservico_sigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Sigla_Internalname, StringUtil.RTrim( A605Servico_Sigla), StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Sigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_vinculado_Internalname, "Servi�o vinculado", "", "", lblTextblockservico_vinculado_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Vinculado_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A631Servico_Vinculado), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Vinculado_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_descricao_Internalname, "Descri��o", "", "", lblTextblockservico_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtServico_Descricao_Internalname, A156Servico_Descricao, "", "", 0, 1, 0, 0, 600, "px", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_anterior_Internalname, "Servico Anterior", "", "", lblTextblockservico_anterior_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynServico_Anterior, dynServico_Anterior_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0)), 1, dynServico_Anterior_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoGeneral.htm");
            dynServico_Anterior.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynServico_Anterior_Internalname, "Values", (String)(dynServico_Anterior.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_posterior_Internalname, "Servico Posterior", "", "", lblTextblockservico_posterior_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynServico_Posterior, dynServico_Posterior_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0)), 1, dynServico_Posterior_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoGeneral.htm");
            dynServico_Posterior.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynServico_Posterior_Internalname, "Values", (String)(dynServico_Posterior.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_terceriza_Internalname, "Pode ser tercerizado?", "", "", lblTextblockservico_terceriza_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_Terceriza, cmbServico_Terceriza_Internalname, StringUtil.BoolToStr( A889Servico_Terceriza), 1, cmbServico_Terceriza_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoGeneral.htm");
            cmbServico_Terceriza.CurrentValue = StringUtil.BoolToStr( A889Servico_Terceriza);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Terceriza_Internalname, "Values", (String)(cmbServico_Terceriza.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_tela_Internalname, "Tela", "", "", lblTextblockservico_tela_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_Tela, cmbServico_Tela_Internalname, StringUtil.RTrim( A1061Servico_Tela), 1, cmbServico_Tela_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoGeneral.htm");
            cmbServico_Tela.CurrentValue = StringUtil.RTrim( A1061Servico_Tela);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Tela_Internalname, "Values", (String)(cmbServico_Tela.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_atende_Internalname, "Atende", "", "", lblTextblockservico_atende_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_Atende, cmbServico_Atende_Internalname, StringUtil.RTrim( A1072Servico_Atende), 1, cmbServico_Atende_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoGeneral.htm");
            cmbServico_Atende.CurrentValue = StringUtil.RTrim( A1072Servico_Atende);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Atende_Internalname, "Values", (String)(cmbServico_Atende.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_obrigavalores_Internalname, "Obriga Valores", "", "", lblTextblockservico_obrigavalores_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_ObrigaValores, cmbServico_ObrigaValores_Internalname, StringUtil.RTrim( A1429Servico_ObrigaValores), 1, cmbServico_ObrigaValores_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoGeneral.htm");
            cmbServico_ObrigaValores.CurrentValue = StringUtil.RTrim( A1429Servico_ObrigaValores);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_ObrigaValores_Internalname, "Values", (String)(cmbServico_ObrigaValores.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_objetocontrole_Internalname, "Objeto de Controle", "", "", lblTextblockservico_objetocontrole_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_ObjetoControle, cmbServico_ObjetoControle_Internalname, StringUtil.RTrim( A1436Servico_ObjetoControle), 1, cmbServico_ObjetoControle_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoGeneral.htm");
            cmbServico_ObjetoControle.CurrentValue = StringUtil.RTrim( A1436Servico_ObjetoControle);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_ObjetoControle_Internalname, "Values", (String)(cmbServico_ObjetoControle.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_percpgm_Internalname, "Perc. de Pagamento", "", "", lblTextblockservico_percpgm_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table4_84_702( true) ;
         }
         else
         {
            wb_table4_84_702( false) ;
         }
         return  ;
      }

      protected void wb_table4_84_702e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_perccnc_Internalname, "Perc. de Cancelamento", "", "", lblTextblockservico_perccnc_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_93_702( true) ;
         }
         else
         {
            wb_table5_93_702( false) ;
         }
         return  ;
      }

      protected void wb_table5_93_702e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockservico_perctmp_cell_Internalname+"\"  class='"+cellTextblockservico_perctmp_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_perctmp_Internalname, "Perc. de Tempo", "", "", lblTextblockservico_perctmp_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellServico_perctmp_cell_Internalname+"\"  class='"+cellServico_perctmp_cell_Class+"'>") ;
            wb_table6_102_702( true) ;
         }
         else
         {
            wb_table6_102_702( false) ;
         }
         return  ;
      }

      protected void wb_table6_102_702e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_ispublico_Internalname, "P�blico?", "", "", lblTextblockservico_ispublico_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_IsPublico, cmbServico_IsPublico_Internalname, StringUtil.BoolToStr( A1635Servico_IsPublico), 1, cmbServico_IsPublico_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoGeneral.htm");
            cmbServico_IsPublico.CurrentValue = StringUtil.BoolToStr( A1635Servico_IsPublico);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_IsPublico_Internalname, "Values", (String)(cmbServico_IsPublico.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_linnegcod_Internalname, "Linha de Neg�cio", "", "", lblTextblockservico_linnegcod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_LinNegCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2047Servico_LinNegCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A2047Servico_LinNegCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_LinNegCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_isorigemreferencia_Internalname, "Exige Origem de Refer�ncia", "", "", lblTextblockservico_isorigemreferencia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_IsOrigemReferencia, cmbServico_IsOrigemReferencia_Internalname, StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia), 1, cmbServico_IsOrigemReferencia_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoGeneral.htm");
            cmbServico_IsOrigemReferencia.CurrentValue = StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_IsOrigemReferencia_Internalname, "Values", (String)(cmbServico_IsOrigemReferencia.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_pausasla_Internalname, "Pausar o prazo da SLA?", "", "", lblTextblockservico_pausasla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_PausaSLA, cmbServico_PausaSLA_Internalname, StringUtil.BoolToStr( A2131Servico_PausaSLA), 1, cmbServico_PausaSLA_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoGeneral.htm");
            cmbServico_PausaSLA.CurrentValue = StringUtil.BoolToStr( A2131Servico_PausaSLA);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_PausaSLA_Internalname, "Values", (String)(cmbServico_PausaSLA.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_ativo_Internalname, "Ativo?", "", "", lblTextblockservico_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_Ativo, cmbServico_Ativo_Internalname, StringUtil.BoolToStr( A632Servico_Ativo), 1, cmbServico_Ativo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoGeneral.htm");
            cmbServico_Ativo.CurrentValue = StringUtil.BoolToStr( A632Servico_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Ativo_Internalname, "Values", (String)(cmbServico_Ativo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_702e( true) ;
         }
         else
         {
            wb_table2_8_702e( false) ;
         }
      }

      protected void wb_table6_102_702( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservico_perctmp_Internalname, tblTablemergedservico_perctmp_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_PercTmp_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1534Servico_PercTmp), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1534Servico_PercTmp), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_PercTmp_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtServico_PercTmp_Visible, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServico_perctmp_righttext_Internalname, "�%", "", "", lblServico_perctmp_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_102_702e( true) ;
         }
         else
         {
            wb_table6_102_702e( false) ;
         }
      }

      protected void wb_table5_93_702( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservico_perccnc_Internalname, tblTablemergedservico_perccnc_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_PercCnc_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1536Servico_PercCnc), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1536Servico_PercCnc), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_PercCnc_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServico_perccnc_righttext_Internalname, "�%", "", "", lblServico_perccnc_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_93_702e( true) ;
         }
         else
         {
            wb_table5_93_702e( false) ;
         }
      }

      protected void wb_table4_84_702( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservico_percpgm_Internalname, tblTablemergedservico_percpgm_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_PercPgm_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1535Servico_PercPgm), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1535Servico_PercPgm), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_PercPgm_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServico_percpgm_righttext_Internalname, "�%", "", "", lblServico_percpgm_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_84_702e( true) ;
         }
         else
         {
            wb_table4_84_702e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A155Servico_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA702( ) ;
         WS702( ) ;
         WE702( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA155Servico_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA702( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "servicogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA702( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A155Servico_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
         wcpOA155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA155Servico_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A155Servico_Codigo != wcpOA155Servico_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA155Servico_Codigo = A155Servico_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA155Servico_Codigo = cgiGet( sPrefix+"A155Servico_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA155Servico_Codigo) > 0 )
         {
            A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA155Servico_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
         else
         {
            A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A155Servico_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA702( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS702( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS702( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A155Servico_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA155Servico_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A155Servico_Codigo_CTRL", StringUtil.RTrim( sCtrlA155Servico_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE702( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202052118153578");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("servicogeneral.js", "?202052118153578");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockservico_uo_Internalname = sPrefix+"TEXTBLOCKSERVICO_UO";
         dynServico_UO_Internalname = sPrefix+"SERVICO_UO";
         lblTextblockservico_uorespexclusiva_Internalname = sPrefix+"TEXTBLOCKSERVICO_UORESPEXCLUSIVA";
         cmbServico_UORespExclusiva_Internalname = sPrefix+"SERVICO_UORESPEXCLUSIVA";
         lblTextblockservicogrupo_descricao_Internalname = sPrefix+"TEXTBLOCKSERVICOGRUPO_DESCRICAO";
         edtServicoGrupo_Descricao_Internalname = sPrefix+"SERVICOGRUPO_DESCRICAO";
         lblTextblockservico_nome_Internalname = sPrefix+"TEXTBLOCKSERVICO_NOME";
         edtServico_Nome_Internalname = sPrefix+"SERVICO_NOME";
         lblTextblockservico_sigla_Internalname = sPrefix+"TEXTBLOCKSERVICO_SIGLA";
         edtServico_Sigla_Internalname = sPrefix+"SERVICO_SIGLA";
         lblTextblockservico_vinculado_Internalname = sPrefix+"TEXTBLOCKSERVICO_VINCULADO";
         edtServico_Vinculado_Internalname = sPrefix+"SERVICO_VINCULADO";
         lblTextblockservico_descricao_Internalname = sPrefix+"TEXTBLOCKSERVICO_DESCRICAO";
         edtServico_Descricao_Internalname = sPrefix+"SERVICO_DESCRICAO";
         lblTextblockservico_anterior_Internalname = sPrefix+"TEXTBLOCKSERVICO_ANTERIOR";
         dynServico_Anterior_Internalname = sPrefix+"SERVICO_ANTERIOR";
         lblTextblockservico_posterior_Internalname = sPrefix+"TEXTBLOCKSERVICO_POSTERIOR";
         dynServico_Posterior_Internalname = sPrefix+"SERVICO_POSTERIOR";
         lblTextblockservico_terceriza_Internalname = sPrefix+"TEXTBLOCKSERVICO_TERCERIZA";
         cmbServico_Terceriza_Internalname = sPrefix+"SERVICO_TERCERIZA";
         lblTextblockservico_tela_Internalname = sPrefix+"TEXTBLOCKSERVICO_TELA";
         cmbServico_Tela_Internalname = sPrefix+"SERVICO_TELA";
         lblTextblockservico_atende_Internalname = sPrefix+"TEXTBLOCKSERVICO_ATENDE";
         cmbServico_Atende_Internalname = sPrefix+"SERVICO_ATENDE";
         lblTextblockservico_obrigavalores_Internalname = sPrefix+"TEXTBLOCKSERVICO_OBRIGAVALORES";
         cmbServico_ObrigaValores_Internalname = sPrefix+"SERVICO_OBRIGAVALORES";
         lblTextblockservico_objetocontrole_Internalname = sPrefix+"TEXTBLOCKSERVICO_OBJETOCONTROLE";
         cmbServico_ObjetoControle_Internalname = sPrefix+"SERVICO_OBJETOCONTROLE";
         lblTextblockservico_percpgm_Internalname = sPrefix+"TEXTBLOCKSERVICO_PERCPGM";
         edtServico_PercPgm_Internalname = sPrefix+"SERVICO_PERCPGM";
         lblServico_percpgm_righttext_Internalname = sPrefix+"SERVICO_PERCPGM_RIGHTTEXT";
         tblTablemergedservico_percpgm_Internalname = sPrefix+"TABLEMERGEDSERVICO_PERCPGM";
         lblTextblockservico_perccnc_Internalname = sPrefix+"TEXTBLOCKSERVICO_PERCCNC";
         edtServico_PercCnc_Internalname = sPrefix+"SERVICO_PERCCNC";
         lblServico_perccnc_righttext_Internalname = sPrefix+"SERVICO_PERCCNC_RIGHTTEXT";
         tblTablemergedservico_perccnc_Internalname = sPrefix+"TABLEMERGEDSERVICO_PERCCNC";
         lblTextblockservico_perctmp_Internalname = sPrefix+"TEXTBLOCKSERVICO_PERCTMP";
         cellTextblockservico_perctmp_cell_Internalname = sPrefix+"TEXTBLOCKSERVICO_PERCTMP_CELL";
         edtServico_PercTmp_Internalname = sPrefix+"SERVICO_PERCTMP";
         lblServico_perctmp_righttext_Internalname = sPrefix+"SERVICO_PERCTMP_RIGHTTEXT";
         tblTablemergedservico_perctmp_Internalname = sPrefix+"TABLEMERGEDSERVICO_PERCTMP";
         cellServico_perctmp_cell_Internalname = sPrefix+"SERVICO_PERCTMP_CELL";
         lblTextblockservico_ispublico_Internalname = sPrefix+"TEXTBLOCKSERVICO_ISPUBLICO";
         cmbServico_IsPublico_Internalname = sPrefix+"SERVICO_ISPUBLICO";
         lblTextblockservico_linnegcod_Internalname = sPrefix+"TEXTBLOCKSERVICO_LINNEGCOD";
         edtServico_LinNegCod_Internalname = sPrefix+"SERVICO_LINNEGCOD";
         lblTextblockservico_isorigemreferencia_Internalname = sPrefix+"TEXTBLOCKSERVICO_ISORIGEMREFERENCIA";
         cmbServico_IsOrigemReferencia_Internalname = sPrefix+"SERVICO_ISORIGEMREFERENCIA";
         lblTextblockservico_pausasla_Internalname = sPrefix+"TEXTBLOCKSERVICO_PAUSASLA";
         cmbServico_PausaSLA_Internalname = sPrefix+"SERVICO_PAUSASLA";
         lblTextblockservico_ativo_Internalname = sPrefix+"TEXTBLOCKSERVICO_ATIVO";
         cmbServico_Ativo_Internalname = sPrefix+"SERVICO_ATIVO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtServico_Codigo_Internalname = sPrefix+"SERVICO_CODIGO";
         edtServicoGrupo_Codigo_Internalname = sPrefix+"SERVICOGRUPO_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtServico_PercPgm_Jsonclick = "";
         edtServico_PercCnc_Jsonclick = "";
         edtServico_PercTmp_Jsonclick = "";
         cmbServico_Ativo_Jsonclick = "";
         cmbServico_PausaSLA_Jsonclick = "";
         cmbServico_IsOrigemReferencia_Jsonclick = "";
         edtServico_LinNegCod_Jsonclick = "";
         cmbServico_IsPublico_Jsonclick = "";
         cellServico_perctmp_cell_Class = "";
         cellTextblockservico_perctmp_cell_Class = "";
         cmbServico_ObjetoControle_Jsonclick = "";
         cmbServico_ObrigaValores_Jsonclick = "";
         cmbServico_Atende_Jsonclick = "";
         cmbServico_Tela_Jsonclick = "";
         cmbServico_Terceriza_Jsonclick = "";
         dynServico_Posterior_Jsonclick = "";
         dynServico_Anterior_Jsonclick = "";
         edtServico_Vinculado_Jsonclick = "";
         edtServico_Sigla_Jsonclick = "";
         edtServico_Nome_Jsonclick = "";
         edtServicoGrupo_Descricao_Jsonclick = "";
         cmbServico_UORespExclusiva_Jsonclick = "";
         dynServico_UO_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtServico_PercTmp_Visible = 1;
         edtServicoGrupo_Descricao_Link = "";
         edtServicoGrupo_Codigo_Jsonclick = "";
         edtServicoGrupo_Codigo_Visible = 1;
         edtServico_Codigo_Jsonclick = "";
         edtServico_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13702',iparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E14702',iparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15702',iparms:[{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A608Servico_Nome = "";
         A605Servico_Sigla = "";
         A156Servico_Descricao = "";
         A1061Servico_Tela = "";
         A1072Servico_Atende = "";
         A1429Servico_ObrigaValores = "";
         A1436Servico_ObjetoControle = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00702_A633Servico_UO = new int[1] ;
         H00702_n633Servico_UO = new bool[] {false} ;
         H00702_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         H00702_n612UnidadeOrganizacional_Nome = new bool[] {false} ;
         H00703_A155Servico_Codigo = new int[1] ;
         H00703_A605Servico_Sigla = new String[] {""} ;
         H00703_A1530Servico_TipoHierarquia = new short[1] ;
         H00703_n1530Servico_TipoHierarquia = new bool[] {false} ;
         H00703_A632Servico_Ativo = new bool[] {false} ;
         H00704_A155Servico_Codigo = new int[1] ;
         H00704_A605Servico_Sigla = new String[] {""} ;
         H00704_A1530Servico_TipoHierarquia = new short[1] ;
         H00704_n1530Servico_TipoHierarquia = new bool[] {false} ;
         H00704_A632Servico_Ativo = new bool[] {false} ;
         H00705_A155Servico_Codigo = new int[1] ;
         H00705_A157ServicoGrupo_Codigo = new int[1] ;
         H00705_A632Servico_Ativo = new bool[] {false} ;
         H00705_A2131Servico_PausaSLA = new bool[] {false} ;
         H00705_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         H00705_A2047Servico_LinNegCod = new int[1] ;
         H00705_n2047Servico_LinNegCod = new bool[] {false} ;
         H00705_A1635Servico_IsPublico = new bool[] {false} ;
         H00705_A1534Servico_PercTmp = new short[1] ;
         H00705_n1534Servico_PercTmp = new bool[] {false} ;
         H00705_A1536Servico_PercCnc = new short[1] ;
         H00705_n1536Servico_PercCnc = new bool[] {false} ;
         H00705_A1535Servico_PercPgm = new short[1] ;
         H00705_n1535Servico_PercPgm = new bool[] {false} ;
         H00705_A1436Servico_ObjetoControle = new String[] {""} ;
         H00705_A1429Servico_ObrigaValores = new String[] {""} ;
         H00705_n1429Servico_ObrigaValores = new bool[] {false} ;
         H00705_A1072Servico_Atende = new String[] {""} ;
         H00705_n1072Servico_Atende = new bool[] {false} ;
         H00705_A1061Servico_Tela = new String[] {""} ;
         H00705_n1061Servico_Tela = new bool[] {false} ;
         H00705_A889Servico_Terceriza = new bool[] {false} ;
         H00705_n889Servico_Terceriza = new bool[] {false} ;
         H00705_A1546Servico_Posterior = new int[1] ;
         H00705_n1546Servico_Posterior = new bool[] {false} ;
         H00705_A1545Servico_Anterior = new int[1] ;
         H00705_n1545Servico_Anterior = new bool[] {false} ;
         H00705_A156Servico_Descricao = new String[] {""} ;
         H00705_n156Servico_Descricao = new bool[] {false} ;
         H00705_A631Servico_Vinculado = new int[1] ;
         H00705_n631Servico_Vinculado = new bool[] {false} ;
         H00705_A605Servico_Sigla = new String[] {""} ;
         H00705_A608Servico_Nome = new String[] {""} ;
         H00705_A158ServicoGrupo_Descricao = new String[] {""} ;
         H00705_A1077Servico_UORespExclusiva = new bool[] {false} ;
         H00705_n1077Servico_UORespExclusiva = new bool[] {false} ;
         H00705_A633Servico_UO = new int[1] ;
         H00705_n633Servico_UO = new bool[] {false} ;
         A158ServicoGrupo_Descricao = "";
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockservico_uo_Jsonclick = "";
         lblTextblockservico_uorespexclusiva_Jsonclick = "";
         lblTextblockservicogrupo_descricao_Jsonclick = "";
         lblTextblockservico_nome_Jsonclick = "";
         lblTextblockservico_sigla_Jsonclick = "";
         lblTextblockservico_vinculado_Jsonclick = "";
         lblTextblockservico_descricao_Jsonclick = "";
         lblTextblockservico_anterior_Jsonclick = "";
         lblTextblockservico_posterior_Jsonclick = "";
         lblTextblockservico_terceriza_Jsonclick = "";
         lblTextblockservico_tela_Jsonclick = "";
         lblTextblockservico_atende_Jsonclick = "";
         lblTextblockservico_obrigavalores_Jsonclick = "";
         lblTextblockservico_objetocontrole_Jsonclick = "";
         lblTextblockservico_percpgm_Jsonclick = "";
         lblTextblockservico_perccnc_Jsonclick = "";
         lblTextblockservico_perctmp_Jsonclick = "";
         lblTextblockservico_ispublico_Jsonclick = "";
         lblTextblockservico_linnegcod_Jsonclick = "";
         lblTextblockservico_isorigemreferencia_Jsonclick = "";
         lblTextblockservico_pausasla_Jsonclick = "";
         lblTextblockservico_ativo_Jsonclick = "";
         lblServico_perctmp_righttext_Jsonclick = "";
         lblServico_perccnc_righttext_Jsonclick = "";
         lblServico_percpgm_righttext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA155Servico_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicogeneral__default(),
            new Object[][] {
                new Object[] {
               H00702_A633Servico_UO, H00702_A612UnidadeOrganizacional_Nome, H00702_n612UnidadeOrganizacional_Nome
               }
               , new Object[] {
               H00703_A155Servico_Codigo, H00703_A605Servico_Sigla, H00703_A1530Servico_TipoHierarquia, H00703_n1530Servico_TipoHierarquia, H00703_A632Servico_Ativo
               }
               , new Object[] {
               H00704_A155Servico_Codigo, H00704_A605Servico_Sigla, H00704_A1530Servico_TipoHierarquia, H00704_n1530Servico_TipoHierarquia, H00704_A632Servico_Ativo
               }
               , new Object[] {
               H00705_A155Servico_Codigo, H00705_A157ServicoGrupo_Codigo, H00705_A632Servico_Ativo, H00705_A2131Servico_PausaSLA, H00705_A2092Servico_IsOrigemReferencia, H00705_A2047Servico_LinNegCod, H00705_n2047Servico_LinNegCod, H00705_A1635Servico_IsPublico, H00705_A1534Servico_PercTmp, H00705_n1534Servico_PercTmp,
               H00705_A1536Servico_PercCnc, H00705_n1536Servico_PercCnc, H00705_A1535Servico_PercPgm, H00705_n1535Servico_PercPgm, H00705_A1436Servico_ObjetoControle, H00705_A1429Servico_ObrigaValores, H00705_n1429Servico_ObrigaValores, H00705_A1072Servico_Atende, H00705_n1072Servico_Atende, H00705_A1061Servico_Tela,
               H00705_n1061Servico_Tela, H00705_A889Servico_Terceriza, H00705_n889Servico_Terceriza, H00705_A1546Servico_Posterior, H00705_n1546Servico_Posterior, H00705_A1545Servico_Anterior, H00705_n1545Servico_Anterior, H00705_A156Servico_Descricao, H00705_n156Servico_Descricao, H00705_A631Servico_Vinculado,
               H00705_n631Servico_Vinculado, H00705_A605Servico_Sigla, H00705_A608Servico_Nome, H00705_A158ServicoGrupo_Descricao, H00705_A1077Servico_UORespExclusiva, H00705_n1077Servico_UORespExclusiva, H00705_A633Servico_UO, H00705_n633Servico_UO
               }
            }
         );
         AV15Pgmname = "ServicoGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "ServicoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A1535Servico_PercPgm ;
      private short A1536Servico_PercCnc ;
      private short A1534Servico_PercTmp ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A155Servico_Codigo ;
      private int wcpOA155Servico_Codigo ;
      private int AV7Servico_Codigo ;
      private int AV12ServicoGrupo_Codigo ;
      private int A633Servico_UO ;
      private int A631Servico_Vinculado ;
      private int A1545Servico_Anterior ;
      private int A1546Servico_Posterior ;
      private int A2047Servico_LinNegCod ;
      private int A157ServicoGrupo_Codigo ;
      private int edtServico_Codigo_Visible ;
      private int edtServicoGrupo_Codigo_Visible ;
      private int gxdynajaxindex ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int edtServico_PercTmp_Visible ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A608Servico_Nome ;
      private String A605Servico_Sigla ;
      private String A1061Servico_Tela ;
      private String A1072Servico_Atende ;
      private String A1429Servico_ObrigaValores ;
      private String A1436Servico_ObjetoControle ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String edtServico_Codigo_Internalname ;
      private String edtServico_Codigo_Jsonclick ;
      private String edtServicoGrupo_Codigo_Internalname ;
      private String edtServicoGrupo_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String dynServico_UO_Internalname ;
      private String cmbServico_UORespExclusiva_Internalname ;
      private String edtServicoGrupo_Descricao_Internalname ;
      private String edtServico_Nome_Internalname ;
      private String edtServico_Sigla_Internalname ;
      private String edtServico_Vinculado_Internalname ;
      private String edtServico_Descricao_Internalname ;
      private String dynServico_Anterior_Internalname ;
      private String dynServico_Posterior_Internalname ;
      private String cmbServico_Terceriza_Internalname ;
      private String cmbServico_Tela_Internalname ;
      private String cmbServico_Atende_Internalname ;
      private String cmbServico_ObrigaValores_Internalname ;
      private String cmbServico_ObjetoControle_Internalname ;
      private String edtServico_PercPgm_Internalname ;
      private String edtServico_PercCnc_Internalname ;
      private String edtServico_PercTmp_Internalname ;
      private String cmbServico_IsPublico_Internalname ;
      private String edtServico_LinNegCod_Internalname ;
      private String cmbServico_IsOrigemReferencia_Internalname ;
      private String cmbServico_PausaSLA_Internalname ;
      private String cmbServico_Ativo_Internalname ;
      private String hsh ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String edtServicoGrupo_Descricao_Link ;
      private String cellServico_perctmp_cell_Class ;
      private String cellServico_perctmp_cell_Internalname ;
      private String cellTextblockservico_perctmp_cell_Class ;
      private String cellTextblockservico_perctmp_cell_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockservico_uo_Internalname ;
      private String lblTextblockservico_uo_Jsonclick ;
      private String dynServico_UO_Jsonclick ;
      private String lblTextblockservico_uorespexclusiva_Internalname ;
      private String lblTextblockservico_uorespexclusiva_Jsonclick ;
      private String cmbServico_UORespExclusiva_Jsonclick ;
      private String lblTextblockservicogrupo_descricao_Internalname ;
      private String lblTextblockservicogrupo_descricao_Jsonclick ;
      private String edtServicoGrupo_Descricao_Jsonclick ;
      private String lblTextblockservico_nome_Internalname ;
      private String lblTextblockservico_nome_Jsonclick ;
      private String edtServico_Nome_Jsonclick ;
      private String lblTextblockservico_sigla_Internalname ;
      private String lblTextblockservico_sigla_Jsonclick ;
      private String edtServico_Sigla_Jsonclick ;
      private String lblTextblockservico_vinculado_Internalname ;
      private String lblTextblockservico_vinculado_Jsonclick ;
      private String edtServico_Vinculado_Jsonclick ;
      private String lblTextblockservico_descricao_Internalname ;
      private String lblTextblockservico_descricao_Jsonclick ;
      private String lblTextblockservico_anterior_Internalname ;
      private String lblTextblockservico_anterior_Jsonclick ;
      private String dynServico_Anterior_Jsonclick ;
      private String lblTextblockservico_posterior_Internalname ;
      private String lblTextblockservico_posterior_Jsonclick ;
      private String dynServico_Posterior_Jsonclick ;
      private String lblTextblockservico_terceriza_Internalname ;
      private String lblTextblockservico_terceriza_Jsonclick ;
      private String cmbServico_Terceriza_Jsonclick ;
      private String lblTextblockservico_tela_Internalname ;
      private String lblTextblockservico_tela_Jsonclick ;
      private String cmbServico_Tela_Jsonclick ;
      private String lblTextblockservico_atende_Internalname ;
      private String lblTextblockservico_atende_Jsonclick ;
      private String cmbServico_Atende_Jsonclick ;
      private String lblTextblockservico_obrigavalores_Internalname ;
      private String lblTextblockservico_obrigavalores_Jsonclick ;
      private String cmbServico_ObrigaValores_Jsonclick ;
      private String lblTextblockservico_objetocontrole_Internalname ;
      private String lblTextblockservico_objetocontrole_Jsonclick ;
      private String cmbServico_ObjetoControle_Jsonclick ;
      private String lblTextblockservico_percpgm_Internalname ;
      private String lblTextblockservico_percpgm_Jsonclick ;
      private String lblTextblockservico_perccnc_Internalname ;
      private String lblTextblockservico_perccnc_Jsonclick ;
      private String lblTextblockservico_perctmp_Internalname ;
      private String lblTextblockservico_perctmp_Jsonclick ;
      private String lblTextblockservico_ispublico_Internalname ;
      private String lblTextblockservico_ispublico_Jsonclick ;
      private String cmbServico_IsPublico_Jsonclick ;
      private String lblTextblockservico_linnegcod_Internalname ;
      private String lblTextblockservico_linnegcod_Jsonclick ;
      private String edtServico_LinNegCod_Jsonclick ;
      private String lblTextblockservico_isorigemreferencia_Internalname ;
      private String lblTextblockservico_isorigemreferencia_Jsonclick ;
      private String cmbServico_IsOrigemReferencia_Jsonclick ;
      private String lblTextblockservico_pausasla_Internalname ;
      private String lblTextblockservico_pausasla_Jsonclick ;
      private String cmbServico_PausaSLA_Jsonclick ;
      private String lblTextblockservico_ativo_Internalname ;
      private String lblTextblockservico_ativo_Jsonclick ;
      private String cmbServico_Ativo_Jsonclick ;
      private String tblTablemergedservico_perctmp_Internalname ;
      private String edtServico_PercTmp_Jsonclick ;
      private String lblServico_perctmp_righttext_Internalname ;
      private String lblServico_perctmp_righttext_Jsonclick ;
      private String tblTablemergedservico_perccnc_Internalname ;
      private String edtServico_PercCnc_Jsonclick ;
      private String lblServico_perccnc_righttext_Internalname ;
      private String lblServico_perccnc_righttext_Jsonclick ;
      private String tblTablemergedservico_percpgm_Internalname ;
      private String edtServico_PercPgm_Jsonclick ;
      private String lblServico_percpgm_righttext_Internalname ;
      private String lblServico_percpgm_righttext_Jsonclick ;
      private String sCtrlA155Servico_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A1077Servico_UORespExclusiva ;
      private bool A889Servico_Terceriza ;
      private bool A1635Servico_IsPublico ;
      private bool A2092Servico_IsOrigemReferencia ;
      private bool A2131Servico_PausaSLA ;
      private bool A632Servico_Ativo ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1077Servico_UORespExclusiva ;
      private bool n889Servico_Terceriza ;
      private bool n1061Servico_Tela ;
      private bool n1072Servico_Atende ;
      private bool n1429Servico_ObrigaValores ;
      private bool n633Servico_UO ;
      private bool n1545Servico_Anterior ;
      private bool n1546Servico_Posterior ;
      private bool n2047Servico_LinNegCod ;
      private bool n1534Servico_PercTmp ;
      private bool n1536Servico_PercCnc ;
      private bool n1535Servico_PercPgm ;
      private bool n156Servico_Descricao ;
      private bool n631Servico_Vinculado ;
      private bool returnInSub ;
      private String A156Servico_Descricao ;
      private String A158ServicoGrupo_Descricao ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynServico_UO ;
      private GXCombobox cmbServico_UORespExclusiva ;
      private GXCombobox dynServico_Anterior ;
      private GXCombobox dynServico_Posterior ;
      private GXCombobox cmbServico_Terceriza ;
      private GXCombobox cmbServico_Tela ;
      private GXCombobox cmbServico_Atende ;
      private GXCombobox cmbServico_ObrigaValores ;
      private GXCombobox cmbServico_ObjetoControle ;
      private GXCombobox cmbServico_IsPublico ;
      private GXCombobox cmbServico_IsOrigemReferencia ;
      private GXCombobox cmbServico_PausaSLA ;
      private GXCombobox cmbServico_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H00702_A633Servico_UO ;
      private bool[] H00702_n633Servico_UO ;
      private String[] H00702_A612UnidadeOrganizacional_Nome ;
      private bool[] H00702_n612UnidadeOrganizacional_Nome ;
      private int[] H00703_A155Servico_Codigo ;
      private String[] H00703_A605Servico_Sigla ;
      private short[] H00703_A1530Servico_TipoHierarquia ;
      private bool[] H00703_n1530Servico_TipoHierarquia ;
      private bool[] H00703_A632Servico_Ativo ;
      private int[] H00704_A155Servico_Codigo ;
      private String[] H00704_A605Servico_Sigla ;
      private short[] H00704_A1530Servico_TipoHierarquia ;
      private bool[] H00704_n1530Servico_TipoHierarquia ;
      private bool[] H00704_A632Servico_Ativo ;
      private int[] H00705_A155Servico_Codigo ;
      private int[] H00705_A157ServicoGrupo_Codigo ;
      private bool[] H00705_A632Servico_Ativo ;
      private bool[] H00705_A2131Servico_PausaSLA ;
      private bool[] H00705_A2092Servico_IsOrigemReferencia ;
      private int[] H00705_A2047Servico_LinNegCod ;
      private bool[] H00705_n2047Servico_LinNegCod ;
      private bool[] H00705_A1635Servico_IsPublico ;
      private short[] H00705_A1534Servico_PercTmp ;
      private bool[] H00705_n1534Servico_PercTmp ;
      private short[] H00705_A1536Servico_PercCnc ;
      private bool[] H00705_n1536Servico_PercCnc ;
      private short[] H00705_A1535Servico_PercPgm ;
      private bool[] H00705_n1535Servico_PercPgm ;
      private String[] H00705_A1436Servico_ObjetoControle ;
      private String[] H00705_A1429Servico_ObrigaValores ;
      private bool[] H00705_n1429Servico_ObrigaValores ;
      private String[] H00705_A1072Servico_Atende ;
      private bool[] H00705_n1072Servico_Atende ;
      private String[] H00705_A1061Servico_Tela ;
      private bool[] H00705_n1061Servico_Tela ;
      private bool[] H00705_A889Servico_Terceriza ;
      private bool[] H00705_n889Servico_Terceriza ;
      private int[] H00705_A1546Servico_Posterior ;
      private bool[] H00705_n1546Servico_Posterior ;
      private int[] H00705_A1545Servico_Anterior ;
      private bool[] H00705_n1545Servico_Anterior ;
      private String[] H00705_A156Servico_Descricao ;
      private bool[] H00705_n156Servico_Descricao ;
      private int[] H00705_A631Servico_Vinculado ;
      private bool[] H00705_n631Servico_Vinculado ;
      private String[] H00705_A605Servico_Sigla ;
      private String[] H00705_A608Servico_Nome ;
      private String[] H00705_A158ServicoGrupo_Descricao ;
      private bool[] H00705_A1077Servico_UORespExclusiva ;
      private bool[] H00705_n1077Servico_UORespExclusiva ;
      private int[] H00705_A633Servico_UO ;
      private bool[] H00705_n633Servico_UO ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class servicogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00702 ;
          prmH00702 = new Object[] {
          } ;
          Object[] prmH00703 ;
          prmH00703 = new Object[] {
          new Object[] {"@AV7Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00704 ;
          prmH00704 = new Object[] {
          new Object[] {"@AV7Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00705 ;
          prmH00705 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00702", "SELECT [UnidadeOrganizacional_Codigo] AS Servico_UO, [UnidadeOrganizacional_Nome] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) ORDER BY [UnidadeOrganizacional_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00702,0,0,true,false )
             ,new CursorDef("H00703", "SELECT [Servico_Codigo], [Servico_Sigla], [Servico_TipoHierarquia], [Servico_Ativo] FROM [Servico] WITH (NOLOCK) WHERE ([Servico_Codigo] <> @AV7Servico_Codigo) AND ([Servico_Ativo] = 1) AND ([Servico_TipoHierarquia] = 1) ORDER BY [Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00703,0,0,true,false )
             ,new CursorDef("H00704", "SELECT [Servico_Codigo], [Servico_Sigla], [Servico_TipoHierarquia], [Servico_Ativo] FROM [Servico] WITH (NOLOCK) WHERE ([Servico_Codigo] <> @AV7Servico_Codigo) AND ([Servico_Ativo] = 1) AND ([Servico_TipoHierarquia] = 1) ORDER BY [Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00704,0,0,true,false )
             ,new CursorDef("H00705", "SELECT T1.[Servico_Codigo], T1.[ServicoGrupo_Codigo], T1.[Servico_Ativo], T1.[Servico_PausaSLA], T1.[Servico_IsOrigemReferencia], T1.[Servico_LinNegCod], T1.[Servico_IsPublico], T1.[Servico_PercTmp], T1.[Servico_PercCnc], T1.[Servico_PercPgm], T1.[Servico_ObjetoControle], T1.[Servico_ObrigaValores], T1.[Servico_Atende], T1.[Servico_Tela], T1.[Servico_Terceriza], T1.[Servico_Posterior], T1.[Servico_Anterior], T1.[Servico_Descricao], T1.[Servico_Vinculado], T1.[Servico_Sigla], T1.[Servico_Nome], T2.[ServicoGrupo_Descricao], T1.[Servico_UORespExclusiva], T1.[Servico_UO] AS Servico_UO FROM ([Servico] T1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = T1.[ServicoGrupo_Codigo]) WHERE T1.[Servico_Codigo] = @Servico_Codigo ORDER BY T1.[Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00705,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((bool[]) buf[7])[0] = rslt.getBool(7) ;
                ((short[]) buf[8])[0] = rslt.getShort(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((short[]) buf[10])[0] = rslt.getShort(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((short[]) buf[12])[0] = rslt.getShort(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getString(11, 3) ;
                ((String[]) buf[15])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(12);
                ((String[]) buf[17])[0] = rslt.getString(13, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(13);
                ((String[]) buf[19])[0] = rslt.getString(14, 3) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((bool[]) buf[21])[0] = rslt.getBool(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((int[]) buf[25])[0] = rslt.getInt(17) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(17);
                ((String[]) buf[27])[0] = rslt.getLongVarchar(18) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(18);
                ((int[]) buf[29])[0] = rslt.getInt(19) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(19);
                ((String[]) buf[31])[0] = rslt.getString(20, 15) ;
                ((String[]) buf[32])[0] = rslt.getString(21, 50) ;
                ((String[]) buf[33])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[34])[0] = rslt.getBool(23) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(23);
                ((int[]) buf[36])[0] = rslt.getInt(24) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(24);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
