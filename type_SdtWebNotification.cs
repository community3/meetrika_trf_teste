/*
               File: type_SdtWebNotification
        Description: WebNotification
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/4/2020 0:22:42.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtWebNotification : GxUserType, IGxExternalObject
   {
      public SdtWebNotification( )
      {
         initialize();
      }

      public SdtWebNotification( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public short notify( SdtNotificationInfo gxTp_sdtNotification )
      {
         short returnnotify ;
         if ( WebNotification_externalReference == null )
         {
            WebNotification_externalReference = new GeneXus.Notifications.GXWebNotification(context);
         }
         returnnotify = 0;
         returnnotify = (short)(WebNotification_externalReference.Notify(gxTp_sdtNotification));
         return returnnotify ;
      }

      public short notifyclient( String gxTp_clientId ,
                                 SdtNotificationInfo gxTp_sdtNotification )
      {
         short returnnotifyclient ;
         if ( WebNotification_externalReference == null )
         {
            WebNotification_externalReference = new GeneXus.Notifications.GXWebNotification(context);
         }
         returnnotifyclient = 0;
         returnnotifyclient = (short)(WebNotification_externalReference.NotifyClient(gxTp_clientId, gxTp_sdtNotification));
         return returnnotifyclient ;
      }

      public void broadcast( SdtNotificationInfo gxTp_sdtNotification )
      {
         if ( WebNotification_externalReference == null )
         {
            WebNotification_externalReference = new GeneXus.Notifications.GXWebNotification(context);
         }
         WebNotification_externalReference.Broadcast(gxTp_sdtNotification);
         return  ;
      }

      public String gxTpr_Clientid
      {
         get {
            if ( WebNotification_externalReference == null )
            {
               WebNotification_externalReference = new GeneXus.Notifications.GXWebNotification(context);
            }
            return WebNotification_externalReference.ClientId ;
         }

      }

      public short gxTpr_Errcode
      {
         get {
            if ( WebNotification_externalReference == null )
            {
               WebNotification_externalReference = new GeneXus.Notifications.GXWebNotification(context);
            }
            return WebNotification_externalReference.ErrCode ;
         }

      }

      public String gxTpr_Errdescription
      {
         get {
            if ( WebNotification_externalReference == null )
            {
               WebNotification_externalReference = new GeneXus.Notifications.GXWebNotification(context);
            }
            return WebNotification_externalReference.ErrDescription ;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( WebNotification_externalReference == null )
            {
               WebNotification_externalReference = new GeneXus.Notifications.GXWebNotification(context);
            }
            return WebNotification_externalReference ;
         }

         set {
            WebNotification_externalReference = (GeneXus.Notifications.GXWebNotification)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected GeneXus.Notifications.GXWebNotification WebNotification_externalReference=null ;
   }

}
