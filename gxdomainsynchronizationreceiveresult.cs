/*
               File: SynchronizationReceiveResult
        Description: SynchronizationReceiveResult
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 21:56:35.23
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomainsynchronizationreceiveresult
   {
      private static Hashtable domain = new Hashtable();
      static gxdomainsynchronizationreceiveresult ()
      {
         domain[(short)0] = "OK";
         domain[(short)1] = "Synchronization Not Needed";
         domain[(short)2] = "Application Is Not Offline";
         domain[(short)3] = "Has Pending Events";
         domain[(short)8] = "Already Running";
         domain[(short)51] = "Server Error 1";
         domain[(short)52] = "Server Error 2";
         domain[(short)53] = "Server Error 3";
         domain[(short)99] = "Unknown Error";
      }

      public static string getDescription( IGxContext context ,
                                           short key )
      {
         return (string)domain[key] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (short key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
