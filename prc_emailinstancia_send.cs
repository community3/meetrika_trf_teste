/*
               File: PRC_EmailInstancia_Send
        Description: PRC_Email Instancia_Send
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:43.69
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_emailinstancia_send : GXProcedure
   {
      public prc_emailinstancia_send( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_emailinstancia_send( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( Guid aP0_Email_Instancia_Guid )
      {
         this.AV10Email_Instancia_Guid = aP0_Email_Instancia_Guid;
         initialize();
         executePrivate();
      }

      public void executeSubmit( Guid aP0_Email_Instancia_Guid )
      {
         prc_emailinstancia_send objprc_emailinstancia_send;
         objprc_emailinstancia_send = new prc_emailinstancia_send();
         objprc_emailinstancia_send.AV10Email_Instancia_Guid = aP0_Email_Instancia_Guid;
         objprc_emailinstancia_send.context.SetSubmitInitialConfig(context);
         objprc_emailinstancia_send.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_emailinstancia_send);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_emailinstancia_send)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00CB2 */
         pr_default.execute(0, new Object[] {AV10Email_Instancia_Guid});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1666Email_Instancia_Guid = (Guid)((Guid)(P00CB2_A1666Email_Instancia_Guid[0]));
            new prc_emailinstanciadestinatario_submit(context).executeSubmit( ) ;
            AV9var = GXUtil.Sleep( 5);
            AV14Cont = (decimal)(AV14Cont+1);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00CB2_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         A1666Email_Instancia_Guid = (Guid)(System.Guid.Empty);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_emailinstancia_send__default(),
            new Object[][] {
                new Object[] {
               P00CB2_A1666Email_Instancia_Guid
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9var ;
      private decimal AV14Cont ;
      private String scmdbuf ;
      private Guid AV10Email_Instancia_Guid ;
      private Guid A1666Email_Instancia_Guid ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private Guid[] P00CB2_A1666Email_Instancia_Guid ;
   }

   public class prc_emailinstancia_send__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CB2 ;
          prmP00CB2 = new Object[] {
          new Object[] {"@AV10Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CB2", "SELECT [Email_Instancia_Guid] FROM [Email_Instancia] WITH (NOLOCK) WHERE [Email_Instancia_Guid] = @AV10Email_Instancia_Guid ORDER BY [Email_Instancia_Guid] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CB2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
       }
    }

 }

}
