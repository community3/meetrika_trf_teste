/*
               File: CheckList
        Description: Check List
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:3:21.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class checklist : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CHECKLIST_NAOCNFCOD") == 0 )
         {
            ajax_req_read_hidden_sdt(GetNextPar( ), AV8WWPContext);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACHECKLIST_NAOCNFCOD2H96( AV8WWPContext) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_32") == 0 )
         {
            A1839Check_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1839Check_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_32( A1839Check_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7CheckList_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7CheckList_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCHECKLIST_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7CheckList_Codigo), "ZZZZZ9")));
               AV9Check_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Check_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynCheck_Codigo.Name = "CHECK_CODIGO";
         dynCheck_Codigo.WebTags = "";
         dynCheck_Codigo.removeAllItems();
         /* Using cursor T002H5 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            dynCheck_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T002H5_A1839Check_Codigo[0]), 6, 0)), T002H5_A1841Check_Nome[0], 0);
            pr_default.readNext(3);
         }
         pr_default.close(3);
         if ( dynCheck_Codigo.ItemCount > 0 )
         {
            A1839Check_Codigo = (int)(NumberUtil.Val( dynCheck_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0))), "."));
            n1839Check_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
         }
         cmbCheck_Momento.Name = "CHECK_MOMENTO";
         cmbCheck_Momento.WebTags = "";
         cmbCheck_Momento.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhum)", 0);
         cmbCheck_Momento.addItem("1", "Ao Iniciar uma Captura", 0);
         cmbCheck_Momento.addItem("2", "Ao Terminar uma Captura", 0);
         cmbCheck_Momento.addItem("3", "Ao Iniciar um Cancelamento", 0);
         cmbCheck_Momento.addItem("4", "Ao Terminar um Cancelamento", 0);
         cmbCheck_Momento.addItem("5", "Ao Iniciar uma Rejei��o", 0);
         cmbCheck_Momento.addItem("6", "Ao Terminar uma Rejei��o", 0);
         cmbCheck_Momento.addItem("7", "Ao Enviar para o Backlog", 0);
         cmbCheck_Momento.addItem("8", "Ao Sair do Backlog", 0);
         cmbCheck_Momento.addItem("9", "Ao Iniciar uma An�lise", 0);
         cmbCheck_Momento.addItem("10", "Ao Terminar uma An�lise", 0);
         cmbCheck_Momento.addItem("11", "Ao Iniciar uma Execu��o", 0);
         cmbCheck_Momento.addItem("12", "Ao Terminar uma Execu��o", 0);
         cmbCheck_Momento.addItem("13", "Ao Acatar uma Diverg�ncia", 0);
         cmbCheck_Momento.addItem("14", "Ao Homologar uma demanda", 0);
         cmbCheck_Momento.addItem("15", "Ao Liquidar uma demanda", 0);
         cmbCheck_Momento.addItem("16", "Ao dar o Aceite de uma Demanda", 0);
         if ( cmbCheck_Momento.ItemCount > 0 )
         {
            A1843Check_Momento = (short)(NumberUtil.Val( cmbCheck_Momento.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1843Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)));
         }
         cmbCheckList_Obrigatorio.Name = "CHECKLIST_OBRIGATORIO";
         cmbCheckList_Obrigatorio.WebTags = "";
         cmbCheckList_Obrigatorio.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbCheckList_Obrigatorio.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbCheckList_Obrigatorio.ItemCount > 0 )
         {
            A1845CheckList_Obrigatorio = StringUtil.StrToBool( cmbCheckList_Obrigatorio.getValidValue(StringUtil.BoolToStr( A1845CheckList_Obrigatorio)));
            n1845CheckList_Obrigatorio = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1845CheckList_Obrigatorio", A1845CheckList_Obrigatorio);
         }
         cmbCheckList_Impeditivo.Name = "CHECKLIST_IMPEDITIVO";
         cmbCheckList_Impeditivo.WebTags = "";
         cmbCheckList_Impeditivo.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbCheckList_Impeditivo.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbCheckList_Impeditivo.ItemCount > 0 )
         {
            A1850CheckList_Impeditivo = StringUtil.StrToBool( cmbCheckList_Impeditivo.getValidValue(StringUtil.BoolToStr( A1850CheckList_Impeditivo)));
            n1850CheckList_Impeditivo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1850CheckList_Impeditivo", A1850CheckList_Impeditivo);
         }
         cmbCheckList_SeNaoCumpre.Name = "CHECKLIST_SENAOCUMPRE";
         cmbCheckList_SeNaoCumpre.WebTags = "";
         cmbCheckList_SeNaoCumpre.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhuma)", 0);
         cmbCheckList_SeNaoCumpre.addItem("1", "Retorna", 0);
         if ( cmbCheckList_SeNaoCumpre.ItemCount > 0 )
         {
            A1848CheckList_SeNaoCumpre = (short)(NumberUtil.Val( cmbCheckList_SeNaoCumpre.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0))), "."));
            n1848CheckList_SeNaoCumpre = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1848CheckList_SeNaoCumpre", StringUtil.LTrim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0)));
         }
         dynCheckList_NaoCnfCod.Name = "CHECKLIST_NAOCNFCOD";
         dynCheckList_NaoCnfCod.WebTags = "";
         cmbCheckList_NaoCnfQdo.Name = "CHECKLIST_NAOCNFQDO";
         cmbCheckList_NaoCnfQdo.WebTags = "";
         cmbCheckList_NaoCnfQdo.addItem("", "---", 0);
         cmbCheckList_NaoCnfQdo.addItem("S", "SIM", 0);
         cmbCheckList_NaoCnfQdo.addItem("N", "N�O", 0);
         if ( cmbCheckList_NaoCnfQdo.ItemCount > 0 )
         {
            A1851CheckList_NaoCnfQdo = cmbCheckList_NaoCnfQdo.getValidValue(A1851CheckList_NaoCnfQdo);
            n1851CheckList_NaoCnfQdo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1851CheckList_NaoCnfQdo", A1851CheckList_NaoCnfQdo);
         }
         chkCheckList_Ativo.Name = "CHECKLIST_ATIVO";
         chkCheckList_Ativo.WebTags = "";
         chkCheckList_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkCheckList_Ativo_Internalname, "TitleCaption", chkCheckList_Ativo.Caption);
         chkCheckList_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Check List", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynCheck_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public checklist( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public checklist( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_CheckList_Codigo ,
                           ref int aP2_Check_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7CheckList_Codigo = aP1_CheckList_Codigo;
         this.AV9Check_Codigo = aP2_Check_Codigo;
         executePrivate();
         aP2_Check_Codigo=this.AV9Check_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynCheck_Codigo = new GXCombobox();
         cmbCheck_Momento = new GXCombobox();
         cmbCheckList_Obrigatorio = new GXCombobox();
         cmbCheckList_Impeditivo = new GXCombobox();
         cmbCheckList_SeNaoCumpre = new GXCombobox();
         dynCheckList_NaoCnfCod = new GXCombobox();
         cmbCheckList_NaoCnfQdo = new GXCombobox();
         chkCheckList_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynCheck_Codigo.ItemCount > 0 )
         {
            A1839Check_Codigo = (int)(NumberUtil.Val( dynCheck_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0))), "."));
            n1839Check_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
         }
         if ( cmbCheck_Momento.ItemCount > 0 )
         {
            A1843Check_Momento = (short)(NumberUtil.Val( cmbCheck_Momento.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1843Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)));
         }
         if ( cmbCheckList_Obrigatorio.ItemCount > 0 )
         {
            A1845CheckList_Obrigatorio = StringUtil.StrToBool( cmbCheckList_Obrigatorio.getValidValue(StringUtil.BoolToStr( A1845CheckList_Obrigatorio)));
            n1845CheckList_Obrigatorio = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1845CheckList_Obrigatorio", A1845CheckList_Obrigatorio);
         }
         if ( cmbCheckList_Impeditivo.ItemCount > 0 )
         {
            A1850CheckList_Impeditivo = StringUtil.StrToBool( cmbCheckList_Impeditivo.getValidValue(StringUtil.BoolToStr( A1850CheckList_Impeditivo)));
            n1850CheckList_Impeditivo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1850CheckList_Impeditivo", A1850CheckList_Impeditivo);
         }
         if ( cmbCheckList_SeNaoCumpre.ItemCount > 0 )
         {
            A1848CheckList_SeNaoCumpre = (short)(NumberUtil.Val( cmbCheckList_SeNaoCumpre.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0))), "."));
            n1848CheckList_SeNaoCumpre = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1848CheckList_SeNaoCumpre", StringUtil.LTrim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0)));
         }
         if ( dynCheckList_NaoCnfCod.ItemCount > 0 )
         {
            A1846CheckList_NaoCnfCod = (int)(NumberUtil.Val( dynCheckList_NaoCnfCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0))), "."));
            n1846CheckList_NaoCnfCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1846CheckList_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0)));
         }
         if ( cmbCheckList_NaoCnfQdo.ItemCount > 0 )
         {
            A1851CheckList_NaoCnfQdo = cmbCheckList_NaoCnfQdo.getValidValue(A1851CheckList_NaoCnfQdo);
            n1851CheckList_NaoCnfQdo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1851CheckList_NaoCnfQdo", A1851CheckList_NaoCnfQdo);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2H96( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2H96e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtCheckList_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A758CheckList_Codigo), 6, 0, ",", "")), ((edtCheckList_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A758CheckList_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A758CheckList_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCheckList_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtCheckList_Codigo_Visible, edtCheckList_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_CheckList.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2H96( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2H96( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2H96e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_60_2H96( true) ;
         }
         return  ;
      }

      protected void wb_table3_60_2H96e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2H96e( true) ;
         }
         else
         {
            wb_table1_2_2H96e( false) ;
         }
      }

      protected void wb_table3_60_2H96( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_60_2H96e( true) ;
         }
         else
         {
            wb_table3_60_2H96e( false) ;
         }
      }

      protected void wb_table2_5_2H96( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2H96( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2H96e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2H96e( true) ;
         }
         else
         {
            wb_table2_5_2H96e( false) ;
         }
      }

      protected void wb_table4_13_2H96( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcheck_codigo_Internalname, "Check List", "", "", lblTextblockcheck_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynCheck_Codigo, dynCheck_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)), 1, dynCheck_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynCheck_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_CheckList.htm");
            dynCheck_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynCheck_Codigo_Internalname, "Values", (String)(dynCheck_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcheck_momento_Internalname, "Momento", "", "", lblTextblockcheck_momento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbCheck_Momento, cmbCheck_Momento_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)), 1, cmbCheck_Momento_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbCheck_Momento.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_CheckList.htm");
            cmbCheck_Momento.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheck_Momento_Internalname, "Values", (String)(cmbCheck_Momento.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockchecklist_descricao_Internalname, "Descri��o", "", "", lblTextblockchecklist_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtCheckList_Descricao_Internalname, A763CheckList_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", 0, 1, edtCheckList_Descricao_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockchecklist_obrigatorio_Internalname, "Obrigat�rio", "", "", lblTextblockchecklist_obrigatorio_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbCheckList_Obrigatorio, cmbCheckList_Obrigatorio_Internalname, StringUtil.BoolToStr( A1845CheckList_Obrigatorio), 1, cmbCheckList_Obrigatorio_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbCheckList_Obrigatorio.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "", true, "HLP_CheckList.htm");
            cmbCheckList_Obrigatorio.CurrentValue = StringUtil.BoolToStr( A1845CheckList_Obrigatorio);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_Obrigatorio_Internalname, "Values", (String)(cmbCheckList_Obrigatorio.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockchecklist_impeditivo_cell_Internalname+"\"  class='"+cellTextblockchecklist_impeditivo_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockchecklist_impeditivo_Internalname, "Impeditivo", "", "", lblTextblockchecklist_impeditivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellChecklist_impeditivo_cell_Internalname+"\"  class='"+cellChecklist_impeditivo_cell_Class+"'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbCheckList_Impeditivo, cmbCheckList_Impeditivo_Internalname, StringUtil.BoolToStr( A1850CheckList_Impeditivo), 1, cmbCheckList_Impeditivo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", cmbCheckList_Impeditivo.Visible, cmbCheckList_Impeditivo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_CheckList.htm");
            cmbCheckList_Impeditivo.CurrentValue = StringUtil.BoolToStr( A1850CheckList_Impeditivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_Impeditivo_Internalname, "Values", (String)(cmbCheckList_Impeditivo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockchecklist_senaocumpre_cell_Internalname+"\"  class='"+cellTextblockchecklist_senaocumpre_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockchecklist_senaocumpre_Internalname, "A��o", "", "", lblTextblockchecklist_senaocumpre_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellChecklist_senaocumpre_cell_Internalname+"\"  class='"+cellChecklist_senaocumpre_cell_Class+"'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbCheckList_SeNaoCumpre, cmbCheckList_SeNaoCumpre_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0)), 1, cmbCheckList_SeNaoCumpre_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbCheckList_SeNaoCumpre.Visible, cmbCheckList_SeNaoCumpre.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_CheckList.htm");
            cmbCheckList_SeNaoCumpre.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_SeNaoCumpre_Internalname, "Values", (String)(cmbCheckList_SeNaoCumpre.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockchecklist_naocnfcod_cell_Internalname+"\"  class='"+cellTextblockchecklist_naocnfcod_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockchecklist_naocnfcod_Internalname, "N�o Conformidade", "", "", lblTextblockchecklist_naocnfcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellChecklist_naocnfcod_cell_Internalname+"\"  class='"+cellChecklist_naocnfcod_cell_Class+"'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynCheckList_NaoCnfCod, dynCheckList_NaoCnfCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0)), 1, dynCheckList_NaoCnfCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynCheckList_NaoCnfCod.Visible, dynCheckList_NaoCnfCod.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_CheckList.htm");
            dynCheckList_NaoCnfCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynCheckList_NaoCnfCod_Internalname, "Values", (String)(dynCheckList_NaoCnfCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockchecklist_naocnfqdo_cell_Internalname+"\"  class='"+cellTextblockchecklist_naocnfqdo_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockchecklist_naocnfqdo_Internalname, "Quando", "", "", lblTextblockchecklist_naocnfqdo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellChecklist_naocnfqdo_cell_Internalname+"\"  class='"+cellChecklist_naocnfqdo_cell_Class+"'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbCheckList_NaoCnfQdo, cmbCheckList_NaoCnfQdo_Internalname, StringUtil.RTrim( A1851CheckList_NaoCnfQdo), 1, cmbCheckList_NaoCnfQdo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbCheckList_NaoCnfQdo.Visible, cmbCheckList_NaoCnfQdo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_CheckList.htm");
            cmbCheckList_NaoCnfQdo.CurrentValue = StringUtil.RTrim( A1851CheckList_NaoCnfQdo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_NaoCnfQdo_Internalname, "Values", (String)(cmbCheckList_NaoCnfQdo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockchecklist_ativo_Internalname, "Ativo?", "", "", lblTextblockchecklist_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockchecklist_ativo_Visible, 1, 0, "HLP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkCheckList_Ativo_Internalname, StringUtil.BoolToStr( A1151CheckList_Ativo), "", "", chkCheckList_Ativo.Visible, chkCheckList_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(55, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2H96e( true) ;
         }
         else
         {
            wb_table4_13_2H96e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112H2 */
         E112H2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynCheck_Codigo.CurrentValue = cgiGet( dynCheck_Codigo_Internalname);
               A1839Check_Codigo = (int)(NumberUtil.Val( cgiGet( dynCheck_Codigo_Internalname), "."));
               n1839Check_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
               n1839Check_Codigo = ((0==A1839Check_Codigo) ? true : false);
               cmbCheck_Momento.CurrentValue = cgiGet( cmbCheck_Momento_Internalname);
               A1843Check_Momento = (short)(NumberUtil.Val( cgiGet( cmbCheck_Momento_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1843Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)));
               A763CheckList_Descricao = cgiGet( edtCheckList_Descricao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A763CheckList_Descricao", A763CheckList_Descricao);
               cmbCheckList_Obrigatorio.CurrentValue = cgiGet( cmbCheckList_Obrigatorio_Internalname);
               A1845CheckList_Obrigatorio = StringUtil.StrToBool( cgiGet( cmbCheckList_Obrigatorio_Internalname));
               n1845CheckList_Obrigatorio = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1845CheckList_Obrigatorio", A1845CheckList_Obrigatorio);
               n1845CheckList_Obrigatorio = ((false==A1845CheckList_Obrigatorio) ? true : false);
               cmbCheckList_Impeditivo.CurrentValue = cgiGet( cmbCheckList_Impeditivo_Internalname);
               A1850CheckList_Impeditivo = StringUtil.StrToBool( cgiGet( cmbCheckList_Impeditivo_Internalname));
               n1850CheckList_Impeditivo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1850CheckList_Impeditivo", A1850CheckList_Impeditivo);
               n1850CheckList_Impeditivo = ((false==A1850CheckList_Impeditivo) ? true : false);
               cmbCheckList_SeNaoCumpre.CurrentValue = cgiGet( cmbCheckList_SeNaoCumpre_Internalname);
               A1848CheckList_SeNaoCumpre = (short)(NumberUtil.Val( cgiGet( cmbCheckList_SeNaoCumpre_Internalname), "."));
               n1848CheckList_SeNaoCumpre = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1848CheckList_SeNaoCumpre", StringUtil.LTrim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0)));
               n1848CheckList_SeNaoCumpre = ((0==A1848CheckList_SeNaoCumpre) ? true : false);
               dynCheckList_NaoCnfCod.CurrentValue = cgiGet( dynCheckList_NaoCnfCod_Internalname);
               A1846CheckList_NaoCnfCod = (int)(NumberUtil.Val( cgiGet( dynCheckList_NaoCnfCod_Internalname), "."));
               n1846CheckList_NaoCnfCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1846CheckList_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0)));
               n1846CheckList_NaoCnfCod = ((0==A1846CheckList_NaoCnfCod) ? true : false);
               cmbCheckList_NaoCnfQdo.CurrentValue = cgiGet( cmbCheckList_NaoCnfQdo_Internalname);
               A1851CheckList_NaoCnfQdo = cgiGet( cmbCheckList_NaoCnfQdo_Internalname);
               n1851CheckList_NaoCnfQdo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1851CheckList_NaoCnfQdo", A1851CheckList_NaoCnfQdo);
               n1851CheckList_NaoCnfQdo = (String.IsNullOrEmpty(StringUtil.RTrim( A1851CheckList_NaoCnfQdo)) ? true : false);
               A1151CheckList_Ativo = StringUtil.StrToBool( cgiGet( chkCheckList_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1151CheckList_Ativo", A1151CheckList_Ativo);
               A758CheckList_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCheckList_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
               /* Read saved values. */
               Z758CheckList_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z758CheckList_Codigo"), ",", "."));
               Z1230CheckList_De = (short)(context.localUtil.CToN( cgiGet( "Z1230CheckList_De"), ",", "."));
               n1230CheckList_De = ((0==A1230CheckList_De) ? true : false);
               Z1845CheckList_Obrigatorio = StringUtil.StrToBool( cgiGet( "Z1845CheckList_Obrigatorio"));
               n1845CheckList_Obrigatorio = ((false==A1845CheckList_Obrigatorio) ? true : false);
               Z1850CheckList_Impeditivo = StringUtil.StrToBool( cgiGet( "Z1850CheckList_Impeditivo"));
               n1850CheckList_Impeditivo = ((false==A1850CheckList_Impeditivo) ? true : false);
               Z1848CheckList_SeNaoCumpre = (short)(context.localUtil.CToN( cgiGet( "Z1848CheckList_SeNaoCumpre"), ",", "."));
               n1848CheckList_SeNaoCumpre = ((0==A1848CheckList_SeNaoCumpre) ? true : false);
               Z1846CheckList_NaoCnfCod = (int)(context.localUtil.CToN( cgiGet( "Z1846CheckList_NaoCnfCod"), ",", "."));
               n1846CheckList_NaoCnfCod = ((0==A1846CheckList_NaoCnfCod) ? true : false);
               Z1851CheckList_NaoCnfQdo = cgiGet( "Z1851CheckList_NaoCnfQdo");
               n1851CheckList_NaoCnfQdo = (String.IsNullOrEmpty(StringUtil.RTrim( A1851CheckList_NaoCnfQdo)) ? true : false);
               Z1151CheckList_Ativo = StringUtil.StrToBool( cgiGet( "Z1151CheckList_Ativo"));
               Z1839Check_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1839Check_Codigo"), ",", "."));
               n1839Check_Codigo = ((0==A1839Check_Codigo) ? true : false);
               A1230CheckList_De = (short)(context.localUtil.CToN( cgiGet( "Z1230CheckList_De"), ",", "."));
               n1230CheckList_De = false;
               n1230CheckList_De = ((0==A1230CheckList_De) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N1839Check_Codigo = (int)(context.localUtil.CToN( cgiGet( "N1839Check_Codigo"), ",", "."));
               n1839Check_Codigo = ((0==A1839Check_Codigo) ? true : false);
               AV7CheckList_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCHECKLIST_CODIGO"), ",", "."));
               AV12Insert_Check_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CHECK_CODIGO"), ",", "."));
               AV9Check_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCHECK_CODIGO"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               ajax_req_read_hidden_sdt(cgiGet( "vWWPCONTEXT"), AV8WWPContext);
               A1230CheckList_De = (short)(context.localUtil.CToN( cgiGet( "CHECKLIST_DE"), ",", "."));
               n1230CheckList_De = ((0==A1230CheckList_De) ? true : false);
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "CheckList";
               A758CheckList_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCheckList_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A758CheckList_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1230CheckList_De), "ZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A758CheckList_Codigo != Z758CheckList_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("checklist:[SecurityCheckFailed value for]"+"CheckList_Codigo:"+context.localUtil.Format( (decimal)(A758CheckList_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("checklist:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("checklist:[SecurityCheckFailed value for]"+"CheckList_De:"+context.localUtil.Format( (decimal)(A1230CheckList_De), "ZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A758CheckList_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode96 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode96;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound96 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_2H0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CHECKLIST_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtCheckList_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112H2 */
                           E112H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122H2 */
                           E122H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E122H2 */
            E122H2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2H96( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2H96( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_2H0( )
      {
         BeforeValidate2H96( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2H96( ) ;
            }
            else
            {
               CheckExtendedTable2H96( ) ;
               CloseExtendedTableCursors2H96( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption2H0( )
      {
      }

      protected void E112H2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV10TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV10TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV10TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Check_Codigo") == 0 )
               {
                  AV12Insert_Check_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Check_Codigo), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
         edtCheckList_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheckList_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCheckList_Codigo_Visible), 5, 0)));
      }

      protected void E122H2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV10TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwchecklist.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)AV9Check_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S112( )
      {
         /* 'ATTRIBUTESSECURITYCODE' Routine */
         cmbCheckList_Impeditivo.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_Impeditivo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_Impeditivo.Visible), 5, 0)));
         cellChecklist_impeditivo_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_impeditivo_cell_Internalname, "Class", cellChecklist_impeditivo_cell_Class);
         cellTextblockchecklist_impeditivo_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_impeditivo_cell_Internalname, "Class", cellTextblockchecklist_impeditivo_cell_Class);
         cmbCheckList_SeNaoCumpre.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_SeNaoCumpre_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_SeNaoCumpre.Visible), 5, 0)));
         cellChecklist_senaocumpre_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_senaocumpre_cell_Internalname, "Class", cellChecklist_senaocumpre_cell_Class);
         cellTextblockchecklist_senaocumpre_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_senaocumpre_cell_Internalname, "Class", cellTextblockchecklist_senaocumpre_cell_Class);
         dynCheckList_NaoCnfCod.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynCheckList_NaoCnfCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynCheckList_NaoCnfCod.Visible), 5, 0)));
         cellChecklist_naocnfcod_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfcod_cell_Internalname, "Class", cellChecklist_naocnfcod_cell_Class);
         cellTextblockchecklist_naocnfcod_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfcod_cell_Internalname, "Class", cellTextblockchecklist_naocnfcod_cell_Class);
         cmbCheckList_NaoCnfQdo.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_NaoCnfQdo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_NaoCnfQdo.Visible), 5, 0)));
         cellChecklist_naocnfqdo_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfqdo_cell_Internalname, "Class", cellChecklist_naocnfqdo_cell_Class);
         cellTextblockchecklist_naocnfqdo_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfqdo_cell_Internalname, "Class", cellTextblockchecklist_naocnfqdo_cell_Class);
      }

      protected void ZM2H96( short GX_JID )
      {
         if ( ( GX_JID == 31 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1230CheckList_De = T002H3_A1230CheckList_De[0];
               Z1845CheckList_Obrigatorio = T002H3_A1845CheckList_Obrigatorio[0];
               Z1850CheckList_Impeditivo = T002H3_A1850CheckList_Impeditivo[0];
               Z1848CheckList_SeNaoCumpre = T002H3_A1848CheckList_SeNaoCumpre[0];
               Z1846CheckList_NaoCnfCod = T002H3_A1846CheckList_NaoCnfCod[0];
               Z1851CheckList_NaoCnfQdo = T002H3_A1851CheckList_NaoCnfQdo[0];
               Z1151CheckList_Ativo = T002H3_A1151CheckList_Ativo[0];
               Z1839Check_Codigo = T002H3_A1839Check_Codigo[0];
            }
            else
            {
               Z1230CheckList_De = A1230CheckList_De;
               Z1845CheckList_Obrigatorio = A1845CheckList_Obrigatorio;
               Z1850CheckList_Impeditivo = A1850CheckList_Impeditivo;
               Z1848CheckList_SeNaoCumpre = A1848CheckList_SeNaoCumpre;
               Z1846CheckList_NaoCnfCod = A1846CheckList_NaoCnfCod;
               Z1851CheckList_NaoCnfQdo = A1851CheckList_NaoCnfQdo;
               Z1151CheckList_Ativo = A1151CheckList_Ativo;
               Z1839Check_Codigo = A1839Check_Codigo;
            }
         }
         if ( GX_JID == -31 )
         {
            Z758CheckList_Codigo = A758CheckList_Codigo;
            Z763CheckList_Descricao = A763CheckList_Descricao;
            Z1230CheckList_De = A1230CheckList_De;
            Z1845CheckList_Obrigatorio = A1845CheckList_Obrigatorio;
            Z1850CheckList_Impeditivo = A1850CheckList_Impeditivo;
            Z1848CheckList_SeNaoCumpre = A1848CheckList_SeNaoCumpre;
            Z1846CheckList_NaoCnfCod = A1846CheckList_NaoCnfCod;
            Z1851CheckList_NaoCnfQdo = A1851CheckList_NaoCnfQdo;
            Z1151CheckList_Ativo = A1151CheckList_Ativo;
            Z1839Check_Codigo = A1839Check_Codigo;
            Z1843Check_Momento = A1843Check_Momento;
         }
      }

      protected void standaloneNotModal( )
      {
         cmbCheck_Momento.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheck_Momento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheck_Momento.Enabled), 5, 0)));
         edtCheckList_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheckList_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCheckList_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV14Pgmname = "CheckList";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         cmbCheck_Momento.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheck_Momento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheck_Momento.Enabled), 5, 0)));
         edtCheckList_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheckList_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCheckList_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7CheckList_Codigo) )
         {
            A758CheckList_Codigo = AV7CheckList_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
         }
         if ( AV9Check_Codigo > 0 )
         {
            dynCheck_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynCheck_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynCheck_Codigo.Enabled), 5, 0)));
         }
         GXACHECKLIST_NAOCNFCOD_html2H96( AV8WWPContext) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Check_Codigo) )
         {
            dynCheck_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynCheck_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynCheck_Codigo.Enabled), 5, 0)));
         }
         else
         {
            if ( AV9Check_Codigo > 0 )
            {
               dynCheck_Codigo.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynCheck_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynCheck_Codigo.Enabled), 5, 0)));
            }
            else
            {
               dynCheck_Codigo.Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynCheck_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynCheck_Codigo.Enabled), 5, 0)));
            }
         }
      }

      protected void standaloneModal( )
      {
         chkCheckList_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkCheckList_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkCheckList_Ativo.Visible), 5, 0)));
         lblTextblockchecklist_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockchecklist_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockchecklist_ativo_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Check_Codigo) )
         {
            A1839Check_Codigo = AV12Insert_Check_Codigo;
            n1839Check_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
         }
         else
         {
            if ( AV9Check_Codigo > 0 )
            {
               A1839Check_Codigo = AV9Check_Codigo;
               n1839Check_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
            }
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1151CheckList_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A1151CheckList_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1151CheckList_Ativo", A1151CheckList_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T002H4 */
            pr_default.execute(2, new Object[] {n1839Check_Codigo, A1839Check_Codigo});
            A1843Check_Momento = T002H4_A1843Check_Momento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1843Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)));
            pr_default.close(2);
            cmbCheckList_Impeditivo.Visible = ((A1843Check_Momento!=10)&&(A1843Check_Momento!=8)&&(A1843Check_Momento!=4)&&(A1843Check_Momento!=12)&&(A1843Check_Momento!=6) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_Impeditivo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_Impeditivo.Visible), 5, 0)));
            if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
            {
               cellChecklist_impeditivo_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_impeditivo_cell_Internalname, "Class", cellChecklist_impeditivo_cell_Class);
            }
            else
            {
               if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
               {
                  cellChecklist_impeditivo_cell_Class = "DataContentCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_impeditivo_cell_Internalname, "Class", cellChecklist_impeditivo_cell_Class);
               }
            }
            if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
            {
               cellTextblockchecklist_impeditivo_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_impeditivo_cell_Internalname, "Class", cellTextblockchecklist_impeditivo_cell_Class);
            }
            else
            {
               if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
               {
                  cellTextblockchecklist_impeditivo_cell_Class = "DataDescriptionCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_impeditivo_cell_Internalname, "Class", cellTextblockchecklist_impeditivo_cell_Class);
               }
            }
            dynCheckList_NaoCnfCod.Visible = ((A1843Check_Momento!=10)&&(A1843Check_Momento!=8)&&(A1843Check_Momento!=4)&&(A1843Check_Momento!=12)&&(A1843Check_Momento!=6) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynCheckList_NaoCnfCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynCheckList_NaoCnfCod.Visible), 5, 0)));
            if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
            {
               cellChecklist_naocnfcod_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfcod_cell_Internalname, "Class", cellChecklist_naocnfcod_cell_Class);
            }
            else
            {
               if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
               {
                  cellChecklist_naocnfcod_cell_Class = "DataContentCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfcod_cell_Internalname, "Class", cellChecklist_naocnfcod_cell_Class);
               }
            }
            if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
            {
               cellTextblockchecklist_naocnfcod_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfcod_cell_Internalname, "Class", cellTextblockchecklist_naocnfcod_cell_Class);
            }
            else
            {
               if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
               {
                  cellTextblockchecklist_naocnfcod_cell_Class = "DataDescriptionCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfcod_cell_Internalname, "Class", cellTextblockchecklist_naocnfcod_cell_Class);
               }
            }
            if ( ( A1843Check_Momento == 10 ) || ( A1843Check_Momento == 8 ) || ( A1843Check_Momento == 4 ) || ( A1843Check_Momento == 12 ) || ( A1843Check_Momento == 6 ) )
            {
               A1850CheckList_Impeditivo = false;
               n1850CheckList_Impeditivo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1850CheckList_Impeditivo", A1850CheckList_Impeditivo);
               n1850CheckList_Impeditivo = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1850CheckList_Impeditivo", A1850CheckList_Impeditivo);
            }
            cmbCheckList_SeNaoCumpre.Visible = (A1850CheckList_Impeditivo ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_SeNaoCumpre_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_SeNaoCumpre.Visible), 5, 0)));
            if ( ! ( A1850CheckList_Impeditivo ) )
            {
               cellChecklist_senaocumpre_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_senaocumpre_cell_Internalname, "Class", cellChecklist_senaocumpre_cell_Class);
            }
            else
            {
               if ( A1850CheckList_Impeditivo )
               {
                  cellChecklist_senaocumpre_cell_Class = "DataContentCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_senaocumpre_cell_Internalname, "Class", cellChecklist_senaocumpre_cell_Class);
               }
            }
            if ( ! ( A1850CheckList_Impeditivo ) )
            {
               cellTextblockchecklist_senaocumpre_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_senaocumpre_cell_Internalname, "Class", cellTextblockchecklist_senaocumpre_cell_Class);
            }
            else
            {
               if ( A1850CheckList_Impeditivo )
               {
                  cellTextblockchecklist_senaocumpre_cell_Class = "DataDescriptionCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_senaocumpre_cell_Internalname, "Class", cellTextblockchecklist_senaocumpre_cell_Class);
               }
            }
            if ( ! A1850CheckList_Impeditivo )
            {
               A1848CheckList_SeNaoCumpre = 0;
               n1848CheckList_SeNaoCumpre = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1848CheckList_SeNaoCumpre", StringUtil.LTrim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0)));
               n1848CheckList_SeNaoCumpre = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1848CheckList_SeNaoCumpre", StringUtil.LTrim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0)));
            }
            if ( ( A1843Check_Momento == 10 ) || ( A1843Check_Momento == 8 ) || ( A1843Check_Momento == 4 ) || ( A1843Check_Momento == 12 ) || ( A1843Check_Momento == 6 ) )
            {
               A1846CheckList_NaoCnfCod = 0;
               n1846CheckList_NaoCnfCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1846CheckList_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0)));
               n1846CheckList_NaoCnfCod = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1846CheckList_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0)));
            }
            cmbCheckList_NaoCnfQdo.Visible = ((A1846CheckList_NaoCnfCod>0) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_NaoCnfQdo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_NaoCnfQdo.Visible), 5, 0)));
            if ( ! ( ( A1846CheckList_NaoCnfCod > 0 ) ) )
            {
               cellChecklist_naocnfqdo_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfqdo_cell_Internalname, "Class", cellChecklist_naocnfqdo_cell_Class);
            }
            else
            {
               if ( A1846CheckList_NaoCnfCod > 0 )
               {
                  cellChecklist_naocnfqdo_cell_Class = "DataContentCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfqdo_cell_Internalname, "Class", cellChecklist_naocnfqdo_cell_Class);
               }
            }
            if ( ! ( ( A1846CheckList_NaoCnfCod > 0 ) ) )
            {
               cellTextblockchecklist_naocnfqdo_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfqdo_cell_Internalname, "Class", cellTextblockchecklist_naocnfqdo_cell_Class);
            }
            else
            {
               if ( A1846CheckList_NaoCnfCod > 0 )
               {
                  cellTextblockchecklist_naocnfqdo_cell_Class = "DataDescriptionCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfqdo_cell_Internalname, "Class", cellTextblockchecklist_naocnfqdo_cell_Class);
               }
            }
            if ( ! ( A1846CheckList_NaoCnfCod > 0 ) )
            {
               A1851CheckList_NaoCnfQdo = "";
               n1851CheckList_NaoCnfQdo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1851CheckList_NaoCnfQdo", A1851CheckList_NaoCnfQdo);
               n1851CheckList_NaoCnfQdo = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1851CheckList_NaoCnfQdo", A1851CheckList_NaoCnfQdo);
            }
         }
      }

      protected void Load2H96( )
      {
         /* Using cursor T002H6 */
         pr_default.execute(4, new Object[] {A758CheckList_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound96 = 1;
            A1843Check_Momento = T002H6_A1843Check_Momento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1843Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)));
            A763CheckList_Descricao = T002H6_A763CheckList_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A763CheckList_Descricao", A763CheckList_Descricao);
            A1230CheckList_De = T002H6_A1230CheckList_De[0];
            n1230CheckList_De = T002H6_n1230CheckList_De[0];
            A1845CheckList_Obrigatorio = T002H6_A1845CheckList_Obrigatorio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1845CheckList_Obrigatorio", A1845CheckList_Obrigatorio);
            n1845CheckList_Obrigatorio = T002H6_n1845CheckList_Obrigatorio[0];
            A1850CheckList_Impeditivo = T002H6_A1850CheckList_Impeditivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1850CheckList_Impeditivo", A1850CheckList_Impeditivo);
            n1850CheckList_Impeditivo = T002H6_n1850CheckList_Impeditivo[0];
            A1848CheckList_SeNaoCumpre = T002H6_A1848CheckList_SeNaoCumpre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1848CheckList_SeNaoCumpre", StringUtil.LTrim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0)));
            n1848CheckList_SeNaoCumpre = T002H6_n1848CheckList_SeNaoCumpre[0];
            A1846CheckList_NaoCnfCod = T002H6_A1846CheckList_NaoCnfCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1846CheckList_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0)));
            n1846CheckList_NaoCnfCod = T002H6_n1846CheckList_NaoCnfCod[0];
            A1851CheckList_NaoCnfQdo = T002H6_A1851CheckList_NaoCnfQdo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1851CheckList_NaoCnfQdo", A1851CheckList_NaoCnfQdo);
            n1851CheckList_NaoCnfQdo = T002H6_n1851CheckList_NaoCnfQdo[0];
            A1151CheckList_Ativo = T002H6_A1151CheckList_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1151CheckList_Ativo", A1151CheckList_Ativo);
            A1839Check_Codigo = T002H6_A1839Check_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
            n1839Check_Codigo = T002H6_n1839Check_Codigo[0];
            ZM2H96( -31) ;
         }
         pr_default.close(4);
         OnLoadActions2H96( ) ;
      }

      protected void OnLoadActions2H96( )
      {
         cmbCheckList_Impeditivo.Visible = ((A1843Check_Momento!=10)&&(A1843Check_Momento!=8)&&(A1843Check_Momento!=4)&&(A1843Check_Momento!=12)&&(A1843Check_Momento!=6) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_Impeditivo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_Impeditivo.Visible), 5, 0)));
         if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
         {
            cellChecklist_impeditivo_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_impeditivo_cell_Internalname, "Class", cellChecklist_impeditivo_cell_Class);
         }
         else
         {
            if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
            {
               cellChecklist_impeditivo_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_impeditivo_cell_Internalname, "Class", cellChecklist_impeditivo_cell_Class);
            }
         }
         if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
         {
            cellTextblockchecklist_impeditivo_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_impeditivo_cell_Internalname, "Class", cellTextblockchecklist_impeditivo_cell_Class);
         }
         else
         {
            if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
            {
               cellTextblockchecklist_impeditivo_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_impeditivo_cell_Internalname, "Class", cellTextblockchecklist_impeditivo_cell_Class);
            }
         }
         dynCheckList_NaoCnfCod.Visible = ((A1843Check_Momento!=10)&&(A1843Check_Momento!=8)&&(A1843Check_Momento!=4)&&(A1843Check_Momento!=12)&&(A1843Check_Momento!=6) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynCheckList_NaoCnfCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynCheckList_NaoCnfCod.Visible), 5, 0)));
         if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
         {
            cellChecklist_naocnfcod_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfcod_cell_Internalname, "Class", cellChecklist_naocnfcod_cell_Class);
         }
         else
         {
            if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
            {
               cellChecklist_naocnfcod_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfcod_cell_Internalname, "Class", cellChecklist_naocnfcod_cell_Class);
            }
         }
         if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
         {
            cellTextblockchecklist_naocnfcod_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfcod_cell_Internalname, "Class", cellTextblockchecklist_naocnfcod_cell_Class);
         }
         else
         {
            if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
            {
               cellTextblockchecklist_naocnfcod_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfcod_cell_Internalname, "Class", cellTextblockchecklist_naocnfcod_cell_Class);
            }
         }
         if ( ( A1843Check_Momento == 10 ) || ( A1843Check_Momento == 8 ) || ( A1843Check_Momento == 4 ) || ( A1843Check_Momento == 12 ) || ( A1843Check_Momento == 6 ) )
         {
            A1850CheckList_Impeditivo = false;
            n1850CheckList_Impeditivo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1850CheckList_Impeditivo", A1850CheckList_Impeditivo);
            n1850CheckList_Impeditivo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1850CheckList_Impeditivo", A1850CheckList_Impeditivo);
         }
         if ( ( A1843Check_Momento == 10 ) || ( A1843Check_Momento == 8 ) || ( A1843Check_Momento == 4 ) || ( A1843Check_Momento == 12 ) || ( A1843Check_Momento == 6 ) )
         {
            A1846CheckList_NaoCnfCod = 0;
            n1846CheckList_NaoCnfCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1846CheckList_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0)));
            n1846CheckList_NaoCnfCod = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1846CheckList_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0)));
         }
         cmbCheckList_SeNaoCumpre.Visible = (A1850CheckList_Impeditivo ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_SeNaoCumpre_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_SeNaoCumpre.Visible), 5, 0)));
         if ( ! ( A1850CheckList_Impeditivo ) )
         {
            cellChecklist_senaocumpre_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_senaocumpre_cell_Internalname, "Class", cellChecklist_senaocumpre_cell_Class);
         }
         else
         {
            if ( A1850CheckList_Impeditivo )
            {
               cellChecklist_senaocumpre_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_senaocumpre_cell_Internalname, "Class", cellChecklist_senaocumpre_cell_Class);
            }
         }
         if ( ! ( A1850CheckList_Impeditivo ) )
         {
            cellTextblockchecklist_senaocumpre_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_senaocumpre_cell_Internalname, "Class", cellTextblockchecklist_senaocumpre_cell_Class);
         }
         else
         {
            if ( A1850CheckList_Impeditivo )
            {
               cellTextblockchecklist_senaocumpre_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_senaocumpre_cell_Internalname, "Class", cellTextblockchecklist_senaocumpre_cell_Class);
            }
         }
         if ( ! A1850CheckList_Impeditivo )
         {
            A1848CheckList_SeNaoCumpre = 0;
            n1848CheckList_SeNaoCumpre = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1848CheckList_SeNaoCumpre", StringUtil.LTrim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0)));
            n1848CheckList_SeNaoCumpre = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1848CheckList_SeNaoCumpre", StringUtil.LTrim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0)));
         }
         cmbCheckList_NaoCnfQdo.Visible = ((A1846CheckList_NaoCnfCod>0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_NaoCnfQdo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_NaoCnfQdo.Visible), 5, 0)));
         if ( ! ( ( A1846CheckList_NaoCnfCod > 0 ) ) )
         {
            cellChecklist_naocnfqdo_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfqdo_cell_Internalname, "Class", cellChecklist_naocnfqdo_cell_Class);
         }
         else
         {
            if ( A1846CheckList_NaoCnfCod > 0 )
            {
               cellChecklist_naocnfqdo_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfqdo_cell_Internalname, "Class", cellChecklist_naocnfqdo_cell_Class);
            }
         }
         if ( ! ( ( A1846CheckList_NaoCnfCod > 0 ) ) )
         {
            cellTextblockchecklist_naocnfqdo_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfqdo_cell_Internalname, "Class", cellTextblockchecklist_naocnfqdo_cell_Class);
         }
         else
         {
            if ( A1846CheckList_NaoCnfCod > 0 )
            {
               cellTextblockchecklist_naocnfqdo_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfqdo_cell_Internalname, "Class", cellTextblockchecklist_naocnfqdo_cell_Class);
            }
         }
         if ( ! ( A1846CheckList_NaoCnfCod > 0 ) )
         {
            A1851CheckList_NaoCnfQdo = "";
            n1851CheckList_NaoCnfQdo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1851CheckList_NaoCnfQdo", A1851CheckList_NaoCnfQdo);
            n1851CheckList_NaoCnfQdo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1851CheckList_NaoCnfQdo", A1851CheckList_NaoCnfQdo);
         }
      }

      protected void CheckExtendedTable2H96( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T002H4 */
         pr_default.execute(2, new Object[] {n1839Check_Codigo, A1839Check_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A1839Check_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe ' T204'.", "ForeignKeyNotFound", 1, "CHECK_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynCheck_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A1843Check_Momento = T002H4_A1843Check_Momento[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1843Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)));
         pr_default.close(2);
         cmbCheckList_Impeditivo.Visible = ((A1843Check_Momento!=10)&&(A1843Check_Momento!=8)&&(A1843Check_Momento!=4)&&(A1843Check_Momento!=12)&&(A1843Check_Momento!=6) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_Impeditivo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_Impeditivo.Visible), 5, 0)));
         if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
         {
            cellChecklist_impeditivo_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_impeditivo_cell_Internalname, "Class", cellChecklist_impeditivo_cell_Class);
         }
         else
         {
            if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
            {
               cellChecklist_impeditivo_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_impeditivo_cell_Internalname, "Class", cellChecklist_impeditivo_cell_Class);
            }
         }
         if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
         {
            cellTextblockchecklist_impeditivo_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_impeditivo_cell_Internalname, "Class", cellTextblockchecklist_impeditivo_cell_Class);
         }
         else
         {
            if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
            {
               cellTextblockchecklist_impeditivo_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_impeditivo_cell_Internalname, "Class", cellTextblockchecklist_impeditivo_cell_Class);
            }
         }
         dynCheckList_NaoCnfCod.Visible = ((A1843Check_Momento!=10)&&(A1843Check_Momento!=8)&&(A1843Check_Momento!=4)&&(A1843Check_Momento!=12)&&(A1843Check_Momento!=6) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynCheckList_NaoCnfCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynCheckList_NaoCnfCod.Visible), 5, 0)));
         if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
         {
            cellChecklist_naocnfcod_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfcod_cell_Internalname, "Class", cellChecklist_naocnfcod_cell_Class);
         }
         else
         {
            if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
            {
               cellChecklist_naocnfcod_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfcod_cell_Internalname, "Class", cellChecklist_naocnfcod_cell_Class);
            }
         }
         if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
         {
            cellTextblockchecklist_naocnfcod_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfcod_cell_Internalname, "Class", cellTextblockchecklist_naocnfcod_cell_Class);
         }
         else
         {
            if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
            {
               cellTextblockchecklist_naocnfcod_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfcod_cell_Internalname, "Class", cellTextblockchecklist_naocnfcod_cell_Class);
            }
         }
         if ( ( A1843Check_Momento == 10 ) || ( A1843Check_Momento == 8 ) || ( A1843Check_Momento == 4 ) || ( A1843Check_Momento == 12 ) || ( A1843Check_Momento == 6 ) )
         {
            A1850CheckList_Impeditivo = false;
            n1850CheckList_Impeditivo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1850CheckList_Impeditivo", A1850CheckList_Impeditivo);
            n1850CheckList_Impeditivo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1850CheckList_Impeditivo", A1850CheckList_Impeditivo);
         }
         if ( ( A1843Check_Momento == 10 ) || ( A1843Check_Momento == 8 ) || ( A1843Check_Momento == 4 ) || ( A1843Check_Momento == 12 ) || ( A1843Check_Momento == 6 ) )
         {
            A1846CheckList_NaoCnfCod = 0;
            n1846CheckList_NaoCnfCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1846CheckList_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0)));
            n1846CheckList_NaoCnfCod = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1846CheckList_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0)));
         }
         cmbCheckList_SeNaoCumpre.Visible = (A1850CheckList_Impeditivo ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_SeNaoCumpre_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_SeNaoCumpre.Visible), 5, 0)));
         if ( ! ( A1850CheckList_Impeditivo ) )
         {
            cellChecklist_senaocumpre_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_senaocumpre_cell_Internalname, "Class", cellChecklist_senaocumpre_cell_Class);
         }
         else
         {
            if ( A1850CheckList_Impeditivo )
            {
               cellChecklist_senaocumpre_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_senaocumpre_cell_Internalname, "Class", cellChecklist_senaocumpre_cell_Class);
            }
         }
         if ( ! ( A1850CheckList_Impeditivo ) )
         {
            cellTextblockchecklist_senaocumpre_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_senaocumpre_cell_Internalname, "Class", cellTextblockchecklist_senaocumpre_cell_Class);
         }
         else
         {
            if ( A1850CheckList_Impeditivo )
            {
               cellTextblockchecklist_senaocumpre_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_senaocumpre_cell_Internalname, "Class", cellTextblockchecklist_senaocumpre_cell_Class);
            }
         }
         if ( ! A1850CheckList_Impeditivo )
         {
            A1848CheckList_SeNaoCumpre = 0;
            n1848CheckList_SeNaoCumpre = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1848CheckList_SeNaoCumpre", StringUtil.LTrim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0)));
            n1848CheckList_SeNaoCumpre = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1848CheckList_SeNaoCumpre", StringUtil.LTrim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0)));
         }
         cmbCheckList_NaoCnfQdo.Visible = ((A1846CheckList_NaoCnfCod>0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_NaoCnfQdo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_NaoCnfQdo.Visible), 5, 0)));
         if ( ! ( ( A1846CheckList_NaoCnfCod > 0 ) ) )
         {
            cellChecklist_naocnfqdo_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfqdo_cell_Internalname, "Class", cellChecklist_naocnfqdo_cell_Class);
         }
         else
         {
            if ( A1846CheckList_NaoCnfCod > 0 )
            {
               cellChecklist_naocnfqdo_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfqdo_cell_Internalname, "Class", cellChecklist_naocnfqdo_cell_Class);
            }
         }
         if ( ! ( ( A1846CheckList_NaoCnfCod > 0 ) ) )
         {
            cellTextblockchecklist_naocnfqdo_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfqdo_cell_Internalname, "Class", cellTextblockchecklist_naocnfqdo_cell_Class);
         }
         else
         {
            if ( A1846CheckList_NaoCnfCod > 0 )
            {
               cellTextblockchecklist_naocnfqdo_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfqdo_cell_Internalname, "Class", cellTextblockchecklist_naocnfqdo_cell_Class);
            }
         }
         if ( ! ( A1846CheckList_NaoCnfCod > 0 ) )
         {
            A1851CheckList_NaoCnfQdo = "";
            n1851CheckList_NaoCnfQdo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1851CheckList_NaoCnfQdo", A1851CheckList_NaoCnfQdo);
            n1851CheckList_NaoCnfQdo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1851CheckList_NaoCnfQdo", A1851CheckList_NaoCnfQdo);
         }
         if ( ( A1846CheckList_NaoCnfCod > 0 ) && String.IsNullOrEmpty(StringUtil.RTrim( A1851CheckList_NaoCnfQdo)) )
         {
            GX_msglist.addItem("N�o Conformidade quando Sim ou N�o?", 1, "CHECKLIST_NAOCNFCOD");
            AnyError = 1;
            GX_FocusControl = dynCheckList_NaoCnfCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors2H96( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_32( int A1839Check_Codigo )
      {
         /* Using cursor T002H7 */
         pr_default.execute(5, new Object[] {n1839Check_Codigo, A1839Check_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A1839Check_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe ' T204'.", "ForeignKeyNotFound", 1, "CHECK_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynCheck_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A1843Check_Momento = T002H7_A1843Check_Momento[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1843Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1843Check_Momento), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void GetKey2H96( )
      {
         /* Using cursor T002H8 */
         pr_default.execute(6, new Object[] {A758CheckList_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound96 = 1;
         }
         else
         {
            RcdFound96 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002H3 */
         pr_default.execute(1, new Object[] {A758CheckList_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2H96( 31) ;
            RcdFound96 = 1;
            A758CheckList_Codigo = T002H3_A758CheckList_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
            A763CheckList_Descricao = T002H3_A763CheckList_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A763CheckList_Descricao", A763CheckList_Descricao);
            A1230CheckList_De = T002H3_A1230CheckList_De[0];
            n1230CheckList_De = T002H3_n1230CheckList_De[0];
            A1845CheckList_Obrigatorio = T002H3_A1845CheckList_Obrigatorio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1845CheckList_Obrigatorio", A1845CheckList_Obrigatorio);
            n1845CheckList_Obrigatorio = T002H3_n1845CheckList_Obrigatorio[0];
            A1850CheckList_Impeditivo = T002H3_A1850CheckList_Impeditivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1850CheckList_Impeditivo", A1850CheckList_Impeditivo);
            n1850CheckList_Impeditivo = T002H3_n1850CheckList_Impeditivo[0];
            A1848CheckList_SeNaoCumpre = T002H3_A1848CheckList_SeNaoCumpre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1848CheckList_SeNaoCumpre", StringUtil.LTrim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0)));
            n1848CheckList_SeNaoCumpre = T002H3_n1848CheckList_SeNaoCumpre[0];
            A1846CheckList_NaoCnfCod = T002H3_A1846CheckList_NaoCnfCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1846CheckList_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0)));
            n1846CheckList_NaoCnfCod = T002H3_n1846CheckList_NaoCnfCod[0];
            A1851CheckList_NaoCnfQdo = T002H3_A1851CheckList_NaoCnfQdo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1851CheckList_NaoCnfQdo", A1851CheckList_NaoCnfQdo);
            n1851CheckList_NaoCnfQdo = T002H3_n1851CheckList_NaoCnfQdo[0];
            A1151CheckList_Ativo = T002H3_A1151CheckList_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1151CheckList_Ativo", A1151CheckList_Ativo);
            A1839Check_Codigo = T002H3_A1839Check_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
            n1839Check_Codigo = T002H3_n1839Check_Codigo[0];
            Z758CheckList_Codigo = A758CheckList_Codigo;
            sMode96 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2H96( ) ;
            if ( AnyError == 1 )
            {
               RcdFound96 = 0;
               InitializeNonKey2H96( ) ;
            }
            Gx_mode = sMode96;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound96 = 0;
            InitializeNonKey2H96( ) ;
            sMode96 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode96;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2H96( ) ;
         if ( RcdFound96 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound96 = 0;
         /* Using cursor T002H9 */
         pr_default.execute(7, new Object[] {A758CheckList_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T002H9_A758CheckList_Codigo[0] < A758CheckList_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T002H9_A758CheckList_Codigo[0] > A758CheckList_Codigo ) ) )
            {
               A758CheckList_Codigo = T002H9_A758CheckList_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
               RcdFound96 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void move_previous( )
      {
         RcdFound96 = 0;
         /* Using cursor T002H10 */
         pr_default.execute(8, new Object[] {A758CheckList_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T002H10_A758CheckList_Codigo[0] > A758CheckList_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T002H10_A758CheckList_Codigo[0] < A758CheckList_Codigo ) ) )
            {
               A758CheckList_Codigo = T002H10_A758CheckList_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
               RcdFound96 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2H96( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynCheck_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2H96( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound96 == 1 )
            {
               if ( A758CheckList_Codigo != Z758CheckList_Codigo )
               {
                  A758CheckList_Codigo = Z758CheckList_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CHECKLIST_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtCheckList_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynCheck_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2H96( ) ;
                  GX_FocusControl = dynCheck_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A758CheckList_Codigo != Z758CheckList_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = dynCheck_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2H96( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CHECKLIST_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtCheckList_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = dynCheck_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2H96( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A758CheckList_Codigo != Z758CheckList_Codigo )
         {
            A758CheckList_Codigo = Z758CheckList_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CHECKLIST_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtCheckList_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynCheck_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2H96( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002H2 */
            pr_default.execute(0, new Object[] {A758CheckList_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"CheckList"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z1230CheckList_De != T002H2_A1230CheckList_De[0] ) || ( Z1845CheckList_Obrigatorio != T002H2_A1845CheckList_Obrigatorio[0] ) || ( Z1850CheckList_Impeditivo != T002H2_A1850CheckList_Impeditivo[0] ) || ( Z1848CheckList_SeNaoCumpre != T002H2_A1848CheckList_SeNaoCumpre[0] ) || ( Z1846CheckList_NaoCnfCod != T002H2_A1846CheckList_NaoCnfCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z1851CheckList_NaoCnfQdo, T002H2_A1851CheckList_NaoCnfQdo[0]) != 0 ) || ( Z1151CheckList_Ativo != T002H2_A1151CheckList_Ativo[0] ) || ( Z1839Check_Codigo != T002H2_A1839Check_Codigo[0] ) )
            {
               if ( Z1230CheckList_De != T002H2_A1230CheckList_De[0] )
               {
                  GXUtil.WriteLog("checklist:[seudo value changed for attri]"+"CheckList_De");
                  GXUtil.WriteLogRaw("Old: ",Z1230CheckList_De);
                  GXUtil.WriteLogRaw("Current: ",T002H2_A1230CheckList_De[0]);
               }
               if ( Z1845CheckList_Obrigatorio != T002H2_A1845CheckList_Obrigatorio[0] )
               {
                  GXUtil.WriteLog("checklist:[seudo value changed for attri]"+"CheckList_Obrigatorio");
                  GXUtil.WriteLogRaw("Old: ",Z1845CheckList_Obrigatorio);
                  GXUtil.WriteLogRaw("Current: ",T002H2_A1845CheckList_Obrigatorio[0]);
               }
               if ( Z1850CheckList_Impeditivo != T002H2_A1850CheckList_Impeditivo[0] )
               {
                  GXUtil.WriteLog("checklist:[seudo value changed for attri]"+"CheckList_Impeditivo");
                  GXUtil.WriteLogRaw("Old: ",Z1850CheckList_Impeditivo);
                  GXUtil.WriteLogRaw("Current: ",T002H2_A1850CheckList_Impeditivo[0]);
               }
               if ( Z1848CheckList_SeNaoCumpre != T002H2_A1848CheckList_SeNaoCumpre[0] )
               {
                  GXUtil.WriteLog("checklist:[seudo value changed for attri]"+"CheckList_SeNaoCumpre");
                  GXUtil.WriteLogRaw("Old: ",Z1848CheckList_SeNaoCumpre);
                  GXUtil.WriteLogRaw("Current: ",T002H2_A1848CheckList_SeNaoCumpre[0]);
               }
               if ( Z1846CheckList_NaoCnfCod != T002H2_A1846CheckList_NaoCnfCod[0] )
               {
                  GXUtil.WriteLog("checklist:[seudo value changed for attri]"+"CheckList_NaoCnfCod");
                  GXUtil.WriteLogRaw("Old: ",Z1846CheckList_NaoCnfCod);
                  GXUtil.WriteLogRaw("Current: ",T002H2_A1846CheckList_NaoCnfCod[0]);
               }
               if ( StringUtil.StrCmp(Z1851CheckList_NaoCnfQdo, T002H2_A1851CheckList_NaoCnfQdo[0]) != 0 )
               {
                  GXUtil.WriteLog("checklist:[seudo value changed for attri]"+"CheckList_NaoCnfQdo");
                  GXUtil.WriteLogRaw("Old: ",Z1851CheckList_NaoCnfQdo);
                  GXUtil.WriteLogRaw("Current: ",T002H2_A1851CheckList_NaoCnfQdo[0]);
               }
               if ( Z1151CheckList_Ativo != T002H2_A1151CheckList_Ativo[0] )
               {
                  GXUtil.WriteLog("checklist:[seudo value changed for attri]"+"CheckList_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z1151CheckList_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T002H2_A1151CheckList_Ativo[0]);
               }
               if ( Z1839Check_Codigo != T002H2_A1839Check_Codigo[0] )
               {
                  GXUtil.WriteLog("checklist:[seudo value changed for attri]"+"Check_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z1839Check_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T002H2_A1839Check_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"CheckList"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2H96( )
      {
         BeforeValidate2H96( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2H96( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2H96( 0) ;
            CheckOptimisticConcurrency2H96( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2H96( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2H96( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002H11 */
                     pr_default.execute(9, new Object[] {A763CheckList_Descricao, n1230CheckList_De, A1230CheckList_De, n1845CheckList_Obrigatorio, A1845CheckList_Obrigatorio, n1850CheckList_Impeditivo, A1850CheckList_Impeditivo, n1848CheckList_SeNaoCumpre, A1848CheckList_SeNaoCumpre, n1846CheckList_NaoCnfCod, A1846CheckList_NaoCnfCod, n1851CheckList_NaoCnfQdo, A1851CheckList_NaoCnfQdo, A1151CheckList_Ativo, n1839Check_Codigo, A1839Check_Codigo});
                     A758CheckList_Codigo = T002H11_A758CheckList_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("CheckList") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2H0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2H96( ) ;
            }
            EndLevel2H96( ) ;
         }
         CloseExtendedTableCursors2H96( ) ;
      }

      protected void Update2H96( )
      {
         BeforeValidate2H96( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2H96( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2H96( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2H96( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2H96( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002H12 */
                     pr_default.execute(10, new Object[] {A763CheckList_Descricao, n1230CheckList_De, A1230CheckList_De, n1845CheckList_Obrigatorio, A1845CheckList_Obrigatorio, n1850CheckList_Impeditivo, A1850CheckList_Impeditivo, n1848CheckList_SeNaoCumpre, A1848CheckList_SeNaoCumpre, n1846CheckList_NaoCnfCod, A1846CheckList_NaoCnfCod, n1851CheckList_NaoCnfQdo, A1851CheckList_NaoCnfQdo, A1151CheckList_Ativo, n1839Check_Codigo, A1839Check_Codigo, A758CheckList_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("CheckList") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"CheckList"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2H96( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2H96( ) ;
         }
         CloseExtendedTableCursors2H96( ) ;
      }

      protected void DeferredUpdate2H96( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2H96( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2H96( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2H96( ) ;
            AfterConfirm2H96( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2H96( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002H13 */
                  pr_default.execute(11, new Object[] {A758CheckList_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("CheckList") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode96 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2H96( ) ;
         Gx_mode = sMode96;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2H96( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002H14 */
            pr_default.execute(12, new Object[] {n1839Check_Codigo, A1839Check_Codigo});
            A1843Check_Momento = T002H14_A1843Check_Momento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1843Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)));
            pr_default.close(12);
            cmbCheckList_Impeditivo.Visible = ((A1843Check_Momento!=10)&&(A1843Check_Momento!=8)&&(A1843Check_Momento!=4)&&(A1843Check_Momento!=12)&&(A1843Check_Momento!=6) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_Impeditivo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_Impeditivo.Visible), 5, 0)));
            if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
            {
               cellChecklist_impeditivo_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_impeditivo_cell_Internalname, "Class", cellChecklist_impeditivo_cell_Class);
            }
            else
            {
               if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
               {
                  cellChecklist_impeditivo_cell_Class = "DataContentCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_impeditivo_cell_Internalname, "Class", cellChecklist_impeditivo_cell_Class);
               }
            }
            if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
            {
               cellTextblockchecklist_impeditivo_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_impeditivo_cell_Internalname, "Class", cellTextblockchecklist_impeditivo_cell_Class);
            }
            else
            {
               if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
               {
                  cellTextblockchecklist_impeditivo_cell_Class = "DataDescriptionCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_impeditivo_cell_Internalname, "Class", cellTextblockchecklist_impeditivo_cell_Class);
               }
            }
            dynCheckList_NaoCnfCod.Visible = ((A1843Check_Momento!=10)&&(A1843Check_Momento!=8)&&(A1843Check_Momento!=4)&&(A1843Check_Momento!=12)&&(A1843Check_Momento!=6) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynCheckList_NaoCnfCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynCheckList_NaoCnfCod.Visible), 5, 0)));
            if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
            {
               cellChecklist_naocnfcod_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfcod_cell_Internalname, "Class", cellChecklist_naocnfcod_cell_Class);
            }
            else
            {
               if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
               {
                  cellChecklist_naocnfcod_cell_Class = "DataContentCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfcod_cell_Internalname, "Class", cellChecklist_naocnfcod_cell_Class);
               }
            }
            if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
            {
               cellTextblockchecklist_naocnfcod_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfcod_cell_Internalname, "Class", cellTextblockchecklist_naocnfcod_cell_Class);
            }
            else
            {
               if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
               {
                  cellTextblockchecklist_naocnfcod_cell_Class = "DataDescriptionCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfcod_cell_Internalname, "Class", cellTextblockchecklist_naocnfcod_cell_Class);
               }
            }
            cmbCheckList_SeNaoCumpre.Visible = (A1850CheckList_Impeditivo ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_SeNaoCumpre_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_SeNaoCumpre.Visible), 5, 0)));
            if ( ! ( A1850CheckList_Impeditivo ) )
            {
               cellChecklist_senaocumpre_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_senaocumpre_cell_Internalname, "Class", cellChecklist_senaocumpre_cell_Class);
            }
            else
            {
               if ( A1850CheckList_Impeditivo )
               {
                  cellChecklist_senaocumpre_cell_Class = "DataContentCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_senaocumpre_cell_Internalname, "Class", cellChecklist_senaocumpre_cell_Class);
               }
            }
            if ( ! ( A1850CheckList_Impeditivo ) )
            {
               cellTextblockchecklist_senaocumpre_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_senaocumpre_cell_Internalname, "Class", cellTextblockchecklist_senaocumpre_cell_Class);
            }
            else
            {
               if ( A1850CheckList_Impeditivo )
               {
                  cellTextblockchecklist_senaocumpre_cell_Class = "DataDescriptionCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_senaocumpre_cell_Internalname, "Class", cellTextblockchecklist_senaocumpre_cell_Class);
               }
            }
            cmbCheckList_NaoCnfQdo.Visible = ((A1846CheckList_NaoCnfCod>0) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_NaoCnfQdo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_NaoCnfQdo.Visible), 5, 0)));
            if ( ! ( ( A1846CheckList_NaoCnfCod > 0 ) ) )
            {
               cellChecklist_naocnfqdo_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfqdo_cell_Internalname, "Class", cellChecklist_naocnfqdo_cell_Class);
            }
            else
            {
               if ( A1846CheckList_NaoCnfCod > 0 )
               {
                  cellChecklist_naocnfqdo_cell_Class = "DataContentCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellChecklist_naocnfqdo_cell_Internalname, "Class", cellChecklist_naocnfqdo_cell_Class);
               }
            }
            if ( ! ( ( A1846CheckList_NaoCnfCod > 0 ) ) )
            {
               cellTextblockchecklist_naocnfqdo_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfqdo_cell_Internalname, "Class", cellTextblockchecklist_naocnfqdo_cell_Class);
            }
            else
            {
               if ( A1846CheckList_NaoCnfCod > 0 )
               {
                  cellTextblockchecklist_naocnfqdo_cell_Class = "DataDescriptionCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockchecklist_naocnfqdo_cell_Internalname, "Class", cellTextblockchecklist_naocnfqdo_cell_Class);
               }
            }
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T002H15 */
            pr_default.execute(13, new Object[] {A758CheckList_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Chck Lst Log"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
            /* Using cursor T002H16 */
            pr_default.execute(14, new Object[] {A758CheckList_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Chck Lst"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
         }
      }

      protected void EndLevel2H96( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2H96( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(12);
            context.CommitDataStores( "CheckList");
            if ( AnyError == 0 )
            {
               ConfirmValues2H0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(12);
            context.RollbackDataStores( "CheckList");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2H96( )
      {
         /* Scan By routine */
         /* Using cursor T002H17 */
         pr_default.execute(15);
         RcdFound96 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound96 = 1;
            A758CheckList_Codigo = T002H17_A758CheckList_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2H96( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound96 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound96 = 1;
            A758CheckList_Codigo = T002H17_A758CheckList_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2H96( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm2H96( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2H96( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2H96( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2H96( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2H96( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2H96( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2H96( )
      {
         dynCheck_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynCheck_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynCheck_Codigo.Enabled), 5, 0)));
         cmbCheck_Momento.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheck_Momento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheck_Momento.Enabled), 5, 0)));
         edtCheckList_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheckList_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCheckList_Descricao_Enabled), 5, 0)));
         cmbCheckList_Obrigatorio.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_Obrigatorio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_Obrigatorio.Enabled), 5, 0)));
         cmbCheckList_Impeditivo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_Impeditivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_Impeditivo.Enabled), 5, 0)));
         cmbCheckList_SeNaoCumpre.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_SeNaoCumpre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_SeNaoCumpre.Enabled), 5, 0)));
         dynCheckList_NaoCnfCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynCheckList_NaoCnfCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynCheckList_NaoCnfCod.Enabled), 5, 0)));
         cmbCheckList_NaoCnfQdo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_NaoCnfQdo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheckList_NaoCnfQdo.Enabled), 5, 0)));
         chkCheckList_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkCheckList_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkCheckList_Ativo.Enabled), 5, 0)));
         edtCheckList_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheckList_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCheckList_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2H0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282332319");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("checklist.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7CheckList_Codigo) + "," + UrlEncode("" +AV9Check_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z758CheckList_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z758CheckList_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1230CheckList_De", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1230CheckList_De), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1845CheckList_Obrigatorio", Z1845CheckList_Obrigatorio);
         GxWebStd.gx_boolean_hidden_field( context, "Z1850CheckList_Impeditivo", Z1850CheckList_Impeditivo);
         GxWebStd.gx_hidden_field( context, "Z1848CheckList_SeNaoCumpre", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1848CheckList_SeNaoCumpre), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1846CheckList_NaoCnfCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1846CheckList_NaoCnfCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1851CheckList_NaoCnfQdo", StringUtil.RTrim( Z1851CheckList_NaoCnfQdo));
         GxWebStd.gx_boolean_hidden_field( context, "Z1151CheckList_Ativo", Z1151CheckList_Ativo);
         GxWebStd.gx_hidden_field( context, "Z1839Check_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1839Check_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N1839Check_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1839Check_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV10TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV10TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCHECKLIST_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7CheckList_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CHECK_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_Check_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCHECK_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Check_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV8WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV8WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "CHECKLIST_DE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1230CheckList_De), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCHECKLIST_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7CheckList_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "CheckList";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A758CheckList_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1230CheckList_De), "ZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("checklist:[SendSecurityCheck value for]"+"CheckList_Codigo:"+context.localUtil.Format( (decimal)(A758CheckList_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("checklist:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("checklist:[SendSecurityCheck value for]"+"CheckList_De:"+context.localUtil.Format( (decimal)(A1230CheckList_De), "ZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("checklist.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7CheckList_Codigo) + "," + UrlEncode("" +AV9Check_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "CheckList" ;
      }

      public override String GetPgmdesc( )
      {
         return "Check List" ;
      }

      protected void InitializeNonKey2H96( )
      {
         A1839Check_Codigo = 0;
         n1839Check_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
         n1839Check_Codigo = ((0==A1839Check_Codigo) ? true : false);
         A1843Check_Momento = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1843Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)));
         A763CheckList_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A763CheckList_Descricao", A763CheckList_Descricao);
         A1230CheckList_De = 0;
         n1230CheckList_De = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1230CheckList_De", StringUtil.LTrim( StringUtil.Str( (decimal)(A1230CheckList_De), 4, 0)));
         A1845CheckList_Obrigatorio = false;
         n1845CheckList_Obrigatorio = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1845CheckList_Obrigatorio", A1845CheckList_Obrigatorio);
         n1845CheckList_Obrigatorio = ((false==A1845CheckList_Obrigatorio) ? true : false);
         A1850CheckList_Impeditivo = false;
         n1850CheckList_Impeditivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1850CheckList_Impeditivo", A1850CheckList_Impeditivo);
         n1850CheckList_Impeditivo = ((false==A1850CheckList_Impeditivo) ? true : false);
         A1848CheckList_SeNaoCumpre = 0;
         n1848CheckList_SeNaoCumpre = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1848CheckList_SeNaoCumpre", StringUtil.LTrim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0)));
         n1848CheckList_SeNaoCumpre = ((0==A1848CheckList_SeNaoCumpre) ? true : false);
         A1846CheckList_NaoCnfCod = 0;
         n1846CheckList_NaoCnfCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1846CheckList_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0)));
         n1846CheckList_NaoCnfCod = ((0==A1846CheckList_NaoCnfCod) ? true : false);
         A1851CheckList_NaoCnfQdo = "";
         n1851CheckList_NaoCnfQdo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1851CheckList_NaoCnfQdo", A1851CheckList_NaoCnfQdo);
         n1851CheckList_NaoCnfQdo = (String.IsNullOrEmpty(StringUtil.RTrim( A1851CheckList_NaoCnfQdo)) ? true : false);
         A1151CheckList_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1151CheckList_Ativo", A1151CheckList_Ativo);
         Z1230CheckList_De = 0;
         Z1845CheckList_Obrigatorio = false;
         Z1850CheckList_Impeditivo = false;
         Z1848CheckList_SeNaoCumpre = 0;
         Z1846CheckList_NaoCnfCod = 0;
         Z1851CheckList_NaoCnfQdo = "";
         Z1151CheckList_Ativo = false;
         Z1839Check_Codigo = 0;
      }

      protected void InitAll2H96( )
      {
         A758CheckList_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
         InitializeNonKey2H96( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1151CheckList_Ativo = i1151CheckList_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1151CheckList_Ativo", A1151CheckList_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282332339");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("checklist.js", "?20204282332339");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcheck_codigo_Internalname = "TEXTBLOCKCHECK_CODIGO";
         dynCheck_Codigo_Internalname = "CHECK_CODIGO";
         lblTextblockcheck_momento_Internalname = "TEXTBLOCKCHECK_MOMENTO";
         cmbCheck_Momento_Internalname = "CHECK_MOMENTO";
         lblTextblockchecklist_descricao_Internalname = "TEXTBLOCKCHECKLIST_DESCRICAO";
         edtCheckList_Descricao_Internalname = "CHECKLIST_DESCRICAO";
         lblTextblockchecklist_obrigatorio_Internalname = "TEXTBLOCKCHECKLIST_OBRIGATORIO";
         cmbCheckList_Obrigatorio_Internalname = "CHECKLIST_OBRIGATORIO";
         lblTextblockchecklist_impeditivo_Internalname = "TEXTBLOCKCHECKLIST_IMPEDITIVO";
         cellTextblockchecklist_impeditivo_cell_Internalname = "TEXTBLOCKCHECKLIST_IMPEDITIVO_CELL";
         cmbCheckList_Impeditivo_Internalname = "CHECKLIST_IMPEDITIVO";
         cellChecklist_impeditivo_cell_Internalname = "CHECKLIST_IMPEDITIVO_CELL";
         lblTextblockchecklist_senaocumpre_Internalname = "TEXTBLOCKCHECKLIST_SENAOCUMPRE";
         cellTextblockchecklist_senaocumpre_cell_Internalname = "TEXTBLOCKCHECKLIST_SENAOCUMPRE_CELL";
         cmbCheckList_SeNaoCumpre_Internalname = "CHECKLIST_SENAOCUMPRE";
         cellChecklist_senaocumpre_cell_Internalname = "CHECKLIST_SENAOCUMPRE_CELL";
         lblTextblockchecklist_naocnfcod_Internalname = "TEXTBLOCKCHECKLIST_NAOCNFCOD";
         cellTextblockchecklist_naocnfcod_cell_Internalname = "TEXTBLOCKCHECKLIST_NAOCNFCOD_CELL";
         dynCheckList_NaoCnfCod_Internalname = "CHECKLIST_NAOCNFCOD";
         cellChecklist_naocnfcod_cell_Internalname = "CHECKLIST_NAOCNFCOD_CELL";
         lblTextblockchecklist_naocnfqdo_Internalname = "TEXTBLOCKCHECKLIST_NAOCNFQDO";
         cellTextblockchecklist_naocnfqdo_cell_Internalname = "TEXTBLOCKCHECKLIST_NAOCNFQDO_CELL";
         cmbCheckList_NaoCnfQdo_Internalname = "CHECKLIST_NAOCNFQDO";
         cellChecklist_naocnfqdo_cell_Internalname = "CHECKLIST_NAOCNFQDO_CELL";
         lblTextblockchecklist_ativo_Internalname = "TEXTBLOCKCHECKLIST_ATIVO";
         chkCheckList_Ativo_Internalname = "CHECKLIST_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtCheckList_Codigo_Internalname = "CHECKLIST_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Item do Check List";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Check List";
         chkCheckList_Ativo.Enabled = 1;
         chkCheckList_Ativo.Visible = 1;
         lblTextblockchecklist_ativo_Visible = 1;
         cmbCheckList_NaoCnfQdo_Jsonclick = "";
         cmbCheckList_NaoCnfQdo.Enabled = 1;
         cmbCheckList_NaoCnfQdo.Visible = 1;
         cellChecklist_naocnfqdo_cell_Class = "";
         cellTextblockchecklist_naocnfqdo_cell_Class = "";
         dynCheckList_NaoCnfCod_Jsonclick = "";
         dynCheckList_NaoCnfCod.Enabled = 1;
         dynCheckList_NaoCnfCod.Visible = 1;
         cellChecklist_naocnfcod_cell_Class = "";
         cellTextblockchecklist_naocnfcod_cell_Class = "";
         cmbCheckList_SeNaoCumpre_Jsonclick = "";
         cmbCheckList_SeNaoCumpre.Enabled = 1;
         cmbCheckList_SeNaoCumpre.Visible = 1;
         cellChecklist_senaocumpre_cell_Class = "";
         cellTextblockchecklist_senaocumpre_cell_Class = "";
         cmbCheckList_Impeditivo_Jsonclick = "";
         cmbCheckList_Impeditivo.Enabled = 1;
         cmbCheckList_Impeditivo.Visible = 1;
         cellChecklist_impeditivo_cell_Class = "";
         cellTextblockchecklist_impeditivo_cell_Class = "";
         cmbCheckList_Obrigatorio_Jsonclick = "";
         cmbCheckList_Obrigatorio.Enabled = 1;
         edtCheckList_Descricao_Enabled = 1;
         cmbCheck_Momento_Jsonclick = "";
         cmbCheck_Momento.Enabled = 0;
         dynCheck_Codigo_Jsonclick = "";
         dynCheck_Codigo.Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtCheckList_Codigo_Jsonclick = "";
         edtCheckList_Codigo_Enabled = 0;
         edtCheckList_Codigo_Visible = 1;
         chkCheckList_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLACHECK_CODIGO2H1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACHECK_CODIGO_data2H1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACHECK_CODIGO_html2H1( )
      {
         int gxdynajaxvalue ;
         GXDLACHECK_CODIGO_data2H1( ) ;
         gxdynajaxindex = 1;
         dynCheck_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynCheck_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACHECK_CODIGO_data2H1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T002H18 */
         pr_default.execute(16);
         while ( (pr_default.getStatus(16) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002H18_A1839Check_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002H18_A1841Check_Nome[0]));
            pr_default.readNext(16);
         }
         pr_default.close(16);
      }

      protected void GXDLACHECKLIST_NAOCNFCOD2H96( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACHECKLIST_NAOCNFCOD_data2H96( AV8WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACHECKLIST_NAOCNFCOD_html2H96( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLACHECKLIST_NAOCNFCOD_data2H96( AV8WWPContext) ;
         gxdynajaxindex = 1;
         dynCheckList_NaoCnfCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynCheckList_NaoCnfCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACHECKLIST_NAOCNFCOD_data2H96( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor T002H19 */
         pr_default.execute(17, new Object[] {AV8WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(17) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002H19_A426NaoConformidade_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002H19_A427NaoConformidade_Nome[0]));
            pr_default.readNext(17);
         }
         pr_default.close(17);
      }

      public void Valid_Check_codigo( GXCombobox dynGX_Parm1 ,
                                      GXCombobox cmbGX_Parm2 ,
                                      GXCombobox cmbGX_Parm3 ,
                                      GXCombobox dynGX_Parm4 )
      {
         dynCheck_Codigo = dynGX_Parm1;
         A1839Check_Codigo = (int)(NumberUtil.Val( dynCheck_Codigo.CurrentValue, "."));
         n1839Check_Codigo = false;
         cmbCheck_Momento = cmbGX_Parm2;
         A1843Check_Momento = (short)(NumberUtil.Val( cmbCheck_Momento.CurrentValue, "."));
         cmbCheck_Momento.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0));
         cmbCheckList_Impeditivo = cmbGX_Parm3;
         A1850CheckList_Impeditivo = StringUtil.StrToBool( cmbCheckList_Impeditivo.CurrentValue);
         n1850CheckList_Impeditivo = false;
         cmbCheckList_Impeditivo.CurrentValue = StringUtil.BoolToStr( A1850CheckList_Impeditivo);
         dynCheckList_NaoCnfCod = dynGX_Parm4;
         A1846CheckList_NaoCnfCod = (int)(NumberUtil.Val( dynCheckList_NaoCnfCod.CurrentValue, "."));
         n1846CheckList_NaoCnfCod = false;
         /* Using cursor T002H20 */
         pr_default.execute(18, new Object[] {n1839Check_Codigo, A1839Check_Codigo});
         if ( (pr_default.getStatus(18) == 101) )
         {
            if ( ! ( (0==A1839Check_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe ' T204'.", "ForeignKeyNotFound", 1, "CHECK_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynCheck_Codigo_Internalname;
            }
         }
         A1843Check_Momento = T002H20_A1843Check_Momento[0];
         cmbCheck_Momento.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0));
         pr_default.close(18);
         cmbCheckList_Impeditivo.Visible = ((A1843Check_Momento!=10)&&(A1843Check_Momento!=8)&&(A1843Check_Momento!=4)&&(A1843Check_Momento!=12)&&(A1843Check_Momento!=6) ? 1 : 0);
         if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
         {
            cellChecklist_impeditivo_cell_Class = "Invisible";
         }
         else
         {
            if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
            {
               cellChecklist_impeditivo_cell_Class = "DataContentCell";
            }
         }
         if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
         {
            cellTextblockchecklist_impeditivo_cell_Class = "Invisible";
         }
         else
         {
            if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
            {
               cellTextblockchecklist_impeditivo_cell_Class = "DataDescriptionCell";
            }
         }
         dynCheckList_NaoCnfCod.Visible = ((A1843Check_Momento!=10)&&(A1843Check_Momento!=8)&&(A1843Check_Momento!=4)&&(A1843Check_Momento!=12)&&(A1843Check_Momento!=6) ? 1 : 0);
         if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
         {
            cellChecklist_naocnfcod_cell_Class = "Invisible";
         }
         else
         {
            if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
            {
               cellChecklist_naocnfcod_cell_Class = "DataContentCell";
            }
         }
         if ( ! ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) ) )
         {
            cellTextblockchecklist_naocnfcod_cell_Class = "Invisible";
         }
         else
         {
            if ( ( A1843Check_Momento != 10 ) && ( A1843Check_Momento != 8 ) && ( A1843Check_Momento != 4 ) && ( A1843Check_Momento != 12 ) && ( A1843Check_Momento != 6 ) )
            {
               cellTextblockchecklist_naocnfcod_cell_Class = "DataDescriptionCell";
            }
         }
         if ( ( A1843Check_Momento == 10 ) || ( A1843Check_Momento == 8 ) || ( A1843Check_Momento == 4 ) || ( A1843Check_Momento == 12 ) || ( A1843Check_Momento == 6 ) )
         {
            A1850CheckList_Impeditivo = false;
            n1850CheckList_Impeditivo = false;
            cmbCheckList_Impeditivo.CurrentValue = StringUtil.BoolToStr( A1850CheckList_Impeditivo);
            n1850CheckList_Impeditivo = true;
            cmbCheckList_Impeditivo.CurrentValue = StringUtil.BoolToStr( A1850CheckList_Impeditivo);
         }
         if ( ( A1843Check_Momento == 10 ) || ( A1843Check_Momento == 8 ) || ( A1843Check_Momento == 4 ) || ( A1843Check_Momento == 12 ) || ( A1843Check_Momento == 6 ) )
         {
            A1846CheckList_NaoCnfCod = 0;
            n1846CheckList_NaoCnfCod = false;
            n1846CheckList_NaoCnfCod = true;
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1843Check_Momento = 0;
            cmbCheck_Momento.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0));
         }
         dynCheckList_NaoCnfCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0));
         cmbCheck_Momento.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0));
         isValidOutput.Add(cmbCheck_Momento);
         isValidOutput.Add(cmbCheckList_Impeditivo.Visible);
         isValidOutput.Add(cellChecklist_impeditivo_cell_Class);
         isValidOutput.Add(cellTextblockchecklist_impeditivo_cell_Class);
         isValidOutput.Add(dynCheckList_NaoCnfCod.Visible);
         isValidOutput.Add(cellChecklist_naocnfcod_cell_Class);
         isValidOutput.Add(cellTextblockchecklist_naocnfcod_cell_Class);
         cmbCheckList_Impeditivo.CurrentValue = StringUtil.BoolToStr( A1850CheckList_Impeditivo);
         isValidOutput.Add(cmbCheckList_Impeditivo);
         if ( dynCheckList_NaoCnfCod.ItemCount > 0 )
         {
            A1846CheckList_NaoCnfCod = (int)(NumberUtil.Val( dynCheckList_NaoCnfCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0))), "."));
            n1846CheckList_NaoCnfCod = false;
         }
         dynCheckList_NaoCnfCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0));
         isValidOutput.Add(dynCheckList_NaoCnfCod);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7CheckList_Codigo',fld:'vCHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV9Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E122H2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV10TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(18);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1851CheckList_NaoCnfQdo = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         T002H5_A1839Check_Codigo = new int[1] ;
         T002H5_n1839Check_Codigo = new bool[] {false} ;
         T002H5_A1841Check_Nome = new String[] {""} ;
         A1851CheckList_NaoCnfQdo = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcheck_codigo_Jsonclick = "";
         lblTextblockcheck_momento_Jsonclick = "";
         lblTextblockchecklist_descricao_Jsonclick = "";
         A763CheckList_Descricao = "";
         lblTextblockchecklist_obrigatorio_Jsonclick = "";
         lblTextblockchecklist_impeditivo_Jsonclick = "";
         lblTextblockchecklist_senaocumpre_Jsonclick = "";
         lblTextblockchecklist_naocnfcod_Jsonclick = "";
         lblTextblockchecklist_naocnfqdo_Jsonclick = "";
         lblTextblockchecklist_ativo_Jsonclick = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode96 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z763CheckList_Descricao = "";
         T002H4_A1843Check_Momento = new short[1] ;
         T002H6_A758CheckList_Codigo = new int[1] ;
         T002H6_A1843Check_Momento = new short[1] ;
         T002H6_A763CheckList_Descricao = new String[] {""} ;
         T002H6_A1230CheckList_De = new short[1] ;
         T002H6_n1230CheckList_De = new bool[] {false} ;
         T002H6_A1845CheckList_Obrigatorio = new bool[] {false} ;
         T002H6_n1845CheckList_Obrigatorio = new bool[] {false} ;
         T002H6_A1850CheckList_Impeditivo = new bool[] {false} ;
         T002H6_n1850CheckList_Impeditivo = new bool[] {false} ;
         T002H6_A1848CheckList_SeNaoCumpre = new short[1] ;
         T002H6_n1848CheckList_SeNaoCumpre = new bool[] {false} ;
         T002H6_A1846CheckList_NaoCnfCod = new int[1] ;
         T002H6_n1846CheckList_NaoCnfCod = new bool[] {false} ;
         T002H6_A1851CheckList_NaoCnfQdo = new String[] {""} ;
         T002H6_n1851CheckList_NaoCnfQdo = new bool[] {false} ;
         T002H6_A1151CheckList_Ativo = new bool[] {false} ;
         T002H6_A1839Check_Codigo = new int[1] ;
         T002H6_n1839Check_Codigo = new bool[] {false} ;
         T002H7_A1843Check_Momento = new short[1] ;
         T002H8_A758CheckList_Codigo = new int[1] ;
         T002H3_A758CheckList_Codigo = new int[1] ;
         T002H3_A763CheckList_Descricao = new String[] {""} ;
         T002H3_A1230CheckList_De = new short[1] ;
         T002H3_n1230CheckList_De = new bool[] {false} ;
         T002H3_A1845CheckList_Obrigatorio = new bool[] {false} ;
         T002H3_n1845CheckList_Obrigatorio = new bool[] {false} ;
         T002H3_A1850CheckList_Impeditivo = new bool[] {false} ;
         T002H3_n1850CheckList_Impeditivo = new bool[] {false} ;
         T002H3_A1848CheckList_SeNaoCumpre = new short[1] ;
         T002H3_n1848CheckList_SeNaoCumpre = new bool[] {false} ;
         T002H3_A1846CheckList_NaoCnfCod = new int[1] ;
         T002H3_n1846CheckList_NaoCnfCod = new bool[] {false} ;
         T002H3_A1851CheckList_NaoCnfQdo = new String[] {""} ;
         T002H3_n1851CheckList_NaoCnfQdo = new bool[] {false} ;
         T002H3_A1151CheckList_Ativo = new bool[] {false} ;
         T002H3_A1839Check_Codigo = new int[1] ;
         T002H3_n1839Check_Codigo = new bool[] {false} ;
         T002H9_A758CheckList_Codigo = new int[1] ;
         T002H10_A758CheckList_Codigo = new int[1] ;
         T002H2_A758CheckList_Codigo = new int[1] ;
         T002H2_A763CheckList_Descricao = new String[] {""} ;
         T002H2_A1230CheckList_De = new short[1] ;
         T002H2_n1230CheckList_De = new bool[] {false} ;
         T002H2_A1845CheckList_Obrigatorio = new bool[] {false} ;
         T002H2_n1845CheckList_Obrigatorio = new bool[] {false} ;
         T002H2_A1850CheckList_Impeditivo = new bool[] {false} ;
         T002H2_n1850CheckList_Impeditivo = new bool[] {false} ;
         T002H2_A1848CheckList_SeNaoCumpre = new short[1] ;
         T002H2_n1848CheckList_SeNaoCumpre = new bool[] {false} ;
         T002H2_A1846CheckList_NaoCnfCod = new int[1] ;
         T002H2_n1846CheckList_NaoCnfCod = new bool[] {false} ;
         T002H2_A1851CheckList_NaoCnfQdo = new String[] {""} ;
         T002H2_n1851CheckList_NaoCnfQdo = new bool[] {false} ;
         T002H2_A1151CheckList_Ativo = new bool[] {false} ;
         T002H2_A1839Check_Codigo = new int[1] ;
         T002H2_n1839Check_Codigo = new bool[] {false} ;
         T002H11_A758CheckList_Codigo = new int[1] ;
         T002H14_A1843Check_Momento = new short[1] ;
         T002H15_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         T002H16_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         T002H17_A758CheckList_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T002H18_A1839Check_Codigo = new int[1] ;
         T002H18_n1839Check_Codigo = new bool[] {false} ;
         T002H18_A1841Check_Nome = new String[] {""} ;
         T002H19_A426NaoConformidade_Codigo = new int[1] ;
         T002H19_A427NaoConformidade_Nome = new String[] {""} ;
         T002H19_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         T002H20_A1843Check_Momento = new short[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.checklist__default(),
            new Object[][] {
                new Object[] {
               T002H2_A758CheckList_Codigo, T002H2_A763CheckList_Descricao, T002H2_A1230CheckList_De, T002H2_n1230CheckList_De, T002H2_A1845CheckList_Obrigatorio, T002H2_n1845CheckList_Obrigatorio, T002H2_A1850CheckList_Impeditivo, T002H2_n1850CheckList_Impeditivo, T002H2_A1848CheckList_SeNaoCumpre, T002H2_n1848CheckList_SeNaoCumpre,
               T002H2_A1846CheckList_NaoCnfCod, T002H2_n1846CheckList_NaoCnfCod, T002H2_A1851CheckList_NaoCnfQdo, T002H2_n1851CheckList_NaoCnfQdo, T002H2_A1151CheckList_Ativo, T002H2_A1839Check_Codigo, T002H2_n1839Check_Codigo
               }
               , new Object[] {
               T002H3_A758CheckList_Codigo, T002H3_A763CheckList_Descricao, T002H3_A1230CheckList_De, T002H3_n1230CheckList_De, T002H3_A1845CheckList_Obrigatorio, T002H3_n1845CheckList_Obrigatorio, T002H3_A1850CheckList_Impeditivo, T002H3_n1850CheckList_Impeditivo, T002H3_A1848CheckList_SeNaoCumpre, T002H3_n1848CheckList_SeNaoCumpre,
               T002H3_A1846CheckList_NaoCnfCod, T002H3_n1846CheckList_NaoCnfCod, T002H3_A1851CheckList_NaoCnfQdo, T002H3_n1851CheckList_NaoCnfQdo, T002H3_A1151CheckList_Ativo, T002H3_A1839Check_Codigo, T002H3_n1839Check_Codigo
               }
               , new Object[] {
               T002H4_A1843Check_Momento
               }
               , new Object[] {
               T002H5_A1839Check_Codigo, T002H5_A1841Check_Nome
               }
               , new Object[] {
               T002H6_A758CheckList_Codigo, T002H6_A1843Check_Momento, T002H6_A763CheckList_Descricao, T002H6_A1230CheckList_De, T002H6_n1230CheckList_De, T002H6_A1845CheckList_Obrigatorio, T002H6_n1845CheckList_Obrigatorio, T002H6_A1850CheckList_Impeditivo, T002H6_n1850CheckList_Impeditivo, T002H6_A1848CheckList_SeNaoCumpre,
               T002H6_n1848CheckList_SeNaoCumpre, T002H6_A1846CheckList_NaoCnfCod, T002H6_n1846CheckList_NaoCnfCod, T002H6_A1851CheckList_NaoCnfQdo, T002H6_n1851CheckList_NaoCnfQdo, T002H6_A1151CheckList_Ativo, T002H6_A1839Check_Codigo, T002H6_n1839Check_Codigo
               }
               , new Object[] {
               T002H7_A1843Check_Momento
               }
               , new Object[] {
               T002H8_A758CheckList_Codigo
               }
               , new Object[] {
               T002H9_A758CheckList_Codigo
               }
               , new Object[] {
               T002H10_A758CheckList_Codigo
               }
               , new Object[] {
               T002H11_A758CheckList_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002H14_A1843Check_Momento
               }
               , new Object[] {
               T002H15_A820ContagemResultadoChckLstLog_Codigo
               }
               , new Object[] {
               T002H16_A761ContagemResultadoChckLst_Codigo
               }
               , new Object[] {
               T002H17_A758CheckList_Codigo
               }
               , new Object[] {
               T002H18_A1839Check_Codigo, T002H18_A1841Check_Nome
               }
               , new Object[] {
               T002H19_A426NaoConformidade_Codigo, T002H19_A427NaoConformidade_Nome, T002H19_A428NaoConformidade_AreaTrabalhoCod
               }
               , new Object[] {
               T002H20_A1843Check_Momento
               }
            }
         );
         Z1151CheckList_Ativo = true;
         A1151CheckList_Ativo = true;
         i1151CheckList_Ativo = true;
         AV14Pgmname = "CheckList";
      }

      private short Z1230CheckList_De ;
      private short Z1848CheckList_SeNaoCumpre ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A1843Check_Momento ;
      private short A1848CheckList_SeNaoCumpre ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A1230CheckList_De ;
      private short Gx_BScreen ;
      private short RcdFound96 ;
      private short GX_JID ;
      private short Z1843Check_Momento ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7CheckList_Codigo ;
      private int wcpOAV9Check_Codigo ;
      private int Z758CheckList_Codigo ;
      private int Z1846CheckList_NaoCnfCod ;
      private int Z1839Check_Codigo ;
      private int N1839Check_Codigo ;
      private int A1839Check_Codigo ;
      private int AV7CheckList_Codigo ;
      private int AV9Check_Codigo ;
      private int trnEnded ;
      private int A1846CheckList_NaoCnfCod ;
      private int A758CheckList_Codigo ;
      private int edtCheckList_Codigo_Enabled ;
      private int edtCheckList_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtCheckList_Descricao_Enabled ;
      private int lblTextblockchecklist_ativo_Visible ;
      private int AV12Insert_Check_Codigo ;
      private int AV15GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z1851CheckList_NaoCnfQdo ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String A1851CheckList_NaoCnfQdo ;
      private String chkCheckList_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynCheck_Codigo_Internalname ;
      private String edtCheckList_Codigo_Internalname ;
      private String edtCheckList_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcheck_codigo_Internalname ;
      private String lblTextblockcheck_codigo_Jsonclick ;
      private String dynCheck_Codigo_Jsonclick ;
      private String lblTextblockcheck_momento_Internalname ;
      private String lblTextblockcheck_momento_Jsonclick ;
      private String cmbCheck_Momento_Internalname ;
      private String cmbCheck_Momento_Jsonclick ;
      private String lblTextblockchecklist_descricao_Internalname ;
      private String lblTextblockchecklist_descricao_Jsonclick ;
      private String edtCheckList_Descricao_Internalname ;
      private String lblTextblockchecklist_obrigatorio_Internalname ;
      private String lblTextblockchecklist_obrigatorio_Jsonclick ;
      private String cmbCheckList_Obrigatorio_Internalname ;
      private String cmbCheckList_Obrigatorio_Jsonclick ;
      private String cellTextblockchecklist_impeditivo_cell_Internalname ;
      private String cellTextblockchecklist_impeditivo_cell_Class ;
      private String lblTextblockchecklist_impeditivo_Internalname ;
      private String lblTextblockchecklist_impeditivo_Jsonclick ;
      private String cellChecklist_impeditivo_cell_Internalname ;
      private String cellChecklist_impeditivo_cell_Class ;
      private String cmbCheckList_Impeditivo_Internalname ;
      private String cmbCheckList_Impeditivo_Jsonclick ;
      private String cellTextblockchecklist_senaocumpre_cell_Internalname ;
      private String cellTextblockchecklist_senaocumpre_cell_Class ;
      private String lblTextblockchecklist_senaocumpre_Internalname ;
      private String lblTextblockchecklist_senaocumpre_Jsonclick ;
      private String cellChecklist_senaocumpre_cell_Internalname ;
      private String cellChecklist_senaocumpre_cell_Class ;
      private String cmbCheckList_SeNaoCumpre_Internalname ;
      private String cmbCheckList_SeNaoCumpre_Jsonclick ;
      private String cellTextblockchecklist_naocnfcod_cell_Internalname ;
      private String cellTextblockchecklist_naocnfcod_cell_Class ;
      private String lblTextblockchecklist_naocnfcod_Internalname ;
      private String lblTextblockchecklist_naocnfcod_Jsonclick ;
      private String cellChecklist_naocnfcod_cell_Internalname ;
      private String cellChecklist_naocnfcod_cell_Class ;
      private String dynCheckList_NaoCnfCod_Internalname ;
      private String dynCheckList_NaoCnfCod_Jsonclick ;
      private String cellTextblockchecklist_naocnfqdo_cell_Internalname ;
      private String cellTextblockchecklist_naocnfqdo_cell_Class ;
      private String lblTextblockchecklist_naocnfqdo_Internalname ;
      private String lblTextblockchecklist_naocnfqdo_Jsonclick ;
      private String cellChecklist_naocnfqdo_cell_Internalname ;
      private String cellChecklist_naocnfqdo_cell_Class ;
      private String cmbCheckList_NaoCnfQdo_Internalname ;
      private String cmbCheckList_NaoCnfQdo_Jsonclick ;
      private String lblTextblockchecklist_ativo_Internalname ;
      private String lblTextblockchecklist_ativo_Jsonclick ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode96 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private bool Z1845CheckList_Obrigatorio ;
      private bool Z1850CheckList_Impeditivo ;
      private bool Z1151CheckList_Ativo ;
      private bool entryPointCalled ;
      private bool n1839Check_Codigo ;
      private bool toggleJsOutput ;
      private bool A1845CheckList_Obrigatorio ;
      private bool n1845CheckList_Obrigatorio ;
      private bool A1850CheckList_Impeditivo ;
      private bool n1850CheckList_Impeditivo ;
      private bool n1848CheckList_SeNaoCumpre ;
      private bool n1851CheckList_NaoCnfQdo ;
      private bool wbErr ;
      private bool n1846CheckList_NaoCnfCod ;
      private bool A1151CheckList_Ativo ;
      private bool n1230CheckList_De ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i1151CheckList_Ativo ;
      private String A763CheckList_Descricao ;
      private String Z763CheckList_Descricao ;
      private IGxSession AV11WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IDataStoreProvider pr_default ;
      private int[] T002H5_A1839Check_Codigo ;
      private bool[] T002H5_n1839Check_Codigo ;
      private String[] T002H5_A1841Check_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_Check_Codigo ;
      private GXCombobox dynCheck_Codigo ;
      private GXCombobox cmbCheck_Momento ;
      private GXCombobox cmbCheckList_Obrigatorio ;
      private GXCombobox cmbCheckList_Impeditivo ;
      private GXCombobox cmbCheckList_SeNaoCumpre ;
      private GXCombobox dynCheckList_NaoCnfCod ;
      private GXCombobox cmbCheckList_NaoCnfQdo ;
      private GXCheckbox chkCheckList_Ativo ;
      private short[] T002H4_A1843Check_Momento ;
      private int[] T002H6_A758CheckList_Codigo ;
      private short[] T002H6_A1843Check_Momento ;
      private String[] T002H6_A763CheckList_Descricao ;
      private short[] T002H6_A1230CheckList_De ;
      private bool[] T002H6_n1230CheckList_De ;
      private bool[] T002H6_A1845CheckList_Obrigatorio ;
      private bool[] T002H6_n1845CheckList_Obrigatorio ;
      private bool[] T002H6_A1850CheckList_Impeditivo ;
      private bool[] T002H6_n1850CheckList_Impeditivo ;
      private short[] T002H6_A1848CheckList_SeNaoCumpre ;
      private bool[] T002H6_n1848CheckList_SeNaoCumpre ;
      private int[] T002H6_A1846CheckList_NaoCnfCod ;
      private bool[] T002H6_n1846CheckList_NaoCnfCod ;
      private String[] T002H6_A1851CheckList_NaoCnfQdo ;
      private bool[] T002H6_n1851CheckList_NaoCnfQdo ;
      private bool[] T002H6_A1151CheckList_Ativo ;
      private int[] T002H6_A1839Check_Codigo ;
      private bool[] T002H6_n1839Check_Codigo ;
      private short[] T002H7_A1843Check_Momento ;
      private int[] T002H8_A758CheckList_Codigo ;
      private int[] T002H3_A758CheckList_Codigo ;
      private String[] T002H3_A763CheckList_Descricao ;
      private short[] T002H3_A1230CheckList_De ;
      private bool[] T002H3_n1230CheckList_De ;
      private bool[] T002H3_A1845CheckList_Obrigatorio ;
      private bool[] T002H3_n1845CheckList_Obrigatorio ;
      private bool[] T002H3_A1850CheckList_Impeditivo ;
      private bool[] T002H3_n1850CheckList_Impeditivo ;
      private short[] T002H3_A1848CheckList_SeNaoCumpre ;
      private bool[] T002H3_n1848CheckList_SeNaoCumpre ;
      private int[] T002H3_A1846CheckList_NaoCnfCod ;
      private bool[] T002H3_n1846CheckList_NaoCnfCod ;
      private String[] T002H3_A1851CheckList_NaoCnfQdo ;
      private bool[] T002H3_n1851CheckList_NaoCnfQdo ;
      private bool[] T002H3_A1151CheckList_Ativo ;
      private int[] T002H3_A1839Check_Codigo ;
      private bool[] T002H3_n1839Check_Codigo ;
      private int[] T002H9_A758CheckList_Codigo ;
      private int[] T002H10_A758CheckList_Codigo ;
      private int[] T002H2_A758CheckList_Codigo ;
      private String[] T002H2_A763CheckList_Descricao ;
      private short[] T002H2_A1230CheckList_De ;
      private bool[] T002H2_n1230CheckList_De ;
      private bool[] T002H2_A1845CheckList_Obrigatorio ;
      private bool[] T002H2_n1845CheckList_Obrigatorio ;
      private bool[] T002H2_A1850CheckList_Impeditivo ;
      private bool[] T002H2_n1850CheckList_Impeditivo ;
      private short[] T002H2_A1848CheckList_SeNaoCumpre ;
      private bool[] T002H2_n1848CheckList_SeNaoCumpre ;
      private int[] T002H2_A1846CheckList_NaoCnfCod ;
      private bool[] T002H2_n1846CheckList_NaoCnfCod ;
      private String[] T002H2_A1851CheckList_NaoCnfQdo ;
      private bool[] T002H2_n1851CheckList_NaoCnfQdo ;
      private bool[] T002H2_A1151CheckList_Ativo ;
      private int[] T002H2_A1839Check_Codigo ;
      private bool[] T002H2_n1839Check_Codigo ;
      private int[] T002H11_A758CheckList_Codigo ;
      private short[] T002H14_A1843Check_Momento ;
      private int[] T002H15_A820ContagemResultadoChckLstLog_Codigo ;
      private short[] T002H16_A761ContagemResultadoChckLst_Codigo ;
      private int[] T002H17_A758CheckList_Codigo ;
      private int[] T002H18_A1839Check_Codigo ;
      private bool[] T002H18_n1839Check_Codigo ;
      private String[] T002H18_A1841Check_Nome ;
      private int[] T002H19_A426NaoConformidade_Codigo ;
      private String[] T002H19_A427NaoConformidade_Nome ;
      private int[] T002H19_A428NaoConformidade_AreaTrabalhoCod ;
      private short[] T002H20_A1843Check_Momento ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class checklist__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002H5 ;
          prmT002H5 = new Object[] {
          } ;
          Object[] prmT002H6 ;
          prmT002H6 = new Object[] {
          new Object[] {"@CheckList_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002H4 ;
          prmT002H4 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002H7 ;
          prmT002H7 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002H8 ;
          prmT002H8 = new Object[] {
          new Object[] {"@CheckList_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002H3 ;
          prmT002H3 = new Object[] {
          new Object[] {"@CheckList_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002H9 ;
          prmT002H9 = new Object[] {
          new Object[] {"@CheckList_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002H10 ;
          prmT002H10 = new Object[] {
          new Object[] {"@CheckList_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002H2 ;
          prmT002H2 = new Object[] {
          new Object[] {"@CheckList_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002H11 ;
          prmT002H11 = new Object[] {
          new Object[] {"@CheckList_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@CheckList_De",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@CheckList_Obrigatorio",SqlDbType.Bit,4,0} ,
          new Object[] {"@CheckList_Impeditivo",SqlDbType.Bit,4,0} ,
          new Object[] {"@CheckList_SeNaoCumpre",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@CheckList_NaoCnfCod",SqlDbType.Int,6,0} ,
          new Object[] {"@CheckList_NaoCnfQdo",SqlDbType.Char,1,0} ,
          new Object[] {"@CheckList_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002H12 ;
          prmT002H12 = new Object[] {
          new Object[] {"@CheckList_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@CheckList_De",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@CheckList_Obrigatorio",SqlDbType.Bit,4,0} ,
          new Object[] {"@CheckList_Impeditivo",SqlDbType.Bit,4,0} ,
          new Object[] {"@CheckList_SeNaoCumpre",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@CheckList_NaoCnfCod",SqlDbType.Int,6,0} ,
          new Object[] {"@CheckList_NaoCnfQdo",SqlDbType.Char,1,0} ,
          new Object[] {"@CheckList_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@CheckList_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002H13 ;
          prmT002H13 = new Object[] {
          new Object[] {"@CheckList_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002H14 ;
          prmT002H14 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002H15 ;
          prmT002H15 = new Object[] {
          new Object[] {"@CheckList_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002H16 ;
          prmT002H16 = new Object[] {
          new Object[] {"@CheckList_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002H17 ;
          prmT002H17 = new Object[] {
          } ;
          Object[] prmT002H18 ;
          prmT002H18 = new Object[] {
          } ;
          Object[] prmT002H19 ;
          prmT002H19 = new Object[] {
          new Object[] {"@AV8WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002H20 ;
          prmT002H20 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002H2", "SELECT [CheckList_Codigo], [CheckList_Descricao], [CheckList_De], [CheckList_Obrigatorio], [CheckList_Impeditivo], [CheckList_SeNaoCumpre], [CheckList_NaoCnfCod], [CheckList_NaoCnfQdo], [CheckList_Ativo], [Check_Codigo] FROM [CheckList] WITH (UPDLOCK) WHERE [CheckList_Codigo] = @CheckList_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002H2,1,0,true,false )
             ,new CursorDef("T002H3", "SELECT [CheckList_Codigo], [CheckList_Descricao], [CheckList_De], [CheckList_Obrigatorio], [CheckList_Impeditivo], [CheckList_SeNaoCumpre], [CheckList_NaoCnfCod], [CheckList_NaoCnfQdo], [CheckList_Ativo], [Check_Codigo] FROM [CheckList] WITH (NOLOCK) WHERE [CheckList_Codigo] = @CheckList_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002H3,1,0,true,false )
             ,new CursorDef("T002H4", "SELECT [Check_Momento] FROM [Check] WITH (NOLOCK) WHERE [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002H4,1,0,true,false )
             ,new CursorDef("T002H5", "SELECT [Check_Codigo], [Check_Nome] FROM [Check] WITH (NOLOCK) ORDER BY [Check_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002H5,0,0,true,false )
             ,new CursorDef("T002H6", "SELECT TM1.[CheckList_Codigo], T2.[Check_Momento], TM1.[CheckList_Descricao], TM1.[CheckList_De], TM1.[CheckList_Obrigatorio], TM1.[CheckList_Impeditivo], TM1.[CheckList_SeNaoCumpre], TM1.[CheckList_NaoCnfCod], TM1.[CheckList_NaoCnfQdo], TM1.[CheckList_Ativo], TM1.[Check_Codigo] FROM ([CheckList] TM1 WITH (NOLOCK) LEFT JOIN [Check] T2 WITH (NOLOCK) ON T2.[Check_Codigo] = TM1.[Check_Codigo]) WHERE TM1.[CheckList_Codigo] = @CheckList_Codigo ORDER BY TM1.[CheckList_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002H6,100,0,true,false )
             ,new CursorDef("T002H7", "SELECT [Check_Momento] FROM [Check] WITH (NOLOCK) WHERE [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002H7,1,0,true,false )
             ,new CursorDef("T002H8", "SELECT [CheckList_Codigo] FROM [CheckList] WITH (NOLOCK) WHERE [CheckList_Codigo] = @CheckList_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002H8,1,0,true,false )
             ,new CursorDef("T002H9", "SELECT TOP 1 [CheckList_Codigo] FROM [CheckList] WITH (NOLOCK) WHERE ( [CheckList_Codigo] > @CheckList_Codigo) ORDER BY [CheckList_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002H9,1,0,true,true )
             ,new CursorDef("T002H10", "SELECT TOP 1 [CheckList_Codigo] FROM [CheckList] WITH (NOLOCK) WHERE ( [CheckList_Codigo] < @CheckList_Codigo) ORDER BY [CheckList_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002H10,1,0,true,true )
             ,new CursorDef("T002H11", "INSERT INTO [CheckList]([CheckList_Descricao], [CheckList_De], [CheckList_Obrigatorio], [CheckList_Impeditivo], [CheckList_SeNaoCumpre], [CheckList_NaoCnfCod], [CheckList_NaoCnfQdo], [CheckList_Ativo], [Check_Codigo]) VALUES(@CheckList_Descricao, @CheckList_De, @CheckList_Obrigatorio, @CheckList_Impeditivo, @CheckList_SeNaoCumpre, @CheckList_NaoCnfCod, @CheckList_NaoCnfQdo, @CheckList_Ativo, @Check_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002H11)
             ,new CursorDef("T002H12", "UPDATE [CheckList] SET [CheckList_Descricao]=@CheckList_Descricao, [CheckList_De]=@CheckList_De, [CheckList_Obrigatorio]=@CheckList_Obrigatorio, [CheckList_Impeditivo]=@CheckList_Impeditivo, [CheckList_SeNaoCumpre]=@CheckList_SeNaoCumpre, [CheckList_NaoCnfCod]=@CheckList_NaoCnfCod, [CheckList_NaoCnfQdo]=@CheckList_NaoCnfQdo, [CheckList_Ativo]=@CheckList_Ativo, [Check_Codigo]=@Check_Codigo  WHERE [CheckList_Codigo] = @CheckList_Codigo", GxErrorMask.GX_NOMASK,prmT002H12)
             ,new CursorDef("T002H13", "DELETE FROM [CheckList]  WHERE [CheckList_Codigo] = @CheckList_Codigo", GxErrorMask.GX_NOMASK,prmT002H13)
             ,new CursorDef("T002H14", "SELECT [Check_Momento] FROM [Check] WITH (NOLOCK) WHERE [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002H14,1,0,true,false )
             ,new CursorDef("T002H15", "SELECT TOP 1 [ContagemResultadoChckLstLog_Codigo] FROM [ContagemResultadoChckLstLog] WITH (NOLOCK) WHERE [ContagemResultadoChckLstLog_ChckLstCod] = @CheckList_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002H15,1,0,true,true )
             ,new CursorDef("T002H16", "SELECT TOP 1 [ContagemResultadoChckLst_Codigo] FROM [ContagemResultadoChckLst] WITH (NOLOCK) WHERE [CheckList_Codigo] = @CheckList_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002H16,1,0,true,true )
             ,new CursorDef("T002H17", "SELECT [CheckList_Codigo] FROM [CheckList] WITH (NOLOCK) ORDER BY [CheckList_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002H17,100,0,true,false )
             ,new CursorDef("T002H18", "SELECT [Check_Codigo], [Check_Nome] FROM [Check] WITH (NOLOCK) ORDER BY [Check_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002H18,0,0,true,false )
             ,new CursorDef("T002H19", "SELECT [NaoConformidade_Codigo], [NaoConformidade_Nome], [NaoConformidade_AreaTrabalhoCod] FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_AreaTrabalhoCod] = @AV8WWPCo_1Areatrabalho_codigo ORDER BY [NaoConformidade_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002H19,0,0,true,false )
             ,new CursorDef("T002H20", "SELECT [Check_Momento] FROM [Check] WITH (NOLOCK) WHERE [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002H20,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((bool[]) buf[14])[0] = rslt.getBool(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((bool[]) buf[14])[0] = rslt.getBool(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                return;
             case 2 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((bool[]) buf[15])[0] = rslt.getBool(10) ;
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                return;
             case 5 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 18 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[12]);
                }
                stmt.SetParameter(8, (bool)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[15]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[12]);
                }
                stmt.SetParameter(8, (bool)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[15]);
                }
                stmt.SetParameter(10, (int)parms[16]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
