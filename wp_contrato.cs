/*
               File: WP_Contrato
        Description: Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:34:27.74
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_contrato : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_contrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_contrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contrato_Codigo )
      {
         this.AV5Contrato_Codigo = aP0_Contrato_Codigo;
         executePrivate();
         aP0_Contrato_Codigo=this.AV5Contrato_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV5Contrato_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Contrato_Codigo), 6, 0)));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAMK2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTMK2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051813342789");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_contrato.aspx") + "?" + UrlEncode("" +AV5Contrato_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_NUMERO", StringUtil.RTrim( A40000Contrato_Numero));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Wccontratogeneral == null ) )
         {
            WebComp_Wccontratogeneral.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEMK2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTMK2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_contrato.aspx") + "?" + UrlEncode("" +AV5Contrato_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "WP_Contrato" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato" ;
      }

      protected void WBMK0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_MK2( true) ;
         }
         else
         {
            wb_table1_2_MK2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MK2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTMK2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contrato", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPMK0( ) ;
      }

      protected void WSMK2( )
      {
         STARTMK2( ) ;
         EVTMK2( ) ;
      }

      protected void EVTMK2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11MK2 */
                              E11MK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12MK2 */
                              E12MK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 66 )
                        {
                           OldWccontratogeneral = cgiGet( "W0066");
                           if ( ( StringUtil.Len( OldWccontratogeneral) == 0 ) || ( StringUtil.StrCmp(OldWccontratogeneral, WebComp_Wccontratogeneral_Component) != 0 ) )
                           {
                              WebComp_Wccontratogeneral = getWebComponent(GetType(), "GeneXus.Programs", OldWccontratogeneral, new Object[] {context} );
                              WebComp_Wccontratogeneral.ComponentInit();
                              WebComp_Wccontratogeneral.Name = "OldWccontratogeneral";
                              WebComp_Wccontratogeneral_Component = OldWccontratogeneral;
                           }
                           if ( StringUtil.Len( WebComp_Wccontratogeneral_Component) != 0 )
                           {
                              WebComp_Wccontratogeneral.componentprocess("W0066", "", sEvt);
                           }
                           WebComp_Wccontratogeneral_Component = OldWccontratogeneral;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEMK2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAMK2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFMK2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFMK2( )
      {
         initialize_formulas( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Wccontratogeneral_Component) != 0 )
               {
                  WebComp_Wccontratogeneral.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E12MK2 */
            E12MK2 ();
            WBMK0( ) ;
         }
      }

      protected void STRUPMK0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Using cursor H00MK3 */
         pr_default.execute(0, new Object[] {AV5Contrato_Codigo});
         if ( (pr_default.getStatus(0) != 101) )
         {
            A40000Contrato_Numero = H00MK3_A40000Contrato_Numero[0];
         }
         else
         {
            A40000Contrato_Numero = "";
         }
         pr_default.close(0);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11MK2 */
         E11MK2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            AV5Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATO_CODIGO"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11MK2 */
         E11MK2 ();
         if (returnInSub) return;
      }

      protected void E11MK2( )
      {
         /* Start Routine */
         /* Using cursor H00MK5 */
         pr_default.execute(1, new Object[] {AV5Contrato_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            A40000Contrato_Numero = H00MK5_A40000Contrato_Numero[0];
         }
         else
         {
            A40000Contrato_Numero = "";
         }
         pr_default.close(1);
         /* Object Property */
         if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Wccontratogeneral_Component), StringUtil.Lower( "ContratoGeneral")) != 0 )
         {
            WebComp_Wccontratogeneral = getWebComponent(GetType(), "GeneXus.Programs", "contratogeneral", new Object[] {context} );
            WebComp_Wccontratogeneral.ComponentInit();
            WebComp_Wccontratogeneral.Name = "ContratoGeneral";
            WebComp_Wccontratogeneral_Component = "ContratoGeneral";
         }
         if ( StringUtil.Len( WebComp_Wccontratogeneral_Component) != 0 )
         {
            WebComp_Wccontratogeneral.setjustcreated();
            WebComp_Wccontratogeneral.componentprepare(new Object[] {(String)"W0066",(String)"",(int)AV5Contrato_Codigo});
            WebComp_Wccontratogeneral.componentbind(new Object[] {(String)""});
         }
         Form.Headerrawhtml = "<script src=\"https://use.fontawesome.com/3d985f5510.js\"></script>";
         lblTextblocktitle_Caption = "Contrato :: "+A40000Contrato_Numero;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblocktitle_Internalname, "Caption", lblTextblocktitle_Caption);
         lblLblvoltar_Link = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLblvoltar_Internalname, "Link", lblLblvoltar_Link);
         lblLblvoltar_Jsonclick = "window.history.back()";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLblvoltar_Internalname, "Jsonclick", lblLblvoltar_Jsonclick);
      }

      protected void nextLoad( )
      {
      }

      protected void E12MK2( )
      {
         /* Load Routine */
         /* Using cursor H00MK6 */
         pr_default.execute(2, new Object[] {AV5Contrato_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A74Contrato_Codigo = H00MK6_A74Contrato_Codigo[0];
            AV6Count = 0;
            /* Using cursor H00MK7 */
            pr_default.execute(3, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               AV6Count = (short)(AV6Count+1);
               lblTb_clausulas_Caption = "Cl�usulas ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV6Count), 4, 0))+")";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_clausulas_Internalname, "Caption", lblTb_clausulas_Caption);
               pr_default.readNext(3);
            }
            pr_default.close(3);
            AV6Count = 0;
            /* Using cursor H00MK8 */
            pr_default.execute(4, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A1207ContratoUnidades_ContratoCod = H00MK8_A1207ContratoUnidades_ContratoCod[0];
               AV6Count = (short)(AV6Count+1);
               lblTb_usc_Caption = "USC ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV6Count), 4, 0))+")";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_usc_Internalname, "Caption", lblTb_usc_Caption);
               pr_default.readNext(4);
            }
            pr_default.close(4);
            AV6Count = 0;
            /* Using cursor H00MK9 */
            pr_default.execute(5, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               AV6Count = (short)(AV6Count+1);
               lblTb_servicos_Caption = "Servi�os ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV6Count), 4, 0))+")";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_servicos_Internalname, "Caption", lblTb_servicos_Caption);
               pr_default.readNext(5);
            }
            pr_default.close(5);
            AV6Count = 0;
            /* Using cursor H00MK10 */
            pr_default.execute(6, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               AV6Count = (short)(AV6Count+1);
               lblTb_garantias_Caption = "Garantias ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV6Count), 4, 0))+")";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_garantias_Internalname, "Caption", lblTb_garantias_Caption);
               pr_default.readNext(6);
            }
            pr_default.close(6);
            AV6Count = 0;
            /* Using cursor H00MK11 */
            pr_default.execute(7, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(7) != 101) )
            {
               AV6Count = (short)(AV6Count+1);
               lblTb_anexos_Caption = "Anexos ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV6Count), 4, 0))+")";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_anexos_Internalname, "Caption", lblTb_anexos_Caption);
               pr_default.readNext(7);
            }
            pr_default.close(7);
            AV6Count = 0;
            /* Using cursor H00MK12 */
            pr_default.execute(8, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(8) != 101) )
            {
               AV6Count = (short)(AV6Count+1);
               lblTb_dadoscertame_Caption = "Dados Certame ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV6Count), 4, 0))+")";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_dadoscertame_Internalname, "Caption", lblTb_dadoscertame_Caption);
               pr_default.readNext(8);
            }
            pr_default.close(8);
            AV6Count = 0;
            /* Using cursor H00MK13 */
            pr_default.execute(9, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(9) != 101) )
            {
               AV6Count = (short)(AV6Count+1);
               lblTb_termoaditivo_Caption = "Termo Aditivo ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV6Count), 4, 0))+")";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_termoaditivo_Internalname, "Caption", lblTb_termoaditivo_Caption);
               pr_default.readNext(9);
            }
            pr_default.close(9);
            AV6Count = 0;
            /* Using cursor H00MK14 */
            pr_default.execute(10, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(10) != 101) )
            {
               AV6Count = (short)(AV6Count+1);
               lblTb_ocorrencias_Caption = "Ocorr�ncias ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV6Count), 4, 0))+")";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_ocorrencias_Internalname, "Caption", lblTb_ocorrencias_Caption);
               pr_default.readNext(10);
            }
            pr_default.close(10);
            AV6Count = 0;
            /* Using cursor H00MK15 */
            pr_default.execute(11, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(11) != 101) )
            {
               AV6Count = (short)(AV6Count+1);
               lblTb_obrigacoes_Caption = "Obriga��es ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV6Count), 4, 0))+")";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_obrigacoes_Internalname, "Caption", lblTb_obrigacoes_Caption);
               pr_default.readNext(11);
            }
            pr_default.close(11);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
      }

      protected void S112( )
      {
         /* 'LIMPAFOCO' Routine */
         lblTb_contrato_Fontunderline = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_contrato_Internalname, "Fontunderline", StringUtil.Str( (decimal)(lblTb_contrato_Fontunderline), 1, 0));
         lblTb_clausulas_Fontunderline = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_clausulas_Internalname, "Fontunderline", StringUtil.Str( (decimal)(lblTb_clausulas_Fontunderline), 1, 0));
         lblTb_usc_Fontunderline = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_usc_Internalname, "Fontunderline", StringUtil.Str( (decimal)(lblTb_usc_Fontunderline), 1, 0));
         lblTb_servicos_Fontunderline = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_servicos_Internalname, "Fontunderline", StringUtil.Str( (decimal)(lblTb_servicos_Fontunderline), 1, 0));
         lblTb_garantias_Fontunderline = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_garantias_Internalname, "Fontunderline", StringUtil.Str( (decimal)(lblTb_garantias_Fontunderline), 1, 0));
         lblTb_anexos_Fontunderline = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_anexos_Internalname, "Fontunderline", StringUtil.Str( (decimal)(lblTb_anexos_Fontunderline), 1, 0));
         lblTb_dadoscertame_Fontunderline = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_dadoscertame_Internalname, "Fontunderline", StringUtil.Str( (decimal)(lblTb_dadoscertame_Fontunderline), 1, 0));
         lblTb_termoaditivo_Fontunderline = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_termoaditivo_Internalname, "Fontunderline", StringUtil.Str( (decimal)(lblTb_termoaditivo_Fontunderline), 1, 0));
         lblTb_ocorrencias_Fontunderline = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_ocorrencias_Internalname, "Fontunderline", StringUtil.Str( (decimal)(lblTb_ocorrencias_Fontunderline), 1, 0));
         lblTb_notaempenho_Fontunderline = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_notaempenho_Internalname, "Fontunderline", StringUtil.Str( (decimal)(lblTb_notaempenho_Fontunderline), 1, 0));
         lblTb_saldo_Fontunderline = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_saldo_Internalname, "Fontunderline", StringUtil.Str( (decimal)(lblTb_saldo_Fontunderline), 1, 0));
         lblTb_historicoconsumo_Fontunderline = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_historicoconsumo_Internalname, "Fontunderline", StringUtil.Str( (decimal)(lblTb_historicoconsumo_Fontunderline), 1, 0));
         lblTb_caixaentrada_Fontunderline = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTb_caixaentrada_Internalname, "Fontunderline", StringUtil.Str( (decimal)(lblTb_caixaentrada_Fontunderline), 1, 0));
      }

      protected void wb_table1_2_MK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktitle_Internalname, lblTextblocktitle_Caption, "", "", lblTextblocktitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\" class='TableTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblvoltar_Internalname, "Voltar", lblLblvoltar_Link, "", lblLblvoltar_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockLink", 0, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_16_MK2( true) ;
         }
         else
         {
            wb_table2_16_MK2( false) ;
         }
         return  ;
      }

      protected void wb_table2_16_MK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MK2e( true) ;
         }
         else
         {
            wb_table1_2_MK2e( false) ;
         }
      }

      protected void wb_table2_16_MK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table3_19_MK2( true) ;
         }
         else
         {
            wb_table3_19_MK2( false) ;
         }
         return  ;
      }

      protected void wb_table3_19_MK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0066"+"", StringUtil.RTrim( WebComp_Wccontratogeneral_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0066"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Wccontratogeneral_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWccontratogeneral), StringUtil.Lower( WebComp_Wccontratogeneral_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0066"+"");
                  }
                  WebComp_Wccontratogeneral.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWccontratogeneral), StringUtil.Lower( WebComp_Wccontratogeneral_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_16_MK2e( true) ;
         }
         else
         {
            wb_table2_16_MK2e( false) ;
         }
      }

      protected void wb_table3_19_MK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablebotoes_Internalname, tblTablebotoes_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='WP_ContratoMenu'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb_contrato_Internalname, " Contrato", "", "", lblTb_contrato_Jsonclick, "'"+""+"'"+",false,"+"'"+"e13mk1_client"+"'", ((0==0)&&(lblTb_contrato_Fontunderline==1) ? "text-decoration:underline;" : ""), "fa fa-book", 7, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='WP_ContratoMenu'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb_clausulas_Internalname, lblTb_clausulas_Caption, "", "", lblTb_clausulas_Jsonclick, "'"+""+"'"+",false,"+"'"+"e14mk1_client"+"'", ((0==0)&&(lblTb_clausulas_Fontunderline==1) ? "text-decoration:underline;" : ""), "fa fa-briefcase", 7, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='WP_ContratoMenu'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb_usc_Internalname, lblTb_usc_Caption, "", "", lblTb_usc_Jsonclick, "'"+""+"'"+",false,"+"'"+"e15mk1_client"+"'", ((0==0)&&(lblTb_usc_Fontunderline==1) ? "text-decoration:underline;" : ""), "fa fa-calculator", 7, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='WP_ContratoMenu'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb_servicos_Internalname, lblTb_servicos_Caption, "", "", lblTb_servicos_Jsonclick, "'"+""+"'"+",false,"+"'"+"e16mk1_client"+"'", ((0==0)&&(lblTb_servicos_Fontunderline==1) ? "text-decoration:underline;" : ""), "fa fa-list-ul", 7, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='WP_ContratoMenu'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb_garantias_Internalname, lblTb_garantias_Caption, "", "", lblTb_garantias_Jsonclick, "'"+""+"'"+",false,"+"'"+"e17mk1_client"+"'", ((0==0)&&(lblTb_garantias_Fontunderline==1) ? "text-decoration:underline;" : ""), "fa fa-usd", 7, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='WP_ContratoMenu'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb_anexos_Internalname, lblTb_anexos_Caption, "", "", lblTb_anexos_Jsonclick, "'"+""+"'"+",false,"+"'"+"e18mk1_client"+"'", ((0==0)&&(lblTb_anexos_Fontunderline==1) ? "text-decoration:underline;" : ""), "fa fa-paperclip", 7, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='WP_ContratoMenu'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb_dadoscertame_Internalname, lblTb_dadoscertame_Caption, "", "", lblTb_dadoscertame_Jsonclick, "'"+""+"'"+",false,"+"'"+"e19mk1_client"+"'", ((0==0)&&(lblTb_dadoscertame_Fontunderline==1) ? "text-decoration:underline;" : ""), "fa fa-file", 7, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='WP_ContratoMenu'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb_ocorrencias_Internalname, lblTb_ocorrencias_Caption, "", "", lblTb_ocorrencias_Jsonclick, "'"+""+"'"+",false,"+"'"+"e20mk1_client"+"'", ((0==0)&&(lblTb_ocorrencias_Fontunderline==1) ? "text-decoration:underline;" : ""), "fa fa-comments", 7, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='WP_ContratoMenu'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb_obrigacoes_Internalname, lblTb_obrigacoes_Caption, "", "", lblTb_obrigacoes_Jsonclick, "'"+""+"'"+",false,"+"'"+"e21mk1_client"+"'", ((0==0)&&(lblTb_obrigacoes_Fontunderline==1) ? "text-decoration:underline;" : ""), "fa fa-credit-card", 7, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='WP_ContratoMenu'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb_saldo_Internalname, " Saldo", "", "", lblTb_saldo_Jsonclick, "'"+""+"'"+",false,"+"'"+"e22mk1_client"+"'", ((0==0)&&(lblTb_saldo_Fontunderline==1) ? "text-decoration:underline;" : ""), "fa fa-line-chart", 7, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='WP_ContratoMenu'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb_termoaditivo_Internalname, lblTb_termoaditivo_Caption, "", "", lblTb_termoaditivo_Jsonclick, "'"+""+"'"+",false,"+"'"+"e23mk1_client"+"'", ((0==0)&&(lblTb_termoaditivo_Fontunderline==1) ? "text-decoration:underline;" : ""), "fa fa-files-o", 7, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='WP_ContratoMenu'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb_notaempenho_Internalname, " Notas de Empenho", "", "", lblTb_notaempenho_Jsonclick, "'"+""+"'"+",false,"+"'"+"e24mk1_client"+"'", ((0==0)&&(lblTb_notaempenho_Fontunderline==1) ? "text-decoration:underline;" : ""), "fa fa-folder-open", 7, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='WP_ContratoMenu'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb_autorizacaoconsumo_Internalname, "Autoriza��o de Consumo", "", "", lblTb_autorizacaoconsumo_Jsonclick, "'"+""+"'"+",false,"+"'"+"e25mk1_client"+"'", ((0==0)&&(lblTb_autorizacaoconsumo_Fontunderline==1) ? "text-decoration:underline;" : ""), "fa fa-calculator", 7, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='WP_ContratoMenu'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb_historicoconsumo_Internalname, " Hist�rico Consumo", "", "", lblTb_historicoconsumo_Jsonclick, "'"+""+"'"+",false,"+"'"+"e26mk1_client"+"'", ((0==0)&&(lblTb_historicoconsumo_Fontunderline==1) ? "text-decoration:underline;" : ""), "fa fa-area-chart", 7, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='WP_ContratoMenu'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb_caixaentrada_Internalname, "Configura��es da Caixa de Entrada", "", "", lblTb_caixaentrada_Jsonclick, "'"+""+"'"+",false,"+"'"+"e27mk1_client"+"'", ((0==0)&&(lblTb_caixaentrada_Fontunderline==1) ? "text-decoration:underline;" : ""), "fa fa-area-chart", 7, "", 1, 1, 0, "HLP_WP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_19_MK2e( true) ;
         }
         else
         {
            wb_table3_19_MK2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV5Contrato_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Contrato_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMK2( ) ;
         WSMK2( ) ;
         WEMK2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         if ( ! ( WebComp_Wccontratogeneral == null ) )
         {
            if ( StringUtil.Len( WebComp_Wccontratogeneral_Component) != 0 )
            {
               WebComp_Wccontratogeneral.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051813342850");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_contrato.js", "?202051813342851");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocktitle_Internalname = "TEXTBLOCKTITLE";
         lblLblvoltar_Internalname = "LBLVOLTAR";
         lblTb_contrato_Internalname = "TB_CONTRATO";
         lblTb_clausulas_Internalname = "TB_CLAUSULAS";
         lblTb_usc_Internalname = "TB_USC";
         lblTb_servicos_Internalname = "TB_SERVICOS";
         lblTb_garantias_Internalname = "TB_GARANTIAS";
         lblTb_anexos_Internalname = "TB_ANEXOS";
         lblTb_dadoscertame_Internalname = "TB_DADOSCERTAME";
         lblTb_ocorrencias_Internalname = "TB_OCORRENCIAS";
         lblTb_obrigacoes_Internalname = "TB_OBRIGACOES";
         lblTb_saldo_Internalname = "TB_SALDO";
         lblTb_termoaditivo_Internalname = "TB_TERMOADITIVO";
         lblTb_notaempenho_Internalname = "TB_NOTAEMPENHO";
         lblTb_autorizacaoconsumo_Internalname = "TB_AUTORIZACAOCONSUMO";
         lblTb_historicoconsumo_Internalname = "TB_HISTORICOCONSUMO";
         lblTb_caixaentrada_Internalname = "TB_CAIXAENTRADA";
         tblTablebotoes_Internalname = "TABLEBOTOES";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         lblTb_autorizacaoconsumo_Fontunderline = 0;
         lblTb_obrigacoes_Fontunderline = 0;
         lblLblvoltar_Link = "";
         lblTb_caixaentrada_Fontunderline = 0;
         lblTb_historicoconsumo_Fontunderline = 0;
         lblTb_saldo_Fontunderline = 0;
         lblTb_notaempenho_Fontunderline = 0;
         lblTb_ocorrencias_Fontunderline = 0;
         lblTb_termoaditivo_Fontunderline = 0;
         lblTb_dadoscertame_Fontunderline = 0;
         lblTb_anexos_Fontunderline = 0;
         lblTb_garantias_Fontunderline = 0;
         lblTb_servicos_Fontunderline = 0;
         lblTb_usc_Fontunderline = 0;
         lblTb_clausulas_Fontunderline = 0;
         lblTb_contrato_Fontunderline = 0;
         lblTb_obrigacoes_Caption = " Obriga��es";
         lblTb_ocorrencias_Caption = " Ocorr�ncias";
         lblTb_termoaditivo_Caption = "  Termo Aditivo";
         lblTb_dadoscertame_Caption = " Dados Certame";
         lblTb_anexos_Caption = " Anexos";
         lblTb_garantias_Caption = " Garantias";
         lblTb_servicos_Caption = " Servi�os";
         lblTb_usc_Caption = " USC";
         lblTb_clausulas_Caption = " Cl�usulas";
         lblTextblocktitle_Caption = "Contrato :: 8888888888888";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contrato";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("TB_CONTRATO.CLICK","{handler:'E13MK1',iparms:[{av:'AV5Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'WCCONTRATOGENERAL'},{av:'lblTb_contrato_Fontunderline',ctrl:'TB_CONTRATO',prop:'Fontunderline'},{av:'lblTb_clausulas_Fontunderline',ctrl:'TB_CLAUSULAS',prop:'Fontunderline'},{av:'lblTb_usc_Fontunderline',ctrl:'TB_USC',prop:'Fontunderline'},{av:'lblTb_servicos_Fontunderline',ctrl:'TB_SERVICOS',prop:'Fontunderline'},{av:'lblTb_garantias_Fontunderline',ctrl:'TB_GARANTIAS',prop:'Fontunderline'},{av:'lblTb_anexos_Fontunderline',ctrl:'TB_ANEXOS',prop:'Fontunderline'},{av:'lblTb_dadoscertame_Fontunderline',ctrl:'TB_DADOSCERTAME',prop:'Fontunderline'},{av:'lblTb_termoaditivo_Fontunderline',ctrl:'TB_TERMOADITIVO',prop:'Fontunderline'},{av:'lblTb_ocorrencias_Fontunderline',ctrl:'TB_OCORRENCIAS',prop:'Fontunderline'},{av:'lblTb_notaempenho_Fontunderline',ctrl:'TB_NOTAEMPENHO',prop:'Fontunderline'},{av:'lblTb_saldo_Fontunderline',ctrl:'TB_SALDO',prop:'Fontunderline'},{av:'lblTb_historicoconsumo_Fontunderline',ctrl:'TB_HISTORICOCONSUMO',prop:'Fontunderline'},{av:'lblTb_caixaentrada_Fontunderline',ctrl:'TB_CAIXAENTRADA',prop:'Fontunderline'}]}");
         setEventMetadata("TB_CLAUSULAS.CLICK","{handler:'E14MK1',iparms:[{av:'AV5Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'WCCONTRATOGENERAL'},{av:'lblTb_clausulas_Fontunderline',ctrl:'TB_CLAUSULAS',prop:'Fontunderline'},{av:'lblTb_contrato_Fontunderline',ctrl:'TB_CONTRATO',prop:'Fontunderline'},{av:'lblTb_usc_Fontunderline',ctrl:'TB_USC',prop:'Fontunderline'},{av:'lblTb_servicos_Fontunderline',ctrl:'TB_SERVICOS',prop:'Fontunderline'},{av:'lblTb_garantias_Fontunderline',ctrl:'TB_GARANTIAS',prop:'Fontunderline'},{av:'lblTb_anexos_Fontunderline',ctrl:'TB_ANEXOS',prop:'Fontunderline'},{av:'lblTb_dadoscertame_Fontunderline',ctrl:'TB_DADOSCERTAME',prop:'Fontunderline'},{av:'lblTb_termoaditivo_Fontunderline',ctrl:'TB_TERMOADITIVO',prop:'Fontunderline'},{av:'lblTb_ocorrencias_Fontunderline',ctrl:'TB_OCORRENCIAS',prop:'Fontunderline'},{av:'lblTb_notaempenho_Fontunderline',ctrl:'TB_NOTAEMPENHO',prop:'Fontunderline'},{av:'lblTb_saldo_Fontunderline',ctrl:'TB_SALDO',prop:'Fontunderline'},{av:'lblTb_historicoconsumo_Fontunderline',ctrl:'TB_HISTORICOCONSUMO',prop:'Fontunderline'},{av:'lblTb_caixaentrada_Fontunderline',ctrl:'TB_CAIXAENTRADA',prop:'Fontunderline'}]}");
         setEventMetadata("TB_USC.CLICK","{handler:'E15MK1',iparms:[{av:'AV5Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'WCCONTRATOGENERAL'},{av:'lblTb_usc_Fontunderline',ctrl:'TB_USC',prop:'Fontunderline'},{av:'lblTb_contrato_Fontunderline',ctrl:'TB_CONTRATO',prop:'Fontunderline'},{av:'lblTb_clausulas_Fontunderline',ctrl:'TB_CLAUSULAS',prop:'Fontunderline'},{av:'lblTb_servicos_Fontunderline',ctrl:'TB_SERVICOS',prop:'Fontunderline'},{av:'lblTb_garantias_Fontunderline',ctrl:'TB_GARANTIAS',prop:'Fontunderline'},{av:'lblTb_anexos_Fontunderline',ctrl:'TB_ANEXOS',prop:'Fontunderline'},{av:'lblTb_dadoscertame_Fontunderline',ctrl:'TB_DADOSCERTAME',prop:'Fontunderline'},{av:'lblTb_termoaditivo_Fontunderline',ctrl:'TB_TERMOADITIVO',prop:'Fontunderline'},{av:'lblTb_ocorrencias_Fontunderline',ctrl:'TB_OCORRENCIAS',prop:'Fontunderline'},{av:'lblTb_notaempenho_Fontunderline',ctrl:'TB_NOTAEMPENHO',prop:'Fontunderline'},{av:'lblTb_saldo_Fontunderline',ctrl:'TB_SALDO',prop:'Fontunderline'},{av:'lblTb_historicoconsumo_Fontunderline',ctrl:'TB_HISTORICOCONSUMO',prop:'Fontunderline'},{av:'lblTb_caixaentrada_Fontunderline',ctrl:'TB_CAIXAENTRADA',prop:'Fontunderline'}]}");
         setEventMetadata("TB_SERVICOS.CLICK","{handler:'E16MK1',iparms:[{av:'AV5Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'WCCONTRATOGENERAL'},{av:'lblTb_servicos_Fontunderline',ctrl:'TB_SERVICOS',prop:'Fontunderline'},{av:'lblTb_contrato_Fontunderline',ctrl:'TB_CONTRATO',prop:'Fontunderline'},{av:'lblTb_clausulas_Fontunderline',ctrl:'TB_CLAUSULAS',prop:'Fontunderline'},{av:'lblTb_usc_Fontunderline',ctrl:'TB_USC',prop:'Fontunderline'},{av:'lblTb_garantias_Fontunderline',ctrl:'TB_GARANTIAS',prop:'Fontunderline'},{av:'lblTb_anexos_Fontunderline',ctrl:'TB_ANEXOS',prop:'Fontunderline'},{av:'lblTb_dadoscertame_Fontunderline',ctrl:'TB_DADOSCERTAME',prop:'Fontunderline'},{av:'lblTb_termoaditivo_Fontunderline',ctrl:'TB_TERMOADITIVO',prop:'Fontunderline'},{av:'lblTb_ocorrencias_Fontunderline',ctrl:'TB_OCORRENCIAS',prop:'Fontunderline'},{av:'lblTb_notaempenho_Fontunderline',ctrl:'TB_NOTAEMPENHO',prop:'Fontunderline'},{av:'lblTb_saldo_Fontunderline',ctrl:'TB_SALDO',prop:'Fontunderline'},{av:'lblTb_historicoconsumo_Fontunderline',ctrl:'TB_HISTORICOCONSUMO',prop:'Fontunderline'},{av:'lblTb_caixaentrada_Fontunderline',ctrl:'TB_CAIXAENTRADA',prop:'Fontunderline'}]}");
         setEventMetadata("TB_GARANTIAS.CLICK","{handler:'E17MK1',iparms:[{av:'AV5Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'WCCONTRATOGENERAL'},{av:'lblTb_garantias_Fontunderline',ctrl:'TB_GARANTIAS',prop:'Fontunderline'},{av:'lblTb_contrato_Fontunderline',ctrl:'TB_CONTRATO',prop:'Fontunderline'},{av:'lblTb_clausulas_Fontunderline',ctrl:'TB_CLAUSULAS',prop:'Fontunderline'},{av:'lblTb_usc_Fontunderline',ctrl:'TB_USC',prop:'Fontunderline'},{av:'lblTb_servicos_Fontunderline',ctrl:'TB_SERVICOS',prop:'Fontunderline'},{av:'lblTb_anexos_Fontunderline',ctrl:'TB_ANEXOS',prop:'Fontunderline'},{av:'lblTb_dadoscertame_Fontunderline',ctrl:'TB_DADOSCERTAME',prop:'Fontunderline'},{av:'lblTb_termoaditivo_Fontunderline',ctrl:'TB_TERMOADITIVO',prop:'Fontunderline'},{av:'lblTb_ocorrencias_Fontunderline',ctrl:'TB_OCORRENCIAS',prop:'Fontunderline'},{av:'lblTb_notaempenho_Fontunderline',ctrl:'TB_NOTAEMPENHO',prop:'Fontunderline'},{av:'lblTb_saldo_Fontunderline',ctrl:'TB_SALDO',prop:'Fontunderline'},{av:'lblTb_historicoconsumo_Fontunderline',ctrl:'TB_HISTORICOCONSUMO',prop:'Fontunderline'},{av:'lblTb_caixaentrada_Fontunderline',ctrl:'TB_CAIXAENTRADA',prop:'Fontunderline'}]}");
         setEventMetadata("TB_ANEXOS.CLICK","{handler:'E18MK1',iparms:[{av:'AV5Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'WCCONTRATOGENERAL'},{av:'lblTb_anexos_Fontunderline',ctrl:'TB_ANEXOS',prop:'Fontunderline'},{av:'lblTb_contrato_Fontunderline',ctrl:'TB_CONTRATO',prop:'Fontunderline'},{av:'lblTb_clausulas_Fontunderline',ctrl:'TB_CLAUSULAS',prop:'Fontunderline'},{av:'lblTb_usc_Fontunderline',ctrl:'TB_USC',prop:'Fontunderline'},{av:'lblTb_servicos_Fontunderline',ctrl:'TB_SERVICOS',prop:'Fontunderline'},{av:'lblTb_garantias_Fontunderline',ctrl:'TB_GARANTIAS',prop:'Fontunderline'},{av:'lblTb_dadoscertame_Fontunderline',ctrl:'TB_DADOSCERTAME',prop:'Fontunderline'},{av:'lblTb_termoaditivo_Fontunderline',ctrl:'TB_TERMOADITIVO',prop:'Fontunderline'},{av:'lblTb_ocorrencias_Fontunderline',ctrl:'TB_OCORRENCIAS',prop:'Fontunderline'},{av:'lblTb_notaempenho_Fontunderline',ctrl:'TB_NOTAEMPENHO',prop:'Fontunderline'},{av:'lblTb_saldo_Fontunderline',ctrl:'TB_SALDO',prop:'Fontunderline'},{av:'lblTb_historicoconsumo_Fontunderline',ctrl:'TB_HISTORICOCONSUMO',prop:'Fontunderline'},{av:'lblTb_caixaentrada_Fontunderline',ctrl:'TB_CAIXAENTRADA',prop:'Fontunderline'}]}");
         setEventMetadata("TB_DADOSCERTAME.CLICK","{handler:'E19MK1',iparms:[{av:'AV5Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'WCCONTRATOGENERAL'},{av:'lblTb_dadoscertame_Fontunderline',ctrl:'TB_DADOSCERTAME',prop:'Fontunderline'},{av:'lblTb_contrato_Fontunderline',ctrl:'TB_CONTRATO',prop:'Fontunderline'},{av:'lblTb_clausulas_Fontunderline',ctrl:'TB_CLAUSULAS',prop:'Fontunderline'},{av:'lblTb_usc_Fontunderline',ctrl:'TB_USC',prop:'Fontunderline'},{av:'lblTb_servicos_Fontunderline',ctrl:'TB_SERVICOS',prop:'Fontunderline'},{av:'lblTb_garantias_Fontunderline',ctrl:'TB_GARANTIAS',prop:'Fontunderline'},{av:'lblTb_anexos_Fontunderline',ctrl:'TB_ANEXOS',prop:'Fontunderline'},{av:'lblTb_termoaditivo_Fontunderline',ctrl:'TB_TERMOADITIVO',prop:'Fontunderline'},{av:'lblTb_ocorrencias_Fontunderline',ctrl:'TB_OCORRENCIAS',prop:'Fontunderline'},{av:'lblTb_notaempenho_Fontunderline',ctrl:'TB_NOTAEMPENHO',prop:'Fontunderline'},{av:'lblTb_saldo_Fontunderline',ctrl:'TB_SALDO',prop:'Fontunderline'},{av:'lblTb_historicoconsumo_Fontunderline',ctrl:'TB_HISTORICOCONSUMO',prop:'Fontunderline'},{av:'lblTb_caixaentrada_Fontunderline',ctrl:'TB_CAIXAENTRADA',prop:'Fontunderline'}]}");
         setEventMetadata("TB_TERMOADITIVO.CLICK","{handler:'E23MK1',iparms:[{av:'AV5Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'WCCONTRATOGENERAL'},{av:'lblTb_termoaditivo_Fontunderline',ctrl:'TB_TERMOADITIVO',prop:'Fontunderline'},{av:'lblTb_contrato_Fontunderline',ctrl:'TB_CONTRATO',prop:'Fontunderline'},{av:'lblTb_clausulas_Fontunderline',ctrl:'TB_CLAUSULAS',prop:'Fontunderline'},{av:'lblTb_usc_Fontunderline',ctrl:'TB_USC',prop:'Fontunderline'},{av:'lblTb_servicos_Fontunderline',ctrl:'TB_SERVICOS',prop:'Fontunderline'},{av:'lblTb_garantias_Fontunderline',ctrl:'TB_GARANTIAS',prop:'Fontunderline'},{av:'lblTb_anexos_Fontunderline',ctrl:'TB_ANEXOS',prop:'Fontunderline'},{av:'lblTb_dadoscertame_Fontunderline',ctrl:'TB_DADOSCERTAME',prop:'Fontunderline'},{av:'lblTb_ocorrencias_Fontunderline',ctrl:'TB_OCORRENCIAS',prop:'Fontunderline'},{av:'lblTb_notaempenho_Fontunderline',ctrl:'TB_NOTAEMPENHO',prop:'Fontunderline'},{av:'lblTb_saldo_Fontunderline',ctrl:'TB_SALDO',prop:'Fontunderline'},{av:'lblTb_historicoconsumo_Fontunderline',ctrl:'TB_HISTORICOCONSUMO',prop:'Fontunderline'},{av:'lblTb_caixaentrada_Fontunderline',ctrl:'TB_CAIXAENTRADA',prop:'Fontunderline'}]}");
         setEventMetadata("TB_OCORRENCIAS.CLICK","{handler:'E20MK1',iparms:[{av:'AV5Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'WCCONTRATOGENERAL'},{av:'lblTb_ocorrencias_Fontunderline',ctrl:'TB_OCORRENCIAS',prop:'Fontunderline'},{av:'lblTb_contrato_Fontunderline',ctrl:'TB_CONTRATO',prop:'Fontunderline'},{av:'lblTb_clausulas_Fontunderline',ctrl:'TB_CLAUSULAS',prop:'Fontunderline'},{av:'lblTb_usc_Fontunderline',ctrl:'TB_USC',prop:'Fontunderline'},{av:'lblTb_servicos_Fontunderline',ctrl:'TB_SERVICOS',prop:'Fontunderline'},{av:'lblTb_garantias_Fontunderline',ctrl:'TB_GARANTIAS',prop:'Fontunderline'},{av:'lblTb_anexos_Fontunderline',ctrl:'TB_ANEXOS',prop:'Fontunderline'},{av:'lblTb_dadoscertame_Fontunderline',ctrl:'TB_DADOSCERTAME',prop:'Fontunderline'},{av:'lblTb_termoaditivo_Fontunderline',ctrl:'TB_TERMOADITIVO',prop:'Fontunderline'},{av:'lblTb_notaempenho_Fontunderline',ctrl:'TB_NOTAEMPENHO',prop:'Fontunderline'},{av:'lblTb_saldo_Fontunderline',ctrl:'TB_SALDO',prop:'Fontunderline'},{av:'lblTb_historicoconsumo_Fontunderline',ctrl:'TB_HISTORICOCONSUMO',prop:'Fontunderline'},{av:'lblTb_caixaentrada_Fontunderline',ctrl:'TB_CAIXAENTRADA',prop:'Fontunderline'}]}");
         setEventMetadata("TB_OBRIGACOES.CLICK","{handler:'E21MK1',iparms:[{av:'AV5Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'WCCONTRATOGENERAL'},{av:'lblTb_obrigacoes_Fontunderline',ctrl:'TB_OBRIGACOES',prop:'Fontunderline'},{av:'lblTb_contrato_Fontunderline',ctrl:'TB_CONTRATO',prop:'Fontunderline'},{av:'lblTb_clausulas_Fontunderline',ctrl:'TB_CLAUSULAS',prop:'Fontunderline'},{av:'lblTb_usc_Fontunderline',ctrl:'TB_USC',prop:'Fontunderline'},{av:'lblTb_servicos_Fontunderline',ctrl:'TB_SERVICOS',prop:'Fontunderline'},{av:'lblTb_garantias_Fontunderline',ctrl:'TB_GARANTIAS',prop:'Fontunderline'},{av:'lblTb_anexos_Fontunderline',ctrl:'TB_ANEXOS',prop:'Fontunderline'},{av:'lblTb_dadoscertame_Fontunderline',ctrl:'TB_DADOSCERTAME',prop:'Fontunderline'},{av:'lblTb_termoaditivo_Fontunderline',ctrl:'TB_TERMOADITIVO',prop:'Fontunderline'},{av:'lblTb_ocorrencias_Fontunderline',ctrl:'TB_OCORRENCIAS',prop:'Fontunderline'},{av:'lblTb_notaempenho_Fontunderline',ctrl:'TB_NOTAEMPENHO',prop:'Fontunderline'},{av:'lblTb_saldo_Fontunderline',ctrl:'TB_SALDO',prop:'Fontunderline'},{av:'lblTb_historicoconsumo_Fontunderline',ctrl:'TB_HISTORICOCONSUMO',prop:'Fontunderline'},{av:'lblTb_caixaentrada_Fontunderline',ctrl:'TB_CAIXAENTRADA',prop:'Fontunderline'}]}");
         setEventMetadata("TB_NOTAEMPENHO.CLICK","{handler:'E24MK1',iparms:[{av:'AV5Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'WCCONTRATOGENERAL'},{av:'lblTb_notaempenho_Fontunderline',ctrl:'TB_NOTAEMPENHO',prop:'Fontunderline'},{av:'lblTb_contrato_Fontunderline',ctrl:'TB_CONTRATO',prop:'Fontunderline'},{av:'lblTb_clausulas_Fontunderline',ctrl:'TB_CLAUSULAS',prop:'Fontunderline'},{av:'lblTb_usc_Fontunderline',ctrl:'TB_USC',prop:'Fontunderline'},{av:'lblTb_servicos_Fontunderline',ctrl:'TB_SERVICOS',prop:'Fontunderline'},{av:'lblTb_garantias_Fontunderline',ctrl:'TB_GARANTIAS',prop:'Fontunderline'},{av:'lblTb_anexos_Fontunderline',ctrl:'TB_ANEXOS',prop:'Fontunderline'},{av:'lblTb_dadoscertame_Fontunderline',ctrl:'TB_DADOSCERTAME',prop:'Fontunderline'},{av:'lblTb_termoaditivo_Fontunderline',ctrl:'TB_TERMOADITIVO',prop:'Fontunderline'},{av:'lblTb_ocorrencias_Fontunderline',ctrl:'TB_OCORRENCIAS',prop:'Fontunderline'},{av:'lblTb_saldo_Fontunderline',ctrl:'TB_SALDO',prop:'Fontunderline'},{av:'lblTb_historicoconsumo_Fontunderline',ctrl:'TB_HISTORICOCONSUMO',prop:'Fontunderline'},{av:'lblTb_caixaentrada_Fontunderline',ctrl:'TB_CAIXAENTRADA',prop:'Fontunderline'}]}");
         setEventMetadata("TB_SALDO.CLICK","{handler:'E22MK1',iparms:[{av:'AV5Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'WCCONTRATOGENERAL'},{av:'lblTb_saldo_Fontunderline',ctrl:'TB_SALDO',prop:'Fontunderline'},{av:'lblTb_contrato_Fontunderline',ctrl:'TB_CONTRATO',prop:'Fontunderline'},{av:'lblTb_clausulas_Fontunderline',ctrl:'TB_CLAUSULAS',prop:'Fontunderline'},{av:'lblTb_usc_Fontunderline',ctrl:'TB_USC',prop:'Fontunderline'},{av:'lblTb_servicos_Fontunderline',ctrl:'TB_SERVICOS',prop:'Fontunderline'},{av:'lblTb_garantias_Fontunderline',ctrl:'TB_GARANTIAS',prop:'Fontunderline'},{av:'lblTb_anexos_Fontunderline',ctrl:'TB_ANEXOS',prop:'Fontunderline'},{av:'lblTb_dadoscertame_Fontunderline',ctrl:'TB_DADOSCERTAME',prop:'Fontunderline'},{av:'lblTb_termoaditivo_Fontunderline',ctrl:'TB_TERMOADITIVO',prop:'Fontunderline'},{av:'lblTb_ocorrencias_Fontunderline',ctrl:'TB_OCORRENCIAS',prop:'Fontunderline'},{av:'lblTb_notaempenho_Fontunderline',ctrl:'TB_NOTAEMPENHO',prop:'Fontunderline'},{av:'lblTb_historicoconsumo_Fontunderline',ctrl:'TB_HISTORICOCONSUMO',prop:'Fontunderline'},{av:'lblTb_caixaentrada_Fontunderline',ctrl:'TB_CAIXAENTRADA',prop:'Fontunderline'}]}");
         setEventMetadata("TB_HISTORICOCONSUMO.CLICK","{handler:'E26MK1',iparms:[{av:'AV5Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'WCCONTRATOGENERAL'},{av:'lblTb_historicoconsumo_Fontunderline',ctrl:'TB_HISTORICOCONSUMO',prop:'Fontunderline'},{av:'lblTb_contrato_Fontunderline',ctrl:'TB_CONTRATO',prop:'Fontunderline'},{av:'lblTb_clausulas_Fontunderline',ctrl:'TB_CLAUSULAS',prop:'Fontunderline'},{av:'lblTb_usc_Fontunderline',ctrl:'TB_USC',prop:'Fontunderline'},{av:'lblTb_servicos_Fontunderline',ctrl:'TB_SERVICOS',prop:'Fontunderline'},{av:'lblTb_garantias_Fontunderline',ctrl:'TB_GARANTIAS',prop:'Fontunderline'},{av:'lblTb_anexos_Fontunderline',ctrl:'TB_ANEXOS',prop:'Fontunderline'},{av:'lblTb_dadoscertame_Fontunderline',ctrl:'TB_DADOSCERTAME',prop:'Fontunderline'},{av:'lblTb_termoaditivo_Fontunderline',ctrl:'TB_TERMOADITIVO',prop:'Fontunderline'},{av:'lblTb_ocorrencias_Fontunderline',ctrl:'TB_OCORRENCIAS',prop:'Fontunderline'},{av:'lblTb_notaempenho_Fontunderline',ctrl:'TB_NOTAEMPENHO',prop:'Fontunderline'},{av:'lblTb_saldo_Fontunderline',ctrl:'TB_SALDO',prop:'Fontunderline'},{av:'lblTb_caixaentrada_Fontunderline',ctrl:'TB_CAIXAENTRADA',prop:'Fontunderline'}]}");
         setEventMetadata("TB_AUTORIZACAOCONSUMO.CLICK","{handler:'E25MK1',iparms:[{av:'AV5Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'WCCONTRATOGENERAL'},{av:'lblTb_autorizacaoconsumo_Fontunderline',ctrl:'TB_AUTORIZACAOCONSUMO',prop:'Fontunderline'},{av:'lblTb_contrato_Fontunderline',ctrl:'TB_CONTRATO',prop:'Fontunderline'},{av:'lblTb_clausulas_Fontunderline',ctrl:'TB_CLAUSULAS',prop:'Fontunderline'},{av:'lblTb_usc_Fontunderline',ctrl:'TB_USC',prop:'Fontunderline'},{av:'lblTb_servicos_Fontunderline',ctrl:'TB_SERVICOS',prop:'Fontunderline'},{av:'lblTb_garantias_Fontunderline',ctrl:'TB_GARANTIAS',prop:'Fontunderline'},{av:'lblTb_anexos_Fontunderline',ctrl:'TB_ANEXOS',prop:'Fontunderline'},{av:'lblTb_dadoscertame_Fontunderline',ctrl:'TB_DADOSCERTAME',prop:'Fontunderline'},{av:'lblTb_termoaditivo_Fontunderline',ctrl:'TB_TERMOADITIVO',prop:'Fontunderline'},{av:'lblTb_ocorrencias_Fontunderline',ctrl:'TB_OCORRENCIAS',prop:'Fontunderline'},{av:'lblTb_notaempenho_Fontunderline',ctrl:'TB_NOTAEMPENHO',prop:'Fontunderline'},{av:'lblTb_saldo_Fontunderline',ctrl:'TB_SALDO',prop:'Fontunderline'},{av:'lblTb_historicoconsumo_Fontunderline',ctrl:'TB_HISTORICOCONSUMO',prop:'Fontunderline'},{av:'lblTb_caixaentrada_Fontunderline',ctrl:'TB_CAIXAENTRADA',prop:'Fontunderline'}]}");
         setEventMetadata("TB_CAIXAENTRADA.CLICK","{handler:'E27MK1',iparms:[{av:'AV5Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'WCCONTRATOGENERAL'},{av:'lblTb_caixaentrada_Fontunderline',ctrl:'TB_CAIXAENTRADA',prop:'Fontunderline'},{av:'lblTb_contrato_Fontunderline',ctrl:'TB_CONTRATO',prop:'Fontunderline'},{av:'lblTb_clausulas_Fontunderline',ctrl:'TB_CLAUSULAS',prop:'Fontunderline'},{av:'lblTb_usc_Fontunderline',ctrl:'TB_USC',prop:'Fontunderline'},{av:'lblTb_servicos_Fontunderline',ctrl:'TB_SERVICOS',prop:'Fontunderline'},{av:'lblTb_garantias_Fontunderline',ctrl:'TB_GARANTIAS',prop:'Fontunderline'},{av:'lblTb_anexos_Fontunderline',ctrl:'TB_ANEXOS',prop:'Fontunderline'},{av:'lblTb_dadoscertame_Fontunderline',ctrl:'TB_DADOSCERTAME',prop:'Fontunderline'},{av:'lblTb_termoaditivo_Fontunderline',ctrl:'TB_TERMOADITIVO',prop:'Fontunderline'},{av:'lblTb_ocorrencias_Fontunderline',ctrl:'TB_OCORRENCIAS',prop:'Fontunderline'},{av:'lblTb_notaempenho_Fontunderline',ctrl:'TB_NOTAEMPENHO',prop:'Fontunderline'},{av:'lblTb_saldo_Fontunderline',ctrl:'TB_SALDO',prop:'Fontunderline'},{av:'lblTb_historicoconsumo_Fontunderline',ctrl:'TB_HISTORICOCONSUMO',prop:'Fontunderline'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A40000Contrato_Numero = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldWccontratogeneral = "";
         WebComp_Wccontratogeneral_Component = "";
         scmdbuf = "";
         H00MK3_A40000Contrato_Numero = new String[] {""} ;
         H00MK5_A40000Contrato_Numero = new String[] {""} ;
         lblLblvoltar_Jsonclick = "";
         H00MK6_A74Contrato_Codigo = new int[1] ;
         H00MK7_A152ContratoClausulas_Codigo = new int[1] ;
         H00MK7_A74Contrato_Codigo = new int[1] ;
         H00MK8_A1204ContratoUnidades_UndMedCod = new int[1] ;
         H00MK8_A1207ContratoUnidades_ContratoCod = new int[1] ;
         H00MK9_A160ContratoServicos_Codigo = new int[1] ;
         H00MK9_A74Contrato_Codigo = new int[1] ;
         H00MK10_A101ContratoGarantia_Codigo = new int[1] ;
         H00MK10_A74Contrato_Codigo = new int[1] ;
         H00MK11_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         H00MK11_A74Contrato_Codigo = new int[1] ;
         H00MK12_A314ContratoDadosCertame_Codigo = new int[1] ;
         H00MK12_A74Contrato_Codigo = new int[1] ;
         H00MK13_A315ContratoTermoAditivo_Codigo = new int[1] ;
         H00MK13_A74Contrato_Codigo = new int[1] ;
         H00MK14_A294ContratoOcorrencia_Codigo = new int[1] ;
         H00MK14_A74Contrato_Codigo = new int[1] ;
         H00MK15_A321ContratoObrigacao_Codigo = new int[1] ;
         H00MK15_A74Contrato_Codigo = new int[1] ;
         sStyleString = "";
         lblTextblocktitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         lblTb_contrato_Jsonclick = "";
         lblTb_clausulas_Jsonclick = "";
         lblTb_usc_Jsonclick = "";
         lblTb_servicos_Jsonclick = "";
         lblTb_garantias_Jsonclick = "";
         lblTb_anexos_Jsonclick = "";
         lblTb_dadoscertame_Jsonclick = "";
         lblTb_ocorrencias_Jsonclick = "";
         lblTb_obrigacoes_Jsonclick = "";
         lblTb_saldo_Jsonclick = "";
         lblTb_termoaditivo_Jsonclick = "";
         lblTb_notaempenho_Jsonclick = "";
         lblTb_autorizacaoconsumo_Jsonclick = "";
         lblTb_historicoconsumo_Jsonclick = "";
         lblTb_caixaentrada_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_contrato__default(),
            new Object[][] {
                new Object[] {
               H00MK3_A40000Contrato_Numero
               }
               , new Object[] {
               H00MK5_A40000Contrato_Numero
               }
               , new Object[] {
               H00MK6_A74Contrato_Codigo
               }
               , new Object[] {
               H00MK7_A152ContratoClausulas_Codigo, H00MK7_A74Contrato_Codigo
               }
               , new Object[] {
               H00MK8_A1204ContratoUnidades_UndMedCod, H00MK8_A1207ContratoUnidades_ContratoCod
               }
               , new Object[] {
               H00MK9_A160ContratoServicos_Codigo, H00MK9_A74Contrato_Codigo
               }
               , new Object[] {
               H00MK10_A101ContratoGarantia_Codigo, H00MK10_A74Contrato_Codigo
               }
               , new Object[] {
               H00MK11_A108ContratoArquivosAnexos_Codigo, H00MK11_A74Contrato_Codigo
               }
               , new Object[] {
               H00MK12_A314ContratoDadosCertame_Codigo, H00MK12_A74Contrato_Codigo
               }
               , new Object[] {
               H00MK13_A315ContratoTermoAditivo_Codigo, H00MK13_A74Contrato_Codigo
               }
               , new Object[] {
               H00MK14_A294ContratoOcorrencia_Codigo, H00MK14_A74Contrato_Codigo
               }
               , new Object[] {
               H00MK15_A321ContratoObrigacao_Codigo, H00MK15_A74Contrato_Codigo
               }
            }
         );
         WebComp_Wccontratogeneral = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nRcdExists_12 ;
      private short nIsMod_12 ;
      private short nRcdExists_11 ;
      private short nIsMod_11 ;
      private short nRcdExists_10 ;
      private short nIsMod_10 ;
      private short nRcdExists_9 ;
      private short nIsMod_9 ;
      private short nRcdExists_8 ;
      private short nIsMod_8 ;
      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV6Count ;
      private short lblTb_contrato_Fontunderline ;
      private short lblTb_clausulas_Fontunderline ;
      private short lblTb_usc_Fontunderline ;
      private short lblTb_servicos_Fontunderline ;
      private short lblTb_garantias_Fontunderline ;
      private short lblTb_anexos_Fontunderline ;
      private short lblTb_dadoscertame_Fontunderline ;
      private short lblTb_termoaditivo_Fontunderline ;
      private short lblTb_ocorrencias_Fontunderline ;
      private short lblTb_notaempenho_Fontunderline ;
      private short lblTb_saldo_Fontunderline ;
      private short lblTb_historicoconsumo_Fontunderline ;
      private short lblTb_caixaentrada_Fontunderline ;
      private short lblTb_obrigacoes_Fontunderline ;
      private short lblTb_autorizacaoconsumo_Fontunderline ;
      private short nGXWrapped ;
      private int AV5Contrato_Codigo ;
      private int wcpOAV5Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1207ContratoUnidades_ContratoCod ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A40000Contrato_Numero ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldWccontratogeneral ;
      private String WebComp_Wccontratogeneral_Component ;
      private String scmdbuf ;
      private String lblTextblocktitle_Caption ;
      private String lblTextblocktitle_Internalname ;
      private String lblLblvoltar_Link ;
      private String lblLblvoltar_Internalname ;
      private String lblLblvoltar_Jsonclick ;
      private String lblTb_clausulas_Caption ;
      private String lblTb_clausulas_Internalname ;
      private String lblTb_usc_Caption ;
      private String lblTb_usc_Internalname ;
      private String lblTb_servicos_Caption ;
      private String lblTb_servicos_Internalname ;
      private String lblTb_garantias_Caption ;
      private String lblTb_garantias_Internalname ;
      private String lblTb_anexos_Caption ;
      private String lblTb_anexos_Internalname ;
      private String lblTb_dadoscertame_Caption ;
      private String lblTb_dadoscertame_Internalname ;
      private String lblTb_termoaditivo_Caption ;
      private String lblTb_termoaditivo_Internalname ;
      private String lblTb_ocorrencias_Caption ;
      private String lblTb_ocorrencias_Internalname ;
      private String lblTb_obrigacoes_Caption ;
      private String lblTb_obrigacoes_Internalname ;
      private String lblTb_contrato_Internalname ;
      private String lblTb_notaempenho_Internalname ;
      private String lblTb_saldo_Internalname ;
      private String lblTb_historicoconsumo_Internalname ;
      private String lblTb_caixaentrada_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTextblocktitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String tblTablecontent_Internalname ;
      private String tblTablebotoes_Internalname ;
      private String lblTb_contrato_Jsonclick ;
      private String lblTb_clausulas_Jsonclick ;
      private String lblTb_usc_Jsonclick ;
      private String lblTb_servicos_Jsonclick ;
      private String lblTb_garantias_Jsonclick ;
      private String lblTb_anexos_Jsonclick ;
      private String lblTb_dadoscertame_Jsonclick ;
      private String lblTb_ocorrencias_Jsonclick ;
      private String lblTb_obrigacoes_Jsonclick ;
      private String lblTb_saldo_Jsonclick ;
      private String lblTb_termoaditivo_Jsonclick ;
      private String lblTb_notaempenho_Jsonclick ;
      private String lblTb_autorizacaoconsumo_Internalname ;
      private String lblTb_autorizacaoconsumo_Jsonclick ;
      private String lblTb_historicoconsumo_Jsonclick ;
      private String lblTb_caixaentrada_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private GXWebComponent WebComp_Wccontratogeneral ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contrato_Codigo ;
      private IDataStoreProvider pr_default ;
      private String[] H00MK3_A40000Contrato_Numero ;
      private String[] H00MK5_A40000Contrato_Numero ;
      private int[] H00MK6_A74Contrato_Codigo ;
      private int[] H00MK7_A152ContratoClausulas_Codigo ;
      private int[] H00MK7_A74Contrato_Codigo ;
      private int[] H00MK8_A1204ContratoUnidades_UndMedCod ;
      private int[] H00MK8_A1207ContratoUnidades_ContratoCod ;
      private int[] H00MK9_A160ContratoServicos_Codigo ;
      private int[] H00MK9_A74Contrato_Codigo ;
      private int[] H00MK10_A101ContratoGarantia_Codigo ;
      private int[] H00MK10_A74Contrato_Codigo ;
      private int[] H00MK11_A108ContratoArquivosAnexos_Codigo ;
      private int[] H00MK11_A74Contrato_Codigo ;
      private int[] H00MK12_A314ContratoDadosCertame_Codigo ;
      private int[] H00MK12_A74Contrato_Codigo ;
      private int[] H00MK13_A315ContratoTermoAditivo_Codigo ;
      private int[] H00MK13_A74Contrato_Codigo ;
      private int[] H00MK14_A294ContratoOcorrencia_Codigo ;
      private int[] H00MK14_A74Contrato_Codigo ;
      private int[] H00MK15_A321ContratoObrigacao_Codigo ;
      private int[] H00MK15_A74Contrato_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
   }

   public class wp_contrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00MK3 ;
          prmH00MK3 = new Object[] {
          new Object[] {"@AV5Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MK5 ;
          prmH00MK5 = new Object[] {
          new Object[] {"@AV5Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MK6 ;
          prmH00MK6 = new Object[] {
          new Object[] {"@AV5Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MK7 ;
          prmH00MK7 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MK8 ;
          prmH00MK8 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MK9 ;
          prmH00MK9 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MK10 ;
          prmH00MK10 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MK11 ;
          prmH00MK11 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MK12 ;
          prmH00MK12 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MK13 ;
          prmH00MK13 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MK14 ;
          prmH00MK14 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MK15 ;
          prmH00MK15 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00MK3", "SELECT COALESCE( T1.[Contrato_Numero], '') AS Contrato_Numero FROM (SELECT [Contrato_Numero] AS Contrato_Numero, [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV5Contrato_Codigo ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MK3,1,0,true,true )
             ,new CursorDef("H00MK5", "SELECT COALESCE( T1.[Contrato_Numero], '') AS Contrato_Numero FROM (SELECT [Contrato_Numero] AS Contrato_Numero, [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV5Contrato_Codigo ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MK5,1,0,true,true )
             ,new CursorDef("H00MK6", "SELECT [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV5Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MK6,1,0,true,true )
             ,new CursorDef("H00MK7", "SELECT [ContratoClausulas_Codigo], [Contrato_Codigo] FROM [ContratoClausulas] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MK7,100,0,false,false )
             ,new CursorDef("H00MK8", "SELECT [ContratoUnidades_UndMedCod], [ContratoUnidades_ContratoCod] FROM [ContratoUnidades] WITH (NOLOCK) WHERE [ContratoUnidades_ContratoCod] = @Contrato_Codigo ORDER BY [ContratoUnidades_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MK8,100,0,false,false )
             ,new CursorDef("H00MK9", "SELECT [ContratoServicos_Codigo], [Contrato_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MK9,100,0,false,false )
             ,new CursorDef("H00MK10", "SELECT [ContratoGarantia_Codigo], [Contrato_Codigo] FROM [ContratoGarantia] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MK10,100,0,false,false )
             ,new CursorDef("H00MK11", "SELECT [ContratoArquivosAnexos_Codigo], [Contrato_Codigo] FROM [ContratoArquivosAnexos] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MK11,100,0,false,false )
             ,new CursorDef("H00MK12", "SELECT [ContratoDadosCertame_Codigo], [Contrato_Codigo] FROM [ContratoDadosCertame] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MK12,100,0,false,false )
             ,new CursorDef("H00MK13", "SELECT [ContratoTermoAditivo_Codigo], [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MK13,100,0,false,false )
             ,new CursorDef("H00MK14", "SELECT [ContratoOcorrencia_Codigo], [Contrato_Codigo] FROM [ContratoOcorrencia] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MK14,100,0,false,false )
             ,new CursorDef("H00MK15", "SELECT [ContratoObrigacao_Codigo], [Contrato_Codigo] FROM [ContratoObrigacao] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MK15,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
