/*
               File: type_SdtSDT_Artefatos
        Description: SDT_Artefatos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:59.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Artefatos" )]
   [XmlType(TypeName =  "SDT_Artefatos" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSDT_Artefatos : GxUserType
   {
      public SdtSDT_Artefatos( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Artefatos_Artefatos_descricao = "";
         gxTv_SdtSDT_Artefatos_Artefatos_link = "";
      }

      public SdtSDT_Artefatos( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Artefatos deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Artefatos)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Artefatos obj ;
         obj = this;
         obj.gxTpr_Artefatos_codigo = deserialized.gxTpr_Artefatos_codigo;
         obj.gxTpr_Artefatos_descricao = deserialized.gxTpr_Artefatos_descricao;
         obj.gxTpr_Artefatos_obrigatorio = deserialized.gxTpr_Artefatos_obrigatorio;
         obj.gxTpr_Artefatos_link = deserialized.gxTpr_Artefatos_link;
         obj.gxTpr_Artefatos_evdcod = deserialized.gxTpr_Artefatos_evdcod;
         obj.gxTpr_Artefatos_anxcod = deserialized.gxTpr_Artefatos_anxcod;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Artefatos_Codigo") )
               {
                  gxTv_SdtSDT_Artefatos_Artefatos_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Artefatos_Descricao") )
               {
                  gxTv_SdtSDT_Artefatos_Artefatos_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Artefatos_Obrigatorio") )
               {
                  gxTv_SdtSDT_Artefatos_Artefatos_obrigatorio = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Artefatos_Link") )
               {
                  gxTv_SdtSDT_Artefatos_Artefatos_link = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Artefatos_EvdCod") )
               {
                  gxTv_SdtSDT_Artefatos_Artefatos_evdcod = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Artefatos_AnxCod") )
               {
                  gxTv_SdtSDT_Artefatos_Artefatos_anxcod = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Artefatos";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Artefatos_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Artefatos_Artefatos_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Artefatos_Descricao", StringUtil.RTrim( gxTv_SdtSDT_Artefatos_Artefatos_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Artefatos_Obrigatorio", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_Artefatos_Artefatos_obrigatorio)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Artefatos_Link", StringUtil.RTrim( gxTv_SdtSDT_Artefatos_Artefatos_link));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Artefatos_EvdCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Artefatos_Artefatos_evdcod), 10, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Artefatos_AnxCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Artefatos_Artefatos_anxcod), 10, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Artefatos_Codigo", gxTv_SdtSDT_Artefatos_Artefatos_codigo, false);
         AddObjectProperty("Artefatos_Descricao", gxTv_SdtSDT_Artefatos_Artefatos_descricao, false);
         AddObjectProperty("Artefatos_Obrigatorio", gxTv_SdtSDT_Artefatos_Artefatos_obrigatorio, false);
         AddObjectProperty("Artefatos_Link", gxTv_SdtSDT_Artefatos_Artefatos_link, false);
         AddObjectProperty("Artefatos_EvdCod", gxTv_SdtSDT_Artefatos_Artefatos_evdcod, false);
         AddObjectProperty("Artefatos_AnxCod", gxTv_SdtSDT_Artefatos_Artefatos_anxcod, false);
         return  ;
      }

      [  SoapElement( ElementName = "Artefatos_Codigo" )]
      [  XmlElement( ElementName = "Artefatos_Codigo"   )]
      public int gxTpr_Artefatos_codigo
      {
         get {
            return gxTv_SdtSDT_Artefatos_Artefatos_codigo ;
         }

         set {
            gxTv_SdtSDT_Artefatos_Artefatos_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Artefatos_Descricao" )]
      [  XmlElement( ElementName = "Artefatos_Descricao"   )]
      public String gxTpr_Artefatos_descricao
      {
         get {
            return gxTv_SdtSDT_Artefatos_Artefatos_descricao ;
         }

         set {
            gxTv_SdtSDT_Artefatos_Artefatos_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Artefatos_Obrigatorio" )]
      [  XmlElement( ElementName = "Artefatos_Obrigatorio"   )]
      public bool gxTpr_Artefatos_obrigatorio
      {
         get {
            return gxTv_SdtSDT_Artefatos_Artefatos_obrigatorio ;
         }

         set {
            gxTv_SdtSDT_Artefatos_Artefatos_obrigatorio = value;
         }

      }

      [  SoapElement( ElementName = "Artefatos_Link" )]
      [  XmlElement( ElementName = "Artefatos_Link"   )]
      public String gxTpr_Artefatos_link
      {
         get {
            return gxTv_SdtSDT_Artefatos_Artefatos_link ;
         }

         set {
            gxTv_SdtSDT_Artefatos_Artefatos_link = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Artefatos_EvdCod" )]
      [  XmlElement( ElementName = "Artefatos_EvdCod"   )]
      public long gxTpr_Artefatos_evdcod
      {
         get {
            return gxTv_SdtSDT_Artefatos_Artefatos_evdcod ;
         }

         set {
            gxTv_SdtSDT_Artefatos_Artefatos_evdcod = (long)(value);
         }

      }

      [  SoapElement( ElementName = "Artefatos_AnxCod" )]
      [  XmlElement( ElementName = "Artefatos_AnxCod"   )]
      public long gxTpr_Artefatos_anxcod
      {
         get {
            return gxTv_SdtSDT_Artefatos_Artefatos_anxcod ;
         }

         set {
            gxTv_SdtSDT_Artefatos_Artefatos_anxcod = (long)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_Artefatos_Artefatos_descricao = "";
         gxTv_SdtSDT_Artefatos_Artefatos_link = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_Artefatos_Artefatos_codigo ;
      protected long gxTv_SdtSDT_Artefatos_Artefatos_evdcod ;
      protected long gxTv_SdtSDT_Artefatos_Artefatos_anxcod ;
      protected String sTagName ;
      protected bool gxTv_SdtSDT_Artefatos_Artefatos_obrigatorio ;
      protected String gxTv_SdtSDT_Artefatos_Artefatos_descricao ;
      protected String gxTv_SdtSDT_Artefatos_Artefatos_link ;
   }

   [DataContract(Name = @"SDT_Artefatos", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_Artefatos_RESTInterface : GxGenericCollectionItem<SdtSDT_Artefatos>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Artefatos_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Artefatos_RESTInterface( SdtSDT_Artefatos psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Artefatos_Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Artefatos_codigo
      {
         get {
            return sdt.gxTpr_Artefatos_codigo ;
         }

         set {
            sdt.gxTpr_Artefatos_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Artefatos_Descricao" , Order = 1 )]
      public String gxTpr_Artefatos_descricao
      {
         get {
            return sdt.gxTpr_Artefatos_descricao ;
         }

         set {
            sdt.gxTpr_Artefatos_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Artefatos_Obrigatorio" , Order = 2 )]
      public bool gxTpr_Artefatos_obrigatorio
      {
         get {
            return sdt.gxTpr_Artefatos_obrigatorio ;
         }

         set {
            sdt.gxTpr_Artefatos_obrigatorio = value;
         }

      }

      [DataMember( Name = "Artefatos_Link" , Order = 3 )]
      public String gxTpr_Artefatos_link
      {
         get {
            return sdt.gxTpr_Artefatos_link ;
         }

         set {
            sdt.gxTpr_Artefatos_link = (String)(value);
         }

      }

      [DataMember( Name = "Artefatos_EvdCod" , Order = 4 )]
      public String gxTpr_Artefatos_evdcod
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Artefatos_evdcod), 10, 0)) ;
         }

         set {
            sdt.gxTpr_Artefatos_evdcod = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Artefatos_AnxCod" , Order = 5 )]
      public String gxTpr_Artefatos_anxcod
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Artefatos_anxcod), 10, 0)) ;
         }

         set {
            sdt.gxTpr_Artefatos_anxcod = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      public SdtSDT_Artefatos sdt
      {
         get {
            return (SdtSDT_Artefatos)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Artefatos() ;
         }
      }

   }

}
