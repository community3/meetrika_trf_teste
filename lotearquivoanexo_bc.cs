/*
               File: LoteArquivoAnexo_BC
        Description: Arquivos Anexos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:51:40.23
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class lotearquivoanexo_bc : GXHttpHandler, IGxSilentTrn
   {
      public lotearquivoanexo_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public lotearquivoanexo_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow2N103( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey2N103( ) ;
         standaloneModal( ) ;
         AddRow2N103( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E112N2 */
            E112N2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z841LoteArquivoAnexo_LoteCod = A841LoteArquivoAnexo_LoteCod;
               Z836LoteArquivoAnexo_Data = A836LoteArquivoAnexo_Data;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_2N0( )
      {
         BeforeValidate2N103( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2N103( ) ;
            }
            else
            {
               CheckExtendedTable2N103( ) ;
               if ( AnyError == 0 )
               {
                  ZM2N103( 6) ;
                  ZM2N103( 7) ;
               }
               CloseExtendedTableCursors2N103( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E122N2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV10TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            while ( AV15GXV1 <= AV10TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV10TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "TipoDocumento_Codigo") == 0 )
               {
                  AV12Insert_TipoDocumento_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
            }
         }
      }

      protected void E112N2( )
      {
         /* After Trn Routine */
      }

      protected void ZM2N103( short GX_JID )
      {
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            Z1027LoteArquivoAnexo_Verificador = A1027LoteArquivoAnexo_Verificador;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
         }
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -5 )
         {
            Z836LoteArquivoAnexo_Data = A836LoteArquivoAnexo_Data;
            Z838LoteArquivoAnexo_Arquivo = A838LoteArquivoAnexo_Arquivo;
            Z837LoteArquivoAnexo_Descricao = A837LoteArquivoAnexo_Descricao;
            Z1027LoteArquivoAnexo_Verificador = A1027LoteArquivoAnexo_Verificador;
            Z840LoteArquivoAnexo_TipoArq = A840LoteArquivoAnexo_TipoArq;
            Z839LoteArquivoAnexo_NomeArq = A839LoteArquivoAnexo_NomeArq;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z841LoteArquivoAnexo_LoteCod = A841LoteArquivoAnexo_LoteCod;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         AV14Pgmname = "LoteArquivoAnexo_BC";
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A836LoteArquivoAnexo_Data) && ( Gx_BScreen == 0 ) )
         {
            A836LoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load2N103( )
      {
         /* Using cursor BC002N6 */
         pr_default.execute(4, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound103 = 1;
            A837LoteArquivoAnexo_Descricao = BC002N6_A837LoteArquivoAnexo_Descricao[0];
            n837LoteArquivoAnexo_Descricao = BC002N6_n837LoteArquivoAnexo_Descricao[0];
            A646TipoDocumento_Nome = BC002N6_A646TipoDocumento_Nome[0];
            A1027LoteArquivoAnexo_Verificador = BC002N6_A1027LoteArquivoAnexo_Verificador[0];
            n1027LoteArquivoAnexo_Verificador = BC002N6_n1027LoteArquivoAnexo_Verificador[0];
            A840LoteArquivoAnexo_TipoArq = BC002N6_A840LoteArquivoAnexo_TipoArq[0];
            n840LoteArquivoAnexo_TipoArq = BC002N6_n840LoteArquivoAnexo_TipoArq[0];
            A838LoteArquivoAnexo_Arquivo_Filetype = A840LoteArquivoAnexo_TipoArq;
            A839LoteArquivoAnexo_NomeArq = BC002N6_A839LoteArquivoAnexo_NomeArq[0];
            n839LoteArquivoAnexo_NomeArq = BC002N6_n839LoteArquivoAnexo_NomeArq[0];
            A838LoteArquivoAnexo_Arquivo_Filename = A839LoteArquivoAnexo_NomeArq;
            A645TipoDocumento_Codigo = BC002N6_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC002N6_n645TipoDocumento_Codigo[0];
            A838LoteArquivoAnexo_Arquivo = BC002N6_A838LoteArquivoAnexo_Arquivo[0];
            n838LoteArquivoAnexo_Arquivo = BC002N6_n838LoteArquivoAnexo_Arquivo[0];
            ZM2N103( -5) ;
         }
         pr_default.close(4);
         OnLoadActions2N103( ) ;
      }

      protected void OnLoadActions2N103( )
      {
      }

      protected void CheckExtendedTable2N103( )
      {
         standaloneModal( ) ;
         /* Using cursor BC002N5 */
         pr_default.execute(3, new Object[] {A841LoteArquivoAnexo_LoteCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Lote Arquivo Anexo_Lote'.", "ForeignKeyNotFound", 1, "LOTEARQUIVOANEXO_LOTECOD");
            AnyError = 1;
         }
         pr_default.close(3);
         if ( ! ( (DateTime.MinValue==A836LoteArquivoAnexo_Data) || ( A836LoteArquivoAnexo_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data/Hora Upload fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC002N4 */
         pr_default.execute(2, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "TIPODOCUMENTO_CODIGO");
               AnyError = 1;
            }
         }
         A646TipoDocumento_Nome = BC002N4_A646TipoDocumento_Nome[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors2N103( )
      {
         pr_default.close(3);
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey2N103( )
      {
         /* Using cursor BC002N7 */
         pr_default.execute(5, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound103 = 1;
         }
         else
         {
            RcdFound103 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC002N3 */
         pr_default.execute(1, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2N103( 5) ;
            RcdFound103 = 1;
            A836LoteArquivoAnexo_Data = BC002N3_A836LoteArquivoAnexo_Data[0];
            A837LoteArquivoAnexo_Descricao = BC002N3_A837LoteArquivoAnexo_Descricao[0];
            n837LoteArquivoAnexo_Descricao = BC002N3_n837LoteArquivoAnexo_Descricao[0];
            A1027LoteArquivoAnexo_Verificador = BC002N3_A1027LoteArquivoAnexo_Verificador[0];
            n1027LoteArquivoAnexo_Verificador = BC002N3_n1027LoteArquivoAnexo_Verificador[0];
            A840LoteArquivoAnexo_TipoArq = BC002N3_A840LoteArquivoAnexo_TipoArq[0];
            n840LoteArquivoAnexo_TipoArq = BC002N3_n840LoteArquivoAnexo_TipoArq[0];
            A838LoteArquivoAnexo_Arquivo_Filetype = A840LoteArquivoAnexo_TipoArq;
            A839LoteArquivoAnexo_NomeArq = BC002N3_A839LoteArquivoAnexo_NomeArq[0];
            n839LoteArquivoAnexo_NomeArq = BC002N3_n839LoteArquivoAnexo_NomeArq[0];
            A838LoteArquivoAnexo_Arquivo_Filename = A839LoteArquivoAnexo_NomeArq;
            A645TipoDocumento_Codigo = BC002N3_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC002N3_n645TipoDocumento_Codigo[0];
            A841LoteArquivoAnexo_LoteCod = BC002N3_A841LoteArquivoAnexo_LoteCod[0];
            A838LoteArquivoAnexo_Arquivo = BC002N3_A838LoteArquivoAnexo_Arquivo[0];
            n838LoteArquivoAnexo_Arquivo = BC002N3_n838LoteArquivoAnexo_Arquivo[0];
            Z841LoteArquivoAnexo_LoteCod = A841LoteArquivoAnexo_LoteCod;
            Z836LoteArquivoAnexo_Data = A836LoteArquivoAnexo_Data;
            sMode103 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load2N103( ) ;
            if ( AnyError == 1 )
            {
               RcdFound103 = 0;
               InitializeNonKey2N103( ) ;
            }
            Gx_mode = sMode103;
         }
         else
         {
            RcdFound103 = 0;
            InitializeNonKey2N103( ) ;
            sMode103 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode103;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2N103( ) ;
         if ( RcdFound103 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_2N0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency2N103( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC002N2 */
            pr_default.execute(0, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"LoteArquivoAnexo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1027LoteArquivoAnexo_Verificador, BC002N2_A1027LoteArquivoAnexo_Verificador[0]) != 0 ) || ( Z645TipoDocumento_Codigo != BC002N2_A645TipoDocumento_Codigo[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"LoteArquivoAnexo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2N103( )
      {
         BeforeValidate2N103( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2N103( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2N103( 0) ;
            CheckOptimisticConcurrency2N103( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2N103( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2N103( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002N8 */
                     pr_default.execute(6, new Object[] {A836LoteArquivoAnexo_Data, n838LoteArquivoAnexo_Arquivo, A838LoteArquivoAnexo_Arquivo, n837LoteArquivoAnexo_Descricao, A837LoteArquivoAnexo_Descricao, n1027LoteArquivoAnexo_Verificador, A1027LoteArquivoAnexo_Verificador, n840LoteArquivoAnexo_TipoArq, A840LoteArquivoAnexo_TipoArq, n839LoteArquivoAnexo_NomeArq, A839LoteArquivoAnexo_NomeArq, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A841LoteArquivoAnexo_LoteCod});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("LoteArquivoAnexo") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2N103( ) ;
            }
            EndLevel2N103( ) ;
         }
         CloseExtendedTableCursors2N103( ) ;
      }

      protected void Update2N103( )
      {
         BeforeValidate2N103( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2N103( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2N103( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2N103( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2N103( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002N9 */
                     pr_default.execute(7, new Object[] {n837LoteArquivoAnexo_Descricao, A837LoteArquivoAnexo_Descricao, n1027LoteArquivoAnexo_Verificador, A1027LoteArquivoAnexo_Verificador, n840LoteArquivoAnexo_TipoArq, A840LoteArquivoAnexo_TipoArq, n839LoteArquivoAnexo_NomeArq, A839LoteArquivoAnexo_NomeArq, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("LoteArquivoAnexo") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"LoteArquivoAnexo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2N103( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2N103( ) ;
         }
         CloseExtendedTableCursors2N103( ) ;
      }

      protected void DeferredUpdate2N103( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor BC002N10 */
            pr_default.execute(8, new Object[] {n838LoteArquivoAnexo_Arquivo, A838LoteArquivoAnexo_Arquivo, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
            pr_default.close(8);
            dsDefault.SmartCacheProvider.SetUpdated("LoteArquivoAnexo") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate2N103( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2N103( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2N103( ) ;
            AfterConfirm2N103( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2N103( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC002N11 */
                  pr_default.execute(9, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
                  pr_default.close(9);
                  dsDefault.SmartCacheProvider.SetUpdated("LoteArquivoAnexo") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode103 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel2N103( ) ;
         Gx_mode = sMode103;
      }

      protected void OnDeleteControls2N103( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC002N12 */
            pr_default.execute(10, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            A646TipoDocumento_Nome = BC002N12_A646TipoDocumento_Nome[0];
            pr_default.close(10);
         }
      }

      protected void EndLevel2N103( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2N103( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart2N103( )
      {
         /* Scan By routine */
         /* Using cursor BC002N13 */
         pr_default.execute(11, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
         RcdFound103 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound103 = 1;
            A836LoteArquivoAnexo_Data = BC002N13_A836LoteArquivoAnexo_Data[0];
            A837LoteArquivoAnexo_Descricao = BC002N13_A837LoteArquivoAnexo_Descricao[0];
            n837LoteArquivoAnexo_Descricao = BC002N13_n837LoteArquivoAnexo_Descricao[0];
            A646TipoDocumento_Nome = BC002N13_A646TipoDocumento_Nome[0];
            A1027LoteArquivoAnexo_Verificador = BC002N13_A1027LoteArquivoAnexo_Verificador[0];
            n1027LoteArquivoAnexo_Verificador = BC002N13_n1027LoteArquivoAnexo_Verificador[0];
            A840LoteArquivoAnexo_TipoArq = BC002N13_A840LoteArquivoAnexo_TipoArq[0];
            n840LoteArquivoAnexo_TipoArq = BC002N13_n840LoteArquivoAnexo_TipoArq[0];
            A838LoteArquivoAnexo_Arquivo_Filetype = A840LoteArquivoAnexo_TipoArq;
            A839LoteArquivoAnexo_NomeArq = BC002N13_A839LoteArquivoAnexo_NomeArq[0];
            n839LoteArquivoAnexo_NomeArq = BC002N13_n839LoteArquivoAnexo_NomeArq[0];
            A838LoteArquivoAnexo_Arquivo_Filename = A839LoteArquivoAnexo_NomeArq;
            A645TipoDocumento_Codigo = BC002N13_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC002N13_n645TipoDocumento_Codigo[0];
            A841LoteArquivoAnexo_LoteCod = BC002N13_A841LoteArquivoAnexo_LoteCod[0];
            A838LoteArquivoAnexo_Arquivo = BC002N13_A838LoteArquivoAnexo_Arquivo[0];
            n838LoteArquivoAnexo_Arquivo = BC002N13_n838LoteArquivoAnexo_Arquivo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext2N103( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound103 = 0;
         ScanKeyLoad2N103( ) ;
      }

      protected void ScanKeyLoad2N103( )
      {
         sMode103 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound103 = 1;
            A836LoteArquivoAnexo_Data = BC002N13_A836LoteArquivoAnexo_Data[0];
            A837LoteArquivoAnexo_Descricao = BC002N13_A837LoteArquivoAnexo_Descricao[0];
            n837LoteArquivoAnexo_Descricao = BC002N13_n837LoteArquivoAnexo_Descricao[0];
            A646TipoDocumento_Nome = BC002N13_A646TipoDocumento_Nome[0];
            A1027LoteArquivoAnexo_Verificador = BC002N13_A1027LoteArquivoAnexo_Verificador[0];
            n1027LoteArquivoAnexo_Verificador = BC002N13_n1027LoteArquivoAnexo_Verificador[0];
            A840LoteArquivoAnexo_TipoArq = BC002N13_A840LoteArquivoAnexo_TipoArq[0];
            n840LoteArquivoAnexo_TipoArq = BC002N13_n840LoteArquivoAnexo_TipoArq[0];
            A838LoteArquivoAnexo_Arquivo_Filetype = A840LoteArquivoAnexo_TipoArq;
            A839LoteArquivoAnexo_NomeArq = BC002N13_A839LoteArquivoAnexo_NomeArq[0];
            n839LoteArquivoAnexo_NomeArq = BC002N13_n839LoteArquivoAnexo_NomeArq[0];
            A838LoteArquivoAnexo_Arquivo_Filename = A839LoteArquivoAnexo_NomeArq;
            A645TipoDocumento_Codigo = BC002N13_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC002N13_n645TipoDocumento_Codigo[0];
            A841LoteArquivoAnexo_LoteCod = BC002N13_A841LoteArquivoAnexo_LoteCod[0];
            A838LoteArquivoAnexo_Arquivo = BC002N13_A838LoteArquivoAnexo_Arquivo[0];
            n838LoteArquivoAnexo_Arquivo = BC002N13_n838LoteArquivoAnexo_Arquivo[0];
         }
         Gx_mode = sMode103;
      }

      protected void ScanKeyEnd2N103( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm2N103( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2N103( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2N103( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2N103( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2N103( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2N103( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2N103( )
      {
      }

      protected void AddRow2N103( )
      {
         VarsToRow103( bcLoteArquivoAnexo) ;
      }

      protected void ReadRow2N103( )
      {
         RowToVars103( bcLoteArquivoAnexo, 1) ;
      }

      protected void InitializeNonKey2N103( )
      {
         A838LoteArquivoAnexo_Arquivo = "";
         n838LoteArquivoAnexo_Arquivo = false;
         A837LoteArquivoAnexo_Descricao = "";
         n837LoteArquivoAnexo_Descricao = false;
         A645TipoDocumento_Codigo = 0;
         n645TipoDocumento_Codigo = false;
         A646TipoDocumento_Nome = "";
         A1027LoteArquivoAnexo_Verificador = "";
         n1027LoteArquivoAnexo_Verificador = false;
         A840LoteArquivoAnexo_TipoArq = "";
         n840LoteArquivoAnexo_TipoArq = false;
         A839LoteArquivoAnexo_NomeArq = "";
         n839LoteArquivoAnexo_NomeArq = false;
         Z1027LoteArquivoAnexo_Verificador = "";
         Z645TipoDocumento_Codigo = 0;
      }

      protected void InitAll2N103( )
      {
         A841LoteArquivoAnexo_LoteCod = 0;
         A836LoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         InitializeNonKey2N103( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow103( SdtLoteArquivoAnexo obj103 )
      {
         obj103.gxTpr_Mode = Gx_mode;
         obj103.gxTpr_Lotearquivoanexo_arquivo = A838LoteArquivoAnexo_Arquivo;
         obj103.gxTpr_Lotearquivoanexo_descricao = A837LoteArquivoAnexo_Descricao;
         obj103.gxTpr_Tipodocumento_codigo = A645TipoDocumento_Codigo;
         obj103.gxTpr_Tipodocumento_nome = A646TipoDocumento_Nome;
         obj103.gxTpr_Lotearquivoanexo_verificador = A1027LoteArquivoAnexo_Verificador;
         obj103.gxTpr_Lotearquivoanexo_tipoarq = A840LoteArquivoAnexo_TipoArq;
         obj103.gxTpr_Lotearquivoanexo_nomearq = A839LoteArquivoAnexo_NomeArq;
         obj103.gxTpr_Lotearquivoanexo_lotecod = A841LoteArquivoAnexo_LoteCod;
         obj103.gxTpr_Lotearquivoanexo_data = A836LoteArquivoAnexo_Data;
         obj103.gxTpr_Lotearquivoanexo_lotecod_Z = Z841LoteArquivoAnexo_LoteCod;
         obj103.gxTpr_Lotearquivoanexo_data_Z = Z836LoteArquivoAnexo_Data;
         obj103.gxTpr_Lotearquivoanexo_nomearq_Z = Z839LoteArquivoAnexo_NomeArq;
         obj103.gxTpr_Lotearquivoanexo_tipoarq_Z = Z840LoteArquivoAnexo_TipoArq;
         obj103.gxTpr_Tipodocumento_codigo_Z = Z645TipoDocumento_Codigo;
         obj103.gxTpr_Tipodocumento_nome_Z = Z646TipoDocumento_Nome;
         obj103.gxTpr_Lotearquivoanexo_verificador_Z = Z1027LoteArquivoAnexo_Verificador;
         obj103.gxTpr_Lotearquivoanexo_arquivo_N = (short)(Convert.ToInt16(n838LoteArquivoAnexo_Arquivo));
         obj103.gxTpr_Lotearquivoanexo_nomearq_N = (short)(Convert.ToInt16(n839LoteArquivoAnexo_NomeArq));
         obj103.gxTpr_Lotearquivoanexo_tipoarq_N = (short)(Convert.ToInt16(n840LoteArquivoAnexo_TipoArq));
         obj103.gxTpr_Lotearquivoanexo_descricao_N = (short)(Convert.ToInt16(n837LoteArquivoAnexo_Descricao));
         obj103.gxTpr_Tipodocumento_codigo_N = (short)(Convert.ToInt16(n645TipoDocumento_Codigo));
         obj103.gxTpr_Lotearquivoanexo_verificador_N = (short)(Convert.ToInt16(n1027LoteArquivoAnexo_Verificador));
         obj103.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow103( SdtLoteArquivoAnexo obj103 )
      {
         obj103.gxTpr_Lotearquivoanexo_lotecod = A841LoteArquivoAnexo_LoteCod;
         obj103.gxTpr_Lotearquivoanexo_data = A836LoteArquivoAnexo_Data;
         return  ;
      }

      public void RowToVars103( SdtLoteArquivoAnexo obj103 ,
                                int forceLoad )
      {
         Gx_mode = obj103.gxTpr_Mode;
         A838LoteArquivoAnexo_Arquivo = obj103.gxTpr_Lotearquivoanexo_arquivo;
         n838LoteArquivoAnexo_Arquivo = false;
         A837LoteArquivoAnexo_Descricao = obj103.gxTpr_Lotearquivoanexo_descricao;
         n837LoteArquivoAnexo_Descricao = false;
         A645TipoDocumento_Codigo = obj103.gxTpr_Tipodocumento_codigo;
         n645TipoDocumento_Codigo = false;
         A646TipoDocumento_Nome = obj103.gxTpr_Tipodocumento_nome;
         A1027LoteArquivoAnexo_Verificador = obj103.gxTpr_Lotearquivoanexo_verificador;
         n1027LoteArquivoAnexo_Verificador = false;
         A840LoteArquivoAnexo_TipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( obj103.gxTpr_Lotearquivoanexo_tipoarq)) ? FileUtil.GetFileType( A838LoteArquivoAnexo_Arquivo) : obj103.gxTpr_Lotearquivoanexo_tipoarq);
         n840LoteArquivoAnexo_TipoArq = false;
         A839LoteArquivoAnexo_NomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( obj103.gxTpr_Lotearquivoanexo_nomearq)) ? FileUtil.GetFileName( A838LoteArquivoAnexo_Arquivo) : obj103.gxTpr_Lotearquivoanexo_nomearq);
         n839LoteArquivoAnexo_NomeArq = false;
         A841LoteArquivoAnexo_LoteCod = obj103.gxTpr_Lotearquivoanexo_lotecod;
         A836LoteArquivoAnexo_Data = obj103.gxTpr_Lotearquivoanexo_data;
         Z841LoteArquivoAnexo_LoteCod = obj103.gxTpr_Lotearquivoanexo_lotecod_Z;
         Z836LoteArquivoAnexo_Data = obj103.gxTpr_Lotearquivoanexo_data_Z;
         Z839LoteArquivoAnexo_NomeArq = obj103.gxTpr_Lotearquivoanexo_nomearq_Z;
         Z840LoteArquivoAnexo_TipoArq = obj103.gxTpr_Lotearquivoanexo_tipoarq_Z;
         Z645TipoDocumento_Codigo = obj103.gxTpr_Tipodocumento_codigo_Z;
         Z646TipoDocumento_Nome = obj103.gxTpr_Tipodocumento_nome_Z;
         Z1027LoteArquivoAnexo_Verificador = obj103.gxTpr_Lotearquivoanexo_verificador_Z;
         n838LoteArquivoAnexo_Arquivo = (bool)(Convert.ToBoolean(obj103.gxTpr_Lotearquivoanexo_arquivo_N));
         n839LoteArquivoAnexo_NomeArq = (bool)(Convert.ToBoolean(obj103.gxTpr_Lotearquivoanexo_nomearq_N));
         n840LoteArquivoAnexo_TipoArq = (bool)(Convert.ToBoolean(obj103.gxTpr_Lotearquivoanexo_tipoarq_N));
         n837LoteArquivoAnexo_Descricao = (bool)(Convert.ToBoolean(obj103.gxTpr_Lotearquivoanexo_descricao_N));
         n645TipoDocumento_Codigo = (bool)(Convert.ToBoolean(obj103.gxTpr_Tipodocumento_codigo_N));
         n1027LoteArquivoAnexo_Verificador = (bool)(Convert.ToBoolean(obj103.gxTpr_Lotearquivoanexo_verificador_N));
         Gx_mode = obj103.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A841LoteArquivoAnexo_LoteCod = (int)getParm(obj,0);
         A836LoteArquivoAnexo_Data = (DateTime)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey2N103( ) ;
         ScanKeyStart2N103( ) ;
         if ( RcdFound103 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC002N14 */
            pr_default.execute(12, new Object[] {A841LoteArquivoAnexo_LoteCod});
            if ( (pr_default.getStatus(12) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Lote Arquivo Anexo_Lote'.", "ForeignKeyNotFound", 1, "LOTEARQUIVOANEXO_LOTECOD");
               AnyError = 1;
            }
            pr_default.close(12);
         }
         else
         {
            Gx_mode = "UPD";
            Z841LoteArquivoAnexo_LoteCod = A841LoteArquivoAnexo_LoteCod;
            Z836LoteArquivoAnexo_Data = A836LoteArquivoAnexo_Data;
         }
         ZM2N103( -5) ;
         OnLoadActions2N103( ) ;
         AddRow2N103( ) ;
         ScanKeyEnd2N103( ) ;
         if ( RcdFound103 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars103( bcLoteArquivoAnexo, 0) ;
         ScanKeyStart2N103( ) ;
         if ( RcdFound103 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC002N14 */
            pr_default.execute(12, new Object[] {A841LoteArquivoAnexo_LoteCod});
            if ( (pr_default.getStatus(12) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Lote Arquivo Anexo_Lote'.", "ForeignKeyNotFound", 1, "LOTEARQUIVOANEXO_LOTECOD");
               AnyError = 1;
            }
            pr_default.close(12);
         }
         else
         {
            Gx_mode = "UPD";
            Z841LoteArquivoAnexo_LoteCod = A841LoteArquivoAnexo_LoteCod;
            Z836LoteArquivoAnexo_Data = A836LoteArquivoAnexo_Data;
         }
         ZM2N103( -5) ;
         OnLoadActions2N103( ) ;
         AddRow2N103( ) ;
         ScanKeyEnd2N103( ) ;
         if ( RcdFound103 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars103( bcLoteArquivoAnexo, 0) ;
         nKeyPressed = 1;
         GetKey2N103( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert2N103( ) ;
         }
         else
         {
            if ( RcdFound103 == 1 )
            {
               if ( ( A841LoteArquivoAnexo_LoteCod != Z841LoteArquivoAnexo_LoteCod ) || ( A836LoteArquivoAnexo_Data != Z836LoteArquivoAnexo_Data ) )
               {
                  A841LoteArquivoAnexo_LoteCod = Z841LoteArquivoAnexo_LoteCod;
                  A836LoteArquivoAnexo_Data = Z836LoteArquivoAnexo_Data;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update2N103( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A841LoteArquivoAnexo_LoteCod != Z841LoteArquivoAnexo_LoteCod ) || ( A836LoteArquivoAnexo_Data != Z836LoteArquivoAnexo_Data ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert2N103( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert2N103( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow103( bcLoteArquivoAnexo) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars103( bcLoteArquivoAnexo, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey2N103( ) ;
         if ( RcdFound103 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A841LoteArquivoAnexo_LoteCod != Z841LoteArquivoAnexo_LoteCod ) || ( A836LoteArquivoAnexo_Data != Z836LoteArquivoAnexo_Data ) )
            {
               A841LoteArquivoAnexo_LoteCod = Z841LoteArquivoAnexo_LoteCod;
               A836LoteArquivoAnexo_Data = Z836LoteArquivoAnexo_Data;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A841LoteArquivoAnexo_LoteCod != Z841LoteArquivoAnexo_LoteCod ) || ( A836LoteArquivoAnexo_Data != Z836LoteArquivoAnexo_Data ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(10);
         pr_default.close(12);
         context.RollbackDataStores( "LoteArquivoAnexo_BC");
         VarsToRow103( bcLoteArquivoAnexo) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcLoteArquivoAnexo.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcLoteArquivoAnexo.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcLoteArquivoAnexo )
         {
            bcLoteArquivoAnexo = (SdtLoteArquivoAnexo)(sdt);
            if ( StringUtil.StrCmp(bcLoteArquivoAnexo.gxTpr_Mode, "") == 0 )
            {
               bcLoteArquivoAnexo.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow103( bcLoteArquivoAnexo) ;
            }
            else
            {
               RowToVars103( bcLoteArquivoAnexo, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcLoteArquivoAnexo.gxTpr_Mode, "") == 0 )
            {
               bcLoteArquivoAnexo.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars103( bcLoteArquivoAnexo, 1) ;
         return  ;
      }

      public SdtLoteArquivoAnexo LoteArquivoAnexo_BC
      {
         get {
            return bcLoteArquivoAnexo ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(10);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z836LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         A836LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         AV14Pgmname = "";
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1027LoteArquivoAnexo_Verificador = "";
         A1027LoteArquivoAnexo_Verificador = "";
         Z646TipoDocumento_Nome = "";
         A646TipoDocumento_Nome = "";
         Z838LoteArquivoAnexo_Arquivo = "";
         A838LoteArquivoAnexo_Arquivo = "";
         Z837LoteArquivoAnexo_Descricao = "";
         A837LoteArquivoAnexo_Descricao = "";
         Z840LoteArquivoAnexo_TipoArq = "";
         A840LoteArquivoAnexo_TipoArq = "";
         Z839LoteArquivoAnexo_NomeArq = "";
         A839LoteArquivoAnexo_NomeArq = "";
         BC002N6_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         BC002N6_A837LoteArquivoAnexo_Descricao = new String[] {""} ;
         BC002N6_n837LoteArquivoAnexo_Descricao = new bool[] {false} ;
         BC002N6_A646TipoDocumento_Nome = new String[] {""} ;
         BC002N6_A1027LoteArquivoAnexo_Verificador = new String[] {""} ;
         BC002N6_n1027LoteArquivoAnexo_Verificador = new bool[] {false} ;
         BC002N6_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         BC002N6_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         BC002N6_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         BC002N6_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         BC002N6_A645TipoDocumento_Codigo = new int[1] ;
         BC002N6_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC002N6_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         BC002N6_A838LoteArquivoAnexo_Arquivo = new String[] {""} ;
         BC002N6_n838LoteArquivoAnexo_Arquivo = new bool[] {false} ;
         A838LoteArquivoAnexo_Arquivo_Filetype = "";
         A838LoteArquivoAnexo_Arquivo_Filename = "";
         BC002N5_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         BC002N4_A646TipoDocumento_Nome = new String[] {""} ;
         BC002N7_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         BC002N7_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         BC002N3_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         BC002N3_A837LoteArquivoAnexo_Descricao = new String[] {""} ;
         BC002N3_n837LoteArquivoAnexo_Descricao = new bool[] {false} ;
         BC002N3_A1027LoteArquivoAnexo_Verificador = new String[] {""} ;
         BC002N3_n1027LoteArquivoAnexo_Verificador = new bool[] {false} ;
         BC002N3_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         BC002N3_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         BC002N3_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         BC002N3_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         BC002N3_A645TipoDocumento_Codigo = new int[1] ;
         BC002N3_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC002N3_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         BC002N3_A838LoteArquivoAnexo_Arquivo = new String[] {""} ;
         BC002N3_n838LoteArquivoAnexo_Arquivo = new bool[] {false} ;
         sMode103 = "";
         BC002N2_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         BC002N2_A837LoteArquivoAnexo_Descricao = new String[] {""} ;
         BC002N2_n837LoteArquivoAnexo_Descricao = new bool[] {false} ;
         BC002N2_A1027LoteArquivoAnexo_Verificador = new String[] {""} ;
         BC002N2_n1027LoteArquivoAnexo_Verificador = new bool[] {false} ;
         BC002N2_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         BC002N2_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         BC002N2_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         BC002N2_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         BC002N2_A645TipoDocumento_Codigo = new int[1] ;
         BC002N2_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC002N2_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         BC002N2_A838LoteArquivoAnexo_Arquivo = new String[] {""} ;
         BC002N2_n838LoteArquivoAnexo_Arquivo = new bool[] {false} ;
         BC002N12_A646TipoDocumento_Nome = new String[] {""} ;
         BC002N13_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         BC002N13_A837LoteArquivoAnexo_Descricao = new String[] {""} ;
         BC002N13_n837LoteArquivoAnexo_Descricao = new bool[] {false} ;
         BC002N13_A646TipoDocumento_Nome = new String[] {""} ;
         BC002N13_A1027LoteArquivoAnexo_Verificador = new String[] {""} ;
         BC002N13_n1027LoteArquivoAnexo_Verificador = new bool[] {false} ;
         BC002N13_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         BC002N13_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         BC002N13_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         BC002N13_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         BC002N13_A645TipoDocumento_Codigo = new int[1] ;
         BC002N13_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC002N13_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         BC002N13_A838LoteArquivoAnexo_Arquivo = new String[] {""} ;
         BC002N13_n838LoteArquivoAnexo_Arquivo = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC002N14_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.lotearquivoanexo_bc__default(),
            new Object[][] {
                new Object[] {
               BC002N2_A836LoteArquivoAnexo_Data, BC002N2_A837LoteArquivoAnexo_Descricao, BC002N2_n837LoteArquivoAnexo_Descricao, BC002N2_A1027LoteArquivoAnexo_Verificador, BC002N2_n1027LoteArquivoAnexo_Verificador, BC002N2_A840LoteArquivoAnexo_TipoArq, BC002N2_n840LoteArquivoAnexo_TipoArq, BC002N2_A839LoteArquivoAnexo_NomeArq, BC002N2_n839LoteArquivoAnexo_NomeArq, BC002N2_A645TipoDocumento_Codigo,
               BC002N2_n645TipoDocumento_Codigo, BC002N2_A841LoteArquivoAnexo_LoteCod, BC002N2_A838LoteArquivoAnexo_Arquivo, BC002N2_n838LoteArquivoAnexo_Arquivo
               }
               , new Object[] {
               BC002N3_A836LoteArquivoAnexo_Data, BC002N3_A837LoteArquivoAnexo_Descricao, BC002N3_n837LoteArquivoAnexo_Descricao, BC002N3_A1027LoteArquivoAnexo_Verificador, BC002N3_n1027LoteArquivoAnexo_Verificador, BC002N3_A840LoteArquivoAnexo_TipoArq, BC002N3_n840LoteArquivoAnexo_TipoArq, BC002N3_A839LoteArquivoAnexo_NomeArq, BC002N3_n839LoteArquivoAnexo_NomeArq, BC002N3_A645TipoDocumento_Codigo,
               BC002N3_n645TipoDocumento_Codigo, BC002N3_A841LoteArquivoAnexo_LoteCod, BC002N3_A838LoteArquivoAnexo_Arquivo, BC002N3_n838LoteArquivoAnexo_Arquivo
               }
               , new Object[] {
               BC002N4_A646TipoDocumento_Nome
               }
               , new Object[] {
               BC002N5_A841LoteArquivoAnexo_LoteCod
               }
               , new Object[] {
               BC002N6_A836LoteArquivoAnexo_Data, BC002N6_A837LoteArquivoAnexo_Descricao, BC002N6_n837LoteArquivoAnexo_Descricao, BC002N6_A646TipoDocumento_Nome, BC002N6_A1027LoteArquivoAnexo_Verificador, BC002N6_n1027LoteArquivoAnexo_Verificador, BC002N6_A840LoteArquivoAnexo_TipoArq, BC002N6_n840LoteArquivoAnexo_TipoArq, BC002N6_A839LoteArquivoAnexo_NomeArq, BC002N6_n839LoteArquivoAnexo_NomeArq,
               BC002N6_A645TipoDocumento_Codigo, BC002N6_n645TipoDocumento_Codigo, BC002N6_A841LoteArquivoAnexo_LoteCod, BC002N6_A838LoteArquivoAnexo_Arquivo, BC002N6_n838LoteArquivoAnexo_Arquivo
               }
               , new Object[] {
               BC002N7_A841LoteArquivoAnexo_LoteCod, BC002N7_A836LoteArquivoAnexo_Data
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC002N12_A646TipoDocumento_Nome
               }
               , new Object[] {
               BC002N13_A836LoteArquivoAnexo_Data, BC002N13_A837LoteArquivoAnexo_Descricao, BC002N13_n837LoteArquivoAnexo_Descricao, BC002N13_A646TipoDocumento_Nome, BC002N13_A1027LoteArquivoAnexo_Verificador, BC002N13_n1027LoteArquivoAnexo_Verificador, BC002N13_A840LoteArquivoAnexo_TipoArq, BC002N13_n840LoteArquivoAnexo_TipoArq, BC002N13_A839LoteArquivoAnexo_NomeArq, BC002N13_n839LoteArquivoAnexo_NomeArq,
               BC002N13_A645TipoDocumento_Codigo, BC002N13_n645TipoDocumento_Codigo, BC002N13_A841LoteArquivoAnexo_LoteCod, BC002N13_A838LoteArquivoAnexo_Arquivo, BC002N13_n838LoteArquivoAnexo_Arquivo
               }
               , new Object[] {
               BC002N14_A841LoteArquivoAnexo_LoteCod
               }
            }
         );
         Z836LoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         A836LoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV14Pgmname = "LoteArquivoAnexo_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E122N2 */
         E122N2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short RcdFound103 ;
      private int trnEnded ;
      private int Z841LoteArquivoAnexo_LoteCod ;
      private int A841LoteArquivoAnexo_LoteCod ;
      private int AV15GXV1 ;
      private int AV12Insert_TipoDocumento_Codigo ;
      private int Z645TipoDocumento_Codigo ;
      private int A645TipoDocumento_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV14Pgmname ;
      private String Z646TipoDocumento_Nome ;
      private String A646TipoDocumento_Nome ;
      private String Z840LoteArquivoAnexo_TipoArq ;
      private String A840LoteArquivoAnexo_TipoArq ;
      private String Z839LoteArquivoAnexo_NomeArq ;
      private String A839LoteArquivoAnexo_NomeArq ;
      private String A838LoteArquivoAnexo_Arquivo_Filetype ;
      private String A838LoteArquivoAnexo_Arquivo_Filename ;
      private String sMode103 ;
      private DateTime Z836LoteArquivoAnexo_Data ;
      private DateTime A836LoteArquivoAnexo_Data ;
      private bool n837LoteArquivoAnexo_Descricao ;
      private bool n1027LoteArquivoAnexo_Verificador ;
      private bool n840LoteArquivoAnexo_TipoArq ;
      private bool n839LoteArquivoAnexo_NomeArq ;
      private bool n645TipoDocumento_Codigo ;
      private bool n838LoteArquivoAnexo_Arquivo ;
      private String Z837LoteArquivoAnexo_Descricao ;
      private String A837LoteArquivoAnexo_Descricao ;
      private String Z1027LoteArquivoAnexo_Verificador ;
      private String A1027LoteArquivoAnexo_Verificador ;
      private String Z838LoteArquivoAnexo_Arquivo ;
      private String A838LoteArquivoAnexo_Arquivo ;
      private IGxSession AV11WebSession ;
      private SdtLoteArquivoAnexo bcLoteArquivoAnexo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private DateTime[] BC002N6_A836LoteArquivoAnexo_Data ;
      private String[] BC002N6_A837LoteArquivoAnexo_Descricao ;
      private bool[] BC002N6_n837LoteArquivoAnexo_Descricao ;
      private String[] BC002N6_A646TipoDocumento_Nome ;
      private String[] BC002N6_A1027LoteArquivoAnexo_Verificador ;
      private bool[] BC002N6_n1027LoteArquivoAnexo_Verificador ;
      private String[] BC002N6_A840LoteArquivoAnexo_TipoArq ;
      private bool[] BC002N6_n840LoteArquivoAnexo_TipoArq ;
      private String[] BC002N6_A839LoteArquivoAnexo_NomeArq ;
      private bool[] BC002N6_n839LoteArquivoAnexo_NomeArq ;
      private int[] BC002N6_A645TipoDocumento_Codigo ;
      private bool[] BC002N6_n645TipoDocumento_Codigo ;
      private int[] BC002N6_A841LoteArquivoAnexo_LoteCod ;
      private String[] BC002N6_A838LoteArquivoAnexo_Arquivo ;
      private bool[] BC002N6_n838LoteArquivoAnexo_Arquivo ;
      private int[] BC002N5_A841LoteArquivoAnexo_LoteCod ;
      private String[] BC002N4_A646TipoDocumento_Nome ;
      private int[] BC002N7_A841LoteArquivoAnexo_LoteCod ;
      private DateTime[] BC002N7_A836LoteArquivoAnexo_Data ;
      private DateTime[] BC002N3_A836LoteArquivoAnexo_Data ;
      private String[] BC002N3_A837LoteArquivoAnexo_Descricao ;
      private bool[] BC002N3_n837LoteArquivoAnexo_Descricao ;
      private String[] BC002N3_A1027LoteArquivoAnexo_Verificador ;
      private bool[] BC002N3_n1027LoteArquivoAnexo_Verificador ;
      private String[] BC002N3_A840LoteArquivoAnexo_TipoArq ;
      private bool[] BC002N3_n840LoteArquivoAnexo_TipoArq ;
      private String[] BC002N3_A839LoteArquivoAnexo_NomeArq ;
      private bool[] BC002N3_n839LoteArquivoAnexo_NomeArq ;
      private int[] BC002N3_A645TipoDocumento_Codigo ;
      private bool[] BC002N3_n645TipoDocumento_Codigo ;
      private int[] BC002N3_A841LoteArquivoAnexo_LoteCod ;
      private String[] BC002N3_A838LoteArquivoAnexo_Arquivo ;
      private bool[] BC002N3_n838LoteArquivoAnexo_Arquivo ;
      private DateTime[] BC002N2_A836LoteArquivoAnexo_Data ;
      private String[] BC002N2_A837LoteArquivoAnexo_Descricao ;
      private bool[] BC002N2_n837LoteArquivoAnexo_Descricao ;
      private String[] BC002N2_A1027LoteArquivoAnexo_Verificador ;
      private bool[] BC002N2_n1027LoteArquivoAnexo_Verificador ;
      private String[] BC002N2_A840LoteArquivoAnexo_TipoArq ;
      private bool[] BC002N2_n840LoteArquivoAnexo_TipoArq ;
      private String[] BC002N2_A839LoteArquivoAnexo_NomeArq ;
      private bool[] BC002N2_n839LoteArquivoAnexo_NomeArq ;
      private int[] BC002N2_A645TipoDocumento_Codigo ;
      private bool[] BC002N2_n645TipoDocumento_Codigo ;
      private int[] BC002N2_A841LoteArquivoAnexo_LoteCod ;
      private String[] BC002N2_A838LoteArquivoAnexo_Arquivo ;
      private bool[] BC002N2_n838LoteArquivoAnexo_Arquivo ;
      private String[] BC002N12_A646TipoDocumento_Nome ;
      private DateTime[] BC002N13_A836LoteArquivoAnexo_Data ;
      private String[] BC002N13_A837LoteArquivoAnexo_Descricao ;
      private bool[] BC002N13_n837LoteArquivoAnexo_Descricao ;
      private String[] BC002N13_A646TipoDocumento_Nome ;
      private String[] BC002N13_A1027LoteArquivoAnexo_Verificador ;
      private bool[] BC002N13_n1027LoteArquivoAnexo_Verificador ;
      private String[] BC002N13_A840LoteArquivoAnexo_TipoArq ;
      private bool[] BC002N13_n840LoteArquivoAnexo_TipoArq ;
      private String[] BC002N13_A839LoteArquivoAnexo_NomeArq ;
      private bool[] BC002N13_n839LoteArquivoAnexo_NomeArq ;
      private int[] BC002N13_A645TipoDocumento_Codigo ;
      private bool[] BC002N13_n645TipoDocumento_Codigo ;
      private int[] BC002N13_A841LoteArquivoAnexo_LoteCod ;
      private String[] BC002N13_A838LoteArquivoAnexo_Arquivo ;
      private bool[] BC002N13_n838LoteArquivoAnexo_Arquivo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] BC002N14_A841LoteArquivoAnexo_LoteCod ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class lotearquivoanexo_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC002N6 ;
          prmBC002N6 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmBC002N5 ;
          prmBC002N5 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002N4 ;
          prmBC002N4 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002N7 ;
          prmBC002N7 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmBC002N3 ;
          prmBC002N3 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmBC002N2 ;
          prmBC002N2 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmBC002N8 ;
          prmBC002N8 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@LoteArquivoAnexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@LoteArquivoAnexo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@LoteArquivoAnexo_Verificador",SqlDbType.VarChar,40,0} ,
          new Object[] {"@LoteArquivoAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@LoteArquivoAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002N9 ;
          prmBC002N9 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@LoteArquivoAnexo_Verificador",SqlDbType.VarChar,40,0} ,
          new Object[] {"@LoteArquivoAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@LoteArquivoAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmBC002N10 ;
          prmBC002N10 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmBC002N11 ;
          prmBC002N11 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmBC002N12 ;
          prmBC002N12 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002N13 ;
          prmBC002N13 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmBC002N14 ;
          prmBC002N14 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC002N2", "SELECT [LoteArquivoAnexo_Data], [LoteArquivoAnexo_Descricao], [LoteArquivoAnexo_Verificador], [LoteArquivoAnexo_TipoArq], [LoteArquivoAnexo_NomeArq], [TipoDocumento_Codigo], [LoteArquivoAnexo_LoteCod] AS LoteArquivoAnexo_LoteCod, [LoteArquivoAnexo_Arquivo] FROM [LoteArquivoAnexo] WITH (UPDLOCK) WHERE [LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod AND [LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002N2,1,0,true,false )
             ,new CursorDef("BC002N3", "SELECT [LoteArquivoAnexo_Data], [LoteArquivoAnexo_Descricao], [LoteArquivoAnexo_Verificador], [LoteArquivoAnexo_TipoArq], [LoteArquivoAnexo_NomeArq], [TipoDocumento_Codigo], [LoteArquivoAnexo_LoteCod] AS LoteArquivoAnexo_LoteCod, [LoteArquivoAnexo_Arquivo] FROM [LoteArquivoAnexo] WITH (NOLOCK) WHERE [LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod AND [LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002N3,1,0,true,false )
             ,new CursorDef("BC002N4", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002N4,1,0,true,false )
             ,new CursorDef("BC002N5", "SELECT [Lote_Codigo] AS LoteArquivoAnexo_LoteCod FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @LoteArquivoAnexo_LoteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002N5,1,0,true,false )
             ,new CursorDef("BC002N6", "SELECT TM1.[LoteArquivoAnexo_Data], TM1.[LoteArquivoAnexo_Descricao], T2.[TipoDocumento_Nome], TM1.[LoteArquivoAnexo_Verificador], TM1.[LoteArquivoAnexo_TipoArq], TM1.[LoteArquivoAnexo_NomeArq], TM1.[TipoDocumento_Codigo], TM1.[LoteArquivoAnexo_LoteCod] AS LoteArquivoAnexo_LoteCod, TM1.[LoteArquivoAnexo_Arquivo] FROM ([LoteArquivoAnexo] TM1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = TM1.[TipoDocumento_Codigo]) WHERE TM1.[LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod and TM1.[LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data ORDER BY TM1.[LoteArquivoAnexo_LoteCod], TM1.[LoteArquivoAnexo_Data]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002N6,100,0,true,false )
             ,new CursorDef("BC002N7", "SELECT [LoteArquivoAnexo_LoteCod] AS LoteArquivoAnexo_LoteCod, [LoteArquivoAnexo_Data] FROM [LoteArquivoAnexo] WITH (NOLOCK) WHERE [LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod AND [LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002N7,1,0,true,false )
             ,new CursorDef("BC002N8", "INSERT INTO [LoteArquivoAnexo]([LoteArquivoAnexo_Data], [LoteArquivoAnexo_Arquivo], [LoteArquivoAnexo_Descricao], [LoteArquivoAnexo_Verificador], [LoteArquivoAnexo_TipoArq], [LoteArquivoAnexo_NomeArq], [TipoDocumento_Codigo], [LoteArquivoAnexo_LoteCod]) VALUES(@LoteArquivoAnexo_Data, @LoteArquivoAnexo_Arquivo, @LoteArquivoAnexo_Descricao, @LoteArquivoAnexo_Verificador, @LoteArquivoAnexo_TipoArq, @LoteArquivoAnexo_NomeArq, @TipoDocumento_Codigo, @LoteArquivoAnexo_LoteCod)", GxErrorMask.GX_NOMASK,prmBC002N8)
             ,new CursorDef("BC002N9", "UPDATE [LoteArquivoAnexo] SET [LoteArquivoAnexo_Descricao]=@LoteArquivoAnexo_Descricao, [LoteArquivoAnexo_Verificador]=@LoteArquivoAnexo_Verificador, [LoteArquivoAnexo_TipoArq]=@LoteArquivoAnexo_TipoArq, [LoteArquivoAnexo_NomeArq]=@LoteArquivoAnexo_NomeArq, [TipoDocumento_Codigo]=@TipoDocumento_Codigo  WHERE [LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod AND [LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data", GxErrorMask.GX_NOMASK,prmBC002N9)
             ,new CursorDef("BC002N10", "UPDATE [LoteArquivoAnexo] SET [LoteArquivoAnexo_Arquivo]=@LoteArquivoAnexo_Arquivo  WHERE [LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod AND [LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data", GxErrorMask.GX_NOMASK,prmBC002N10)
             ,new CursorDef("BC002N11", "DELETE FROM [LoteArquivoAnexo]  WHERE [LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod AND [LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data", GxErrorMask.GX_NOMASK,prmBC002N11)
             ,new CursorDef("BC002N12", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002N12,1,0,true,false )
             ,new CursorDef("BC002N13", "SELECT TM1.[LoteArquivoAnexo_Data], TM1.[LoteArquivoAnexo_Descricao], T2.[TipoDocumento_Nome], TM1.[LoteArquivoAnexo_Verificador], TM1.[LoteArquivoAnexo_TipoArq], TM1.[LoteArquivoAnexo_NomeArq], TM1.[TipoDocumento_Codigo], TM1.[LoteArquivoAnexo_LoteCod] AS LoteArquivoAnexo_LoteCod, TM1.[LoteArquivoAnexo_Arquivo] FROM ([LoteArquivoAnexo] TM1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = TM1.[TipoDocumento_Codigo]) WHERE TM1.[LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod and TM1.[LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data ORDER BY TM1.[LoteArquivoAnexo_LoteCod], TM1.[LoteArquivoAnexo_Data]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002N13,100,0,true,false )
             ,new CursorDef("BC002N14", "SELECT [Lote_Codigo] AS LoteArquivoAnexo_LoteCod FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @LoteArquivoAnexo_LoteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002N14,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((String[]) buf[12])[0] = rslt.getBLOBFile(8, rslt.getString(4, 10), rslt.getString(5, 50)) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 1 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((String[]) buf[12])[0] = rslt.getBLOBFile(8, rslt.getString(4, 10), rslt.getString(5, 50)) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((String[]) buf[13])[0] = rslt.getBLOBFile(9, rslt.getString(5, 10), rslt.getString(6, 50)) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 11 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((String[]) buf[13])[0] = rslt.getBLOBFile(9, rslt.getString(5, 10), rslt.getString(6, 50)) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 6 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[12]);
                }
                stmt.SetParameter(8, (int)parms[13]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                stmt.SetParameterDatetime(7, (DateTime)parms[11]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameterDatetime(3, (DateTime)parms[3]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
