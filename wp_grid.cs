/*
               File: WP_Grid
        Description: Grid
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:24:55.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_grid : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_grid( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_grid( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_28 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_28_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_28_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV65TFContagemResultado_DataEntrega = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContagemResultado_DataEntrega", context.localUtil.Format(AV65TFContagemResultado_DataEntrega, "99/99/99"));
               AV66TFContagemResultado_DataEntrega_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemResultado_DataEntrega_To", context.localUtil.Format(AV66TFContagemResultado_DataEntrega_To, "99/99/99"));
               AV71TFContagemResultado_DataDmn = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemResultado_DataDmn", context.localUtil.Format(AV71TFContagemResultado_DataDmn, "99/99/99"));
               AV72TFContagemResultado_DataDmn_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFContagemResultado_DataDmn_To", context.localUtil.Format(AV72TFContagemResultado_DataDmn_To, "99/99/99"));
               AV77TFContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(context.localUtil.ParseDTimeParm( GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContagemResultado_HoraEntrega", context.localUtil.TToC( AV77TFContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " "));
               AV78TFContagemResultado_HoraEntrega_To = DateTimeUtil.ResetDate(context.localUtil.ParseDTimeParm( GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContagemResultado_HoraEntrega_To", context.localUtil.TToC( AV78TFContagemResultado_HoraEntrega_To, 0, 5, 0, 3, "/", ":", " "));
               AV83TFContagemResultado_ContratadaPessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFContagemResultado_ContratadaPessoaNom", AV83TFContagemResultado_ContratadaPessoaNom);
               AV84TFContagemResultado_ContratadaPessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContagemResultado_ContratadaPessoaNom_Sel", AV84TFContagemResultado_ContratadaPessoaNom_Sel);
               AV87TFContagemResultado_DemandaFM = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFContagemResultado_DemandaFM", AV87TFContagemResultado_DemandaFM);
               AV88TFContagemResultado_DemandaFM_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFContagemResultado_DemandaFM_Sel", AV88TFContagemResultado_DemandaFM_Sel);
               AV91TFContagemResultado_ServicoSigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFContagemResultado_ServicoSigla", AV91TFContagemResultado_ServicoSigla);
               AV92TFContagemResultado_ServicoSigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFContagemResultado_ServicoSigla_Sel", AV92TFContagemResultado_ServicoSigla_Sel);
               AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace", AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace);
               AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace", AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace);
               AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace", AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace);
               AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace", AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace);
               AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace", AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace);
               AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace", AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace);
               AV114Pgmname = GetNextPar( );
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV65TFContagemResultado_DataEntrega, AV66TFContagemResultado_DataEntrega_To, AV71TFContagemResultado_DataDmn, AV72TFContagemResultado_DataDmn_To, AV77TFContagemResultado_HoraEntrega, AV78TFContagemResultado_HoraEntrega_To, AV83TFContagemResultado_ContratadaPessoaNom, AV84TFContagemResultado_ContratadaPessoaNom_Sel, AV87TFContagemResultado_DemandaFM, AV88TFContagemResultado_DemandaFM_Sel, AV91TFContagemResultado_ServicoSigla, AV92TFContagemResultado_ServicoSigla_Sel, AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace, AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace, AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace, AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace, AV114Pgmname, A456ContagemResultado_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAKV2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTKV2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216245561");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/ext/ext-all.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/ext/ext-all.js", "");
         context.AddJavascriptSource("gxui/gxui-all.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("gxui/gxui-all.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_grid.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DATAENTREGA", context.localUtil.Format(AV65TFContagemResultado_DataEntrega, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DATAENTREGA_TO", context.localUtil.Format(AV66TFContagemResultado_DataEntrega_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DATADMN", context.localUtil.Format(AV71TFContagemResultado_DataDmn, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DATADMN_TO", context.localUtil.Format(AV72TFContagemResultado_DataDmn_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_HORAENTREGA", context.localUtil.TToC( AV77TFContagemResultado_HoraEntrega, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_HORAENTREGA_TO", context.localUtil.TToC( AV78TFContagemResultado_HoraEntrega_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM", StringUtil.RTrim( AV83TFContagemResultado_ContratadaPessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL", StringUtil.RTrim( AV84TFContagemResultado_ContratadaPessoaNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM", AV87TFContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM_SEL", AV88TFContagemResultado_DemandaFM_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_SERVICOSIGLA", StringUtil.RTrim( AV91TFContagemResultado_ServicoSigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL", StringUtil.RTrim( AV92TFContagemResultado_ServicoSigla_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_28", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_28), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV96GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV97GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV94DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV94DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_DATAENTREGATITLEFILTERDATA", AV64ContagemResultado_DataEntregaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_DATAENTREGATITLEFILTERDATA", AV64ContagemResultado_DataEntregaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_DATADMNTITLEFILTERDATA", AV70ContagemResultado_DataDmnTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_DATADMNTITLEFILTERDATA", AV70ContagemResultado_DataDmnTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_HORAENTREGATITLEFILTERDATA", AV76ContagemResultado_HoraEntregaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_HORAENTREGATITLEFILTERDATA", AV76ContagemResultado_HoraEntregaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLEFILTERDATA", AV82ContagemResultado_ContratadaPessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLEFILTERDATA", AV82ContagemResultado_ContratadaPessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA", AV86ContagemResultado_DemandaFMTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA", AV86ContagemResultado_DemandaFMTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_SERVICOSIGLATITLEFILTERDATA", AV90ContagemResultado_ServicoSiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_SERVICOSIGLATITLEFILTERDATA", AV90ContagemResultado_ServicoSiglaTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV114Pgmname));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Caption", StringUtil.RTrim( Ddo_contagemresultado_dataentrega_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_dataentrega_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Cls", StringUtil.RTrim( Ddo_contagemresultado_dataentrega_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_dataentrega_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultado_dataentrega_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_dataentrega_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_dataentrega_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_dataentrega_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_dataentrega_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_dataentrega_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_dataentrega_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_dataentrega_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_dataentrega_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_dataentrega_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultado_dataentrega_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultado_dataentrega_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_dataentrega_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Caption", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Cls", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_datadmn_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_datadmn_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_datadmn_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_datadmn_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_datadmn_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Caption", StringUtil.RTrim( Ddo_contagemresultado_horaentrega_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_horaentrega_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Cls", StringUtil.RTrim( Ddo_contagemresultado_horaentrega_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_horaentrega_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultado_horaentrega_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_horaentrega_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_horaentrega_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_horaentrega_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_horaentrega_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_horaentrega_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_horaentrega_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_horaentrega_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_horaentrega_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_horaentrega_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultado_horaentrega_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultado_horaentrega_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_horaentrega_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Caption", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Cls", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_contratadapessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_contratadapessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_contratadapessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_contratadapessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_contratadapessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultado_contratadapessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Caption", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Cls", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalisttype", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalistproc", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultado_demandafm_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Loadingdata", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Caption", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Cls", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_servicosigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_servicosigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_servicosigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_servicosigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_servicosigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Datalisttype", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Datalistproc", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultado_servicosigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Loadingdata", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRID_Grouping", StringUtil.RTrim( subGrid_Grouping));
         GxWebStd.gx_hidden_field( context, "GRID_Class", StringUtil.RTrim( subGrid_Class));
         GxWebStd.gx_hidden_field( context, "GRID_Addtoparentgxuicontrol", StringUtil.RTrim( subGrid_Addtoparentgxuicontrol));
         GxWebStd.gx_hidden_field( context, "GRID_Groupfield", StringUtil.RTrim( subGrid_Groupfield));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_dataentrega_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_dataentrega_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATAENTREGA_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultado_dataentrega_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DATADMN_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultado_datadmn_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_horaentrega_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_horaentrega_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_HORAENTREGA_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultado_horaentrega_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultado_contratadapessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_DEMANDAFM_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultado_servicosigla_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEKV2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTKV2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_grid.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_Grid" ;
      }

      public override String GetPgmdesc( )
      {
         return "Grid" ;
      }

      protected void WBKV0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_KV2( true) ;
         }
         else
         {
            wb_table1_2_KV2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_KV2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_28_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultado_dataentrega_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_dataentrega_Internalname, context.localUtil.Format(AV65TFContagemResultado_DataEntrega, "99/99/99"), context.localUtil.Format( AV65TFContagemResultado_DataEntrega, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_dataentrega_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_dataentrega_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Grid.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultado_dataentrega_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultado_dataentrega_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Grid.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_28_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultado_dataentrega_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_dataentrega_to_Internalname, context.localUtil.Format(AV66TFContagemResultado_DataEntrega_To, "99/99/99"), context.localUtil.Format( AV66TFContagemResultado_DataEntrega_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_dataentrega_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_dataentrega_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Grid.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultado_dataentrega_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultado_dataentrega_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Grid.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contagemresultado_dataentregaauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_28_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultado_dataentregaauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultado_dataentregaauxdate_Internalname, context.localUtil.Format(AV67DDO_ContagemResultado_DataEntregaAuxDate, "99/99/99"), context.localUtil.Format( AV67DDO_ContagemResultado_DataEntregaAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultado_dataentregaauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Grid.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultado_dataentregaauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Grid.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_28_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultado_dataentregaauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultado_dataentregaauxdateto_Internalname, context.localUtil.Format(AV68DDO_ContagemResultado_DataEntregaAuxDateTo, "99/99/99"), context.localUtil.Format( AV68DDO_ContagemResultado_DataEntregaAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultado_dataentregaauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Grid.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultado_dataentregaauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Grid.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_28_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultado_datadmn_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_datadmn_Internalname, context.localUtil.Format(AV71TFContagemResultado_DataDmn, "99/99/99"), context.localUtil.Format( AV71TFContagemResultado_DataDmn, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_datadmn_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_datadmn_Visible, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Grid.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultado_datadmn_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultado_datadmn_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Grid.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_28_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultado_datadmn_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_datadmn_to_Internalname, context.localUtil.Format(AV72TFContagemResultado_DataDmn_To, "99/99/99"), context.localUtil.Format( AV72TFContagemResultado_DataDmn_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_datadmn_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_datadmn_to_Visible, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Grid.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultado_datadmn_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultado_datadmn_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Grid.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contagemresultado_datadmnauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_28_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultado_datadmnauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultado_datadmnauxdate_Internalname, context.localUtil.Format(AV73DDO_ContagemResultado_DataDmnAuxDate, "99/99/99"), context.localUtil.Format( AV73DDO_ContagemResultado_DataDmnAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultado_datadmnauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Grid.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultado_datadmnauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Grid.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_28_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultado_datadmnauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultado_datadmnauxdateto_Internalname, context.localUtil.Format(AV74DDO_ContagemResultado_DataDmnAuxDateTo, "99/99/99"), context.localUtil.Format( AV74DDO_ContagemResultado_DataDmnAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultado_datadmnauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Grid.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultado_datadmnauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Grid.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_28_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultado_horaentrega_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_horaentrega_Internalname, context.localUtil.TToC( AV77TFContagemResultado_HoraEntrega, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV77TFContagemResultado_HoraEntrega, "99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 0,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_horaentrega_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_horaentrega_Visible, 1, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Grid.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultado_horaentrega_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultado_horaentrega_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Grid.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_28_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultado_horaentrega_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_horaentrega_to_Internalname, context.localUtil.TToC( AV78TFContagemResultado_HoraEntrega_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV78TFContagemResultado_HoraEntrega_To, "99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 0,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_horaentrega_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_horaentrega_to_Visible, 1, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Grid.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultado_horaentrega_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultado_horaentrega_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Grid.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contagemresultado_horaentregaauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_28_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultado_horaentregaauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultado_horaentregaauxdate_Internalname, context.localUtil.Format(AV79DDO_ContagemResultado_HoraEntregaAuxDate, "99/99/99"), context.localUtil.Format( AV79DDO_ContagemResultado_HoraEntregaAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultado_horaentregaauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Grid.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultado_horaentregaauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Grid.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_28_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultado_horaentregaauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultado_horaentregaauxdateto_Internalname, context.localUtil.Format(AV80DDO_ContagemResultado_HoraEntregaAuxDateTo, "99/99/99"), context.localUtil.Format( AV80DDO_ContagemResultado_HoraEntregaAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultado_horaentregaauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Grid.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultado_horaentregaauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Grid.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_28_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_contratadapessoanom_Internalname, StringUtil.RTrim( AV83TFContagemResultado_ContratadaPessoaNom), StringUtil.RTrim( context.localUtil.Format( AV83TFContagemResultado_ContratadaPessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_contratadapessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontagemresultado_contratadapessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Grid.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_28_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_contratadapessoanom_sel_Internalname, StringUtil.RTrim( AV84TFContagemResultado_ContratadaPessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV84TFContagemResultado_ContratadaPessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_contratadapessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontagemresultado_contratadapessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Grid.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_28_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_demandafm_Internalname, AV87TFContagemResultado_DemandaFM, StringUtil.RTrim( context.localUtil.Format( AV87TFContagemResultado_DemandaFM, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_demandafm_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_demandafm_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Grid.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_28_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_demandafm_sel_Internalname, AV88TFContagemResultado_DemandaFM_Sel, StringUtil.RTrim( context.localUtil.Format( AV88TFContagemResultado_DemandaFM_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_demandafm_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_demandafm_sel_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Grid.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_28_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_servicosigla_Internalname, StringUtil.RTrim( AV91TFContagemResultado_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( AV91TFContagemResultado_ServicoSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_servicosigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_servicosigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Grid.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_28_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_servicosigla_sel_Internalname, StringUtil.RTrim( AV92TFContagemResultado_ServicoSigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV92TFContagemResultado_ServicoSigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_servicosigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_servicosigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Grid.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_DATAENTREGAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_28_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_dataentregatitlecontrolidtoreplace_Internalname, AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", 0, edtavDdo_contagemresultado_dataentregatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WP_Grid.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_DATADMNContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_28_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Internalname, AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"", 0, edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WP_Grid.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_HORAENTREGAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_28_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_horaentregatitlecontrolidtoreplace_Internalname, AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", 0, edtavDdo_contagemresultado_horaentregatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WP_Grid.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_28_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Internalname, AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", 0, edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WP_Grid.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_DEMANDAFMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_28_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname, AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", 0, edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WP_Grid.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_28_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_servicosiglatitlecontrolidtoreplace_Internalname, AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", 0, edtavDdo_contagemresultado_servicosiglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WP_Grid.htm");
         }
         wbLoad = true;
      }

      protected void STARTKV2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Grid", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPKV0( ) ;
      }

      protected void WSKV2( )
      {
         STARTKV2( ) ;
         EVTKV2( ) ;
      }

      protected void EVTKV2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11KV2 */
                              E11KV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_DATAENTREGA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12KV2 */
                              E12KV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_DATADMN.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13KV2 */
                              E13KV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_HORAENTREGA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14KV2 */
                              E14KV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15KV2 */
                              E15KV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_DEMANDAFM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16KV2 */
                              E16KV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_SERVICOSIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17KV2 */
                              E17KV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_28_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_28_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_28_idx), 4, 0)), 4, "0");
                              SubsflControlProps_282( ) ;
                              AV59Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV59Update)) ? AV112Update_GXI : context.convertURL( context.PathToRelativeUrl( AV59Update))));
                              AV60Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV60Delete)) ? AV113Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV60Delete))));
                              A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagemResultado_DataEntrega_Internalname), 0));
                              n472ContagemResultado_DataEntrega = false;
                              A471ContagemResultado_DataDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagemResultado_DataDmn_Internalname), 0));
                              A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( edtContagemResultado_HoraEntrega_Internalname), 0));
                              n912ContagemResultado_HoraEntrega = false;
                              A500ContagemResultado_ContratadaPessoaNom = StringUtil.Upper( cgiGet( edtContagemResultado_ContratadaPessoaNom_Internalname));
                              n500ContagemResultado_ContratadaPessoaNom = false;
                              A493ContagemResultado_DemandaFM = cgiGet( edtContagemResultado_DemandaFM_Internalname);
                              n493ContagemResultado_DemandaFM = false;
                              A801ContagemResultado_ServicoSigla = StringUtil.Upper( cgiGet( edtContagemResultado_ServicoSigla_Internalname));
                              n801ContagemResultado_ServicoSigla = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E18KV2 */
                                    E18KV2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E19KV2 */
                                    E19KV2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E20KV2 */
                                    E20KV2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Tfcontagemresultado_dataentrega Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DATAENTREGA"), 0) != AV65TFContagemResultado_DataEntrega )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_dataentrega_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DATAENTREGA_TO"), 0) != AV66TFContagemResultado_DataEntrega_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_datadmn Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DATADMN"), 0) != AV71TFContagemResultado_DataDmn )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_datadmn_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DATADMN_TO"), 0) != AV72TFContagemResultado_DataDmn_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_horaentrega Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_HORAENTREGA"), 0) != AV77TFContagemResultado_HoraEntrega )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_horaentrega_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_HORAENTREGA_TO"), 0) != AV78TFContagemResultado_HoraEntrega_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_contratadapessoanom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM"), AV83TFContagemResultado_ContratadaPessoaNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_contratadapessoanom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL"), AV84TFContagemResultado_ContratadaPessoaNom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_demandafm Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM"), AV87TFContagemResultado_DemandaFM) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_demandafm_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM_SEL"), AV88TFContagemResultado_DemandaFM_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_servicosigla Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_SERVICOSIGLA"), AV91TFContagemResultado_ServicoSigla) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_servicosigla_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL"), AV92TFContagemResultado_ServicoSigla_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEKV2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAKV2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavTfcontagemresultado_dataentrega_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_282( ) ;
         while ( nGXsfl_28_idx <= nRC_GXsfl_28 )
         {
            sendrow_282( ) ;
            nGXsfl_28_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_28_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_28_idx+1));
            sGXsfl_28_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_28_idx), 4, 0)), 4, "0");
            SubsflControlProps_282( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       DateTime AV65TFContagemResultado_DataEntrega ,
                                       DateTime AV66TFContagemResultado_DataEntrega_To ,
                                       DateTime AV71TFContagemResultado_DataDmn ,
                                       DateTime AV72TFContagemResultado_DataDmn_To ,
                                       DateTime AV77TFContagemResultado_HoraEntrega ,
                                       DateTime AV78TFContagemResultado_HoraEntrega_To ,
                                       String AV83TFContagemResultado_ContratadaPessoaNom ,
                                       String AV84TFContagemResultado_ContratadaPessoaNom_Sel ,
                                       String AV87TFContagemResultado_DemandaFM ,
                                       String AV88TFContagemResultado_DemandaFM_Sel ,
                                       String AV91TFContagemResultado_ServicoSigla ,
                                       String AV92TFContagemResultado_ServicoSigla_Sel ,
                                       String AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace ,
                                       String AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace ,
                                       String AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace ,
                                       String AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace ,
                                       String AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace ,
                                       String AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace ,
                                       String AV114Pgmname ,
                                       int A456ContagemResultado_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFKV2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DATAENTREGA", GetSecureSignedToken( "", A472ContagemResultado_DataEntrega));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATAENTREGA", context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DATADMN", GetSecureSignedToken( "", A471ContagemResultado_DataDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATADMN", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_HORAENTREGA", GetSecureSignedToken( "", context.localUtil.Format( A912ContagemResultado_HoraEntrega, "99:99")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_HORAENTREGA", context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DEMANDAFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFKV2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV114Pgmname = "WP_Grid";
         context.Gx_err = 0;
      }

      protected void RFKV2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 28;
         /* Execute user event: E19KV2 */
         E19KV2 ();
         nGXsfl_28_idx = 1;
         sGXsfl_28_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_28_idx), 4, 0)), 4, "0");
         SubsflControlProps_282( ) ;
         nGXsfl_28_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_282( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV100WP_GridDS_1_Tfcontagemresultado_dataentrega ,
                                                 AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to ,
                                                 AV102WP_GridDS_3_Tfcontagemresultado_datadmn ,
                                                 AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to ,
                                                 AV104WP_GridDS_5_Tfcontagemresultado_horaentrega ,
                                                 AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to ,
                                                 AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel ,
                                                 AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom ,
                                                 AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel ,
                                                 AV108WP_GridDS_9_Tfcontagemresultado_demandafm ,
                                                 AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel ,
                                                 AV110WP_GridDS_11_Tfcontagemresultado_servicosigla ,
                                                 A472ContagemResultado_DataEntrega ,
                                                 A471ContagemResultado_DataDmn ,
                                                 A912ContagemResultado_HoraEntrega ,
                                                 A500ContagemResultado_ContratadaPessoaNom ,
                                                 A493ContagemResultado_DemandaFM ,
                                                 A801ContagemResultado_ServicoSigla },
                                                 new int[] {
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                 }
            });
            lV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = StringUtil.PadR( StringUtil.RTrim( AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom), 100, "%");
            lV108WP_GridDS_9_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV108WP_GridDS_9_Tfcontagemresultado_demandafm), "%", "");
            lV110WP_GridDS_11_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV110WP_GridDS_11_Tfcontagemresultado_servicosigla), 15, "%");
            /* Using cursor H00KV2 */
            pr_default.execute(0, new Object[] {AV100WP_GridDS_1_Tfcontagemresultado_dataentrega, AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to, AV102WP_GridDS_3_Tfcontagemresultado_datadmn, AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to, AV104WP_GridDS_5_Tfcontagemresultado_horaentrega, AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to, lV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom, AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel, lV108WP_GridDS_9_Tfcontagemresultado_demandafm, AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel, lV110WP_GridDS_11_Tfcontagemresultado_servicosigla, AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_28_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A490ContagemResultado_ContratadaCod = H00KV2_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00KV2_n490ContagemResultado_ContratadaCod[0];
               A499ContagemResultado_ContratadaPessoaCod = H00KV2_A499ContagemResultado_ContratadaPessoaCod[0];
               n499ContagemResultado_ContratadaPessoaCod = H00KV2_n499ContagemResultado_ContratadaPessoaCod[0];
               A1553ContagemResultado_CntSrvCod = H00KV2_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00KV2_n1553ContagemResultado_CntSrvCod[0];
               A601ContagemResultado_Servico = H00KV2_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00KV2_n601ContagemResultado_Servico[0];
               A456ContagemResultado_Codigo = H00KV2_A456ContagemResultado_Codigo[0];
               A801ContagemResultado_ServicoSigla = H00KV2_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = H00KV2_n801ContagemResultado_ServicoSigla[0];
               A493ContagemResultado_DemandaFM = H00KV2_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00KV2_n493ContagemResultado_DemandaFM[0];
               A500ContagemResultado_ContratadaPessoaNom = H00KV2_A500ContagemResultado_ContratadaPessoaNom[0];
               n500ContagemResultado_ContratadaPessoaNom = H00KV2_n500ContagemResultado_ContratadaPessoaNom[0];
               A912ContagemResultado_HoraEntrega = H00KV2_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = H00KV2_n912ContagemResultado_HoraEntrega[0];
               A471ContagemResultado_DataDmn = H00KV2_A471ContagemResultado_DataDmn[0];
               A472ContagemResultado_DataEntrega = H00KV2_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = H00KV2_n472ContagemResultado_DataEntrega[0];
               A499ContagemResultado_ContratadaPessoaCod = H00KV2_A499ContagemResultado_ContratadaPessoaCod[0];
               n499ContagemResultado_ContratadaPessoaCod = H00KV2_n499ContagemResultado_ContratadaPessoaCod[0];
               A500ContagemResultado_ContratadaPessoaNom = H00KV2_A500ContagemResultado_ContratadaPessoaNom[0];
               n500ContagemResultado_ContratadaPessoaNom = H00KV2_n500ContagemResultado_ContratadaPessoaNom[0];
               A601ContagemResultado_Servico = H00KV2_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00KV2_n601ContagemResultado_Servico[0];
               A801ContagemResultado_ServicoSigla = H00KV2_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = H00KV2_n801ContagemResultado_ServicoSigla[0];
               /* Execute user event: E20KV2 */
               E20KV2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 28;
            WBKV0( ) ;
         }
         nGXsfl_28_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV100WP_GridDS_1_Tfcontagemresultado_dataentrega = AV65TFContagemResultado_DataEntrega;
         AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to = AV66TFContagemResultado_DataEntrega_To;
         AV102WP_GridDS_3_Tfcontagemresultado_datadmn = AV71TFContagemResultado_DataDmn;
         AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to = AV72TFContagemResultado_DataDmn_To;
         AV104WP_GridDS_5_Tfcontagemresultado_horaentrega = AV77TFContagemResultado_HoraEntrega;
         AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to = AV78TFContagemResultado_HoraEntrega_To;
         AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = AV83TFContagemResultado_ContratadaPessoaNom;
         AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel = AV84TFContagemResultado_ContratadaPessoaNom_Sel;
         AV108WP_GridDS_9_Tfcontagemresultado_demandafm = AV87TFContagemResultado_DemandaFM;
         AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel = AV88TFContagemResultado_DemandaFM_Sel;
         AV110WP_GridDS_11_Tfcontagemresultado_servicosigla = AV91TFContagemResultado_ServicoSigla;
         AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel = AV92TFContagemResultado_ServicoSigla_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV100WP_GridDS_1_Tfcontagemresultado_dataentrega ,
                                              AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to ,
                                              AV102WP_GridDS_3_Tfcontagemresultado_datadmn ,
                                              AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to ,
                                              AV104WP_GridDS_5_Tfcontagemresultado_horaentrega ,
                                              AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to ,
                                              AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel ,
                                              AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom ,
                                              AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel ,
                                              AV108WP_GridDS_9_Tfcontagemresultado_demandafm ,
                                              AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel ,
                                              AV110WP_GridDS_11_Tfcontagemresultado_servicosigla ,
                                              A472ContagemResultado_DataEntrega ,
                                              A471ContagemResultado_DataDmn ,
                                              A912ContagemResultado_HoraEntrega ,
                                              A500ContagemResultado_ContratadaPessoaNom ,
                                              A493ContagemResultado_DemandaFM ,
                                              A801ContagemResultado_ServicoSigla },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = StringUtil.PadR( StringUtil.RTrim( AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom), 100, "%");
         lV108WP_GridDS_9_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV108WP_GridDS_9_Tfcontagemresultado_demandafm), "%", "");
         lV110WP_GridDS_11_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV110WP_GridDS_11_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor H00KV3 */
         pr_default.execute(1, new Object[] {AV100WP_GridDS_1_Tfcontagemresultado_dataentrega, AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to, AV102WP_GridDS_3_Tfcontagemresultado_datadmn, AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to, AV104WP_GridDS_5_Tfcontagemresultado_horaentrega, AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to, lV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom, AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel, lV108WP_GridDS_9_Tfcontagemresultado_demandafm, AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel, lV110WP_GridDS_11_Tfcontagemresultado_servicosigla, AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel});
         GRID_nRecordCount = H00KV3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV100WP_GridDS_1_Tfcontagemresultado_dataentrega = AV65TFContagemResultado_DataEntrega;
         AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to = AV66TFContagemResultado_DataEntrega_To;
         AV102WP_GridDS_3_Tfcontagemresultado_datadmn = AV71TFContagemResultado_DataDmn;
         AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to = AV72TFContagemResultado_DataDmn_To;
         AV104WP_GridDS_5_Tfcontagemresultado_horaentrega = AV77TFContagemResultado_HoraEntrega;
         AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to = AV78TFContagemResultado_HoraEntrega_To;
         AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = AV83TFContagemResultado_ContratadaPessoaNom;
         AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel = AV84TFContagemResultado_ContratadaPessoaNom_Sel;
         AV108WP_GridDS_9_Tfcontagemresultado_demandafm = AV87TFContagemResultado_DemandaFM;
         AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel = AV88TFContagemResultado_DemandaFM_Sel;
         AV110WP_GridDS_11_Tfcontagemresultado_servicosigla = AV91TFContagemResultado_ServicoSigla;
         AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel = AV92TFContagemResultado_ServicoSigla_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV65TFContagemResultado_DataEntrega, AV66TFContagemResultado_DataEntrega_To, AV71TFContagemResultado_DataDmn, AV72TFContagemResultado_DataDmn_To, AV77TFContagemResultado_HoraEntrega, AV78TFContagemResultado_HoraEntrega_To, AV83TFContagemResultado_ContratadaPessoaNom, AV84TFContagemResultado_ContratadaPessoaNom_Sel, AV87TFContagemResultado_DemandaFM, AV88TFContagemResultado_DemandaFM_Sel, AV91TFContagemResultado_ServicoSigla, AV92TFContagemResultado_ServicoSigla_Sel, AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace, AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace, AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace, AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace, AV114Pgmname, A456ContagemResultado_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV100WP_GridDS_1_Tfcontagemresultado_dataentrega = AV65TFContagemResultado_DataEntrega;
         AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to = AV66TFContagemResultado_DataEntrega_To;
         AV102WP_GridDS_3_Tfcontagemresultado_datadmn = AV71TFContagemResultado_DataDmn;
         AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to = AV72TFContagemResultado_DataDmn_To;
         AV104WP_GridDS_5_Tfcontagemresultado_horaentrega = AV77TFContagemResultado_HoraEntrega;
         AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to = AV78TFContagemResultado_HoraEntrega_To;
         AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = AV83TFContagemResultado_ContratadaPessoaNom;
         AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel = AV84TFContagemResultado_ContratadaPessoaNom_Sel;
         AV108WP_GridDS_9_Tfcontagemresultado_demandafm = AV87TFContagemResultado_DemandaFM;
         AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel = AV88TFContagemResultado_DemandaFM_Sel;
         AV110WP_GridDS_11_Tfcontagemresultado_servicosigla = AV91TFContagemResultado_ServicoSigla;
         AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel = AV92TFContagemResultado_ServicoSigla_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV65TFContagemResultado_DataEntrega, AV66TFContagemResultado_DataEntrega_To, AV71TFContagemResultado_DataDmn, AV72TFContagemResultado_DataDmn_To, AV77TFContagemResultado_HoraEntrega, AV78TFContagemResultado_HoraEntrega_To, AV83TFContagemResultado_ContratadaPessoaNom, AV84TFContagemResultado_ContratadaPessoaNom_Sel, AV87TFContagemResultado_DemandaFM, AV88TFContagemResultado_DemandaFM_Sel, AV91TFContagemResultado_ServicoSigla, AV92TFContagemResultado_ServicoSigla_Sel, AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace, AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace, AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace, AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace, AV114Pgmname, A456ContagemResultado_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV100WP_GridDS_1_Tfcontagemresultado_dataentrega = AV65TFContagemResultado_DataEntrega;
         AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to = AV66TFContagemResultado_DataEntrega_To;
         AV102WP_GridDS_3_Tfcontagemresultado_datadmn = AV71TFContagemResultado_DataDmn;
         AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to = AV72TFContagemResultado_DataDmn_To;
         AV104WP_GridDS_5_Tfcontagemresultado_horaentrega = AV77TFContagemResultado_HoraEntrega;
         AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to = AV78TFContagemResultado_HoraEntrega_To;
         AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = AV83TFContagemResultado_ContratadaPessoaNom;
         AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel = AV84TFContagemResultado_ContratadaPessoaNom_Sel;
         AV108WP_GridDS_9_Tfcontagemresultado_demandafm = AV87TFContagemResultado_DemandaFM;
         AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel = AV88TFContagemResultado_DemandaFM_Sel;
         AV110WP_GridDS_11_Tfcontagemresultado_servicosigla = AV91TFContagemResultado_ServicoSigla;
         AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel = AV92TFContagemResultado_ServicoSigla_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV65TFContagemResultado_DataEntrega, AV66TFContagemResultado_DataEntrega_To, AV71TFContagemResultado_DataDmn, AV72TFContagemResultado_DataDmn_To, AV77TFContagemResultado_HoraEntrega, AV78TFContagemResultado_HoraEntrega_To, AV83TFContagemResultado_ContratadaPessoaNom, AV84TFContagemResultado_ContratadaPessoaNom_Sel, AV87TFContagemResultado_DemandaFM, AV88TFContagemResultado_DemandaFM_Sel, AV91TFContagemResultado_ServicoSigla, AV92TFContagemResultado_ServicoSigla_Sel, AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace, AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace, AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace, AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace, AV114Pgmname, A456ContagemResultado_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV100WP_GridDS_1_Tfcontagemresultado_dataentrega = AV65TFContagemResultado_DataEntrega;
         AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to = AV66TFContagemResultado_DataEntrega_To;
         AV102WP_GridDS_3_Tfcontagemresultado_datadmn = AV71TFContagemResultado_DataDmn;
         AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to = AV72TFContagemResultado_DataDmn_To;
         AV104WP_GridDS_5_Tfcontagemresultado_horaentrega = AV77TFContagemResultado_HoraEntrega;
         AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to = AV78TFContagemResultado_HoraEntrega_To;
         AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = AV83TFContagemResultado_ContratadaPessoaNom;
         AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel = AV84TFContagemResultado_ContratadaPessoaNom_Sel;
         AV108WP_GridDS_9_Tfcontagemresultado_demandafm = AV87TFContagemResultado_DemandaFM;
         AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel = AV88TFContagemResultado_DemandaFM_Sel;
         AV110WP_GridDS_11_Tfcontagemresultado_servicosigla = AV91TFContagemResultado_ServicoSigla;
         AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel = AV92TFContagemResultado_ServicoSigla_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV65TFContagemResultado_DataEntrega, AV66TFContagemResultado_DataEntrega_To, AV71TFContagemResultado_DataDmn, AV72TFContagemResultado_DataDmn_To, AV77TFContagemResultado_HoraEntrega, AV78TFContagemResultado_HoraEntrega_To, AV83TFContagemResultado_ContratadaPessoaNom, AV84TFContagemResultado_ContratadaPessoaNom_Sel, AV87TFContagemResultado_DemandaFM, AV88TFContagemResultado_DemandaFM_Sel, AV91TFContagemResultado_ServicoSigla, AV92TFContagemResultado_ServicoSigla_Sel, AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace, AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace, AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace, AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace, AV114Pgmname, A456ContagemResultado_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV100WP_GridDS_1_Tfcontagemresultado_dataentrega = AV65TFContagemResultado_DataEntrega;
         AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to = AV66TFContagemResultado_DataEntrega_To;
         AV102WP_GridDS_3_Tfcontagemresultado_datadmn = AV71TFContagemResultado_DataDmn;
         AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to = AV72TFContagemResultado_DataDmn_To;
         AV104WP_GridDS_5_Tfcontagemresultado_horaentrega = AV77TFContagemResultado_HoraEntrega;
         AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to = AV78TFContagemResultado_HoraEntrega_To;
         AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = AV83TFContagemResultado_ContratadaPessoaNom;
         AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel = AV84TFContagemResultado_ContratadaPessoaNom_Sel;
         AV108WP_GridDS_9_Tfcontagemresultado_demandafm = AV87TFContagemResultado_DemandaFM;
         AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel = AV88TFContagemResultado_DemandaFM_Sel;
         AV110WP_GridDS_11_Tfcontagemresultado_servicosigla = AV91TFContagemResultado_ServicoSigla;
         AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel = AV92TFContagemResultado_ServicoSigla_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV65TFContagemResultado_DataEntrega, AV66TFContagemResultado_DataEntrega_To, AV71TFContagemResultado_DataDmn, AV72TFContagemResultado_DataDmn_To, AV77TFContagemResultado_HoraEntrega, AV78TFContagemResultado_HoraEntrega_To, AV83TFContagemResultado_ContratadaPessoaNom, AV84TFContagemResultado_ContratadaPessoaNom_Sel, AV87TFContagemResultado_DemandaFM, AV88TFContagemResultado_DemandaFM_Sel, AV91TFContagemResultado_ServicoSigla, AV92TFContagemResultado_ServicoSigla_Sel, AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace, AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace, AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace, AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace, AV114Pgmname, A456ContagemResultado_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPKV0( )
      {
         /* Before Start, stand alone formulas. */
         AV114Pgmname = "WP_Grid";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E18KV2 */
         E18KV2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV94DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_DATAENTREGATITLEFILTERDATA"), AV64ContagemResultado_DataEntregaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_DATADMNTITLEFILTERDATA"), AV70ContagemResultado_DataDmnTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_HORAENTREGATITLEFILTERDATA"), AV76ContagemResultado_HoraEntregaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLEFILTERDATA"), AV82ContagemResultado_ContratadaPessoaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA"), AV86ContagemResultado_DemandaFMTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_SERVICOSIGLATITLEFILTERDATA"), AV90ContagemResultado_ServicoSiglaTitleFilterData);
            /* Read variables values. */
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultado_dataentrega_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContagem Resultado_Data Entrega"}), 1, "vTFCONTAGEMRESULTADO_DATAENTREGA");
               GX_FocusControl = edtavTfcontagemresultado_dataentrega_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65TFContagemResultado_DataEntrega = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContagemResultado_DataEntrega", context.localUtil.Format(AV65TFContagemResultado_DataEntrega, "99/99/99"));
            }
            else
            {
               AV65TFContagemResultado_DataEntrega = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontagemresultado_dataentrega_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContagemResultado_DataEntrega", context.localUtil.Format(AV65TFContagemResultado_DataEntrega, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultado_dataentrega_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContagem Resultado_Data Entrega_To"}), 1, "vTFCONTAGEMRESULTADO_DATAENTREGA_TO");
               GX_FocusControl = edtavTfcontagemresultado_dataentrega_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66TFContagemResultado_DataEntrega_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemResultado_DataEntrega_To", context.localUtil.Format(AV66TFContagemResultado_DataEntrega_To, "99/99/99"));
            }
            else
            {
               AV66TFContagemResultado_DataEntrega_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontagemresultado_dataentrega_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemResultado_DataEntrega_To", context.localUtil.Format(AV66TFContagemResultado_DataEntrega_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultado_dataentregaauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado_Data Entrega Aux Date"}), 1, "vDDO_CONTAGEMRESULTADO_DATAENTREGAAUXDATE");
               GX_FocusControl = edtavDdo_contagemresultado_dataentregaauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV67DDO_ContagemResultado_DataEntregaAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67DDO_ContagemResultado_DataEntregaAuxDate", context.localUtil.Format(AV67DDO_ContagemResultado_DataEntregaAuxDate, "99/99/99"));
            }
            else
            {
               AV67DDO_ContagemResultado_DataEntregaAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultado_dataentregaauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67DDO_ContagemResultado_DataEntregaAuxDate", context.localUtil.Format(AV67DDO_ContagemResultado_DataEntregaAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultado_dataentregaauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado_Data Entrega Aux Date To"}), 1, "vDDO_CONTAGEMRESULTADO_DATAENTREGAAUXDATETO");
               GX_FocusControl = edtavDdo_contagemresultado_dataentregaauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV68DDO_ContagemResultado_DataEntregaAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68DDO_ContagemResultado_DataEntregaAuxDateTo", context.localUtil.Format(AV68DDO_ContagemResultado_DataEntregaAuxDateTo, "99/99/99"));
            }
            else
            {
               AV68DDO_ContagemResultado_DataEntregaAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultado_dataentregaauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68DDO_ContagemResultado_DataEntregaAuxDateTo", context.localUtil.Format(AV68DDO_ContagemResultado_DataEntregaAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultado_datadmn_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContagem Resultado_Data Dmn"}), 1, "vTFCONTAGEMRESULTADO_DATADMN");
               GX_FocusControl = edtavTfcontagemresultado_datadmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV71TFContagemResultado_DataDmn = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemResultado_DataDmn", context.localUtil.Format(AV71TFContagemResultado_DataDmn, "99/99/99"));
            }
            else
            {
               AV71TFContagemResultado_DataDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontagemresultado_datadmn_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemResultado_DataDmn", context.localUtil.Format(AV71TFContagemResultado_DataDmn, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultado_datadmn_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContagem Resultado_Data Dmn_To"}), 1, "vTFCONTAGEMRESULTADO_DATADMN_TO");
               GX_FocusControl = edtavTfcontagemresultado_datadmn_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV72TFContagemResultado_DataDmn_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFContagemResultado_DataDmn_To", context.localUtil.Format(AV72TFContagemResultado_DataDmn_To, "99/99/99"));
            }
            else
            {
               AV72TFContagemResultado_DataDmn_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontagemresultado_datadmn_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFContagemResultado_DataDmn_To", context.localUtil.Format(AV72TFContagemResultado_DataDmn_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultado_datadmnauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado_Data Dmn Aux Date"}), 1, "vDDO_CONTAGEMRESULTADO_DATADMNAUXDATE");
               GX_FocusControl = edtavDdo_contagemresultado_datadmnauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV73DDO_ContagemResultado_DataDmnAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73DDO_ContagemResultado_DataDmnAuxDate", context.localUtil.Format(AV73DDO_ContagemResultado_DataDmnAuxDate, "99/99/99"));
            }
            else
            {
               AV73DDO_ContagemResultado_DataDmnAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultado_datadmnauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73DDO_ContagemResultado_DataDmnAuxDate", context.localUtil.Format(AV73DDO_ContagemResultado_DataDmnAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultado_datadmnauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado_Data Dmn Aux Date To"}), 1, "vDDO_CONTAGEMRESULTADO_DATADMNAUXDATETO");
               GX_FocusControl = edtavDdo_contagemresultado_datadmnauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV74DDO_ContagemResultado_DataDmnAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74DDO_ContagemResultado_DataDmnAuxDateTo", context.localUtil.Format(AV74DDO_ContagemResultado_DataDmnAuxDateTo, "99/99/99"));
            }
            else
            {
               AV74DDO_ContagemResultado_DataDmnAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultado_datadmnauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74DDO_ContagemResultado_DataDmnAuxDateTo", context.localUtil.Format(AV74DDO_ContagemResultado_DataDmnAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultado_horaentrega_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badtime", new   object[]  {"TFContagem Resultado_Hora Entrega"}), 1, "vTFCONTAGEMRESULTADO_HORAENTREGA");
               GX_FocusControl = edtavTfcontagemresultado_horaentrega_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV77TFContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContagemResultado_HoraEntrega", context.localUtil.TToC( AV77TFContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV77TFContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( edtavTfcontagemresultado_horaentrega_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContagemResultado_HoraEntrega", context.localUtil.TToC( AV77TFContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultado_horaentrega_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badtime", new   object[]  {"TFContagem Resultado_Hora Entrega_To"}), 1, "vTFCONTAGEMRESULTADO_HORAENTREGA_TO");
               GX_FocusControl = edtavTfcontagemresultado_horaentrega_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV78TFContagemResultado_HoraEntrega_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContagemResultado_HoraEntrega_To", context.localUtil.TToC( AV78TFContagemResultado_HoraEntrega_To, 0, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV78TFContagemResultado_HoraEntrega_To = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( edtavTfcontagemresultado_horaentrega_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContagemResultado_HoraEntrega_To", context.localUtil.TToC( AV78TFContagemResultado_HoraEntrega_To, 0, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultado_horaentregaauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado_Hora Entrega Aux Date"}), 1, "vDDO_CONTAGEMRESULTADO_HORAENTREGAAUXDATE");
               GX_FocusControl = edtavDdo_contagemresultado_horaentregaauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV79DDO_ContagemResultado_HoraEntregaAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79DDO_ContagemResultado_HoraEntregaAuxDate", context.localUtil.Format(AV79DDO_ContagemResultado_HoraEntregaAuxDate, "99/99/99"));
            }
            else
            {
               AV79DDO_ContagemResultado_HoraEntregaAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultado_horaentregaauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79DDO_ContagemResultado_HoraEntregaAuxDate", context.localUtil.Format(AV79DDO_ContagemResultado_HoraEntregaAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultado_horaentregaauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado_Hora Entrega Aux Date To"}), 1, "vDDO_CONTAGEMRESULTADO_HORAENTREGAAUXDATETO");
               GX_FocusControl = edtavDdo_contagemresultado_horaentregaauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV80DDO_ContagemResultado_HoraEntregaAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80DDO_ContagemResultado_HoraEntregaAuxDateTo", context.localUtil.Format(AV80DDO_ContagemResultado_HoraEntregaAuxDateTo, "99/99/99"));
            }
            else
            {
               AV80DDO_ContagemResultado_HoraEntregaAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultado_horaentregaauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80DDO_ContagemResultado_HoraEntregaAuxDateTo", context.localUtil.Format(AV80DDO_ContagemResultado_HoraEntregaAuxDateTo, "99/99/99"));
            }
            AV83TFContagemResultado_ContratadaPessoaNom = StringUtil.Upper( cgiGet( edtavTfcontagemresultado_contratadapessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFContagemResultado_ContratadaPessoaNom", AV83TFContagemResultado_ContratadaPessoaNom);
            AV84TFContagemResultado_ContratadaPessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontagemresultado_contratadapessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContagemResultado_ContratadaPessoaNom_Sel", AV84TFContagemResultado_ContratadaPessoaNom_Sel);
            AV87TFContagemResultado_DemandaFM = cgiGet( edtavTfcontagemresultado_demandafm_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFContagemResultado_DemandaFM", AV87TFContagemResultado_DemandaFM);
            AV88TFContagemResultado_DemandaFM_Sel = cgiGet( edtavTfcontagemresultado_demandafm_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFContagemResultado_DemandaFM_Sel", AV88TFContagemResultado_DemandaFM_Sel);
            AV91TFContagemResultado_ServicoSigla = StringUtil.Upper( cgiGet( edtavTfcontagemresultado_servicosigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFContagemResultado_ServicoSigla", AV91TFContagemResultado_ServicoSigla);
            AV92TFContagemResultado_ServicoSigla_Sel = StringUtil.Upper( cgiGet( edtavTfcontagemresultado_servicosigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFContagemResultado_ServicoSigla_Sel", AV92TFContagemResultado_ServicoSigla_Sel);
            AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_dataentregatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace", AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace);
            AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace", AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace);
            AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_horaentregatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace", AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace);
            AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace", AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace);
            AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace", AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace);
            AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_servicosiglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace", AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_28 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_28"), ",", "."));
            AV96GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV97GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contagemresultado_dataentrega_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Caption");
            Ddo_contagemresultado_dataentrega_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Tooltip");
            Ddo_contagemresultado_dataentrega_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Cls");
            Ddo_contagemresultado_dataentrega_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Filteredtext_set");
            Ddo_contagemresultado_dataentrega_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Filteredtextto_set");
            Ddo_contagemresultado_dataentrega_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Dropdownoptionstype");
            Ddo_contagemresultado_dataentrega_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Titlecontrolidtoreplace");
            Ddo_contagemresultado_dataentrega_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Includesortasc"));
            Ddo_contagemresultado_dataentrega_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Includesortdsc"));
            Ddo_contagemresultado_dataentrega_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Includefilter"));
            Ddo_contagemresultado_dataentrega_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Filtertype");
            Ddo_contagemresultado_dataentrega_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Filterisrange"));
            Ddo_contagemresultado_dataentrega_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Includedatalist"));
            Ddo_contagemresultado_dataentrega_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Cleanfilter");
            Ddo_contagemresultado_dataentrega_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Rangefilterfrom");
            Ddo_contagemresultado_dataentrega_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Rangefilterto");
            Ddo_contagemresultado_dataentrega_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Searchbuttontext");
            Ddo_contagemresultado_datadmn_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Caption");
            Ddo_contagemresultado_datadmn_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Tooltip");
            Ddo_contagemresultado_datadmn_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Cls");
            Ddo_contagemresultado_datadmn_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Filteredtext_set");
            Ddo_contagemresultado_datadmn_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Filteredtextto_set");
            Ddo_contagemresultado_datadmn_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Dropdownoptionstype");
            Ddo_contagemresultado_datadmn_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Titlecontrolidtoreplace");
            Ddo_contagemresultado_datadmn_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Includesortasc"));
            Ddo_contagemresultado_datadmn_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Includesortdsc"));
            Ddo_contagemresultado_datadmn_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Includefilter"));
            Ddo_contagemresultado_datadmn_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Filtertype");
            Ddo_contagemresultado_datadmn_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Filterisrange"));
            Ddo_contagemresultado_datadmn_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Includedatalist"));
            Ddo_contagemresultado_datadmn_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Cleanfilter");
            Ddo_contagemresultado_datadmn_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Rangefilterfrom");
            Ddo_contagemresultado_datadmn_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Rangefilterto");
            Ddo_contagemresultado_datadmn_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Searchbuttontext");
            Ddo_contagemresultado_horaentrega_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Caption");
            Ddo_contagemresultado_horaentrega_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Tooltip");
            Ddo_contagemresultado_horaentrega_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Cls");
            Ddo_contagemresultado_horaentrega_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Filteredtext_set");
            Ddo_contagemresultado_horaentrega_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Filteredtextto_set");
            Ddo_contagemresultado_horaentrega_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Dropdownoptionstype");
            Ddo_contagemresultado_horaentrega_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Titlecontrolidtoreplace");
            Ddo_contagemresultado_horaentrega_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Includesortasc"));
            Ddo_contagemresultado_horaentrega_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Includesortdsc"));
            Ddo_contagemresultado_horaentrega_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Includefilter"));
            Ddo_contagemresultado_horaentrega_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Filtertype");
            Ddo_contagemresultado_horaentrega_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Filterisrange"));
            Ddo_contagemresultado_horaentrega_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Includedatalist"));
            Ddo_contagemresultado_horaentrega_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Cleanfilter");
            Ddo_contagemresultado_horaentrega_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Rangefilterfrom");
            Ddo_contagemresultado_horaentrega_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Rangefilterto");
            Ddo_contagemresultado_horaentrega_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Searchbuttontext");
            Ddo_contagemresultado_contratadapessoanom_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Caption");
            Ddo_contagemresultado_contratadapessoanom_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Tooltip");
            Ddo_contagemresultado_contratadapessoanom_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Cls");
            Ddo_contagemresultado_contratadapessoanom_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Filteredtext_set");
            Ddo_contagemresultado_contratadapessoanom_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Selectedvalue_set");
            Ddo_contagemresultado_contratadapessoanom_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Dropdownoptionstype");
            Ddo_contagemresultado_contratadapessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Titlecontrolidtoreplace");
            Ddo_contagemresultado_contratadapessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Includesortasc"));
            Ddo_contagemresultado_contratadapessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Includesortdsc"));
            Ddo_contagemresultado_contratadapessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Includefilter"));
            Ddo_contagemresultado_contratadapessoanom_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Filtertype");
            Ddo_contagemresultado_contratadapessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Filterisrange"));
            Ddo_contagemresultado_contratadapessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Includedatalist"));
            Ddo_contagemresultado_contratadapessoanom_Datalisttype = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Datalisttype");
            Ddo_contagemresultado_contratadapessoanom_Datalistproc = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Datalistproc");
            Ddo_contagemresultado_contratadapessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultado_contratadapessoanom_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Loadingdata");
            Ddo_contagemresultado_contratadapessoanom_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Cleanfilter");
            Ddo_contagemresultado_contratadapessoanom_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Noresultsfound");
            Ddo_contagemresultado_contratadapessoanom_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Searchbuttontext");
            Ddo_contagemresultado_demandafm_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Caption");
            Ddo_contagemresultado_demandafm_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Tooltip");
            Ddo_contagemresultado_demandafm_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Cls");
            Ddo_contagemresultado_demandafm_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filteredtext_set");
            Ddo_contagemresultado_demandafm_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Selectedvalue_set");
            Ddo_contagemresultado_demandafm_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Dropdownoptionstype");
            Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Titlecontrolidtoreplace");
            Ddo_contagemresultado_demandafm_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includesortasc"));
            Ddo_contagemresultado_demandafm_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includesortdsc"));
            Ddo_contagemresultado_demandafm_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includefilter"));
            Ddo_contagemresultado_demandafm_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filtertype");
            Ddo_contagemresultado_demandafm_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filterisrange"));
            Ddo_contagemresultado_demandafm_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Includedatalist"));
            Ddo_contagemresultado_demandafm_Datalisttype = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalisttype");
            Ddo_contagemresultado_demandafm_Datalistproc = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalistproc");
            Ddo_contagemresultado_demandafm_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultado_demandafm_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Loadingdata");
            Ddo_contagemresultado_demandafm_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Cleanfilter");
            Ddo_contagemresultado_demandafm_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Noresultsfound");
            Ddo_contagemresultado_demandafm_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Searchbuttontext");
            Ddo_contagemresultado_servicosigla_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Caption");
            Ddo_contagemresultado_servicosigla_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Tooltip");
            Ddo_contagemresultado_servicosigla_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Cls");
            Ddo_contagemresultado_servicosigla_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Filteredtext_set");
            Ddo_contagemresultado_servicosigla_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Selectedvalue_set");
            Ddo_contagemresultado_servicosigla_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Dropdownoptionstype");
            Ddo_contagemresultado_servicosigla_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Titlecontrolidtoreplace");
            Ddo_contagemresultado_servicosigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Includesortasc"));
            Ddo_contagemresultado_servicosigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Includesortdsc"));
            Ddo_contagemresultado_servicosigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Includefilter"));
            Ddo_contagemresultado_servicosigla_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Filtertype");
            Ddo_contagemresultado_servicosigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Filterisrange"));
            Ddo_contagemresultado_servicosigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Includedatalist"));
            Ddo_contagemresultado_servicosigla_Datalisttype = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Datalisttype");
            Ddo_contagemresultado_servicosigla_Datalistproc = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Datalistproc");
            Ddo_contagemresultado_servicosigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultado_servicosigla_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Loadingdata");
            Ddo_contagemresultado_servicosigla_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Cleanfilter");
            Ddo_contagemresultado_servicosigla_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Noresultsfound");
            Ddo_contagemresultado_servicosigla_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Searchbuttontext");
            subGrid_Grouping = cgiGet( "GRID_Grouping");
            subGrid_Class = cgiGet( "GRID_Class");
            subGrid_Addtoparentgxuicontrol = cgiGet( "GRID_Addtoparentgxuicontrol");
            subGrid_Groupfield = cgiGet( "GRID_Groupfield");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contagemresultado_dataentrega_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Activeeventkey");
            Ddo_contagemresultado_dataentrega_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Filteredtext_get");
            Ddo_contagemresultado_dataentrega_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADO_DATAENTREGA_Filteredtextto_get");
            Ddo_contagemresultado_datadmn_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Activeeventkey");
            Ddo_contagemresultado_datadmn_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Filteredtext_get");
            Ddo_contagemresultado_datadmn_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADO_DATADMN_Filteredtextto_get");
            Ddo_contagemresultado_horaentrega_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Activeeventkey");
            Ddo_contagemresultado_horaentrega_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Filteredtext_get");
            Ddo_contagemresultado_horaentrega_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADO_HORAENTREGA_Filteredtextto_get");
            Ddo_contagemresultado_contratadapessoanom_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Activeeventkey");
            Ddo_contagemresultado_contratadapessoanom_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Filteredtext_get");
            Ddo_contagemresultado_contratadapessoanom_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM_Selectedvalue_get");
            Ddo_contagemresultado_demandafm_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Activeeventkey");
            Ddo_contagemresultado_demandafm_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Filteredtext_get");
            Ddo_contagemresultado_demandafm_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRESULTADO_DEMANDAFM_Selectedvalue_get");
            Ddo_contagemresultado_servicosigla_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Activeeventkey");
            Ddo_contagemresultado_servicosigla_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Filteredtext_get");
            Ddo_contagemresultado_servicosigla_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRESULTADO_SERVICOSIGLA_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DATAENTREGA"), 0) != AV65TFContagemResultado_DataEntrega )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DATAENTREGA_TO"), 0) != AV66TFContagemResultado_DataEntrega_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DATADMN"), 0) != AV71TFContagemResultado_DataDmn )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_DATADMN_TO"), 0) != AV72TFContagemResultado_DataDmn_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_HORAENTREGA"), 0) != AV77TFContagemResultado_HoraEntrega )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADO_HORAENTREGA_TO"), 0) != AV78TFContagemResultado_HoraEntrega_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM"), AV83TFContagemResultado_ContratadaPessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL"), AV84TFContagemResultado_ContratadaPessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM"), AV87TFContagemResultado_DemandaFM) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_DEMANDAFM_SEL"), AV88TFContagemResultado_DemandaFM_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_SERVICOSIGLA"), AV91TFContagemResultado_ServicoSigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL"), AV92TFContagemResultado_ServicoSigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E18KV2 */
         E18KV2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18KV2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontagemresultado_dataentrega_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_dataentrega_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_dataentrega_Visible), 5, 0)));
         edtavTfcontagemresultado_dataentrega_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_dataentrega_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_dataentrega_to_Visible), 5, 0)));
         edtavTfcontagemresultado_datadmn_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_datadmn_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_datadmn_Visible), 5, 0)));
         edtavTfcontagemresultado_datadmn_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_datadmn_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_datadmn_to_Visible), 5, 0)));
         edtavTfcontagemresultado_horaentrega_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_horaentrega_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_horaentrega_Visible), 5, 0)));
         edtavTfcontagemresultado_horaentrega_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_horaentrega_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_horaentrega_to_Visible), 5, 0)));
         edtavTfcontagemresultado_contratadapessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_contratadapessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_contratadapessoanom_Visible), 5, 0)));
         edtavTfcontagemresultado_contratadapessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_contratadapessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_contratadapessoanom_sel_Visible), 5, 0)));
         edtavTfcontagemresultado_demandafm_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_demandafm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_demandafm_Visible), 5, 0)));
         edtavTfcontagemresultado_demandafm_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_demandafm_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_demandafm_sel_Visible), 5, 0)));
         edtavTfcontagemresultado_servicosigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_servicosigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_servicosigla_Visible), 5, 0)));
         edtavTfcontagemresultado_servicosigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_servicosigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_servicosigla_sel_Visible), 5, 0)));
         Ddo_contagemresultado_dataentrega_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_DataEntrega";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_dataentrega_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_dataentrega_Titlecontrolidtoreplace);
         AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace = Ddo_contagemresultado_dataentrega_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace", AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace);
         edtavDdo_contagemresultado_dataentregatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_dataentregatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_dataentregatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_datadmn_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_DataDmn";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datadmn_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_datadmn_Titlecontrolidtoreplace);
         AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace = Ddo_contagemresultado_datadmn_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace", AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace);
         edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_horaentrega_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_HoraEntrega";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_horaentrega_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_horaentrega_Titlecontrolidtoreplace);
         AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace = Ddo_contagemresultado_horaentrega_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace", AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace);
         edtavDdo_contagemresultado_horaentregatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_horaentregatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_horaentregatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_contratadapessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_ContratadaPessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contratadapessoanom_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_contratadapessoanom_Titlecontrolidtoreplace);
         AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace = Ddo_contagemresultado_contratadapessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace", AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace);
         edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_DemandaFM";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace);
         AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace", AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace);
         edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_servicosigla_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_ServicoSigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_servicosigla_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_servicosigla_Titlecontrolidtoreplace);
         AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace = Ddo_contagemresultado_servicosigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace", AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace);
         edtavDdo_contagemresultado_servicosiglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_servicosiglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_servicosiglatitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Resultado das Contagens";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV94DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV94DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
      }

      protected void E19KV2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV64ContagemResultado_DataEntregaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70ContagemResultado_DataDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76ContagemResultado_HoraEntregaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV82ContagemResultado_ContratadaPessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV86ContagemResultado_DemandaFMTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV90ContagemResultado_ServicoSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContagemResultado_DataEntrega_Titleformat = 2;
         edtContagemResultado_DataEntrega_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "para Entrega", AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DataEntrega_Internalname, "Title", edtContagemResultado_DataEntrega_Title);
         edtContagemResultado_DataDmn_Titleformat = 2;
         edtContagemResultado_DataDmn_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "da OS", AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DataDmn_Internalname, "Title", edtContagemResultado_DataDmn_Title);
         edtContagemResultado_HoraEntrega_Titleformat = 2;
         edtContagemResultado_HoraEntrega_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de Entrega", AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_HoraEntrega_Internalname, "Title", edtContagemResultado_HoraEntrega_Title);
         edtContagemResultado_ContratadaPessoaNom_Titleformat = 2;
         edtContagemResultado_ContratadaPessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contratada", AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_ContratadaPessoaNom_Internalname, "Title", edtContagemResultado_ContratadaPessoaNom_Title);
         edtContagemResultado_DemandaFM_Titleformat = 2;
         edtContagemResultado_DemandaFM_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "OS", AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DemandaFM_Internalname, "Title", edtContagemResultado_DemandaFM_Title);
         edtContagemResultado_ServicoSigla_Titleformat = 2;
         edtContagemResultado_ServicoSigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Servi�o", AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_ServicoSigla_Internalname, "Title", edtContagemResultado_ServicoSigla_Title);
         AV96GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96GridCurrentPage), 10, 0)));
         AV97GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV97GridPageCount), 10, 0)));
         AV100WP_GridDS_1_Tfcontagemresultado_dataentrega = AV65TFContagemResultado_DataEntrega;
         AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to = AV66TFContagemResultado_DataEntrega_To;
         AV102WP_GridDS_3_Tfcontagemresultado_datadmn = AV71TFContagemResultado_DataDmn;
         AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to = AV72TFContagemResultado_DataDmn_To;
         AV104WP_GridDS_5_Tfcontagemresultado_horaentrega = AV77TFContagemResultado_HoraEntrega;
         AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to = AV78TFContagemResultado_HoraEntrega_To;
         AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = AV83TFContagemResultado_ContratadaPessoaNom;
         AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel = AV84TFContagemResultado_ContratadaPessoaNom_Sel;
         AV108WP_GridDS_9_Tfcontagemresultado_demandafm = AV87TFContagemResultado_DemandaFM;
         AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel = AV88TFContagemResultado_DemandaFM_Sel;
         AV110WP_GridDS_11_Tfcontagemresultado_servicosigla = AV91TFContagemResultado_ServicoSigla;
         AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel = AV92TFContagemResultado_ServicoSigla_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64ContagemResultado_DataEntregaTitleFilterData", AV64ContagemResultado_DataEntregaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV70ContagemResultado_DataDmnTitleFilterData", AV70ContagemResultado_DataDmnTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV76ContagemResultado_HoraEntregaTitleFilterData", AV76ContagemResultado_HoraEntregaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV82ContagemResultado_ContratadaPessoaNomTitleFilterData", AV82ContagemResultado_ContratadaPessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV86ContagemResultado_DemandaFMTitleFilterData", AV86ContagemResultado_DemandaFMTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV90ContagemResultado_ServicoSiglaTitleFilterData", AV90ContagemResultado_ServicoSiglaTitleFilterData);
      }

      protected void E11KV2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV95PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV95PageToGo) ;
         }
      }

      protected void E12KV2( )
      {
         /* Ddo_contagemresultado_dataentrega_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_dataentrega_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV65TFContagemResultado_DataEntrega = context.localUtil.CToD( Ddo_contagemresultado_dataentrega_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContagemResultado_DataEntrega", context.localUtil.Format(AV65TFContagemResultado_DataEntrega, "99/99/99"));
            AV66TFContagemResultado_DataEntrega_To = context.localUtil.CToD( Ddo_contagemresultado_dataentrega_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemResultado_DataEntrega_To", context.localUtil.Format(AV66TFContagemResultado_DataEntrega_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
      }

      protected void E13KV2( )
      {
         /* Ddo_contagemresultado_datadmn_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_datadmn_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV71TFContagemResultado_DataDmn = context.localUtil.CToD( Ddo_contagemresultado_datadmn_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemResultado_DataDmn", context.localUtil.Format(AV71TFContagemResultado_DataDmn, "99/99/99"));
            AV72TFContagemResultado_DataDmn_To = context.localUtil.CToD( Ddo_contagemresultado_datadmn_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFContagemResultado_DataDmn_To", context.localUtil.Format(AV72TFContagemResultado_DataDmn_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
      }

      protected void E14KV2( )
      {
         /* Ddo_contagemresultado_horaentrega_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_horaentrega_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV77TFContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(context.localUtil.CToT( Ddo_contagemresultado_horaentrega_Filteredtext_get, 2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContagemResultado_HoraEntrega", context.localUtil.TToC( AV77TFContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " "));
            AV78TFContagemResultado_HoraEntrega_To = DateTimeUtil.ResetDate(context.localUtil.CToT( Ddo_contagemresultado_horaentrega_Filteredtextto_get, 2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContagemResultado_HoraEntrega_To", context.localUtil.TToC( AV78TFContagemResultado_HoraEntrega_To, 0, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV78TFContagemResultado_HoraEntrega_To) )
            {
               AV78TFContagemResultado_HoraEntrega_To = DateTimeUtil.ResetDate(context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV78TFContagemResultado_HoraEntrega_To)), (short)(DateTimeUtil.Month( AV78TFContagemResultado_HoraEntrega_To)), (short)(DateTimeUtil.Day( AV78TFContagemResultado_HoraEntrega_To)), 23, 59, 59));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContagemResultado_HoraEntrega_To", context.localUtil.TToC( AV78TFContagemResultado_HoraEntrega_To, 0, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
      }

      protected void E15KV2( )
      {
         /* Ddo_contagemresultado_contratadapessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_contratadapessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV83TFContagemResultado_ContratadaPessoaNom = Ddo_contagemresultado_contratadapessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFContagemResultado_ContratadaPessoaNom", AV83TFContagemResultado_ContratadaPessoaNom);
            AV84TFContagemResultado_ContratadaPessoaNom_Sel = Ddo_contagemresultado_contratadapessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContagemResultado_ContratadaPessoaNom_Sel", AV84TFContagemResultado_ContratadaPessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E16KV2( )
      {
         /* Ddo_contagemresultado_demandafm_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_demandafm_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV87TFContagemResultado_DemandaFM = Ddo_contagemresultado_demandafm_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFContagemResultado_DemandaFM", AV87TFContagemResultado_DemandaFM);
            AV88TFContagemResultado_DemandaFM_Sel = Ddo_contagemresultado_demandafm_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFContagemResultado_DemandaFM_Sel", AV88TFContagemResultado_DemandaFM_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E17KV2( )
      {
         /* Ddo_contagemresultado_servicosigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_servicosigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV91TFContagemResultado_ServicoSigla = Ddo_contagemresultado_servicosigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFContagemResultado_ServicoSigla", AV91TFContagemResultado_ServicoSigla);
            AV92TFContagemResultado_ServicoSigla_Sel = Ddo_contagemresultado_servicosigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFContagemResultado_ServicoSigla_Sel", AV92TFContagemResultado_ServicoSigla_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E20KV2( )
      {
         /* Grid_Load Routine */
         AV59Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV59Update);
         AV112Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contagemresultado.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A456ContagemResultado_Codigo);
         AV60Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV60Delete);
         AV113Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contagemresultado.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A456ContagemResultado_Codigo);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 28;
         }
         sendrow_282( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_28_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(28, GridRow);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV61Session.Get(AV114Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV114Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV61Session.Get(AV114Pgmname+"GridState"), "");
         }
         AV115GXV1 = 1;
         while ( AV115GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV115GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DATAENTREGA") == 0 )
            {
               AV65TFContagemResultado_DataEntrega = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContagemResultado_DataEntrega", context.localUtil.Format(AV65TFContagemResultado_DataEntrega, "99/99/99"));
               AV66TFContagemResultado_DataEntrega_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemResultado_DataEntrega_To", context.localUtil.Format(AV66TFContagemResultado_DataEntrega_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV65TFContagemResultado_DataEntrega) )
               {
                  Ddo_contagemresultado_dataentrega_Filteredtext_set = context.localUtil.DToC( AV65TFContagemResultado_DataEntrega, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_dataentrega_Internalname, "FilteredText_set", Ddo_contagemresultado_dataentrega_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV66TFContagemResultado_DataEntrega_To) )
               {
                  Ddo_contagemresultado_dataentrega_Filteredtextto_set = context.localUtil.DToC( AV66TFContagemResultado_DataEntrega_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_dataentrega_Internalname, "FilteredTextTo_set", Ddo_contagemresultado_dataentrega_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV71TFContagemResultado_DataDmn = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContagemResultado_DataDmn", context.localUtil.Format(AV71TFContagemResultado_DataDmn, "99/99/99"));
               AV72TFContagemResultado_DataDmn_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFContagemResultado_DataDmn_To", context.localUtil.Format(AV72TFContagemResultado_DataDmn_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV71TFContagemResultado_DataDmn) )
               {
                  Ddo_contagemresultado_datadmn_Filteredtext_set = context.localUtil.DToC( AV71TFContagemResultado_DataDmn, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datadmn_Internalname, "FilteredText_set", Ddo_contagemresultado_datadmn_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV72TFContagemResultado_DataDmn_To) )
               {
                  Ddo_contagemresultado_datadmn_Filteredtextto_set = context.localUtil.DToC( AV72TFContagemResultado_DataDmn_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_datadmn_Internalname, "FilteredTextTo_set", Ddo_contagemresultado_datadmn_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_HORAENTREGA") == 0 )
            {
               AV77TFContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Value, 2));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContagemResultado_HoraEntrega", context.localUtil.TToC( AV77TFContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " "));
               AV78TFContagemResultado_HoraEntrega_To = DateTimeUtil.ResetDate(context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Valueto, 2));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContagemResultado_HoraEntrega_To", context.localUtil.TToC( AV78TFContagemResultado_HoraEntrega_To, 0, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV77TFContagemResultado_HoraEntrega) )
               {
                  AV79DDO_ContagemResultado_HoraEntregaAuxDate = DateTimeUtil.ResetTime(AV77TFContagemResultado_HoraEntrega);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79DDO_ContagemResultado_HoraEntregaAuxDate", context.localUtil.Format(AV79DDO_ContagemResultado_HoraEntregaAuxDate, "99/99/99"));
                  Ddo_contagemresultado_horaentrega_Filteredtext_set = context.localUtil.DToC( AV79DDO_ContagemResultado_HoraEntregaAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_horaentrega_Internalname, "FilteredText_set", Ddo_contagemresultado_horaentrega_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV78TFContagemResultado_HoraEntrega_To) )
               {
                  AV80DDO_ContagemResultado_HoraEntregaAuxDateTo = DateTimeUtil.ResetTime(AV78TFContagemResultado_HoraEntrega_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80DDO_ContagemResultado_HoraEntregaAuxDateTo", context.localUtil.Format(AV80DDO_ContagemResultado_HoraEntregaAuxDateTo, "99/99/99"));
                  Ddo_contagemresultado_horaentrega_Filteredtextto_set = context.localUtil.DToC( AV80DDO_ContagemResultado_HoraEntregaAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_horaentrega_Internalname, "FilteredTextTo_set", Ddo_contagemresultado_horaentrega_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CONTRATADAPESSOANOM") == 0 )
            {
               AV83TFContagemResultado_ContratadaPessoaNom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFContagemResultado_ContratadaPessoaNom", AV83TFContagemResultado_ContratadaPessoaNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83TFContagemResultado_ContratadaPessoaNom)) )
               {
                  Ddo_contagemresultado_contratadapessoanom_Filteredtext_set = AV83TFContagemResultado_ContratadaPessoaNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contratadapessoanom_Internalname, "FilteredText_set", Ddo_contagemresultado_contratadapessoanom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL") == 0 )
            {
               AV84TFContagemResultado_ContratadaPessoaNom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContagemResultado_ContratadaPessoaNom_Sel", AV84TFContagemResultado_ContratadaPessoaNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84TFContagemResultado_ContratadaPessoaNom_Sel)) )
               {
                  Ddo_contagemresultado_contratadapessoanom_Selectedvalue_set = AV84TFContagemResultado_ContratadaPessoaNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_contratadapessoanom_Internalname, "SelectedValue_set", Ddo_contagemresultado_contratadapessoanom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               AV87TFContagemResultado_DemandaFM = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFContagemResultado_DemandaFM", AV87TFContagemResultado_DemandaFM);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87TFContagemResultado_DemandaFM)) )
               {
                  Ddo_contagemresultado_demandafm_Filteredtext_set = AV87TFContagemResultado_DemandaFM;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "FilteredText_set", Ddo_contagemresultado_demandafm_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM_SEL") == 0 )
            {
               AV88TFContagemResultado_DemandaFM_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFContagemResultado_DemandaFM_Sel", AV88TFContagemResultado_DemandaFM_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88TFContagemResultado_DemandaFM_Sel)) )
               {
                  Ddo_contagemresultado_demandafm_Selectedvalue_set = AV88TFContagemResultado_DemandaFM_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_demandafm_Internalname, "SelectedValue_set", Ddo_contagemresultado_demandafm_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_SERVICOSIGLA") == 0 )
            {
               AV91TFContagemResultado_ServicoSigla = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFContagemResultado_ServicoSigla", AV91TFContagemResultado_ServicoSigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91TFContagemResultado_ServicoSigla)) )
               {
                  Ddo_contagemresultado_servicosigla_Filteredtext_set = AV91TFContagemResultado_ServicoSigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_servicosigla_Internalname, "FilteredText_set", Ddo_contagemresultado_servicosigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_SERVICOSIGLA_SEL") == 0 )
            {
               AV92TFContagemResultado_ServicoSigla_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFContagemResultado_ServicoSigla_Sel", AV92TFContagemResultado_ServicoSigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92TFContagemResultado_ServicoSigla_Sel)) )
               {
                  Ddo_contagemresultado_servicosigla_Selectedvalue_set = AV92TFContagemResultado_ServicoSigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_servicosigla_Internalname, "SelectedValue_set", Ddo_contagemresultado_servicosigla_Selectedvalue_set);
               }
            }
            AV115GXV1 = (int)(AV115GXV1+1);
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV61Session.Get(AV114Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (DateTime.MinValue==AV65TFContagemResultado_DataEntrega) && (DateTime.MinValue==AV66TFContagemResultado_DataEntrega_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DATAENTREGA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV65TFContagemResultado_DataEntrega, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV66TFContagemResultado_DataEntrega_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV71TFContagemResultado_DataDmn) && (DateTime.MinValue==AV72TFContagemResultado_DataDmn_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DATADMN";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV71TFContagemResultado_DataDmn, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV72TFContagemResultado_DataDmn_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV77TFContagemResultado_HoraEntrega) && (DateTime.MinValue==AV78TFContagemResultado_HoraEntrega_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_HORAENTREGA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV77TFContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV78TFContagemResultado_HoraEntrega_To, 0, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83TFContagemResultado_ContratadaPessoaNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_CONTRATADAPESSOANOM";
            AV11GridStateFilterValue.gxTpr_Value = AV83TFContagemResultado_ContratadaPessoaNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84TFContagemResultado_ContratadaPessoaNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV84TFContagemResultado_ContratadaPessoaNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87TFContagemResultado_DemandaFM)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DEMANDAFM";
            AV11GridStateFilterValue.gxTpr_Value = AV87TFContagemResultado_DemandaFM;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88TFContagemResultado_DemandaFM_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DEMANDAFM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV88TFContagemResultado_DemandaFM_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91TFContagemResultado_ServicoSigla)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_SERVICOSIGLA";
            AV11GridStateFilterValue.gxTpr_Value = AV91TFContagemResultado_ServicoSigla;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92TFContagemResultado_ServicoSigla_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_SERVICOSIGLA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV92TFContagemResultado_ServicoSigla_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV114Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV114Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContagemResultado";
         AV61Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_KV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_KV2( true) ;
         }
         else
         {
            wb_table2_8_KV2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_KV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_22_KV2( true) ;
         }
         else
         {
            wb_table3_22_KV2( false) ;
         }
         return  ;
      }

      protected void wb_table3_22_KV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_KV2e( true) ;
         }
         else
         {
            wb_table1_2_KV2e( false) ;
         }
      }

      protected void wb_table3_22_KV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table4_25_KV2( true) ;
         }
         else
         {
            wb_table4_25_KV2( false) ;
         }
         return  ;
      }

      protected void wb_table4_25_KV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, "tbJava", "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_Grid.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtncerrar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(28), 2, 0)+","+"null"+");", "Fechar", bttBtncerrar_Jsonclick, 7, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e21kv1_client"+"'", TempTags, "", 2, "HLP_WP_Grid.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_22_KV2e( true) ;
         }
         else
         {
            wb_table3_22_KV2e( false) ;
         }
      }

      protected void wb_table4_25_KV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"28\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_DataEntrega_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_DataEntrega_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_DataEntrega_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(56), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_DataDmn_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_DataDmn_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_DataDmn_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_HoraEntrega_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_HoraEntrega_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_HoraEntrega_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_ContratadaPessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_ContratadaPessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_ContratadaPessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_DemandaFM_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_DemandaFM_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_DemandaFM_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_ServicoSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_ServicoSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_ServicoSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV59Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Lock", 0);
               GridColumn.AddObjectProperty("Resizable", 0);
               GridColumn.AddObjectProperty("Sortable", 0);
               GridColumn.AddObjectProperty("Hidden", 0);
               GridColumn.AddObjectProperty("Hideable", 0);
               GridColumn.AddObjectProperty("MenuDisabled", -1);
               GridColumn.AddObjectProperty("AutoExpand", 0);
               GridColumn.AddObjectProperty("SummaryType", "none");
               GridColumn.AddObjectProperty("HeaderGroup", "");
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV60Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Lock", 0);
               GridColumn.AddObjectProperty("Resizable", 0);
               GridColumn.AddObjectProperty("Sortable", 0);
               GridColumn.AddObjectProperty("Hidden", 0);
               GridColumn.AddObjectProperty("Hideable", 0);
               GridColumn.AddObjectProperty("MenuDisabled", -1);
               GridColumn.AddObjectProperty("AutoExpand", 0);
               GridColumn.AddObjectProperty("SummaryType", "none");
               GridColumn.AddObjectProperty("HeaderGroup", "");
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_DataEntrega_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_DataEntrega_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Lock", 0);
               GridColumn.AddObjectProperty("Resizable", -1);
               GridColumn.AddObjectProperty("Sortable", 0);
               GridColumn.AddObjectProperty("Hidden", 0);
               GridColumn.AddObjectProperty("Hideable", -1);
               GridColumn.AddObjectProperty("MenuDisabled", 0);
               GridColumn.AddObjectProperty("AutoExpand", 0);
               GridColumn.AddObjectProperty("SummaryType", "none");
               GridColumn.AddObjectProperty("HeaderGroup", "");
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_DataDmn_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_DataDmn_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Lock", 0);
               GridColumn.AddObjectProperty("Resizable", -1);
               GridColumn.AddObjectProperty("Sortable", 0);
               GridColumn.AddObjectProperty("Hidden", 0);
               GridColumn.AddObjectProperty("Hideable", -1);
               GridColumn.AddObjectProperty("MenuDisabled", 0);
               GridColumn.AddObjectProperty("AutoExpand", 0);
               GridColumn.AddObjectProperty("SummaryType", "none");
               GridColumn.AddObjectProperty("HeaderGroup", "");
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_HoraEntrega_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_HoraEntrega_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Lock", 0);
               GridColumn.AddObjectProperty("Resizable", -1);
               GridColumn.AddObjectProperty("Sortable", 0);
               GridColumn.AddObjectProperty("Hidden", 0);
               GridColumn.AddObjectProperty("Hideable", -1);
               GridColumn.AddObjectProperty("MenuDisabled", 0);
               GridColumn.AddObjectProperty("AutoExpand", 0);
               GridColumn.AddObjectProperty("SummaryType", "none");
               GridColumn.AddObjectProperty("HeaderGroup", "");
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A500ContagemResultado_ContratadaPessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_ContratadaPessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_ContratadaPessoaNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Lock", 0);
               GridColumn.AddObjectProperty("Resizable", -1);
               GridColumn.AddObjectProperty("Sortable", 0);
               GridColumn.AddObjectProperty("Hidden", 0);
               GridColumn.AddObjectProperty("Hideable", -1);
               GridColumn.AddObjectProperty("MenuDisabled", 0);
               GridColumn.AddObjectProperty("AutoExpand", 0);
               GridColumn.AddObjectProperty("SummaryType", "none");
               GridColumn.AddObjectProperty("HeaderGroup", "");
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A493ContagemResultado_DemandaFM);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_DemandaFM_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_DemandaFM_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Lock", 0);
               GridColumn.AddObjectProperty("Resizable", -1);
               GridColumn.AddObjectProperty("Sortable", 0);
               GridColumn.AddObjectProperty("Hidden", 0);
               GridColumn.AddObjectProperty("Hideable", -1);
               GridColumn.AddObjectProperty("MenuDisabled", 0);
               GridColumn.AddObjectProperty("AutoExpand", 0);
               GridColumn.AddObjectProperty("SummaryType", "none");
               GridColumn.AddObjectProperty("HeaderGroup", "");
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A801ContagemResultado_ServicoSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_ServicoSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_ServicoSigla_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Lock", 0);
               GridColumn.AddObjectProperty("Resizable", -1);
               GridColumn.AddObjectProperty("Sortable", 0);
               GridColumn.AddObjectProperty("Hidden", 0);
               GridColumn.AddObjectProperty("Hideable", -1);
               GridColumn.AddObjectProperty("MenuDisabled", 0);
               GridColumn.AddObjectProperty("AutoExpand", 0);
               GridColumn.AddObjectProperty("SummaryType", "none");
               GridColumn.AddObjectProperty("HeaderGroup", "");
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 28 )
         {
            wbEnd = 0;
            nRC_GXsfl_28 = (short)(nGXsfl_28_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_25_KV2e( true) ;
         }
         else
         {
            wb_table4_25_KV2e( false) ;
         }
      }

      protected void wb_table2_8_KV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadotitle_Internalname, "Resultado das Contagens", "", "", lblContagemresultadotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_Grid.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_KV2( true) ;
         }
         else
         {
            wb_table5_13_KV2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_KV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_17_KV2( true) ;
         }
         else
         {
            wb_table6_17_KV2( false) ;
         }
         return  ;
      }

      protected void wb_table6_17_KV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_KV2e( true) ;
         }
         else
         {
            wb_table2_8_KV2e( false) ;
         }
      }

      protected void wb_table6_17_KV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_17_KV2e( true) ;
         }
         else
         {
            wb_table6_17_KV2e( false) ;
         }
      }

      protected void wb_table5_13_KV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_KV2e( true) ;
         }
         else
         {
            wb_table5_13_KV2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAKV2( ) ;
         WSKV2( ) ;
         WEKV2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("Shared/ext/resources/css/ext-all.css", "");
         AddStyleSheetFile("Shared/ext/resources/css/ext-all.css", "");
         AddStyleSheetFile("gxui/gxui-all.css", "");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("gxui/gxui-all.css", "");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020621625211");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_grid.js", "?2020621625211");
         context.AddJavascriptSource("Shared/ext/ext-all.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/ext/ext-all.js", "");
         context.AddJavascriptSource("gxui/gxui-all.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("gxui/gxui-all.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_282( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_28_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_28_idx;
         edtContagemResultado_DataEntrega_Internalname = "CONTAGEMRESULTADO_DATAENTREGA_"+sGXsfl_28_idx;
         edtContagemResultado_DataDmn_Internalname = "CONTAGEMRESULTADO_DATADMN_"+sGXsfl_28_idx;
         edtContagemResultado_HoraEntrega_Internalname = "CONTAGEMRESULTADO_HORAENTREGA_"+sGXsfl_28_idx;
         edtContagemResultado_ContratadaPessoaNom_Internalname = "CONTAGEMRESULTADO_CONTRATADAPESSOANOM_"+sGXsfl_28_idx;
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_28_idx;
         edtContagemResultado_ServicoSigla_Internalname = "CONTAGEMRESULTADO_SERVICOSIGLA_"+sGXsfl_28_idx;
      }

      protected void SubsflControlProps_fel_282( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_28_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_28_fel_idx;
         edtContagemResultado_DataEntrega_Internalname = "CONTAGEMRESULTADO_DATAENTREGA_"+sGXsfl_28_fel_idx;
         edtContagemResultado_DataDmn_Internalname = "CONTAGEMRESULTADO_DATADMN_"+sGXsfl_28_fel_idx;
         edtContagemResultado_HoraEntrega_Internalname = "CONTAGEMRESULTADO_HORAENTREGA_"+sGXsfl_28_fel_idx;
         edtContagemResultado_ContratadaPessoaNom_Internalname = "CONTAGEMRESULTADO_CONTRATADAPESSOANOM_"+sGXsfl_28_fel_idx;
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_28_fel_idx;
         edtContagemResultado_ServicoSigla_Internalname = "CONTAGEMRESULTADO_SERVICOSIGLA_"+sGXsfl_28_fel_idx;
      }

      protected void sendrow_282( )
      {
         SubsflControlProps_282( ) ;
         WBKV0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_28_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_28_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_28_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV59Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV59Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV112Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV59Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV59Update)) ? AV112Update_GXI : context.PathToRelativeUrl( AV59Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV59Update_IsBlob,(bool)false});
            GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridRow.AddRenderProperties(GridColumn);
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV60Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV60Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV113Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV60Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV60Delete)) ? AV113Delete_GXI : context.PathToRelativeUrl( AV60Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV60Delete_IsBlob,(bool)false});
            GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridRow.AddRenderProperties(GridColumn);
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DataEntrega_Internalname,context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"),context.localUtil.Format( A472ContagemResultado_DataEntrega, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DataEntrega_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)28,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridRow.AddRenderProperties(GridColumn);
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DataDmn_Internalname,context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"),context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DataDmn_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)56,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)28,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridRow.AddRenderProperties(GridColumn);
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_HoraEntrega_Internalname,context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A912ContagemResultado_HoraEntrega, "99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_HoraEntrega_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)28,(short)1,(short)-1,(short)0,(bool)true,(String)"Time",(String)"right",(bool)false});
            GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridRow.AddRenderProperties(GridColumn);
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_ContratadaPessoaNom_Internalname,StringUtil.RTrim( A500ContagemResultado_ContratadaPessoaNom),StringUtil.RTrim( context.localUtil.Format( A500ContagemResultado_ContratadaPessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_ContratadaPessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)28,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridRow.AddRenderProperties(GridColumn);
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DemandaFM_Internalname,(String)A493ContagemResultado_DemandaFM,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DemandaFM_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)28,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridRow.AddRenderProperties(GridColumn);
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_ServicoSigla_Internalname,StringUtil.RTrim( A801ContagemResultado_ServicoSigla),StringUtil.RTrim( context.localUtil.Format( A801ContagemResultado_ServicoSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_ServicoSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)28,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridRow.AddRenderProperties(GridColumn);
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DATAENTREGA"+"_"+sGXsfl_28_idx, GetSecureSignedToken( sGXsfl_28_idx, A472ContagemResultado_DataEntrega));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DATADMN"+"_"+sGXsfl_28_idx, GetSecureSignedToken( sGXsfl_28_idx, A471ContagemResultado_DataDmn));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_HORAENTREGA"+"_"+sGXsfl_28_idx, GetSecureSignedToken( sGXsfl_28_idx, context.localUtil.Format( A912ContagemResultado_HoraEntrega, "99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DEMANDAFM"+"_"+sGXsfl_28_idx, GetSecureSignedToken( sGXsfl_28_idx, StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_28_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_28_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_28_idx+1));
            sGXsfl_28_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_28_idx), 4, 0)), 4, "0");
            SubsflControlProps_282( ) ;
         }
         /* End function sendrow_282 */
      }

      protected void init_default_properties( )
      {
         lblContagemresultadotitle_Internalname = "CONTAGEMRESULTADOTITLE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtContagemResultado_DataEntrega_Internalname = "CONTAGEMRESULTADO_DATAENTREGA";
         edtContagemResultado_DataDmn_Internalname = "CONTAGEMRESULTADO_DATADMN";
         edtContagemResultado_HoraEntrega_Internalname = "CONTAGEMRESULTADO_HORAENTREGA";
         edtContagemResultado_ContratadaPessoaNom_Internalname = "CONTAGEMRESULTADO_CONTRATADAPESSOANOM";
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM";
         edtContagemResultado_ServicoSigla_Internalname = "CONTAGEMRESULTADO_SERVICOSIGLA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         lblTbjava_Internalname = "TBJAVA";
         bttBtncerrar_Internalname = "BTNCERRAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavTfcontagemresultado_dataentrega_Internalname = "vTFCONTAGEMRESULTADO_DATAENTREGA";
         edtavTfcontagemresultado_dataentrega_to_Internalname = "vTFCONTAGEMRESULTADO_DATAENTREGA_TO";
         edtavDdo_contagemresultado_dataentregaauxdate_Internalname = "vDDO_CONTAGEMRESULTADO_DATAENTREGAAUXDATE";
         edtavDdo_contagemresultado_dataentregaauxdateto_Internalname = "vDDO_CONTAGEMRESULTADO_DATAENTREGAAUXDATETO";
         divDdo_contagemresultado_dataentregaauxdates_Internalname = "DDO_CONTAGEMRESULTADO_DATAENTREGAAUXDATES";
         edtavTfcontagemresultado_datadmn_Internalname = "vTFCONTAGEMRESULTADO_DATADMN";
         edtavTfcontagemresultado_datadmn_to_Internalname = "vTFCONTAGEMRESULTADO_DATADMN_TO";
         edtavDdo_contagemresultado_datadmnauxdate_Internalname = "vDDO_CONTAGEMRESULTADO_DATADMNAUXDATE";
         edtavDdo_contagemresultado_datadmnauxdateto_Internalname = "vDDO_CONTAGEMRESULTADO_DATADMNAUXDATETO";
         divDdo_contagemresultado_datadmnauxdates_Internalname = "DDO_CONTAGEMRESULTADO_DATADMNAUXDATES";
         edtavTfcontagemresultado_horaentrega_Internalname = "vTFCONTAGEMRESULTADO_HORAENTREGA";
         edtavTfcontagemresultado_horaentrega_to_Internalname = "vTFCONTAGEMRESULTADO_HORAENTREGA_TO";
         edtavDdo_contagemresultado_horaentregaauxdate_Internalname = "vDDO_CONTAGEMRESULTADO_HORAENTREGAAUXDATE";
         edtavDdo_contagemresultado_horaentregaauxdateto_Internalname = "vDDO_CONTAGEMRESULTADO_HORAENTREGAAUXDATETO";
         divDdo_contagemresultado_horaentregaauxdates_Internalname = "DDO_CONTAGEMRESULTADO_HORAENTREGAAUXDATES";
         edtavTfcontagemresultado_contratadapessoanom_Internalname = "vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM";
         edtavTfcontagemresultado_contratadapessoanom_sel_Internalname = "vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL";
         edtavTfcontagemresultado_demandafm_Internalname = "vTFCONTAGEMRESULTADO_DEMANDAFM";
         edtavTfcontagemresultado_demandafm_sel_Internalname = "vTFCONTAGEMRESULTADO_DEMANDAFM_SEL";
         edtavTfcontagemresultado_servicosigla_Internalname = "vTFCONTAGEMRESULTADO_SERVICOSIGLA";
         edtavTfcontagemresultado_servicosigla_sel_Internalname = "vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL";
         Ddo_contagemresultado_dataentrega_Internalname = "DDO_CONTAGEMRESULTADO_DATAENTREGA";
         edtavDdo_contagemresultado_dataentregatitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_datadmn_Internalname = "DDO_CONTAGEMRESULTADO_DATADMN";
         edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_horaentrega_Internalname = "DDO_CONTAGEMRESULTADO_HORAENTREGA";
         edtavDdo_contagemresultado_horaentregatitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_contratadapessoanom_Internalname = "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM";
         edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_demandafm_Internalname = "DDO_CONTAGEMRESULTADO_DEMANDAFM";
         edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_servicosigla_Internalname = "DDO_CONTAGEMRESULTADO_SERVICOSIGLA";
         edtavDdo_contagemresultado_servicosiglatitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContagemResultado_ServicoSigla_Jsonclick = "";
         edtContagemResultado_DemandaFM_Jsonclick = "";
         edtContagemResultado_ContratadaPessoaNom_Jsonclick = "";
         edtContagemResultado_HoraEntrega_Jsonclick = "";
         edtContagemResultado_DataDmn_Jsonclick = "";
         edtContagemResultado_DataEntrega_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtContagemResultado_ServicoSigla_Titleformat = 0;
         edtContagemResultado_DemandaFM_Titleformat = 0;
         edtContagemResultado_ContratadaPessoaNom_Titleformat = 0;
         edtContagemResultado_HoraEntrega_Titleformat = 0;
         edtContagemResultado_DataDmn_Titleformat = 0;
         edtContagemResultado_DataEntrega_Titleformat = 0;
         lblTbjava_Visible = 1;
         edtContagemResultado_ServicoSigla_Title = "Servi�o";
         edtContagemResultado_DemandaFM_Title = "OS";
         edtContagemResultado_ContratadaPessoaNom_Title = "Contratada";
         edtContagemResultado_HoraEntrega_Title = "de Entrega";
         edtContagemResultado_DataDmn_Title = "da OS";
         edtContagemResultado_DataEntrega_Title = "para Entrega";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_contagemresultado_servicosiglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_horaentregatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_dataentregatitlecontrolidtoreplace_Visible = 1;
         edtavTfcontagemresultado_servicosigla_sel_Jsonclick = "";
         edtavTfcontagemresultado_servicosigla_sel_Visible = 1;
         edtavTfcontagemresultado_servicosigla_Jsonclick = "";
         edtavTfcontagemresultado_servicosigla_Visible = 1;
         edtavTfcontagemresultado_demandafm_sel_Jsonclick = "";
         edtavTfcontagemresultado_demandafm_sel_Visible = 1;
         edtavTfcontagemresultado_demandafm_Jsonclick = "";
         edtavTfcontagemresultado_demandafm_Visible = 1;
         edtavTfcontagemresultado_contratadapessoanom_sel_Jsonclick = "";
         edtavTfcontagemresultado_contratadapessoanom_sel_Visible = 1;
         edtavTfcontagemresultado_contratadapessoanom_Jsonclick = "";
         edtavTfcontagemresultado_contratadapessoanom_Visible = 1;
         edtavDdo_contagemresultado_horaentregaauxdateto_Jsonclick = "";
         edtavDdo_contagemresultado_horaentregaauxdate_Jsonclick = "";
         edtavTfcontagemresultado_horaentrega_to_Jsonclick = "";
         edtavTfcontagemresultado_horaentrega_to_Visible = 1;
         edtavTfcontagemresultado_horaentrega_Jsonclick = "";
         edtavTfcontagemresultado_horaentrega_Visible = 1;
         edtavDdo_contagemresultado_datadmnauxdateto_Jsonclick = "";
         edtavDdo_contagemresultado_datadmnauxdate_Jsonclick = "";
         edtavTfcontagemresultado_datadmn_to_Jsonclick = "";
         edtavTfcontagemresultado_datadmn_to_Visible = 1;
         edtavTfcontagemresultado_datadmn_Jsonclick = "";
         edtavTfcontagemresultado_datadmn_Visible = 1;
         edtavDdo_contagemresultado_dataentregaauxdateto_Jsonclick = "";
         edtavDdo_contagemresultado_dataentregaauxdate_Jsonclick = "";
         edtavTfcontagemresultado_dataentrega_to_Jsonclick = "";
         edtavTfcontagemresultado_dataentrega_to_Visible = 1;
         edtavTfcontagemresultado_dataentrega_Jsonclick = "";
         edtavTfcontagemresultado_dataentrega_Visible = 1;
         subGrid_Groupfield = "ContagemResultado_DataEntrega";
         subGrid_Addtoparentgxuicontrol = StringUtil.Str( (decimal)(0), 1, 0);
         subGrid_Class = "WorkWithBorder WorkWith";
         subGrid_Grouping = StringUtil.LTrim( StringUtil.Str( (decimal)(-1), 2, 0));
         Ddo_contagemresultado_servicosigla_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_servicosigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultado_servicosigla_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_servicosigla_Loadingdata = "Carregando dados...";
         Ddo_contagemresultado_servicosigla_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultado_servicosigla_Datalistproc = "GetWP_GridFilterData";
         Ddo_contagemresultado_servicosigla_Datalisttype = "Dynamic";
         Ddo_contagemresultado_servicosigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultado_servicosigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultado_servicosigla_Filtertype = "Character";
         Ddo_contagemresultado_servicosigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_servicosigla_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contagemresultado_servicosigla_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contagemresultado_servicosigla_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_servicosigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_servicosigla_Cls = "ColumnSettings";
         Ddo_contagemresultado_servicosigla_Tooltip = "Op��es";
         Ddo_contagemresultado_servicosigla_Caption = "";
         Ddo_contagemresultado_demandafm_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_demandafm_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultado_demandafm_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_demandafm_Loadingdata = "Carregando dados...";
         Ddo_contagemresultado_demandafm_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultado_demandafm_Datalistproc = "GetWP_GridFilterData";
         Ddo_contagemresultado_demandafm_Datalisttype = "Dynamic";
         Ddo_contagemresultado_demandafm_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demandafm_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultado_demandafm_Filtertype = "Character";
         Ddo_contagemresultado_demandafm_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demandafm_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contagemresultado_demandafm_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_demandafm_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_demandafm_Cls = "ColumnSettings";
         Ddo_contagemresultado_demandafm_Tooltip = "Op��es";
         Ddo_contagemresultado_demandafm_Caption = "";
         Ddo_contagemresultado_contratadapessoanom_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_contratadapessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultado_contratadapessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_contratadapessoanom_Loadingdata = "Carregando dados...";
         Ddo_contagemresultado_contratadapessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultado_contratadapessoanom_Datalistproc = "GetWP_GridFilterData";
         Ddo_contagemresultado_contratadapessoanom_Datalisttype = "Dynamic";
         Ddo_contagemresultado_contratadapessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultado_contratadapessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultado_contratadapessoanom_Filtertype = "Character";
         Ddo_contagemresultado_contratadapessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_contratadapessoanom_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contagemresultado_contratadapessoanom_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contagemresultado_contratadapessoanom_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_contratadapessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_contratadapessoanom_Cls = "ColumnSettings";
         Ddo_contagemresultado_contratadapessoanom_Tooltip = "Op��es";
         Ddo_contagemresultado_contratadapessoanom_Caption = "";
         Ddo_contagemresultado_horaentrega_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_horaentrega_Rangefilterto = "At�";
         Ddo_contagemresultado_horaentrega_Rangefilterfrom = "Desde";
         Ddo_contagemresultado_horaentrega_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_horaentrega_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultado_horaentrega_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultado_horaentrega_Filtertype = "Date";
         Ddo_contagemresultado_horaentrega_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_horaentrega_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contagemresultado_horaentrega_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contagemresultado_horaentrega_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_horaentrega_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_horaentrega_Cls = "ColumnSettings";
         Ddo_contagemresultado_horaentrega_Tooltip = "Op��es";
         Ddo_contagemresultado_horaentrega_Caption = "";
         Ddo_contagemresultado_datadmn_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_datadmn_Rangefilterto = "At�";
         Ddo_contagemresultado_datadmn_Rangefilterfrom = "Desde";
         Ddo_contagemresultado_datadmn_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_datadmn_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultado_datadmn_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultado_datadmn_Filtertype = "Date";
         Ddo_contagemresultado_datadmn_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_datadmn_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contagemresultado_datadmn_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contagemresultado_datadmn_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_datadmn_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_datadmn_Cls = "ColumnSettings";
         Ddo_contagemresultado_datadmn_Tooltip = "Op��es";
         Ddo_contagemresultado_datadmn_Caption = "";
         Ddo_contagemresultado_dataentrega_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_dataentrega_Rangefilterto = "At�";
         Ddo_contagemresultado_dataentrega_Rangefilterfrom = "Desde";
         Ddo_contagemresultado_dataentrega_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_dataentrega_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultado_dataentrega_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultado_dataentrega_Filtertype = "Date";
         Ddo_contagemresultado_dataentrega_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_dataentrega_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contagemresultado_dataentrega_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contagemresultado_dataentrega_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_dataentrega_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_dataentrega_Cls = "ColumnSettings";
         Ddo_contagemresultado_dataentrega_Tooltip = "Op��es";
         Ddo_contagemresultado_dataentrega_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Grid";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''},{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''},{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV64ContagemResultado_DataEntregaTitleFilterData',fld:'vCONTAGEMRESULTADO_DATAENTREGATITLEFILTERDATA',pic:'',nv:null},{av:'AV70ContagemResultado_DataDmnTitleFilterData',fld:'vCONTAGEMRESULTADO_DATADMNTITLEFILTERDATA',pic:'',nv:null},{av:'AV76ContagemResultado_HoraEntregaTitleFilterData',fld:'vCONTAGEMRESULTADO_HORAENTREGATITLEFILTERDATA',pic:'',nv:null},{av:'AV82ContagemResultado_ContratadaPessoaNomTitleFilterData',fld:'vCONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV86ContagemResultado_DemandaFMTitleFilterData',fld:'vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA',pic:'',nv:null},{av:'AV90ContagemResultado_ServicoSiglaTitleFilterData',fld:'vCONTAGEMRESULTADO_SERVICOSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'edtContagemResultado_DataEntrega_Titleformat',ctrl:'CONTAGEMRESULTADO_DATAENTREGA',prop:'Titleformat'},{av:'edtContagemResultado_DataEntrega_Title',ctrl:'CONTAGEMRESULTADO_DATAENTREGA',prop:'Title'},{av:'edtContagemResultado_DataDmn_Titleformat',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Titleformat'},{av:'edtContagemResultado_DataDmn_Title',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Title'},{av:'edtContagemResultado_HoraEntrega_Titleformat',ctrl:'CONTAGEMRESULTADO_HORAENTREGA',prop:'Titleformat'},{av:'edtContagemResultado_HoraEntrega_Title',ctrl:'CONTAGEMRESULTADO_HORAENTREGA',prop:'Title'},{av:'edtContagemResultado_ContratadaPessoaNom_Titleformat',ctrl:'CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'Titleformat'},{av:'edtContagemResultado_ContratadaPessoaNom_Title',ctrl:'CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'Title'},{av:'edtContagemResultado_DemandaFM_Titleformat',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Titleformat'},{av:'edtContagemResultado_DemandaFM_Title',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Title'},{av:'edtContagemResultado_ServicoSigla_Titleformat',ctrl:'CONTAGEMRESULTADO_SERVICOSIGLA',prop:'Titleformat'},{av:'edtContagemResultado_ServicoSigla_Title',ctrl:'CONTAGEMRESULTADO_SERVICOSIGLA',prop:'Title'},{av:'AV96GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV97GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11KV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''},{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''},{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_DATAENTREGA.ONOPTIONCLICKED","{handler:'E12KV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''},{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''},{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultado_dataentrega_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_DATAENTREGA',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_dataentrega_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_DATAENTREGA',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_dataentrega_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADO_DATAENTREGA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_DATADMN.ONOPTIONCLICKED","{handler:'E13KV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''},{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''},{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultado_datadmn_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_datadmn_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_datadmn_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'FilteredTextTo_get'}],oparms:[{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_HORAENTREGA.ONOPTIONCLICKED","{handler:'E14KV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''},{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''},{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultado_horaentrega_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_HORAENTREGA',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_horaentrega_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_HORAENTREGA',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_horaentrega_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADO_HORAENTREGA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM.ONOPTIONCLICKED","{handler:'E15KV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''},{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''},{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultado_contratadapessoanom_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_contratadapessoanom_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_contratadapessoanom_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_DEMANDAFM.ONOPTIONCLICKED","{handler:'E16KV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''},{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''},{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultado_demandafm_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_demandafm_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_demandafm_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SelectedValue_get'}],oparms:[{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_SERVICOSIGLA.ONOPTIONCLICKED","{handler:'E17KV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''},{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''},{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultado_servicosigla_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_SERVICOSIGLA',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_servicosigla_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_SERVICOSIGLA',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_servicosigla_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADO_SERVICOSIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''}]}");
         setEventMetadata("GRID.LOAD","{handler:'E20KV2',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV59Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV60Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'}]}");
         setEventMetadata("'DOCERRAR'","{handler:'E21KV1',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contagemresultado_dataentrega_Activeeventkey = "";
         Ddo_contagemresultado_dataentrega_Filteredtext_get = "";
         Ddo_contagemresultado_dataentrega_Filteredtextto_get = "";
         Ddo_contagemresultado_datadmn_Activeeventkey = "";
         Ddo_contagemresultado_datadmn_Filteredtext_get = "";
         Ddo_contagemresultado_datadmn_Filteredtextto_get = "";
         Ddo_contagemresultado_horaentrega_Activeeventkey = "";
         Ddo_contagemresultado_horaentrega_Filteredtext_get = "";
         Ddo_contagemresultado_horaentrega_Filteredtextto_get = "";
         Ddo_contagemresultado_contratadapessoanom_Activeeventkey = "";
         Ddo_contagemresultado_contratadapessoanom_Filteredtext_get = "";
         Ddo_contagemresultado_contratadapessoanom_Selectedvalue_get = "";
         Ddo_contagemresultado_demandafm_Activeeventkey = "";
         Ddo_contagemresultado_demandafm_Filteredtext_get = "";
         Ddo_contagemresultado_demandafm_Selectedvalue_get = "";
         Ddo_contagemresultado_servicosigla_Activeeventkey = "";
         Ddo_contagemresultado_servicosigla_Filteredtext_get = "";
         Ddo_contagemresultado_servicosigla_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV65TFContagemResultado_DataEntrega = DateTime.MinValue;
         AV66TFContagemResultado_DataEntrega_To = DateTime.MinValue;
         AV71TFContagemResultado_DataDmn = DateTime.MinValue;
         AV72TFContagemResultado_DataDmn_To = DateTime.MinValue;
         AV77TFContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         AV78TFContagemResultado_HoraEntrega_To = (DateTime)(DateTime.MinValue);
         AV83TFContagemResultado_ContratadaPessoaNom = "";
         AV84TFContagemResultado_ContratadaPessoaNom_Sel = "";
         AV87TFContagemResultado_DemandaFM = "";
         AV88TFContagemResultado_DemandaFM_Sel = "";
         AV91TFContagemResultado_ServicoSigla = "";
         AV92TFContagemResultado_ServicoSigla_Sel = "";
         AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace = "";
         AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace = "";
         AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace = "";
         AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace = "";
         AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = "";
         AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace = "";
         AV114Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV94DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV64ContagemResultado_DataEntregaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70ContagemResultado_DataDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76ContagemResultado_HoraEntregaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV82ContagemResultado_ContratadaPessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV86ContagemResultado_DemandaFMTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV90ContagemResultado_ServicoSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contagemresultado_dataentrega_Filteredtext_set = "";
         Ddo_contagemresultado_dataentrega_Filteredtextto_set = "";
         Ddo_contagemresultado_datadmn_Filteredtext_set = "";
         Ddo_contagemresultado_datadmn_Filteredtextto_set = "";
         Ddo_contagemresultado_horaentrega_Filteredtext_set = "";
         Ddo_contagemresultado_horaentrega_Filteredtextto_set = "";
         Ddo_contagemresultado_contratadapessoanom_Filteredtext_set = "";
         Ddo_contagemresultado_contratadapessoanom_Selectedvalue_set = "";
         Ddo_contagemresultado_demandafm_Filteredtext_set = "";
         Ddo_contagemresultado_demandafm_Selectedvalue_set = "";
         Ddo_contagemresultado_servicosigla_Filteredtext_set = "";
         Ddo_contagemresultado_servicosigla_Selectedvalue_set = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         AV67DDO_ContagemResultado_DataEntregaAuxDate = DateTime.MinValue;
         AV68DDO_ContagemResultado_DataEntregaAuxDateTo = DateTime.MinValue;
         AV73DDO_ContagemResultado_DataDmnAuxDate = DateTime.MinValue;
         AV74DDO_ContagemResultado_DataDmnAuxDateTo = DateTime.MinValue;
         AV79DDO_ContagemResultado_HoraEntregaAuxDate = DateTime.MinValue;
         AV80DDO_ContagemResultado_HoraEntregaAuxDateTo = DateTime.MinValue;
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV59Update = "";
         AV112Update_GXI = "";
         AV60Delete = "";
         AV113Delete_GXI = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A500ContagemResultado_ContratadaPessoaNom = "";
         A493ContagemResultado_DemandaFM = "";
         A801ContagemResultado_ServicoSigla = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = "";
         lV108WP_GridDS_9_Tfcontagemresultado_demandafm = "";
         lV110WP_GridDS_11_Tfcontagemresultado_servicosigla = "";
         AV100WP_GridDS_1_Tfcontagemresultado_dataentrega = DateTime.MinValue;
         AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to = DateTime.MinValue;
         AV102WP_GridDS_3_Tfcontagemresultado_datadmn = DateTime.MinValue;
         AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to = DateTime.MinValue;
         AV104WP_GridDS_5_Tfcontagemresultado_horaentrega = (DateTime)(DateTime.MinValue);
         AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to = (DateTime)(DateTime.MinValue);
         AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel = "";
         AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = "";
         AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel = "";
         AV108WP_GridDS_9_Tfcontagemresultado_demandafm = "";
         AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel = "";
         AV110WP_GridDS_11_Tfcontagemresultado_servicosigla = "";
         H00KV2_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00KV2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00KV2_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00KV2_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00KV2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00KV2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00KV2_A601ContagemResultado_Servico = new int[1] ;
         H00KV2_n601ContagemResultado_Servico = new bool[] {false} ;
         H00KV2_A456ContagemResultado_Codigo = new int[1] ;
         H00KV2_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00KV2_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00KV2_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00KV2_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00KV2_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         H00KV2_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         H00KV2_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         H00KV2_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         H00KV2_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00KV2_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00KV2_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00KV3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV61Session = context.GetSession();
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         lblTbjava_Jsonclick = "";
         bttBtncerrar_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContagemresultadotitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_grid__default(),
            new Object[][] {
                new Object[] {
               H00KV2_A490ContagemResultado_ContratadaCod, H00KV2_n490ContagemResultado_ContratadaCod, H00KV2_A499ContagemResultado_ContratadaPessoaCod, H00KV2_n499ContagemResultado_ContratadaPessoaCod, H00KV2_A1553ContagemResultado_CntSrvCod, H00KV2_n1553ContagemResultado_CntSrvCod, H00KV2_A601ContagemResultado_Servico, H00KV2_n601ContagemResultado_Servico, H00KV2_A456ContagemResultado_Codigo, H00KV2_A801ContagemResultado_ServicoSigla,
               H00KV2_n801ContagemResultado_ServicoSigla, H00KV2_A493ContagemResultado_DemandaFM, H00KV2_n493ContagemResultado_DemandaFM, H00KV2_A500ContagemResultado_ContratadaPessoaNom, H00KV2_n500ContagemResultado_ContratadaPessoaNom, H00KV2_A912ContagemResultado_HoraEntrega, H00KV2_n912ContagemResultado_HoraEntrega, H00KV2_A471ContagemResultado_DataDmn, H00KV2_A472ContagemResultado_DataEntrega, H00KV2_n472ContagemResultado_DataEntrega
               }
               , new Object[] {
               H00KV3_AGRID_nRecordCount
               }
            }
         );
         AV114Pgmname = "WP_Grid";
         /* GeneXus formulas. */
         AV114Pgmname = "WP_Grid";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_28 ;
      private short nGXsfl_28_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_28_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagemResultado_DataEntrega_Titleformat ;
      private short edtContagemResultado_DataDmn_Titleformat ;
      private short edtContagemResultado_HoraEntrega_Titleformat ;
      private short edtContagemResultado_ContratadaPessoaNom_Titleformat ;
      private short edtContagemResultado_DemandaFM_Titleformat ;
      private short edtContagemResultado_ServicoSigla_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A456ContagemResultado_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contagemresultado_contratadapessoanom_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultado_demandafm_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultado_servicosigla_Datalistupdateminimumcharacters ;
      private int edtavTfcontagemresultado_dataentrega_Visible ;
      private int edtavTfcontagemresultado_dataentrega_to_Visible ;
      private int edtavTfcontagemresultado_datadmn_Visible ;
      private int edtavTfcontagemresultado_datadmn_to_Visible ;
      private int edtavTfcontagemresultado_horaentrega_Visible ;
      private int edtavTfcontagemresultado_horaentrega_to_Visible ;
      private int edtavTfcontagemresultado_contratadapessoanom_Visible ;
      private int edtavTfcontagemresultado_contratadapessoanom_sel_Visible ;
      private int edtavTfcontagemresultado_demandafm_Visible ;
      private int edtavTfcontagemresultado_demandafm_sel_Visible ;
      private int edtavTfcontagemresultado_servicosigla_Visible ;
      private int edtavTfcontagemresultado_servicosigla_sel_Visible ;
      private int edtavDdo_contagemresultado_dataentregatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_horaentregatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_servicosiglatitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A499ContagemResultado_ContratadaPessoaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int lblTbjava_Visible ;
      private int AV95PageToGo ;
      private int AV115GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV96GridCurrentPage ;
      private long AV97GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contagemresultado_dataentrega_Activeeventkey ;
      private String Ddo_contagemresultado_dataentrega_Filteredtext_get ;
      private String Ddo_contagemresultado_dataentrega_Filteredtextto_get ;
      private String Ddo_contagemresultado_datadmn_Activeeventkey ;
      private String Ddo_contagemresultado_datadmn_Filteredtext_get ;
      private String Ddo_contagemresultado_datadmn_Filteredtextto_get ;
      private String Ddo_contagemresultado_horaentrega_Activeeventkey ;
      private String Ddo_contagemresultado_horaentrega_Filteredtext_get ;
      private String Ddo_contagemresultado_horaentrega_Filteredtextto_get ;
      private String Ddo_contagemresultado_contratadapessoanom_Activeeventkey ;
      private String Ddo_contagemresultado_contratadapessoanom_Filteredtext_get ;
      private String Ddo_contagemresultado_contratadapessoanom_Selectedvalue_get ;
      private String Ddo_contagemresultado_demandafm_Activeeventkey ;
      private String Ddo_contagemresultado_demandafm_Filteredtext_get ;
      private String Ddo_contagemresultado_demandafm_Selectedvalue_get ;
      private String Ddo_contagemresultado_servicosigla_Activeeventkey ;
      private String Ddo_contagemresultado_servicosigla_Filteredtext_get ;
      private String Ddo_contagemresultado_servicosigla_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_28_idx="0001" ;
      private String AV83TFContagemResultado_ContratadaPessoaNom ;
      private String AV84TFContagemResultado_ContratadaPessoaNom_Sel ;
      private String AV91TFContagemResultado_ServicoSigla ;
      private String AV92TFContagemResultado_ServicoSigla_Sel ;
      private String AV114Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contagemresultado_dataentrega_Caption ;
      private String Ddo_contagemresultado_dataentrega_Tooltip ;
      private String Ddo_contagemresultado_dataentrega_Cls ;
      private String Ddo_contagemresultado_dataentrega_Filteredtext_set ;
      private String Ddo_contagemresultado_dataentrega_Filteredtextto_set ;
      private String Ddo_contagemresultado_dataentrega_Dropdownoptionstype ;
      private String Ddo_contagemresultado_dataentrega_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_dataentrega_Filtertype ;
      private String Ddo_contagemresultado_dataentrega_Cleanfilter ;
      private String Ddo_contagemresultado_dataentrega_Rangefilterfrom ;
      private String Ddo_contagemresultado_dataentrega_Rangefilterto ;
      private String Ddo_contagemresultado_dataentrega_Searchbuttontext ;
      private String Ddo_contagemresultado_datadmn_Caption ;
      private String Ddo_contagemresultado_datadmn_Tooltip ;
      private String Ddo_contagemresultado_datadmn_Cls ;
      private String Ddo_contagemresultado_datadmn_Filteredtext_set ;
      private String Ddo_contagemresultado_datadmn_Filteredtextto_set ;
      private String Ddo_contagemresultado_datadmn_Dropdownoptionstype ;
      private String Ddo_contagemresultado_datadmn_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_datadmn_Filtertype ;
      private String Ddo_contagemresultado_datadmn_Cleanfilter ;
      private String Ddo_contagemresultado_datadmn_Rangefilterfrom ;
      private String Ddo_contagemresultado_datadmn_Rangefilterto ;
      private String Ddo_contagemresultado_datadmn_Searchbuttontext ;
      private String Ddo_contagemresultado_horaentrega_Caption ;
      private String Ddo_contagemresultado_horaentrega_Tooltip ;
      private String Ddo_contagemresultado_horaentrega_Cls ;
      private String Ddo_contagemresultado_horaentrega_Filteredtext_set ;
      private String Ddo_contagemresultado_horaentrega_Filteredtextto_set ;
      private String Ddo_contagemresultado_horaentrega_Dropdownoptionstype ;
      private String Ddo_contagemresultado_horaentrega_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_horaentrega_Filtertype ;
      private String Ddo_contagemresultado_horaentrega_Cleanfilter ;
      private String Ddo_contagemresultado_horaentrega_Rangefilterfrom ;
      private String Ddo_contagemresultado_horaentrega_Rangefilterto ;
      private String Ddo_contagemresultado_horaentrega_Searchbuttontext ;
      private String Ddo_contagemresultado_contratadapessoanom_Caption ;
      private String Ddo_contagemresultado_contratadapessoanom_Tooltip ;
      private String Ddo_contagemresultado_contratadapessoanom_Cls ;
      private String Ddo_contagemresultado_contratadapessoanom_Filteredtext_set ;
      private String Ddo_contagemresultado_contratadapessoanom_Selectedvalue_set ;
      private String Ddo_contagemresultado_contratadapessoanom_Dropdownoptionstype ;
      private String Ddo_contagemresultado_contratadapessoanom_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_contratadapessoanom_Filtertype ;
      private String Ddo_contagemresultado_contratadapessoanom_Datalisttype ;
      private String Ddo_contagemresultado_contratadapessoanom_Datalistproc ;
      private String Ddo_contagemresultado_contratadapessoanom_Loadingdata ;
      private String Ddo_contagemresultado_contratadapessoanom_Cleanfilter ;
      private String Ddo_contagemresultado_contratadapessoanom_Noresultsfound ;
      private String Ddo_contagemresultado_contratadapessoanom_Searchbuttontext ;
      private String Ddo_contagemresultado_demandafm_Caption ;
      private String Ddo_contagemresultado_demandafm_Tooltip ;
      private String Ddo_contagemresultado_demandafm_Cls ;
      private String Ddo_contagemresultado_demandafm_Filteredtext_set ;
      private String Ddo_contagemresultado_demandafm_Selectedvalue_set ;
      private String Ddo_contagemresultado_demandafm_Dropdownoptionstype ;
      private String Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_demandafm_Filtertype ;
      private String Ddo_contagemresultado_demandafm_Datalisttype ;
      private String Ddo_contagemresultado_demandafm_Datalistproc ;
      private String Ddo_contagemresultado_demandafm_Loadingdata ;
      private String Ddo_contagemresultado_demandafm_Cleanfilter ;
      private String Ddo_contagemresultado_demandafm_Noresultsfound ;
      private String Ddo_contagemresultado_demandafm_Searchbuttontext ;
      private String Ddo_contagemresultado_servicosigla_Caption ;
      private String Ddo_contagemresultado_servicosigla_Tooltip ;
      private String Ddo_contagemresultado_servicosigla_Cls ;
      private String Ddo_contagemresultado_servicosigla_Filteredtext_set ;
      private String Ddo_contagemresultado_servicosigla_Selectedvalue_set ;
      private String Ddo_contagemresultado_servicosigla_Dropdownoptionstype ;
      private String Ddo_contagemresultado_servicosigla_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_servicosigla_Filtertype ;
      private String Ddo_contagemresultado_servicosigla_Datalisttype ;
      private String Ddo_contagemresultado_servicosigla_Datalistproc ;
      private String Ddo_contagemresultado_servicosigla_Loadingdata ;
      private String Ddo_contagemresultado_servicosigla_Cleanfilter ;
      private String Ddo_contagemresultado_servicosigla_Noresultsfound ;
      private String Ddo_contagemresultado_servicosigla_Searchbuttontext ;
      private String subGrid_Grouping ;
      private String subGrid_Class ;
      private String subGrid_Addtoparentgxuicontrol ;
      private String subGrid_Groupfield ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavTfcontagemresultado_dataentrega_Internalname ;
      private String edtavTfcontagemresultado_dataentrega_Jsonclick ;
      private String edtavTfcontagemresultado_dataentrega_to_Internalname ;
      private String edtavTfcontagemresultado_dataentrega_to_Jsonclick ;
      private String divDdo_contagemresultado_dataentregaauxdates_Internalname ;
      private String edtavDdo_contagemresultado_dataentregaauxdate_Internalname ;
      private String edtavDdo_contagemresultado_dataentregaauxdate_Jsonclick ;
      private String edtavDdo_contagemresultado_dataentregaauxdateto_Internalname ;
      private String edtavDdo_contagemresultado_dataentregaauxdateto_Jsonclick ;
      private String edtavTfcontagemresultado_datadmn_Internalname ;
      private String edtavTfcontagemresultado_datadmn_Jsonclick ;
      private String edtavTfcontagemresultado_datadmn_to_Internalname ;
      private String edtavTfcontagemresultado_datadmn_to_Jsonclick ;
      private String divDdo_contagemresultado_datadmnauxdates_Internalname ;
      private String edtavDdo_contagemresultado_datadmnauxdate_Internalname ;
      private String edtavDdo_contagemresultado_datadmnauxdate_Jsonclick ;
      private String edtavDdo_contagemresultado_datadmnauxdateto_Internalname ;
      private String edtavDdo_contagemresultado_datadmnauxdateto_Jsonclick ;
      private String edtavTfcontagemresultado_horaentrega_Internalname ;
      private String edtavTfcontagemresultado_horaentrega_Jsonclick ;
      private String edtavTfcontagemresultado_horaentrega_to_Internalname ;
      private String edtavTfcontagemresultado_horaentrega_to_Jsonclick ;
      private String divDdo_contagemresultado_horaentregaauxdates_Internalname ;
      private String edtavDdo_contagemresultado_horaentregaauxdate_Internalname ;
      private String edtavDdo_contagemresultado_horaentregaauxdate_Jsonclick ;
      private String edtavDdo_contagemresultado_horaentregaauxdateto_Internalname ;
      private String edtavDdo_contagemresultado_horaentregaauxdateto_Jsonclick ;
      private String edtavTfcontagemresultado_contratadapessoanom_Internalname ;
      private String edtavTfcontagemresultado_contratadapessoanom_Jsonclick ;
      private String edtavTfcontagemresultado_contratadapessoanom_sel_Internalname ;
      private String edtavTfcontagemresultado_contratadapessoanom_sel_Jsonclick ;
      private String edtavTfcontagemresultado_demandafm_Internalname ;
      private String edtavTfcontagemresultado_demandafm_Jsonclick ;
      private String edtavTfcontagemresultado_demandafm_sel_Internalname ;
      private String edtavTfcontagemresultado_demandafm_sel_Jsonclick ;
      private String edtavTfcontagemresultado_servicosigla_Internalname ;
      private String edtavTfcontagemresultado_servicosigla_Jsonclick ;
      private String edtavTfcontagemresultado_servicosigla_sel_Internalname ;
      private String edtavTfcontagemresultado_servicosigla_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_contagemresultado_dataentregatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_datadmntitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_horaentregatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_contratadapessoanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_servicosiglatitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContagemResultado_DataEntrega_Internalname ;
      private String edtContagemResultado_DataDmn_Internalname ;
      private String edtContagemResultado_HoraEntrega_Internalname ;
      private String A500ContagemResultado_ContratadaPessoaNom ;
      private String edtContagemResultado_ContratadaPessoaNom_Internalname ;
      private String edtContagemResultado_DemandaFM_Internalname ;
      private String A801ContagemResultado_ServicoSigla ;
      private String edtContagemResultado_ServicoSigla_Internalname ;
      private String scmdbuf ;
      private String lV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom ;
      private String lV110WP_GridDS_11_Tfcontagemresultado_servicosigla ;
      private String AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel ;
      private String AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom ;
      private String AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel ;
      private String AV110WP_GridDS_11_Tfcontagemresultado_servicosigla ;
      private String subGrid_Internalname ;
      private String Ddo_contagemresultado_dataentrega_Internalname ;
      private String Ddo_contagemresultado_datadmn_Internalname ;
      private String Ddo_contagemresultado_horaentrega_Internalname ;
      private String Ddo_contagemresultado_contratadapessoanom_Internalname ;
      private String Ddo_contagemresultado_demandafm_Internalname ;
      private String Ddo_contagemresultado_servicosigla_Internalname ;
      private String lblTbjava_Internalname ;
      private String edtContagemResultado_DataEntrega_Title ;
      private String edtContagemResultado_DataDmn_Title ;
      private String edtContagemResultado_HoraEntrega_Title ;
      private String edtContagemResultado_ContratadaPessoaNom_Title ;
      private String edtContagemResultado_DemandaFM_Title ;
      private String edtContagemResultado_ServicoSigla_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String lblTbjava_Jsonclick ;
      private String bttBtncerrar_Internalname ;
      private String bttBtncerrar_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContagemresultadotitle_Internalname ;
      private String lblContagemresultadotitle_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String tblTableactions_Internalname ;
      private String sGXsfl_28_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContagemResultado_DataEntrega_Jsonclick ;
      private String edtContagemResultado_DataDmn_Jsonclick ;
      private String edtContagemResultado_HoraEntrega_Jsonclick ;
      private String edtContagemResultado_ContratadaPessoaNom_Jsonclick ;
      private String edtContagemResultado_DemandaFM_Jsonclick ;
      private String edtContagemResultado_ServicoSigla_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private DateTime AV77TFContagemResultado_HoraEntrega ;
      private DateTime AV78TFContagemResultado_HoraEntrega_To ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime AV104WP_GridDS_5_Tfcontagemresultado_horaentrega ;
      private DateTime AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to ;
      private DateTime AV65TFContagemResultado_DataEntrega ;
      private DateTime AV66TFContagemResultado_DataEntrega_To ;
      private DateTime AV71TFContagemResultado_DataDmn ;
      private DateTime AV72TFContagemResultado_DataDmn_To ;
      private DateTime AV67DDO_ContagemResultado_DataEntregaAuxDate ;
      private DateTime AV68DDO_ContagemResultado_DataEntregaAuxDateTo ;
      private DateTime AV73DDO_ContagemResultado_DataDmnAuxDate ;
      private DateTime AV74DDO_ContagemResultado_DataDmnAuxDateTo ;
      private DateTime AV79DDO_ContagemResultado_HoraEntregaAuxDate ;
      private DateTime AV80DDO_ContagemResultado_HoraEntregaAuxDateTo ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime AV100WP_GridDS_1_Tfcontagemresultado_dataentrega ;
      private DateTime AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to ;
      private DateTime AV102WP_GridDS_3_Tfcontagemresultado_datadmn ;
      private DateTime AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contagemresultado_dataentrega_Includesortasc ;
      private bool Ddo_contagemresultado_dataentrega_Includesortdsc ;
      private bool Ddo_contagemresultado_dataentrega_Includefilter ;
      private bool Ddo_contagemresultado_dataentrega_Filterisrange ;
      private bool Ddo_contagemresultado_dataentrega_Includedatalist ;
      private bool Ddo_contagemresultado_datadmn_Includesortasc ;
      private bool Ddo_contagemresultado_datadmn_Includesortdsc ;
      private bool Ddo_contagemresultado_datadmn_Includefilter ;
      private bool Ddo_contagemresultado_datadmn_Filterisrange ;
      private bool Ddo_contagemresultado_datadmn_Includedatalist ;
      private bool Ddo_contagemresultado_horaentrega_Includesortasc ;
      private bool Ddo_contagemresultado_horaentrega_Includesortdsc ;
      private bool Ddo_contagemresultado_horaentrega_Includefilter ;
      private bool Ddo_contagemresultado_horaentrega_Filterisrange ;
      private bool Ddo_contagemresultado_horaentrega_Includedatalist ;
      private bool Ddo_contagemresultado_contratadapessoanom_Includesortasc ;
      private bool Ddo_contagemresultado_contratadapessoanom_Includesortdsc ;
      private bool Ddo_contagemresultado_contratadapessoanom_Includefilter ;
      private bool Ddo_contagemresultado_contratadapessoanom_Filterisrange ;
      private bool Ddo_contagemresultado_contratadapessoanom_Includedatalist ;
      private bool Ddo_contagemresultado_demandafm_Includesortasc ;
      private bool Ddo_contagemresultado_demandafm_Includesortdsc ;
      private bool Ddo_contagemresultado_demandafm_Includefilter ;
      private bool Ddo_contagemresultado_demandafm_Filterisrange ;
      private bool Ddo_contagemresultado_demandafm_Includedatalist ;
      private bool Ddo_contagemresultado_servicosigla_Includesortasc ;
      private bool Ddo_contagemresultado_servicosigla_Includesortdsc ;
      private bool Ddo_contagemresultado_servicosigla_Includefilter ;
      private bool Ddo_contagemresultado_servicosigla_Filterisrange ;
      private bool Ddo_contagemresultado_servicosigla_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n500ContagemResultado_ContratadaPessoaNom ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n499ContagemResultado_ContratadaPessoaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV59Update_IsBlob ;
      private bool AV60Delete_IsBlob ;
      private String AV87TFContagemResultado_DemandaFM ;
      private String AV88TFContagemResultado_DemandaFM_Sel ;
      private String AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace ;
      private String AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace ;
      private String AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace ;
      private String AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace ;
      private String AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace ;
      private String AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace ;
      private String AV112Update_GXI ;
      private String AV113Delete_GXI ;
      private String A493ContagemResultado_DemandaFM ;
      private String lV108WP_GridDS_9_Tfcontagemresultado_demandafm ;
      private String AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel ;
      private String AV108WP_GridDS_9_Tfcontagemresultado_demandafm ;
      private String AV59Update ;
      private String AV60Delete ;
      private IGxSession AV61Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00KV2_A490ContagemResultado_ContratadaCod ;
      private bool[] H00KV2_n490ContagemResultado_ContratadaCod ;
      private int[] H00KV2_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00KV2_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] H00KV2_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00KV2_n1553ContagemResultado_CntSrvCod ;
      private int[] H00KV2_A601ContagemResultado_Servico ;
      private bool[] H00KV2_n601ContagemResultado_Servico ;
      private int[] H00KV2_A456ContagemResultado_Codigo ;
      private String[] H00KV2_A801ContagemResultado_ServicoSigla ;
      private bool[] H00KV2_n801ContagemResultado_ServicoSigla ;
      private String[] H00KV2_A493ContagemResultado_DemandaFM ;
      private bool[] H00KV2_n493ContagemResultado_DemandaFM ;
      private String[] H00KV2_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] H00KV2_n500ContagemResultado_ContratadaPessoaNom ;
      private DateTime[] H00KV2_A912ContagemResultado_HoraEntrega ;
      private bool[] H00KV2_n912ContagemResultado_HoraEntrega ;
      private DateTime[] H00KV2_A471ContagemResultado_DataDmn ;
      private DateTime[] H00KV2_A472ContagemResultado_DataEntrega ;
      private bool[] H00KV2_n472ContagemResultado_DataEntrega ;
      private long[] H00KV3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV64ContagemResultado_DataEntregaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV70ContagemResultado_DataDmnTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV76ContagemResultado_HoraEntregaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV82ContagemResultado_ContratadaPessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV86ContagemResultado_DemandaFMTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV90ContagemResultado_ServicoSiglaTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV94DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wp_grid__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00KV2( IGxContext context ,
                                             DateTime AV100WP_GridDS_1_Tfcontagemresultado_dataentrega ,
                                             DateTime AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to ,
                                             DateTime AV102WP_GridDS_3_Tfcontagemresultado_datadmn ,
                                             DateTime AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to ,
                                             DateTime AV104WP_GridDS_5_Tfcontagemresultado_horaentrega ,
                                             DateTime AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to ,
                                             String AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel ,
                                             String AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom ,
                                             String AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel ,
                                             String AV108WP_GridDS_9_Tfcontagemresultado_demandafm ,
                                             String AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel ,
                                             String AV110WP_GridDS_11_Tfcontagemresultado_servicosigla ,
                                             DateTime A472ContagemResultado_DataEntrega ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A912ContagemResultado_HoraEntrega ,
                                             String A500ContagemResultado_ContratadaPessoaNom ,
                                             String A493ContagemResultado_DemandaFM ,
                                             String A801ContagemResultado_ServicoSigla )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [17] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo], T5.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_DemandaFM], T3.[Pessoa_Nome] AS ContagemResultado_ContratadaPessoaNom, T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataEntrega]";
         sFromString = " FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[Servico_Codigo])";
         sOrderString = "";
         if ( ! (DateTime.MinValue==AV100WP_GridDS_1_Tfcontagemresultado_dataentrega) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntrega] >= @AV100WP_GridDS_1_Tfcontagemresultado_dataentrega)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataEntrega] >= @AV100WP_GridDS_1_Tfcontagemresultado_dataentrega)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ! (DateTime.MinValue==AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntrega] <= @AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataEntrega] <= @AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV102WP_GridDS_3_Tfcontagemresultado_datadmn) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV102WP_GridDS_3_Tfcontagemresultado_datadmn)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] >= @AV102WP_GridDS_3_Tfcontagemresultado_datadmn)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] <= @AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV104WP_GridDS_5_Tfcontagemresultado_horaentrega) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraEntrega] >= @AV104WP_GridDS_5_Tfcontagemresultado_horaentrega)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_HoraEntrega] >= @AV104WP_GridDS_5_Tfcontagemresultado_horaentrega)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraEntrega] <= @AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_HoraEntrega] <= @AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WP_GridDS_9_Tfcontagemresultado_demandafm)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV108WP_GridDS_9_Tfcontagemresultado_demandafm)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DemandaFM] like @lV108WP_GridDS_9_Tfcontagemresultado_demandafm)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DemandaFM] = @AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WP_GridDS_11_Tfcontagemresultado_servicosigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV110WP_GridDS_11_Tfcontagemresultado_servicosigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] like @lV110WP_GridDS_11_Tfcontagemresultado_servicosigla)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] = @AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] = @AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_DataEntrega]";
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00KV3( IGxContext context ,
                                             DateTime AV100WP_GridDS_1_Tfcontagemresultado_dataentrega ,
                                             DateTime AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to ,
                                             DateTime AV102WP_GridDS_3_Tfcontagemresultado_datadmn ,
                                             DateTime AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to ,
                                             DateTime AV104WP_GridDS_5_Tfcontagemresultado_horaentrega ,
                                             DateTime AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to ,
                                             String AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel ,
                                             String AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom ,
                                             String AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel ,
                                             String AV108WP_GridDS_9_Tfcontagemresultado_demandafm ,
                                             String AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel ,
                                             String AV110WP_GridDS_11_Tfcontagemresultado_servicosigla ,
                                             DateTime A472ContagemResultado_DataEntrega ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A912ContagemResultado_HoraEntrega ,
                                             String A500ContagemResultado_ContratadaPessoaNom ,
                                             String A493ContagemResultado_DemandaFM ,
                                             String A801ContagemResultado_ServicoSigla )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [12] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[Servico_Codigo])";
         if ( ! (DateTime.MinValue==AV100WP_GridDS_1_Tfcontagemresultado_dataentrega) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntrega] >= @AV100WP_GridDS_1_Tfcontagemresultado_dataentrega)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataEntrega] >= @AV100WP_GridDS_1_Tfcontagemresultado_dataentrega)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ! (DateTime.MinValue==AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntrega] <= @AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataEntrega] <= @AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV102WP_GridDS_3_Tfcontagemresultado_datadmn) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV102WP_GridDS_3_Tfcontagemresultado_datadmn)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] >= @AV102WP_GridDS_3_Tfcontagemresultado_datadmn)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] <= @AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV104WP_GridDS_5_Tfcontagemresultado_horaentrega) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraEntrega] >= @AV104WP_GridDS_5_Tfcontagemresultado_horaentrega)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_HoraEntrega] >= @AV104WP_GridDS_5_Tfcontagemresultado_horaentrega)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraEntrega] <= @AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_HoraEntrega] <= @AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WP_GridDS_9_Tfcontagemresultado_demandafm)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV108WP_GridDS_9_Tfcontagemresultado_demandafm)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DemandaFM] like @lV108WP_GridDS_9_Tfcontagemresultado_demandafm)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DemandaFM] = @AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WP_GridDS_11_Tfcontagemresultado_servicosigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV110WP_GridDS_11_Tfcontagemresultado_servicosigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] like @lV110WP_GridDS_11_Tfcontagemresultado_servicosigla)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] = @AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] = @AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + "";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00KV2(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] );
               case 1 :
                     return conditional_H00KV3(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00KV2 ;
          prmH00KV2 = new Object[] {
          new Object[] {"@AV100WP_GridDS_1_Tfcontagemresultado_dataentrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV102WP_GridDS_3_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV104WP_GridDS_5_Tfcontagemresultado_horaentrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to",SqlDbType.DateTime,0,5} ,
          new Object[] {"@lV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV108WP_GridDS_9_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV110WP_GridDS_11_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00KV3 ;
          prmH00KV3 = new Object[] {
          new Object[] {"@AV100WP_GridDS_1_Tfcontagemresultado_dataentrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV101WP_GridDS_2_Tfcontagemresultado_dataentrega_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV102WP_GridDS_3_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV103WP_GridDS_4_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV104WP_GridDS_5_Tfcontagemresultado_horaentrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV105WP_GridDS_6_Tfcontagemresultado_horaentrega_to",SqlDbType.DateTime,0,5} ,
          new Object[] {"@lV106WP_GridDS_7_Tfcontagemresultado_contratadapessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV107WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV108WP_GridDS_9_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV109WP_GridDS_10_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV110WP_GridDS_11_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV111WP_GridDS_12_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00KV2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KV2,11,0,true,false )
             ,new CursorDef("H00KV3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KV3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[15])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(10) ;
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

}
