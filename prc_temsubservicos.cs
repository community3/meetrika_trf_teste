/*
               File: PRC_TemSubServicos
        Description: Tem Sub Servicos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:26.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_temsubservicos : GXProcedure
   {
      public prc_temsubservicos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_temsubservicos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Servico_Codigo ,
                           out bool aP1_TemSubServicos )
      {
         this.A155Servico_Codigo = aP0_Servico_Codigo;
         this.AV8TemSubServicos = false ;
         initialize();
         executePrivate();
         aP0_Servico_Codigo=this.A155Servico_Codigo;
         aP1_TemSubServicos=this.AV8TemSubServicos;
      }

      public bool executeUdp( ref int aP0_Servico_Codigo )
      {
         this.A155Servico_Codigo = aP0_Servico_Codigo;
         this.AV8TemSubServicos = false ;
         initialize();
         executePrivate();
         aP0_Servico_Codigo=this.A155Servico_Codigo;
         aP1_TemSubServicos=this.AV8TemSubServicos;
         return AV8TemSubServicos ;
      }

      public void executeSubmit( ref int aP0_Servico_Codigo ,
                                 out bool aP1_TemSubServicos )
      {
         prc_temsubservicos objprc_temsubservicos;
         objprc_temsubservicos = new prc_temsubservicos();
         objprc_temsubservicos.A155Servico_Codigo = aP0_Servico_Codigo;
         objprc_temsubservicos.AV8TemSubServicos = false ;
         objprc_temsubservicos.context.SetSubmitInitialConfig(context);
         objprc_temsubservicos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_temsubservicos);
         aP0_Servico_Codigo=this.A155Servico_Codigo;
         aP1_TemSubServicos=this.AV8TemSubServicos;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_temsubservicos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P004N2 */
         pr_default.execute(0, new Object[] {A155Servico_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            GXt_int1 = A640Servico_Vinculados;
            new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int1) ;
            A640Servico_Vinculados = GXt_int1;
            AV8TemSubServicos = (bool)((A640Servico_Vinculados>0));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P004N2_A155Servico_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_temsubservicos__default(),
            new Object[][] {
                new Object[] {
               P004N2_A155Servico_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A640Servico_Vinculados ;
      private short GXt_int1 ;
      private int A155Servico_Codigo ;
      private String scmdbuf ;
      private bool AV8TemSubServicos ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Servico_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P004N2_A155Servico_Codigo ;
      private bool aP1_TemSubServicos ;
   }

   public class prc_temsubservicos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004N2 ;
          prmP004N2 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004N2", "SELECT TOP 1 [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004N2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
