/*
               File: PRC_ExisteContratada
        Description: Existe Contratada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:49.44
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_existecontratada : GXProcedure
   {
      public prc_existecontratada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_existecontratada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Contratada_PessoaCNPJ ,
                           ref int aP1_Contratada_AreaTrabalhoCod ,
                           out bool aP2_ExisteContratada )
      {
         this.A42Contratada_PessoaCNPJ = aP0_Contratada_PessoaCNPJ;
         this.A52Contratada_AreaTrabalhoCod = aP1_Contratada_AreaTrabalhoCod;
         this.AV8ExisteContratada = false ;
         initialize();
         executePrivate();
         aP0_Contratada_PessoaCNPJ=this.A42Contratada_PessoaCNPJ;
         aP1_Contratada_AreaTrabalhoCod=this.A52Contratada_AreaTrabalhoCod;
         aP2_ExisteContratada=this.AV8ExisteContratada;
      }

      public bool executeUdp( ref String aP0_Contratada_PessoaCNPJ ,
                              ref int aP1_Contratada_AreaTrabalhoCod )
      {
         this.A42Contratada_PessoaCNPJ = aP0_Contratada_PessoaCNPJ;
         this.A52Contratada_AreaTrabalhoCod = aP1_Contratada_AreaTrabalhoCod;
         this.AV8ExisteContratada = false ;
         initialize();
         executePrivate();
         aP0_Contratada_PessoaCNPJ=this.A42Contratada_PessoaCNPJ;
         aP1_Contratada_AreaTrabalhoCod=this.A52Contratada_AreaTrabalhoCod;
         aP2_ExisteContratada=this.AV8ExisteContratada;
         return AV8ExisteContratada ;
      }

      public void executeSubmit( ref String aP0_Contratada_PessoaCNPJ ,
                                 ref int aP1_Contratada_AreaTrabalhoCod ,
                                 out bool aP2_ExisteContratada )
      {
         prc_existecontratada objprc_existecontratada;
         objprc_existecontratada = new prc_existecontratada();
         objprc_existecontratada.A42Contratada_PessoaCNPJ = aP0_Contratada_PessoaCNPJ;
         objprc_existecontratada.A52Contratada_AreaTrabalhoCod = aP1_Contratada_AreaTrabalhoCod;
         objprc_existecontratada.AV8ExisteContratada = false ;
         objprc_existecontratada.context.SetSubmitInitialConfig(context);
         objprc_existecontratada.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_existecontratada);
         aP0_Contratada_PessoaCNPJ=this.A42Contratada_PessoaCNPJ;
         aP1_Contratada_AreaTrabalhoCod=this.A52Contratada_AreaTrabalhoCod;
         aP2_ExisteContratada=this.AV8ExisteContratada;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_existecontratada)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8ExisteContratada = false;
         /* Using cursor P001T2 */
         pr_default.execute(0, new Object[] {A52Contratada_AreaTrabalhoCod, n42Contratada_PessoaCNPJ, A42Contratada_PessoaCNPJ});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A40Contratada_PessoaCod = P001T2_A40Contratada_PessoaCod[0];
            A43Contratada_Ativo = P001T2_A43Contratada_Ativo[0];
            A39Contratada_Codigo = P001T2_A39Contratada_Codigo[0];
            AV8ExisteContratada = true;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P001T2_A40Contratada_PessoaCod = new int[1] ;
         P001T2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P001T2_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P001T2_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P001T2_A43Contratada_Ativo = new bool[] {false} ;
         P001T2_A39Contratada_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_existecontratada__default(),
            new Object[][] {
                new Object[] {
               P001T2_A40Contratada_PessoaCod, P001T2_A52Contratada_AreaTrabalhoCod, P001T2_A42Contratada_PessoaCNPJ, P001T2_n42Contratada_PessoaCNPJ, P001T2_A43Contratada_Ativo, P001T2_A39Contratada_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A52Contratada_AreaTrabalhoCod ;
      private int A40Contratada_PessoaCod ;
      private int A39Contratada_Codigo ;
      private String scmdbuf ;
      private bool AV8ExisteContratada ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool A43Contratada_Ativo ;
      private String A42Contratada_PessoaCNPJ ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Contratada_PessoaCNPJ ;
      private int aP1_Contratada_AreaTrabalhoCod ;
      private IDataStoreProvider pr_default ;
      private int[] P001T2_A40Contratada_PessoaCod ;
      private int[] P001T2_A52Contratada_AreaTrabalhoCod ;
      private String[] P001T2_A42Contratada_PessoaCNPJ ;
      private bool[] P001T2_n42Contratada_PessoaCNPJ ;
      private bool[] P001T2_A43Contratada_Ativo ;
      private int[] P001T2_A39Contratada_Codigo ;
      private bool aP2_ExisteContratada ;
   }

   public class prc_existecontratada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP001T2 ;
          prmP001T2 = new Object[] {
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_PessoaCNPJ",SqlDbType.VarChar,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P001T2", "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_AreaTrabalhoCod], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[Contratada_Ativo], T1.[Contratada_Codigo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE (T1.[Contratada_AreaTrabalhoCod] = @Contratada_AreaTrabalhoCod) AND (T2.[Pessoa_Docto] = @Contratada_PessoaCNPJ) AND (T1.[Contratada_Ativo] = 1) ORDER BY T1.[Contratada_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001T2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                return;
       }
    }

 }

}
