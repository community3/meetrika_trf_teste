/*
               File: Proposta
        Description: Proposta
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:55.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class proposta : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_9") == 0 )
         {
            A648Projeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n648Projeto_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_9( A648Projeto_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A5AreaTrabalho_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A1686Proposta_OSCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1686Proposta_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1686Proposta_OSCodigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A1686Proposta_OSCodigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A1705Proposta_OSServico = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1705Proposta_OSServico = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1705Proposta_OSServico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1705Proposta_OSServico), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A1705Proposta_OSServico) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A1713Proposta_OSUndCntCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1713Proposta_OSUndCntCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1713Proposta_OSUndCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1713Proposta_OSUndCntCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A1713Proposta_OSUndCntCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
         {
            A1686Proposta_OSCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1686Proposta_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1686Proposta_OSCodigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_13( A1686Proposta_OSCodigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_14") == 0 )
         {
            A1686Proposta_OSCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1686Proposta_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1686Proposta_OSCodigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_14( A1686Proposta_OSCodigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_15") == 0 )
         {
            A1686Proposta_OSCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1686Proposta_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1686Proposta_OSCodigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_15( A1686Proposta_OSCodigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_16") == 0 )
         {
            A1686Proposta_OSCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1686Proposta_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1686Proposta_OSCodigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_16( A1686Proposta_OSCodigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Proposta_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Proposta_Codigo), 9, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynProjeto_Codigo.Name = "PROJETO_CODIGO";
         dynProjeto_Codigo.WebTags = "";
         dynProjeto_Codigo.removeAllItems();
         /* Using cursor T004817 */
         pr_default.execute(11);
         while ( (pr_default.getStatus(11) != 101) )
         {
            dynProjeto_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T004817_A648Projeto_Codigo[0]), 6, 0)), T004817_A650Projeto_Sigla[0], 0);
            pr_default.readNext(11);
         }
         pr_default.close(11);
         if ( dynProjeto_Codigo.ItemCount > 0 )
         {
            A648Projeto_Codigo = (int)(NumberUtil.Val( dynProjeto_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0))), "."));
            n648Projeto_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         }
         chkProposta_Ativo.Name = "PROPOSTA_ATIVO";
         chkProposta_Ativo.WebTags = "";
         chkProposta_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkProposta_Ativo_Internalname, "TitleCaption", chkProposta_Ativo.Caption);
         chkProposta_Ativo.CheckedValue = "false";
         cmbProposta_OSPrzTpDias.Name = "PROPOSTA_OSPRZTPDIAS";
         cmbProposta_OSPrzTpDias.WebTags = "";
         cmbProposta_OSPrzTpDias.addItem("", "(Nenhum)", 0);
         cmbProposta_OSPrzTpDias.addItem("U", "�teis", 0);
         cmbProposta_OSPrzTpDias.addItem("C", "Corridos", 0);
         if ( cmbProposta_OSPrzTpDias.ItemCount > 0 )
         {
            A1707Proposta_OSPrzTpDias = cmbProposta_OSPrzTpDias.getValidValue(A1707Proposta_OSPrzTpDias);
            n1707Proposta_OSPrzTpDias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1707Proposta_OSPrzTpDias", A1707Proposta_OSPrzTpDias);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Proposta", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtProposta_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public proposta( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public proposta( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Gx_mode ,
                           ref int aP1_Proposta_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Proposta_Codigo = aP1_Proposta_Codigo;
         executePrivate();
         aP0_Gx_mode=this.Gx_mode;
         aP1_Proposta_Codigo=this.AV7Proposta_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynProjeto_Codigo = new GXCombobox();
         chkProposta_Ativo = new GXCheckbox();
         cmbProposta_OSPrzTpDias = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynProjeto_Codigo.ItemCount > 0 )
         {
            A648Projeto_Codigo = (int)(NumberUtil.Val( dynProjeto_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0))), "."));
            n648Projeto_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         }
         if ( cmbProposta_OSPrzTpDias.ItemCount > 0 )
         {
            A1707Proposta_OSPrzTpDias = cmbProposta_OSPrzTpDias.getValidValue(A1707Proposta_OSPrzTpDias);
            n1707Proposta_OSPrzTpDias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1707Proposta_OSPrzTpDias", A1707Proposta_OSPrzTpDias);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_48187( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_48187e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_48187( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_48187( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_48187e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_139_48187( true) ;
         }
         return  ;
      }

      protected void wb_table3_139_48187e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_48187e( true) ;
         }
         else
         {
            wb_table1_2_48187e( false) ;
         }
      }

      protected void wb_table3_139_48187( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_139_48187e( true) ;
         }
         else
         {
            wb_table3_139_48187e( false) ;
         }
      }

      protected void wb_table2_5_48187( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_48187( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_48187e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_48187e( true) ;
         }
         else
         {
            wb_table2_5_48187e( false) ;
         }
      }

      protected void wb_table4_13_48187( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_codigo_Internalname, "Proposta_Codigo", "", "", lblTextblockproposta_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProposta_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1685Proposta_Codigo), 9, 0, ",", "")), ((edtProposta_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1685Proposta_Codigo), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1685Proposta_Codigo), "ZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProposta_Codigo_Enabled, 0, "text", "", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_codigo_Internalname, "Projeto", "", "", lblTextblockprojeto_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynProjeto_Codigo, dynProjeto_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)), 1, dynProjeto_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynProjeto_Codigo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", true, "HLP_Proposta.htm");
            dynProjeto_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_Codigo_Internalname, "Values", (String)(dynProjeto_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_vigencia_Internalname, "Proposta_Vigencia", "", "", lblTextblockproposta_vigencia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtProposta_Vigencia_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtProposta_Vigencia_Internalname, context.localUtil.TToC( A1687Proposta_Vigencia, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1687Proposta_Vigencia, "99/99/9999 99:99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',8,24,'por',false,0);"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_Vigencia_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtProposta_Vigencia_Enabled, 0, "text", "", 19, "chr", 1, "row", 19, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Proposta.htm");
            GxWebStd.gx_bitmap( context, edtProposta_Vigencia_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtProposta_Vigencia_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Proposta.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_valor_Internalname, "Proposta_Valor", "", "", lblTextblockproposta_valor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProposta_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A1688Proposta_Valor, 16, 4, ",", "")), ((edtProposta_Valor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1688Proposta_Valor, "Z,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1688Proposta_Valor, "Z,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','4');"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_Valor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProposta_Valor_Enabled, 0, "text", "", 16, "chr", 1, "row", 16, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_status_Internalname, "Proposta_Status", "", "", lblTextblockproposta_status_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProposta_Status_Internalname, StringUtil.RTrim( A1689Proposta_Status), StringUtil.RTrim( context.localUtil.Format( A1689Proposta_Status, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_Status_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProposta_Status_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_codigo_Internalname, "C�digo Area de Trabalho", "", "", lblTextblockareatrabalho_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAreaTrabalho_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")), ((edtAreaTrabalho_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAreaTrabalho_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAreaTrabalho_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_objetivo_Internalname, "Proposta_Objetivo", "", "", lblTextblockproposta_objetivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtProposta_Objetivo_Internalname, A1690Proposta_Objetivo, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", 0, 1, edtProposta_Objetivo_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2048", -1, "", "", -1, true, "", "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_escopo_Internalname, "Proposta_Escopo", "", "", lblTextblockproposta_escopo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtProposta_Escopo_Internalname, A1691Proposta_Escopo, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", 0, 1, edtProposta_Escopo_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2048", -1, "", "", -1, true, "", "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_oportunidade_Internalname, "Proposta_Oportunidade", "", "", lblTextblockproposta_oportunidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtProposta_Oportunidade_Internalname, A1692Proposta_Oportunidade, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", 0, 1, edtProposta_Oportunidade_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2048", -1, "", "", -1, true, "", "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_perspectiva_Internalname, "Proposta_Perspectiva", "", "", lblTextblockproposta_perspectiva_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtProposta_Perspectiva_Internalname, A1693Proposta_Perspectiva, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", 0, 1, edtProposta_Perspectiva_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2048", -1, "", "", -1, true, "", "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_restricaoimplantacao_Internalname, "Proposta_Restricao Implantacao", "", "", lblTextblockproposta_restricaoimplantacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtProposta_RestricaoImplantacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtProposta_RestricaoImplantacao_Internalname, context.localUtil.TToC( A1694Proposta_RestricaoImplantacao, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1694Proposta_RestricaoImplantacao, "99/99/9999 99:99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',8,24,'por',false,0);"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_RestricaoImplantacao_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtProposta_RestricaoImplantacao_Enabled, 0, "text", "", 19, "chr", 1, "row", 19, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Proposta.htm");
            GxWebStd.gx_bitmap( context, edtProposta_RestricaoImplantacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtProposta_RestricaoImplantacao_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Proposta.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_restricaocusto_Internalname, "Proposta_Restricao Custo", "", "", lblTextblockproposta_restricaocusto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProposta_RestricaoCusto_Internalname, StringUtil.LTrim( StringUtil.NToC( A1695Proposta_RestricaoCusto, 15, 4, ",", "")), ((edtProposta_RestricaoCusto_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1695Proposta_RestricaoCusto, "ZZZZZZZZZ9.9999")) : context.localUtil.Format( A1695Proposta_RestricaoCusto, "ZZZZZZZZZ9.9999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','4');"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_RestricaoCusto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProposta_RestricaoCusto_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_ativo_Internalname, "Proposta_Ativo", "", "", lblTextblockproposta_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkProposta_Ativo_Internalname, StringUtil.BoolToStr( A1696Proposta_Ativo), "", "", 1, chkProposta_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(78, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_prestatoracod_Internalname, "Proposta_Prestatora Cod", "", "", lblTextblockproposta_prestatoracod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_prestadorarazsocial_Internalname, "Proposta_Prestadora Raz Social", "", "", lblTextblockproposta_prestadorarazsocial_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_contratadacod_Internalname, "Proposta_Contratada Cod", "", "", lblTextblockproposta_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProposta_ContratadaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1699Proposta_ContratadaCod), 6, 0, ",", "")), ((edtProposta_ContratadaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1699Proposta_ContratadaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1699Proposta_ContratadaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_ContratadaCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProposta_ContratadaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_contratadarazsocial_Internalname, "Proposta_Contratada Raz Social", "", "", lblTextblockproposta_contratadarazsocial_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProposta_ContratadaRazSocial_Internalname, A1700Proposta_ContratadaRazSocial, StringUtil.RTrim( context.localUtil.Format( A1700Proposta_ContratadaRazSocial, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_ContratadaRazSocial_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProposta_ContratadaRazSocial_Enabled, 0, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_prazo_Internalname, "Prazo", "", "", lblTextblockproposta_prazo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtProposta_Prazo_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtProposta_Prazo_Internalname, context.localUtil.Format(A1702Proposta_Prazo, "99/99/99"), context.localUtil.Format( A1702Proposta_Prazo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_Prazo_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtProposta_Prazo_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Proposta.htm");
            GxWebStd.gx_bitmap( context, edtProposta_Prazo_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtProposta_Prazo_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Proposta.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_esforco_Internalname, "Esforco", "", "", lblTextblockproposta_esforco_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProposta_Esforco_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1703Proposta_Esforco), 8, 0, ",", "")), ((edtProposta_Esforco_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1703Proposta_Esforco), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1703Proposta_Esforco), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_Esforco_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProposta_Esforco_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_cntsrvcod_Internalname, "proposto", "", "", lblTextblockproposta_cntsrvcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProposta_CntSrvCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1704Proposta_CntSrvCod), 6, 0, ",", "")), ((edtProposta_CntSrvCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1704Proposta_CntSrvCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1704Proposta_CntSrvCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_CntSrvCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProposta_CntSrvCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_oscodigo_Internalname, "Proposta_OSCodigo", "", "", lblTextblockproposta_oscodigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProposta_OSCodigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1686Proposta_OSCodigo), 6, 0, ",", "")), ((edtProposta_OSCodigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1686Proposta_OSCodigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1686Proposta_OSCodigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_OSCodigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProposta_OSCodigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_osservico_Internalname, "Proposta_OSServico", "", "", lblTextblockproposta_osservico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProposta_OSServico_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1705Proposta_OSServico), 6, 0, ",", "")), ((edtProposta_OSServico_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1705Proposta_OSServico), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1705Proposta_OSServico), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_OSServico_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProposta_OSServico_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_osdmnfm_Internalname, "FM", "", "", lblTextblockproposta_osdmnfm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProposta_OSDmnFM_Internalname, A1711Proposta_OSDmnFM, StringUtil.RTrim( context.localUtil.Format( A1711Proposta_OSDmnFM, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_OSDmnFM_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProposta_OSDmnFM_Enabled, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_osundcntsgl_Internalname, "Cnt Sgl", "", "", lblTextblockproposta_osundcntsgl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProposta_OSUndCntSgl_Internalname, StringUtil.RTrim( A1710Proposta_OSUndCntSgl), StringUtil.RTrim( context.localUtil.Format( A1710Proposta_OSUndCntSgl, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_OSUndCntSgl_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProposta_OSUndCntSgl_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_osprztpdias_Internalname, "Tp Dias", "", "", lblTextblockproposta_osprztpdias_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Proposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbProposta_OSPrzTpDias, cmbProposta_OSPrzTpDias_Internalname, StringUtil.RTrim( A1707Proposta_OSPrzTpDias), 1, cmbProposta_OSPrzTpDias_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbProposta_OSPrzTpDias.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_Proposta.htm");
            cmbProposta_OSPrzTpDias.CurrentValue = StringUtil.RTrim( A1707Proposta_OSPrzTpDias);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbProposta_OSPrzTpDias_Internalname, "Values", (String)(cmbProposta_OSPrzTpDias.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_48187e( true) ;
         }
         else
         {
            wb_table4_13_48187e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11482 */
         E11482 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtProposta_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProposta_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROPOSTA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtProposta_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1685Proposta_Codigo = 0;
                  n1685Proposta_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1685Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1685Proposta_Codigo), 9, 0)));
               }
               else
               {
                  A1685Proposta_Codigo = (int)(context.localUtil.CToN( cgiGet( edtProposta_Codigo_Internalname), ",", "."));
                  n1685Proposta_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1685Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1685Proposta_Codigo), 9, 0)));
               }
               dynProjeto_Codigo.CurrentValue = cgiGet( dynProjeto_Codigo_Internalname);
               A648Projeto_Codigo = (int)(NumberUtil.Val( cgiGet( dynProjeto_Codigo_Internalname), "."));
               n648Projeto_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
               n648Projeto_Codigo = ((0==A648Projeto_Codigo) ? true : false);
               if ( context.localUtil.VCDateTime( cgiGet( edtProposta_Vigencia_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Proposta_Vigencia"}), 1, "PROPOSTA_VIGENCIA");
                  AnyError = 1;
                  GX_FocusControl = edtProposta_Vigencia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1687Proposta_Vigencia = (DateTime)(DateTime.MinValue);
                  n1687Proposta_Vigencia = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1687Proposta_Vigencia", context.localUtil.TToC( A1687Proposta_Vigencia, 10, 8, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1687Proposta_Vigencia = context.localUtil.CToT( cgiGet( edtProposta_Vigencia_Internalname));
                  n1687Proposta_Vigencia = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1687Proposta_Vigencia", context.localUtil.TToC( A1687Proposta_Vigencia, 10, 8, 0, 3, "/", ":", " "));
               }
               n1687Proposta_Vigencia = ((DateTime.MinValue==A1687Proposta_Vigencia) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtProposta_Valor_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProposta_Valor_Internalname), ",", ".") > 9999999999.9999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROPOSTA_VALOR");
                  AnyError = 1;
                  GX_FocusControl = edtProposta_Valor_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1688Proposta_Valor = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1688Proposta_Valor", StringUtil.LTrim( StringUtil.Str( A1688Proposta_Valor, 15, 4)));
               }
               else
               {
                  A1688Proposta_Valor = context.localUtil.CToN( cgiGet( edtProposta_Valor_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1688Proposta_Valor", StringUtil.LTrim( StringUtil.Str( A1688Proposta_Valor, 15, 4)));
               }
               A1689Proposta_Status = cgiGet( edtProposta_Status_Internalname);
               n1689Proposta_Status = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1689Proposta_Status", A1689Proposta_Status);
               n1689Proposta_Status = (String.IsNullOrEmpty(StringUtil.RTrim( A1689Proposta_Status)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtAreaTrabalho_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAreaTrabalho_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "AREATRABALHO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A5AreaTrabalho_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               }
               else
               {
                  A5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               }
               A1690Proposta_Objetivo = cgiGet( edtProposta_Objetivo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1690Proposta_Objetivo", A1690Proposta_Objetivo);
               A1691Proposta_Escopo = cgiGet( edtProposta_Escopo_Internalname);
               n1691Proposta_Escopo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1691Proposta_Escopo", A1691Proposta_Escopo);
               n1691Proposta_Escopo = (String.IsNullOrEmpty(StringUtil.RTrim( A1691Proposta_Escopo)) ? true : false);
               A1692Proposta_Oportunidade = cgiGet( edtProposta_Oportunidade_Internalname);
               n1692Proposta_Oportunidade = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1692Proposta_Oportunidade", A1692Proposta_Oportunidade);
               n1692Proposta_Oportunidade = (String.IsNullOrEmpty(StringUtil.RTrim( A1692Proposta_Oportunidade)) ? true : false);
               A1693Proposta_Perspectiva = cgiGet( edtProposta_Perspectiva_Internalname);
               n1693Proposta_Perspectiva = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1693Proposta_Perspectiva", A1693Proposta_Perspectiva);
               n1693Proposta_Perspectiva = (String.IsNullOrEmpty(StringUtil.RTrim( A1693Proposta_Perspectiva)) ? true : false);
               if ( context.localUtil.VCDateTime( cgiGet( edtProposta_RestricaoImplantacao_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Proposta_Restricao Implantacao"}), 1, "PROPOSTA_RESTRICAOIMPLANTACAO");
                  AnyError = 1;
                  GX_FocusControl = edtProposta_RestricaoImplantacao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1694Proposta_RestricaoImplantacao = (DateTime)(DateTime.MinValue);
                  n1694Proposta_RestricaoImplantacao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1694Proposta_RestricaoImplantacao", context.localUtil.TToC( A1694Proposta_RestricaoImplantacao, 10, 8, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1694Proposta_RestricaoImplantacao = context.localUtil.CToT( cgiGet( edtProposta_RestricaoImplantacao_Internalname));
                  n1694Proposta_RestricaoImplantacao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1694Proposta_RestricaoImplantacao", context.localUtil.TToC( A1694Proposta_RestricaoImplantacao, 10, 8, 0, 3, "/", ":", " "));
               }
               n1694Proposta_RestricaoImplantacao = ((DateTime.MinValue==A1694Proposta_RestricaoImplantacao) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtProposta_RestricaoCusto_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProposta_RestricaoCusto_Internalname), ",", ".") > 9999999999.9999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROPOSTA_RESTRICAOCUSTO");
                  AnyError = 1;
                  GX_FocusControl = edtProposta_RestricaoCusto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1695Proposta_RestricaoCusto = 0;
                  n1695Proposta_RestricaoCusto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1695Proposta_RestricaoCusto", StringUtil.LTrim( StringUtil.Str( A1695Proposta_RestricaoCusto, 15, 4)));
               }
               else
               {
                  A1695Proposta_RestricaoCusto = context.localUtil.CToN( cgiGet( edtProposta_RestricaoCusto_Internalname), ",", ".");
                  n1695Proposta_RestricaoCusto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1695Proposta_RestricaoCusto", StringUtil.LTrim( StringUtil.Str( A1695Proposta_RestricaoCusto, 15, 4)));
               }
               n1695Proposta_RestricaoCusto = ((Convert.ToDecimal(0)==A1695Proposta_RestricaoCusto) ? true : false);
               A1696Proposta_Ativo = StringUtil.StrToBool( cgiGet( chkProposta_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1696Proposta_Ativo", A1696Proposta_Ativo);
               if ( ( ( context.localUtil.CToN( cgiGet( edtProposta_ContratadaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProposta_ContratadaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROPOSTA_CONTRATADACOD");
                  AnyError = 1;
                  GX_FocusControl = edtProposta_ContratadaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1699Proposta_ContratadaCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1699Proposta_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1699Proposta_ContratadaCod), 6, 0)));
               }
               else
               {
                  A1699Proposta_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtProposta_ContratadaCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1699Proposta_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1699Proposta_ContratadaCod), 6, 0)));
               }
               A1700Proposta_ContratadaRazSocial = cgiGet( edtProposta_ContratadaRazSocial_Internalname);
               n1700Proposta_ContratadaRazSocial = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1700Proposta_ContratadaRazSocial", A1700Proposta_ContratadaRazSocial);
               n1700Proposta_ContratadaRazSocial = (String.IsNullOrEmpty(StringUtil.RTrim( A1700Proposta_ContratadaRazSocial)) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtProposta_Prazo_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Prazo"}), 1, "PROPOSTA_PRAZO");
                  AnyError = 1;
                  GX_FocusControl = edtProposta_Prazo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1702Proposta_Prazo = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1702Proposta_Prazo", context.localUtil.Format(A1702Proposta_Prazo, "99/99/99"));
               }
               else
               {
                  A1702Proposta_Prazo = context.localUtil.CToD( cgiGet( edtProposta_Prazo_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1702Proposta_Prazo", context.localUtil.Format(A1702Proposta_Prazo, "99/99/99"));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtProposta_Esforco_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProposta_Esforco_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROPOSTA_ESFORCO");
                  AnyError = 1;
                  GX_FocusControl = edtProposta_Esforco_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1703Proposta_Esforco = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1703Proposta_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A1703Proposta_Esforco), 8, 0)));
               }
               else
               {
                  A1703Proposta_Esforco = (int)(context.localUtil.CToN( cgiGet( edtProposta_Esforco_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1703Proposta_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A1703Proposta_Esforco), 8, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtProposta_CntSrvCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProposta_CntSrvCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROPOSTA_CNTSRVCOD");
                  AnyError = 1;
                  GX_FocusControl = edtProposta_CntSrvCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1704Proposta_CntSrvCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1704Proposta_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1704Proposta_CntSrvCod), 6, 0)));
               }
               else
               {
                  A1704Proposta_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtProposta_CntSrvCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1704Proposta_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1704Proposta_CntSrvCod), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtProposta_OSCodigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProposta_OSCodigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROPOSTA_OSCODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtProposta_OSCodigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1686Proposta_OSCodigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1686Proposta_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1686Proposta_OSCodigo), 6, 0)));
               }
               else
               {
                  A1686Proposta_OSCodigo = (int)(context.localUtil.CToN( cgiGet( edtProposta_OSCodigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1686Proposta_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1686Proposta_OSCodigo), 6, 0)));
               }
               A1705Proposta_OSServico = (int)(context.localUtil.CToN( cgiGet( edtProposta_OSServico_Internalname), ",", "."));
               n1705Proposta_OSServico = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1705Proposta_OSServico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1705Proposta_OSServico), 6, 0)));
               A1711Proposta_OSDmnFM = cgiGet( edtProposta_OSDmnFM_Internalname);
               n1711Proposta_OSDmnFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1711Proposta_OSDmnFM", A1711Proposta_OSDmnFM);
               A1710Proposta_OSUndCntSgl = StringUtil.Upper( cgiGet( edtProposta_OSUndCntSgl_Internalname));
               n1710Proposta_OSUndCntSgl = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1710Proposta_OSUndCntSgl", A1710Proposta_OSUndCntSgl);
               cmbProposta_OSPrzTpDias.CurrentValue = cgiGet( cmbProposta_OSPrzTpDias_Internalname);
               A1707Proposta_OSPrzTpDias = cgiGet( cmbProposta_OSPrzTpDias_Internalname);
               n1707Proposta_OSPrzTpDias = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1707Proposta_OSPrzTpDias", A1707Proposta_OSPrzTpDias);
               /* Read saved values. */
               Z1685Proposta_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1685Proposta_Codigo"), ",", "."));
               Z1687Proposta_Vigencia = context.localUtil.CToT( cgiGet( "Z1687Proposta_Vigencia"), 0);
               n1687Proposta_Vigencia = ((DateTime.MinValue==A1687Proposta_Vigencia) ? true : false);
               Z1688Proposta_Valor = context.localUtil.CToN( cgiGet( "Z1688Proposta_Valor"), ",", ".");
               Z1689Proposta_Status = cgiGet( "Z1689Proposta_Status");
               n1689Proposta_Status = (String.IsNullOrEmpty(StringUtil.RTrim( A1689Proposta_Status)) ? true : false);
               Z1694Proposta_RestricaoImplantacao = context.localUtil.CToT( cgiGet( "Z1694Proposta_RestricaoImplantacao"), 0);
               n1694Proposta_RestricaoImplantacao = ((DateTime.MinValue==A1694Proposta_RestricaoImplantacao) ? true : false);
               Z1695Proposta_RestricaoCusto = context.localUtil.CToN( cgiGet( "Z1695Proposta_RestricaoCusto"), ",", ".");
               n1695Proposta_RestricaoCusto = ((Convert.ToDecimal(0)==A1695Proposta_RestricaoCusto) ? true : false);
               Z1696Proposta_Ativo = StringUtil.StrToBool( cgiGet( "Z1696Proposta_Ativo"));
               Z1699Proposta_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( "Z1699Proposta_ContratadaCod"), ",", "."));
               Z1700Proposta_ContratadaRazSocial = cgiGet( "Z1700Proposta_ContratadaRazSocial");
               n1700Proposta_ContratadaRazSocial = (String.IsNullOrEmpty(StringUtil.RTrim( A1700Proposta_ContratadaRazSocial)) ? true : false);
               Z1702Proposta_Prazo = context.localUtil.CToD( cgiGet( "Z1702Proposta_Prazo"), 0);
               Z1793Proposta_PrazoDias = (short)(context.localUtil.CToN( cgiGet( "Z1793Proposta_PrazoDias"), ",", "."));
               n1793Proposta_PrazoDias = ((0==A1793Proposta_PrazoDias) ? true : false);
               Z1703Proposta_Esforco = (int)(context.localUtil.CToN( cgiGet( "Z1703Proposta_Esforco"), ",", "."));
               Z1794Proposta_Liquido = (int)(context.localUtil.CToN( cgiGet( "Z1794Proposta_Liquido"), ",", "."));
               n1794Proposta_Liquido = ((0==A1794Proposta_Liquido) ? true : false);
               Z1704Proposta_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "Z1704Proposta_CntSrvCod"), ",", "."));
               Z1722Proposta_ValorUndCnt = context.localUtil.CToN( cgiGet( "Z1722Proposta_ValorUndCnt"), ",", ".");
               Z5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z5AreaTrabalho_Codigo"), ",", "."));
               Z648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z648Projeto_Codigo"), ",", "."));
               n648Projeto_Codigo = ((0==A648Projeto_Codigo) ? true : false);
               Z1686Proposta_OSCodigo = (int)(context.localUtil.CToN( cgiGet( "Z1686Proposta_OSCodigo"), ",", "."));
               A1793Proposta_PrazoDias = (short)(context.localUtil.CToN( cgiGet( "Z1793Proposta_PrazoDias"), ",", "."));
               n1793Proposta_PrazoDias = false;
               n1793Proposta_PrazoDias = ((0==A1793Proposta_PrazoDias) ? true : false);
               A1794Proposta_Liquido = (int)(context.localUtil.CToN( cgiGet( "Z1794Proposta_Liquido"), ",", "."));
               n1794Proposta_Liquido = false;
               n1794Proposta_Liquido = ((0==A1794Proposta_Liquido) ? true : false);
               A1722Proposta_ValorUndCnt = context.localUtil.CToN( cgiGet( "Z1722Proposta_ValorUndCnt"), ",", ".");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7Proposta_Codigo = (int)(context.localUtil.CToN( cgiGet( "vPROPOSTA_CODIGO"), ",", "."));
               A1793Proposta_PrazoDias = (short)(context.localUtil.CToN( cgiGet( "PROPOSTA_PRAZODIAS"), ",", "."));
               n1793Proposta_PrazoDias = ((0==A1793Proposta_PrazoDias) ? true : false);
               A1794Proposta_Liquido = (int)(context.localUtil.CToN( cgiGet( "PROPOSTA_LIQUIDO"), ",", "."));
               n1794Proposta_Liquido = ((0==A1794Proposta_Liquido) ? true : false);
               A1722Proposta_ValorUndCnt = context.localUtil.CToN( cgiGet( "PROPOSTA_VALORUNDCNT"), ",", ".");
               A1764Proposta_OSDmn = cgiGet( "PROPOSTA_OSDMN");
               n1764Proposta_OSDmn = false;
               A1792Proposta_OSInicio = context.localUtil.CToD( cgiGet( "PROPOSTA_OSINICIO"), 0);
               n1792Proposta_OSInicio = false;
               A1721Proposta_OSPrzRsp = (short)(context.localUtil.CToN( cgiGet( "PROPOSTA_OSPRZRSP"), ",", "."));
               n1721Proposta_OSPrzRsp = false;
               A1763Proposta_OSCntSrvFtrm = cgiGet( "PROPOSTA_OSCNTSRVFTRM");
               n1763Proposta_OSCntSrvFtrm = false;
               A1715Proposta_OSCntCod = (int)(context.localUtil.CToN( cgiGet( "PROPOSTA_OSCNTCOD"), ",", "."));
               n1715Proposta_OSCntCod = false;
               A1713Proposta_OSUndCntCod = (int)(context.localUtil.CToN( cgiGet( "PROPOSTA_OSUNDCNTCOD"), ",", "."));
               A1717Proposta_OSPFB = context.localUtil.CToN( cgiGet( "PROPOSTA_OSPFB"), ",", ".");
               n1717Proposta_OSPFB = false;
               A1718Proposta_OSPFL = context.localUtil.CToN( cgiGet( "PROPOSTA_OSPFL"), ",", ".");
               n1718Proposta_OSPFL = false;
               A1719Proposta_OSDataCnt = context.localUtil.CToD( cgiGet( "PROPOSTA_OSDATACNT"), 0);
               n1719Proposta_OSDataCnt = false;
               A1720Proposta_OSHoraCnt = cgiGet( "PROPOSTA_OSHORACNT");
               n1720Proposta_OSHoraCnt = false;
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Proposta";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1794Proposta_Liquido), "ZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1722Proposta_ValorUndCnt, "Z,ZZZ,ZZ9.99");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1685Proposta_Codigo != Z1685Proposta_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("proposta:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("proposta:[SecurityCheckFailed value for]"+"Proposta_PrazoDias:"+context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9"));
                  GXUtil.WriteLog("proposta:[SecurityCheckFailed value for]"+"Proposta_Liquido:"+context.localUtil.Format( (decimal)(A1794Proposta_Liquido), "ZZZZZZZ9"));
                  GXUtil.WriteLog("proposta:[SecurityCheckFailed value for]"+"Proposta_ValorUndCnt:"+context.localUtil.Format( A1722Proposta_ValorUndCnt, "Z,ZZZ,ZZ9.99"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1685Proposta_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1685Proposta_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1685Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1685Proposta_Codigo), 9, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode187 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     Gx_mode = sMode187;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound187 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_480( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "PROPOSTA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtProposta_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11482 */
                           E11482 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12482 */
                           E12482 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12482 */
            E12482 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll48187( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes48187( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_480( )
      {
         BeforeValidate48187( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls48187( ) ;
            }
            else
            {
               CheckExtendedTable48187( ) ;
               CloseExtendedTableCursors48187( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption480( )
      {
      }

      protected void E11482( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "AreaTrabalho_Codigo") == 0 )
               {
                  AV11Insert_AreaTrabalho_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_AreaTrabalho_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Proposta_OSCodigo") == 0 )
               {
                  AV12Insert_Proposta_OSCodigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Proposta_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Proposta_OSCodigo), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
      }

      protected void E12482( )
      {
         /* After Trn Routine */
         context.setWebReturnParms(new Object[] {(String)Gx_mode,(int)AV7Proposta_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM48187( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1687Proposta_Vigencia = T00483_A1687Proposta_Vigencia[0];
               Z1688Proposta_Valor = T00483_A1688Proposta_Valor[0];
               Z1689Proposta_Status = T00483_A1689Proposta_Status[0];
               Z1694Proposta_RestricaoImplantacao = T00483_A1694Proposta_RestricaoImplantacao[0];
               Z1695Proposta_RestricaoCusto = T00483_A1695Proposta_RestricaoCusto[0];
               Z1696Proposta_Ativo = T00483_A1696Proposta_Ativo[0];
               Z1699Proposta_ContratadaCod = T00483_A1699Proposta_ContratadaCod[0];
               Z1700Proposta_ContratadaRazSocial = T00483_A1700Proposta_ContratadaRazSocial[0];
               Z1702Proposta_Prazo = T00483_A1702Proposta_Prazo[0];
               Z1793Proposta_PrazoDias = T00483_A1793Proposta_PrazoDias[0];
               Z1703Proposta_Esforco = T00483_A1703Proposta_Esforco[0];
               Z1794Proposta_Liquido = T00483_A1794Proposta_Liquido[0];
               Z1704Proposta_CntSrvCod = T00483_A1704Proposta_CntSrvCod[0];
               Z1722Proposta_ValorUndCnt = T00483_A1722Proposta_ValorUndCnt[0];
               Z5AreaTrabalho_Codigo = T00483_A5AreaTrabalho_Codigo[0];
               Z648Projeto_Codigo = T00483_A648Projeto_Codigo[0];
               Z1686Proposta_OSCodigo = T00483_A1686Proposta_OSCodigo[0];
            }
            else
            {
               Z1687Proposta_Vigencia = A1687Proposta_Vigencia;
               Z1688Proposta_Valor = A1688Proposta_Valor;
               Z1689Proposta_Status = A1689Proposta_Status;
               Z1694Proposta_RestricaoImplantacao = A1694Proposta_RestricaoImplantacao;
               Z1695Proposta_RestricaoCusto = A1695Proposta_RestricaoCusto;
               Z1696Proposta_Ativo = A1696Proposta_Ativo;
               Z1699Proposta_ContratadaCod = A1699Proposta_ContratadaCod;
               Z1700Proposta_ContratadaRazSocial = A1700Proposta_ContratadaRazSocial;
               Z1702Proposta_Prazo = A1702Proposta_Prazo;
               Z1793Proposta_PrazoDias = A1793Proposta_PrazoDias;
               Z1703Proposta_Esforco = A1703Proposta_Esforco;
               Z1794Proposta_Liquido = A1794Proposta_Liquido;
               Z1704Proposta_CntSrvCod = A1704Proposta_CntSrvCod;
               Z1722Proposta_ValorUndCnt = A1722Proposta_ValorUndCnt;
               Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
               Z648Projeto_Codigo = A648Projeto_Codigo;
               Z1686Proposta_OSCodigo = A1686Proposta_OSCodigo;
            }
         }
         if ( GX_JID == -7 )
         {
            Z1685Proposta_Codigo = A1685Proposta_Codigo;
            Z1687Proposta_Vigencia = A1687Proposta_Vigencia;
            Z1688Proposta_Valor = A1688Proposta_Valor;
            Z1689Proposta_Status = A1689Proposta_Status;
            Z1690Proposta_Objetivo = A1690Proposta_Objetivo;
            Z1691Proposta_Escopo = A1691Proposta_Escopo;
            Z1692Proposta_Oportunidade = A1692Proposta_Oportunidade;
            Z1693Proposta_Perspectiva = A1693Proposta_Perspectiva;
            Z1694Proposta_RestricaoImplantacao = A1694Proposta_RestricaoImplantacao;
            Z1695Proposta_RestricaoCusto = A1695Proposta_RestricaoCusto;
            Z1696Proposta_Ativo = A1696Proposta_Ativo;
            Z1699Proposta_ContratadaCod = A1699Proposta_ContratadaCod;
            Z1700Proposta_ContratadaRazSocial = A1700Proposta_ContratadaRazSocial;
            Z1702Proposta_Prazo = A1702Proposta_Prazo;
            Z1793Proposta_PrazoDias = A1793Proposta_PrazoDias;
            Z1703Proposta_Esforco = A1703Proposta_Esforco;
            Z1794Proposta_Liquido = A1794Proposta_Liquido;
            Z1704Proposta_CntSrvCod = A1704Proposta_CntSrvCod;
            Z1722Proposta_ValorUndCnt = A1722Proposta_ValorUndCnt;
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            Z648Projeto_Codigo = A648Projeto_Codigo;
            Z1686Proposta_OSCodigo = A1686Proposta_OSCodigo;
            Z1764Proposta_OSDmn = A1764Proposta_OSDmn;
            Z1711Proposta_OSDmnFM = A1711Proposta_OSDmnFM;
            Z1792Proposta_OSInicio = A1792Proposta_OSInicio;
            Z1705Proposta_OSServico = A1705Proposta_OSServico;
            Z1707Proposta_OSPrzTpDias = A1707Proposta_OSPrzTpDias;
            Z1721Proposta_OSPrzRsp = A1721Proposta_OSPrzRsp;
            Z1763Proposta_OSCntSrvFtrm = A1763Proposta_OSCntSrvFtrm;
            Z1715Proposta_OSCntCod = A1715Proposta_OSCntCod;
            Z1713Proposta_OSUndCntCod = A1713Proposta_OSUndCntCod;
            Z1710Proposta_OSUndCntSgl = A1710Proposta_OSUndCntSgl;
            Z1717Proposta_OSPFB = A1717Proposta_OSPFB;
            Z1718Proposta_OSPFL = A1718Proposta_OSPFL;
            Z1719Proposta_OSDataCnt = A1719Proposta_OSDataCnt;
            Z1720Proposta_OSHoraCnt = A1720Proposta_OSHoraCnt;
         }
      }

      protected void standaloneNotModal( )
      {
         AV14Pgmname = "Proposta";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( AV7Proposta_Codigo > 0 )
         {
            A1685Proposta_Codigo = AV7Proposta_Codigo;
            n1685Proposta_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1685Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1685Proposta_Codigo), 9, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load48187( )
      {
         /* Using cursor T004822 */
         pr_default.execute(12, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound187 = 1;
            A1687Proposta_Vigencia = T004822_A1687Proposta_Vigencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1687Proposta_Vigencia", context.localUtil.TToC( A1687Proposta_Vigencia, 10, 8, 0, 3, "/", ":", " "));
            n1687Proposta_Vigencia = T004822_n1687Proposta_Vigencia[0];
            A1688Proposta_Valor = T004822_A1688Proposta_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1688Proposta_Valor", StringUtil.LTrim( StringUtil.Str( A1688Proposta_Valor, 15, 4)));
            A1689Proposta_Status = T004822_A1689Proposta_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1689Proposta_Status", A1689Proposta_Status);
            n1689Proposta_Status = T004822_n1689Proposta_Status[0];
            A1690Proposta_Objetivo = T004822_A1690Proposta_Objetivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1690Proposta_Objetivo", A1690Proposta_Objetivo);
            A1691Proposta_Escopo = T004822_A1691Proposta_Escopo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1691Proposta_Escopo", A1691Proposta_Escopo);
            n1691Proposta_Escopo = T004822_n1691Proposta_Escopo[0];
            A1692Proposta_Oportunidade = T004822_A1692Proposta_Oportunidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1692Proposta_Oportunidade", A1692Proposta_Oportunidade);
            n1692Proposta_Oportunidade = T004822_n1692Proposta_Oportunidade[0];
            A1693Proposta_Perspectiva = T004822_A1693Proposta_Perspectiva[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1693Proposta_Perspectiva", A1693Proposta_Perspectiva);
            n1693Proposta_Perspectiva = T004822_n1693Proposta_Perspectiva[0];
            A1694Proposta_RestricaoImplantacao = T004822_A1694Proposta_RestricaoImplantacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1694Proposta_RestricaoImplantacao", context.localUtil.TToC( A1694Proposta_RestricaoImplantacao, 10, 8, 0, 3, "/", ":", " "));
            n1694Proposta_RestricaoImplantacao = T004822_n1694Proposta_RestricaoImplantacao[0];
            A1695Proposta_RestricaoCusto = T004822_A1695Proposta_RestricaoCusto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1695Proposta_RestricaoCusto", StringUtil.LTrim( StringUtil.Str( A1695Proposta_RestricaoCusto, 15, 4)));
            n1695Proposta_RestricaoCusto = T004822_n1695Proposta_RestricaoCusto[0];
            A1696Proposta_Ativo = T004822_A1696Proposta_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1696Proposta_Ativo", A1696Proposta_Ativo);
            A1699Proposta_ContratadaCod = T004822_A1699Proposta_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1699Proposta_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1699Proposta_ContratadaCod), 6, 0)));
            A1700Proposta_ContratadaRazSocial = T004822_A1700Proposta_ContratadaRazSocial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1700Proposta_ContratadaRazSocial", A1700Proposta_ContratadaRazSocial);
            n1700Proposta_ContratadaRazSocial = T004822_n1700Proposta_ContratadaRazSocial[0];
            A1702Proposta_Prazo = T004822_A1702Proposta_Prazo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1702Proposta_Prazo", context.localUtil.Format(A1702Proposta_Prazo, "99/99/99"));
            A1793Proposta_PrazoDias = T004822_A1793Proposta_PrazoDias[0];
            n1793Proposta_PrazoDias = T004822_n1793Proposta_PrazoDias[0];
            A1703Proposta_Esforco = T004822_A1703Proposta_Esforco[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1703Proposta_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A1703Proposta_Esforco), 8, 0)));
            A1794Proposta_Liquido = T004822_A1794Proposta_Liquido[0];
            n1794Proposta_Liquido = T004822_n1794Proposta_Liquido[0];
            A1704Proposta_CntSrvCod = T004822_A1704Proposta_CntSrvCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1704Proposta_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1704Proposta_CntSrvCod), 6, 0)));
            A1722Proposta_ValorUndCnt = T004822_A1722Proposta_ValorUndCnt[0];
            A1764Proposta_OSDmn = T004822_A1764Proposta_OSDmn[0];
            n1764Proposta_OSDmn = T004822_n1764Proposta_OSDmn[0];
            A1711Proposta_OSDmnFM = T004822_A1711Proposta_OSDmnFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1711Proposta_OSDmnFM", A1711Proposta_OSDmnFM);
            n1711Proposta_OSDmnFM = T004822_n1711Proposta_OSDmnFM[0];
            A1710Proposta_OSUndCntSgl = T004822_A1710Proposta_OSUndCntSgl[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1710Proposta_OSUndCntSgl", A1710Proposta_OSUndCntSgl);
            n1710Proposta_OSUndCntSgl = T004822_n1710Proposta_OSUndCntSgl[0];
            A1707Proposta_OSPrzTpDias = T004822_A1707Proposta_OSPrzTpDias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1707Proposta_OSPrzTpDias", A1707Proposta_OSPrzTpDias);
            n1707Proposta_OSPrzTpDias = T004822_n1707Proposta_OSPrzTpDias[0];
            A1721Proposta_OSPrzRsp = T004822_A1721Proposta_OSPrzRsp[0];
            n1721Proposta_OSPrzRsp = T004822_n1721Proposta_OSPrzRsp[0];
            A1763Proposta_OSCntSrvFtrm = T004822_A1763Proposta_OSCntSrvFtrm[0];
            n1763Proposta_OSCntSrvFtrm = T004822_n1763Proposta_OSCntSrvFtrm[0];
            A1792Proposta_OSInicio = T004822_A1792Proposta_OSInicio[0];
            n1792Proposta_OSInicio = T004822_n1792Proposta_OSInicio[0];
            A5AreaTrabalho_Codigo = T004822_A5AreaTrabalho_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            A648Projeto_Codigo = T004822_A648Projeto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
            n648Projeto_Codigo = T004822_n648Projeto_Codigo[0];
            A1686Proposta_OSCodigo = T004822_A1686Proposta_OSCodigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1686Proposta_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1686Proposta_OSCodigo), 6, 0)));
            A1705Proposta_OSServico = T004822_A1705Proposta_OSServico[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1705Proposta_OSServico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1705Proposta_OSServico), 6, 0)));
            n1705Proposta_OSServico = T004822_n1705Proposta_OSServico[0];
            A1715Proposta_OSCntCod = T004822_A1715Proposta_OSCntCod[0];
            n1715Proposta_OSCntCod = T004822_n1715Proposta_OSCntCod[0];
            A1713Proposta_OSUndCntCod = T004822_A1713Proposta_OSUndCntCod[0];
            n1713Proposta_OSUndCntCod = T004822_n1713Proposta_OSUndCntCod[0];
            A1717Proposta_OSPFB = T004822_A1717Proposta_OSPFB[0];
            n1717Proposta_OSPFB = T004822_n1717Proposta_OSPFB[0];
            A1718Proposta_OSPFL = T004822_A1718Proposta_OSPFL[0];
            n1718Proposta_OSPFL = T004822_n1718Proposta_OSPFL[0];
            A1719Proposta_OSDataCnt = T004822_A1719Proposta_OSDataCnt[0];
            n1719Proposta_OSDataCnt = T004822_n1719Proposta_OSDataCnt[0];
            A1720Proposta_OSHoraCnt = T004822_A1720Proposta_OSHoraCnt[0];
            n1720Proposta_OSHoraCnt = T004822_n1720Proposta_OSHoraCnt[0];
            ZM48187( -7) ;
         }
         pr_default.close(12);
         OnLoadActions48187( ) ;
      }

      protected void OnLoadActions48187( )
      {
      }

      protected void CheckExtendedTable48187( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T00485 */
         pr_default.execute(3, new Object[] {n648Projeto_Codigo, A648Projeto_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A648Projeto_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto'.", "ForeignKeyNotFound", 1, "PROJETO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynProjeto_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(3);
         if ( ! ( (DateTime.MinValue==A1687Proposta_Vigencia) || ( A1687Proposta_Vigencia >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Proposta_Vigencia fora do intervalo", "OutOfRange", 1, "PROPOSTA_VIGENCIA");
            AnyError = 1;
            GX_FocusControl = edtProposta_Vigencia_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00484 */
         pr_default.execute(2, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "AREATRABALHO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         if ( ! ( (DateTime.MinValue==A1694Proposta_RestricaoImplantacao) || ( A1694Proposta_RestricaoImplantacao >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Proposta_Restricao Implantacao fora do intervalo", "OutOfRange", 1, "PROPOSTA_RESTRICAOIMPLANTACAO");
            AnyError = 1;
            GX_FocusControl = edtProposta_RestricaoImplantacao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A1702Proposta_Prazo) || ( A1702Proposta_Prazo >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Prazo fora do intervalo", "OutOfRange", 1, "PROPOSTA_PRAZO");
            AnyError = 1;
            GX_FocusControl = edtProposta_Prazo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00486 */
         pr_default.execute(4, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Dados da OS'.", "ForeignKeyNotFound", 1, "PROPOSTA_OSCODIGO");
            AnyError = 1;
            GX_FocusControl = edtProposta_OSCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1764Proposta_OSDmn = T00486_A1764Proposta_OSDmn[0];
         n1764Proposta_OSDmn = T00486_n1764Proposta_OSDmn[0];
         A1711Proposta_OSDmnFM = T00486_A1711Proposta_OSDmnFM[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1711Proposta_OSDmnFM", A1711Proposta_OSDmnFM);
         n1711Proposta_OSDmnFM = T00486_n1711Proposta_OSDmnFM[0];
         A1792Proposta_OSInicio = T00486_A1792Proposta_OSInicio[0];
         n1792Proposta_OSInicio = T00486_n1792Proposta_OSInicio[0];
         A1705Proposta_OSServico = T00486_A1705Proposta_OSServico[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1705Proposta_OSServico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1705Proposta_OSServico), 6, 0)));
         n1705Proposta_OSServico = T00486_n1705Proposta_OSServico[0];
         pr_default.close(4);
         /* Using cursor T00487 */
         pr_default.execute(5, new Object[] {n1705Proposta_OSServico, A1705Proposta_OSServico});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A1705Proposta_OSServico) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1707Proposta_OSPrzTpDias = T00487_A1707Proposta_OSPrzTpDias[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1707Proposta_OSPrzTpDias", A1707Proposta_OSPrzTpDias);
         n1707Proposta_OSPrzTpDias = T00487_n1707Proposta_OSPrzTpDias[0];
         A1721Proposta_OSPrzRsp = T00487_A1721Proposta_OSPrzRsp[0];
         n1721Proposta_OSPrzRsp = T00487_n1721Proposta_OSPrzRsp[0];
         A1763Proposta_OSCntSrvFtrm = T00487_A1763Proposta_OSCntSrvFtrm[0];
         n1763Proposta_OSCntSrvFtrm = T00487_n1763Proposta_OSCntSrvFtrm[0];
         A1715Proposta_OSCntCod = T00487_A1715Proposta_OSCntCod[0];
         n1715Proposta_OSCntCod = T00487_n1715Proposta_OSCntCod[0];
         A1713Proposta_OSUndCntCod = T00487_A1713Proposta_OSUndCntCod[0];
         n1713Proposta_OSUndCntCod = T00487_n1713Proposta_OSUndCntCod[0];
         pr_default.close(5);
         /* Using cursor T00488 */
         pr_default.execute(6, new Object[] {n1713Proposta_OSUndCntCod, A1713Proposta_OSUndCntCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A1713Proposta_OSUndCntCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Unidades de Medi��o'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1710Proposta_OSUndCntSgl = T00488_A1710Proposta_OSUndCntSgl[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1710Proposta_OSUndCntSgl", A1710Proposta_OSUndCntSgl);
         n1710Proposta_OSUndCntSgl = T00488_n1710Proposta_OSUndCntSgl[0];
         pr_default.close(6);
         /* Using cursor T004810 */
         pr_default.execute(7, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            A1717Proposta_OSPFB = T004810_A1717Proposta_OSPFB[0];
            n1717Proposta_OSPFB = T004810_n1717Proposta_OSPFB[0];
         }
         else
         {
            A1717Proposta_OSPFB = 0;
            n1717Proposta_OSPFB = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1717Proposta_OSPFB", StringUtil.LTrim( StringUtil.Str( A1717Proposta_OSPFB, 14, 5)));
         }
         pr_default.close(7);
         /* Using cursor T004812 */
         pr_default.execute(8, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            A1718Proposta_OSPFL = T004812_A1718Proposta_OSPFL[0];
            n1718Proposta_OSPFL = T004812_n1718Proposta_OSPFL[0];
         }
         else
         {
            A1718Proposta_OSPFL = 0;
            n1718Proposta_OSPFL = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1718Proposta_OSPFL", StringUtil.LTrim( StringUtil.Str( A1718Proposta_OSPFL, 14, 5)));
         }
         pr_default.close(8);
         /* Using cursor T004814 */
         pr_default.execute(9, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            A1719Proposta_OSDataCnt = T004814_A1719Proposta_OSDataCnt[0];
            n1719Proposta_OSDataCnt = T004814_n1719Proposta_OSDataCnt[0];
         }
         else
         {
            A1719Proposta_OSDataCnt = DateTime.MinValue;
            n1719Proposta_OSDataCnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1719Proposta_OSDataCnt", context.localUtil.Format(A1719Proposta_OSDataCnt, "99/99/99"));
         }
         pr_default.close(9);
         /* Using cursor T004816 */
         pr_default.execute(10, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            A1720Proposta_OSHoraCnt = T004816_A1720Proposta_OSHoraCnt[0];
            n1720Proposta_OSHoraCnt = T004816_n1720Proposta_OSHoraCnt[0];
         }
         else
         {
            A1720Proposta_OSHoraCnt = "";
            n1720Proposta_OSHoraCnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1720Proposta_OSHoraCnt", A1720Proposta_OSHoraCnt);
         }
         pr_default.close(10);
      }

      protected void CloseExtendedTableCursors48187( )
      {
         pr_default.close(3);
         pr_default.close(2);
         pr_default.close(4);
         pr_default.close(5);
         pr_default.close(6);
         pr_default.close(7);
         pr_default.close(8);
         pr_default.close(9);
         pr_default.close(10);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_9( int A648Projeto_Codigo )
      {
         /* Using cursor T004823 */
         pr_default.execute(13, new Object[] {n648Projeto_Codigo, A648Projeto_Codigo});
         if ( (pr_default.getStatus(13) == 101) )
         {
            if ( ! ( (0==A648Projeto_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto'.", "ForeignKeyNotFound", 1, "PROJETO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynProjeto_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(13) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(13);
      }

      protected void gxLoad_8( int A5AreaTrabalho_Codigo )
      {
         /* Using cursor T004824 */
         pr_default.execute(14, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "AREATRABALHO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(14) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(14);
      }

      protected void gxLoad_10( int A1686Proposta_OSCodigo )
      {
         /* Using cursor T004825 */
         pr_default.execute(15, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Dados da OS'.", "ForeignKeyNotFound", 1, "PROPOSTA_OSCODIGO");
            AnyError = 1;
            GX_FocusControl = edtProposta_OSCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1764Proposta_OSDmn = T004825_A1764Proposta_OSDmn[0];
         n1764Proposta_OSDmn = T004825_n1764Proposta_OSDmn[0];
         A1711Proposta_OSDmnFM = T004825_A1711Proposta_OSDmnFM[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1711Proposta_OSDmnFM", A1711Proposta_OSDmnFM);
         n1711Proposta_OSDmnFM = T004825_n1711Proposta_OSDmnFM[0];
         A1792Proposta_OSInicio = T004825_A1792Proposta_OSInicio[0];
         n1792Proposta_OSInicio = T004825_n1792Proposta_OSInicio[0];
         A1705Proposta_OSServico = T004825_A1705Proposta_OSServico[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1705Proposta_OSServico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1705Proposta_OSServico), 6, 0)));
         n1705Proposta_OSServico = T004825_n1705Proposta_OSServico[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A1764Proposta_OSDmn)+"\""+","+"\""+GXUtil.EncodeJSConstant( A1711Proposta_OSDmnFM)+"\""+","+"\""+GXUtil.EncodeJSConstant( context.localUtil.Format(A1792Proposta_OSInicio, "99/99/99"))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1705Proposta_OSServico), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(15) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(15);
      }

      protected void gxLoad_11( int A1705Proposta_OSServico )
      {
         /* Using cursor T004826 */
         pr_default.execute(16, new Object[] {n1705Proposta_OSServico, A1705Proposta_OSServico});
         if ( (pr_default.getStatus(16) == 101) )
         {
            if ( ! ( (0==A1705Proposta_OSServico) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1707Proposta_OSPrzTpDias = T004826_A1707Proposta_OSPrzTpDias[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1707Proposta_OSPrzTpDias", A1707Proposta_OSPrzTpDias);
         n1707Proposta_OSPrzTpDias = T004826_n1707Proposta_OSPrzTpDias[0];
         A1721Proposta_OSPrzRsp = T004826_A1721Proposta_OSPrzRsp[0];
         n1721Proposta_OSPrzRsp = T004826_n1721Proposta_OSPrzRsp[0];
         A1763Proposta_OSCntSrvFtrm = T004826_A1763Proposta_OSCntSrvFtrm[0];
         n1763Proposta_OSCntSrvFtrm = T004826_n1763Proposta_OSCntSrvFtrm[0];
         A1715Proposta_OSCntCod = T004826_A1715Proposta_OSCntCod[0];
         n1715Proposta_OSCntCod = T004826_n1715Proposta_OSCntCod[0];
         A1713Proposta_OSUndCntCod = T004826_A1713Proposta_OSUndCntCod[0];
         n1713Proposta_OSUndCntCod = T004826_n1713Proposta_OSUndCntCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1707Proposta_OSPrzTpDias))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1721Proposta_OSPrzRsp), 4, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( A1763Proposta_OSCntSrvFtrm)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1715Proposta_OSCntCod), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1713Proposta_OSUndCntCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(16) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(16);
      }

      protected void gxLoad_12( int A1713Proposta_OSUndCntCod )
      {
         /* Using cursor T004827 */
         pr_default.execute(17, new Object[] {n1713Proposta_OSUndCntCod, A1713Proposta_OSUndCntCod});
         if ( (pr_default.getStatus(17) == 101) )
         {
            if ( ! ( (0==A1713Proposta_OSUndCntCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Unidades de Medi��o'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1710Proposta_OSUndCntSgl = T004827_A1710Proposta_OSUndCntSgl[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1710Proposta_OSUndCntSgl", A1710Proposta_OSUndCntSgl);
         n1710Proposta_OSUndCntSgl = T004827_n1710Proposta_OSUndCntSgl[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1710Proposta_OSUndCntSgl))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(17) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(17);
      }

      protected void gxLoad_13( int A1686Proposta_OSCodigo )
      {
         /* Using cursor T004829 */
         pr_default.execute(18, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(18) != 101) )
         {
            A1717Proposta_OSPFB = T004829_A1717Proposta_OSPFB[0];
            n1717Proposta_OSPFB = T004829_n1717Proposta_OSPFB[0];
         }
         else
         {
            A1717Proposta_OSPFB = 0;
            n1717Proposta_OSPFB = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1717Proposta_OSPFB", StringUtil.LTrim( StringUtil.Str( A1717Proposta_OSPFB, 14, 5)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A1717Proposta_OSPFB, 14, 5, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(18) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(18);
      }

      protected void gxLoad_14( int A1686Proposta_OSCodigo )
      {
         /* Using cursor T004831 */
         pr_default.execute(19, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(19) != 101) )
         {
            A1718Proposta_OSPFL = T004831_A1718Proposta_OSPFL[0];
            n1718Proposta_OSPFL = T004831_n1718Proposta_OSPFL[0];
         }
         else
         {
            A1718Proposta_OSPFL = 0;
            n1718Proposta_OSPFL = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1718Proposta_OSPFL", StringUtil.LTrim( StringUtil.Str( A1718Proposta_OSPFL, 14, 5)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A1718Proposta_OSPFL, 14, 5, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(19) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(19);
      }

      protected void gxLoad_15( int A1686Proposta_OSCodigo )
      {
         /* Using cursor T004833 */
         pr_default.execute(20, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(20) != 101) )
         {
            A1719Proposta_OSDataCnt = T004833_A1719Proposta_OSDataCnt[0];
            n1719Proposta_OSDataCnt = T004833_n1719Proposta_OSDataCnt[0];
         }
         else
         {
            A1719Proposta_OSDataCnt = DateTime.MinValue;
            n1719Proposta_OSDataCnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1719Proposta_OSDataCnt", context.localUtil.Format(A1719Proposta_OSDataCnt, "99/99/99"));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( context.localUtil.Format(A1719Proposta_OSDataCnt, "99/99/99"))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(20) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(20);
      }

      protected void gxLoad_16( int A1686Proposta_OSCodigo )
      {
         /* Using cursor T004835 */
         pr_default.execute(21, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(21) != 101) )
         {
            A1720Proposta_OSHoraCnt = T004835_A1720Proposta_OSHoraCnt[0];
            n1720Proposta_OSHoraCnt = T004835_n1720Proposta_OSHoraCnt[0];
         }
         else
         {
            A1720Proposta_OSHoraCnt = "";
            n1720Proposta_OSHoraCnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1720Proposta_OSHoraCnt", A1720Proposta_OSHoraCnt);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1720Proposta_OSHoraCnt))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(21) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(21);
      }

      protected void GetKey48187( )
      {
         /* Using cursor T004836 */
         pr_default.execute(22, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
         if ( (pr_default.getStatus(22) != 101) )
         {
            RcdFound187 = 1;
         }
         else
         {
            RcdFound187 = 0;
         }
         pr_default.close(22);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00483 */
         pr_default.execute(1, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM48187( 7) ;
            RcdFound187 = 1;
            A1685Proposta_Codigo = T00483_A1685Proposta_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1685Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1685Proposta_Codigo), 9, 0)));
            n1685Proposta_Codigo = T00483_n1685Proposta_Codigo[0];
            A1687Proposta_Vigencia = T00483_A1687Proposta_Vigencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1687Proposta_Vigencia", context.localUtil.TToC( A1687Proposta_Vigencia, 10, 8, 0, 3, "/", ":", " "));
            n1687Proposta_Vigencia = T00483_n1687Proposta_Vigencia[0];
            A1688Proposta_Valor = T00483_A1688Proposta_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1688Proposta_Valor", StringUtil.LTrim( StringUtil.Str( A1688Proposta_Valor, 15, 4)));
            A1689Proposta_Status = T00483_A1689Proposta_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1689Proposta_Status", A1689Proposta_Status);
            n1689Proposta_Status = T00483_n1689Proposta_Status[0];
            A1690Proposta_Objetivo = T00483_A1690Proposta_Objetivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1690Proposta_Objetivo", A1690Proposta_Objetivo);
            A1691Proposta_Escopo = T00483_A1691Proposta_Escopo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1691Proposta_Escopo", A1691Proposta_Escopo);
            n1691Proposta_Escopo = T00483_n1691Proposta_Escopo[0];
            A1692Proposta_Oportunidade = T00483_A1692Proposta_Oportunidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1692Proposta_Oportunidade", A1692Proposta_Oportunidade);
            n1692Proposta_Oportunidade = T00483_n1692Proposta_Oportunidade[0];
            A1693Proposta_Perspectiva = T00483_A1693Proposta_Perspectiva[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1693Proposta_Perspectiva", A1693Proposta_Perspectiva);
            n1693Proposta_Perspectiva = T00483_n1693Proposta_Perspectiva[0];
            A1694Proposta_RestricaoImplantacao = T00483_A1694Proposta_RestricaoImplantacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1694Proposta_RestricaoImplantacao", context.localUtil.TToC( A1694Proposta_RestricaoImplantacao, 10, 8, 0, 3, "/", ":", " "));
            n1694Proposta_RestricaoImplantacao = T00483_n1694Proposta_RestricaoImplantacao[0];
            A1695Proposta_RestricaoCusto = T00483_A1695Proposta_RestricaoCusto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1695Proposta_RestricaoCusto", StringUtil.LTrim( StringUtil.Str( A1695Proposta_RestricaoCusto, 15, 4)));
            n1695Proposta_RestricaoCusto = T00483_n1695Proposta_RestricaoCusto[0];
            A1696Proposta_Ativo = T00483_A1696Proposta_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1696Proposta_Ativo", A1696Proposta_Ativo);
            A1699Proposta_ContratadaCod = T00483_A1699Proposta_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1699Proposta_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1699Proposta_ContratadaCod), 6, 0)));
            A1700Proposta_ContratadaRazSocial = T00483_A1700Proposta_ContratadaRazSocial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1700Proposta_ContratadaRazSocial", A1700Proposta_ContratadaRazSocial);
            n1700Proposta_ContratadaRazSocial = T00483_n1700Proposta_ContratadaRazSocial[0];
            A1702Proposta_Prazo = T00483_A1702Proposta_Prazo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1702Proposta_Prazo", context.localUtil.Format(A1702Proposta_Prazo, "99/99/99"));
            A1793Proposta_PrazoDias = T00483_A1793Proposta_PrazoDias[0];
            n1793Proposta_PrazoDias = T00483_n1793Proposta_PrazoDias[0];
            A1703Proposta_Esforco = T00483_A1703Proposta_Esforco[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1703Proposta_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A1703Proposta_Esforco), 8, 0)));
            A1794Proposta_Liquido = T00483_A1794Proposta_Liquido[0];
            n1794Proposta_Liquido = T00483_n1794Proposta_Liquido[0];
            A1704Proposta_CntSrvCod = T00483_A1704Proposta_CntSrvCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1704Proposta_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1704Proposta_CntSrvCod), 6, 0)));
            A1722Proposta_ValorUndCnt = T00483_A1722Proposta_ValorUndCnt[0];
            A5AreaTrabalho_Codigo = T00483_A5AreaTrabalho_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            A648Projeto_Codigo = T00483_A648Projeto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
            n648Projeto_Codigo = T00483_n648Projeto_Codigo[0];
            A1686Proposta_OSCodigo = T00483_A1686Proposta_OSCodigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1686Proposta_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1686Proposta_OSCodigo), 6, 0)));
            Z1685Proposta_Codigo = A1685Proposta_Codigo;
            sMode187 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            Load48187( ) ;
            if ( AnyError == 1 )
            {
               RcdFound187 = 0;
               InitializeNonKey48187( ) ;
            }
            Gx_mode = sMode187;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound187 = 0;
            InitializeNonKey48187( ) ;
            sMode187 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode187;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey48187( ) ;
         if ( RcdFound187 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound187 = 0;
         /* Using cursor T004837 */
         pr_default.execute(23, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
         if ( (pr_default.getStatus(23) != 101) )
         {
            while ( (pr_default.getStatus(23) != 101) && ( ( T004837_A1685Proposta_Codigo[0] < A1685Proposta_Codigo ) ) )
            {
               pr_default.readNext(23);
            }
            if ( (pr_default.getStatus(23) != 101) && ( ( T004837_A1685Proposta_Codigo[0] > A1685Proposta_Codigo ) ) )
            {
               A1685Proposta_Codigo = T004837_A1685Proposta_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1685Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1685Proposta_Codigo), 9, 0)));
               n1685Proposta_Codigo = T004837_n1685Proposta_Codigo[0];
               RcdFound187 = 1;
            }
         }
         pr_default.close(23);
      }

      protected void move_previous( )
      {
         RcdFound187 = 0;
         /* Using cursor T004838 */
         pr_default.execute(24, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
         if ( (pr_default.getStatus(24) != 101) )
         {
            while ( (pr_default.getStatus(24) != 101) && ( ( T004838_A1685Proposta_Codigo[0] > A1685Proposta_Codigo ) ) )
            {
               pr_default.readNext(24);
            }
            if ( (pr_default.getStatus(24) != 101) && ( ( T004838_A1685Proposta_Codigo[0] < A1685Proposta_Codigo ) ) )
            {
               A1685Proposta_Codigo = T004838_A1685Proposta_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1685Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1685Proposta_Codigo), 9, 0)));
               n1685Proposta_Codigo = T004838_n1685Proposta_Codigo[0];
               RcdFound187 = 1;
            }
         }
         pr_default.close(24);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey48187( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtProposta_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert48187( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound187 == 1 )
            {
               if ( A1685Proposta_Codigo != Z1685Proposta_Codigo )
               {
                  A1685Proposta_Codigo = Z1685Proposta_Codigo;
                  n1685Proposta_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1685Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1685Proposta_Codigo), 9, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "PROPOSTA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtProposta_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtProposta_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update48187( ) ;
                  GX_FocusControl = edtProposta_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1685Proposta_Codigo != Z1685Proposta_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtProposta_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert48187( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "PROPOSTA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtProposta_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtProposta_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert48187( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1685Proposta_Codigo != Z1685Proposta_Codigo )
         {
            A1685Proposta_Codigo = Z1685Proposta_Codigo;
            n1685Proposta_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1685Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1685Proposta_Codigo), 9, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "PROPOSTA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtProposta_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtProposta_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency48187( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00482 */
            pr_default.execute(0, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"PROPOSTA"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z1687Proposta_Vigencia != T00482_A1687Proposta_Vigencia[0] ) || ( Z1688Proposta_Valor != T00482_A1688Proposta_Valor[0] ) || ( StringUtil.StrCmp(Z1689Proposta_Status, T00482_A1689Proposta_Status[0]) != 0 ) || ( Z1694Proposta_RestricaoImplantacao != T00482_A1694Proposta_RestricaoImplantacao[0] ) || ( Z1695Proposta_RestricaoCusto != T00482_A1695Proposta_RestricaoCusto[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1696Proposta_Ativo != T00482_A1696Proposta_Ativo[0] ) || ( Z1699Proposta_ContratadaCod != T00482_A1699Proposta_ContratadaCod[0] ) || ( StringUtil.StrCmp(Z1700Proposta_ContratadaRazSocial, T00482_A1700Proposta_ContratadaRazSocial[0]) != 0 ) || ( Z1702Proposta_Prazo != T00482_A1702Proposta_Prazo[0] ) || ( Z1793Proposta_PrazoDias != T00482_A1793Proposta_PrazoDias[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1703Proposta_Esforco != T00482_A1703Proposta_Esforco[0] ) || ( Z1794Proposta_Liquido != T00482_A1794Proposta_Liquido[0] ) || ( Z1704Proposta_CntSrvCod != T00482_A1704Proposta_CntSrvCod[0] ) || ( Z1722Proposta_ValorUndCnt != T00482_A1722Proposta_ValorUndCnt[0] ) || ( Z5AreaTrabalho_Codigo != T00482_A5AreaTrabalho_Codigo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z648Projeto_Codigo != T00482_A648Projeto_Codigo[0] ) || ( Z1686Proposta_OSCodigo != T00482_A1686Proposta_OSCodigo[0] ) )
            {
               if ( Z1687Proposta_Vigencia != T00482_A1687Proposta_Vigencia[0] )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"Proposta_Vigencia");
                  GXUtil.WriteLogRaw("Old: ",Z1687Proposta_Vigencia);
                  GXUtil.WriteLogRaw("Current: ",T00482_A1687Proposta_Vigencia[0]);
               }
               if ( Z1688Proposta_Valor != T00482_A1688Proposta_Valor[0] )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"Proposta_Valor");
                  GXUtil.WriteLogRaw("Old: ",Z1688Proposta_Valor);
                  GXUtil.WriteLogRaw("Current: ",T00482_A1688Proposta_Valor[0]);
               }
               if ( StringUtil.StrCmp(Z1689Proposta_Status, T00482_A1689Proposta_Status[0]) != 0 )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"Proposta_Status");
                  GXUtil.WriteLogRaw("Old: ",Z1689Proposta_Status);
                  GXUtil.WriteLogRaw("Current: ",T00482_A1689Proposta_Status[0]);
               }
               if ( Z1694Proposta_RestricaoImplantacao != T00482_A1694Proposta_RestricaoImplantacao[0] )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"Proposta_RestricaoImplantacao");
                  GXUtil.WriteLogRaw("Old: ",Z1694Proposta_RestricaoImplantacao);
                  GXUtil.WriteLogRaw("Current: ",T00482_A1694Proposta_RestricaoImplantacao[0]);
               }
               if ( Z1695Proposta_RestricaoCusto != T00482_A1695Proposta_RestricaoCusto[0] )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"Proposta_RestricaoCusto");
                  GXUtil.WriteLogRaw("Old: ",Z1695Proposta_RestricaoCusto);
                  GXUtil.WriteLogRaw("Current: ",T00482_A1695Proposta_RestricaoCusto[0]);
               }
               if ( Z1696Proposta_Ativo != T00482_A1696Proposta_Ativo[0] )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"Proposta_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z1696Proposta_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T00482_A1696Proposta_Ativo[0]);
               }
               if ( Z1699Proposta_ContratadaCod != T00482_A1699Proposta_ContratadaCod[0] )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"Proposta_ContratadaCod");
                  GXUtil.WriteLogRaw("Old: ",Z1699Proposta_ContratadaCod);
                  GXUtil.WriteLogRaw("Current: ",T00482_A1699Proposta_ContratadaCod[0]);
               }
               if ( StringUtil.StrCmp(Z1700Proposta_ContratadaRazSocial, T00482_A1700Proposta_ContratadaRazSocial[0]) != 0 )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"Proposta_ContratadaRazSocial");
                  GXUtil.WriteLogRaw("Old: ",Z1700Proposta_ContratadaRazSocial);
                  GXUtil.WriteLogRaw("Current: ",T00482_A1700Proposta_ContratadaRazSocial[0]);
               }
               if ( Z1702Proposta_Prazo != T00482_A1702Proposta_Prazo[0] )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"Proposta_Prazo");
                  GXUtil.WriteLogRaw("Old: ",Z1702Proposta_Prazo);
                  GXUtil.WriteLogRaw("Current: ",T00482_A1702Proposta_Prazo[0]);
               }
               if ( Z1793Proposta_PrazoDias != T00482_A1793Proposta_PrazoDias[0] )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"Proposta_PrazoDias");
                  GXUtil.WriteLogRaw("Old: ",Z1793Proposta_PrazoDias);
                  GXUtil.WriteLogRaw("Current: ",T00482_A1793Proposta_PrazoDias[0]);
               }
               if ( Z1703Proposta_Esforco != T00482_A1703Proposta_Esforco[0] )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"Proposta_Esforco");
                  GXUtil.WriteLogRaw("Old: ",Z1703Proposta_Esforco);
                  GXUtil.WriteLogRaw("Current: ",T00482_A1703Proposta_Esforco[0]);
               }
               if ( Z1794Proposta_Liquido != T00482_A1794Proposta_Liquido[0] )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"Proposta_Liquido");
                  GXUtil.WriteLogRaw("Old: ",Z1794Proposta_Liquido);
                  GXUtil.WriteLogRaw("Current: ",T00482_A1794Proposta_Liquido[0]);
               }
               if ( Z1704Proposta_CntSrvCod != T00482_A1704Proposta_CntSrvCod[0] )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"Proposta_CntSrvCod");
                  GXUtil.WriteLogRaw("Old: ",Z1704Proposta_CntSrvCod);
                  GXUtil.WriteLogRaw("Current: ",T00482_A1704Proposta_CntSrvCod[0]);
               }
               if ( Z1722Proposta_ValorUndCnt != T00482_A1722Proposta_ValorUndCnt[0] )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"Proposta_ValorUndCnt");
                  GXUtil.WriteLogRaw("Old: ",Z1722Proposta_ValorUndCnt);
                  GXUtil.WriteLogRaw("Current: ",T00482_A1722Proposta_ValorUndCnt[0]);
               }
               if ( Z5AreaTrabalho_Codigo != T00482_A5AreaTrabalho_Codigo[0] )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"AreaTrabalho_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z5AreaTrabalho_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00482_A5AreaTrabalho_Codigo[0]);
               }
               if ( Z648Projeto_Codigo != T00482_A648Projeto_Codigo[0] )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"Projeto_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z648Projeto_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00482_A648Projeto_Codigo[0]);
               }
               if ( Z1686Proposta_OSCodigo != T00482_A1686Proposta_OSCodigo[0] )
               {
                  GXUtil.WriteLog("proposta:[seudo value changed for attri]"+"Proposta_OSCodigo");
                  GXUtil.WriteLogRaw("Old: ",Z1686Proposta_OSCodigo);
                  GXUtil.WriteLogRaw("Current: ",T00482_A1686Proposta_OSCodigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"PROPOSTA"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert48187( )
      {
         BeforeValidate48187( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable48187( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM48187( 0) ;
            CheckOptimisticConcurrency48187( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm48187( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert48187( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004839 */
                     pr_default.execute(25, new Object[] {n1687Proposta_Vigencia, A1687Proposta_Vigencia, A1688Proposta_Valor, n1689Proposta_Status, A1689Proposta_Status, A1690Proposta_Objetivo, n1691Proposta_Escopo, A1691Proposta_Escopo, n1692Proposta_Oportunidade, A1692Proposta_Oportunidade, n1693Proposta_Perspectiva, A1693Proposta_Perspectiva, n1694Proposta_RestricaoImplantacao, A1694Proposta_RestricaoImplantacao, n1695Proposta_RestricaoCusto, A1695Proposta_RestricaoCusto, A1696Proposta_Ativo, A1699Proposta_ContratadaCod, n1700Proposta_ContratadaRazSocial, A1700Proposta_ContratadaRazSocial, A1702Proposta_Prazo, n1793Proposta_PrazoDias, A1793Proposta_PrazoDias, A1703Proposta_Esforco, n1794Proposta_Liquido, A1794Proposta_Liquido, A1704Proposta_CntSrvCod, A1722Proposta_ValorUndCnt, A5AreaTrabalho_Codigo, n648Projeto_Codigo, A648Projeto_Codigo, A1686Proposta_OSCodigo});
                     A1685Proposta_Codigo = T004839_A1685Proposta_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1685Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1685Proposta_Codigo), 9, 0)));
                     n1685Proposta_Codigo = T004839_n1685Proposta_Codigo[0];
                     pr_default.close(25);
                     dsDefault.SmartCacheProvider.SetUpdated("PROPOSTA") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption480( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load48187( ) ;
            }
            EndLevel48187( ) ;
         }
         CloseExtendedTableCursors48187( ) ;
      }

      protected void Update48187( )
      {
         BeforeValidate48187( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable48187( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency48187( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm48187( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate48187( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004840 */
                     pr_default.execute(26, new Object[] {n1687Proposta_Vigencia, A1687Proposta_Vigencia, A1688Proposta_Valor, n1689Proposta_Status, A1689Proposta_Status, A1690Proposta_Objetivo, n1691Proposta_Escopo, A1691Proposta_Escopo, n1692Proposta_Oportunidade, A1692Proposta_Oportunidade, n1693Proposta_Perspectiva, A1693Proposta_Perspectiva, n1694Proposta_RestricaoImplantacao, A1694Proposta_RestricaoImplantacao, n1695Proposta_RestricaoCusto, A1695Proposta_RestricaoCusto, A1696Proposta_Ativo, A1699Proposta_ContratadaCod, n1700Proposta_ContratadaRazSocial, A1700Proposta_ContratadaRazSocial, A1702Proposta_Prazo, n1793Proposta_PrazoDias, A1793Proposta_PrazoDias, A1703Proposta_Esforco, n1794Proposta_Liquido, A1794Proposta_Liquido, A1704Proposta_CntSrvCod, A1722Proposta_ValorUndCnt, A5AreaTrabalho_Codigo, n648Projeto_Codigo, A648Projeto_Codigo, A1686Proposta_OSCodigo, n1685Proposta_Codigo, A1685Proposta_Codigo});
                     pr_default.close(26);
                     dsDefault.SmartCacheProvider.SetUpdated("PROPOSTA") ;
                     if ( (pr_default.getStatus(26) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"PROPOSTA"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate48187( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel48187( ) ;
         }
         CloseExtendedTableCursors48187( ) ;
      }

      protected void DeferredUpdate48187( )
      {
      }

      protected void delete( )
      {
         BeforeValidate48187( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency48187( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls48187( ) ;
            AfterConfirm48187( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete48187( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004841 */
                  pr_default.execute(27, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
                  pr_default.close(27);
                  dsDefault.SmartCacheProvider.SetUpdated("PROPOSTA") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode187 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel48187( ) ;
         Gx_mode = sMode187;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls48187( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T004842 */
            pr_default.execute(28, new Object[] {A1686Proposta_OSCodigo});
            A1764Proposta_OSDmn = T004842_A1764Proposta_OSDmn[0];
            n1764Proposta_OSDmn = T004842_n1764Proposta_OSDmn[0];
            A1711Proposta_OSDmnFM = T004842_A1711Proposta_OSDmnFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1711Proposta_OSDmnFM", A1711Proposta_OSDmnFM);
            n1711Proposta_OSDmnFM = T004842_n1711Proposta_OSDmnFM[0];
            A1792Proposta_OSInicio = T004842_A1792Proposta_OSInicio[0];
            n1792Proposta_OSInicio = T004842_n1792Proposta_OSInicio[0];
            A1705Proposta_OSServico = T004842_A1705Proposta_OSServico[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1705Proposta_OSServico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1705Proposta_OSServico), 6, 0)));
            n1705Proposta_OSServico = T004842_n1705Proposta_OSServico[0];
            pr_default.close(28);
            /* Using cursor T004843 */
            pr_default.execute(29, new Object[] {n1705Proposta_OSServico, A1705Proposta_OSServico});
            A1707Proposta_OSPrzTpDias = T004843_A1707Proposta_OSPrzTpDias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1707Proposta_OSPrzTpDias", A1707Proposta_OSPrzTpDias);
            n1707Proposta_OSPrzTpDias = T004843_n1707Proposta_OSPrzTpDias[0];
            A1721Proposta_OSPrzRsp = T004843_A1721Proposta_OSPrzRsp[0];
            n1721Proposta_OSPrzRsp = T004843_n1721Proposta_OSPrzRsp[0];
            A1763Proposta_OSCntSrvFtrm = T004843_A1763Proposta_OSCntSrvFtrm[0];
            n1763Proposta_OSCntSrvFtrm = T004843_n1763Proposta_OSCntSrvFtrm[0];
            A1715Proposta_OSCntCod = T004843_A1715Proposta_OSCntCod[0];
            n1715Proposta_OSCntCod = T004843_n1715Proposta_OSCntCod[0];
            A1713Proposta_OSUndCntCod = T004843_A1713Proposta_OSUndCntCod[0];
            n1713Proposta_OSUndCntCod = T004843_n1713Proposta_OSUndCntCod[0];
            pr_default.close(29);
            /* Using cursor T004844 */
            pr_default.execute(30, new Object[] {n1713Proposta_OSUndCntCod, A1713Proposta_OSUndCntCod});
            A1710Proposta_OSUndCntSgl = T004844_A1710Proposta_OSUndCntSgl[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1710Proposta_OSUndCntSgl", A1710Proposta_OSUndCntSgl);
            n1710Proposta_OSUndCntSgl = T004844_n1710Proposta_OSUndCntSgl[0];
            pr_default.close(30);
            /* Using cursor T004846 */
            pr_default.execute(31, new Object[] {A1686Proposta_OSCodigo});
            if ( (pr_default.getStatus(31) != 101) )
            {
               A1717Proposta_OSPFB = T004846_A1717Proposta_OSPFB[0];
               n1717Proposta_OSPFB = T004846_n1717Proposta_OSPFB[0];
            }
            else
            {
               A1717Proposta_OSPFB = 0;
               n1717Proposta_OSPFB = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1717Proposta_OSPFB", StringUtil.LTrim( StringUtil.Str( A1717Proposta_OSPFB, 14, 5)));
            }
            pr_default.close(31);
            /* Using cursor T004848 */
            pr_default.execute(32, new Object[] {A1686Proposta_OSCodigo});
            if ( (pr_default.getStatus(32) != 101) )
            {
               A1718Proposta_OSPFL = T004848_A1718Proposta_OSPFL[0];
               n1718Proposta_OSPFL = T004848_n1718Proposta_OSPFL[0];
            }
            else
            {
               A1718Proposta_OSPFL = 0;
               n1718Proposta_OSPFL = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1718Proposta_OSPFL", StringUtil.LTrim( StringUtil.Str( A1718Proposta_OSPFL, 14, 5)));
            }
            pr_default.close(32);
            /* Using cursor T004850 */
            pr_default.execute(33, new Object[] {A1686Proposta_OSCodigo});
            if ( (pr_default.getStatus(33) != 101) )
            {
               A1719Proposta_OSDataCnt = T004850_A1719Proposta_OSDataCnt[0];
               n1719Proposta_OSDataCnt = T004850_n1719Proposta_OSDataCnt[0];
            }
            else
            {
               A1719Proposta_OSDataCnt = DateTime.MinValue;
               n1719Proposta_OSDataCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1719Proposta_OSDataCnt", context.localUtil.Format(A1719Proposta_OSDataCnt, "99/99/99"));
            }
            pr_default.close(33);
            /* Using cursor T004852 */
            pr_default.execute(34, new Object[] {A1686Proposta_OSCodigo});
            if ( (pr_default.getStatus(34) != 101) )
            {
               A1720Proposta_OSHoraCnt = T004852_A1720Proposta_OSHoraCnt[0];
               n1720Proposta_OSHoraCnt = T004852_n1720Proposta_OSHoraCnt[0];
            }
            else
            {
               A1720Proposta_OSHoraCnt = "";
               n1720Proposta_OSHoraCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1720Proposta_OSHoraCnt", A1720Proposta_OSHoraCnt);
            }
            pr_default.close(34);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T004853 */
            pr_default.execute(35, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
            if ( (pr_default.getStatus(35) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T213"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(35);
         }
      }

      protected void EndLevel48187( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete48187( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(28);
            pr_default.close(29);
            pr_default.close(30);
            pr_default.close(31);
            pr_default.close(32);
            pr_default.close(33);
            pr_default.close(34);
            context.CommitDataStores( "Proposta");
            if ( AnyError == 0 )
            {
               ConfirmValues480( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(28);
            pr_default.close(29);
            pr_default.close(30);
            pr_default.close(31);
            pr_default.close(32);
            pr_default.close(33);
            pr_default.close(34);
            context.RollbackDataStores( "Proposta");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart48187( )
      {
         /* Scan By routine */
         /* Using cursor T004854 */
         pr_default.execute(36);
         RcdFound187 = 0;
         if ( (pr_default.getStatus(36) != 101) )
         {
            RcdFound187 = 1;
            A1685Proposta_Codigo = T004854_A1685Proposta_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1685Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1685Proposta_Codigo), 9, 0)));
            n1685Proposta_Codigo = T004854_n1685Proposta_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext48187( )
      {
         /* Scan next routine */
         pr_default.readNext(36);
         RcdFound187 = 0;
         if ( (pr_default.getStatus(36) != 101) )
         {
            RcdFound187 = 1;
            A1685Proposta_Codigo = T004854_A1685Proposta_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1685Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1685Proposta_Codigo), 9, 0)));
            n1685Proposta_Codigo = T004854_n1685Proposta_Codigo[0];
         }
      }

      protected void ScanEnd48187( )
      {
         pr_default.close(36);
      }

      protected void AfterConfirm48187( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert48187( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate48187( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete48187( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete48187( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate48187( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes48187( )
      {
         edtProposta_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_Codigo_Enabled), 5, 0)));
         dynProjeto_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjeto_Codigo.Enabled), 5, 0)));
         edtProposta_Vigencia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_Vigencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_Vigencia_Enabled), 5, 0)));
         edtProposta_Valor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_Valor_Enabled), 5, 0)));
         edtProposta_Status_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_Status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_Status_Enabled), 5, 0)));
         edtAreaTrabalho_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_Codigo_Enabled), 5, 0)));
         edtProposta_Objetivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_Objetivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_Objetivo_Enabled), 5, 0)));
         edtProposta_Escopo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_Escopo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_Escopo_Enabled), 5, 0)));
         edtProposta_Oportunidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_Oportunidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_Oportunidade_Enabled), 5, 0)));
         edtProposta_Perspectiva_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_Perspectiva_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_Perspectiva_Enabled), 5, 0)));
         edtProposta_RestricaoImplantacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_RestricaoImplantacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_RestricaoImplantacao_Enabled), 5, 0)));
         edtProposta_RestricaoCusto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_RestricaoCusto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_RestricaoCusto_Enabled), 5, 0)));
         chkProposta_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkProposta_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkProposta_Ativo.Enabled), 5, 0)));
         edtProposta_ContratadaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_ContratadaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_ContratadaCod_Enabled), 5, 0)));
         edtProposta_ContratadaRazSocial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_ContratadaRazSocial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_ContratadaRazSocial_Enabled), 5, 0)));
         edtProposta_Prazo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_Prazo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_Prazo_Enabled), 5, 0)));
         edtProposta_Esforco_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_Esforco_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_Esforco_Enabled), 5, 0)));
         edtProposta_CntSrvCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_CntSrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_CntSrvCod_Enabled), 5, 0)));
         edtProposta_OSCodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_OSCodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_OSCodigo_Enabled), 5, 0)));
         edtProposta_OSServico_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_OSServico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_OSServico_Enabled), 5, 0)));
         edtProposta_OSDmnFM_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_OSDmnFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_OSDmnFM_Enabled), 5, 0)));
         edtProposta_OSUndCntSgl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_OSUndCntSgl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_OSUndCntSgl_Enabled), 5, 0)));
         cmbProposta_OSPrzTpDias.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbProposta_OSPrzTpDias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbProposta_OSPrzTpDias.Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues480( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202062161804");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("proposta.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Proposta_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1685Proposta_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1685Proposta_Codigo), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1687Proposta_Vigencia", context.localUtil.TToC( Z1687Proposta_Vigencia, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1688Proposta_Valor", StringUtil.LTrim( StringUtil.NToC( Z1688Proposta_Valor, 15, 4, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1689Proposta_Status", StringUtil.RTrim( Z1689Proposta_Status));
         GxWebStd.gx_hidden_field( context, "Z1694Proposta_RestricaoImplantacao", context.localUtil.TToC( Z1694Proposta_RestricaoImplantacao, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1695Proposta_RestricaoCusto", StringUtil.LTrim( StringUtil.NToC( Z1695Proposta_RestricaoCusto, 15, 4, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1696Proposta_Ativo", Z1696Proposta_Ativo);
         GxWebStd.gx_hidden_field( context, "Z1699Proposta_ContratadaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1699Proposta_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1700Proposta_ContratadaRazSocial", Z1700Proposta_ContratadaRazSocial);
         GxWebStd.gx_hidden_field( context, "Z1702Proposta_Prazo", context.localUtil.DToC( Z1702Proposta_Prazo, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1793Proposta_PrazoDias", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1793Proposta_PrazoDias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1703Proposta_Esforco", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1703Proposta_Esforco), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1794Proposta_Liquido", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1794Proposta_Liquido), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1704Proposta_CntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1704Proposta_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1722Proposta_ValorUndCnt", StringUtil.LTrim( StringUtil.NToC( Z1722Proposta_ValorUndCnt, 10, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z648Projeto_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z648Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1686Proposta_OSCodigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1686Proposta_OSCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vPROPOSTA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Proposta_Codigo), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROPOSTA_PRAZODIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1793Proposta_PrazoDias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROPOSTA_LIQUIDO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1794Proposta_Liquido), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROPOSTA_VALORUNDCNT", StringUtil.LTrim( StringUtil.NToC( A1722Proposta_ValorUndCnt, 10, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROPOSTA_OSDMN", A1764Proposta_OSDmn);
         GxWebStd.gx_hidden_field( context, "PROPOSTA_OSINICIO", context.localUtil.DToC( A1792Proposta_OSInicio, 0, "/"));
         GxWebStd.gx_hidden_field( context, "PROPOSTA_OSPRZRSP", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1721Proposta_OSPrzRsp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROPOSTA_OSCNTSRVFTRM", A1763Proposta_OSCntSrvFtrm);
         GxWebStd.gx_hidden_field( context, "PROPOSTA_OSCNTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1715Proposta_OSCntCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROPOSTA_OSUNDCNTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1713Proposta_OSUndCntCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROPOSTA_OSPFB", StringUtil.LTrim( StringUtil.NToC( A1717Proposta_OSPFB, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROPOSTA_OSPFL", StringUtil.LTrim( StringUtil.NToC( A1718Proposta_OSPFL, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROPOSTA_OSDATACNT", context.localUtil.DToC( A1719Proposta_OSDataCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, "PROPOSTA_OSHORACNT", StringUtil.RTrim( A1720Proposta_OSHoraCnt));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Proposta";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1794Proposta_Liquido), "ZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1722Proposta_ValorUndCnt, "Z,ZZZ,ZZ9.99");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("proposta:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("proposta:[SendSecurityCheck value for]"+"Proposta_PrazoDias:"+context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9"));
         GXUtil.WriteLog("proposta:[SendSecurityCheck value for]"+"Proposta_Liquido:"+context.localUtil.Format( (decimal)(A1794Proposta_Liquido), "ZZZZZZZ9"));
         GXUtil.WriteLog("proposta:[SendSecurityCheck value for]"+"Proposta_ValorUndCnt:"+context.localUtil.Format( A1722Proposta_ValorUndCnt, "Z,ZZZ,ZZ9.99"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("proposta.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Proposta_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Proposta" ;
      }

      public override String GetPgmdesc( )
      {
         return "Proposta" ;
      }

      protected void InitializeNonKey48187( )
      {
         A648Projeto_Codigo = 0;
         n648Projeto_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         n648Projeto_Codigo = ((0==A648Projeto_Codigo) ? true : false);
         A1687Proposta_Vigencia = (DateTime)(DateTime.MinValue);
         n1687Proposta_Vigencia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1687Proposta_Vigencia", context.localUtil.TToC( A1687Proposta_Vigencia, 10, 8, 0, 3, "/", ":", " "));
         n1687Proposta_Vigencia = ((DateTime.MinValue==A1687Proposta_Vigencia) ? true : false);
         A1688Proposta_Valor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1688Proposta_Valor", StringUtil.LTrim( StringUtil.Str( A1688Proposta_Valor, 15, 4)));
         A1689Proposta_Status = "";
         n1689Proposta_Status = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1689Proposta_Status", A1689Proposta_Status);
         n1689Proposta_Status = (String.IsNullOrEmpty(StringUtil.RTrim( A1689Proposta_Status)) ? true : false);
         A5AreaTrabalho_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         A1690Proposta_Objetivo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1690Proposta_Objetivo", A1690Proposta_Objetivo);
         A1691Proposta_Escopo = "";
         n1691Proposta_Escopo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1691Proposta_Escopo", A1691Proposta_Escopo);
         n1691Proposta_Escopo = (String.IsNullOrEmpty(StringUtil.RTrim( A1691Proposta_Escopo)) ? true : false);
         A1692Proposta_Oportunidade = "";
         n1692Proposta_Oportunidade = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1692Proposta_Oportunidade", A1692Proposta_Oportunidade);
         n1692Proposta_Oportunidade = (String.IsNullOrEmpty(StringUtil.RTrim( A1692Proposta_Oportunidade)) ? true : false);
         A1693Proposta_Perspectiva = "";
         n1693Proposta_Perspectiva = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1693Proposta_Perspectiva", A1693Proposta_Perspectiva);
         n1693Proposta_Perspectiva = (String.IsNullOrEmpty(StringUtil.RTrim( A1693Proposta_Perspectiva)) ? true : false);
         A1694Proposta_RestricaoImplantacao = (DateTime)(DateTime.MinValue);
         n1694Proposta_RestricaoImplantacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1694Proposta_RestricaoImplantacao", context.localUtil.TToC( A1694Proposta_RestricaoImplantacao, 10, 8, 0, 3, "/", ":", " "));
         n1694Proposta_RestricaoImplantacao = ((DateTime.MinValue==A1694Proposta_RestricaoImplantacao) ? true : false);
         A1695Proposta_RestricaoCusto = 0;
         n1695Proposta_RestricaoCusto = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1695Proposta_RestricaoCusto", StringUtil.LTrim( StringUtil.Str( A1695Proposta_RestricaoCusto, 15, 4)));
         n1695Proposta_RestricaoCusto = ((Convert.ToDecimal(0)==A1695Proposta_RestricaoCusto) ? true : false);
         A1696Proposta_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1696Proposta_Ativo", A1696Proposta_Ativo);
         A1699Proposta_ContratadaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1699Proposta_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1699Proposta_ContratadaCod), 6, 0)));
         A1700Proposta_ContratadaRazSocial = "";
         n1700Proposta_ContratadaRazSocial = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1700Proposta_ContratadaRazSocial", A1700Proposta_ContratadaRazSocial);
         n1700Proposta_ContratadaRazSocial = (String.IsNullOrEmpty(StringUtil.RTrim( A1700Proposta_ContratadaRazSocial)) ? true : false);
         A1702Proposta_Prazo = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1702Proposta_Prazo", context.localUtil.Format(A1702Proposta_Prazo, "99/99/99"));
         A1793Proposta_PrazoDias = 0;
         n1793Proposta_PrazoDias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1793Proposta_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1793Proposta_PrazoDias), 4, 0)));
         A1703Proposta_Esforco = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1703Proposta_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A1703Proposta_Esforco), 8, 0)));
         A1794Proposta_Liquido = 0;
         n1794Proposta_Liquido = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1794Proposta_Liquido", StringUtil.LTrim( StringUtil.Str( (decimal)(A1794Proposta_Liquido), 8, 0)));
         A1704Proposta_CntSrvCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1704Proposta_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1704Proposta_CntSrvCod), 6, 0)));
         A1722Proposta_ValorUndCnt = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1722Proposta_ValorUndCnt", StringUtil.LTrim( StringUtil.Str( A1722Proposta_ValorUndCnt, 10, 2)));
         A1686Proposta_OSCodigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1686Proposta_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1686Proposta_OSCodigo), 6, 0)));
         A1705Proposta_OSServico = 0;
         n1705Proposta_OSServico = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1705Proposta_OSServico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1705Proposta_OSServico), 6, 0)));
         A1715Proposta_OSCntCod = 0;
         n1715Proposta_OSCntCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1715Proposta_OSCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1715Proposta_OSCntCod), 6, 0)));
         A1764Proposta_OSDmn = "";
         n1764Proposta_OSDmn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1764Proposta_OSDmn", A1764Proposta_OSDmn);
         A1711Proposta_OSDmnFM = "";
         n1711Proposta_OSDmnFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1711Proposta_OSDmnFM", A1711Proposta_OSDmnFM);
         A1713Proposta_OSUndCntCod = 0;
         n1713Proposta_OSUndCntCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1713Proposta_OSUndCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1713Proposta_OSUndCntCod), 6, 0)));
         A1710Proposta_OSUndCntSgl = "";
         n1710Proposta_OSUndCntSgl = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1710Proposta_OSUndCntSgl", A1710Proposta_OSUndCntSgl);
         A1707Proposta_OSPrzTpDias = "";
         n1707Proposta_OSPrzTpDias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1707Proposta_OSPrzTpDias", A1707Proposta_OSPrzTpDias);
         A1721Proposta_OSPrzRsp = 0;
         n1721Proposta_OSPrzRsp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1721Proposta_OSPrzRsp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1721Proposta_OSPrzRsp), 4, 0)));
         A1717Proposta_OSPFB = 0;
         n1717Proposta_OSPFB = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1717Proposta_OSPFB", StringUtil.LTrim( StringUtil.Str( A1717Proposta_OSPFB, 14, 5)));
         A1718Proposta_OSPFL = 0;
         n1718Proposta_OSPFL = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1718Proposta_OSPFL", StringUtil.LTrim( StringUtil.Str( A1718Proposta_OSPFL, 14, 5)));
         A1719Proposta_OSDataCnt = DateTime.MinValue;
         n1719Proposta_OSDataCnt = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1719Proposta_OSDataCnt", context.localUtil.Format(A1719Proposta_OSDataCnt, "99/99/99"));
         A1720Proposta_OSHoraCnt = "";
         n1720Proposta_OSHoraCnt = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1720Proposta_OSHoraCnt", A1720Proposta_OSHoraCnt);
         A1763Proposta_OSCntSrvFtrm = "";
         n1763Proposta_OSCntSrvFtrm = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1763Proposta_OSCntSrvFtrm", A1763Proposta_OSCntSrvFtrm);
         A1792Proposta_OSInicio = DateTime.MinValue;
         n1792Proposta_OSInicio = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1792Proposta_OSInicio", context.localUtil.Format(A1792Proposta_OSInicio, "99/99/99"));
         Z1687Proposta_Vigencia = (DateTime)(DateTime.MinValue);
         Z1688Proposta_Valor = 0;
         Z1689Proposta_Status = "";
         Z1694Proposta_RestricaoImplantacao = (DateTime)(DateTime.MinValue);
         Z1695Proposta_RestricaoCusto = 0;
         Z1696Proposta_Ativo = false;
         Z1699Proposta_ContratadaCod = 0;
         Z1700Proposta_ContratadaRazSocial = "";
         Z1702Proposta_Prazo = DateTime.MinValue;
         Z1793Proposta_PrazoDias = 0;
         Z1703Proposta_Esforco = 0;
         Z1794Proposta_Liquido = 0;
         Z1704Proposta_CntSrvCod = 0;
         Z1722Proposta_ValorUndCnt = 0;
         Z5AreaTrabalho_Codigo = 0;
         Z648Projeto_Codigo = 0;
         Z1686Proposta_OSCodigo = 0;
      }

      protected void InitAll48187( )
      {
         A1685Proposta_Codigo = 0;
         n1685Proposta_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1685Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1685Proposta_Codigo), 9, 0)));
         InitializeNonKey48187( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020621618047");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("proposta.js", "?2020621618047");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockproposta_codigo_Internalname = "TEXTBLOCKPROPOSTA_CODIGO";
         edtProposta_Codigo_Internalname = "PROPOSTA_CODIGO";
         lblTextblockprojeto_codigo_Internalname = "TEXTBLOCKPROJETO_CODIGO";
         dynProjeto_Codigo_Internalname = "PROJETO_CODIGO";
         lblTextblockproposta_vigencia_Internalname = "TEXTBLOCKPROPOSTA_VIGENCIA";
         edtProposta_Vigencia_Internalname = "PROPOSTA_VIGENCIA";
         lblTextblockproposta_valor_Internalname = "TEXTBLOCKPROPOSTA_VALOR";
         edtProposta_Valor_Internalname = "PROPOSTA_VALOR";
         lblTextblockproposta_status_Internalname = "TEXTBLOCKPROPOSTA_STATUS";
         edtProposta_Status_Internalname = "PROPOSTA_STATUS";
         lblTextblockareatrabalho_codigo_Internalname = "TEXTBLOCKAREATRABALHO_CODIGO";
         edtAreaTrabalho_Codigo_Internalname = "AREATRABALHO_CODIGO";
         lblTextblockproposta_objetivo_Internalname = "TEXTBLOCKPROPOSTA_OBJETIVO";
         edtProposta_Objetivo_Internalname = "PROPOSTA_OBJETIVO";
         lblTextblockproposta_escopo_Internalname = "TEXTBLOCKPROPOSTA_ESCOPO";
         edtProposta_Escopo_Internalname = "PROPOSTA_ESCOPO";
         lblTextblockproposta_oportunidade_Internalname = "TEXTBLOCKPROPOSTA_OPORTUNIDADE";
         edtProposta_Oportunidade_Internalname = "PROPOSTA_OPORTUNIDADE";
         lblTextblockproposta_perspectiva_Internalname = "TEXTBLOCKPROPOSTA_PERSPECTIVA";
         edtProposta_Perspectiva_Internalname = "PROPOSTA_PERSPECTIVA";
         lblTextblockproposta_restricaoimplantacao_Internalname = "TEXTBLOCKPROPOSTA_RESTRICAOIMPLANTACAO";
         edtProposta_RestricaoImplantacao_Internalname = "PROPOSTA_RESTRICAOIMPLANTACAO";
         lblTextblockproposta_restricaocusto_Internalname = "TEXTBLOCKPROPOSTA_RESTRICAOCUSTO";
         edtProposta_RestricaoCusto_Internalname = "PROPOSTA_RESTRICAOCUSTO";
         lblTextblockproposta_ativo_Internalname = "TEXTBLOCKPROPOSTA_ATIVO";
         chkProposta_Ativo_Internalname = "PROPOSTA_ATIVO";
         lblTextblockproposta_prestatoracod_Internalname = "TEXTBLOCKPROPOSTA_PRESTATORACOD";
         lblTextblockproposta_prestadorarazsocial_Internalname = "TEXTBLOCKPROPOSTA_PRESTADORARAZSOCIAL";
         lblTextblockproposta_contratadacod_Internalname = "TEXTBLOCKPROPOSTA_CONTRATADACOD";
         edtProposta_ContratadaCod_Internalname = "PROPOSTA_CONTRATADACOD";
         lblTextblockproposta_contratadarazsocial_Internalname = "TEXTBLOCKPROPOSTA_CONTRATADARAZSOCIAL";
         edtProposta_ContratadaRazSocial_Internalname = "PROPOSTA_CONTRATADARAZSOCIAL";
         lblTextblockproposta_prazo_Internalname = "TEXTBLOCKPROPOSTA_PRAZO";
         edtProposta_Prazo_Internalname = "PROPOSTA_PRAZO";
         lblTextblockproposta_esforco_Internalname = "TEXTBLOCKPROPOSTA_ESFORCO";
         edtProposta_Esforco_Internalname = "PROPOSTA_ESFORCO";
         lblTextblockproposta_cntsrvcod_Internalname = "TEXTBLOCKPROPOSTA_CNTSRVCOD";
         edtProposta_CntSrvCod_Internalname = "PROPOSTA_CNTSRVCOD";
         lblTextblockproposta_oscodigo_Internalname = "TEXTBLOCKPROPOSTA_OSCODIGO";
         edtProposta_OSCodigo_Internalname = "PROPOSTA_OSCODIGO";
         lblTextblockproposta_osservico_Internalname = "TEXTBLOCKPROPOSTA_OSSERVICO";
         edtProposta_OSServico_Internalname = "PROPOSTA_OSSERVICO";
         lblTextblockproposta_osdmnfm_Internalname = "TEXTBLOCKPROPOSTA_OSDMNFM";
         edtProposta_OSDmnFM_Internalname = "PROPOSTA_OSDMNFM";
         lblTextblockproposta_osundcntsgl_Internalname = "TEXTBLOCKPROPOSTA_OSUNDCNTSGL";
         edtProposta_OSUndCntSgl_Internalname = "PROPOSTA_OSUNDCNTSGL";
         lblTextblockproposta_osprztpdias_Internalname = "TEXTBLOCKPROPOSTA_OSPRZTPDIAS";
         cmbProposta_OSPrzTpDias_Internalname = "PROPOSTA_OSPRZTPDIAS";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Proposta";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Proposta";
         cmbProposta_OSPrzTpDias_Jsonclick = "";
         cmbProposta_OSPrzTpDias.Enabled = 0;
         edtProposta_OSUndCntSgl_Jsonclick = "";
         edtProposta_OSUndCntSgl_Enabled = 0;
         edtProposta_OSDmnFM_Jsonclick = "";
         edtProposta_OSDmnFM_Enabled = 0;
         edtProposta_OSServico_Jsonclick = "";
         edtProposta_OSServico_Enabled = 0;
         edtProposta_OSCodigo_Jsonclick = "";
         edtProposta_OSCodigo_Enabled = 1;
         edtProposta_CntSrvCod_Jsonclick = "";
         edtProposta_CntSrvCod_Enabled = 1;
         edtProposta_Esforco_Jsonclick = "";
         edtProposta_Esforco_Enabled = 1;
         edtProposta_Prazo_Jsonclick = "";
         edtProposta_Prazo_Enabled = 1;
         edtProposta_ContratadaRazSocial_Jsonclick = "";
         edtProposta_ContratadaRazSocial_Enabled = 1;
         edtProposta_ContratadaCod_Jsonclick = "";
         edtProposta_ContratadaCod_Enabled = 1;
         chkProposta_Ativo.Enabled = 1;
         edtProposta_RestricaoCusto_Jsonclick = "";
         edtProposta_RestricaoCusto_Enabled = 1;
         edtProposta_RestricaoImplantacao_Jsonclick = "";
         edtProposta_RestricaoImplantacao_Enabled = 1;
         edtProposta_Perspectiva_Enabled = 1;
         edtProposta_Oportunidade_Enabled = 1;
         edtProposta_Escopo_Enabled = 1;
         edtProposta_Objetivo_Enabled = 1;
         edtAreaTrabalho_Codigo_Jsonclick = "";
         edtAreaTrabalho_Codigo_Enabled = 1;
         edtProposta_Status_Jsonclick = "";
         edtProposta_Status_Enabled = 1;
         edtProposta_Valor_Jsonclick = "";
         edtProposta_Valor_Enabled = 1;
         edtProposta_Vigencia_Jsonclick = "";
         edtProposta_Vigencia_Enabled = 1;
         dynProjeto_Codigo_Jsonclick = "";
         dynProjeto_Codigo.Enabled = 1;
         edtProposta_Codigo_Jsonclick = "";
         edtProposta_Codigo_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         chkProposta_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAPROJETO_CODIGO481( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAPROJETO_CODIGO_data481( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAPROJETO_CODIGO_html481( )
      {
         int gxdynajaxvalue ;
         GXDLAPROJETO_CODIGO_data481( ) ;
         gxdynajaxindex = 1;
         dynProjeto_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynProjeto_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAPROJETO_CODIGO_data481( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T004855 */
         pr_default.execute(37);
         while ( (pr_default.getStatus(37) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T004855_A648Projeto_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T004855_A650Projeto_Sigla[0]));
            pr_default.readNext(37);
         }
         pr_default.close(37);
      }

      public void Valid_Projeto_codigo( GXCombobox dynGX_Parm1 )
      {
         dynProjeto_Codigo = dynGX_Parm1;
         A648Projeto_Codigo = (int)(NumberUtil.Val( dynProjeto_Codigo.CurrentValue, "."));
         n648Projeto_Codigo = false;
         /* Using cursor T004856 */
         pr_default.execute(38, new Object[] {n648Projeto_Codigo, A648Projeto_Codigo});
         if ( (pr_default.getStatus(38) == 101) )
         {
            if ( ! ( (0==A648Projeto_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto'.", "ForeignKeyNotFound", 1, "PROJETO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynProjeto_Codigo_Internalname;
            }
         }
         pr_default.close(38);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Areatrabalho_codigo( int GX_Parm1 )
      {
         A5AreaTrabalho_Codigo = GX_Parm1;
         /* Using cursor T004857 */
         pr_default.execute(39, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(39) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "AREATRABALHO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
         }
         pr_default.close(39);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Proposta_oscodigo( int GX_Parm1 ,
                                           int GX_Parm2 ,
                                           int GX_Parm3 ,
                                           String GX_Parm4 ,
                                           String GX_Parm5 ,
                                           DateTime GX_Parm6 ,
                                           GXCombobox cmbGX_Parm7 ,
                                           short GX_Parm8 ,
                                           String GX_Parm9 ,
                                           int GX_Parm10 ,
                                           String GX_Parm11 ,
                                           decimal GX_Parm12 ,
                                           decimal GX_Parm13 ,
                                           DateTime GX_Parm14 ,
                                           String GX_Parm15 )
      {
         A1686Proposta_OSCodigo = GX_Parm1;
         A1705Proposta_OSServico = GX_Parm2;
         n1705Proposta_OSServico = false;
         A1713Proposta_OSUndCntCod = GX_Parm3;
         n1713Proposta_OSUndCntCod = false;
         A1764Proposta_OSDmn = GX_Parm4;
         n1764Proposta_OSDmn = false;
         A1711Proposta_OSDmnFM = GX_Parm5;
         n1711Proposta_OSDmnFM = false;
         A1792Proposta_OSInicio = GX_Parm6;
         n1792Proposta_OSInicio = false;
         cmbProposta_OSPrzTpDias = cmbGX_Parm7;
         A1707Proposta_OSPrzTpDias = cmbProposta_OSPrzTpDias.CurrentValue;
         n1707Proposta_OSPrzTpDias = false;
         cmbProposta_OSPrzTpDias.CurrentValue = A1707Proposta_OSPrzTpDias;
         A1721Proposta_OSPrzRsp = GX_Parm8;
         n1721Proposta_OSPrzRsp = false;
         A1763Proposta_OSCntSrvFtrm = GX_Parm9;
         n1763Proposta_OSCntSrvFtrm = false;
         A1715Proposta_OSCntCod = GX_Parm10;
         n1715Proposta_OSCntCod = false;
         A1710Proposta_OSUndCntSgl = GX_Parm11;
         n1710Proposta_OSUndCntSgl = false;
         A1717Proposta_OSPFB = GX_Parm12;
         n1717Proposta_OSPFB = false;
         A1718Proposta_OSPFL = GX_Parm13;
         n1718Proposta_OSPFL = false;
         A1719Proposta_OSDataCnt = GX_Parm14;
         n1719Proposta_OSDataCnt = false;
         A1720Proposta_OSHoraCnt = GX_Parm15;
         n1720Proposta_OSHoraCnt = false;
         /* Using cursor T004842 */
         pr_default.execute(28, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(28) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Dados da OS'.", "ForeignKeyNotFound", 1, "PROPOSTA_OSCODIGO");
            AnyError = 1;
            GX_FocusControl = edtProposta_OSCodigo_Internalname;
         }
         A1764Proposta_OSDmn = T004842_A1764Proposta_OSDmn[0];
         n1764Proposta_OSDmn = T004842_n1764Proposta_OSDmn[0];
         A1711Proposta_OSDmnFM = T004842_A1711Proposta_OSDmnFM[0];
         n1711Proposta_OSDmnFM = T004842_n1711Proposta_OSDmnFM[0];
         A1792Proposta_OSInicio = T004842_A1792Proposta_OSInicio[0];
         n1792Proposta_OSInicio = T004842_n1792Proposta_OSInicio[0];
         A1705Proposta_OSServico = T004842_A1705Proposta_OSServico[0];
         n1705Proposta_OSServico = T004842_n1705Proposta_OSServico[0];
         pr_default.close(28);
         /* Using cursor T004843 */
         pr_default.execute(29, new Object[] {n1705Proposta_OSServico, A1705Proposta_OSServico});
         if ( (pr_default.getStatus(29) == 101) )
         {
            if ( ! ( (0==A1705Proposta_OSServico) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1707Proposta_OSPrzTpDias = T004843_A1707Proposta_OSPrzTpDias[0];
         cmbProposta_OSPrzTpDias.CurrentValue = A1707Proposta_OSPrzTpDias;
         n1707Proposta_OSPrzTpDias = T004843_n1707Proposta_OSPrzTpDias[0];
         A1721Proposta_OSPrzRsp = T004843_A1721Proposta_OSPrzRsp[0];
         n1721Proposta_OSPrzRsp = T004843_n1721Proposta_OSPrzRsp[0];
         A1763Proposta_OSCntSrvFtrm = T004843_A1763Proposta_OSCntSrvFtrm[0];
         n1763Proposta_OSCntSrvFtrm = T004843_n1763Proposta_OSCntSrvFtrm[0];
         A1715Proposta_OSCntCod = T004843_A1715Proposta_OSCntCod[0];
         n1715Proposta_OSCntCod = T004843_n1715Proposta_OSCntCod[0];
         A1713Proposta_OSUndCntCod = T004843_A1713Proposta_OSUndCntCod[0];
         n1713Proposta_OSUndCntCod = T004843_n1713Proposta_OSUndCntCod[0];
         pr_default.close(29);
         /* Using cursor T004844 */
         pr_default.execute(30, new Object[] {n1713Proposta_OSUndCntCod, A1713Proposta_OSUndCntCod});
         if ( (pr_default.getStatus(30) == 101) )
         {
            if ( ! ( (0==A1713Proposta_OSUndCntCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Unidades de Medi��o'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1710Proposta_OSUndCntSgl = T004844_A1710Proposta_OSUndCntSgl[0];
         n1710Proposta_OSUndCntSgl = T004844_n1710Proposta_OSUndCntSgl[0];
         pr_default.close(30);
         /* Using cursor T004846 */
         pr_default.execute(31, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(31) != 101) )
         {
            A1717Proposta_OSPFB = T004846_A1717Proposta_OSPFB[0];
            n1717Proposta_OSPFB = T004846_n1717Proposta_OSPFB[0];
         }
         else
         {
            A1717Proposta_OSPFB = 0;
            n1717Proposta_OSPFB = false;
         }
         pr_default.close(31);
         /* Using cursor T004848 */
         pr_default.execute(32, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(32) != 101) )
         {
            A1718Proposta_OSPFL = T004848_A1718Proposta_OSPFL[0];
            n1718Proposta_OSPFL = T004848_n1718Proposta_OSPFL[0];
         }
         else
         {
            A1718Proposta_OSPFL = 0;
            n1718Proposta_OSPFL = false;
         }
         pr_default.close(32);
         /* Using cursor T004850 */
         pr_default.execute(33, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(33) != 101) )
         {
            A1719Proposta_OSDataCnt = T004850_A1719Proposta_OSDataCnt[0];
            n1719Proposta_OSDataCnt = T004850_n1719Proposta_OSDataCnt[0];
         }
         else
         {
            A1719Proposta_OSDataCnt = DateTime.MinValue;
            n1719Proposta_OSDataCnt = false;
         }
         pr_default.close(33);
         /* Using cursor T004852 */
         pr_default.execute(34, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(34) != 101) )
         {
            A1720Proposta_OSHoraCnt = T004852_A1720Proposta_OSHoraCnt[0];
            n1720Proposta_OSHoraCnt = T004852_n1720Proposta_OSHoraCnt[0];
         }
         else
         {
            A1720Proposta_OSHoraCnt = "";
            n1720Proposta_OSHoraCnt = false;
         }
         pr_default.close(34);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1764Proposta_OSDmn = "";
            n1764Proposta_OSDmn = false;
            A1711Proposta_OSDmnFM = "";
            n1711Proposta_OSDmnFM = false;
            A1792Proposta_OSInicio = DateTime.MinValue;
            n1792Proposta_OSInicio = false;
            A1705Proposta_OSServico = 0;
            n1705Proposta_OSServico = false;
            A1707Proposta_OSPrzTpDias = "";
            n1707Proposta_OSPrzTpDias = false;
            cmbProposta_OSPrzTpDias.CurrentValue = A1707Proposta_OSPrzTpDias;
            A1721Proposta_OSPrzRsp = 0;
            n1721Proposta_OSPrzRsp = false;
            A1763Proposta_OSCntSrvFtrm = "";
            n1763Proposta_OSCntSrvFtrm = false;
            A1715Proposta_OSCntCod = 0;
            n1715Proposta_OSCntCod = false;
            A1713Proposta_OSUndCntCod = 0;
            n1713Proposta_OSUndCntCod = false;
            A1710Proposta_OSUndCntSgl = "";
            n1710Proposta_OSUndCntSgl = false;
            A1717Proposta_OSPFB = 0;
            n1717Proposta_OSPFB = false;
            A1718Proposta_OSPFL = 0;
            n1718Proposta_OSPFL = false;
            A1719Proposta_OSDataCnt = DateTime.MinValue;
            n1719Proposta_OSDataCnt = false;
            A1720Proposta_OSHoraCnt = "";
            n1720Proposta_OSHoraCnt = false;
         }
         isValidOutput.Add(A1764Proposta_OSDmn);
         isValidOutput.Add(A1711Proposta_OSDmnFM);
         isValidOutput.Add(context.localUtil.Format(A1792Proposta_OSInicio, "99/99/99"));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1705Proposta_OSServico), 6, 0, ".", "")));
         cmbProposta_OSPrzTpDias.CurrentValue = A1707Proposta_OSPrzTpDias;
         isValidOutput.Add(cmbProposta_OSPrzTpDias);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1721Proposta_OSPrzRsp), 4, 0, ".", "")));
         isValidOutput.Add(A1763Proposta_OSCntSrvFtrm);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1715Proposta_OSCntCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1713Proposta_OSUndCntCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1710Proposta_OSUndCntSgl));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A1717Proposta_OSPFB, 14, 5, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A1718Proposta_OSPFL, 14, 5, ".", "")));
         isValidOutput.Add(context.localUtil.Format(A1719Proposta_OSDataCnt, "99/99/99"));
         isValidOutput.Add(StringUtil.RTrim( A1720Proposta_OSHoraCnt));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',nv:''},{av:'AV7Proposta_Codigo',fld:'vPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12482',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(39);
         pr_default.close(38);
         pr_default.close(28);
         pr_default.close(29);
         pr_default.close(30);
         pr_default.close(31);
         pr_default.close(32);
         pr_default.close(33);
         pr_default.close(34);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1687Proposta_Vigencia = (DateTime)(DateTime.MinValue);
         Z1689Proposta_Status = "";
         Z1694Proposta_RestricaoImplantacao = (DateTime)(DateTime.MinValue);
         Z1700Proposta_ContratadaRazSocial = "";
         Z1702Proposta_Prazo = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         T004817_A648Projeto_Codigo = new int[1] ;
         T004817_n648Projeto_Codigo = new bool[] {false} ;
         T004817_A650Projeto_Sigla = new String[] {""} ;
         A1707Proposta_OSPrzTpDias = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockproposta_codigo_Jsonclick = "";
         lblTextblockprojeto_codigo_Jsonclick = "";
         lblTextblockproposta_vigencia_Jsonclick = "";
         A1687Proposta_Vigencia = (DateTime)(DateTime.MinValue);
         lblTextblockproposta_valor_Jsonclick = "";
         lblTextblockproposta_status_Jsonclick = "";
         A1689Proposta_Status = "";
         lblTextblockareatrabalho_codigo_Jsonclick = "";
         lblTextblockproposta_objetivo_Jsonclick = "";
         A1690Proposta_Objetivo = "";
         lblTextblockproposta_escopo_Jsonclick = "";
         A1691Proposta_Escopo = "";
         lblTextblockproposta_oportunidade_Jsonclick = "";
         A1692Proposta_Oportunidade = "";
         lblTextblockproposta_perspectiva_Jsonclick = "";
         A1693Proposta_Perspectiva = "";
         lblTextblockproposta_restricaoimplantacao_Jsonclick = "";
         A1694Proposta_RestricaoImplantacao = (DateTime)(DateTime.MinValue);
         lblTextblockproposta_restricaocusto_Jsonclick = "";
         lblTextblockproposta_ativo_Jsonclick = "";
         lblTextblockproposta_prestatoracod_Jsonclick = "";
         lblTextblockproposta_prestadorarazsocial_Jsonclick = "";
         lblTextblockproposta_contratadacod_Jsonclick = "";
         lblTextblockproposta_contratadarazsocial_Jsonclick = "";
         A1700Proposta_ContratadaRazSocial = "";
         lblTextblockproposta_prazo_Jsonclick = "";
         A1702Proposta_Prazo = DateTime.MinValue;
         lblTextblockproposta_esforco_Jsonclick = "";
         lblTextblockproposta_cntsrvcod_Jsonclick = "";
         lblTextblockproposta_oscodigo_Jsonclick = "";
         lblTextblockproposta_osservico_Jsonclick = "";
         lblTextblockproposta_osdmnfm_Jsonclick = "";
         A1711Proposta_OSDmnFM = "";
         lblTextblockproposta_osundcntsgl_Jsonclick = "";
         A1710Proposta_OSUndCntSgl = "";
         lblTextblockproposta_osprztpdias_Jsonclick = "";
         A1764Proposta_OSDmn = "";
         A1792Proposta_OSInicio = DateTime.MinValue;
         A1763Proposta_OSCntSrvFtrm = "";
         A1719Proposta_OSDataCnt = DateTime.MinValue;
         A1720Proposta_OSHoraCnt = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode187 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1690Proposta_Objetivo = "";
         Z1691Proposta_Escopo = "";
         Z1692Proposta_Oportunidade = "";
         Z1693Proposta_Perspectiva = "";
         Z1764Proposta_OSDmn = "";
         Z1711Proposta_OSDmnFM = "";
         Z1792Proposta_OSInicio = DateTime.MinValue;
         Z1707Proposta_OSPrzTpDias = "";
         Z1763Proposta_OSCntSrvFtrm = "";
         Z1710Proposta_OSUndCntSgl = "";
         Z1719Proposta_OSDataCnt = DateTime.MinValue;
         Z1720Proposta_OSHoraCnt = "";
         T004822_A1685Proposta_Codigo = new int[1] ;
         T004822_n1685Proposta_Codigo = new bool[] {false} ;
         T004822_A1687Proposta_Vigencia = new DateTime[] {DateTime.MinValue} ;
         T004822_n1687Proposta_Vigencia = new bool[] {false} ;
         T004822_A1688Proposta_Valor = new decimal[1] ;
         T004822_A1689Proposta_Status = new String[] {""} ;
         T004822_n1689Proposta_Status = new bool[] {false} ;
         T004822_A1690Proposta_Objetivo = new String[] {""} ;
         T004822_A1691Proposta_Escopo = new String[] {""} ;
         T004822_n1691Proposta_Escopo = new bool[] {false} ;
         T004822_A1692Proposta_Oportunidade = new String[] {""} ;
         T004822_n1692Proposta_Oportunidade = new bool[] {false} ;
         T004822_A1693Proposta_Perspectiva = new String[] {""} ;
         T004822_n1693Proposta_Perspectiva = new bool[] {false} ;
         T004822_A1694Proposta_RestricaoImplantacao = new DateTime[] {DateTime.MinValue} ;
         T004822_n1694Proposta_RestricaoImplantacao = new bool[] {false} ;
         T004822_A1695Proposta_RestricaoCusto = new decimal[1] ;
         T004822_n1695Proposta_RestricaoCusto = new bool[] {false} ;
         T004822_A1696Proposta_Ativo = new bool[] {false} ;
         T004822_A1699Proposta_ContratadaCod = new int[1] ;
         T004822_A1700Proposta_ContratadaRazSocial = new String[] {""} ;
         T004822_n1700Proposta_ContratadaRazSocial = new bool[] {false} ;
         T004822_A1702Proposta_Prazo = new DateTime[] {DateTime.MinValue} ;
         T004822_A1793Proposta_PrazoDias = new short[1] ;
         T004822_n1793Proposta_PrazoDias = new bool[] {false} ;
         T004822_A1703Proposta_Esforco = new int[1] ;
         T004822_A1794Proposta_Liquido = new int[1] ;
         T004822_n1794Proposta_Liquido = new bool[] {false} ;
         T004822_A1704Proposta_CntSrvCod = new int[1] ;
         T004822_A1722Proposta_ValorUndCnt = new decimal[1] ;
         T004822_A1764Proposta_OSDmn = new String[] {""} ;
         T004822_n1764Proposta_OSDmn = new bool[] {false} ;
         T004822_A1711Proposta_OSDmnFM = new String[] {""} ;
         T004822_n1711Proposta_OSDmnFM = new bool[] {false} ;
         T004822_A1710Proposta_OSUndCntSgl = new String[] {""} ;
         T004822_n1710Proposta_OSUndCntSgl = new bool[] {false} ;
         T004822_A1707Proposta_OSPrzTpDias = new String[] {""} ;
         T004822_n1707Proposta_OSPrzTpDias = new bool[] {false} ;
         T004822_A1721Proposta_OSPrzRsp = new short[1] ;
         T004822_n1721Proposta_OSPrzRsp = new bool[] {false} ;
         T004822_A1763Proposta_OSCntSrvFtrm = new String[] {""} ;
         T004822_n1763Proposta_OSCntSrvFtrm = new bool[] {false} ;
         T004822_A1792Proposta_OSInicio = new DateTime[] {DateTime.MinValue} ;
         T004822_n1792Proposta_OSInicio = new bool[] {false} ;
         T004822_A5AreaTrabalho_Codigo = new int[1] ;
         T004822_A648Projeto_Codigo = new int[1] ;
         T004822_n648Projeto_Codigo = new bool[] {false} ;
         T004822_A1686Proposta_OSCodigo = new int[1] ;
         T004822_A1705Proposta_OSServico = new int[1] ;
         T004822_n1705Proposta_OSServico = new bool[] {false} ;
         T004822_A1715Proposta_OSCntCod = new int[1] ;
         T004822_n1715Proposta_OSCntCod = new bool[] {false} ;
         T004822_A1713Proposta_OSUndCntCod = new int[1] ;
         T004822_n1713Proposta_OSUndCntCod = new bool[] {false} ;
         T004822_A1717Proposta_OSPFB = new decimal[1] ;
         T004822_n1717Proposta_OSPFB = new bool[] {false} ;
         T004822_A1718Proposta_OSPFL = new decimal[1] ;
         T004822_n1718Proposta_OSPFL = new bool[] {false} ;
         T004822_A1719Proposta_OSDataCnt = new DateTime[] {DateTime.MinValue} ;
         T004822_n1719Proposta_OSDataCnt = new bool[] {false} ;
         T004822_A1720Proposta_OSHoraCnt = new String[] {""} ;
         T004822_n1720Proposta_OSHoraCnt = new bool[] {false} ;
         T00485_A648Projeto_Codigo = new int[1] ;
         T00485_n648Projeto_Codigo = new bool[] {false} ;
         T00484_A5AreaTrabalho_Codigo = new int[1] ;
         T00486_A1764Proposta_OSDmn = new String[] {""} ;
         T00486_n1764Proposta_OSDmn = new bool[] {false} ;
         T00486_A1711Proposta_OSDmnFM = new String[] {""} ;
         T00486_n1711Proposta_OSDmnFM = new bool[] {false} ;
         T00486_A1792Proposta_OSInicio = new DateTime[] {DateTime.MinValue} ;
         T00486_n1792Proposta_OSInicio = new bool[] {false} ;
         T00486_A1705Proposta_OSServico = new int[1] ;
         T00486_n1705Proposta_OSServico = new bool[] {false} ;
         T00487_A1707Proposta_OSPrzTpDias = new String[] {""} ;
         T00487_n1707Proposta_OSPrzTpDias = new bool[] {false} ;
         T00487_A1721Proposta_OSPrzRsp = new short[1] ;
         T00487_n1721Proposta_OSPrzRsp = new bool[] {false} ;
         T00487_A1763Proposta_OSCntSrvFtrm = new String[] {""} ;
         T00487_n1763Proposta_OSCntSrvFtrm = new bool[] {false} ;
         T00487_A1715Proposta_OSCntCod = new int[1] ;
         T00487_n1715Proposta_OSCntCod = new bool[] {false} ;
         T00487_A1713Proposta_OSUndCntCod = new int[1] ;
         T00487_n1713Proposta_OSUndCntCod = new bool[] {false} ;
         T00488_A1710Proposta_OSUndCntSgl = new String[] {""} ;
         T00488_n1710Proposta_OSUndCntSgl = new bool[] {false} ;
         T004810_A1717Proposta_OSPFB = new decimal[1] ;
         T004810_n1717Proposta_OSPFB = new bool[] {false} ;
         T004812_A1718Proposta_OSPFL = new decimal[1] ;
         T004812_n1718Proposta_OSPFL = new bool[] {false} ;
         T004814_A1719Proposta_OSDataCnt = new DateTime[] {DateTime.MinValue} ;
         T004814_n1719Proposta_OSDataCnt = new bool[] {false} ;
         T004816_A1720Proposta_OSHoraCnt = new String[] {""} ;
         T004816_n1720Proposta_OSHoraCnt = new bool[] {false} ;
         T004823_A648Projeto_Codigo = new int[1] ;
         T004823_n648Projeto_Codigo = new bool[] {false} ;
         T004824_A5AreaTrabalho_Codigo = new int[1] ;
         T004825_A1764Proposta_OSDmn = new String[] {""} ;
         T004825_n1764Proposta_OSDmn = new bool[] {false} ;
         T004825_A1711Proposta_OSDmnFM = new String[] {""} ;
         T004825_n1711Proposta_OSDmnFM = new bool[] {false} ;
         T004825_A1792Proposta_OSInicio = new DateTime[] {DateTime.MinValue} ;
         T004825_n1792Proposta_OSInicio = new bool[] {false} ;
         T004825_A1705Proposta_OSServico = new int[1] ;
         T004825_n1705Proposta_OSServico = new bool[] {false} ;
         T004826_A1707Proposta_OSPrzTpDias = new String[] {""} ;
         T004826_n1707Proposta_OSPrzTpDias = new bool[] {false} ;
         T004826_A1721Proposta_OSPrzRsp = new short[1] ;
         T004826_n1721Proposta_OSPrzRsp = new bool[] {false} ;
         T004826_A1763Proposta_OSCntSrvFtrm = new String[] {""} ;
         T004826_n1763Proposta_OSCntSrvFtrm = new bool[] {false} ;
         T004826_A1715Proposta_OSCntCod = new int[1] ;
         T004826_n1715Proposta_OSCntCod = new bool[] {false} ;
         T004826_A1713Proposta_OSUndCntCod = new int[1] ;
         T004826_n1713Proposta_OSUndCntCod = new bool[] {false} ;
         T004827_A1710Proposta_OSUndCntSgl = new String[] {""} ;
         T004827_n1710Proposta_OSUndCntSgl = new bool[] {false} ;
         T004829_A1717Proposta_OSPFB = new decimal[1] ;
         T004829_n1717Proposta_OSPFB = new bool[] {false} ;
         T004831_A1718Proposta_OSPFL = new decimal[1] ;
         T004831_n1718Proposta_OSPFL = new bool[] {false} ;
         T004833_A1719Proposta_OSDataCnt = new DateTime[] {DateTime.MinValue} ;
         T004833_n1719Proposta_OSDataCnt = new bool[] {false} ;
         T004835_A1720Proposta_OSHoraCnt = new String[] {""} ;
         T004835_n1720Proposta_OSHoraCnt = new bool[] {false} ;
         T004836_A1685Proposta_Codigo = new int[1] ;
         T004836_n1685Proposta_Codigo = new bool[] {false} ;
         T00483_A1685Proposta_Codigo = new int[1] ;
         T00483_n1685Proposta_Codigo = new bool[] {false} ;
         T00483_A1687Proposta_Vigencia = new DateTime[] {DateTime.MinValue} ;
         T00483_n1687Proposta_Vigencia = new bool[] {false} ;
         T00483_A1688Proposta_Valor = new decimal[1] ;
         T00483_A1689Proposta_Status = new String[] {""} ;
         T00483_n1689Proposta_Status = new bool[] {false} ;
         T00483_A1690Proposta_Objetivo = new String[] {""} ;
         T00483_A1691Proposta_Escopo = new String[] {""} ;
         T00483_n1691Proposta_Escopo = new bool[] {false} ;
         T00483_A1692Proposta_Oportunidade = new String[] {""} ;
         T00483_n1692Proposta_Oportunidade = new bool[] {false} ;
         T00483_A1693Proposta_Perspectiva = new String[] {""} ;
         T00483_n1693Proposta_Perspectiva = new bool[] {false} ;
         T00483_A1694Proposta_RestricaoImplantacao = new DateTime[] {DateTime.MinValue} ;
         T00483_n1694Proposta_RestricaoImplantacao = new bool[] {false} ;
         T00483_A1695Proposta_RestricaoCusto = new decimal[1] ;
         T00483_n1695Proposta_RestricaoCusto = new bool[] {false} ;
         T00483_A1696Proposta_Ativo = new bool[] {false} ;
         T00483_A1699Proposta_ContratadaCod = new int[1] ;
         T00483_A1700Proposta_ContratadaRazSocial = new String[] {""} ;
         T00483_n1700Proposta_ContratadaRazSocial = new bool[] {false} ;
         T00483_A1702Proposta_Prazo = new DateTime[] {DateTime.MinValue} ;
         T00483_A1793Proposta_PrazoDias = new short[1] ;
         T00483_n1793Proposta_PrazoDias = new bool[] {false} ;
         T00483_A1703Proposta_Esforco = new int[1] ;
         T00483_A1794Proposta_Liquido = new int[1] ;
         T00483_n1794Proposta_Liquido = new bool[] {false} ;
         T00483_A1704Proposta_CntSrvCod = new int[1] ;
         T00483_A1722Proposta_ValorUndCnt = new decimal[1] ;
         T00483_A5AreaTrabalho_Codigo = new int[1] ;
         T00483_A648Projeto_Codigo = new int[1] ;
         T00483_n648Projeto_Codigo = new bool[] {false} ;
         T00483_A1686Proposta_OSCodigo = new int[1] ;
         T004837_A1685Proposta_Codigo = new int[1] ;
         T004837_n1685Proposta_Codigo = new bool[] {false} ;
         T004838_A1685Proposta_Codigo = new int[1] ;
         T004838_n1685Proposta_Codigo = new bool[] {false} ;
         T00482_A1685Proposta_Codigo = new int[1] ;
         T00482_n1685Proposta_Codigo = new bool[] {false} ;
         T00482_A1687Proposta_Vigencia = new DateTime[] {DateTime.MinValue} ;
         T00482_n1687Proposta_Vigencia = new bool[] {false} ;
         T00482_A1688Proposta_Valor = new decimal[1] ;
         T00482_A1689Proposta_Status = new String[] {""} ;
         T00482_n1689Proposta_Status = new bool[] {false} ;
         T00482_A1690Proposta_Objetivo = new String[] {""} ;
         T00482_A1691Proposta_Escopo = new String[] {""} ;
         T00482_n1691Proposta_Escopo = new bool[] {false} ;
         T00482_A1692Proposta_Oportunidade = new String[] {""} ;
         T00482_n1692Proposta_Oportunidade = new bool[] {false} ;
         T00482_A1693Proposta_Perspectiva = new String[] {""} ;
         T00482_n1693Proposta_Perspectiva = new bool[] {false} ;
         T00482_A1694Proposta_RestricaoImplantacao = new DateTime[] {DateTime.MinValue} ;
         T00482_n1694Proposta_RestricaoImplantacao = new bool[] {false} ;
         T00482_A1695Proposta_RestricaoCusto = new decimal[1] ;
         T00482_n1695Proposta_RestricaoCusto = new bool[] {false} ;
         T00482_A1696Proposta_Ativo = new bool[] {false} ;
         T00482_A1699Proposta_ContratadaCod = new int[1] ;
         T00482_A1700Proposta_ContratadaRazSocial = new String[] {""} ;
         T00482_n1700Proposta_ContratadaRazSocial = new bool[] {false} ;
         T00482_A1702Proposta_Prazo = new DateTime[] {DateTime.MinValue} ;
         T00482_A1793Proposta_PrazoDias = new short[1] ;
         T00482_n1793Proposta_PrazoDias = new bool[] {false} ;
         T00482_A1703Proposta_Esforco = new int[1] ;
         T00482_A1794Proposta_Liquido = new int[1] ;
         T00482_n1794Proposta_Liquido = new bool[] {false} ;
         T00482_A1704Proposta_CntSrvCod = new int[1] ;
         T00482_A1722Proposta_ValorUndCnt = new decimal[1] ;
         T00482_A5AreaTrabalho_Codigo = new int[1] ;
         T00482_A648Projeto_Codigo = new int[1] ;
         T00482_n648Projeto_Codigo = new bool[] {false} ;
         T00482_A1686Proposta_OSCodigo = new int[1] ;
         T004839_A1685Proposta_Codigo = new int[1] ;
         T004839_n1685Proposta_Codigo = new bool[] {false} ;
         T004842_A1764Proposta_OSDmn = new String[] {""} ;
         T004842_n1764Proposta_OSDmn = new bool[] {false} ;
         T004842_A1711Proposta_OSDmnFM = new String[] {""} ;
         T004842_n1711Proposta_OSDmnFM = new bool[] {false} ;
         T004842_A1792Proposta_OSInicio = new DateTime[] {DateTime.MinValue} ;
         T004842_n1792Proposta_OSInicio = new bool[] {false} ;
         T004842_A1705Proposta_OSServico = new int[1] ;
         T004842_n1705Proposta_OSServico = new bool[] {false} ;
         T004843_A1707Proposta_OSPrzTpDias = new String[] {""} ;
         T004843_n1707Proposta_OSPrzTpDias = new bool[] {false} ;
         T004843_A1721Proposta_OSPrzRsp = new short[1] ;
         T004843_n1721Proposta_OSPrzRsp = new bool[] {false} ;
         T004843_A1763Proposta_OSCntSrvFtrm = new String[] {""} ;
         T004843_n1763Proposta_OSCntSrvFtrm = new bool[] {false} ;
         T004843_A1715Proposta_OSCntCod = new int[1] ;
         T004843_n1715Proposta_OSCntCod = new bool[] {false} ;
         T004843_A1713Proposta_OSUndCntCod = new int[1] ;
         T004843_n1713Proposta_OSUndCntCod = new bool[] {false} ;
         T004844_A1710Proposta_OSUndCntSgl = new String[] {""} ;
         T004844_n1710Proposta_OSUndCntSgl = new bool[] {false} ;
         T004846_A1717Proposta_OSPFB = new decimal[1] ;
         T004846_n1717Proposta_OSPFB = new bool[] {false} ;
         T004848_A1718Proposta_OSPFL = new decimal[1] ;
         T004848_n1718Proposta_OSPFL = new bool[] {false} ;
         T004850_A1719Proposta_OSDataCnt = new DateTime[] {DateTime.MinValue} ;
         T004850_n1719Proposta_OSDataCnt = new bool[] {false} ;
         T004852_A1720Proposta_OSHoraCnt = new String[] {""} ;
         T004852_n1720Proposta_OSHoraCnt = new bool[] {false} ;
         T004853_A1919Requisito_Codigo = new int[1] ;
         T004854_A1685Proposta_Codigo = new int[1] ;
         T004854_n1685Proposta_Codigo = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T004855_A648Projeto_Codigo = new int[1] ;
         T004855_n648Projeto_Codigo = new bool[] {false} ;
         T004855_A650Projeto_Sigla = new String[] {""} ;
         T004856_A648Projeto_Codigo = new int[1] ;
         T004856_n648Projeto_Codigo = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T004857_A5AreaTrabalho_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.proposta__default(),
            new Object[][] {
                new Object[] {
               T00482_A1685Proposta_Codigo, T00482_A1687Proposta_Vigencia, T00482_n1687Proposta_Vigencia, T00482_A1688Proposta_Valor, T00482_A1689Proposta_Status, T00482_n1689Proposta_Status, T00482_A1690Proposta_Objetivo, T00482_A1691Proposta_Escopo, T00482_n1691Proposta_Escopo, T00482_A1692Proposta_Oportunidade,
               T00482_n1692Proposta_Oportunidade, T00482_A1693Proposta_Perspectiva, T00482_n1693Proposta_Perspectiva, T00482_A1694Proposta_RestricaoImplantacao, T00482_n1694Proposta_RestricaoImplantacao, T00482_A1695Proposta_RestricaoCusto, T00482_n1695Proposta_RestricaoCusto, T00482_A1696Proposta_Ativo, T00482_A1699Proposta_ContratadaCod, T00482_A1700Proposta_ContratadaRazSocial,
               T00482_n1700Proposta_ContratadaRazSocial, T00482_A1702Proposta_Prazo, T00482_A1793Proposta_PrazoDias, T00482_n1793Proposta_PrazoDias, T00482_A1703Proposta_Esforco, T00482_A1794Proposta_Liquido, T00482_n1794Proposta_Liquido, T00482_A1704Proposta_CntSrvCod, T00482_A1722Proposta_ValorUndCnt, T00482_A5AreaTrabalho_Codigo,
               T00482_A648Projeto_Codigo, T00482_n648Projeto_Codigo, T00482_A1686Proposta_OSCodigo
               }
               , new Object[] {
               T00483_A1685Proposta_Codigo, T00483_A1687Proposta_Vigencia, T00483_n1687Proposta_Vigencia, T00483_A1688Proposta_Valor, T00483_A1689Proposta_Status, T00483_n1689Proposta_Status, T00483_A1690Proposta_Objetivo, T00483_A1691Proposta_Escopo, T00483_n1691Proposta_Escopo, T00483_A1692Proposta_Oportunidade,
               T00483_n1692Proposta_Oportunidade, T00483_A1693Proposta_Perspectiva, T00483_n1693Proposta_Perspectiva, T00483_A1694Proposta_RestricaoImplantacao, T00483_n1694Proposta_RestricaoImplantacao, T00483_A1695Proposta_RestricaoCusto, T00483_n1695Proposta_RestricaoCusto, T00483_A1696Proposta_Ativo, T00483_A1699Proposta_ContratadaCod, T00483_A1700Proposta_ContratadaRazSocial,
               T00483_n1700Proposta_ContratadaRazSocial, T00483_A1702Proposta_Prazo, T00483_A1793Proposta_PrazoDias, T00483_n1793Proposta_PrazoDias, T00483_A1703Proposta_Esforco, T00483_A1794Proposta_Liquido, T00483_n1794Proposta_Liquido, T00483_A1704Proposta_CntSrvCod, T00483_A1722Proposta_ValorUndCnt, T00483_A5AreaTrabalho_Codigo,
               T00483_A648Projeto_Codigo, T00483_n648Projeto_Codigo, T00483_A1686Proposta_OSCodigo
               }
               , new Object[] {
               T00484_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               T00485_A648Projeto_Codigo
               }
               , new Object[] {
               T00486_A1764Proposta_OSDmn, T00486_n1764Proposta_OSDmn, T00486_A1711Proposta_OSDmnFM, T00486_n1711Proposta_OSDmnFM, T00486_A1792Proposta_OSInicio, T00486_n1792Proposta_OSInicio, T00486_A1705Proposta_OSServico, T00486_n1705Proposta_OSServico
               }
               , new Object[] {
               T00487_A1707Proposta_OSPrzTpDias, T00487_n1707Proposta_OSPrzTpDias, T00487_A1721Proposta_OSPrzRsp, T00487_n1721Proposta_OSPrzRsp, T00487_A1763Proposta_OSCntSrvFtrm, T00487_n1763Proposta_OSCntSrvFtrm, T00487_A1715Proposta_OSCntCod, T00487_n1715Proposta_OSCntCod, T00487_A1713Proposta_OSUndCntCod, T00487_n1713Proposta_OSUndCntCod
               }
               , new Object[] {
               T00488_A1710Proposta_OSUndCntSgl, T00488_n1710Proposta_OSUndCntSgl
               }
               , new Object[] {
               T004810_A1717Proposta_OSPFB, T004810_n1717Proposta_OSPFB
               }
               , new Object[] {
               T004812_A1718Proposta_OSPFL, T004812_n1718Proposta_OSPFL
               }
               , new Object[] {
               T004814_A1719Proposta_OSDataCnt, T004814_n1719Proposta_OSDataCnt
               }
               , new Object[] {
               T004816_A1720Proposta_OSHoraCnt, T004816_n1720Proposta_OSHoraCnt
               }
               , new Object[] {
               T004817_A648Projeto_Codigo, T004817_A650Projeto_Sigla
               }
               , new Object[] {
               T004822_A1685Proposta_Codigo, T004822_A1687Proposta_Vigencia, T004822_n1687Proposta_Vigencia, T004822_A1688Proposta_Valor, T004822_A1689Proposta_Status, T004822_n1689Proposta_Status, T004822_A1690Proposta_Objetivo, T004822_A1691Proposta_Escopo, T004822_n1691Proposta_Escopo, T004822_A1692Proposta_Oportunidade,
               T004822_n1692Proposta_Oportunidade, T004822_A1693Proposta_Perspectiva, T004822_n1693Proposta_Perspectiva, T004822_A1694Proposta_RestricaoImplantacao, T004822_n1694Proposta_RestricaoImplantacao, T004822_A1695Proposta_RestricaoCusto, T004822_n1695Proposta_RestricaoCusto, T004822_A1696Proposta_Ativo, T004822_A1699Proposta_ContratadaCod, T004822_A1700Proposta_ContratadaRazSocial,
               T004822_n1700Proposta_ContratadaRazSocial, T004822_A1702Proposta_Prazo, T004822_A1793Proposta_PrazoDias, T004822_n1793Proposta_PrazoDias, T004822_A1703Proposta_Esforco, T004822_A1794Proposta_Liquido, T004822_n1794Proposta_Liquido, T004822_A1704Proposta_CntSrvCod, T004822_A1722Proposta_ValorUndCnt, T004822_A1764Proposta_OSDmn,
               T004822_n1764Proposta_OSDmn, T004822_A1711Proposta_OSDmnFM, T004822_n1711Proposta_OSDmnFM, T004822_A1710Proposta_OSUndCntSgl, T004822_n1710Proposta_OSUndCntSgl, T004822_A1707Proposta_OSPrzTpDias, T004822_n1707Proposta_OSPrzTpDias, T004822_A1721Proposta_OSPrzRsp, T004822_n1721Proposta_OSPrzRsp, T004822_A1763Proposta_OSCntSrvFtrm,
               T004822_n1763Proposta_OSCntSrvFtrm, T004822_A1792Proposta_OSInicio, T004822_n1792Proposta_OSInicio, T004822_A5AreaTrabalho_Codigo, T004822_A648Projeto_Codigo, T004822_n648Projeto_Codigo, T004822_A1686Proposta_OSCodigo, T004822_A1705Proposta_OSServico, T004822_n1705Proposta_OSServico, T004822_A1715Proposta_OSCntCod,
               T004822_n1715Proposta_OSCntCod, T004822_A1713Proposta_OSUndCntCod, T004822_n1713Proposta_OSUndCntCod, T004822_A1717Proposta_OSPFB, T004822_n1717Proposta_OSPFB, T004822_A1718Proposta_OSPFL, T004822_n1718Proposta_OSPFL, T004822_A1719Proposta_OSDataCnt, T004822_n1719Proposta_OSDataCnt, T004822_A1720Proposta_OSHoraCnt,
               T004822_n1720Proposta_OSHoraCnt
               }
               , new Object[] {
               T004823_A648Projeto_Codigo
               }
               , new Object[] {
               T004824_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               T004825_A1764Proposta_OSDmn, T004825_n1764Proposta_OSDmn, T004825_A1711Proposta_OSDmnFM, T004825_n1711Proposta_OSDmnFM, T004825_A1792Proposta_OSInicio, T004825_n1792Proposta_OSInicio, T004825_A1705Proposta_OSServico, T004825_n1705Proposta_OSServico
               }
               , new Object[] {
               T004826_A1707Proposta_OSPrzTpDias, T004826_n1707Proposta_OSPrzTpDias, T004826_A1721Proposta_OSPrzRsp, T004826_n1721Proposta_OSPrzRsp, T004826_A1763Proposta_OSCntSrvFtrm, T004826_n1763Proposta_OSCntSrvFtrm, T004826_A1715Proposta_OSCntCod, T004826_n1715Proposta_OSCntCod, T004826_A1713Proposta_OSUndCntCod, T004826_n1713Proposta_OSUndCntCod
               }
               , new Object[] {
               T004827_A1710Proposta_OSUndCntSgl, T004827_n1710Proposta_OSUndCntSgl
               }
               , new Object[] {
               T004829_A1717Proposta_OSPFB, T004829_n1717Proposta_OSPFB
               }
               , new Object[] {
               T004831_A1718Proposta_OSPFL, T004831_n1718Proposta_OSPFL
               }
               , new Object[] {
               T004833_A1719Proposta_OSDataCnt, T004833_n1719Proposta_OSDataCnt
               }
               , new Object[] {
               T004835_A1720Proposta_OSHoraCnt, T004835_n1720Proposta_OSHoraCnt
               }
               , new Object[] {
               T004836_A1685Proposta_Codigo
               }
               , new Object[] {
               T004837_A1685Proposta_Codigo
               }
               , new Object[] {
               T004838_A1685Proposta_Codigo
               }
               , new Object[] {
               T004839_A1685Proposta_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004842_A1764Proposta_OSDmn, T004842_n1764Proposta_OSDmn, T004842_A1711Proposta_OSDmnFM, T004842_n1711Proposta_OSDmnFM, T004842_A1792Proposta_OSInicio, T004842_n1792Proposta_OSInicio, T004842_A1705Proposta_OSServico, T004842_n1705Proposta_OSServico
               }
               , new Object[] {
               T004843_A1707Proposta_OSPrzTpDias, T004843_n1707Proposta_OSPrzTpDias, T004843_A1721Proposta_OSPrzRsp, T004843_n1721Proposta_OSPrzRsp, T004843_A1763Proposta_OSCntSrvFtrm, T004843_n1763Proposta_OSCntSrvFtrm, T004843_A1715Proposta_OSCntCod, T004843_n1715Proposta_OSCntCod, T004843_A1713Proposta_OSUndCntCod, T004843_n1713Proposta_OSUndCntCod
               }
               , new Object[] {
               T004844_A1710Proposta_OSUndCntSgl, T004844_n1710Proposta_OSUndCntSgl
               }
               , new Object[] {
               T004846_A1717Proposta_OSPFB, T004846_n1717Proposta_OSPFB
               }
               , new Object[] {
               T004848_A1718Proposta_OSPFL, T004848_n1718Proposta_OSPFL
               }
               , new Object[] {
               T004850_A1719Proposta_OSDataCnt, T004850_n1719Proposta_OSDataCnt
               }
               , new Object[] {
               T004852_A1720Proposta_OSHoraCnt, T004852_n1720Proposta_OSHoraCnt
               }
               , new Object[] {
               T004853_A1919Requisito_Codigo
               }
               , new Object[] {
               T004854_A1685Proposta_Codigo
               }
               , new Object[] {
               T004855_A648Projeto_Codigo, T004855_A650Projeto_Sigla
               }
               , new Object[] {
               T004856_A648Projeto_Codigo
               }
               , new Object[] {
               T004857_A5AreaTrabalho_Codigo
               }
            }
         );
         AV14Pgmname = "Proposta";
      }

      private short Z1793Proposta_PrazoDias ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A1793Proposta_PrazoDias ;
      private short A1721Proposta_OSPrzRsp ;
      private short RcdFound187 ;
      private short GX_JID ;
      private short Z1721Proposta_OSPrzRsp ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Proposta_Codigo ;
      private int Z1685Proposta_Codigo ;
      private int Z1699Proposta_ContratadaCod ;
      private int Z1703Proposta_Esforco ;
      private int Z1794Proposta_Liquido ;
      private int Z1704Proposta_CntSrvCod ;
      private int Z5AreaTrabalho_Codigo ;
      private int Z648Projeto_Codigo ;
      private int Z1686Proposta_OSCodigo ;
      private int A648Projeto_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A1686Proposta_OSCodigo ;
      private int A1705Proposta_OSServico ;
      private int A1713Proposta_OSUndCntCod ;
      private int AV7Proposta_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int A1685Proposta_Codigo ;
      private int edtProposta_Codigo_Enabled ;
      private int edtProposta_Vigencia_Enabled ;
      private int edtProposta_Valor_Enabled ;
      private int edtProposta_Status_Enabled ;
      private int edtAreaTrabalho_Codigo_Enabled ;
      private int edtProposta_Objetivo_Enabled ;
      private int edtProposta_Escopo_Enabled ;
      private int edtProposta_Oportunidade_Enabled ;
      private int edtProposta_Perspectiva_Enabled ;
      private int edtProposta_RestricaoImplantacao_Enabled ;
      private int edtProposta_RestricaoCusto_Enabled ;
      private int A1699Proposta_ContratadaCod ;
      private int edtProposta_ContratadaCod_Enabled ;
      private int edtProposta_ContratadaRazSocial_Enabled ;
      private int edtProposta_Prazo_Enabled ;
      private int A1703Proposta_Esforco ;
      private int edtProposta_Esforco_Enabled ;
      private int A1704Proposta_CntSrvCod ;
      private int edtProposta_CntSrvCod_Enabled ;
      private int edtProposta_OSCodigo_Enabled ;
      private int edtProposta_OSServico_Enabled ;
      private int edtProposta_OSDmnFM_Enabled ;
      private int edtProposta_OSUndCntSgl_Enabled ;
      private int A1794Proposta_Liquido ;
      private int A1715Proposta_OSCntCod ;
      private int AV15GXV1 ;
      private int AV11Insert_AreaTrabalho_Codigo ;
      private int AV12Insert_Proposta_OSCodigo ;
      private int Z1705Proposta_OSServico ;
      private int Z1715Proposta_OSCntCod ;
      private int Z1713Proposta_OSUndCntCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal Z1688Proposta_Valor ;
      private decimal Z1695Proposta_RestricaoCusto ;
      private decimal Z1722Proposta_ValorUndCnt ;
      private decimal A1688Proposta_Valor ;
      private decimal A1695Proposta_RestricaoCusto ;
      private decimal A1722Proposta_ValorUndCnt ;
      private decimal A1717Proposta_OSPFB ;
      private decimal A1718Proposta_OSPFL ;
      private decimal Z1717Proposta_OSPFB ;
      private decimal Z1718Proposta_OSPFL ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z1689Proposta_Status ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkProposta_Ativo_Internalname ;
      private String A1707Proposta_OSPrzTpDias ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtProposta_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockproposta_codigo_Internalname ;
      private String lblTextblockproposta_codigo_Jsonclick ;
      private String edtProposta_Codigo_Jsonclick ;
      private String lblTextblockprojeto_codigo_Internalname ;
      private String lblTextblockprojeto_codigo_Jsonclick ;
      private String dynProjeto_Codigo_Internalname ;
      private String dynProjeto_Codigo_Jsonclick ;
      private String lblTextblockproposta_vigencia_Internalname ;
      private String lblTextblockproposta_vigencia_Jsonclick ;
      private String edtProposta_Vigencia_Internalname ;
      private String edtProposta_Vigencia_Jsonclick ;
      private String lblTextblockproposta_valor_Internalname ;
      private String lblTextblockproposta_valor_Jsonclick ;
      private String edtProposta_Valor_Internalname ;
      private String edtProposta_Valor_Jsonclick ;
      private String lblTextblockproposta_status_Internalname ;
      private String lblTextblockproposta_status_Jsonclick ;
      private String edtProposta_Status_Internalname ;
      private String A1689Proposta_Status ;
      private String edtProposta_Status_Jsonclick ;
      private String lblTextblockareatrabalho_codigo_Internalname ;
      private String lblTextblockareatrabalho_codigo_Jsonclick ;
      private String edtAreaTrabalho_Codigo_Internalname ;
      private String edtAreaTrabalho_Codigo_Jsonclick ;
      private String lblTextblockproposta_objetivo_Internalname ;
      private String lblTextblockproposta_objetivo_Jsonclick ;
      private String edtProposta_Objetivo_Internalname ;
      private String lblTextblockproposta_escopo_Internalname ;
      private String lblTextblockproposta_escopo_Jsonclick ;
      private String edtProposta_Escopo_Internalname ;
      private String lblTextblockproposta_oportunidade_Internalname ;
      private String lblTextblockproposta_oportunidade_Jsonclick ;
      private String edtProposta_Oportunidade_Internalname ;
      private String lblTextblockproposta_perspectiva_Internalname ;
      private String lblTextblockproposta_perspectiva_Jsonclick ;
      private String edtProposta_Perspectiva_Internalname ;
      private String lblTextblockproposta_restricaoimplantacao_Internalname ;
      private String lblTextblockproposta_restricaoimplantacao_Jsonclick ;
      private String edtProposta_RestricaoImplantacao_Internalname ;
      private String edtProposta_RestricaoImplantacao_Jsonclick ;
      private String lblTextblockproposta_restricaocusto_Internalname ;
      private String lblTextblockproposta_restricaocusto_Jsonclick ;
      private String edtProposta_RestricaoCusto_Internalname ;
      private String edtProposta_RestricaoCusto_Jsonclick ;
      private String lblTextblockproposta_ativo_Internalname ;
      private String lblTextblockproposta_ativo_Jsonclick ;
      private String lblTextblockproposta_prestatoracod_Internalname ;
      private String lblTextblockproposta_prestatoracod_Jsonclick ;
      private String lblTextblockproposta_prestadorarazsocial_Internalname ;
      private String lblTextblockproposta_prestadorarazsocial_Jsonclick ;
      private String lblTextblockproposta_contratadacod_Internalname ;
      private String lblTextblockproposta_contratadacod_Jsonclick ;
      private String edtProposta_ContratadaCod_Internalname ;
      private String edtProposta_ContratadaCod_Jsonclick ;
      private String lblTextblockproposta_contratadarazsocial_Internalname ;
      private String lblTextblockproposta_contratadarazsocial_Jsonclick ;
      private String edtProposta_ContratadaRazSocial_Internalname ;
      private String edtProposta_ContratadaRazSocial_Jsonclick ;
      private String lblTextblockproposta_prazo_Internalname ;
      private String lblTextblockproposta_prazo_Jsonclick ;
      private String edtProposta_Prazo_Internalname ;
      private String edtProposta_Prazo_Jsonclick ;
      private String lblTextblockproposta_esforco_Internalname ;
      private String lblTextblockproposta_esforco_Jsonclick ;
      private String edtProposta_Esforco_Internalname ;
      private String edtProposta_Esforco_Jsonclick ;
      private String lblTextblockproposta_cntsrvcod_Internalname ;
      private String lblTextblockproposta_cntsrvcod_Jsonclick ;
      private String edtProposta_CntSrvCod_Internalname ;
      private String edtProposta_CntSrvCod_Jsonclick ;
      private String lblTextblockproposta_oscodigo_Internalname ;
      private String lblTextblockproposta_oscodigo_Jsonclick ;
      private String edtProposta_OSCodigo_Internalname ;
      private String edtProposta_OSCodigo_Jsonclick ;
      private String lblTextblockproposta_osservico_Internalname ;
      private String lblTextblockproposta_osservico_Jsonclick ;
      private String edtProposta_OSServico_Internalname ;
      private String edtProposta_OSServico_Jsonclick ;
      private String lblTextblockproposta_osdmnfm_Internalname ;
      private String lblTextblockproposta_osdmnfm_Jsonclick ;
      private String edtProposta_OSDmnFM_Internalname ;
      private String edtProposta_OSDmnFM_Jsonclick ;
      private String lblTextblockproposta_osundcntsgl_Internalname ;
      private String lblTextblockproposta_osundcntsgl_Jsonclick ;
      private String edtProposta_OSUndCntSgl_Internalname ;
      private String A1710Proposta_OSUndCntSgl ;
      private String edtProposta_OSUndCntSgl_Jsonclick ;
      private String lblTextblockproposta_osprztpdias_Internalname ;
      private String lblTextblockproposta_osprztpdias_Jsonclick ;
      private String cmbProposta_OSPrzTpDias_Internalname ;
      private String cmbProposta_OSPrzTpDias_Jsonclick ;
      private String A1720Proposta_OSHoraCnt ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode187 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1707Proposta_OSPrzTpDias ;
      private String Z1710Proposta_OSUndCntSgl ;
      private String Z1720Proposta_OSHoraCnt ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private DateTime Z1687Proposta_Vigencia ;
      private DateTime Z1694Proposta_RestricaoImplantacao ;
      private DateTime A1687Proposta_Vigencia ;
      private DateTime A1694Proposta_RestricaoImplantacao ;
      private DateTime Z1702Proposta_Prazo ;
      private DateTime A1702Proposta_Prazo ;
      private DateTime A1792Proposta_OSInicio ;
      private DateTime A1719Proposta_OSDataCnt ;
      private DateTime Z1792Proposta_OSInicio ;
      private DateTime Z1719Proposta_OSDataCnt ;
      private bool Z1696Proposta_Ativo ;
      private bool entryPointCalled ;
      private bool n648Projeto_Codigo ;
      private bool n1705Proposta_OSServico ;
      private bool n1713Proposta_OSUndCntCod ;
      private bool toggleJsOutput ;
      private bool n1707Proposta_OSPrzTpDias ;
      private bool wbErr ;
      private bool A1696Proposta_Ativo ;
      private bool n1685Proposta_Codigo ;
      private bool n1687Proposta_Vigencia ;
      private bool n1689Proposta_Status ;
      private bool n1691Proposta_Escopo ;
      private bool n1692Proposta_Oportunidade ;
      private bool n1693Proposta_Perspectiva ;
      private bool n1694Proposta_RestricaoImplantacao ;
      private bool n1695Proposta_RestricaoCusto ;
      private bool n1700Proposta_ContratadaRazSocial ;
      private bool n1711Proposta_OSDmnFM ;
      private bool n1710Proposta_OSUndCntSgl ;
      private bool n1793Proposta_PrazoDias ;
      private bool n1794Proposta_Liquido ;
      private bool n1764Proposta_OSDmn ;
      private bool n1792Proposta_OSInicio ;
      private bool n1721Proposta_OSPrzRsp ;
      private bool n1763Proposta_OSCntSrvFtrm ;
      private bool n1715Proposta_OSCntCod ;
      private bool n1717Proposta_OSPFB ;
      private bool n1718Proposta_OSPFL ;
      private bool n1719Proposta_OSDataCnt ;
      private bool n1720Proposta_OSHoraCnt ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String A1690Proposta_Objetivo ;
      private String A1691Proposta_Escopo ;
      private String A1692Proposta_Oportunidade ;
      private String A1693Proposta_Perspectiva ;
      private String Z1690Proposta_Objetivo ;
      private String Z1691Proposta_Escopo ;
      private String Z1692Proposta_Oportunidade ;
      private String Z1693Proposta_Perspectiva ;
      private String Z1700Proposta_ContratadaRazSocial ;
      private String A1700Proposta_ContratadaRazSocial ;
      private String A1711Proposta_OSDmnFM ;
      private String A1764Proposta_OSDmn ;
      private String A1763Proposta_OSCntSrvFtrm ;
      private String Z1764Proposta_OSDmn ;
      private String Z1711Proposta_OSDmnFM ;
      private String Z1763Proposta_OSCntSrvFtrm ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IDataStoreProvider pr_default ;
      private int[] T004817_A648Projeto_Codigo ;
      private bool[] T004817_n648Projeto_Codigo ;
      private String[] T004817_A650Projeto_Sigla ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Gx_mode ;
      private int aP1_Proposta_Codigo ;
      private GXCombobox dynProjeto_Codigo ;
      private GXCheckbox chkProposta_Ativo ;
      private GXCombobox cmbProposta_OSPrzTpDias ;
      private int[] T004822_A1685Proposta_Codigo ;
      private bool[] T004822_n1685Proposta_Codigo ;
      private DateTime[] T004822_A1687Proposta_Vigencia ;
      private bool[] T004822_n1687Proposta_Vigencia ;
      private decimal[] T004822_A1688Proposta_Valor ;
      private String[] T004822_A1689Proposta_Status ;
      private bool[] T004822_n1689Proposta_Status ;
      private String[] T004822_A1690Proposta_Objetivo ;
      private String[] T004822_A1691Proposta_Escopo ;
      private bool[] T004822_n1691Proposta_Escopo ;
      private String[] T004822_A1692Proposta_Oportunidade ;
      private bool[] T004822_n1692Proposta_Oportunidade ;
      private String[] T004822_A1693Proposta_Perspectiva ;
      private bool[] T004822_n1693Proposta_Perspectiva ;
      private DateTime[] T004822_A1694Proposta_RestricaoImplantacao ;
      private bool[] T004822_n1694Proposta_RestricaoImplantacao ;
      private decimal[] T004822_A1695Proposta_RestricaoCusto ;
      private bool[] T004822_n1695Proposta_RestricaoCusto ;
      private bool[] T004822_A1696Proposta_Ativo ;
      private int[] T004822_A1699Proposta_ContratadaCod ;
      private String[] T004822_A1700Proposta_ContratadaRazSocial ;
      private bool[] T004822_n1700Proposta_ContratadaRazSocial ;
      private DateTime[] T004822_A1702Proposta_Prazo ;
      private short[] T004822_A1793Proposta_PrazoDias ;
      private bool[] T004822_n1793Proposta_PrazoDias ;
      private int[] T004822_A1703Proposta_Esforco ;
      private int[] T004822_A1794Proposta_Liquido ;
      private bool[] T004822_n1794Proposta_Liquido ;
      private int[] T004822_A1704Proposta_CntSrvCod ;
      private decimal[] T004822_A1722Proposta_ValorUndCnt ;
      private String[] T004822_A1764Proposta_OSDmn ;
      private bool[] T004822_n1764Proposta_OSDmn ;
      private String[] T004822_A1711Proposta_OSDmnFM ;
      private bool[] T004822_n1711Proposta_OSDmnFM ;
      private String[] T004822_A1710Proposta_OSUndCntSgl ;
      private bool[] T004822_n1710Proposta_OSUndCntSgl ;
      private String[] T004822_A1707Proposta_OSPrzTpDias ;
      private bool[] T004822_n1707Proposta_OSPrzTpDias ;
      private short[] T004822_A1721Proposta_OSPrzRsp ;
      private bool[] T004822_n1721Proposta_OSPrzRsp ;
      private String[] T004822_A1763Proposta_OSCntSrvFtrm ;
      private bool[] T004822_n1763Proposta_OSCntSrvFtrm ;
      private DateTime[] T004822_A1792Proposta_OSInicio ;
      private bool[] T004822_n1792Proposta_OSInicio ;
      private int[] T004822_A5AreaTrabalho_Codigo ;
      private int[] T004822_A648Projeto_Codigo ;
      private bool[] T004822_n648Projeto_Codigo ;
      private int[] T004822_A1686Proposta_OSCodigo ;
      private int[] T004822_A1705Proposta_OSServico ;
      private bool[] T004822_n1705Proposta_OSServico ;
      private int[] T004822_A1715Proposta_OSCntCod ;
      private bool[] T004822_n1715Proposta_OSCntCod ;
      private int[] T004822_A1713Proposta_OSUndCntCod ;
      private bool[] T004822_n1713Proposta_OSUndCntCod ;
      private decimal[] T004822_A1717Proposta_OSPFB ;
      private bool[] T004822_n1717Proposta_OSPFB ;
      private decimal[] T004822_A1718Proposta_OSPFL ;
      private bool[] T004822_n1718Proposta_OSPFL ;
      private DateTime[] T004822_A1719Proposta_OSDataCnt ;
      private bool[] T004822_n1719Proposta_OSDataCnt ;
      private String[] T004822_A1720Proposta_OSHoraCnt ;
      private bool[] T004822_n1720Proposta_OSHoraCnt ;
      private int[] T00485_A648Projeto_Codigo ;
      private bool[] T00485_n648Projeto_Codigo ;
      private int[] T00484_A5AreaTrabalho_Codigo ;
      private String[] T00486_A1764Proposta_OSDmn ;
      private bool[] T00486_n1764Proposta_OSDmn ;
      private String[] T00486_A1711Proposta_OSDmnFM ;
      private bool[] T00486_n1711Proposta_OSDmnFM ;
      private DateTime[] T00486_A1792Proposta_OSInicio ;
      private bool[] T00486_n1792Proposta_OSInicio ;
      private int[] T00486_A1705Proposta_OSServico ;
      private bool[] T00486_n1705Proposta_OSServico ;
      private String[] T00487_A1707Proposta_OSPrzTpDias ;
      private bool[] T00487_n1707Proposta_OSPrzTpDias ;
      private short[] T00487_A1721Proposta_OSPrzRsp ;
      private bool[] T00487_n1721Proposta_OSPrzRsp ;
      private String[] T00487_A1763Proposta_OSCntSrvFtrm ;
      private bool[] T00487_n1763Proposta_OSCntSrvFtrm ;
      private int[] T00487_A1715Proposta_OSCntCod ;
      private bool[] T00487_n1715Proposta_OSCntCod ;
      private int[] T00487_A1713Proposta_OSUndCntCod ;
      private bool[] T00487_n1713Proposta_OSUndCntCod ;
      private String[] T00488_A1710Proposta_OSUndCntSgl ;
      private bool[] T00488_n1710Proposta_OSUndCntSgl ;
      private decimal[] T004810_A1717Proposta_OSPFB ;
      private bool[] T004810_n1717Proposta_OSPFB ;
      private decimal[] T004812_A1718Proposta_OSPFL ;
      private bool[] T004812_n1718Proposta_OSPFL ;
      private DateTime[] T004814_A1719Proposta_OSDataCnt ;
      private bool[] T004814_n1719Proposta_OSDataCnt ;
      private String[] T004816_A1720Proposta_OSHoraCnt ;
      private bool[] T004816_n1720Proposta_OSHoraCnt ;
      private int[] T004823_A648Projeto_Codigo ;
      private bool[] T004823_n648Projeto_Codigo ;
      private int[] T004824_A5AreaTrabalho_Codigo ;
      private String[] T004825_A1764Proposta_OSDmn ;
      private bool[] T004825_n1764Proposta_OSDmn ;
      private String[] T004825_A1711Proposta_OSDmnFM ;
      private bool[] T004825_n1711Proposta_OSDmnFM ;
      private DateTime[] T004825_A1792Proposta_OSInicio ;
      private bool[] T004825_n1792Proposta_OSInicio ;
      private int[] T004825_A1705Proposta_OSServico ;
      private bool[] T004825_n1705Proposta_OSServico ;
      private String[] T004826_A1707Proposta_OSPrzTpDias ;
      private bool[] T004826_n1707Proposta_OSPrzTpDias ;
      private short[] T004826_A1721Proposta_OSPrzRsp ;
      private bool[] T004826_n1721Proposta_OSPrzRsp ;
      private String[] T004826_A1763Proposta_OSCntSrvFtrm ;
      private bool[] T004826_n1763Proposta_OSCntSrvFtrm ;
      private int[] T004826_A1715Proposta_OSCntCod ;
      private bool[] T004826_n1715Proposta_OSCntCod ;
      private int[] T004826_A1713Proposta_OSUndCntCod ;
      private bool[] T004826_n1713Proposta_OSUndCntCod ;
      private String[] T004827_A1710Proposta_OSUndCntSgl ;
      private bool[] T004827_n1710Proposta_OSUndCntSgl ;
      private decimal[] T004829_A1717Proposta_OSPFB ;
      private bool[] T004829_n1717Proposta_OSPFB ;
      private decimal[] T004831_A1718Proposta_OSPFL ;
      private bool[] T004831_n1718Proposta_OSPFL ;
      private DateTime[] T004833_A1719Proposta_OSDataCnt ;
      private bool[] T004833_n1719Proposta_OSDataCnt ;
      private String[] T004835_A1720Proposta_OSHoraCnt ;
      private bool[] T004835_n1720Proposta_OSHoraCnt ;
      private int[] T004836_A1685Proposta_Codigo ;
      private bool[] T004836_n1685Proposta_Codigo ;
      private int[] T00483_A1685Proposta_Codigo ;
      private bool[] T00483_n1685Proposta_Codigo ;
      private DateTime[] T00483_A1687Proposta_Vigencia ;
      private bool[] T00483_n1687Proposta_Vigencia ;
      private decimal[] T00483_A1688Proposta_Valor ;
      private String[] T00483_A1689Proposta_Status ;
      private bool[] T00483_n1689Proposta_Status ;
      private String[] T00483_A1690Proposta_Objetivo ;
      private String[] T00483_A1691Proposta_Escopo ;
      private bool[] T00483_n1691Proposta_Escopo ;
      private String[] T00483_A1692Proposta_Oportunidade ;
      private bool[] T00483_n1692Proposta_Oportunidade ;
      private String[] T00483_A1693Proposta_Perspectiva ;
      private bool[] T00483_n1693Proposta_Perspectiva ;
      private DateTime[] T00483_A1694Proposta_RestricaoImplantacao ;
      private bool[] T00483_n1694Proposta_RestricaoImplantacao ;
      private decimal[] T00483_A1695Proposta_RestricaoCusto ;
      private bool[] T00483_n1695Proposta_RestricaoCusto ;
      private bool[] T00483_A1696Proposta_Ativo ;
      private int[] T00483_A1699Proposta_ContratadaCod ;
      private String[] T00483_A1700Proposta_ContratadaRazSocial ;
      private bool[] T00483_n1700Proposta_ContratadaRazSocial ;
      private DateTime[] T00483_A1702Proposta_Prazo ;
      private short[] T00483_A1793Proposta_PrazoDias ;
      private bool[] T00483_n1793Proposta_PrazoDias ;
      private int[] T00483_A1703Proposta_Esforco ;
      private int[] T00483_A1794Proposta_Liquido ;
      private bool[] T00483_n1794Proposta_Liquido ;
      private int[] T00483_A1704Proposta_CntSrvCod ;
      private decimal[] T00483_A1722Proposta_ValorUndCnt ;
      private int[] T00483_A5AreaTrabalho_Codigo ;
      private int[] T00483_A648Projeto_Codigo ;
      private bool[] T00483_n648Projeto_Codigo ;
      private int[] T00483_A1686Proposta_OSCodigo ;
      private int[] T004837_A1685Proposta_Codigo ;
      private bool[] T004837_n1685Proposta_Codigo ;
      private int[] T004838_A1685Proposta_Codigo ;
      private bool[] T004838_n1685Proposta_Codigo ;
      private int[] T00482_A1685Proposta_Codigo ;
      private bool[] T00482_n1685Proposta_Codigo ;
      private DateTime[] T00482_A1687Proposta_Vigencia ;
      private bool[] T00482_n1687Proposta_Vigencia ;
      private decimal[] T00482_A1688Proposta_Valor ;
      private String[] T00482_A1689Proposta_Status ;
      private bool[] T00482_n1689Proposta_Status ;
      private String[] T00482_A1690Proposta_Objetivo ;
      private String[] T00482_A1691Proposta_Escopo ;
      private bool[] T00482_n1691Proposta_Escopo ;
      private String[] T00482_A1692Proposta_Oportunidade ;
      private bool[] T00482_n1692Proposta_Oportunidade ;
      private String[] T00482_A1693Proposta_Perspectiva ;
      private bool[] T00482_n1693Proposta_Perspectiva ;
      private DateTime[] T00482_A1694Proposta_RestricaoImplantacao ;
      private bool[] T00482_n1694Proposta_RestricaoImplantacao ;
      private decimal[] T00482_A1695Proposta_RestricaoCusto ;
      private bool[] T00482_n1695Proposta_RestricaoCusto ;
      private bool[] T00482_A1696Proposta_Ativo ;
      private int[] T00482_A1699Proposta_ContratadaCod ;
      private String[] T00482_A1700Proposta_ContratadaRazSocial ;
      private bool[] T00482_n1700Proposta_ContratadaRazSocial ;
      private DateTime[] T00482_A1702Proposta_Prazo ;
      private short[] T00482_A1793Proposta_PrazoDias ;
      private bool[] T00482_n1793Proposta_PrazoDias ;
      private int[] T00482_A1703Proposta_Esforco ;
      private int[] T00482_A1794Proposta_Liquido ;
      private bool[] T00482_n1794Proposta_Liquido ;
      private int[] T00482_A1704Proposta_CntSrvCod ;
      private decimal[] T00482_A1722Proposta_ValorUndCnt ;
      private int[] T00482_A5AreaTrabalho_Codigo ;
      private int[] T00482_A648Projeto_Codigo ;
      private bool[] T00482_n648Projeto_Codigo ;
      private int[] T00482_A1686Proposta_OSCodigo ;
      private int[] T004839_A1685Proposta_Codigo ;
      private bool[] T004839_n1685Proposta_Codigo ;
      private String[] T004842_A1764Proposta_OSDmn ;
      private bool[] T004842_n1764Proposta_OSDmn ;
      private String[] T004842_A1711Proposta_OSDmnFM ;
      private bool[] T004842_n1711Proposta_OSDmnFM ;
      private DateTime[] T004842_A1792Proposta_OSInicio ;
      private bool[] T004842_n1792Proposta_OSInicio ;
      private int[] T004842_A1705Proposta_OSServico ;
      private bool[] T004842_n1705Proposta_OSServico ;
      private String[] T004843_A1707Proposta_OSPrzTpDias ;
      private bool[] T004843_n1707Proposta_OSPrzTpDias ;
      private short[] T004843_A1721Proposta_OSPrzRsp ;
      private bool[] T004843_n1721Proposta_OSPrzRsp ;
      private String[] T004843_A1763Proposta_OSCntSrvFtrm ;
      private bool[] T004843_n1763Proposta_OSCntSrvFtrm ;
      private int[] T004843_A1715Proposta_OSCntCod ;
      private bool[] T004843_n1715Proposta_OSCntCod ;
      private int[] T004843_A1713Proposta_OSUndCntCod ;
      private bool[] T004843_n1713Proposta_OSUndCntCod ;
      private String[] T004844_A1710Proposta_OSUndCntSgl ;
      private bool[] T004844_n1710Proposta_OSUndCntSgl ;
      private decimal[] T004846_A1717Proposta_OSPFB ;
      private bool[] T004846_n1717Proposta_OSPFB ;
      private decimal[] T004848_A1718Proposta_OSPFL ;
      private bool[] T004848_n1718Proposta_OSPFL ;
      private DateTime[] T004850_A1719Proposta_OSDataCnt ;
      private bool[] T004850_n1719Proposta_OSDataCnt ;
      private String[] T004852_A1720Proposta_OSHoraCnt ;
      private bool[] T004852_n1720Proposta_OSHoraCnt ;
      private int[] T004853_A1919Requisito_Codigo ;
      private int[] T004854_A1685Proposta_Codigo ;
      private bool[] T004854_n1685Proposta_Codigo ;
      private int[] T004855_A648Projeto_Codigo ;
      private bool[] T004855_n648Projeto_Codigo ;
      private String[] T004855_A650Projeto_Sigla ;
      private int[] T004856_A648Projeto_Codigo ;
      private bool[] T004856_n648Projeto_Codigo ;
      private int[] T004857_A5AreaTrabalho_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class proposta__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new UpdateCursor(def[26])
         ,new UpdateCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004817 ;
          prmT004817 = new Object[] {
          } ;
          Object[] prmT004822 ;
          prmT004822 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          String cmdBufferT004822 ;
          cmdBufferT004822=" SELECT TM1.[Proposta_Codigo], TM1.[Proposta_Vigencia], TM1.[Proposta_Valor], TM1.[Proposta_Status], TM1.[Proposta_Objetivo], TM1.[Proposta_Escopo], TM1.[Proposta_Oportunidade], TM1.[Proposta_Perspectiva], TM1.[Proposta_RestricaoImplantacao], TM1.[Proposta_RestricaoCusto], TM1.[Proposta_Ativo], TM1.[Proposta_PrestatoraCod], TM1.[Proposta_PrestadoraRazSocial], TM1.[Proposta_Prazo], TM1.[Proposta_PrazoDias], TM1.[Proposta_Esforco], TM1.[Proposta_Liquido], TM1.[Proposta_CntSrvCod], TM1.[Proposta_ValorUndCnt], T2.[ContagemResultado_Demanda] AS Proposta_OSDmn, T2.[ContagemResultado_DemandaFM] AS Proposta_OSDmnFM, T4.[UnidadeMedicao_Sigla] AS Proposta_OSUndCntSgl, T3.[ContratoServicos_PrazoTpDias] AS Proposta_OSPrzTpDias, T3.[ContratoServicos_PrazoResposta] AS Proposta_OSPrzRsp, T3.[ServicoContrato_Faturamento] AS Proposta_OSCntSrvFtrm, T2.[ContagemResultado_DataInicio] AS Proposta_OSInicio, TM1.[AreaTrabalho_Codigo], TM1.[Projeto_Codigo], TM1.[Proposta_OSCodigo] AS Proposta_OSCodigo, T2.[ContagemResultado_CntSrvCod] AS Proposta_OSServico, T3.[Contrato_Codigo] AS Proposta_OSCntCod, T3.[ContratoServicos_UnidadeContratada] AS Proposta_OSUndCntCod, COALESCE( T5.[Proposta_OSPFB], 0) AS Proposta_OSPFB, COALESCE( T6.[Proposta_OSPFL], 0) AS Proposta_OSPFL, COALESCE( T7.[Proposta_OSDataCnt], convert( DATETIME, '17530101', 112 )) AS Proposta_OSDataCnt, COALESCE( T8.[Proposta_OSHoraCnt], '') AS Proposta_OSHoraCnt FROM (((((((dbo.[Proposta] TM1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = TM1.[Proposta_OSCodigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [UnidadeMedicao] T4 WITH (NOLOCK) ON T4.[UnidadeMedicao_Codigo] = T3.[ContratoServicos_UnidadeContratada]) "
          + " LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS Proposta_OSPFB, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T5 ON T5.[ContagemResultado_Codigo] = TM1.[Proposta_OSCodigo]) LEFT JOIN (SELECT MIN([ContagemResultado_PFLFM]) AS Proposta_OSPFL, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T6 ON T6.[ContagemResultado_Codigo] = TM1.[Proposta_OSCodigo]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS Proposta_OSDataCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = TM1.[Proposta_OSCodigo]) LEFT JOIN (SELECT MIN([ContagemResultado_HoraCnt]) AS Proposta_OSHoraCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = TM1.[Proposta_OSCodigo]) WHERE TM1.[Proposta_Codigo] = @Proposta_Codigo ORDER BY TM1.[Proposta_Codigo]  OPTION (FAST 100)" ;
          Object[] prmT00485 ;
          prmT00485 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00484 ;
          prmT00484 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00486 ;
          prmT00486 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00487 ;
          prmT00487 = new Object[] {
          new Object[] {"@Proposta_OSServico",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00488 ;
          prmT00488 = new Object[] {
          new Object[] {"@Proposta_OSUndCntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004810 ;
          prmT004810 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004812 ;
          prmT004812 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004814 ;
          prmT004814 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004816 ;
          prmT004816 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004823 ;
          prmT004823 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004824 ;
          prmT004824 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004825 ;
          prmT004825 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004826 ;
          prmT004826 = new Object[] {
          new Object[] {"@Proposta_OSServico",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004827 ;
          prmT004827 = new Object[] {
          new Object[] {"@Proposta_OSUndCntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004829 ;
          prmT004829 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004831 ;
          prmT004831 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004833 ;
          prmT004833 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004835 ;
          prmT004835 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004836 ;
          prmT004836 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00483 ;
          prmT00483 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmT004837 ;
          prmT004837 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmT004838 ;
          prmT004838 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00482 ;
          prmT00482 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmT004839 ;
          prmT004839 = new Object[] {
          new Object[] {"@Proposta_Vigencia",SqlDbType.DateTime,10,8} ,
          new Object[] {"@Proposta_Valor",SqlDbType.Decimal,15,4} ,
          new Object[] {"@Proposta_Status",SqlDbType.Char,3,0} ,
          new Object[] {"@Proposta_Objetivo",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Proposta_Escopo",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Proposta_Oportunidade",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Proposta_Perspectiva",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Proposta_RestricaoImplantacao",SqlDbType.DateTime,10,8} ,
          new Object[] {"@Proposta_RestricaoCusto",SqlDbType.Decimal,15,4} ,
          new Object[] {"@Proposta_Ativo",SqlDbType.Bit,1,0} ,
          new Object[] {"@Proposta_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Proposta_ContratadaRazSocial",SqlDbType.VarChar,120,0} ,
          new Object[] {"@Proposta_Prazo",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Proposta_PrazoDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Proposta_Esforco",SqlDbType.Int,8,0} ,
          new Object[] {"@Proposta_Liquido",SqlDbType.Int,8,0} ,
          new Object[] {"@Proposta_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Proposta_ValorUndCnt",SqlDbType.Decimal,10,2} ,
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004840 ;
          prmT004840 = new Object[] {
          new Object[] {"@Proposta_Vigencia",SqlDbType.DateTime,10,8} ,
          new Object[] {"@Proposta_Valor",SqlDbType.Decimal,15,4} ,
          new Object[] {"@Proposta_Status",SqlDbType.Char,3,0} ,
          new Object[] {"@Proposta_Objetivo",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Proposta_Escopo",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Proposta_Oportunidade",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Proposta_Perspectiva",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Proposta_RestricaoImplantacao",SqlDbType.DateTime,10,8} ,
          new Object[] {"@Proposta_RestricaoCusto",SqlDbType.Decimal,15,4} ,
          new Object[] {"@Proposta_Ativo",SqlDbType.Bit,1,0} ,
          new Object[] {"@Proposta_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Proposta_ContratadaRazSocial",SqlDbType.VarChar,120,0} ,
          new Object[] {"@Proposta_Prazo",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Proposta_PrazoDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Proposta_Esforco",SqlDbType.Int,8,0} ,
          new Object[] {"@Proposta_Liquido",SqlDbType.Int,8,0} ,
          new Object[] {"@Proposta_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Proposta_ValorUndCnt",SqlDbType.Decimal,10,2} ,
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmT004841 ;
          prmT004841 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmT004853 ;
          prmT004853 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmT004854 ;
          prmT004854 = new Object[] {
          } ;
          Object[] prmT004855 ;
          prmT004855 = new Object[] {
          } ;
          Object[] prmT004856 ;
          prmT004856 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004857 ;
          prmT004857 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004842 ;
          prmT004842 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004843 ;
          prmT004843 = new Object[] {
          new Object[] {"@Proposta_OSServico",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004844 ;
          prmT004844 = new Object[] {
          new Object[] {"@Proposta_OSUndCntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004846 ;
          prmT004846 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004848 ;
          prmT004848 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004850 ;
          prmT004850 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004852 ;
          prmT004852 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00482", "SELECT [Proposta_Codigo], [Proposta_Vigencia], [Proposta_Valor], [Proposta_Status], [Proposta_Objetivo], [Proposta_Escopo], [Proposta_Oportunidade], [Proposta_Perspectiva], [Proposta_RestricaoImplantacao], [Proposta_RestricaoCusto], [Proposta_Ativo], [Proposta_PrestatoraCod], [Proposta_PrestadoraRazSocial], [Proposta_Prazo], [Proposta_PrazoDias], [Proposta_Esforco], [Proposta_Liquido], [Proposta_CntSrvCod], [Proposta_ValorUndCnt], [AreaTrabalho_Codigo], [Projeto_Codigo], [Proposta_OSCodigo] AS Proposta_OSCodigo FROM dbo.[Proposta] WITH (UPDLOCK) WHERE [Proposta_Codigo] = @Proposta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00482,1,0,true,false )
             ,new CursorDef("T00483", "SELECT [Proposta_Codigo], [Proposta_Vigencia], [Proposta_Valor], [Proposta_Status], [Proposta_Objetivo], [Proposta_Escopo], [Proposta_Oportunidade], [Proposta_Perspectiva], [Proposta_RestricaoImplantacao], [Proposta_RestricaoCusto], [Proposta_Ativo], [Proposta_PrestatoraCod], [Proposta_PrestadoraRazSocial], [Proposta_Prazo], [Proposta_PrazoDias], [Proposta_Esforco], [Proposta_Liquido], [Proposta_CntSrvCod], [Proposta_ValorUndCnt], [AreaTrabalho_Codigo], [Projeto_Codigo], [Proposta_OSCodigo] AS Proposta_OSCodigo FROM dbo.[Proposta] WITH (NOLOCK) WHERE [Proposta_Codigo] = @Proposta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00483,1,0,true,false )
             ,new CursorDef("T00484", "SELECT [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00484,1,0,true,false )
             ,new CursorDef("T00485", "SELECT [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00485,1,0,true,false )
             ,new CursorDef("T00486", "SELECT [ContagemResultado_Demanda] AS Proposta_OSDmn, [ContagemResultado_DemandaFM] AS Proposta_OSDmnFM, [ContagemResultado_DataInicio] AS Proposta_OSInicio, [ContagemResultado_CntSrvCod] AS Proposta_OSServico FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00486,1,0,true,false )
             ,new CursorDef("T00487", "SELECT [ContratoServicos_PrazoTpDias] AS Proposta_OSPrzTpDias, [ContratoServicos_PrazoResposta] AS Proposta_OSPrzRsp, [ServicoContrato_Faturamento] AS Proposta_OSCntSrvFtrm, [Contrato_Codigo] AS Proposta_OSCntCod, [ContratoServicos_UnidadeContratada] AS Proposta_OSUndCntCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @Proposta_OSServico ",true, GxErrorMask.GX_NOMASK, false, this,prmT00487,1,0,true,false )
             ,new CursorDef("T00488", "SELECT [UnidadeMedicao_Sigla] AS Proposta_OSUndCntSgl FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @Proposta_OSUndCntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00488,1,0,true,false )
             ,new CursorDef("T004810", "SELECT COALESCE( T1.[Proposta_OSPFB], 0) AS Proposta_OSPFB FROM (SELECT MIN([ContagemResultado_PFBFM]) AS Proposta_OSPFB, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004810,1,0,true,false )
             ,new CursorDef("T004812", "SELECT COALESCE( T1.[Proposta_OSPFL], 0) AS Proposta_OSPFL FROM (SELECT MIN([ContagemResultado_PFLFM]) AS Proposta_OSPFL, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004812,1,0,true,false )
             ,new CursorDef("T004814", "SELECT COALESCE( T1.[Proposta_OSDataCnt], convert( DATETIME, '17530101', 112 )) AS Proposta_OSDataCnt FROM (SELECT MIN([ContagemResultado_DataCnt]) AS Proposta_OSDataCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004814,1,0,true,false )
             ,new CursorDef("T004816", "SELECT COALESCE( T1.[Proposta_OSHoraCnt], '') AS Proposta_OSHoraCnt FROM (SELECT MIN([ContagemResultado_HoraCnt]) AS Proposta_OSHoraCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004816,1,0,true,false )
             ,new CursorDef("T004817", "SELECT [Projeto_Codigo], [Projeto_Sigla] FROM [Projeto] WITH (NOLOCK) ORDER BY [Projeto_Sigla] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004817,0,0,true,false )
             ,new CursorDef("T004822", cmdBufferT004822,true, GxErrorMask.GX_NOMASK, false, this,prmT004822,100,0,true,false )
             ,new CursorDef("T004823", "SELECT [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004823,1,0,true,false )
             ,new CursorDef("T004824", "SELECT [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004824,1,0,true,false )
             ,new CursorDef("T004825", "SELECT [ContagemResultado_Demanda] AS Proposta_OSDmn, [ContagemResultado_DemandaFM] AS Proposta_OSDmnFM, [ContagemResultado_DataInicio] AS Proposta_OSInicio, [ContagemResultado_CntSrvCod] AS Proposta_OSServico FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004825,1,0,true,false )
             ,new CursorDef("T004826", "SELECT [ContratoServicos_PrazoTpDias] AS Proposta_OSPrzTpDias, [ContratoServicos_PrazoResposta] AS Proposta_OSPrzRsp, [ServicoContrato_Faturamento] AS Proposta_OSCntSrvFtrm, [Contrato_Codigo] AS Proposta_OSCntCod, [ContratoServicos_UnidadeContratada] AS Proposta_OSUndCntCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @Proposta_OSServico ",true, GxErrorMask.GX_NOMASK, false, this,prmT004826,1,0,true,false )
             ,new CursorDef("T004827", "SELECT [UnidadeMedicao_Sigla] AS Proposta_OSUndCntSgl FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @Proposta_OSUndCntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004827,1,0,true,false )
             ,new CursorDef("T004829", "SELECT COALESCE( T1.[Proposta_OSPFB], 0) AS Proposta_OSPFB FROM (SELECT MIN([ContagemResultado_PFBFM]) AS Proposta_OSPFB, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004829,1,0,true,false )
             ,new CursorDef("T004831", "SELECT COALESCE( T1.[Proposta_OSPFL], 0) AS Proposta_OSPFL FROM (SELECT MIN([ContagemResultado_PFLFM]) AS Proposta_OSPFL, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004831,1,0,true,false )
             ,new CursorDef("T004833", "SELECT COALESCE( T1.[Proposta_OSDataCnt], convert( DATETIME, '17530101', 112 )) AS Proposta_OSDataCnt FROM (SELECT MIN([ContagemResultado_DataCnt]) AS Proposta_OSDataCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004833,1,0,true,false )
             ,new CursorDef("T004835", "SELECT COALESCE( T1.[Proposta_OSHoraCnt], '') AS Proposta_OSHoraCnt FROM (SELECT MIN([ContagemResultado_HoraCnt]) AS Proposta_OSHoraCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004835,1,0,true,false )
             ,new CursorDef("T004836", "SELECT [Proposta_Codigo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE [Proposta_Codigo] = @Proposta_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004836,1,0,true,false )
             ,new CursorDef("T004837", "SELECT TOP 1 [Proposta_Codigo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE ( [Proposta_Codigo] > @Proposta_Codigo) ORDER BY [Proposta_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004837,1,0,true,true )
             ,new CursorDef("T004838", "SELECT TOP 1 [Proposta_Codigo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE ( [Proposta_Codigo] < @Proposta_Codigo) ORDER BY [Proposta_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004838,1,0,true,true )
             ,new CursorDef("T004839", "INSERT INTO dbo.[Proposta]([Proposta_Vigencia], [Proposta_Valor], [Proposta_Status], [Proposta_Objetivo], [Proposta_Escopo], [Proposta_Oportunidade], [Proposta_Perspectiva], [Proposta_RestricaoImplantacao], [Proposta_RestricaoCusto], [Proposta_Ativo], [Proposta_PrestatoraCod], [Proposta_PrestadoraRazSocial], [Proposta_Prazo], [Proposta_PrazoDias], [Proposta_Esforco], [Proposta_Liquido], [Proposta_CntSrvCod], [Proposta_ValorUndCnt], [AreaTrabalho_Codigo], [Projeto_Codigo], [Proposta_OSCodigo]) VALUES(@Proposta_Vigencia, @Proposta_Valor, @Proposta_Status, @Proposta_Objetivo, @Proposta_Escopo, @Proposta_Oportunidade, @Proposta_Perspectiva, @Proposta_RestricaoImplantacao, @Proposta_RestricaoCusto, @Proposta_Ativo, @Proposta_ContratadaCod, @Proposta_ContratadaRazSocial, @Proposta_Prazo, @Proposta_PrazoDias, @Proposta_Esforco, @Proposta_Liquido, @Proposta_CntSrvCod, @Proposta_ValorUndCnt, @AreaTrabalho_Codigo, @Projeto_Codigo, @Proposta_OSCodigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT004839)
             ,new CursorDef("T004840", "UPDATE dbo.[Proposta] SET [Proposta_Vigencia]=@Proposta_Vigencia, [Proposta_Valor]=@Proposta_Valor, [Proposta_Status]=@Proposta_Status, [Proposta_Objetivo]=@Proposta_Objetivo, [Proposta_Escopo]=@Proposta_Escopo, [Proposta_Oportunidade]=@Proposta_Oportunidade, [Proposta_Perspectiva]=@Proposta_Perspectiva, [Proposta_RestricaoImplantacao]=@Proposta_RestricaoImplantacao, [Proposta_RestricaoCusto]=@Proposta_RestricaoCusto, [Proposta_Ativo]=@Proposta_Ativo, [Proposta_PrestatoraCod]=@Proposta_ContratadaCod, [Proposta_PrestadoraRazSocial]=@Proposta_ContratadaRazSocial, [Proposta_Prazo]=@Proposta_Prazo, [Proposta_PrazoDias]=@Proposta_PrazoDias, [Proposta_Esforco]=@Proposta_Esforco, [Proposta_Liquido]=@Proposta_Liquido, [Proposta_CntSrvCod]=@Proposta_CntSrvCod, [Proposta_ValorUndCnt]=@Proposta_ValorUndCnt, [AreaTrabalho_Codigo]=@AreaTrabalho_Codigo, [Projeto_Codigo]=@Projeto_Codigo, [Proposta_OSCodigo]=@Proposta_OSCodigo  WHERE [Proposta_Codigo] = @Proposta_Codigo", GxErrorMask.GX_NOMASK,prmT004840)
             ,new CursorDef("T004841", "DELETE FROM dbo.[Proposta]  WHERE [Proposta_Codigo] = @Proposta_Codigo", GxErrorMask.GX_NOMASK,prmT004841)
             ,new CursorDef("T004842", "SELECT [ContagemResultado_Demanda] AS Proposta_OSDmn, [ContagemResultado_DemandaFM] AS Proposta_OSDmnFM, [ContagemResultado_DataInicio] AS Proposta_OSInicio, [ContagemResultado_CntSrvCod] AS Proposta_OSServico FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004842,1,0,true,false )
             ,new CursorDef("T004843", "SELECT [ContratoServicos_PrazoTpDias] AS Proposta_OSPrzTpDias, [ContratoServicos_PrazoResposta] AS Proposta_OSPrzRsp, [ServicoContrato_Faturamento] AS Proposta_OSCntSrvFtrm, [Contrato_Codigo] AS Proposta_OSCntCod, [ContratoServicos_UnidadeContratada] AS Proposta_OSUndCntCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @Proposta_OSServico ",true, GxErrorMask.GX_NOMASK, false, this,prmT004843,1,0,true,false )
             ,new CursorDef("T004844", "SELECT [UnidadeMedicao_Sigla] AS Proposta_OSUndCntSgl FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @Proposta_OSUndCntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004844,1,0,true,false )
             ,new CursorDef("T004846", "SELECT COALESCE( T1.[Proposta_OSPFB], 0) AS Proposta_OSPFB FROM (SELECT MIN([ContagemResultado_PFBFM]) AS Proposta_OSPFB, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004846,1,0,true,false )
             ,new CursorDef("T004848", "SELECT COALESCE( T1.[Proposta_OSPFL], 0) AS Proposta_OSPFL FROM (SELECT MIN([ContagemResultado_PFLFM]) AS Proposta_OSPFL, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004848,1,0,true,false )
             ,new CursorDef("T004850", "SELECT COALESCE( T1.[Proposta_OSDataCnt], convert( DATETIME, '17530101', 112 )) AS Proposta_OSDataCnt FROM (SELECT MIN([ContagemResultado_DataCnt]) AS Proposta_OSDataCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004850,1,0,true,false )
             ,new CursorDef("T004852", "SELECT COALESCE( T1.[Proposta_OSHoraCnt], '') AS Proposta_OSHoraCnt FROM (SELECT MIN([ContagemResultado_HoraCnt]) AS Proposta_OSHoraCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004852,1,0,true,false )
             ,new CursorDef("T004853", "SELECT TOP 1 [Requisito_Codigo] FROM [Requisito] WITH (NOLOCK) WHERE [Proposta_Codigo] = @Proposta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004853,1,0,true,true )
             ,new CursorDef("T004854", "SELECT [Proposta_Codigo] FROM dbo.[Proposta] WITH (NOLOCK) ORDER BY [Proposta_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004854,100,0,true,false )
             ,new CursorDef("T004855", "SELECT [Projeto_Codigo], [Projeto_Sigla] FROM [Projeto] WITH (NOLOCK) ORDER BY [Projeto_Sigla] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004855,0,0,true,false )
             ,new CursorDef("T004856", "SELECT [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004856,1,0,true,false )
             ,new CursorDef("T004857", "SELECT [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004857,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((bool[]) buf[17])[0] = rslt.getBool(11) ;
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((String[]) buf[19])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(14) ;
                ((short[]) buf[22])[0] = rslt.getShort(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((int[]) buf[24])[0] = rslt.getInt(16) ;
                ((int[]) buf[25])[0] = rslt.getInt(17) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(17);
                ((int[]) buf[27])[0] = rslt.getInt(18) ;
                ((decimal[]) buf[28])[0] = rslt.getDecimal(19) ;
                ((int[]) buf[29])[0] = rslt.getInt(20) ;
                ((int[]) buf[30])[0] = rslt.getInt(21) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(21);
                ((int[]) buf[32])[0] = rslt.getInt(22) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((bool[]) buf[17])[0] = rslt.getBool(11) ;
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((String[]) buf[19])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(14) ;
                ((short[]) buf[22])[0] = rslt.getShort(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((int[]) buf[24])[0] = rslt.getInt(16) ;
                ((int[]) buf[25])[0] = rslt.getInt(17) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(17);
                ((int[]) buf[27])[0] = rslt.getInt(18) ;
                ((decimal[]) buf[28])[0] = rslt.getDecimal(19) ;
                ((int[]) buf[29])[0] = rslt.getInt(20) ;
                ((int[]) buf[30])[0] = rslt.getInt(21) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(21);
                ((int[]) buf[32])[0] = rslt.getInt(22) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 5) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((bool[]) buf[17])[0] = rslt.getBool(11) ;
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((String[]) buf[19])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(14) ;
                ((short[]) buf[22])[0] = rslt.getShort(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((int[]) buf[24])[0] = rslt.getInt(16) ;
                ((int[]) buf[25])[0] = rslt.getInt(17) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(17);
                ((int[]) buf[27])[0] = rslt.getInt(18) ;
                ((decimal[]) buf[28])[0] = rslt.getDecimal(19) ;
                ((String[]) buf[29])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(20);
                ((String[]) buf[31])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(21);
                ((String[]) buf[33])[0] = rslt.getString(22, 15) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(22);
                ((String[]) buf[35])[0] = rslt.getString(23, 1) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((short[]) buf[37])[0] = rslt.getShort(24) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(24);
                ((String[]) buf[39])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(25);
                ((DateTime[]) buf[41])[0] = rslt.getGXDate(26) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(26);
                ((int[]) buf[43])[0] = rslt.getInt(27) ;
                ((int[]) buf[44])[0] = rslt.getInt(28) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(28);
                ((int[]) buf[46])[0] = rslt.getInt(29) ;
                ((int[]) buf[47])[0] = rslt.getInt(30) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(30);
                ((int[]) buf[49])[0] = rslt.getInt(31) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(31);
                ((int[]) buf[51])[0] = rslt.getInt(32) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(32);
                ((decimal[]) buf[53])[0] = rslt.getDecimal(33) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(33);
                ((decimal[]) buf[55])[0] = rslt.getDecimal(34) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(34);
                ((DateTime[]) buf[57])[0] = rslt.getGXDate(35) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(35);
                ((String[]) buf[59])[0] = rslt.getString(36, 5) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(36);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 19 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 21 :
                ((String[]) buf[0])[0] = rslt.getString(1, 5) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 28 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 29 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 31 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 32 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 33 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 34 :
                ((String[]) buf[0])[0] = rslt.getString(1, 5) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 39 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (decimal)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                stmt.SetParameter(4, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(8, (DateTime)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[15]);
                }
                stmt.SetParameter(10, (bool)parms[16]);
                stmt.SetParameter(11, (int)parms[17]);
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[19]);
                }
                stmt.SetParameter(13, (DateTime)parms[20]);
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 14 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(14, (short)parms[22]);
                }
                stmt.SetParameter(15, (int)parms[23]);
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[25]);
                }
                stmt.SetParameter(17, (int)parms[26]);
                stmt.SetParameter(18, (decimal)parms[27]);
                stmt.SetParameter(19, (int)parms[28]);
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 20 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(20, (int)parms[30]);
                }
                stmt.SetParameter(21, (int)parms[31]);
                return;
             case 26 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (decimal)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                stmt.SetParameter(4, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(8, (DateTime)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[15]);
                }
                stmt.SetParameter(10, (bool)parms[16]);
                stmt.SetParameter(11, (int)parms[17]);
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[19]);
                }
                stmt.SetParameter(13, (DateTime)parms[20]);
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 14 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(14, (short)parms[22]);
                }
                stmt.SetParameter(15, (int)parms[23]);
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[25]);
                }
                stmt.SetParameter(17, (int)parms[26]);
                stmt.SetParameter(18, (decimal)parms[27]);
                stmt.SetParameter(19, (int)parms[28]);
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 20 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(20, (int)parms[30]);
                }
                stmt.SetParameter(21, (int)parms[31]);
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[33]);
                }
                return;
             case 27 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 34 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 35 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 38 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 39 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
