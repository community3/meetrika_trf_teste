/*
               File: Contrato_BC
        Description: Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:10:55.99
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contrato_bc : GXHttpHandler, IGxSilentTrn
   {
      public contrato_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contrato_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow0G17( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey0G17( ) ;
         standaloneModal( ) ;
         AddRow0G17( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E110G2 */
            E110G2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z74Contrato_Codigo = A74Contrato_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_0G0( )
      {
         BeforeValidate0G17( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0G17( ) ;
            }
            else
            {
               CheckExtendedTable0G17( ) ;
               if ( AnyError == 0 )
               {
                  ZM0G17( 31) ;
                  ZM0G17( 32) ;
                  ZM0G17( 33) ;
                  ZM0G17( 34) ;
                  ZM0G17( 35) ;
                  ZM0G17( 36) ;
                  ZM0G17( 37) ;
               }
               CloseExtendedTableCursors0G17( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E120G2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV27Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV28GXV1 = 1;
            while ( AV28GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV28GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Contrato_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_Contrato_AreaTrabalhoCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Contratada_Codigo") == 0 )
               {
                  AV12Insert_Contratada_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Contrato_PrepostoCod") == 0 )
               {
                  AV16Insert_Contrato_PrepostoCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV28GXV1 = (int)(AV28GXV1+1);
            }
         }
      }

      protected void E110G2( )
      {
         /* After Trn Routine */
         new wwpbaseobjects.audittransaction(context ).execute(  AV24AuditingObject,  AV27Pgmname) ;
         if ( false )
         {
            new wwpbaseobjects.audittransaction(context ).execute(  AV24AuditingObject,  AV27Pgmname) ;
         }
      }

      protected void E130G2( )
      {
         /* Contrato_ValorUnidadeContratacao_Isvalid Routine */
      }

      protected void ZM0G17( short GX_JID )
      {
         if ( ( GX_JID == 30 ) || ( GX_JID == 0 ) )
         {
            Z77Contrato_Numero = A77Contrato_Numero;
            Z78Contrato_NumeroAta = A78Contrato_NumeroAta;
            Z79Contrato_Ano = A79Contrato_Ano;
            Z115Contrato_UnidadeContratacao = A115Contrato_UnidadeContratacao;
            Z81Contrato_Quantidade = A81Contrato_Quantidade;
            Z82Contrato_DataVigenciaInicio = A82Contrato_DataVigenciaInicio;
            Z83Contrato_DataVigenciaTermino = A83Contrato_DataVigenciaTermino;
            Z84Contrato_DataPublicacaoDOU = A84Contrato_DataPublicacaoDOU;
            Z85Contrato_DataAssinatura = A85Contrato_DataAssinatura;
            Z86Contrato_DataPedidoReajuste = A86Contrato_DataPedidoReajuste;
            Z87Contrato_DataTerminoAta = A87Contrato_DataTerminoAta;
            Z88Contrato_DataFimAdaptacao = A88Contrato_DataFimAdaptacao;
            Z89Contrato_Valor = A89Contrato_Valor;
            Z116Contrato_ValorUnidadeContratacao = A116Contrato_ValorUnidadeContratacao;
            Z91Contrato_DiasPagto = A91Contrato_DiasPagto;
            Z452Contrato_CalculoDivergencia = A452Contrato_CalculoDivergencia;
            Z453Contrato_IndiceDivergencia = A453Contrato_IndiceDivergencia;
            Z1150Contrato_AceitaPFFS = A1150Contrato_AceitaPFFS;
            Z92Contrato_Ativo = A92Contrato_Ativo;
            Z1354Contrato_PrdFtrCada = A1354Contrato_PrdFtrCada;
            Z2086Contrato_LmtFtr = A2086Contrato_LmtFtr;
            Z1357Contrato_PrdFtrIni = A1357Contrato_PrdFtrIni;
            Z1358Contrato_PrdFtrFim = A1358Contrato_PrdFtrFim;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z75Contrato_AreaTrabalhoCod = A75Contrato_AreaTrabalhoCod;
            Z1013Contrato_PrepostoCod = A1013Contrato_PrepostoCod;
            Z842Contrato_DataInicioTA = A842Contrato_DataInicioTA;
            Z843Contrato_DataFimTA = A843Contrato_DataFimTA;
            Z1869Contrato_DataTermino = A1869Contrato_DataTermino;
            Z1870Contrato_ValorUndCntAtual = A1870Contrato_ValorUndCntAtual;
            Z2096Contrato_Identificacao = A2096Contrato_Identificacao;
         }
         if ( ( GX_JID == 31 ) || ( GX_JID == 0 ) )
         {
            Z438Contratada_Sigla = A438Contratada_Sigla;
            Z516Contratada_TipoFabrica = A516Contratada_TipoFabrica;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z842Contrato_DataInicioTA = A842Contrato_DataInicioTA;
            Z843Contrato_DataFimTA = A843Contrato_DataFimTA;
            Z1869Contrato_DataTermino = A1869Contrato_DataTermino;
            Z1870Contrato_ValorUndCntAtual = A1870Contrato_ValorUndCntAtual;
            Z2096Contrato_Identificacao = A2096Contrato_Identificacao;
         }
         if ( ( GX_JID == 32 ) || ( GX_JID == 0 ) )
         {
            Z76Contrato_AreaTrabalhoDes = A76Contrato_AreaTrabalhoDes;
            Z842Contrato_DataInicioTA = A842Contrato_DataInicioTA;
            Z843Contrato_DataFimTA = A843Contrato_DataFimTA;
            Z1869Contrato_DataTermino = A1869Contrato_DataTermino;
            Z1870Contrato_ValorUndCntAtual = A1870Contrato_ValorUndCntAtual;
            Z2096Contrato_Identificacao = A2096Contrato_Identificacao;
         }
         if ( ( GX_JID == 33 ) || ( GX_JID == 0 ) )
         {
            Z1016Contrato_PrepostoPesCod = A1016Contrato_PrepostoPesCod;
            Z842Contrato_DataInicioTA = A842Contrato_DataInicioTA;
            Z843Contrato_DataFimTA = A843Contrato_DataFimTA;
            Z1869Contrato_DataTermino = A1869Contrato_DataTermino;
            Z1870Contrato_ValorUndCntAtual = A1870Contrato_ValorUndCntAtual;
            Z2096Contrato_Identificacao = A2096Contrato_Identificacao;
         }
         if ( ( GX_JID == 34 ) || ( GX_JID == 0 ) )
         {
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
            Z842Contrato_DataInicioTA = A842Contrato_DataInicioTA;
            Z843Contrato_DataFimTA = A843Contrato_DataFimTA;
            Z1869Contrato_DataTermino = A1869Contrato_DataTermino;
            Z1870Contrato_ValorUndCntAtual = A1870Contrato_ValorUndCntAtual;
            Z2096Contrato_Identificacao = A2096Contrato_Identificacao;
         }
         if ( ( GX_JID == 35 ) || ( GX_JID == 0 ) )
         {
            Z1015Contrato_PrepostoNom = A1015Contrato_PrepostoNom;
            Z842Contrato_DataInicioTA = A842Contrato_DataInicioTA;
            Z843Contrato_DataFimTA = A843Contrato_DataFimTA;
            Z1869Contrato_DataTermino = A1869Contrato_DataTermino;
            Z1870Contrato_ValorUndCntAtual = A1870Contrato_ValorUndCntAtual;
            Z2096Contrato_Identificacao = A2096Contrato_Identificacao;
         }
         if ( ( GX_JID == 36 ) || ( GX_JID == 0 ) )
         {
            Z842Contrato_DataInicioTA = A842Contrato_DataInicioTA;
            Z843Contrato_DataFimTA = A843Contrato_DataFimTA;
            Z1869Contrato_DataTermino = A1869Contrato_DataTermino;
            Z1870Contrato_ValorUndCntAtual = A1870Contrato_ValorUndCntAtual;
            Z2096Contrato_Identificacao = A2096Contrato_Identificacao;
         }
         if ( ( GX_JID == 37 ) || ( GX_JID == 0 ) )
         {
            Z842Contrato_DataInicioTA = A842Contrato_DataInicioTA;
            Z843Contrato_DataFimTA = A843Contrato_DataFimTA;
            Z1869Contrato_DataTermino = A1869Contrato_DataTermino;
            Z1870Contrato_ValorUndCntAtual = A1870Contrato_ValorUndCntAtual;
            Z2096Contrato_Identificacao = A2096Contrato_Identificacao;
         }
         if ( GX_JID == -30 )
         {
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z77Contrato_Numero = A77Contrato_Numero;
            Z78Contrato_NumeroAta = A78Contrato_NumeroAta;
            Z79Contrato_Ano = A79Contrato_Ano;
            Z80Contrato_Objeto = A80Contrato_Objeto;
            Z115Contrato_UnidadeContratacao = A115Contrato_UnidadeContratacao;
            Z81Contrato_Quantidade = A81Contrato_Quantidade;
            Z82Contrato_DataVigenciaInicio = A82Contrato_DataVigenciaInicio;
            Z83Contrato_DataVigenciaTermino = A83Contrato_DataVigenciaTermino;
            Z84Contrato_DataPublicacaoDOU = A84Contrato_DataPublicacaoDOU;
            Z85Contrato_DataAssinatura = A85Contrato_DataAssinatura;
            Z86Contrato_DataPedidoReajuste = A86Contrato_DataPedidoReajuste;
            Z87Contrato_DataTerminoAta = A87Contrato_DataTerminoAta;
            Z88Contrato_DataFimAdaptacao = A88Contrato_DataFimAdaptacao;
            Z89Contrato_Valor = A89Contrato_Valor;
            Z116Contrato_ValorUnidadeContratacao = A116Contrato_ValorUnidadeContratacao;
            Z90Contrato_RegrasPagto = A90Contrato_RegrasPagto;
            Z91Contrato_DiasPagto = A91Contrato_DiasPagto;
            Z452Contrato_CalculoDivergencia = A452Contrato_CalculoDivergencia;
            Z453Contrato_IndiceDivergencia = A453Contrato_IndiceDivergencia;
            Z1150Contrato_AceitaPFFS = A1150Contrato_AceitaPFFS;
            Z92Contrato_Ativo = A92Contrato_Ativo;
            Z1354Contrato_PrdFtrCada = A1354Contrato_PrdFtrCada;
            Z2086Contrato_LmtFtr = A2086Contrato_LmtFtr;
            Z1357Contrato_PrdFtrIni = A1357Contrato_PrdFtrIni;
            Z1358Contrato_PrdFtrFim = A1358Contrato_PrdFtrFim;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z75Contrato_AreaTrabalhoCod = A75Contrato_AreaTrabalhoCod;
            Z1013Contrato_PrepostoCod = A1013Contrato_PrepostoCod;
            Z842Contrato_DataInicioTA = A842Contrato_DataInicioTA;
            Z843Contrato_DataFimTA = A843Contrato_DataFimTA;
            Z76Contrato_AreaTrabalhoDes = A76Contrato_AreaTrabalhoDes;
            Z438Contratada_Sigla = A438Contratada_Sigla;
            Z516Contratada_TipoFabrica = A516Contratada_TipoFabrica;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
            Z1016Contrato_PrepostoPesCod = A1016Contrato_PrepostoPesCod;
            Z1015Contrato_PrepostoNom = A1015Contrato_PrepostoNom;
         }
      }

      protected void standaloneNotModal( )
      {
         AV27Pgmname = "Contrato_BC";
         Gx_BScreen = 0;
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A92Contrato_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A92Contrato_Ativo = true;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A453Contrato_IndiceDivergencia) && ( Gx_BScreen == 0 ) )
         {
            A453Contrato_IndiceDivergencia = (decimal)(10);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A452Contrato_CalculoDivergencia)) && ( Gx_BScreen == 0 ) )
         {
            A452Contrato_CalculoDivergencia = "B";
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A91Contrato_DiasPagto) && ( Gx_BScreen == 0 ) )
         {
            A91Contrato_DiasPagto = 30;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A75Contrato_AreaTrabalhoCod) && ( Gx_BScreen == 0 ) )
         {
            A75Contrato_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor BC000G5 */
            pr_default.execute(3, new Object[] {A75Contrato_AreaTrabalhoCod});
            A76Contrato_AreaTrabalhoDes = BC000G5_A76Contrato_AreaTrabalhoDes[0];
            n76Contrato_AreaTrabalhoDes = BC000G5_n76Contrato_AreaTrabalhoDes[0];
            pr_default.close(3);
         }
      }

      protected void Load0G17( )
      {
         /* Using cursor BC000G19 */
         pr_default.execute(9, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound17 = 1;
            A76Contrato_AreaTrabalhoDes = BC000G19_A76Contrato_AreaTrabalhoDes[0];
            n76Contrato_AreaTrabalhoDes = BC000G19_n76Contrato_AreaTrabalhoDes[0];
            A41Contratada_PessoaNom = BC000G19_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = BC000G19_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = BC000G19_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = BC000G19_n42Contratada_PessoaCNPJ[0];
            A438Contratada_Sigla = BC000G19_A438Contratada_Sigla[0];
            A516Contratada_TipoFabrica = BC000G19_A516Contratada_TipoFabrica[0];
            A77Contrato_Numero = BC000G19_A77Contrato_Numero[0];
            A78Contrato_NumeroAta = BC000G19_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = BC000G19_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = BC000G19_A79Contrato_Ano[0];
            A80Contrato_Objeto = BC000G19_A80Contrato_Objeto[0];
            A115Contrato_UnidadeContratacao = BC000G19_A115Contrato_UnidadeContratacao[0];
            A81Contrato_Quantidade = BC000G19_A81Contrato_Quantidade[0];
            A82Contrato_DataVigenciaInicio = BC000G19_A82Contrato_DataVigenciaInicio[0];
            A83Contrato_DataVigenciaTermino = BC000G19_A83Contrato_DataVigenciaTermino[0];
            A84Contrato_DataPublicacaoDOU = BC000G19_A84Contrato_DataPublicacaoDOU[0];
            A85Contrato_DataAssinatura = BC000G19_A85Contrato_DataAssinatura[0];
            A86Contrato_DataPedidoReajuste = BC000G19_A86Contrato_DataPedidoReajuste[0];
            A87Contrato_DataTerminoAta = BC000G19_A87Contrato_DataTerminoAta[0];
            A88Contrato_DataFimAdaptacao = BC000G19_A88Contrato_DataFimAdaptacao[0];
            A89Contrato_Valor = BC000G19_A89Contrato_Valor[0];
            A116Contrato_ValorUnidadeContratacao = BC000G19_A116Contrato_ValorUnidadeContratacao[0];
            A90Contrato_RegrasPagto = BC000G19_A90Contrato_RegrasPagto[0];
            A91Contrato_DiasPagto = BC000G19_A91Contrato_DiasPagto[0];
            A452Contrato_CalculoDivergencia = BC000G19_A452Contrato_CalculoDivergencia[0];
            A453Contrato_IndiceDivergencia = BC000G19_A453Contrato_IndiceDivergencia[0];
            A1150Contrato_AceitaPFFS = BC000G19_A1150Contrato_AceitaPFFS[0];
            n1150Contrato_AceitaPFFS = BC000G19_n1150Contrato_AceitaPFFS[0];
            A92Contrato_Ativo = BC000G19_A92Contrato_Ativo[0];
            A1015Contrato_PrepostoNom = BC000G19_A1015Contrato_PrepostoNom[0];
            n1015Contrato_PrepostoNom = BC000G19_n1015Contrato_PrepostoNom[0];
            A1354Contrato_PrdFtrCada = BC000G19_A1354Contrato_PrdFtrCada[0];
            n1354Contrato_PrdFtrCada = BC000G19_n1354Contrato_PrdFtrCada[0];
            A2086Contrato_LmtFtr = BC000G19_A2086Contrato_LmtFtr[0];
            n2086Contrato_LmtFtr = BC000G19_n2086Contrato_LmtFtr[0];
            A1357Contrato_PrdFtrIni = BC000G19_A1357Contrato_PrdFtrIni[0];
            n1357Contrato_PrdFtrIni = BC000G19_n1357Contrato_PrdFtrIni[0];
            A1358Contrato_PrdFtrFim = BC000G19_A1358Contrato_PrdFtrFim[0];
            n1358Contrato_PrdFtrFim = BC000G19_n1358Contrato_PrdFtrFim[0];
            A39Contratada_Codigo = BC000G19_A39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = BC000G19_A75Contrato_AreaTrabalhoCod[0];
            A1013Contrato_PrepostoCod = BC000G19_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = BC000G19_n1013Contrato_PrepostoCod[0];
            A40Contratada_PessoaCod = BC000G19_A40Contratada_PessoaCod[0];
            A1016Contrato_PrepostoPesCod = BC000G19_A1016Contrato_PrepostoPesCod[0];
            n1016Contrato_PrepostoPesCod = BC000G19_n1016Contrato_PrepostoPesCod[0];
            A842Contrato_DataInicioTA = BC000G19_A842Contrato_DataInicioTA[0];
            n842Contrato_DataInicioTA = BC000G19_n842Contrato_DataInicioTA[0];
            A843Contrato_DataFimTA = BC000G19_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = BC000G19_n843Contrato_DataFimTA[0];
            ZM0G17( -30) ;
         }
         pr_default.close(9);
         OnLoadActions0G17( ) ;
      }

      protected void OnLoadActions0G17( )
      {
         A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
         A1870Contrato_ValorUndCntAtual = ((GetContrato_ValorUndCntAtual0( A74Contrato_Codigo)>Convert.ToDecimal(0)) ? GetContrato_ValorUndCntAtual1( A74Contrato_Codigo) : A116Contrato_ValorUnidadeContratacao);
         A2096Contrato_Identificacao = StringUtil.Format( "%1 - %2 %3", A77Contrato_Numero, A41Contratada_PessoaNom, (A92Contrato_Ativo ? "(Vigente)" : "(Encerrado)"), "", "", "", "", "", "");
      }

      protected void CheckExtendedTable0G17( )
      {
         standaloneModal( ) ;
         /* Using cursor BC000G11 */
         pr_default.execute(7, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            A842Contrato_DataInicioTA = BC000G11_A842Contrato_DataInicioTA[0];
            n842Contrato_DataInicioTA = BC000G11_n842Contrato_DataInicioTA[0];
         }
         else
         {
            A842Contrato_DataInicioTA = DateTime.MinValue;
            n842Contrato_DataInicioTA = false;
         }
         pr_default.close(7);
         /* Using cursor BC000G14 */
         pr_default.execute(8, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            A843Contrato_DataFimTA = BC000G14_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = BC000G14_n843Contrato_DataFimTA[0];
         }
         else
         {
            A843Contrato_DataFimTA = DateTime.MinValue;
            n843Contrato_DataFimTA = false;
         }
         pr_default.close(8);
         A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
         A1870Contrato_ValorUndCntAtual = ((GetContrato_ValorUndCntAtual0( A74Contrato_Codigo)>Convert.ToDecimal(0)) ? GetContrato_ValorUndCntAtual1( A74Contrato_Codigo) : A116Contrato_ValorUnidadeContratacao);
         /* Using cursor BC000G5 */
         pr_default.execute(3, new Object[] {A75Contrato_AreaTrabalhoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contrato_Area Trabalho'.", "ForeignKeyNotFound", 1, "CONTRATO_AREATRABALHOCOD");
            AnyError = 1;
         }
         A76Contrato_AreaTrabalhoDes = BC000G5_A76Contrato_AreaTrabalhoDes[0];
         n76Contrato_AreaTrabalhoDes = BC000G5_n76Contrato_AreaTrabalhoDes[0];
         pr_default.close(3);
         /* Using cursor BC000G4 */
         pr_default.execute(2, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "CONTRATADA_CODIGO");
            AnyError = 1;
         }
         A438Contratada_Sigla = BC000G4_A438Contratada_Sigla[0];
         A516Contratada_TipoFabrica = BC000G4_A516Contratada_TipoFabrica[0];
         A40Contratada_PessoaCod = BC000G4_A40Contratada_PessoaCod[0];
         pr_default.close(2);
         /* Using cursor BC000G7 */
         pr_default.execute(5, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = BC000G7_A41Contratada_PessoaNom[0];
         n41Contratada_PessoaNom = BC000G7_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = BC000G7_A42Contratada_PessoaCNPJ[0];
         n42Contratada_PessoaCNPJ = BC000G7_n42Contratada_PessoaCNPJ[0];
         pr_default.close(5);
         A2096Contrato_Identificacao = StringUtil.Format( "%1 - %2 %3", A77Contrato_Numero, A41Contratada_PessoaNom, (A92Contrato_Ativo ? "(Vigente)" : "(Encerrado)"), "", "", "", "", "", "");
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
         {
            GX_msglist.addItem("N�mero do Contrato � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         if ( (0==A79Contrato_Ano) )
         {
            GX_msglist.addItem("Ano � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A80Contrato_Objeto)) )
         {
            GX_msglist.addItem("Objeto do Contrato � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( A115Contrato_UnidadeContratacao == 1 ) || ( A115Contrato_UnidadeContratacao == 2 ) || ( A115Contrato_UnidadeContratacao == 3 ) ) )
         {
            GX_msglist.addItem("Campo Unidade fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A82Contrato_DataVigenciaInicio) || ( A82Contrato_DataVigenciaInicio >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Vig�ncia Inicio fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A83Contrato_DataVigenciaTermino) || ( A83Contrato_DataVigenciaTermino >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Vig�ncia T�rmino fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A84Contrato_DataPublicacaoDOU) || ( A84Contrato_DataPublicacaoDOU >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Publica��o DOU fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A85Contrato_DataAssinatura) || ( A85Contrato_DataAssinatura >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Assinatura fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A86Contrato_DataPedidoReajuste) || ( A86Contrato_DataPedidoReajuste >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Pedido Reajuste fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A87Contrato_DataTerminoAta) || ( A87Contrato_DataTerminoAta >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo T�rmino da Ata fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A88Contrato_DataFimAdaptacao) || ( A88Contrato_DataFimAdaptacao >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Fim Adapta��o fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( (0==A91Contrato_DiasPagto) )
         {
            GX_msglist.addItem("Dias para Pagamento � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC000G6 */
         pr_default.execute(4, new Object[] {n1013Contrato_PrepostoCod, A1013Contrato_PrepostoCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A1013Contrato_PrepostoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATO_PREPOSTOCOD");
               AnyError = 1;
            }
         }
         A1016Contrato_PrepostoPesCod = BC000G6_A1016Contrato_PrepostoPesCod[0];
         n1016Contrato_PrepostoPesCod = BC000G6_n1016Contrato_PrepostoPesCod[0];
         pr_default.close(4);
         /* Using cursor BC000G8 */
         pr_default.execute(6, new Object[] {n1016Contrato_PrepostoPesCod, A1016Contrato_PrepostoPesCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A1016Contrato_PrepostoPesCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1015Contrato_PrepostoNom = BC000G8_A1015Contrato_PrepostoNom[0];
         n1015Contrato_PrepostoNom = BC000G8_n1015Contrato_PrepostoNom[0];
         pr_default.close(6);
      }

      protected void CloseExtendedTableCursors0G17( )
      {
         pr_default.close(7);
         pr_default.close(8);
         pr_default.close(3);
         pr_default.close(2);
         pr_default.close(5);
         pr_default.close(4);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0G17( )
      {
         /* Using cursor BC000G20 */
         pr_default.execute(10, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound17 = 1;
         }
         else
         {
            RcdFound17 = 0;
         }
         pr_default.close(10);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC000G3 */
         pr_default.execute(1, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0G17( 30) ;
            RcdFound17 = 1;
            A74Contrato_Codigo = BC000G3_A74Contrato_Codigo[0];
            n74Contrato_Codigo = BC000G3_n74Contrato_Codigo[0];
            A77Contrato_Numero = BC000G3_A77Contrato_Numero[0];
            A78Contrato_NumeroAta = BC000G3_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = BC000G3_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = BC000G3_A79Contrato_Ano[0];
            A80Contrato_Objeto = BC000G3_A80Contrato_Objeto[0];
            A115Contrato_UnidadeContratacao = BC000G3_A115Contrato_UnidadeContratacao[0];
            A81Contrato_Quantidade = BC000G3_A81Contrato_Quantidade[0];
            A82Contrato_DataVigenciaInicio = BC000G3_A82Contrato_DataVigenciaInicio[0];
            A83Contrato_DataVigenciaTermino = BC000G3_A83Contrato_DataVigenciaTermino[0];
            A84Contrato_DataPublicacaoDOU = BC000G3_A84Contrato_DataPublicacaoDOU[0];
            A85Contrato_DataAssinatura = BC000G3_A85Contrato_DataAssinatura[0];
            A86Contrato_DataPedidoReajuste = BC000G3_A86Contrato_DataPedidoReajuste[0];
            A87Contrato_DataTerminoAta = BC000G3_A87Contrato_DataTerminoAta[0];
            A88Contrato_DataFimAdaptacao = BC000G3_A88Contrato_DataFimAdaptacao[0];
            A89Contrato_Valor = BC000G3_A89Contrato_Valor[0];
            A116Contrato_ValorUnidadeContratacao = BC000G3_A116Contrato_ValorUnidadeContratacao[0];
            A90Contrato_RegrasPagto = BC000G3_A90Contrato_RegrasPagto[0];
            A91Contrato_DiasPagto = BC000G3_A91Contrato_DiasPagto[0];
            A452Contrato_CalculoDivergencia = BC000G3_A452Contrato_CalculoDivergencia[0];
            A453Contrato_IndiceDivergencia = BC000G3_A453Contrato_IndiceDivergencia[0];
            A1150Contrato_AceitaPFFS = BC000G3_A1150Contrato_AceitaPFFS[0];
            n1150Contrato_AceitaPFFS = BC000G3_n1150Contrato_AceitaPFFS[0];
            A92Contrato_Ativo = BC000G3_A92Contrato_Ativo[0];
            A1354Contrato_PrdFtrCada = BC000G3_A1354Contrato_PrdFtrCada[0];
            n1354Contrato_PrdFtrCada = BC000G3_n1354Contrato_PrdFtrCada[0];
            A2086Contrato_LmtFtr = BC000G3_A2086Contrato_LmtFtr[0];
            n2086Contrato_LmtFtr = BC000G3_n2086Contrato_LmtFtr[0];
            A1357Contrato_PrdFtrIni = BC000G3_A1357Contrato_PrdFtrIni[0];
            n1357Contrato_PrdFtrIni = BC000G3_n1357Contrato_PrdFtrIni[0];
            A1358Contrato_PrdFtrFim = BC000G3_A1358Contrato_PrdFtrFim[0];
            n1358Contrato_PrdFtrFim = BC000G3_n1358Contrato_PrdFtrFim[0];
            A39Contratada_Codigo = BC000G3_A39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = BC000G3_A75Contrato_AreaTrabalhoCod[0];
            A1013Contrato_PrepostoCod = BC000G3_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = BC000G3_n1013Contrato_PrepostoCod[0];
            O1013Contrato_PrepostoCod = A1013Contrato_PrepostoCod;
            n1013Contrato_PrepostoCod = false;
            Z74Contrato_Codigo = A74Contrato_Codigo;
            sMode17 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load0G17( ) ;
            if ( AnyError == 1 )
            {
               RcdFound17 = 0;
               InitializeNonKey0G17( ) ;
            }
            Gx_mode = sMode17;
         }
         else
         {
            RcdFound17 = 0;
            InitializeNonKey0G17( ) ;
            sMode17 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode17;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0G17( ) ;
         if ( RcdFound17 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_0G0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency0G17( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC000G2 */
            pr_default.execute(0, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Contrato"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z77Contrato_Numero, BC000G2_A77Contrato_Numero[0]) != 0 ) || ( StringUtil.StrCmp(Z78Contrato_NumeroAta, BC000G2_A78Contrato_NumeroAta[0]) != 0 ) || ( Z79Contrato_Ano != BC000G2_A79Contrato_Ano[0] ) || ( Z115Contrato_UnidadeContratacao != BC000G2_A115Contrato_UnidadeContratacao[0] ) || ( Z81Contrato_Quantidade != BC000G2_A81Contrato_Quantidade[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z82Contrato_DataVigenciaInicio != BC000G2_A82Contrato_DataVigenciaInicio[0] ) || ( Z83Contrato_DataVigenciaTermino != BC000G2_A83Contrato_DataVigenciaTermino[0] ) || ( Z84Contrato_DataPublicacaoDOU != BC000G2_A84Contrato_DataPublicacaoDOU[0] ) || ( Z85Contrato_DataAssinatura != BC000G2_A85Contrato_DataAssinatura[0] ) || ( Z86Contrato_DataPedidoReajuste != BC000G2_A86Contrato_DataPedidoReajuste[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z87Contrato_DataTerminoAta != BC000G2_A87Contrato_DataTerminoAta[0] ) || ( Z88Contrato_DataFimAdaptacao != BC000G2_A88Contrato_DataFimAdaptacao[0] ) || ( Z89Contrato_Valor != BC000G2_A89Contrato_Valor[0] ) || ( Z116Contrato_ValorUnidadeContratacao != BC000G2_A116Contrato_ValorUnidadeContratacao[0] ) || ( Z91Contrato_DiasPagto != BC000G2_A91Contrato_DiasPagto[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z452Contrato_CalculoDivergencia, BC000G2_A452Contrato_CalculoDivergencia[0]) != 0 ) || ( Z453Contrato_IndiceDivergencia != BC000G2_A453Contrato_IndiceDivergencia[0] ) || ( Z1150Contrato_AceitaPFFS != BC000G2_A1150Contrato_AceitaPFFS[0] ) || ( Z92Contrato_Ativo != BC000G2_A92Contrato_Ativo[0] ) || ( StringUtil.StrCmp(Z1354Contrato_PrdFtrCada, BC000G2_A1354Contrato_PrdFtrCada[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2086Contrato_LmtFtr != BC000G2_A2086Contrato_LmtFtr[0] ) || ( Z1357Contrato_PrdFtrIni != BC000G2_A1357Contrato_PrdFtrIni[0] ) || ( Z1358Contrato_PrdFtrFim != BC000G2_A1358Contrato_PrdFtrFim[0] ) || ( Z39Contratada_Codigo != BC000G2_A39Contratada_Codigo[0] ) || ( Z75Contrato_AreaTrabalhoCod != BC000G2_A75Contrato_AreaTrabalhoCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1013Contrato_PrepostoCod != BC000G2_A1013Contrato_PrepostoCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Contrato"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0G17( )
      {
         BeforeValidate0G17( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0G17( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0G17( 0) ;
            CheckOptimisticConcurrency0G17( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0G17( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0G17( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000G21 */
                     pr_default.execute(11, new Object[] {A77Contrato_Numero, n78Contrato_NumeroAta, A78Contrato_NumeroAta, A79Contrato_Ano, A80Contrato_Objeto, A115Contrato_UnidadeContratacao, A81Contrato_Quantidade, A82Contrato_DataVigenciaInicio, A83Contrato_DataVigenciaTermino, A84Contrato_DataPublicacaoDOU, A85Contrato_DataAssinatura, A86Contrato_DataPedidoReajuste, A87Contrato_DataTerminoAta, A88Contrato_DataFimAdaptacao, A89Contrato_Valor, A116Contrato_ValorUnidadeContratacao, A90Contrato_RegrasPagto, A91Contrato_DiasPagto, A452Contrato_CalculoDivergencia, A453Contrato_IndiceDivergencia, n1150Contrato_AceitaPFFS, A1150Contrato_AceitaPFFS, A92Contrato_Ativo, n1354Contrato_PrdFtrCada, A1354Contrato_PrdFtrCada, n2086Contrato_LmtFtr, A2086Contrato_LmtFtr, n1357Contrato_PrdFtrIni, A1357Contrato_PrdFtrIni, n1358Contrato_PrdFtrFim, A1358Contrato_PrdFtrFim, A39Contratada_Codigo, A75Contrato_AreaTrabalhoCod, n1013Contrato_PrepostoCod, A1013Contrato_PrepostoCod});
                     A74Contrato_Codigo = BC000G21_A74Contrato_Codigo[0];
                     n74Contrato_Codigo = BC000G21_n74Contrato_Codigo[0];
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("Contrato") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        new prc_inscontratogestor(context ).execute( ref  A74Contrato_Codigo) ;
                        if ( ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) && ! BC000G3_n115Contrato_UnidadeContratacao[0] )
                        {
                           new prc_saldocontratocriar(context ).execute(  A74Contrato_Codigo,  A115Contrato_UnidadeContratacao,  A82Contrato_DataVigenciaInicio,  A83Contrato_DataVigenciaTermino, out  AV22SaldoContrato_Codigo) ;
                        }
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0G17( ) ;
            }
            EndLevel0G17( ) ;
         }
         CloseExtendedTableCursors0G17( ) ;
      }

      protected void Update0G17( )
      {
         BeforeValidate0G17( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0G17( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0G17( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0G17( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0G17( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000G22 */
                     pr_default.execute(12, new Object[] {A77Contrato_Numero, n78Contrato_NumeroAta, A78Contrato_NumeroAta, A79Contrato_Ano, A80Contrato_Objeto, A115Contrato_UnidadeContratacao, A81Contrato_Quantidade, A82Contrato_DataVigenciaInicio, A83Contrato_DataVigenciaTermino, A84Contrato_DataPublicacaoDOU, A85Contrato_DataAssinatura, A86Contrato_DataPedidoReajuste, A87Contrato_DataTerminoAta, A88Contrato_DataFimAdaptacao, A89Contrato_Valor, A116Contrato_ValorUnidadeContratacao, A90Contrato_RegrasPagto, A91Contrato_DiasPagto, A452Contrato_CalculoDivergencia, A453Contrato_IndiceDivergencia, n1150Contrato_AceitaPFFS, A1150Contrato_AceitaPFFS, A92Contrato_Ativo, n1354Contrato_PrdFtrCada, A1354Contrato_PrdFtrCada, n2086Contrato_LmtFtr, A2086Contrato_LmtFtr, n1357Contrato_PrdFtrIni, A1357Contrato_PrdFtrIni, n1358Contrato_PrdFtrFim, A1358Contrato_PrdFtrFim, A39Contratada_Codigo, A75Contrato_AreaTrabalhoCod, n1013Contrato_PrepostoCod, A1013Contrato_PrepostoCod, n74Contrato_Codigo, A74Contrato_Codigo});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("Contrato") ;
                     if ( (pr_default.getStatus(12) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Contrato"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0G17( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        new prc_inscontratogestor(context ).execute( ref  A74Contrato_Codigo) ;
                        if ( A1013Contrato_PrepostoCod != O1013Contrato_PrepostoCod )
                        {
                           new prc_prepostogestor(context ).execute( ref  A74Contrato_Codigo,  A1013Contrato_PrepostoCod,  O1013Contrato_PrepostoCod) ;
                        }
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0G17( ) ;
         }
         CloseExtendedTableCursors0G17( ) ;
      }

      protected void DeferredUpdate0G17( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate0G17( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0G17( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0G17( ) ;
            AfterConfirm0G17( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0G17( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000G23 */
                  pr_default.execute(13, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
                  pr_default.close(13);
                  dsDefault.SmartCacheProvider.SetUpdated("Contrato") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode17 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel0G17( ) ;
         Gx_mode = sMode17;
      }

      protected void OnDeleteControls0G17( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC000G26 */
            pr_default.execute(14, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               A842Contrato_DataInicioTA = BC000G26_A842Contrato_DataInicioTA[0];
               n842Contrato_DataInicioTA = BC000G26_n842Contrato_DataInicioTA[0];
            }
            else
            {
               A842Contrato_DataInicioTA = DateTime.MinValue;
               n842Contrato_DataInicioTA = false;
            }
            pr_default.close(14);
            /* Using cursor BC000G29 */
            pr_default.execute(15, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(15) != 101) )
            {
               A843Contrato_DataFimTA = BC000G29_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = BC000G29_n843Contrato_DataFimTA[0];
            }
            else
            {
               A843Contrato_DataFimTA = DateTime.MinValue;
               n843Contrato_DataFimTA = false;
            }
            pr_default.close(15);
            /* Using cursor BC000G30 */
            pr_default.execute(16, new Object[] {A75Contrato_AreaTrabalhoCod});
            A76Contrato_AreaTrabalhoDes = BC000G30_A76Contrato_AreaTrabalhoDes[0];
            n76Contrato_AreaTrabalhoDes = BC000G30_n76Contrato_AreaTrabalhoDes[0];
            pr_default.close(16);
            /* Using cursor BC000G31 */
            pr_default.execute(17, new Object[] {A39Contratada_Codigo});
            A438Contratada_Sigla = BC000G31_A438Contratada_Sigla[0];
            A516Contratada_TipoFabrica = BC000G31_A516Contratada_TipoFabrica[0];
            A40Contratada_PessoaCod = BC000G31_A40Contratada_PessoaCod[0];
            pr_default.close(17);
            /* Using cursor BC000G32 */
            pr_default.execute(18, new Object[] {A40Contratada_PessoaCod});
            A41Contratada_PessoaNom = BC000G32_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = BC000G32_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = BC000G32_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = BC000G32_n42Contratada_PessoaCNPJ[0];
            pr_default.close(18);
            A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
            A1870Contrato_ValorUndCntAtual = ((GetContrato_ValorUndCntAtual0( A74Contrato_Codigo)>Convert.ToDecimal(0)) ? GetContrato_ValorUndCntAtual1( A74Contrato_Codigo) : A116Contrato_ValorUnidadeContratacao);
            A2096Contrato_Identificacao = StringUtil.Format( "%1 - %2 %3", A77Contrato_Numero, A41Contratada_PessoaNom, (A92Contrato_Ativo ? "(Vigente)" : "(Encerrado)"), "", "", "", "", "", "");
            /* Using cursor BC000G33 */
            pr_default.execute(19, new Object[] {n1013Contrato_PrepostoCod, A1013Contrato_PrepostoCod});
            A1016Contrato_PrepostoPesCod = BC000G33_A1016Contrato_PrepostoPesCod[0];
            n1016Contrato_PrepostoPesCod = BC000G33_n1016Contrato_PrepostoPesCod[0];
            pr_default.close(19);
            /* Using cursor BC000G34 */
            pr_default.execute(20, new Object[] {n1016Contrato_PrepostoPesCod, A1016Contrato_PrepostoPesCod});
            A1015Contrato_PrepostoNom = BC000G34_A1015Contrato_PrepostoNom[0];
            n1015Contrato_PrepostoNom = BC000G34_n1015Contrato_PrepostoNom[0];
            pr_default.close(20);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC000G35 */
            pr_default.execute(21, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Auxiliar"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(21);
            /* Using cursor BC000G36 */
            pr_default.execute(22, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistemas atendidos pelo Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(22);
            /* Using cursor BC000G37 */
            pr_default.execute(23, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Unidades Contratadas"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
            /* Using cursor BC000G38 */
            pr_default.execute(24, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(24) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Gestor do Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(24);
            /* Using cursor BC000G39 */
            pr_default.execute(25, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(25) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T197"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(25);
            /* Using cursor BC000G40 */
            pr_default.execute(26, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(26) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T179"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(26);
            /* Using cursor BC000G41 */
            pr_default.execute(27, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(27) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Obrigacao"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(27);
            /* Using cursor BC000G42 */
            pr_default.execute(28, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(28) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Termo Aditivo"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(28);
            /* Using cursor BC000G43 */
            pr_default.execute(29, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(29) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Dados Certame"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(29);
            /* Using cursor BC000G44 */
            pr_default.execute(30, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(30) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Ocorr�ncia"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(30);
            /* Using cursor BC000G45 */
            pr_default.execute(31, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(31) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Servicos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(31);
            /* Using cursor BC000G46 */
            pr_default.execute(32, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(32) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Clausulas"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(32);
            /* Using cursor BC000G47 */
            pr_default.execute(33, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(33) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Arquivos Anexos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(33);
            /* Using cursor BC000G48 */
            pr_default.execute(34, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(34) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Garantia"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(34);
            /* Using cursor BC000G49 */
            pr_default.execute(35, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(35) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Configura��es da Caixa de Entrada"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(35);
         }
      }

      protected void EndLevel0G17( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0G17( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart0G17( )
      {
         /* Scan By routine */
         /* Using cursor BC000G54 */
         pr_default.execute(36, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         RcdFound17 = 0;
         if ( (pr_default.getStatus(36) != 101) )
         {
            RcdFound17 = 1;
            A74Contrato_Codigo = BC000G54_A74Contrato_Codigo[0];
            n74Contrato_Codigo = BC000G54_n74Contrato_Codigo[0];
            A76Contrato_AreaTrabalhoDes = BC000G54_A76Contrato_AreaTrabalhoDes[0];
            n76Contrato_AreaTrabalhoDes = BC000G54_n76Contrato_AreaTrabalhoDes[0];
            A41Contratada_PessoaNom = BC000G54_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = BC000G54_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = BC000G54_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = BC000G54_n42Contratada_PessoaCNPJ[0];
            A438Contratada_Sigla = BC000G54_A438Contratada_Sigla[0];
            A516Contratada_TipoFabrica = BC000G54_A516Contratada_TipoFabrica[0];
            A77Contrato_Numero = BC000G54_A77Contrato_Numero[0];
            A78Contrato_NumeroAta = BC000G54_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = BC000G54_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = BC000G54_A79Contrato_Ano[0];
            A80Contrato_Objeto = BC000G54_A80Contrato_Objeto[0];
            A115Contrato_UnidadeContratacao = BC000G54_A115Contrato_UnidadeContratacao[0];
            A81Contrato_Quantidade = BC000G54_A81Contrato_Quantidade[0];
            A82Contrato_DataVigenciaInicio = BC000G54_A82Contrato_DataVigenciaInicio[0];
            A83Contrato_DataVigenciaTermino = BC000G54_A83Contrato_DataVigenciaTermino[0];
            A84Contrato_DataPublicacaoDOU = BC000G54_A84Contrato_DataPublicacaoDOU[0];
            A85Contrato_DataAssinatura = BC000G54_A85Contrato_DataAssinatura[0];
            A86Contrato_DataPedidoReajuste = BC000G54_A86Contrato_DataPedidoReajuste[0];
            A87Contrato_DataTerminoAta = BC000G54_A87Contrato_DataTerminoAta[0];
            A88Contrato_DataFimAdaptacao = BC000G54_A88Contrato_DataFimAdaptacao[0];
            A89Contrato_Valor = BC000G54_A89Contrato_Valor[0];
            A116Contrato_ValorUnidadeContratacao = BC000G54_A116Contrato_ValorUnidadeContratacao[0];
            A90Contrato_RegrasPagto = BC000G54_A90Contrato_RegrasPagto[0];
            A91Contrato_DiasPagto = BC000G54_A91Contrato_DiasPagto[0];
            A452Contrato_CalculoDivergencia = BC000G54_A452Contrato_CalculoDivergencia[0];
            A453Contrato_IndiceDivergencia = BC000G54_A453Contrato_IndiceDivergencia[0];
            A1150Contrato_AceitaPFFS = BC000G54_A1150Contrato_AceitaPFFS[0];
            n1150Contrato_AceitaPFFS = BC000G54_n1150Contrato_AceitaPFFS[0];
            A92Contrato_Ativo = BC000G54_A92Contrato_Ativo[0];
            A1015Contrato_PrepostoNom = BC000G54_A1015Contrato_PrepostoNom[0];
            n1015Contrato_PrepostoNom = BC000G54_n1015Contrato_PrepostoNom[0];
            A1354Contrato_PrdFtrCada = BC000G54_A1354Contrato_PrdFtrCada[0];
            n1354Contrato_PrdFtrCada = BC000G54_n1354Contrato_PrdFtrCada[0];
            A2086Contrato_LmtFtr = BC000G54_A2086Contrato_LmtFtr[0];
            n2086Contrato_LmtFtr = BC000G54_n2086Contrato_LmtFtr[0];
            A1357Contrato_PrdFtrIni = BC000G54_A1357Contrato_PrdFtrIni[0];
            n1357Contrato_PrdFtrIni = BC000G54_n1357Contrato_PrdFtrIni[0];
            A1358Contrato_PrdFtrFim = BC000G54_A1358Contrato_PrdFtrFim[0];
            n1358Contrato_PrdFtrFim = BC000G54_n1358Contrato_PrdFtrFim[0];
            A39Contratada_Codigo = BC000G54_A39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = BC000G54_A75Contrato_AreaTrabalhoCod[0];
            A1013Contrato_PrepostoCod = BC000G54_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = BC000G54_n1013Contrato_PrepostoCod[0];
            A40Contratada_PessoaCod = BC000G54_A40Contratada_PessoaCod[0];
            A1016Contrato_PrepostoPesCod = BC000G54_A1016Contrato_PrepostoPesCod[0];
            n1016Contrato_PrepostoPesCod = BC000G54_n1016Contrato_PrepostoPesCod[0];
            A842Contrato_DataInicioTA = BC000G54_A842Contrato_DataInicioTA[0];
            n842Contrato_DataInicioTA = BC000G54_n842Contrato_DataInicioTA[0];
            A843Contrato_DataFimTA = BC000G54_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = BC000G54_n843Contrato_DataFimTA[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext0G17( )
      {
         /* Scan next routine */
         pr_default.readNext(36);
         RcdFound17 = 0;
         ScanKeyLoad0G17( ) ;
      }

      protected void ScanKeyLoad0G17( )
      {
         sMode17 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(36) != 101) )
         {
            RcdFound17 = 1;
            A74Contrato_Codigo = BC000G54_A74Contrato_Codigo[0];
            n74Contrato_Codigo = BC000G54_n74Contrato_Codigo[0];
            A76Contrato_AreaTrabalhoDes = BC000G54_A76Contrato_AreaTrabalhoDes[0];
            n76Contrato_AreaTrabalhoDes = BC000G54_n76Contrato_AreaTrabalhoDes[0];
            A41Contratada_PessoaNom = BC000G54_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = BC000G54_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = BC000G54_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = BC000G54_n42Contratada_PessoaCNPJ[0];
            A438Contratada_Sigla = BC000G54_A438Contratada_Sigla[0];
            A516Contratada_TipoFabrica = BC000G54_A516Contratada_TipoFabrica[0];
            A77Contrato_Numero = BC000G54_A77Contrato_Numero[0];
            A78Contrato_NumeroAta = BC000G54_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = BC000G54_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = BC000G54_A79Contrato_Ano[0];
            A80Contrato_Objeto = BC000G54_A80Contrato_Objeto[0];
            A115Contrato_UnidadeContratacao = BC000G54_A115Contrato_UnidadeContratacao[0];
            A81Contrato_Quantidade = BC000G54_A81Contrato_Quantidade[0];
            A82Contrato_DataVigenciaInicio = BC000G54_A82Contrato_DataVigenciaInicio[0];
            A83Contrato_DataVigenciaTermino = BC000G54_A83Contrato_DataVigenciaTermino[0];
            A84Contrato_DataPublicacaoDOU = BC000G54_A84Contrato_DataPublicacaoDOU[0];
            A85Contrato_DataAssinatura = BC000G54_A85Contrato_DataAssinatura[0];
            A86Contrato_DataPedidoReajuste = BC000G54_A86Contrato_DataPedidoReajuste[0];
            A87Contrato_DataTerminoAta = BC000G54_A87Contrato_DataTerminoAta[0];
            A88Contrato_DataFimAdaptacao = BC000G54_A88Contrato_DataFimAdaptacao[0];
            A89Contrato_Valor = BC000G54_A89Contrato_Valor[0];
            A116Contrato_ValorUnidadeContratacao = BC000G54_A116Contrato_ValorUnidadeContratacao[0];
            A90Contrato_RegrasPagto = BC000G54_A90Contrato_RegrasPagto[0];
            A91Contrato_DiasPagto = BC000G54_A91Contrato_DiasPagto[0];
            A452Contrato_CalculoDivergencia = BC000G54_A452Contrato_CalculoDivergencia[0];
            A453Contrato_IndiceDivergencia = BC000G54_A453Contrato_IndiceDivergencia[0];
            A1150Contrato_AceitaPFFS = BC000G54_A1150Contrato_AceitaPFFS[0];
            n1150Contrato_AceitaPFFS = BC000G54_n1150Contrato_AceitaPFFS[0];
            A92Contrato_Ativo = BC000G54_A92Contrato_Ativo[0];
            A1015Contrato_PrepostoNom = BC000G54_A1015Contrato_PrepostoNom[0];
            n1015Contrato_PrepostoNom = BC000G54_n1015Contrato_PrepostoNom[0];
            A1354Contrato_PrdFtrCada = BC000G54_A1354Contrato_PrdFtrCada[0];
            n1354Contrato_PrdFtrCada = BC000G54_n1354Contrato_PrdFtrCada[0];
            A2086Contrato_LmtFtr = BC000G54_A2086Contrato_LmtFtr[0];
            n2086Contrato_LmtFtr = BC000G54_n2086Contrato_LmtFtr[0];
            A1357Contrato_PrdFtrIni = BC000G54_A1357Contrato_PrdFtrIni[0];
            n1357Contrato_PrdFtrIni = BC000G54_n1357Contrato_PrdFtrIni[0];
            A1358Contrato_PrdFtrFim = BC000G54_A1358Contrato_PrdFtrFim[0];
            n1358Contrato_PrdFtrFim = BC000G54_n1358Contrato_PrdFtrFim[0];
            A39Contratada_Codigo = BC000G54_A39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = BC000G54_A75Contrato_AreaTrabalhoCod[0];
            A1013Contrato_PrepostoCod = BC000G54_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = BC000G54_n1013Contrato_PrepostoCod[0];
            A40Contratada_PessoaCod = BC000G54_A40Contratada_PessoaCod[0];
            A1016Contrato_PrepostoPesCod = BC000G54_A1016Contrato_PrepostoPesCod[0];
            n1016Contrato_PrepostoPesCod = BC000G54_n1016Contrato_PrepostoPesCod[0];
            A842Contrato_DataInicioTA = BC000G54_A842Contrato_DataInicioTA[0];
            n842Contrato_DataInicioTA = BC000G54_n842Contrato_DataInicioTA[0];
            A843Contrato_DataFimTA = BC000G54_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = BC000G54_n843Contrato_DataFimTA[0];
         }
         Gx_mode = sMode17;
      }

      protected void ScanKeyEnd0G17( )
      {
         pr_default.close(36);
      }

      protected void AfterConfirm0G17( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0G17( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0G17( )
      {
         /* Before Update Rules */
         new loadauditcontrato(context ).execute(  "Y", ref  AV24AuditingObject,  A74Contrato_Codigo,  Gx_mode) ;
      }

      protected void BeforeDelete0G17( )
      {
         /* Before Delete Rules */
         new loadauditcontrato(context ).execute(  "Y", ref  AV24AuditingObject,  A74Contrato_Codigo,  Gx_mode) ;
      }

      protected void BeforeComplete0G17( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditcontrato(context ).execute(  "N", ref  AV24AuditingObject,  A74Contrato_Codigo,  Gx_mode) ;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditcontrato(context ).execute(  "N", ref  AV24AuditingObject,  A74Contrato_Codigo,  Gx_mode) ;
         }
      }

      protected void BeforeValidate0G17( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0G17( )
      {
      }

      protected void AddRow0G17( )
      {
         VarsToRow17( bcContrato) ;
      }

      protected void ReadRow0G17( )
      {
         RowToVars17( bcContrato, 1) ;
      }

      protected void InitializeNonKey0G17( )
      {
         AV24AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV22SaldoContrato_Codigo = 0;
         A1870Contrato_ValorUndCntAtual = 0;
         A1869Contrato_DataTermino = DateTime.MinValue;
         A2096Contrato_Identificacao = "";
         A76Contrato_AreaTrabalhoDes = "";
         n76Contrato_AreaTrabalhoDes = false;
         A39Contratada_Codigo = 0;
         A40Contratada_PessoaCod = 0;
         A41Contratada_PessoaNom = "";
         n41Contratada_PessoaNom = false;
         A42Contratada_PessoaCNPJ = "";
         n42Contratada_PessoaCNPJ = false;
         A438Contratada_Sigla = "";
         A516Contratada_TipoFabrica = "";
         A77Contrato_Numero = "";
         A78Contrato_NumeroAta = "";
         n78Contrato_NumeroAta = false;
         A79Contrato_Ano = 0;
         A80Contrato_Objeto = "";
         A115Contrato_UnidadeContratacao = 0;
         A81Contrato_Quantidade = 0;
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         A842Contrato_DataInicioTA = DateTime.MinValue;
         n842Contrato_DataInicioTA = false;
         A843Contrato_DataFimTA = DateTime.MinValue;
         n843Contrato_DataFimTA = false;
         A84Contrato_DataPublicacaoDOU = DateTime.MinValue;
         A85Contrato_DataAssinatura = DateTime.MinValue;
         A86Contrato_DataPedidoReajuste = DateTime.MinValue;
         A87Contrato_DataTerminoAta = DateTime.MinValue;
         A88Contrato_DataFimAdaptacao = DateTime.MinValue;
         A89Contrato_Valor = 0;
         A116Contrato_ValorUnidadeContratacao = 0;
         A90Contrato_RegrasPagto = "";
         A1150Contrato_AceitaPFFS = false;
         n1150Contrato_AceitaPFFS = false;
         A1013Contrato_PrepostoCod = 0;
         n1013Contrato_PrepostoCod = false;
         A1016Contrato_PrepostoPesCod = 0;
         n1016Contrato_PrepostoPesCod = false;
         A1015Contrato_PrepostoNom = "";
         n1015Contrato_PrepostoNom = false;
         A1354Contrato_PrdFtrCada = "";
         n1354Contrato_PrdFtrCada = false;
         A2086Contrato_LmtFtr = 0;
         n2086Contrato_LmtFtr = false;
         A1357Contrato_PrdFtrIni = 0;
         n1357Contrato_PrdFtrIni = false;
         A1358Contrato_PrdFtrFim = 0;
         n1358Contrato_PrdFtrFim = false;
         A75Contrato_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A91Contrato_DiasPagto = 30;
         A452Contrato_CalculoDivergencia = "B";
         A453Contrato_IndiceDivergencia = (decimal)(10);
         A92Contrato_Ativo = true;
         O1013Contrato_PrepostoCod = A1013Contrato_PrepostoCod;
         n1013Contrato_PrepostoCod = false;
         Z77Contrato_Numero = "";
         Z78Contrato_NumeroAta = "";
         Z79Contrato_Ano = 0;
         Z115Contrato_UnidadeContratacao = 0;
         Z81Contrato_Quantidade = 0;
         Z82Contrato_DataVigenciaInicio = DateTime.MinValue;
         Z83Contrato_DataVigenciaTermino = DateTime.MinValue;
         Z84Contrato_DataPublicacaoDOU = DateTime.MinValue;
         Z85Contrato_DataAssinatura = DateTime.MinValue;
         Z86Contrato_DataPedidoReajuste = DateTime.MinValue;
         Z87Contrato_DataTerminoAta = DateTime.MinValue;
         Z88Contrato_DataFimAdaptacao = DateTime.MinValue;
         Z89Contrato_Valor = 0;
         Z116Contrato_ValorUnidadeContratacao = 0;
         Z91Contrato_DiasPagto = 0;
         Z452Contrato_CalculoDivergencia = "";
         Z453Contrato_IndiceDivergencia = 0;
         Z1150Contrato_AceitaPFFS = false;
         Z92Contrato_Ativo = false;
         Z1354Contrato_PrdFtrCada = "";
         Z2086Contrato_LmtFtr = 0;
         Z1357Contrato_PrdFtrIni = 0;
         Z1358Contrato_PrdFtrFim = 0;
         Z39Contratada_Codigo = 0;
         Z75Contrato_AreaTrabalhoCod = 0;
         Z1013Contrato_PrepostoCod = 0;
      }

      protected void InitAll0G17( )
      {
         A74Contrato_Codigo = 0;
         n74Contrato_Codigo = false;
         InitializeNonKey0G17( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A92Contrato_Ativo = i92Contrato_Ativo;
         A453Contrato_IndiceDivergencia = i453Contrato_IndiceDivergencia;
         A452Contrato_CalculoDivergencia = i452Contrato_CalculoDivergencia;
         A91Contrato_DiasPagto = i91Contrato_DiasPagto;
         A75Contrato_AreaTrabalhoCod = i75Contrato_AreaTrabalhoCod;
      }

      public void VarsToRow17( SdtContrato obj17 )
      {
         obj17.gxTpr_Mode = Gx_mode;
         obj17.gxTpr_Contrato_valorundcntatual = A1870Contrato_ValorUndCntAtual;
         obj17.gxTpr_Contrato_datatermino = A1869Contrato_DataTermino;
         obj17.gxTpr_Contrato_identificacao = A2096Contrato_Identificacao;
         obj17.gxTpr_Contrato_areatrabalhodes = A76Contrato_AreaTrabalhoDes;
         obj17.gxTpr_Contratada_codigo = A39Contratada_Codigo;
         obj17.gxTpr_Contratada_pessoacod = A40Contratada_PessoaCod;
         obj17.gxTpr_Contratada_pessoanom = A41Contratada_PessoaNom;
         obj17.gxTpr_Contratada_pessoacnpj = A42Contratada_PessoaCNPJ;
         obj17.gxTpr_Contratada_sigla = A438Contratada_Sigla;
         obj17.gxTpr_Contratada_tipofabrica = A516Contratada_TipoFabrica;
         obj17.gxTpr_Contrato_numero = A77Contrato_Numero;
         obj17.gxTpr_Contrato_numeroata = A78Contrato_NumeroAta;
         obj17.gxTpr_Contrato_ano = A79Contrato_Ano;
         obj17.gxTpr_Contrato_objeto = A80Contrato_Objeto;
         obj17.gxTpr_Contrato_unidadecontratacao = A115Contrato_UnidadeContratacao;
         obj17.gxTpr_Contrato_quantidade = A81Contrato_Quantidade;
         obj17.gxTpr_Contrato_datavigenciainicio = A82Contrato_DataVigenciaInicio;
         obj17.gxTpr_Contrato_datavigenciatermino = A83Contrato_DataVigenciaTermino;
         obj17.gxTpr_Contrato_datainiciota = A842Contrato_DataInicioTA;
         obj17.gxTpr_Contrato_datafimta = A843Contrato_DataFimTA;
         obj17.gxTpr_Contrato_datapublicacaodou = A84Contrato_DataPublicacaoDOU;
         obj17.gxTpr_Contrato_dataassinatura = A85Contrato_DataAssinatura;
         obj17.gxTpr_Contrato_datapedidoreajuste = A86Contrato_DataPedidoReajuste;
         obj17.gxTpr_Contrato_dataterminoata = A87Contrato_DataTerminoAta;
         obj17.gxTpr_Contrato_datafimadaptacao = A88Contrato_DataFimAdaptacao;
         obj17.gxTpr_Contrato_valor = A89Contrato_Valor;
         obj17.gxTpr_Contrato_valorunidadecontratacao = A116Contrato_ValorUnidadeContratacao;
         obj17.gxTpr_Contrato_regraspagto = A90Contrato_RegrasPagto;
         obj17.gxTpr_Contrato_aceitapffs = A1150Contrato_AceitaPFFS;
         obj17.gxTpr_Contrato_prepostocod = A1013Contrato_PrepostoCod;
         obj17.gxTpr_Contrato_prepostopescod = A1016Contrato_PrepostoPesCod;
         obj17.gxTpr_Contrato_prepostonom = A1015Contrato_PrepostoNom;
         obj17.gxTpr_Contrato_prdftrcada = A1354Contrato_PrdFtrCada;
         obj17.gxTpr_Contrato_lmtftr = A2086Contrato_LmtFtr;
         obj17.gxTpr_Contrato_prdftrini = A1357Contrato_PrdFtrIni;
         obj17.gxTpr_Contrato_prdftrfim = A1358Contrato_PrdFtrFim;
         obj17.gxTpr_Contrato_areatrabalhocod = A75Contrato_AreaTrabalhoCod;
         obj17.gxTpr_Contrato_diaspagto = A91Contrato_DiasPagto;
         obj17.gxTpr_Contrato_calculodivergencia = A452Contrato_CalculoDivergencia;
         obj17.gxTpr_Contrato_indicedivergencia = A453Contrato_IndiceDivergencia;
         obj17.gxTpr_Contrato_ativo = A92Contrato_Ativo;
         obj17.gxTpr_Contrato_codigo = A74Contrato_Codigo;
         obj17.gxTpr_Contrato_codigo_Z = Z74Contrato_Codigo;
         obj17.gxTpr_Contrato_areatrabalhocod_Z = Z75Contrato_AreaTrabalhoCod;
         obj17.gxTpr_Contrato_areatrabalhodes_Z = Z76Contrato_AreaTrabalhoDes;
         obj17.gxTpr_Contratada_codigo_Z = Z39Contratada_Codigo;
         obj17.gxTpr_Contratada_pessoacod_Z = Z40Contratada_PessoaCod;
         obj17.gxTpr_Contratada_pessoanom_Z = Z41Contratada_PessoaNom;
         obj17.gxTpr_Contratada_pessoacnpj_Z = Z42Contratada_PessoaCNPJ;
         obj17.gxTpr_Contratada_sigla_Z = Z438Contratada_Sigla;
         obj17.gxTpr_Contratada_tipofabrica_Z = Z516Contratada_TipoFabrica;
         obj17.gxTpr_Contrato_numero_Z = Z77Contrato_Numero;
         obj17.gxTpr_Contrato_numeroata_Z = Z78Contrato_NumeroAta;
         obj17.gxTpr_Contrato_ano_Z = Z79Contrato_Ano;
         obj17.gxTpr_Contrato_unidadecontratacao_Z = Z115Contrato_UnidadeContratacao;
         obj17.gxTpr_Contrato_quantidade_Z = Z81Contrato_Quantidade;
         obj17.gxTpr_Contrato_datavigenciainicio_Z = Z82Contrato_DataVigenciaInicio;
         obj17.gxTpr_Contrato_datavigenciatermino_Z = Z83Contrato_DataVigenciaTermino;
         obj17.gxTpr_Contrato_datainiciota_Z = Z842Contrato_DataInicioTA;
         obj17.gxTpr_Contrato_datafimta_Z = Z843Contrato_DataFimTA;
         obj17.gxTpr_Contrato_datapublicacaodou_Z = Z84Contrato_DataPublicacaoDOU;
         obj17.gxTpr_Contrato_datatermino_Z = Z1869Contrato_DataTermino;
         obj17.gxTpr_Contrato_dataassinatura_Z = Z85Contrato_DataAssinatura;
         obj17.gxTpr_Contrato_datapedidoreajuste_Z = Z86Contrato_DataPedidoReajuste;
         obj17.gxTpr_Contrato_dataterminoata_Z = Z87Contrato_DataTerminoAta;
         obj17.gxTpr_Contrato_datafimadaptacao_Z = Z88Contrato_DataFimAdaptacao;
         obj17.gxTpr_Contrato_valor_Z = Z89Contrato_Valor;
         obj17.gxTpr_Contrato_valorunidadecontratacao_Z = Z116Contrato_ValorUnidadeContratacao;
         obj17.gxTpr_Contrato_valorundcntatual_Z = Z1870Contrato_ValorUndCntAtual;
         obj17.gxTpr_Contrato_diaspagto_Z = Z91Contrato_DiasPagto;
         obj17.gxTpr_Contrato_calculodivergencia_Z = Z452Contrato_CalculoDivergencia;
         obj17.gxTpr_Contrato_indicedivergencia_Z = Z453Contrato_IndiceDivergencia;
         obj17.gxTpr_Contrato_aceitapffs_Z = Z1150Contrato_AceitaPFFS;
         obj17.gxTpr_Contrato_ativo_Z = Z92Contrato_Ativo;
         obj17.gxTpr_Contrato_prepostocod_Z = Z1013Contrato_PrepostoCod;
         obj17.gxTpr_Contrato_prepostopescod_Z = Z1016Contrato_PrepostoPesCod;
         obj17.gxTpr_Contrato_prepostonom_Z = Z1015Contrato_PrepostoNom;
         obj17.gxTpr_Contrato_prdftrcada_Z = Z1354Contrato_PrdFtrCada;
         obj17.gxTpr_Contrato_lmtftr_Z = Z2086Contrato_LmtFtr;
         obj17.gxTpr_Contrato_prdftrini_Z = Z1357Contrato_PrdFtrIni;
         obj17.gxTpr_Contrato_prdftrfim_Z = Z1358Contrato_PrdFtrFim;
         obj17.gxTpr_Contrato_identificacao_Z = Z2096Contrato_Identificacao;
         obj17.gxTpr_Contrato_codigo_N = (short)(Convert.ToInt16(n74Contrato_Codigo));
         obj17.gxTpr_Contrato_areatrabalhodes_N = (short)(Convert.ToInt16(n76Contrato_AreaTrabalhoDes));
         obj17.gxTpr_Contratada_pessoanom_N = (short)(Convert.ToInt16(n41Contratada_PessoaNom));
         obj17.gxTpr_Contratada_pessoacnpj_N = (short)(Convert.ToInt16(n42Contratada_PessoaCNPJ));
         obj17.gxTpr_Contrato_numeroata_N = (short)(Convert.ToInt16(n78Contrato_NumeroAta));
         obj17.gxTpr_Contrato_datainiciota_N = (short)(Convert.ToInt16(n842Contrato_DataInicioTA));
         obj17.gxTpr_Contrato_datafimta_N = (short)(Convert.ToInt16(n843Contrato_DataFimTA));
         obj17.gxTpr_Contrato_aceitapffs_N = (short)(Convert.ToInt16(n1150Contrato_AceitaPFFS));
         obj17.gxTpr_Contrato_prepostocod_N = (short)(Convert.ToInt16(n1013Contrato_PrepostoCod));
         obj17.gxTpr_Contrato_prepostopescod_N = (short)(Convert.ToInt16(n1016Contrato_PrepostoPesCod));
         obj17.gxTpr_Contrato_prepostonom_N = (short)(Convert.ToInt16(n1015Contrato_PrepostoNom));
         obj17.gxTpr_Contrato_prdftrcada_N = (short)(Convert.ToInt16(n1354Contrato_PrdFtrCada));
         obj17.gxTpr_Contrato_lmtftr_N = (short)(Convert.ToInt16(n2086Contrato_LmtFtr));
         obj17.gxTpr_Contrato_prdftrini_N = (short)(Convert.ToInt16(n1357Contrato_PrdFtrIni));
         obj17.gxTpr_Contrato_prdftrfim_N = (short)(Convert.ToInt16(n1358Contrato_PrdFtrFim));
         obj17.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow17( SdtContrato obj17 )
      {
         obj17.gxTpr_Contrato_codigo = A74Contrato_Codigo;
         return  ;
      }

      public void RowToVars17( SdtContrato obj17 ,
                               int forceLoad )
      {
         Gx_mode = obj17.gxTpr_Mode;
         A1870Contrato_ValorUndCntAtual = obj17.gxTpr_Contrato_valorundcntatual;
         A1869Contrato_DataTermino = obj17.gxTpr_Contrato_datatermino;
         A2096Contrato_Identificacao = obj17.gxTpr_Contrato_identificacao;
         A76Contrato_AreaTrabalhoDes = obj17.gxTpr_Contrato_areatrabalhodes;
         n76Contrato_AreaTrabalhoDes = false;
         A39Contratada_Codigo = obj17.gxTpr_Contratada_codigo;
         A40Contratada_PessoaCod = obj17.gxTpr_Contratada_pessoacod;
         A41Contratada_PessoaNom = obj17.gxTpr_Contratada_pessoanom;
         n41Contratada_PessoaNom = false;
         A42Contratada_PessoaCNPJ = obj17.gxTpr_Contratada_pessoacnpj;
         n42Contratada_PessoaCNPJ = false;
         A438Contratada_Sigla = obj17.gxTpr_Contratada_sigla;
         A516Contratada_TipoFabrica = obj17.gxTpr_Contratada_tipofabrica;
         A77Contrato_Numero = obj17.gxTpr_Contrato_numero;
         A78Contrato_NumeroAta = obj17.gxTpr_Contrato_numeroata;
         n78Contrato_NumeroAta = false;
         A79Contrato_Ano = obj17.gxTpr_Contrato_ano;
         A80Contrato_Objeto = obj17.gxTpr_Contrato_objeto;
         A115Contrato_UnidadeContratacao = obj17.gxTpr_Contrato_unidadecontratacao;
         A81Contrato_Quantidade = obj17.gxTpr_Contrato_quantidade;
         A82Contrato_DataVigenciaInicio = obj17.gxTpr_Contrato_datavigenciainicio;
         A83Contrato_DataVigenciaTermino = obj17.gxTpr_Contrato_datavigenciatermino;
         A842Contrato_DataInicioTA = obj17.gxTpr_Contrato_datainiciota;
         n842Contrato_DataInicioTA = false;
         A843Contrato_DataFimTA = obj17.gxTpr_Contrato_datafimta;
         n843Contrato_DataFimTA = false;
         A84Contrato_DataPublicacaoDOU = obj17.gxTpr_Contrato_datapublicacaodou;
         A85Contrato_DataAssinatura = obj17.gxTpr_Contrato_dataassinatura;
         A86Contrato_DataPedidoReajuste = obj17.gxTpr_Contrato_datapedidoreajuste;
         A87Contrato_DataTerminoAta = obj17.gxTpr_Contrato_dataterminoata;
         A88Contrato_DataFimAdaptacao = obj17.gxTpr_Contrato_datafimadaptacao;
         A89Contrato_Valor = obj17.gxTpr_Contrato_valor;
         A116Contrato_ValorUnidadeContratacao = obj17.gxTpr_Contrato_valorunidadecontratacao;
         A90Contrato_RegrasPagto = obj17.gxTpr_Contrato_regraspagto;
         A1150Contrato_AceitaPFFS = obj17.gxTpr_Contrato_aceitapffs;
         n1150Contrato_AceitaPFFS = false;
         A1013Contrato_PrepostoCod = obj17.gxTpr_Contrato_prepostocod;
         n1013Contrato_PrepostoCod = false;
         A1016Contrato_PrepostoPesCod = obj17.gxTpr_Contrato_prepostopescod;
         n1016Contrato_PrepostoPesCod = false;
         A1015Contrato_PrepostoNom = obj17.gxTpr_Contrato_prepostonom;
         n1015Contrato_PrepostoNom = false;
         A1354Contrato_PrdFtrCada = obj17.gxTpr_Contrato_prdftrcada;
         n1354Contrato_PrdFtrCada = false;
         A2086Contrato_LmtFtr = obj17.gxTpr_Contrato_lmtftr;
         n2086Contrato_LmtFtr = false;
         A1357Contrato_PrdFtrIni = obj17.gxTpr_Contrato_prdftrini;
         n1357Contrato_PrdFtrIni = false;
         A1358Contrato_PrdFtrFim = obj17.gxTpr_Contrato_prdftrfim;
         n1358Contrato_PrdFtrFim = false;
         A75Contrato_AreaTrabalhoCod = obj17.gxTpr_Contrato_areatrabalhocod;
         A91Contrato_DiasPagto = obj17.gxTpr_Contrato_diaspagto;
         A452Contrato_CalculoDivergencia = obj17.gxTpr_Contrato_calculodivergencia;
         A453Contrato_IndiceDivergencia = obj17.gxTpr_Contrato_indicedivergencia;
         A92Contrato_Ativo = obj17.gxTpr_Contrato_ativo;
         A74Contrato_Codigo = obj17.gxTpr_Contrato_codigo;
         n74Contrato_Codigo = false;
         Z74Contrato_Codigo = obj17.gxTpr_Contrato_codigo_Z;
         Z75Contrato_AreaTrabalhoCod = obj17.gxTpr_Contrato_areatrabalhocod_Z;
         Z76Contrato_AreaTrabalhoDes = obj17.gxTpr_Contrato_areatrabalhodes_Z;
         Z39Contratada_Codigo = obj17.gxTpr_Contratada_codigo_Z;
         Z40Contratada_PessoaCod = obj17.gxTpr_Contratada_pessoacod_Z;
         Z41Contratada_PessoaNom = obj17.gxTpr_Contratada_pessoanom_Z;
         Z42Contratada_PessoaCNPJ = obj17.gxTpr_Contratada_pessoacnpj_Z;
         Z438Contratada_Sigla = obj17.gxTpr_Contratada_sigla_Z;
         Z516Contratada_TipoFabrica = obj17.gxTpr_Contratada_tipofabrica_Z;
         Z77Contrato_Numero = obj17.gxTpr_Contrato_numero_Z;
         Z78Contrato_NumeroAta = obj17.gxTpr_Contrato_numeroata_Z;
         Z79Contrato_Ano = obj17.gxTpr_Contrato_ano_Z;
         Z115Contrato_UnidadeContratacao = obj17.gxTpr_Contrato_unidadecontratacao_Z;
         Z81Contrato_Quantidade = obj17.gxTpr_Contrato_quantidade_Z;
         Z82Contrato_DataVigenciaInicio = obj17.gxTpr_Contrato_datavigenciainicio_Z;
         Z83Contrato_DataVigenciaTermino = obj17.gxTpr_Contrato_datavigenciatermino_Z;
         Z842Contrato_DataInicioTA = obj17.gxTpr_Contrato_datainiciota_Z;
         Z843Contrato_DataFimTA = obj17.gxTpr_Contrato_datafimta_Z;
         Z84Contrato_DataPublicacaoDOU = obj17.gxTpr_Contrato_datapublicacaodou_Z;
         Z1869Contrato_DataTermino = obj17.gxTpr_Contrato_datatermino_Z;
         Z85Contrato_DataAssinatura = obj17.gxTpr_Contrato_dataassinatura_Z;
         Z86Contrato_DataPedidoReajuste = obj17.gxTpr_Contrato_datapedidoreajuste_Z;
         Z87Contrato_DataTerminoAta = obj17.gxTpr_Contrato_dataterminoata_Z;
         Z88Contrato_DataFimAdaptacao = obj17.gxTpr_Contrato_datafimadaptacao_Z;
         Z89Contrato_Valor = obj17.gxTpr_Contrato_valor_Z;
         Z116Contrato_ValorUnidadeContratacao = obj17.gxTpr_Contrato_valorunidadecontratacao_Z;
         Z1870Contrato_ValorUndCntAtual = obj17.gxTpr_Contrato_valorundcntatual_Z;
         Z91Contrato_DiasPagto = obj17.gxTpr_Contrato_diaspagto_Z;
         Z452Contrato_CalculoDivergencia = obj17.gxTpr_Contrato_calculodivergencia_Z;
         Z453Contrato_IndiceDivergencia = obj17.gxTpr_Contrato_indicedivergencia_Z;
         Z1150Contrato_AceitaPFFS = obj17.gxTpr_Contrato_aceitapffs_Z;
         Z92Contrato_Ativo = obj17.gxTpr_Contrato_ativo_Z;
         Z1013Contrato_PrepostoCod = obj17.gxTpr_Contrato_prepostocod_Z;
         O1013Contrato_PrepostoCod = obj17.gxTpr_Contrato_prepostocod_Z;
         Z1016Contrato_PrepostoPesCod = obj17.gxTpr_Contrato_prepostopescod_Z;
         Z1015Contrato_PrepostoNom = obj17.gxTpr_Contrato_prepostonom_Z;
         Z1354Contrato_PrdFtrCada = obj17.gxTpr_Contrato_prdftrcada_Z;
         Z2086Contrato_LmtFtr = obj17.gxTpr_Contrato_lmtftr_Z;
         Z1357Contrato_PrdFtrIni = obj17.gxTpr_Contrato_prdftrini_Z;
         Z1358Contrato_PrdFtrFim = obj17.gxTpr_Contrato_prdftrfim_Z;
         Z2096Contrato_Identificacao = obj17.gxTpr_Contrato_identificacao_Z;
         n74Contrato_Codigo = (bool)(Convert.ToBoolean(obj17.gxTpr_Contrato_codigo_N));
         n76Contrato_AreaTrabalhoDes = (bool)(Convert.ToBoolean(obj17.gxTpr_Contrato_areatrabalhodes_N));
         n41Contratada_PessoaNom = (bool)(Convert.ToBoolean(obj17.gxTpr_Contratada_pessoanom_N));
         n42Contratada_PessoaCNPJ = (bool)(Convert.ToBoolean(obj17.gxTpr_Contratada_pessoacnpj_N));
         n78Contrato_NumeroAta = (bool)(Convert.ToBoolean(obj17.gxTpr_Contrato_numeroata_N));
         n842Contrato_DataInicioTA = (bool)(Convert.ToBoolean(obj17.gxTpr_Contrato_datainiciota_N));
         n843Contrato_DataFimTA = (bool)(Convert.ToBoolean(obj17.gxTpr_Contrato_datafimta_N));
         n1150Contrato_AceitaPFFS = (bool)(Convert.ToBoolean(obj17.gxTpr_Contrato_aceitapffs_N));
         n1013Contrato_PrepostoCod = (bool)(Convert.ToBoolean(obj17.gxTpr_Contrato_prepostocod_N));
         n1016Contrato_PrepostoPesCod = (bool)(Convert.ToBoolean(obj17.gxTpr_Contrato_prepostopescod_N));
         n1015Contrato_PrepostoNom = (bool)(Convert.ToBoolean(obj17.gxTpr_Contrato_prepostonom_N));
         n1354Contrato_PrdFtrCada = (bool)(Convert.ToBoolean(obj17.gxTpr_Contrato_prdftrcada_N));
         n2086Contrato_LmtFtr = (bool)(Convert.ToBoolean(obj17.gxTpr_Contrato_lmtftr_N));
         n1357Contrato_PrdFtrIni = (bool)(Convert.ToBoolean(obj17.gxTpr_Contrato_prdftrini_N));
         n1358Contrato_PrdFtrFim = (bool)(Convert.ToBoolean(obj17.gxTpr_Contrato_prdftrfim_N));
         Gx_mode = obj17.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A74Contrato_Codigo = (int)getParm(obj,0);
         n74Contrato_Codigo = false;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey0G17( ) ;
         ScanKeyStart0G17( ) ;
         if ( RcdFound17 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z74Contrato_Codigo = A74Contrato_Codigo;
            O1013Contrato_PrepostoCod = A1013Contrato_PrepostoCod;
            n1013Contrato_PrepostoCod = false;
         }
         ZM0G17( -30) ;
         OnLoadActions0G17( ) ;
         AddRow0G17( ) ;
         ScanKeyEnd0G17( ) ;
         if ( RcdFound17 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars17( bcContrato, 0) ;
         ScanKeyStart0G17( ) ;
         if ( RcdFound17 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z74Contrato_Codigo = A74Contrato_Codigo;
            O1013Contrato_PrepostoCod = A1013Contrato_PrepostoCod;
            n1013Contrato_PrepostoCod = false;
         }
         ZM0G17( -30) ;
         OnLoadActions0G17( ) ;
         AddRow0G17( ) ;
         ScanKeyEnd0G17( ) ;
         if ( RcdFound17 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars17( bcContrato, 0) ;
         nKeyPressed = 1;
         GetKey0G17( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert0G17( ) ;
         }
         else
         {
            if ( RcdFound17 == 1 )
            {
               if ( A74Contrato_Codigo != Z74Contrato_Codigo )
               {
                  A74Contrato_Codigo = Z74Contrato_Codigo;
                  n74Contrato_Codigo = false;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update0G17( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A74Contrato_Codigo != Z74Contrato_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0G17( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0G17( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow17( bcContrato) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars17( bcContrato, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey0G17( ) ;
         if ( RcdFound17 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A74Contrato_Codigo != Z74Contrato_Codigo )
            {
               A74Contrato_Codigo = Z74Contrato_Codigo;
               n74Contrato_Codigo = false;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A74Contrato_Codigo != Z74Contrato_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(17);
         pr_default.close(16);
         pr_default.close(19);
         pr_default.close(18);
         pr_default.close(20);
         pr_default.close(14);
         pr_default.close(15);
         context.RollbackDataStores( "Contrato_BC");
         VarsToRow17( bcContrato) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContrato.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContrato.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContrato )
         {
            bcContrato = (SdtContrato)(sdt);
            if ( StringUtil.StrCmp(bcContrato.gxTpr_Mode, "") == 0 )
            {
               bcContrato.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow17( bcContrato) ;
            }
            else
            {
               RowToVars17( bcContrato, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContrato.gxTpr_Mode, "") == 0 )
            {
               bcContrato.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars17( bcContrato, 1) ;
         return  ;
      }

      public SdtContrato Contrato_BC
      {
         get {
            return bcContrato ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      protected decimal GetContrato_ValorUndCntAtual1( int E74Contrato_Codigo )
      {
         X1361ContratoTermoAditivo_VlrUntUndCnt = 0;
         X315ContratoTermoAditivo_Codigo = 0;
         /* Using cursor BC000G55 */
         pr_default.execute(37, new Object[] {nA74Contrato_Codigo, E74Contrato_Codigo});
         if ( (pr_default.getStatus(37) != 101) )
         {
            X315ContratoTermoAditivo_Codigo = BC000G55_A315ContratoTermoAditivo_Codigo[0];
            /* Using cursor BC000G56 */
            pr_default.execute(38, new Object[] {nE74Contrato_Codigo, E74Contrato_Codigo, X315ContratoTermoAditivo_Codigo});
            if ( (pr_default.getStatus(38) != 101) )
            {
               X1361ContratoTermoAditivo_VlrUntUndCnt = BC000G56_A1361ContratoTermoAditivo_VlrUntUndCnt[0];
               nX1361ContratoTermoAditivo_VlrUntUndCnt = BC000G56_n1361ContratoTermoAditivo_VlrUntUndCnt[0];
            }
            pr_default.close(38);
         }
         pr_default.close(37);
         return X1361ContratoTermoAditivo_VlrUntUndCnt ;
      }

      protected decimal GetContrato_ValorUndCntAtual0( int E74Contrato_Codigo )
      {
         X1361ContratoTermoAditivo_VlrUntUndCnt = 0;
         X315ContratoTermoAditivo_Codigo = 0;
         /* Using cursor BC000G57 */
         pr_default.execute(39, new Object[] {nE74Contrato_Codigo, E74Contrato_Codigo});
         if ( (pr_default.getStatus(39) != 101) )
         {
            X315ContratoTermoAditivo_Codigo = BC000G57_A315ContratoTermoAditivo_Codigo[0];
            /* Using cursor BC000G58 */
            pr_default.execute(40, new Object[] {nE74Contrato_Codigo, E74Contrato_Codigo, X315ContratoTermoAditivo_Codigo});
            if ( (pr_default.getStatus(40) != 101) )
            {
               X1361ContratoTermoAditivo_VlrUntUndCnt = BC000G58_A1361ContratoTermoAditivo_VlrUntUndCnt[0];
               nX1361ContratoTermoAditivo_VlrUntUndCnt = BC000G58_n1361ContratoTermoAditivo_VlrUntUndCnt[0];
            }
            pr_default.close(40);
         }
         pr_default.close(39);
         return X1361ContratoTermoAditivo_VlrUntUndCnt ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(17);
         pr_default.close(16);
         pr_default.close(19);
         pr_default.close(18);
         pr_default.close(20);
         pr_default.close(14);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV27Pgmname = "";
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV24AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         Z77Contrato_Numero = "";
         A77Contrato_Numero = "";
         Z78Contrato_NumeroAta = "";
         A78Contrato_NumeroAta = "";
         Z82Contrato_DataVigenciaInicio = DateTime.MinValue;
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         Z83Contrato_DataVigenciaTermino = DateTime.MinValue;
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         Z84Contrato_DataPublicacaoDOU = DateTime.MinValue;
         A84Contrato_DataPublicacaoDOU = DateTime.MinValue;
         Z85Contrato_DataAssinatura = DateTime.MinValue;
         A85Contrato_DataAssinatura = DateTime.MinValue;
         Z86Contrato_DataPedidoReajuste = DateTime.MinValue;
         A86Contrato_DataPedidoReajuste = DateTime.MinValue;
         Z87Contrato_DataTerminoAta = DateTime.MinValue;
         A87Contrato_DataTerminoAta = DateTime.MinValue;
         Z88Contrato_DataFimAdaptacao = DateTime.MinValue;
         A88Contrato_DataFimAdaptacao = DateTime.MinValue;
         Z452Contrato_CalculoDivergencia = "";
         A452Contrato_CalculoDivergencia = "";
         Z1354Contrato_PrdFtrCada = "";
         A1354Contrato_PrdFtrCada = "";
         Z842Contrato_DataInicioTA = DateTime.MinValue;
         A842Contrato_DataInicioTA = DateTime.MinValue;
         Z843Contrato_DataFimTA = DateTime.MinValue;
         A843Contrato_DataFimTA = DateTime.MinValue;
         Z1869Contrato_DataTermino = DateTime.MinValue;
         A1869Contrato_DataTermino = DateTime.MinValue;
         Z2096Contrato_Identificacao = "";
         A2096Contrato_Identificacao = "";
         Z438Contratada_Sigla = "";
         A438Contratada_Sigla = "";
         Z516Contratada_TipoFabrica = "";
         A516Contratada_TipoFabrica = "";
         Z76Contrato_AreaTrabalhoDes = "";
         A76Contrato_AreaTrabalhoDes = "";
         Z41Contratada_PessoaNom = "";
         A41Contratada_PessoaNom = "";
         Z42Contratada_PessoaCNPJ = "";
         A42Contratada_PessoaCNPJ = "";
         Z1015Contrato_PrepostoNom = "";
         A1015Contrato_PrepostoNom = "";
         Z80Contrato_Objeto = "";
         A80Contrato_Objeto = "";
         Z90Contrato_RegrasPagto = "";
         A90Contrato_RegrasPagto = "";
         BC000G5_A76Contrato_AreaTrabalhoDes = new String[] {""} ;
         BC000G5_n76Contrato_AreaTrabalhoDes = new bool[] {false} ;
         BC000G19_A74Contrato_Codigo = new int[1] ;
         BC000G19_n74Contrato_Codigo = new bool[] {false} ;
         BC000G19_A76Contrato_AreaTrabalhoDes = new String[] {""} ;
         BC000G19_n76Contrato_AreaTrabalhoDes = new bool[] {false} ;
         BC000G19_A41Contratada_PessoaNom = new String[] {""} ;
         BC000G19_n41Contratada_PessoaNom = new bool[] {false} ;
         BC000G19_A42Contratada_PessoaCNPJ = new String[] {""} ;
         BC000G19_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         BC000G19_A438Contratada_Sigla = new String[] {""} ;
         BC000G19_A516Contratada_TipoFabrica = new String[] {""} ;
         BC000G19_A77Contrato_Numero = new String[] {""} ;
         BC000G19_A78Contrato_NumeroAta = new String[] {""} ;
         BC000G19_n78Contrato_NumeroAta = new bool[] {false} ;
         BC000G19_A79Contrato_Ano = new short[1] ;
         BC000G19_A80Contrato_Objeto = new String[] {""} ;
         BC000G19_A115Contrato_UnidadeContratacao = new short[1] ;
         BC000G19_A81Contrato_Quantidade = new int[1] ;
         BC000G19_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         BC000G19_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         BC000G19_A84Contrato_DataPublicacaoDOU = new DateTime[] {DateTime.MinValue} ;
         BC000G19_A85Contrato_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         BC000G19_A86Contrato_DataPedidoReajuste = new DateTime[] {DateTime.MinValue} ;
         BC000G19_A87Contrato_DataTerminoAta = new DateTime[] {DateTime.MinValue} ;
         BC000G19_A88Contrato_DataFimAdaptacao = new DateTime[] {DateTime.MinValue} ;
         BC000G19_A89Contrato_Valor = new decimal[1] ;
         BC000G19_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         BC000G19_A90Contrato_RegrasPagto = new String[] {""} ;
         BC000G19_A91Contrato_DiasPagto = new short[1] ;
         BC000G19_A452Contrato_CalculoDivergencia = new String[] {""} ;
         BC000G19_A453Contrato_IndiceDivergencia = new decimal[1] ;
         BC000G19_A1150Contrato_AceitaPFFS = new bool[] {false} ;
         BC000G19_n1150Contrato_AceitaPFFS = new bool[] {false} ;
         BC000G19_A92Contrato_Ativo = new bool[] {false} ;
         BC000G19_A1015Contrato_PrepostoNom = new String[] {""} ;
         BC000G19_n1015Contrato_PrepostoNom = new bool[] {false} ;
         BC000G19_A1354Contrato_PrdFtrCada = new String[] {""} ;
         BC000G19_n1354Contrato_PrdFtrCada = new bool[] {false} ;
         BC000G19_A2086Contrato_LmtFtr = new decimal[1] ;
         BC000G19_n2086Contrato_LmtFtr = new bool[] {false} ;
         BC000G19_A1357Contrato_PrdFtrIni = new short[1] ;
         BC000G19_n1357Contrato_PrdFtrIni = new bool[] {false} ;
         BC000G19_A1358Contrato_PrdFtrFim = new short[1] ;
         BC000G19_n1358Contrato_PrdFtrFim = new bool[] {false} ;
         BC000G19_A39Contratada_Codigo = new int[1] ;
         BC000G19_A75Contrato_AreaTrabalhoCod = new int[1] ;
         BC000G19_A1013Contrato_PrepostoCod = new int[1] ;
         BC000G19_n1013Contrato_PrepostoCod = new bool[] {false} ;
         BC000G19_A40Contratada_PessoaCod = new int[1] ;
         BC000G19_A1016Contrato_PrepostoPesCod = new int[1] ;
         BC000G19_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         BC000G19_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         BC000G19_n842Contrato_DataInicioTA = new bool[] {false} ;
         BC000G19_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         BC000G19_n843Contrato_DataFimTA = new bool[] {false} ;
         BC000G11_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         BC000G11_n842Contrato_DataInicioTA = new bool[] {false} ;
         BC000G14_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         BC000G14_n843Contrato_DataFimTA = new bool[] {false} ;
         BC000G4_A438Contratada_Sigla = new String[] {""} ;
         BC000G4_A516Contratada_TipoFabrica = new String[] {""} ;
         BC000G4_A40Contratada_PessoaCod = new int[1] ;
         BC000G7_A41Contratada_PessoaNom = new String[] {""} ;
         BC000G7_n41Contratada_PessoaNom = new bool[] {false} ;
         BC000G7_A42Contratada_PessoaCNPJ = new String[] {""} ;
         BC000G7_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         BC000G6_A1016Contrato_PrepostoPesCod = new int[1] ;
         BC000G6_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         BC000G8_A1015Contrato_PrepostoNom = new String[] {""} ;
         BC000G8_n1015Contrato_PrepostoNom = new bool[] {false} ;
         BC000G20_A74Contrato_Codigo = new int[1] ;
         BC000G20_n74Contrato_Codigo = new bool[] {false} ;
         BC000G3_A74Contrato_Codigo = new int[1] ;
         BC000G3_n74Contrato_Codigo = new bool[] {false} ;
         BC000G3_A77Contrato_Numero = new String[] {""} ;
         BC000G3_A78Contrato_NumeroAta = new String[] {""} ;
         BC000G3_n78Contrato_NumeroAta = new bool[] {false} ;
         BC000G3_A79Contrato_Ano = new short[1] ;
         BC000G3_A80Contrato_Objeto = new String[] {""} ;
         BC000G3_A115Contrato_UnidadeContratacao = new short[1] ;
         BC000G3_A81Contrato_Quantidade = new int[1] ;
         BC000G3_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         BC000G3_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         BC000G3_A84Contrato_DataPublicacaoDOU = new DateTime[] {DateTime.MinValue} ;
         BC000G3_A85Contrato_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         BC000G3_A86Contrato_DataPedidoReajuste = new DateTime[] {DateTime.MinValue} ;
         BC000G3_A87Contrato_DataTerminoAta = new DateTime[] {DateTime.MinValue} ;
         BC000G3_A88Contrato_DataFimAdaptacao = new DateTime[] {DateTime.MinValue} ;
         BC000G3_A89Contrato_Valor = new decimal[1] ;
         BC000G3_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         BC000G3_A90Contrato_RegrasPagto = new String[] {""} ;
         BC000G3_A91Contrato_DiasPagto = new short[1] ;
         BC000G3_A452Contrato_CalculoDivergencia = new String[] {""} ;
         BC000G3_A453Contrato_IndiceDivergencia = new decimal[1] ;
         BC000G3_A1150Contrato_AceitaPFFS = new bool[] {false} ;
         BC000G3_n1150Contrato_AceitaPFFS = new bool[] {false} ;
         BC000G3_A92Contrato_Ativo = new bool[] {false} ;
         BC000G3_A1354Contrato_PrdFtrCada = new String[] {""} ;
         BC000G3_n1354Contrato_PrdFtrCada = new bool[] {false} ;
         BC000G3_A2086Contrato_LmtFtr = new decimal[1] ;
         BC000G3_n2086Contrato_LmtFtr = new bool[] {false} ;
         BC000G3_A1357Contrato_PrdFtrIni = new short[1] ;
         BC000G3_n1357Contrato_PrdFtrIni = new bool[] {false} ;
         BC000G3_A1358Contrato_PrdFtrFim = new short[1] ;
         BC000G3_n1358Contrato_PrdFtrFim = new bool[] {false} ;
         BC000G3_A39Contratada_Codigo = new int[1] ;
         BC000G3_A75Contrato_AreaTrabalhoCod = new int[1] ;
         BC000G3_A1013Contrato_PrepostoCod = new int[1] ;
         BC000G3_n1013Contrato_PrepostoCod = new bool[] {false} ;
         sMode17 = "";
         BC000G2_A74Contrato_Codigo = new int[1] ;
         BC000G2_n74Contrato_Codigo = new bool[] {false} ;
         BC000G2_A77Contrato_Numero = new String[] {""} ;
         BC000G2_A78Contrato_NumeroAta = new String[] {""} ;
         BC000G2_n78Contrato_NumeroAta = new bool[] {false} ;
         BC000G2_A79Contrato_Ano = new short[1] ;
         BC000G2_A80Contrato_Objeto = new String[] {""} ;
         BC000G2_A115Contrato_UnidadeContratacao = new short[1] ;
         BC000G2_A81Contrato_Quantidade = new int[1] ;
         BC000G2_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         BC000G2_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         BC000G2_A84Contrato_DataPublicacaoDOU = new DateTime[] {DateTime.MinValue} ;
         BC000G2_A85Contrato_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         BC000G2_A86Contrato_DataPedidoReajuste = new DateTime[] {DateTime.MinValue} ;
         BC000G2_A87Contrato_DataTerminoAta = new DateTime[] {DateTime.MinValue} ;
         BC000G2_A88Contrato_DataFimAdaptacao = new DateTime[] {DateTime.MinValue} ;
         BC000G2_A89Contrato_Valor = new decimal[1] ;
         BC000G2_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         BC000G2_A90Contrato_RegrasPagto = new String[] {""} ;
         BC000G2_A91Contrato_DiasPagto = new short[1] ;
         BC000G2_A452Contrato_CalculoDivergencia = new String[] {""} ;
         BC000G2_A453Contrato_IndiceDivergencia = new decimal[1] ;
         BC000G2_A1150Contrato_AceitaPFFS = new bool[] {false} ;
         BC000G2_n1150Contrato_AceitaPFFS = new bool[] {false} ;
         BC000G2_A92Contrato_Ativo = new bool[] {false} ;
         BC000G2_A1354Contrato_PrdFtrCada = new String[] {""} ;
         BC000G2_n1354Contrato_PrdFtrCada = new bool[] {false} ;
         BC000G2_A2086Contrato_LmtFtr = new decimal[1] ;
         BC000G2_n2086Contrato_LmtFtr = new bool[] {false} ;
         BC000G2_A1357Contrato_PrdFtrIni = new short[1] ;
         BC000G2_n1357Contrato_PrdFtrIni = new bool[] {false} ;
         BC000G2_A1358Contrato_PrdFtrFim = new short[1] ;
         BC000G2_n1358Contrato_PrdFtrFim = new bool[] {false} ;
         BC000G2_A39Contratada_Codigo = new int[1] ;
         BC000G2_A75Contrato_AreaTrabalhoCod = new int[1] ;
         BC000G2_A1013Contrato_PrepostoCod = new int[1] ;
         BC000G2_n1013Contrato_PrepostoCod = new bool[] {false} ;
         BC000G21_A74Contrato_Codigo = new int[1] ;
         BC000G21_n74Contrato_Codigo = new bool[] {false} ;
         BC000G3_n115Contrato_UnidadeContratacao = new bool[] {false} ;
         BC000G26_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         BC000G26_n842Contrato_DataInicioTA = new bool[] {false} ;
         BC000G29_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         BC000G29_n843Contrato_DataFimTA = new bool[] {false} ;
         BC000G30_A76Contrato_AreaTrabalhoDes = new String[] {""} ;
         BC000G30_n76Contrato_AreaTrabalhoDes = new bool[] {false} ;
         BC000G31_A438Contratada_Sigla = new String[] {""} ;
         BC000G31_A516Contratada_TipoFabrica = new String[] {""} ;
         BC000G31_A40Contratada_PessoaCod = new int[1] ;
         BC000G32_A41Contratada_PessoaNom = new String[] {""} ;
         BC000G32_n41Contratada_PessoaNom = new bool[] {false} ;
         BC000G32_A42Contratada_PessoaCNPJ = new String[] {""} ;
         BC000G32_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         BC000G33_A1016Contrato_PrepostoPesCod = new int[1] ;
         BC000G33_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         BC000G34_A1015Contrato_PrepostoNom = new String[] {""} ;
         BC000G34_n1015Contrato_PrepostoNom = new bool[] {false} ;
         BC000G35_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         BC000G35_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         BC000G36_A1725ContratoSistemas_CntCod = new int[1] ;
         BC000G36_A1726ContratoSistemas_SistemaCod = new int[1] ;
         BC000G37_A1207ContratoUnidades_ContratoCod = new int[1] ;
         BC000G37_A1204ContratoUnidades_UndMedCod = new int[1] ;
         BC000G38_A1078ContratoGestor_ContratoCod = new int[1] ;
         BC000G38_A1079ContratoGestor_UsuarioCod = new int[1] ;
         BC000G39_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         BC000G40_A1561SaldoContrato_Codigo = new int[1] ;
         BC000G41_A321ContratoObrigacao_Codigo = new int[1] ;
         BC000G42_A315ContratoTermoAditivo_Codigo = new int[1] ;
         BC000G43_A314ContratoDadosCertame_Codigo = new int[1] ;
         BC000G44_A294ContratoOcorrencia_Codigo = new int[1] ;
         BC000G45_A160ContratoServicos_Codigo = new int[1] ;
         BC000G46_A152ContratoClausulas_Codigo = new int[1] ;
         BC000G47_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         BC000G48_A101ContratoGarantia_Codigo = new int[1] ;
         BC000G49_A2098ContratoCaixaEntrada_Codigo = new int[1] ;
         BC000G49_A74Contrato_Codigo = new int[1] ;
         BC000G49_n74Contrato_Codigo = new bool[] {false} ;
         BC000G54_A74Contrato_Codigo = new int[1] ;
         BC000G54_n74Contrato_Codigo = new bool[] {false} ;
         BC000G54_A76Contrato_AreaTrabalhoDes = new String[] {""} ;
         BC000G54_n76Contrato_AreaTrabalhoDes = new bool[] {false} ;
         BC000G54_A41Contratada_PessoaNom = new String[] {""} ;
         BC000G54_n41Contratada_PessoaNom = new bool[] {false} ;
         BC000G54_A42Contratada_PessoaCNPJ = new String[] {""} ;
         BC000G54_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         BC000G54_A438Contratada_Sigla = new String[] {""} ;
         BC000G54_A516Contratada_TipoFabrica = new String[] {""} ;
         BC000G54_A77Contrato_Numero = new String[] {""} ;
         BC000G54_A78Contrato_NumeroAta = new String[] {""} ;
         BC000G54_n78Contrato_NumeroAta = new bool[] {false} ;
         BC000G54_A79Contrato_Ano = new short[1] ;
         BC000G54_A80Contrato_Objeto = new String[] {""} ;
         BC000G54_A115Contrato_UnidadeContratacao = new short[1] ;
         BC000G54_A81Contrato_Quantidade = new int[1] ;
         BC000G54_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         BC000G54_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         BC000G54_A84Contrato_DataPublicacaoDOU = new DateTime[] {DateTime.MinValue} ;
         BC000G54_A85Contrato_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         BC000G54_A86Contrato_DataPedidoReajuste = new DateTime[] {DateTime.MinValue} ;
         BC000G54_A87Contrato_DataTerminoAta = new DateTime[] {DateTime.MinValue} ;
         BC000G54_A88Contrato_DataFimAdaptacao = new DateTime[] {DateTime.MinValue} ;
         BC000G54_A89Contrato_Valor = new decimal[1] ;
         BC000G54_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         BC000G54_A90Contrato_RegrasPagto = new String[] {""} ;
         BC000G54_A91Contrato_DiasPagto = new short[1] ;
         BC000G54_A452Contrato_CalculoDivergencia = new String[] {""} ;
         BC000G54_A453Contrato_IndiceDivergencia = new decimal[1] ;
         BC000G54_A1150Contrato_AceitaPFFS = new bool[] {false} ;
         BC000G54_n1150Contrato_AceitaPFFS = new bool[] {false} ;
         BC000G54_A92Contrato_Ativo = new bool[] {false} ;
         BC000G54_A1015Contrato_PrepostoNom = new String[] {""} ;
         BC000G54_n1015Contrato_PrepostoNom = new bool[] {false} ;
         BC000G54_A1354Contrato_PrdFtrCada = new String[] {""} ;
         BC000G54_n1354Contrato_PrdFtrCada = new bool[] {false} ;
         BC000G54_A2086Contrato_LmtFtr = new decimal[1] ;
         BC000G54_n2086Contrato_LmtFtr = new bool[] {false} ;
         BC000G54_A1357Contrato_PrdFtrIni = new short[1] ;
         BC000G54_n1357Contrato_PrdFtrIni = new bool[] {false} ;
         BC000G54_A1358Contrato_PrdFtrFim = new short[1] ;
         BC000G54_n1358Contrato_PrdFtrFim = new bool[] {false} ;
         BC000G54_A39Contratada_Codigo = new int[1] ;
         BC000G54_A75Contrato_AreaTrabalhoCod = new int[1] ;
         BC000G54_A1013Contrato_PrepostoCod = new int[1] ;
         BC000G54_n1013Contrato_PrepostoCod = new bool[] {false} ;
         BC000G54_A40Contratada_PessoaCod = new int[1] ;
         BC000G54_A1016Contrato_PrepostoPesCod = new int[1] ;
         BC000G54_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         BC000G54_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         BC000G54_n842Contrato_DataInicioTA = new bool[] {false} ;
         BC000G54_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         BC000G54_n843Contrato_DataFimTA = new bool[] {false} ;
         i452Contrato_CalculoDivergencia = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC000G55_A315ContratoTermoAditivo_Codigo = new int[1] ;
         BC000G56_A1361ContratoTermoAditivo_VlrUntUndCnt = new decimal[1] ;
         BC000G56_n1361ContratoTermoAditivo_VlrUntUndCnt = new bool[] {false} ;
         BC000G57_A315ContratoTermoAditivo_Codigo = new int[1] ;
         BC000G58_A1361ContratoTermoAditivo_VlrUntUndCnt = new decimal[1] ;
         BC000G58_n1361ContratoTermoAditivo_VlrUntUndCnt = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contrato_bc__default(),
            new Object[][] {
                new Object[] {
               BC000G2_A74Contrato_Codigo, BC000G2_A77Contrato_Numero, BC000G2_A78Contrato_NumeroAta, BC000G2_n78Contrato_NumeroAta, BC000G2_A79Contrato_Ano, BC000G2_A80Contrato_Objeto, BC000G2_A115Contrato_UnidadeContratacao, BC000G2_A81Contrato_Quantidade, BC000G2_A82Contrato_DataVigenciaInicio, BC000G2_A83Contrato_DataVigenciaTermino,
               BC000G2_A84Contrato_DataPublicacaoDOU, BC000G2_A85Contrato_DataAssinatura, BC000G2_A86Contrato_DataPedidoReajuste, BC000G2_A87Contrato_DataTerminoAta, BC000G2_A88Contrato_DataFimAdaptacao, BC000G2_A89Contrato_Valor, BC000G2_A116Contrato_ValorUnidadeContratacao, BC000G2_A90Contrato_RegrasPagto, BC000G2_A91Contrato_DiasPagto, BC000G2_A452Contrato_CalculoDivergencia,
               BC000G2_A453Contrato_IndiceDivergencia, BC000G2_A1150Contrato_AceitaPFFS, BC000G2_n1150Contrato_AceitaPFFS, BC000G2_A92Contrato_Ativo, BC000G2_A1354Contrato_PrdFtrCada, BC000G2_n1354Contrato_PrdFtrCada, BC000G2_A2086Contrato_LmtFtr, BC000G2_n2086Contrato_LmtFtr, BC000G2_A1357Contrato_PrdFtrIni, BC000G2_n1357Contrato_PrdFtrIni,
               BC000G2_A1358Contrato_PrdFtrFim, BC000G2_n1358Contrato_PrdFtrFim, BC000G2_A39Contratada_Codigo, BC000G2_A75Contrato_AreaTrabalhoCod, BC000G2_A1013Contrato_PrepostoCod, BC000G2_n1013Contrato_PrepostoCod
               }
               , new Object[] {
               BC000G3_A74Contrato_Codigo, BC000G3_A77Contrato_Numero, BC000G3_A78Contrato_NumeroAta, BC000G3_n78Contrato_NumeroAta, BC000G3_A79Contrato_Ano, BC000G3_A80Contrato_Objeto, BC000G3_A115Contrato_UnidadeContratacao, BC000G3_A81Contrato_Quantidade, BC000G3_A82Contrato_DataVigenciaInicio, BC000G3_A83Contrato_DataVigenciaTermino,
               BC000G3_A84Contrato_DataPublicacaoDOU, BC000G3_A85Contrato_DataAssinatura, BC000G3_A86Contrato_DataPedidoReajuste, BC000G3_A87Contrato_DataTerminoAta, BC000G3_A88Contrato_DataFimAdaptacao, BC000G3_A89Contrato_Valor, BC000G3_A116Contrato_ValorUnidadeContratacao, BC000G3_A90Contrato_RegrasPagto, BC000G3_A91Contrato_DiasPagto, BC000G3_A452Contrato_CalculoDivergencia,
               BC000G3_A453Contrato_IndiceDivergencia, BC000G3_A1150Contrato_AceitaPFFS, BC000G3_n1150Contrato_AceitaPFFS, BC000G3_A92Contrato_Ativo, BC000G3_A1354Contrato_PrdFtrCada, BC000G3_n1354Contrato_PrdFtrCada, BC000G3_A2086Contrato_LmtFtr, BC000G3_n2086Contrato_LmtFtr, BC000G3_A1357Contrato_PrdFtrIni, BC000G3_n1357Contrato_PrdFtrIni,
               BC000G3_A1358Contrato_PrdFtrFim, BC000G3_n1358Contrato_PrdFtrFim, BC000G3_A39Contratada_Codigo, BC000G3_A75Contrato_AreaTrabalhoCod, BC000G3_A1013Contrato_PrepostoCod, BC000G3_n1013Contrato_PrepostoCod
               }
               , new Object[] {
               BC000G4_A438Contratada_Sigla, BC000G4_A516Contratada_TipoFabrica, BC000G4_A40Contratada_PessoaCod
               }
               , new Object[] {
               BC000G5_A76Contrato_AreaTrabalhoDes, BC000G5_n76Contrato_AreaTrabalhoDes
               }
               , new Object[] {
               BC000G6_A1016Contrato_PrepostoPesCod, BC000G6_n1016Contrato_PrepostoPesCod
               }
               , new Object[] {
               BC000G7_A41Contratada_PessoaNom, BC000G7_n41Contratada_PessoaNom, BC000G7_A42Contratada_PessoaCNPJ, BC000G7_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               BC000G8_A1015Contrato_PrepostoNom, BC000G8_n1015Contrato_PrepostoNom
               }
               , new Object[] {
               BC000G11_A842Contrato_DataInicioTA, BC000G11_n842Contrato_DataInicioTA
               }
               , new Object[] {
               BC000G14_A843Contrato_DataFimTA, BC000G14_n843Contrato_DataFimTA
               }
               , new Object[] {
               BC000G19_A74Contrato_Codigo, BC000G19_A76Contrato_AreaTrabalhoDes, BC000G19_n76Contrato_AreaTrabalhoDes, BC000G19_A41Contratada_PessoaNom, BC000G19_n41Contratada_PessoaNom, BC000G19_A42Contratada_PessoaCNPJ, BC000G19_n42Contratada_PessoaCNPJ, BC000G19_A438Contratada_Sigla, BC000G19_A516Contratada_TipoFabrica, BC000G19_A77Contrato_Numero,
               BC000G19_A78Contrato_NumeroAta, BC000G19_n78Contrato_NumeroAta, BC000G19_A79Contrato_Ano, BC000G19_A80Contrato_Objeto, BC000G19_A115Contrato_UnidadeContratacao, BC000G19_A81Contrato_Quantidade, BC000G19_A82Contrato_DataVigenciaInicio, BC000G19_A83Contrato_DataVigenciaTermino, BC000G19_A84Contrato_DataPublicacaoDOU, BC000G19_A85Contrato_DataAssinatura,
               BC000G19_A86Contrato_DataPedidoReajuste, BC000G19_A87Contrato_DataTerminoAta, BC000G19_A88Contrato_DataFimAdaptacao, BC000G19_A89Contrato_Valor, BC000G19_A116Contrato_ValorUnidadeContratacao, BC000G19_A90Contrato_RegrasPagto, BC000G19_A91Contrato_DiasPagto, BC000G19_A452Contrato_CalculoDivergencia, BC000G19_A453Contrato_IndiceDivergencia, BC000G19_A1150Contrato_AceitaPFFS,
               BC000G19_n1150Contrato_AceitaPFFS, BC000G19_A92Contrato_Ativo, BC000G19_A1015Contrato_PrepostoNom, BC000G19_n1015Contrato_PrepostoNom, BC000G19_A1354Contrato_PrdFtrCada, BC000G19_n1354Contrato_PrdFtrCada, BC000G19_A2086Contrato_LmtFtr, BC000G19_n2086Contrato_LmtFtr, BC000G19_A1357Contrato_PrdFtrIni, BC000G19_n1357Contrato_PrdFtrIni,
               BC000G19_A1358Contrato_PrdFtrFim, BC000G19_n1358Contrato_PrdFtrFim, BC000G19_A39Contratada_Codigo, BC000G19_A75Contrato_AreaTrabalhoCod, BC000G19_A1013Contrato_PrepostoCod, BC000G19_n1013Contrato_PrepostoCod, BC000G19_A40Contratada_PessoaCod, BC000G19_A1016Contrato_PrepostoPesCod, BC000G19_n1016Contrato_PrepostoPesCod, BC000G19_A842Contrato_DataInicioTA,
               BC000G19_n842Contrato_DataInicioTA, BC000G19_A843Contrato_DataFimTA, BC000G19_n843Contrato_DataFimTA
               }
               , new Object[] {
               BC000G20_A74Contrato_Codigo
               }
               , new Object[] {
               BC000G21_A74Contrato_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000G26_A842Contrato_DataInicioTA, BC000G26_n842Contrato_DataInicioTA
               }
               , new Object[] {
               BC000G29_A843Contrato_DataFimTA, BC000G29_n843Contrato_DataFimTA
               }
               , new Object[] {
               BC000G30_A76Contrato_AreaTrabalhoDes, BC000G30_n76Contrato_AreaTrabalhoDes
               }
               , new Object[] {
               BC000G31_A438Contratada_Sigla, BC000G31_A516Contratada_TipoFabrica, BC000G31_A40Contratada_PessoaCod
               }
               , new Object[] {
               BC000G32_A41Contratada_PessoaNom, BC000G32_n41Contratada_PessoaNom, BC000G32_A42Contratada_PessoaCNPJ, BC000G32_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               BC000G33_A1016Contrato_PrepostoPesCod, BC000G33_n1016Contrato_PrepostoPesCod
               }
               , new Object[] {
               BC000G34_A1015Contrato_PrepostoNom, BC000G34_n1015Contrato_PrepostoNom
               }
               , new Object[] {
               BC000G35_A1824ContratoAuxiliar_ContratoCod, BC000G35_A1825ContratoAuxiliar_UsuarioCod
               }
               , new Object[] {
               BC000G36_A1725ContratoSistemas_CntCod, BC000G36_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               BC000G37_A1207ContratoUnidades_ContratoCod, BC000G37_A1204ContratoUnidades_UndMedCod
               }
               , new Object[] {
               BC000G38_A1078ContratoGestor_ContratoCod, BC000G38_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               BC000G39_A1774AutorizacaoConsumo_Codigo
               }
               , new Object[] {
               BC000G40_A1561SaldoContrato_Codigo
               }
               , new Object[] {
               BC000G41_A321ContratoObrigacao_Codigo
               }
               , new Object[] {
               BC000G42_A315ContratoTermoAditivo_Codigo
               }
               , new Object[] {
               BC000G43_A314ContratoDadosCertame_Codigo
               }
               , new Object[] {
               BC000G44_A294ContratoOcorrencia_Codigo
               }
               , new Object[] {
               BC000G45_A160ContratoServicos_Codigo
               }
               , new Object[] {
               BC000G46_A152ContratoClausulas_Codigo
               }
               , new Object[] {
               BC000G47_A108ContratoArquivosAnexos_Codigo
               }
               , new Object[] {
               BC000G48_A101ContratoGarantia_Codigo
               }
               , new Object[] {
               BC000G49_A2098ContratoCaixaEntrada_Codigo, BC000G49_A74Contrato_Codigo
               }
               , new Object[] {
               BC000G54_A74Contrato_Codigo, BC000G54_A76Contrato_AreaTrabalhoDes, BC000G54_n76Contrato_AreaTrabalhoDes, BC000G54_A41Contratada_PessoaNom, BC000G54_n41Contratada_PessoaNom, BC000G54_A42Contratada_PessoaCNPJ, BC000G54_n42Contratada_PessoaCNPJ, BC000G54_A438Contratada_Sigla, BC000G54_A516Contratada_TipoFabrica, BC000G54_A77Contrato_Numero,
               BC000G54_A78Contrato_NumeroAta, BC000G54_n78Contrato_NumeroAta, BC000G54_A79Contrato_Ano, BC000G54_A80Contrato_Objeto, BC000G54_A115Contrato_UnidadeContratacao, BC000G54_A81Contrato_Quantidade, BC000G54_A82Contrato_DataVigenciaInicio, BC000G54_A83Contrato_DataVigenciaTermino, BC000G54_A84Contrato_DataPublicacaoDOU, BC000G54_A85Contrato_DataAssinatura,
               BC000G54_A86Contrato_DataPedidoReajuste, BC000G54_A87Contrato_DataTerminoAta, BC000G54_A88Contrato_DataFimAdaptacao, BC000G54_A89Contrato_Valor, BC000G54_A116Contrato_ValorUnidadeContratacao, BC000G54_A90Contrato_RegrasPagto, BC000G54_A91Contrato_DiasPagto, BC000G54_A452Contrato_CalculoDivergencia, BC000G54_A453Contrato_IndiceDivergencia, BC000G54_A1150Contrato_AceitaPFFS,
               BC000G54_n1150Contrato_AceitaPFFS, BC000G54_A92Contrato_Ativo, BC000G54_A1015Contrato_PrepostoNom, BC000G54_n1015Contrato_PrepostoNom, BC000G54_A1354Contrato_PrdFtrCada, BC000G54_n1354Contrato_PrdFtrCada, BC000G54_A2086Contrato_LmtFtr, BC000G54_n2086Contrato_LmtFtr, BC000G54_A1357Contrato_PrdFtrIni, BC000G54_n1357Contrato_PrdFtrIni,
               BC000G54_A1358Contrato_PrdFtrFim, BC000G54_n1358Contrato_PrdFtrFim, BC000G54_A39Contratada_Codigo, BC000G54_A75Contrato_AreaTrabalhoCod, BC000G54_A1013Contrato_PrepostoCod, BC000G54_n1013Contrato_PrepostoCod, BC000G54_A40Contratada_PessoaCod, BC000G54_A1016Contrato_PrepostoPesCod, BC000G54_n1016Contrato_PrepostoPesCod, BC000G54_A842Contrato_DataInicioTA,
               BC000G54_n842Contrato_DataInicioTA, BC000G54_A843Contrato_DataFimTA, BC000G54_n843Contrato_DataFimTA
               }
               , new Object[] {
               BC000G55_A315ContratoTermoAditivo_Codigo
               }
               , new Object[] {
               BC000G56_A1361ContratoTermoAditivo_VlrUntUndCnt, BC000G56_n1361ContratoTermoAditivo_VlrUntUndCnt
               }
               , new Object[] {
               BC000G57_A315ContratoTermoAditivo_Codigo
               }
               , new Object[] {
               BC000G58_A1361ContratoTermoAditivo_VlrUntUndCnt, BC000G58_n1361ContratoTermoAditivo_VlrUntUndCnt
               }
            }
         );
         Z92Contrato_Ativo = true;
         A92Contrato_Ativo = true;
         i92Contrato_Ativo = true;
         Z453Contrato_IndiceDivergencia = (decimal)(10);
         A453Contrato_IndiceDivergencia = (decimal)(10);
         i453Contrato_IndiceDivergencia = (decimal)(10);
         Z452Contrato_CalculoDivergencia = "B";
         A452Contrato_CalculoDivergencia = "B";
         i452Contrato_CalculoDivergencia = "B";
         Z91Contrato_DiasPagto = 30;
         A91Contrato_DiasPagto = 30;
         i91Contrato_DiasPagto = 30;
         AV27Pgmname = "Contrato_BC";
         Z75Contrato_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A75Contrato_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         i75Contrato_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E120G2 */
         E120G2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z79Contrato_Ano ;
      private short A79Contrato_Ano ;
      private short Z115Contrato_UnidadeContratacao ;
      private short A115Contrato_UnidadeContratacao ;
      private short Z91Contrato_DiasPagto ;
      private short A91Contrato_DiasPagto ;
      private short Z1357Contrato_PrdFtrIni ;
      private short A1357Contrato_PrdFtrIni ;
      private short Z1358Contrato_PrdFtrFim ;
      private short A1358Contrato_PrdFtrFim ;
      private short Gx_BScreen ;
      private short RcdFound17 ;
      private short i91Contrato_DiasPagto ;
      private int trnEnded ;
      private int Z74Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int AV28GXV1 ;
      private int AV11Insert_Contrato_AreaTrabalhoCod ;
      private int AV12Insert_Contratada_Codigo ;
      private int AV16Insert_Contrato_PrepostoCod ;
      private int Z81Contrato_Quantidade ;
      private int A81Contrato_Quantidade ;
      private int Z39Contratada_Codigo ;
      private int A39Contratada_Codigo ;
      private int Z75Contrato_AreaTrabalhoCod ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int Z1013Contrato_PrepostoCod ;
      private int A1013Contrato_PrepostoCod ;
      private int Z40Contratada_PessoaCod ;
      private int A40Contratada_PessoaCod ;
      private int Z1016Contrato_PrepostoPesCod ;
      private int A1016Contrato_PrepostoPesCod ;
      private int O1013Contrato_PrepostoCod ;
      private int AV22SaldoContrato_Codigo ;
      private int i75Contrato_AreaTrabalhoCod ;
      private int X315ContratoTermoAditivo_Codigo ;
      private int E74Contrato_Codigo ;
      private decimal Z89Contrato_Valor ;
      private decimal A89Contrato_Valor ;
      private decimal Z116Contrato_ValorUnidadeContratacao ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal Z453Contrato_IndiceDivergencia ;
      private decimal A453Contrato_IndiceDivergencia ;
      private decimal Z2086Contrato_LmtFtr ;
      private decimal A2086Contrato_LmtFtr ;
      private decimal Z1870Contrato_ValorUndCntAtual ;
      private decimal A1870Contrato_ValorUndCntAtual ;
      private decimal i453Contrato_IndiceDivergencia ;
      private decimal X1361ContratoTermoAditivo_VlrUntUndCnt ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV27Pgmname ;
      private String Z77Contrato_Numero ;
      private String A77Contrato_Numero ;
      private String Z78Contrato_NumeroAta ;
      private String A78Contrato_NumeroAta ;
      private String Z452Contrato_CalculoDivergencia ;
      private String A452Contrato_CalculoDivergencia ;
      private String Z1354Contrato_PrdFtrCada ;
      private String A1354Contrato_PrdFtrCada ;
      private String Z438Contratada_Sigla ;
      private String A438Contratada_Sigla ;
      private String Z516Contratada_TipoFabrica ;
      private String A516Contratada_TipoFabrica ;
      private String Z41Contratada_PessoaNom ;
      private String A41Contratada_PessoaNom ;
      private String Z1015Contrato_PrepostoNom ;
      private String A1015Contrato_PrepostoNom ;
      private String sMode17 ;
      private String i452Contrato_CalculoDivergencia ;
      private DateTime Z82Contrato_DataVigenciaInicio ;
      private DateTime A82Contrato_DataVigenciaInicio ;
      private DateTime Z83Contrato_DataVigenciaTermino ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime Z84Contrato_DataPublicacaoDOU ;
      private DateTime A84Contrato_DataPublicacaoDOU ;
      private DateTime Z85Contrato_DataAssinatura ;
      private DateTime A85Contrato_DataAssinatura ;
      private DateTime Z86Contrato_DataPedidoReajuste ;
      private DateTime A86Contrato_DataPedidoReajuste ;
      private DateTime Z87Contrato_DataTerminoAta ;
      private DateTime A87Contrato_DataTerminoAta ;
      private DateTime Z88Contrato_DataFimAdaptacao ;
      private DateTime A88Contrato_DataFimAdaptacao ;
      private DateTime Z842Contrato_DataInicioTA ;
      private DateTime A842Contrato_DataInicioTA ;
      private DateTime Z843Contrato_DataFimTA ;
      private DateTime A843Contrato_DataFimTA ;
      private DateTime Z1869Contrato_DataTermino ;
      private DateTime A1869Contrato_DataTermino ;
      private bool Z1150Contrato_AceitaPFFS ;
      private bool A1150Contrato_AceitaPFFS ;
      private bool Z92Contrato_Ativo ;
      private bool A92Contrato_Ativo ;
      private bool n76Contrato_AreaTrabalhoDes ;
      private bool n74Contrato_Codigo ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n78Contrato_NumeroAta ;
      private bool n1150Contrato_AceitaPFFS ;
      private bool n1015Contrato_PrepostoNom ;
      private bool n1354Contrato_PrdFtrCada ;
      private bool n2086Contrato_LmtFtr ;
      private bool n1357Contrato_PrdFtrIni ;
      private bool n1358Contrato_PrdFtrFim ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n1016Contrato_PrepostoPesCod ;
      private bool n842Contrato_DataInicioTA ;
      private bool n843Contrato_DataFimTA ;
      private bool Gx_longc ;
      private bool i92Contrato_Ativo ;
      private bool nA74Contrato_Codigo ;
      private bool nE74Contrato_Codigo ;
      private bool nX1361ContratoTermoAditivo_VlrUntUndCnt ;
      private String Z80Contrato_Objeto ;
      private String A80Contrato_Objeto ;
      private String Z90Contrato_RegrasPagto ;
      private String A90Contrato_RegrasPagto ;
      private String Z2096Contrato_Identificacao ;
      private String A2096Contrato_Identificacao ;
      private String Z76Contrato_AreaTrabalhoDes ;
      private String A76Contrato_AreaTrabalhoDes ;
      private String Z42Contratada_PessoaCNPJ ;
      private String A42Contratada_PessoaCNPJ ;
      private IGxSession AV10WebSession ;
      private SdtContrato bcContrato ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC000G5_A76Contrato_AreaTrabalhoDes ;
      private bool[] BC000G5_n76Contrato_AreaTrabalhoDes ;
      private int[] BC000G19_A74Contrato_Codigo ;
      private bool[] BC000G19_n74Contrato_Codigo ;
      private String[] BC000G19_A76Contrato_AreaTrabalhoDes ;
      private bool[] BC000G19_n76Contrato_AreaTrabalhoDes ;
      private String[] BC000G19_A41Contratada_PessoaNom ;
      private bool[] BC000G19_n41Contratada_PessoaNom ;
      private String[] BC000G19_A42Contratada_PessoaCNPJ ;
      private bool[] BC000G19_n42Contratada_PessoaCNPJ ;
      private String[] BC000G19_A438Contratada_Sigla ;
      private String[] BC000G19_A516Contratada_TipoFabrica ;
      private String[] BC000G19_A77Contrato_Numero ;
      private String[] BC000G19_A78Contrato_NumeroAta ;
      private bool[] BC000G19_n78Contrato_NumeroAta ;
      private short[] BC000G19_A79Contrato_Ano ;
      private String[] BC000G19_A80Contrato_Objeto ;
      private short[] BC000G19_A115Contrato_UnidadeContratacao ;
      private int[] BC000G19_A81Contrato_Quantidade ;
      private DateTime[] BC000G19_A82Contrato_DataVigenciaInicio ;
      private DateTime[] BC000G19_A83Contrato_DataVigenciaTermino ;
      private DateTime[] BC000G19_A84Contrato_DataPublicacaoDOU ;
      private DateTime[] BC000G19_A85Contrato_DataAssinatura ;
      private DateTime[] BC000G19_A86Contrato_DataPedidoReajuste ;
      private DateTime[] BC000G19_A87Contrato_DataTerminoAta ;
      private DateTime[] BC000G19_A88Contrato_DataFimAdaptacao ;
      private decimal[] BC000G19_A89Contrato_Valor ;
      private decimal[] BC000G19_A116Contrato_ValorUnidadeContratacao ;
      private String[] BC000G19_A90Contrato_RegrasPagto ;
      private short[] BC000G19_A91Contrato_DiasPagto ;
      private String[] BC000G19_A452Contrato_CalculoDivergencia ;
      private decimal[] BC000G19_A453Contrato_IndiceDivergencia ;
      private bool[] BC000G19_A1150Contrato_AceitaPFFS ;
      private bool[] BC000G19_n1150Contrato_AceitaPFFS ;
      private bool[] BC000G19_A92Contrato_Ativo ;
      private String[] BC000G19_A1015Contrato_PrepostoNom ;
      private bool[] BC000G19_n1015Contrato_PrepostoNom ;
      private String[] BC000G19_A1354Contrato_PrdFtrCada ;
      private bool[] BC000G19_n1354Contrato_PrdFtrCada ;
      private decimal[] BC000G19_A2086Contrato_LmtFtr ;
      private bool[] BC000G19_n2086Contrato_LmtFtr ;
      private short[] BC000G19_A1357Contrato_PrdFtrIni ;
      private bool[] BC000G19_n1357Contrato_PrdFtrIni ;
      private short[] BC000G19_A1358Contrato_PrdFtrFim ;
      private bool[] BC000G19_n1358Contrato_PrdFtrFim ;
      private int[] BC000G19_A39Contratada_Codigo ;
      private int[] BC000G19_A75Contrato_AreaTrabalhoCod ;
      private int[] BC000G19_A1013Contrato_PrepostoCod ;
      private bool[] BC000G19_n1013Contrato_PrepostoCod ;
      private int[] BC000G19_A40Contratada_PessoaCod ;
      private int[] BC000G19_A1016Contrato_PrepostoPesCod ;
      private bool[] BC000G19_n1016Contrato_PrepostoPesCod ;
      private DateTime[] BC000G19_A842Contrato_DataInicioTA ;
      private bool[] BC000G19_n842Contrato_DataInicioTA ;
      private DateTime[] BC000G19_A843Contrato_DataFimTA ;
      private bool[] BC000G19_n843Contrato_DataFimTA ;
      private DateTime[] BC000G11_A842Contrato_DataInicioTA ;
      private bool[] BC000G11_n842Contrato_DataInicioTA ;
      private DateTime[] BC000G14_A843Contrato_DataFimTA ;
      private bool[] BC000G14_n843Contrato_DataFimTA ;
      private String[] BC000G4_A438Contratada_Sigla ;
      private String[] BC000G4_A516Contratada_TipoFabrica ;
      private int[] BC000G4_A40Contratada_PessoaCod ;
      private String[] BC000G7_A41Contratada_PessoaNom ;
      private bool[] BC000G7_n41Contratada_PessoaNom ;
      private String[] BC000G7_A42Contratada_PessoaCNPJ ;
      private bool[] BC000G7_n42Contratada_PessoaCNPJ ;
      private int[] BC000G6_A1016Contrato_PrepostoPesCod ;
      private bool[] BC000G6_n1016Contrato_PrepostoPesCod ;
      private String[] BC000G8_A1015Contrato_PrepostoNom ;
      private bool[] BC000G8_n1015Contrato_PrepostoNom ;
      private int[] BC000G20_A74Contrato_Codigo ;
      private bool[] BC000G20_n74Contrato_Codigo ;
      private int[] BC000G3_A74Contrato_Codigo ;
      private bool[] BC000G3_n74Contrato_Codigo ;
      private String[] BC000G3_A77Contrato_Numero ;
      private String[] BC000G3_A78Contrato_NumeroAta ;
      private bool[] BC000G3_n78Contrato_NumeroAta ;
      private short[] BC000G3_A79Contrato_Ano ;
      private String[] BC000G3_A80Contrato_Objeto ;
      private short[] BC000G3_A115Contrato_UnidadeContratacao ;
      private int[] BC000G3_A81Contrato_Quantidade ;
      private DateTime[] BC000G3_A82Contrato_DataVigenciaInicio ;
      private DateTime[] BC000G3_A83Contrato_DataVigenciaTermino ;
      private DateTime[] BC000G3_A84Contrato_DataPublicacaoDOU ;
      private DateTime[] BC000G3_A85Contrato_DataAssinatura ;
      private DateTime[] BC000G3_A86Contrato_DataPedidoReajuste ;
      private DateTime[] BC000G3_A87Contrato_DataTerminoAta ;
      private DateTime[] BC000G3_A88Contrato_DataFimAdaptacao ;
      private decimal[] BC000G3_A89Contrato_Valor ;
      private decimal[] BC000G3_A116Contrato_ValorUnidadeContratacao ;
      private String[] BC000G3_A90Contrato_RegrasPagto ;
      private short[] BC000G3_A91Contrato_DiasPagto ;
      private String[] BC000G3_A452Contrato_CalculoDivergencia ;
      private decimal[] BC000G3_A453Contrato_IndiceDivergencia ;
      private bool[] BC000G3_A1150Contrato_AceitaPFFS ;
      private bool[] BC000G3_n1150Contrato_AceitaPFFS ;
      private bool[] BC000G3_A92Contrato_Ativo ;
      private String[] BC000G3_A1354Contrato_PrdFtrCada ;
      private bool[] BC000G3_n1354Contrato_PrdFtrCada ;
      private decimal[] BC000G3_A2086Contrato_LmtFtr ;
      private bool[] BC000G3_n2086Contrato_LmtFtr ;
      private short[] BC000G3_A1357Contrato_PrdFtrIni ;
      private bool[] BC000G3_n1357Contrato_PrdFtrIni ;
      private short[] BC000G3_A1358Contrato_PrdFtrFim ;
      private bool[] BC000G3_n1358Contrato_PrdFtrFim ;
      private int[] BC000G3_A39Contratada_Codigo ;
      private int[] BC000G3_A75Contrato_AreaTrabalhoCod ;
      private int[] BC000G3_A1013Contrato_PrepostoCod ;
      private bool[] BC000G3_n1013Contrato_PrepostoCod ;
      private int[] BC000G2_A74Contrato_Codigo ;
      private bool[] BC000G2_n74Contrato_Codigo ;
      private String[] BC000G2_A77Contrato_Numero ;
      private String[] BC000G2_A78Contrato_NumeroAta ;
      private bool[] BC000G2_n78Contrato_NumeroAta ;
      private short[] BC000G2_A79Contrato_Ano ;
      private String[] BC000G2_A80Contrato_Objeto ;
      private short[] BC000G2_A115Contrato_UnidadeContratacao ;
      private int[] BC000G2_A81Contrato_Quantidade ;
      private DateTime[] BC000G2_A82Contrato_DataVigenciaInicio ;
      private DateTime[] BC000G2_A83Contrato_DataVigenciaTermino ;
      private DateTime[] BC000G2_A84Contrato_DataPublicacaoDOU ;
      private DateTime[] BC000G2_A85Contrato_DataAssinatura ;
      private DateTime[] BC000G2_A86Contrato_DataPedidoReajuste ;
      private DateTime[] BC000G2_A87Contrato_DataTerminoAta ;
      private DateTime[] BC000G2_A88Contrato_DataFimAdaptacao ;
      private decimal[] BC000G2_A89Contrato_Valor ;
      private decimal[] BC000G2_A116Contrato_ValorUnidadeContratacao ;
      private String[] BC000G2_A90Contrato_RegrasPagto ;
      private short[] BC000G2_A91Contrato_DiasPagto ;
      private String[] BC000G2_A452Contrato_CalculoDivergencia ;
      private decimal[] BC000G2_A453Contrato_IndiceDivergencia ;
      private bool[] BC000G2_A1150Contrato_AceitaPFFS ;
      private bool[] BC000G2_n1150Contrato_AceitaPFFS ;
      private bool[] BC000G2_A92Contrato_Ativo ;
      private String[] BC000G2_A1354Contrato_PrdFtrCada ;
      private bool[] BC000G2_n1354Contrato_PrdFtrCada ;
      private decimal[] BC000G2_A2086Contrato_LmtFtr ;
      private bool[] BC000G2_n2086Contrato_LmtFtr ;
      private short[] BC000G2_A1357Contrato_PrdFtrIni ;
      private bool[] BC000G2_n1357Contrato_PrdFtrIni ;
      private short[] BC000G2_A1358Contrato_PrdFtrFim ;
      private bool[] BC000G2_n1358Contrato_PrdFtrFim ;
      private int[] BC000G2_A39Contratada_Codigo ;
      private int[] BC000G2_A75Contrato_AreaTrabalhoCod ;
      private int[] BC000G2_A1013Contrato_PrepostoCod ;
      private bool[] BC000G2_n1013Contrato_PrepostoCod ;
      private int[] BC000G21_A74Contrato_Codigo ;
      private bool[] BC000G21_n74Contrato_Codigo ;
      private bool[] BC000G3_n115Contrato_UnidadeContratacao ;
      private DateTime[] BC000G26_A842Contrato_DataInicioTA ;
      private bool[] BC000G26_n842Contrato_DataInicioTA ;
      private DateTime[] BC000G29_A843Contrato_DataFimTA ;
      private bool[] BC000G29_n843Contrato_DataFimTA ;
      private String[] BC000G30_A76Contrato_AreaTrabalhoDes ;
      private bool[] BC000G30_n76Contrato_AreaTrabalhoDes ;
      private String[] BC000G31_A438Contratada_Sigla ;
      private String[] BC000G31_A516Contratada_TipoFabrica ;
      private int[] BC000G31_A40Contratada_PessoaCod ;
      private String[] BC000G32_A41Contratada_PessoaNom ;
      private bool[] BC000G32_n41Contratada_PessoaNom ;
      private String[] BC000G32_A42Contratada_PessoaCNPJ ;
      private bool[] BC000G32_n42Contratada_PessoaCNPJ ;
      private int[] BC000G33_A1016Contrato_PrepostoPesCod ;
      private bool[] BC000G33_n1016Contrato_PrepostoPesCod ;
      private String[] BC000G34_A1015Contrato_PrepostoNom ;
      private bool[] BC000G34_n1015Contrato_PrepostoNom ;
      private int[] BC000G35_A1824ContratoAuxiliar_ContratoCod ;
      private int[] BC000G35_A1825ContratoAuxiliar_UsuarioCod ;
      private int[] BC000G36_A1725ContratoSistemas_CntCod ;
      private int[] BC000G36_A1726ContratoSistemas_SistemaCod ;
      private int[] BC000G37_A1207ContratoUnidades_ContratoCod ;
      private int[] BC000G37_A1204ContratoUnidades_UndMedCod ;
      private int[] BC000G38_A1078ContratoGestor_ContratoCod ;
      private int[] BC000G38_A1079ContratoGestor_UsuarioCod ;
      private int[] BC000G39_A1774AutorizacaoConsumo_Codigo ;
      private int[] BC000G40_A1561SaldoContrato_Codigo ;
      private int[] BC000G41_A321ContratoObrigacao_Codigo ;
      private int[] BC000G42_A315ContratoTermoAditivo_Codigo ;
      private int[] BC000G43_A314ContratoDadosCertame_Codigo ;
      private int[] BC000G44_A294ContratoOcorrencia_Codigo ;
      private int[] BC000G45_A160ContratoServicos_Codigo ;
      private int[] BC000G46_A152ContratoClausulas_Codigo ;
      private int[] BC000G47_A108ContratoArquivosAnexos_Codigo ;
      private int[] BC000G48_A101ContratoGarantia_Codigo ;
      private int[] BC000G49_A2098ContratoCaixaEntrada_Codigo ;
      private int[] BC000G49_A74Contrato_Codigo ;
      private bool[] BC000G49_n74Contrato_Codigo ;
      private int[] BC000G54_A74Contrato_Codigo ;
      private bool[] BC000G54_n74Contrato_Codigo ;
      private String[] BC000G54_A76Contrato_AreaTrabalhoDes ;
      private bool[] BC000G54_n76Contrato_AreaTrabalhoDes ;
      private String[] BC000G54_A41Contratada_PessoaNom ;
      private bool[] BC000G54_n41Contratada_PessoaNom ;
      private String[] BC000G54_A42Contratada_PessoaCNPJ ;
      private bool[] BC000G54_n42Contratada_PessoaCNPJ ;
      private String[] BC000G54_A438Contratada_Sigla ;
      private String[] BC000G54_A516Contratada_TipoFabrica ;
      private String[] BC000G54_A77Contrato_Numero ;
      private String[] BC000G54_A78Contrato_NumeroAta ;
      private bool[] BC000G54_n78Contrato_NumeroAta ;
      private short[] BC000G54_A79Contrato_Ano ;
      private String[] BC000G54_A80Contrato_Objeto ;
      private short[] BC000G54_A115Contrato_UnidadeContratacao ;
      private int[] BC000G54_A81Contrato_Quantidade ;
      private DateTime[] BC000G54_A82Contrato_DataVigenciaInicio ;
      private DateTime[] BC000G54_A83Contrato_DataVigenciaTermino ;
      private DateTime[] BC000G54_A84Contrato_DataPublicacaoDOU ;
      private DateTime[] BC000G54_A85Contrato_DataAssinatura ;
      private DateTime[] BC000G54_A86Contrato_DataPedidoReajuste ;
      private DateTime[] BC000G54_A87Contrato_DataTerminoAta ;
      private DateTime[] BC000G54_A88Contrato_DataFimAdaptacao ;
      private decimal[] BC000G54_A89Contrato_Valor ;
      private decimal[] BC000G54_A116Contrato_ValorUnidadeContratacao ;
      private String[] BC000G54_A90Contrato_RegrasPagto ;
      private short[] BC000G54_A91Contrato_DiasPagto ;
      private String[] BC000G54_A452Contrato_CalculoDivergencia ;
      private decimal[] BC000G54_A453Contrato_IndiceDivergencia ;
      private bool[] BC000G54_A1150Contrato_AceitaPFFS ;
      private bool[] BC000G54_n1150Contrato_AceitaPFFS ;
      private bool[] BC000G54_A92Contrato_Ativo ;
      private String[] BC000G54_A1015Contrato_PrepostoNom ;
      private bool[] BC000G54_n1015Contrato_PrepostoNom ;
      private String[] BC000G54_A1354Contrato_PrdFtrCada ;
      private bool[] BC000G54_n1354Contrato_PrdFtrCada ;
      private decimal[] BC000G54_A2086Contrato_LmtFtr ;
      private bool[] BC000G54_n2086Contrato_LmtFtr ;
      private short[] BC000G54_A1357Contrato_PrdFtrIni ;
      private bool[] BC000G54_n1357Contrato_PrdFtrIni ;
      private short[] BC000G54_A1358Contrato_PrdFtrFim ;
      private bool[] BC000G54_n1358Contrato_PrdFtrFim ;
      private int[] BC000G54_A39Contratada_Codigo ;
      private int[] BC000G54_A75Contrato_AreaTrabalhoCod ;
      private int[] BC000G54_A1013Contrato_PrepostoCod ;
      private bool[] BC000G54_n1013Contrato_PrepostoCod ;
      private int[] BC000G54_A40Contratada_PessoaCod ;
      private int[] BC000G54_A1016Contrato_PrepostoPesCod ;
      private bool[] BC000G54_n1016Contrato_PrepostoPesCod ;
      private DateTime[] BC000G54_A842Contrato_DataInicioTA ;
      private bool[] BC000G54_n842Contrato_DataInicioTA ;
      private DateTime[] BC000G54_A843Contrato_DataFimTA ;
      private bool[] BC000G54_n843Contrato_DataFimTA ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] BC000G55_A315ContratoTermoAditivo_Codigo ;
      private decimal[] BC000G56_A1361ContratoTermoAditivo_VlrUntUndCnt ;
      private bool[] BC000G56_n1361ContratoTermoAditivo_VlrUntUndCnt ;
      private int[] BC000G57_A315ContratoTermoAditivo_Codigo ;
      private decimal[] BC000G58_A1361ContratoTermoAditivo_VlrUntUndCnt ;
      private bool[] BC000G58_n1361ContratoTermoAditivo_VlrUntUndCnt ;
      private wwpbaseobjects.SdtAuditingObject AV24AuditingObject ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class contrato_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
         ,new ForEachCursor(def[40])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC000G19 ;
          prmBC000G19 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC000G19 ;
          cmdBufferBC000G19=" SELECT TM1.[Contrato_Codigo], T4.[AreaTrabalho_Descricao] AS Contrato_AreaTrabalhoDes, T6.[Pessoa_Nome] AS Contratada_PessoaNom, T6.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T5.[Contratada_Sigla], T5.[Contratada_TipoFabrica], TM1.[Contrato_Numero], TM1.[Contrato_NumeroAta], TM1.[Contrato_Ano], TM1.[Contrato_Objeto], TM1.[Contrato_UnidadeContratacao], TM1.[Contrato_Quantidade], TM1.[Contrato_DataVigenciaInicio], TM1.[Contrato_DataVigenciaTermino], TM1.[Contrato_DataPublicacaoDOU], TM1.[Contrato_DataAssinatura], TM1.[Contrato_DataPedidoReajuste], TM1.[Contrato_DataTerminoAta], TM1.[Contrato_DataFimAdaptacao], TM1.[Contrato_Valor], TM1.[Contrato_ValorUnidadeContratacao], TM1.[Contrato_RegrasPagto], TM1.[Contrato_DiasPagto], TM1.[Contrato_CalculoDivergencia], TM1.[Contrato_IndiceDivergencia], TM1.[Contrato_AceitaPFFS], TM1.[Contrato_Ativo], T8.[Pessoa_Nome] AS Contrato_PrepostoNom, TM1.[Contrato_PrdFtrCada], TM1.[Contrato_LmtFtr], TM1.[Contrato_PrdFtrIni], TM1.[Contrato_PrdFtrFim], TM1.[Contratada_Codigo], TM1.[Contrato_AreaTrabalhoCod] AS Contrato_AreaTrabalhoCod, TM1.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T5.[Contratada_PessoaCod] AS Contratada_PessoaCod, T7.[Usuario_PessoaCod] AS Contrato_PrepostoPesCod, COALESCE( T2.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA, COALESCE( T3.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM ((((((([Contrato] TM1 WITH (NOLOCK) LEFT JOIN (SELECT T9.[ContratoTermoAditivo_DataInicio], T9.[Contrato_Codigo], T9.[ContratoTermoAditivo_Codigo], T10.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T9 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] "
          + " ) T10 ON T10.[Contrato_Codigo] = T9.[Contrato_Codigo]) WHERE T9.[ContratoTermoAditivo_Codigo] = T10.[GXC1] ) T2 ON T2.[Contrato_Codigo] = TM1.[Contrato_Codigo]) LEFT JOIN (SELECT T9.[ContratoTermoAditivo_DataFim], T9.[Contrato_Codigo], T9.[ContratoTermoAditivo_Codigo], T10.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T9 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T10 ON T10.[Contrato_Codigo] = T9.[Contrato_Codigo]) WHERE T9.[ContratoTermoAditivo_Codigo] = T10.[GXC1] ) T3 ON T3.[Contrato_Codigo] = TM1.[Contrato_Codigo]) INNER JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = TM1.[Contrato_AreaTrabalhoCod]) INNER JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = TM1.[Contratada_Codigo]) INNER JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Contratada_PessoaCod]) LEFT JOIN [Usuario] T7 WITH (NOLOCK) ON T7.[Usuario_Codigo] = TM1.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T8 WITH (NOLOCK) ON T8.[Pessoa_Codigo] = T7.[Usuario_PessoaCod]) WHERE TM1.[Contrato_Codigo] = @Contrato_Codigo ORDER BY TM1.[Contrato_Codigo]  OPTION (FAST 100)" ;
          Object[] prmBC000G11 ;
          prmBC000G11 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G14 ;
          prmBC000G14 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G5 ;
          prmBC000G5 = new Object[] {
          new Object[] {"@Contrato_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G4 ;
          prmBC000G4 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G7 ;
          prmBC000G7 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G6 ;
          prmBC000G6 = new Object[] {
          new Object[] {"@Contrato_PrepostoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G8 ;
          prmBC000G8 = new Object[] {
          new Object[] {"@Contrato_PrepostoPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G20 ;
          prmBC000G20 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G3 ;
          prmBC000G3 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G2 ;
          prmBC000G2 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G21 ;
          prmBC000G21 = new Object[] {
          new Object[] {"@Contrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@Contrato_NumeroAta",SqlDbType.Char,10,0} ,
          new Object[] {"@Contrato_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contrato_Objeto",SqlDbType.VarChar,10000,0} ,
          new Object[] {"@Contrato_UnidadeContratacao",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@Contrato_Quantidade",SqlDbType.Int,9,0} ,
          new Object[] {"@Contrato_DataVigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataVigenciaTermino",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataPublicacaoDOU",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataAssinatura",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataPedidoReajuste",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataTerminoAta",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataFimAdaptacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contrato_ValorUnidadeContratacao",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contrato_RegrasPagto",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Contrato_DiasPagto",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contrato_CalculoDivergencia",SqlDbType.Char,1,0} ,
          new Object[] {"@Contrato_IndiceDivergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Contrato_AceitaPFFS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contrato_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contrato_PrdFtrCada",SqlDbType.Char,1,0} ,
          new Object[] {"@Contrato_LmtFtr",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contrato_PrdFtrIni",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contrato_PrdFtrFim",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_PrepostoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G22 ;
          prmBC000G22 = new Object[] {
          new Object[] {"@Contrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@Contrato_NumeroAta",SqlDbType.Char,10,0} ,
          new Object[] {"@Contrato_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contrato_Objeto",SqlDbType.VarChar,10000,0} ,
          new Object[] {"@Contrato_UnidadeContratacao",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@Contrato_Quantidade",SqlDbType.Int,9,0} ,
          new Object[] {"@Contrato_DataVigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataVigenciaTermino",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataPublicacaoDOU",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataAssinatura",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataPedidoReajuste",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataTerminoAta",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataFimAdaptacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contrato_ValorUnidadeContratacao",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contrato_RegrasPagto",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Contrato_DiasPagto",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contrato_CalculoDivergencia",SqlDbType.Char,1,0} ,
          new Object[] {"@Contrato_IndiceDivergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Contrato_AceitaPFFS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contrato_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contrato_PrdFtrCada",SqlDbType.Char,1,0} ,
          new Object[] {"@Contrato_LmtFtr",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contrato_PrdFtrIni",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contrato_PrdFtrFim",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_PrepostoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G23 ;
          prmBC000G23 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G26 ;
          prmBC000G26 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G29 ;
          prmBC000G29 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G30 ;
          prmBC000G30 = new Object[] {
          new Object[] {"@Contrato_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G31 ;
          prmBC000G31 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G32 ;
          prmBC000G32 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G33 ;
          prmBC000G33 = new Object[] {
          new Object[] {"@Contrato_PrepostoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G34 ;
          prmBC000G34 = new Object[] {
          new Object[] {"@Contrato_PrepostoPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G35 ;
          prmBC000G35 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G36 ;
          prmBC000G36 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G37 ;
          prmBC000G37 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G38 ;
          prmBC000G38 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G39 ;
          prmBC000G39 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G40 ;
          prmBC000G40 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G41 ;
          prmBC000G41 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G42 ;
          prmBC000G42 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G43 ;
          prmBC000G43 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G44 ;
          prmBC000G44 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G45 ;
          prmBC000G45 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G46 ;
          prmBC000G46 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G47 ;
          prmBC000G47 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G48 ;
          prmBC000G48 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G49 ;
          prmBC000G49 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G54 ;
          prmBC000G54 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC000G54 ;
          cmdBufferBC000G54=" SELECT TM1.[Contrato_Codigo], T4.[AreaTrabalho_Descricao] AS Contrato_AreaTrabalhoDes, T6.[Pessoa_Nome] AS Contratada_PessoaNom, T6.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T5.[Contratada_Sigla], T5.[Contratada_TipoFabrica], TM1.[Contrato_Numero], TM1.[Contrato_NumeroAta], TM1.[Contrato_Ano], TM1.[Contrato_Objeto], TM1.[Contrato_UnidadeContratacao], TM1.[Contrato_Quantidade], TM1.[Contrato_DataVigenciaInicio], TM1.[Contrato_DataVigenciaTermino], TM1.[Contrato_DataPublicacaoDOU], TM1.[Contrato_DataAssinatura], TM1.[Contrato_DataPedidoReajuste], TM1.[Contrato_DataTerminoAta], TM1.[Contrato_DataFimAdaptacao], TM1.[Contrato_Valor], TM1.[Contrato_ValorUnidadeContratacao], TM1.[Contrato_RegrasPagto], TM1.[Contrato_DiasPagto], TM1.[Contrato_CalculoDivergencia], TM1.[Contrato_IndiceDivergencia], TM1.[Contrato_AceitaPFFS], TM1.[Contrato_Ativo], T8.[Pessoa_Nome] AS Contrato_PrepostoNom, TM1.[Contrato_PrdFtrCada], TM1.[Contrato_LmtFtr], TM1.[Contrato_PrdFtrIni], TM1.[Contrato_PrdFtrFim], TM1.[Contratada_Codigo], TM1.[Contrato_AreaTrabalhoCod] AS Contrato_AreaTrabalhoCod, TM1.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T5.[Contratada_PessoaCod] AS Contratada_PessoaCod, T7.[Usuario_PessoaCod] AS Contrato_PrepostoPesCod, COALESCE( T2.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA, COALESCE( T3.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM ((((((([Contrato] TM1 WITH (NOLOCK) LEFT JOIN (SELECT T9.[ContratoTermoAditivo_DataInicio], T9.[Contrato_Codigo], T9.[ContratoTermoAditivo_Codigo], T10.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T9 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] "
          + " ) T10 ON T10.[Contrato_Codigo] = T9.[Contrato_Codigo]) WHERE T9.[ContratoTermoAditivo_Codigo] = T10.[GXC1] ) T2 ON T2.[Contrato_Codigo] = TM1.[Contrato_Codigo]) LEFT JOIN (SELECT T9.[ContratoTermoAditivo_DataFim], T9.[Contrato_Codigo], T9.[ContratoTermoAditivo_Codigo], T10.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T9 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T10 ON T10.[Contrato_Codigo] = T9.[Contrato_Codigo]) WHERE T9.[ContratoTermoAditivo_Codigo] = T10.[GXC1] ) T3 ON T3.[Contrato_Codigo] = TM1.[Contrato_Codigo]) INNER JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = TM1.[Contrato_AreaTrabalhoCod]) INNER JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = TM1.[Contratada_Codigo]) INNER JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Contratada_PessoaCod]) LEFT JOIN [Usuario] T7 WITH (NOLOCK) ON T7.[Usuario_Codigo] = TM1.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T8 WITH (NOLOCK) ON T8.[Pessoa_Codigo] = T7.[Usuario_PessoaCod]) WHERE TM1.[Contrato_Codigo] = @Contrato_Codigo ORDER BY TM1.[Contrato_Codigo]  OPTION (FAST 100)" ;
          Object[] prmBC000G55 ;
          prmBC000G55 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G56 ;
          prmBC000G56 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoTermoAditivo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G57 ;
          prmBC000G57 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000G58 ;
          prmBC000G58 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoTermoAditivo_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC000G2", "SELECT [Contrato_Codigo], [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano], [Contrato_Objeto], [Contrato_UnidadeContratacao], [Contrato_Quantidade], [Contrato_DataVigenciaInicio], [Contrato_DataVigenciaTermino], [Contrato_DataPublicacaoDOU], [Contrato_DataAssinatura], [Contrato_DataPedidoReajuste], [Contrato_DataTerminoAta], [Contrato_DataFimAdaptacao], [Contrato_Valor], [Contrato_ValorUnidadeContratacao], [Contrato_RegrasPagto], [Contrato_DiasPagto], [Contrato_CalculoDivergencia], [Contrato_IndiceDivergencia], [Contrato_AceitaPFFS], [Contrato_Ativo], [Contrato_PrdFtrCada], [Contrato_LmtFtr], [Contrato_PrdFtrIni], [Contrato_PrdFtrFim], [Contratada_Codigo], [Contrato_AreaTrabalhoCod] AS Contrato_AreaTrabalhoCod, [Contrato_PrepostoCod] AS Contrato_PrepostoCod FROM [Contrato] WITH (UPDLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G2,1,0,true,false )
             ,new CursorDef("BC000G3", "SELECT [Contrato_Codigo], [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano], [Contrato_Objeto], [Contrato_UnidadeContratacao], [Contrato_Quantidade], [Contrato_DataVigenciaInicio], [Contrato_DataVigenciaTermino], [Contrato_DataPublicacaoDOU], [Contrato_DataAssinatura], [Contrato_DataPedidoReajuste], [Contrato_DataTerminoAta], [Contrato_DataFimAdaptacao], [Contrato_Valor], [Contrato_ValorUnidadeContratacao], [Contrato_RegrasPagto], [Contrato_DiasPagto], [Contrato_CalculoDivergencia], [Contrato_IndiceDivergencia], [Contrato_AceitaPFFS], [Contrato_Ativo], [Contrato_PrdFtrCada], [Contrato_LmtFtr], [Contrato_PrdFtrIni], [Contrato_PrdFtrFim], [Contratada_Codigo], [Contrato_AreaTrabalhoCod] AS Contrato_AreaTrabalhoCod, [Contrato_PrepostoCod] AS Contrato_PrepostoCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G3,1,0,true,false )
             ,new CursorDef("BC000G4", "SELECT [Contratada_Sigla], [Contratada_TipoFabrica], [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G4,1,0,true,false )
             ,new CursorDef("BC000G5", "SELECT [AreaTrabalho_Descricao] AS Contrato_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Contrato_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G5,1,0,true,false )
             ,new CursorDef("BC000G6", "SELECT [Usuario_PessoaCod] AS Contrato_PrepostoPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Contrato_PrepostoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G6,1,0,true,false )
             ,new CursorDef("BC000G7", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G7,1,0,true,false )
             ,new CursorDef("BC000G8", "SELECT [Pessoa_Nome] AS Contrato_PrepostoNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contrato_PrepostoPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G8,1,0,true,false )
             ,new CursorDef("BC000G11", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA FROM (SELECT T2.[ContratoTermoAditivo_DataInicio], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC1] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G11,1,0,true,false )
             ,new CursorDef("BC000G14", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM (SELECT T2.[ContratoTermoAditivo_DataFim], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC1] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G14,1,0,true,false )
             ,new CursorDef("BC000G19", cmdBufferBC000G19,true, GxErrorMask.GX_NOMASK, false, this,prmBC000G19,100,0,true,false )
             ,new CursorDef("BC000G20", "SELECT [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G20,1,0,true,false )
             ,new CursorDef("BC000G21", "INSERT INTO [Contrato]([Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano], [Contrato_Objeto], [Contrato_UnidadeContratacao], [Contrato_Quantidade], [Contrato_DataVigenciaInicio], [Contrato_DataVigenciaTermino], [Contrato_DataPublicacaoDOU], [Contrato_DataAssinatura], [Contrato_DataPedidoReajuste], [Contrato_DataTerminoAta], [Contrato_DataFimAdaptacao], [Contrato_Valor], [Contrato_ValorUnidadeContratacao], [Contrato_RegrasPagto], [Contrato_DiasPagto], [Contrato_CalculoDivergencia], [Contrato_IndiceDivergencia], [Contrato_AceitaPFFS], [Contrato_Ativo], [Contrato_PrdFtrCada], [Contrato_LmtFtr], [Contrato_PrdFtrIni], [Contrato_PrdFtrFim], [Contratada_Codigo], [Contrato_AreaTrabalhoCod], [Contrato_PrepostoCod]) VALUES(@Contrato_Numero, @Contrato_NumeroAta, @Contrato_Ano, @Contrato_Objeto, @Contrato_UnidadeContratacao, @Contrato_Quantidade, @Contrato_DataVigenciaInicio, @Contrato_DataVigenciaTermino, @Contrato_DataPublicacaoDOU, @Contrato_DataAssinatura, @Contrato_DataPedidoReajuste, @Contrato_DataTerminoAta, @Contrato_DataFimAdaptacao, @Contrato_Valor, @Contrato_ValorUnidadeContratacao, @Contrato_RegrasPagto, @Contrato_DiasPagto, @Contrato_CalculoDivergencia, @Contrato_IndiceDivergencia, @Contrato_AceitaPFFS, @Contrato_Ativo, @Contrato_PrdFtrCada, @Contrato_LmtFtr, @Contrato_PrdFtrIni, @Contrato_PrdFtrFim, @Contratada_Codigo, @Contrato_AreaTrabalhoCod, @Contrato_PrepostoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC000G21)
             ,new CursorDef("BC000G22", "UPDATE [Contrato] SET [Contrato_Numero]=@Contrato_Numero, [Contrato_NumeroAta]=@Contrato_NumeroAta, [Contrato_Ano]=@Contrato_Ano, [Contrato_Objeto]=@Contrato_Objeto, [Contrato_UnidadeContratacao]=@Contrato_UnidadeContratacao, [Contrato_Quantidade]=@Contrato_Quantidade, [Contrato_DataVigenciaInicio]=@Contrato_DataVigenciaInicio, [Contrato_DataVigenciaTermino]=@Contrato_DataVigenciaTermino, [Contrato_DataPublicacaoDOU]=@Contrato_DataPublicacaoDOU, [Contrato_DataAssinatura]=@Contrato_DataAssinatura, [Contrato_DataPedidoReajuste]=@Contrato_DataPedidoReajuste, [Contrato_DataTerminoAta]=@Contrato_DataTerminoAta, [Contrato_DataFimAdaptacao]=@Contrato_DataFimAdaptacao, [Contrato_Valor]=@Contrato_Valor, [Contrato_ValorUnidadeContratacao]=@Contrato_ValorUnidadeContratacao, [Contrato_RegrasPagto]=@Contrato_RegrasPagto, [Contrato_DiasPagto]=@Contrato_DiasPagto, [Contrato_CalculoDivergencia]=@Contrato_CalculoDivergencia, [Contrato_IndiceDivergencia]=@Contrato_IndiceDivergencia, [Contrato_AceitaPFFS]=@Contrato_AceitaPFFS, [Contrato_Ativo]=@Contrato_Ativo, [Contrato_PrdFtrCada]=@Contrato_PrdFtrCada, [Contrato_LmtFtr]=@Contrato_LmtFtr, [Contrato_PrdFtrIni]=@Contrato_PrdFtrIni, [Contrato_PrdFtrFim]=@Contrato_PrdFtrFim, [Contratada_Codigo]=@Contratada_Codigo, [Contrato_AreaTrabalhoCod]=@Contrato_AreaTrabalhoCod, [Contrato_PrepostoCod]=@Contrato_PrepostoCod  WHERE [Contrato_Codigo] = @Contrato_Codigo", GxErrorMask.GX_NOMASK,prmBC000G22)
             ,new CursorDef("BC000G23", "DELETE FROM [Contrato]  WHERE [Contrato_Codigo] = @Contrato_Codigo", GxErrorMask.GX_NOMASK,prmBC000G23)
             ,new CursorDef("BC000G26", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA FROM (SELECT T2.[ContratoTermoAditivo_DataInicio], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC1] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G26,1,0,true,false )
             ,new CursorDef("BC000G29", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM (SELECT T2.[ContratoTermoAditivo_DataFim], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC1] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G29,1,0,true,false )
             ,new CursorDef("BC000G30", "SELECT [AreaTrabalho_Descricao] AS Contrato_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Contrato_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G30,1,0,true,false )
             ,new CursorDef("BC000G31", "SELECT [Contratada_Sigla], [Contratada_TipoFabrica], [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G31,1,0,true,false )
             ,new CursorDef("BC000G32", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G32,1,0,true,false )
             ,new CursorDef("BC000G33", "SELECT [Usuario_PessoaCod] AS Contrato_PrepostoPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Contrato_PrepostoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G33,1,0,true,false )
             ,new CursorDef("BC000G34", "SELECT [Pessoa_Nome] AS Contrato_PrepostoNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contrato_PrepostoPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G34,1,0,true,false )
             ,new CursorDef("BC000G35", "SELECT TOP 1 [ContratoAuxiliar_ContratoCod], [ContratoAuxiliar_UsuarioCod] FROM [ContratoAuxiliar] WITH (NOLOCK) WHERE [ContratoAuxiliar_ContratoCod] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G35,1,0,true,true )
             ,new CursorDef("BC000G36", "SELECT TOP 1 [ContratoSistemas_CntCod], [ContratoSistemas_SistemaCod] FROM [ContratoSistemas] WITH (NOLOCK) WHERE [ContratoSistemas_CntCod] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G36,1,0,true,true )
             ,new CursorDef("BC000G37", "SELECT TOP 1 [ContratoUnidades_ContratoCod], [ContratoUnidades_UndMedCod] FROM [ContratoUnidades] WITH (NOLOCK) WHERE [ContratoUnidades_ContratoCod] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G37,1,0,true,true )
             ,new CursorDef("BC000G38", "SELECT TOP 1 [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G38,1,0,true,true )
             ,new CursorDef("BC000G39", "SELECT TOP 1 [AutorizacaoConsumo_Codigo] FROM [AutorizacaoConsumo] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G39,1,0,true,true )
             ,new CursorDef("BC000G40", "SELECT TOP 1 [SaldoContrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G40,1,0,true,true )
             ,new CursorDef("BC000G41", "SELECT TOP 1 [ContratoObrigacao_Codigo] FROM [ContratoObrigacao] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G41,1,0,true,true )
             ,new CursorDef("BC000G42", "SELECT TOP 1 [ContratoTermoAditivo_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G42,1,0,true,true )
             ,new CursorDef("BC000G43", "SELECT TOP 1 [ContratoDadosCertame_Codigo] FROM [ContratoDadosCertame] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G43,1,0,true,true )
             ,new CursorDef("BC000G44", "SELECT TOP 1 [ContratoOcorrencia_Codigo] FROM [ContratoOcorrencia] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G44,1,0,true,true )
             ,new CursorDef("BC000G45", "SELECT TOP 1 [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G45,1,0,true,true )
             ,new CursorDef("BC000G46", "SELECT TOP 1 [ContratoClausulas_Codigo] FROM [ContratoClausulas] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G46,1,0,true,true )
             ,new CursorDef("BC000G47", "SELECT TOP 1 [ContratoArquivosAnexos_Codigo] FROM [ContratoArquivosAnexos] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G47,1,0,true,true )
             ,new CursorDef("BC000G48", "SELECT TOP 1 [ContratoGarantia_Codigo] FROM [ContratoGarantia] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G48,1,0,true,true )
             ,new CursorDef("BC000G49", "SELECT TOP 1 [ContratoCaixaEntrada_Codigo], [Contrato_Codigo] FROM [ContratoCaixaEntrada] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G49,1,0,true,true )
             ,new CursorDef("BC000G54", cmdBufferBC000G54,true, GxErrorMask.GX_NOMASK, false, this,prmBC000G54,100,0,true,false )
             ,new CursorDef("BC000G55", "SELECT MAX([ContratoTermoAditivo_Codigo]) FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G55,1,0,true,false )
             ,new CursorDef("BC000G56", "SELECT [ContratoTermoAditivo_VlrUntUndCnt] FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE ( [Contrato_Codigo] = @Contrato_Codigo) and ( [ContratoTermoAditivo_Codigo] = @ContratoTermoAditivo_Codigo) ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G56,1,0,true,false )
             ,new CursorDef("BC000G57", "SELECT MAX([ContratoTermoAditivo_Codigo]) FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G57,1,0,true,false )
             ,new CursorDef("BC000G58", "SELECT [ContratoTermoAditivo_VlrUntUndCnt] FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE ( [Contrato_Codigo] = @Contrato_Codigo) and ( [ContratoTermoAditivo_Codigo] = @ContratoTermoAditivo_Codigo) ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000G58,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(9) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(10) ;
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(11) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(12) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(13) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(14) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(15) ;
                ((decimal[]) buf[16])[0] = rslt.getDecimal(16) ;
                ((String[]) buf[17])[0] = rslt.getLongVarchar(17) ;
                ((short[]) buf[18])[0] = rslt.getShort(18) ;
                ((String[]) buf[19])[0] = rslt.getString(19, 1) ;
                ((decimal[]) buf[20])[0] = rslt.getDecimal(20) ;
                ((bool[]) buf[21])[0] = rslt.getBool(21) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(21);
                ((bool[]) buf[23])[0] = rslt.getBool(22) ;
                ((String[]) buf[24])[0] = rslt.getString(23, 1) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(23);
                ((decimal[]) buf[26])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(24);
                ((short[]) buf[28])[0] = rslt.getShort(25) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(25);
                ((short[]) buf[30])[0] = rslt.getShort(26) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(26);
                ((int[]) buf[32])[0] = rslt.getInt(27) ;
                ((int[]) buf[33])[0] = rslt.getInt(28) ;
                ((int[]) buf[34])[0] = rslt.getInt(29) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(29);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(9) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(10) ;
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(11) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(12) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(13) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(14) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(15) ;
                ((decimal[]) buf[16])[0] = rslt.getDecimal(16) ;
                ((String[]) buf[17])[0] = rslt.getLongVarchar(17) ;
                ((short[]) buf[18])[0] = rslt.getShort(18) ;
                ((String[]) buf[19])[0] = rslt.getString(19, 1) ;
                ((decimal[]) buf[20])[0] = rslt.getDecimal(20) ;
                ((bool[]) buf[21])[0] = rslt.getBool(21) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(21);
                ((bool[]) buf[23])[0] = rslt.getBool(22) ;
                ((String[]) buf[24])[0] = rslt.getString(23, 1) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(23);
                ((decimal[]) buf[26])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(24);
                ((short[]) buf[28])[0] = rslt.getShort(25) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(25);
                ((short[]) buf[30])[0] = rslt.getShort(26) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(26);
                ((int[]) buf[32])[0] = rslt.getInt(27) ;
                ((int[]) buf[33])[0] = rslt.getInt(28) ;
                ((int[]) buf[34])[0] = rslt.getInt(29) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(29);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 1) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 20) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((short[]) buf[12])[0] = rslt.getShort(9) ;
                ((String[]) buf[13])[0] = rslt.getLongVarchar(10) ;
                ((short[]) buf[14])[0] = rslt.getShort(11) ;
                ((int[]) buf[15])[0] = rslt.getInt(12) ;
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(13) ;
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(14) ;
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(15) ;
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(16) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(17) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(18) ;
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(19) ;
                ((decimal[]) buf[23])[0] = rslt.getDecimal(20) ;
                ((decimal[]) buf[24])[0] = rslt.getDecimal(21) ;
                ((String[]) buf[25])[0] = rslt.getLongVarchar(22) ;
                ((short[]) buf[26])[0] = rslt.getShort(23) ;
                ((String[]) buf[27])[0] = rslt.getString(24, 1) ;
                ((decimal[]) buf[28])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[29])[0] = rslt.getBool(26) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(26);
                ((bool[]) buf[31])[0] = rslt.getBool(27) ;
                ((String[]) buf[32])[0] = rslt.getString(28, 100) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(28);
                ((String[]) buf[34])[0] = rslt.getString(29, 1) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(29);
                ((decimal[]) buf[36])[0] = rslt.getDecimal(30) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(30);
                ((short[]) buf[38])[0] = rslt.getShort(31) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(31);
                ((short[]) buf[40])[0] = rslt.getShort(32) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(32);
                ((int[]) buf[42])[0] = rslt.getInt(33) ;
                ((int[]) buf[43])[0] = rslt.getInt(34) ;
                ((int[]) buf[44])[0] = rslt.getInt(35) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(35);
                ((int[]) buf[46])[0] = rslt.getInt(36) ;
                ((int[]) buf[47])[0] = rslt.getInt(37) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(37);
                ((DateTime[]) buf[49])[0] = rslt.getGXDate(38) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(38);
                ((DateTime[]) buf[51])[0] = rslt.getGXDate(39) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(39);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 1) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 20) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((short[]) buf[12])[0] = rslt.getShort(9) ;
                ((String[]) buf[13])[0] = rslt.getLongVarchar(10) ;
                ((short[]) buf[14])[0] = rslt.getShort(11) ;
                ((int[]) buf[15])[0] = rslt.getInt(12) ;
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(13) ;
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(14) ;
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(15) ;
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(16) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(17) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(18) ;
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(19) ;
                ((decimal[]) buf[23])[0] = rslt.getDecimal(20) ;
                ((decimal[]) buf[24])[0] = rslt.getDecimal(21) ;
                ((String[]) buf[25])[0] = rslt.getLongVarchar(22) ;
                ((short[]) buf[26])[0] = rslt.getShort(23) ;
                ((String[]) buf[27])[0] = rslt.getString(24, 1) ;
                ((decimal[]) buf[28])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[29])[0] = rslt.getBool(26) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(26);
                ((bool[]) buf[31])[0] = rslt.getBool(27) ;
                ((String[]) buf[32])[0] = rslt.getString(28, 100) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(28);
                ((String[]) buf[34])[0] = rslt.getString(29, 1) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(29);
                ((decimal[]) buf[36])[0] = rslt.getDecimal(30) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(30);
                ((short[]) buf[38])[0] = rslt.getShort(31) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(31);
                ((short[]) buf[40])[0] = rslt.getShort(32) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(32);
                ((int[]) buf[42])[0] = rslt.getInt(33) ;
                ((int[]) buf[43])[0] = rslt.getInt(34) ;
                ((int[]) buf[44])[0] = rslt.getInt(35) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(35);
                ((int[]) buf[46])[0] = rslt.getInt(36) ;
                ((int[]) buf[47])[0] = rslt.getInt(37) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(37);
                ((DateTime[]) buf[49])[0] = rslt.getGXDate(38) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(38);
                ((DateTime[]) buf[51])[0] = rslt.getGXDate(39) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(39);
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 38 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 39 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 40 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (short)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                stmt.SetParameter(5, (short)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                stmt.SetParameter(7, (DateTime)parms[7]);
                stmt.SetParameter(8, (DateTime)parms[8]);
                stmt.SetParameter(9, (DateTime)parms[9]);
                stmt.SetParameter(10, (DateTime)parms[10]);
                stmt.SetParameter(11, (DateTime)parms[11]);
                stmt.SetParameter(12, (DateTime)parms[12]);
                stmt.SetParameter(13, (DateTime)parms[13]);
                stmt.SetParameter(14, (decimal)parms[14]);
                stmt.SetParameter(15, (decimal)parms[15]);
                stmt.SetParameter(16, (String)parms[16]);
                stmt.SetParameter(17, (short)parms[17]);
                stmt.SetParameter(18, (String)parms[18]);
                stmt.SetParameter(19, (decimal)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 20 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(20, (bool)parms[21]);
                }
                stmt.SetParameter(21, (bool)parms[22]);
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 22 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(22, (String)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 23 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(23, (decimal)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 24 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(24, (short)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 25 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(25, (short)parms[30]);
                }
                stmt.SetParameter(26, (int)parms[31]);
                stmt.SetParameter(27, (int)parms[32]);
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[34]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (short)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                stmt.SetParameter(5, (short)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                stmt.SetParameter(7, (DateTime)parms[7]);
                stmt.SetParameter(8, (DateTime)parms[8]);
                stmt.SetParameter(9, (DateTime)parms[9]);
                stmt.SetParameter(10, (DateTime)parms[10]);
                stmt.SetParameter(11, (DateTime)parms[11]);
                stmt.SetParameter(12, (DateTime)parms[12]);
                stmt.SetParameter(13, (DateTime)parms[13]);
                stmt.SetParameter(14, (decimal)parms[14]);
                stmt.SetParameter(15, (decimal)parms[15]);
                stmt.SetParameter(16, (String)parms[16]);
                stmt.SetParameter(17, (short)parms[17]);
                stmt.SetParameter(18, (String)parms[18]);
                stmt.SetParameter(19, (decimal)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 20 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(20, (bool)parms[21]);
                }
                stmt.SetParameter(21, (bool)parms[22]);
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 22 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(22, (String)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 23 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(23, (decimal)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 24 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(24, (short)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 25 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(25, (short)parms[30]);
                }
                stmt.SetParameter(26, (int)parms[31]);
                stmt.SetParameter(27, (int)parms[32]);
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 29 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(29, (int)parms[36]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 26 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 27 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 29 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 31 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 32 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 33 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 34 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 35 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 36 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 37 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 38 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 39 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 40 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
