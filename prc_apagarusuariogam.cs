/*
               File: PRC_ApagarUsuarioGAM
        Description: Apagar Usuario GAM
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:49.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_apagarusuariogam : GXProcedure
   {
      public prc_apagarusuariogam( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_apagarusuariogam( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Usuario_GAMGuid )
      {
         this.AV9Usuario_GAMGuid = aP0_Usuario_GAMGuid;
         initialize();
         executePrivate();
         aP0_Usuario_GAMGuid=this.AV9Usuario_GAMGuid;
      }

      public String executeUdp( )
      {
         this.AV9Usuario_GAMGuid = aP0_Usuario_GAMGuid;
         initialize();
         executePrivate();
         aP0_Usuario_GAMGuid=this.AV9Usuario_GAMGuid;
         return AV9Usuario_GAMGuid ;
      }

      public void executeSubmit( ref String aP0_Usuario_GAMGuid )
      {
         prc_apagarusuariogam objprc_apagarusuariogam;
         objprc_apagarusuariogam = new prc_apagarusuariogam();
         objprc_apagarusuariogam.AV9Usuario_GAMGuid = aP0_Usuario_GAMGuid;
         objprc_apagarusuariogam.context.SetSubmitInitialConfig(context);
         objprc_apagarusuariogam.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_apagarusuariogam);
         aP0_Usuario_GAMGuid=this.AV9Usuario_GAMGuid;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_apagarusuariogam)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8UserGam.load( AV9Usuario_GAMGuid);
         AV8UserGam.delete();
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8UserGam = new SdtGAMUser(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV9Usuario_GAMGuid ;
      private String aP0_Usuario_GAMGuid ;
      private SdtGAMUser AV8UserGam ;
   }

}
