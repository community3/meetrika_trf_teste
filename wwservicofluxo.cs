/*
               File: WWServicoFluxo
        Description:  Sequ�ncia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:43:20.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwservicofluxo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwservicofluxo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwservicofluxo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbServicoFluxo_ServicoTpHrq = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_88 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_88_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_88_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ServicoFluxo_ServicoSigla1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoFluxo_ServicoSigla1", AV17ServicoFluxo_ServicoSigla1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21ServicoFluxo_ServicoSigla2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ServicoFluxo_ServicoSigla2", AV21ServicoFluxo_ServicoSigla2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25ServicoFluxo_ServicoSigla3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ServicoFluxo_ServicoSigla3", AV25ServicoFluxo_ServicoSigla3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV34TFServicoFluxo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFServicoFluxo_Codigo), 6, 0)));
               AV35TFServicoFluxo_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFServicoFluxo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFServicoFluxo_Codigo_To), 6, 0)));
               AV38TFServicoFluxo_ServicoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFServicoFluxo_ServicoCod), 6, 0)));
               AV39TFServicoFluxo_ServicoCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFServicoFluxo_ServicoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFServicoFluxo_ServicoCod_To), 6, 0)));
               AV42TFServicoFluxo_ServicoSigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFServicoFluxo_ServicoSigla", AV42TFServicoFluxo_ServicoSigla);
               AV43TFServicoFluxo_ServicoSigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFServicoFluxo_ServicoSigla_Sel", AV43TFServicoFluxo_ServicoSigla_Sel);
               AV50TFServicoFluxo_Ordem = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFServicoFluxo_Ordem), 3, 0)));
               AV51TFServicoFluxo_Ordem_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFServicoFluxo_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFServicoFluxo_Ordem_To), 3, 0)));
               AV54TFServicoFluxo_ServicoPos = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFServicoFluxo_ServicoPos), 6, 0)));
               AV55TFServicoFluxo_ServicoPos_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFServicoFluxo_ServicoPos_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFServicoFluxo_ServicoPos_To), 6, 0)));
               AV58TFServicoFluxo_SrvPosSigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFServicoFluxo_SrvPosSigla", AV58TFServicoFluxo_SrvPosSigla);
               AV59TFServicoFluxo_SrvPosSigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFServicoFluxo_SrvPosSigla_Sel", AV59TFServicoFluxo_SrvPosSigla_Sel);
               AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace", AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace);
               AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace", AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace);
               AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace", AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace);
               AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace", AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace);
               AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace", AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace);
               AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace", AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace);
               AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace", AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV47TFServicoFluxo_ServicoTpHrq_Sels);
               AV93Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A1528ServicoFluxo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoFluxo_ServicoSigla1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoFluxo_ServicoSigla2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoFluxo_ServicoSigla3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoFluxo_Codigo, AV35TFServicoFluxo_Codigo_To, AV38TFServicoFluxo_ServicoCod, AV39TFServicoFluxo_ServicoCod_To, AV42TFServicoFluxo_ServicoSigla, AV43TFServicoFluxo_ServicoSigla_Sel, AV50TFServicoFluxo_Ordem, AV51TFServicoFluxo_Ordem_To, AV54TFServicoFluxo_ServicoPos, AV55TFServicoFluxo_ServicoPos_To, AV58TFServicoFluxo_SrvPosSigla, AV59TFServicoFluxo_SrvPosSigla_Sel, AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace, AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace, AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace, AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace, AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace, AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace, AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, AV47TFServicoFluxo_ServicoTpHrq_Sels, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1528ServicoFluxo_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PALF2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTLF2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299432057");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwservicofluxo.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOFLUXO_SERVICOSIGLA1", StringUtil.RTrim( AV17ServicoFluxo_ServicoSigla1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOFLUXO_SERVICOSIGLA2", StringUtil.RTrim( AV21ServicoFluxo_ServicoSigla2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOFLUXO_SERVICOSIGLA3", StringUtil.RTrim( AV25ServicoFluxo_ServicoSigla3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOFLUXO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34TFServicoFluxo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOFLUXO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFServicoFluxo_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOFLUXO_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFServicoFluxo_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOFLUXO_SERVICOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFServicoFluxo_ServicoCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOFLUXO_SERVICOSIGLA", StringUtil.RTrim( AV42TFServicoFluxo_ServicoSigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOFLUXO_SERVICOSIGLA_SEL", StringUtil.RTrim( AV43TFServicoFluxo_ServicoSigla_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOFLUXO_ORDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50TFServicoFluxo_Ordem), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOFLUXO_ORDEM_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFServicoFluxo_Ordem_To), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOFLUXO_SERVICOPOS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54TFServicoFluxo_ServicoPos), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOFLUXO_SERVICOPOS_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFServicoFluxo_ServicoPos_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOFLUXO_SRVPOSSIGLA", StringUtil.RTrim( AV58TFServicoFluxo_SrvPosSigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOFLUXO_SRVPOSSIGLA_SEL", StringUtil.RTrim( AV59TFServicoFluxo_SrvPosSigla_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_88", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_88), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV64GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV61DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV61DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICOFLUXO_CODIGOTITLEFILTERDATA", AV33ServicoFluxo_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICOFLUXO_CODIGOTITLEFILTERDATA", AV33ServicoFluxo_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICOFLUXO_SERVICOCODTITLEFILTERDATA", AV37ServicoFluxo_ServicoCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICOFLUXO_SERVICOCODTITLEFILTERDATA", AV37ServicoFluxo_ServicoCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICOFLUXO_SERVICOSIGLATITLEFILTERDATA", AV41ServicoFluxo_ServicoSiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICOFLUXO_SERVICOSIGLATITLEFILTERDATA", AV41ServicoFluxo_ServicoSiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICOFLUXO_SERVICOTPHRQTITLEFILTERDATA", AV45ServicoFluxo_ServicoTpHrqTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICOFLUXO_SERVICOTPHRQTITLEFILTERDATA", AV45ServicoFluxo_ServicoTpHrqTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICOFLUXO_ORDEMTITLEFILTERDATA", AV49ServicoFluxo_OrdemTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICOFLUXO_ORDEMTITLEFILTERDATA", AV49ServicoFluxo_OrdemTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICOFLUXO_SERVICOPOSTITLEFILTERDATA", AV53ServicoFluxo_ServicoPosTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICOFLUXO_SERVICOPOSTITLEFILTERDATA", AV53ServicoFluxo_ServicoPosTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICOFLUXO_SRVPOSSIGLATITLEFILTERDATA", AV57ServicoFluxo_SrvPosSiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICOFLUXO_SRVPOSSIGLATITLEFILTERDATA", AV57ServicoFluxo_SrvPosSiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFSERVICOFLUXO_SERVICOTPHRQ_SELS", AV47TFServicoFluxo_ServicoTpHrq_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFSERVICOFLUXO_SERVICOTPHRQ_SELS", AV47TFServicoFluxo_ServicoTpHrq_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV93Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Caption", StringUtil.RTrim( Ddo_servicofluxo_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_servicofluxo_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Cls", StringUtil.RTrim( Ddo_servicofluxo_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_servicofluxo_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_servicofluxo_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicofluxo_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicofluxo_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_servicofluxo_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_servicofluxo_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_servicofluxo_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_servicofluxo_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_servicofluxo_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_servicofluxo_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_servicofluxo_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_servicofluxo_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_servicofluxo_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_servicofluxo_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_servicofluxo_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_servicofluxo_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_servicofluxo_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Caption", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Tooltip", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Cls", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_servicofluxo_servicocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_servicofluxo_servicocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Sortedstatus", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Includefilter", StringUtil.BoolToStr( Ddo_servicofluxo_servicocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Filtertype", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_servicofluxo_servicocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_servicofluxo_servicocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Sortasc", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Sortdsc", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Cleanfilter", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Rangefilterto", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Caption", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Tooltip", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Cls", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_servicofluxo_servicosigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_servicofluxo_servicosigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Sortedstatus", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Includefilter", StringUtil.BoolToStr( Ddo_servicofluxo_servicosigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Filtertype", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_servicofluxo_servicosigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_servicofluxo_servicosigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Datalisttype", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Datalistproc", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servicofluxo_servicosigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Sortasc", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Sortdsc", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Loadingdata", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Cleanfilter", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Noresultsfound", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Caption", StringUtil.RTrim( Ddo_servicofluxo_servicotphrq_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Tooltip", StringUtil.RTrim( Ddo_servicofluxo_servicotphrq_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Cls", StringUtil.RTrim( Ddo_servicofluxo_servicotphrq_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Selectedvalue_set", StringUtil.RTrim( Ddo_servicofluxo_servicotphrq_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicofluxo_servicotphrq_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicofluxo_servicotphrq_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Includesortasc", StringUtil.BoolToStr( Ddo_servicofluxo_servicotphrq_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Includesortdsc", StringUtil.BoolToStr( Ddo_servicofluxo_servicotphrq_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Sortedstatus", StringUtil.RTrim( Ddo_servicofluxo_servicotphrq_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Includefilter", StringUtil.BoolToStr( Ddo_servicofluxo_servicotphrq_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Includedatalist", StringUtil.BoolToStr( Ddo_servicofluxo_servicotphrq_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Datalisttype", StringUtil.RTrim( Ddo_servicofluxo_servicotphrq_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Allowmultipleselection", StringUtil.BoolToStr( Ddo_servicofluxo_servicotphrq_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Datalistfixedvalues", StringUtil.RTrim( Ddo_servicofluxo_servicotphrq_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Sortasc", StringUtil.RTrim( Ddo_servicofluxo_servicotphrq_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Sortdsc", StringUtil.RTrim( Ddo_servicofluxo_servicotphrq_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Cleanfilter", StringUtil.RTrim( Ddo_servicofluxo_servicotphrq_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Searchbuttontext", StringUtil.RTrim( Ddo_servicofluxo_servicotphrq_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Caption", StringUtil.RTrim( Ddo_servicofluxo_ordem_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Tooltip", StringUtil.RTrim( Ddo_servicofluxo_ordem_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Cls", StringUtil.RTrim( Ddo_servicofluxo_ordem_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Filteredtext_set", StringUtil.RTrim( Ddo_servicofluxo_ordem_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Filteredtextto_set", StringUtil.RTrim( Ddo_servicofluxo_ordem_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicofluxo_ordem_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicofluxo_ordem_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Includesortasc", StringUtil.BoolToStr( Ddo_servicofluxo_ordem_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Includesortdsc", StringUtil.BoolToStr( Ddo_servicofluxo_ordem_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Sortedstatus", StringUtil.RTrim( Ddo_servicofluxo_ordem_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Includefilter", StringUtil.BoolToStr( Ddo_servicofluxo_ordem_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Filtertype", StringUtil.RTrim( Ddo_servicofluxo_ordem_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Filterisrange", StringUtil.BoolToStr( Ddo_servicofluxo_ordem_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Includedatalist", StringUtil.BoolToStr( Ddo_servicofluxo_ordem_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Sortasc", StringUtil.RTrim( Ddo_servicofluxo_ordem_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Sortdsc", StringUtil.RTrim( Ddo_servicofluxo_ordem_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Cleanfilter", StringUtil.RTrim( Ddo_servicofluxo_ordem_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Rangefilterfrom", StringUtil.RTrim( Ddo_servicofluxo_ordem_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Rangefilterto", StringUtil.RTrim( Ddo_servicofluxo_ordem_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Searchbuttontext", StringUtil.RTrim( Ddo_servicofluxo_ordem_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Caption", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Tooltip", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Cls", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Filteredtext_set", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Filteredtextto_set", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Includesortasc", StringUtil.BoolToStr( Ddo_servicofluxo_servicopos_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Includesortdsc", StringUtil.BoolToStr( Ddo_servicofluxo_servicopos_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Sortedstatus", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Includefilter", StringUtil.BoolToStr( Ddo_servicofluxo_servicopos_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Filtertype", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Filterisrange", StringUtil.BoolToStr( Ddo_servicofluxo_servicopos_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Includedatalist", StringUtil.BoolToStr( Ddo_servicofluxo_servicopos_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Sortasc", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Sortdsc", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Cleanfilter", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Rangefilterfrom", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Rangefilterto", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Searchbuttontext", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Caption", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Tooltip", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Cls", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_servicofluxo_srvpossigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_servicofluxo_srvpossigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Sortedstatus", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Includefilter", StringUtil.BoolToStr( Ddo_servicofluxo_srvpossigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Filtertype", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_servicofluxo_srvpossigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_servicofluxo_srvpossigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Datalisttype", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Datalistproc", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servicofluxo_srvpossigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Sortasc", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Sortdsc", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Loadingdata", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Cleanfilter", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Noresultsfound", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_servicofluxo_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_servicofluxo_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_servicofluxo_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Activeeventkey", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_servicofluxo_servicocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Activeeventkey", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOSIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_servicofluxo_servicosigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Activeeventkey", StringUtil.RTrim( Ddo_servicofluxo_servicotphrq_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOTPHRQ_Selectedvalue_get", StringUtil.RTrim( Ddo_servicofluxo_servicotphrq_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Activeeventkey", StringUtil.RTrim( Ddo_servicofluxo_ordem_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Filteredtext_get", StringUtil.RTrim( Ddo_servicofluxo_ordem_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_ORDEM_Filteredtextto_get", StringUtil.RTrim( Ddo_servicofluxo_ordem_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Activeeventkey", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Filteredtext_get", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SERVICOPOS_Filteredtextto_get", StringUtil.RTrim( Ddo_servicofluxo_servicopos_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Activeeventkey", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOFLUXO_SRVPOSSIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WELF2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTLF2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwservicofluxo.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWServicoFluxo" ;
      }

      public override String GetPgmdesc( )
      {
         return " Sequ�ncia" ;
      }

      protected void WBLF0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_LF2( true) ;
         }
         else
         {
            wb_table1_2_LF2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_LF2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(102, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(103, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34TFServicoFluxo_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV34TFServicoFluxo_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFServicoFluxo_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35TFServicoFluxo_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_servicocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFServicoFluxo_ServicoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV38TFServicoFluxo_ServicoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_servicocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_servicocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_servicocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFServicoFluxo_ServicoCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV39TFServicoFluxo_ServicoCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_servicocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_servicocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_servicosigla_Internalname, StringUtil.RTrim( AV42TFServicoFluxo_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( AV42TFServicoFluxo_ServicoSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_servicosigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_servicosigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_servicosigla_sel_Internalname, StringUtil.RTrim( AV43TFServicoFluxo_ServicoSigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV43TFServicoFluxo_ServicoSigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_servicosigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_servicosigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50TFServicoFluxo_Ordem), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV50TFServicoFluxo_Ordem), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_ordem_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_ordem_Visible, 1, 0, "text", "", 50, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_ordem_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFServicoFluxo_Ordem_To), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV51TFServicoFluxo_Ordem_To), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_ordem_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_ordem_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_servicopos_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54TFServicoFluxo_ServicoPos), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV54TFServicoFluxo_ServicoPos), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_servicopos_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_servicopos_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_servicopos_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFServicoFluxo_ServicoPos_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV55TFServicoFluxo_ServicoPos_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_servicopos_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_servicopos_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_srvpossigla_Internalname, StringUtil.RTrim( AV58TFServicoFluxo_SrvPosSigla), StringUtil.RTrim( context.localUtil.Format( AV58TFServicoFluxo_SrvPosSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_srvpossigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_srvpossigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_srvpossigla_sel_Internalname, StringUtil.RTrim( AV59TFServicoFluxo_SrvPosSigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV59TFServicoFluxo_SrvPosSigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_srvpossigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_srvpossigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoFluxo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICOFLUXO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicofluxo_codigotitlecontrolidtoreplace_Internalname, AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"", 0, edtavDdo_servicofluxo_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWServicoFluxo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICOFLUXO_SERVICOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicofluxo_servicocodtitlecontrolidtoreplace_Internalname, AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", 0, edtavDdo_servicofluxo_servicocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWServicoFluxo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICOFLUXO_SERVICOSIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicofluxo_servicosiglatitlecontrolidtoreplace_Internalname, AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", 0, edtavDdo_servicofluxo_servicosiglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWServicoFluxo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICOFLUXO_SERVICOTPHRQContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicofluxo_servicotphrqtitlecontrolidtoreplace_Internalname, AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", 0, edtavDdo_servicofluxo_servicotphrqtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWServicoFluxo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICOFLUXO_ORDEMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Internalname, AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", 0, edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWServicoFluxo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICOFLUXO_SERVICOPOSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicofluxo_servicopostitlecontrolidtoreplace_Internalname, AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", 0, edtavDdo_servicofluxo_servicopostitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWServicoFluxo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICOFLUXO_SRVPOSSIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Internalname, AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"", 0, edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWServicoFluxo.htm");
         }
         wbLoad = true;
      }

      protected void STARTLF2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Sequ�ncia", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPLF0( ) ;
      }

      protected void WSLF2( )
      {
         STARTLF2( ) ;
         EVTLF2( ) ;
      }

      protected void EVTLF2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11LF2 */
                              E11LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOFLUXO_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12LF2 */
                              E12LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOFLUXO_SERVICOCOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13LF2 */
                              E13LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOFLUXO_SERVICOSIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14LF2 */
                              E14LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOFLUXO_SERVICOTPHRQ.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15LF2 */
                              E15LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOFLUXO_ORDEM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16LF2 */
                              E16LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOFLUXO_SERVICOPOS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17LF2 */
                              E17LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOFLUXO_SRVPOSSIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18LF2 */
                              E18LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19LF2 */
                              E19LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20LF2 */
                              E20LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21LF2 */
                              E21LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22LF2 */
                              E22LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23LF2 */
                              E23LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24LF2 */
                              E24LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25LF2 */
                              E25LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26LF2 */
                              E26LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27LF2 */
                              E27LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E28LF2 */
                              E28LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E29LF2 */
                              E29LF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_88_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
                              SubsflControlProps_882( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV91Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV92Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A1528ServicoFluxo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoFluxo_Codigo_Internalname), ",", "."));
                              A1522ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( edtServicoFluxo_ServicoCod_Internalname), ",", "."));
                              A1523ServicoFluxo_ServicoSigla = StringUtil.Upper( cgiGet( edtServicoFluxo_ServicoSigla_Internalname));
                              n1523ServicoFluxo_ServicoSigla = false;
                              cmbServicoFluxo_ServicoTpHrq.Name = cmbServicoFluxo_ServicoTpHrq_Internalname;
                              cmbServicoFluxo_ServicoTpHrq.CurrentValue = cgiGet( cmbServicoFluxo_ServicoTpHrq_Internalname);
                              A1533ServicoFluxo_ServicoTpHrq = (short)(NumberUtil.Val( cgiGet( cmbServicoFluxo_ServicoTpHrq_Internalname), "."));
                              n1533ServicoFluxo_ServicoTpHrq = false;
                              A1532ServicoFluxo_Ordem = (short)(context.localUtil.CToN( cgiGet( edtServicoFluxo_Ordem_Internalname), ",", "."));
                              n1532ServicoFluxo_Ordem = false;
                              A1526ServicoFluxo_ServicoPos = (int)(context.localUtil.CToN( cgiGet( edtServicoFluxo_ServicoPos_Internalname), ",", "."));
                              n1526ServicoFluxo_ServicoPos = false;
                              A1527ServicoFluxo_SrvPosSigla = StringUtil.Upper( cgiGet( edtServicoFluxo_SrvPosSigla_Internalname));
                              n1527ServicoFluxo_SrvPosSigla = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E30LF2 */
                                    E30LF2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E31LF2 */
                                    E31LF2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E32LF2 */
                                    E32LF2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servicofluxo_servicosigla1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOFLUXO_SERVICOSIGLA1"), AV17ServicoFluxo_ServicoSigla1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servicofluxo_servicosigla2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOFLUXO_SERVICOSIGLA2"), AV21ServicoFluxo_ServicoSigla2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servicofluxo_servicosigla3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOFLUXO_SERVICOSIGLA3"), AV25ServicoFluxo_ServicoSigla3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicofluxo_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOFLUXO_CODIGO"), ",", ".") != Convert.ToDecimal( AV34TFServicoFluxo_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicofluxo_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOFLUXO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV35TFServicoFluxo_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicofluxo_servicocod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOFLUXO_SERVICOCOD"), ",", ".") != Convert.ToDecimal( AV38TFServicoFluxo_ServicoCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicofluxo_servicocod_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOFLUXO_SERVICOCOD_TO"), ",", ".") != Convert.ToDecimal( AV39TFServicoFluxo_ServicoCod_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicofluxo_servicosigla Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOFLUXO_SERVICOSIGLA"), AV42TFServicoFluxo_ServicoSigla) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicofluxo_servicosigla_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOFLUXO_SERVICOSIGLA_SEL"), AV43TFServicoFluxo_ServicoSigla_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicofluxo_ordem Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOFLUXO_ORDEM"), ",", ".") != Convert.ToDecimal( AV50TFServicoFluxo_Ordem )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicofluxo_ordem_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOFLUXO_ORDEM_TO"), ",", ".") != Convert.ToDecimal( AV51TFServicoFluxo_Ordem_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicofluxo_servicopos Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOFLUXO_SERVICOPOS"), ",", ".") != Convert.ToDecimal( AV54TFServicoFluxo_ServicoPos )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicofluxo_servicopos_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOFLUXO_SERVICOPOS_TO"), ",", ".") != Convert.ToDecimal( AV55TFServicoFluxo_ServicoPos_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicofluxo_srvpossigla Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOFLUXO_SRVPOSSIGLA"), AV58TFServicoFluxo_SrvPosSigla) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicofluxo_srvpossigla_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOFLUXO_SRVPOSSIGLA_SEL"), AV59TFServicoFluxo_SrvPosSigla_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WELF2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PALF2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SERVICOFLUXO_SERVICOSIGLA", "Servi�o", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("SERVICOFLUXO_SERVICOSIGLA", "Servi�o", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("SERVICOFLUXO_SERVICOSIGLA", "Servi�o", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "SERVICOFLUXO_SERVICOTPHRQ_" + sGXsfl_88_idx;
            cmbServicoFluxo_ServicoTpHrq.Name = GXCCtl;
            cmbServicoFluxo_ServicoTpHrq.WebTags = "";
            cmbServicoFluxo_ServicoTpHrq.addItem("1", "Prim�rio", 0);
            cmbServicoFluxo_ServicoTpHrq.addItem("2", "Secund�rio", 0);
            if ( cmbServicoFluxo_ServicoTpHrq.ItemCount > 0 )
            {
               A1533ServicoFluxo_ServicoTpHrq = (short)(NumberUtil.Val( cmbServicoFluxo_ServicoTpHrq.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1533ServicoFluxo_ServicoTpHrq), 4, 0))), "."));
               n1533ServicoFluxo_ServicoTpHrq = false;
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_882( ) ;
         while ( nGXsfl_88_idx <= nRC_GXsfl_88 )
         {
            sendrow_882( ) ;
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17ServicoFluxo_ServicoSigla1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21ServicoFluxo_ServicoSigla2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25ServicoFluxo_ServicoSigla3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV34TFServicoFluxo_Codigo ,
                                       int AV35TFServicoFluxo_Codigo_To ,
                                       int AV38TFServicoFluxo_ServicoCod ,
                                       int AV39TFServicoFluxo_ServicoCod_To ,
                                       String AV42TFServicoFluxo_ServicoSigla ,
                                       String AV43TFServicoFluxo_ServicoSigla_Sel ,
                                       short AV50TFServicoFluxo_Ordem ,
                                       short AV51TFServicoFluxo_Ordem_To ,
                                       int AV54TFServicoFluxo_ServicoPos ,
                                       int AV55TFServicoFluxo_ServicoPos_To ,
                                       String AV58TFServicoFluxo_SrvPosSigla ,
                                       String AV59TFServicoFluxo_SrvPosSigla_Sel ,
                                       String AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace ,
                                       String AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace ,
                                       String AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace ,
                                       String AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace ,
                                       String AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace ,
                                       String AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace ,
                                       String AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace ,
                                       IGxCollection AV47TFServicoFluxo_ServicoTpHrq_Sels ,
                                       String AV93Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A1528ServicoFluxo_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFLF2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICOFLUXO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1528ServicoFluxo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SERVICOFLUXO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1528ServicoFluxo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICOFLUXO_SERVICOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1522ServicoFluxo_ServicoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SERVICOFLUXO_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICOFLUXO_ORDEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1532ServicoFluxo_Ordem), "ZZ9")));
         GxWebStd.gx_hidden_field( context, "SERVICOFLUXO_ORDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1532ServicoFluxo_Ordem), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICOFLUXO_SERVICOPOS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SERVICOFLUXO_SERVICOPOS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFLF2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV93Pgmname = "WWServicoFluxo";
         context.Gx_err = 0;
      }

      protected void RFLF2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 88;
         /* Execute user event: E31LF2 */
         E31LF2 ();
         nGXsfl_88_idx = 1;
         sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
         SubsflControlProps_882( ) ;
         nGXsfl_88_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_882( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A1533ServicoFluxo_ServicoTpHrq ,
                                                 AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels ,
                                                 AV67WWServicoFluxoDS_1_Dynamicfiltersselector1 ,
                                                 AV68WWServicoFluxoDS_2_Dynamicfiltersoperator1 ,
                                                 AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 ,
                                                 AV70WWServicoFluxoDS_4_Dynamicfiltersenabled2 ,
                                                 AV71WWServicoFluxoDS_5_Dynamicfiltersselector2 ,
                                                 AV72WWServicoFluxoDS_6_Dynamicfiltersoperator2 ,
                                                 AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 ,
                                                 AV74WWServicoFluxoDS_8_Dynamicfiltersenabled3 ,
                                                 AV75WWServicoFluxoDS_9_Dynamicfiltersselector3 ,
                                                 AV76WWServicoFluxoDS_10_Dynamicfiltersoperator3 ,
                                                 AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 ,
                                                 AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo ,
                                                 AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to ,
                                                 AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod ,
                                                 AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to ,
                                                 AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel ,
                                                 AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla ,
                                                 AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels.Count ,
                                                 AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem ,
                                                 AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to ,
                                                 AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos ,
                                                 AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to ,
                                                 AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel ,
                                                 AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla ,
                                                 A1523ServicoFluxo_ServicoSigla ,
                                                 A1528ServicoFluxo_Codigo ,
                                                 A1522ServicoFluxo_ServicoCod ,
                                                 A1532ServicoFluxo_Ordem ,
                                                 A1526ServicoFluxo_ServicoPos ,
                                                 A1527ServicoFluxo_SrvPosSigla ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 = StringUtil.PadR( StringUtil.RTrim( AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1), 15, "%");
            lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 = StringUtil.PadR( StringUtil.RTrim( AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1), 15, "%");
            lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 = StringUtil.PadR( StringUtil.RTrim( AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2), 15, "%");
            lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 = StringUtil.PadR( StringUtil.RTrim( AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2), 15, "%");
            lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 = StringUtil.PadR( StringUtil.RTrim( AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3), 15, "%");
            lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 = StringUtil.PadR( StringUtil.RTrim( AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3), 15, "%");
            lV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla), 15, "%");
            lV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla = StringUtil.PadR( StringUtil.RTrim( AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla), 15, "%");
            /* Using cursor H00LF2 */
            pr_default.execute(0, new Object[] {lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1, lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1, lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2, lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2, lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3, lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3, AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo, AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to, AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod, AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to, lV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla, AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel, AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem, AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to, AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos, AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to, lV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla, AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_88_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1527ServicoFluxo_SrvPosSigla = H00LF2_A1527ServicoFluxo_SrvPosSigla[0];
               n1527ServicoFluxo_SrvPosSigla = H00LF2_n1527ServicoFluxo_SrvPosSigla[0];
               A1526ServicoFluxo_ServicoPos = H00LF2_A1526ServicoFluxo_ServicoPos[0];
               n1526ServicoFluxo_ServicoPos = H00LF2_n1526ServicoFluxo_ServicoPos[0];
               A1532ServicoFluxo_Ordem = H00LF2_A1532ServicoFluxo_Ordem[0];
               n1532ServicoFluxo_Ordem = H00LF2_n1532ServicoFluxo_Ordem[0];
               A1533ServicoFluxo_ServicoTpHrq = H00LF2_A1533ServicoFluxo_ServicoTpHrq[0];
               n1533ServicoFluxo_ServicoTpHrq = H00LF2_n1533ServicoFluxo_ServicoTpHrq[0];
               A1523ServicoFluxo_ServicoSigla = H00LF2_A1523ServicoFluxo_ServicoSigla[0];
               n1523ServicoFluxo_ServicoSigla = H00LF2_n1523ServicoFluxo_ServicoSigla[0];
               A1522ServicoFluxo_ServicoCod = H00LF2_A1522ServicoFluxo_ServicoCod[0];
               A1528ServicoFluxo_Codigo = H00LF2_A1528ServicoFluxo_Codigo[0];
               A1527ServicoFluxo_SrvPosSigla = H00LF2_A1527ServicoFluxo_SrvPosSigla[0];
               n1527ServicoFluxo_SrvPosSigla = H00LF2_n1527ServicoFluxo_SrvPosSigla[0];
               A1533ServicoFluxo_ServicoTpHrq = H00LF2_A1533ServicoFluxo_ServicoTpHrq[0];
               n1533ServicoFluxo_ServicoTpHrq = H00LF2_n1533ServicoFluxo_ServicoTpHrq[0];
               A1523ServicoFluxo_ServicoSigla = H00LF2_A1523ServicoFluxo_ServicoSigla[0];
               n1523ServicoFluxo_ServicoSigla = H00LF2_n1523ServicoFluxo_ServicoSigla[0];
               /* Execute user event: E32LF2 */
               E32LF2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 88;
            WBLF0( ) ;
         }
         nGXsfl_88_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV67WWServicoFluxoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWServicoFluxoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 = AV17ServicoFluxo_ServicoSigla1;
         AV70WWServicoFluxoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV71WWServicoFluxoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV72WWServicoFluxoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 = AV21ServicoFluxo_ServicoSigla2;
         AV74WWServicoFluxoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV75WWServicoFluxoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV76WWServicoFluxoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 = AV25ServicoFluxo_ServicoSigla3;
         AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo = AV34TFServicoFluxo_Codigo;
         AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to = AV35TFServicoFluxo_Codigo_To;
         AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod = AV38TFServicoFluxo_ServicoCod;
         AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to = AV39TFServicoFluxo_ServicoCod_To;
         AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla = AV42TFServicoFluxo_ServicoSigla;
         AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel = AV43TFServicoFluxo_ServicoSigla_Sel;
         AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels = AV47TFServicoFluxo_ServicoTpHrq_Sels;
         AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem = AV50TFServicoFluxo_Ordem;
         AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to = AV51TFServicoFluxo_Ordem_To;
         AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos = AV54TFServicoFluxo_ServicoPos;
         AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to = AV55TFServicoFluxo_ServicoPos_To;
         AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla = AV58TFServicoFluxo_SrvPosSigla;
         AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel = AV59TFServicoFluxo_SrvPosSigla_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1533ServicoFluxo_ServicoTpHrq ,
                                              AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels ,
                                              AV67WWServicoFluxoDS_1_Dynamicfiltersselector1 ,
                                              AV68WWServicoFluxoDS_2_Dynamicfiltersoperator1 ,
                                              AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 ,
                                              AV70WWServicoFluxoDS_4_Dynamicfiltersenabled2 ,
                                              AV71WWServicoFluxoDS_5_Dynamicfiltersselector2 ,
                                              AV72WWServicoFluxoDS_6_Dynamicfiltersoperator2 ,
                                              AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 ,
                                              AV74WWServicoFluxoDS_8_Dynamicfiltersenabled3 ,
                                              AV75WWServicoFluxoDS_9_Dynamicfiltersselector3 ,
                                              AV76WWServicoFluxoDS_10_Dynamicfiltersoperator3 ,
                                              AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 ,
                                              AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo ,
                                              AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to ,
                                              AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod ,
                                              AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to ,
                                              AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel ,
                                              AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla ,
                                              AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels.Count ,
                                              AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem ,
                                              AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to ,
                                              AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos ,
                                              AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to ,
                                              AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel ,
                                              AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla ,
                                              A1523ServicoFluxo_ServicoSigla ,
                                              A1528ServicoFluxo_Codigo ,
                                              A1522ServicoFluxo_ServicoCod ,
                                              A1532ServicoFluxo_Ordem ,
                                              A1526ServicoFluxo_ServicoPos ,
                                              A1527ServicoFluxo_SrvPosSigla ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 = StringUtil.PadR( StringUtil.RTrim( AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1), 15, "%");
         lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 = StringUtil.PadR( StringUtil.RTrim( AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1), 15, "%");
         lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 = StringUtil.PadR( StringUtil.RTrim( AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2), 15, "%");
         lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 = StringUtil.PadR( StringUtil.RTrim( AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2), 15, "%");
         lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 = StringUtil.PadR( StringUtil.RTrim( AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3), 15, "%");
         lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 = StringUtil.PadR( StringUtil.RTrim( AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3), 15, "%");
         lV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla), 15, "%");
         lV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla = StringUtil.PadR( StringUtil.RTrim( AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla), 15, "%");
         /* Using cursor H00LF3 */
         pr_default.execute(1, new Object[] {lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1, lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1, lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2, lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2, lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3, lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3, AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo, AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to, AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod, AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to, lV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla, AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel, AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem, AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to, AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos, AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to, lV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla, AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel});
         GRID_nRecordCount = H00LF3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV67WWServicoFluxoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWServicoFluxoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 = AV17ServicoFluxo_ServicoSigla1;
         AV70WWServicoFluxoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV71WWServicoFluxoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV72WWServicoFluxoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 = AV21ServicoFluxo_ServicoSigla2;
         AV74WWServicoFluxoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV75WWServicoFluxoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV76WWServicoFluxoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 = AV25ServicoFluxo_ServicoSigla3;
         AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo = AV34TFServicoFluxo_Codigo;
         AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to = AV35TFServicoFluxo_Codigo_To;
         AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod = AV38TFServicoFluxo_ServicoCod;
         AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to = AV39TFServicoFluxo_ServicoCod_To;
         AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla = AV42TFServicoFluxo_ServicoSigla;
         AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel = AV43TFServicoFluxo_ServicoSigla_Sel;
         AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels = AV47TFServicoFluxo_ServicoTpHrq_Sels;
         AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem = AV50TFServicoFluxo_Ordem;
         AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to = AV51TFServicoFluxo_Ordem_To;
         AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos = AV54TFServicoFluxo_ServicoPos;
         AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to = AV55TFServicoFluxo_ServicoPos_To;
         AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla = AV58TFServicoFluxo_SrvPosSigla;
         AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel = AV59TFServicoFluxo_SrvPosSigla_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoFluxo_ServicoSigla1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoFluxo_ServicoSigla2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoFluxo_ServicoSigla3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoFluxo_Codigo, AV35TFServicoFluxo_Codigo_To, AV38TFServicoFluxo_ServicoCod, AV39TFServicoFluxo_ServicoCod_To, AV42TFServicoFluxo_ServicoSigla, AV43TFServicoFluxo_ServicoSigla_Sel, AV50TFServicoFluxo_Ordem, AV51TFServicoFluxo_Ordem_To, AV54TFServicoFluxo_ServicoPos, AV55TFServicoFluxo_ServicoPos_To, AV58TFServicoFluxo_SrvPosSigla, AV59TFServicoFluxo_SrvPosSigla_Sel, AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace, AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace, AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace, AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace, AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace, AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace, AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, AV47TFServicoFluxo_ServicoTpHrq_Sels, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1528ServicoFluxo_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV67WWServicoFluxoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWServicoFluxoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 = AV17ServicoFluxo_ServicoSigla1;
         AV70WWServicoFluxoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV71WWServicoFluxoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV72WWServicoFluxoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 = AV21ServicoFluxo_ServicoSigla2;
         AV74WWServicoFluxoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV75WWServicoFluxoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV76WWServicoFluxoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 = AV25ServicoFluxo_ServicoSigla3;
         AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo = AV34TFServicoFluxo_Codigo;
         AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to = AV35TFServicoFluxo_Codigo_To;
         AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod = AV38TFServicoFluxo_ServicoCod;
         AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to = AV39TFServicoFluxo_ServicoCod_To;
         AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla = AV42TFServicoFluxo_ServicoSigla;
         AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel = AV43TFServicoFluxo_ServicoSigla_Sel;
         AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels = AV47TFServicoFluxo_ServicoTpHrq_Sels;
         AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem = AV50TFServicoFluxo_Ordem;
         AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to = AV51TFServicoFluxo_Ordem_To;
         AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos = AV54TFServicoFluxo_ServicoPos;
         AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to = AV55TFServicoFluxo_ServicoPos_To;
         AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla = AV58TFServicoFluxo_SrvPosSigla;
         AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel = AV59TFServicoFluxo_SrvPosSigla_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoFluxo_ServicoSigla1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoFluxo_ServicoSigla2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoFluxo_ServicoSigla3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoFluxo_Codigo, AV35TFServicoFluxo_Codigo_To, AV38TFServicoFluxo_ServicoCod, AV39TFServicoFluxo_ServicoCod_To, AV42TFServicoFluxo_ServicoSigla, AV43TFServicoFluxo_ServicoSigla_Sel, AV50TFServicoFluxo_Ordem, AV51TFServicoFluxo_Ordem_To, AV54TFServicoFluxo_ServicoPos, AV55TFServicoFluxo_ServicoPos_To, AV58TFServicoFluxo_SrvPosSigla, AV59TFServicoFluxo_SrvPosSigla_Sel, AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace, AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace, AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace, AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace, AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace, AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace, AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, AV47TFServicoFluxo_ServicoTpHrq_Sels, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1528ServicoFluxo_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV67WWServicoFluxoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWServicoFluxoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 = AV17ServicoFluxo_ServicoSigla1;
         AV70WWServicoFluxoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV71WWServicoFluxoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV72WWServicoFluxoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 = AV21ServicoFluxo_ServicoSigla2;
         AV74WWServicoFluxoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV75WWServicoFluxoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV76WWServicoFluxoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 = AV25ServicoFluxo_ServicoSigla3;
         AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo = AV34TFServicoFluxo_Codigo;
         AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to = AV35TFServicoFluxo_Codigo_To;
         AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod = AV38TFServicoFluxo_ServicoCod;
         AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to = AV39TFServicoFluxo_ServicoCod_To;
         AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla = AV42TFServicoFluxo_ServicoSigla;
         AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel = AV43TFServicoFluxo_ServicoSigla_Sel;
         AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels = AV47TFServicoFluxo_ServicoTpHrq_Sels;
         AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem = AV50TFServicoFluxo_Ordem;
         AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to = AV51TFServicoFluxo_Ordem_To;
         AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos = AV54TFServicoFluxo_ServicoPos;
         AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to = AV55TFServicoFluxo_ServicoPos_To;
         AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla = AV58TFServicoFluxo_SrvPosSigla;
         AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel = AV59TFServicoFluxo_SrvPosSigla_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoFluxo_ServicoSigla1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoFluxo_ServicoSigla2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoFluxo_ServicoSigla3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoFluxo_Codigo, AV35TFServicoFluxo_Codigo_To, AV38TFServicoFluxo_ServicoCod, AV39TFServicoFluxo_ServicoCod_To, AV42TFServicoFluxo_ServicoSigla, AV43TFServicoFluxo_ServicoSigla_Sel, AV50TFServicoFluxo_Ordem, AV51TFServicoFluxo_Ordem_To, AV54TFServicoFluxo_ServicoPos, AV55TFServicoFluxo_ServicoPos_To, AV58TFServicoFluxo_SrvPosSigla, AV59TFServicoFluxo_SrvPosSigla_Sel, AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace, AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace, AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace, AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace, AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace, AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace, AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, AV47TFServicoFluxo_ServicoTpHrq_Sels, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1528ServicoFluxo_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV67WWServicoFluxoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWServicoFluxoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 = AV17ServicoFluxo_ServicoSigla1;
         AV70WWServicoFluxoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV71WWServicoFluxoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV72WWServicoFluxoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 = AV21ServicoFluxo_ServicoSigla2;
         AV74WWServicoFluxoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV75WWServicoFluxoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV76WWServicoFluxoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 = AV25ServicoFluxo_ServicoSigla3;
         AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo = AV34TFServicoFluxo_Codigo;
         AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to = AV35TFServicoFluxo_Codigo_To;
         AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod = AV38TFServicoFluxo_ServicoCod;
         AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to = AV39TFServicoFluxo_ServicoCod_To;
         AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla = AV42TFServicoFluxo_ServicoSigla;
         AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel = AV43TFServicoFluxo_ServicoSigla_Sel;
         AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels = AV47TFServicoFluxo_ServicoTpHrq_Sels;
         AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem = AV50TFServicoFluxo_Ordem;
         AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to = AV51TFServicoFluxo_Ordem_To;
         AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos = AV54TFServicoFluxo_ServicoPos;
         AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to = AV55TFServicoFluxo_ServicoPos_To;
         AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla = AV58TFServicoFluxo_SrvPosSigla;
         AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel = AV59TFServicoFluxo_SrvPosSigla_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoFluxo_ServicoSigla1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoFluxo_ServicoSigla2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoFluxo_ServicoSigla3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoFluxo_Codigo, AV35TFServicoFluxo_Codigo_To, AV38TFServicoFluxo_ServicoCod, AV39TFServicoFluxo_ServicoCod_To, AV42TFServicoFluxo_ServicoSigla, AV43TFServicoFluxo_ServicoSigla_Sel, AV50TFServicoFluxo_Ordem, AV51TFServicoFluxo_Ordem_To, AV54TFServicoFluxo_ServicoPos, AV55TFServicoFluxo_ServicoPos_To, AV58TFServicoFluxo_SrvPosSigla, AV59TFServicoFluxo_SrvPosSigla_Sel, AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace, AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace, AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace, AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace, AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace, AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace, AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, AV47TFServicoFluxo_ServicoTpHrq_Sels, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1528ServicoFluxo_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV67WWServicoFluxoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWServicoFluxoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 = AV17ServicoFluxo_ServicoSigla1;
         AV70WWServicoFluxoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV71WWServicoFluxoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV72WWServicoFluxoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 = AV21ServicoFluxo_ServicoSigla2;
         AV74WWServicoFluxoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV75WWServicoFluxoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV76WWServicoFluxoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 = AV25ServicoFluxo_ServicoSigla3;
         AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo = AV34TFServicoFluxo_Codigo;
         AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to = AV35TFServicoFluxo_Codigo_To;
         AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod = AV38TFServicoFluxo_ServicoCod;
         AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to = AV39TFServicoFluxo_ServicoCod_To;
         AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla = AV42TFServicoFluxo_ServicoSigla;
         AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel = AV43TFServicoFluxo_ServicoSigla_Sel;
         AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels = AV47TFServicoFluxo_ServicoTpHrq_Sels;
         AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem = AV50TFServicoFluxo_Ordem;
         AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to = AV51TFServicoFluxo_Ordem_To;
         AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos = AV54TFServicoFluxo_ServicoPos;
         AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to = AV55TFServicoFluxo_ServicoPos_To;
         AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla = AV58TFServicoFluxo_SrvPosSigla;
         AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel = AV59TFServicoFluxo_SrvPosSigla_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoFluxo_ServicoSigla1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoFluxo_ServicoSigla2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoFluxo_ServicoSigla3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoFluxo_Codigo, AV35TFServicoFluxo_Codigo_To, AV38TFServicoFluxo_ServicoCod, AV39TFServicoFluxo_ServicoCod_To, AV42TFServicoFluxo_ServicoSigla, AV43TFServicoFluxo_ServicoSigla_Sel, AV50TFServicoFluxo_Ordem, AV51TFServicoFluxo_Ordem_To, AV54TFServicoFluxo_ServicoPos, AV55TFServicoFluxo_ServicoPos_To, AV58TFServicoFluxo_SrvPosSigla, AV59TFServicoFluxo_SrvPosSigla_Sel, AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace, AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace, AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace, AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace, AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace, AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace, AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, AV47TFServicoFluxo_ServicoTpHrq_Sels, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1528ServicoFluxo_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPLF0( )
      {
         /* Before Start, stand alone formulas. */
         AV93Pgmname = "WWServicoFluxo";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E30LF2 */
         E30LF2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV61DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICOFLUXO_CODIGOTITLEFILTERDATA"), AV33ServicoFluxo_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICOFLUXO_SERVICOCODTITLEFILTERDATA"), AV37ServicoFluxo_ServicoCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICOFLUXO_SERVICOSIGLATITLEFILTERDATA"), AV41ServicoFluxo_ServicoSiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICOFLUXO_SERVICOTPHRQTITLEFILTERDATA"), AV45ServicoFluxo_ServicoTpHrqTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICOFLUXO_ORDEMTITLEFILTERDATA"), AV49ServicoFluxo_OrdemTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICOFLUXO_SERVICOPOSTITLEFILTERDATA"), AV53ServicoFluxo_ServicoPosTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICOFLUXO_SRVPOSSIGLATITLEFILTERDATA"), AV57ServicoFluxo_SrvPosSiglaTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17ServicoFluxo_ServicoSigla1 = StringUtil.Upper( cgiGet( edtavServicofluxo_servicosigla1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoFluxo_ServicoSigla1", AV17ServicoFluxo_ServicoSigla1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21ServicoFluxo_ServicoSigla2 = StringUtil.Upper( cgiGet( edtavServicofluxo_servicosigla2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ServicoFluxo_ServicoSigla2", AV21ServicoFluxo_ServicoSigla2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25ServicoFluxo_ServicoSigla3 = StringUtil.Upper( cgiGet( edtavServicofluxo_servicosigla3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ServicoFluxo_ServicoSigla3", AV25ServicoFluxo_ServicoSigla3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOFLUXO_CODIGO");
               GX_FocusControl = edtavTfservicofluxo_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34TFServicoFluxo_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFServicoFluxo_Codigo), 6, 0)));
            }
            else
            {
               AV34TFServicoFluxo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfservicofluxo_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFServicoFluxo_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOFLUXO_CODIGO_TO");
               GX_FocusControl = edtavTfservicofluxo_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFServicoFluxo_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFServicoFluxo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFServicoFluxo_Codigo_To), 6, 0)));
            }
            else
            {
               AV35TFServicoFluxo_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfservicofluxo_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFServicoFluxo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFServicoFluxo_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_servicocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_servicocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOFLUXO_SERVICOCOD");
               GX_FocusControl = edtavTfservicofluxo_servicocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38TFServicoFluxo_ServicoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFServicoFluxo_ServicoCod), 6, 0)));
            }
            else
            {
               AV38TFServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( edtavTfservicofluxo_servicocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFServicoFluxo_ServicoCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_servicocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_servicocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOFLUXO_SERVICOCOD_TO");
               GX_FocusControl = edtavTfservicofluxo_servicocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFServicoFluxo_ServicoCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFServicoFluxo_ServicoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFServicoFluxo_ServicoCod_To), 6, 0)));
            }
            else
            {
               AV39TFServicoFluxo_ServicoCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfservicofluxo_servicocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFServicoFluxo_ServicoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFServicoFluxo_ServicoCod_To), 6, 0)));
            }
            AV42TFServicoFluxo_ServicoSigla = StringUtil.Upper( cgiGet( edtavTfservicofluxo_servicosigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFServicoFluxo_ServicoSigla", AV42TFServicoFluxo_ServicoSigla);
            AV43TFServicoFluxo_ServicoSigla_Sel = StringUtil.Upper( cgiGet( edtavTfservicofluxo_servicosigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFServicoFluxo_ServicoSigla_Sel", AV43TFServicoFluxo_ServicoSigla_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_ordem_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_ordem_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOFLUXO_ORDEM");
               GX_FocusControl = edtavTfservicofluxo_ordem_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50TFServicoFluxo_Ordem = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFServicoFluxo_Ordem), 3, 0)));
            }
            else
            {
               AV50TFServicoFluxo_Ordem = (short)(context.localUtil.CToN( cgiGet( edtavTfservicofluxo_ordem_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFServicoFluxo_Ordem), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_ordem_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_ordem_to_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOFLUXO_ORDEM_TO");
               GX_FocusControl = edtavTfservicofluxo_ordem_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51TFServicoFluxo_Ordem_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFServicoFluxo_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFServicoFluxo_Ordem_To), 3, 0)));
            }
            else
            {
               AV51TFServicoFluxo_Ordem_To = (short)(context.localUtil.CToN( cgiGet( edtavTfservicofluxo_ordem_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFServicoFluxo_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFServicoFluxo_Ordem_To), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_servicopos_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_servicopos_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOFLUXO_SERVICOPOS");
               GX_FocusControl = edtavTfservicofluxo_servicopos_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54TFServicoFluxo_ServicoPos = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFServicoFluxo_ServicoPos), 6, 0)));
            }
            else
            {
               AV54TFServicoFluxo_ServicoPos = (int)(context.localUtil.CToN( cgiGet( edtavTfservicofluxo_servicopos_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFServicoFluxo_ServicoPos), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_servicopos_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_servicopos_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOFLUXO_SERVICOPOS_TO");
               GX_FocusControl = edtavTfservicofluxo_servicopos_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFServicoFluxo_ServicoPos_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFServicoFluxo_ServicoPos_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFServicoFluxo_ServicoPos_To), 6, 0)));
            }
            else
            {
               AV55TFServicoFluxo_ServicoPos_To = (int)(context.localUtil.CToN( cgiGet( edtavTfservicofluxo_servicopos_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFServicoFluxo_ServicoPos_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFServicoFluxo_ServicoPos_To), 6, 0)));
            }
            AV58TFServicoFluxo_SrvPosSigla = StringUtil.Upper( cgiGet( edtavTfservicofluxo_srvpossigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFServicoFluxo_SrvPosSigla", AV58TFServicoFluxo_SrvPosSigla);
            AV59TFServicoFluxo_SrvPosSigla_Sel = StringUtil.Upper( cgiGet( edtavTfservicofluxo_srvpossigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFServicoFluxo_SrvPosSigla_Sel", AV59TFServicoFluxo_SrvPosSigla_Sel);
            AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_servicofluxo_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace", AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace);
            AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace = cgiGet( edtavDdo_servicofluxo_servicocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace", AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace);
            AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace = cgiGet( edtavDdo_servicofluxo_servicosiglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace", AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace);
            AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace = cgiGet( edtavDdo_servicofluxo_servicotphrqtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace", AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace);
            AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace = cgiGet( edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace", AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace);
            AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace = cgiGet( edtavDdo_servicofluxo_servicopostitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace", AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace);
            AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace = cgiGet( edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace", AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_88 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_88"), ",", "."));
            AV63GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV64GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_servicofluxo_codigo_Caption = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Caption");
            Ddo_servicofluxo_codigo_Tooltip = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Tooltip");
            Ddo_servicofluxo_codigo_Cls = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Cls");
            Ddo_servicofluxo_codigo_Filteredtext_set = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Filteredtext_set");
            Ddo_servicofluxo_codigo_Filteredtextto_set = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Filteredtextto_set");
            Ddo_servicofluxo_codigo_Dropdownoptionstype = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Dropdownoptionstype");
            Ddo_servicofluxo_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Titlecontrolidtoreplace");
            Ddo_servicofluxo_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_CODIGO_Includesortasc"));
            Ddo_servicofluxo_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_CODIGO_Includesortdsc"));
            Ddo_servicofluxo_codigo_Sortedstatus = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Sortedstatus");
            Ddo_servicofluxo_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_CODIGO_Includefilter"));
            Ddo_servicofluxo_codigo_Filtertype = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Filtertype");
            Ddo_servicofluxo_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_CODIGO_Filterisrange"));
            Ddo_servicofluxo_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_CODIGO_Includedatalist"));
            Ddo_servicofluxo_codigo_Sortasc = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Sortasc");
            Ddo_servicofluxo_codigo_Sortdsc = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Sortdsc");
            Ddo_servicofluxo_codigo_Cleanfilter = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Cleanfilter");
            Ddo_servicofluxo_codigo_Rangefilterfrom = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Rangefilterfrom");
            Ddo_servicofluxo_codigo_Rangefilterto = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Rangefilterto");
            Ddo_servicofluxo_codigo_Searchbuttontext = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Searchbuttontext");
            Ddo_servicofluxo_servicocod_Caption = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Caption");
            Ddo_servicofluxo_servicocod_Tooltip = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Tooltip");
            Ddo_servicofluxo_servicocod_Cls = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Cls");
            Ddo_servicofluxo_servicocod_Filteredtext_set = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Filteredtext_set");
            Ddo_servicofluxo_servicocod_Filteredtextto_set = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Filteredtextto_set");
            Ddo_servicofluxo_servicocod_Dropdownoptionstype = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Dropdownoptionstype");
            Ddo_servicofluxo_servicocod_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Titlecontrolidtoreplace");
            Ddo_servicofluxo_servicocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Includesortasc"));
            Ddo_servicofluxo_servicocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Includesortdsc"));
            Ddo_servicofluxo_servicocod_Sortedstatus = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Sortedstatus");
            Ddo_servicofluxo_servicocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Includefilter"));
            Ddo_servicofluxo_servicocod_Filtertype = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Filtertype");
            Ddo_servicofluxo_servicocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Filterisrange"));
            Ddo_servicofluxo_servicocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Includedatalist"));
            Ddo_servicofluxo_servicocod_Sortasc = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Sortasc");
            Ddo_servicofluxo_servicocod_Sortdsc = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Sortdsc");
            Ddo_servicofluxo_servicocod_Cleanfilter = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Cleanfilter");
            Ddo_servicofluxo_servicocod_Rangefilterfrom = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Rangefilterfrom");
            Ddo_servicofluxo_servicocod_Rangefilterto = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Rangefilterto");
            Ddo_servicofluxo_servicocod_Searchbuttontext = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Searchbuttontext");
            Ddo_servicofluxo_servicosigla_Caption = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Caption");
            Ddo_servicofluxo_servicosigla_Tooltip = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Tooltip");
            Ddo_servicofluxo_servicosigla_Cls = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Cls");
            Ddo_servicofluxo_servicosigla_Filteredtext_set = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Filteredtext_set");
            Ddo_servicofluxo_servicosigla_Selectedvalue_set = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Selectedvalue_set");
            Ddo_servicofluxo_servicosigla_Dropdownoptionstype = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Dropdownoptionstype");
            Ddo_servicofluxo_servicosigla_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Titlecontrolidtoreplace");
            Ddo_servicofluxo_servicosigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Includesortasc"));
            Ddo_servicofluxo_servicosigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Includesortdsc"));
            Ddo_servicofluxo_servicosigla_Sortedstatus = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Sortedstatus");
            Ddo_servicofluxo_servicosigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Includefilter"));
            Ddo_servicofluxo_servicosigla_Filtertype = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Filtertype");
            Ddo_servicofluxo_servicosigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Filterisrange"));
            Ddo_servicofluxo_servicosigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Includedatalist"));
            Ddo_servicofluxo_servicosigla_Datalisttype = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Datalisttype");
            Ddo_servicofluxo_servicosigla_Datalistproc = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Datalistproc");
            Ddo_servicofluxo_servicosigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servicofluxo_servicosigla_Sortasc = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Sortasc");
            Ddo_servicofluxo_servicosigla_Sortdsc = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Sortdsc");
            Ddo_servicofluxo_servicosigla_Loadingdata = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Loadingdata");
            Ddo_servicofluxo_servicosigla_Cleanfilter = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Cleanfilter");
            Ddo_servicofluxo_servicosigla_Noresultsfound = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Noresultsfound");
            Ddo_servicofluxo_servicosigla_Searchbuttontext = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Searchbuttontext");
            Ddo_servicofluxo_servicotphrq_Caption = cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Caption");
            Ddo_servicofluxo_servicotphrq_Tooltip = cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Tooltip");
            Ddo_servicofluxo_servicotphrq_Cls = cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Cls");
            Ddo_servicofluxo_servicotphrq_Selectedvalue_set = cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Selectedvalue_set");
            Ddo_servicofluxo_servicotphrq_Dropdownoptionstype = cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Dropdownoptionstype");
            Ddo_servicofluxo_servicotphrq_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Titlecontrolidtoreplace");
            Ddo_servicofluxo_servicotphrq_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Includesortasc"));
            Ddo_servicofluxo_servicotphrq_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Includesortdsc"));
            Ddo_servicofluxo_servicotphrq_Sortedstatus = cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Sortedstatus");
            Ddo_servicofluxo_servicotphrq_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Includefilter"));
            Ddo_servicofluxo_servicotphrq_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Includedatalist"));
            Ddo_servicofluxo_servicotphrq_Datalisttype = cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Datalisttype");
            Ddo_servicofluxo_servicotphrq_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Allowmultipleselection"));
            Ddo_servicofluxo_servicotphrq_Datalistfixedvalues = cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Datalistfixedvalues");
            Ddo_servicofluxo_servicotphrq_Sortasc = cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Sortasc");
            Ddo_servicofluxo_servicotphrq_Sortdsc = cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Sortdsc");
            Ddo_servicofluxo_servicotphrq_Cleanfilter = cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Cleanfilter");
            Ddo_servicofluxo_servicotphrq_Searchbuttontext = cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Searchbuttontext");
            Ddo_servicofluxo_ordem_Caption = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Caption");
            Ddo_servicofluxo_ordem_Tooltip = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Tooltip");
            Ddo_servicofluxo_ordem_Cls = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Cls");
            Ddo_servicofluxo_ordem_Filteredtext_set = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Filteredtext_set");
            Ddo_servicofluxo_ordem_Filteredtextto_set = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Filteredtextto_set");
            Ddo_servicofluxo_ordem_Dropdownoptionstype = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Dropdownoptionstype");
            Ddo_servicofluxo_ordem_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Titlecontrolidtoreplace");
            Ddo_servicofluxo_ordem_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_ORDEM_Includesortasc"));
            Ddo_servicofluxo_ordem_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_ORDEM_Includesortdsc"));
            Ddo_servicofluxo_ordem_Sortedstatus = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Sortedstatus");
            Ddo_servicofluxo_ordem_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_ORDEM_Includefilter"));
            Ddo_servicofluxo_ordem_Filtertype = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Filtertype");
            Ddo_servicofluxo_ordem_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_ORDEM_Filterisrange"));
            Ddo_servicofluxo_ordem_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_ORDEM_Includedatalist"));
            Ddo_servicofluxo_ordem_Sortasc = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Sortasc");
            Ddo_servicofluxo_ordem_Sortdsc = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Sortdsc");
            Ddo_servicofluxo_ordem_Cleanfilter = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Cleanfilter");
            Ddo_servicofluxo_ordem_Rangefilterfrom = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Rangefilterfrom");
            Ddo_servicofluxo_ordem_Rangefilterto = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Rangefilterto");
            Ddo_servicofluxo_ordem_Searchbuttontext = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Searchbuttontext");
            Ddo_servicofluxo_servicopos_Caption = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Caption");
            Ddo_servicofluxo_servicopos_Tooltip = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Tooltip");
            Ddo_servicofluxo_servicopos_Cls = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Cls");
            Ddo_servicofluxo_servicopos_Filteredtext_set = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Filteredtext_set");
            Ddo_servicofluxo_servicopos_Filteredtextto_set = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Filteredtextto_set");
            Ddo_servicofluxo_servicopos_Dropdownoptionstype = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Dropdownoptionstype");
            Ddo_servicofluxo_servicopos_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Titlecontrolidtoreplace");
            Ddo_servicofluxo_servicopos_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Includesortasc"));
            Ddo_servicofluxo_servicopos_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Includesortdsc"));
            Ddo_servicofluxo_servicopos_Sortedstatus = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Sortedstatus");
            Ddo_servicofluxo_servicopos_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Includefilter"));
            Ddo_servicofluxo_servicopos_Filtertype = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Filtertype");
            Ddo_servicofluxo_servicopos_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Filterisrange"));
            Ddo_servicofluxo_servicopos_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Includedatalist"));
            Ddo_servicofluxo_servicopos_Sortasc = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Sortasc");
            Ddo_servicofluxo_servicopos_Sortdsc = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Sortdsc");
            Ddo_servicofluxo_servicopos_Cleanfilter = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Cleanfilter");
            Ddo_servicofluxo_servicopos_Rangefilterfrom = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Rangefilterfrom");
            Ddo_servicofluxo_servicopos_Rangefilterto = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Rangefilterto");
            Ddo_servicofluxo_servicopos_Searchbuttontext = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Searchbuttontext");
            Ddo_servicofluxo_srvpossigla_Caption = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Caption");
            Ddo_servicofluxo_srvpossigla_Tooltip = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Tooltip");
            Ddo_servicofluxo_srvpossigla_Cls = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Cls");
            Ddo_servicofluxo_srvpossigla_Filteredtext_set = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Filteredtext_set");
            Ddo_servicofluxo_srvpossigla_Selectedvalue_set = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Selectedvalue_set");
            Ddo_servicofluxo_srvpossigla_Dropdownoptionstype = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Dropdownoptionstype");
            Ddo_servicofluxo_srvpossigla_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Titlecontrolidtoreplace");
            Ddo_servicofluxo_srvpossigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Includesortasc"));
            Ddo_servicofluxo_srvpossigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Includesortdsc"));
            Ddo_servicofluxo_srvpossigla_Sortedstatus = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Sortedstatus");
            Ddo_servicofluxo_srvpossigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Includefilter"));
            Ddo_servicofluxo_srvpossigla_Filtertype = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Filtertype");
            Ddo_servicofluxo_srvpossigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Filterisrange"));
            Ddo_servicofluxo_srvpossigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Includedatalist"));
            Ddo_servicofluxo_srvpossigla_Datalisttype = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Datalisttype");
            Ddo_servicofluxo_srvpossigla_Datalistproc = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Datalistproc");
            Ddo_servicofluxo_srvpossigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servicofluxo_srvpossigla_Sortasc = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Sortasc");
            Ddo_servicofluxo_srvpossigla_Sortdsc = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Sortdsc");
            Ddo_servicofluxo_srvpossigla_Loadingdata = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Loadingdata");
            Ddo_servicofluxo_srvpossigla_Cleanfilter = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Cleanfilter");
            Ddo_servicofluxo_srvpossigla_Noresultsfound = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Noresultsfound");
            Ddo_servicofluxo_srvpossigla_Searchbuttontext = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_servicofluxo_codigo_Activeeventkey = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Activeeventkey");
            Ddo_servicofluxo_codigo_Filteredtext_get = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Filteredtext_get");
            Ddo_servicofluxo_codigo_Filteredtextto_get = cgiGet( "DDO_SERVICOFLUXO_CODIGO_Filteredtextto_get");
            Ddo_servicofluxo_servicocod_Activeeventkey = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Activeeventkey");
            Ddo_servicofluxo_servicocod_Filteredtext_get = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Filteredtext_get");
            Ddo_servicofluxo_servicocod_Filteredtextto_get = cgiGet( "DDO_SERVICOFLUXO_SERVICOCOD_Filteredtextto_get");
            Ddo_servicofluxo_servicosigla_Activeeventkey = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Activeeventkey");
            Ddo_servicofluxo_servicosigla_Filteredtext_get = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Filteredtext_get");
            Ddo_servicofluxo_servicosigla_Selectedvalue_get = cgiGet( "DDO_SERVICOFLUXO_SERVICOSIGLA_Selectedvalue_get");
            Ddo_servicofluxo_servicotphrq_Activeeventkey = cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Activeeventkey");
            Ddo_servicofluxo_servicotphrq_Selectedvalue_get = cgiGet( "DDO_SERVICOFLUXO_SERVICOTPHRQ_Selectedvalue_get");
            Ddo_servicofluxo_ordem_Activeeventkey = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Activeeventkey");
            Ddo_servicofluxo_ordem_Filteredtext_get = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Filteredtext_get");
            Ddo_servicofluxo_ordem_Filteredtextto_get = cgiGet( "DDO_SERVICOFLUXO_ORDEM_Filteredtextto_get");
            Ddo_servicofluxo_servicopos_Activeeventkey = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Activeeventkey");
            Ddo_servicofluxo_servicopos_Filteredtext_get = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Filteredtext_get");
            Ddo_servicofluxo_servicopos_Filteredtextto_get = cgiGet( "DDO_SERVICOFLUXO_SERVICOPOS_Filteredtextto_get");
            Ddo_servicofluxo_srvpossigla_Activeeventkey = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Activeeventkey");
            Ddo_servicofluxo_srvpossigla_Filteredtext_get = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Filteredtext_get");
            Ddo_servicofluxo_srvpossigla_Selectedvalue_get = cgiGet( "DDO_SERVICOFLUXO_SRVPOSSIGLA_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOFLUXO_SERVICOSIGLA1"), AV17ServicoFluxo_ServicoSigla1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOFLUXO_SERVICOSIGLA2"), AV21ServicoFluxo_ServicoSigla2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOFLUXO_SERVICOSIGLA3"), AV25ServicoFluxo_ServicoSigla3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOFLUXO_CODIGO"), ",", ".") != Convert.ToDecimal( AV34TFServicoFluxo_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOFLUXO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV35TFServicoFluxo_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOFLUXO_SERVICOCOD"), ",", ".") != Convert.ToDecimal( AV38TFServicoFluxo_ServicoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOFLUXO_SERVICOCOD_TO"), ",", ".") != Convert.ToDecimal( AV39TFServicoFluxo_ServicoCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOFLUXO_SERVICOSIGLA"), AV42TFServicoFluxo_ServicoSigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOFLUXO_SERVICOSIGLA_SEL"), AV43TFServicoFluxo_ServicoSigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOFLUXO_ORDEM"), ",", ".") != Convert.ToDecimal( AV50TFServicoFluxo_Ordem )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOFLUXO_ORDEM_TO"), ",", ".") != Convert.ToDecimal( AV51TFServicoFluxo_Ordem_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOFLUXO_SERVICOPOS"), ",", ".") != Convert.ToDecimal( AV54TFServicoFluxo_ServicoPos )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICOFLUXO_SERVICOPOS_TO"), ",", ".") != Convert.ToDecimal( AV55TFServicoFluxo_ServicoPos_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOFLUXO_SRVPOSSIGLA"), AV58TFServicoFluxo_SrvPosSigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOFLUXO_SRVPOSSIGLA_SEL"), AV59TFServicoFluxo_SrvPosSigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E30LF2 */
         E30LF2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E30LF2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "SERVICOFLUXO_SERVICOSIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "SERVICOFLUXO_SERVICOSIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "SERVICOFLUXO_SERVICOSIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfservicofluxo_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicofluxo_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_codigo_Visible), 5, 0)));
         edtavTfservicofluxo_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicofluxo_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_codigo_to_Visible), 5, 0)));
         edtavTfservicofluxo_servicocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicofluxo_servicocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_servicocod_Visible), 5, 0)));
         edtavTfservicofluxo_servicocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicofluxo_servicocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_servicocod_to_Visible), 5, 0)));
         edtavTfservicofluxo_servicosigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicofluxo_servicosigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_servicosigla_Visible), 5, 0)));
         edtavTfservicofluxo_servicosigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicofluxo_servicosigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_servicosigla_sel_Visible), 5, 0)));
         edtavTfservicofluxo_ordem_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicofluxo_ordem_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_ordem_Visible), 5, 0)));
         edtavTfservicofluxo_ordem_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicofluxo_ordem_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_ordem_to_Visible), 5, 0)));
         edtavTfservicofluxo_servicopos_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicofluxo_servicopos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_servicopos_Visible), 5, 0)));
         edtavTfservicofluxo_servicopos_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicofluxo_servicopos_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_servicopos_to_Visible), 5, 0)));
         edtavTfservicofluxo_srvpossigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicofluxo_srvpossigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_srvpossigla_Visible), 5, 0)));
         edtavTfservicofluxo_srvpossigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicofluxo_srvpossigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_srvpossigla_sel_Visible), 5, 0)));
         Ddo_servicofluxo_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoFluxo_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_codigo_Internalname, "TitleControlIdToReplace", Ddo_servicofluxo_codigo_Titlecontrolidtoreplace);
         AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace = Ddo_servicofluxo_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace", AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace);
         edtavDdo_servicofluxo_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servicofluxo_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicofluxo_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servicofluxo_servicocod_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoFluxo_ServicoCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicocod_Internalname, "TitleControlIdToReplace", Ddo_servicofluxo_servicocod_Titlecontrolidtoreplace);
         AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace = Ddo_servicofluxo_servicocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace", AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace);
         edtavDdo_servicofluxo_servicocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servicofluxo_servicocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicofluxo_servicocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servicofluxo_servicosigla_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoFluxo_ServicoSigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicosigla_Internalname, "TitleControlIdToReplace", Ddo_servicofluxo_servicosigla_Titlecontrolidtoreplace);
         AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace = Ddo_servicofluxo_servicosigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace", AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace);
         edtavDdo_servicofluxo_servicosiglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servicofluxo_servicosiglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicofluxo_servicosiglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servicofluxo_servicotphrq_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoFluxo_ServicoTpHrq";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicotphrq_Internalname, "TitleControlIdToReplace", Ddo_servicofluxo_servicotphrq_Titlecontrolidtoreplace);
         AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace = Ddo_servicofluxo_servicotphrq_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace", AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace);
         edtavDdo_servicofluxo_servicotphrqtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servicofluxo_servicotphrqtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicofluxo_servicotphrqtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servicofluxo_ordem_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoFluxo_Ordem";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_ordem_Internalname, "TitleControlIdToReplace", Ddo_servicofluxo_ordem_Titlecontrolidtoreplace);
         AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace = Ddo_servicofluxo_ordem_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace", AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace);
         edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servicofluxo_servicopos_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoFluxo_ServicoPos";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicopos_Internalname, "TitleControlIdToReplace", Ddo_servicofluxo_servicopos_Titlecontrolidtoreplace);
         AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace = Ddo_servicofluxo_servicopos_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace", AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace);
         edtavDdo_servicofluxo_servicopostitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servicofluxo_servicopostitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicofluxo_servicopostitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servicofluxo_srvpossigla_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoFluxo_SrvPosSigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_srvpossigla_Internalname, "TitleControlIdToReplace", Ddo_servicofluxo_srvpossigla_Titlecontrolidtoreplace);
         AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace = Ddo_servicofluxo_srvpossigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace", AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace);
         edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Sequ�ncia";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Servi�o", 0);
         cmbavOrderedby.addItem("2", "Codigo", 0);
         cmbavOrderedby.addItem("3", "Servi�o", 0);
         cmbavOrderedby.addItem("4", "de Hierarquia", 0);
         cmbavOrderedby.addItem("5", "Ordem", 0);
         cmbavOrderedby.addItem("6", "Processo", 0);
         cmbavOrderedby.addItem("7", "A��o", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV61DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV61DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E31LF2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV33ServicoFluxo_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37ServicoFluxo_ServicoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41ServicoFluxo_ServicoSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45ServicoFluxo_ServicoTpHrqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49ServicoFluxo_OrdemTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53ServicoFluxo_ServicoPosTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV57ServicoFluxo_SrvPosSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtServicoFluxo_Codigo_Titleformat = 2;
         edtServicoFluxo_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Codigo", AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoFluxo_Codigo_Internalname, "Title", edtServicoFluxo_Codigo_Title);
         edtServicoFluxo_ServicoCod_Titleformat = 2;
         edtServicoFluxo_ServicoCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Servi�o", AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoFluxo_ServicoCod_Internalname, "Title", edtServicoFluxo_ServicoCod_Title);
         edtServicoFluxo_ServicoSigla_Titleformat = 2;
         edtServicoFluxo_ServicoSigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Servi�o", AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoFluxo_ServicoSigla_Internalname, "Title", edtServicoFluxo_ServicoSigla_Title);
         cmbServicoFluxo_ServicoTpHrq_Titleformat = 2;
         cmbServicoFluxo_ServicoTpHrq.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de Hierarquia", AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServicoFluxo_ServicoTpHrq_Internalname, "Title", cmbServicoFluxo_ServicoTpHrq.Title.Text);
         edtServicoFluxo_Ordem_Titleformat = 2;
         edtServicoFluxo_Ordem_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ordem", AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoFluxo_Ordem_Internalname, "Title", edtServicoFluxo_Ordem_Title);
         edtServicoFluxo_ServicoPos_Titleformat = 2;
         edtServicoFluxo_ServicoPos_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Processo", AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoFluxo_ServicoPos_Internalname, "Title", edtServicoFluxo_ServicoPos_Title);
         edtServicoFluxo_SrvPosSigla_Titleformat = 2;
         edtServicoFluxo_SrvPosSigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "A��o", AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoFluxo_SrvPosSigla_Internalname, "Title", edtServicoFluxo_SrvPosSigla_Title);
         AV63GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63GridCurrentPage), 10, 0)));
         AV64GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64GridPageCount), 10, 0)));
         AV67WWServicoFluxoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWServicoFluxoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 = AV17ServicoFluxo_ServicoSigla1;
         AV70WWServicoFluxoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV71WWServicoFluxoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV72WWServicoFluxoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 = AV21ServicoFluxo_ServicoSigla2;
         AV74WWServicoFluxoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV75WWServicoFluxoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV76WWServicoFluxoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 = AV25ServicoFluxo_ServicoSigla3;
         AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo = AV34TFServicoFluxo_Codigo;
         AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to = AV35TFServicoFluxo_Codigo_To;
         AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod = AV38TFServicoFluxo_ServicoCod;
         AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to = AV39TFServicoFluxo_ServicoCod_To;
         AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla = AV42TFServicoFluxo_ServicoSigla;
         AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel = AV43TFServicoFluxo_ServicoSigla_Sel;
         AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels = AV47TFServicoFluxo_ServicoTpHrq_Sels;
         AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem = AV50TFServicoFluxo_Ordem;
         AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to = AV51TFServicoFluxo_Ordem_To;
         AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos = AV54TFServicoFluxo_ServicoPos;
         AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to = AV55TFServicoFluxo_ServicoPos_To;
         AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla = AV58TFServicoFluxo_SrvPosSigla;
         AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel = AV59TFServicoFluxo_SrvPosSigla_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33ServicoFluxo_CodigoTitleFilterData", AV33ServicoFluxo_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37ServicoFluxo_ServicoCodTitleFilterData", AV37ServicoFluxo_ServicoCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV41ServicoFluxo_ServicoSiglaTitleFilterData", AV41ServicoFluxo_ServicoSiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV45ServicoFluxo_ServicoTpHrqTitleFilterData", AV45ServicoFluxo_ServicoTpHrqTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV49ServicoFluxo_OrdemTitleFilterData", AV49ServicoFluxo_OrdemTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV53ServicoFluxo_ServicoPosTitleFilterData", AV53ServicoFluxo_ServicoPosTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV57ServicoFluxo_SrvPosSiglaTitleFilterData", AV57ServicoFluxo_SrvPosSiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11LF2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV62PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV62PageToGo) ;
         }
      }

      protected void E12LF2( )
      {
         /* Ddo_servicofluxo_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicofluxo_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicofluxo_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_codigo_Internalname, "SortedStatus", Ddo_servicofluxo_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicofluxo_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicofluxo_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_codigo_Internalname, "SortedStatus", Ddo_servicofluxo_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicofluxo_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV34TFServicoFluxo_Codigo = (int)(NumberUtil.Val( Ddo_servicofluxo_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFServicoFluxo_Codigo), 6, 0)));
            AV35TFServicoFluxo_Codigo_To = (int)(NumberUtil.Val( Ddo_servicofluxo_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFServicoFluxo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFServicoFluxo_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13LF2( )
      {
         /* Ddo_servicofluxo_servicocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicofluxo_servicocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicofluxo_servicocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicocod_Internalname, "SortedStatus", Ddo_servicofluxo_servicocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicofluxo_servicocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicofluxo_servicocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicocod_Internalname, "SortedStatus", Ddo_servicofluxo_servicocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicofluxo_servicocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV38TFServicoFluxo_ServicoCod = (int)(NumberUtil.Val( Ddo_servicofluxo_servicocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFServicoFluxo_ServicoCod), 6, 0)));
            AV39TFServicoFluxo_ServicoCod_To = (int)(NumberUtil.Val( Ddo_servicofluxo_servicocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFServicoFluxo_ServicoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFServicoFluxo_ServicoCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14LF2( )
      {
         /* Ddo_servicofluxo_servicosigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicofluxo_servicosigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicofluxo_servicosigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicosigla_Internalname, "SortedStatus", Ddo_servicofluxo_servicosigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicofluxo_servicosigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicofluxo_servicosigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicosigla_Internalname, "SortedStatus", Ddo_servicofluxo_servicosigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicofluxo_servicosigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV42TFServicoFluxo_ServicoSigla = Ddo_servicofluxo_servicosigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFServicoFluxo_ServicoSigla", AV42TFServicoFluxo_ServicoSigla);
            AV43TFServicoFluxo_ServicoSigla_Sel = Ddo_servicofluxo_servicosigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFServicoFluxo_ServicoSigla_Sel", AV43TFServicoFluxo_ServicoSigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15LF2( )
      {
         /* Ddo_servicofluxo_servicotphrq_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicofluxo_servicotphrq_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicofluxo_servicotphrq_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicotphrq_Internalname, "SortedStatus", Ddo_servicofluxo_servicotphrq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicofluxo_servicotphrq_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicofluxo_servicotphrq_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicotphrq_Internalname, "SortedStatus", Ddo_servicofluxo_servicotphrq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicofluxo_servicotphrq_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV46TFServicoFluxo_ServicoTpHrq_SelsJson = Ddo_servicofluxo_servicotphrq_Selectedvalue_get;
            AV47TFServicoFluxo_ServicoTpHrq_Sels.FromJSonString(StringUtil.StringReplace( AV46TFServicoFluxo_ServicoTpHrq_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47TFServicoFluxo_ServicoTpHrq_Sels", AV47TFServicoFluxo_ServicoTpHrq_Sels);
      }

      protected void E16LF2( )
      {
         /* Ddo_servicofluxo_ordem_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicofluxo_ordem_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicofluxo_ordem_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_ordem_Internalname, "SortedStatus", Ddo_servicofluxo_ordem_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicofluxo_ordem_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicofluxo_ordem_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_ordem_Internalname, "SortedStatus", Ddo_servicofluxo_ordem_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicofluxo_ordem_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV50TFServicoFluxo_Ordem = (short)(NumberUtil.Val( Ddo_servicofluxo_ordem_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFServicoFluxo_Ordem), 3, 0)));
            AV51TFServicoFluxo_Ordem_To = (short)(NumberUtil.Val( Ddo_servicofluxo_ordem_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFServicoFluxo_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFServicoFluxo_Ordem_To), 3, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17LF2( )
      {
         /* Ddo_servicofluxo_servicopos_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicofluxo_servicopos_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicofluxo_servicopos_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicopos_Internalname, "SortedStatus", Ddo_servicofluxo_servicopos_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicofluxo_servicopos_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicofluxo_servicopos_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicopos_Internalname, "SortedStatus", Ddo_servicofluxo_servicopos_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicofluxo_servicopos_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV54TFServicoFluxo_ServicoPos = (int)(NumberUtil.Val( Ddo_servicofluxo_servicopos_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFServicoFluxo_ServicoPos), 6, 0)));
            AV55TFServicoFluxo_ServicoPos_To = (int)(NumberUtil.Val( Ddo_servicofluxo_servicopos_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFServicoFluxo_ServicoPos_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFServicoFluxo_ServicoPos_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18LF2( )
      {
         /* Ddo_servicofluxo_srvpossigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicofluxo_srvpossigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicofluxo_srvpossigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_srvpossigla_Internalname, "SortedStatus", Ddo_servicofluxo_srvpossigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicofluxo_srvpossigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicofluxo_srvpossigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_srvpossigla_Internalname, "SortedStatus", Ddo_servicofluxo_srvpossigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicofluxo_srvpossigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV58TFServicoFluxo_SrvPosSigla = Ddo_servicofluxo_srvpossigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFServicoFluxo_SrvPosSigla", AV58TFServicoFluxo_SrvPosSigla);
            AV59TFServicoFluxo_SrvPosSigla_Sel = Ddo_servicofluxo_srvpossigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFServicoFluxo_SrvPosSigla_Sel", AV59TFServicoFluxo_SrvPosSigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E32LF2( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV91Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("servicofluxo.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1528ServicoFluxo_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV92Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("servicofluxo.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1528ServicoFluxo_Codigo);
         edtServicoFluxo_ServicoSigla_Link = formatLink("viewservicofluxo.aspx") + "?" + UrlEncode("" +A1528ServicoFluxo_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 88;
         }
         sendrow_882( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_88_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(88, GridRow);
         }
      }

      protected void E19LF2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E25LF2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E20LF2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoFluxo_ServicoSigla1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoFluxo_ServicoSigla2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoFluxo_ServicoSigla3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoFluxo_Codigo, AV35TFServicoFluxo_Codigo_To, AV38TFServicoFluxo_ServicoCod, AV39TFServicoFluxo_ServicoCod_To, AV42TFServicoFluxo_ServicoSigla, AV43TFServicoFluxo_ServicoSigla_Sel, AV50TFServicoFluxo_Ordem, AV51TFServicoFluxo_Ordem_To, AV54TFServicoFluxo_ServicoPos, AV55TFServicoFluxo_ServicoPos_To, AV58TFServicoFluxo_SrvPosSigla, AV59TFServicoFluxo_SrvPosSigla_Sel, AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace, AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace, AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace, AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace, AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace, AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace, AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, AV47TFServicoFluxo_ServicoTpHrq_Sels, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1528ServicoFluxo_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E26LF2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27LF2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E21LF2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoFluxo_ServicoSigla1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoFluxo_ServicoSigla2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoFluxo_ServicoSigla3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoFluxo_Codigo, AV35TFServicoFluxo_Codigo_To, AV38TFServicoFluxo_ServicoCod, AV39TFServicoFluxo_ServicoCod_To, AV42TFServicoFluxo_ServicoSigla, AV43TFServicoFluxo_ServicoSigla_Sel, AV50TFServicoFluxo_Ordem, AV51TFServicoFluxo_Ordem_To, AV54TFServicoFluxo_ServicoPos, AV55TFServicoFluxo_ServicoPos_To, AV58TFServicoFluxo_SrvPosSigla, AV59TFServicoFluxo_SrvPosSigla_Sel, AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace, AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace, AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace, AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace, AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace, AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace, AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, AV47TFServicoFluxo_ServicoTpHrq_Sels, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1528ServicoFluxo_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E28LF2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22LF2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoFluxo_ServicoSigla1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ServicoFluxo_ServicoSigla2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ServicoFluxo_ServicoSigla3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFServicoFluxo_Codigo, AV35TFServicoFluxo_Codigo_To, AV38TFServicoFluxo_ServicoCod, AV39TFServicoFluxo_ServicoCod_To, AV42TFServicoFluxo_ServicoSigla, AV43TFServicoFluxo_ServicoSigla_Sel, AV50TFServicoFluxo_Ordem, AV51TFServicoFluxo_Ordem_To, AV54TFServicoFluxo_ServicoPos, AV55TFServicoFluxo_ServicoPos_To, AV58TFServicoFluxo_SrvPosSigla, AV59TFServicoFluxo_SrvPosSigla_Sel, AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace, AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace, AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace, AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace, AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace, AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace, AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, AV47TFServicoFluxo_ServicoTpHrq_Sels, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1528ServicoFluxo_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E29LF2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23LF2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47TFServicoFluxo_ServicoTpHrq_Sels", AV47TFServicoFluxo_ServicoTpHrq_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E24LF2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("servicofluxo.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_servicofluxo_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_codigo_Internalname, "SortedStatus", Ddo_servicofluxo_codigo_Sortedstatus);
         Ddo_servicofluxo_servicocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicocod_Internalname, "SortedStatus", Ddo_servicofluxo_servicocod_Sortedstatus);
         Ddo_servicofluxo_servicosigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicosigla_Internalname, "SortedStatus", Ddo_servicofluxo_servicosigla_Sortedstatus);
         Ddo_servicofluxo_servicotphrq_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicotphrq_Internalname, "SortedStatus", Ddo_servicofluxo_servicotphrq_Sortedstatus);
         Ddo_servicofluxo_ordem_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_ordem_Internalname, "SortedStatus", Ddo_servicofluxo_ordem_Sortedstatus);
         Ddo_servicofluxo_servicopos_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicopos_Internalname, "SortedStatus", Ddo_servicofluxo_servicopos_Sortedstatus);
         Ddo_servicofluxo_srvpossigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_srvpossigla_Internalname, "SortedStatus", Ddo_servicofluxo_srvpossigla_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_servicofluxo_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_codigo_Internalname, "SortedStatus", Ddo_servicofluxo_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_servicofluxo_servicocod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicocod_Internalname, "SortedStatus", Ddo_servicofluxo_servicocod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_servicofluxo_servicosigla_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicosigla_Internalname, "SortedStatus", Ddo_servicofluxo_servicosigla_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_servicofluxo_servicotphrq_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicotphrq_Internalname, "SortedStatus", Ddo_servicofluxo_servicotphrq_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_servicofluxo_ordem_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_ordem_Internalname, "SortedStatus", Ddo_servicofluxo_ordem_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_servicofluxo_servicopos_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicopos_Internalname, "SortedStatus", Ddo_servicofluxo_servicopos_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_servicofluxo_srvpossigla_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_srvpossigla_Internalname, "SortedStatus", Ddo_servicofluxo_srvpossigla_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavServicofluxo_servicosigla1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicofluxo_servicosigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicofluxo_servicosigla1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOFLUXO_SERVICOSIGLA") == 0 )
         {
            edtavServicofluxo_servicosigla1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicofluxo_servicosigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicofluxo_servicosigla1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavServicofluxo_servicosigla2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicofluxo_servicosigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicofluxo_servicosigla2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICOFLUXO_SERVICOSIGLA") == 0 )
         {
            edtavServicofluxo_servicosigla2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicofluxo_servicosigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicofluxo_servicosigla2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavServicofluxo_servicosigla3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicofluxo_servicosigla3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicofluxo_servicosigla3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SERVICOFLUXO_SERVICOSIGLA") == 0 )
         {
            edtavServicofluxo_servicosigla3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicofluxo_servicosigla3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicofluxo_servicosigla3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "SERVICOFLUXO_SERVICOSIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21ServicoFluxo_ServicoSigla2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ServicoFluxo_ServicoSigla2", AV21ServicoFluxo_ServicoSigla2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "SERVICOFLUXO_SERVICOSIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25ServicoFluxo_ServicoSigla3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ServicoFluxo_ServicoSigla3", AV25ServicoFluxo_ServicoSigla3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV34TFServicoFluxo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFServicoFluxo_Codigo), 6, 0)));
         Ddo_servicofluxo_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_codigo_Internalname, "FilteredText_set", Ddo_servicofluxo_codigo_Filteredtext_set);
         AV35TFServicoFluxo_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFServicoFluxo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFServicoFluxo_Codigo_To), 6, 0)));
         Ddo_servicofluxo_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_codigo_Internalname, "FilteredTextTo_set", Ddo_servicofluxo_codigo_Filteredtextto_set);
         AV38TFServicoFluxo_ServicoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFServicoFluxo_ServicoCod), 6, 0)));
         Ddo_servicofluxo_servicocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicocod_Internalname, "FilteredText_set", Ddo_servicofluxo_servicocod_Filteredtext_set);
         AV39TFServicoFluxo_ServicoCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFServicoFluxo_ServicoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFServicoFluxo_ServicoCod_To), 6, 0)));
         Ddo_servicofluxo_servicocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicocod_Internalname, "FilteredTextTo_set", Ddo_servicofluxo_servicocod_Filteredtextto_set);
         AV42TFServicoFluxo_ServicoSigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFServicoFluxo_ServicoSigla", AV42TFServicoFluxo_ServicoSigla);
         Ddo_servicofluxo_servicosigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicosigla_Internalname, "FilteredText_set", Ddo_servicofluxo_servicosigla_Filteredtext_set);
         AV43TFServicoFluxo_ServicoSigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFServicoFluxo_ServicoSigla_Sel", AV43TFServicoFluxo_ServicoSigla_Sel);
         Ddo_servicofluxo_servicosigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicosigla_Internalname, "SelectedValue_set", Ddo_servicofluxo_servicosigla_Selectedvalue_set);
         AV47TFServicoFluxo_ServicoTpHrq_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_servicofluxo_servicotphrq_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicotphrq_Internalname, "SelectedValue_set", Ddo_servicofluxo_servicotphrq_Selectedvalue_set);
         AV50TFServicoFluxo_Ordem = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFServicoFluxo_Ordem), 3, 0)));
         Ddo_servicofluxo_ordem_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_ordem_Internalname, "FilteredText_set", Ddo_servicofluxo_ordem_Filteredtext_set);
         AV51TFServicoFluxo_Ordem_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFServicoFluxo_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFServicoFluxo_Ordem_To), 3, 0)));
         Ddo_servicofluxo_ordem_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_ordem_Internalname, "FilteredTextTo_set", Ddo_servicofluxo_ordem_Filteredtextto_set);
         AV54TFServicoFluxo_ServicoPos = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFServicoFluxo_ServicoPos), 6, 0)));
         Ddo_servicofluxo_servicopos_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicopos_Internalname, "FilteredText_set", Ddo_servicofluxo_servicopos_Filteredtext_set);
         AV55TFServicoFluxo_ServicoPos_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFServicoFluxo_ServicoPos_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFServicoFluxo_ServicoPos_To), 6, 0)));
         Ddo_servicofluxo_servicopos_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicopos_Internalname, "FilteredTextTo_set", Ddo_servicofluxo_servicopos_Filteredtextto_set);
         AV58TFServicoFluxo_SrvPosSigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFServicoFluxo_SrvPosSigla", AV58TFServicoFluxo_SrvPosSigla);
         Ddo_servicofluxo_srvpossigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_srvpossigla_Internalname, "FilteredText_set", Ddo_servicofluxo_srvpossigla_Filteredtext_set);
         AV59TFServicoFluxo_SrvPosSigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFServicoFluxo_SrvPosSigla_Sel", AV59TFServicoFluxo_SrvPosSigla_Sel);
         Ddo_servicofluxo_srvpossigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_srvpossigla_Internalname, "SelectedValue_set", Ddo_servicofluxo_srvpossigla_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "SERVICOFLUXO_SERVICOSIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17ServicoFluxo_ServicoSigla1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoFluxo_ServicoSigla1", AV17ServicoFluxo_ServicoSigla1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV93Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV93Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV93Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV94GXV1 = 1;
         while ( AV94GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV94GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_CODIGO") == 0 )
            {
               AV34TFServicoFluxo_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFServicoFluxo_Codigo), 6, 0)));
               AV35TFServicoFluxo_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFServicoFluxo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFServicoFluxo_Codigo_To), 6, 0)));
               if ( ! (0==AV34TFServicoFluxo_Codigo) )
               {
                  Ddo_servicofluxo_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV34TFServicoFluxo_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_codigo_Internalname, "FilteredText_set", Ddo_servicofluxo_codigo_Filteredtext_set);
               }
               if ( ! (0==AV35TFServicoFluxo_Codigo_To) )
               {
                  Ddo_servicofluxo_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV35TFServicoFluxo_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_codigo_Internalname, "FilteredTextTo_set", Ddo_servicofluxo_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SERVICOCOD") == 0 )
            {
               AV38TFServicoFluxo_ServicoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFServicoFluxo_ServicoCod), 6, 0)));
               AV39TFServicoFluxo_ServicoCod_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFServicoFluxo_ServicoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFServicoFluxo_ServicoCod_To), 6, 0)));
               if ( ! (0==AV38TFServicoFluxo_ServicoCod) )
               {
                  Ddo_servicofluxo_servicocod_Filteredtext_set = StringUtil.Str( (decimal)(AV38TFServicoFluxo_ServicoCod), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicocod_Internalname, "FilteredText_set", Ddo_servicofluxo_servicocod_Filteredtext_set);
               }
               if ( ! (0==AV39TFServicoFluxo_ServicoCod_To) )
               {
                  Ddo_servicofluxo_servicocod_Filteredtextto_set = StringUtil.Str( (decimal)(AV39TFServicoFluxo_ServicoCod_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicocod_Internalname, "FilteredTextTo_set", Ddo_servicofluxo_servicocod_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SERVICOSIGLA") == 0 )
            {
               AV42TFServicoFluxo_ServicoSigla = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFServicoFluxo_ServicoSigla", AV42TFServicoFluxo_ServicoSigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFServicoFluxo_ServicoSigla)) )
               {
                  Ddo_servicofluxo_servicosigla_Filteredtext_set = AV42TFServicoFluxo_ServicoSigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicosigla_Internalname, "FilteredText_set", Ddo_servicofluxo_servicosigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SERVICOSIGLA_SEL") == 0 )
            {
               AV43TFServicoFluxo_ServicoSigla_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFServicoFluxo_ServicoSigla_Sel", AV43TFServicoFluxo_ServicoSigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFServicoFluxo_ServicoSigla_Sel)) )
               {
                  Ddo_servicofluxo_servicosigla_Selectedvalue_set = AV43TFServicoFluxo_ServicoSigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicosigla_Internalname, "SelectedValue_set", Ddo_servicofluxo_servicosigla_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SERVICOTPHRQ_SEL") == 0 )
            {
               AV46TFServicoFluxo_ServicoTpHrq_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV47TFServicoFluxo_ServicoTpHrq_Sels.FromJSonString(AV46TFServicoFluxo_ServicoTpHrq_SelsJson);
               if ( ! ( AV47TFServicoFluxo_ServicoTpHrq_Sels.Count == 0 ) )
               {
                  Ddo_servicofluxo_servicotphrq_Selectedvalue_set = AV46TFServicoFluxo_ServicoTpHrq_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicotphrq_Internalname, "SelectedValue_set", Ddo_servicofluxo_servicotphrq_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_ORDEM") == 0 )
            {
               AV50TFServicoFluxo_Ordem = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFServicoFluxo_Ordem), 3, 0)));
               AV51TFServicoFluxo_Ordem_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFServicoFluxo_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFServicoFluxo_Ordem_To), 3, 0)));
               if ( ! (0==AV50TFServicoFluxo_Ordem) )
               {
                  Ddo_servicofluxo_ordem_Filteredtext_set = StringUtil.Str( (decimal)(AV50TFServicoFluxo_Ordem), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_ordem_Internalname, "FilteredText_set", Ddo_servicofluxo_ordem_Filteredtext_set);
               }
               if ( ! (0==AV51TFServicoFluxo_Ordem_To) )
               {
                  Ddo_servicofluxo_ordem_Filteredtextto_set = StringUtil.Str( (decimal)(AV51TFServicoFluxo_Ordem_To), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_ordem_Internalname, "FilteredTextTo_set", Ddo_servicofluxo_ordem_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SERVICOPOS") == 0 )
            {
               AV54TFServicoFluxo_ServicoPos = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFServicoFluxo_ServicoPos), 6, 0)));
               AV55TFServicoFluxo_ServicoPos_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFServicoFluxo_ServicoPos_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFServicoFluxo_ServicoPos_To), 6, 0)));
               if ( ! (0==AV54TFServicoFluxo_ServicoPos) )
               {
                  Ddo_servicofluxo_servicopos_Filteredtext_set = StringUtil.Str( (decimal)(AV54TFServicoFluxo_ServicoPos), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicopos_Internalname, "FilteredText_set", Ddo_servicofluxo_servicopos_Filteredtext_set);
               }
               if ( ! (0==AV55TFServicoFluxo_ServicoPos_To) )
               {
                  Ddo_servicofluxo_servicopos_Filteredtextto_set = StringUtil.Str( (decimal)(AV55TFServicoFluxo_ServicoPos_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_servicopos_Internalname, "FilteredTextTo_set", Ddo_servicofluxo_servicopos_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SRVPOSSIGLA") == 0 )
            {
               AV58TFServicoFluxo_SrvPosSigla = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFServicoFluxo_SrvPosSigla", AV58TFServicoFluxo_SrvPosSigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFServicoFluxo_SrvPosSigla)) )
               {
                  Ddo_servicofluxo_srvpossigla_Filteredtext_set = AV58TFServicoFluxo_SrvPosSigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_srvpossigla_Internalname, "FilteredText_set", Ddo_servicofluxo_srvpossigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SRVPOSSIGLA_SEL") == 0 )
            {
               AV59TFServicoFluxo_SrvPosSigla_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFServicoFluxo_SrvPosSigla_Sel", AV59TFServicoFluxo_SrvPosSigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFServicoFluxo_SrvPosSigla_Sel)) )
               {
                  Ddo_servicofluxo_srvpossigla_Selectedvalue_set = AV59TFServicoFluxo_SrvPosSigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicofluxo_srvpossigla_Internalname, "SelectedValue_set", Ddo_servicofluxo_srvpossigla_Selectedvalue_set);
               }
            }
            AV94GXV1 = (int)(AV94GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOFLUXO_SERVICOSIGLA") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ServicoFluxo_ServicoSigla1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoFluxo_ServicoSigla1", AV17ServicoFluxo_ServicoSigla1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICOFLUXO_SERVICOSIGLA") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21ServicoFluxo_ServicoSigla2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ServicoFluxo_ServicoSigla2", AV21ServicoFluxo_ServicoSigla2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SERVICOFLUXO_SERVICOSIGLA") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25ServicoFluxo_ServicoSigla3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ServicoFluxo_ServicoSigla3", AV25ServicoFluxo_ServicoSigla3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV93Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV34TFServicoFluxo_Codigo) && (0==AV35TFServicoFluxo_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOFLUXO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV34TFServicoFluxo_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV35TFServicoFluxo_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV38TFServicoFluxo_ServicoCod) && (0==AV39TFServicoFluxo_ServicoCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOFLUXO_SERVICOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV38TFServicoFluxo_ServicoCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV39TFServicoFluxo_ServicoCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFServicoFluxo_ServicoSigla)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOFLUXO_SERVICOSIGLA";
            AV11GridStateFilterValue.gxTpr_Value = AV42TFServicoFluxo_ServicoSigla;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFServicoFluxo_ServicoSigla_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOFLUXO_SERVICOSIGLA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV43TFServicoFluxo_ServicoSigla_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV47TFServicoFluxo_ServicoTpHrq_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOFLUXO_SERVICOTPHRQ_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV47TFServicoFluxo_ServicoTpHrq_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV50TFServicoFluxo_Ordem) && (0==AV51TFServicoFluxo_Ordem_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOFLUXO_ORDEM";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV50TFServicoFluxo_Ordem), 3, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV51TFServicoFluxo_Ordem_To), 3, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV54TFServicoFluxo_ServicoPos) && (0==AV55TFServicoFluxo_ServicoPos_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOFLUXO_SERVICOPOS";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV54TFServicoFluxo_ServicoPos), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV55TFServicoFluxo_ServicoPos_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFServicoFluxo_SrvPosSigla)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOFLUXO_SRVPOSSIGLA";
            AV11GridStateFilterValue.gxTpr_Value = AV58TFServicoFluxo_SrvPosSigla;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFServicoFluxo_SrvPosSigla_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOFLUXO_SRVPOSSIGLA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV59TFServicoFluxo_SrvPosSigla_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV93Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ServicoFluxo_ServicoSigla1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17ServicoFluxo_ServicoSigla1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ServicoFluxo_ServicoSigla2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21ServicoFluxo_ServicoSigla2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ServicoFluxo_ServicoSigla3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25ServicoFluxo_ServicoSigla3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV93Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ServicoFluxo";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_LF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_LF2( true) ;
         }
         else
         {
            wb_table2_8_LF2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_LF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_82_LF2( true) ;
         }
         else
         {
            wb_table3_82_LF2( false) ;
         }
         return  ;
      }

      protected void wb_table3_82_LF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_LF2e( true) ;
         }
         else
         {
            wb_table1_2_LF2e( false) ;
         }
      }

      protected void wb_table3_82_LF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_85_LF2( true) ;
         }
         else
         {
            wb_table4_85_LF2( false) ;
         }
         return  ;
      }

      protected void wb_table4_85_LF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_82_LF2e( true) ;
         }
         else
         {
            wb_table3_82_LF2e( false) ;
         }
      }

      protected void wb_table4_85_LF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"88\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoFluxo_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoFluxo_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoFluxo_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoFluxo_ServicoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoFluxo_ServicoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoFluxo_ServicoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoFluxo_ServicoSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoFluxo_ServicoSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoFluxo_ServicoSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbServicoFluxo_ServicoTpHrq_Titleformat == 0 )
               {
                  context.SendWebValue( cmbServicoFluxo_ServicoTpHrq.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbServicoFluxo_ServicoTpHrq.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(43), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoFluxo_Ordem_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoFluxo_Ordem_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoFluxo_Ordem_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoFluxo_ServicoPos_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoFluxo_ServicoPos_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoFluxo_ServicoPos_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoFluxo_SrvPosSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoFluxo_SrvPosSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoFluxo_SrvPosSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1528ServicoFluxo_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoFluxo_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoFluxo_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoFluxo_ServicoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoFluxo_ServicoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1523ServicoFluxo_ServicoSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoFluxo_ServicoSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoFluxo_ServicoSigla_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtServicoFluxo_ServicoSigla_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1533ServicoFluxo_ServicoTpHrq), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbServicoFluxo_ServicoTpHrq.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbServicoFluxo_ServicoTpHrq_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1532ServicoFluxo_Ordem), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoFluxo_Ordem_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoFluxo_Ordem_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoFluxo_ServicoPos_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoFluxo_ServicoPos_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1527ServicoFluxo_SrvPosSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoFluxo_SrvPosSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoFluxo_SrvPosSigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 88 )
         {
            wbEnd = 0;
            nRC_GXsfl_88 = (short)(nGXsfl_88_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_85_LF2e( true) ;
         }
         else
         {
            wb_table4_85_LF2e( false) ;
         }
      }

      protected void wb_table2_8_LF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServicofluxotitle_Internalname, "Sequ�ncia", "", "", lblServicofluxotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_LF2( true) ;
         }
         else
         {
            wb_table5_13_LF2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_LF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWServicoFluxo.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_LF2( true) ;
         }
         else
         {
            wb_table6_23_LF2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_LF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_LF2e( true) ;
         }
         else
         {
            wb_table2_8_LF2e( false) ;
         }
      }

      protected void wb_table6_23_LF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_LF2( true) ;
         }
         else
         {
            wb_table7_28_LF2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_LF2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_LF2e( true) ;
         }
         else
         {
            wb_table6_23_LF2e( false) ;
         }
      }

      protected void wb_table7_28_LF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWServicoFluxo.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_LF2( true) ;
         }
         else
         {
            wb_table8_37_LF2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_LF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoFluxo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WWServicoFluxo.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_54_LF2( true) ;
         }
         else
         {
            wb_table9_54_LF2( false) ;
         }
         return  ;
      }

      protected void wb_table9_54_LF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoFluxo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_WWServicoFluxo.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_71_LF2( true) ;
         }
         else
         {
            wb_table10_71_LF2( false) ;
         }
         return  ;
      }

      protected void wb_table10_71_LF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_LF2e( true) ;
         }
         else
         {
            wb_table7_28_LF2e( false) ;
         }
      }

      protected void wb_table10_71_LF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_WWServicoFluxo.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicofluxo_servicosigla3_Internalname, StringUtil.RTrim( AV25ServicoFluxo_ServicoSigla3), StringUtil.RTrim( context.localUtil.Format( AV25ServicoFluxo_ServicoSigla3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicofluxo_servicosigla3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServicofluxo_servicosigla3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_71_LF2e( true) ;
         }
         else
         {
            wb_table10_71_LF2e( false) ;
         }
      }

      protected void wb_table9_54_LF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WWServicoFluxo.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicofluxo_servicosigla2_Internalname, StringUtil.RTrim( AV21ServicoFluxo_ServicoSigla2), StringUtil.RTrim( context.localUtil.Format( AV21ServicoFluxo_ServicoSigla2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicofluxo_servicosigla2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServicofluxo_servicosigla2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_54_LF2e( true) ;
         }
         else
         {
            wb_table9_54_LF2e( false) ;
         }
      }

      protected void wb_table8_37_LF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWServicoFluxo.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicofluxo_servicosigla1_Internalname, StringUtil.RTrim( AV17ServicoFluxo_ServicoSigla1), StringUtil.RTrim( context.localUtil.Format( AV17ServicoFluxo_ServicoSigla1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicofluxo_servicosigla1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServicofluxo_servicosigla1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_LF2e( true) ;
         }
         else
         {
            wb_table8_37_LF2e( false) ;
         }
      }

      protected void wb_table5_13_LF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_LF2e( true) ;
         }
         else
         {
            wb_table5_13_LF2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PALF2( ) ;
         WSLF2( ) ;
         WELF2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299432888");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwservicofluxo.js", "?20205299432889");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_idx;
         edtServicoFluxo_Codigo_Internalname = "SERVICOFLUXO_CODIGO_"+sGXsfl_88_idx;
         edtServicoFluxo_ServicoCod_Internalname = "SERVICOFLUXO_SERVICOCOD_"+sGXsfl_88_idx;
         edtServicoFluxo_ServicoSigla_Internalname = "SERVICOFLUXO_SERVICOSIGLA_"+sGXsfl_88_idx;
         cmbServicoFluxo_ServicoTpHrq_Internalname = "SERVICOFLUXO_SERVICOTPHRQ_"+sGXsfl_88_idx;
         edtServicoFluxo_Ordem_Internalname = "SERVICOFLUXO_ORDEM_"+sGXsfl_88_idx;
         edtServicoFluxo_ServicoPos_Internalname = "SERVICOFLUXO_SERVICOPOS_"+sGXsfl_88_idx;
         edtServicoFluxo_SrvPosSigla_Internalname = "SERVICOFLUXO_SRVPOSSIGLA_"+sGXsfl_88_idx;
      }

      protected void SubsflControlProps_fel_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_fel_idx;
         edtServicoFluxo_Codigo_Internalname = "SERVICOFLUXO_CODIGO_"+sGXsfl_88_fel_idx;
         edtServicoFluxo_ServicoCod_Internalname = "SERVICOFLUXO_SERVICOCOD_"+sGXsfl_88_fel_idx;
         edtServicoFluxo_ServicoSigla_Internalname = "SERVICOFLUXO_SERVICOSIGLA_"+sGXsfl_88_fel_idx;
         cmbServicoFluxo_ServicoTpHrq_Internalname = "SERVICOFLUXO_SERVICOTPHRQ_"+sGXsfl_88_fel_idx;
         edtServicoFluxo_Ordem_Internalname = "SERVICOFLUXO_ORDEM_"+sGXsfl_88_fel_idx;
         edtServicoFluxo_ServicoPos_Internalname = "SERVICOFLUXO_SERVICOPOS_"+sGXsfl_88_fel_idx;
         edtServicoFluxo_SrvPosSigla_Internalname = "SERVICOFLUXO_SRVPOSSIGLA_"+sGXsfl_88_fel_idx;
      }

      protected void sendrow_882( )
      {
         SubsflControlProps_882( ) ;
         WBLF0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_88_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_88_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_88_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV91Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV91Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV92Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV92Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1528ServicoFluxo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1528ServicoFluxo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoFluxo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_ServicoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1522ServicoFluxo_ServicoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoFluxo_ServicoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_ServicoSigla_Internalname,StringUtil.RTrim( A1523ServicoFluxo_ServicoSigla),StringUtil.RTrim( context.localUtil.Format( A1523ServicoFluxo_ServicoSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtServicoFluxo_ServicoSigla_Link,(String)"",(String)"",(String)"",(String)edtServicoFluxo_ServicoSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "SERVICOFLUXO_SERVICOTPHRQ_" + sGXsfl_88_idx;
            cmbServicoFluxo_ServicoTpHrq.Name = GXCCtl;
            cmbServicoFluxo_ServicoTpHrq.WebTags = "";
            cmbServicoFluxo_ServicoTpHrq.addItem("1", "Prim�rio", 0);
            cmbServicoFluxo_ServicoTpHrq.addItem("2", "Secund�rio", 0);
            if ( cmbServicoFluxo_ServicoTpHrq.ItemCount > 0 )
            {
               A1533ServicoFluxo_ServicoTpHrq = (short)(NumberUtil.Val( cmbServicoFluxo_ServicoTpHrq.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1533ServicoFluxo_ServicoTpHrq), 4, 0))), "."));
               n1533ServicoFluxo_ServicoTpHrq = false;
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbServicoFluxo_ServicoTpHrq,(String)cmbServicoFluxo_ServicoTpHrq_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1533ServicoFluxo_ServicoTpHrq), 4, 0)),(short)1,(String)cmbServicoFluxo_ServicoTpHrq_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbServicoFluxo_ServicoTpHrq.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1533ServicoFluxo_ServicoTpHrq), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServicoFluxo_ServicoTpHrq_Internalname, "Values", (String)(cmbServicoFluxo_ServicoTpHrq.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_Ordem_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1532ServicoFluxo_Ordem), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A1532ServicoFluxo_Ordem), "ZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoFluxo_Ordem_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)43,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Ordem",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_ServicoPos_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoFluxo_ServicoPos_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_SrvPosSigla_Internalname,StringUtil.RTrim( A1527ServicoFluxo_SrvPosSigla),StringUtil.RTrim( context.localUtil.Format( A1527ServicoFluxo_SrvPosSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoFluxo_SrvPosSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICOFLUXO_CODIGO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A1528ServicoFluxo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICOFLUXO_SERVICOCOD"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A1522ServicoFluxo_ServicoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICOFLUXO_ORDEM"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A1532ServicoFluxo_Ordem), "ZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICOFLUXO_SERVICOPOS"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         /* End function sendrow_882 */
      }

      protected void init_default_properties( )
      {
         lblServicofluxotitle_Internalname = "SERVICOFLUXOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavServicofluxo_servicosigla1_Internalname = "vSERVICOFLUXO_SERVICOSIGLA1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavServicofluxo_servicosigla2_Internalname = "vSERVICOFLUXO_SERVICOSIGLA2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavServicofluxo_servicosigla3_Internalname = "vSERVICOFLUXO_SERVICOSIGLA3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtServicoFluxo_Codigo_Internalname = "SERVICOFLUXO_CODIGO";
         edtServicoFluxo_ServicoCod_Internalname = "SERVICOFLUXO_SERVICOCOD";
         edtServicoFluxo_ServicoSigla_Internalname = "SERVICOFLUXO_SERVICOSIGLA";
         cmbServicoFluxo_ServicoTpHrq_Internalname = "SERVICOFLUXO_SERVICOTPHRQ";
         edtServicoFluxo_Ordem_Internalname = "SERVICOFLUXO_ORDEM";
         edtServicoFluxo_ServicoPos_Internalname = "SERVICOFLUXO_SERVICOPOS";
         edtServicoFluxo_SrvPosSigla_Internalname = "SERVICOFLUXO_SRVPOSSIGLA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfservicofluxo_codigo_Internalname = "vTFSERVICOFLUXO_CODIGO";
         edtavTfservicofluxo_codigo_to_Internalname = "vTFSERVICOFLUXO_CODIGO_TO";
         edtavTfservicofluxo_servicocod_Internalname = "vTFSERVICOFLUXO_SERVICOCOD";
         edtavTfservicofluxo_servicocod_to_Internalname = "vTFSERVICOFLUXO_SERVICOCOD_TO";
         edtavTfservicofluxo_servicosigla_Internalname = "vTFSERVICOFLUXO_SERVICOSIGLA";
         edtavTfservicofluxo_servicosigla_sel_Internalname = "vTFSERVICOFLUXO_SERVICOSIGLA_SEL";
         edtavTfservicofluxo_ordem_Internalname = "vTFSERVICOFLUXO_ORDEM";
         edtavTfservicofluxo_ordem_to_Internalname = "vTFSERVICOFLUXO_ORDEM_TO";
         edtavTfservicofluxo_servicopos_Internalname = "vTFSERVICOFLUXO_SERVICOPOS";
         edtavTfservicofluxo_servicopos_to_Internalname = "vTFSERVICOFLUXO_SERVICOPOS_TO";
         edtavTfservicofluxo_srvpossigla_Internalname = "vTFSERVICOFLUXO_SRVPOSSIGLA";
         edtavTfservicofluxo_srvpossigla_sel_Internalname = "vTFSERVICOFLUXO_SRVPOSSIGLA_SEL";
         Ddo_servicofluxo_codigo_Internalname = "DDO_SERVICOFLUXO_CODIGO";
         edtavDdo_servicofluxo_codigotitlecontrolidtoreplace_Internalname = "vDDO_SERVICOFLUXO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_servicofluxo_servicocod_Internalname = "DDO_SERVICOFLUXO_SERVICOCOD";
         edtavDdo_servicofluxo_servicocodtitlecontrolidtoreplace_Internalname = "vDDO_SERVICOFLUXO_SERVICOCODTITLECONTROLIDTOREPLACE";
         Ddo_servicofluxo_servicosigla_Internalname = "DDO_SERVICOFLUXO_SERVICOSIGLA";
         edtavDdo_servicofluxo_servicosiglatitlecontrolidtoreplace_Internalname = "vDDO_SERVICOFLUXO_SERVICOSIGLATITLECONTROLIDTOREPLACE";
         Ddo_servicofluxo_servicotphrq_Internalname = "DDO_SERVICOFLUXO_SERVICOTPHRQ";
         edtavDdo_servicofluxo_servicotphrqtitlecontrolidtoreplace_Internalname = "vDDO_SERVICOFLUXO_SERVICOTPHRQTITLECONTROLIDTOREPLACE";
         Ddo_servicofluxo_ordem_Internalname = "DDO_SERVICOFLUXO_ORDEM";
         edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Internalname = "vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE";
         Ddo_servicofluxo_servicopos_Internalname = "DDO_SERVICOFLUXO_SERVICOPOS";
         edtavDdo_servicofluxo_servicopostitlecontrolidtoreplace_Internalname = "vDDO_SERVICOFLUXO_SERVICOPOSTITLECONTROLIDTOREPLACE";
         Ddo_servicofluxo_srvpossigla_Internalname = "DDO_SERVICOFLUXO_SRVPOSSIGLA";
         edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Internalname = "vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtServicoFluxo_SrvPosSigla_Jsonclick = "";
         edtServicoFluxo_ServicoPos_Jsonclick = "";
         edtServicoFluxo_Ordem_Jsonclick = "";
         cmbServicoFluxo_ServicoTpHrq_Jsonclick = "";
         edtServicoFluxo_ServicoSigla_Jsonclick = "";
         edtServicoFluxo_ServicoCod_Jsonclick = "";
         edtServicoFluxo_Codigo_Jsonclick = "";
         edtavServicofluxo_servicosigla1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavServicofluxo_servicosigla2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavServicofluxo_servicosigla3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtServicoFluxo_ServicoSigla_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtServicoFluxo_SrvPosSigla_Titleformat = 0;
         edtServicoFluxo_ServicoPos_Titleformat = 0;
         edtServicoFluxo_Ordem_Titleformat = 0;
         cmbServicoFluxo_ServicoTpHrq_Titleformat = 0;
         edtServicoFluxo_ServicoSigla_Titleformat = 0;
         edtServicoFluxo_ServicoCod_Titleformat = 0;
         edtServicoFluxo_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavServicofluxo_servicosigla3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavServicofluxo_servicosigla2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavServicofluxo_servicosigla1_Visible = 1;
         edtServicoFluxo_SrvPosSigla_Title = "A��o";
         edtServicoFluxo_ServicoPos_Title = "Processo";
         edtServicoFluxo_Ordem_Title = "Ordem";
         cmbServicoFluxo_ServicoTpHrq.Title.Text = "de Hierarquia";
         edtServicoFluxo_ServicoSigla_Title = "Servi�o";
         edtServicoFluxo_ServicoCod_Title = "Servi�o";
         edtServicoFluxo_Codigo_Title = "Codigo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servicofluxo_servicopostitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servicofluxo_servicotphrqtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servicofluxo_servicosiglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servicofluxo_servicocodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servicofluxo_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfservicofluxo_srvpossigla_sel_Jsonclick = "";
         edtavTfservicofluxo_srvpossigla_sel_Visible = 1;
         edtavTfservicofluxo_srvpossigla_Jsonclick = "";
         edtavTfservicofluxo_srvpossigla_Visible = 1;
         edtavTfservicofluxo_servicopos_to_Jsonclick = "";
         edtavTfservicofluxo_servicopos_to_Visible = 1;
         edtavTfservicofluxo_servicopos_Jsonclick = "";
         edtavTfservicofluxo_servicopos_Visible = 1;
         edtavTfservicofluxo_ordem_to_Jsonclick = "";
         edtavTfservicofluxo_ordem_to_Visible = 1;
         edtavTfservicofluxo_ordem_Jsonclick = "";
         edtavTfservicofluxo_ordem_Visible = 1;
         edtavTfservicofluxo_servicosigla_sel_Jsonclick = "";
         edtavTfservicofluxo_servicosigla_sel_Visible = 1;
         edtavTfservicofluxo_servicosigla_Jsonclick = "";
         edtavTfservicofluxo_servicosigla_Visible = 1;
         edtavTfservicofluxo_servicocod_to_Jsonclick = "";
         edtavTfservicofluxo_servicocod_to_Visible = 1;
         edtavTfservicofluxo_servicocod_Jsonclick = "";
         edtavTfservicofluxo_servicocod_Visible = 1;
         edtavTfservicofluxo_codigo_to_Jsonclick = "";
         edtavTfservicofluxo_codigo_to_Visible = 1;
         edtavTfservicofluxo_codigo_Jsonclick = "";
         edtavTfservicofluxo_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_servicofluxo_srvpossigla_Searchbuttontext = "Pesquisar";
         Ddo_servicofluxo_srvpossigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servicofluxo_srvpossigla_Cleanfilter = "Limpar pesquisa";
         Ddo_servicofluxo_srvpossigla_Loadingdata = "Carregando dados...";
         Ddo_servicofluxo_srvpossigla_Sortdsc = "Ordenar de Z � A";
         Ddo_servicofluxo_srvpossigla_Sortasc = "Ordenar de A � Z";
         Ddo_servicofluxo_srvpossigla_Datalistupdateminimumcharacters = 0;
         Ddo_servicofluxo_srvpossigla_Datalistproc = "GetWWServicoFluxoFilterData";
         Ddo_servicofluxo_srvpossigla_Datalisttype = "Dynamic";
         Ddo_servicofluxo_srvpossigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servicofluxo_srvpossigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servicofluxo_srvpossigla_Filtertype = "Character";
         Ddo_servicofluxo_srvpossigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicofluxo_srvpossigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicofluxo_srvpossigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicofluxo_srvpossigla_Titlecontrolidtoreplace = "";
         Ddo_servicofluxo_srvpossigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicofluxo_srvpossigla_Cls = "ColumnSettings";
         Ddo_servicofluxo_srvpossigla_Tooltip = "Op��es";
         Ddo_servicofluxo_srvpossigla_Caption = "";
         Ddo_servicofluxo_servicopos_Searchbuttontext = "Pesquisar";
         Ddo_servicofluxo_servicopos_Rangefilterto = "At�";
         Ddo_servicofluxo_servicopos_Rangefilterfrom = "Desde";
         Ddo_servicofluxo_servicopos_Cleanfilter = "Limpar pesquisa";
         Ddo_servicofluxo_servicopos_Sortdsc = "Ordenar de Z � A";
         Ddo_servicofluxo_servicopos_Sortasc = "Ordenar de A � Z";
         Ddo_servicofluxo_servicopos_Includedatalist = Convert.ToBoolean( 0);
         Ddo_servicofluxo_servicopos_Filterisrange = Convert.ToBoolean( -1);
         Ddo_servicofluxo_servicopos_Filtertype = "Numeric";
         Ddo_servicofluxo_servicopos_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicofluxo_servicopos_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicofluxo_servicopos_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicofluxo_servicopos_Titlecontrolidtoreplace = "";
         Ddo_servicofluxo_servicopos_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicofluxo_servicopos_Cls = "ColumnSettings";
         Ddo_servicofluxo_servicopos_Tooltip = "Op��es";
         Ddo_servicofluxo_servicopos_Caption = "";
         Ddo_servicofluxo_ordem_Searchbuttontext = "Pesquisar";
         Ddo_servicofluxo_ordem_Rangefilterto = "At�";
         Ddo_servicofluxo_ordem_Rangefilterfrom = "Desde";
         Ddo_servicofluxo_ordem_Cleanfilter = "Limpar pesquisa";
         Ddo_servicofluxo_ordem_Sortdsc = "Ordenar de Z � A";
         Ddo_servicofluxo_ordem_Sortasc = "Ordenar de A � Z";
         Ddo_servicofluxo_ordem_Includedatalist = Convert.ToBoolean( 0);
         Ddo_servicofluxo_ordem_Filterisrange = Convert.ToBoolean( -1);
         Ddo_servicofluxo_ordem_Filtertype = "Numeric";
         Ddo_servicofluxo_ordem_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicofluxo_ordem_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicofluxo_ordem_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicofluxo_ordem_Titlecontrolidtoreplace = "";
         Ddo_servicofluxo_ordem_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicofluxo_ordem_Cls = "ColumnSettings";
         Ddo_servicofluxo_ordem_Tooltip = "Op��es";
         Ddo_servicofluxo_ordem_Caption = "";
         Ddo_servicofluxo_servicotphrq_Searchbuttontext = "Filtrar Selecionados";
         Ddo_servicofluxo_servicotphrq_Cleanfilter = "Limpar pesquisa";
         Ddo_servicofluxo_servicotphrq_Sortdsc = "Ordenar de Z � A";
         Ddo_servicofluxo_servicotphrq_Sortasc = "Ordenar de A � Z";
         Ddo_servicofluxo_servicotphrq_Datalistfixedvalues = "1:Prim�rio,2:Secund�rio";
         Ddo_servicofluxo_servicotphrq_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_servicofluxo_servicotphrq_Datalisttype = "FixedValues";
         Ddo_servicofluxo_servicotphrq_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servicofluxo_servicotphrq_Includefilter = Convert.ToBoolean( 0);
         Ddo_servicofluxo_servicotphrq_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicofluxo_servicotphrq_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicofluxo_servicotphrq_Titlecontrolidtoreplace = "";
         Ddo_servicofluxo_servicotphrq_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicofluxo_servicotphrq_Cls = "ColumnSettings";
         Ddo_servicofluxo_servicotphrq_Tooltip = "Op��es";
         Ddo_servicofluxo_servicotphrq_Caption = "";
         Ddo_servicofluxo_servicosigla_Searchbuttontext = "Pesquisar";
         Ddo_servicofluxo_servicosigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servicofluxo_servicosigla_Cleanfilter = "Limpar pesquisa";
         Ddo_servicofluxo_servicosigla_Loadingdata = "Carregando dados...";
         Ddo_servicofluxo_servicosigla_Sortdsc = "Ordenar de Z � A";
         Ddo_servicofluxo_servicosigla_Sortasc = "Ordenar de A � Z";
         Ddo_servicofluxo_servicosigla_Datalistupdateminimumcharacters = 0;
         Ddo_servicofluxo_servicosigla_Datalistproc = "GetWWServicoFluxoFilterData";
         Ddo_servicofluxo_servicosigla_Datalisttype = "Dynamic";
         Ddo_servicofluxo_servicosigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servicofluxo_servicosigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servicofluxo_servicosigla_Filtertype = "Character";
         Ddo_servicofluxo_servicosigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicofluxo_servicosigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicofluxo_servicosigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicofluxo_servicosigla_Titlecontrolidtoreplace = "";
         Ddo_servicofluxo_servicosigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicofluxo_servicosigla_Cls = "ColumnSettings";
         Ddo_servicofluxo_servicosigla_Tooltip = "Op��es";
         Ddo_servicofluxo_servicosigla_Caption = "";
         Ddo_servicofluxo_servicocod_Searchbuttontext = "Pesquisar";
         Ddo_servicofluxo_servicocod_Rangefilterto = "At�";
         Ddo_servicofluxo_servicocod_Rangefilterfrom = "Desde";
         Ddo_servicofluxo_servicocod_Cleanfilter = "Limpar pesquisa";
         Ddo_servicofluxo_servicocod_Sortdsc = "Ordenar de Z � A";
         Ddo_servicofluxo_servicocod_Sortasc = "Ordenar de A � Z";
         Ddo_servicofluxo_servicocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_servicofluxo_servicocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_servicofluxo_servicocod_Filtertype = "Numeric";
         Ddo_servicofluxo_servicocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicofluxo_servicocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicofluxo_servicocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicofluxo_servicocod_Titlecontrolidtoreplace = "";
         Ddo_servicofluxo_servicocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicofluxo_servicocod_Cls = "ColumnSettings";
         Ddo_servicofluxo_servicocod_Tooltip = "Op��es";
         Ddo_servicofluxo_servicocod_Caption = "";
         Ddo_servicofluxo_codigo_Searchbuttontext = "Pesquisar";
         Ddo_servicofluxo_codigo_Rangefilterto = "At�";
         Ddo_servicofluxo_codigo_Rangefilterfrom = "Desde";
         Ddo_servicofluxo_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_servicofluxo_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_servicofluxo_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_servicofluxo_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_servicofluxo_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_servicofluxo_codigo_Filtertype = "Numeric";
         Ddo_servicofluxo_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicofluxo_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicofluxo_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicofluxo_codigo_Titlecontrolidtoreplace = "";
         Ddo_servicofluxo_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicofluxo_codigo_Cls = "ColumnSettings";
         Ddo_servicofluxo_codigo_Tooltip = "Op��es";
         Ddo_servicofluxo_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Sequ�ncia";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOTPHRQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'AV34TFServicoFluxo_Codigo',fld:'vTFSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFServicoFluxo_Codigo_To',fld:'vTFSERVICOFLUXO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFServicoFluxo_ServicoCod',fld:'vTFSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFServicoFluxo_ServicoCod_To',fld:'vTFSERVICOFLUXO_SERVICOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFServicoFluxo_ServicoSigla',fld:'vTFSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV43TFServicoFluxo_ServicoSigla_Sel',fld:'vTFSERVICOFLUXO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV47TFServicoFluxo_ServicoTpHrq_Sels',fld:'vTFSERVICOFLUXO_SERVICOTPHRQ_SELS',pic:'',nv:null},{av:'AV50TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV54TFServicoFluxo_ServicoPos',fld:'vTFSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'AV55TFServicoFluxo_ServicoPos_To',fld:'vTFSERVICOFLUXO_SERVICOPOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV59TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV33ServicoFluxo_CodigoTitleFilterData',fld:'vSERVICOFLUXO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV37ServicoFluxo_ServicoCodTitleFilterData',fld:'vSERVICOFLUXO_SERVICOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV41ServicoFluxo_ServicoSiglaTitleFilterData',fld:'vSERVICOFLUXO_SERVICOSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV45ServicoFluxo_ServicoTpHrqTitleFilterData',fld:'vSERVICOFLUXO_SERVICOTPHRQTITLEFILTERDATA',pic:'',nv:null},{av:'AV49ServicoFluxo_OrdemTitleFilterData',fld:'vSERVICOFLUXO_ORDEMTITLEFILTERDATA',pic:'',nv:null},{av:'AV53ServicoFluxo_ServicoPosTitleFilterData',fld:'vSERVICOFLUXO_SERVICOPOSTITLEFILTERDATA',pic:'',nv:null},{av:'AV57ServicoFluxo_SrvPosSiglaTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'edtServicoFluxo_Codigo_Titleformat',ctrl:'SERVICOFLUXO_CODIGO',prop:'Titleformat'},{av:'edtServicoFluxo_Codigo_Title',ctrl:'SERVICOFLUXO_CODIGO',prop:'Title'},{av:'edtServicoFluxo_ServicoCod_Titleformat',ctrl:'SERVICOFLUXO_SERVICOCOD',prop:'Titleformat'},{av:'edtServicoFluxo_ServicoCod_Title',ctrl:'SERVICOFLUXO_SERVICOCOD',prop:'Title'},{av:'edtServicoFluxo_ServicoSigla_Titleformat',ctrl:'SERVICOFLUXO_SERVICOSIGLA',prop:'Titleformat'},{av:'edtServicoFluxo_ServicoSigla_Title',ctrl:'SERVICOFLUXO_SERVICOSIGLA',prop:'Title'},{av:'cmbServicoFluxo_ServicoTpHrq'},{av:'edtServicoFluxo_Ordem_Titleformat',ctrl:'SERVICOFLUXO_ORDEM',prop:'Titleformat'},{av:'edtServicoFluxo_Ordem_Title',ctrl:'SERVICOFLUXO_ORDEM',prop:'Title'},{av:'edtServicoFluxo_ServicoPos_Titleformat',ctrl:'SERVICOFLUXO_SERVICOPOS',prop:'Titleformat'},{av:'edtServicoFluxo_ServicoPos_Title',ctrl:'SERVICOFLUXO_SERVICOPOS',prop:'Title'},{av:'edtServicoFluxo_SrvPosSigla_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosSigla_Title',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Title'},{av:'AV63GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11LF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoFluxo_Codigo',fld:'vTFSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFServicoFluxo_Codigo_To',fld:'vTFSERVICOFLUXO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFServicoFluxo_ServicoCod',fld:'vTFSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFServicoFluxo_ServicoCod_To',fld:'vTFSERVICOFLUXO_SERVICOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFServicoFluxo_ServicoSigla',fld:'vTFSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV43TFServicoFluxo_ServicoSigla_Sel',fld:'vTFSERVICOFLUXO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV50TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV54TFServicoFluxo_ServicoPos',fld:'vTFSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'AV55TFServicoFluxo_ServicoPos_To',fld:'vTFSERVICOFLUXO_SERVICOPOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV59TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOTPHRQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47TFServicoFluxo_ServicoTpHrq_Sels',fld:'vTFSERVICOFLUXO_SERVICOTPHRQ_SELS',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SERVICOFLUXO_CODIGO.ONOPTIONCLICKED","{handler:'E12LF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoFluxo_Codigo',fld:'vTFSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFServicoFluxo_Codigo_To',fld:'vTFSERVICOFLUXO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFServicoFluxo_ServicoCod',fld:'vTFSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFServicoFluxo_ServicoCod_To',fld:'vTFSERVICOFLUXO_SERVICOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFServicoFluxo_ServicoSigla',fld:'vTFSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV43TFServicoFluxo_ServicoSigla_Sel',fld:'vTFSERVICOFLUXO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV50TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV54TFServicoFluxo_ServicoPos',fld:'vTFSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'AV55TFServicoFluxo_ServicoPos_To',fld:'vTFSERVICOFLUXO_SERVICOPOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV59TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOTPHRQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47TFServicoFluxo_ServicoTpHrq_Sels',fld:'vTFSERVICOFLUXO_SERVICOTPHRQ_SELS',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_servicofluxo_codigo_Activeeventkey',ctrl:'DDO_SERVICOFLUXO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_servicofluxo_codigo_Filteredtext_get',ctrl:'DDO_SERVICOFLUXO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_servicofluxo_codigo_Filteredtextto_get',ctrl:'DDO_SERVICOFLUXO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicofluxo_codigo_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_CODIGO',prop:'SortedStatus'},{av:'AV34TFServicoFluxo_Codigo',fld:'vTFSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFServicoFluxo_Codigo_To',fld:'vTFSERVICOFLUXO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_servicofluxo_servicocod_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOCOD',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicosigla_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicotphrq_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOTPHRQ',prop:'SortedStatus'},{av:'Ddo_servicofluxo_ordem_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_ORDEM',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicopos_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOPOS',prop:'SortedStatus'},{av:'Ddo_servicofluxo_srvpossigla_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SRVPOSSIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICOFLUXO_SERVICOCOD.ONOPTIONCLICKED","{handler:'E13LF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoFluxo_Codigo',fld:'vTFSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFServicoFluxo_Codigo_To',fld:'vTFSERVICOFLUXO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFServicoFluxo_ServicoCod',fld:'vTFSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFServicoFluxo_ServicoCod_To',fld:'vTFSERVICOFLUXO_SERVICOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFServicoFluxo_ServicoSigla',fld:'vTFSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV43TFServicoFluxo_ServicoSigla_Sel',fld:'vTFSERVICOFLUXO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV50TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV54TFServicoFluxo_ServicoPos',fld:'vTFSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'AV55TFServicoFluxo_ServicoPos_To',fld:'vTFSERVICOFLUXO_SERVICOPOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV59TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOTPHRQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47TFServicoFluxo_ServicoTpHrq_Sels',fld:'vTFSERVICOFLUXO_SERVICOTPHRQ_SELS',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_servicofluxo_servicocod_Activeeventkey',ctrl:'DDO_SERVICOFLUXO_SERVICOCOD',prop:'ActiveEventKey'},{av:'Ddo_servicofluxo_servicocod_Filteredtext_get',ctrl:'DDO_SERVICOFLUXO_SERVICOCOD',prop:'FilteredText_get'},{av:'Ddo_servicofluxo_servicocod_Filteredtextto_get',ctrl:'DDO_SERVICOFLUXO_SERVICOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicofluxo_servicocod_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOCOD',prop:'SortedStatus'},{av:'AV38TFServicoFluxo_ServicoCod',fld:'vTFSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFServicoFluxo_ServicoCod_To',fld:'vTFSERVICOFLUXO_SERVICOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_servicofluxo_codigo_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_CODIGO',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicosigla_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicotphrq_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOTPHRQ',prop:'SortedStatus'},{av:'Ddo_servicofluxo_ordem_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_ORDEM',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicopos_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOPOS',prop:'SortedStatus'},{av:'Ddo_servicofluxo_srvpossigla_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SRVPOSSIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICOFLUXO_SERVICOSIGLA.ONOPTIONCLICKED","{handler:'E14LF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoFluxo_Codigo',fld:'vTFSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFServicoFluxo_Codigo_To',fld:'vTFSERVICOFLUXO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFServicoFluxo_ServicoCod',fld:'vTFSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFServicoFluxo_ServicoCod_To',fld:'vTFSERVICOFLUXO_SERVICOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFServicoFluxo_ServicoSigla',fld:'vTFSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV43TFServicoFluxo_ServicoSigla_Sel',fld:'vTFSERVICOFLUXO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV50TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV54TFServicoFluxo_ServicoPos',fld:'vTFSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'AV55TFServicoFluxo_ServicoPos_To',fld:'vTFSERVICOFLUXO_SERVICOPOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV59TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOTPHRQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47TFServicoFluxo_ServicoTpHrq_Sels',fld:'vTFSERVICOFLUXO_SERVICOTPHRQ_SELS',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_servicofluxo_servicosigla_Activeeventkey',ctrl:'DDO_SERVICOFLUXO_SERVICOSIGLA',prop:'ActiveEventKey'},{av:'Ddo_servicofluxo_servicosigla_Filteredtext_get',ctrl:'DDO_SERVICOFLUXO_SERVICOSIGLA',prop:'FilteredText_get'},{av:'Ddo_servicofluxo_servicosigla_Selectedvalue_get',ctrl:'DDO_SERVICOFLUXO_SERVICOSIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicofluxo_servicosigla_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOSIGLA',prop:'SortedStatus'},{av:'AV42TFServicoFluxo_ServicoSigla',fld:'vTFSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV43TFServicoFluxo_ServicoSigla_Sel',fld:'vTFSERVICOFLUXO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_servicofluxo_codigo_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_CODIGO',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicocod_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOCOD',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicotphrq_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOTPHRQ',prop:'SortedStatus'},{av:'Ddo_servicofluxo_ordem_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_ORDEM',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicopos_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOPOS',prop:'SortedStatus'},{av:'Ddo_servicofluxo_srvpossigla_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SRVPOSSIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICOFLUXO_SERVICOTPHRQ.ONOPTIONCLICKED","{handler:'E15LF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoFluxo_Codigo',fld:'vTFSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFServicoFluxo_Codigo_To',fld:'vTFSERVICOFLUXO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFServicoFluxo_ServicoCod',fld:'vTFSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFServicoFluxo_ServicoCod_To',fld:'vTFSERVICOFLUXO_SERVICOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFServicoFluxo_ServicoSigla',fld:'vTFSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV43TFServicoFluxo_ServicoSigla_Sel',fld:'vTFSERVICOFLUXO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV50TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV54TFServicoFluxo_ServicoPos',fld:'vTFSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'AV55TFServicoFluxo_ServicoPos_To',fld:'vTFSERVICOFLUXO_SERVICOPOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV59TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOTPHRQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47TFServicoFluxo_ServicoTpHrq_Sels',fld:'vTFSERVICOFLUXO_SERVICOTPHRQ_SELS',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_servicofluxo_servicotphrq_Activeeventkey',ctrl:'DDO_SERVICOFLUXO_SERVICOTPHRQ',prop:'ActiveEventKey'},{av:'Ddo_servicofluxo_servicotphrq_Selectedvalue_get',ctrl:'DDO_SERVICOFLUXO_SERVICOTPHRQ',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicofluxo_servicotphrq_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOTPHRQ',prop:'SortedStatus'},{av:'AV47TFServicoFluxo_ServicoTpHrq_Sels',fld:'vTFSERVICOFLUXO_SERVICOTPHRQ_SELS',pic:'',nv:null},{av:'Ddo_servicofluxo_codigo_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_CODIGO',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicocod_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOCOD',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicosigla_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_servicofluxo_ordem_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_ORDEM',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicopos_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOPOS',prop:'SortedStatus'},{av:'Ddo_servicofluxo_srvpossigla_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SRVPOSSIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICOFLUXO_ORDEM.ONOPTIONCLICKED","{handler:'E16LF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoFluxo_Codigo',fld:'vTFSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFServicoFluxo_Codigo_To',fld:'vTFSERVICOFLUXO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFServicoFluxo_ServicoCod',fld:'vTFSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFServicoFluxo_ServicoCod_To',fld:'vTFSERVICOFLUXO_SERVICOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFServicoFluxo_ServicoSigla',fld:'vTFSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV43TFServicoFluxo_ServicoSigla_Sel',fld:'vTFSERVICOFLUXO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV50TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV54TFServicoFluxo_ServicoPos',fld:'vTFSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'AV55TFServicoFluxo_ServicoPos_To',fld:'vTFSERVICOFLUXO_SERVICOPOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV59TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOTPHRQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47TFServicoFluxo_ServicoTpHrq_Sels',fld:'vTFSERVICOFLUXO_SERVICOTPHRQ_SELS',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_servicofluxo_ordem_Activeeventkey',ctrl:'DDO_SERVICOFLUXO_ORDEM',prop:'ActiveEventKey'},{av:'Ddo_servicofluxo_ordem_Filteredtext_get',ctrl:'DDO_SERVICOFLUXO_ORDEM',prop:'FilteredText_get'},{av:'Ddo_servicofluxo_ordem_Filteredtextto_get',ctrl:'DDO_SERVICOFLUXO_ORDEM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicofluxo_ordem_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_ORDEM',prop:'SortedStatus'},{av:'AV50TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'Ddo_servicofluxo_codigo_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_CODIGO',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicocod_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOCOD',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicosigla_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicotphrq_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOTPHRQ',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicopos_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOPOS',prop:'SortedStatus'},{av:'Ddo_servicofluxo_srvpossigla_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SRVPOSSIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICOFLUXO_SERVICOPOS.ONOPTIONCLICKED","{handler:'E17LF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoFluxo_Codigo',fld:'vTFSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFServicoFluxo_Codigo_To',fld:'vTFSERVICOFLUXO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFServicoFluxo_ServicoCod',fld:'vTFSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFServicoFluxo_ServicoCod_To',fld:'vTFSERVICOFLUXO_SERVICOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFServicoFluxo_ServicoSigla',fld:'vTFSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV43TFServicoFluxo_ServicoSigla_Sel',fld:'vTFSERVICOFLUXO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV50TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV54TFServicoFluxo_ServicoPos',fld:'vTFSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'AV55TFServicoFluxo_ServicoPos_To',fld:'vTFSERVICOFLUXO_SERVICOPOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV59TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOTPHRQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47TFServicoFluxo_ServicoTpHrq_Sels',fld:'vTFSERVICOFLUXO_SERVICOTPHRQ_SELS',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_servicofluxo_servicopos_Activeeventkey',ctrl:'DDO_SERVICOFLUXO_SERVICOPOS',prop:'ActiveEventKey'},{av:'Ddo_servicofluxo_servicopos_Filteredtext_get',ctrl:'DDO_SERVICOFLUXO_SERVICOPOS',prop:'FilteredText_get'},{av:'Ddo_servicofluxo_servicopos_Filteredtextto_get',ctrl:'DDO_SERVICOFLUXO_SERVICOPOS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicofluxo_servicopos_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOPOS',prop:'SortedStatus'},{av:'AV54TFServicoFluxo_ServicoPos',fld:'vTFSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'AV55TFServicoFluxo_ServicoPos_To',fld:'vTFSERVICOFLUXO_SERVICOPOS_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_servicofluxo_codigo_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_CODIGO',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicocod_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOCOD',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicosigla_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicotphrq_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOTPHRQ',prop:'SortedStatus'},{av:'Ddo_servicofluxo_ordem_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_ORDEM',prop:'SortedStatus'},{av:'Ddo_servicofluxo_srvpossigla_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SRVPOSSIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICOFLUXO_SRVPOSSIGLA.ONOPTIONCLICKED","{handler:'E18LF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoFluxo_Codigo',fld:'vTFSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFServicoFluxo_Codigo_To',fld:'vTFSERVICOFLUXO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFServicoFluxo_ServicoCod',fld:'vTFSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFServicoFluxo_ServicoCod_To',fld:'vTFSERVICOFLUXO_SERVICOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFServicoFluxo_ServicoSigla',fld:'vTFSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV43TFServicoFluxo_ServicoSigla_Sel',fld:'vTFSERVICOFLUXO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV50TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV54TFServicoFluxo_ServicoPos',fld:'vTFSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'AV55TFServicoFluxo_ServicoPos_To',fld:'vTFSERVICOFLUXO_SERVICOPOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV59TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOTPHRQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47TFServicoFluxo_ServicoTpHrq_Sels',fld:'vTFSERVICOFLUXO_SERVICOTPHRQ_SELS',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_servicofluxo_srvpossigla_Activeeventkey',ctrl:'DDO_SERVICOFLUXO_SRVPOSSIGLA',prop:'ActiveEventKey'},{av:'Ddo_servicofluxo_srvpossigla_Filteredtext_get',ctrl:'DDO_SERVICOFLUXO_SRVPOSSIGLA',prop:'FilteredText_get'},{av:'Ddo_servicofluxo_srvpossigla_Selectedvalue_get',ctrl:'DDO_SERVICOFLUXO_SRVPOSSIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicofluxo_srvpossigla_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SRVPOSSIGLA',prop:'SortedStatus'},{av:'AV58TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV59TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_servicofluxo_codigo_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_CODIGO',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicocod_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOCOD',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicosigla_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicotphrq_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOTPHRQ',prop:'SortedStatus'},{av:'Ddo_servicofluxo_ordem_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_ORDEM',prop:'SortedStatus'},{av:'Ddo_servicofluxo_servicopos_Sortedstatus',ctrl:'DDO_SERVICOFLUXO_SERVICOPOS',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E32LF2',iparms:[{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtServicoFluxo_ServicoSigla_Link',ctrl:'SERVICOFLUXO_SERVICOSIGLA',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E19LF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoFluxo_Codigo',fld:'vTFSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFServicoFluxo_Codigo_To',fld:'vTFSERVICOFLUXO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFServicoFluxo_ServicoCod',fld:'vTFSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFServicoFluxo_ServicoCod_To',fld:'vTFSERVICOFLUXO_SERVICOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFServicoFluxo_ServicoSigla',fld:'vTFSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV43TFServicoFluxo_ServicoSigla_Sel',fld:'vTFSERVICOFLUXO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV50TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV54TFServicoFluxo_ServicoPos',fld:'vTFSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'AV55TFServicoFluxo_ServicoPos_To',fld:'vTFSERVICOFLUXO_SERVICOPOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV59TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOTPHRQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47TFServicoFluxo_ServicoTpHrq_Sels',fld:'vTFSERVICOFLUXO_SERVICOTPHRQ_SELS',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E25LF2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E20LF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoFluxo_Codigo',fld:'vTFSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFServicoFluxo_Codigo_To',fld:'vTFSERVICOFLUXO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFServicoFluxo_ServicoCod',fld:'vTFSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFServicoFluxo_ServicoCod_To',fld:'vTFSERVICOFLUXO_SERVICOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFServicoFluxo_ServicoSigla',fld:'vTFSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV43TFServicoFluxo_ServicoSigla_Sel',fld:'vTFSERVICOFLUXO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV50TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV54TFServicoFluxo_ServicoPos',fld:'vTFSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'AV55TFServicoFluxo_ServicoPos_To',fld:'vTFSERVICOFLUXO_SERVICOPOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV59TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOTPHRQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47TFServicoFluxo_ServicoTpHrq_Sels',fld:'vTFSERVICOFLUXO_SERVICOTPHRQ_SELS',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavServicofluxo_servicosigla2_Visible',ctrl:'vSERVICOFLUXO_SERVICOSIGLA2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServicofluxo_servicosigla3_Visible',ctrl:'vSERVICOFLUXO_SERVICOSIGLA3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavServicofluxo_servicosigla1_Visible',ctrl:'vSERVICOFLUXO_SERVICOSIGLA1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E26LF2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavServicofluxo_servicosigla1_Visible',ctrl:'vSERVICOFLUXO_SERVICOSIGLA1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E27LF2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E21LF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoFluxo_Codigo',fld:'vTFSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFServicoFluxo_Codigo_To',fld:'vTFSERVICOFLUXO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFServicoFluxo_ServicoCod',fld:'vTFSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFServicoFluxo_ServicoCod_To',fld:'vTFSERVICOFLUXO_SERVICOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFServicoFluxo_ServicoSigla',fld:'vTFSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV43TFServicoFluxo_ServicoSigla_Sel',fld:'vTFSERVICOFLUXO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV50TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV54TFServicoFluxo_ServicoPos',fld:'vTFSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'AV55TFServicoFluxo_ServicoPos_To',fld:'vTFSERVICOFLUXO_SERVICOPOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV59TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOTPHRQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47TFServicoFluxo_ServicoTpHrq_Sels',fld:'vTFSERVICOFLUXO_SERVICOTPHRQ_SELS',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavServicofluxo_servicosigla2_Visible',ctrl:'vSERVICOFLUXO_SERVICOSIGLA2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServicofluxo_servicosigla3_Visible',ctrl:'vSERVICOFLUXO_SERVICOSIGLA3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavServicofluxo_servicosigla1_Visible',ctrl:'vSERVICOFLUXO_SERVICOSIGLA1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E28LF2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavServicofluxo_servicosigla2_Visible',ctrl:'vSERVICOFLUXO_SERVICOSIGLA2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E22LF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoFluxo_Codigo',fld:'vTFSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFServicoFluxo_Codigo_To',fld:'vTFSERVICOFLUXO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFServicoFluxo_ServicoCod',fld:'vTFSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFServicoFluxo_ServicoCod_To',fld:'vTFSERVICOFLUXO_SERVICOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFServicoFluxo_ServicoSigla',fld:'vTFSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV43TFServicoFluxo_ServicoSigla_Sel',fld:'vTFSERVICOFLUXO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV50TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV54TFServicoFluxo_ServicoPos',fld:'vTFSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'AV55TFServicoFluxo_ServicoPos_To',fld:'vTFSERVICOFLUXO_SERVICOPOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV59TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOTPHRQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47TFServicoFluxo_ServicoTpHrq_Sels',fld:'vTFSERVICOFLUXO_SERVICOTPHRQ_SELS',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavServicofluxo_servicosigla2_Visible',ctrl:'vSERVICOFLUXO_SERVICOSIGLA2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServicofluxo_servicosigla3_Visible',ctrl:'vSERVICOFLUXO_SERVICOSIGLA3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavServicofluxo_servicosigla1_Visible',ctrl:'vSERVICOFLUXO_SERVICOSIGLA1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E29LF2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavServicofluxo_servicosigla3_Visible',ctrl:'vSERVICOFLUXO_SERVICOSIGLA3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E23LF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFServicoFluxo_Codigo',fld:'vTFSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFServicoFluxo_Codigo_To',fld:'vTFSERVICOFLUXO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFServicoFluxo_ServicoCod',fld:'vTFSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFServicoFluxo_ServicoCod_To',fld:'vTFSERVICOFLUXO_SERVICOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFServicoFluxo_ServicoSigla',fld:'vTFSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV43TFServicoFluxo_ServicoSigla_Sel',fld:'vTFSERVICOFLUXO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV50TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV54TFServicoFluxo_ServicoPos',fld:'vTFSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'AV55TFServicoFluxo_ServicoPos_To',fld:'vTFSERVICOFLUXO_SERVICOPOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV59TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOTPHRQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SERVICOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47TFServicoFluxo_ServicoTpHrq_Sels',fld:'vTFSERVICOFLUXO_SERVICOTPHRQ_SELS',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV34TFServicoFluxo_Codigo',fld:'vTFSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_servicofluxo_codigo_Filteredtext_set',ctrl:'DDO_SERVICOFLUXO_CODIGO',prop:'FilteredText_set'},{av:'AV35TFServicoFluxo_Codigo_To',fld:'vTFSERVICOFLUXO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_servicofluxo_codigo_Filteredtextto_set',ctrl:'DDO_SERVICOFLUXO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV38TFServicoFluxo_ServicoCod',fld:'vTFSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_servicofluxo_servicocod_Filteredtext_set',ctrl:'DDO_SERVICOFLUXO_SERVICOCOD',prop:'FilteredText_set'},{av:'AV39TFServicoFluxo_ServicoCod_To',fld:'vTFSERVICOFLUXO_SERVICOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_servicofluxo_servicocod_Filteredtextto_set',ctrl:'DDO_SERVICOFLUXO_SERVICOCOD',prop:'FilteredTextTo_set'},{av:'AV42TFServicoFluxo_ServicoSigla',fld:'vTFSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''},{av:'Ddo_servicofluxo_servicosigla_Filteredtext_set',ctrl:'DDO_SERVICOFLUXO_SERVICOSIGLA',prop:'FilteredText_set'},{av:'AV43TFServicoFluxo_ServicoSigla_Sel',fld:'vTFSERVICOFLUXO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_servicofluxo_servicosigla_Selectedvalue_set',ctrl:'DDO_SERVICOFLUXO_SERVICOSIGLA',prop:'SelectedValue_set'},{av:'AV47TFServicoFluxo_ServicoTpHrq_Sels',fld:'vTFSERVICOFLUXO_SERVICOTPHRQ_SELS',pic:'',nv:null},{av:'Ddo_servicofluxo_servicotphrq_Selectedvalue_set',ctrl:'DDO_SERVICOFLUXO_SERVICOTPHRQ',prop:'SelectedValue_set'},{av:'AV50TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'Ddo_servicofluxo_ordem_Filteredtext_set',ctrl:'DDO_SERVICOFLUXO_ORDEM',prop:'FilteredText_set'},{av:'AV51TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'Ddo_servicofluxo_ordem_Filteredtextto_set',ctrl:'DDO_SERVICOFLUXO_ORDEM',prop:'FilteredTextTo_set'},{av:'AV54TFServicoFluxo_ServicoPos',fld:'vTFSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'Ddo_servicofluxo_servicopos_Filteredtext_set',ctrl:'DDO_SERVICOFLUXO_SERVICOPOS',prop:'FilteredText_set'},{av:'AV55TFServicoFluxo_ServicoPos_To',fld:'vTFSERVICOFLUXO_SERVICOPOS_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_servicofluxo_servicopos_Filteredtextto_set',ctrl:'DDO_SERVICOFLUXO_SERVICOPOS',prop:'FilteredTextTo_set'},{av:'AV58TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'Ddo_servicofluxo_srvpossigla_Filteredtext_set',ctrl:'DDO_SERVICOFLUXO_SRVPOSSIGLA',prop:'FilteredText_set'},{av:'AV59TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_servicofluxo_srvpossigla_Selectedvalue_set',ctrl:'DDO_SERVICOFLUXO_SRVPOSSIGLA',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoFluxo_ServicoSigla1',fld:'vSERVICOFLUXO_SERVICOSIGLA1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavServicofluxo_servicosigla1_Visible',ctrl:'vSERVICOFLUXO_SERVICOSIGLA1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ServicoFluxo_ServicoSigla2',fld:'vSERVICOFLUXO_SERVICOSIGLA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ServicoFluxo_ServicoSigla3',fld:'vSERVICOFLUXO_SERVICOSIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavServicofluxo_servicosigla2_Visible',ctrl:'vSERVICOFLUXO_SERVICOSIGLA2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServicofluxo_servicosigla3_Visible',ctrl:'vSERVICOFLUXO_SERVICOSIGLA3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E24LF2',iparms:[{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_servicofluxo_codigo_Activeeventkey = "";
         Ddo_servicofluxo_codigo_Filteredtext_get = "";
         Ddo_servicofluxo_codigo_Filteredtextto_get = "";
         Ddo_servicofluxo_servicocod_Activeeventkey = "";
         Ddo_servicofluxo_servicocod_Filteredtext_get = "";
         Ddo_servicofluxo_servicocod_Filteredtextto_get = "";
         Ddo_servicofluxo_servicosigla_Activeeventkey = "";
         Ddo_servicofluxo_servicosigla_Filteredtext_get = "";
         Ddo_servicofluxo_servicosigla_Selectedvalue_get = "";
         Ddo_servicofluxo_servicotphrq_Activeeventkey = "";
         Ddo_servicofluxo_servicotphrq_Selectedvalue_get = "";
         Ddo_servicofluxo_ordem_Activeeventkey = "";
         Ddo_servicofluxo_ordem_Filteredtext_get = "";
         Ddo_servicofluxo_ordem_Filteredtextto_get = "";
         Ddo_servicofluxo_servicopos_Activeeventkey = "";
         Ddo_servicofluxo_servicopos_Filteredtext_get = "";
         Ddo_servicofluxo_servicopos_Filteredtextto_get = "";
         Ddo_servicofluxo_srvpossigla_Activeeventkey = "";
         Ddo_servicofluxo_srvpossigla_Filteredtext_get = "";
         Ddo_servicofluxo_srvpossigla_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17ServicoFluxo_ServicoSigla1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21ServicoFluxo_ServicoSigla2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25ServicoFluxo_ServicoSigla3 = "";
         AV42TFServicoFluxo_ServicoSigla = "";
         AV43TFServicoFluxo_ServicoSigla_Sel = "";
         AV58TFServicoFluxo_SrvPosSigla = "";
         AV59TFServicoFluxo_SrvPosSigla_Sel = "";
         AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace = "";
         AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace = "";
         AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace = "";
         AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace = "";
         AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace = "";
         AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace = "";
         AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace = "";
         AV47TFServicoFluxo_ServicoTpHrq_Sels = new GxSimpleCollection();
         AV93Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV61DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV33ServicoFluxo_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37ServicoFluxo_ServicoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41ServicoFluxo_ServicoSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45ServicoFluxo_ServicoTpHrqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49ServicoFluxo_OrdemTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53ServicoFluxo_ServicoPosTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV57ServicoFluxo_SrvPosSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_servicofluxo_codigo_Filteredtext_set = "";
         Ddo_servicofluxo_codigo_Filteredtextto_set = "";
         Ddo_servicofluxo_codigo_Sortedstatus = "";
         Ddo_servicofluxo_servicocod_Filteredtext_set = "";
         Ddo_servicofluxo_servicocod_Filteredtextto_set = "";
         Ddo_servicofluxo_servicocod_Sortedstatus = "";
         Ddo_servicofluxo_servicosigla_Filteredtext_set = "";
         Ddo_servicofluxo_servicosigla_Selectedvalue_set = "";
         Ddo_servicofluxo_servicosigla_Sortedstatus = "";
         Ddo_servicofluxo_servicotphrq_Selectedvalue_set = "";
         Ddo_servicofluxo_servicotphrq_Sortedstatus = "";
         Ddo_servicofluxo_ordem_Filteredtext_set = "";
         Ddo_servicofluxo_ordem_Filteredtextto_set = "";
         Ddo_servicofluxo_ordem_Sortedstatus = "";
         Ddo_servicofluxo_servicopos_Filteredtext_set = "";
         Ddo_servicofluxo_servicopos_Filteredtextto_set = "";
         Ddo_servicofluxo_servicopos_Sortedstatus = "";
         Ddo_servicofluxo_srvpossigla_Filteredtext_set = "";
         Ddo_servicofluxo_srvpossigla_Selectedvalue_set = "";
         Ddo_servicofluxo_srvpossigla_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV91Update_GXI = "";
         AV29Delete = "";
         AV92Delete_GXI = "";
         A1523ServicoFluxo_ServicoSigla = "";
         A1527ServicoFluxo_SrvPosSigla = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 = "";
         lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 = "";
         lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 = "";
         lV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla = "";
         lV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla = "";
         AV67WWServicoFluxoDS_1_Dynamicfiltersselector1 = "";
         AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 = "";
         AV71WWServicoFluxoDS_5_Dynamicfiltersselector2 = "";
         AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 = "";
         AV75WWServicoFluxoDS_9_Dynamicfiltersselector3 = "";
         AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 = "";
         AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel = "";
         AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla = "";
         AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel = "";
         AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla = "";
         H00LF2_A1527ServicoFluxo_SrvPosSigla = new String[] {""} ;
         H00LF2_n1527ServicoFluxo_SrvPosSigla = new bool[] {false} ;
         H00LF2_A1526ServicoFluxo_ServicoPos = new int[1] ;
         H00LF2_n1526ServicoFluxo_ServicoPos = new bool[] {false} ;
         H00LF2_A1532ServicoFluxo_Ordem = new short[1] ;
         H00LF2_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         H00LF2_A1533ServicoFluxo_ServicoTpHrq = new short[1] ;
         H00LF2_n1533ServicoFluxo_ServicoTpHrq = new bool[] {false} ;
         H00LF2_A1523ServicoFluxo_ServicoSigla = new String[] {""} ;
         H00LF2_n1523ServicoFluxo_ServicoSigla = new bool[] {false} ;
         H00LF2_A1522ServicoFluxo_ServicoCod = new int[1] ;
         H00LF2_A1528ServicoFluxo_Codigo = new int[1] ;
         H00LF3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV46TFServicoFluxo_ServicoTpHrq_SelsJson = "";
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblServicofluxotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwservicofluxo__default(),
            new Object[][] {
                new Object[] {
               H00LF2_A1527ServicoFluxo_SrvPosSigla, H00LF2_n1527ServicoFluxo_SrvPosSigla, H00LF2_A1526ServicoFluxo_ServicoPos, H00LF2_n1526ServicoFluxo_ServicoPos, H00LF2_A1532ServicoFluxo_Ordem, H00LF2_n1532ServicoFluxo_Ordem, H00LF2_A1533ServicoFluxo_ServicoTpHrq, H00LF2_n1533ServicoFluxo_ServicoTpHrq, H00LF2_A1523ServicoFluxo_ServicoSigla, H00LF2_n1523ServicoFluxo_ServicoSigla,
               H00LF2_A1522ServicoFluxo_ServicoCod, H00LF2_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               H00LF3_AGRID_nRecordCount
               }
            }
         );
         AV93Pgmname = "WWServicoFluxo";
         /* GeneXus formulas. */
         AV93Pgmname = "WWServicoFluxo";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_88 ;
      private short nGXsfl_88_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short AV50TFServicoFluxo_Ordem ;
      private short AV51TFServicoFluxo_Ordem_To ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A1533ServicoFluxo_ServicoTpHrq ;
      private short A1532ServicoFluxo_Ordem ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_88_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV68WWServicoFluxoDS_2_Dynamicfiltersoperator1 ;
      private short AV72WWServicoFluxoDS_6_Dynamicfiltersoperator2 ;
      private short AV76WWServicoFluxoDS_10_Dynamicfiltersoperator3 ;
      private short AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem ;
      private short AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to ;
      private short edtServicoFluxo_Codigo_Titleformat ;
      private short edtServicoFluxo_ServicoCod_Titleformat ;
      private short edtServicoFluxo_ServicoSigla_Titleformat ;
      private short cmbServicoFluxo_ServicoTpHrq_Titleformat ;
      private short edtServicoFluxo_Ordem_Titleformat ;
      private short edtServicoFluxo_ServicoPos_Titleformat ;
      private short edtServicoFluxo_SrvPosSigla_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV34TFServicoFluxo_Codigo ;
      private int AV35TFServicoFluxo_Codigo_To ;
      private int AV38TFServicoFluxo_ServicoCod ;
      private int AV39TFServicoFluxo_ServicoCod_To ;
      private int AV54TFServicoFluxo_ServicoPos ;
      private int AV55TFServicoFluxo_ServicoPos_To ;
      private int A1528ServicoFluxo_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_servicofluxo_servicosigla_Datalistupdateminimumcharacters ;
      private int Ddo_servicofluxo_srvpossigla_Datalistupdateminimumcharacters ;
      private int edtavTfservicofluxo_codigo_Visible ;
      private int edtavTfservicofluxo_codigo_to_Visible ;
      private int edtavTfservicofluxo_servicocod_Visible ;
      private int edtavTfservicofluxo_servicocod_to_Visible ;
      private int edtavTfservicofluxo_servicosigla_Visible ;
      private int edtavTfservicofluxo_servicosigla_sel_Visible ;
      private int edtavTfservicofluxo_ordem_Visible ;
      private int edtavTfservicofluxo_ordem_to_Visible ;
      private int edtavTfservicofluxo_servicopos_Visible ;
      private int edtavTfservicofluxo_servicopos_to_Visible ;
      private int edtavTfservicofluxo_srvpossigla_Visible ;
      private int edtavTfservicofluxo_srvpossigla_sel_Visible ;
      private int edtavDdo_servicofluxo_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servicofluxo_servicocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servicofluxo_servicosiglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servicofluxo_servicotphrqtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servicofluxo_servicopostitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Visible ;
      private int A1522ServicoFluxo_ServicoCod ;
      private int A1526ServicoFluxo_ServicoPos ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels_Count ;
      private int AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo ;
      private int AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to ;
      private int AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod ;
      private int AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to ;
      private int AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos ;
      private int AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV62PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavServicofluxo_servicosigla1_Visible ;
      private int edtavServicofluxo_servicosigla2_Visible ;
      private int edtavServicofluxo_servicosigla3_Visible ;
      private int AV94GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV63GridCurrentPage ;
      private long AV64GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_servicofluxo_codigo_Activeeventkey ;
      private String Ddo_servicofluxo_codigo_Filteredtext_get ;
      private String Ddo_servicofluxo_codigo_Filteredtextto_get ;
      private String Ddo_servicofluxo_servicocod_Activeeventkey ;
      private String Ddo_servicofluxo_servicocod_Filteredtext_get ;
      private String Ddo_servicofluxo_servicocod_Filteredtextto_get ;
      private String Ddo_servicofluxo_servicosigla_Activeeventkey ;
      private String Ddo_servicofluxo_servicosigla_Filteredtext_get ;
      private String Ddo_servicofluxo_servicosigla_Selectedvalue_get ;
      private String Ddo_servicofluxo_servicotphrq_Activeeventkey ;
      private String Ddo_servicofluxo_servicotphrq_Selectedvalue_get ;
      private String Ddo_servicofluxo_ordem_Activeeventkey ;
      private String Ddo_servicofluxo_ordem_Filteredtext_get ;
      private String Ddo_servicofluxo_ordem_Filteredtextto_get ;
      private String Ddo_servicofluxo_servicopos_Activeeventkey ;
      private String Ddo_servicofluxo_servicopos_Filteredtext_get ;
      private String Ddo_servicofluxo_servicopos_Filteredtextto_get ;
      private String Ddo_servicofluxo_srvpossigla_Activeeventkey ;
      private String Ddo_servicofluxo_srvpossigla_Filteredtext_get ;
      private String Ddo_servicofluxo_srvpossigla_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_88_idx="0001" ;
      private String AV17ServicoFluxo_ServicoSigla1 ;
      private String AV21ServicoFluxo_ServicoSigla2 ;
      private String AV25ServicoFluxo_ServicoSigla3 ;
      private String AV42TFServicoFluxo_ServicoSigla ;
      private String AV43TFServicoFluxo_ServicoSigla_Sel ;
      private String AV58TFServicoFluxo_SrvPosSigla ;
      private String AV59TFServicoFluxo_SrvPosSigla_Sel ;
      private String AV93Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_servicofluxo_codigo_Caption ;
      private String Ddo_servicofluxo_codigo_Tooltip ;
      private String Ddo_servicofluxo_codigo_Cls ;
      private String Ddo_servicofluxo_codigo_Filteredtext_set ;
      private String Ddo_servicofluxo_codigo_Filteredtextto_set ;
      private String Ddo_servicofluxo_codigo_Dropdownoptionstype ;
      private String Ddo_servicofluxo_codigo_Titlecontrolidtoreplace ;
      private String Ddo_servicofluxo_codigo_Sortedstatus ;
      private String Ddo_servicofluxo_codigo_Filtertype ;
      private String Ddo_servicofluxo_codigo_Sortasc ;
      private String Ddo_servicofluxo_codigo_Sortdsc ;
      private String Ddo_servicofluxo_codigo_Cleanfilter ;
      private String Ddo_servicofluxo_codigo_Rangefilterfrom ;
      private String Ddo_servicofluxo_codigo_Rangefilterto ;
      private String Ddo_servicofluxo_codigo_Searchbuttontext ;
      private String Ddo_servicofluxo_servicocod_Caption ;
      private String Ddo_servicofluxo_servicocod_Tooltip ;
      private String Ddo_servicofluxo_servicocod_Cls ;
      private String Ddo_servicofluxo_servicocod_Filteredtext_set ;
      private String Ddo_servicofluxo_servicocod_Filteredtextto_set ;
      private String Ddo_servicofluxo_servicocod_Dropdownoptionstype ;
      private String Ddo_servicofluxo_servicocod_Titlecontrolidtoreplace ;
      private String Ddo_servicofluxo_servicocod_Sortedstatus ;
      private String Ddo_servicofluxo_servicocod_Filtertype ;
      private String Ddo_servicofluxo_servicocod_Sortasc ;
      private String Ddo_servicofluxo_servicocod_Sortdsc ;
      private String Ddo_servicofluxo_servicocod_Cleanfilter ;
      private String Ddo_servicofluxo_servicocod_Rangefilterfrom ;
      private String Ddo_servicofluxo_servicocod_Rangefilterto ;
      private String Ddo_servicofluxo_servicocod_Searchbuttontext ;
      private String Ddo_servicofluxo_servicosigla_Caption ;
      private String Ddo_servicofluxo_servicosigla_Tooltip ;
      private String Ddo_servicofluxo_servicosigla_Cls ;
      private String Ddo_servicofluxo_servicosigla_Filteredtext_set ;
      private String Ddo_servicofluxo_servicosigla_Selectedvalue_set ;
      private String Ddo_servicofluxo_servicosigla_Dropdownoptionstype ;
      private String Ddo_servicofluxo_servicosigla_Titlecontrolidtoreplace ;
      private String Ddo_servicofluxo_servicosigla_Sortedstatus ;
      private String Ddo_servicofluxo_servicosigla_Filtertype ;
      private String Ddo_servicofluxo_servicosigla_Datalisttype ;
      private String Ddo_servicofluxo_servicosigla_Datalistproc ;
      private String Ddo_servicofluxo_servicosigla_Sortasc ;
      private String Ddo_servicofluxo_servicosigla_Sortdsc ;
      private String Ddo_servicofluxo_servicosigla_Loadingdata ;
      private String Ddo_servicofluxo_servicosigla_Cleanfilter ;
      private String Ddo_servicofluxo_servicosigla_Noresultsfound ;
      private String Ddo_servicofluxo_servicosigla_Searchbuttontext ;
      private String Ddo_servicofluxo_servicotphrq_Caption ;
      private String Ddo_servicofluxo_servicotphrq_Tooltip ;
      private String Ddo_servicofluxo_servicotphrq_Cls ;
      private String Ddo_servicofluxo_servicotphrq_Selectedvalue_set ;
      private String Ddo_servicofluxo_servicotphrq_Dropdownoptionstype ;
      private String Ddo_servicofluxo_servicotphrq_Titlecontrolidtoreplace ;
      private String Ddo_servicofluxo_servicotphrq_Sortedstatus ;
      private String Ddo_servicofluxo_servicotphrq_Datalisttype ;
      private String Ddo_servicofluxo_servicotphrq_Datalistfixedvalues ;
      private String Ddo_servicofluxo_servicotphrq_Sortasc ;
      private String Ddo_servicofluxo_servicotphrq_Sortdsc ;
      private String Ddo_servicofluxo_servicotphrq_Cleanfilter ;
      private String Ddo_servicofluxo_servicotphrq_Searchbuttontext ;
      private String Ddo_servicofluxo_ordem_Caption ;
      private String Ddo_servicofluxo_ordem_Tooltip ;
      private String Ddo_servicofluxo_ordem_Cls ;
      private String Ddo_servicofluxo_ordem_Filteredtext_set ;
      private String Ddo_servicofluxo_ordem_Filteredtextto_set ;
      private String Ddo_servicofluxo_ordem_Dropdownoptionstype ;
      private String Ddo_servicofluxo_ordem_Titlecontrolidtoreplace ;
      private String Ddo_servicofluxo_ordem_Sortedstatus ;
      private String Ddo_servicofluxo_ordem_Filtertype ;
      private String Ddo_servicofluxo_ordem_Sortasc ;
      private String Ddo_servicofluxo_ordem_Sortdsc ;
      private String Ddo_servicofluxo_ordem_Cleanfilter ;
      private String Ddo_servicofluxo_ordem_Rangefilterfrom ;
      private String Ddo_servicofluxo_ordem_Rangefilterto ;
      private String Ddo_servicofluxo_ordem_Searchbuttontext ;
      private String Ddo_servicofluxo_servicopos_Caption ;
      private String Ddo_servicofluxo_servicopos_Tooltip ;
      private String Ddo_servicofluxo_servicopos_Cls ;
      private String Ddo_servicofluxo_servicopos_Filteredtext_set ;
      private String Ddo_servicofluxo_servicopos_Filteredtextto_set ;
      private String Ddo_servicofluxo_servicopos_Dropdownoptionstype ;
      private String Ddo_servicofluxo_servicopos_Titlecontrolidtoreplace ;
      private String Ddo_servicofluxo_servicopos_Sortedstatus ;
      private String Ddo_servicofluxo_servicopos_Filtertype ;
      private String Ddo_servicofluxo_servicopos_Sortasc ;
      private String Ddo_servicofluxo_servicopos_Sortdsc ;
      private String Ddo_servicofluxo_servicopos_Cleanfilter ;
      private String Ddo_servicofluxo_servicopos_Rangefilterfrom ;
      private String Ddo_servicofluxo_servicopos_Rangefilterto ;
      private String Ddo_servicofluxo_servicopos_Searchbuttontext ;
      private String Ddo_servicofluxo_srvpossigla_Caption ;
      private String Ddo_servicofluxo_srvpossigla_Tooltip ;
      private String Ddo_servicofluxo_srvpossigla_Cls ;
      private String Ddo_servicofluxo_srvpossigla_Filteredtext_set ;
      private String Ddo_servicofluxo_srvpossigla_Selectedvalue_set ;
      private String Ddo_servicofluxo_srvpossigla_Dropdownoptionstype ;
      private String Ddo_servicofluxo_srvpossigla_Titlecontrolidtoreplace ;
      private String Ddo_servicofluxo_srvpossigla_Sortedstatus ;
      private String Ddo_servicofluxo_srvpossigla_Filtertype ;
      private String Ddo_servicofluxo_srvpossigla_Datalisttype ;
      private String Ddo_servicofluxo_srvpossigla_Datalistproc ;
      private String Ddo_servicofluxo_srvpossigla_Sortasc ;
      private String Ddo_servicofluxo_srvpossigla_Sortdsc ;
      private String Ddo_servicofluxo_srvpossigla_Loadingdata ;
      private String Ddo_servicofluxo_srvpossigla_Cleanfilter ;
      private String Ddo_servicofluxo_srvpossigla_Noresultsfound ;
      private String Ddo_servicofluxo_srvpossigla_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfservicofluxo_codigo_Internalname ;
      private String edtavTfservicofluxo_codigo_Jsonclick ;
      private String edtavTfservicofluxo_codigo_to_Internalname ;
      private String edtavTfservicofluxo_codigo_to_Jsonclick ;
      private String edtavTfservicofluxo_servicocod_Internalname ;
      private String edtavTfservicofluxo_servicocod_Jsonclick ;
      private String edtavTfservicofluxo_servicocod_to_Internalname ;
      private String edtavTfservicofluxo_servicocod_to_Jsonclick ;
      private String edtavTfservicofluxo_servicosigla_Internalname ;
      private String edtavTfservicofluxo_servicosigla_Jsonclick ;
      private String edtavTfservicofluxo_servicosigla_sel_Internalname ;
      private String edtavTfservicofluxo_servicosigla_sel_Jsonclick ;
      private String edtavTfservicofluxo_ordem_Internalname ;
      private String edtavTfservicofluxo_ordem_Jsonclick ;
      private String edtavTfservicofluxo_ordem_to_Internalname ;
      private String edtavTfservicofluxo_ordem_to_Jsonclick ;
      private String edtavTfservicofluxo_servicopos_Internalname ;
      private String edtavTfservicofluxo_servicopos_Jsonclick ;
      private String edtavTfservicofluxo_servicopos_to_Internalname ;
      private String edtavTfservicofluxo_servicopos_to_Jsonclick ;
      private String edtavTfservicofluxo_srvpossigla_Internalname ;
      private String edtavTfservicofluxo_srvpossigla_Jsonclick ;
      private String edtavTfservicofluxo_srvpossigla_sel_Internalname ;
      private String edtavTfservicofluxo_srvpossigla_sel_Jsonclick ;
      private String edtavDdo_servicofluxo_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servicofluxo_servicocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servicofluxo_servicosiglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servicofluxo_servicotphrqtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servicofluxo_servicopostitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtServicoFluxo_Codigo_Internalname ;
      private String edtServicoFluxo_ServicoCod_Internalname ;
      private String A1523ServicoFluxo_ServicoSigla ;
      private String edtServicoFluxo_ServicoSigla_Internalname ;
      private String cmbServicoFluxo_ServicoTpHrq_Internalname ;
      private String edtServicoFluxo_Ordem_Internalname ;
      private String edtServicoFluxo_ServicoPos_Internalname ;
      private String A1527ServicoFluxo_SrvPosSigla ;
      private String edtServicoFluxo_SrvPosSigla_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 ;
      private String lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 ;
      private String lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 ;
      private String lV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla ;
      private String lV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla ;
      private String AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 ;
      private String AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 ;
      private String AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 ;
      private String AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel ;
      private String AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla ;
      private String AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel ;
      private String AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavServicofluxo_servicosigla1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavServicofluxo_servicosigla2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavServicofluxo_servicosigla3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_servicofluxo_codigo_Internalname ;
      private String Ddo_servicofluxo_servicocod_Internalname ;
      private String Ddo_servicofluxo_servicosigla_Internalname ;
      private String Ddo_servicofluxo_servicotphrq_Internalname ;
      private String Ddo_servicofluxo_ordem_Internalname ;
      private String Ddo_servicofluxo_servicopos_Internalname ;
      private String Ddo_servicofluxo_srvpossigla_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtServicoFluxo_Codigo_Title ;
      private String edtServicoFluxo_ServicoCod_Title ;
      private String edtServicoFluxo_ServicoSigla_Title ;
      private String edtServicoFluxo_Ordem_Title ;
      private String edtServicoFluxo_ServicoPos_Title ;
      private String edtServicoFluxo_SrvPosSigla_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtServicoFluxo_ServicoSigla_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblServicofluxotitle_Internalname ;
      private String lblServicofluxotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavServicofluxo_servicosigla3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavServicofluxo_servicosigla2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavServicofluxo_servicosigla1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_88_fel_idx="0001" ;
      private String ROClassString ;
      private String edtServicoFluxo_Codigo_Jsonclick ;
      private String edtServicoFluxo_ServicoCod_Jsonclick ;
      private String edtServicoFluxo_ServicoSigla_Jsonclick ;
      private String cmbServicoFluxo_ServicoTpHrq_Jsonclick ;
      private String edtServicoFluxo_Ordem_Jsonclick ;
      private String edtServicoFluxo_ServicoPos_Jsonclick ;
      private String edtServicoFluxo_SrvPosSigla_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_servicofluxo_codigo_Includesortasc ;
      private bool Ddo_servicofluxo_codigo_Includesortdsc ;
      private bool Ddo_servicofluxo_codigo_Includefilter ;
      private bool Ddo_servicofluxo_codigo_Filterisrange ;
      private bool Ddo_servicofluxo_codigo_Includedatalist ;
      private bool Ddo_servicofluxo_servicocod_Includesortasc ;
      private bool Ddo_servicofluxo_servicocod_Includesortdsc ;
      private bool Ddo_servicofluxo_servicocod_Includefilter ;
      private bool Ddo_servicofluxo_servicocod_Filterisrange ;
      private bool Ddo_servicofluxo_servicocod_Includedatalist ;
      private bool Ddo_servicofluxo_servicosigla_Includesortasc ;
      private bool Ddo_servicofluxo_servicosigla_Includesortdsc ;
      private bool Ddo_servicofluxo_servicosigla_Includefilter ;
      private bool Ddo_servicofluxo_servicosigla_Filterisrange ;
      private bool Ddo_servicofluxo_servicosigla_Includedatalist ;
      private bool Ddo_servicofluxo_servicotphrq_Includesortasc ;
      private bool Ddo_servicofluxo_servicotphrq_Includesortdsc ;
      private bool Ddo_servicofluxo_servicotphrq_Includefilter ;
      private bool Ddo_servicofluxo_servicotphrq_Includedatalist ;
      private bool Ddo_servicofluxo_servicotphrq_Allowmultipleselection ;
      private bool Ddo_servicofluxo_ordem_Includesortasc ;
      private bool Ddo_servicofluxo_ordem_Includesortdsc ;
      private bool Ddo_servicofluxo_ordem_Includefilter ;
      private bool Ddo_servicofluxo_ordem_Filterisrange ;
      private bool Ddo_servicofluxo_ordem_Includedatalist ;
      private bool Ddo_servicofluxo_servicopos_Includesortasc ;
      private bool Ddo_servicofluxo_servicopos_Includesortdsc ;
      private bool Ddo_servicofluxo_servicopos_Includefilter ;
      private bool Ddo_servicofluxo_servicopos_Filterisrange ;
      private bool Ddo_servicofluxo_servicopos_Includedatalist ;
      private bool Ddo_servicofluxo_srvpossigla_Includesortasc ;
      private bool Ddo_servicofluxo_srvpossigla_Includesortdsc ;
      private bool Ddo_servicofluxo_srvpossigla_Includefilter ;
      private bool Ddo_servicofluxo_srvpossigla_Filterisrange ;
      private bool Ddo_servicofluxo_srvpossigla_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1523ServicoFluxo_ServicoSigla ;
      private bool n1533ServicoFluxo_ServicoTpHrq ;
      private bool n1532ServicoFluxo_Ordem ;
      private bool n1526ServicoFluxo_ServicoPos ;
      private bool n1527ServicoFluxo_SrvPosSigla ;
      private bool AV70WWServicoFluxoDS_4_Dynamicfiltersenabled2 ;
      private bool AV74WWServicoFluxoDS_8_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String AV46TFServicoFluxo_ServicoTpHrq_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV36ddo_ServicoFluxo_CodigoTitleControlIdToReplace ;
      private String AV40ddo_ServicoFluxo_ServicoCodTitleControlIdToReplace ;
      private String AV44ddo_ServicoFluxo_ServicoSiglaTitleControlIdToReplace ;
      private String AV48ddo_ServicoFluxo_ServicoTpHrqTitleControlIdToReplace ;
      private String AV52ddo_ServicoFluxo_OrdemTitleControlIdToReplace ;
      private String AV56ddo_ServicoFluxo_ServicoPosTitleControlIdToReplace ;
      private String AV60ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace ;
      private String AV91Update_GXI ;
      private String AV92Delete_GXI ;
      private String AV67WWServicoFluxoDS_1_Dynamicfiltersselector1 ;
      private String AV71WWServicoFluxoDS_5_Dynamicfiltersselector2 ;
      private String AV75WWServicoFluxoDS_9_Dynamicfiltersselector3 ;
      private String AV28Update ;
      private String AV29Delete ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbServicoFluxo_ServicoTpHrq ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00LF2_A1527ServicoFluxo_SrvPosSigla ;
      private bool[] H00LF2_n1527ServicoFluxo_SrvPosSigla ;
      private int[] H00LF2_A1526ServicoFluxo_ServicoPos ;
      private bool[] H00LF2_n1526ServicoFluxo_ServicoPos ;
      private short[] H00LF2_A1532ServicoFluxo_Ordem ;
      private bool[] H00LF2_n1532ServicoFluxo_Ordem ;
      private short[] H00LF2_A1533ServicoFluxo_ServicoTpHrq ;
      private bool[] H00LF2_n1533ServicoFluxo_ServicoTpHrq ;
      private String[] H00LF2_A1523ServicoFluxo_ServicoSigla ;
      private bool[] H00LF2_n1523ServicoFluxo_ServicoSigla ;
      private int[] H00LF2_A1522ServicoFluxo_ServicoCod ;
      private int[] H00LF2_A1528ServicoFluxo_Codigo ;
      private long[] H00LF3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV47TFServicoFluxo_ServicoTpHrq_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV33ServicoFluxo_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV37ServicoFluxo_ServicoCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV41ServicoFluxo_ServicoSiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV45ServicoFluxo_ServicoTpHrqTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV49ServicoFluxo_OrdemTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV53ServicoFluxo_ServicoPosTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV57ServicoFluxo_SrvPosSiglaTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV61DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwservicofluxo__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00LF2( IGxContext context ,
                                             short A1533ServicoFluxo_ServicoTpHrq ,
                                             IGxCollection AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels ,
                                             String AV67WWServicoFluxoDS_1_Dynamicfiltersselector1 ,
                                             short AV68WWServicoFluxoDS_2_Dynamicfiltersoperator1 ,
                                             String AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 ,
                                             bool AV70WWServicoFluxoDS_4_Dynamicfiltersenabled2 ,
                                             String AV71WWServicoFluxoDS_5_Dynamicfiltersselector2 ,
                                             short AV72WWServicoFluxoDS_6_Dynamicfiltersoperator2 ,
                                             String AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 ,
                                             bool AV74WWServicoFluxoDS_8_Dynamicfiltersenabled3 ,
                                             String AV75WWServicoFluxoDS_9_Dynamicfiltersselector3 ,
                                             short AV76WWServicoFluxoDS_10_Dynamicfiltersoperator3 ,
                                             String AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 ,
                                             int AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo ,
                                             int AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to ,
                                             int AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod ,
                                             int AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to ,
                                             String AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel ,
                                             String AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla ,
                                             int AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels_Count ,
                                             short AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem ,
                                             short AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to ,
                                             int AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos ,
                                             int AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to ,
                                             String AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel ,
                                             String AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla ,
                                             String A1523ServicoFluxo_ServicoSigla ,
                                             int A1528ServicoFluxo_Codigo ,
                                             int A1522ServicoFluxo_ServicoCod ,
                                             short A1532ServicoFluxo_Ordem ,
                                             int A1526ServicoFluxo_ServicoPos ,
                                             String A1527ServicoFluxo_SrvPosSigla ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [23] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Servico_Sigla] AS ServicoFluxo_SrvPosSigla, T1.[ServicoFluxo_ServicoPos] AS ServicoFluxo_ServicoPos, T1.[ServicoFluxo_Ordem], T3.[Servico_TipoHierarquia] AS ServicoFluxo_ServicoTpHrq, T3.[Servico_Sigla] AS ServicoFluxo_ServicoSigla, T1.[ServicoFluxo_ServicoCod] AS ServicoFluxo_ServicoCod, T1.[ServicoFluxo_Codigo]";
         sFromString = " FROM (([ServicoFluxo] T1 WITH (NOLOCK) LEFT JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[ServicoFluxo_ServicoPos]) INNER JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T1.[ServicoFluxo_ServicoCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV67WWServicoFluxoDS_1_Dynamicfiltersselector1, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV68WWServicoFluxoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWServicoFluxoDS_1_Dynamicfiltersselector1, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV68WWServicoFluxoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like '%' + @lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like '%' + @lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV70WWServicoFluxoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWServicoFluxoDS_5_Dynamicfiltersselector2, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV72WWServicoFluxoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV70WWServicoFluxoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWServicoFluxoDS_5_Dynamicfiltersselector2, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV72WWServicoFluxoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like '%' + @lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like '%' + @lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV74WWServicoFluxoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV75WWServicoFluxoDS_9_Dynamicfiltersselector3, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV76WWServicoFluxoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV74WWServicoFluxoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV75WWServicoFluxoDS_9_Dynamicfiltersselector3, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV76WWServicoFluxoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like '%' + @lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like '%' + @lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_Codigo] >= @AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_Codigo] >= @AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_Codigo] <= @AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_Codigo] <= @AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (0==AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_ServicoCod] >= @AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_ServicoCod] >= @AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_ServicoCod] <= @AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_ServicoCod] <= @AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] = @AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels, "T3.[Servico_TipoHierarquia] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels, "T3.[Servico_TipoHierarquia] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_Ordem] >= @AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_Ordem] >= @AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (0==AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_Ordem] <= @AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_Ordem] <= @AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (0==AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_ServicoPos] >= @AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_ServicoPos] >= @AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (0==AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_ServicoPos] <= @AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_ServicoPos] <= @AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Sigla] like @lV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Sigla] like @lV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Sigla] = @AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Sigla] = @AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Servico_Sigla]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Servico_Sigla] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_ServicoCod]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_ServicoCod] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Servico_TipoHierarquia]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Servico_TipoHierarquia] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_Ordem]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_Ordem] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_ServicoPos]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_ServicoPos] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Servico_Sigla]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Servico_Sigla] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00LF3( IGxContext context ,
                                             short A1533ServicoFluxo_ServicoTpHrq ,
                                             IGxCollection AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels ,
                                             String AV67WWServicoFluxoDS_1_Dynamicfiltersselector1 ,
                                             short AV68WWServicoFluxoDS_2_Dynamicfiltersoperator1 ,
                                             String AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1 ,
                                             bool AV70WWServicoFluxoDS_4_Dynamicfiltersenabled2 ,
                                             String AV71WWServicoFluxoDS_5_Dynamicfiltersselector2 ,
                                             short AV72WWServicoFluxoDS_6_Dynamicfiltersoperator2 ,
                                             String AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2 ,
                                             bool AV74WWServicoFluxoDS_8_Dynamicfiltersenabled3 ,
                                             String AV75WWServicoFluxoDS_9_Dynamicfiltersselector3 ,
                                             short AV76WWServicoFluxoDS_10_Dynamicfiltersoperator3 ,
                                             String AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3 ,
                                             int AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo ,
                                             int AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to ,
                                             int AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod ,
                                             int AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to ,
                                             String AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel ,
                                             String AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla ,
                                             int AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels_Count ,
                                             short AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem ,
                                             short AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to ,
                                             int AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos ,
                                             int AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to ,
                                             String AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel ,
                                             String AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla ,
                                             String A1523ServicoFluxo_ServicoSigla ,
                                             int A1528ServicoFluxo_Codigo ,
                                             int A1522ServicoFluxo_ServicoCod ,
                                             short A1532ServicoFluxo_Ordem ,
                                             int A1526ServicoFluxo_ServicoPos ,
                                             String A1527ServicoFluxo_SrvPosSigla ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [18] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([ServicoFluxo] T1 WITH (NOLOCK) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T1.[ServicoFluxo_ServicoPos]) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[ServicoFluxo_ServicoCod])";
         if ( ( StringUtil.StrCmp(AV67WWServicoFluxoDS_1_Dynamicfiltersselector1, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV68WWServicoFluxoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Sigla] like @lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Sigla] like @lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWServicoFluxoDS_1_Dynamicfiltersselector1, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV68WWServicoFluxoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Sigla] like '%' + @lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Sigla] like '%' + @lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV70WWServicoFluxoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWServicoFluxoDS_5_Dynamicfiltersselector2, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV72WWServicoFluxoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Sigla] like @lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Sigla] like @lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV70WWServicoFluxoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWServicoFluxoDS_5_Dynamicfiltersselector2, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV72WWServicoFluxoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Sigla] like '%' + @lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Sigla] like '%' + @lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV74WWServicoFluxoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV75WWServicoFluxoDS_9_Dynamicfiltersselector3, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV76WWServicoFluxoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Sigla] like @lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Sigla] like @lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV74WWServicoFluxoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV75WWServicoFluxoDS_9_Dynamicfiltersselector3, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV76WWServicoFluxoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Sigla] like '%' + @lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Sigla] like '%' + @lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_Codigo] >= @AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_Codigo] >= @AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_Codigo] <= @AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_Codigo] <= @AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (0==AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_ServicoCod] >= @AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_ServicoCod] >= @AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_ServicoCod] <= @AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_ServicoCod] <= @AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Sigla] like @lV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Sigla] like @lV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Sigla] = @AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Sigla] = @AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels, "T2.[Servico_TipoHierarquia] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV84WWServicoFluxoDS_18_Tfservicofluxo_servicotphrq_sels, "T2.[Servico_TipoHierarquia] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_Ordem] >= @AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_Ordem] >= @AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (0==AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_Ordem] <= @AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_Ordem] <= @AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (0==AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_ServicoPos] >= @AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_ServicoPos] >= @AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (0==AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_ServicoPos] <= @AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_ServicoPos] <= @AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] = @AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00LF2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (short)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] );
               case 1 :
                     return conditional_H00LF3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (short)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00LF2 ;
          prmH00LF2 = new Object[] {
          new Object[] {"@lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos",SqlDbType.Int,6,0} ,
          new Object[] {"@AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00LF3 ;
          prmH00LF3 = new Object[] {
          new Object[] {"@lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV69WWServicoFluxoDS_3_Servicofluxo_servicosigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV73WWServicoFluxoDS_7_Servicofluxo_servicosigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV77WWServicoFluxoDS_11_Servicofluxo_servicosigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@AV78WWServicoFluxoDS_12_Tfservicofluxo_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV79WWServicoFluxoDS_13_Tfservicofluxo_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV80WWServicoFluxoDS_14_Tfservicofluxo_servicocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV81WWServicoFluxoDS_15_Tfservicofluxo_servicocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV82WWServicoFluxoDS_16_Tfservicofluxo_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV83WWServicoFluxoDS_17_Tfservicofluxo_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV85WWServicoFluxoDS_19_Tfservicofluxo_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV86WWServicoFluxoDS_20_Tfservicofluxo_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV87WWServicoFluxoDS_21_Tfservicofluxo_servicopos",SqlDbType.Int,6,0} ,
          new Object[] {"@AV88WWServicoFluxoDS_22_Tfservicofluxo_servicopos_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV89WWServicoFluxoDS_23_Tfservicofluxo_srvpossigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV90WWServicoFluxoDS_24_Tfservicofluxo_srvpossigla_sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00LF2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LF2,11,0,true,false )
             ,new CursorDef("H00LF3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LF3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                return;
       }
    }

 }

}
