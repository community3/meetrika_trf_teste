/*
               File: PRC_VincularOS
        Description: Vincular OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:18.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_vincularos : GXProcedure
   {
      public prc_vincularos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_vincularos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Codigo ,
                           int aP1_ContagemResultado_OSVinculada )
      {
         this.AV17Codigo = aP0_Codigo;
         this.AV8ContagemResultado_OSVinculada = aP1_ContagemResultado_OSVinculada;
         initialize();
         executePrivate();
         aP0_Codigo=this.AV17Codigo;
      }

      public void executeSubmit( ref int aP0_Codigo ,
                                 int aP1_ContagemResultado_OSVinculada )
      {
         prc_vincularos objprc_vincularos;
         objprc_vincularos = new prc_vincularos();
         objprc_vincularos.AV17Codigo = aP0_Codigo;
         objprc_vincularos.AV8ContagemResultado_OSVinculada = aP1_ContagemResultado_OSVinculada;
         objprc_vincularos.context.SetSubmitInitialConfig(context);
         objprc_vincularos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_vincularos);
         aP0_Codigo=this.AV17Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_vincularos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00652 */
         pr_default.execute(0, new Object[] {AV17Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A456ContagemResultado_Codigo = P00652_A456ContagemResultado_Codigo[0];
            A602ContagemResultado_OSVinculada = P00652_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00652_n602ContagemResultado_OSVinculada[0];
            A602ContagemResultado_OSVinculada = AV8ContagemResultado_OSVinculada;
            n602ContagemResultado_OSVinculada = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00653 */
            pr_default.execute(1, new Object[] {n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P00654 */
            pr_default.execute(2, new Object[] {n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         AV20Requisitos.FromXml(AV10WebSession.Get("Requisitos"), "Collection");
         if ( AV20Requisitos.Count > 0 )
         {
            AV10WebSession.Remove("Requisitos");
            AV19i = 1;
            while ( AV19i <= AV20Requisitos.Count )
            {
               /*
                  INSERT RECORD ON TABLE ContagemResultadoRequisito

               */
               A2003ContagemResultadoRequisito_OSCod = AV17Codigo;
               A2004ContagemResultadoRequisito_ReqCod = (int)(AV20Requisitos.GetNumeric(AV19i));
               A2006ContagemResultadoRequisito_Owner = false;
               /* Using cursor P00655 */
               pr_default.execute(3, new Object[] {A2003ContagemResultadoRequisito_OSCod, A2004ContagemResultadoRequisito_ReqCod, A2006ContagemResultadoRequisito_Owner});
               A2005ContagemResultadoRequisito_Codigo = P00655_A2005ContagemResultadoRequisito_Codigo[0];
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoRequisito") ;
               if ( (pr_default.getStatus(3) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
               AV19i = (short)(AV19i+1);
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_VincularOS");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00652_A456ContagemResultado_Codigo = new int[1] ;
         P00652_A602ContagemResultado_OSVinculada = new int[1] ;
         P00652_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         AV20Requisitos = new GxSimpleCollection();
         AV10WebSession = context.GetSession();
         P00655_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_vincularos__default(),
            new Object[][] {
                new Object[] {
               P00652_A456ContagemResultado_Codigo, P00652_A602ContagemResultado_OSVinculada, P00652_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00655_A2005ContagemResultadoRequisito_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV19i ;
      private int AV17Codigo ;
      private int AV8ContagemResultado_OSVinculada ;
      private int A456ContagemResultado_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int GX_INS221 ;
      private int A2003ContagemResultadoRequisito_OSCod ;
      private int A2004ContagemResultadoRequisito_ReqCod ;
      private int A2005ContagemResultadoRequisito_Codigo ;
      private String scmdbuf ;
      private String Gx_emsg ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool A2006ContagemResultadoRequisito_Owner ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00652_A456ContagemResultado_Codigo ;
      private int[] P00652_A602ContagemResultado_OSVinculada ;
      private bool[] P00652_n602ContagemResultado_OSVinculada ;
      private int[] P00655_A2005ContagemResultadoRequisito_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV20Requisitos ;
   }

   public class prc_vincularos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00652 ;
          prmP00652 = new Object[] {
          new Object[] {"@AV17Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00653 ;
          prmP00653 = new Object[] {
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00654 ;
          prmP00654 = new Object[] {
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00655 ;
          prmP00655 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoRequisito_ReqCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoRequisito_Owner",SqlDbType.Bit,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00652", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_OSVinculada] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV17Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00652,1,0,true,true )
             ,new CursorDef("P00653", "UPDATE [ContagemResultado] SET [ContagemResultado_OSVinculada]=@ContagemResultado_OSVinculada  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00653)
             ,new CursorDef("P00654", "UPDATE [ContagemResultado] SET [ContagemResultado_OSVinculada]=@ContagemResultado_OSVinculada  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00654)
             ,new CursorDef("P00655", "INSERT INTO [ContagemResultadoRequisito]([ContagemResultadoRequisito_OSCod], [ContagemResultadoRequisito_ReqCod], [ContagemResultadoRequisito_Owner]) VALUES(@ContagemResultadoRequisito_OSCod, @ContagemResultadoRequisito_ReqCod, @ContagemResultadoRequisito_Owner); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00655)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                return;
       }
    }

 }

}
