/*
               File: type_SdtUsuario
        Description: Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:15:50.49
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Usuario" )]
   [XmlType(TypeName =  "Usuario" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtUsuario : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtUsuario( )
      {
         /* Constructor for serialization */
         gxTv_SdtUsuario_Usuario_cargonom = "";
         gxTv_SdtUsuario_Usuario_cargouonom = "";
         gxTv_SdtUsuario_Usuario_entidade = "";
         gxTv_SdtUsuario_Usuario_pessoanom = "";
         gxTv_SdtUsuario_Usuario_pessoatip = "";
         gxTv_SdtUsuario_Usuario_pessoadoc = "";
         gxTv_SdtUsuario_Usuario_pessoatelefone = "";
         gxTv_SdtUsuario_Usuario_usergamguid = "";
         gxTv_SdtUsuario_Usuario_nome = "";
         gxTv_SdtUsuario_Usuario_crtfpath = "";
         gxTv_SdtUsuario_Usuario_notificar = "";
         gxTv_SdtUsuario_Usuario_email = "";
         gxTv_SdtUsuario_Usuario_foto = "";
         gxTv_SdtUsuario_Usuario_foto_gxi = "";
         gxTv_SdtUsuario_Mode = "";
         gxTv_SdtUsuario_Usuario_nome_Z = "";
         gxTv_SdtUsuario_Usuario_usergamguid_Z = "";
         gxTv_SdtUsuario_Usuario_cargonom_Z = "";
         gxTv_SdtUsuario_Usuario_cargouonom_Z = "";
         gxTv_SdtUsuario_Usuario_entidade_Z = "";
         gxTv_SdtUsuario_Usuario_pessoanom_Z = "";
         gxTv_SdtUsuario_Usuario_pessoatip_Z = "";
         gxTv_SdtUsuario_Usuario_pessoadoc_Z = "";
         gxTv_SdtUsuario_Usuario_pessoatelefone_Z = "";
         gxTv_SdtUsuario_Usuario_crtfpath_Z = "";
         gxTv_SdtUsuario_Usuario_notificar_Z = "";
         gxTv_SdtUsuario_Usuario_email_Z = "";
         gxTv_SdtUsuario_Usuario_foto_gxi_Z = "";
      }

      public SdtUsuario( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1Usuario_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1Usuario_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Usuario_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Usuario");
         metadata.Set("BT", "Usuario");
         metadata.Set("PK", "[ \"Usuario_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Usuario_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Cargo_Codigo\" ],\"FKMap\":[ \"Usuario_CargoCod-Cargo_Codigo\" ] },{ \"FK\":[ \"Pessoa_Codigo\" ],\"FKMap\":[ \"Usuario_PessoaCod-Pessoa_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Usuario_foto_gxi" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_usergamguid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_cargocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_cargonom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_cargouocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_cargouonom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_entidade_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_pessoacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_pessoanom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_pessoatip_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_pessoadoc_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_pessoatelefone_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_ehcontador_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_ehauditorfm_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_ehcontratada_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_ehcontratante_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_ehfinanceiro_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_ehgestor_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_ehpreposto_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_crtfpath_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_notificar_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_email_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_deferias_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_ultimaarea_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_foto_gxi_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_cargocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_cargonom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_cargouocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_cargouonom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_pessoanom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_pessoatip_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_pessoadoc_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_pessoatelefone_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_nome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_crtfpath_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_notificar_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_email_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_foto_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_deferias_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_ultimaarea_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_foto_gxi_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtUsuario deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtUsuario)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtUsuario obj ;
         obj = this;
         obj.gxTpr_Usuario_codigo = deserialized.gxTpr_Usuario_codigo;
         obj.gxTpr_Usuario_cargocod = deserialized.gxTpr_Usuario_cargocod;
         obj.gxTpr_Usuario_cargonom = deserialized.gxTpr_Usuario_cargonom;
         obj.gxTpr_Usuario_cargouocod = deserialized.gxTpr_Usuario_cargouocod;
         obj.gxTpr_Usuario_cargouonom = deserialized.gxTpr_Usuario_cargouonom;
         obj.gxTpr_Usuario_entidade = deserialized.gxTpr_Usuario_entidade;
         obj.gxTpr_Usuario_pessoacod = deserialized.gxTpr_Usuario_pessoacod;
         obj.gxTpr_Usuario_pessoanom = deserialized.gxTpr_Usuario_pessoanom;
         obj.gxTpr_Usuario_pessoatip = deserialized.gxTpr_Usuario_pessoatip;
         obj.gxTpr_Usuario_pessoadoc = deserialized.gxTpr_Usuario_pessoadoc;
         obj.gxTpr_Usuario_pessoatelefone = deserialized.gxTpr_Usuario_pessoatelefone;
         obj.gxTpr_Usuario_usergamguid = deserialized.gxTpr_Usuario_usergamguid;
         obj.gxTpr_Usuario_nome = deserialized.gxTpr_Usuario_nome;
         obj.gxTpr_Usuario_ehcontador = deserialized.gxTpr_Usuario_ehcontador;
         obj.gxTpr_Usuario_ehauditorfm = deserialized.gxTpr_Usuario_ehauditorfm;
         obj.gxTpr_Usuario_ehcontratada = deserialized.gxTpr_Usuario_ehcontratada;
         obj.gxTpr_Usuario_ehcontratante = deserialized.gxTpr_Usuario_ehcontratante;
         obj.gxTpr_Usuario_ehfinanceiro = deserialized.gxTpr_Usuario_ehfinanceiro;
         obj.gxTpr_Usuario_ehgestor = deserialized.gxTpr_Usuario_ehgestor;
         obj.gxTpr_Usuario_ehpreposto = deserialized.gxTpr_Usuario_ehpreposto;
         obj.gxTpr_Usuario_crtfpath = deserialized.gxTpr_Usuario_crtfpath;
         obj.gxTpr_Usuario_notificar = deserialized.gxTpr_Usuario_notificar;
         obj.gxTpr_Usuario_ativo = deserialized.gxTpr_Usuario_ativo;
         obj.gxTpr_Usuario_email = deserialized.gxTpr_Usuario_email;
         obj.gxTpr_Usuario_foto = deserialized.gxTpr_Usuario_foto;
         obj.gxTpr_Usuario_foto_gxi = deserialized.gxTpr_Usuario_foto_gxi;
         obj.gxTpr_Usuario_deferias = deserialized.gxTpr_Usuario_deferias;
         obj.gxTpr_Usuario_ultimaarea = deserialized.gxTpr_Usuario_ultimaarea;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Usuario_nome_Z = deserialized.gxTpr_Usuario_nome_Z;
         obj.gxTpr_Usuario_usergamguid_Z = deserialized.gxTpr_Usuario_usergamguid_Z;
         obj.gxTpr_Usuario_codigo_Z = deserialized.gxTpr_Usuario_codigo_Z;
         obj.gxTpr_Usuario_cargocod_Z = deserialized.gxTpr_Usuario_cargocod_Z;
         obj.gxTpr_Usuario_cargonom_Z = deserialized.gxTpr_Usuario_cargonom_Z;
         obj.gxTpr_Usuario_cargouocod_Z = deserialized.gxTpr_Usuario_cargouocod_Z;
         obj.gxTpr_Usuario_cargouonom_Z = deserialized.gxTpr_Usuario_cargouonom_Z;
         obj.gxTpr_Usuario_entidade_Z = deserialized.gxTpr_Usuario_entidade_Z;
         obj.gxTpr_Usuario_pessoacod_Z = deserialized.gxTpr_Usuario_pessoacod_Z;
         obj.gxTpr_Usuario_pessoanom_Z = deserialized.gxTpr_Usuario_pessoanom_Z;
         obj.gxTpr_Usuario_pessoatip_Z = deserialized.gxTpr_Usuario_pessoatip_Z;
         obj.gxTpr_Usuario_pessoadoc_Z = deserialized.gxTpr_Usuario_pessoadoc_Z;
         obj.gxTpr_Usuario_pessoatelefone_Z = deserialized.gxTpr_Usuario_pessoatelefone_Z;
         obj.gxTpr_Usuario_ehcontador_Z = deserialized.gxTpr_Usuario_ehcontador_Z;
         obj.gxTpr_Usuario_ehauditorfm_Z = deserialized.gxTpr_Usuario_ehauditorfm_Z;
         obj.gxTpr_Usuario_ehcontratada_Z = deserialized.gxTpr_Usuario_ehcontratada_Z;
         obj.gxTpr_Usuario_ehcontratante_Z = deserialized.gxTpr_Usuario_ehcontratante_Z;
         obj.gxTpr_Usuario_ehfinanceiro_Z = deserialized.gxTpr_Usuario_ehfinanceiro_Z;
         obj.gxTpr_Usuario_ehgestor_Z = deserialized.gxTpr_Usuario_ehgestor_Z;
         obj.gxTpr_Usuario_ehpreposto_Z = deserialized.gxTpr_Usuario_ehpreposto_Z;
         obj.gxTpr_Usuario_crtfpath_Z = deserialized.gxTpr_Usuario_crtfpath_Z;
         obj.gxTpr_Usuario_notificar_Z = deserialized.gxTpr_Usuario_notificar_Z;
         obj.gxTpr_Usuario_ativo_Z = deserialized.gxTpr_Usuario_ativo_Z;
         obj.gxTpr_Usuario_email_Z = deserialized.gxTpr_Usuario_email_Z;
         obj.gxTpr_Usuario_deferias_Z = deserialized.gxTpr_Usuario_deferias_Z;
         obj.gxTpr_Usuario_ultimaarea_Z = deserialized.gxTpr_Usuario_ultimaarea_Z;
         obj.gxTpr_Usuario_foto_gxi_Z = deserialized.gxTpr_Usuario_foto_gxi_Z;
         obj.gxTpr_Usuario_codigo_N = deserialized.gxTpr_Usuario_codigo_N;
         obj.gxTpr_Usuario_cargocod_N = deserialized.gxTpr_Usuario_cargocod_N;
         obj.gxTpr_Usuario_cargonom_N = deserialized.gxTpr_Usuario_cargonom_N;
         obj.gxTpr_Usuario_cargouocod_N = deserialized.gxTpr_Usuario_cargouocod_N;
         obj.gxTpr_Usuario_cargouonom_N = deserialized.gxTpr_Usuario_cargouonom_N;
         obj.gxTpr_Usuario_pessoanom_N = deserialized.gxTpr_Usuario_pessoanom_N;
         obj.gxTpr_Usuario_pessoatip_N = deserialized.gxTpr_Usuario_pessoatip_N;
         obj.gxTpr_Usuario_pessoadoc_N = deserialized.gxTpr_Usuario_pessoadoc_N;
         obj.gxTpr_Usuario_pessoatelefone_N = deserialized.gxTpr_Usuario_pessoatelefone_N;
         obj.gxTpr_Usuario_nome_N = deserialized.gxTpr_Usuario_nome_N;
         obj.gxTpr_Usuario_crtfpath_N = deserialized.gxTpr_Usuario_crtfpath_N;
         obj.gxTpr_Usuario_notificar_N = deserialized.gxTpr_Usuario_notificar_N;
         obj.gxTpr_Usuario_email_N = deserialized.gxTpr_Usuario_email_N;
         obj.gxTpr_Usuario_foto_N = deserialized.gxTpr_Usuario_foto_N;
         obj.gxTpr_Usuario_deferias_N = deserialized.gxTpr_Usuario_deferias_N;
         obj.gxTpr_Usuario_ultimaarea_N = deserialized.gxTpr_Usuario_ultimaarea_N;
         obj.gxTpr_Usuario_foto_gxi_N = deserialized.gxTpr_Usuario_foto_gxi_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Codigo") )
               {
                  gxTv_SdtUsuario_Usuario_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoCod") )
               {
                  gxTv_SdtUsuario_Usuario_cargocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoNom") )
               {
                  gxTv_SdtUsuario_Usuario_cargonom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoUOCod") )
               {
                  gxTv_SdtUsuario_Usuario_cargouocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoUONom") )
               {
                  gxTv_SdtUsuario_Usuario_cargouonom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Entidade") )
               {
                  gxTv_SdtUsuario_Usuario_entidade = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaCod") )
               {
                  gxTv_SdtUsuario_Usuario_pessoacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaNom") )
               {
                  gxTv_SdtUsuario_Usuario_pessoanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaTip") )
               {
                  gxTv_SdtUsuario_Usuario_pessoatip = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaDoc") )
               {
                  gxTv_SdtUsuario_Usuario_pessoadoc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaTelefone") )
               {
                  gxTv_SdtUsuario_Usuario_pessoatelefone = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_UserGamGuid") )
               {
                  gxTv_SdtUsuario_Usuario_usergamguid = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Nome") )
               {
                  gxTv_SdtUsuario_Usuario_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhContador") )
               {
                  gxTv_SdtUsuario_Usuario_ehcontador = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhAuditorFM") )
               {
                  gxTv_SdtUsuario_Usuario_ehauditorfm = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhContratada") )
               {
                  gxTv_SdtUsuario_Usuario_ehcontratada = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhContratante") )
               {
                  gxTv_SdtUsuario_Usuario_ehcontratante = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhFinanceiro") )
               {
                  gxTv_SdtUsuario_Usuario_ehfinanceiro = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhGestor") )
               {
                  gxTv_SdtUsuario_Usuario_ehgestor = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhPreposto") )
               {
                  gxTv_SdtUsuario_Usuario_ehpreposto = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CrtfPath") )
               {
                  gxTv_SdtUsuario_Usuario_crtfpath = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Notificar") )
               {
                  gxTv_SdtUsuario_Usuario_notificar = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Ativo") )
               {
                  gxTv_SdtUsuario_Usuario_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Email") )
               {
                  gxTv_SdtUsuario_Usuario_email = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Foto") )
               {
                  gxTv_SdtUsuario_Usuario_foto = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Foto_GXI") )
               {
                  gxTv_SdtUsuario_Usuario_foto_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_DeFerias") )
               {
                  gxTv_SdtUsuario_Usuario_deferias = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_UltimaArea") )
               {
                  gxTv_SdtUsuario_Usuario_ultimaarea = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtUsuario_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtUsuario_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Nome_Z") )
               {
                  gxTv_SdtUsuario_Usuario_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_UserGamGuid_Z") )
               {
                  gxTv_SdtUsuario_Usuario_usergamguid_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Codigo_Z") )
               {
                  gxTv_SdtUsuario_Usuario_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoCod_Z") )
               {
                  gxTv_SdtUsuario_Usuario_cargocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoNom_Z") )
               {
                  gxTv_SdtUsuario_Usuario_cargonom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoUOCod_Z") )
               {
                  gxTv_SdtUsuario_Usuario_cargouocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoUONom_Z") )
               {
                  gxTv_SdtUsuario_Usuario_cargouonom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Entidade_Z") )
               {
                  gxTv_SdtUsuario_Usuario_entidade_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaCod_Z") )
               {
                  gxTv_SdtUsuario_Usuario_pessoacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaNom_Z") )
               {
                  gxTv_SdtUsuario_Usuario_pessoanom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaTip_Z") )
               {
                  gxTv_SdtUsuario_Usuario_pessoatip_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaDoc_Z") )
               {
                  gxTv_SdtUsuario_Usuario_pessoadoc_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaTelefone_Z") )
               {
                  gxTv_SdtUsuario_Usuario_pessoatelefone_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhContador_Z") )
               {
                  gxTv_SdtUsuario_Usuario_ehcontador_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhAuditorFM_Z") )
               {
                  gxTv_SdtUsuario_Usuario_ehauditorfm_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhContratada_Z") )
               {
                  gxTv_SdtUsuario_Usuario_ehcontratada_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhContratante_Z") )
               {
                  gxTv_SdtUsuario_Usuario_ehcontratante_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhFinanceiro_Z") )
               {
                  gxTv_SdtUsuario_Usuario_ehfinanceiro_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhGestor_Z") )
               {
                  gxTv_SdtUsuario_Usuario_ehgestor_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhPreposto_Z") )
               {
                  gxTv_SdtUsuario_Usuario_ehpreposto_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CrtfPath_Z") )
               {
                  gxTv_SdtUsuario_Usuario_crtfpath_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Notificar_Z") )
               {
                  gxTv_SdtUsuario_Usuario_notificar_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Ativo_Z") )
               {
                  gxTv_SdtUsuario_Usuario_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Email_Z") )
               {
                  gxTv_SdtUsuario_Usuario_email_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_DeFerias_Z") )
               {
                  gxTv_SdtUsuario_Usuario_deferias_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_UltimaArea_Z") )
               {
                  gxTv_SdtUsuario_Usuario_ultimaarea_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Foto_GXI_Z") )
               {
                  gxTv_SdtUsuario_Usuario_foto_gxi_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Codigo_N") )
               {
                  gxTv_SdtUsuario_Usuario_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoCod_N") )
               {
                  gxTv_SdtUsuario_Usuario_cargocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoNom_N") )
               {
                  gxTv_SdtUsuario_Usuario_cargonom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoUOCod_N") )
               {
                  gxTv_SdtUsuario_Usuario_cargouocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoUONom_N") )
               {
                  gxTv_SdtUsuario_Usuario_cargouonom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaNom_N") )
               {
                  gxTv_SdtUsuario_Usuario_pessoanom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaTip_N") )
               {
                  gxTv_SdtUsuario_Usuario_pessoatip_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaDoc_N") )
               {
                  gxTv_SdtUsuario_Usuario_pessoadoc_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaTelefone_N") )
               {
                  gxTv_SdtUsuario_Usuario_pessoatelefone_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Nome_N") )
               {
                  gxTv_SdtUsuario_Usuario_nome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CrtfPath_N") )
               {
                  gxTv_SdtUsuario_Usuario_crtfpath_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Notificar_N") )
               {
                  gxTv_SdtUsuario_Usuario_notificar_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Email_N") )
               {
                  gxTv_SdtUsuario_Usuario_email_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Foto_N") )
               {
                  gxTv_SdtUsuario_Usuario_foto_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_DeFerias_N") )
               {
                  gxTv_SdtUsuario_Usuario_deferias_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_UltimaArea_N") )
               {
                  gxTv_SdtUsuario_Usuario_ultimaarea_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Foto_GXI_N") )
               {
                  gxTv_SdtUsuario_Usuario_foto_gxi_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Usuario";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Usuario_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_CargoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_cargocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_CargoNom", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_cargonom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_CargoUOCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_cargouocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_CargoUONom", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_cargouonom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_Entidade", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_entidade));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_PessoaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_pessoacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_PessoaNom", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_pessoanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_PessoaTip", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_pessoatip));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_PessoaDoc", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_pessoadoc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_PessoaTelefone", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_pessoatelefone));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_UserGamGuid", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_usergamguid));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_Nome", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_EhContador", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_ehcontador)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_EhAuditorFM", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_ehauditorfm)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_EhContratada", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_ehcontratada)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_EhContratante", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_ehcontratante)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_EhFinanceiro", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_ehfinanceiro)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_EhGestor", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_ehgestor)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_EhPreposto", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_ehpreposto)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_CrtfPath", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_crtfpath));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_Notificar", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_notificar));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_Email", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_email));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_Foto", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_foto));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_DeFerias", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_deferias)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_UltimaArea", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_ultimaarea), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Usuario_Foto_GXI", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_foto_gxi));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtUsuario_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Nome_Z", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_UserGamGuid_Z", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_usergamguid_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_CargoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_cargocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_CargoNom_Z", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_cargonom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_CargoUOCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_cargouocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_CargoUONom_Z", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_cargouonom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Entidade_Z", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_entidade_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_PessoaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_pessoacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_PessoaNom_Z", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_pessoanom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_PessoaTip_Z", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_pessoatip_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_PessoaDoc_Z", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_pessoadoc_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_PessoaTelefone_Z", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_pessoatelefone_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_EhContador_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_ehcontador_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_EhAuditorFM_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_ehauditorfm_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_EhContratada_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_ehcontratada_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_EhContratante_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_ehcontratante_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_EhFinanceiro_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_ehfinanceiro_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_EhGestor_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_ehgestor_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_EhPreposto_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_ehpreposto_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_CrtfPath_Z", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_crtfpath_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Notificar_Z", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_notificar_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Email_Z", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_email_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_DeFerias_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuario_Usuario_deferias_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_UltimaArea_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_ultimaarea_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Foto_GXI_Z", StringUtil.RTrim( gxTv_SdtUsuario_Usuario_foto_gxi_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_CargoCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_cargocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_CargoNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_cargonom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_CargoUOCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_cargouocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_CargoUONom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_cargouonom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_PessoaNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_pessoanom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_PessoaTip_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_pessoatip_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_PessoaDoc_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_pessoadoc_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_PessoaTelefone_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_pessoatelefone_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Nome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_nome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_CrtfPath_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_crtfpath_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Notificar_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_notificar_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Email_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_email_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Foto_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_foto_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_DeFerias_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_deferias_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_UltimaArea_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_ultimaarea_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Foto_GXI_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuario_Usuario_foto_gxi_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Usuario_Codigo", gxTv_SdtUsuario_Usuario_codigo, false);
         AddObjectProperty("Usuario_CargoCod", gxTv_SdtUsuario_Usuario_cargocod, false);
         AddObjectProperty("Usuario_CargoNom", gxTv_SdtUsuario_Usuario_cargonom, false);
         AddObjectProperty("Usuario_CargoUOCod", gxTv_SdtUsuario_Usuario_cargouocod, false);
         AddObjectProperty("Usuario_CargoUONom", gxTv_SdtUsuario_Usuario_cargouonom, false);
         AddObjectProperty("Usuario_Entidade", gxTv_SdtUsuario_Usuario_entidade, false);
         AddObjectProperty("Usuario_PessoaCod", gxTv_SdtUsuario_Usuario_pessoacod, false);
         AddObjectProperty("Usuario_PessoaNom", gxTv_SdtUsuario_Usuario_pessoanom, false);
         AddObjectProperty("Usuario_PessoaTip", gxTv_SdtUsuario_Usuario_pessoatip, false);
         AddObjectProperty("Usuario_PessoaDoc", gxTv_SdtUsuario_Usuario_pessoadoc, false);
         AddObjectProperty("Usuario_PessoaTelefone", gxTv_SdtUsuario_Usuario_pessoatelefone, false);
         AddObjectProperty("Usuario_UserGamGuid", gxTv_SdtUsuario_Usuario_usergamguid, false);
         AddObjectProperty("Usuario_Nome", gxTv_SdtUsuario_Usuario_nome, false);
         AddObjectProperty("Usuario_EhContador", gxTv_SdtUsuario_Usuario_ehcontador, false);
         AddObjectProperty("Usuario_EhAuditorFM", gxTv_SdtUsuario_Usuario_ehauditorfm, false);
         AddObjectProperty("Usuario_EhContratada", gxTv_SdtUsuario_Usuario_ehcontratada, false);
         AddObjectProperty("Usuario_EhContratante", gxTv_SdtUsuario_Usuario_ehcontratante, false);
         AddObjectProperty("Usuario_EhFinanceiro", gxTv_SdtUsuario_Usuario_ehfinanceiro, false);
         AddObjectProperty("Usuario_EhGestor", gxTv_SdtUsuario_Usuario_ehgestor, false);
         AddObjectProperty("Usuario_EhPreposto", gxTv_SdtUsuario_Usuario_ehpreposto, false);
         AddObjectProperty("Usuario_CrtfPath", gxTv_SdtUsuario_Usuario_crtfpath, false);
         AddObjectProperty("Usuario_Notificar", gxTv_SdtUsuario_Usuario_notificar, false);
         AddObjectProperty("Usuario_Ativo", gxTv_SdtUsuario_Usuario_ativo, false);
         AddObjectProperty("Usuario_Email", gxTv_SdtUsuario_Usuario_email, false);
         AddObjectProperty("Usuario_Foto", gxTv_SdtUsuario_Usuario_foto, false);
         AddObjectProperty("Usuario_DeFerias", gxTv_SdtUsuario_Usuario_deferias, false);
         AddObjectProperty("Usuario_UltimaArea", gxTv_SdtUsuario_Usuario_ultimaarea, false);
         if ( includeState )
         {
            AddObjectProperty("Usuario_Foto_GXI", gxTv_SdtUsuario_Usuario_foto_gxi, false);
            AddObjectProperty("Mode", gxTv_SdtUsuario_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtUsuario_Initialized, false);
            AddObjectProperty("Usuario_Nome_Z", gxTv_SdtUsuario_Usuario_nome_Z, false);
            AddObjectProperty("Usuario_UserGamGuid_Z", gxTv_SdtUsuario_Usuario_usergamguid_Z, false);
            AddObjectProperty("Usuario_Codigo_Z", gxTv_SdtUsuario_Usuario_codigo_Z, false);
            AddObjectProperty("Usuario_CargoCod_Z", gxTv_SdtUsuario_Usuario_cargocod_Z, false);
            AddObjectProperty("Usuario_CargoNom_Z", gxTv_SdtUsuario_Usuario_cargonom_Z, false);
            AddObjectProperty("Usuario_CargoUOCod_Z", gxTv_SdtUsuario_Usuario_cargouocod_Z, false);
            AddObjectProperty("Usuario_CargoUONom_Z", gxTv_SdtUsuario_Usuario_cargouonom_Z, false);
            AddObjectProperty("Usuario_Entidade_Z", gxTv_SdtUsuario_Usuario_entidade_Z, false);
            AddObjectProperty("Usuario_PessoaCod_Z", gxTv_SdtUsuario_Usuario_pessoacod_Z, false);
            AddObjectProperty("Usuario_PessoaNom_Z", gxTv_SdtUsuario_Usuario_pessoanom_Z, false);
            AddObjectProperty("Usuario_PessoaTip_Z", gxTv_SdtUsuario_Usuario_pessoatip_Z, false);
            AddObjectProperty("Usuario_PessoaDoc_Z", gxTv_SdtUsuario_Usuario_pessoadoc_Z, false);
            AddObjectProperty("Usuario_PessoaTelefone_Z", gxTv_SdtUsuario_Usuario_pessoatelefone_Z, false);
            AddObjectProperty("Usuario_EhContador_Z", gxTv_SdtUsuario_Usuario_ehcontador_Z, false);
            AddObjectProperty("Usuario_EhAuditorFM_Z", gxTv_SdtUsuario_Usuario_ehauditorfm_Z, false);
            AddObjectProperty("Usuario_EhContratada_Z", gxTv_SdtUsuario_Usuario_ehcontratada_Z, false);
            AddObjectProperty("Usuario_EhContratante_Z", gxTv_SdtUsuario_Usuario_ehcontratante_Z, false);
            AddObjectProperty("Usuario_EhFinanceiro_Z", gxTv_SdtUsuario_Usuario_ehfinanceiro_Z, false);
            AddObjectProperty("Usuario_EhGestor_Z", gxTv_SdtUsuario_Usuario_ehgestor_Z, false);
            AddObjectProperty("Usuario_EhPreposto_Z", gxTv_SdtUsuario_Usuario_ehpreposto_Z, false);
            AddObjectProperty("Usuario_CrtfPath_Z", gxTv_SdtUsuario_Usuario_crtfpath_Z, false);
            AddObjectProperty("Usuario_Notificar_Z", gxTv_SdtUsuario_Usuario_notificar_Z, false);
            AddObjectProperty("Usuario_Ativo_Z", gxTv_SdtUsuario_Usuario_ativo_Z, false);
            AddObjectProperty("Usuario_Email_Z", gxTv_SdtUsuario_Usuario_email_Z, false);
            AddObjectProperty("Usuario_DeFerias_Z", gxTv_SdtUsuario_Usuario_deferias_Z, false);
            AddObjectProperty("Usuario_UltimaArea_Z", gxTv_SdtUsuario_Usuario_ultimaarea_Z, false);
            AddObjectProperty("Usuario_Foto_GXI_Z", gxTv_SdtUsuario_Usuario_foto_gxi_Z, false);
            AddObjectProperty("Usuario_Codigo_N", gxTv_SdtUsuario_Usuario_codigo_N, false);
            AddObjectProperty("Usuario_CargoCod_N", gxTv_SdtUsuario_Usuario_cargocod_N, false);
            AddObjectProperty("Usuario_CargoNom_N", gxTv_SdtUsuario_Usuario_cargonom_N, false);
            AddObjectProperty("Usuario_CargoUOCod_N", gxTv_SdtUsuario_Usuario_cargouocod_N, false);
            AddObjectProperty("Usuario_CargoUONom_N", gxTv_SdtUsuario_Usuario_cargouonom_N, false);
            AddObjectProperty("Usuario_PessoaNom_N", gxTv_SdtUsuario_Usuario_pessoanom_N, false);
            AddObjectProperty("Usuario_PessoaTip_N", gxTv_SdtUsuario_Usuario_pessoatip_N, false);
            AddObjectProperty("Usuario_PessoaDoc_N", gxTv_SdtUsuario_Usuario_pessoadoc_N, false);
            AddObjectProperty("Usuario_PessoaTelefone_N", gxTv_SdtUsuario_Usuario_pessoatelefone_N, false);
            AddObjectProperty("Usuario_Nome_N", gxTv_SdtUsuario_Usuario_nome_N, false);
            AddObjectProperty("Usuario_CrtfPath_N", gxTv_SdtUsuario_Usuario_crtfpath_N, false);
            AddObjectProperty("Usuario_Notificar_N", gxTv_SdtUsuario_Usuario_notificar_N, false);
            AddObjectProperty("Usuario_Email_N", gxTv_SdtUsuario_Usuario_email_N, false);
            AddObjectProperty("Usuario_Foto_N", gxTv_SdtUsuario_Usuario_foto_N, false);
            AddObjectProperty("Usuario_DeFerias_N", gxTv_SdtUsuario_Usuario_deferias_N, false);
            AddObjectProperty("Usuario_UltimaArea_N", gxTv_SdtUsuario_Usuario_ultimaarea_N, false);
            AddObjectProperty("Usuario_Foto_GXI_N", gxTv_SdtUsuario_Usuario_foto_gxi_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Usuario_Codigo" )]
      [  XmlElement( ElementName = "Usuario_Codigo"   )]
      public int gxTpr_Usuario_codigo
      {
         get {
            return gxTv_SdtUsuario_Usuario_codigo ;
         }

         set {
            if ( gxTv_SdtUsuario_Usuario_codigo != value )
            {
               gxTv_SdtUsuario_Mode = "INS";
               this.gxTv_SdtUsuario_Usuario_nome_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_usergamguid_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_codigo_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_cargocod_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_cargonom_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_cargouocod_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_cargouonom_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_entidade_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_pessoacod_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_pessoanom_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_pessoatip_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_pessoadoc_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_pessoatelefone_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_usergamguid_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_nome_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_ehcontador_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_ehauditorfm_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_ehcontratada_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_ehcontratante_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_ehfinanceiro_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_ehgestor_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_ehpreposto_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_crtfpath_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_notificar_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_ativo_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_email_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_deferias_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_ultimaarea_Z_SetNull( );
               this.gxTv_SdtUsuario_Usuario_foto_gxi_Z_SetNull( );
            }
            gxTv_SdtUsuario_Usuario_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_CargoCod" )]
      [  XmlElement( ElementName = "Usuario_CargoCod"   )]
      public int gxTpr_Usuario_cargocod
      {
         get {
            return gxTv_SdtUsuario_Usuario_cargocod ;
         }

         set {
            gxTv_SdtUsuario_Usuario_cargocod_N = 0;
            gxTv_SdtUsuario_Usuario_cargocod = (int)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_cargocod_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_cargocod_N = 1;
         gxTv_SdtUsuario_Usuario_cargocod = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_cargocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_CargoNom" )]
      [  XmlElement( ElementName = "Usuario_CargoNom"   )]
      public String gxTpr_Usuario_cargonom
      {
         get {
            return gxTv_SdtUsuario_Usuario_cargonom ;
         }

         set {
            gxTv_SdtUsuario_Usuario_cargonom_N = 0;
            gxTv_SdtUsuario_Usuario_cargonom = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_cargonom_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_cargonom_N = 1;
         gxTv_SdtUsuario_Usuario_cargonom = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_cargonom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_CargoUOCod" )]
      [  XmlElement( ElementName = "Usuario_CargoUOCod"   )]
      public int gxTpr_Usuario_cargouocod
      {
         get {
            return gxTv_SdtUsuario_Usuario_cargouocod ;
         }

         set {
            gxTv_SdtUsuario_Usuario_cargouocod_N = 0;
            gxTv_SdtUsuario_Usuario_cargouocod = (int)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_cargouocod_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_cargouocod_N = 1;
         gxTv_SdtUsuario_Usuario_cargouocod = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_cargouocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_CargoUONom" )]
      [  XmlElement( ElementName = "Usuario_CargoUONom"   )]
      public String gxTpr_Usuario_cargouonom
      {
         get {
            return gxTv_SdtUsuario_Usuario_cargouonom ;
         }

         set {
            gxTv_SdtUsuario_Usuario_cargouonom_N = 0;
            gxTv_SdtUsuario_Usuario_cargouonom = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_cargouonom_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_cargouonom_N = 1;
         gxTv_SdtUsuario_Usuario_cargouonom = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_cargouonom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Entidade" )]
      [  XmlElement( ElementName = "Usuario_Entidade"   )]
      public String gxTpr_Usuario_entidade
      {
         get {
            return gxTv_SdtUsuario_Usuario_entidade ;
         }

         set {
            gxTv_SdtUsuario_Usuario_entidade = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_entidade_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_entidade = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_entidade_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaCod" )]
      [  XmlElement( ElementName = "Usuario_PessoaCod"   )]
      public int gxTpr_Usuario_pessoacod
      {
         get {
            return gxTv_SdtUsuario_Usuario_pessoacod ;
         }

         set {
            gxTv_SdtUsuario_Usuario_pessoacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_PessoaNom" )]
      [  XmlElement( ElementName = "Usuario_PessoaNom"   )]
      public String gxTpr_Usuario_pessoanom
      {
         get {
            return gxTv_SdtUsuario_Usuario_pessoanom ;
         }

         set {
            gxTv_SdtUsuario_Usuario_pessoanom_N = 0;
            gxTv_SdtUsuario_Usuario_pessoanom = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_pessoanom_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_pessoanom_N = 1;
         gxTv_SdtUsuario_Usuario_pessoanom = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_pessoanom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaTip" )]
      [  XmlElement( ElementName = "Usuario_PessoaTip"   )]
      public String gxTpr_Usuario_pessoatip
      {
         get {
            return gxTv_SdtUsuario_Usuario_pessoatip ;
         }

         set {
            gxTv_SdtUsuario_Usuario_pessoatip_N = 0;
            gxTv_SdtUsuario_Usuario_pessoatip = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_pessoatip_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_pessoatip_N = 1;
         gxTv_SdtUsuario_Usuario_pessoatip = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_pessoatip_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaDoc" )]
      [  XmlElement( ElementName = "Usuario_PessoaDoc"   )]
      public String gxTpr_Usuario_pessoadoc
      {
         get {
            return gxTv_SdtUsuario_Usuario_pessoadoc ;
         }

         set {
            gxTv_SdtUsuario_Usuario_pessoadoc_N = 0;
            gxTv_SdtUsuario_Usuario_pessoadoc = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_pessoadoc_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_pessoadoc_N = 1;
         gxTv_SdtUsuario_Usuario_pessoadoc = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_pessoadoc_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaTelefone" )]
      [  XmlElement( ElementName = "Usuario_PessoaTelefone"   )]
      public String gxTpr_Usuario_pessoatelefone
      {
         get {
            return gxTv_SdtUsuario_Usuario_pessoatelefone ;
         }

         set {
            gxTv_SdtUsuario_Usuario_pessoatelefone_N = 0;
            gxTv_SdtUsuario_Usuario_pessoatelefone = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_pessoatelefone_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_pessoatelefone_N = 1;
         gxTv_SdtUsuario_Usuario_pessoatelefone = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_pessoatelefone_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_UserGamGuid" )]
      [  XmlElement( ElementName = "Usuario_UserGamGuid"   )]
      public String gxTpr_Usuario_usergamguid
      {
         get {
            return gxTv_SdtUsuario_Usuario_usergamguid ;
         }

         set {
            gxTv_SdtUsuario_Usuario_usergamguid = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_Nome" )]
      [  XmlElement( ElementName = "Usuario_Nome"   )]
      public String gxTpr_Usuario_nome
      {
         get {
            return gxTv_SdtUsuario_Usuario_nome ;
         }

         set {
            gxTv_SdtUsuario_Usuario_nome_N = 0;
            gxTv_SdtUsuario_Usuario_nome = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_nome_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_nome_N = 1;
         gxTv_SdtUsuario_Usuario_nome = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_nome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_EhContador" )]
      [  XmlElement( ElementName = "Usuario_EhContador"   )]
      public bool gxTpr_Usuario_ehcontador
      {
         get {
            return gxTv_SdtUsuario_Usuario_ehcontador ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ehcontador = value;
         }

      }

      [  SoapElement( ElementName = "Usuario_EhAuditorFM" )]
      [  XmlElement( ElementName = "Usuario_EhAuditorFM"   )]
      public bool gxTpr_Usuario_ehauditorfm
      {
         get {
            return gxTv_SdtUsuario_Usuario_ehauditorfm ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ehauditorfm = value;
         }

      }

      public void gxTv_SdtUsuario_Usuario_ehauditorfm_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_ehauditorfm = false;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_ehauditorfm_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_EhContratada" )]
      [  XmlElement( ElementName = "Usuario_EhContratada"   )]
      public bool gxTpr_Usuario_ehcontratada
      {
         get {
            return gxTv_SdtUsuario_Usuario_ehcontratada ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ehcontratada = value;
         }

      }

      [  SoapElement( ElementName = "Usuario_EhContratante" )]
      [  XmlElement( ElementName = "Usuario_EhContratante"   )]
      public bool gxTpr_Usuario_ehcontratante
      {
         get {
            return gxTv_SdtUsuario_Usuario_ehcontratante ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ehcontratante = value;
         }

      }

      [  SoapElement( ElementName = "Usuario_EhFinanceiro" )]
      [  XmlElement( ElementName = "Usuario_EhFinanceiro"   )]
      public bool gxTpr_Usuario_ehfinanceiro
      {
         get {
            return gxTv_SdtUsuario_Usuario_ehfinanceiro ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ehfinanceiro = value;
         }

      }

      [  SoapElement( ElementName = "Usuario_EhGestor" )]
      [  XmlElement( ElementName = "Usuario_EhGestor"   )]
      public bool gxTpr_Usuario_ehgestor
      {
         get {
            return gxTv_SdtUsuario_Usuario_ehgestor ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ehgestor = value;
         }

      }

      [  SoapElement( ElementName = "Usuario_EhPreposto" )]
      [  XmlElement( ElementName = "Usuario_EhPreposto"   )]
      public bool gxTpr_Usuario_ehpreposto
      {
         get {
            return gxTv_SdtUsuario_Usuario_ehpreposto ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ehpreposto = value;
         }

      }

      [  SoapElement( ElementName = "Usuario_CrtfPath" )]
      [  XmlElement( ElementName = "Usuario_CrtfPath"   )]
      public String gxTpr_Usuario_crtfpath
      {
         get {
            return gxTv_SdtUsuario_Usuario_crtfpath ;
         }

         set {
            gxTv_SdtUsuario_Usuario_crtfpath_N = 0;
            gxTv_SdtUsuario_Usuario_crtfpath = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_crtfpath_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_crtfpath_N = 1;
         gxTv_SdtUsuario_Usuario_crtfpath = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_crtfpath_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Notificar" )]
      [  XmlElement( ElementName = "Usuario_Notificar"   )]
      public String gxTpr_Usuario_notificar
      {
         get {
            return gxTv_SdtUsuario_Usuario_notificar ;
         }

         set {
            gxTv_SdtUsuario_Usuario_notificar_N = 0;
            gxTv_SdtUsuario_Usuario_notificar = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_notificar_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_notificar_N = 1;
         gxTv_SdtUsuario_Usuario_notificar = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_notificar_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Ativo" )]
      [  XmlElement( ElementName = "Usuario_Ativo"   )]
      public bool gxTpr_Usuario_ativo
      {
         get {
            return gxTv_SdtUsuario_Usuario_ativo ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Usuario_Email" )]
      [  XmlElement( ElementName = "Usuario_Email"   )]
      public String gxTpr_Usuario_email
      {
         get {
            return gxTv_SdtUsuario_Usuario_email ;
         }

         set {
            gxTv_SdtUsuario_Usuario_email_N = 0;
            gxTv_SdtUsuario_Usuario_email = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_email_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_email_N = 1;
         gxTv_SdtUsuario_Usuario_email = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_email_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Foto" )]
      [  XmlElement( ElementName = "Usuario_Foto"   )]
      [GxUpload()]
      public String gxTpr_Usuario_foto
      {
         get {
            return gxTv_SdtUsuario_Usuario_foto ;
         }

         set {
            gxTv_SdtUsuario_Usuario_foto_N = 0;
            gxTv_SdtUsuario_Usuario_foto = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_foto_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_foto_N = 1;
         gxTv_SdtUsuario_Usuario_foto = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_foto_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Foto_GXI" )]
      [  XmlElement( ElementName = "Usuario_Foto_GXI"   )]
      public String gxTpr_Usuario_foto_gxi
      {
         get {
            return gxTv_SdtUsuario_Usuario_foto_gxi ;
         }

         set {
            gxTv_SdtUsuario_Usuario_foto_gxi_N = 0;
            gxTv_SdtUsuario_Usuario_foto_gxi = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_foto_gxi_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_foto_gxi_N = 1;
         gxTv_SdtUsuario_Usuario_foto_gxi = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_foto_gxi_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_DeFerias" )]
      [  XmlElement( ElementName = "Usuario_DeFerias"   )]
      public bool gxTpr_Usuario_deferias
      {
         get {
            return gxTv_SdtUsuario_Usuario_deferias ;
         }

         set {
            gxTv_SdtUsuario_Usuario_deferias_N = 0;
            gxTv_SdtUsuario_Usuario_deferias = value;
         }

      }

      public void gxTv_SdtUsuario_Usuario_deferias_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_deferias_N = 1;
         gxTv_SdtUsuario_Usuario_deferias = false;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_deferias_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_UltimaArea" )]
      [  XmlElement( ElementName = "Usuario_UltimaArea"   )]
      public int gxTpr_Usuario_ultimaarea
      {
         get {
            return gxTv_SdtUsuario_Usuario_ultimaarea ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ultimaarea_N = 0;
            gxTv_SdtUsuario_Usuario_ultimaarea = (int)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_ultimaarea_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_ultimaarea_N = 1;
         gxTv_SdtUsuario_Usuario_ultimaarea = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_ultimaarea_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtUsuario_Mode ;
         }

         set {
            gxTv_SdtUsuario_Mode = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Mode_SetNull( )
      {
         gxTv_SdtUsuario_Mode = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtUsuario_Initialized ;
         }

         set {
            gxTv_SdtUsuario_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Initialized_SetNull( )
      {
         gxTv_SdtUsuario_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Nome_Z" )]
      [  XmlElement( ElementName = "Usuario_Nome_Z"   )]
      public String gxTpr_Usuario_nome_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_nome_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_nome_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_UserGamGuid_Z" )]
      [  XmlElement( ElementName = "Usuario_UserGamGuid_Z"   )]
      public String gxTpr_Usuario_usergamguid_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_usergamguid_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_usergamguid_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_usergamguid_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_usergamguid_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_usergamguid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Codigo_Z" )]
      [  XmlElement( ElementName = "Usuario_Codigo_Z"   )]
      public int gxTpr_Usuario_codigo_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_codigo_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_codigo_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_CargoCod_Z" )]
      [  XmlElement( ElementName = "Usuario_CargoCod_Z"   )]
      public int gxTpr_Usuario_cargocod_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_cargocod_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_cargocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_cargocod_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_cargocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_cargocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_CargoNom_Z" )]
      [  XmlElement( ElementName = "Usuario_CargoNom_Z"   )]
      public String gxTpr_Usuario_cargonom_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_cargonom_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_cargonom_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_cargonom_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_cargonom_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_cargonom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_CargoUOCod_Z" )]
      [  XmlElement( ElementName = "Usuario_CargoUOCod_Z"   )]
      public int gxTpr_Usuario_cargouocod_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_cargouocod_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_cargouocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_cargouocod_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_cargouocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_cargouocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_CargoUONom_Z" )]
      [  XmlElement( ElementName = "Usuario_CargoUONom_Z"   )]
      public String gxTpr_Usuario_cargouonom_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_cargouonom_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_cargouonom_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_cargouonom_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_cargouonom_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_cargouonom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Entidade_Z" )]
      [  XmlElement( ElementName = "Usuario_Entidade_Z"   )]
      public String gxTpr_Usuario_entidade_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_entidade_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_entidade_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_entidade_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_entidade_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_entidade_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaCod_Z" )]
      [  XmlElement( ElementName = "Usuario_PessoaCod_Z"   )]
      public int gxTpr_Usuario_pessoacod_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_pessoacod_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_pessoacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_pessoacod_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_pessoacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_pessoacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaNom_Z" )]
      [  XmlElement( ElementName = "Usuario_PessoaNom_Z"   )]
      public String gxTpr_Usuario_pessoanom_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_pessoanom_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_pessoanom_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_pessoanom_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_pessoanom_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_pessoanom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaTip_Z" )]
      [  XmlElement( ElementName = "Usuario_PessoaTip_Z"   )]
      public String gxTpr_Usuario_pessoatip_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_pessoatip_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_pessoatip_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_pessoatip_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_pessoatip_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_pessoatip_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaDoc_Z" )]
      [  XmlElement( ElementName = "Usuario_PessoaDoc_Z"   )]
      public String gxTpr_Usuario_pessoadoc_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_pessoadoc_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_pessoadoc_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_pessoadoc_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_pessoadoc_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_pessoadoc_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaTelefone_Z" )]
      [  XmlElement( ElementName = "Usuario_PessoaTelefone_Z"   )]
      public String gxTpr_Usuario_pessoatelefone_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_pessoatelefone_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_pessoatelefone_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_pessoatelefone_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_pessoatelefone_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_pessoatelefone_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_EhContador_Z" )]
      [  XmlElement( ElementName = "Usuario_EhContador_Z"   )]
      public bool gxTpr_Usuario_ehcontador_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_ehcontador_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ehcontador_Z = value;
         }

      }

      public void gxTv_SdtUsuario_Usuario_ehcontador_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_ehcontador_Z = false;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_ehcontador_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_EhAuditorFM_Z" )]
      [  XmlElement( ElementName = "Usuario_EhAuditorFM_Z"   )]
      public bool gxTpr_Usuario_ehauditorfm_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_ehauditorfm_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ehauditorfm_Z = value;
         }

      }

      public void gxTv_SdtUsuario_Usuario_ehauditorfm_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_ehauditorfm_Z = false;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_ehauditorfm_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_EhContratada_Z" )]
      [  XmlElement( ElementName = "Usuario_EhContratada_Z"   )]
      public bool gxTpr_Usuario_ehcontratada_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_ehcontratada_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ehcontratada_Z = value;
         }

      }

      public void gxTv_SdtUsuario_Usuario_ehcontratada_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_ehcontratada_Z = false;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_ehcontratada_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_EhContratante_Z" )]
      [  XmlElement( ElementName = "Usuario_EhContratante_Z"   )]
      public bool gxTpr_Usuario_ehcontratante_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_ehcontratante_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ehcontratante_Z = value;
         }

      }

      public void gxTv_SdtUsuario_Usuario_ehcontratante_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_ehcontratante_Z = false;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_ehcontratante_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_EhFinanceiro_Z" )]
      [  XmlElement( ElementName = "Usuario_EhFinanceiro_Z"   )]
      public bool gxTpr_Usuario_ehfinanceiro_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_ehfinanceiro_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ehfinanceiro_Z = value;
         }

      }

      public void gxTv_SdtUsuario_Usuario_ehfinanceiro_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_ehfinanceiro_Z = false;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_ehfinanceiro_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_EhGestor_Z" )]
      [  XmlElement( ElementName = "Usuario_EhGestor_Z"   )]
      public bool gxTpr_Usuario_ehgestor_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_ehgestor_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ehgestor_Z = value;
         }

      }

      public void gxTv_SdtUsuario_Usuario_ehgestor_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_ehgestor_Z = false;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_ehgestor_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_EhPreposto_Z" )]
      [  XmlElement( ElementName = "Usuario_EhPreposto_Z"   )]
      public bool gxTpr_Usuario_ehpreposto_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_ehpreposto_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ehpreposto_Z = value;
         }

      }

      public void gxTv_SdtUsuario_Usuario_ehpreposto_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_ehpreposto_Z = false;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_ehpreposto_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_CrtfPath_Z" )]
      [  XmlElement( ElementName = "Usuario_CrtfPath_Z"   )]
      public String gxTpr_Usuario_crtfpath_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_crtfpath_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_crtfpath_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_crtfpath_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_crtfpath_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_crtfpath_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Notificar_Z" )]
      [  XmlElement( ElementName = "Usuario_Notificar_Z"   )]
      public String gxTpr_Usuario_notificar_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_notificar_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_notificar_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_notificar_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_notificar_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_notificar_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Ativo_Z" )]
      [  XmlElement( ElementName = "Usuario_Ativo_Z"   )]
      public bool gxTpr_Usuario_ativo_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_ativo_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ativo_Z = value;
         }

      }

      public void gxTv_SdtUsuario_Usuario_ativo_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Email_Z" )]
      [  XmlElement( ElementName = "Usuario_Email_Z"   )]
      public String gxTpr_Usuario_email_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_email_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_email_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_email_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_email_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_email_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_DeFerias_Z" )]
      [  XmlElement( ElementName = "Usuario_DeFerias_Z"   )]
      public bool gxTpr_Usuario_deferias_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_deferias_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_deferias_Z = value;
         }

      }

      public void gxTv_SdtUsuario_Usuario_deferias_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_deferias_Z = false;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_deferias_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_UltimaArea_Z" )]
      [  XmlElement( ElementName = "Usuario_UltimaArea_Z"   )]
      public int gxTpr_Usuario_ultimaarea_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_ultimaarea_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ultimaarea_Z = (int)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_ultimaarea_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_ultimaarea_Z = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_ultimaarea_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Foto_GXI_Z" )]
      [  XmlElement( ElementName = "Usuario_Foto_GXI_Z"   )]
      public String gxTpr_Usuario_foto_gxi_Z
      {
         get {
            return gxTv_SdtUsuario_Usuario_foto_gxi_Z ;
         }

         set {
            gxTv_SdtUsuario_Usuario_foto_gxi_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_foto_gxi_Z_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_foto_gxi_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_foto_gxi_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Codigo_N" )]
      [  XmlElement( ElementName = "Usuario_Codigo_N"   )]
      public short gxTpr_Usuario_codigo_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_codigo_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_codigo_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_CargoCod_N" )]
      [  XmlElement( ElementName = "Usuario_CargoCod_N"   )]
      public short gxTpr_Usuario_cargocod_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_cargocod_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_cargocod_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_cargocod_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_cargocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_cargocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_CargoNom_N" )]
      [  XmlElement( ElementName = "Usuario_CargoNom_N"   )]
      public short gxTpr_Usuario_cargonom_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_cargonom_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_cargonom_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_cargonom_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_cargonom_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_cargonom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_CargoUOCod_N" )]
      [  XmlElement( ElementName = "Usuario_CargoUOCod_N"   )]
      public short gxTpr_Usuario_cargouocod_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_cargouocod_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_cargouocod_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_cargouocod_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_cargouocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_cargouocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_CargoUONom_N" )]
      [  XmlElement( ElementName = "Usuario_CargoUONom_N"   )]
      public short gxTpr_Usuario_cargouonom_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_cargouonom_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_cargouonom_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_cargouonom_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_cargouonom_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_cargouonom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaNom_N" )]
      [  XmlElement( ElementName = "Usuario_PessoaNom_N"   )]
      public short gxTpr_Usuario_pessoanom_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_pessoanom_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_pessoanom_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_pessoanom_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_pessoanom_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_pessoanom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaTip_N" )]
      [  XmlElement( ElementName = "Usuario_PessoaTip_N"   )]
      public short gxTpr_Usuario_pessoatip_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_pessoatip_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_pessoatip_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_pessoatip_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_pessoatip_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_pessoatip_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaDoc_N" )]
      [  XmlElement( ElementName = "Usuario_PessoaDoc_N"   )]
      public short gxTpr_Usuario_pessoadoc_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_pessoadoc_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_pessoadoc_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_pessoadoc_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_pessoadoc_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_pessoadoc_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaTelefone_N" )]
      [  XmlElement( ElementName = "Usuario_PessoaTelefone_N"   )]
      public short gxTpr_Usuario_pessoatelefone_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_pessoatelefone_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_pessoatelefone_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_pessoatelefone_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_pessoatelefone_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_pessoatelefone_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Nome_N" )]
      [  XmlElement( ElementName = "Usuario_Nome_N"   )]
      public short gxTpr_Usuario_nome_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_nome_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_nome_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_nome_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_nome_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_nome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_CrtfPath_N" )]
      [  XmlElement( ElementName = "Usuario_CrtfPath_N"   )]
      public short gxTpr_Usuario_crtfpath_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_crtfpath_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_crtfpath_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_crtfpath_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_crtfpath_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_crtfpath_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Notificar_N" )]
      [  XmlElement( ElementName = "Usuario_Notificar_N"   )]
      public short gxTpr_Usuario_notificar_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_notificar_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_notificar_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_notificar_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_notificar_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_notificar_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Email_N" )]
      [  XmlElement( ElementName = "Usuario_Email_N"   )]
      public short gxTpr_Usuario_email_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_email_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_email_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_email_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_email_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_email_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Foto_N" )]
      [  XmlElement( ElementName = "Usuario_Foto_N"   )]
      public short gxTpr_Usuario_foto_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_foto_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_foto_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_foto_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_foto_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_foto_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_DeFerias_N" )]
      [  XmlElement( ElementName = "Usuario_DeFerias_N"   )]
      public short gxTpr_Usuario_deferias_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_deferias_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_deferias_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_deferias_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_deferias_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_deferias_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_UltimaArea_N" )]
      [  XmlElement( ElementName = "Usuario_UltimaArea_N"   )]
      public short gxTpr_Usuario_ultimaarea_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_ultimaarea_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_ultimaarea_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_ultimaarea_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_ultimaarea_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_ultimaarea_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Foto_GXI_N" )]
      [  XmlElement( ElementName = "Usuario_Foto_GXI_N"   )]
      public short gxTpr_Usuario_foto_gxi_N
      {
         get {
            return gxTv_SdtUsuario_Usuario_foto_gxi_N ;
         }

         set {
            gxTv_SdtUsuario_Usuario_foto_gxi_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuario_Usuario_foto_gxi_N_SetNull( )
      {
         gxTv_SdtUsuario_Usuario_foto_gxi_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuario_Usuario_foto_gxi_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtUsuario_Usuario_cargonom = "";
         gxTv_SdtUsuario_Usuario_cargouonom = "";
         gxTv_SdtUsuario_Usuario_entidade = "";
         gxTv_SdtUsuario_Usuario_pessoanom = "";
         gxTv_SdtUsuario_Usuario_pessoatip = "";
         gxTv_SdtUsuario_Usuario_pessoadoc = "";
         gxTv_SdtUsuario_Usuario_pessoatelefone = "";
         gxTv_SdtUsuario_Usuario_usergamguid = "";
         gxTv_SdtUsuario_Usuario_nome = "";
         gxTv_SdtUsuario_Usuario_crtfpath = "";
         gxTv_SdtUsuario_Usuario_notificar = "A";
         gxTv_SdtUsuario_Usuario_email = "";
         gxTv_SdtUsuario_Usuario_foto = "";
         gxTv_SdtUsuario_Usuario_foto_gxi = "";
         gxTv_SdtUsuario_Mode = "";
         gxTv_SdtUsuario_Usuario_nome_Z = "";
         gxTv_SdtUsuario_Usuario_usergamguid_Z = "";
         gxTv_SdtUsuario_Usuario_cargonom_Z = "";
         gxTv_SdtUsuario_Usuario_cargouonom_Z = "";
         gxTv_SdtUsuario_Usuario_entidade_Z = "";
         gxTv_SdtUsuario_Usuario_pessoanom_Z = "";
         gxTv_SdtUsuario_Usuario_pessoatip_Z = "";
         gxTv_SdtUsuario_Usuario_pessoadoc_Z = "";
         gxTv_SdtUsuario_Usuario_pessoatelefone_Z = "";
         gxTv_SdtUsuario_Usuario_crtfpath_Z = "";
         gxTv_SdtUsuario_Usuario_notificar_Z = "";
         gxTv_SdtUsuario_Usuario_email_Z = "";
         gxTv_SdtUsuario_Usuario_foto_gxi_Z = "";
         gxTv_SdtUsuario_Usuario_ehgestor = false;
         gxTv_SdtUsuario_Usuario_ehpreposto = false;
         gxTv_SdtUsuario_Usuario_ativo = true;
         gxTv_SdtUsuario_Usuario_deferias = false;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "usuario", "GeneXus.Programs.usuario_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtUsuario_Initialized ;
      private short gxTv_SdtUsuario_Usuario_codigo_N ;
      private short gxTv_SdtUsuario_Usuario_cargocod_N ;
      private short gxTv_SdtUsuario_Usuario_cargonom_N ;
      private short gxTv_SdtUsuario_Usuario_cargouocod_N ;
      private short gxTv_SdtUsuario_Usuario_cargouonom_N ;
      private short gxTv_SdtUsuario_Usuario_pessoanom_N ;
      private short gxTv_SdtUsuario_Usuario_pessoatip_N ;
      private short gxTv_SdtUsuario_Usuario_pessoadoc_N ;
      private short gxTv_SdtUsuario_Usuario_pessoatelefone_N ;
      private short gxTv_SdtUsuario_Usuario_nome_N ;
      private short gxTv_SdtUsuario_Usuario_crtfpath_N ;
      private short gxTv_SdtUsuario_Usuario_notificar_N ;
      private short gxTv_SdtUsuario_Usuario_email_N ;
      private short gxTv_SdtUsuario_Usuario_foto_N ;
      private short gxTv_SdtUsuario_Usuario_deferias_N ;
      private short gxTv_SdtUsuario_Usuario_ultimaarea_N ;
      private short gxTv_SdtUsuario_Usuario_foto_gxi_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtUsuario_Usuario_codigo ;
      private int gxTv_SdtUsuario_Usuario_cargocod ;
      private int gxTv_SdtUsuario_Usuario_cargouocod ;
      private int gxTv_SdtUsuario_Usuario_pessoacod ;
      private int gxTv_SdtUsuario_Usuario_ultimaarea ;
      private int gxTv_SdtUsuario_Usuario_codigo_Z ;
      private int gxTv_SdtUsuario_Usuario_cargocod_Z ;
      private int gxTv_SdtUsuario_Usuario_cargouocod_Z ;
      private int gxTv_SdtUsuario_Usuario_pessoacod_Z ;
      private int gxTv_SdtUsuario_Usuario_ultimaarea_Z ;
      private String gxTv_SdtUsuario_Usuario_cargouonom ;
      private String gxTv_SdtUsuario_Usuario_entidade ;
      private String gxTv_SdtUsuario_Usuario_pessoanom ;
      private String gxTv_SdtUsuario_Usuario_pessoatip ;
      private String gxTv_SdtUsuario_Usuario_pessoatelefone ;
      private String gxTv_SdtUsuario_Usuario_usergamguid ;
      private String gxTv_SdtUsuario_Usuario_nome ;
      private String gxTv_SdtUsuario_Usuario_notificar ;
      private String gxTv_SdtUsuario_Mode ;
      private String gxTv_SdtUsuario_Usuario_nome_Z ;
      private String gxTv_SdtUsuario_Usuario_usergamguid_Z ;
      private String gxTv_SdtUsuario_Usuario_cargouonom_Z ;
      private String gxTv_SdtUsuario_Usuario_entidade_Z ;
      private String gxTv_SdtUsuario_Usuario_pessoanom_Z ;
      private String gxTv_SdtUsuario_Usuario_pessoatip_Z ;
      private String gxTv_SdtUsuario_Usuario_pessoatelefone_Z ;
      private String gxTv_SdtUsuario_Usuario_notificar_Z ;
      private String sTagName ;
      private bool gxTv_SdtUsuario_Usuario_ehcontador ;
      private bool gxTv_SdtUsuario_Usuario_ehauditorfm ;
      private bool gxTv_SdtUsuario_Usuario_ehcontratada ;
      private bool gxTv_SdtUsuario_Usuario_ehcontratante ;
      private bool gxTv_SdtUsuario_Usuario_ehfinanceiro ;
      private bool gxTv_SdtUsuario_Usuario_ehgestor ;
      private bool gxTv_SdtUsuario_Usuario_ehpreposto ;
      private bool gxTv_SdtUsuario_Usuario_ativo ;
      private bool gxTv_SdtUsuario_Usuario_deferias ;
      private bool gxTv_SdtUsuario_Usuario_ehcontador_Z ;
      private bool gxTv_SdtUsuario_Usuario_ehauditorfm_Z ;
      private bool gxTv_SdtUsuario_Usuario_ehcontratada_Z ;
      private bool gxTv_SdtUsuario_Usuario_ehcontratante_Z ;
      private bool gxTv_SdtUsuario_Usuario_ehfinanceiro_Z ;
      private bool gxTv_SdtUsuario_Usuario_ehgestor_Z ;
      private bool gxTv_SdtUsuario_Usuario_ehpreposto_Z ;
      private bool gxTv_SdtUsuario_Usuario_ativo_Z ;
      private bool gxTv_SdtUsuario_Usuario_deferias_Z ;
      private String gxTv_SdtUsuario_Usuario_cargonom ;
      private String gxTv_SdtUsuario_Usuario_pessoadoc ;
      private String gxTv_SdtUsuario_Usuario_crtfpath ;
      private String gxTv_SdtUsuario_Usuario_email ;
      private String gxTv_SdtUsuario_Usuario_foto_gxi ;
      private String gxTv_SdtUsuario_Usuario_cargonom_Z ;
      private String gxTv_SdtUsuario_Usuario_pessoadoc_Z ;
      private String gxTv_SdtUsuario_Usuario_crtfpath_Z ;
      private String gxTv_SdtUsuario_Usuario_email_Z ;
      private String gxTv_SdtUsuario_Usuario_foto_gxi_Z ;
      private String gxTv_SdtUsuario_Usuario_foto ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Usuario", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtUsuario_RESTInterface : GxGenericCollectionItem<SdtUsuario>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtUsuario_RESTInterface( ) : base()
      {
      }

      public SdtUsuario_RESTInterface( SdtUsuario psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Usuario_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Usuario_codigo
      {
         get {
            return sdt.gxTpr_Usuario_codigo ;
         }

         set {
            sdt.gxTpr_Usuario_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Usuario_CargoCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Usuario_cargocod
      {
         get {
            return sdt.gxTpr_Usuario_cargocod ;
         }

         set {
            sdt.gxTpr_Usuario_cargocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Usuario_CargoNom" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Usuario_cargonom
      {
         get {
            return sdt.gxTpr_Usuario_cargonom ;
         }

         set {
            sdt.gxTpr_Usuario_cargonom = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_CargoUOCod" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Usuario_cargouocod
      {
         get {
            return sdt.gxTpr_Usuario_cargouocod ;
         }

         set {
            sdt.gxTpr_Usuario_cargouocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Usuario_CargoUONom" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Usuario_cargouonom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_cargouonom) ;
         }

         set {
            sdt.gxTpr_Usuario_cargouonom = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_Entidade" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Usuario_entidade
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_entidade) ;
         }

         set {
            sdt.gxTpr_Usuario_entidade = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_PessoaCod" , Order = 6 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Usuario_pessoacod
      {
         get {
            return sdt.gxTpr_Usuario_pessoacod ;
         }

         set {
            sdt.gxTpr_Usuario_pessoacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Usuario_PessoaNom" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Usuario_pessoanom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_pessoanom) ;
         }

         set {
            sdt.gxTpr_Usuario_pessoanom = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_PessoaTip" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Usuario_pessoatip
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_pessoatip) ;
         }

         set {
            sdt.gxTpr_Usuario_pessoatip = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_PessoaDoc" , Order = 9 )]
      [GxSeudo()]
      public String gxTpr_Usuario_pessoadoc
      {
         get {
            return sdt.gxTpr_Usuario_pessoadoc ;
         }

         set {
            sdt.gxTpr_Usuario_pessoadoc = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_PessoaTelefone" , Order = 10 )]
      [GxSeudo()]
      public String gxTpr_Usuario_pessoatelefone
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_pessoatelefone) ;
         }

         set {
            sdt.gxTpr_Usuario_pessoatelefone = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_UserGamGuid" , Order = 11 )]
      [GxSeudo()]
      public String gxTpr_Usuario_usergamguid
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_usergamguid) ;
         }

         set {
            sdt.gxTpr_Usuario_usergamguid = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_Nome" , Order = 12 )]
      [GxSeudo()]
      public String gxTpr_Usuario_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_nome) ;
         }

         set {
            sdt.gxTpr_Usuario_nome = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_EhContador" , Order = 13 )]
      [GxSeudo()]
      public bool gxTpr_Usuario_ehcontador
      {
         get {
            return sdt.gxTpr_Usuario_ehcontador ;
         }

         set {
            sdt.gxTpr_Usuario_ehcontador = value;
         }

      }

      [DataMember( Name = "Usuario_EhAuditorFM" , Order = 14 )]
      [GxSeudo()]
      public bool gxTpr_Usuario_ehauditorfm
      {
         get {
            return sdt.gxTpr_Usuario_ehauditorfm ;
         }

         set {
            sdt.gxTpr_Usuario_ehauditorfm = value;
         }

      }

      [DataMember( Name = "Usuario_EhContratada" , Order = 15 )]
      [GxSeudo()]
      public bool gxTpr_Usuario_ehcontratada
      {
         get {
            return sdt.gxTpr_Usuario_ehcontratada ;
         }

         set {
            sdt.gxTpr_Usuario_ehcontratada = value;
         }

      }

      [DataMember( Name = "Usuario_EhContratante" , Order = 16 )]
      [GxSeudo()]
      public bool gxTpr_Usuario_ehcontratante
      {
         get {
            return sdt.gxTpr_Usuario_ehcontratante ;
         }

         set {
            sdt.gxTpr_Usuario_ehcontratante = value;
         }

      }

      [DataMember( Name = "Usuario_EhFinanceiro" , Order = 17 )]
      [GxSeudo()]
      public bool gxTpr_Usuario_ehfinanceiro
      {
         get {
            return sdt.gxTpr_Usuario_ehfinanceiro ;
         }

         set {
            sdt.gxTpr_Usuario_ehfinanceiro = value;
         }

      }

      [DataMember( Name = "Usuario_EhGestor" , Order = 18 )]
      [GxSeudo()]
      public bool gxTpr_Usuario_ehgestor
      {
         get {
            return sdt.gxTpr_Usuario_ehgestor ;
         }

         set {
            sdt.gxTpr_Usuario_ehgestor = value;
         }

      }

      [DataMember( Name = "Usuario_EhPreposto" , Order = 19 )]
      [GxSeudo()]
      public bool gxTpr_Usuario_ehpreposto
      {
         get {
            return sdt.gxTpr_Usuario_ehpreposto ;
         }

         set {
            sdt.gxTpr_Usuario_ehpreposto = value;
         }

      }

      [DataMember( Name = "Usuario_CrtfPath" , Order = 20 )]
      [GxSeudo()]
      public String gxTpr_Usuario_crtfpath
      {
         get {
            return sdt.gxTpr_Usuario_crtfpath ;
         }

         set {
            sdt.gxTpr_Usuario_crtfpath = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_Notificar" , Order = 21 )]
      [GxSeudo()]
      public String gxTpr_Usuario_notificar
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_notificar) ;
         }

         set {
            sdt.gxTpr_Usuario_notificar = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_Ativo" , Order = 22 )]
      [GxSeudo()]
      public bool gxTpr_Usuario_ativo
      {
         get {
            return sdt.gxTpr_Usuario_ativo ;
         }

         set {
            sdt.gxTpr_Usuario_ativo = value;
         }

      }

      [DataMember( Name = "Usuario_Email" , Order = 23 )]
      [GxSeudo()]
      public String gxTpr_Usuario_email
      {
         get {
            return sdt.gxTpr_Usuario_email ;
         }

         set {
            sdt.gxTpr_Usuario_email = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_Foto" , Order = 24 )]
      [GxUpload()]
      public String gxTpr_Usuario_foto
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Usuario_foto)) ? PathUtil.RelativePath( sdt.gxTpr_Usuario_foto) : StringUtil.RTrim( sdt.gxTpr_Usuario_foto_gxi)) ;
         }

         set {
            sdt.gxTpr_Usuario_foto = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_DeFerias" , Order = 26 )]
      [GxSeudo()]
      public bool gxTpr_Usuario_deferias
      {
         get {
            return sdt.gxTpr_Usuario_deferias ;
         }

         set {
            sdt.gxTpr_Usuario_deferias = value;
         }

      }

      [DataMember( Name = "Usuario_UltimaArea" , Order = 27 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Usuario_ultimaarea
      {
         get {
            return sdt.gxTpr_Usuario_ultimaarea ;
         }

         set {
            sdt.gxTpr_Usuario_ultimaarea = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtUsuario sdt
      {
         get {
            return (SdtUsuario)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtUsuario() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 74 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
