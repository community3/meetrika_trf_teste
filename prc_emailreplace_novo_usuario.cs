/*
               File: PRC_EmailReplace_Novo_Usuario
        Description: Stub for PRC_EmailReplace_Novo_Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:53.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_emailreplace_novo_usuario : GXProcedure
   {
      public prc_emailreplace_novo_usuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public prc_emailreplace_novo_usuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref Guid aP0_Email_Instancia_Guid )
      {
         this.AV2Email_Instancia_Guid = aP0_Email_Instancia_Guid;
         initialize();
         executePrivate();
         aP0_Email_Instancia_Guid=this.AV2Email_Instancia_Guid;
      }

      public Guid executeUdp( )
      {
         this.AV2Email_Instancia_Guid = aP0_Email_Instancia_Guid;
         initialize();
         executePrivate();
         aP0_Email_Instancia_Guid=this.AV2Email_Instancia_Guid;
         return AV2Email_Instancia_Guid ;
      }

      public void executeSubmit( ref Guid aP0_Email_Instancia_Guid )
      {
         prc_emailreplace_novo_usuario objprc_emailreplace_novo_usuario;
         objprc_emailreplace_novo_usuario = new prc_emailreplace_novo_usuario();
         objprc_emailreplace_novo_usuario.AV2Email_Instancia_Guid = aP0_Email_Instancia_Guid;
         objprc_emailreplace_novo_usuario.context.SetSubmitInitialConfig(context);
         objprc_emailreplace_novo_usuario.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_emailreplace_novo_usuario);
         aP0_Email_Instancia_Guid=this.AV2Email_Instancia_Guid;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_emailreplace_novo_usuario)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(Guid)AV2Email_Instancia_Guid} ;
         ClassLoader.Execute("aprc_emailreplace_novo_usuario","GeneXus.Programs.aprc_emailreplace_novo_usuario", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 1 ) )
         {
            AV2Email_Instancia_Guid = (Guid)((args[0])) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private Guid AV2Email_Instancia_Guid ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Guid aP0_Email_Instancia_Guid ;
      private Object[] args ;
   }

}
