/*
               File: TipodeConta
        Description: Plano de Contas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:51:43.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tipodeconta : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"TIPODECONTA_CONTAPAICOD") == 0 )
         {
            AV13TipodeConta_Tipo = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TipodeConta_Tipo", AV13TipodeConta_Tipo);
            AV7TipodeConta_Codigo = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TipodeConta_Codigo", AV7TipodeConta_Codigo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTIPODECONTA_CODIGO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TipodeConta_Codigo, ""))));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLATIPODECONTA_CONTAPAICOD2R109( AV13TipodeConta_Tipo, AV7TipodeConta_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_16") == 0 )
         {
            A885TipoDeConta_ContaPaiCod = GetNextPar( );
            n885TipoDeConta_ContaPaiCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A885TipoDeConta_ContaPaiCod", A885TipoDeConta_ContaPaiCod);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_16( A885TipoDeConta_ContaPaiCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7TipodeConta_Codigo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TipodeConta_Codigo", AV7TipodeConta_Codigo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTIPODECONTA_CODIGO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TipodeConta_Codigo, ""))));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbavTipodeconta_tipo.Name = "vTIPODECONTA_TIPO";
         cmbavTipodeconta_tipo.WebTags = "";
         cmbavTipodeconta_tipo.addItem("", "(Nenhum)", 0);
         cmbavTipodeconta_tipo.addItem("D", "D�bito", 0);
         cmbavTipodeconta_tipo.addItem("C", "Cr�dito", 0);
         if ( cmbavTipodeconta_tipo.ItemCount > 0 )
         {
            AV13TipodeConta_Tipo = cmbavTipodeconta_tipo.getValidValue(AV13TipodeConta_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TipodeConta_Tipo", AV13TipodeConta_Tipo);
         }
         dynTipoDeConta_ContaPaiCod.Name = "TIPODECONTA_CONTAPAICOD";
         dynTipoDeConta_ContaPaiCod.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Plano de Contas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtTipodeConta_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public tipodeconta( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public tipodeconta( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           String aP1_TipodeConta_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7TipodeConta_Codigo = aP1_TipodeConta_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavTipodeconta_tipo = new GXCombobox();
         dynTipoDeConta_ContaPaiCod = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavTipodeconta_tipo.ItemCount > 0 )
         {
            AV13TipodeConta_Tipo = cmbavTipodeconta_tipo.getValidValue(AV13TipodeConta_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TipodeConta_Tipo", AV13TipodeConta_Tipo);
         }
         if ( dynTipoDeConta_ContaPaiCod.ItemCount > 0 )
         {
            A885TipoDeConta_ContaPaiCod = dynTipoDeConta_ContaPaiCod.getValidValue(A885TipoDeConta_ContaPaiCod);
            n885TipoDeConta_ContaPaiCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A885TipoDeConta_ContaPaiCod", A885TipoDeConta_ContaPaiCod);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2R109( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2R109e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2R109( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2R109( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2R109e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_41_2R109( true) ;
         }
         return  ;
      }

      protected void wb_table3_41_2R109e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2R109e( true) ;
         }
         else
         {
            wb_table1_2_2R109e( false) ;
         }
      }

      protected void wb_table3_41_2R109( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_41_2R109e( true) ;
         }
         else
         {
            wb_table3_41_2R109e( false) ;
         }
      }

      protected void wb_table2_5_2R109( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2R109( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2R109e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2R109e( true) ;
         }
         else
         {
            wb_table2_5_2R109e( false) ;
         }
      }

      protected void wb_table4_13_2R109( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipodeconta_codigo_Internalname, "Conta", "", "", lblTextblocktipodeconta_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTipodeConta_Codigo_Internalname, StringUtil.RTrim( A870TipodeConta_Codigo), StringUtil.RTrim( context.localUtil.Format( A870TipodeConta_Codigo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipodeConta_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtTipodeConta_Codigo_Enabled, 1, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipodeconta_nome_Internalname, "Nome", "", "", lblTextblocktipodeconta_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTipodeConta_Nome_Internalname, StringUtil.RTrim( A871TipodeConta_Nome), StringUtil.RTrim( context.localUtil.Format( A871TipodeConta_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipodeConta_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtTipodeConta_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_TipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipodeconta_descricao_Internalname, "Descri��o", "", "", lblTextblocktipodeconta_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTipodeConta_Descricao_Internalname, A872TipodeConta_Descricao, StringUtil.RTrim( context.localUtil.Format( A872TipodeConta_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipodeConta_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtTipodeConta_Descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_TipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipodeconta_tipo_Internalname, "Tipo", "", "", lblTextblocktipodeconta_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavTipodeconta_tipo, cmbavTipodeconta_tipo_Internalname, StringUtil.RTrim( AV13TipodeConta_Tipo), 1, cmbavTipodeconta_tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavTipodeconta_tipo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_TipodeConta.htm");
            cmbavTipodeconta_tipo.CurrentValue = StringUtil.RTrim( AV13TipodeConta_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTipodeconta_tipo_Internalname, "Values", (String)(cmbavTipodeconta_tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipodeconta_contapaicod_Internalname, "Conta Pai", "", "", lblTextblocktipodeconta_contapaicod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynTipoDeConta_ContaPaiCod, dynTipoDeConta_ContaPaiCod_Internalname, StringUtil.RTrim( A885TipoDeConta_ContaPaiCod), 1, dynTipoDeConta_ContaPaiCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, dynTipoDeConta_ContaPaiCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_TipodeConta.htm");
            dynTipoDeConta_ContaPaiCod.CurrentValue = StringUtil.RTrim( A885TipoDeConta_ContaPaiCod);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoDeConta_ContaPaiCod_Internalname, "Values", (String)(dynTipoDeConta_ContaPaiCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2R109e( true) ;
         }
         else
         {
            wb_table4_13_2R109e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112R2 */
         E112R2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A870TipodeConta_Codigo = cgiGet( edtTipodeConta_Codigo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A870TipodeConta_Codigo", A870TipodeConta_Codigo);
               A871TipodeConta_Nome = StringUtil.Upper( cgiGet( edtTipodeConta_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A871TipodeConta_Nome", A871TipodeConta_Nome);
               A872TipodeConta_Descricao = StringUtil.Upper( cgiGet( edtTipodeConta_Descricao_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A872TipodeConta_Descricao", A872TipodeConta_Descricao);
               cmbavTipodeconta_tipo.CurrentValue = cgiGet( cmbavTipodeconta_tipo_Internalname);
               AV13TipodeConta_Tipo = cgiGet( cmbavTipodeconta_tipo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TipodeConta_Tipo", AV13TipodeConta_Tipo);
               dynTipoDeConta_ContaPaiCod.CurrentValue = cgiGet( dynTipoDeConta_ContaPaiCod_Internalname);
               A885TipoDeConta_ContaPaiCod = cgiGet( dynTipoDeConta_ContaPaiCod_Internalname);
               n885TipoDeConta_ContaPaiCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A885TipoDeConta_ContaPaiCod", A885TipoDeConta_ContaPaiCod);
               n885TipoDeConta_ContaPaiCod = (String.IsNullOrEmpty(StringUtil.RTrim( A885TipoDeConta_ContaPaiCod)) ? true : false);
               /* Read saved values. */
               Z870TipodeConta_Codigo = cgiGet( "Z870TipodeConta_Codigo");
               Z871TipodeConta_Nome = cgiGet( "Z871TipodeConta_Nome");
               Z872TipodeConta_Descricao = cgiGet( "Z872TipodeConta_Descricao");
               Z873TipodeConta_Tipo = cgiGet( "Z873TipodeConta_Tipo");
               Z885TipoDeConta_ContaPaiCod = cgiGet( "Z885TipoDeConta_ContaPaiCod");
               n885TipoDeConta_ContaPaiCod = (String.IsNullOrEmpty(StringUtil.RTrim( A885TipoDeConta_ContaPaiCod)) ? true : false);
               A873TipodeConta_Tipo = cgiGet( "Z873TipodeConta_Tipo");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N885TipoDeConta_ContaPaiCod = cgiGet( "N885TipoDeConta_ContaPaiCod");
               n885TipoDeConta_ContaPaiCod = (String.IsNullOrEmpty(StringUtil.RTrim( A885TipoDeConta_ContaPaiCod)) ? true : false);
               AV7TipodeConta_Codigo = cgiGet( "vTIPODECONTA_CODIGO");
               AV11Insert_TipoDeConta_ContaPaiCod = cgiGet( "vINSERT_TIPODECONTA_CONTAPAICOD");
               A873TipodeConta_Tipo = cgiGet( "TIPODECONTA_TIPO");
               A886TipoDeConta_ContaPaiNome = cgiGet( "TIPODECONTA_CONTAPAINOME");
               n886TipoDeConta_ContaPaiNome = false;
               A887TipoDeConta_ContaPaiTipo = cgiGet( "TIPODECONTA_CONTAPAITIPO");
               n887TipoDeConta_ContaPaiTipo = false;
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "TipodeConta";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( StringUtil.StrCmp(A870TipodeConta_Codigo, Z870TipodeConta_Codigo) != 0 ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("tipodeconta:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A870TipodeConta_Codigo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A870TipodeConta_Codigo", A870TipodeConta_Codigo);
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode109 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode109;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound109 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_2R0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "TIPODECONTA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtTipodeConta_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112R2 */
                           E112R2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122R2 */
                           E122R2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E122R2 */
            E122R2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2R109( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2R109( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTipodeconta_tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTipodeconta_tipo.Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_2R0( )
      {
         BeforeValidate2R109( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2R109( ) ;
            }
            else
            {
               CheckExtendedTable2R109( ) ;
               CloseExtendedTableCursors2R109( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption2R0( )
      {
      }

      protected void E112R2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "TipoDeConta_ContaPaiCod") == 0 )
               {
                  AV11Insert_TipoDeConta_ContaPaiCod = AV12TrnContextAtt.gxTpr_Attributevalue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_TipoDeConta_ContaPaiCod", AV11Insert_TipoDeConta_ContaPaiCod);
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
      }

      protected void E122R2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwtipodeconta.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2R109( short GX_JID )
      {
         if ( ( GX_JID == 15 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z871TipodeConta_Nome = T002R3_A871TipodeConta_Nome[0];
               Z872TipodeConta_Descricao = T002R3_A872TipodeConta_Descricao[0];
               Z873TipodeConta_Tipo = T002R3_A873TipodeConta_Tipo[0];
               Z885TipoDeConta_ContaPaiCod = T002R3_A885TipoDeConta_ContaPaiCod[0];
            }
            else
            {
               Z871TipodeConta_Nome = A871TipodeConta_Nome;
               Z872TipodeConta_Descricao = A872TipodeConta_Descricao;
               Z873TipodeConta_Tipo = A873TipodeConta_Tipo;
               Z885TipoDeConta_ContaPaiCod = A885TipoDeConta_ContaPaiCod;
            }
         }
         if ( GX_JID == -15 )
         {
            Z870TipodeConta_Codigo = A870TipodeConta_Codigo;
            Z871TipodeConta_Nome = A871TipodeConta_Nome;
            Z872TipodeConta_Descricao = A872TipodeConta_Descricao;
            Z873TipodeConta_Tipo = A873TipodeConta_Tipo;
            Z885TipoDeConta_ContaPaiCod = A885TipoDeConta_ContaPaiCod;
            Z886TipoDeConta_ContaPaiNome = A886TipoDeConta_ContaPaiNome;
            Z887TipoDeConta_ContaPaiTipo = A887TipoDeConta_ContaPaiTipo;
         }
      }

      protected void standaloneNotModal( )
      {
         AV14Pgmname = "TipodeConta";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV7TipodeConta_Codigo)) )
         {
            A870TipodeConta_Codigo = AV7TipodeConta_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A870TipodeConta_Codigo", A870TipodeConta_Codigo);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV7TipodeConta_Codigo)) )
         {
            edtTipodeConta_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipodeConta_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipodeConta_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtTipodeConta_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipodeConta_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipodeConta_Codigo_Enabled), 5, 0)));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV7TipodeConta_Codigo)) )
         {
            edtTipodeConta_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipodeConta_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipodeConta_Codigo_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV11Insert_TipoDeConta_ContaPaiCod)) )
         {
            dynTipoDeConta_ContaPaiCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoDeConta_ContaPaiCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTipoDeConta_ContaPaiCod.Enabled), 5, 0)));
         }
         else
         {
            dynTipoDeConta_ContaPaiCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoDeConta_ContaPaiCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTipoDeConta_ContaPaiCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV11Insert_TipoDeConta_ContaPaiCod)) )
         {
            A885TipoDeConta_ContaPaiCod = AV11Insert_TipoDeConta_ContaPaiCod;
            n885TipoDeConta_ContaPaiCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A885TipoDeConta_ContaPaiCod", A885TipoDeConta_ContaPaiCod);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T002R4 */
            pr_default.execute(2, new Object[] {n885TipoDeConta_ContaPaiCod, A885TipoDeConta_ContaPaiCod});
            A886TipoDeConta_ContaPaiNome = T002R4_A886TipoDeConta_ContaPaiNome[0];
            n886TipoDeConta_ContaPaiNome = T002R4_n886TipoDeConta_ContaPaiNome[0];
            A887TipoDeConta_ContaPaiTipo = T002R4_A887TipoDeConta_ContaPaiTipo[0];
            n887TipoDeConta_ContaPaiTipo = T002R4_n887TipoDeConta_ContaPaiTipo[0];
            pr_default.close(2);
         }
      }

      protected void Load2R109( )
      {
         /* Using cursor T002R5 */
         pr_default.execute(3, new Object[] {A870TipodeConta_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound109 = 1;
            A871TipodeConta_Nome = T002R5_A871TipodeConta_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A871TipodeConta_Nome", A871TipodeConta_Nome);
            A872TipodeConta_Descricao = T002R5_A872TipodeConta_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A872TipodeConta_Descricao", A872TipodeConta_Descricao);
            A873TipodeConta_Tipo = T002R5_A873TipodeConta_Tipo[0];
            A886TipoDeConta_ContaPaiNome = T002R5_A886TipoDeConta_ContaPaiNome[0];
            n886TipoDeConta_ContaPaiNome = T002R5_n886TipoDeConta_ContaPaiNome[0];
            A887TipoDeConta_ContaPaiTipo = T002R5_A887TipoDeConta_ContaPaiTipo[0];
            n887TipoDeConta_ContaPaiTipo = T002R5_n887TipoDeConta_ContaPaiTipo[0];
            A885TipoDeConta_ContaPaiCod = T002R5_A885TipoDeConta_ContaPaiCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A885TipoDeConta_ContaPaiCod", A885TipoDeConta_ContaPaiCod);
            n885TipoDeConta_ContaPaiCod = T002R5_n885TipoDeConta_ContaPaiCod[0];
            ZM2R109( -15) ;
         }
         pr_default.close(3);
         OnLoadActions2R109( ) ;
      }

      protected void OnLoadActions2R109( )
      {
         GXATIPODECONTA_CONTAPAICOD_html2R109( AV13TipodeConta_Tipo, AV7TipodeConta_Codigo) ;
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV13TipodeConta_Tipo = A873TipodeConta_Tipo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TipodeConta_Tipo", AV13TipodeConta_Tipo);
         }
      }

      protected void CheckExtendedTable2R109( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A870TipodeConta_Codigo)) )
         {
            GX_msglist.addItem("Conta � obrigat�rio.", 1, "TIPODECONTA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtTipodeConta_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A871TipodeConta_Nome)) )
         {
            GX_msglist.addItem("Nome � obrigat�rio.", 1, "TIPODECONTA_NOME");
            AnyError = 1;
            GX_FocusControl = edtTipodeConta_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T002R4 */
         pr_default.execute(2, new Object[] {n885TipoDeConta_ContaPaiCod, A885TipoDeConta_ContaPaiCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( A885TipoDeConta_ContaPaiCod)) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo De Conta_Conta Pai'.", "ForeignKeyNotFound", 1, "TIPODECONTA_CONTAPAICOD");
               AnyError = 1;
               GX_FocusControl = dynTipoDeConta_ContaPaiCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A886TipoDeConta_ContaPaiNome = T002R4_A886TipoDeConta_ContaPaiNome[0];
         n886TipoDeConta_ContaPaiNome = T002R4_n886TipoDeConta_ContaPaiNome[0];
         A887TipoDeConta_ContaPaiTipo = T002R4_A887TipoDeConta_ContaPaiTipo[0];
         n887TipoDeConta_ContaPaiTipo = T002R4_n887TipoDeConta_ContaPaiTipo[0];
         pr_default.close(2);
         GXATIPODECONTA_CONTAPAICOD_html2R109( AV13TipodeConta_Tipo, AV7TipodeConta_Codigo) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TipodeConta_Tipo)) )
         {
            GX_msglist.addItem("Tipo � obrigat�rio.", 1, "vTIPODECONTA_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbavTipodeconta_tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV13TipodeConta_Tipo = A873TipodeConta_Tipo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TipodeConta_Tipo", AV13TipodeConta_Tipo);
         }
      }

      protected void CloseExtendedTableCursors2R109( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_16( String A885TipoDeConta_ContaPaiCod )
      {
         /* Using cursor T002R6 */
         pr_default.execute(4, new Object[] {n885TipoDeConta_ContaPaiCod, A885TipoDeConta_ContaPaiCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( A885TipoDeConta_ContaPaiCod)) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo De Conta_Conta Pai'.", "ForeignKeyNotFound", 1, "TIPODECONTA_CONTAPAICOD");
               AnyError = 1;
               GX_FocusControl = dynTipoDeConta_ContaPaiCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A886TipoDeConta_ContaPaiNome = T002R6_A886TipoDeConta_ContaPaiNome[0];
         n886TipoDeConta_ContaPaiNome = T002R6_n886TipoDeConta_ContaPaiNome[0];
         A887TipoDeConta_ContaPaiTipo = T002R6_A887TipoDeConta_ContaPaiTipo[0];
         n887TipoDeConta_ContaPaiTipo = T002R6_n887TipoDeConta_ContaPaiTipo[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A886TipoDeConta_ContaPaiNome))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A887TipoDeConta_ContaPaiTipo))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey2R109( )
      {
         /* Using cursor T002R7 */
         pr_default.execute(5, new Object[] {A870TipodeConta_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound109 = 1;
         }
         else
         {
            RcdFound109 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002R3 */
         pr_default.execute(1, new Object[] {A870TipodeConta_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2R109( 15) ;
            RcdFound109 = 1;
            A870TipodeConta_Codigo = T002R3_A870TipodeConta_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A870TipodeConta_Codigo", A870TipodeConta_Codigo);
            A871TipodeConta_Nome = T002R3_A871TipodeConta_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A871TipodeConta_Nome", A871TipodeConta_Nome);
            A872TipodeConta_Descricao = T002R3_A872TipodeConta_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A872TipodeConta_Descricao", A872TipodeConta_Descricao);
            A873TipodeConta_Tipo = T002R3_A873TipodeConta_Tipo[0];
            A885TipoDeConta_ContaPaiCod = T002R3_A885TipoDeConta_ContaPaiCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A885TipoDeConta_ContaPaiCod", A885TipoDeConta_ContaPaiCod);
            n885TipoDeConta_ContaPaiCod = T002R3_n885TipoDeConta_ContaPaiCod[0];
            Z870TipodeConta_Codigo = A870TipodeConta_Codigo;
            sMode109 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2R109( ) ;
            if ( AnyError == 1 )
            {
               RcdFound109 = 0;
               InitializeNonKey2R109( ) ;
            }
            Gx_mode = sMode109;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound109 = 0;
            InitializeNonKey2R109( ) ;
            sMode109 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode109;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2R109( ) ;
         if ( RcdFound109 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound109 = 0;
         /* Using cursor T002R8 */
         pr_default.execute(6, new Object[] {A870TipodeConta_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( StringUtil.StrCmp(T002R8_A870TipodeConta_Codigo[0], A870TipodeConta_Codigo) < 0 ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( StringUtil.StrCmp(T002R8_A870TipodeConta_Codigo[0], A870TipodeConta_Codigo) > 0 ) ) )
            {
               A870TipodeConta_Codigo = T002R8_A870TipodeConta_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A870TipodeConta_Codigo", A870TipodeConta_Codigo);
               RcdFound109 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound109 = 0;
         /* Using cursor T002R9 */
         pr_default.execute(7, new Object[] {A870TipodeConta_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( StringUtil.StrCmp(T002R9_A870TipodeConta_Codigo[0], A870TipodeConta_Codigo) > 0 ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( StringUtil.StrCmp(T002R9_A870TipodeConta_Codigo[0], A870TipodeConta_Codigo) < 0 ) ) )
            {
               A870TipodeConta_Codigo = T002R9_A870TipodeConta_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A870TipodeConta_Codigo", A870TipodeConta_Codigo);
               RcdFound109 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2R109( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtTipodeConta_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2R109( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound109 == 1 )
            {
               if ( StringUtil.StrCmp(A870TipodeConta_Codigo, Z870TipodeConta_Codigo) != 0 )
               {
                  A870TipodeConta_Codigo = Z870TipodeConta_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A870TipodeConta_Codigo", A870TipodeConta_Codigo);
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "TIPODECONTA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtTipodeConta_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtTipodeConta_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2R109( ) ;
                  GX_FocusControl = edtTipodeConta_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( StringUtil.StrCmp(A870TipodeConta_Codigo, Z870TipodeConta_Codigo) != 0 )
               {
                  /* Insert record */
                  GX_FocusControl = edtTipodeConta_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2R109( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "TIPODECONTA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtTipodeConta_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtTipodeConta_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2R109( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( StringUtil.StrCmp(A870TipodeConta_Codigo, Z870TipodeConta_Codigo) != 0 )
         {
            A870TipodeConta_Codigo = Z870TipodeConta_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A870TipodeConta_Codigo", A870TipodeConta_Codigo);
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "TIPODECONTA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtTipodeConta_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtTipodeConta_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2R109( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002R2 */
            pr_default.execute(0, new Object[] {A870TipodeConta_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"TipodeConta"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z871TipodeConta_Nome, T002R2_A871TipodeConta_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z872TipodeConta_Descricao, T002R2_A872TipodeConta_Descricao[0]) != 0 ) || ( StringUtil.StrCmp(Z873TipodeConta_Tipo, T002R2_A873TipodeConta_Tipo[0]) != 0 ) || ( StringUtil.StrCmp(Z885TipoDeConta_ContaPaiCod, T002R2_A885TipoDeConta_ContaPaiCod[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z871TipodeConta_Nome, T002R2_A871TipodeConta_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("tipodeconta:[seudo value changed for attri]"+"TipodeConta_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z871TipodeConta_Nome);
                  GXUtil.WriteLogRaw("Current: ",T002R2_A871TipodeConta_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z872TipodeConta_Descricao, T002R2_A872TipodeConta_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("tipodeconta:[seudo value changed for attri]"+"TipodeConta_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z872TipodeConta_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T002R2_A872TipodeConta_Descricao[0]);
               }
               if ( StringUtil.StrCmp(Z873TipodeConta_Tipo, T002R2_A873TipodeConta_Tipo[0]) != 0 )
               {
                  GXUtil.WriteLog("tipodeconta:[seudo value changed for attri]"+"TipodeConta_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z873TipodeConta_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T002R2_A873TipodeConta_Tipo[0]);
               }
               if ( StringUtil.StrCmp(Z885TipoDeConta_ContaPaiCod, T002R2_A885TipoDeConta_ContaPaiCod[0]) != 0 )
               {
                  GXUtil.WriteLog("tipodeconta:[seudo value changed for attri]"+"TipoDeConta_ContaPaiCod");
                  GXUtil.WriteLogRaw("Old: ",Z885TipoDeConta_ContaPaiCod);
                  GXUtil.WriteLogRaw("Current: ",T002R2_A885TipoDeConta_ContaPaiCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"TipodeConta"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2R109( )
      {
         BeforeValidate2R109( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2R109( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2R109( 0) ;
            CheckOptimisticConcurrency2R109( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2R109( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2R109( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002R10 */
                     pr_default.execute(8, new Object[] {A870TipodeConta_Codigo, A871TipodeConta_Nome, A872TipodeConta_Descricao, A873TipodeConta_Tipo, n885TipoDeConta_ContaPaiCod, A885TipoDeConta_ContaPaiCod});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("TipodeConta") ;
                     if ( (pr_default.getStatus(8) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2R0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2R109( ) ;
            }
            EndLevel2R109( ) ;
         }
         CloseExtendedTableCursors2R109( ) ;
      }

      protected void Update2R109( )
      {
         BeforeValidate2R109( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2R109( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2R109( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2R109( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2R109( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002R11 */
                     pr_default.execute(9, new Object[] {A871TipodeConta_Nome, A872TipodeConta_Descricao, A873TipodeConta_Tipo, n885TipoDeConta_ContaPaiCod, A885TipoDeConta_ContaPaiCod, A870TipodeConta_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("TipodeConta") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"TipodeConta"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2R109( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2R109( ) ;
         }
         CloseExtendedTableCursors2R109( ) ;
      }

      protected void DeferredUpdate2R109( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2R109( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2R109( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2R109( ) ;
            AfterConfirm2R109( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2R109( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002R12 */
                  pr_default.execute(10, new Object[] {A870TipodeConta_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("TipodeConta") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode109 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2R109( ) ;
         Gx_mode = sMode109;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2R109( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002R13 */
            pr_default.execute(11, new Object[] {n885TipoDeConta_ContaPaiCod, A885TipoDeConta_ContaPaiCod});
            A886TipoDeConta_ContaPaiNome = T002R13_A886TipoDeConta_ContaPaiNome[0];
            n886TipoDeConta_ContaPaiNome = T002R13_n886TipoDeConta_ContaPaiNome[0];
            A887TipoDeConta_ContaPaiTipo = T002R13_A887TipoDeConta_ContaPaiTipo[0];
            n887TipoDeConta_ContaPaiTipo = T002R13_n887TipoDeConta_ContaPaiTipo[0];
            pr_default.close(11);
            GXATIPODECONTA_CONTAPAICOD_html2R109( AV13TipodeConta_Tipo, AV7TipodeConta_Codigo) ;
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV13TipodeConta_Tipo = A873TipodeConta_Tipo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TipodeConta_Tipo", AV13TipodeConta_Tipo);
            }
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T002R14 */
            pr_default.execute(12, new Object[] {A870TipodeConta_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Tipo de Contas"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor T002R15 */
            pr_default.execute(13, new Object[] {A870TipodeConta_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Fluxo de Caixa"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
         }
      }

      protected void EndLevel2R109( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2R109( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores( "TipodeConta");
            if ( AnyError == 0 )
            {
               ConfirmValues2R0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores( "TipodeConta");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2R109( )
      {
         /* Scan By routine */
         /* Using cursor T002R16 */
         pr_default.execute(14);
         RcdFound109 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound109 = 1;
            A870TipodeConta_Codigo = T002R16_A870TipodeConta_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A870TipodeConta_Codigo", A870TipodeConta_Codigo);
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2R109( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound109 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound109 = 1;
            A870TipodeConta_Codigo = T002R16_A870TipodeConta_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A870TipodeConta_Codigo", A870TipodeConta_Codigo);
         }
      }

      protected void ScanEnd2R109( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm2R109( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2R109( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2R109( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2R109( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2R109( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2R109( )
      {
         /* Before Validate Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            A873TipodeConta_Tipo = AV13TipodeConta_Tipo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A873TipodeConta_Tipo", A873TipodeConta_Tipo);
         }
      }

      protected void DisableAttributes2R109( )
      {
         edtTipodeConta_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipodeConta_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipodeConta_Codigo_Enabled), 5, 0)));
         edtTipodeConta_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipodeConta_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipodeConta_Nome_Enabled), 5, 0)));
         edtTipodeConta_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipodeConta_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipodeConta_Descricao_Enabled), 5, 0)));
         cmbavTipodeconta_tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTipodeconta_tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTipodeconta_tipo.Enabled), 5, 0)));
         dynTipoDeConta_ContaPaiCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoDeConta_ContaPaiCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTipoDeConta_ContaPaiCod.Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2R0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812514465");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("tipodeconta.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(StringUtil.RTrim(AV7TipodeConta_Codigo))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z870TipodeConta_Codigo", StringUtil.RTrim( Z870TipodeConta_Codigo));
         GxWebStd.gx_hidden_field( context, "Z871TipodeConta_Nome", StringUtil.RTrim( Z871TipodeConta_Nome));
         GxWebStd.gx_hidden_field( context, "Z872TipodeConta_Descricao", Z872TipodeConta_Descricao);
         GxWebStd.gx_hidden_field( context, "Z873TipodeConta_Tipo", StringUtil.RTrim( Z873TipodeConta_Tipo));
         GxWebStd.gx_hidden_field( context, "Z885TipoDeConta_ContaPaiCod", StringUtil.RTrim( Z885TipoDeConta_ContaPaiCod));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N885TipoDeConta_ContaPaiCod", StringUtil.RTrim( A885TipoDeConta_ContaPaiCod));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vTIPODECONTA_CODIGO", StringUtil.RTrim( AV7TipodeConta_Codigo));
         GxWebStd.gx_hidden_field( context, "vINSERT_TIPODECONTA_CONTAPAICOD", StringUtil.RTrim( AV11Insert_TipoDeConta_ContaPaiCod));
         GxWebStd.gx_hidden_field( context, "TIPODECONTA_TIPO", StringUtil.RTrim( A873TipodeConta_Tipo));
         GxWebStd.gx_hidden_field( context, "TIPODECONTA_CONTAPAINOME", StringUtil.RTrim( A886TipoDeConta_ContaPaiNome));
         GxWebStd.gx_hidden_field( context, "TIPODECONTA_CONTAPAITIPO", StringUtil.RTrim( A887TipoDeConta_ContaPaiTipo));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vTIPODECONTA_CODIGO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TipodeConta_Codigo, ""))));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "TipodeConta";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("tipodeconta:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("tipodeconta.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(StringUtil.RTrim(AV7TipodeConta_Codigo)) ;
      }

      public override String GetPgmname( )
      {
         return "TipodeConta" ;
      }

      public override String GetPgmdesc( )
      {
         return "Plano de Contas" ;
      }

      protected void InitializeNonKey2R109( )
      {
         A885TipoDeConta_ContaPaiCod = "";
         n885TipoDeConta_ContaPaiCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A885TipoDeConta_ContaPaiCod", A885TipoDeConta_ContaPaiCod);
         n885TipoDeConta_ContaPaiCod = (String.IsNullOrEmpty(StringUtil.RTrim( A885TipoDeConta_ContaPaiCod)) ? true : false);
         AV13TipodeConta_Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TipodeConta_Tipo", AV13TipodeConta_Tipo);
         A871TipodeConta_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A871TipodeConta_Nome", A871TipodeConta_Nome);
         A872TipodeConta_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A872TipodeConta_Descricao", A872TipodeConta_Descricao);
         A873TipodeConta_Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A873TipodeConta_Tipo", A873TipodeConta_Tipo);
         A886TipoDeConta_ContaPaiNome = "";
         n886TipoDeConta_ContaPaiNome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A886TipoDeConta_ContaPaiNome", A886TipoDeConta_ContaPaiNome);
         A887TipoDeConta_ContaPaiTipo = "";
         n887TipoDeConta_ContaPaiTipo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A887TipoDeConta_ContaPaiTipo", A887TipoDeConta_ContaPaiTipo);
         Z871TipodeConta_Nome = "";
         Z872TipodeConta_Descricao = "";
         Z873TipodeConta_Tipo = "";
         Z885TipoDeConta_ContaPaiCod = "";
      }

      protected void InitAll2R109( )
      {
         A870TipodeConta_Codigo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A870TipodeConta_Codigo", A870TipodeConta_Codigo);
         InitializeNonKey2R109( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812514492");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("tipodeconta.js", "?202051812514492");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocktipodeconta_codigo_Internalname = "TEXTBLOCKTIPODECONTA_CODIGO";
         edtTipodeConta_Codigo_Internalname = "TIPODECONTA_CODIGO";
         lblTextblocktipodeconta_nome_Internalname = "TEXTBLOCKTIPODECONTA_NOME";
         edtTipodeConta_Nome_Internalname = "TIPODECONTA_NOME";
         lblTextblocktipodeconta_descricao_Internalname = "TEXTBLOCKTIPODECONTA_DESCRICAO";
         edtTipodeConta_Descricao_Internalname = "TIPODECONTA_DESCRICAO";
         lblTextblocktipodeconta_tipo_Internalname = "TEXTBLOCKTIPODECONTA_TIPO";
         cmbavTipodeconta_tipo_Internalname = "vTIPODECONTA_TIPO";
         lblTextblocktipodeconta_contapaicod_Internalname = "TEXTBLOCKTIPODECONTA_CONTAPAICOD";
         dynTipoDeConta_ContaPaiCod_Internalname = "TIPODECONTA_CONTAPAICOD";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Tipo de Contas";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Plano de Contas";
         dynTipoDeConta_ContaPaiCod_Jsonclick = "";
         dynTipoDeConta_ContaPaiCod.Enabled = 1;
         cmbavTipodeconta_tipo_Jsonclick = "";
         cmbavTipodeconta_tipo.Enabled = 1;
         edtTipodeConta_Descricao_Jsonclick = "";
         edtTipodeConta_Descricao_Enabled = 1;
         edtTipodeConta_Nome_Jsonclick = "";
         edtTipodeConta_Nome_Enabled = 1;
         edtTipodeConta_Codigo_Jsonclick = "";
         edtTipodeConta_Codigo_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         GXATIPODECONTA_CONTAPAICOD_html2R109( AV13TipodeConta_Tipo, AV7TipodeConta_Codigo) ;
         GXATIPODECONTA_CONTAPAICOD_html2R109( AV13TipodeConta_Tipo, AV7TipodeConta_Codigo) ;
         /* End function dynload_actions */
      }

      protected void GXDLATIPODECONTA_CONTAPAICOD2R109( String AV13TipodeConta_Tipo ,
                                                        String AV7TipodeConta_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLATIPODECONTA_CONTAPAICOD_data2R109( AV13TipodeConta_Tipo, AV7TipodeConta_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXATIPODECONTA_CONTAPAICOD_html2R109( String AV13TipodeConta_Tipo ,
                                                           String AV7TipodeConta_Codigo )
      {
         String gxdynajaxvalue ;
         GXDLATIPODECONTA_CONTAPAICOD_data2R109( AV13TipodeConta_Tipo, AV7TipodeConta_Codigo) ;
         gxdynajaxindex = 1;
         dynTipoDeConta_ContaPaiCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynTipoDeConta_ContaPaiCod.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLATIPODECONTA_CONTAPAICOD_data2R109( String AV13TipodeConta_Tipo ,
                                                             String AV7TipodeConta_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T002R17 */
         pr_default.execute(15, new Object[] {AV7TipodeConta_Codigo, AV13TipodeConta_Tipo});
         while ( (pr_default.getStatus(15) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( T002R17_A870TipodeConta_Codigo[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002R17_A871TipodeConta_Nome[0]));
            pr_default.readNext(15);
         }
         pr_default.close(15);
      }

      public void Validv_Tipodeconta_tipo( GXCombobox cmbGX_Parm1 ,
                                           String GX_Parm2 ,
                                           GXCombobox dynGX_Parm3 )
      {
         cmbavTipodeconta_tipo = cmbGX_Parm1;
         AV13TipodeConta_Tipo = cmbavTipodeconta_tipo.CurrentValue;
         AV7TipodeConta_Codigo = GX_Parm2;
         dynTipoDeConta_ContaPaiCod = dynGX_Parm3;
         A885TipoDeConta_ContaPaiCod = dynTipoDeConta_ContaPaiCod.CurrentValue;
         n885TipoDeConta_ContaPaiCod = false;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TipodeConta_Tipo)) )
         {
            GX_msglist.addItem("Tipo � obrigat�rio.", 1, "vTIPODECONTA_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbavTipodeconta_tipo_Internalname;
         }
         GXATIPODECONTA_CONTAPAICOD_html2R109( AV13TipodeConta_Tipo, AV7TipodeConta_Codigo) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         dynTipoDeConta_ContaPaiCod.CurrentValue = A885TipoDeConta_ContaPaiCod;
         if ( dynTipoDeConta_ContaPaiCod.ItemCount > 0 )
         {
            A885TipoDeConta_ContaPaiCod = dynTipoDeConta_ContaPaiCod.getValidValue(A885TipoDeConta_ContaPaiCod);
            n885TipoDeConta_ContaPaiCod = false;
         }
         dynTipoDeConta_ContaPaiCod.CurrentValue = A885TipoDeConta_ContaPaiCod;
         isValidOutput.Add(dynTipoDeConta_ContaPaiCod);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Tipodeconta_contapaicod( GXCombobox dynGX_Parm1 ,
                                                 String GX_Parm2 ,
                                                 String GX_Parm3 )
      {
         dynTipoDeConta_ContaPaiCod = dynGX_Parm1;
         A885TipoDeConta_ContaPaiCod = dynTipoDeConta_ContaPaiCod.CurrentValue;
         n885TipoDeConta_ContaPaiCod = false;
         A886TipoDeConta_ContaPaiNome = GX_Parm2;
         n886TipoDeConta_ContaPaiNome = false;
         A887TipoDeConta_ContaPaiTipo = GX_Parm3;
         n887TipoDeConta_ContaPaiTipo = false;
         /* Using cursor T002R18 */
         pr_default.execute(16, new Object[] {n885TipoDeConta_ContaPaiCod, A885TipoDeConta_ContaPaiCod});
         if ( (pr_default.getStatus(16) == 101) )
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( A885TipoDeConta_ContaPaiCod)) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo De Conta_Conta Pai'.", "ForeignKeyNotFound", 1, "TIPODECONTA_CONTAPAICOD");
               AnyError = 1;
               GX_FocusControl = dynTipoDeConta_ContaPaiCod_Internalname;
            }
         }
         A886TipoDeConta_ContaPaiNome = T002R18_A886TipoDeConta_ContaPaiNome[0];
         n886TipoDeConta_ContaPaiNome = T002R18_n886TipoDeConta_ContaPaiNome[0];
         A887TipoDeConta_ContaPaiTipo = T002R18_A887TipoDeConta_ContaPaiTipo[0];
         n887TipoDeConta_ContaPaiTipo = T002R18_n887TipoDeConta_ContaPaiTipo[0];
         pr_default.close(16);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A886TipoDeConta_ContaPaiNome = "";
            n886TipoDeConta_ContaPaiNome = false;
            A887TipoDeConta_ContaPaiTipo = "";
            n887TipoDeConta_ContaPaiTipo = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A886TipoDeConta_ContaPaiNome));
         isValidOutput.Add(StringUtil.RTrim( A887TipoDeConta_ContaPaiTipo));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7TipodeConta_Codigo',fld:'vTIPODECONTA_CODIGO',pic:'',hsh:true,nv:''}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E122R2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(16);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         wcpOAV7TipodeConta_Codigo = "";
         Z870TipodeConta_Codigo = "";
         Z871TipodeConta_Nome = "";
         Z872TipodeConta_Descricao = "";
         Z873TipodeConta_Tipo = "";
         Z885TipoDeConta_ContaPaiCod = "";
         N885TipoDeConta_ContaPaiCod = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV13TipodeConta_Tipo = "";
         A885TipoDeConta_ContaPaiCod = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblocktipodeconta_codigo_Jsonclick = "";
         A870TipodeConta_Codigo = "";
         lblTextblocktipodeconta_nome_Jsonclick = "";
         A871TipodeConta_Nome = "";
         lblTextblocktipodeconta_descricao_Jsonclick = "";
         A872TipodeConta_Descricao = "";
         lblTextblocktipodeconta_tipo_Jsonclick = "";
         lblTextblocktipodeconta_contapaicod_Jsonclick = "";
         A873TipodeConta_Tipo = "";
         AV11Insert_TipoDeConta_ContaPaiCod = "";
         A886TipoDeConta_ContaPaiNome = "";
         A887TipoDeConta_ContaPaiTipo = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode109 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z886TipoDeConta_ContaPaiNome = "";
         Z887TipoDeConta_ContaPaiTipo = "";
         T002R4_A886TipoDeConta_ContaPaiNome = new String[] {""} ;
         T002R4_n886TipoDeConta_ContaPaiNome = new bool[] {false} ;
         T002R4_A887TipoDeConta_ContaPaiTipo = new String[] {""} ;
         T002R4_n887TipoDeConta_ContaPaiTipo = new bool[] {false} ;
         T002R5_A870TipodeConta_Codigo = new String[] {""} ;
         T002R5_A871TipodeConta_Nome = new String[] {""} ;
         T002R5_A872TipodeConta_Descricao = new String[] {""} ;
         T002R5_A873TipodeConta_Tipo = new String[] {""} ;
         T002R5_A886TipoDeConta_ContaPaiNome = new String[] {""} ;
         T002R5_n886TipoDeConta_ContaPaiNome = new bool[] {false} ;
         T002R5_A887TipoDeConta_ContaPaiTipo = new String[] {""} ;
         T002R5_n887TipoDeConta_ContaPaiTipo = new bool[] {false} ;
         T002R5_A885TipoDeConta_ContaPaiCod = new String[] {""} ;
         T002R5_n885TipoDeConta_ContaPaiCod = new bool[] {false} ;
         T002R6_A886TipoDeConta_ContaPaiNome = new String[] {""} ;
         T002R6_n886TipoDeConta_ContaPaiNome = new bool[] {false} ;
         T002R6_A887TipoDeConta_ContaPaiTipo = new String[] {""} ;
         T002R6_n887TipoDeConta_ContaPaiTipo = new bool[] {false} ;
         T002R7_A870TipodeConta_Codigo = new String[] {""} ;
         T002R3_A870TipodeConta_Codigo = new String[] {""} ;
         T002R3_A871TipodeConta_Nome = new String[] {""} ;
         T002R3_A872TipodeConta_Descricao = new String[] {""} ;
         T002R3_A873TipodeConta_Tipo = new String[] {""} ;
         T002R3_A885TipoDeConta_ContaPaiCod = new String[] {""} ;
         T002R3_n885TipoDeConta_ContaPaiCod = new bool[] {false} ;
         T002R8_A870TipodeConta_Codigo = new String[] {""} ;
         T002R9_A870TipodeConta_Codigo = new String[] {""} ;
         T002R2_A870TipodeConta_Codigo = new String[] {""} ;
         T002R2_A871TipodeConta_Nome = new String[] {""} ;
         T002R2_A872TipodeConta_Descricao = new String[] {""} ;
         T002R2_A873TipodeConta_Tipo = new String[] {""} ;
         T002R2_A885TipoDeConta_ContaPaiCod = new String[] {""} ;
         T002R2_n885TipoDeConta_ContaPaiCod = new bool[] {false} ;
         T002R13_A886TipoDeConta_ContaPaiNome = new String[] {""} ;
         T002R13_n886TipoDeConta_ContaPaiNome = new bool[] {false} ;
         T002R13_A887TipoDeConta_ContaPaiTipo = new String[] {""} ;
         T002R13_n887TipoDeConta_ContaPaiTipo = new bool[] {false} ;
         T002R14_A885TipoDeConta_ContaPaiCod = new String[] {""} ;
         T002R14_n885TipoDeConta_ContaPaiCod = new bool[] {false} ;
         T002R15_A874Caixa_Codigo = new int[1] ;
         T002R16_A870TipodeConta_Codigo = new String[] {""} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T002R17_A870TipodeConta_Codigo = new String[] {""} ;
         T002R17_A871TipodeConta_Nome = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         T002R18_A886TipoDeConta_ContaPaiNome = new String[] {""} ;
         T002R18_n886TipoDeConta_ContaPaiNome = new bool[] {false} ;
         T002R18_A887TipoDeConta_ContaPaiTipo = new String[] {""} ;
         T002R18_n887TipoDeConta_ContaPaiTipo = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tipodeconta__default(),
            new Object[][] {
                new Object[] {
               T002R2_A870TipodeConta_Codigo, T002R2_A871TipodeConta_Nome, T002R2_A872TipodeConta_Descricao, T002R2_A873TipodeConta_Tipo, T002R2_A885TipoDeConta_ContaPaiCod, T002R2_n885TipoDeConta_ContaPaiCod
               }
               , new Object[] {
               T002R3_A870TipodeConta_Codigo, T002R3_A871TipodeConta_Nome, T002R3_A872TipodeConta_Descricao, T002R3_A873TipodeConta_Tipo, T002R3_A885TipoDeConta_ContaPaiCod, T002R3_n885TipoDeConta_ContaPaiCod
               }
               , new Object[] {
               T002R4_A886TipoDeConta_ContaPaiNome, T002R4_n886TipoDeConta_ContaPaiNome, T002R4_A887TipoDeConta_ContaPaiTipo, T002R4_n887TipoDeConta_ContaPaiTipo
               }
               , new Object[] {
               T002R5_A870TipodeConta_Codigo, T002R5_A871TipodeConta_Nome, T002R5_A872TipodeConta_Descricao, T002R5_A873TipodeConta_Tipo, T002R5_A886TipoDeConta_ContaPaiNome, T002R5_n886TipoDeConta_ContaPaiNome, T002R5_A887TipoDeConta_ContaPaiTipo, T002R5_n887TipoDeConta_ContaPaiTipo, T002R5_A885TipoDeConta_ContaPaiCod, T002R5_n885TipoDeConta_ContaPaiCod
               }
               , new Object[] {
               T002R6_A886TipoDeConta_ContaPaiNome, T002R6_n886TipoDeConta_ContaPaiNome, T002R6_A887TipoDeConta_ContaPaiTipo, T002R6_n887TipoDeConta_ContaPaiTipo
               }
               , new Object[] {
               T002R7_A870TipodeConta_Codigo
               }
               , new Object[] {
               T002R8_A870TipodeConta_Codigo
               }
               , new Object[] {
               T002R9_A870TipodeConta_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002R13_A886TipoDeConta_ContaPaiNome, T002R13_n886TipoDeConta_ContaPaiNome, T002R13_A887TipoDeConta_ContaPaiTipo, T002R13_n887TipoDeConta_ContaPaiTipo
               }
               , new Object[] {
               T002R14_A885TipoDeConta_ContaPaiCod
               }
               , new Object[] {
               T002R15_A874Caixa_Codigo
               }
               , new Object[] {
               T002R16_A870TipodeConta_Codigo
               }
               , new Object[] {
               T002R17_A870TipodeConta_Codigo, T002R17_A871TipodeConta_Nome
               }
               , new Object[] {
               T002R18_A886TipoDeConta_ContaPaiNome, T002R18_n886TipoDeConta_ContaPaiNome, T002R18_A887TipoDeConta_ContaPaiTipo, T002R18_n887TipoDeConta_ContaPaiTipo
               }
            }
         );
         AV14Pgmname = "TipodeConta";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound109 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtTipodeConta_Codigo_Enabled ;
      private int edtTipodeConta_Nome_Enabled ;
      private int edtTipodeConta_Descricao_Enabled ;
      private int AV15GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String wcpOAV7TipodeConta_Codigo ;
      private String Z870TipodeConta_Codigo ;
      private String Z871TipodeConta_Nome ;
      private String Z873TipodeConta_Tipo ;
      private String Z885TipoDeConta_ContaPaiCod ;
      private String N885TipoDeConta_ContaPaiCod ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String AV13TipodeConta_Tipo ;
      private String AV7TipodeConta_Codigo ;
      private String A885TipoDeConta_ContaPaiCod ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtTipodeConta_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocktipodeconta_codigo_Internalname ;
      private String lblTextblocktipodeconta_codigo_Jsonclick ;
      private String A870TipodeConta_Codigo ;
      private String edtTipodeConta_Codigo_Jsonclick ;
      private String lblTextblocktipodeconta_nome_Internalname ;
      private String lblTextblocktipodeconta_nome_Jsonclick ;
      private String edtTipodeConta_Nome_Internalname ;
      private String A871TipodeConta_Nome ;
      private String edtTipodeConta_Nome_Jsonclick ;
      private String lblTextblocktipodeconta_descricao_Internalname ;
      private String lblTextblocktipodeconta_descricao_Jsonclick ;
      private String edtTipodeConta_Descricao_Internalname ;
      private String edtTipodeConta_Descricao_Jsonclick ;
      private String lblTextblocktipodeconta_tipo_Internalname ;
      private String lblTextblocktipodeconta_tipo_Jsonclick ;
      private String cmbavTipodeconta_tipo_Internalname ;
      private String cmbavTipodeconta_tipo_Jsonclick ;
      private String lblTextblocktipodeconta_contapaicod_Internalname ;
      private String lblTextblocktipodeconta_contapaicod_Jsonclick ;
      private String dynTipoDeConta_ContaPaiCod_Internalname ;
      private String dynTipoDeConta_ContaPaiCod_Jsonclick ;
      private String A873TipodeConta_Tipo ;
      private String AV11Insert_TipoDeConta_ContaPaiCod ;
      private String A886TipoDeConta_ContaPaiNome ;
      private String A887TipoDeConta_ContaPaiTipo ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode109 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z886TipoDeConta_ContaPaiNome ;
      private String Z887TipoDeConta_ContaPaiTipo ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool n885TipoDeConta_ContaPaiCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n886TipoDeConta_ContaPaiNome ;
      private bool n887TipoDeConta_ContaPaiTipo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String Z872TipodeConta_Descricao ;
      private String A872TipodeConta_Descricao ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavTipodeconta_tipo ;
      private GXCombobox dynTipoDeConta_ContaPaiCod ;
      private IDataStoreProvider pr_default ;
      private String[] T002R4_A886TipoDeConta_ContaPaiNome ;
      private bool[] T002R4_n886TipoDeConta_ContaPaiNome ;
      private String[] T002R4_A887TipoDeConta_ContaPaiTipo ;
      private bool[] T002R4_n887TipoDeConta_ContaPaiTipo ;
      private String[] T002R5_A870TipodeConta_Codigo ;
      private String[] T002R5_A871TipodeConta_Nome ;
      private String[] T002R5_A872TipodeConta_Descricao ;
      private String[] T002R5_A873TipodeConta_Tipo ;
      private String[] T002R5_A886TipoDeConta_ContaPaiNome ;
      private bool[] T002R5_n886TipoDeConta_ContaPaiNome ;
      private String[] T002R5_A887TipoDeConta_ContaPaiTipo ;
      private bool[] T002R5_n887TipoDeConta_ContaPaiTipo ;
      private String[] T002R5_A885TipoDeConta_ContaPaiCod ;
      private bool[] T002R5_n885TipoDeConta_ContaPaiCod ;
      private String[] T002R6_A886TipoDeConta_ContaPaiNome ;
      private bool[] T002R6_n886TipoDeConta_ContaPaiNome ;
      private String[] T002R6_A887TipoDeConta_ContaPaiTipo ;
      private bool[] T002R6_n887TipoDeConta_ContaPaiTipo ;
      private String[] T002R7_A870TipodeConta_Codigo ;
      private String[] T002R3_A870TipodeConta_Codigo ;
      private String[] T002R3_A871TipodeConta_Nome ;
      private String[] T002R3_A872TipodeConta_Descricao ;
      private String[] T002R3_A873TipodeConta_Tipo ;
      private String[] T002R3_A885TipoDeConta_ContaPaiCod ;
      private bool[] T002R3_n885TipoDeConta_ContaPaiCod ;
      private String[] T002R8_A870TipodeConta_Codigo ;
      private String[] T002R9_A870TipodeConta_Codigo ;
      private String[] T002R2_A870TipodeConta_Codigo ;
      private String[] T002R2_A871TipodeConta_Nome ;
      private String[] T002R2_A872TipodeConta_Descricao ;
      private String[] T002R2_A873TipodeConta_Tipo ;
      private String[] T002R2_A885TipoDeConta_ContaPaiCod ;
      private bool[] T002R2_n885TipoDeConta_ContaPaiCod ;
      private String[] T002R13_A886TipoDeConta_ContaPaiNome ;
      private bool[] T002R13_n886TipoDeConta_ContaPaiNome ;
      private String[] T002R13_A887TipoDeConta_ContaPaiTipo ;
      private bool[] T002R13_n887TipoDeConta_ContaPaiTipo ;
      private String[] T002R14_A885TipoDeConta_ContaPaiCod ;
      private bool[] T002R14_n885TipoDeConta_ContaPaiCod ;
      private int[] T002R15_A874Caixa_Codigo ;
      private String[] T002R16_A870TipodeConta_Codigo ;
      private String[] T002R17_A870TipodeConta_Codigo ;
      private String[] T002R17_A871TipodeConta_Nome ;
      private String[] T002R18_A886TipoDeConta_ContaPaiNome ;
      private bool[] T002R18_n886TipoDeConta_ContaPaiNome ;
      private String[] T002R18_A887TipoDeConta_ContaPaiTipo ;
      private bool[] T002R18_n887TipoDeConta_ContaPaiTipo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class tipodeconta__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002R5 ;
          prmT002R5 = new Object[] {
          new Object[] {"@TipodeConta_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002R4 ;
          prmT002R4 = new Object[] {
          new Object[] {"@TipoDeConta_ContaPaiCod",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002R6 ;
          prmT002R6 = new Object[] {
          new Object[] {"@TipoDeConta_ContaPaiCod",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002R7 ;
          prmT002R7 = new Object[] {
          new Object[] {"@TipodeConta_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002R3 ;
          prmT002R3 = new Object[] {
          new Object[] {"@TipodeConta_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002R8 ;
          prmT002R8 = new Object[] {
          new Object[] {"@TipodeConta_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002R9 ;
          prmT002R9 = new Object[] {
          new Object[] {"@TipodeConta_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002R2 ;
          prmT002R2 = new Object[] {
          new Object[] {"@TipodeConta_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002R10 ;
          prmT002R10 = new Object[] {
          new Object[] {"@TipodeConta_Codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@TipodeConta_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@TipodeConta_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@TipodeConta_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@TipoDeConta_ContaPaiCod",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002R11 ;
          prmT002R11 = new Object[] {
          new Object[] {"@TipodeConta_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@TipodeConta_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@TipodeConta_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@TipoDeConta_ContaPaiCod",SqlDbType.Char,20,0} ,
          new Object[] {"@TipodeConta_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002R12 ;
          prmT002R12 = new Object[] {
          new Object[] {"@TipodeConta_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002R13 ;
          prmT002R13 = new Object[] {
          new Object[] {"@TipoDeConta_ContaPaiCod",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002R14 ;
          prmT002R14 = new Object[] {
          new Object[] {"@TipodeConta_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002R15 ;
          prmT002R15 = new Object[] {
          new Object[] {"@TipodeConta_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002R16 ;
          prmT002R16 = new Object[] {
          } ;
          Object[] prmT002R17 ;
          prmT002R17 = new Object[] {
          new Object[] {"@AV7TipodeConta_Codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV13TipodeConta_Tipo",SqlDbType.Char,1,0}
          } ;
          Object[] prmT002R18 ;
          prmT002R18 = new Object[] {
          new Object[] {"@TipoDeConta_ContaPaiCod",SqlDbType.Char,20,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002R2", "SELECT [TipodeConta_Codigo], [TipodeConta_Nome], [TipodeConta_Descricao], [TipodeConta_Tipo], [TipoDeConta_ContaPaiCod] AS TipoDeConta_ContaPaiCod FROM [TipodeConta] WITH (UPDLOCK) WHERE [TipodeConta_Codigo] = @TipodeConta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002R2,1,0,true,false )
             ,new CursorDef("T002R3", "SELECT [TipodeConta_Codigo], [TipodeConta_Nome], [TipodeConta_Descricao], [TipodeConta_Tipo], [TipoDeConta_ContaPaiCod] AS TipoDeConta_ContaPaiCod FROM [TipodeConta] WITH (NOLOCK) WHERE [TipodeConta_Codigo] = @TipodeConta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002R3,1,0,true,false )
             ,new CursorDef("T002R4", "SELECT [TipodeConta_Nome] AS TipoDeConta_ContaPaiNome, [TipodeConta_Tipo] AS TipoDeConta_ContaPaiTipo FROM [TipodeConta] WITH (NOLOCK) WHERE [TipodeConta_Codigo] = @TipoDeConta_ContaPaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002R4,1,0,true,false )
             ,new CursorDef("T002R5", "SELECT TM1.[TipodeConta_Codigo], TM1.[TipodeConta_Nome], TM1.[TipodeConta_Descricao], TM1.[TipodeConta_Tipo], T2.[TipodeConta_Nome] AS TipoDeConta_ContaPaiNome, T2.[TipodeConta_Tipo] AS TipoDeConta_ContaPaiTipo, TM1.[TipoDeConta_ContaPaiCod] AS TipoDeConta_ContaPaiCod FROM ([TipodeConta] TM1 WITH (NOLOCK) LEFT JOIN [TipodeConta] T2 WITH (NOLOCK) ON T2.[TipodeConta_Codigo] = TM1.[TipoDeConta_ContaPaiCod]) WHERE TM1.[TipodeConta_Codigo] = @TipodeConta_Codigo ORDER BY TM1.[TipodeConta_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002R5,100,0,true,false )
             ,new CursorDef("T002R6", "SELECT [TipodeConta_Nome] AS TipoDeConta_ContaPaiNome, [TipodeConta_Tipo] AS TipoDeConta_ContaPaiTipo FROM [TipodeConta] WITH (NOLOCK) WHERE [TipodeConta_Codigo] = @TipoDeConta_ContaPaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002R6,1,0,true,false )
             ,new CursorDef("T002R7", "SELECT [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK) WHERE [TipodeConta_Codigo] = @TipodeConta_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002R7,1,0,true,false )
             ,new CursorDef("T002R8", "SELECT TOP 1 [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK) WHERE ( [TipodeConta_Codigo] > @TipodeConta_Codigo) ORDER BY [TipodeConta_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002R8,1,0,true,true )
             ,new CursorDef("T002R9", "SELECT TOP 1 [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK) WHERE ( [TipodeConta_Codigo] < @TipodeConta_Codigo) ORDER BY [TipodeConta_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002R9,1,0,true,true )
             ,new CursorDef("T002R10", "INSERT INTO [TipodeConta]([TipodeConta_Codigo], [TipodeConta_Nome], [TipodeConta_Descricao], [TipodeConta_Tipo], [TipoDeConta_ContaPaiCod]) VALUES(@TipodeConta_Codigo, @TipodeConta_Nome, @TipodeConta_Descricao, @TipodeConta_Tipo, @TipoDeConta_ContaPaiCod)", GxErrorMask.GX_NOMASK,prmT002R10)
             ,new CursorDef("T002R11", "UPDATE [TipodeConta] SET [TipodeConta_Nome]=@TipodeConta_Nome, [TipodeConta_Descricao]=@TipodeConta_Descricao, [TipodeConta_Tipo]=@TipodeConta_Tipo, [TipoDeConta_ContaPaiCod]=@TipoDeConta_ContaPaiCod  WHERE [TipodeConta_Codigo] = @TipodeConta_Codigo", GxErrorMask.GX_NOMASK,prmT002R11)
             ,new CursorDef("T002R12", "DELETE FROM [TipodeConta]  WHERE [TipodeConta_Codigo] = @TipodeConta_Codigo", GxErrorMask.GX_NOMASK,prmT002R12)
             ,new CursorDef("T002R13", "SELECT [TipodeConta_Nome] AS TipoDeConta_ContaPaiNome, [TipodeConta_Tipo] AS TipoDeConta_ContaPaiTipo FROM [TipodeConta] WITH (NOLOCK) WHERE [TipodeConta_Codigo] = @TipoDeConta_ContaPaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002R13,1,0,true,false )
             ,new CursorDef("T002R14", "SELECT TOP 1 [TipodeConta_Codigo] AS TipoDeConta_ContaPaiCod FROM [TipodeConta] WITH (NOLOCK) WHERE [TipoDeConta_ContaPaiCod] = @TipodeConta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002R14,1,0,true,true )
             ,new CursorDef("T002R15", "SELECT TOP 1 [Caixa_Codigo] FROM [Caixa] WITH (NOLOCK) WHERE [Caixa_TipoDeContaCod] = @TipodeConta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002R15,1,0,true,true )
             ,new CursorDef("T002R16", "SELECT [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK) ORDER BY [TipodeConta_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002R16,100,0,true,false )
             ,new CursorDef("T002R17", "SELECT [TipodeConta_Codigo], [TipodeConta_Nome] FROM [TipodeConta] WITH (NOLOCK) WHERE ([TipodeConta_Codigo] <> @AV7TipodeConta_Codigo) AND ([TipodeConta_Tipo] = @AV13TipodeConta_Tipo) ORDER BY [TipodeConta_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002R17,0,0,true,false )
             ,new CursorDef("T002R18", "SELECT [TipodeConta_Nome] AS TipoDeConta_ContaPaiNome, [TipodeConta_Tipo] AS TipoDeConta_ContaPaiTipo FROM [TipodeConta] WITH (NOLOCK) WHERE [TipodeConta_Codigo] = @TipoDeConta_ContaPaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002R18,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 20) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[5]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                stmt.SetParameter(5, (String)parms[5]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                return;
       }
    }

 }

}
