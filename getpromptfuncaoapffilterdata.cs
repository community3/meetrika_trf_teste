/*
               File: GetPromptFuncaoAPFFilterData
        Description: Get Prompt Funcao APFFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:35:4.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptfuncaoapffilterdata : GXProcedure
   {
      public getpromptfuncaoapffilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptfuncaoapffilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV28DDOName = aP0_DDOName;
         this.AV26SearchTxt = aP1_SearchTxt;
         this.AV27SearchTxtTo = aP2_SearchTxtTo;
         this.AV32OptionsJson = "" ;
         this.AV35OptionsDescJson = "" ;
         this.AV37OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV32OptionsJson;
         aP4_OptionsDescJson=this.AV35OptionsDescJson;
         aP5_OptionIndexesJson=this.AV37OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV28DDOName = aP0_DDOName;
         this.AV26SearchTxt = aP1_SearchTxt;
         this.AV27SearchTxtTo = aP2_SearchTxtTo;
         this.AV32OptionsJson = "" ;
         this.AV35OptionsDescJson = "" ;
         this.AV37OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV32OptionsJson;
         aP4_OptionsDescJson=this.AV35OptionsDescJson;
         aP5_OptionIndexesJson=this.AV37OptionIndexesJson;
         return AV37OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptfuncaoapffilterdata objgetpromptfuncaoapffilterdata;
         objgetpromptfuncaoapffilterdata = new getpromptfuncaoapffilterdata();
         objgetpromptfuncaoapffilterdata.AV28DDOName = aP0_DDOName;
         objgetpromptfuncaoapffilterdata.AV26SearchTxt = aP1_SearchTxt;
         objgetpromptfuncaoapffilterdata.AV27SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptfuncaoapffilterdata.AV32OptionsJson = "" ;
         objgetpromptfuncaoapffilterdata.AV35OptionsDescJson = "" ;
         objgetpromptfuncaoapffilterdata.AV37OptionIndexesJson = "" ;
         objgetpromptfuncaoapffilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptfuncaoapffilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptfuncaoapffilterdata);
         aP3_OptionsJson=this.AV32OptionsJson;
         aP4_OptionsDescJson=this.AV35OptionsDescJson;
         aP5_OptionIndexesJson=this.AV37OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptfuncaoapffilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV31Options = (IGxCollection)(new GxSimpleCollection());
         AV34OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV36OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV28DDOName), "DDO_FUNCAOAPF_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPF_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV28DDOName), "DDO_FUNCAOAPF_FUNAPFPAINOM") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPF_FUNAPFPAINOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV32OptionsJson = AV31Options.ToJSonString(false);
         AV35OptionsDescJson = AV34OptionsDesc.ToJSonString(false);
         AV37OptionIndexesJson = AV36OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV39Session.Get("PromptFuncaoAPFGridState"), "") == 0 )
         {
            AV41GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptFuncaoAPFGridState"), "");
         }
         else
         {
            AV41GridState.FromXml(AV39Session.Get("PromptFuncaoAPFGridState"), "");
         }
         AV69GXV1 = 1;
         while ( AV69GXV1 <= AV41GridState.gxTpr_Filtervalues.Count )
         {
            AV42GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV41GridState.gxTpr_Filtervalues.Item(AV69GXV1));
            if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_NOME") == 0 )
            {
               AV10TFFuncaoAPF_Nome = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_NOME_SEL") == 0 )
            {
               AV11TFFuncaoAPF_Nome_Sel = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_TIPO_SEL") == 0 )
            {
               AV12TFFuncaoAPF_Tipo_SelsJson = AV42GridStateFilterValue.gxTpr_Value;
               AV13TFFuncaoAPF_Tipo_Sels.FromJSonString(AV12TFFuncaoAPF_Tipo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_COMPLEXIDADE_SEL") == 0 )
            {
               AV14TFFuncaoAPF_Complexidade_SelsJson = AV42GridStateFilterValue.gxTpr_Value;
               AV15TFFuncaoAPF_Complexidade_Sels.FromJSonString(AV14TFFuncaoAPF_Complexidade_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_TD") == 0 )
            {
               AV16TFFuncaoAPF_TD = (short)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Value, "."));
               AV17TFFuncaoAPF_TD_To = (short)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_AR") == 0 )
            {
               AV18TFFuncaoAPF_AR = (short)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Value, "."));
               AV19TFFuncaoAPF_AR_To = (short)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_PF") == 0 )
            {
               AV20TFFuncaoAPF_PF = NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Value, ".");
               AV21TFFuncaoAPF_PF_To = NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_FUNAPFPAINOM") == 0 )
            {
               AV22TFFuncaoAPF_FunAPFPaiNom = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_FUNAPFPAINOM_SEL") == 0 )
            {
               AV23TFFuncaoAPF_FunAPFPaiNom_Sel = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_ATIVO_SEL") == 0 )
            {
               AV24TFFuncaoAPF_Ativo_SelsJson = AV42GridStateFilterValue.gxTpr_Value;
               AV25TFFuncaoAPF_Ativo_Sels.FromJSonString(AV24TFFuncaoAPF_Ativo_SelsJson);
            }
            AV69GXV1 = (int)(AV69GXV1+1);
         }
         if ( AV41GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV43GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV41GridState.gxTpr_Dynamicfilters.Item(1));
            AV44DynamicFiltersSelector1 = AV43GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
            {
               AV45DynamicFiltersOperator1 = AV43GridStateDynamicFilter.gxTpr_Operator;
               AV46FuncaoAPF_Nome1 = AV43GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_SISTEMADES") == 0 )
            {
               AV45DynamicFiltersOperator1 = AV43GridStateDynamicFilter.gxTpr_Operator;
               AV47FuncaoAPF_SistemaDes1 = AV43GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_MODULONOM") == 0 )
            {
               AV45DynamicFiltersOperator1 = AV43GridStateDynamicFilter.gxTpr_Operator;
               AV48FuncaoAPF_ModuloNom1 = AV43GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_FUNAPFPAINOM") == 0 )
            {
               AV45DynamicFiltersOperator1 = AV43GridStateDynamicFilter.gxTpr_Operator;
               AV49FuncaoAPF_FunAPFPaiNom1 = AV43GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_TIPO") == 0 )
            {
               AV50FuncaoAPF_Tipo1 = AV43GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV41GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV51DynamicFiltersEnabled2 = true;
               AV43GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV41GridState.gxTpr_Dynamicfilters.Item(2));
               AV52DynamicFiltersSelector2 = AV43GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
               {
                  AV53DynamicFiltersOperator2 = AV43GridStateDynamicFilter.gxTpr_Operator;
                  AV54FuncaoAPF_Nome2 = AV43GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_SISTEMADES") == 0 )
               {
                  AV53DynamicFiltersOperator2 = AV43GridStateDynamicFilter.gxTpr_Operator;
                  AV55FuncaoAPF_SistemaDes2 = AV43GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_MODULONOM") == 0 )
               {
                  AV53DynamicFiltersOperator2 = AV43GridStateDynamicFilter.gxTpr_Operator;
                  AV56FuncaoAPF_ModuloNom2 = AV43GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_FUNAPFPAINOM") == 0 )
               {
                  AV53DynamicFiltersOperator2 = AV43GridStateDynamicFilter.gxTpr_Operator;
                  AV57FuncaoAPF_FunAPFPaiNom2 = AV43GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_TIPO") == 0 )
               {
                  AV58FuncaoAPF_Tipo2 = AV43GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV41GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV59DynamicFiltersEnabled3 = true;
                  AV43GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV41GridState.gxTpr_Dynamicfilters.Item(3));
                  AV60DynamicFiltersSelector3 = AV43GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
                  {
                     AV61DynamicFiltersOperator3 = AV43GridStateDynamicFilter.gxTpr_Operator;
                     AV62FuncaoAPF_Nome3 = AV43GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_SISTEMADES") == 0 )
                  {
                     AV61DynamicFiltersOperator3 = AV43GridStateDynamicFilter.gxTpr_Operator;
                     AV63FuncaoAPF_SistemaDes3 = AV43GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_MODULONOM") == 0 )
                  {
                     AV61DynamicFiltersOperator3 = AV43GridStateDynamicFilter.gxTpr_Operator;
                     AV64FuncaoAPF_ModuloNom3 = AV43GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_FUNAPFPAINOM") == 0 )
                  {
                     AV61DynamicFiltersOperator3 = AV43GridStateDynamicFilter.gxTpr_Operator;
                     AV65FuncaoAPF_FunAPFPaiNom3 = AV43GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_TIPO") == 0 )
                  {
                     AV66FuncaoAPF_Tipo3 = AV43GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAOAPF_NOMEOPTIONS' Routine */
         AV10TFFuncaoAPF_Nome = AV26SearchTxt;
         AV11TFFuncaoAPF_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A185FuncaoAPF_Complexidade ,
                                              AV15TFFuncaoAPF_Complexidade_Sels ,
                                              A184FuncaoAPF_Tipo ,
                                              AV13TFFuncaoAPF_Tipo_Sels ,
                                              A183FuncaoAPF_Ativo ,
                                              AV25TFFuncaoAPF_Ativo_Sels ,
                                              AV44DynamicFiltersSelector1 ,
                                              AV45DynamicFiltersOperator1 ,
                                              AV46FuncaoAPF_Nome1 ,
                                              AV47FuncaoAPF_SistemaDes1 ,
                                              AV48FuncaoAPF_ModuloNom1 ,
                                              AV49FuncaoAPF_FunAPFPaiNom1 ,
                                              AV50FuncaoAPF_Tipo1 ,
                                              AV51DynamicFiltersEnabled2 ,
                                              AV52DynamicFiltersSelector2 ,
                                              AV53DynamicFiltersOperator2 ,
                                              AV54FuncaoAPF_Nome2 ,
                                              AV55FuncaoAPF_SistemaDes2 ,
                                              AV56FuncaoAPF_ModuloNom2 ,
                                              AV57FuncaoAPF_FunAPFPaiNom2 ,
                                              AV58FuncaoAPF_Tipo2 ,
                                              AV59DynamicFiltersEnabled3 ,
                                              AV60DynamicFiltersSelector3 ,
                                              AV61DynamicFiltersOperator3 ,
                                              AV62FuncaoAPF_Nome3 ,
                                              AV63FuncaoAPF_SistemaDes3 ,
                                              AV64FuncaoAPF_ModuloNom3 ,
                                              AV65FuncaoAPF_FunAPFPaiNom3 ,
                                              AV66FuncaoAPF_Tipo3 ,
                                              AV11TFFuncaoAPF_Nome_Sel ,
                                              AV10TFFuncaoAPF_Nome ,
                                              AV13TFFuncaoAPF_Tipo_Sels.Count ,
                                              AV23TFFuncaoAPF_FunAPFPaiNom_Sel ,
                                              AV22TFFuncaoAPF_FunAPFPaiNom ,
                                              AV25TFFuncaoAPF_Ativo_Sels.Count ,
                                              A166FuncaoAPF_Nome ,
                                              A362FuncaoAPF_SistemaDes ,
                                              A361FuncaoAPF_ModuloNom ,
                                              A363FuncaoAPF_FunAPFPaiNom ,
                                              AV15TFFuncaoAPF_Complexidade_Sels.Count ,
                                              AV16TFFuncaoAPF_TD ,
                                              A388FuncaoAPF_TD ,
                                              AV17TFFuncaoAPF_TD_To ,
                                              AV18TFFuncaoAPF_AR ,
                                              A387FuncaoAPF_AR ,
                                              AV19TFFuncaoAPF_AR_To ,
                                              AV20TFFuncaoAPF_PF ,
                                              A386FuncaoAPF_PF ,
                                              AV21TFFuncaoAPF_PF_To },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                              }
         });
         lV46FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV46FuncaoAPF_Nome1), "%", "");
         lV46FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV46FuncaoAPF_Nome1), "%", "");
         lV47FuncaoAPF_SistemaDes1 = StringUtil.Concat( StringUtil.RTrim( AV47FuncaoAPF_SistemaDes1), "%", "");
         lV47FuncaoAPF_SistemaDes1 = StringUtil.Concat( StringUtil.RTrim( AV47FuncaoAPF_SistemaDes1), "%", "");
         lV48FuncaoAPF_ModuloNom1 = StringUtil.PadR( StringUtil.RTrim( AV48FuncaoAPF_ModuloNom1), 50, "%");
         lV48FuncaoAPF_ModuloNom1 = StringUtil.PadR( StringUtil.RTrim( AV48FuncaoAPF_ModuloNom1), 50, "%");
         lV49FuncaoAPF_FunAPFPaiNom1 = StringUtil.Concat( StringUtil.RTrim( AV49FuncaoAPF_FunAPFPaiNom1), "%", "");
         lV49FuncaoAPF_FunAPFPaiNom1 = StringUtil.Concat( StringUtil.RTrim( AV49FuncaoAPF_FunAPFPaiNom1), "%", "");
         lV54FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV54FuncaoAPF_Nome2), "%", "");
         lV54FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV54FuncaoAPF_Nome2), "%", "");
         lV55FuncaoAPF_SistemaDes2 = StringUtil.Concat( StringUtil.RTrim( AV55FuncaoAPF_SistemaDes2), "%", "");
         lV55FuncaoAPF_SistemaDes2 = StringUtil.Concat( StringUtil.RTrim( AV55FuncaoAPF_SistemaDes2), "%", "");
         lV56FuncaoAPF_ModuloNom2 = StringUtil.PadR( StringUtil.RTrim( AV56FuncaoAPF_ModuloNom2), 50, "%");
         lV56FuncaoAPF_ModuloNom2 = StringUtil.PadR( StringUtil.RTrim( AV56FuncaoAPF_ModuloNom2), 50, "%");
         lV57FuncaoAPF_FunAPFPaiNom2 = StringUtil.Concat( StringUtil.RTrim( AV57FuncaoAPF_FunAPFPaiNom2), "%", "");
         lV57FuncaoAPF_FunAPFPaiNom2 = StringUtil.Concat( StringUtil.RTrim( AV57FuncaoAPF_FunAPFPaiNom2), "%", "");
         lV62FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV62FuncaoAPF_Nome3), "%", "");
         lV62FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV62FuncaoAPF_Nome3), "%", "");
         lV63FuncaoAPF_SistemaDes3 = StringUtil.Concat( StringUtil.RTrim( AV63FuncaoAPF_SistemaDes3), "%", "");
         lV63FuncaoAPF_SistemaDes3 = StringUtil.Concat( StringUtil.RTrim( AV63FuncaoAPF_SistemaDes3), "%", "");
         lV64FuncaoAPF_ModuloNom3 = StringUtil.PadR( StringUtil.RTrim( AV64FuncaoAPF_ModuloNom3), 50, "%");
         lV64FuncaoAPF_ModuloNom3 = StringUtil.PadR( StringUtil.RTrim( AV64FuncaoAPF_ModuloNom3), 50, "%");
         lV65FuncaoAPF_FunAPFPaiNom3 = StringUtil.Concat( StringUtil.RTrim( AV65FuncaoAPF_FunAPFPaiNom3), "%", "");
         lV65FuncaoAPF_FunAPFPaiNom3 = StringUtil.Concat( StringUtil.RTrim( AV65FuncaoAPF_FunAPFPaiNom3), "%", "");
         lV10TFFuncaoAPF_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFFuncaoAPF_Nome), "%", "");
         lV22TFFuncaoAPF_FunAPFPaiNom = StringUtil.Concat( StringUtil.RTrim( AV22TFFuncaoAPF_FunAPFPaiNom), "%", "");
         /* Using cursor P00U72 */
         pr_default.execute(0, new Object[] {AV15TFFuncaoAPF_Complexidade_Sels.Count, lV46FuncaoAPF_Nome1, lV46FuncaoAPF_Nome1, lV47FuncaoAPF_SistemaDes1, lV47FuncaoAPF_SistemaDes1, lV48FuncaoAPF_ModuloNom1, lV48FuncaoAPF_ModuloNom1, lV49FuncaoAPF_FunAPFPaiNom1, lV49FuncaoAPF_FunAPFPaiNom1, AV50FuncaoAPF_Tipo1, lV54FuncaoAPF_Nome2, lV54FuncaoAPF_Nome2, lV55FuncaoAPF_SistemaDes2, lV55FuncaoAPF_SistemaDes2, lV56FuncaoAPF_ModuloNom2, lV56FuncaoAPF_ModuloNom2, lV57FuncaoAPF_FunAPFPaiNom2, lV57FuncaoAPF_FunAPFPaiNom2, AV58FuncaoAPF_Tipo2, lV62FuncaoAPF_Nome3, lV62FuncaoAPF_Nome3, lV63FuncaoAPF_SistemaDes3, lV63FuncaoAPF_SistemaDes3, lV64FuncaoAPF_ModuloNom3, lV64FuncaoAPF_ModuloNom3, lV65FuncaoAPF_FunAPFPaiNom3, lV65FuncaoAPF_FunAPFPaiNom3, AV66FuncaoAPF_Tipo3, lV10TFFuncaoAPF_Nome, AV11TFFuncaoAPF_Nome_Sel, lV22TFFuncaoAPF_FunAPFPaiNom, AV23TFFuncaoAPF_FunAPFPaiNom_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKU72 = false;
            A360FuncaoAPF_SistemaCod = P00U72_A360FuncaoAPF_SistemaCod[0];
            n360FuncaoAPF_SistemaCod = P00U72_n360FuncaoAPF_SistemaCod[0];
            A359FuncaoAPF_ModuloCod = P00U72_A359FuncaoAPF_ModuloCod[0];
            n359FuncaoAPF_ModuloCod = P00U72_n359FuncaoAPF_ModuloCod[0];
            A358FuncaoAPF_FunAPFPaiCod = P00U72_A358FuncaoAPF_FunAPFPaiCod[0];
            n358FuncaoAPF_FunAPFPaiCod = P00U72_n358FuncaoAPF_FunAPFPaiCod[0];
            A166FuncaoAPF_Nome = P00U72_A166FuncaoAPF_Nome[0];
            A183FuncaoAPF_Ativo = P00U72_A183FuncaoAPF_Ativo[0];
            A184FuncaoAPF_Tipo = P00U72_A184FuncaoAPF_Tipo[0];
            A363FuncaoAPF_FunAPFPaiNom = P00U72_A363FuncaoAPF_FunAPFPaiNom[0];
            n363FuncaoAPF_FunAPFPaiNom = P00U72_n363FuncaoAPF_FunAPFPaiNom[0];
            A361FuncaoAPF_ModuloNom = P00U72_A361FuncaoAPF_ModuloNom[0];
            n361FuncaoAPF_ModuloNom = P00U72_n361FuncaoAPF_ModuloNom[0];
            A362FuncaoAPF_SistemaDes = P00U72_A362FuncaoAPF_SistemaDes[0];
            n362FuncaoAPF_SistemaDes = P00U72_n362FuncaoAPF_SistemaDes[0];
            A165FuncaoAPF_Codigo = P00U72_A165FuncaoAPF_Codigo[0];
            A362FuncaoAPF_SistemaDes = P00U72_A362FuncaoAPF_SistemaDes[0];
            n362FuncaoAPF_SistemaDes = P00U72_n362FuncaoAPF_SistemaDes[0];
            A361FuncaoAPF_ModuloNom = P00U72_A361FuncaoAPF_ModuloNom[0];
            n361FuncaoAPF_ModuloNom = P00U72_n361FuncaoAPF_ModuloNom[0];
            A363FuncaoAPF_FunAPFPaiNom = P00U72_A363FuncaoAPF_FunAPFPaiNom[0];
            n363FuncaoAPF_FunAPFPaiNom = P00U72_n363FuncaoAPF_FunAPFPaiNom[0];
            GXt_char1 = A185FuncaoAPF_Complexidade;
            new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char1) ;
            A185FuncaoAPF_Complexidade = GXt_char1;
            if ( ( AV15TFFuncaoAPF_Complexidade_Sels.Count <= 0 ) || ( (AV15TFFuncaoAPF_Complexidade_Sels.IndexOf(StringUtil.RTrim( A185FuncaoAPF_Complexidade))>0) ) )
            {
               GXt_int2 = A388FuncaoAPF_TD;
               new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int2) ;
               A388FuncaoAPF_TD = GXt_int2;
               if ( (0==AV16TFFuncaoAPF_TD) || ( ( A388FuncaoAPF_TD >= AV16TFFuncaoAPF_TD ) ) )
               {
                  if ( (0==AV17TFFuncaoAPF_TD_To) || ( ( A388FuncaoAPF_TD <= AV17TFFuncaoAPF_TD_To ) ) )
                  {
                     GXt_int2 = A387FuncaoAPF_AR;
                     new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int2) ;
                     A387FuncaoAPF_AR = GXt_int2;
                     if ( (0==AV18TFFuncaoAPF_AR) || ( ( A387FuncaoAPF_AR >= AV18TFFuncaoAPF_AR ) ) )
                     {
                        if ( (0==AV19TFFuncaoAPF_AR_To) || ( ( A387FuncaoAPF_AR <= AV19TFFuncaoAPF_AR_To ) ) )
                        {
                           GXt_decimal3 = A386FuncaoAPF_PF;
                           new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
                           A386FuncaoAPF_PF = GXt_decimal3;
                           if ( (Convert.ToDecimal(0)==AV20TFFuncaoAPF_PF) || ( ( A386FuncaoAPF_PF >= AV20TFFuncaoAPF_PF ) ) )
                           {
                              if ( (Convert.ToDecimal(0)==AV21TFFuncaoAPF_PF_To) || ( ( A386FuncaoAPF_PF <= AV21TFFuncaoAPF_PF_To ) ) )
                              {
                                 AV38count = 0;
                                 while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00U72_A166FuncaoAPF_Nome[0], A166FuncaoAPF_Nome) == 0 ) )
                                 {
                                    BRKU72 = false;
                                    A165FuncaoAPF_Codigo = P00U72_A165FuncaoAPF_Codigo[0];
                                    AV38count = (long)(AV38count+1);
                                    BRKU72 = true;
                                    pr_default.readNext(0);
                                 }
                                 if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A166FuncaoAPF_Nome)) )
                                 {
                                    AV30Option = A166FuncaoAPF_Nome;
                                    AV31Options.Add(AV30Option, 0);
                                    AV36OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV38count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                                 }
                                 if ( AV31Options.Count == 50 )
                                 {
                                    /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                    if (true) break;
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRKU72 )
            {
               BRKU72 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADFUNCAOAPF_FUNAPFPAINOMOPTIONS' Routine */
         AV22TFFuncaoAPF_FunAPFPaiNom = AV26SearchTxt;
         AV23TFFuncaoAPF_FunAPFPaiNom_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A185FuncaoAPF_Complexidade ,
                                              AV15TFFuncaoAPF_Complexidade_Sels ,
                                              A184FuncaoAPF_Tipo ,
                                              AV13TFFuncaoAPF_Tipo_Sels ,
                                              A183FuncaoAPF_Ativo ,
                                              AV25TFFuncaoAPF_Ativo_Sels ,
                                              AV44DynamicFiltersSelector1 ,
                                              AV45DynamicFiltersOperator1 ,
                                              AV46FuncaoAPF_Nome1 ,
                                              AV47FuncaoAPF_SistemaDes1 ,
                                              AV48FuncaoAPF_ModuloNom1 ,
                                              AV49FuncaoAPF_FunAPFPaiNom1 ,
                                              AV50FuncaoAPF_Tipo1 ,
                                              AV51DynamicFiltersEnabled2 ,
                                              AV52DynamicFiltersSelector2 ,
                                              AV53DynamicFiltersOperator2 ,
                                              AV54FuncaoAPF_Nome2 ,
                                              AV55FuncaoAPF_SistemaDes2 ,
                                              AV56FuncaoAPF_ModuloNom2 ,
                                              AV57FuncaoAPF_FunAPFPaiNom2 ,
                                              AV58FuncaoAPF_Tipo2 ,
                                              AV59DynamicFiltersEnabled3 ,
                                              AV60DynamicFiltersSelector3 ,
                                              AV61DynamicFiltersOperator3 ,
                                              AV62FuncaoAPF_Nome3 ,
                                              AV63FuncaoAPF_SistemaDes3 ,
                                              AV64FuncaoAPF_ModuloNom3 ,
                                              AV65FuncaoAPF_FunAPFPaiNom3 ,
                                              AV66FuncaoAPF_Tipo3 ,
                                              AV11TFFuncaoAPF_Nome_Sel ,
                                              AV10TFFuncaoAPF_Nome ,
                                              AV13TFFuncaoAPF_Tipo_Sels.Count ,
                                              AV23TFFuncaoAPF_FunAPFPaiNom_Sel ,
                                              AV22TFFuncaoAPF_FunAPFPaiNom ,
                                              AV25TFFuncaoAPF_Ativo_Sels.Count ,
                                              A166FuncaoAPF_Nome ,
                                              A362FuncaoAPF_SistemaDes ,
                                              A361FuncaoAPF_ModuloNom ,
                                              A363FuncaoAPF_FunAPFPaiNom ,
                                              AV15TFFuncaoAPF_Complexidade_Sels.Count ,
                                              AV16TFFuncaoAPF_TD ,
                                              A388FuncaoAPF_TD ,
                                              AV17TFFuncaoAPF_TD_To ,
                                              AV18TFFuncaoAPF_AR ,
                                              A387FuncaoAPF_AR ,
                                              AV19TFFuncaoAPF_AR_To ,
                                              AV20TFFuncaoAPF_PF ,
                                              A386FuncaoAPF_PF ,
                                              AV21TFFuncaoAPF_PF_To },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                              }
         });
         lV46FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV46FuncaoAPF_Nome1), "%", "");
         lV46FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV46FuncaoAPF_Nome1), "%", "");
         lV47FuncaoAPF_SistemaDes1 = StringUtil.Concat( StringUtil.RTrim( AV47FuncaoAPF_SistemaDes1), "%", "");
         lV47FuncaoAPF_SistemaDes1 = StringUtil.Concat( StringUtil.RTrim( AV47FuncaoAPF_SistemaDes1), "%", "");
         lV48FuncaoAPF_ModuloNom1 = StringUtil.PadR( StringUtil.RTrim( AV48FuncaoAPF_ModuloNom1), 50, "%");
         lV48FuncaoAPF_ModuloNom1 = StringUtil.PadR( StringUtil.RTrim( AV48FuncaoAPF_ModuloNom1), 50, "%");
         lV49FuncaoAPF_FunAPFPaiNom1 = StringUtil.Concat( StringUtil.RTrim( AV49FuncaoAPF_FunAPFPaiNom1), "%", "");
         lV49FuncaoAPF_FunAPFPaiNom1 = StringUtil.Concat( StringUtil.RTrim( AV49FuncaoAPF_FunAPFPaiNom1), "%", "");
         lV54FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV54FuncaoAPF_Nome2), "%", "");
         lV54FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV54FuncaoAPF_Nome2), "%", "");
         lV55FuncaoAPF_SistemaDes2 = StringUtil.Concat( StringUtil.RTrim( AV55FuncaoAPF_SistemaDes2), "%", "");
         lV55FuncaoAPF_SistemaDes2 = StringUtil.Concat( StringUtil.RTrim( AV55FuncaoAPF_SistemaDes2), "%", "");
         lV56FuncaoAPF_ModuloNom2 = StringUtil.PadR( StringUtil.RTrim( AV56FuncaoAPF_ModuloNom2), 50, "%");
         lV56FuncaoAPF_ModuloNom2 = StringUtil.PadR( StringUtil.RTrim( AV56FuncaoAPF_ModuloNom2), 50, "%");
         lV57FuncaoAPF_FunAPFPaiNom2 = StringUtil.Concat( StringUtil.RTrim( AV57FuncaoAPF_FunAPFPaiNom2), "%", "");
         lV57FuncaoAPF_FunAPFPaiNom2 = StringUtil.Concat( StringUtil.RTrim( AV57FuncaoAPF_FunAPFPaiNom2), "%", "");
         lV62FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV62FuncaoAPF_Nome3), "%", "");
         lV62FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV62FuncaoAPF_Nome3), "%", "");
         lV63FuncaoAPF_SistemaDes3 = StringUtil.Concat( StringUtil.RTrim( AV63FuncaoAPF_SistemaDes3), "%", "");
         lV63FuncaoAPF_SistemaDes3 = StringUtil.Concat( StringUtil.RTrim( AV63FuncaoAPF_SistemaDes3), "%", "");
         lV64FuncaoAPF_ModuloNom3 = StringUtil.PadR( StringUtil.RTrim( AV64FuncaoAPF_ModuloNom3), 50, "%");
         lV64FuncaoAPF_ModuloNom3 = StringUtil.PadR( StringUtil.RTrim( AV64FuncaoAPF_ModuloNom3), 50, "%");
         lV65FuncaoAPF_FunAPFPaiNom3 = StringUtil.Concat( StringUtil.RTrim( AV65FuncaoAPF_FunAPFPaiNom3), "%", "");
         lV65FuncaoAPF_FunAPFPaiNom3 = StringUtil.Concat( StringUtil.RTrim( AV65FuncaoAPF_FunAPFPaiNom3), "%", "");
         lV10TFFuncaoAPF_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFFuncaoAPF_Nome), "%", "");
         lV22TFFuncaoAPF_FunAPFPaiNom = StringUtil.Concat( StringUtil.RTrim( AV22TFFuncaoAPF_FunAPFPaiNom), "%", "");
         /* Using cursor P00U73 */
         pr_default.execute(1, new Object[] {AV15TFFuncaoAPF_Complexidade_Sels.Count, lV46FuncaoAPF_Nome1, lV46FuncaoAPF_Nome1, lV47FuncaoAPF_SistemaDes1, lV47FuncaoAPF_SistemaDes1, lV48FuncaoAPF_ModuloNom1, lV48FuncaoAPF_ModuloNom1, lV49FuncaoAPF_FunAPFPaiNom1, lV49FuncaoAPF_FunAPFPaiNom1, AV50FuncaoAPF_Tipo1, lV54FuncaoAPF_Nome2, lV54FuncaoAPF_Nome2, lV55FuncaoAPF_SistemaDes2, lV55FuncaoAPF_SistemaDes2, lV56FuncaoAPF_ModuloNom2, lV56FuncaoAPF_ModuloNom2, lV57FuncaoAPF_FunAPFPaiNom2, lV57FuncaoAPF_FunAPFPaiNom2, AV58FuncaoAPF_Tipo2, lV62FuncaoAPF_Nome3, lV62FuncaoAPF_Nome3, lV63FuncaoAPF_SistemaDes3, lV63FuncaoAPF_SistemaDes3, lV64FuncaoAPF_ModuloNom3, lV64FuncaoAPF_ModuloNom3, lV65FuncaoAPF_FunAPFPaiNom3, lV65FuncaoAPF_FunAPFPaiNom3, AV66FuncaoAPF_Tipo3, lV10TFFuncaoAPF_Nome, AV11TFFuncaoAPF_Nome_Sel, lV22TFFuncaoAPF_FunAPFPaiNom, AV23TFFuncaoAPF_FunAPFPaiNom_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKU74 = false;
            A360FuncaoAPF_SistemaCod = P00U73_A360FuncaoAPF_SistemaCod[0];
            n360FuncaoAPF_SistemaCod = P00U73_n360FuncaoAPF_SistemaCod[0];
            A359FuncaoAPF_ModuloCod = P00U73_A359FuncaoAPF_ModuloCod[0];
            n359FuncaoAPF_ModuloCod = P00U73_n359FuncaoAPF_ModuloCod[0];
            A358FuncaoAPF_FunAPFPaiCod = P00U73_A358FuncaoAPF_FunAPFPaiCod[0];
            n358FuncaoAPF_FunAPFPaiCod = P00U73_n358FuncaoAPF_FunAPFPaiCod[0];
            A363FuncaoAPF_FunAPFPaiNom = P00U73_A363FuncaoAPF_FunAPFPaiNom[0];
            n363FuncaoAPF_FunAPFPaiNom = P00U73_n363FuncaoAPF_FunAPFPaiNom[0];
            A183FuncaoAPF_Ativo = P00U73_A183FuncaoAPF_Ativo[0];
            A184FuncaoAPF_Tipo = P00U73_A184FuncaoAPF_Tipo[0];
            A361FuncaoAPF_ModuloNom = P00U73_A361FuncaoAPF_ModuloNom[0];
            n361FuncaoAPF_ModuloNom = P00U73_n361FuncaoAPF_ModuloNom[0];
            A362FuncaoAPF_SistemaDes = P00U73_A362FuncaoAPF_SistemaDes[0];
            n362FuncaoAPF_SistemaDes = P00U73_n362FuncaoAPF_SistemaDes[0];
            A166FuncaoAPF_Nome = P00U73_A166FuncaoAPF_Nome[0];
            A165FuncaoAPF_Codigo = P00U73_A165FuncaoAPF_Codigo[0];
            A362FuncaoAPF_SistemaDes = P00U73_A362FuncaoAPF_SistemaDes[0];
            n362FuncaoAPF_SistemaDes = P00U73_n362FuncaoAPF_SistemaDes[0];
            A361FuncaoAPF_ModuloNom = P00U73_A361FuncaoAPF_ModuloNom[0];
            n361FuncaoAPF_ModuloNom = P00U73_n361FuncaoAPF_ModuloNom[0];
            A363FuncaoAPF_FunAPFPaiNom = P00U73_A363FuncaoAPF_FunAPFPaiNom[0];
            n363FuncaoAPF_FunAPFPaiNom = P00U73_n363FuncaoAPF_FunAPFPaiNom[0];
            GXt_char1 = A185FuncaoAPF_Complexidade;
            new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char1) ;
            A185FuncaoAPF_Complexidade = GXt_char1;
            if ( ( AV15TFFuncaoAPF_Complexidade_Sels.Count <= 0 ) || ( (AV15TFFuncaoAPF_Complexidade_Sels.IndexOf(StringUtil.RTrim( A185FuncaoAPF_Complexidade))>0) ) )
            {
               GXt_int2 = A388FuncaoAPF_TD;
               new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int2) ;
               A388FuncaoAPF_TD = GXt_int2;
               if ( (0==AV16TFFuncaoAPF_TD) || ( ( A388FuncaoAPF_TD >= AV16TFFuncaoAPF_TD ) ) )
               {
                  if ( (0==AV17TFFuncaoAPF_TD_To) || ( ( A388FuncaoAPF_TD <= AV17TFFuncaoAPF_TD_To ) ) )
                  {
                     GXt_int2 = A387FuncaoAPF_AR;
                     new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int2) ;
                     A387FuncaoAPF_AR = GXt_int2;
                     if ( (0==AV18TFFuncaoAPF_AR) || ( ( A387FuncaoAPF_AR >= AV18TFFuncaoAPF_AR ) ) )
                     {
                        if ( (0==AV19TFFuncaoAPF_AR_To) || ( ( A387FuncaoAPF_AR <= AV19TFFuncaoAPF_AR_To ) ) )
                        {
                           GXt_decimal3 = A386FuncaoAPF_PF;
                           new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
                           A386FuncaoAPF_PF = GXt_decimal3;
                           if ( (Convert.ToDecimal(0)==AV20TFFuncaoAPF_PF) || ( ( A386FuncaoAPF_PF >= AV20TFFuncaoAPF_PF ) ) )
                           {
                              if ( (Convert.ToDecimal(0)==AV21TFFuncaoAPF_PF_To) || ( ( A386FuncaoAPF_PF <= AV21TFFuncaoAPF_PF_To ) ) )
                              {
                                 AV38count = 0;
                                 while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00U73_A363FuncaoAPF_FunAPFPaiNom[0], A363FuncaoAPF_FunAPFPaiNom) == 0 ) )
                                 {
                                    BRKU74 = false;
                                    A358FuncaoAPF_FunAPFPaiCod = P00U73_A358FuncaoAPF_FunAPFPaiCod[0];
                                    n358FuncaoAPF_FunAPFPaiCod = P00U73_n358FuncaoAPF_FunAPFPaiCod[0];
                                    A165FuncaoAPF_Codigo = P00U73_A165FuncaoAPF_Codigo[0];
                                    AV38count = (long)(AV38count+1);
                                    BRKU74 = true;
                                    pr_default.readNext(1);
                                 }
                                 if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A363FuncaoAPF_FunAPFPaiNom)) )
                                 {
                                    AV30Option = A363FuncaoAPF_FunAPFPaiNom;
                                    AV31Options.Add(AV30Option, 0);
                                    AV36OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV38count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                                 }
                                 if ( AV31Options.Count == 50 )
                                 {
                                    /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                    if (true) break;
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRKU74 )
            {
               BRKU74 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV31Options = new GxSimpleCollection();
         AV34OptionsDesc = new GxSimpleCollection();
         AV36OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV39Session = context.GetSession();
         AV41GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV42GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFFuncaoAPF_Nome = "";
         AV11TFFuncaoAPF_Nome_Sel = "";
         AV12TFFuncaoAPF_Tipo_SelsJson = "";
         AV13TFFuncaoAPF_Tipo_Sels = new GxSimpleCollection();
         AV14TFFuncaoAPF_Complexidade_SelsJson = "";
         AV15TFFuncaoAPF_Complexidade_Sels = new GxSimpleCollection();
         AV22TFFuncaoAPF_FunAPFPaiNom = "";
         AV23TFFuncaoAPF_FunAPFPaiNom_Sel = "";
         AV24TFFuncaoAPF_Ativo_SelsJson = "";
         AV25TFFuncaoAPF_Ativo_Sels = new GxSimpleCollection();
         AV43GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV44DynamicFiltersSelector1 = "";
         AV46FuncaoAPF_Nome1 = "";
         AV47FuncaoAPF_SistemaDes1 = "";
         AV48FuncaoAPF_ModuloNom1 = "";
         AV49FuncaoAPF_FunAPFPaiNom1 = "";
         AV50FuncaoAPF_Tipo1 = "";
         AV52DynamicFiltersSelector2 = "";
         AV54FuncaoAPF_Nome2 = "";
         AV55FuncaoAPF_SistemaDes2 = "";
         AV56FuncaoAPF_ModuloNom2 = "";
         AV57FuncaoAPF_FunAPFPaiNom2 = "";
         AV58FuncaoAPF_Tipo2 = "";
         AV60DynamicFiltersSelector3 = "";
         AV62FuncaoAPF_Nome3 = "";
         AV63FuncaoAPF_SistemaDes3 = "";
         AV64FuncaoAPF_ModuloNom3 = "";
         AV65FuncaoAPF_FunAPFPaiNom3 = "";
         AV66FuncaoAPF_Tipo3 = "";
         scmdbuf = "";
         lV46FuncaoAPF_Nome1 = "";
         lV47FuncaoAPF_SistemaDes1 = "";
         lV48FuncaoAPF_ModuloNom1 = "";
         lV49FuncaoAPF_FunAPFPaiNom1 = "";
         lV54FuncaoAPF_Nome2 = "";
         lV55FuncaoAPF_SistemaDes2 = "";
         lV56FuncaoAPF_ModuloNom2 = "";
         lV57FuncaoAPF_FunAPFPaiNom2 = "";
         lV62FuncaoAPF_Nome3 = "";
         lV63FuncaoAPF_SistemaDes3 = "";
         lV64FuncaoAPF_ModuloNom3 = "";
         lV65FuncaoAPF_FunAPFPaiNom3 = "";
         lV10TFFuncaoAPF_Nome = "";
         lV22TFFuncaoAPF_FunAPFPaiNom = "";
         A185FuncaoAPF_Complexidade = "";
         A184FuncaoAPF_Tipo = "";
         A183FuncaoAPF_Ativo = "";
         A166FuncaoAPF_Nome = "";
         A362FuncaoAPF_SistemaDes = "";
         A361FuncaoAPF_ModuloNom = "";
         A363FuncaoAPF_FunAPFPaiNom = "";
         P00U72_A360FuncaoAPF_SistemaCod = new int[1] ;
         P00U72_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         P00U72_A359FuncaoAPF_ModuloCod = new int[1] ;
         P00U72_n359FuncaoAPF_ModuloCod = new bool[] {false} ;
         P00U72_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         P00U72_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         P00U72_A166FuncaoAPF_Nome = new String[] {""} ;
         P00U72_A183FuncaoAPF_Ativo = new String[] {""} ;
         P00U72_A184FuncaoAPF_Tipo = new String[] {""} ;
         P00U72_A363FuncaoAPF_FunAPFPaiNom = new String[] {""} ;
         P00U72_n363FuncaoAPF_FunAPFPaiNom = new bool[] {false} ;
         P00U72_A361FuncaoAPF_ModuloNom = new String[] {""} ;
         P00U72_n361FuncaoAPF_ModuloNom = new bool[] {false} ;
         P00U72_A362FuncaoAPF_SistemaDes = new String[] {""} ;
         P00U72_n362FuncaoAPF_SistemaDes = new bool[] {false} ;
         P00U72_A165FuncaoAPF_Codigo = new int[1] ;
         AV30Option = "";
         P00U73_A360FuncaoAPF_SistemaCod = new int[1] ;
         P00U73_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         P00U73_A359FuncaoAPF_ModuloCod = new int[1] ;
         P00U73_n359FuncaoAPF_ModuloCod = new bool[] {false} ;
         P00U73_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         P00U73_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         P00U73_A363FuncaoAPF_FunAPFPaiNom = new String[] {""} ;
         P00U73_n363FuncaoAPF_FunAPFPaiNom = new bool[] {false} ;
         P00U73_A183FuncaoAPF_Ativo = new String[] {""} ;
         P00U73_A184FuncaoAPF_Tipo = new String[] {""} ;
         P00U73_A361FuncaoAPF_ModuloNom = new String[] {""} ;
         P00U73_n361FuncaoAPF_ModuloNom = new bool[] {false} ;
         P00U73_A362FuncaoAPF_SistemaDes = new String[] {""} ;
         P00U73_n362FuncaoAPF_SistemaDes = new bool[] {false} ;
         P00U73_A166FuncaoAPF_Nome = new String[] {""} ;
         P00U73_A165FuncaoAPF_Codigo = new int[1] ;
         GXt_char1 = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptfuncaoapffilterdata__default(),
            new Object[][] {
                new Object[] {
               P00U72_A360FuncaoAPF_SistemaCod, P00U72_n360FuncaoAPF_SistemaCod, P00U72_A359FuncaoAPF_ModuloCod, P00U72_n359FuncaoAPF_ModuloCod, P00U72_A358FuncaoAPF_FunAPFPaiCod, P00U72_n358FuncaoAPF_FunAPFPaiCod, P00U72_A166FuncaoAPF_Nome, P00U72_A183FuncaoAPF_Ativo, P00U72_A184FuncaoAPF_Tipo, P00U72_A363FuncaoAPF_FunAPFPaiNom,
               P00U72_n363FuncaoAPF_FunAPFPaiNom, P00U72_A361FuncaoAPF_ModuloNom, P00U72_n361FuncaoAPF_ModuloNom, P00U72_A362FuncaoAPF_SistemaDes, P00U72_n362FuncaoAPF_SistemaDes, P00U72_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               P00U73_A360FuncaoAPF_SistemaCod, P00U73_n360FuncaoAPF_SistemaCod, P00U73_A359FuncaoAPF_ModuloCod, P00U73_n359FuncaoAPF_ModuloCod, P00U73_A358FuncaoAPF_FunAPFPaiCod, P00U73_n358FuncaoAPF_FunAPFPaiCod, P00U73_A363FuncaoAPF_FunAPFPaiNom, P00U73_n363FuncaoAPF_FunAPFPaiNom, P00U73_A183FuncaoAPF_Ativo, P00U73_A184FuncaoAPF_Tipo,
               P00U73_A361FuncaoAPF_ModuloNom, P00U73_n361FuncaoAPF_ModuloNom, P00U73_A362FuncaoAPF_SistemaDes, P00U73_n362FuncaoAPF_SistemaDes, P00U73_A166FuncaoAPF_Nome, P00U73_A165FuncaoAPF_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16TFFuncaoAPF_TD ;
      private short AV17TFFuncaoAPF_TD_To ;
      private short AV18TFFuncaoAPF_AR ;
      private short AV19TFFuncaoAPF_AR_To ;
      private short AV45DynamicFiltersOperator1 ;
      private short AV53DynamicFiltersOperator2 ;
      private short AV61DynamicFiltersOperator3 ;
      private short A388FuncaoAPF_TD ;
      private short A387FuncaoAPF_AR ;
      private short GXt_int2 ;
      private int AV69GXV1 ;
      private int AV13TFFuncaoAPF_Tipo_Sels_Count ;
      private int AV25TFFuncaoAPF_Ativo_Sels_Count ;
      private int AV15TFFuncaoAPF_Complexidade_Sels_Count ;
      private int A360FuncaoAPF_SistemaCod ;
      private int A359FuncaoAPF_ModuloCod ;
      private int A358FuncaoAPF_FunAPFPaiCod ;
      private int A165FuncaoAPF_Codigo ;
      private long AV38count ;
      private decimal AV20TFFuncaoAPF_PF ;
      private decimal AV21TFFuncaoAPF_PF_To ;
      private decimal A386FuncaoAPF_PF ;
      private decimal GXt_decimal3 ;
      private String AV48FuncaoAPF_ModuloNom1 ;
      private String AV50FuncaoAPF_Tipo1 ;
      private String AV56FuncaoAPF_ModuloNom2 ;
      private String AV58FuncaoAPF_Tipo2 ;
      private String AV64FuncaoAPF_ModuloNom3 ;
      private String AV66FuncaoAPF_Tipo3 ;
      private String scmdbuf ;
      private String lV48FuncaoAPF_ModuloNom1 ;
      private String lV56FuncaoAPF_ModuloNom2 ;
      private String lV64FuncaoAPF_ModuloNom3 ;
      private String A185FuncaoAPF_Complexidade ;
      private String A184FuncaoAPF_Tipo ;
      private String A183FuncaoAPF_Ativo ;
      private String A361FuncaoAPF_ModuloNom ;
      private String GXt_char1 ;
      private bool returnInSub ;
      private bool AV51DynamicFiltersEnabled2 ;
      private bool AV59DynamicFiltersEnabled3 ;
      private bool BRKU72 ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool n359FuncaoAPF_ModuloCod ;
      private bool n358FuncaoAPF_FunAPFPaiCod ;
      private bool n363FuncaoAPF_FunAPFPaiNom ;
      private bool n361FuncaoAPF_ModuloNom ;
      private bool n362FuncaoAPF_SistemaDes ;
      private bool BRKU74 ;
      private String AV37OptionIndexesJson ;
      private String AV32OptionsJson ;
      private String AV35OptionsDescJson ;
      private String AV12TFFuncaoAPF_Tipo_SelsJson ;
      private String AV14TFFuncaoAPF_Complexidade_SelsJson ;
      private String AV24TFFuncaoAPF_Ativo_SelsJson ;
      private String AV28DDOName ;
      private String AV26SearchTxt ;
      private String AV27SearchTxtTo ;
      private String AV10TFFuncaoAPF_Nome ;
      private String AV11TFFuncaoAPF_Nome_Sel ;
      private String AV22TFFuncaoAPF_FunAPFPaiNom ;
      private String AV23TFFuncaoAPF_FunAPFPaiNom_Sel ;
      private String AV44DynamicFiltersSelector1 ;
      private String AV46FuncaoAPF_Nome1 ;
      private String AV47FuncaoAPF_SistemaDes1 ;
      private String AV49FuncaoAPF_FunAPFPaiNom1 ;
      private String AV52DynamicFiltersSelector2 ;
      private String AV54FuncaoAPF_Nome2 ;
      private String AV55FuncaoAPF_SistemaDes2 ;
      private String AV57FuncaoAPF_FunAPFPaiNom2 ;
      private String AV60DynamicFiltersSelector3 ;
      private String AV62FuncaoAPF_Nome3 ;
      private String AV63FuncaoAPF_SistemaDes3 ;
      private String AV65FuncaoAPF_FunAPFPaiNom3 ;
      private String lV46FuncaoAPF_Nome1 ;
      private String lV47FuncaoAPF_SistemaDes1 ;
      private String lV49FuncaoAPF_FunAPFPaiNom1 ;
      private String lV54FuncaoAPF_Nome2 ;
      private String lV55FuncaoAPF_SistemaDes2 ;
      private String lV57FuncaoAPF_FunAPFPaiNom2 ;
      private String lV62FuncaoAPF_Nome3 ;
      private String lV63FuncaoAPF_SistemaDes3 ;
      private String lV65FuncaoAPF_FunAPFPaiNom3 ;
      private String lV10TFFuncaoAPF_Nome ;
      private String lV22TFFuncaoAPF_FunAPFPaiNom ;
      private String A166FuncaoAPF_Nome ;
      private String A362FuncaoAPF_SistemaDes ;
      private String A363FuncaoAPF_FunAPFPaiNom ;
      private String AV30Option ;
      private IGxSession AV39Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00U72_A360FuncaoAPF_SistemaCod ;
      private bool[] P00U72_n360FuncaoAPF_SistemaCod ;
      private int[] P00U72_A359FuncaoAPF_ModuloCod ;
      private bool[] P00U72_n359FuncaoAPF_ModuloCod ;
      private int[] P00U72_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] P00U72_n358FuncaoAPF_FunAPFPaiCod ;
      private String[] P00U72_A166FuncaoAPF_Nome ;
      private String[] P00U72_A183FuncaoAPF_Ativo ;
      private String[] P00U72_A184FuncaoAPF_Tipo ;
      private String[] P00U72_A363FuncaoAPF_FunAPFPaiNom ;
      private bool[] P00U72_n363FuncaoAPF_FunAPFPaiNom ;
      private String[] P00U72_A361FuncaoAPF_ModuloNom ;
      private bool[] P00U72_n361FuncaoAPF_ModuloNom ;
      private String[] P00U72_A362FuncaoAPF_SistemaDes ;
      private bool[] P00U72_n362FuncaoAPF_SistemaDes ;
      private int[] P00U72_A165FuncaoAPF_Codigo ;
      private int[] P00U73_A360FuncaoAPF_SistemaCod ;
      private bool[] P00U73_n360FuncaoAPF_SistemaCod ;
      private int[] P00U73_A359FuncaoAPF_ModuloCod ;
      private bool[] P00U73_n359FuncaoAPF_ModuloCod ;
      private int[] P00U73_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] P00U73_n358FuncaoAPF_FunAPFPaiCod ;
      private String[] P00U73_A363FuncaoAPF_FunAPFPaiNom ;
      private bool[] P00U73_n363FuncaoAPF_FunAPFPaiNom ;
      private String[] P00U73_A183FuncaoAPF_Ativo ;
      private String[] P00U73_A184FuncaoAPF_Tipo ;
      private String[] P00U73_A361FuncaoAPF_ModuloNom ;
      private bool[] P00U73_n361FuncaoAPF_ModuloNom ;
      private String[] P00U73_A362FuncaoAPF_SistemaDes ;
      private bool[] P00U73_n362FuncaoAPF_SistemaDes ;
      private String[] P00U73_A166FuncaoAPF_Nome ;
      private int[] P00U73_A165FuncaoAPF_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFFuncaoAPF_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV15TFFuncaoAPF_Complexidade_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25TFFuncaoAPF_Ativo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV31Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV36OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV41GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV42GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV43GridStateDynamicFilter ;
   }

   public class getpromptfuncaoapffilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00U72( IGxContext context ,
                                             String A185FuncaoAPF_Complexidade ,
                                             IGxCollection AV15TFFuncaoAPF_Complexidade_Sels ,
                                             String A184FuncaoAPF_Tipo ,
                                             IGxCollection AV13TFFuncaoAPF_Tipo_Sels ,
                                             String A183FuncaoAPF_Ativo ,
                                             IGxCollection AV25TFFuncaoAPF_Ativo_Sels ,
                                             String AV44DynamicFiltersSelector1 ,
                                             short AV45DynamicFiltersOperator1 ,
                                             String AV46FuncaoAPF_Nome1 ,
                                             String AV47FuncaoAPF_SistemaDes1 ,
                                             String AV48FuncaoAPF_ModuloNom1 ,
                                             String AV49FuncaoAPF_FunAPFPaiNom1 ,
                                             String AV50FuncaoAPF_Tipo1 ,
                                             bool AV51DynamicFiltersEnabled2 ,
                                             String AV52DynamicFiltersSelector2 ,
                                             short AV53DynamicFiltersOperator2 ,
                                             String AV54FuncaoAPF_Nome2 ,
                                             String AV55FuncaoAPF_SistemaDes2 ,
                                             String AV56FuncaoAPF_ModuloNom2 ,
                                             String AV57FuncaoAPF_FunAPFPaiNom2 ,
                                             String AV58FuncaoAPF_Tipo2 ,
                                             bool AV59DynamicFiltersEnabled3 ,
                                             String AV60DynamicFiltersSelector3 ,
                                             short AV61DynamicFiltersOperator3 ,
                                             String AV62FuncaoAPF_Nome3 ,
                                             String AV63FuncaoAPF_SistemaDes3 ,
                                             String AV64FuncaoAPF_ModuloNom3 ,
                                             String AV65FuncaoAPF_FunAPFPaiNom3 ,
                                             String AV66FuncaoAPF_Tipo3 ,
                                             String AV11TFFuncaoAPF_Nome_Sel ,
                                             String AV10TFFuncaoAPF_Nome ,
                                             int AV13TFFuncaoAPF_Tipo_Sels_Count ,
                                             String AV23TFFuncaoAPF_FunAPFPaiNom_Sel ,
                                             String AV22TFFuncaoAPF_FunAPFPaiNom ,
                                             int AV25TFFuncaoAPF_Ativo_Sels_Count ,
                                             String A166FuncaoAPF_Nome ,
                                             String A362FuncaoAPF_SistemaDes ,
                                             String A361FuncaoAPF_ModuloNom ,
                                             String A363FuncaoAPF_FunAPFPaiNom ,
                                             int AV15TFFuncaoAPF_Complexidade_Sels_Count ,
                                             short AV16TFFuncaoAPF_TD ,
                                             short A388FuncaoAPF_TD ,
                                             short AV17TFFuncaoAPF_TD_To ,
                                             short AV18TFFuncaoAPF_AR ,
                                             short A387FuncaoAPF_AR ,
                                             short AV19TFFuncaoAPF_AR_To ,
                                             decimal AV20TFFuncaoAPF_PF ,
                                             decimal A386FuncaoAPF_PF ,
                                             decimal AV21TFFuncaoAPF_PF_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [32] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoAPF_SistemaCod] AS FuncaoAPF_SistemaCod, T1.[FuncaoAPF_ModuloCod] AS FuncaoAPF_ModuloCod, T1.[FuncaoAPF_FunAPFPaiCod] AS FuncaoAPF_FunAPFPaiCod, T1.[FuncaoAPF_Nome], T1.[FuncaoAPF_Ativo], T1.[FuncaoAPF_Tipo], T4.[FuncaoAPF_Nome] AS FuncaoAPF_FunAPFPaiNom, T3.[Modulo_Nome] AS FuncaoAPF_ModuloNom, T2.[Sistema_Nome] AS FuncaoAPF_SistemaDes, T1.[FuncaoAPF_Codigo] FROM ((([FuncoesAPF] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[FuncaoAPF_SistemaCod]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = T1.[FuncaoAPF_ModuloCod]) LEFT JOIN [FuncoesAPF] T4 WITH (NOLOCK) ON T4.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_FunAPFPaiCod])";
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( AV45DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46FuncaoAPF_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV46FuncaoAPF_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV46FuncaoAPF_Nome1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( AV45DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46FuncaoAPF_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like '%' + @lV46FuncaoAPF_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like '%' + @lV46FuncaoAPF_Nome1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV45DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47FuncaoAPF_SistemaDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV47FuncaoAPF_SistemaDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like @lV47FuncaoAPF_SistemaDes1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV45DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47FuncaoAPF_SistemaDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like '%' + @lV47FuncaoAPF_SistemaDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like '%' + @lV47FuncaoAPF_SistemaDes1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV45DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48FuncaoAPF_ModuloNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like @lV48FuncaoAPF_ModuloNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like @lV48FuncaoAPF_ModuloNom1)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV45DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48FuncaoAPF_ModuloNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like '%' + @lV48FuncaoAPF_ModuloNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like '%' + @lV48FuncaoAPF_ModuloNom1)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV45DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49FuncaoAPF_FunAPFPaiNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV49FuncaoAPF_FunAPFPaiNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV49FuncaoAPF_FunAPFPaiNom1)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV45DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49FuncaoAPF_FunAPFPaiNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV49FuncaoAPF_FunAPFPaiNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV49FuncaoAPF_FunAPFPaiNom1)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50FuncaoAPF_Tipo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV50FuncaoAPF_Tipo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV50FuncaoAPF_Tipo1)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( AV53DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54FuncaoAPF_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV54FuncaoAPF_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV54FuncaoAPF_Nome2)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( AV53DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54FuncaoAPF_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like '%' + @lV54FuncaoAPF_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like '%' + @lV54FuncaoAPF_Nome2)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV53DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55FuncaoAPF_SistemaDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV55FuncaoAPF_SistemaDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like @lV55FuncaoAPF_SistemaDes2)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV53DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55FuncaoAPF_SistemaDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like '%' + @lV55FuncaoAPF_SistemaDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like '%' + @lV55FuncaoAPF_SistemaDes2)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV53DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56FuncaoAPF_ModuloNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like @lV56FuncaoAPF_ModuloNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like @lV56FuncaoAPF_ModuloNom2)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV53DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56FuncaoAPF_ModuloNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like '%' + @lV56FuncaoAPF_ModuloNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like '%' + @lV56FuncaoAPF_ModuloNom2)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV53DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57FuncaoAPF_FunAPFPaiNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV57FuncaoAPF_FunAPFPaiNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV57FuncaoAPF_FunAPFPaiNom2)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV53DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57FuncaoAPF_FunAPFPaiNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV57FuncaoAPF_FunAPFPaiNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV57FuncaoAPF_FunAPFPaiNom2)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58FuncaoAPF_Tipo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV58FuncaoAPF_Tipo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV58FuncaoAPF_Tipo2)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( AV61DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62FuncaoAPF_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV62FuncaoAPF_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV62FuncaoAPF_Nome3)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( AV61DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62FuncaoAPF_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like '%' + @lV62FuncaoAPF_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like '%' + @lV62FuncaoAPF_Nome3)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV61DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63FuncaoAPF_SistemaDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV63FuncaoAPF_SistemaDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like @lV63FuncaoAPF_SistemaDes3)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV61DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63FuncaoAPF_SistemaDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like '%' + @lV63FuncaoAPF_SistemaDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like '%' + @lV63FuncaoAPF_SistemaDes3)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV61DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64FuncaoAPF_ModuloNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like @lV64FuncaoAPF_ModuloNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like @lV64FuncaoAPF_ModuloNom3)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV61DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64FuncaoAPF_ModuloNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like '%' + @lV64FuncaoAPF_ModuloNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like '%' + @lV64FuncaoAPF_ModuloNom3)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV61DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65FuncaoAPF_FunAPFPaiNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV65FuncaoAPF_FunAPFPaiNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV65FuncaoAPF_FunAPFPaiNom3)";
            }
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV61DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65FuncaoAPF_FunAPFPaiNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV65FuncaoAPF_FunAPFPaiNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV65FuncaoAPF_FunAPFPaiNom3)";
            }
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66FuncaoAPF_Tipo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV66FuncaoAPF_Tipo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV66FuncaoAPF_Tipo3)";
            }
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPF_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncaoAPF_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV10TFFuncaoAPF_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV10TFFuncaoAPF_Nome)";
            }
         }
         else
         {
            GXv_int4[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPF_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] = @AV11TFFuncaoAPF_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] = @AV11TFFuncaoAPF_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[28] = 1;
         }
         if ( AV13TFFuncaoAPF_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFFuncaoAPF_Tipo_Sels, "T1.[FuncaoAPF_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFFuncaoAPF_Tipo_Sels, "T1.[FuncaoAPF_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFFuncaoAPF_FunAPFPaiNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFFuncaoAPF_FunAPFPaiNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV22TFFuncaoAPF_FunAPFPaiNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV22TFFuncaoAPF_FunAPFPaiNom)";
            }
         }
         else
         {
            GXv_int4[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFFuncaoAPF_FunAPFPaiNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] = @AV23TFFuncaoAPF_FunAPFPaiNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] = @AV23TFFuncaoAPF_FunAPFPaiNom_Sel)";
            }
         }
         else
         {
            GXv_int4[30] = 1;
         }
         if ( AV25TFFuncaoAPF_Ativo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV25TFFuncaoAPF_Ativo_Sels, "T1.[FuncaoAPF_Ativo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV25TFFuncaoAPF_Ativo_Sels, "T1.[FuncaoAPF_Ativo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Nome]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_P00U73( IGxContext context ,
                                             String A185FuncaoAPF_Complexidade ,
                                             IGxCollection AV15TFFuncaoAPF_Complexidade_Sels ,
                                             String A184FuncaoAPF_Tipo ,
                                             IGxCollection AV13TFFuncaoAPF_Tipo_Sels ,
                                             String A183FuncaoAPF_Ativo ,
                                             IGxCollection AV25TFFuncaoAPF_Ativo_Sels ,
                                             String AV44DynamicFiltersSelector1 ,
                                             short AV45DynamicFiltersOperator1 ,
                                             String AV46FuncaoAPF_Nome1 ,
                                             String AV47FuncaoAPF_SistemaDes1 ,
                                             String AV48FuncaoAPF_ModuloNom1 ,
                                             String AV49FuncaoAPF_FunAPFPaiNom1 ,
                                             String AV50FuncaoAPF_Tipo1 ,
                                             bool AV51DynamicFiltersEnabled2 ,
                                             String AV52DynamicFiltersSelector2 ,
                                             short AV53DynamicFiltersOperator2 ,
                                             String AV54FuncaoAPF_Nome2 ,
                                             String AV55FuncaoAPF_SistemaDes2 ,
                                             String AV56FuncaoAPF_ModuloNom2 ,
                                             String AV57FuncaoAPF_FunAPFPaiNom2 ,
                                             String AV58FuncaoAPF_Tipo2 ,
                                             bool AV59DynamicFiltersEnabled3 ,
                                             String AV60DynamicFiltersSelector3 ,
                                             short AV61DynamicFiltersOperator3 ,
                                             String AV62FuncaoAPF_Nome3 ,
                                             String AV63FuncaoAPF_SistemaDes3 ,
                                             String AV64FuncaoAPF_ModuloNom3 ,
                                             String AV65FuncaoAPF_FunAPFPaiNom3 ,
                                             String AV66FuncaoAPF_Tipo3 ,
                                             String AV11TFFuncaoAPF_Nome_Sel ,
                                             String AV10TFFuncaoAPF_Nome ,
                                             int AV13TFFuncaoAPF_Tipo_Sels_Count ,
                                             String AV23TFFuncaoAPF_FunAPFPaiNom_Sel ,
                                             String AV22TFFuncaoAPF_FunAPFPaiNom ,
                                             int AV25TFFuncaoAPF_Ativo_Sels_Count ,
                                             String A166FuncaoAPF_Nome ,
                                             String A362FuncaoAPF_SistemaDes ,
                                             String A361FuncaoAPF_ModuloNom ,
                                             String A363FuncaoAPF_FunAPFPaiNom ,
                                             int AV15TFFuncaoAPF_Complexidade_Sels_Count ,
                                             short AV16TFFuncaoAPF_TD ,
                                             short A388FuncaoAPF_TD ,
                                             short AV17TFFuncaoAPF_TD_To ,
                                             short AV18TFFuncaoAPF_AR ,
                                             short A387FuncaoAPF_AR ,
                                             short AV19TFFuncaoAPF_AR_To ,
                                             decimal AV20TFFuncaoAPF_PF ,
                                             decimal A386FuncaoAPF_PF ,
                                             decimal AV21TFFuncaoAPF_PF_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [32] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoAPF_SistemaCod] AS FuncaoAPF_SistemaCod, T1.[FuncaoAPF_ModuloCod] AS FuncaoAPF_ModuloCod, T1.[FuncaoAPF_FunAPFPaiCod] AS FuncaoAPF_FunAPFPaiCod, T4.[FuncaoAPF_Nome] AS FuncaoAPF_FunAPFPaiNom, T1.[FuncaoAPF_Ativo], T1.[FuncaoAPF_Tipo], T3.[Modulo_Nome] AS FuncaoAPF_ModuloNom, T2.[Sistema_Nome] AS FuncaoAPF_SistemaDes, T1.[FuncaoAPF_Nome], T1.[FuncaoAPF_Codigo] FROM ((([FuncoesAPF] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[FuncaoAPF_SistemaCod]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = T1.[FuncaoAPF_ModuloCod]) LEFT JOIN [FuncoesAPF] T4 WITH (NOLOCK) ON T4.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_FunAPFPaiCod])";
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( AV45DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46FuncaoAPF_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV46FuncaoAPF_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV46FuncaoAPF_Nome1)";
            }
         }
         else
         {
            GXv_int6[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( AV45DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46FuncaoAPF_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like '%' + @lV46FuncaoAPF_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like '%' + @lV46FuncaoAPF_Nome1)";
            }
         }
         else
         {
            GXv_int6[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV45DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47FuncaoAPF_SistemaDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV47FuncaoAPF_SistemaDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like @lV47FuncaoAPF_SistemaDes1)";
            }
         }
         else
         {
            GXv_int6[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV45DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47FuncaoAPF_SistemaDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like '%' + @lV47FuncaoAPF_SistemaDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like '%' + @lV47FuncaoAPF_SistemaDes1)";
            }
         }
         else
         {
            GXv_int6[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV45DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48FuncaoAPF_ModuloNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like @lV48FuncaoAPF_ModuloNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like @lV48FuncaoAPF_ModuloNom1)";
            }
         }
         else
         {
            GXv_int6[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV45DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48FuncaoAPF_ModuloNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like '%' + @lV48FuncaoAPF_ModuloNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like '%' + @lV48FuncaoAPF_ModuloNom1)";
            }
         }
         else
         {
            GXv_int6[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV45DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49FuncaoAPF_FunAPFPaiNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV49FuncaoAPF_FunAPFPaiNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV49FuncaoAPF_FunAPFPaiNom1)";
            }
         }
         else
         {
            GXv_int6[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV45DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49FuncaoAPF_FunAPFPaiNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV49FuncaoAPF_FunAPFPaiNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV49FuncaoAPF_FunAPFPaiNom1)";
            }
         }
         else
         {
            GXv_int6[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50FuncaoAPF_Tipo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV50FuncaoAPF_Tipo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV50FuncaoAPF_Tipo1)";
            }
         }
         else
         {
            GXv_int6[8] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( AV53DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54FuncaoAPF_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV54FuncaoAPF_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV54FuncaoAPF_Nome2)";
            }
         }
         else
         {
            GXv_int6[9] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( AV53DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54FuncaoAPF_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like '%' + @lV54FuncaoAPF_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like '%' + @lV54FuncaoAPF_Nome2)";
            }
         }
         else
         {
            GXv_int6[10] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV53DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55FuncaoAPF_SistemaDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV55FuncaoAPF_SistemaDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like @lV55FuncaoAPF_SistemaDes2)";
            }
         }
         else
         {
            GXv_int6[11] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV53DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55FuncaoAPF_SistemaDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like '%' + @lV55FuncaoAPF_SistemaDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like '%' + @lV55FuncaoAPF_SistemaDes2)";
            }
         }
         else
         {
            GXv_int6[12] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV53DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56FuncaoAPF_ModuloNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like @lV56FuncaoAPF_ModuloNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like @lV56FuncaoAPF_ModuloNom2)";
            }
         }
         else
         {
            GXv_int6[13] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV53DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56FuncaoAPF_ModuloNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like '%' + @lV56FuncaoAPF_ModuloNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like '%' + @lV56FuncaoAPF_ModuloNom2)";
            }
         }
         else
         {
            GXv_int6[14] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV53DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57FuncaoAPF_FunAPFPaiNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV57FuncaoAPF_FunAPFPaiNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV57FuncaoAPF_FunAPFPaiNom2)";
            }
         }
         else
         {
            GXv_int6[15] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV53DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57FuncaoAPF_FunAPFPaiNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV57FuncaoAPF_FunAPFPaiNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV57FuncaoAPF_FunAPFPaiNom2)";
            }
         }
         else
         {
            GXv_int6[16] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58FuncaoAPF_Tipo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV58FuncaoAPF_Tipo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV58FuncaoAPF_Tipo2)";
            }
         }
         else
         {
            GXv_int6[17] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( AV61DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62FuncaoAPF_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV62FuncaoAPF_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV62FuncaoAPF_Nome3)";
            }
         }
         else
         {
            GXv_int6[18] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( AV61DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62FuncaoAPF_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like '%' + @lV62FuncaoAPF_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like '%' + @lV62FuncaoAPF_Nome3)";
            }
         }
         else
         {
            GXv_int6[19] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV61DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63FuncaoAPF_SistemaDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV63FuncaoAPF_SistemaDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like @lV63FuncaoAPF_SistemaDes3)";
            }
         }
         else
         {
            GXv_int6[20] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV61DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63FuncaoAPF_SistemaDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like '%' + @lV63FuncaoAPF_SistemaDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like '%' + @lV63FuncaoAPF_SistemaDes3)";
            }
         }
         else
         {
            GXv_int6[21] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV61DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64FuncaoAPF_ModuloNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like @lV64FuncaoAPF_ModuloNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like @lV64FuncaoAPF_ModuloNom3)";
            }
         }
         else
         {
            GXv_int6[22] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV61DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64FuncaoAPF_ModuloNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like '%' + @lV64FuncaoAPF_ModuloNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like '%' + @lV64FuncaoAPF_ModuloNom3)";
            }
         }
         else
         {
            GXv_int6[23] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV61DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65FuncaoAPF_FunAPFPaiNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV65FuncaoAPF_FunAPFPaiNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV65FuncaoAPF_FunAPFPaiNom3)";
            }
         }
         else
         {
            GXv_int6[24] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV61DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65FuncaoAPF_FunAPFPaiNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV65FuncaoAPF_FunAPFPaiNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV65FuncaoAPF_FunAPFPaiNom3)";
            }
         }
         else
         {
            GXv_int6[25] = 1;
         }
         if ( AV59DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector3, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66FuncaoAPF_Tipo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV66FuncaoAPF_Tipo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV66FuncaoAPF_Tipo3)";
            }
         }
         else
         {
            GXv_int6[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPF_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncaoAPF_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV10TFFuncaoAPF_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV10TFFuncaoAPF_Nome)";
            }
         }
         else
         {
            GXv_int6[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPF_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] = @AV11TFFuncaoAPF_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] = @AV11TFFuncaoAPF_Nome_Sel)";
            }
         }
         else
         {
            GXv_int6[28] = 1;
         }
         if ( AV13TFFuncaoAPF_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFFuncaoAPF_Tipo_Sels, "T1.[FuncaoAPF_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFFuncaoAPF_Tipo_Sels, "T1.[FuncaoAPF_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFFuncaoAPF_FunAPFPaiNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFFuncaoAPF_FunAPFPaiNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV22TFFuncaoAPF_FunAPFPaiNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV22TFFuncaoAPF_FunAPFPaiNom)";
            }
         }
         else
         {
            GXv_int6[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFFuncaoAPF_FunAPFPaiNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] = @AV23TFFuncaoAPF_FunAPFPaiNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] = @AV23TFFuncaoAPF_FunAPFPaiNom_Sel)";
            }
         }
         else
         {
            GXv_int6[30] = 1;
         }
         if ( AV25TFFuncaoAPF_Ativo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV25TFFuncaoAPF_Ativo_Sels, "T1.[FuncaoAPF_Ativo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV25TFFuncaoAPF_Ativo_Sels, "T1.[FuncaoAPF_Ativo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[FuncaoAPF_Nome]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00U72(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (bool)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (int)dynConstraints[39] , (short)dynConstraints[40] , (short)dynConstraints[41] , (short)dynConstraints[42] , (short)dynConstraints[43] , (short)dynConstraints[44] , (short)dynConstraints[45] , (decimal)dynConstraints[46] , (decimal)dynConstraints[47] , (decimal)dynConstraints[48] );
               case 1 :
                     return conditional_P00U73(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (bool)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (int)dynConstraints[39] , (short)dynConstraints[40] , (short)dynConstraints[41] , (short)dynConstraints[42] , (short)dynConstraints[43] , (short)dynConstraints[44] , (short)dynConstraints[45] , (decimal)dynConstraints[46] , (decimal)dynConstraints[47] , (decimal)dynConstraints[48] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00U72 ;
          prmP00U72 = new Object[] {
          new Object[] {"@AV15TFFuCount",SqlDbType.Int,9,0} ,
          new Object[] {"@lV46FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV46FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV47FuncaoAPF_SistemaDes1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV47FuncaoAPF_SistemaDes1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV48FuncaoAPF_ModuloNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48FuncaoAPF_ModuloNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49FuncaoAPF_FunAPFPaiNom1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV49FuncaoAPF_FunAPFPaiNom1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV50FuncaoAPF_Tipo1",SqlDbType.Char,3,0} ,
          new Object[] {"@lV54FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV54FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV55FuncaoAPF_SistemaDes2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV55FuncaoAPF_SistemaDes2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV56FuncaoAPF_ModuloNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV56FuncaoAPF_ModuloNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57FuncaoAPF_FunAPFPaiNom2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV57FuncaoAPF_FunAPFPaiNom2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV58FuncaoAPF_Tipo2",SqlDbType.Char,3,0} ,
          new Object[] {"@lV62FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV62FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV63FuncaoAPF_SistemaDes3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV63FuncaoAPF_SistemaDes3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV64FuncaoAPF_ModuloNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64FuncaoAPF_ModuloNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65FuncaoAPF_FunAPFPaiNom3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV65FuncaoAPF_FunAPFPaiNom3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV66FuncaoAPF_Tipo3",SqlDbType.Char,3,0} ,
          new Object[] {"@lV10TFFuncaoAPF_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV11TFFuncaoAPF_Nome_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV22TFFuncaoAPF_FunAPFPaiNom",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV23TFFuncaoAPF_FunAPFPaiNom_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00U73 ;
          prmP00U73 = new Object[] {
          new Object[] {"@AV15TFFuCount",SqlDbType.Int,9,0} ,
          new Object[] {"@lV46FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV46FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV47FuncaoAPF_SistemaDes1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV47FuncaoAPF_SistemaDes1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV48FuncaoAPF_ModuloNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48FuncaoAPF_ModuloNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49FuncaoAPF_FunAPFPaiNom1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV49FuncaoAPF_FunAPFPaiNom1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV50FuncaoAPF_Tipo1",SqlDbType.Char,3,0} ,
          new Object[] {"@lV54FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV54FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV55FuncaoAPF_SistemaDes2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV55FuncaoAPF_SistemaDes2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV56FuncaoAPF_ModuloNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV56FuncaoAPF_ModuloNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57FuncaoAPF_FunAPFPaiNom2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV57FuncaoAPF_FunAPFPaiNom2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV58FuncaoAPF_Tipo2",SqlDbType.Char,3,0} ,
          new Object[] {"@lV62FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV62FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV63FuncaoAPF_SistemaDes3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV63FuncaoAPF_SistemaDes3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV64FuncaoAPF_ModuloNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64FuncaoAPF_ModuloNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65FuncaoAPF_FunAPFPaiNom3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV65FuncaoAPF_FunAPFPaiNom3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV66FuncaoAPF_Tipo3",SqlDbType.Char,3,0} ,
          new Object[] {"@lV10TFFuncaoAPF_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV11TFFuncaoAPF_Nome_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV22TFFuncaoAPF_FunAPFPaiNom",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV23TFFuncaoAPF_FunAPFPaiNom_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00U72", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U72,100,0,true,false )
             ,new CursorDef("P00U73", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U73,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 3) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 3) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptfuncaoapffilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptfuncaoapffilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptfuncaoapffilterdata") )
          {
             return  ;
          }
          getpromptfuncaoapffilterdata worker = new getpromptfuncaoapffilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
