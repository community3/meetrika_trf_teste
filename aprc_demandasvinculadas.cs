/*
               File: PRC_DemandasVinculadas
        Description: Demandas Vinculadas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/4/2020 0:16:43.64
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_demandasvinculadas : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV54Contratada_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV130Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV57ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV189PrazoDemanda = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV187ContagemResultado_AgrupadorAux = GetNextPar( );
                  AV185IsModificarAgrupador = (short)(NumberUtil.Val( GetNextPar( ), "."));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aprc_demandasvinculadas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_demandasvinculadas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_Codigo ,
                           int aP1_Contrato_Codigo ,
                           int aP2_ContratoServicos_Codigo ,
                           DateTime aP3_PrazoDemanda ,
                           String aP4_ContagemResultado_AgrupadorAux ,
                           short aP5_IsModificarAgrupador )
      {
         this.AV54Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV130Contrato_Codigo = aP1_Contrato_Codigo;
         this.AV57ContratoServicos_Codigo = aP2_ContratoServicos_Codigo;
         this.AV189PrazoDemanda = aP3_PrazoDemanda;
         this.AV187ContagemResultado_AgrupadorAux = aP4_ContagemResultado_AgrupadorAux;
         this.AV185IsModificarAgrupador = aP5_IsModificarAgrupador;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_Codigo ,
                                 int aP1_Contrato_Codigo ,
                                 int aP2_ContratoServicos_Codigo ,
                                 DateTime aP3_PrazoDemanda ,
                                 String aP4_ContagemResultado_AgrupadorAux ,
                                 short aP5_IsModificarAgrupador )
      {
         aprc_demandasvinculadas objaprc_demandasvinculadas;
         objaprc_demandasvinculadas = new aprc_demandasvinculadas();
         objaprc_demandasvinculadas.AV54Contratada_Codigo = aP0_Contratada_Codigo;
         objaprc_demandasvinculadas.AV130Contrato_Codigo = aP1_Contrato_Codigo;
         objaprc_demandasvinculadas.AV57ContratoServicos_Codigo = aP2_ContratoServicos_Codigo;
         objaprc_demandasvinculadas.AV189PrazoDemanda = aP3_PrazoDemanda;
         objaprc_demandasvinculadas.AV187ContagemResultado_AgrupadorAux = aP4_ContagemResultado_AgrupadorAux;
         objaprc_demandasvinculadas.AV185IsModificarAgrupador = aP5_IsModificarAgrupador;
         objaprc_demandasvinculadas.context.SetSubmitInitialConfig(context);
         objaprc_demandasvinculadas.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_demandasvinculadas);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_demandasvinculadas)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 1, 15840, 12240, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV126WWPContext) ;
            AV110ServerNow = DateTimeUtil.ServerNow( context, "DEFAULT");
            AV170ServerDate = DateTimeUtil.ServerDate( context, "DEFAULT");
            /* Execute user subroutine: 'DADOS.CONTRATADA' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            AV40ContagemResltado_Deflator = (decimal)(1);
            /* Using cursor P00YR2 */
            pr_default.execute(0, new Object[] {AV57ContratoServicos_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A74Contrato_Codigo = P00YR2_A74Contrato_Codigo[0];
               A39Contratada_Codigo = P00YR2_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00YR2_A40Contratada_PessoaCod[0];
               A52Contratada_AreaTrabalhoCod = P00YR2_A52Contratada_AreaTrabalhoCod[0];
               A160ContratoServicos_Codigo = P00YR2_A160ContratoServicos_Codigo[0];
               A41Contratada_PessoaNom = P00YR2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = P00YR2_n41Contratada_PessoaNom[0];
               A558Servico_Percentual = P00YR2_A558Servico_Percentual[0];
               n558Servico_Percentual = P00YR2_n558Servico_Percentual[0];
               A116Contrato_ValorUnidadeContratacao = P00YR2_A116Contrato_ValorUnidadeContratacao[0];
               A1224ContratoServicos_PrazoCorrecao = P00YR2_A1224ContratoServicos_PrazoCorrecao[0];
               n1224ContratoServicos_PrazoCorrecao = P00YR2_n1224ContratoServicos_PrazoCorrecao[0];
               A1225ContratoServicos_PrazoCorrecaoTipo = P00YR2_A1225ContratoServicos_PrazoCorrecaoTipo[0];
               n1225ContratoServicos_PrazoCorrecaoTipo = P00YR2_n1225ContratoServicos_PrazoCorrecaoTipo[0];
               A1649ContratoServicos_PrazoInicio = P00YR2_A1649ContratoServicos_PrazoInicio[0];
               n1649ContratoServicos_PrazoInicio = P00YR2_n1649ContratoServicos_PrazoInicio[0];
               A1152ContratoServicos_PrazoAnalise = P00YR2_A1152ContratoServicos_PrazoAnalise[0];
               n1152ContratoServicos_PrazoAnalise = P00YR2_n1152ContratoServicos_PrazoAnalise[0];
               A557Servico_VlrUnidadeContratada = P00YR2_A557Servico_VlrUnidadeContratada[0];
               A1454ContratoServicos_PrazoTpDias = P00YR2_A1454ContratoServicos_PrazoTpDias[0];
               n1454ContratoServicos_PrazoTpDias = P00YR2_n1454ContratoServicos_PrazoTpDias[0];
               A39Contratada_Codigo = P00YR2_A39Contratada_Codigo[0];
               A116Contrato_ValorUnidadeContratacao = P00YR2_A116Contrato_ValorUnidadeContratacao[0];
               A40Contratada_PessoaCod = P00YR2_A40Contratada_PessoaCod[0];
               A52Contratada_AreaTrabalhoCod = P00YR2_A52Contratada_AreaTrabalhoCod[0];
               A41Contratada_PessoaNom = P00YR2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = P00YR2_n41Contratada_PessoaNom[0];
               OV106PrazoInicio = AV106PrazoInicio;
               AV56ContratadaFS_Nome = A41Contratada_PessoaNom;
               AV40ContagemResltado_Deflator = A558Servico_Percentual;
               AV123ValorPFFS = A116Contrato_ValorUnidadeContratacao;
               AV102PrazoCoreecao = A1224ContratoServicos_PrazoCorrecao;
               AV103PrazoCorrecaoTipo = A1225ContratoServicos_PrazoCorrecaoTipo;
               AV106PrazoInicio = A1649ContratoServicos_PrazoInicio;
               AV66DiasParaAnalise = A1152ContratoServicos_PrazoAnalise;
               AV53ContagemResultado_ValorPF = A557Servico_VlrUnidadeContratada;
               AV199TipoDias = A1454ContratoServicos_PrazoTpDias;
               /* Using cursor P00YR3 */
               pr_default.execute(1, new Object[] {A52Contratada_AreaTrabalhoCod});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A5AreaTrabalho_Codigo = P00YR3_A5AreaTrabalho_Codigo[0];
                  A29Contratante_Codigo = P00YR3_A29Contratante_Codigo[0];
                  n29Contratante_Codigo = P00YR3_n29Contratante_Codigo[0];
                  A1192Contratante_FimDoExpediente = P00YR3_A1192Contratante_FimDoExpediente[0];
                  n1192Contratante_FimDoExpediente = P00YR3_n1192Contratante_FimDoExpediente[0];
                  A1192Contratante_FimDoExpediente = P00YR3_A1192Contratante_FimDoExpediente[0];
                  n1192Contratante_FimDoExpediente = P00YR3_n1192Contratante_FimDoExpediente[0];
                  /* Using cursor P00YR4 */
                  pr_default.execute(2, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
                  while ( (pr_default.getStatus(2) != 101) )
                  {
                     AV76FimDoExpediente = A1192Contratante_FimDoExpediente;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  pr_default.close(2);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(1);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            AV132ContagemResultado_CodigoAux.FromXml(AV125WebSession.Get("SdtCodigos"), "Collection");
            pr_default.dynParam(3, new Object[]{ new Object[]{
                                                 A456ContagemResultado_Codigo ,
                                                 AV132ContagemResultado_CodigoAux },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor P00YR5 */
            pr_default.execute(3);
            while ( (pr_default.getStatus(3) != 101) )
            {
               A601ContagemResultado_Servico = P00YR5_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00YR5_n601ContagemResultado_Servico[0];
               A2133ContagemResultado_QuantidadeSolicitada = P00YR5_A2133ContagemResultado_QuantidadeSolicitada[0];
               n2133ContagemResultado_QuantidadeSolicitada = P00YR5_n2133ContagemResultado_QuantidadeSolicitada[0];
               A2017ContagemResultado_DataEntregaReal = P00YR5_A2017ContagemResultado_DataEntregaReal[0];
               n2017ContagemResultado_DataEntregaReal = P00YR5_n2017ContagemResultado_DataEntregaReal[0];
               A1903ContagemResultado_DataPrvPgm = P00YR5_A1903ContagemResultado_DataPrvPgm[0];
               n1903ContagemResultado_DataPrvPgm = P00YR5_n1903ContagemResultado_DataPrvPgm[0];
               A1553ContagemResultado_CntSrvCod = P00YR5_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00YR5_n1553ContagemResultado_CntSrvCod[0];
               A1520ContagemResultado_InicioAnl = P00YR5_A1520ContagemResultado_InicioAnl[0];
               n1520ContagemResultado_InicioAnl = P00YR5_n1520ContagemResultado_InicioAnl[0];
               A1519ContagemResultado_TmpEstAnl = P00YR5_A1519ContagemResultado_TmpEstAnl[0];
               n1519ContagemResultado_TmpEstAnl = P00YR5_n1519ContagemResultado_TmpEstAnl[0];
               A1515ContagemResultado_Evento = P00YR5_A1515ContagemResultado_Evento[0];
               n1515ContagemResultado_Evento = P00YR5_n1515ContagemResultado_Evento[0];
               A1511ContagemResultado_InicioCrr = P00YR5_A1511ContagemResultado_InicioCrr[0];
               n1511ContagemResultado_InicioCrr = P00YR5_n1511ContagemResultado_InicioCrr[0];
               A1510ContagemResultado_FimExc = P00YR5_A1510ContagemResultado_FimExc[0];
               n1510ContagemResultado_FimExc = P00YR5_n1510ContagemResultado_FimExc[0];
               A1509ContagemResultado_InicioExc = P00YR5_A1509ContagemResultado_InicioExc[0];
               n1509ContagemResultado_InicioExc = P00YR5_n1509ContagemResultado_InicioExc[0];
               A1505ContagemResultado_TmpEstExc = P00YR5_A1505ContagemResultado_TmpEstExc[0];
               n1505ContagemResultado_TmpEstExc = P00YR5_n1505ContagemResultado_TmpEstExc[0];
               A1452ContagemResultado_SS = P00YR5_A1452ContagemResultado_SS[0];
               n1452ContagemResultado_SS = P00YR5_n1452ContagemResultado_SS[0];
               A1351ContagemResultado_DataPrevista = P00YR5_A1351ContagemResultado_DataPrevista[0];
               n1351ContagemResultado_DataPrevista = P00YR5_n1351ContagemResultado_DataPrevista[0];
               A1350ContagemResultado_DataCadastro = P00YR5_A1350ContagemResultado_DataCadastro[0];
               n1350ContagemResultado_DataCadastro = P00YR5_n1350ContagemResultado_DataCadastro[0];
               A1349ContagemResultado_DataExecucao = P00YR5_A1349ContagemResultado_DataExecucao[0];
               n1349ContagemResultado_DataExecucao = P00YR5_n1349ContagemResultado_DataExecucao[0];
               A1237ContagemResultado_PrazoMaisDias = P00YR5_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = P00YR5_n1237ContagemResultado_PrazoMaisDias[0];
               A1227ContagemResultado_PrazoInicialDias = P00YR5_A1227ContagemResultado_PrazoInicialDias[0];
               n1227ContagemResultado_PrazoInicialDias = P00YR5_n1227ContagemResultado_PrazoInicialDias[0];
               A1179ContagemResultado_PLFinal = P00YR5_A1179ContagemResultado_PLFinal[0];
               n1179ContagemResultado_PLFinal = P00YR5_n1179ContagemResultado_PLFinal[0];
               A1178ContagemResultado_PBFinal = P00YR5_A1178ContagemResultado_PBFinal[0];
               n1178ContagemResultado_PBFinal = P00YR5_n1178ContagemResultado_PBFinal[0];
               A1046ContagemResultado_Agrupador = P00YR5_A1046ContagemResultado_Agrupador[0];
               n1046ContagemResultado_Agrupador = P00YR5_n1046ContagemResultado_Agrupador[0];
               A912ContagemResultado_HoraEntrega = P00YR5_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P00YR5_n912ContagemResultado_HoraEntrega[0];
               A890ContagemResultado_Responsavel = P00YR5_A890ContagemResultado_Responsavel[0];
               n890ContagemResultado_Responsavel = P00YR5_n890ContagemResultado_Responsavel[0];
               A805ContagemResultado_ContratadaOrigemCod = P00YR5_A805ContagemResultado_ContratadaOrigemCod[0];
               n805ContagemResultado_ContratadaOrigemCod = P00YR5_n805ContagemResultado_ContratadaOrigemCod[0];
               A602ContagemResultado_OSVinculada = P00YR5_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = P00YR5_n602ContagemResultado_OSVinculada[0];
               A512ContagemResultado_ValorPF = P00YR5_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P00YR5_n512ContagemResultado_ValorPF[0];
               A508ContagemResultado_Owner = P00YR5_A508ContagemResultado_Owner[0];
               A146Modulo_Codigo = P00YR5_A146Modulo_Codigo[0];
               n146Modulo_Codigo = P00YR5_n146Modulo_Codigo[0];
               A489ContagemResultado_SistemaCod = P00YR5_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P00YR5_n489ContagemResultado_SistemaCod[0];
               A493ContagemResultado_DemandaFM = P00YR5_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P00YR5_n493ContagemResultado_DemandaFM[0];
               A490ContagemResultado_ContratadaCod = P00YR5_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00YR5_n490ContagemResultado_ContratadaCod[0];
               A484ContagemResultado_StatusDmn = P00YR5_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P00YR5_n484ContagemResultado_StatusDmn[0];
               A457ContagemResultado_Demanda = P00YR5_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P00YR5_n457ContagemResultado_Demanda[0];
               A472ContagemResultado_DataEntrega = P00YR5_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P00YR5_n472ContagemResultado_DataEntrega[0];
               A471ContagemResultado_DataDmn = P00YR5_A471ContagemResultado_DataDmn[0];
               A1855ContagemResultado_PFCnc = P00YR5_A1855ContagemResultado_PFCnc[0];
               n1855ContagemResultado_PFCnc = P00YR5_n1855ContagemResultado_PFCnc[0];
               A1854ContagemResultado_VlrCnc = P00YR5_A1854ContagemResultado_VlrCnc[0];
               n1854ContagemResultado_VlrCnc = P00YR5_n1854ContagemResultado_VlrCnc[0];
               A1791ContagemResultado_SemCusto = P00YR5_A1791ContagemResultado_SemCusto[0];
               n1791ContagemResultado_SemCusto = P00YR5_n1791ContagemResultado_SemCusto[0];
               A1790ContagemResultado_DataInicio = P00YR5_A1790ContagemResultado_DataInicio[0];
               n1790ContagemResultado_DataInicio = P00YR5_n1790ContagemResultado_DataInicio[0];
               A1762ContagemResultado_Entrega = P00YR5_A1762ContagemResultado_Entrega[0];
               n1762ContagemResultado_Entrega = P00YR5_n1762ContagemResultado_Entrega[0];
               A1714ContagemResultado_Combinada = P00YR5_A1714ContagemResultado_Combinada[0];
               n1714ContagemResultado_Combinada = P00YR5_n1714ContagemResultado_Combinada[0];
               A1636ContagemResultado_ServicoSS = P00YR5_A1636ContagemResultado_ServicoSS[0];
               n1636ContagemResultado_ServicoSS = P00YR5_n1636ContagemResultado_ServicoSS[0];
               A1587ContagemResultado_PrioridadePrevista = P00YR5_A1587ContagemResultado_PrioridadePrevista[0];
               n1587ContagemResultado_PrioridadePrevista = P00YR5_n1587ContagemResultado_PrioridadePrevista[0];
               A1586ContagemResultado_Restricoes = P00YR5_A1586ContagemResultado_Restricoes[0];
               n1586ContagemResultado_Restricoes = P00YR5_n1586ContagemResultado_Restricoes[0];
               A1585ContagemResultado_Referencia = P00YR5_A1585ContagemResultado_Referencia[0];
               n1585ContagemResultado_Referencia = P00YR5_n1585ContagemResultado_Referencia[0];
               A1584ContagemResultado_UOOwner = P00YR5_A1584ContagemResultado_UOOwner[0];
               n1584ContagemResultado_UOOwner = P00YR5_n1584ContagemResultado_UOOwner[0];
               A1583ContagemResultado_TipoRegistro = P00YR5_A1583ContagemResultado_TipoRegistro[0];
               A1559ContagemResultado_VlrAceite = P00YR5_A1559ContagemResultado_VlrAceite[0];
               n1559ContagemResultado_VlrAceite = P00YR5_n1559ContagemResultado_VlrAceite[0];
               A1544ContagemResultado_ProjetoCod = P00YR5_A1544ContagemResultado_ProjetoCod[0];
               n1544ContagemResultado_ProjetoCod = P00YR5_n1544ContagemResultado_ProjetoCod[0];
               A1521ContagemResultado_FimAnl = P00YR5_A1521ContagemResultado_FimAnl[0];
               n1521ContagemResultado_FimAnl = P00YR5_n1521ContagemResultado_FimAnl[0];
               A1512ContagemResultado_FimCrr = P00YR5_A1512ContagemResultado_FimCrr[0];
               n1512ContagemResultado_FimCrr = P00YR5_n1512ContagemResultado_FimCrr[0];
               A1506ContagemResultado_TmpEstCrr = P00YR5_A1506ContagemResultado_TmpEstCrr[0];
               n1506ContagemResultado_TmpEstCrr = P00YR5_n1506ContagemResultado_TmpEstCrr[0];
               A1457ContagemResultado_TemDpnHmlg = P00YR5_A1457ContagemResultado_TemDpnHmlg[0];
               n1457ContagemResultado_TemDpnHmlg = P00YR5_n1457ContagemResultado_TemDpnHmlg[0];
               A1445ContagemResultado_CntSrvPrrCst = P00YR5_A1445ContagemResultado_CntSrvPrrCst[0];
               n1445ContagemResultado_CntSrvPrrCst = P00YR5_n1445ContagemResultado_CntSrvPrrCst[0];
               A1444ContagemResultado_CntSrvPrrPrz = P00YR5_A1444ContagemResultado_CntSrvPrrPrz[0];
               n1444ContagemResultado_CntSrvPrrPrz = P00YR5_n1444ContagemResultado_CntSrvPrrPrz[0];
               A1443ContagemResultado_CntSrvPrrCod = P00YR5_A1443ContagemResultado_CntSrvPrrCod[0];
               n1443ContagemResultado_CntSrvPrrCod = P00YR5_n1443ContagemResultado_CntSrvPrrCod[0];
               A1392ContagemResultado_RdmnUpdated = P00YR5_A1392ContagemResultado_RdmnUpdated[0];
               n1392ContagemResultado_RdmnUpdated = P00YR5_n1392ContagemResultado_RdmnUpdated[0];
               A1390ContagemResultado_RdmnProjectId = P00YR5_A1390ContagemResultado_RdmnProjectId[0];
               n1390ContagemResultado_RdmnProjectId = P00YR5_n1390ContagemResultado_RdmnProjectId[0];
               A1389ContagemResultado_RdmnIssueId = P00YR5_A1389ContagemResultado_RdmnIssueId[0];
               n1389ContagemResultado_RdmnIssueId = P00YR5_n1389ContagemResultado_RdmnIssueId[0];
               A1348ContagemResultado_DataHomologacao = P00YR5_A1348ContagemResultado_DataHomologacao[0];
               n1348ContagemResultado_DataHomologacao = P00YR5_n1348ContagemResultado_DataHomologacao[0];
               A1180ContagemResultado_Custo = P00YR5_A1180ContagemResultado_Custo[0];
               n1180ContagemResultado_Custo = P00YR5_n1180ContagemResultado_Custo[0];
               A1173ContagemResultado_OSManual = P00YR5_A1173ContagemResultado_OSManual[0];
               n1173ContagemResultado_OSManual = P00YR5_n1173ContagemResultado_OSManual[0];
               A1052ContagemResultado_GlsUser = P00YR5_A1052ContagemResultado_GlsUser[0];
               n1052ContagemResultado_GlsUser = P00YR5_n1052ContagemResultado_GlsUser[0];
               A1051ContagemResultado_GlsValor = P00YR5_A1051ContagemResultado_GlsValor[0];
               n1051ContagemResultado_GlsValor = P00YR5_n1051ContagemResultado_GlsValor[0];
               A1050ContagemResultado_GlsDescricao = P00YR5_A1050ContagemResultado_GlsDescricao[0];
               n1050ContagemResultado_GlsDescricao = P00YR5_n1050ContagemResultado_GlsDescricao[0];
               A1049ContagemResultado_GlsData = P00YR5_A1049ContagemResultado_GlsData[0];
               n1049ContagemResultado_GlsData = P00YR5_n1049ContagemResultado_GlsData[0];
               A1044ContagemResultado_FncUsrCod = P00YR5_A1044ContagemResultado_FncUsrCod[0];
               n1044ContagemResultado_FncUsrCod = P00YR5_n1044ContagemResultado_FncUsrCod[0];
               A1043ContagemResultado_LiqLogCod = P00YR5_A1043ContagemResultado_LiqLogCod[0];
               n1043ContagemResultado_LiqLogCod = P00YR5_n1043ContagemResultado_LiqLogCod[0];
               A799ContagemResultado_PFLFSImp = P00YR5_A799ContagemResultado_PFLFSImp[0];
               n799ContagemResultado_PFLFSImp = P00YR5_n799ContagemResultado_PFLFSImp[0];
               A798ContagemResultado_PFBFSImp = P00YR5_A798ContagemResultado_PFBFSImp[0];
               n798ContagemResultado_PFBFSImp = P00YR5_n798ContagemResultado_PFBFSImp[0];
               A598ContagemResultado_Baseline = P00YR5_A598ContagemResultado_Baseline[0];
               n598ContagemResultado_Baseline = P00YR5_n598ContagemResultado_Baseline[0];
               A597ContagemResultado_LoteAceiteCod = P00YR5_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P00YR5_n597ContagemResultado_LoteAceiteCod[0];
               A592ContagemResultado_Evidencia = P00YR5_A592ContagemResultado_Evidencia[0];
               n592ContagemResultado_Evidencia = P00YR5_n592ContagemResultado_Evidencia[0];
               A514ContagemResultado_Observacao = P00YR5_A514ContagemResultado_Observacao[0];
               n514ContagemResultado_Observacao = P00YR5_n514ContagemResultado_Observacao[0];
               A494ContagemResultado_Descricao = P00YR5_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = P00YR5_n494ContagemResultado_Descricao[0];
               A485ContagemResultado_EhValidacao = P00YR5_A485ContagemResultado_EhValidacao[0];
               n485ContagemResultado_EhValidacao = P00YR5_n485ContagemResultado_EhValidacao[0];
               A468ContagemResultado_NaoCnfDmnCod = P00YR5_A468ContagemResultado_NaoCnfDmnCod[0];
               n468ContagemResultado_NaoCnfDmnCod = P00YR5_n468ContagemResultado_NaoCnfDmnCod[0];
               A465ContagemResultado_Link = P00YR5_A465ContagemResultado_Link[0];
               n465ContagemResultado_Link = P00YR5_n465ContagemResultado_Link[0];
               A454ContagemResultado_ContadorFSCod = P00YR5_A454ContagemResultado_ContadorFSCod[0];
               n454ContagemResultado_ContadorFSCod = P00YR5_n454ContagemResultado_ContadorFSCod[0];
               A456ContagemResultado_Codigo = P00YR5_A456ContagemResultado_Codigo[0];
               A801ContagemResultado_ServicoSigla = P00YR5_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = P00YR5_n801ContagemResultado_ServicoSigla[0];
               A601ContagemResultado_Servico = P00YR5_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00YR5_n601ContagemResultado_Servico[0];
               A801ContagemResultado_ServicoSigla = P00YR5_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = P00YR5_n801ContagemResultado_ServicoSigla[0];
               AV41ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               AV175ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
               AV146ContagemResultado_Demanda = A457ContagemResultado_Demanda;
               AV147ContagemResultado_Responsavel = A890ContagemResultado_Responsavel;
               AV128ContagemResultado_Agrupador = A1046ContagemResultado_Agrupador;
               AV148ContagemResultado_SistemaCod = A489ContagemResultado_SistemaCod;
               AV149Modulo_Codigo = A146Modulo_Codigo;
               AV158ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
               AV45ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
               /* Execute user subroutine: 'VERIFICA.EXITE.DEMANDA' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(3);
                  this.cleanup();
                  if (true) return;
               }
               if ( ! AV177IsDemandaCadastrada )
               {
                  /*
                     INSERT RECORD ON TABLE ContagemResultado

                  */
                  W490ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
                  n490ContagemResultado_ContratadaCod = false;
                  W1553ContagemResultado_CntSrvCod = A1553ContagemResultado_CntSrvCod;
                  n1553ContagemResultado_CntSrvCod = false;
                  W457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
                  n457ContagemResultado_Demanda = false;
                  W493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
                  n493ContagemResultado_DemandaFM = false;
                  W472ContagemResultado_DataEntrega = A472ContagemResultado_DataEntrega;
                  n472ContagemResultado_DataEntrega = false;
                  W1351ContagemResultado_DataPrevista = A1351ContagemResultado_DataPrevista;
                  n1351ContagemResultado_DataPrevista = false;
                  W912ContagemResultado_HoraEntrega = A912ContagemResultado_HoraEntrega;
                  n912ContagemResultado_HoraEntrega = false;
                  W1903ContagemResultado_DataPrvPgm = A1903ContagemResultado_DataPrvPgm;
                  n1903ContagemResultado_DataPrvPgm = false;
                  W484ContagemResultado_StatusDmn = A484ContagemResultado_StatusDmn;
                  n484ContagemResultado_StatusDmn = false;
                  W512ContagemResultado_ValorPF = A512ContagemResultado_ValorPF;
                  n512ContagemResultado_ValorPF = false;
                  W602ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
                  n602ContagemResultado_OSVinculada = false;
                  W1178ContagemResultado_PBFinal = A1178ContagemResultado_PBFinal;
                  n1178ContagemResultado_PBFinal = false;
                  W1179ContagemResultado_PLFinal = A1179ContagemResultado_PLFinal;
                  n1179ContagemResultado_PLFinal = false;
                  W1452ContagemResultado_SS = A1452ContagemResultado_SS;
                  n1452ContagemResultado_SS = false;
                  W2017ContagemResultado_DataEntregaReal = A2017ContagemResultado_DataEntregaReal;
                  n2017ContagemResultado_DataEntregaReal = false;
                  W1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
                  n1227ContagemResultado_PrazoInicialDias = false;
                  W1237ContagemResultado_PrazoMaisDias = A1237ContagemResultado_PrazoMaisDias;
                  n1237ContagemResultado_PrazoMaisDias = false;
                  W1505ContagemResultado_TmpEstExc = A1505ContagemResultado_TmpEstExc;
                  n1505ContagemResultado_TmpEstExc = false;
                  W1509ContagemResultado_InicioExc = A1509ContagemResultado_InicioExc;
                  n1509ContagemResultado_InicioExc = false;
                  W1511ContagemResultado_InicioCrr = A1511ContagemResultado_InicioCrr;
                  n1511ContagemResultado_InicioCrr = false;
                  W1510ContagemResultado_FimExc = A1510ContagemResultado_FimExc;
                  n1510ContagemResultado_FimExc = false;
                  W1519ContagemResultado_TmpEstAnl = A1519ContagemResultado_TmpEstAnl;
                  n1519ContagemResultado_TmpEstAnl = false;
                  W1520ContagemResultado_InicioAnl = A1520ContagemResultado_InicioAnl;
                  n1520ContagemResultado_InicioAnl = false;
                  W2133ContagemResultado_QuantidadeSolicitada = A2133ContagemResultado_QuantidadeSolicitada;
                  n2133ContagemResultado_QuantidadeSolicitada = false;
                  W1350ContagemResultado_DataCadastro = A1350ContagemResultado_DataCadastro;
                  n1350ContagemResultado_DataCadastro = false;
                  W471ContagemResultado_DataDmn = A471ContagemResultado_DataDmn;
                  W1349ContagemResultado_DataExecucao = A1349ContagemResultado_DataExecucao;
                  n1349ContagemResultado_DataExecucao = false;
                  W1515ContagemResultado_Evento = A1515ContagemResultado_Evento;
                  n1515ContagemResultado_Evento = false;
                  W805ContagemResultado_ContratadaOrigemCod = A805ContagemResultado_ContratadaOrigemCod;
                  n805ContagemResultado_ContratadaOrigemCod = false;
                  W1046ContagemResultado_Agrupador = A1046ContagemResultado_Agrupador;
                  n1046ContagemResultado_Agrupador = false;
                  W1046ContagemResultado_Agrupador = A1046ContagemResultado_Agrupador;
                  n1046ContagemResultado_Agrupador = false;
                  W890ContagemResultado_Responsavel = A890ContagemResultado_Responsavel;
                  n890ContagemResultado_Responsavel = false;
                  W890ContagemResultado_Responsavel = A890ContagemResultado_Responsavel;
                  n890ContagemResultado_Responsavel = false;
                  W508ContagemResultado_Owner = A508ContagemResultado_Owner;
                  W489ContagemResultado_SistemaCod = A489ContagemResultado_SistemaCod;
                  n489ContagemResultado_SistemaCod = false;
                  W489ContagemResultado_SistemaCod = A489ContagemResultado_SistemaCod;
                  n489ContagemResultado_SistemaCod = false;
                  W146Modulo_Codigo = A146Modulo_Codigo;
                  n146Modulo_Codigo = false;
                  W146Modulo_Codigo = A146Modulo_Codigo;
                  n146Modulo_Codigo = false;
                  A490ContagemResultado_ContratadaCod = AV54Contratada_Codigo;
                  n490ContagemResultado_ContratadaCod = false;
                  A1553ContagemResultado_CntSrvCod = AV57ContratoServicos_Codigo;
                  n1553ContagemResultado_CntSrvCod = false;
                  A457ContagemResultado_Demanda = AV146ContagemResultado_Demanda;
                  n457ContagemResultado_Demanda = false;
                  A493ContagemResultado_DemandaFM = AV175ContagemResultado_DemandaFM;
                  n493ContagemResultado_DemandaFM = false;
                  A472ContagemResultado_DataEntrega = AV189PrazoDemanda;
                  n472ContagemResultado_DataEntrega = false;
                  A1351ContagemResultado_DataPrevista = DateTimeUtil.ResetTime( AV189PrazoDemanda ) ;
                  n1351ContagemResultado_DataPrevista = false;
                  A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV189PrazoDemanda)), (short)(DateTimeUtil.Month( AV189PrazoDemanda)), (short)(DateTimeUtil.Day( AV189PrazoDemanda)), 23, 59, 0));
                  n912ContagemResultado_HoraEntrega = false;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV189PrazoDemanda ) ;
                  A1903ContagemResultado_DataPrvPgm = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime1, 86400*(30)));
                  n1903ContagemResultado_DataPrvPgm = false;
                  A484ContagemResultado_StatusDmn = "S";
                  n484ContagemResultado_StatusDmn = false;
                  A512ContagemResultado_ValorPF = AV53ContagemResultado_ValorPF;
                  n512ContagemResultado_ValorPF = false;
                  A602ContagemResultado_OSVinculada = AV41ContagemResultado_Codigo;
                  n602ContagemResultado_OSVinculada = false;
                  A1178ContagemResultado_PBFinal = 0;
                  n1178ContagemResultado_PBFinal = false;
                  A1179ContagemResultado_PLFinal = 0;
                  n1179ContagemResultado_PLFinal = false;
                  A1452ContagemResultado_SS = 0;
                  n1452ContagemResultado_SS = false;
                  A2017ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
                  n2017ContagemResultado_DataEntregaReal = false;
                  n2017ContagemResultado_DataEntregaReal = true;
                  GXt_int2 = A1227ContagemResultado_PrazoInicialDias;
                  new prc_diasuteisentre(context ).execute(  AV170ServerDate,  AV189PrazoDemanda,  AV199TipoDias, out  GXt_int2) ;
                  A1227ContagemResultado_PrazoInicialDias = GXt_int2;
                  n1227ContagemResultado_PrazoInicialDias = false;
                  A1237ContagemResultado_PrazoMaisDias = 0;
                  n1237ContagemResultado_PrazoMaisDias = false;
                  A1505ContagemResultado_TmpEstExc = 0;
                  n1505ContagemResultado_TmpEstExc = false;
                  A1509ContagemResultado_InicioExc = (DateTime)(DateTime.MinValue);
                  n1509ContagemResultado_InicioExc = false;
                  n1509ContagemResultado_InicioExc = true;
                  A1511ContagemResultado_InicioCrr = (DateTime)(DateTime.MinValue);
                  n1511ContagemResultado_InicioCrr = false;
                  n1511ContagemResultado_InicioCrr = true;
                  A1510ContagemResultado_FimExc = (DateTime)(DateTime.MinValue);
                  n1510ContagemResultado_FimExc = false;
                  n1510ContagemResultado_FimExc = true;
                  A1519ContagemResultado_TmpEstAnl = 1;
                  n1519ContagemResultado_TmpEstAnl = false;
                  A1520ContagemResultado_InicioAnl = (DateTime)(DateTime.MinValue);
                  n1520ContagemResultado_InicioAnl = false;
                  n1520ContagemResultado_InicioAnl = true;
                  A2133ContagemResultado_QuantidadeSolicitada = 0;
                  n2133ContagemResultado_QuantidadeSolicitada = false;
                  A1350ContagemResultado_DataCadastro = AV110ServerNow;
                  n1350ContagemResultado_DataCadastro = false;
                  A471ContagemResultado_DataDmn = AV170ServerDate;
                  A1349ContagemResultado_DataExecucao = (DateTime)(DateTime.MinValue);
                  n1349ContagemResultado_DataExecucao = false;
                  n1349ContagemResultado_DataExecucao = true;
                  A1515ContagemResultado_Evento = 1;
                  n1515ContagemResultado_Evento = false;
                  A805ContagemResultado_ContratadaOrigemCod = AV45ContagemResultado_ContratadaCod;
                  n805ContagemResultado_ContratadaOrigemCod = false;
                  if ( String.IsNullOrEmpty(StringUtil.RTrim( AV187ContagemResultado_AgrupadorAux)) )
                  {
                     A1046ContagemResultado_Agrupador = AV128ContagemResultado_Agrupador;
                     n1046ContagemResultado_Agrupador = false;
                  }
                  else
                  {
                     A1046ContagemResultado_Agrupador = AV187ContagemResultado_AgrupadorAux;
                     n1046ContagemResultado_Agrupador = false;
                  }
                  /* Execute user subroutine: 'DADOS.PREPOSTO' */
                  S131 ();
                  if ( returnInSub )
                  {
                     pr_default.close(3);
                     this.cleanup();
                     if (true) return;
                  }
                  if ( (0==AV155Contrato_PrepostoCod) )
                  {
                     A890ContagemResultado_Responsavel = 0;
                     n890ContagemResultado_Responsavel = false;
                     n890ContagemResultado_Responsavel = true;
                  }
                  else
                  {
                     A890ContagemResultado_Responsavel = AV155Contrato_PrepostoCod;
                     n890ContagemResultado_Responsavel = false;
                  }
                  A508ContagemResultado_Owner = AV126WWPContext.gxTpr_Userid;
                  if ( (0==A489ContagemResultado_SistemaCod) )
                  {
                     A489ContagemResultado_SistemaCod = 0;
                     n489ContagemResultado_SistemaCod = false;
                     n489ContagemResultado_SistemaCod = true;
                  }
                  else
                  {
                     A489ContagemResultado_SistemaCod = AV148ContagemResultado_SistemaCod;
                     n489ContagemResultado_SistemaCod = false;
                  }
                  if ( (0==A146Modulo_Codigo) )
                  {
                     A146Modulo_Codigo = 0;
                     n146Modulo_Codigo = false;
                     n146Modulo_Codigo = true;
                  }
                  else
                  {
                     A146Modulo_Codigo = AV149Modulo_Codigo;
                     n146Modulo_Codigo = false;
                  }
                  AV156TotalDemandas = (short)(+1);
                  /* Using cursor P00YR6 */
                  pr_default.execute(4, new Object[] {A471ContagemResultado_DataDmn, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod, n457ContagemResultado_Demanda, A457ContagemResultado_Demanda, n465ContagemResultado_Link, A465ContagemResultado_Link, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n485ContagemResultado_EhValidacao, A485ContagemResultado_EhValidacao, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n493ContagemResultado_DemandaFM, A493ContagemResultado_DemandaFM, n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod, n146Modulo_Codigo, A146Modulo_Codigo, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, A508ContagemResultado_Owner, n512ContagemResultado_ValorPF, A512ContagemResultado_ValorPF, n592ContagemResultado_Evidencia, A592ContagemResultado_Evidencia, n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod, n598ContagemResultado_Baseline, A598ContagemResultado_Baseline, n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada, n798ContagemResultado_PFBFSImp, A798ContagemResultado_PFBFSImp, n799ContagemResultado_PFLFSImp, A799ContagemResultado_PFLFSImp, n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, n1044ContagemResultado_FncUsrCod, A1044ContagemResultado_FncUsrCod, n1046ContagemResultado_Agrupador, A1046ContagemResultado_Agrupador, n1049ContagemResultado_GlsData, A1049ContagemResultado_GlsData, n1050ContagemResultado_GlsDescricao, A1050ContagemResultado_GlsDescricao, n1051ContagemResultado_GlsValor, A1051ContagemResultado_GlsValor, n1052ContagemResultado_GlsUser, A1052ContagemResultado_GlsUser, n1173ContagemResultado_OSManual, A1173ContagemResultado_OSManual, n1178ContagemResultado_PBFinal, A1178ContagemResultado_PBFinal, n1179ContagemResultado_PLFinal, A1179ContagemResultado_PLFinal, n1180ContagemResultado_Custo, A1180ContagemResultado_Custo, n1227ContagemResultado_PrazoInicialDias, A1227ContagemResultado_PrazoInicialDias, n1237ContagemResultado_PrazoMaisDias, A1237ContagemResultado_PrazoMaisDias, n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, n1349ContagemResultado_DataExecucao, A1349ContagemResultado_DataExecucao, n1350ContagemResultado_DataCadastro, A1350ContagemResultado_DataCadastro, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1389ContagemResultado_RdmnIssueId, A1389ContagemResultado_RdmnIssueId, n1390ContagemResultado_RdmnProjectId, A1390ContagemResultado_RdmnProjectId, n1392ContagemResultado_RdmnUpdated, A1392ContagemResultado_RdmnUpdated, n1443ContagemResultado_CntSrvPrrCod, A1443ContagemResultado_CntSrvPrrCod, n1444ContagemResultado_CntSrvPrrPrz, A1444ContagemResultado_CntSrvPrrPrz, n1445ContagemResultado_CntSrvPrrCst, A1445ContagemResultado_CntSrvPrrCst, n1452ContagemResultado_SS, A1452ContagemResultado_SS, n1457ContagemResultado_TemDpnHmlg, A1457ContagemResultado_TemDpnHmlg, n1505ContagemResultado_TmpEstExc, A1505ContagemResultado_TmpEstExc, n1506ContagemResultado_TmpEstCrr, A1506ContagemResultado_TmpEstCrr, n1509ContagemResultado_InicioExc, A1509ContagemResultado_InicioExc, n1510ContagemResultado_FimExc, A1510ContagemResultado_FimExc, n1511ContagemResultado_InicioCrr, A1511ContagemResultado_InicioCrr, n1512ContagemResultado_FimCrr, A1512ContagemResultado_FimCrr, n1515ContagemResultado_Evento, A1515ContagemResultado_Evento, n1519ContagemResultado_TmpEstAnl, A1519ContagemResultado_TmpEstAnl, n1520ContagemResultado_InicioAnl, A1520ContagemResultado_InicioAnl, n1521ContagemResultado_FimAnl, A1521ContagemResultado_FimAnl, n1544ContagemResultado_ProjetoCod, A1544ContagemResultado_ProjetoCod, n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod,
                  n1559ContagemResultado_VlrAceite, A1559ContagemResultado_VlrAceite, A1583ContagemResultado_TipoRegistro, n1584ContagemResultado_UOOwner, A1584ContagemResultado_UOOwner, n1585ContagemResultado_Referencia, A1585ContagemResultado_Referencia, n1586ContagemResultado_Restricoes, A1586ContagemResultado_Restricoes, n1587ContagemResultado_PrioridadePrevista, A1587ContagemResultado_PrioridadePrevista, n1636ContagemResultado_ServicoSS, A1636ContagemResultado_ServicoSS, n1714ContagemResultado_Combinada, A1714ContagemResultado_Combinada, n1762ContagemResultado_Entrega, A1762ContagemResultado_Entrega, n1790ContagemResultado_DataInicio, A1790ContagemResultado_DataInicio, n1791ContagemResultado_SemCusto, A1791ContagemResultado_SemCusto, n1854ContagemResultado_VlrCnc, A1854ContagemResultado_VlrCnc, n1855ContagemResultado_PFCnc, A1855ContagemResultado_PFCnc, n1903ContagemResultado_DataPrvPgm, A1903ContagemResultado_DataPrvPgm, n2017ContagemResultado_DataEntregaReal, A2017ContagemResultado_DataEntregaReal, n2133ContagemResultado_QuantidadeSolicitada, A2133ContagemResultado_QuantidadeSolicitada});
                  A456ContagemResultado_Codigo = P00YR6_A456ContagemResultado_Codigo[0];
                  pr_default.close(4);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                  if ( (pr_default.getStatus(4) == 1) )
                  {
                     context.Gx_err = 1;
                     Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
                  }
                  else
                  {
                     context.Gx_err = 0;
                     Gx_emsg = "";
                  }
                  A490ContagemResultado_ContratadaCod = W490ContagemResultado_ContratadaCod;
                  n490ContagemResultado_ContratadaCod = false;
                  A1553ContagemResultado_CntSrvCod = W1553ContagemResultado_CntSrvCod;
                  n1553ContagemResultado_CntSrvCod = false;
                  A457ContagemResultado_Demanda = W457ContagemResultado_Demanda;
                  n457ContagemResultado_Demanda = false;
                  A493ContagemResultado_DemandaFM = W493ContagemResultado_DemandaFM;
                  n493ContagemResultado_DemandaFM = false;
                  A472ContagemResultado_DataEntrega = W472ContagemResultado_DataEntrega;
                  n472ContagemResultado_DataEntrega = false;
                  A1351ContagemResultado_DataPrevista = W1351ContagemResultado_DataPrevista;
                  n1351ContagemResultado_DataPrevista = false;
                  A912ContagemResultado_HoraEntrega = W912ContagemResultado_HoraEntrega;
                  n912ContagemResultado_HoraEntrega = false;
                  A1903ContagemResultado_DataPrvPgm = W1903ContagemResultado_DataPrvPgm;
                  n1903ContagemResultado_DataPrvPgm = false;
                  A484ContagemResultado_StatusDmn = W484ContagemResultado_StatusDmn;
                  n484ContagemResultado_StatusDmn = false;
                  A512ContagemResultado_ValorPF = W512ContagemResultado_ValorPF;
                  n512ContagemResultado_ValorPF = false;
                  A602ContagemResultado_OSVinculada = W602ContagemResultado_OSVinculada;
                  n602ContagemResultado_OSVinculada = false;
                  A1178ContagemResultado_PBFinal = W1178ContagemResultado_PBFinal;
                  n1178ContagemResultado_PBFinal = false;
                  A1179ContagemResultado_PLFinal = W1179ContagemResultado_PLFinal;
                  n1179ContagemResultado_PLFinal = false;
                  A1452ContagemResultado_SS = W1452ContagemResultado_SS;
                  n1452ContagemResultado_SS = false;
                  A2017ContagemResultado_DataEntregaReal = W2017ContagemResultado_DataEntregaReal;
                  n2017ContagemResultado_DataEntregaReal = false;
                  A1227ContagemResultado_PrazoInicialDias = W1227ContagemResultado_PrazoInicialDias;
                  n1227ContagemResultado_PrazoInicialDias = false;
                  A1237ContagemResultado_PrazoMaisDias = W1237ContagemResultado_PrazoMaisDias;
                  n1237ContagemResultado_PrazoMaisDias = false;
                  A1505ContagemResultado_TmpEstExc = W1505ContagemResultado_TmpEstExc;
                  n1505ContagemResultado_TmpEstExc = false;
                  A1509ContagemResultado_InicioExc = W1509ContagemResultado_InicioExc;
                  n1509ContagemResultado_InicioExc = false;
                  A1511ContagemResultado_InicioCrr = W1511ContagemResultado_InicioCrr;
                  n1511ContagemResultado_InicioCrr = false;
                  A1510ContagemResultado_FimExc = W1510ContagemResultado_FimExc;
                  n1510ContagemResultado_FimExc = false;
                  A1519ContagemResultado_TmpEstAnl = W1519ContagemResultado_TmpEstAnl;
                  n1519ContagemResultado_TmpEstAnl = false;
                  A1520ContagemResultado_InicioAnl = W1520ContagemResultado_InicioAnl;
                  n1520ContagemResultado_InicioAnl = false;
                  A2133ContagemResultado_QuantidadeSolicitada = W2133ContagemResultado_QuantidadeSolicitada;
                  n2133ContagemResultado_QuantidadeSolicitada = false;
                  A1350ContagemResultado_DataCadastro = W1350ContagemResultado_DataCadastro;
                  n1350ContagemResultado_DataCadastro = false;
                  A471ContagemResultado_DataDmn = W471ContagemResultado_DataDmn;
                  A1349ContagemResultado_DataExecucao = W1349ContagemResultado_DataExecucao;
                  n1349ContagemResultado_DataExecucao = false;
                  A1515ContagemResultado_Evento = W1515ContagemResultado_Evento;
                  n1515ContagemResultado_Evento = false;
                  A805ContagemResultado_ContratadaOrigemCod = W805ContagemResultado_ContratadaOrigemCod;
                  n805ContagemResultado_ContratadaOrigemCod = false;
                  A1046ContagemResultado_Agrupador = W1046ContagemResultado_Agrupador;
                  n1046ContagemResultado_Agrupador = false;
                  A1046ContagemResultado_Agrupador = W1046ContagemResultado_Agrupador;
                  n1046ContagemResultado_Agrupador = false;
                  A890ContagemResultado_Responsavel = W890ContagemResultado_Responsavel;
                  n890ContagemResultado_Responsavel = false;
                  A890ContagemResultado_Responsavel = W890ContagemResultado_Responsavel;
                  n890ContagemResultado_Responsavel = false;
                  A508ContagemResultado_Owner = W508ContagemResultado_Owner;
                  A489ContagemResultado_SistemaCod = W489ContagemResultado_SistemaCod;
                  n489ContagemResultado_SistemaCod = false;
                  A489ContagemResultado_SistemaCod = W489ContagemResultado_SistemaCod;
                  n489ContagemResultado_SistemaCod = false;
                  A146Modulo_Codigo = W146Modulo_Codigo;
                  n146Modulo_Codigo = false;
                  A146Modulo_Codigo = W146Modulo_Codigo;
                  n146Modulo_Codigo = false;
                  /* End Insert */
                  AV144NewCodigo = A456ContagemResultado_Codigo;
                  AV159Codigos.Add(AV144NewCodigo, 0);
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV189PrazoDemanda ) ;
                  new prc_inslogresponsavel(context ).execute( ref  AV144NewCodigo,  AV155Contrato_PrepostoCod,  "S",  "D",  AV126WWPContext.gxTpr_Userid,  0,  "",  "S",  "",  GXt_dtime1,  false) ;
                  /* Execute user subroutine: 'ATUALIZA.DADOS.DEMANDA.PRINCIPAL' */
                  S171 ();
                  if ( returnInSub )
                  {
                     pr_default.close(3);
                     this.cleanup();
                     if (true) return;
                  }
                  context.CommitDataStores( "PRC_DemandasVinculadas");
               }
               else
               {
                  AV178CodigosNaoGeradas.Add(A456ContagemResultado_Codigo, 0);
               }
               pr_default.readNext(3);
            }
            pr_default.close(3);
            if ( ! (0==AV159Codigos.Count) )
            {
               AV156TotalDemandas = 0;
               AV183IdentificacaoListaDemandas = "Demanda(s) criada(s)";
               HYR0( false, 25) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV183IdentificacaoListaDemandas, "")), 0, Gx_line+0, 750, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+25);
               HYR0( false, 18) ;
               getPrinter().GxDrawLine(0, Gx_line+17, 825, Gx_line+17, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Contrato", 742, Gx_line+0, 825, Gx_line+18, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Data", 0, Gx_line+0, 58, Gx_line+18, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("N�| N�Ref.", 67, Gx_line+0, 192, Gx_line+18, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Descri��o", 192, Gx_line+0, 447, Gx_line+18, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Status", 450, Gx_line+0, 542, Gx_line+18, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Servi�o", 550, Gx_line+0, 739, Gx_line+18, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+18);
               AV179ContagemResultado_DataDmn = DateTime.MinValue;
               AV180ContagemResultado_OsFsOsFm = "";
               AV181ContagemResultado_Descricao = "";
               AV158ContagemResultado_ServicoSigla = "";
               AV182ContagemResultado_CntNum = "";
               AV156TotalDemandas = 0;
               pr_default.dynParam(5, new Object[]{ new Object[]{
                                                    A456ContagemResultado_Codigo ,
                                                    AV159Codigos },
                                                    new int[] {
                                                    TypeConstants.INT
                                                    }
               });
               /* Using cursor P00YR7 */
               pr_default.execute(5);
               while ( (pr_default.getStatus(5) != 101) )
               {
                  A1553ContagemResultado_CntSrvCod = P00YR7_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00YR7_n1553ContagemResultado_CntSrvCod[0];
                  A1603ContagemResultado_CntCod = P00YR7_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P00YR7_n1603ContagemResultado_CntCod[0];
                  A601ContagemResultado_Servico = P00YR7_A601ContagemResultado_Servico[0];
                  n601ContagemResultado_Servico = P00YR7_n601ContagemResultado_Servico[0];
                  A456ContagemResultado_Codigo = P00YR7_A456ContagemResultado_Codigo[0];
                  A471ContagemResultado_DataDmn = P00YR7_A471ContagemResultado_DataDmn[0];
                  A494ContagemResultado_Descricao = P00YR7_A494ContagemResultado_Descricao[0];
                  n494ContagemResultado_Descricao = P00YR7_n494ContagemResultado_Descricao[0];
                  A484ContagemResultado_StatusDmn = P00YR7_A484ContagemResultado_StatusDmn[0];
                  n484ContagemResultado_StatusDmn = P00YR7_n484ContagemResultado_StatusDmn[0];
                  A801ContagemResultado_ServicoSigla = P00YR7_A801ContagemResultado_ServicoSigla[0];
                  n801ContagemResultado_ServicoSigla = P00YR7_n801ContagemResultado_ServicoSigla[0];
                  A1612ContagemResultado_CntNum = P00YR7_A1612ContagemResultado_CntNum[0];
                  n1612ContagemResultado_CntNum = P00YR7_n1612ContagemResultado_CntNum[0];
                  A493ContagemResultado_DemandaFM = P00YR7_A493ContagemResultado_DemandaFM[0];
                  n493ContagemResultado_DemandaFM = P00YR7_n493ContagemResultado_DemandaFM[0];
                  A457ContagemResultado_Demanda = P00YR7_A457ContagemResultado_Demanda[0];
                  n457ContagemResultado_Demanda = P00YR7_n457ContagemResultado_Demanda[0];
                  A1603ContagemResultado_CntCod = P00YR7_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P00YR7_n1603ContagemResultado_CntCod[0];
                  A601ContagemResultado_Servico = P00YR7_A601ContagemResultado_Servico[0];
                  n601ContagemResultado_Servico = P00YR7_n601ContagemResultado_Servico[0];
                  A1612ContagemResultado_CntNum = P00YR7_A1612ContagemResultado_CntNum[0];
                  n1612ContagemResultado_CntNum = P00YR7_n1612ContagemResultado_CntNum[0];
                  A801ContagemResultado_ServicoSigla = P00YR7_A801ContagemResultado_ServicoSigla[0];
                  n801ContagemResultado_ServicoSigla = P00YR7_n801ContagemResultado_ServicoSigla[0];
                  A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
                  AV179ContagemResultado_DataDmn = A471ContagemResultado_DataDmn;
                  AV180ContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
                  AV181ContagemResultado_Descricao = A494ContagemResultado_Descricao;
                  AV153StatusDmn = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
                  AV158ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
                  AV182ContagemResultado_CntNum = A1612ContagemResultado_CntNum;
                  HYR0( false, 15) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV179ContagemResultado_DataDmn, "99/99/99"), 0, Gx_line+0, 58, Gx_line+15, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV180ContagemResultado_OsFsOsFm, "")), 64, Gx_line+0, 189, Gx_line+15, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV181ContagemResultado_Descricao, "")), 192, Gx_line+0, 447, Gx_line+15, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV158ContagemResultado_ServicoSigla, "@!")), 550, Gx_line+0, 739, Gx_line+15, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A1612ContagemResultado_CntNum, "")), 742, Gx_line+0, 825, Gx_line+15, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV153StatusDmn, "")), 450, Gx_line+0, 542, Gx_line+15, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  AV156TotalDemandas = (short)(+1);
                  pr_default.readNext(5);
               }
               pr_default.close(5);
               if ( ! (0==AV156TotalDemandas) )
               {
                  HYR0( false, 35) ;
                  getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Total:", 683, Gx_line+17, 741, Gx_line+35, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV156TotalDemandas), "ZZZ9")), 742, Gx_line+17, 825, Gx_line+34, 2, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+35);
               }
            }
            if ( ! (0==AV178CodigosNaoGeradas.Count) )
            {
               AV156TotalDemandas = 0;
               AV183IdentificacaoListaDemandas = "Demanda(s) j� existente(s). Demanda(s) vinculada(s) n�o criada(s)!";
               HYR0( false, 25) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV183IdentificacaoListaDemandas, "")), 0, Gx_line+0, 750, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+25);
               HYR0( false, 18) ;
               getPrinter().GxDrawLine(0, Gx_line+17, 825, Gx_line+17, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Contrato", 742, Gx_line+0, 825, Gx_line+18, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Data", 0, Gx_line+0, 58, Gx_line+18, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("N�| N�Ref.", 67, Gx_line+0, 192, Gx_line+18, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Descri��o", 192, Gx_line+0, 447, Gx_line+18, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Status", 450, Gx_line+0, 542, Gx_line+18, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Servi�o", 550, Gx_line+0, 739, Gx_line+18, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+18);
               AV179ContagemResultado_DataDmn = DateTime.MinValue;
               AV180ContagemResultado_OsFsOsFm = "";
               AV181ContagemResultado_Descricao = "";
               AV158ContagemResultado_ServicoSigla = "";
               AV182ContagemResultado_CntNum = "";
               AV156TotalDemandas = 0;
               pr_default.dynParam(6, new Object[]{ new Object[]{
                                                    A456ContagemResultado_Codigo ,
                                                    AV178CodigosNaoGeradas },
                                                    new int[] {
                                                    TypeConstants.INT
                                                    }
               });
               /* Using cursor P00YR8 */
               pr_default.execute(6);
               while ( (pr_default.getStatus(6) != 101) )
               {
                  A1553ContagemResultado_CntSrvCod = P00YR8_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00YR8_n1553ContagemResultado_CntSrvCod[0];
                  A1603ContagemResultado_CntCod = P00YR8_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P00YR8_n1603ContagemResultado_CntCod[0];
                  A601ContagemResultado_Servico = P00YR8_A601ContagemResultado_Servico[0];
                  n601ContagemResultado_Servico = P00YR8_n601ContagemResultado_Servico[0];
                  A456ContagemResultado_Codigo = P00YR8_A456ContagemResultado_Codigo[0];
                  A471ContagemResultado_DataDmn = P00YR8_A471ContagemResultado_DataDmn[0];
                  A494ContagemResultado_Descricao = P00YR8_A494ContagemResultado_Descricao[0];
                  n494ContagemResultado_Descricao = P00YR8_n494ContagemResultado_Descricao[0];
                  A484ContagemResultado_StatusDmn = P00YR8_A484ContagemResultado_StatusDmn[0];
                  n484ContagemResultado_StatusDmn = P00YR8_n484ContagemResultado_StatusDmn[0];
                  A801ContagemResultado_ServicoSigla = P00YR8_A801ContagemResultado_ServicoSigla[0];
                  n801ContagemResultado_ServicoSigla = P00YR8_n801ContagemResultado_ServicoSigla[0];
                  A1612ContagemResultado_CntNum = P00YR8_A1612ContagemResultado_CntNum[0];
                  n1612ContagemResultado_CntNum = P00YR8_n1612ContagemResultado_CntNum[0];
                  A493ContagemResultado_DemandaFM = P00YR8_A493ContagemResultado_DemandaFM[0];
                  n493ContagemResultado_DemandaFM = P00YR8_n493ContagemResultado_DemandaFM[0];
                  A457ContagemResultado_Demanda = P00YR8_A457ContagemResultado_Demanda[0];
                  n457ContagemResultado_Demanda = P00YR8_n457ContagemResultado_Demanda[0];
                  A1603ContagemResultado_CntCod = P00YR8_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P00YR8_n1603ContagemResultado_CntCod[0];
                  A601ContagemResultado_Servico = P00YR8_A601ContagemResultado_Servico[0];
                  n601ContagemResultado_Servico = P00YR8_n601ContagemResultado_Servico[0];
                  A1612ContagemResultado_CntNum = P00YR8_A1612ContagemResultado_CntNum[0];
                  n1612ContagemResultado_CntNum = P00YR8_n1612ContagemResultado_CntNum[0];
                  A801ContagemResultado_ServicoSigla = P00YR8_A801ContagemResultado_ServicoSigla[0];
                  n801ContagemResultado_ServicoSigla = P00YR8_n801ContagemResultado_ServicoSigla[0];
                  A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
                  AV179ContagemResultado_DataDmn = A471ContagemResultado_DataDmn;
                  AV180ContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
                  AV181ContagemResultado_Descricao = A494ContagemResultado_Descricao;
                  AV153StatusDmn = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
                  AV158ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
                  AV182ContagemResultado_CntNum = A1612ContagemResultado_CntNum;
                  HYR0( false, 15) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV179ContagemResultado_DataDmn, "99/99/99"), 0, Gx_line+0, 58, Gx_line+15, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV180ContagemResultado_OsFsOsFm, "")), 64, Gx_line+0, 189, Gx_line+15, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV181ContagemResultado_Descricao, "")), 192, Gx_line+0, 447, Gx_line+15, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV158ContagemResultado_ServicoSigla, "@!")), 550, Gx_line+0, 739, Gx_line+15, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A1612ContagemResultado_CntNum, "")), 742, Gx_line+0, 825, Gx_line+15, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV153StatusDmn, "")), 450, Gx_line+0, 542, Gx_line+15, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  AV156TotalDemandas = (short)(+1);
                  pr_default.readNext(6);
               }
               pr_default.close(6);
               if ( ! (0==AV156TotalDemandas) )
               {
                  HYR0( false, 35) ;
                  getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Total:", 683, Gx_line+17, 741, Gx_line+35, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV156TotalDemandas), "ZZZ9")), 742, Gx_line+17, 825, Gx_line+34, 2, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+35);
               }
            }
            /* Execute user subroutine: 'ENVIO.NOTIFICACAO.EMAIL' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            HYR0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'VERIFICA.EXITE.DEMANDA' Routine */
         AV177IsDemandaCadastrada = false;
         pr_default.dynParam(7, new Object[]{ new Object[]{
                                              AV148ContagemResultado_SistemaCod ,
                                              A489ContagemResultado_SistemaCod ,
                                              A493ContagemResultado_DemandaFM ,
                                              AV175ContagemResultado_DemandaFM ,
                                              AV54Contratada_Codigo ,
                                              A490ContagemResultado_ContratadaCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P00YR9 */
         pr_default.execute(7, new Object[] {AV54Contratada_Codigo, AV175ContagemResultado_DemandaFM, AV148ContagemResultado_SistemaCod});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A489ContagemResultado_SistemaCod = P00YR9_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00YR9_n489ContagemResultado_SistemaCod[0];
            A493ContagemResultado_DemandaFM = P00YR9_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YR9_n493ContagemResultado_DemandaFM[0];
            A490ContagemResultado_ContratadaCod = P00YR9_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YR9_n490ContagemResultado_ContratadaCod[0];
            A456ContagemResultado_Codigo = P00YR9_A456ContagemResultado_Codigo[0];
            AV177IsDemandaCadastrada = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(7);
         }
         pr_default.close(7);
      }

      protected void S121( )
      {
         /* 'DADOS.CONTRATADA' Routine */
         AV154Contratada_PessoaNom = "";
         /* Using cursor P00YR10 */
         pr_default.execute(8, new Object[] {AV54Contratada_Codigo});
         while ( (pr_default.getStatus(8) != 101) )
         {
            A40Contratada_PessoaCod = P00YR10_A40Contratada_PessoaCod[0];
            A39Contratada_Codigo = P00YR10_A39Contratada_Codigo[0];
            A41Contratada_PessoaNom = P00YR10_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00YR10_n41Contratada_PessoaNom[0];
            A41Contratada_PessoaNom = P00YR10_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00YR10_n41Contratada_PessoaNom[0];
            AV154Contratada_PessoaNom = A41Contratada_PessoaNom;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(8);
      }

      protected void S131( )
      {
         /* 'DADOS.PREPOSTO' Routine */
         AV155Contrato_PrepostoCod = 0;
         /* Using cursor P00YR11 */
         pr_default.execute(9, new Object[] {AV130Contrato_Codigo, AV54Contratada_Codigo});
         while ( (pr_default.getStatus(9) != 101) )
         {
            A39Contratada_Codigo = P00YR11_A39Contratada_Codigo[0];
            A74Contrato_Codigo = P00YR11_A74Contrato_Codigo[0];
            A1013Contrato_PrepostoCod = P00YR11_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = P00YR11_n1013Contrato_PrepostoCod[0];
            AV155Contrato_PrepostoCod = A1013Contrato_PrepostoCod;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(9);
      }

      protected void S141( )
      {
         /* 'ENVIO.NOTIFICACAO.EMAIL' Routine */
         /* Using cursor P00YR13 */
         pr_default.execute(10, new Object[] {AV126WWPContext.gxTpr_Userid});
         if ( (pr_default.getStatus(10) != 101) )
         {
            A40000Usuario_PessoaNom = P00YR13_A40000Usuario_PessoaNom[0];
         }
         else
         {
            A40000Usuario_PessoaNom = "";
         }
         pr_default.close(10);
         AV167ContagemResultado_Owner = AV126WWPContext.gxTpr_Userid;
         AV164ContagemResultado_Owner_Nome = A40000Usuario_PessoaNom;
         if ( ! (0==AV159Codigos.Count) )
         {
            /* Using cursor P00YR14 */
            pr_default.execute(11, new Object[] {AV126WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(11) != 101) )
            {
               A29Contratante_Codigo = P00YR14_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00YR14_n29Contratante_Codigo[0];
               A548Contratante_EmailSdaUser = P00YR14_A548Contratante_EmailSdaUser[0];
               n548Contratante_EmailSdaUser = P00YR14_n548Contratante_EmailSdaUser[0];
               A552Contratante_EmailSdaPort = P00YR14_A552Contratante_EmailSdaPort[0];
               n552Contratante_EmailSdaPort = P00YR14_n552Contratante_EmailSdaPort[0];
               A547Contratante_EmailSdaHost = P00YR14_A547Contratante_EmailSdaHost[0];
               n547Contratante_EmailSdaHost = P00YR14_n547Contratante_EmailSdaHost[0];
               A5AreaTrabalho_Codigo = P00YR14_A5AreaTrabalho_Codigo[0];
               A548Contratante_EmailSdaUser = P00YR14_A548Contratante_EmailSdaUser[0];
               n548Contratante_EmailSdaUser = P00YR14_n548Contratante_EmailSdaUser[0];
               A552Contratante_EmailSdaPort = P00YR14_A552Contratante_EmailSdaPort[0];
               n552Contratante_EmailSdaPort = P00YR14_n552Contratante_EmailSdaPort[0];
               A547Contratante_EmailSdaHost = P00YR14_A547Contratante_EmailSdaHost[0];
               n547Contratante_EmailSdaHost = P00YR14_n547Contratante_EmailSdaHost[0];
               AV157ContratanteSemEmailSda = true;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(11);
            if ( AV157ContratanteSemEmailSda )
            {
               AV109Resultado = "Cadastro da Contratante sem dados configurados para envio de e-mails!";
            }
            else
            {
               /* Execute user subroutine: 'GETEMAILS' */
               S151 ();
               if (returnInSub) return;
               if ( AV122Usuarios.Count > 0 )
               {
                  AV166EmailText = StringUtil.NewLine( ) + "Prezado(a)," + StringUtil.NewLine( );
                  AV166EmailText = AV166EmailText + "H� nova(s) solicita��o(�es) para a prestadora do servi�o de " + AV158ContagemResultado_ServicoSigla + " conforme segue abaixo os detalhes." + StringUtil.NewLine( ) + StringUtil.NewLine( );
                  /* Execute user subroutine: 'BUSCA.DADOS.NOVA.OS' */
                  S161 ();
                  if (returnInSub) return;
                  AV166EmailText = AV166EmailText + "Para atendimento da demanda acesse o MEETRIKA - Sistema de Gest�o Contratual e Automa��o de Processos " + StringUtil.NewLine( );
                  AV166EmailText = AV166EmailText + StringUtil.NewLine( ) + StringUtil.Trim( AV126WWPContext.gxTpr_Areatrabalho_descricao) + ", usu�rio " + StringUtil.Trim( AV126WWPContext.gxTpr_Username) + StringUtil.NewLine( );
                  if ( AV167ContagemResultado_Owner > 0 )
                  {
                     AV166EmailText = AV166EmailText + " (autorizado por " + AV164ContagemResultado_Owner_Nome + ")" + StringUtil.NewLine( );
                  }
                  else
                  {
                     AV166EmailText = AV166EmailText + StringUtil.NewLine( );
                  }
                  AV166EmailText = AV166EmailText + StringUtil.NewLine( ) + StringUtil.NewLine( ) + "Esta mensagem pode conter informa��o confidencial e/ou privilegiada. Se voc� n�o for o destinat�rio ou a pessoa autorizada a receber esta mensagem, n�o pode usar, copiar ou divulgar as informa��es nela contidas ou tomar qualquer a��o baseada nessas informa��es. Se voc� recebeu esta mensagem por engano, por favor avise imediatamente o remetente, respondendo o e-mail e em seguida apague-o.";
                  AV117Subject = "[" + StringUtil.Trim( AV126WWPContext.gxTpr_Parametrossistema_nomesistema) + " - " + StringUtil.Trim( AV126WWPContext.gxTpr_Areatrabalho_descricao) + "] OS solicitada(s)" + " (No reply)";
                  AV125WebSession.Set("DemandaCodigo", AV159Codigos.ToXml(false, true, "Collection", ""));
                  new prc_enviaremail(context ).execute(  AV126WWPContext.gxTpr_Areatrabalho_codigo,  AV122Usuarios,  AV117Subject,  AV166EmailText,  AV14Attachments, ref  AV109Resultado) ;
                  AV125WebSession.Remove("DemandaCodigo");
               }
               else
               {
                  AV109Resultado = "Sem usu�rio preposto no requisitado para envio da notifica��o!";
               }
            }
         }
      }

      protected void S151( )
      {
         /* 'GETEMAILS' Routine */
         AV122Usuarios.Clear();
         /* Using cursor P00YR15 */
         pr_default.execute(12, new Object[] {AV130Contrato_Codigo});
         while ( (pr_default.getStatus(12) != 101) )
         {
            A74Contrato_Codigo = P00YR15_A74Contrato_Codigo[0];
            A1013Contrato_PrepostoCod = P00YR15_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = P00YR15_n1013Contrato_PrepostoCod[0];
            if ( A1013Contrato_PrepostoCod > 0 )
            {
               AV122Usuarios.Add(A1013Contrato_PrepostoCod, 0);
            }
            else
            {
               /* Using cursor P00YR16 */
               pr_default.execute(13, new Object[] {A74Contrato_Codigo});
               while ( (pr_default.getStatus(13) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = P00YR16_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = P00YR16_A1079ContratoGestor_UsuarioCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = P00YR16_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = P00YR16_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = P00YR16_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = P00YR16_n1446ContratoGestor_ContratadaAreaCod[0];
                  GXt_boolean3 = A1135ContratoGestor_UsuarioEhContratante;
                  new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean3) ;
                  A1135ContratoGestor_UsuarioEhContratante = GXt_boolean3;
                  if ( ! A1135ContratoGestor_UsuarioEhContratante )
                  {
                     AV122Usuarios.Add(A1079ContratoGestor_UsuarioCod, 0);
                  }
                  pr_default.readNext(13);
               }
               pr_default.close(13);
            }
            /* Using cursor P00YR17 */
            pr_default.execute(14, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(14) != 101) )
            {
               A1078ContratoGestor_ContratoCod = P00YR17_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = P00YR17_A1079ContratoGestor_UsuarioCod[0];
               A1446ContratoGestor_ContratadaAreaCod = P00YR17_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = P00YR17_n1446ContratoGestor_ContratadaAreaCod[0];
               A1446ContratoGestor_ContratadaAreaCod = P00YR17_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = P00YR17_n1446ContratoGestor_ContratadaAreaCod[0];
               GXt_boolean3 = A1135ContratoGestor_UsuarioEhContratante;
               new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean3) ;
               A1135ContratoGestor_UsuarioEhContratante = GXt_boolean3;
               if ( A1135ContratoGestor_UsuarioEhContratante )
               {
                  AV122Usuarios.Add(A1079ContratoGestor_UsuarioCod, 0);
               }
               pr_default.readNext(14);
            }
            pr_default.close(14);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(12);
      }

      protected void S161( )
      {
         /* 'BUSCA.DADOS.NOVA.OS' Routine */
         pr_default.dynParam(15, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV159Codigos },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor P00YR19 */
         pr_default.execute(15);
         while ( (pr_default.getStatus(15) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00YR19_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00YR19_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P00YR19_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YR19_n601ContagemResultado_Servico[0];
            A508ContagemResultado_Owner = P00YR19_A508ContagemResultado_Owner[0];
            A456ContagemResultado_Codigo = P00YR19_A456ContagemResultado_Codigo[0];
            A493ContagemResultado_DemandaFM = P00YR19_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YR19_n493ContagemResultado_DemandaFM[0];
            A801ContagemResultado_ServicoSigla = P00YR19_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YR19_n801ContagemResultado_ServicoSigla[0];
            A494ContagemResultado_Descricao = P00YR19_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00YR19_n494ContagemResultado_Descricao[0];
            A471ContagemResultado_DataDmn = P00YR19_A471ContagemResultado_DataDmn[0];
            A472ContagemResultado_DataEntrega = P00YR19_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00YR19_n472ContagemResultado_DataEntrega[0];
            A40001Usuario_PessoaNom = P00YR19_A40001Usuario_PessoaNom[0];
            n40001Usuario_PessoaNom = P00YR19_n40001Usuario_PessoaNom[0];
            A601ContagemResultado_Servico = P00YR19_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YR19_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00YR19_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YR19_n801ContagemResultado_ServicoSigla[0];
            A40001Usuario_PessoaNom = P00YR19_A40001Usuario_PessoaNom[0];
            n40001Usuario_PessoaNom = P00YR19_n40001Usuario_PessoaNom[0];
            AV166EmailText = AV166EmailText + "No da OS�������������������" + A493ContagemResultado_DemandaFM + StringUtil.NewLine( );
            AV166EmailText = AV166EmailText + "Servi�o��������������������" + A801ContagemResultado_ServicoSigla + StringUtil.NewLine( );
            AV166EmailText = AV166EmailText + "Descri��o������������������" + StringUtil.Trim( A494ContagemResultado_Descricao) + StringUtil.NewLine( );
            AV166EmailText = AV166EmailText + "Data de Abertura�����������" + context.localUtil.DToC( A471ContagemResultado_DataDmn, 2, "/") + StringUtil.NewLine( );
            AV166EmailText = AV166EmailText + "Expectativa de Atendimento�" + context.localUtil.DToC( A472ContagemResultado_DataEntrega, 2, "/") + StringUtil.NewLine( );
            AV166EmailText = AV166EmailText + "Requisitante���������������" + A40001Usuario_PessoaNom + StringUtil.NewLine( );
            AV166EmailText = AV166EmailText + "Telefone�������������������" + AV126WWPContext.gxTpr_Contratante_telefone + StringUtil.NewLine( );
            AV166EmailText = AV166EmailText + "Unidade Organizacional�����" + StringUtil.Trim( AV126WWPContext.gxTpr_Areatrabalho_descricao) + StringUtil.NewLine( ) + StringUtil.NewLine( );
            pr_default.readNext(15);
         }
         pr_default.close(15);
      }

      protected void S171( )
      {
         /* 'ATUALIZA.DADOS.DEMANDA.PRINCIPAL' Routine */
         /* Using cursor P00YR20 */
         pr_default.execute(16, new Object[] {AV41ContagemResultado_Codigo});
         while ( (pr_default.getStatus(16) != 101) )
         {
            A456ContagemResultado_Codigo = P00YR20_A456ContagemResultado_Codigo[0];
            A602ContagemResultado_OSVinculada = P00YR20_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00YR20_n602ContagemResultado_OSVinculada[0];
            A1046ContagemResultado_Agrupador = P00YR20_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00YR20_n1046ContagemResultado_Agrupador[0];
            A1515ContagemResultado_Evento = P00YR20_A1515ContagemResultado_Evento[0];
            n1515ContagemResultado_Evento = P00YR20_n1515ContagemResultado_Evento[0];
            A602ContagemResultado_OSVinculada = AV144NewCodigo;
            n602ContagemResultado_OSVinculada = false;
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV187ContagemResultado_AgrupadorAux)) && ( AV185IsModificarAgrupador == 1 ) )
            {
               A1046ContagemResultado_Agrupador = AV187ContagemResultado_AgrupadorAux;
               n1046ContagemResultado_Agrupador = false;
            }
            if ( P00YR20_n1515ContagemResultado_Evento[0] )
            {
               A1515ContagemResultado_Evento = 1;
               n1515ContagemResultado_Evento = false;
            }
            /* Using cursor P00YR21 */
            pr_default.execute(17, new Object[] {n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada, n1046ContagemResultado_Agrupador, A1046ContagemResultado_Agrupador, n1515ContagemResultado_Evento, A1515ContagemResultado_Evento, A456ContagemResultado_Codigo});
            pr_default.close(17);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(16);
      }

      protected void HYR0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("P�gina", 675, Gx_line+0, 710, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 750, Gx_line+0, 764, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 767, Gx_line+0, 816, Gx_line+14, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 708, Gx_line+0, 747, Gx_line+15, 1+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 133, Gx_line+67, 258, Gx_line+84, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV154Contratada_PessoaNom, "@!")), 133, Gx_line+83, 358, Gx_line+101, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Data de Importa��o:", 0, Gx_line+67, 125, Gx_line+85, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Prestadora:", 0, Gx_line+83, 125, Gx_line+101, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( Gx_time, "")), 725, Gx_line+9, 818, Gx_line+24, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 667, Gx_line+9, 716, Gx_line+24, 2+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 14, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Demadas", 0, Gx_line+0, 817, Gx_line+25, 1, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+132);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Calibri", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV126WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV110ServerNow = (DateTime)(DateTime.MinValue);
         AV170ServerDate = DateTime.MinValue;
         scmdbuf = "";
         P00YR2_A74Contrato_Codigo = new int[1] ;
         P00YR2_A39Contratada_Codigo = new int[1] ;
         P00YR2_A40Contratada_PessoaCod = new int[1] ;
         P00YR2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00YR2_A160ContratoServicos_Codigo = new int[1] ;
         P00YR2_A41Contratada_PessoaNom = new String[] {""} ;
         P00YR2_n41Contratada_PessoaNom = new bool[] {false} ;
         P00YR2_A558Servico_Percentual = new decimal[1] ;
         P00YR2_n558Servico_Percentual = new bool[] {false} ;
         P00YR2_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         P00YR2_A1224ContratoServicos_PrazoCorrecao = new short[1] ;
         P00YR2_n1224ContratoServicos_PrazoCorrecao = new bool[] {false} ;
         P00YR2_A1225ContratoServicos_PrazoCorrecaoTipo = new String[] {""} ;
         P00YR2_n1225ContratoServicos_PrazoCorrecaoTipo = new bool[] {false} ;
         P00YR2_A1649ContratoServicos_PrazoInicio = new short[1] ;
         P00YR2_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         P00YR2_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         P00YR2_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         P00YR2_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         P00YR2_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         P00YR2_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         A1225ContratoServicos_PrazoCorrecaoTipo = "";
         A1454ContratoServicos_PrazoTpDias = "";
         AV106PrazoInicio = 1;
         AV56ContratadaFS_Nome = "";
         AV103PrazoCorrecaoTipo = "";
         AV199TipoDias = "";
         P00YR3_A5AreaTrabalho_Codigo = new int[1] ;
         P00YR3_A29Contratante_Codigo = new int[1] ;
         P00YR3_n29Contratante_Codigo = new bool[] {false} ;
         P00YR3_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00YR3_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         A1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         P00YR4_A29Contratante_Codigo = new int[1] ;
         P00YR4_n29Contratante_Codigo = new bool[] {false} ;
         AV76FimDoExpediente = (DateTime)(DateTime.MinValue);
         AV132ContagemResultado_CodigoAux = new GxSimpleCollection();
         AV125WebSession = context.GetSession();
         P00YR5_A601ContagemResultado_Servico = new int[1] ;
         P00YR5_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YR5_A2133ContagemResultado_QuantidadeSolicitada = new decimal[1] ;
         P00YR5_n2133ContagemResultado_QuantidadeSolicitada = new bool[] {false} ;
         P00YR5_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         P00YR5_A1903ContagemResultado_DataPrvPgm = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n1903ContagemResultado_DataPrvPgm = new bool[] {false} ;
         P00YR5_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YR5_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YR5_A1520ContagemResultado_InicioAnl = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n1520ContagemResultado_InicioAnl = new bool[] {false} ;
         P00YR5_A1519ContagemResultado_TmpEstAnl = new int[1] ;
         P00YR5_n1519ContagemResultado_TmpEstAnl = new bool[] {false} ;
         P00YR5_A1515ContagemResultado_Evento = new short[1] ;
         P00YR5_n1515ContagemResultado_Evento = new bool[] {false} ;
         P00YR5_A1511ContagemResultado_InicioCrr = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n1511ContagemResultado_InicioCrr = new bool[] {false} ;
         P00YR5_A1510ContagemResultado_FimExc = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n1510ContagemResultado_FimExc = new bool[] {false} ;
         P00YR5_A1509ContagemResultado_InicioExc = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n1509ContagemResultado_InicioExc = new bool[] {false} ;
         P00YR5_A1505ContagemResultado_TmpEstExc = new int[1] ;
         P00YR5_n1505ContagemResultado_TmpEstExc = new bool[] {false} ;
         P00YR5_A1452ContagemResultado_SS = new int[1] ;
         P00YR5_n1452ContagemResultado_SS = new bool[] {false} ;
         P00YR5_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00YR5_A1350ContagemResultado_DataCadastro = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n1350ContagemResultado_DataCadastro = new bool[] {false} ;
         P00YR5_A1349ContagemResultado_DataExecucao = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n1349ContagemResultado_DataExecucao = new bool[] {false} ;
         P00YR5_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P00YR5_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P00YR5_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P00YR5_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P00YR5_A1179ContagemResultado_PLFinal = new decimal[1] ;
         P00YR5_n1179ContagemResultado_PLFinal = new bool[] {false} ;
         P00YR5_A1178ContagemResultado_PBFinal = new decimal[1] ;
         P00YR5_n1178ContagemResultado_PBFinal = new bool[] {false} ;
         P00YR5_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00YR5_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00YR5_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00YR5_A890ContagemResultado_Responsavel = new int[1] ;
         P00YR5_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00YR5_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P00YR5_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P00YR5_A602ContagemResultado_OSVinculada = new int[1] ;
         P00YR5_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00YR5_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00YR5_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00YR5_A508ContagemResultado_Owner = new int[1] ;
         P00YR5_A146Modulo_Codigo = new int[1] ;
         P00YR5_n146Modulo_Codigo = new bool[] {false} ;
         P00YR5_A489ContagemResultado_SistemaCod = new int[1] ;
         P00YR5_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00YR5_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YR5_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YR5_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YR5_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YR5_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00YR5_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00YR5_A457ContagemResultado_Demanda = new String[] {""} ;
         P00YR5_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00YR5_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00YR5_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YR5_A1855ContagemResultado_PFCnc = new decimal[1] ;
         P00YR5_n1855ContagemResultado_PFCnc = new bool[] {false} ;
         P00YR5_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P00YR5_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P00YR5_A1791ContagemResultado_SemCusto = new bool[] {false} ;
         P00YR5_n1791ContagemResultado_SemCusto = new bool[] {false} ;
         P00YR5_A1790ContagemResultado_DataInicio = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n1790ContagemResultado_DataInicio = new bool[] {false} ;
         P00YR5_A1762ContagemResultado_Entrega = new short[1] ;
         P00YR5_n1762ContagemResultado_Entrega = new bool[] {false} ;
         P00YR5_A1714ContagemResultado_Combinada = new bool[] {false} ;
         P00YR5_n1714ContagemResultado_Combinada = new bool[] {false} ;
         P00YR5_A1636ContagemResultado_ServicoSS = new int[1] ;
         P00YR5_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         P00YR5_A1587ContagemResultado_PrioridadePrevista = new String[] {""} ;
         P00YR5_n1587ContagemResultado_PrioridadePrevista = new bool[] {false} ;
         P00YR5_A1586ContagemResultado_Restricoes = new String[] {""} ;
         P00YR5_n1586ContagemResultado_Restricoes = new bool[] {false} ;
         P00YR5_A1585ContagemResultado_Referencia = new String[] {""} ;
         P00YR5_n1585ContagemResultado_Referencia = new bool[] {false} ;
         P00YR5_A1584ContagemResultado_UOOwner = new int[1] ;
         P00YR5_n1584ContagemResultado_UOOwner = new bool[] {false} ;
         P00YR5_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00YR5_A1559ContagemResultado_VlrAceite = new decimal[1] ;
         P00YR5_n1559ContagemResultado_VlrAceite = new bool[] {false} ;
         P00YR5_A1544ContagemResultado_ProjetoCod = new int[1] ;
         P00YR5_n1544ContagemResultado_ProjetoCod = new bool[] {false} ;
         P00YR5_A1521ContagemResultado_FimAnl = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n1521ContagemResultado_FimAnl = new bool[] {false} ;
         P00YR5_A1512ContagemResultado_FimCrr = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n1512ContagemResultado_FimCrr = new bool[] {false} ;
         P00YR5_A1506ContagemResultado_TmpEstCrr = new int[1] ;
         P00YR5_n1506ContagemResultado_TmpEstCrr = new bool[] {false} ;
         P00YR5_A1457ContagemResultado_TemDpnHmlg = new bool[] {false} ;
         P00YR5_n1457ContagemResultado_TemDpnHmlg = new bool[] {false} ;
         P00YR5_A1445ContagemResultado_CntSrvPrrCst = new decimal[1] ;
         P00YR5_n1445ContagemResultado_CntSrvPrrCst = new bool[] {false} ;
         P00YR5_A1444ContagemResultado_CntSrvPrrPrz = new decimal[1] ;
         P00YR5_n1444ContagemResultado_CntSrvPrrPrz = new bool[] {false} ;
         P00YR5_A1443ContagemResultado_CntSrvPrrCod = new int[1] ;
         P00YR5_n1443ContagemResultado_CntSrvPrrCod = new bool[] {false} ;
         P00YR5_A1392ContagemResultado_RdmnUpdated = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n1392ContagemResultado_RdmnUpdated = new bool[] {false} ;
         P00YR5_A1390ContagemResultado_RdmnProjectId = new int[1] ;
         P00YR5_n1390ContagemResultado_RdmnProjectId = new bool[] {false} ;
         P00YR5_A1389ContagemResultado_RdmnIssueId = new int[1] ;
         P00YR5_n1389ContagemResultado_RdmnIssueId = new bool[] {false} ;
         P00YR5_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         P00YR5_A1180ContagemResultado_Custo = new decimal[1] ;
         P00YR5_n1180ContagemResultado_Custo = new bool[] {false} ;
         P00YR5_A1173ContagemResultado_OSManual = new bool[] {false} ;
         P00YR5_n1173ContagemResultado_OSManual = new bool[] {false} ;
         P00YR5_A1052ContagemResultado_GlsUser = new int[1] ;
         P00YR5_n1052ContagemResultado_GlsUser = new bool[] {false} ;
         P00YR5_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P00YR5_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P00YR5_A1050ContagemResultado_GlsDescricao = new String[] {""} ;
         P00YR5_n1050ContagemResultado_GlsDescricao = new bool[] {false} ;
         P00YR5_A1049ContagemResultado_GlsData = new DateTime[] {DateTime.MinValue} ;
         P00YR5_n1049ContagemResultado_GlsData = new bool[] {false} ;
         P00YR5_A1044ContagemResultado_FncUsrCod = new int[1] ;
         P00YR5_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         P00YR5_A1043ContagemResultado_LiqLogCod = new int[1] ;
         P00YR5_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         P00YR5_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         P00YR5_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         P00YR5_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         P00YR5_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         P00YR5_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00YR5_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00YR5_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00YR5_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00YR5_A592ContagemResultado_Evidencia = new String[] {""} ;
         P00YR5_n592ContagemResultado_Evidencia = new bool[] {false} ;
         P00YR5_A514ContagemResultado_Observacao = new String[] {""} ;
         P00YR5_n514ContagemResultado_Observacao = new bool[] {false} ;
         P00YR5_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YR5_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YR5_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         P00YR5_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         P00YR5_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P00YR5_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P00YR5_A465ContagemResultado_Link = new String[] {""} ;
         P00YR5_n465ContagemResultado_Link = new bool[] {false} ;
         P00YR5_A454ContagemResultado_ContadorFSCod = new int[1] ;
         P00YR5_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         P00YR5_A456ContagemResultado_Codigo = new int[1] ;
         P00YR5_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00YR5_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         A2017ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
         A1903ContagemResultado_DataPrvPgm = DateTime.MinValue;
         A1520ContagemResultado_InicioAnl = (DateTime)(DateTime.MinValue);
         A1511ContagemResultado_InicioCrr = (DateTime)(DateTime.MinValue);
         A1510ContagemResultado_FimExc = (DateTime)(DateTime.MinValue);
         A1509ContagemResultado_InicioExc = (DateTime)(DateTime.MinValue);
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         A1349ContagemResultado_DataExecucao = (DateTime)(DateTime.MinValue);
         A1046ContagemResultado_Agrupador = "";
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A493ContagemResultado_DemandaFM = "";
         A484ContagemResultado_StatusDmn = "";
         A457ContagemResultado_Demanda = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1790ContagemResultado_DataInicio = DateTime.MinValue;
         A1587ContagemResultado_PrioridadePrevista = "";
         A1586ContagemResultado_Restricoes = "";
         A1585ContagemResultado_Referencia = "";
         A1521ContagemResultado_FimAnl = (DateTime)(DateTime.MinValue);
         A1512ContagemResultado_FimCrr = (DateTime)(DateTime.MinValue);
         A1392ContagemResultado_RdmnUpdated = (DateTime)(DateTime.MinValue);
         A1348ContagemResultado_DataHomologacao = (DateTime)(DateTime.MinValue);
         A1050ContagemResultado_GlsDescricao = "";
         A1049ContagemResultado_GlsData = DateTime.MinValue;
         A592ContagemResultado_Evidencia = "";
         A514ContagemResultado_Observacao = "";
         A494ContagemResultado_Descricao = "";
         A465ContagemResultado_Link = "";
         A801ContagemResultado_ServicoSigla = "";
         AV175ContagemResultado_DemandaFM = "";
         AV146ContagemResultado_Demanda = "";
         AV128ContagemResultado_Agrupador = "";
         AV158ContagemResultado_ServicoSigla = "";
         W457ContagemResultado_Demanda = "";
         W493ContagemResultado_DemandaFM = "";
         W472ContagemResultado_DataEntrega = DateTime.MinValue;
         W1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         W912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         W1903ContagemResultado_DataPrvPgm = DateTime.MinValue;
         W484ContagemResultado_StatusDmn = "";
         W2017ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
         W1509ContagemResultado_InicioExc = (DateTime)(DateTime.MinValue);
         W1511ContagemResultado_InicioCrr = (DateTime)(DateTime.MinValue);
         W1510ContagemResultado_FimExc = (DateTime)(DateTime.MinValue);
         W1520ContagemResultado_InicioAnl = (DateTime)(DateTime.MinValue);
         W1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         W471ContagemResultado_DataDmn = DateTime.MinValue;
         W1349ContagemResultado_DataExecucao = (DateTime)(DateTime.MinValue);
         W1046ContagemResultado_Agrupador = "";
         P00YR6_A456ContagemResultado_Codigo = new int[1] ;
         Gx_emsg = "";
         AV159Codigos = new GxSimpleCollection();
         AV114Status = "";
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         AV178CodigosNaoGeradas = new GxSimpleCollection();
         AV183IdentificacaoListaDemandas = "";
         AV179ContagemResultado_DataDmn = DateTime.MinValue;
         AV180ContagemResultado_OsFsOsFm = "";
         AV181ContagemResultado_Descricao = "";
         AV182ContagemResultado_CntNum = "";
         P00YR7_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YR7_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YR7_A1603ContagemResultado_CntCod = new int[1] ;
         P00YR7_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00YR7_A601ContagemResultado_Servico = new int[1] ;
         P00YR7_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YR7_A456ContagemResultado_Codigo = new int[1] ;
         P00YR7_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YR7_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YR7_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YR7_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00YR7_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00YR7_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00YR7_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00YR7_A1612ContagemResultado_CntNum = new String[] {""} ;
         P00YR7_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P00YR7_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YR7_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YR7_A457ContagemResultado_Demanda = new String[] {""} ;
         P00YR7_n457ContagemResultado_Demanda = new bool[] {false} ;
         A1612ContagemResultado_CntNum = "";
         A501ContagemResultado_OsFsOsFm = "";
         AV153StatusDmn = "";
         P00YR8_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YR8_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YR8_A1603ContagemResultado_CntCod = new int[1] ;
         P00YR8_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00YR8_A601ContagemResultado_Servico = new int[1] ;
         P00YR8_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YR8_A456ContagemResultado_Codigo = new int[1] ;
         P00YR8_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YR8_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YR8_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YR8_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00YR8_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00YR8_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00YR8_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00YR8_A1612ContagemResultado_CntNum = new String[] {""} ;
         P00YR8_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P00YR8_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YR8_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YR8_A457ContagemResultado_Demanda = new String[] {""} ;
         P00YR8_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00YR9_A489ContagemResultado_SistemaCod = new int[1] ;
         P00YR9_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00YR9_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YR9_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YR9_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YR9_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YR9_A456ContagemResultado_Codigo = new int[1] ;
         AV154Contratada_PessoaNom = "";
         P00YR10_A40Contratada_PessoaCod = new int[1] ;
         P00YR10_A39Contratada_Codigo = new int[1] ;
         P00YR10_A41Contratada_PessoaNom = new String[] {""} ;
         P00YR10_n41Contratada_PessoaNom = new bool[] {false} ;
         P00YR11_A39Contratada_Codigo = new int[1] ;
         P00YR11_A74Contrato_Codigo = new int[1] ;
         P00YR11_A1013Contrato_PrepostoCod = new int[1] ;
         P00YR11_n1013Contrato_PrepostoCod = new bool[] {false} ;
         P00YR13_A40000Usuario_PessoaNom = new String[] {""} ;
         A40000Usuario_PessoaNom = "";
         AV164ContagemResultado_Owner_Nome = "";
         P00YR14_A29Contratante_Codigo = new int[1] ;
         P00YR14_n29Contratante_Codigo = new bool[] {false} ;
         P00YR14_A548Contratante_EmailSdaUser = new String[] {""} ;
         P00YR14_n548Contratante_EmailSdaUser = new bool[] {false} ;
         P00YR14_A552Contratante_EmailSdaPort = new short[1] ;
         P00YR14_n552Contratante_EmailSdaPort = new bool[] {false} ;
         P00YR14_A547Contratante_EmailSdaHost = new String[] {""} ;
         P00YR14_n547Contratante_EmailSdaHost = new bool[] {false} ;
         P00YR14_A5AreaTrabalho_Codigo = new int[1] ;
         A548Contratante_EmailSdaUser = "";
         A547Contratante_EmailSdaHost = "";
         AV109Resultado = "";
         AV122Usuarios = new GxSimpleCollection();
         AV166EmailText = "";
         AV117Subject = "";
         AV14Attachments = new GxSimpleCollection();
         P00YR15_A74Contrato_Codigo = new int[1] ;
         P00YR15_A1013Contrato_PrepostoCod = new int[1] ;
         P00YR15_n1013Contrato_PrepostoCod = new bool[] {false} ;
         P00YR16_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00YR16_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00YR16_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P00YR16_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         P00YR17_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00YR17_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00YR17_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P00YR17_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         P00YR19_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YR19_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YR19_A601ContagemResultado_Servico = new int[1] ;
         P00YR19_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YR19_A508ContagemResultado_Owner = new int[1] ;
         P00YR19_A456ContagemResultado_Codigo = new int[1] ;
         P00YR19_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YR19_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YR19_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00YR19_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00YR19_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YR19_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YR19_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YR19_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00YR19_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00YR19_A40001Usuario_PessoaNom = new String[] {""} ;
         P00YR19_n40001Usuario_PessoaNom = new bool[] {false} ;
         A40001Usuario_PessoaNom = "";
         P00YR20_A456ContagemResultado_Codigo = new int[1] ;
         P00YR20_A602ContagemResultado_OSVinculada = new int[1] ;
         P00YR20_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00YR20_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00YR20_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00YR20_A1515ContagemResultado_Evento = new short[1] ;
         P00YR20_n1515ContagemResultado_Evento = new bool[] {false} ;
         Gx_date = DateTime.MinValue;
         Gx_time = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aprc_demandasvinculadas__default(),
            new Object[][] {
                new Object[] {
               P00YR2_A74Contrato_Codigo, P00YR2_A39Contratada_Codigo, P00YR2_A40Contratada_PessoaCod, P00YR2_A52Contratada_AreaTrabalhoCod, P00YR2_A160ContratoServicos_Codigo, P00YR2_A41Contratada_PessoaNom, P00YR2_n41Contratada_PessoaNom, P00YR2_A558Servico_Percentual, P00YR2_n558Servico_Percentual, P00YR2_A116Contrato_ValorUnidadeContratacao,
               P00YR2_A1224ContratoServicos_PrazoCorrecao, P00YR2_n1224ContratoServicos_PrazoCorrecao, P00YR2_A1225ContratoServicos_PrazoCorrecaoTipo, P00YR2_n1225ContratoServicos_PrazoCorrecaoTipo, P00YR2_A1649ContratoServicos_PrazoInicio, P00YR2_n1649ContratoServicos_PrazoInicio, P00YR2_A1152ContratoServicos_PrazoAnalise, P00YR2_n1152ContratoServicos_PrazoAnalise, P00YR2_A557Servico_VlrUnidadeContratada, P00YR2_A1454ContratoServicos_PrazoTpDias,
               P00YR2_n1454ContratoServicos_PrazoTpDias
               }
               , new Object[] {
               P00YR3_A5AreaTrabalho_Codigo, P00YR3_A29Contratante_Codigo, P00YR3_n29Contratante_Codigo, P00YR3_A1192Contratante_FimDoExpediente, P00YR3_n1192Contratante_FimDoExpediente
               }
               , new Object[] {
               P00YR4_A29Contratante_Codigo
               }
               , new Object[] {
               P00YR5_A601ContagemResultado_Servico, P00YR5_n601ContagemResultado_Servico, P00YR5_A2133ContagemResultado_QuantidadeSolicitada, P00YR5_n2133ContagemResultado_QuantidadeSolicitada, P00YR5_A2017ContagemResultado_DataEntregaReal, P00YR5_n2017ContagemResultado_DataEntregaReal, P00YR5_A1903ContagemResultado_DataPrvPgm, P00YR5_n1903ContagemResultado_DataPrvPgm, P00YR5_A1553ContagemResultado_CntSrvCod, P00YR5_n1553ContagemResultado_CntSrvCod,
               P00YR5_A1520ContagemResultado_InicioAnl, P00YR5_n1520ContagemResultado_InicioAnl, P00YR5_A1519ContagemResultado_TmpEstAnl, P00YR5_n1519ContagemResultado_TmpEstAnl, P00YR5_A1515ContagemResultado_Evento, P00YR5_n1515ContagemResultado_Evento, P00YR5_A1511ContagemResultado_InicioCrr, P00YR5_n1511ContagemResultado_InicioCrr, P00YR5_A1510ContagemResultado_FimExc, P00YR5_n1510ContagemResultado_FimExc,
               P00YR5_A1509ContagemResultado_InicioExc, P00YR5_n1509ContagemResultado_InicioExc, P00YR5_A1505ContagemResultado_TmpEstExc, P00YR5_n1505ContagemResultado_TmpEstExc, P00YR5_A1452ContagemResultado_SS, P00YR5_n1452ContagemResultado_SS, P00YR5_A1351ContagemResultado_DataPrevista, P00YR5_n1351ContagemResultado_DataPrevista, P00YR5_A1350ContagemResultado_DataCadastro, P00YR5_n1350ContagemResultado_DataCadastro,
               P00YR5_A1349ContagemResultado_DataExecucao, P00YR5_n1349ContagemResultado_DataExecucao, P00YR5_A1237ContagemResultado_PrazoMaisDias, P00YR5_n1237ContagemResultado_PrazoMaisDias, P00YR5_A1227ContagemResultado_PrazoInicialDias, P00YR5_n1227ContagemResultado_PrazoInicialDias, P00YR5_A1179ContagemResultado_PLFinal, P00YR5_n1179ContagemResultado_PLFinal, P00YR5_A1178ContagemResultado_PBFinal, P00YR5_n1178ContagemResultado_PBFinal,
               P00YR5_A1046ContagemResultado_Agrupador, P00YR5_n1046ContagemResultado_Agrupador, P00YR5_A912ContagemResultado_HoraEntrega, P00YR5_n912ContagemResultado_HoraEntrega, P00YR5_A890ContagemResultado_Responsavel, P00YR5_n890ContagemResultado_Responsavel, P00YR5_A805ContagemResultado_ContratadaOrigemCod, P00YR5_n805ContagemResultado_ContratadaOrigemCod, P00YR5_A602ContagemResultado_OSVinculada, P00YR5_n602ContagemResultado_OSVinculada,
               P00YR5_A512ContagemResultado_ValorPF, P00YR5_n512ContagemResultado_ValorPF, P00YR5_A508ContagemResultado_Owner, P00YR5_A146Modulo_Codigo, P00YR5_n146Modulo_Codigo, P00YR5_A489ContagemResultado_SistemaCod, P00YR5_n489ContagemResultado_SistemaCod, P00YR5_A493ContagemResultado_DemandaFM, P00YR5_n493ContagemResultado_DemandaFM, P00YR5_A490ContagemResultado_ContratadaCod,
               P00YR5_n490ContagemResultado_ContratadaCod, P00YR5_A484ContagemResultado_StatusDmn, P00YR5_n484ContagemResultado_StatusDmn, P00YR5_A457ContagemResultado_Demanda, P00YR5_n457ContagemResultado_Demanda, P00YR5_A472ContagemResultado_DataEntrega, P00YR5_n472ContagemResultado_DataEntrega, P00YR5_A471ContagemResultado_DataDmn, P00YR5_A1855ContagemResultado_PFCnc, P00YR5_n1855ContagemResultado_PFCnc,
               P00YR5_A1854ContagemResultado_VlrCnc, P00YR5_n1854ContagemResultado_VlrCnc, P00YR5_A1791ContagemResultado_SemCusto, P00YR5_n1791ContagemResultado_SemCusto, P00YR5_A1790ContagemResultado_DataInicio, P00YR5_n1790ContagemResultado_DataInicio, P00YR5_A1762ContagemResultado_Entrega, P00YR5_n1762ContagemResultado_Entrega, P00YR5_A1714ContagemResultado_Combinada, P00YR5_n1714ContagemResultado_Combinada,
               P00YR5_A1636ContagemResultado_ServicoSS, P00YR5_n1636ContagemResultado_ServicoSS, P00YR5_A1587ContagemResultado_PrioridadePrevista, P00YR5_n1587ContagemResultado_PrioridadePrevista, P00YR5_A1586ContagemResultado_Restricoes, P00YR5_n1586ContagemResultado_Restricoes, P00YR5_A1585ContagemResultado_Referencia, P00YR5_n1585ContagemResultado_Referencia, P00YR5_A1584ContagemResultado_UOOwner, P00YR5_n1584ContagemResultado_UOOwner,
               P00YR5_A1583ContagemResultado_TipoRegistro, P00YR5_A1559ContagemResultado_VlrAceite, P00YR5_n1559ContagemResultado_VlrAceite, P00YR5_A1544ContagemResultado_ProjetoCod, P00YR5_n1544ContagemResultado_ProjetoCod, P00YR5_A1521ContagemResultado_FimAnl, P00YR5_n1521ContagemResultado_FimAnl, P00YR5_A1512ContagemResultado_FimCrr, P00YR5_n1512ContagemResultado_FimCrr, P00YR5_A1506ContagemResultado_TmpEstCrr,
               P00YR5_n1506ContagemResultado_TmpEstCrr, P00YR5_A1457ContagemResultado_TemDpnHmlg, P00YR5_n1457ContagemResultado_TemDpnHmlg, P00YR5_A1445ContagemResultado_CntSrvPrrCst, P00YR5_n1445ContagemResultado_CntSrvPrrCst, P00YR5_A1444ContagemResultado_CntSrvPrrPrz, P00YR5_n1444ContagemResultado_CntSrvPrrPrz, P00YR5_A1443ContagemResultado_CntSrvPrrCod, P00YR5_n1443ContagemResultado_CntSrvPrrCod, P00YR5_A1392ContagemResultado_RdmnUpdated,
               P00YR5_n1392ContagemResultado_RdmnUpdated, P00YR5_A1390ContagemResultado_RdmnProjectId, P00YR5_n1390ContagemResultado_RdmnProjectId, P00YR5_A1389ContagemResultado_RdmnIssueId, P00YR5_n1389ContagemResultado_RdmnIssueId, P00YR5_A1348ContagemResultado_DataHomologacao, P00YR5_n1348ContagemResultado_DataHomologacao, P00YR5_A1180ContagemResultado_Custo, P00YR5_n1180ContagemResultado_Custo, P00YR5_A1173ContagemResultado_OSManual,
               P00YR5_n1173ContagemResultado_OSManual, P00YR5_A1052ContagemResultado_GlsUser, P00YR5_n1052ContagemResultado_GlsUser, P00YR5_A1051ContagemResultado_GlsValor, P00YR5_n1051ContagemResultado_GlsValor, P00YR5_A1050ContagemResultado_GlsDescricao, P00YR5_n1050ContagemResultado_GlsDescricao, P00YR5_A1049ContagemResultado_GlsData, P00YR5_n1049ContagemResultado_GlsData, P00YR5_A1044ContagemResultado_FncUsrCod,
               P00YR5_n1044ContagemResultado_FncUsrCod, P00YR5_A1043ContagemResultado_LiqLogCod, P00YR5_n1043ContagemResultado_LiqLogCod, P00YR5_A799ContagemResultado_PFLFSImp, P00YR5_n799ContagemResultado_PFLFSImp, P00YR5_A798ContagemResultado_PFBFSImp, P00YR5_n798ContagemResultado_PFBFSImp, P00YR5_A598ContagemResultado_Baseline, P00YR5_n598ContagemResultado_Baseline, P00YR5_A597ContagemResultado_LoteAceiteCod,
               P00YR5_n597ContagemResultado_LoteAceiteCod, P00YR5_A592ContagemResultado_Evidencia, P00YR5_n592ContagemResultado_Evidencia, P00YR5_A514ContagemResultado_Observacao, P00YR5_n514ContagemResultado_Observacao, P00YR5_A494ContagemResultado_Descricao, P00YR5_n494ContagemResultado_Descricao, P00YR5_A485ContagemResultado_EhValidacao, P00YR5_n485ContagemResultado_EhValidacao, P00YR5_A468ContagemResultado_NaoCnfDmnCod,
               P00YR5_n468ContagemResultado_NaoCnfDmnCod, P00YR5_A465ContagemResultado_Link, P00YR5_n465ContagemResultado_Link, P00YR5_A454ContagemResultado_ContadorFSCod, P00YR5_n454ContagemResultado_ContadorFSCod, P00YR5_A456ContagemResultado_Codigo, P00YR5_A801ContagemResultado_ServicoSigla, P00YR5_n801ContagemResultado_ServicoSigla
               }
               , new Object[] {
               P00YR6_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00YR7_A1553ContagemResultado_CntSrvCod, P00YR7_n1553ContagemResultado_CntSrvCod, P00YR7_A1603ContagemResultado_CntCod, P00YR7_n1603ContagemResultado_CntCod, P00YR7_A601ContagemResultado_Servico, P00YR7_n601ContagemResultado_Servico, P00YR7_A456ContagemResultado_Codigo, P00YR7_A471ContagemResultado_DataDmn, P00YR7_A494ContagemResultado_Descricao, P00YR7_n494ContagemResultado_Descricao,
               P00YR7_A484ContagemResultado_StatusDmn, P00YR7_n484ContagemResultado_StatusDmn, P00YR7_A801ContagemResultado_ServicoSigla, P00YR7_n801ContagemResultado_ServicoSigla, P00YR7_A1612ContagemResultado_CntNum, P00YR7_n1612ContagemResultado_CntNum, P00YR7_A493ContagemResultado_DemandaFM, P00YR7_n493ContagemResultado_DemandaFM, P00YR7_A457ContagemResultado_Demanda, P00YR7_n457ContagemResultado_Demanda
               }
               , new Object[] {
               P00YR8_A1553ContagemResultado_CntSrvCod, P00YR8_n1553ContagemResultado_CntSrvCod, P00YR8_A1603ContagemResultado_CntCod, P00YR8_n1603ContagemResultado_CntCod, P00YR8_A601ContagemResultado_Servico, P00YR8_n601ContagemResultado_Servico, P00YR8_A456ContagemResultado_Codigo, P00YR8_A471ContagemResultado_DataDmn, P00YR8_A494ContagemResultado_Descricao, P00YR8_n494ContagemResultado_Descricao,
               P00YR8_A484ContagemResultado_StatusDmn, P00YR8_n484ContagemResultado_StatusDmn, P00YR8_A801ContagemResultado_ServicoSigla, P00YR8_n801ContagemResultado_ServicoSigla, P00YR8_A1612ContagemResultado_CntNum, P00YR8_n1612ContagemResultado_CntNum, P00YR8_A493ContagemResultado_DemandaFM, P00YR8_n493ContagemResultado_DemandaFM, P00YR8_A457ContagemResultado_Demanda, P00YR8_n457ContagemResultado_Demanda
               }
               , new Object[] {
               P00YR9_A489ContagemResultado_SistemaCod, P00YR9_n489ContagemResultado_SistemaCod, P00YR9_A493ContagemResultado_DemandaFM, P00YR9_n493ContagemResultado_DemandaFM, P00YR9_A490ContagemResultado_ContratadaCod, P00YR9_n490ContagemResultado_ContratadaCod, P00YR9_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00YR10_A40Contratada_PessoaCod, P00YR10_A39Contratada_Codigo, P00YR10_A41Contratada_PessoaNom, P00YR10_n41Contratada_PessoaNom
               }
               , new Object[] {
               P00YR11_A39Contratada_Codigo, P00YR11_A74Contrato_Codigo, P00YR11_A1013Contrato_PrepostoCod, P00YR11_n1013Contrato_PrepostoCod
               }
               , new Object[] {
               P00YR13_A40000Usuario_PessoaNom
               }
               , new Object[] {
               P00YR14_A29Contratante_Codigo, P00YR14_n29Contratante_Codigo, P00YR14_A548Contratante_EmailSdaUser, P00YR14_n548Contratante_EmailSdaUser, P00YR14_A552Contratante_EmailSdaPort, P00YR14_n552Contratante_EmailSdaPort, P00YR14_A547Contratante_EmailSdaHost, P00YR14_n547Contratante_EmailSdaHost, P00YR14_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               P00YR15_A74Contrato_Codigo, P00YR15_A1013Contrato_PrepostoCod, P00YR15_n1013Contrato_PrepostoCod
               }
               , new Object[] {
               P00YR16_A1078ContratoGestor_ContratoCod, P00YR16_A1079ContratoGestor_UsuarioCod, P00YR16_A1446ContratoGestor_ContratadaAreaCod, P00YR16_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               P00YR17_A1078ContratoGestor_ContratoCod, P00YR17_A1079ContratoGestor_UsuarioCod, P00YR17_A1446ContratoGestor_ContratadaAreaCod, P00YR17_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               P00YR19_A1553ContagemResultado_CntSrvCod, P00YR19_n1553ContagemResultado_CntSrvCod, P00YR19_A601ContagemResultado_Servico, P00YR19_n601ContagemResultado_Servico, P00YR19_A508ContagemResultado_Owner, P00YR19_A456ContagemResultado_Codigo, P00YR19_A493ContagemResultado_DemandaFM, P00YR19_n493ContagemResultado_DemandaFM, P00YR19_A801ContagemResultado_ServicoSigla, P00YR19_n801ContagemResultado_ServicoSigla,
               P00YR19_A494ContagemResultado_Descricao, P00YR19_n494ContagemResultado_Descricao, P00YR19_A471ContagemResultado_DataDmn, P00YR19_A472ContagemResultado_DataEntrega, P00YR19_n472ContagemResultado_DataEntrega, P00YR19_A40001Usuario_PessoaNom, P00YR19_n40001Usuario_PessoaNom
               }
               , new Object[] {
               P00YR20_A456ContagemResultado_Codigo, P00YR20_A602ContagemResultado_OSVinculada, P00YR20_n602ContagemResultado_OSVinculada, P00YR20_A1046ContagemResultado_Agrupador, P00YR20_n1046ContagemResultado_Agrupador, P00YR20_A1515ContagemResultado_Evento, P00YR20_n1515ContagemResultado_Evento
               }
               , new Object[] {
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         Gx_time = context.localUtil.Time( );
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_date = DateTimeUtil.Today( context);
         Gx_time = context.localUtil.Time( );
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV185IsModificarAgrupador ;
      private short GxWebError ;
      private short A1224ContratoServicos_PrazoCorrecao ;
      private short A1649ContratoServicos_PrazoInicio ;
      private short A1152ContratoServicos_PrazoAnalise ;
      private short OV106PrazoInicio ;
      private short AV106PrazoInicio ;
      private short AV102PrazoCoreecao ;
      private short AV66DiasParaAnalise ;
      private short A1515ContagemResultado_Evento ;
      private short A1237ContagemResultado_PrazoMaisDias ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short A1762ContagemResultado_Entrega ;
      private short A1583ContagemResultado_TipoRegistro ;
      private short W1227ContagemResultado_PrazoInicialDias ;
      private short W1237ContagemResultado_PrazoMaisDias ;
      private short W1515ContagemResultado_Evento ;
      private short GXt_int2 ;
      private short AV156TotalDemandas ;
      private short A552Contratante_EmailSdaPort ;
      private int AV54Contratada_Codigo ;
      private int AV130Contrato_Codigo ;
      private int AV57ContratoServicos_Codigo ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A160ContratoServicos_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A601ContagemResultado_Servico ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1519ContagemResultado_TmpEstAnl ;
      private int A1505ContagemResultado_TmpEstExc ;
      private int A1452ContagemResultado_SS ;
      private int A890ContagemResultado_Responsavel ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int A508ContagemResultado_Owner ;
      private int A146Modulo_Codigo ;
      private int A489ContagemResultado_SistemaCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A1584ContagemResultado_UOOwner ;
      private int A1544ContagemResultado_ProjetoCod ;
      private int A1506ContagemResultado_TmpEstCrr ;
      private int A1443ContagemResultado_CntSrvPrrCod ;
      private int A1390ContagemResultado_RdmnProjectId ;
      private int A1389ContagemResultado_RdmnIssueId ;
      private int A1052ContagemResultado_GlsUser ;
      private int A1044ContagemResultado_FncUsrCod ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A454ContagemResultado_ContadorFSCod ;
      private int AV41ContagemResultado_Codigo ;
      private int AV147ContagemResultado_Responsavel ;
      private int AV148ContagemResultado_SistemaCod ;
      private int AV149Modulo_Codigo ;
      private int AV45ContagemResultado_ContratadaCod ;
      private int GX_INS69 ;
      private int W490ContagemResultado_ContratadaCod ;
      private int W1553ContagemResultado_CntSrvCod ;
      private int W602ContagemResultado_OSVinculada ;
      private int W1452ContagemResultado_SS ;
      private int W1505ContagemResultado_TmpEstExc ;
      private int W1519ContagemResultado_TmpEstAnl ;
      private int W805ContagemResultado_ContratadaOrigemCod ;
      private int W890ContagemResultado_Responsavel ;
      private int W508ContagemResultado_Owner ;
      private int W489ContagemResultado_SistemaCod ;
      private int W146Modulo_Codigo ;
      private int AV155Contrato_PrepostoCod ;
      private int AV144NewCodigo ;
      private int Gx_OldLine ;
      private int A1603ContagemResultado_CntCod ;
      private int A1013Contrato_PrepostoCod ;
      private int AV167ContagemResultado_Owner ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private decimal AV40ContagemResltado_Deflator ;
      private decimal A558Servico_Percentual ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private decimal AV123ValorPFFS ;
      private decimal AV53ContagemResultado_ValorPF ;
      private decimal A2133ContagemResultado_QuantidadeSolicitada ;
      private decimal A1179ContagemResultado_PLFinal ;
      private decimal A1178ContagemResultado_PBFinal ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A1855ContagemResultado_PFCnc ;
      private decimal A1854ContagemResultado_VlrCnc ;
      private decimal A1559ContagemResultado_VlrAceite ;
      private decimal A1445ContagemResultado_CntSrvPrrCst ;
      private decimal A1444ContagemResultado_CntSrvPrrPrz ;
      private decimal A1180ContagemResultado_Custo ;
      private decimal A1051ContagemResultado_GlsValor ;
      private decimal A799ContagemResultado_PFLFSImp ;
      private decimal A798ContagemResultado_PFBFSImp ;
      private decimal W512ContagemResultado_ValorPF ;
      private decimal W1178ContagemResultado_PBFinal ;
      private decimal W1179ContagemResultado_PLFinal ;
      private decimal W2133ContagemResultado_QuantidadeSolicitada ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV187ContagemResultado_AgrupadorAux ;
      private String scmdbuf ;
      private String A41Contratada_PessoaNom ;
      private String A1225ContratoServicos_PrazoCorrecaoTipo ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String AV56ContratadaFS_Nome ;
      private String AV103PrazoCorrecaoTipo ;
      private String AV199TipoDias ;
      private String A1046ContagemResultado_Agrupador ;
      private String A484ContagemResultado_StatusDmn ;
      private String A801ContagemResultado_ServicoSigla ;
      private String AV128ContagemResultado_Agrupador ;
      private String AV158ContagemResultado_ServicoSigla ;
      private String W484ContagemResultado_StatusDmn ;
      private String W1046ContagemResultado_Agrupador ;
      private String Gx_emsg ;
      private String AV114Status ;
      private String AV182ContagemResultado_CntNum ;
      private String A1612ContagemResultado_CntNum ;
      private String AV154Contratada_PessoaNom ;
      private String A40000Usuario_PessoaNom ;
      private String AV164ContagemResultado_Owner_Nome ;
      private String AV109Resultado ;
      private String AV166EmailText ;
      private String AV117Subject ;
      private String A40001Usuario_PessoaNom ;
      private String Gx_time ;
      private DateTime AV110ServerNow ;
      private DateTime A1192Contratante_FimDoExpediente ;
      private DateTime AV76FimDoExpediente ;
      private DateTime A2017ContagemResultado_DataEntregaReal ;
      private DateTime A1520ContagemResultado_InicioAnl ;
      private DateTime A1511ContagemResultado_InicioCrr ;
      private DateTime A1510ContagemResultado_FimExc ;
      private DateTime A1509ContagemResultado_InicioExc ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A1350ContagemResultado_DataCadastro ;
      private DateTime A1349ContagemResultado_DataExecucao ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A1521ContagemResultado_FimAnl ;
      private DateTime A1512ContagemResultado_FimCrr ;
      private DateTime A1392ContagemResultado_RdmnUpdated ;
      private DateTime A1348ContagemResultado_DataHomologacao ;
      private DateTime W1351ContagemResultado_DataPrevista ;
      private DateTime W912ContagemResultado_HoraEntrega ;
      private DateTime W2017ContagemResultado_DataEntregaReal ;
      private DateTime W1509ContagemResultado_InicioExc ;
      private DateTime W1511ContagemResultado_InicioCrr ;
      private DateTime W1510ContagemResultado_FimExc ;
      private DateTime W1520ContagemResultado_InicioAnl ;
      private DateTime W1350ContagemResultado_DataCadastro ;
      private DateTime W1349ContagemResultado_DataExecucao ;
      private DateTime GXt_dtime1 ;
      private DateTime AV189PrazoDemanda ;
      private DateTime AV170ServerDate ;
      private DateTime A1903ContagemResultado_DataPrvPgm ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A1790ContagemResultado_DataInicio ;
      private DateTime A1049ContagemResultado_GlsData ;
      private DateTime W472ContagemResultado_DataEntrega ;
      private DateTime W1903ContagemResultado_DataPrvPgm ;
      private DateTime W471ContagemResultado_DataDmn ;
      private DateTime AV179ContagemResultado_DataDmn ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool returnInSub ;
      private bool n41Contratada_PessoaNom ;
      private bool n558Servico_Percentual ;
      private bool n1224ContratoServicos_PrazoCorrecao ;
      private bool n1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool n1649ContratoServicos_PrazoInicio ;
      private bool n1152ContratoServicos_PrazoAnalise ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private bool n29Contratante_Codigo ;
      private bool n1192Contratante_FimDoExpediente ;
      private bool n601ContagemResultado_Servico ;
      private bool n2133ContagemResultado_QuantidadeSolicitada ;
      private bool n2017ContagemResultado_DataEntregaReal ;
      private bool n1903ContagemResultado_DataPrvPgm ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1520ContagemResultado_InicioAnl ;
      private bool n1519ContagemResultado_TmpEstAnl ;
      private bool n1515ContagemResultado_Evento ;
      private bool n1511ContagemResultado_InicioCrr ;
      private bool n1510ContagemResultado_FimExc ;
      private bool n1509ContagemResultado_InicioExc ;
      private bool n1505ContagemResultado_TmpEstExc ;
      private bool n1452ContagemResultado_SS ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n1350ContagemResultado_DataCadastro ;
      private bool n1349ContagemResultado_DataExecucao ;
      private bool n1237ContagemResultado_PrazoMaisDias ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n1179ContagemResultado_PLFinal ;
      private bool n1178ContagemResultado_PBFinal ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n146Modulo_Codigo ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n457ContagemResultado_Demanda ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n1855ContagemResultado_PFCnc ;
      private bool n1854ContagemResultado_VlrCnc ;
      private bool A1791ContagemResultado_SemCusto ;
      private bool n1791ContagemResultado_SemCusto ;
      private bool n1790ContagemResultado_DataInicio ;
      private bool n1762ContagemResultado_Entrega ;
      private bool A1714ContagemResultado_Combinada ;
      private bool n1714ContagemResultado_Combinada ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool n1587ContagemResultado_PrioridadePrevista ;
      private bool n1586ContagemResultado_Restricoes ;
      private bool n1585ContagemResultado_Referencia ;
      private bool n1584ContagemResultado_UOOwner ;
      private bool n1559ContagemResultado_VlrAceite ;
      private bool n1544ContagemResultado_ProjetoCod ;
      private bool n1521ContagemResultado_FimAnl ;
      private bool n1512ContagemResultado_FimCrr ;
      private bool n1506ContagemResultado_TmpEstCrr ;
      private bool A1457ContagemResultado_TemDpnHmlg ;
      private bool n1457ContagemResultado_TemDpnHmlg ;
      private bool n1445ContagemResultado_CntSrvPrrCst ;
      private bool n1444ContagemResultado_CntSrvPrrPrz ;
      private bool n1443ContagemResultado_CntSrvPrrCod ;
      private bool n1392ContagemResultado_RdmnUpdated ;
      private bool n1390ContagemResultado_RdmnProjectId ;
      private bool n1389ContagemResultado_RdmnIssueId ;
      private bool n1348ContagemResultado_DataHomologacao ;
      private bool n1180ContagemResultado_Custo ;
      private bool A1173ContagemResultado_OSManual ;
      private bool n1173ContagemResultado_OSManual ;
      private bool n1052ContagemResultado_GlsUser ;
      private bool n1051ContagemResultado_GlsValor ;
      private bool n1050ContagemResultado_GlsDescricao ;
      private bool n1049ContagemResultado_GlsData ;
      private bool n1044ContagemResultado_FncUsrCod ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool n799ContagemResultado_PFLFSImp ;
      private bool n798ContagemResultado_PFBFSImp ;
      private bool A598ContagemResultado_Baseline ;
      private bool n598ContagemResultado_Baseline ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n592ContagemResultado_Evidencia ;
      private bool n514ContagemResultado_Observacao ;
      private bool n494ContagemResultado_Descricao ;
      private bool A485ContagemResultado_EhValidacao ;
      private bool n485ContagemResultado_EhValidacao ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n465ContagemResultado_Link ;
      private bool n454ContagemResultado_ContadorFSCod ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool AV177IsDemandaCadastrada ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1612ContagemResultado_CntNum ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n552Contratante_EmailSdaPort ;
      private bool n547Contratante_EmailSdaHost ;
      private bool AV157ContratanteSemEmailSda ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool GXt_boolean3 ;
      private bool n40001Usuario_PessoaNom ;
      private String A1050ContagemResultado_GlsDescricao ;
      private String A592ContagemResultado_Evidencia ;
      private String A514ContagemResultado_Observacao ;
      private String A465ContagemResultado_Link ;
      private String A493ContagemResultado_DemandaFM ;
      private String A457ContagemResultado_Demanda ;
      private String A1587ContagemResultado_PrioridadePrevista ;
      private String A1586ContagemResultado_Restricoes ;
      private String A1585ContagemResultado_Referencia ;
      private String A494ContagemResultado_Descricao ;
      private String AV175ContagemResultado_DemandaFM ;
      private String AV146ContagemResultado_Demanda ;
      private String W457ContagemResultado_Demanda ;
      private String W493ContagemResultado_DemandaFM ;
      private String AV183IdentificacaoListaDemandas ;
      private String AV180ContagemResultado_OsFsOsFm ;
      private String AV181ContagemResultado_Descricao ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String AV153StatusDmn ;
      private String A548Contratante_EmailSdaUser ;
      private String A547Contratante_EmailSdaHost ;
      private IGxSession AV125WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00YR2_A74Contrato_Codigo ;
      private int[] P00YR2_A39Contratada_Codigo ;
      private int[] P00YR2_A40Contratada_PessoaCod ;
      private int[] P00YR2_A52Contratada_AreaTrabalhoCod ;
      private int[] P00YR2_A160ContratoServicos_Codigo ;
      private String[] P00YR2_A41Contratada_PessoaNom ;
      private bool[] P00YR2_n41Contratada_PessoaNom ;
      private decimal[] P00YR2_A558Servico_Percentual ;
      private bool[] P00YR2_n558Servico_Percentual ;
      private decimal[] P00YR2_A116Contrato_ValorUnidadeContratacao ;
      private short[] P00YR2_A1224ContratoServicos_PrazoCorrecao ;
      private bool[] P00YR2_n1224ContratoServicos_PrazoCorrecao ;
      private String[] P00YR2_A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool[] P00YR2_n1225ContratoServicos_PrazoCorrecaoTipo ;
      private short[] P00YR2_A1649ContratoServicos_PrazoInicio ;
      private bool[] P00YR2_n1649ContratoServicos_PrazoInicio ;
      private short[] P00YR2_A1152ContratoServicos_PrazoAnalise ;
      private bool[] P00YR2_n1152ContratoServicos_PrazoAnalise ;
      private decimal[] P00YR2_A557Servico_VlrUnidadeContratada ;
      private String[] P00YR2_A1454ContratoServicos_PrazoTpDias ;
      private bool[] P00YR2_n1454ContratoServicos_PrazoTpDias ;
      private int[] P00YR3_A5AreaTrabalho_Codigo ;
      private int[] P00YR3_A29Contratante_Codigo ;
      private bool[] P00YR3_n29Contratante_Codigo ;
      private DateTime[] P00YR3_A1192Contratante_FimDoExpediente ;
      private bool[] P00YR3_n1192Contratante_FimDoExpediente ;
      private int[] P00YR4_A29Contratante_Codigo ;
      private bool[] P00YR4_n29Contratante_Codigo ;
      private int[] P00YR5_A601ContagemResultado_Servico ;
      private bool[] P00YR5_n601ContagemResultado_Servico ;
      private decimal[] P00YR5_A2133ContagemResultado_QuantidadeSolicitada ;
      private bool[] P00YR5_n2133ContagemResultado_QuantidadeSolicitada ;
      private DateTime[] P00YR5_A2017ContagemResultado_DataEntregaReal ;
      private bool[] P00YR5_n2017ContagemResultado_DataEntregaReal ;
      private DateTime[] P00YR5_A1903ContagemResultado_DataPrvPgm ;
      private bool[] P00YR5_n1903ContagemResultado_DataPrvPgm ;
      private int[] P00YR5_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YR5_n1553ContagemResultado_CntSrvCod ;
      private DateTime[] P00YR5_A1520ContagemResultado_InicioAnl ;
      private bool[] P00YR5_n1520ContagemResultado_InicioAnl ;
      private int[] P00YR5_A1519ContagemResultado_TmpEstAnl ;
      private bool[] P00YR5_n1519ContagemResultado_TmpEstAnl ;
      private short[] P00YR5_A1515ContagemResultado_Evento ;
      private bool[] P00YR5_n1515ContagemResultado_Evento ;
      private DateTime[] P00YR5_A1511ContagemResultado_InicioCrr ;
      private bool[] P00YR5_n1511ContagemResultado_InicioCrr ;
      private DateTime[] P00YR5_A1510ContagemResultado_FimExc ;
      private bool[] P00YR5_n1510ContagemResultado_FimExc ;
      private DateTime[] P00YR5_A1509ContagemResultado_InicioExc ;
      private bool[] P00YR5_n1509ContagemResultado_InicioExc ;
      private int[] P00YR5_A1505ContagemResultado_TmpEstExc ;
      private bool[] P00YR5_n1505ContagemResultado_TmpEstExc ;
      private int[] P00YR5_A1452ContagemResultado_SS ;
      private bool[] P00YR5_n1452ContagemResultado_SS ;
      private DateTime[] P00YR5_A1351ContagemResultado_DataPrevista ;
      private bool[] P00YR5_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00YR5_A1350ContagemResultado_DataCadastro ;
      private bool[] P00YR5_n1350ContagemResultado_DataCadastro ;
      private DateTime[] P00YR5_A1349ContagemResultado_DataExecucao ;
      private bool[] P00YR5_n1349ContagemResultado_DataExecucao ;
      private short[] P00YR5_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P00YR5_n1237ContagemResultado_PrazoMaisDias ;
      private short[] P00YR5_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P00YR5_n1227ContagemResultado_PrazoInicialDias ;
      private decimal[] P00YR5_A1179ContagemResultado_PLFinal ;
      private bool[] P00YR5_n1179ContagemResultado_PLFinal ;
      private decimal[] P00YR5_A1178ContagemResultado_PBFinal ;
      private bool[] P00YR5_n1178ContagemResultado_PBFinal ;
      private String[] P00YR5_A1046ContagemResultado_Agrupador ;
      private bool[] P00YR5_n1046ContagemResultado_Agrupador ;
      private DateTime[] P00YR5_A912ContagemResultado_HoraEntrega ;
      private bool[] P00YR5_n912ContagemResultado_HoraEntrega ;
      private int[] P00YR5_A890ContagemResultado_Responsavel ;
      private bool[] P00YR5_n890ContagemResultado_Responsavel ;
      private int[] P00YR5_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P00YR5_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] P00YR5_A602ContagemResultado_OSVinculada ;
      private bool[] P00YR5_n602ContagemResultado_OSVinculada ;
      private decimal[] P00YR5_A512ContagemResultado_ValorPF ;
      private bool[] P00YR5_n512ContagemResultado_ValorPF ;
      private int[] P00YR5_A508ContagemResultado_Owner ;
      private int[] P00YR5_A146Modulo_Codigo ;
      private bool[] P00YR5_n146Modulo_Codigo ;
      private int[] P00YR5_A489ContagemResultado_SistemaCod ;
      private bool[] P00YR5_n489ContagemResultado_SistemaCod ;
      private String[] P00YR5_A493ContagemResultado_DemandaFM ;
      private bool[] P00YR5_n493ContagemResultado_DemandaFM ;
      private int[] P00YR5_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YR5_n490ContagemResultado_ContratadaCod ;
      private String[] P00YR5_A484ContagemResultado_StatusDmn ;
      private bool[] P00YR5_n484ContagemResultado_StatusDmn ;
      private String[] P00YR5_A457ContagemResultado_Demanda ;
      private bool[] P00YR5_n457ContagemResultado_Demanda ;
      private DateTime[] P00YR5_A472ContagemResultado_DataEntrega ;
      private bool[] P00YR5_n472ContagemResultado_DataEntrega ;
      private DateTime[] P00YR5_A471ContagemResultado_DataDmn ;
      private decimal[] P00YR5_A1855ContagemResultado_PFCnc ;
      private bool[] P00YR5_n1855ContagemResultado_PFCnc ;
      private decimal[] P00YR5_A1854ContagemResultado_VlrCnc ;
      private bool[] P00YR5_n1854ContagemResultado_VlrCnc ;
      private bool[] P00YR5_A1791ContagemResultado_SemCusto ;
      private bool[] P00YR5_n1791ContagemResultado_SemCusto ;
      private DateTime[] P00YR5_A1790ContagemResultado_DataInicio ;
      private bool[] P00YR5_n1790ContagemResultado_DataInicio ;
      private short[] P00YR5_A1762ContagemResultado_Entrega ;
      private bool[] P00YR5_n1762ContagemResultado_Entrega ;
      private bool[] P00YR5_A1714ContagemResultado_Combinada ;
      private bool[] P00YR5_n1714ContagemResultado_Combinada ;
      private int[] P00YR5_A1636ContagemResultado_ServicoSS ;
      private bool[] P00YR5_n1636ContagemResultado_ServicoSS ;
      private String[] P00YR5_A1587ContagemResultado_PrioridadePrevista ;
      private bool[] P00YR5_n1587ContagemResultado_PrioridadePrevista ;
      private String[] P00YR5_A1586ContagemResultado_Restricoes ;
      private bool[] P00YR5_n1586ContagemResultado_Restricoes ;
      private String[] P00YR5_A1585ContagemResultado_Referencia ;
      private bool[] P00YR5_n1585ContagemResultado_Referencia ;
      private int[] P00YR5_A1584ContagemResultado_UOOwner ;
      private bool[] P00YR5_n1584ContagemResultado_UOOwner ;
      private short[] P00YR5_A1583ContagemResultado_TipoRegistro ;
      private decimal[] P00YR5_A1559ContagemResultado_VlrAceite ;
      private bool[] P00YR5_n1559ContagemResultado_VlrAceite ;
      private int[] P00YR5_A1544ContagemResultado_ProjetoCod ;
      private bool[] P00YR5_n1544ContagemResultado_ProjetoCod ;
      private DateTime[] P00YR5_A1521ContagemResultado_FimAnl ;
      private bool[] P00YR5_n1521ContagemResultado_FimAnl ;
      private DateTime[] P00YR5_A1512ContagemResultado_FimCrr ;
      private bool[] P00YR5_n1512ContagemResultado_FimCrr ;
      private int[] P00YR5_A1506ContagemResultado_TmpEstCrr ;
      private bool[] P00YR5_n1506ContagemResultado_TmpEstCrr ;
      private bool[] P00YR5_A1457ContagemResultado_TemDpnHmlg ;
      private bool[] P00YR5_n1457ContagemResultado_TemDpnHmlg ;
      private decimal[] P00YR5_A1445ContagemResultado_CntSrvPrrCst ;
      private bool[] P00YR5_n1445ContagemResultado_CntSrvPrrCst ;
      private decimal[] P00YR5_A1444ContagemResultado_CntSrvPrrPrz ;
      private bool[] P00YR5_n1444ContagemResultado_CntSrvPrrPrz ;
      private int[] P00YR5_A1443ContagemResultado_CntSrvPrrCod ;
      private bool[] P00YR5_n1443ContagemResultado_CntSrvPrrCod ;
      private DateTime[] P00YR5_A1392ContagemResultado_RdmnUpdated ;
      private bool[] P00YR5_n1392ContagemResultado_RdmnUpdated ;
      private int[] P00YR5_A1390ContagemResultado_RdmnProjectId ;
      private bool[] P00YR5_n1390ContagemResultado_RdmnProjectId ;
      private int[] P00YR5_A1389ContagemResultado_RdmnIssueId ;
      private bool[] P00YR5_n1389ContagemResultado_RdmnIssueId ;
      private DateTime[] P00YR5_A1348ContagemResultado_DataHomologacao ;
      private bool[] P00YR5_n1348ContagemResultado_DataHomologacao ;
      private decimal[] P00YR5_A1180ContagemResultado_Custo ;
      private bool[] P00YR5_n1180ContagemResultado_Custo ;
      private bool[] P00YR5_A1173ContagemResultado_OSManual ;
      private bool[] P00YR5_n1173ContagemResultado_OSManual ;
      private int[] P00YR5_A1052ContagemResultado_GlsUser ;
      private bool[] P00YR5_n1052ContagemResultado_GlsUser ;
      private decimal[] P00YR5_A1051ContagemResultado_GlsValor ;
      private bool[] P00YR5_n1051ContagemResultado_GlsValor ;
      private String[] P00YR5_A1050ContagemResultado_GlsDescricao ;
      private bool[] P00YR5_n1050ContagemResultado_GlsDescricao ;
      private DateTime[] P00YR5_A1049ContagemResultado_GlsData ;
      private bool[] P00YR5_n1049ContagemResultado_GlsData ;
      private int[] P00YR5_A1044ContagemResultado_FncUsrCod ;
      private bool[] P00YR5_n1044ContagemResultado_FncUsrCod ;
      private int[] P00YR5_A1043ContagemResultado_LiqLogCod ;
      private bool[] P00YR5_n1043ContagemResultado_LiqLogCod ;
      private decimal[] P00YR5_A799ContagemResultado_PFLFSImp ;
      private bool[] P00YR5_n799ContagemResultado_PFLFSImp ;
      private decimal[] P00YR5_A798ContagemResultado_PFBFSImp ;
      private bool[] P00YR5_n798ContagemResultado_PFBFSImp ;
      private bool[] P00YR5_A598ContagemResultado_Baseline ;
      private bool[] P00YR5_n598ContagemResultado_Baseline ;
      private int[] P00YR5_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00YR5_n597ContagemResultado_LoteAceiteCod ;
      private String[] P00YR5_A592ContagemResultado_Evidencia ;
      private bool[] P00YR5_n592ContagemResultado_Evidencia ;
      private String[] P00YR5_A514ContagemResultado_Observacao ;
      private bool[] P00YR5_n514ContagemResultado_Observacao ;
      private String[] P00YR5_A494ContagemResultado_Descricao ;
      private bool[] P00YR5_n494ContagemResultado_Descricao ;
      private bool[] P00YR5_A485ContagemResultado_EhValidacao ;
      private bool[] P00YR5_n485ContagemResultado_EhValidacao ;
      private int[] P00YR5_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P00YR5_n468ContagemResultado_NaoCnfDmnCod ;
      private String[] P00YR5_A465ContagemResultado_Link ;
      private bool[] P00YR5_n465ContagemResultado_Link ;
      private int[] P00YR5_A454ContagemResultado_ContadorFSCod ;
      private bool[] P00YR5_n454ContagemResultado_ContadorFSCod ;
      private int[] P00YR5_A456ContagemResultado_Codigo ;
      private String[] P00YR5_A801ContagemResultado_ServicoSigla ;
      private bool[] P00YR5_n801ContagemResultado_ServicoSigla ;
      private int[] P00YR6_A456ContagemResultado_Codigo ;
      private int[] P00YR7_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YR7_n1553ContagemResultado_CntSrvCod ;
      private int[] P00YR7_A1603ContagemResultado_CntCod ;
      private bool[] P00YR7_n1603ContagemResultado_CntCod ;
      private int[] P00YR7_A601ContagemResultado_Servico ;
      private bool[] P00YR7_n601ContagemResultado_Servico ;
      private int[] P00YR7_A456ContagemResultado_Codigo ;
      private DateTime[] P00YR7_A471ContagemResultado_DataDmn ;
      private String[] P00YR7_A494ContagemResultado_Descricao ;
      private bool[] P00YR7_n494ContagemResultado_Descricao ;
      private String[] P00YR7_A484ContagemResultado_StatusDmn ;
      private bool[] P00YR7_n484ContagemResultado_StatusDmn ;
      private String[] P00YR7_A801ContagemResultado_ServicoSigla ;
      private bool[] P00YR7_n801ContagemResultado_ServicoSigla ;
      private String[] P00YR7_A1612ContagemResultado_CntNum ;
      private bool[] P00YR7_n1612ContagemResultado_CntNum ;
      private String[] P00YR7_A493ContagemResultado_DemandaFM ;
      private bool[] P00YR7_n493ContagemResultado_DemandaFM ;
      private String[] P00YR7_A457ContagemResultado_Demanda ;
      private bool[] P00YR7_n457ContagemResultado_Demanda ;
      private int[] P00YR8_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YR8_n1553ContagemResultado_CntSrvCod ;
      private int[] P00YR8_A1603ContagemResultado_CntCod ;
      private bool[] P00YR8_n1603ContagemResultado_CntCod ;
      private int[] P00YR8_A601ContagemResultado_Servico ;
      private bool[] P00YR8_n601ContagemResultado_Servico ;
      private int[] P00YR8_A456ContagemResultado_Codigo ;
      private DateTime[] P00YR8_A471ContagemResultado_DataDmn ;
      private String[] P00YR8_A494ContagemResultado_Descricao ;
      private bool[] P00YR8_n494ContagemResultado_Descricao ;
      private String[] P00YR8_A484ContagemResultado_StatusDmn ;
      private bool[] P00YR8_n484ContagemResultado_StatusDmn ;
      private String[] P00YR8_A801ContagemResultado_ServicoSigla ;
      private bool[] P00YR8_n801ContagemResultado_ServicoSigla ;
      private String[] P00YR8_A1612ContagemResultado_CntNum ;
      private bool[] P00YR8_n1612ContagemResultado_CntNum ;
      private String[] P00YR8_A493ContagemResultado_DemandaFM ;
      private bool[] P00YR8_n493ContagemResultado_DemandaFM ;
      private String[] P00YR8_A457ContagemResultado_Demanda ;
      private bool[] P00YR8_n457ContagemResultado_Demanda ;
      private int[] P00YR9_A489ContagemResultado_SistemaCod ;
      private bool[] P00YR9_n489ContagemResultado_SistemaCod ;
      private String[] P00YR9_A493ContagemResultado_DemandaFM ;
      private bool[] P00YR9_n493ContagemResultado_DemandaFM ;
      private int[] P00YR9_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YR9_n490ContagemResultado_ContratadaCod ;
      private int[] P00YR9_A456ContagemResultado_Codigo ;
      private int[] P00YR10_A40Contratada_PessoaCod ;
      private int[] P00YR10_A39Contratada_Codigo ;
      private String[] P00YR10_A41Contratada_PessoaNom ;
      private bool[] P00YR10_n41Contratada_PessoaNom ;
      private int[] P00YR11_A39Contratada_Codigo ;
      private int[] P00YR11_A74Contrato_Codigo ;
      private int[] P00YR11_A1013Contrato_PrepostoCod ;
      private bool[] P00YR11_n1013Contrato_PrepostoCod ;
      private String[] P00YR13_A40000Usuario_PessoaNom ;
      private int[] P00YR14_A29Contratante_Codigo ;
      private bool[] P00YR14_n29Contratante_Codigo ;
      private String[] P00YR14_A548Contratante_EmailSdaUser ;
      private bool[] P00YR14_n548Contratante_EmailSdaUser ;
      private short[] P00YR14_A552Contratante_EmailSdaPort ;
      private bool[] P00YR14_n552Contratante_EmailSdaPort ;
      private String[] P00YR14_A547Contratante_EmailSdaHost ;
      private bool[] P00YR14_n547Contratante_EmailSdaHost ;
      private int[] P00YR14_A5AreaTrabalho_Codigo ;
      private int[] P00YR15_A74Contrato_Codigo ;
      private int[] P00YR15_A1013Contrato_PrepostoCod ;
      private bool[] P00YR15_n1013Contrato_PrepostoCod ;
      private int[] P00YR16_A1078ContratoGestor_ContratoCod ;
      private int[] P00YR16_A1079ContratoGestor_UsuarioCod ;
      private int[] P00YR16_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00YR16_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] P00YR17_A1078ContratoGestor_ContratoCod ;
      private int[] P00YR17_A1079ContratoGestor_UsuarioCod ;
      private int[] P00YR17_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00YR17_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] P00YR19_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YR19_n1553ContagemResultado_CntSrvCod ;
      private int[] P00YR19_A601ContagemResultado_Servico ;
      private bool[] P00YR19_n601ContagemResultado_Servico ;
      private int[] P00YR19_A508ContagemResultado_Owner ;
      private int[] P00YR19_A456ContagemResultado_Codigo ;
      private String[] P00YR19_A493ContagemResultado_DemandaFM ;
      private bool[] P00YR19_n493ContagemResultado_DemandaFM ;
      private String[] P00YR19_A801ContagemResultado_ServicoSigla ;
      private bool[] P00YR19_n801ContagemResultado_ServicoSigla ;
      private String[] P00YR19_A494ContagemResultado_Descricao ;
      private bool[] P00YR19_n494ContagemResultado_Descricao ;
      private DateTime[] P00YR19_A471ContagemResultado_DataDmn ;
      private DateTime[] P00YR19_A472ContagemResultado_DataEntrega ;
      private bool[] P00YR19_n472ContagemResultado_DataEntrega ;
      private String[] P00YR19_A40001Usuario_PessoaNom ;
      private bool[] P00YR19_n40001Usuario_PessoaNom ;
      private int[] P00YR20_A456ContagemResultado_Codigo ;
      private int[] P00YR20_A602ContagemResultado_OSVinculada ;
      private bool[] P00YR20_n602ContagemResultado_OSVinculada ;
      private String[] P00YR20_A1046ContagemResultado_Agrupador ;
      private bool[] P00YR20_n1046ContagemResultado_Agrupador ;
      private short[] P00YR20_A1515ContagemResultado_Evento ;
      private bool[] P00YR20_n1515ContagemResultado_Evento ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV132ContagemResultado_CodigoAux ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV159Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV178CodigosNaoGeradas ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV122Usuarios ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV14Attachments ;
      private wwpbaseobjects.SdtWWPContext AV126WWPContext ;
   }

   public class aprc_demandasvinculadas__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00YR5( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV132ContagemResultado_CodigoAux )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_QuantidadeSolicitada], T1.[ContagemResultado_DataEntregaReal], T1.[ContagemResultado_DataPrvPgm], T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_InicioAnl], T1.[ContagemResultado_TmpEstAnl], T1.[ContagemResultado_Evento], T1.[ContagemResultado_InicioCrr], T1.[ContagemResultado_FimExc], T1.[ContagemResultado_InicioExc], T1.[ContagemResultado_TmpEstExc], T1.[ContagemResultado_SS], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataCadastro], T1.[ContagemResultado_DataExecucao], T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_PLFinal], T1.[ContagemResultado_PBFinal], T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_ContratadaOrigemCod], T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_ValorPF], T1.[ContagemResultado_Owner], T1.[Modulo_Codigo], T1.[ContagemResultado_SistemaCod], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_PFCnc], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_SemCusto], T1.[ContagemResultado_DataInicio], T1.[ContagemResultado_Entrega], T1.[ContagemResultado_Combinada], T1.[ContagemResultado_ServicoSS], T1.[ContagemResultado_PrioridadePrevista], T1.[ContagemResultado_Restricoes], T1.[ContagemResultado_Referencia], T1.[ContagemResultado_UOOwner], T1.[ContagemResultado_TipoRegistro], T1.[ContagemResultado_VlrAceite], T1.[ContagemResultado_ProjetoCod], T1.[ContagemResultado_FimAnl], T1.[ContagemResultado_FimCrr],";
         scmdbuf = scmdbuf + " T1.[ContagemResultado_TmpEstCrr], T1.[ContagemResultado_TemDpnHmlg], T1.[ContagemResultado_CntSrvPrrCst], T1.[ContagemResultado_CntSrvPrrPrz], T1.[ContagemResultado_CntSrvPrrCod], T1.[ContagemResultado_RdmnUpdated], T1.[ContagemResultado_RdmnProjectId], T1.[ContagemResultado_RdmnIssueId], T1.[ContagemResultado_DataHomologacao], T1.[ContagemResultado_Custo], T1.[ContagemResultado_OSManual], T1.[ContagemResultado_GlsUser], T1.[ContagemResultado_GlsValor], T1.[ContagemResultado_GlsDescricao], T1.[ContagemResultado_GlsData], T1.[ContagemResultado_FncUsrCod], T1.[ContagemResultado_LiqLogCod], T1.[ContagemResultado_PFLFSImp], T1.[ContagemResultado_PFBFSImp], T1.[ContagemResultado_Baseline], T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_Evidencia], T1.[ContagemResultado_Observacao], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_EhValidacao], T1.[ContagemResultado_NaoCnfDmnCod], T1.[ContagemResultado_Link], T1.[ContagemResultado_ContadorFSCod], T1.[ContagemResultado_Codigo], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV132ContagemResultado_CodigoAux, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo]";
         GXv_Object4[0] = scmdbuf;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00YR7( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV159Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_StatusDmn], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T3.[Contrato_Numero] AS ContagemResultado_CntNum, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda] FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T2.[Servico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV159Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo]";
         GXv_Object6[0] = scmdbuf;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00YR8( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV178CodigosNaoGeradas )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_StatusDmn], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T3.[Contrato_Numero] AS ContagemResultado_CntNum, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda] FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T2.[Servico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV178CodigosNaoGeradas, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo]";
         GXv_Object8[0] = scmdbuf;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00YR9( IGxContext context ,
                                             int AV148ContagemResultado_SistemaCod ,
                                             int A489ContagemResultado_SistemaCod ,
                                             String A493ContagemResultado_DemandaFM ,
                                             String AV175ContagemResultado_DemandaFM ,
                                             int AV54Contratada_Codigo ,
                                             int A490ContagemResultado_ContratadaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int10 ;
         GXv_int10 = new short [3] ;
         Object[] GXv_Object11 ;
         GXv_Object11 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 [ContagemResultado_SistemaCod], [ContagemResultado_DemandaFM], [ContagemResultado_ContratadaCod], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ContagemResultado_ContratadaCod] = @AV54Contratada_Codigo)";
         scmdbuf = scmdbuf + " and ([ContagemResultado_DemandaFM] = RTRIM(LTRIM(@AV175ContagemResultado_DemandaFM)))";
         if ( ! (0==AV148ContagemResultado_SistemaCod) )
         {
            sWhereString = sWhereString + " and ([ContagemResultado_SistemaCod] = @AV148ContagemResultado_SistemaCod)";
         }
         else
         {
            GXv_int10[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_ContratadaCod]";
         GXv_Object11[0] = scmdbuf;
         GXv_Object11[1] = GXv_int10;
         return GXv_Object11 ;
      }

      protected Object[] conditional_P00YR19( IGxContext context ,
                                              int A456ContagemResultado_Codigo ,
                                              IGxCollection AV159Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object12 ;
         GXv_Object12 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Owner], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DemandaFM], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_Descricao], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataEntrega], COALESCE( T4.[Usuario_PessoaNom], '') AS Usuario_PessoaNom FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT T6.[Pessoa_Nome] AS Usuario_PessoaNom, T5.[Usuario_Codigo] FROM ([Usuario] T5 WITH (NOLOCK) INNER JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) ) T4 ON T4.[Usuario_Codigo] = T1.[ContagemResultado_Owner])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV159Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo]";
         GXv_Object12[0] = scmdbuf;
         return GXv_Object12 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 3 :
                     return conditional_P00YR5(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 5 :
                     return conditional_P00YR7(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 6 :
                     return conditional_P00YR8(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 7 :
                     return conditional_P00YR9(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
               case 15 :
                     return conditional_P00YR19(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new UpdateCursor(def[17])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00YR2 ;
          prmP00YR2 = new Object[] {
          new Object[] {"@AV57ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YR3 ;
          prmP00YR3 = new Object[] {
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YR4 ;
          prmP00YR4 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YR6 ;
          prmP00YR6 = new Object[] {
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultado_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_EhValidacao",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Evidencia",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Baseline",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_PFBFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_FncUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@ContagemResultado_GlsData",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_GlsDescricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_GlsValor",SqlDbType.Decimal,12,2} ,
          new Object[] {"@ContagemResultado_GlsUser",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_OSManual",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_PBFinal",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PLFinal",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Custo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_PrazoInicialDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_PrazoMaisDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataExecucao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataCadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_RdmnIssueId",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_RdmnProjectId",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_RdmnUpdated",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_CntSrvPrrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_CntSrvPrrPrz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_CntSrvPrrCst",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_TemDpnHmlg",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_InicioExc",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_FimExc",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_InicioCrr",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_FimCrr",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Evento",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_InicioAnl",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_FimAnl",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_VlrAceite",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_TipoRegistro",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_UOOwner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Referencia",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_Restricoes",SqlDbType.VarChar,250,0} ,
          new Object[] {"@ContagemResultado_PrioridadePrevista",SqlDbType.VarChar,250,0} ,
          new Object[] {"@ContagemResultado_ServicoSS",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Combinada",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Entrega",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_DataInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_SemCusto",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_VlrCnc",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_PFCnc",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_DataPrvPgm",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_DataEntregaReal",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_QuantidadeSolicitada",SqlDbType.Decimal,9,4}
          } ;
          String cmdBufferP00YR6 ;
          cmdBufferP00YR6=" INSERT INTO [ContagemResultado]([ContagemResultado_DataDmn], [ContagemResultado_DataEntrega], [ContagemResultado_ContadorFSCod], [ContagemResultado_Demanda], [ContagemResultado_Link], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_StatusDmn], [ContagemResultado_EhValidacao], [ContagemResultado_ContratadaCod], [ContagemResultado_DemandaFM], [ContagemResultado_Descricao], [ContagemResultado_SistemaCod], [Modulo_Codigo], [ContagemResultado_Observacao], [ContagemResultado_Owner], [ContagemResultado_ValorPF], [ContagemResultado_Evidencia], [ContagemResultado_LoteAceiteCod], [ContagemResultado_Baseline], [ContagemResultado_OSVinculada], [ContagemResultado_PFBFSImp], [ContagemResultado_PFLFSImp], [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_Responsavel], [ContagemResultado_HoraEntrega], [ContagemResultado_LiqLogCod], [ContagemResultado_FncUsrCod], [ContagemResultado_Agrupador], [ContagemResultado_GlsData], [ContagemResultado_GlsDescricao], [ContagemResultado_GlsValor], [ContagemResultado_GlsUser], [ContagemResultado_OSManual], [ContagemResultado_PBFinal], [ContagemResultado_PLFinal], [ContagemResultado_Custo], [ContagemResultado_PrazoInicialDias], [ContagemResultado_PrazoMaisDias], [ContagemResultado_DataHomologacao], [ContagemResultado_DataExecucao], [ContagemResultado_DataCadastro], [ContagemResultado_DataPrevista], [ContagemResultado_RdmnIssueId], [ContagemResultado_RdmnProjectId], [ContagemResultado_RdmnUpdated], [ContagemResultado_CntSrvPrrCod], [ContagemResultado_CntSrvPrrPrz], [ContagemResultado_CntSrvPrrCst], [ContagemResultado_SS], [ContagemResultado_TemDpnHmlg], [ContagemResultado_TmpEstExc], [ContagemResultado_TmpEstCrr], [ContagemResultado_InicioExc], [ContagemResultado_FimExc], [ContagemResultado_InicioCrr], [ContagemResultado_FimCrr], [ContagemResultado_Evento], "
          + " [ContagemResultado_TmpEstAnl], [ContagemResultado_InicioAnl], [ContagemResultado_FimAnl], [ContagemResultado_ProjetoCod], [ContagemResultado_CntSrvCod], [ContagemResultado_VlrAceite], [ContagemResultado_TipoRegistro], [ContagemResultado_UOOwner], [ContagemResultado_Referencia], [ContagemResultado_Restricoes], [ContagemResultado_PrioridadePrevista], [ContagemResultado_ServicoSS], [ContagemResultado_Combinada], [ContagemResultado_Entrega], [ContagemResultado_DataInicio], [ContagemResultado_SemCusto], [ContagemResultado_VlrCnc], [ContagemResultado_PFCnc], [ContagemResultado_DataPrvPgm], [ContagemResultado_DataEntregaReal], [ContagemResultado_QuantidadeSolicitada]) VALUES(@ContagemResultado_DataDmn, @ContagemResultado_DataEntrega, @ContagemResultado_ContadorFSCod, @ContagemResultado_Demanda, @ContagemResultado_Link, @ContagemResultado_NaoCnfDmnCod, @ContagemResultado_StatusDmn, @ContagemResultado_EhValidacao, @ContagemResultado_ContratadaCod, @ContagemResultado_DemandaFM, @ContagemResultado_Descricao, @ContagemResultado_SistemaCod, @Modulo_Codigo, @ContagemResultado_Observacao, @ContagemResultado_Owner, @ContagemResultado_ValorPF, @ContagemResultado_Evidencia, @ContagemResultado_LoteAceiteCod, @ContagemResultado_Baseline, @ContagemResultado_OSVinculada, @ContagemResultado_PFBFSImp, @ContagemResultado_PFLFSImp, @ContagemResultado_ContratadaOrigemCod, @ContagemResultado_Responsavel, @ContagemResultado_HoraEntrega, @ContagemResultado_LiqLogCod, @ContagemResultado_FncUsrCod, @ContagemResultado_Agrupador, @ContagemResultado_GlsData, @ContagemResultado_GlsDescricao, @ContagemResultado_GlsValor, @ContagemResultado_GlsUser, @ContagemResultado_OSManual, @ContagemResultado_PBFinal, @ContagemResultado_PLFinal, @ContagemResultado_Custo, @ContagemResultado_PrazoInicialDias, @ContagemResultado_PrazoMaisDias,"
          + " @ContagemResultado_DataHomologacao, @ContagemResultado_DataExecucao, @ContagemResultado_DataCadastro, @ContagemResultado_DataPrevista, @ContagemResultado_RdmnIssueId, @ContagemResultado_RdmnProjectId, @ContagemResultado_RdmnUpdated, @ContagemResultado_CntSrvPrrCod, @ContagemResultado_CntSrvPrrPrz, @ContagemResultado_CntSrvPrrCst, @ContagemResultado_SS, @ContagemResultado_TemDpnHmlg, @ContagemResultado_TmpEstExc, @ContagemResultado_TmpEstCrr, @ContagemResultado_InicioExc, @ContagemResultado_FimExc, @ContagemResultado_InicioCrr, @ContagemResultado_FimCrr, @ContagemResultado_Evento, @ContagemResultado_TmpEstAnl, @ContagemResultado_InicioAnl, @ContagemResultado_FimAnl, @ContagemResultado_ProjetoCod, @ContagemResultado_CntSrvCod, @ContagemResultado_VlrAceite, @ContagemResultado_TipoRegistro, @ContagemResultado_UOOwner, @ContagemResultado_Referencia, @ContagemResultado_Restricoes, @ContagemResultado_PrioridadePrevista, @ContagemResultado_ServicoSS, @ContagemResultado_Combinada, @ContagemResultado_Entrega, @ContagemResultado_DataInicio, @ContagemResultado_SemCusto, @ContagemResultado_VlrCnc, @ContagemResultado_PFCnc, @ContagemResultado_DataPrvPgm, @ContagemResultado_DataEntregaReal, @ContagemResultado_QuantidadeSolicitada); SELECT SCOPE_IDENTITY()" ;
          Object[] prmP00YR10 ;
          prmP00YR10 = new Object[] {
          new Object[] {"@AV54Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YR11 ;
          prmP00YR11 = new Object[] {
          new Object[] {"@AV130Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV54Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YR13 ;
          prmP00YR13 = new Object[] {
          new Object[] {"@AV126WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00YR14 ;
          prmP00YR14 = new Object[] {
          new Object[] {"@AV126WWP_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YR15 ;
          prmP00YR15 = new Object[] {
          new Object[] {"@AV130Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YR16 ;
          prmP00YR16 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YR17 ;
          prmP00YR17 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YR20 ;
          prmP00YR20 = new Object[] {
          new Object[] {"@AV41ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YR21 ;
          prmP00YR21 = new Object[] {
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@ContagemResultado_Evento",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YR5 ;
          prmP00YR5 = new Object[] {
          } ;
          Object[] prmP00YR7 ;
          prmP00YR7 = new Object[] {
          } ;
          Object[] prmP00YR8 ;
          prmP00YR8 = new Object[] {
          } ;
          Object[] prmP00YR9 ;
          prmP00YR9 = new Object[] {
          new Object[] {"@AV54Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV175ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV148ContagemResultado_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YR19 ;
          prmP00YR19 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00YR2", "SELECT T1.[Contrato_Codigo], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T3.[Contratada_AreaTrabalhoCod], T1.[ContratoServicos_Codigo], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Servico_Percentual], T2.[Contrato_ValorUnidadeContratacao], T1.[ContratoServicos_PrazoCorrecao], T1.[ContratoServicos_PrazoCorrecaoTipo], T1.[ContratoServicos_PrazoInicio], T1.[ContratoServicos_PrazoAnalise], T1.[Servico_VlrUnidadeContratada], T1.[ContratoServicos_PrazoTpDias] FROM ((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) WHERE T1.[ContratoServicos_Codigo] = @AV57ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YR2,1,0,true,true )
             ,new CursorDef("P00YR3", "SELECT TOP 1 T1.[AreaTrabalho_Codigo], T1.[Contratante_Codigo], T2.[Contratante_FimDoExpediente] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @Contratada_AreaTrabalhoCod ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YR3,1,0,true,true )
             ,new CursorDef("P00YR4", "SELECT TOP 1 [Contratante_Codigo] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ORDER BY [Contratante_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YR4,1,0,false,true )
             ,new CursorDef("P00YR5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YR5,100,0,true,false )
             ,new CursorDef("P00YR6", cmdBufferP00YR6, GxErrorMask.GX_NOMASK,prmP00YR6)
             ,new CursorDef("P00YR7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YR7,100,0,false,false )
             ,new CursorDef("P00YR8", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YR8,100,0,false,false )
             ,new CursorDef("P00YR9", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YR9,1,0,false,true )
             ,new CursorDef("P00YR10", "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_Codigo] = @AV54Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YR10,1,0,false,true )
             ,new CursorDef("P00YR11", "SELECT [Contratada_Codigo], [Contrato_Codigo], [Contrato_PrepostoCod] FROM [Contrato] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @AV130Contrato_Codigo) AND ([Contratada_Codigo] = @AV54Contratada_Codigo) ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YR11,1,0,false,true )
             ,new CursorDef("P00YR13", "SELECT COALESCE( T1.[Usuario_PessoaNom], '') AS Usuario_PessoaNom FROM (SELECT MIN(T3.[Pessoa_Nome]) AS Usuario_PessoaNom FROM ([Usuario] T2 WITH (NOLOCK) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T2.[Usuario_Codigo] = @AV126WWPContext__Userid ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YR13,1,0,true,false )
             ,new CursorDef("P00YR14", "SELECT T1.[Contratante_Codigo], T2.[Contratante_EmailSdaUser], T2.[Contratante_EmailSdaPort], T2.[Contratante_EmailSdaHost], T1.[AreaTrabalho_Codigo] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE (T1.[AreaTrabalho_Codigo] = @AV126WWP_1Areatrabalho_codigo) AND (T2.[Contratante_EmailSdaHost] IS NULL or T2.[Contratante_EmailSdaPort] IS NULL or T2.[Contratante_EmailSdaUser] IS NULL) ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YR14,1,0,false,true )
             ,new CursorDef("P00YR15", "SELECT TOP 1 [Contrato_Codigo], [Contrato_PrepostoCod] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV130Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YR15,1,0,true,true )
             ,new CursorDef("P00YR16", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YR16,100,0,true,false )
             ,new CursorDef("P00YR17", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YR17,100,0,true,false )
             ,new CursorDef("P00YR19", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YR19,100,0,false,false )
             ,new CursorDef("P00YR20", "SELECT [ContagemResultado_Codigo], [ContagemResultado_OSVinculada], [ContagemResultado_Agrupador], [ContagemResultado_Evento] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV41ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YR20,1,0,true,true )
             ,new CursorDef("P00YR21", "UPDATE [ContagemResultado] SET [ContagemResultado_OSVinculada]=@ContagemResultado_OSVinculada, [ContagemResultado_Agrupador]=@ContagemResultado_Agrupador, [ContagemResultado_Evento]=@ContagemResultado_Evento  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00YR21)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(8) ;
                ((short[]) buf[10])[0] = rslt.getShort(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((String[]) buf[12])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((short[]) buf[14])[0] = rslt.getShort(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((short[]) buf[16])[0] = rslt.getShort(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(13) ;
                ((String[]) buf[19])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((short[]) buf[14])[0] = rslt.getShort(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[16])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[20])[0] = rslt.getGXDateTime(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((int[]) buf[24])[0] = rslt.getInt(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[26])[0] = rslt.getGXDateTime(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[28])[0] = rslt.getGXDateTime(15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[30])[0] = rslt.getGXDateTime(16) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(16);
                ((short[]) buf[32])[0] = rslt.getShort(17) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(17);
                ((short[]) buf[34])[0] = rslt.getShort(18) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(18);
                ((decimal[]) buf[36])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(19);
                ((decimal[]) buf[38])[0] = rslt.getDecimal(20) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(20);
                ((String[]) buf[40])[0] = rslt.getString(21, 15) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(21);
                ((DateTime[]) buf[42])[0] = rslt.getGXDateTime(22) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(22);
                ((int[]) buf[44])[0] = rslt.getInt(23) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(23);
                ((int[]) buf[46])[0] = rslt.getInt(24) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(24);
                ((int[]) buf[48])[0] = rslt.getInt(25) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(25);
                ((decimal[]) buf[50])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(26);
                ((int[]) buf[52])[0] = rslt.getInt(27) ;
                ((int[]) buf[53])[0] = rslt.getInt(28) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(28);
                ((int[]) buf[55])[0] = rslt.getInt(29) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(29);
                ((String[]) buf[57])[0] = rslt.getVarchar(30) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(30);
                ((int[]) buf[59])[0] = rslt.getInt(31) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(31);
                ((String[]) buf[61])[0] = rslt.getString(32, 1) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(32);
                ((String[]) buf[63])[0] = rslt.getVarchar(33) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(33);
                ((DateTime[]) buf[65])[0] = rslt.getGXDate(34) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(34);
                ((DateTime[]) buf[67])[0] = rslt.getGXDate(35) ;
                ((decimal[]) buf[68])[0] = rslt.getDecimal(36) ;
                ((bool[]) buf[69])[0] = rslt.wasNull(36);
                ((decimal[]) buf[70])[0] = rslt.getDecimal(37) ;
                ((bool[]) buf[71])[0] = rslt.wasNull(37);
                ((bool[]) buf[72])[0] = rslt.getBool(38) ;
                ((bool[]) buf[73])[0] = rslt.wasNull(38);
                ((DateTime[]) buf[74])[0] = rslt.getGXDate(39) ;
                ((bool[]) buf[75])[0] = rslt.wasNull(39);
                ((short[]) buf[76])[0] = rslt.getShort(40) ;
                ((bool[]) buf[77])[0] = rslt.wasNull(40);
                ((bool[]) buf[78])[0] = rslt.getBool(41) ;
                ((bool[]) buf[79])[0] = rslt.wasNull(41);
                ((int[]) buf[80])[0] = rslt.getInt(42) ;
                ((bool[]) buf[81])[0] = rslt.wasNull(42);
                ((String[]) buf[82])[0] = rslt.getVarchar(43) ;
                ((bool[]) buf[83])[0] = rslt.wasNull(43);
                ((String[]) buf[84])[0] = rslt.getVarchar(44) ;
                ((bool[]) buf[85])[0] = rslt.wasNull(44);
                ((String[]) buf[86])[0] = rslt.getVarchar(45) ;
                ((bool[]) buf[87])[0] = rslt.wasNull(45);
                ((int[]) buf[88])[0] = rslt.getInt(46) ;
                ((bool[]) buf[89])[0] = rslt.wasNull(46);
                ((short[]) buf[90])[0] = rslt.getShort(47) ;
                ((decimal[]) buf[91])[0] = rslt.getDecimal(48) ;
                ((bool[]) buf[92])[0] = rslt.wasNull(48);
                ((int[]) buf[93])[0] = rslt.getInt(49) ;
                ((bool[]) buf[94])[0] = rslt.wasNull(49);
                ((DateTime[]) buf[95])[0] = rslt.getGXDateTime(50) ;
                ((bool[]) buf[96])[0] = rslt.wasNull(50);
                ((DateTime[]) buf[97])[0] = rslt.getGXDateTime(51) ;
                ((bool[]) buf[98])[0] = rslt.wasNull(51);
                ((int[]) buf[99])[0] = rslt.getInt(52) ;
                ((bool[]) buf[100])[0] = rslt.wasNull(52);
                ((bool[]) buf[101])[0] = rslt.getBool(53) ;
                ((bool[]) buf[102])[0] = rslt.wasNull(53);
                ((decimal[]) buf[103])[0] = rslt.getDecimal(54) ;
                ((bool[]) buf[104])[0] = rslt.wasNull(54);
                ((decimal[]) buf[105])[0] = rslt.getDecimal(55) ;
                ((bool[]) buf[106])[0] = rslt.wasNull(55);
                ((int[]) buf[107])[0] = rslt.getInt(56) ;
                ((bool[]) buf[108])[0] = rslt.wasNull(56);
                ((DateTime[]) buf[109])[0] = rslt.getGXDateTime(57) ;
                ((bool[]) buf[110])[0] = rslt.wasNull(57);
                ((int[]) buf[111])[0] = rslt.getInt(58) ;
                ((bool[]) buf[112])[0] = rslt.wasNull(58);
                ((int[]) buf[113])[0] = rslt.getInt(59) ;
                ((bool[]) buf[114])[0] = rslt.wasNull(59);
                ((DateTime[]) buf[115])[0] = rslt.getGXDateTime(60) ;
                ((bool[]) buf[116])[0] = rslt.wasNull(60);
                ((decimal[]) buf[117])[0] = rslt.getDecimal(61) ;
                ((bool[]) buf[118])[0] = rslt.wasNull(61);
                ((bool[]) buf[119])[0] = rslt.getBool(62) ;
                ((bool[]) buf[120])[0] = rslt.wasNull(62);
                ((int[]) buf[121])[0] = rslt.getInt(63) ;
                ((bool[]) buf[122])[0] = rslt.wasNull(63);
                ((decimal[]) buf[123])[0] = rslt.getDecimal(64) ;
                ((bool[]) buf[124])[0] = rslt.wasNull(64);
                ((String[]) buf[125])[0] = rslt.getLongVarchar(65) ;
                ((bool[]) buf[126])[0] = rslt.wasNull(65);
                ((DateTime[]) buf[127])[0] = rslt.getGXDate(66) ;
                ((bool[]) buf[128])[0] = rslt.wasNull(66);
                ((int[]) buf[129])[0] = rslt.getInt(67) ;
                ((bool[]) buf[130])[0] = rslt.wasNull(67);
                ((int[]) buf[131])[0] = rslt.getInt(68) ;
                ((bool[]) buf[132])[0] = rslt.wasNull(68);
                ((decimal[]) buf[133])[0] = rslt.getDecimal(69) ;
                ((bool[]) buf[134])[0] = rslt.wasNull(69);
                ((decimal[]) buf[135])[0] = rslt.getDecimal(70) ;
                ((bool[]) buf[136])[0] = rslt.wasNull(70);
                ((bool[]) buf[137])[0] = rslt.getBool(71) ;
                ((bool[]) buf[138])[0] = rslt.wasNull(71);
                ((int[]) buf[139])[0] = rslt.getInt(72) ;
                ((bool[]) buf[140])[0] = rslt.wasNull(72);
                ((String[]) buf[141])[0] = rslt.getLongVarchar(73) ;
                ((bool[]) buf[142])[0] = rslt.wasNull(73);
                ((String[]) buf[143])[0] = rslt.getLongVarchar(74) ;
                ((bool[]) buf[144])[0] = rslt.wasNull(74);
                ((String[]) buf[145])[0] = rslt.getVarchar(75) ;
                ((bool[]) buf[146])[0] = rslt.wasNull(75);
                ((bool[]) buf[147])[0] = rslt.getBool(76) ;
                ((bool[]) buf[148])[0] = rslt.wasNull(76);
                ((int[]) buf[149])[0] = rslt.getInt(77) ;
                ((bool[]) buf[150])[0] = rslt.wasNull(77);
                ((String[]) buf[151])[0] = rslt.getLongVarchar(78) ;
                ((bool[]) buf[152])[0] = rslt.wasNull(78);
                ((int[]) buf[153])[0] = rslt.getInt(79) ;
                ((bool[]) buf[154])[0] = rslt.wasNull(79);
                ((int[]) buf[155])[0] = rslt.getInt(80) ;
                ((String[]) buf[156])[0] = rslt.getString(81, 15) ;
                ((bool[]) buf[157])[0] = rslt.wasNull(81);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(5) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 20) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(5) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 20) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(8) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(8, (bool)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[26]);
                }
                stmt.SetParameter(15, (int)parms[27]);
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 17 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 19 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(19, (bool)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 20 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(20, (int)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 21 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(21, (decimal)parms[39]);
                }
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 22 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(22, (decimal)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 24 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(24, (int)parms[45]);
                }
                if ( (bool)parms[46] )
                {
                   stmt.setNull( 25 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(25, (DateTime)parms[47]);
                }
                if ( (bool)parms[48] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[49]);
                }
                if ( (bool)parms[50] )
                {
                   stmt.setNull( 27 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(27, (int)parms[51]);
                }
                if ( (bool)parms[52] )
                {
                   stmt.setNull( 28 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(28, (String)parms[53]);
                }
                if ( (bool)parms[54] )
                {
                   stmt.setNull( 29 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(29, (DateTime)parms[55]);
                }
                if ( (bool)parms[56] )
                {
                   stmt.setNull( 30 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(30, (String)parms[57]);
                }
                if ( (bool)parms[58] )
                {
                   stmt.setNull( 31 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(31, (decimal)parms[59]);
                }
                if ( (bool)parms[60] )
                {
                   stmt.setNull( 32 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(32, (int)parms[61]);
                }
                if ( (bool)parms[62] )
                {
                   stmt.setNull( 33 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(33, (bool)parms[63]);
                }
                if ( (bool)parms[64] )
                {
                   stmt.setNull( 34 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(34, (decimal)parms[65]);
                }
                if ( (bool)parms[66] )
                {
                   stmt.setNull( 35 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(35, (decimal)parms[67]);
                }
                if ( (bool)parms[68] )
                {
                   stmt.setNull( 36 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(36, (decimal)parms[69]);
                }
                if ( (bool)parms[70] )
                {
                   stmt.setNull( 37 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(37, (short)parms[71]);
                }
                if ( (bool)parms[72] )
                {
                   stmt.setNull( 38 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(38, (short)parms[73]);
                }
                if ( (bool)parms[74] )
                {
                   stmt.setNull( 39 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(39, (DateTime)parms[75]);
                }
                if ( (bool)parms[76] )
                {
                   stmt.setNull( 40 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(40, (DateTime)parms[77]);
                }
                if ( (bool)parms[78] )
                {
                   stmt.setNull( 41 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(41, (DateTime)parms[79]);
                }
                if ( (bool)parms[80] )
                {
                   stmt.setNull( 42 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(42, (DateTime)parms[81]);
                }
                if ( (bool)parms[82] )
                {
                   stmt.setNull( 43 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(43, (int)parms[83]);
                }
                if ( (bool)parms[84] )
                {
                   stmt.setNull( 44 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(44, (int)parms[85]);
                }
                if ( (bool)parms[86] )
                {
                   stmt.setNull( 45 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(45, (DateTime)parms[87]);
                }
                if ( (bool)parms[88] )
                {
                   stmt.setNull( 46 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(46, (int)parms[89]);
                }
                if ( (bool)parms[90] )
                {
                   stmt.setNull( 47 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(47, (decimal)parms[91]);
                }
                if ( (bool)parms[92] )
                {
                   stmt.setNull( 48 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(48, (decimal)parms[93]);
                }
                if ( (bool)parms[94] )
                {
                   stmt.setNull( 49 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(49, (int)parms[95]);
                }
                if ( (bool)parms[96] )
                {
                   stmt.setNull( 50 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(50, (bool)parms[97]);
                }
                if ( (bool)parms[98] )
                {
                   stmt.setNull( 51 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(51, (int)parms[99]);
                }
                if ( (bool)parms[100] )
                {
                   stmt.setNull( 52 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(52, (int)parms[101]);
                }
                if ( (bool)parms[102] )
                {
                   stmt.setNull( 53 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(53, (DateTime)parms[103]);
                }
                if ( (bool)parms[104] )
                {
                   stmt.setNull( 54 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(54, (DateTime)parms[105]);
                }
                if ( (bool)parms[106] )
                {
                   stmt.setNull( 55 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(55, (DateTime)parms[107]);
                }
                if ( (bool)parms[108] )
                {
                   stmt.setNull( 56 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(56, (DateTime)parms[109]);
                }
                if ( (bool)parms[110] )
                {
                   stmt.setNull( 57 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(57, (short)parms[111]);
                }
                if ( (bool)parms[112] )
                {
                   stmt.setNull( 58 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(58, (int)parms[113]);
                }
                if ( (bool)parms[114] )
                {
                   stmt.setNull( 59 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(59, (DateTime)parms[115]);
                }
                if ( (bool)parms[116] )
                {
                   stmt.setNull( 60 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(60, (DateTime)parms[117]);
                }
                if ( (bool)parms[118] )
                {
                   stmt.setNull( 61 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(61, (int)parms[119]);
                }
                if ( (bool)parms[120] )
                {
                   stmt.setNull( 62 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(62, (int)parms[121]);
                }
                if ( (bool)parms[122] )
                {
                   stmt.setNull( 63 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(63, (decimal)parms[123]);
                }
                stmt.SetParameter(64, (short)parms[124]);
                if ( (bool)parms[125] )
                {
                   stmt.setNull( 65 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(65, (int)parms[126]);
                }
                if ( (bool)parms[127] )
                {
                   stmt.setNull( 66 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(66, (String)parms[128]);
                }
                if ( (bool)parms[129] )
                {
                   stmt.setNull( 67 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(67, (String)parms[130]);
                }
                if ( (bool)parms[131] )
                {
                   stmt.setNull( 68 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(68, (String)parms[132]);
                }
                if ( (bool)parms[133] )
                {
                   stmt.setNull( 69 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(69, (int)parms[134]);
                }
                if ( (bool)parms[135] )
                {
                   stmt.setNull( 70 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(70, (bool)parms[136]);
                }
                if ( (bool)parms[137] )
                {
                   stmt.setNull( 71 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(71, (short)parms[138]);
                }
                if ( (bool)parms[139] )
                {
                   stmt.setNull( 72 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(72, (DateTime)parms[140]);
                }
                if ( (bool)parms[141] )
                {
                   stmt.setNull( 73 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(73, (bool)parms[142]);
                }
                if ( (bool)parms[143] )
                {
                   stmt.setNull( 74 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(74, (decimal)parms[144]);
                }
                if ( (bool)parms[145] )
                {
                   stmt.setNull( 75 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(75, (decimal)parms[146]);
                }
                if ( (bool)parms[147] )
                {
                   stmt.setNull( 76 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(76, (DateTime)parms[148]);
                }
                if ( (bool)parms[149] )
                {
                   stmt.setNull( 77 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(77, (DateTime)parms[150]);
                }
                if ( (bool)parms[151] )
                {
                   stmt.setNull( 78 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(78, (decimal)parms[152]);
                }
                return;
             case 7 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
       }
    }

 }

}
