/*
               File: PRC_LerPlanilhaContagem
        Description: Ler Planilha Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:35.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_lerplanilhacontagem : GXProcedure
   {
      public prc_lerplanilhacontagem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_lerplanilhacontagem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( ref DateTime aP0_ContagemResultado_DataCnt ,
                           ref decimal aP1_ContagemResultado_PFBFS ,
                           ref decimal aP2_ContagemResultado_PFLFS ,
                           ref decimal aP3_ContagemResultado_PFBFM ,
                           ref decimal aP4_ContagemResultado_PFLFM )
      {
         this.AV11ContagemResultado_DataCnt = aP0_ContagemResultado_DataCnt;
         this.AV12ContagemResultado_PFBFS = aP1_ContagemResultado_PFBFS;
         this.AV9ContagemResultado_PFLFS = aP2_ContagemResultado_PFLFS;
         this.AV8ContagemResultado_PFBFM = aP3_ContagemResultado_PFBFM;
         this.AV10ContagemResultado_PFLFM = aP4_ContagemResultado_PFLFM;
         initialize();
         executePrivate();
         aP0_ContagemResultado_DataCnt=this.AV11ContagemResultado_DataCnt;
         aP1_ContagemResultado_PFBFS=this.AV12ContagemResultado_PFBFS;
         aP2_ContagemResultado_PFLFS=this.AV9ContagemResultado_PFLFS;
         aP3_ContagemResultado_PFBFM=this.AV8ContagemResultado_PFBFM;
         aP4_ContagemResultado_PFLFM=this.AV10ContagemResultado_PFLFM;
      }

      public decimal executeUdp( ref DateTime aP0_ContagemResultado_DataCnt ,
                                 ref decimal aP1_ContagemResultado_PFBFS ,
                                 ref decimal aP2_ContagemResultado_PFLFS ,
                                 ref decimal aP3_ContagemResultado_PFBFM )
      {
         this.AV11ContagemResultado_DataCnt = aP0_ContagemResultado_DataCnt;
         this.AV12ContagemResultado_PFBFS = aP1_ContagemResultado_PFBFS;
         this.AV9ContagemResultado_PFLFS = aP2_ContagemResultado_PFLFS;
         this.AV8ContagemResultado_PFBFM = aP3_ContagemResultado_PFBFM;
         this.AV10ContagemResultado_PFLFM = aP4_ContagemResultado_PFLFM;
         initialize();
         executePrivate();
         aP0_ContagemResultado_DataCnt=this.AV11ContagemResultado_DataCnt;
         aP1_ContagemResultado_PFBFS=this.AV12ContagemResultado_PFBFS;
         aP2_ContagemResultado_PFLFS=this.AV9ContagemResultado_PFLFS;
         aP3_ContagemResultado_PFBFM=this.AV8ContagemResultado_PFBFM;
         aP4_ContagemResultado_PFLFM=this.AV10ContagemResultado_PFLFM;
         return AV10ContagemResultado_PFLFM ;
      }

      public void executeSubmit( ref DateTime aP0_ContagemResultado_DataCnt ,
                                 ref decimal aP1_ContagemResultado_PFBFS ,
                                 ref decimal aP2_ContagemResultado_PFLFS ,
                                 ref decimal aP3_ContagemResultado_PFBFM ,
                                 ref decimal aP4_ContagemResultado_PFLFM )
      {
         prc_lerplanilhacontagem objprc_lerplanilhacontagem;
         objprc_lerplanilhacontagem = new prc_lerplanilhacontagem();
         objprc_lerplanilhacontagem.AV11ContagemResultado_DataCnt = aP0_ContagemResultado_DataCnt;
         objprc_lerplanilhacontagem.AV12ContagemResultado_PFBFS = aP1_ContagemResultado_PFBFS;
         objprc_lerplanilhacontagem.AV9ContagemResultado_PFLFS = aP2_ContagemResultado_PFLFS;
         objprc_lerplanilhacontagem.AV8ContagemResultado_PFBFM = aP3_ContagemResultado_PFBFM;
         objprc_lerplanilhacontagem.AV10ContagemResultado_PFLFM = aP4_ContagemResultado_PFLFM;
         objprc_lerplanilhacontagem.context.SetSubmitInitialConfig(context);
         objprc_lerplanilhacontagem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_lerplanilhacontagem);
         aP0_ContagemResultado_DataCnt=this.AV11ContagemResultado_DataCnt;
         aP1_ContagemResultado_PFBFS=this.AV12ContagemResultado_PFBFS;
         aP2_ContagemResultado_PFLFS=this.AV9ContagemResultado_PFLFS;
         aP3_ContagemResultado_PFBFM=this.AV8ContagemResultado_PFBFM;
         aP4_ContagemResultado_PFLFM=this.AV10ContagemResultado_PFLFM;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_lerplanilhacontagem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV22Sdt_Contagens.FromXml(AV23WebSession.Get("PlanilhaContagem"), "");
         if ( AV22Sdt_Contagens.Count > 0 )
         {
            AV21File.Source = "C:\\SITES\\MEETRIKA\\web2\\PublicTempStorage\\"+((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV22Sdt_Contagens.Item(1)).gxTpr_Contagemresultadoevidencia_nomearq+"."+((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV22Sdt_Contagens.Item(1)).gxTpr_Contagemresultadoevidencia_tipoarq;
            if ( ! AV21File.Exists() )
            {
               AV21File.Source = "C:\\SITES\\MEETRIKA\\web\\PublicTempStorage\\"+((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV22Sdt_Contagens.Item(1)).gxTpr_Contagemresultadoevidencia_nomearq+"."+((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV22Sdt_Contagens.Item(1)).gxTpr_Contagemresultadoevidencia_tipoarq;
            }
            AV15i = AV16ExcelDocument.Open(AV21File.Source);
            if ( AV15i == 0 )
            {
               AV11ContagemResultado_DataCnt = DateTimeUtil.ResetTime(AV16ExcelDocument.get_Cells(24, 4, 1, 1).Date);
               if ( (Convert.ToDecimal(0)==AV12ContagemResultado_PFBFS) )
               {
                  AV12ContagemResultado_PFBFS = NumberUtil.Val( AV16ExcelDocument.get_Cells(18, 4, 1, 1).Value, ".");
                  AV9ContagemResultado_PFLFS = NumberUtil.Val( AV16ExcelDocument.get_Cells(18, 8, 1, 1).Value, ".");
                  if ( (Convert.ToDecimal(0)==AV12ContagemResultado_PFBFS) )
                  {
                     AV12ContagemResultado_PFBFS = (decimal)(AV16ExcelDocument.get_Cells(18, 4, 1, 1).Number);
                     AV9ContagemResultado_PFLFS = (decimal)(AV16ExcelDocument.get_Cells(18, 8, 1, 1).Number);
                  }
               }
               AV8ContagemResultado_PFBFM = NumberUtil.Val( AV16ExcelDocument.get_Cells(26, 4, 1, 1).Value, ".");
               AV10ContagemResultado_PFLFM = NumberUtil.Val( AV16ExcelDocument.get_Cells(26, 8, 1, 1).Value, ".");
               if ( (Convert.ToDecimal(0)==AV8ContagemResultado_PFBFM) )
               {
                  AV8ContagemResultado_PFBFM = (decimal)(AV16ExcelDocument.get_Cells(26, 4, 1, 1).Number);
                  AV10ContagemResultado_PFLFM = (decimal)(AV16ExcelDocument.get_Cells(26, 8, 1, 1).Number);
               }
               AV16ExcelDocument.Close();
            }
         }
         this.cleanup();
         if (true) return;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV22Sdt_Contagens = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_MeetrikaVs3", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         AV23WebSession = context.GetSession();
         AV21File = new GxFile(context.GetPhysicalPath());
         AV16ExcelDocument = new ExcelDocumentI();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV15i ;
      private decimal AV12ContagemResultado_PFBFS ;
      private decimal AV9ContagemResultado_PFLFS ;
      private decimal AV8ContagemResultado_PFBFM ;
      private decimal AV10ContagemResultado_PFLFM ;
      private DateTime AV11ContagemResultado_DataCnt ;
      private IGxSession AV23WebSession ;
      private ExcelDocumentI AV16ExcelDocument ;
      private DateTime aP0_ContagemResultado_DataCnt ;
      private decimal aP1_ContagemResultado_PFBFS ;
      private decimal aP2_ContagemResultado_PFLFS ;
      private decimal aP3_ContagemResultado_PFBFM ;
      private decimal aP4_ContagemResultado_PFLFM ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV22Sdt_Contagens ;
      private GxFile AV21File ;
   }

}
