/*
               File: PRC_VerificaContratadaContratanteUsuario
        Description: Verifica se a Contratada e/ou Contratante possuem Usu�rios vinculados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:6:49.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_verificacontratadacontratanteusuario : GXProcedure
   {
      public prc_verificacontratadacontratanteusuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_verificacontratadacontratanteusuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_Codigo ,
                           out bool aP1_IsUsuarios )
      {
         this.AV10Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV8IsUsuarios = false ;
         initialize();
         executePrivate();
         aP1_IsUsuarios=this.AV8IsUsuarios;
      }

      public bool executeUdp( int aP0_Contratada_Codigo )
      {
         this.AV10Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV8IsUsuarios = false ;
         initialize();
         executePrivate();
         aP1_IsUsuarios=this.AV8IsUsuarios;
         return AV8IsUsuarios ;
      }

      public void executeSubmit( int aP0_Contratada_Codigo ,
                                 out bool aP1_IsUsuarios )
      {
         prc_verificacontratadacontratanteusuario objprc_verificacontratadacontratanteusuario;
         objprc_verificacontratadacontratanteusuario = new prc_verificacontratadacontratanteusuario();
         objprc_verificacontratadacontratanteusuario.AV10Contratada_Codigo = aP0_Contratada_Codigo;
         objprc_verificacontratadacontratanteusuario.AV8IsUsuarios = false ;
         objprc_verificacontratadacontratanteusuario.context.SetSubmitInitialConfig(context);
         objprc_verificacontratadacontratanteusuario.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_verificacontratadacontratanteusuario);
         aP1_IsUsuarios=this.AV8IsUsuarios;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_verificacontratadacontratanteusuario)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8IsUsuarios = false;
         /* Using cursor P00XX2 */
         pr_default.execute(0, new Object[] {AV10Contratada_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A52Contratada_AreaTrabalhoCod = P00XX2_A52Contratada_AreaTrabalhoCod[0];
            A39Contratada_Codigo = P00XX2_A39Contratada_Codigo[0];
            A29Contratante_Codigo = P00XX2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00XX2_n29Contratante_Codigo[0];
            A29Contratante_Codigo = P00XX2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00XX2_n29Contratante_Codigo[0];
            AV11Contratante_Codigo = A29Contratante_Codigo;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Using cursor P00XX3 */
         pr_default.execute(1, new Object[] {AV11Contratante_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A60ContratanteUsuario_UsuarioCod = P00XX3_A60ContratanteUsuario_UsuarioCod[0];
            A54Usuario_Ativo = P00XX3_A54Usuario_Ativo[0];
            n54Usuario_Ativo = P00XX3_n54Usuario_Ativo[0];
            A63ContratanteUsuario_ContratanteCod = P00XX3_A63ContratanteUsuario_ContratanteCod[0];
            A54Usuario_Ativo = P00XX3_A54Usuario_Ativo[0];
            n54Usuario_Ativo = P00XX3_n54Usuario_Ativo[0];
            AV8IsUsuarios = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( ! AV8IsUsuarios )
         {
            /* Using cursor P00XX4 */
            pr_default.execute(2, new Object[] {AV10Contratada_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A69ContratadaUsuario_UsuarioCod = P00XX4_A69ContratadaUsuario_UsuarioCod[0];
               A1394ContratadaUsuario_UsuarioAtivo = P00XX4_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = P00XX4_n1394ContratadaUsuario_UsuarioAtivo[0];
               A66ContratadaUsuario_ContratadaCod = P00XX4_A66ContratadaUsuario_ContratadaCod[0];
               A1394ContratadaUsuario_UsuarioAtivo = P00XX4_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = P00XX4_n1394ContratadaUsuario_UsuarioAtivo[0];
               AV8IsUsuarios = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(2);
            }
            pr_default.close(2);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00XX2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00XX2_A39Contratada_Codigo = new int[1] ;
         P00XX2_A29Contratante_Codigo = new int[1] ;
         P00XX2_n29Contratante_Codigo = new bool[] {false} ;
         P00XX3_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P00XX3_A54Usuario_Ativo = new bool[] {false} ;
         P00XX3_n54Usuario_Ativo = new bool[] {false} ;
         P00XX3_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P00XX4_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00XX4_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P00XX4_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P00XX4_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_verificacontratadacontratanteusuario__default(),
            new Object[][] {
                new Object[] {
               P00XX2_A52Contratada_AreaTrabalhoCod, P00XX2_A39Contratada_Codigo, P00XX2_A29Contratante_Codigo, P00XX2_n29Contratante_Codigo
               }
               , new Object[] {
               P00XX3_A60ContratanteUsuario_UsuarioCod, P00XX3_A54Usuario_Ativo, P00XX3_n54Usuario_Ativo, P00XX3_A63ContratanteUsuario_ContratanteCod
               }
               , new Object[] {
               P00XX4_A69ContratadaUsuario_UsuarioCod, P00XX4_A1394ContratadaUsuario_UsuarioAtivo, P00XX4_n1394ContratadaUsuario_UsuarioAtivo, P00XX4_A66ContratadaUsuario_ContratadaCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV10Contratada_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A39Contratada_Codigo ;
      private int A29Contratante_Codigo ;
      private int AV11Contratante_Codigo ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private String scmdbuf ;
      private bool AV8IsUsuarios ;
      private bool n29Contratante_Codigo ;
      private bool A54Usuario_Ativo ;
      private bool n54Usuario_Ativo ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00XX2_A52Contratada_AreaTrabalhoCod ;
      private int[] P00XX2_A39Contratada_Codigo ;
      private int[] P00XX2_A29Contratante_Codigo ;
      private bool[] P00XX2_n29Contratante_Codigo ;
      private int[] P00XX3_A60ContratanteUsuario_UsuarioCod ;
      private bool[] P00XX3_A54Usuario_Ativo ;
      private bool[] P00XX3_n54Usuario_Ativo ;
      private int[] P00XX3_A63ContratanteUsuario_ContratanteCod ;
      private int[] P00XX4_A69ContratadaUsuario_UsuarioCod ;
      private bool[] P00XX4_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] P00XX4_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] P00XX4_A66ContratadaUsuario_ContratadaCod ;
      private bool aP1_IsUsuarios ;
   }

   public class prc_verificacontratadacontratanteusuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XX2 ;
          prmP00XX2 = new Object[] {
          new Object[] {"@AV10Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XX3 ;
          prmP00XX3 = new Object[] {
          new Object[] {"@AV11Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XX4 ;
          prmP00XX4 = new Object[] {
          new Object[] {"@AV10Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XX2", "SELECT T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[Contratada_Codigo], T2.[Contratante_Codigo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) WHERE T1.[Contratada_Codigo] = @AV10Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XX2,1,0,false,true )
             ,new CursorDef("P00XX3", "SELECT TOP 1 T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T2.[Usuario_Ativo], T1.[ContratanteUsuario_ContratanteCod] FROM ([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) WHERE (T1.[ContratanteUsuario_ContratanteCod] = @AV11Contratante_Codigo) AND (T2.[Usuario_Ativo] = 1) ORDER BY T1.[ContratanteUsuario_ContratanteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XX3,1,0,false,true )
             ,new CursorDef("P00XX4", "SELECT TOP 1 T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T1.[ContratadaUsuario_ContratadaCod] FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) WHERE (T1.[ContratadaUsuario_ContratadaCod] = @AV10Contratada_Codigo) AND (T2.[Usuario_Ativo] = 1) ORDER BY T1.[ContratadaUsuario_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XX4,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
