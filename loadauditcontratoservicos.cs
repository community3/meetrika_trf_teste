/*
               File: LoadAuditContratoServicos
        Description: Load Audit Contrato Servicos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:3:58.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class loadauditcontratoservicos : GXProcedure
   {
      public loadauditcontratoservicos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public loadauditcontratoservicos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_SaveOldValues ,
                           ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                           int aP2_ContratoServicos_Codigo ,
                           String aP3_ActualMode )
      {
         this.AV13SaveOldValues = aP0_SaveOldValues;
         this.AV10AuditingObject = aP1_AuditingObject;
         this.AV16ContratoServicos_Codigo = aP2_ContratoServicos_Codigo;
         this.AV14ActualMode = aP3_ActualMode;
         initialize();
         executePrivate();
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      public void executeSubmit( String aP0_SaveOldValues ,
                                 ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                                 int aP2_ContratoServicos_Codigo ,
                                 String aP3_ActualMode )
      {
         loadauditcontratoservicos objloadauditcontratoservicos;
         objloadauditcontratoservicos = new loadauditcontratoservicos();
         objloadauditcontratoservicos.AV13SaveOldValues = aP0_SaveOldValues;
         objloadauditcontratoservicos.AV10AuditingObject = aP1_AuditingObject;
         objloadauditcontratoservicos.AV16ContratoServicos_Codigo = aP2_ContratoServicos_Codigo;
         objloadauditcontratoservicos.AV14ActualMode = aP3_ActualMode;
         objloadauditcontratoservicos.context.SetSubmitInitialConfig(context);
         objloadauditcontratoservicos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objloadauditcontratoservicos);
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((loadauditcontratoservicos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV13SaveOldValues, "Y") == 0 )
         {
            if ( ( StringUtil.StrCmp(AV14ActualMode, "DLT") == 0 ) || ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 ) )
            {
               /* Execute user subroutine: 'LOADOLDVALUES' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'LOADNEWVALUES' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADOLDVALUES' Routine */
         /* Using cursor P00A55 */
         pr_default.execute(0, new Object[] {AV16ContratoServicos_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A74Contrato_Codigo = P00A55_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00A55_A77Contrato_Numero[0];
            A78Contrato_NumeroAta = P00A55_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = P00A55_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = P00A55_A79Contrato_Ano[0];
            A116Contrato_ValorUnidadeContratacao = P00A55_A116Contrato_ValorUnidadeContratacao[0];
            A75Contrato_AreaTrabalhoCod = P00A55_A75Contrato_AreaTrabalhoCod[0];
            A39Contratada_Codigo = P00A55_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00A55_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00A55_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00A55_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P00A55_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00A55_n42Contratada_PessoaCNPJ[0];
            A516Contratada_TipoFabrica = P00A55_A516Contratada_TipoFabrica[0];
            A1858ContratoServicos_Alias = P00A55_A1858ContratoServicos_Alias[0];
            n1858ContratoServicos_Alias = P00A55_n1858ContratoServicos_Alias[0];
            A1061Servico_Tela = P00A55_A1061Servico_Tela[0];
            n1061Servico_Tela = P00A55_n1061Servico_Tela[0];
            A632Servico_Ativo = P00A55_A632Servico_Ativo[0];
            A156Servico_Descricao = P00A55_A156Servico_Descricao[0];
            n156Servico_Descricao = P00A55_n156Servico_Descricao[0];
            A631Servico_Vinculado = P00A55_A631Servico_Vinculado[0];
            n631Servico_Vinculado = P00A55_n631Servico_Vinculado[0];
            A157ServicoGrupo_Codigo = P00A55_A157ServicoGrupo_Codigo[0];
            A158ServicoGrupo_Descricao = P00A55_A158ServicoGrupo_Descricao[0];
            A2092Servico_IsOrigemReferencia = P00A55_A2092Servico_IsOrigemReferencia[0];
            A1212ContratoServicos_UnidadeContratada = P00A55_A1212ContratoServicos_UnidadeContratada[0];
            A2132ContratoServicos_UndCntNome = P00A55_A2132ContratoServicos_UndCntNome[0];
            n2132ContratoServicos_UndCntNome = P00A55_n2132ContratoServicos_UndCntNome[0];
            A1712ContratoServicos_UndCntSgl = P00A55_A1712ContratoServicos_UndCntSgl[0];
            n1712ContratoServicos_UndCntSgl = P00A55_n1712ContratoServicos_UndCntSgl[0];
            A555Servico_QtdContratada = P00A55_A555Servico_QtdContratada[0];
            n555Servico_QtdContratada = P00A55_n555Servico_QtdContratada[0];
            A557Servico_VlrUnidadeContratada = P00A55_A557Servico_VlrUnidadeContratada[0];
            A558Servico_Percentual = P00A55_A558Servico_Percentual[0];
            n558Servico_Percentual = P00A55_n558Servico_Percentual[0];
            A607ServicoContrato_Faturamento = P00A55_A607ServicoContrato_Faturamento[0];
            A639ContratoServicos_LocalExec = P00A55_A639ContratoServicos_LocalExec[0];
            A868ContratoServicos_TipoVnc = P00A55_A868ContratoServicos_TipoVnc[0];
            n868ContratoServicos_TipoVnc = P00A55_n868ContratoServicos_TipoVnc[0];
            A888ContratoServicos_HmlSemCnf = P00A55_A888ContratoServicos_HmlSemCnf[0];
            n888ContratoServicos_HmlSemCnf = P00A55_n888ContratoServicos_HmlSemCnf[0];
            A1454ContratoServicos_PrazoTpDias = P00A55_A1454ContratoServicos_PrazoTpDias[0];
            n1454ContratoServicos_PrazoTpDias = P00A55_n1454ContratoServicos_PrazoTpDias[0];
            A1152ContratoServicos_PrazoAnalise = P00A55_A1152ContratoServicos_PrazoAnalise[0];
            n1152ContratoServicos_PrazoAnalise = P00A55_n1152ContratoServicos_PrazoAnalise[0];
            A1153ContratoServicos_PrazoResposta = P00A55_A1153ContratoServicos_PrazoResposta[0];
            n1153ContratoServicos_PrazoResposta = P00A55_n1153ContratoServicos_PrazoResposta[0];
            A1181ContratoServicos_PrazoGarantia = P00A55_A1181ContratoServicos_PrazoGarantia[0];
            n1181ContratoServicos_PrazoGarantia = P00A55_n1181ContratoServicos_PrazoGarantia[0];
            A1182ContratoServicos_PrazoAtendeGarantia = P00A55_A1182ContratoServicos_PrazoAtendeGarantia[0];
            n1182ContratoServicos_PrazoAtendeGarantia = P00A55_n1182ContratoServicos_PrazoAtendeGarantia[0];
            A1224ContratoServicos_PrazoCorrecao = P00A55_A1224ContratoServicos_PrazoCorrecao[0];
            n1224ContratoServicos_PrazoCorrecao = P00A55_n1224ContratoServicos_PrazoCorrecao[0];
            A1225ContratoServicos_PrazoCorrecaoTipo = P00A55_A1225ContratoServicos_PrazoCorrecaoTipo[0];
            n1225ContratoServicos_PrazoCorrecaoTipo = P00A55_n1225ContratoServicos_PrazoCorrecaoTipo[0];
            A1649ContratoServicos_PrazoInicio = P00A55_A1649ContratoServicos_PrazoInicio[0];
            n1649ContratoServicos_PrazoInicio = P00A55_n1649ContratoServicos_PrazoInicio[0];
            A1190ContratoServicos_PrazoImediato = P00A55_A1190ContratoServicos_PrazoImediato[0];
            n1190ContratoServicos_PrazoImediato = P00A55_n1190ContratoServicos_PrazoImediato[0];
            A1191ContratoServicos_Produtividade = P00A55_A1191ContratoServicos_Produtividade[0];
            n1191ContratoServicos_Produtividade = P00A55_n1191ContratoServicos_Produtividade[0];
            A1217ContratoServicos_EspelhaAceite = P00A55_A1217ContratoServicos_EspelhaAceite[0];
            n1217ContratoServicos_EspelhaAceite = P00A55_n1217ContratoServicos_EspelhaAceite[0];
            A1266ContratoServicos_Momento = P00A55_A1266ContratoServicos_Momento[0];
            n1266ContratoServicos_Momento = P00A55_n1266ContratoServicos_Momento[0];
            A1325ContratoServicos_StatusPagFnc = P00A55_A1325ContratoServicos_StatusPagFnc[0];
            n1325ContratoServicos_StatusPagFnc = P00A55_n1325ContratoServicos_StatusPagFnc[0];
            A1340ContratoServicos_QntUntCns = P00A55_A1340ContratoServicos_QntUntCns[0];
            n1340ContratoServicos_QntUntCns = P00A55_n1340ContratoServicos_QntUntCns[0];
            A1341ContratoServicos_FatorCnvUndCnt = P00A55_A1341ContratoServicos_FatorCnvUndCnt[0];
            n1341ContratoServicos_FatorCnvUndCnt = P00A55_n1341ContratoServicos_FatorCnvUndCnt[0];
            A1397ContratoServicos_NaoRequerAtr = P00A55_A1397ContratoServicos_NaoRequerAtr[0];
            n1397ContratoServicos_NaoRequerAtr = P00A55_n1397ContratoServicos_NaoRequerAtr[0];
            A453Contrato_IndiceDivergencia = P00A55_A453Contrato_IndiceDivergencia[0];
            A1455ContratoServicos_IndiceDivergencia = P00A55_A1455ContratoServicos_IndiceDivergencia[0];
            n1455ContratoServicos_IndiceDivergencia = P00A55_n1455ContratoServicos_IndiceDivergencia[0];
            A1516ContratoServicos_TmpEstAnl = P00A55_A1516ContratoServicos_TmpEstAnl[0];
            n1516ContratoServicos_TmpEstAnl = P00A55_n1516ContratoServicos_TmpEstAnl[0];
            A1501ContratoServicos_TmpEstExc = P00A55_A1501ContratoServicos_TmpEstExc[0];
            n1501ContratoServicos_TmpEstExc = P00A55_n1501ContratoServicos_TmpEstExc[0];
            A1502ContratoServicos_TmpEstCrr = P00A55_A1502ContratoServicos_TmpEstCrr[0];
            n1502ContratoServicos_TmpEstCrr = P00A55_n1502ContratoServicos_TmpEstCrr[0];
            A1531ContratoServicos_TipoHierarquia = P00A55_A1531ContratoServicos_TipoHierarquia[0];
            n1531ContratoServicos_TipoHierarquia = P00A55_n1531ContratoServicos_TipoHierarquia[0];
            A1537ContratoServicos_PercTmp = P00A55_A1537ContratoServicos_PercTmp[0];
            n1537ContratoServicos_PercTmp = P00A55_n1537ContratoServicos_PercTmp[0];
            A1538ContratoServicos_PercPgm = P00A55_A1538ContratoServicos_PercPgm[0];
            n1538ContratoServicos_PercPgm = P00A55_n1538ContratoServicos_PercPgm[0];
            A1539ContratoServicos_PercCnc = P00A55_A1539ContratoServicos_PercCnc[0];
            n1539ContratoServicos_PercCnc = P00A55_n1539ContratoServicos_PercCnc[0];
            A638ContratoServicos_Ativo = P00A55_A638ContratoServicos_Ativo[0];
            A1723ContratoServicos_CodigoFiscal = P00A55_A1723ContratoServicos_CodigoFiscal[0];
            n1723ContratoServicos_CodigoFiscal = P00A55_n1723ContratoServicos_CodigoFiscal[0];
            A1799ContratoServicos_LimiteProposta = P00A55_A1799ContratoServicos_LimiteProposta[0];
            n1799ContratoServicos_LimiteProposta = P00A55_n1799ContratoServicos_LimiteProposta[0];
            A2074ContratoServicos_CalculoRmn = P00A55_A2074ContratoServicos_CalculoRmn[0];
            n2074ContratoServicos_CalculoRmn = P00A55_n2074ContratoServicos_CalculoRmn[0];
            A2094ContratoServicos_SolicitaGestorSistema = P00A55_A2094ContratoServicos_SolicitaGestorSistema[0];
            A913ContratoServicos_PrazoTipo = P00A55_A913ContratoServicos_PrazoTipo[0];
            n913ContratoServicos_PrazoTipo = P00A55_n913ContratoServicos_PrazoTipo[0];
            A914ContratoServicos_PrazoDias = P00A55_A914ContratoServicos_PrazoDias[0];
            n914ContratoServicos_PrazoDias = P00A55_n914ContratoServicos_PrazoDias[0];
            A911ContratoServicos_Prazos = P00A55_A911ContratoServicos_Prazos[0];
            n911ContratoServicos_Prazos = P00A55_n911ContratoServicos_Prazos[0];
            A160ContratoServicos_Codigo = P00A55_A160ContratoServicos_Codigo[0];
            A1377ContratoServicos_Indicadores = P00A55_A1377ContratoServicos_Indicadores[0];
            n1377ContratoServicos_Indicadores = P00A55_n1377ContratoServicos_Indicadores[0];
            A2073ContratoServicos_QtdRmn = P00A55_A2073ContratoServicos_QtdRmn[0];
            n2073ContratoServicos_QtdRmn = P00A55_n2073ContratoServicos_QtdRmn[0];
            A605Servico_Sigla = P00A55_A605Servico_Sigla[0];
            A608Servico_Nome = P00A55_A608Servico_Nome[0];
            A155Servico_Codigo = P00A55_A155Servico_Codigo[0];
            A77Contrato_Numero = P00A55_A77Contrato_Numero[0];
            A78Contrato_NumeroAta = P00A55_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = P00A55_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = P00A55_A79Contrato_Ano[0];
            A116Contrato_ValorUnidadeContratacao = P00A55_A116Contrato_ValorUnidadeContratacao[0];
            A75Contrato_AreaTrabalhoCod = P00A55_A75Contrato_AreaTrabalhoCod[0];
            A39Contratada_Codigo = P00A55_A39Contratada_Codigo[0];
            A453Contrato_IndiceDivergencia = P00A55_A453Contrato_IndiceDivergencia[0];
            A40Contratada_PessoaCod = P00A55_A40Contratada_PessoaCod[0];
            A516Contratada_TipoFabrica = P00A55_A516Contratada_TipoFabrica[0];
            A41Contratada_PessoaNom = P00A55_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00A55_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P00A55_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00A55_n42Contratada_PessoaCNPJ[0];
            A2132ContratoServicos_UndCntNome = P00A55_A2132ContratoServicos_UndCntNome[0];
            n2132ContratoServicos_UndCntNome = P00A55_n2132ContratoServicos_UndCntNome[0];
            A1712ContratoServicos_UndCntSgl = P00A55_A1712ContratoServicos_UndCntSgl[0];
            n1712ContratoServicos_UndCntSgl = P00A55_n1712ContratoServicos_UndCntSgl[0];
            A913ContratoServicos_PrazoTipo = P00A55_A913ContratoServicos_PrazoTipo[0];
            n913ContratoServicos_PrazoTipo = P00A55_n913ContratoServicos_PrazoTipo[0];
            A914ContratoServicos_PrazoDias = P00A55_A914ContratoServicos_PrazoDias[0];
            n914ContratoServicos_PrazoDias = P00A55_n914ContratoServicos_PrazoDias[0];
            A911ContratoServicos_Prazos = P00A55_A911ContratoServicos_Prazos[0];
            n911ContratoServicos_Prazos = P00A55_n911ContratoServicos_Prazos[0];
            A1377ContratoServicos_Indicadores = P00A55_A1377ContratoServicos_Indicadores[0];
            n1377ContratoServicos_Indicadores = P00A55_n1377ContratoServicos_Indicadores[0];
            A2073ContratoServicos_QtdRmn = P00A55_A2073ContratoServicos_QtdRmn[0];
            n2073ContratoServicos_QtdRmn = P00A55_n2073ContratoServicos_QtdRmn[0];
            A1061Servico_Tela = P00A55_A1061Servico_Tela[0];
            n1061Servico_Tela = P00A55_n1061Servico_Tela[0];
            A632Servico_Ativo = P00A55_A632Servico_Ativo[0];
            A156Servico_Descricao = P00A55_A156Servico_Descricao[0];
            n156Servico_Descricao = P00A55_n156Servico_Descricao[0];
            A631Servico_Vinculado = P00A55_A631Servico_Vinculado[0];
            n631Servico_Vinculado = P00A55_n631Servico_Vinculado[0];
            A157ServicoGrupo_Codigo = P00A55_A157ServicoGrupo_Codigo[0];
            A2092Servico_IsOrigemReferencia = P00A55_A2092Servico_IsOrigemReferencia[0];
            A605Servico_Sigla = P00A55_A605Servico_Sigla[0];
            A608Servico_Nome = P00A55_A608Servico_Nome[0];
            A158ServicoGrupo_Descricao = P00A55_A158ServicoGrupo_Descricao[0];
            GXt_int1 = A1551Servico_Responsavel;
            new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int1) ;
            A1551Servico_Responsavel = GXt_int1;
            GXt_int2 = A640Servico_Vinculados;
            new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int2) ;
            A640Servico_Vinculados = GXt_int2;
            A827ContratoServicos_ServicoCod = A155Servico_Codigo;
            A826ContratoServicos_ServicoSigla = A605Servico_Sigla;
            A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
            AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
            AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
            AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
            AV11AuditingObjectRecordItem.gxTpr_Tablename = "ContratoServicos";
            AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
            AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo do Servi�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Numero";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N� do Contrato";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A77Contrato_Numero;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_NumeroAta";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N� da Ata";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A78Contrato_NumeroAta;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Ano";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ano";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_ValorUnidadeContratacao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor da Unidade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_AreaTrabalhoCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�d. �rea de Trabalho";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�d. da Pessoa";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaNom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A41Contratada_PessoaNom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaCNPJ";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "CNPJ";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A42Contratada_PessoaCNPJ;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_TipoFabrica";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A516Contratada_TipoFabrica;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_Alias";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Alias";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1858ContratoServicos_Alias;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_ServicoCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato Servicos_Servico Cod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A827ContratoServicos_ServicoCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A608Servico_Nome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A605Servico_Sigla;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Tela";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tela";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1061Servico_Tela;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Ativo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A632Servico_Ativo);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Identificacao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Identifica��o do Servi�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2107Servico_Identificacao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_ServicoSigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A826ContratoServicos_ServicoSigla;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Descricao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A156Servico_Descricao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Vinculado";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o vinculado";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Vinculados";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servico_Vinculados";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Responsavel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ServicoGrupo_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Grupo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ServicoGrupo_Descricao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Grupo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A158ServicoGrupo_Descricao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_IsOrigemReferencia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Exige Origem de Refer�ncia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_UnidadeContratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidade Contratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_UndCntNome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidade Contratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2132ContratoServicos_UndCntNome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_UndCntSgl";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato Servicos_Und Cnt Sgl";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1712ContratoServicos_UndCntSgl;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_QtdContratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Qtde. Contratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A555Servico_QtdContratada), 10, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_VlrUnidadeContratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor da Unidade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Percentual";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Deflator";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A558Servico_Percentual, 7, 3);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ServicoContrato_Faturamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Faturamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A607ServicoContrato_Faturamento;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_LocalExec";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Local de Execu��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A639ContratoServicos_LocalExec;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_TipoVnc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de Vinculo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A868ContratoServicos_TipoVnc;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_HmlSemCnf";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Homologa sem conferir";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A888ContratoServicos_HmlSemCnf);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoTipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Execu��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A913ContratoServicos_PrazoTipo;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoDias";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A914ContratoServicos_PrazoDias), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoTpDias";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de dias";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1454ContratoServicos_PrazoTpDias;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_Prazos";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazos";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A911ContratoServicos_Prazos), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_Indicadores";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indicadores";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1377ContratoServicos_Indicadores), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoAnalise";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "����������������������An�lise";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1152ContratoServicos_PrazoAnalise), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoResposta";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "������������������Resposta";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1153ContratoServicos_PrazoResposta), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoGarantia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Garantia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1181ContratoServicos_PrazoGarantia), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoAtendeGarantia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "������������Execu��o da Gtia.";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoCorrecao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoCorrecaoTipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Corre��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1225ContratoServicos_PrazoCorrecaoTipo;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoInicio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoImediato";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio dos Prazos";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1190ContratoServicos_PrazoImediato);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_Produtividade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Produtividade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1191ContratoServicos_Produtividade, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_EspelhaAceite";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Espelhar Aceite na FS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1217ContratoServicos_EspelhaAceite);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_Momento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Momento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1266ContratoServicos_Momento;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_StatusPagFnc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pagar em";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1325ContratoServicos_StatusPagFnc;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_QntUntCns";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Qtde. Unit. de Consumo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1340ContratoServicos_QntUntCns, 9, 4);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_FatorCnvUndCnt";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fator de convers�o em horas";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1341ContratoServicos_FatorCnvUndCnt, 9, 4);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_NaoRequerAtr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�o requer atribui��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1397ContratoServicos_NaoRequerAtr);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_IndiceDivergencia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indice de Aceita��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_IndiceDivergencia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indice de Aceita��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1455ContratoServicos_IndiceDivergencia, 6, 2);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_TmpEstAnl";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Estimado de an�lise";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1516ContratoServicos_TmpEstAnl), 8, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_TmpEstExc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Estimado de execu��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1501ContratoServicos_TmpEstExc), 8, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_TmpEstCrr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Estimado de corre��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1502ContratoServicos_TmpEstCrr), 8, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_TipoHierarquia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de Hierarquia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1531ContratoServicos_TipoHierarquia), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PercTmp";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Perc. de Tempo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1537ContratoServicos_PercTmp), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PercPgm";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Perc. de Pagamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1538ContratoServicos_PercPgm), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PercCnc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor do Cancelamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1539ContratoServicos_PercCnc), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_Ativo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A638ContratoServicos_Ativo);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_CodigoFiscal";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo Fiscal";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1723ContratoServicos_CodigoFiscal;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_LimiteProposta";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Limite das Propostas";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1799ContratoServicos_LimiteProposta, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_CalculoRmn";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�lculo da Remunera��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_QtdRmn";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato Servicos_Qtd Rmn";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_SolicitaGestorSistema";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Exibir Gestor Respons�vel pelo Sistema";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A2094ContratoServicos_SolicitaGestorSistema);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            /* Using cursor P00A56 */
            pr_default.execute(1, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A2069ContratoServicosRmn_Sequencial = P00A56_A2069ContratoServicosRmn_Sequencial[0];
               A2070ContratoServicosRmn_Inicio = P00A56_A2070ContratoServicosRmn_Inicio[0];
               A2071ContratoServicosRmn_Fim = P00A56_A2071ContratoServicosRmn_Fim[0];
               A2072ContratoServicosRmn_Valor = P00A56_A2072ContratoServicosRmn_Valor[0];
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "ContratoServicosRmn";
               AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosRmn_Sequencial";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2069ContratoServicosRmn_Sequencial), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosRmn_Inicio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Desde";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A2070ContratoServicosRmn_Inicio, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosRmn_Fim";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "At�";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A2071ContratoServicosRmn_Fim, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosRmn_Valor";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A2072ContratoServicosRmn_Valor, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
      }

      protected void S121( )
      {
         /* 'LOADNEWVALUES' Routine */
         /* Using cursor P00A511 */
         pr_default.execute(2, new Object[] {AV16ContratoServicos_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A74Contrato_Codigo = P00A511_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00A511_A77Contrato_Numero[0];
            A78Contrato_NumeroAta = P00A511_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = P00A511_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = P00A511_A79Contrato_Ano[0];
            A116Contrato_ValorUnidadeContratacao = P00A511_A116Contrato_ValorUnidadeContratacao[0];
            A75Contrato_AreaTrabalhoCod = P00A511_A75Contrato_AreaTrabalhoCod[0];
            A39Contratada_Codigo = P00A511_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00A511_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00A511_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00A511_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P00A511_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00A511_n42Contratada_PessoaCNPJ[0];
            A516Contratada_TipoFabrica = P00A511_A516Contratada_TipoFabrica[0];
            A1858ContratoServicos_Alias = P00A511_A1858ContratoServicos_Alias[0];
            n1858ContratoServicos_Alias = P00A511_n1858ContratoServicos_Alias[0];
            A1061Servico_Tela = P00A511_A1061Servico_Tela[0];
            n1061Servico_Tela = P00A511_n1061Servico_Tela[0];
            A632Servico_Ativo = P00A511_A632Servico_Ativo[0];
            A156Servico_Descricao = P00A511_A156Servico_Descricao[0];
            n156Servico_Descricao = P00A511_n156Servico_Descricao[0];
            A631Servico_Vinculado = P00A511_A631Servico_Vinculado[0];
            n631Servico_Vinculado = P00A511_n631Servico_Vinculado[0];
            A157ServicoGrupo_Codigo = P00A511_A157ServicoGrupo_Codigo[0];
            A158ServicoGrupo_Descricao = P00A511_A158ServicoGrupo_Descricao[0];
            A2092Servico_IsOrigemReferencia = P00A511_A2092Servico_IsOrigemReferencia[0];
            A1212ContratoServicos_UnidadeContratada = P00A511_A1212ContratoServicos_UnidadeContratada[0];
            A2132ContratoServicos_UndCntNome = P00A511_A2132ContratoServicos_UndCntNome[0];
            n2132ContratoServicos_UndCntNome = P00A511_n2132ContratoServicos_UndCntNome[0];
            A1712ContratoServicos_UndCntSgl = P00A511_A1712ContratoServicos_UndCntSgl[0];
            n1712ContratoServicos_UndCntSgl = P00A511_n1712ContratoServicos_UndCntSgl[0];
            A555Servico_QtdContratada = P00A511_A555Servico_QtdContratada[0];
            n555Servico_QtdContratada = P00A511_n555Servico_QtdContratada[0];
            A557Servico_VlrUnidadeContratada = P00A511_A557Servico_VlrUnidadeContratada[0];
            A558Servico_Percentual = P00A511_A558Servico_Percentual[0];
            n558Servico_Percentual = P00A511_n558Servico_Percentual[0];
            A607ServicoContrato_Faturamento = P00A511_A607ServicoContrato_Faturamento[0];
            A639ContratoServicos_LocalExec = P00A511_A639ContratoServicos_LocalExec[0];
            A868ContratoServicos_TipoVnc = P00A511_A868ContratoServicos_TipoVnc[0];
            n868ContratoServicos_TipoVnc = P00A511_n868ContratoServicos_TipoVnc[0];
            A888ContratoServicos_HmlSemCnf = P00A511_A888ContratoServicos_HmlSemCnf[0];
            n888ContratoServicos_HmlSemCnf = P00A511_n888ContratoServicos_HmlSemCnf[0];
            A1454ContratoServicos_PrazoTpDias = P00A511_A1454ContratoServicos_PrazoTpDias[0];
            n1454ContratoServicos_PrazoTpDias = P00A511_n1454ContratoServicos_PrazoTpDias[0];
            A1152ContratoServicos_PrazoAnalise = P00A511_A1152ContratoServicos_PrazoAnalise[0];
            n1152ContratoServicos_PrazoAnalise = P00A511_n1152ContratoServicos_PrazoAnalise[0];
            A1153ContratoServicos_PrazoResposta = P00A511_A1153ContratoServicos_PrazoResposta[0];
            n1153ContratoServicos_PrazoResposta = P00A511_n1153ContratoServicos_PrazoResposta[0];
            A1181ContratoServicos_PrazoGarantia = P00A511_A1181ContratoServicos_PrazoGarantia[0];
            n1181ContratoServicos_PrazoGarantia = P00A511_n1181ContratoServicos_PrazoGarantia[0];
            A1182ContratoServicos_PrazoAtendeGarantia = P00A511_A1182ContratoServicos_PrazoAtendeGarantia[0];
            n1182ContratoServicos_PrazoAtendeGarantia = P00A511_n1182ContratoServicos_PrazoAtendeGarantia[0];
            A1224ContratoServicos_PrazoCorrecao = P00A511_A1224ContratoServicos_PrazoCorrecao[0];
            n1224ContratoServicos_PrazoCorrecao = P00A511_n1224ContratoServicos_PrazoCorrecao[0];
            A1225ContratoServicos_PrazoCorrecaoTipo = P00A511_A1225ContratoServicos_PrazoCorrecaoTipo[0];
            n1225ContratoServicos_PrazoCorrecaoTipo = P00A511_n1225ContratoServicos_PrazoCorrecaoTipo[0];
            A1649ContratoServicos_PrazoInicio = P00A511_A1649ContratoServicos_PrazoInicio[0];
            n1649ContratoServicos_PrazoInicio = P00A511_n1649ContratoServicos_PrazoInicio[0];
            A1190ContratoServicos_PrazoImediato = P00A511_A1190ContratoServicos_PrazoImediato[0];
            n1190ContratoServicos_PrazoImediato = P00A511_n1190ContratoServicos_PrazoImediato[0];
            A1191ContratoServicos_Produtividade = P00A511_A1191ContratoServicos_Produtividade[0];
            n1191ContratoServicos_Produtividade = P00A511_n1191ContratoServicos_Produtividade[0];
            A1217ContratoServicos_EspelhaAceite = P00A511_A1217ContratoServicos_EspelhaAceite[0];
            n1217ContratoServicos_EspelhaAceite = P00A511_n1217ContratoServicos_EspelhaAceite[0];
            A1266ContratoServicos_Momento = P00A511_A1266ContratoServicos_Momento[0];
            n1266ContratoServicos_Momento = P00A511_n1266ContratoServicos_Momento[0];
            A1325ContratoServicos_StatusPagFnc = P00A511_A1325ContratoServicos_StatusPagFnc[0];
            n1325ContratoServicos_StatusPagFnc = P00A511_n1325ContratoServicos_StatusPagFnc[0];
            A1340ContratoServicos_QntUntCns = P00A511_A1340ContratoServicos_QntUntCns[0];
            n1340ContratoServicos_QntUntCns = P00A511_n1340ContratoServicos_QntUntCns[0];
            A1341ContratoServicos_FatorCnvUndCnt = P00A511_A1341ContratoServicos_FatorCnvUndCnt[0];
            n1341ContratoServicos_FatorCnvUndCnt = P00A511_n1341ContratoServicos_FatorCnvUndCnt[0];
            A1397ContratoServicos_NaoRequerAtr = P00A511_A1397ContratoServicos_NaoRequerAtr[0];
            n1397ContratoServicos_NaoRequerAtr = P00A511_n1397ContratoServicos_NaoRequerAtr[0];
            A453Contrato_IndiceDivergencia = P00A511_A453Contrato_IndiceDivergencia[0];
            A1455ContratoServicos_IndiceDivergencia = P00A511_A1455ContratoServicos_IndiceDivergencia[0];
            n1455ContratoServicos_IndiceDivergencia = P00A511_n1455ContratoServicos_IndiceDivergencia[0];
            A1516ContratoServicos_TmpEstAnl = P00A511_A1516ContratoServicos_TmpEstAnl[0];
            n1516ContratoServicos_TmpEstAnl = P00A511_n1516ContratoServicos_TmpEstAnl[0];
            A1501ContratoServicos_TmpEstExc = P00A511_A1501ContratoServicos_TmpEstExc[0];
            n1501ContratoServicos_TmpEstExc = P00A511_n1501ContratoServicos_TmpEstExc[0];
            A1502ContratoServicos_TmpEstCrr = P00A511_A1502ContratoServicos_TmpEstCrr[0];
            n1502ContratoServicos_TmpEstCrr = P00A511_n1502ContratoServicos_TmpEstCrr[0];
            A1531ContratoServicos_TipoHierarquia = P00A511_A1531ContratoServicos_TipoHierarquia[0];
            n1531ContratoServicos_TipoHierarquia = P00A511_n1531ContratoServicos_TipoHierarquia[0];
            A1537ContratoServicos_PercTmp = P00A511_A1537ContratoServicos_PercTmp[0];
            n1537ContratoServicos_PercTmp = P00A511_n1537ContratoServicos_PercTmp[0];
            A1538ContratoServicos_PercPgm = P00A511_A1538ContratoServicos_PercPgm[0];
            n1538ContratoServicos_PercPgm = P00A511_n1538ContratoServicos_PercPgm[0];
            A1539ContratoServicos_PercCnc = P00A511_A1539ContratoServicos_PercCnc[0];
            n1539ContratoServicos_PercCnc = P00A511_n1539ContratoServicos_PercCnc[0];
            A638ContratoServicos_Ativo = P00A511_A638ContratoServicos_Ativo[0];
            A1723ContratoServicos_CodigoFiscal = P00A511_A1723ContratoServicos_CodigoFiscal[0];
            n1723ContratoServicos_CodigoFiscal = P00A511_n1723ContratoServicos_CodigoFiscal[0];
            A1799ContratoServicos_LimiteProposta = P00A511_A1799ContratoServicos_LimiteProposta[0];
            n1799ContratoServicos_LimiteProposta = P00A511_n1799ContratoServicos_LimiteProposta[0];
            A2074ContratoServicos_CalculoRmn = P00A511_A2074ContratoServicos_CalculoRmn[0];
            n2074ContratoServicos_CalculoRmn = P00A511_n2074ContratoServicos_CalculoRmn[0];
            A2094ContratoServicos_SolicitaGestorSistema = P00A511_A2094ContratoServicos_SolicitaGestorSistema[0];
            A913ContratoServicos_PrazoTipo = P00A511_A913ContratoServicos_PrazoTipo[0];
            n913ContratoServicos_PrazoTipo = P00A511_n913ContratoServicos_PrazoTipo[0];
            A914ContratoServicos_PrazoDias = P00A511_A914ContratoServicos_PrazoDias[0];
            n914ContratoServicos_PrazoDias = P00A511_n914ContratoServicos_PrazoDias[0];
            A911ContratoServicos_Prazos = P00A511_A911ContratoServicos_Prazos[0];
            n911ContratoServicos_Prazos = P00A511_n911ContratoServicos_Prazos[0];
            A160ContratoServicos_Codigo = P00A511_A160ContratoServicos_Codigo[0];
            A1377ContratoServicos_Indicadores = P00A511_A1377ContratoServicos_Indicadores[0];
            n1377ContratoServicos_Indicadores = P00A511_n1377ContratoServicos_Indicadores[0];
            A2073ContratoServicos_QtdRmn = P00A511_A2073ContratoServicos_QtdRmn[0];
            n2073ContratoServicos_QtdRmn = P00A511_n2073ContratoServicos_QtdRmn[0];
            A40000ContratoServicos_QtdRmn = P00A511_A40000ContratoServicos_QtdRmn[0];
            n40000ContratoServicos_QtdRmn = P00A511_n40000ContratoServicos_QtdRmn[0];
            A605Servico_Sigla = P00A511_A605Servico_Sigla[0];
            A608Servico_Nome = P00A511_A608Servico_Nome[0];
            A155Servico_Codigo = P00A511_A155Servico_Codigo[0];
            A77Contrato_Numero = P00A511_A77Contrato_Numero[0];
            A78Contrato_NumeroAta = P00A511_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = P00A511_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = P00A511_A79Contrato_Ano[0];
            A116Contrato_ValorUnidadeContratacao = P00A511_A116Contrato_ValorUnidadeContratacao[0];
            A75Contrato_AreaTrabalhoCod = P00A511_A75Contrato_AreaTrabalhoCod[0];
            A39Contratada_Codigo = P00A511_A39Contratada_Codigo[0];
            A453Contrato_IndiceDivergencia = P00A511_A453Contrato_IndiceDivergencia[0];
            A40Contratada_PessoaCod = P00A511_A40Contratada_PessoaCod[0];
            A516Contratada_TipoFabrica = P00A511_A516Contratada_TipoFabrica[0];
            A41Contratada_PessoaNom = P00A511_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00A511_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P00A511_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00A511_n42Contratada_PessoaCNPJ[0];
            A2132ContratoServicos_UndCntNome = P00A511_A2132ContratoServicos_UndCntNome[0];
            n2132ContratoServicos_UndCntNome = P00A511_n2132ContratoServicos_UndCntNome[0];
            A1712ContratoServicos_UndCntSgl = P00A511_A1712ContratoServicos_UndCntSgl[0];
            n1712ContratoServicos_UndCntSgl = P00A511_n1712ContratoServicos_UndCntSgl[0];
            A913ContratoServicos_PrazoTipo = P00A511_A913ContratoServicos_PrazoTipo[0];
            n913ContratoServicos_PrazoTipo = P00A511_n913ContratoServicos_PrazoTipo[0];
            A914ContratoServicos_PrazoDias = P00A511_A914ContratoServicos_PrazoDias[0];
            n914ContratoServicos_PrazoDias = P00A511_n914ContratoServicos_PrazoDias[0];
            A911ContratoServicos_Prazos = P00A511_A911ContratoServicos_Prazos[0];
            n911ContratoServicos_Prazos = P00A511_n911ContratoServicos_Prazos[0];
            A1377ContratoServicos_Indicadores = P00A511_A1377ContratoServicos_Indicadores[0];
            n1377ContratoServicos_Indicadores = P00A511_n1377ContratoServicos_Indicadores[0];
            A2073ContratoServicos_QtdRmn = P00A511_A2073ContratoServicos_QtdRmn[0];
            n2073ContratoServicos_QtdRmn = P00A511_n2073ContratoServicos_QtdRmn[0];
            A1061Servico_Tela = P00A511_A1061Servico_Tela[0];
            n1061Servico_Tela = P00A511_n1061Servico_Tela[0];
            A632Servico_Ativo = P00A511_A632Servico_Ativo[0];
            A156Servico_Descricao = P00A511_A156Servico_Descricao[0];
            n156Servico_Descricao = P00A511_n156Servico_Descricao[0];
            A631Servico_Vinculado = P00A511_A631Servico_Vinculado[0];
            n631Servico_Vinculado = P00A511_n631Servico_Vinculado[0];
            A157ServicoGrupo_Codigo = P00A511_A157ServicoGrupo_Codigo[0];
            A2092Servico_IsOrigemReferencia = P00A511_A2092Servico_IsOrigemReferencia[0];
            A605Servico_Sigla = P00A511_A605Servico_Sigla[0];
            A608Servico_Nome = P00A511_A608Servico_Nome[0];
            A158ServicoGrupo_Descricao = P00A511_A158ServicoGrupo_Descricao[0];
            A40000ContratoServicos_QtdRmn = P00A511_A40000ContratoServicos_QtdRmn[0];
            n40000ContratoServicos_QtdRmn = P00A511_n40000ContratoServicos_QtdRmn[0];
            GXt_int1 = A1551Servico_Responsavel;
            new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int1) ;
            A1551Servico_Responsavel = GXt_int1;
            GXt_int2 = A640Servico_Vinculados;
            new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int2) ;
            A640Servico_Vinculados = GXt_int2;
            A827ContratoServicos_ServicoCod = A155Servico_Codigo;
            A826ContratoServicos_ServicoSigla = A605Servico_Sigla;
            A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
            if ( StringUtil.StrCmp(AV14ActualMode, "INS") == 0 )
            {
               AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
               AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "ContratoServicos";
               AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo do Servi�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Numero";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N� do Contrato";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A77Contrato_Numero;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_NumeroAta";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N� da Ata";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A78Contrato_NumeroAta;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Ano";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ano";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_ValorUnidadeContratacao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor da Unidade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_AreaTrabalhoCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�d. �rea de Trabalho";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�d. da Pessoa";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaNom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A41Contratada_PessoaNom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaCNPJ";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "CNPJ";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A42Contratada_PessoaCNPJ;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_TipoFabrica";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A516Contratada_TipoFabrica;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_Alias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Alias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1858ContratoServicos_Alias;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_ServicoCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato Servicos_Servico Cod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A827ContratoServicos_ServicoCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A608Servico_Nome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A605Servico_Sigla;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Tela";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tela";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1061Servico_Tela;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Ativo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A632Servico_Ativo);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Identificacao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Identifica��o do Servi�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2107Servico_Identificacao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_ServicoSigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A826ContratoServicos_ServicoSigla;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Descricao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A156Servico_Descricao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Vinculado";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o vinculado";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Vinculados";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servico_Vinculados";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Responsavel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ServicoGrupo_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Grupo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ServicoGrupo_Descricao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Grupo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A158ServicoGrupo_Descricao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_IsOrigemReferencia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Exige Origem de Refer�ncia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_UnidadeContratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidade Contratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_UndCntNome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidade Contratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2132ContratoServicos_UndCntNome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_UndCntSgl";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato Servicos_Und Cnt Sgl";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1712ContratoServicos_UndCntSgl;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_QtdContratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Qtde. Contratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A555Servico_QtdContratada), 10, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_VlrUnidadeContratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor da Unidade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Percentual";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Deflator";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A558Servico_Percentual, 7, 3);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ServicoContrato_Faturamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Faturamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A607ServicoContrato_Faturamento;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_LocalExec";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Local de Execu��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A639ContratoServicos_LocalExec;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_TipoVnc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de Vinculo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A868ContratoServicos_TipoVnc;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_HmlSemCnf";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Homologa sem conferir";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A888ContratoServicos_HmlSemCnf);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoTipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Execu��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A913ContratoServicos_PrazoTipo;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoDias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A914ContratoServicos_PrazoDias), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoTpDias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de dias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1454ContratoServicos_PrazoTpDias;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_Prazos";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazos";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A911ContratoServicos_Prazos), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_Indicadores";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indicadores";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1377ContratoServicos_Indicadores), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoAnalise";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "����������������������An�lise";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1152ContratoServicos_PrazoAnalise), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoResposta";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "������������������Resposta";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1153ContratoServicos_PrazoResposta), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoGarantia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Garantia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1181ContratoServicos_PrazoGarantia), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoAtendeGarantia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "������������Execu��o da Gtia.";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoCorrecao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoCorrecaoTipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Corre��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1225ContratoServicos_PrazoCorrecaoTipo;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoInicio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PrazoImediato";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio dos Prazos";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1190ContratoServicos_PrazoImediato);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_Produtividade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Produtividade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1191ContratoServicos_Produtividade, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_EspelhaAceite";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Espelhar Aceite na FS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1217ContratoServicos_EspelhaAceite);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_Momento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Momento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1266ContratoServicos_Momento;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_StatusPagFnc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pagar em";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1325ContratoServicos_StatusPagFnc;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_QntUntCns";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Qtde. Unit. de Consumo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1340ContratoServicos_QntUntCns, 9, 4);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_FatorCnvUndCnt";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fator de convers�o em horas";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1341ContratoServicos_FatorCnvUndCnt, 9, 4);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_NaoRequerAtr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�o requer atribui��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1397ContratoServicos_NaoRequerAtr);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_IndiceDivergencia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indice de Aceita��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_IndiceDivergencia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indice de Aceita��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1455ContratoServicos_IndiceDivergencia, 6, 2);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_TmpEstAnl";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Estimado de an�lise";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1516ContratoServicos_TmpEstAnl), 8, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_TmpEstExc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Estimado de execu��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1501ContratoServicos_TmpEstExc), 8, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_TmpEstCrr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Estimado de corre��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1502ContratoServicos_TmpEstCrr), 8, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_TipoHierarquia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de Hierarquia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1531ContratoServicos_TipoHierarquia), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PercTmp";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Perc. de Tempo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1537ContratoServicos_PercTmp), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PercPgm";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Perc. de Pagamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1538ContratoServicos_PercPgm), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_PercCnc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor do Cancelamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1539ContratoServicos_PercCnc), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_Ativo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A638ContratoServicos_Ativo);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_CodigoFiscal";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo Fiscal";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1723ContratoServicos_CodigoFiscal;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_LimiteProposta";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Limite das Propostas";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1799ContratoServicos_LimiteProposta, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_CalculoRmn";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�lculo da Remunera��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_QtdRmn";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato Servicos_Qtd Rmn";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_SolicitaGestorSistema";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Exibir Gestor Respons�vel pelo Sistema";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2094ContratoServicos_SolicitaGestorSistema);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               /* Using cursor P00A512 */
               pr_default.execute(3, new Object[] {A160ContratoServicos_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A2069ContratoServicosRmn_Sequencial = P00A512_A2069ContratoServicosRmn_Sequencial[0];
                  A2070ContratoServicosRmn_Inicio = P00A512_A2070ContratoServicosRmn_Inicio[0];
                  A2071ContratoServicosRmn_Fim = P00A512_A2071ContratoServicosRmn_Fim[0];
                  A2072ContratoServicosRmn_Valor = P00A512_A2072ContratoServicosRmn_Valor[0];
                  AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
                  AV11AuditingObjectRecordItem.gxTpr_Tablename = "ContratoServicosRmn";
                  AV11AuditingObjectRecordItem.gxTpr_Mode = "INS";
                  AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosRmn_Sequencial";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2069ContratoServicosRmn_Sequencial), 4, 0);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosRmn_Inicio";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Desde";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A2070ContratoServicosRmn_Inicio, 14, 5);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosRmn_Fim";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "At�";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A2071ContratoServicosRmn_Fim, 14, 5);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosRmn_Valor";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A2072ContratoServicosRmn_Valor, 14, 5);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  pr_default.readNext(3);
               }
               pr_default.close(3);
            }
            if ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 )
            {
               AV21CountUpdatedContratoServicosRmn_Sequencial = 0;
               AV30GXV1 = 1;
               while ( AV30GXV1 <= AV10AuditingObject.gxTpr_Record.Count )
               {
                  AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV30GXV1));
                  if ( StringUtil.StrCmp(AV11AuditingObjectRecordItem.gxTpr_Tablename, "ContratoServicos") == 0 )
                  {
                     while ( (pr_default.getStatus(2) != 101) && ( P00A511_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) )
                     {
                        A74Contrato_Codigo = P00A511_A74Contrato_Codigo[0];
                        A77Contrato_Numero = P00A511_A77Contrato_Numero[0];
                        A78Contrato_NumeroAta = P00A511_A78Contrato_NumeroAta[0];
                        n78Contrato_NumeroAta = P00A511_n78Contrato_NumeroAta[0];
                        A79Contrato_Ano = P00A511_A79Contrato_Ano[0];
                        A116Contrato_ValorUnidadeContratacao = P00A511_A116Contrato_ValorUnidadeContratacao[0];
                        A75Contrato_AreaTrabalhoCod = P00A511_A75Contrato_AreaTrabalhoCod[0];
                        A39Contratada_Codigo = P00A511_A39Contratada_Codigo[0];
                        A40Contratada_PessoaCod = P00A511_A40Contratada_PessoaCod[0];
                        A41Contratada_PessoaNom = P00A511_A41Contratada_PessoaNom[0];
                        n41Contratada_PessoaNom = P00A511_n41Contratada_PessoaNom[0];
                        A42Contratada_PessoaCNPJ = P00A511_A42Contratada_PessoaCNPJ[0];
                        n42Contratada_PessoaCNPJ = P00A511_n42Contratada_PessoaCNPJ[0];
                        A516Contratada_TipoFabrica = P00A511_A516Contratada_TipoFabrica[0];
                        A1858ContratoServicos_Alias = P00A511_A1858ContratoServicos_Alias[0];
                        n1858ContratoServicos_Alias = P00A511_n1858ContratoServicos_Alias[0];
                        A1061Servico_Tela = P00A511_A1061Servico_Tela[0];
                        n1061Servico_Tela = P00A511_n1061Servico_Tela[0];
                        A632Servico_Ativo = P00A511_A632Servico_Ativo[0];
                        A156Servico_Descricao = P00A511_A156Servico_Descricao[0];
                        n156Servico_Descricao = P00A511_n156Servico_Descricao[0];
                        A631Servico_Vinculado = P00A511_A631Servico_Vinculado[0];
                        n631Servico_Vinculado = P00A511_n631Servico_Vinculado[0];
                        A157ServicoGrupo_Codigo = P00A511_A157ServicoGrupo_Codigo[0];
                        A158ServicoGrupo_Descricao = P00A511_A158ServicoGrupo_Descricao[0];
                        A2092Servico_IsOrigemReferencia = P00A511_A2092Servico_IsOrigemReferencia[0];
                        A1212ContratoServicos_UnidadeContratada = P00A511_A1212ContratoServicos_UnidadeContratada[0];
                        A2132ContratoServicos_UndCntNome = P00A511_A2132ContratoServicos_UndCntNome[0];
                        n2132ContratoServicos_UndCntNome = P00A511_n2132ContratoServicos_UndCntNome[0];
                        A1712ContratoServicos_UndCntSgl = P00A511_A1712ContratoServicos_UndCntSgl[0];
                        n1712ContratoServicos_UndCntSgl = P00A511_n1712ContratoServicos_UndCntSgl[0];
                        A555Servico_QtdContratada = P00A511_A555Servico_QtdContratada[0];
                        n555Servico_QtdContratada = P00A511_n555Servico_QtdContratada[0];
                        A557Servico_VlrUnidadeContratada = P00A511_A557Servico_VlrUnidadeContratada[0];
                        A558Servico_Percentual = P00A511_A558Servico_Percentual[0];
                        n558Servico_Percentual = P00A511_n558Servico_Percentual[0];
                        A607ServicoContrato_Faturamento = P00A511_A607ServicoContrato_Faturamento[0];
                        A639ContratoServicos_LocalExec = P00A511_A639ContratoServicos_LocalExec[0];
                        A868ContratoServicos_TipoVnc = P00A511_A868ContratoServicos_TipoVnc[0];
                        n868ContratoServicos_TipoVnc = P00A511_n868ContratoServicos_TipoVnc[0];
                        A888ContratoServicos_HmlSemCnf = P00A511_A888ContratoServicos_HmlSemCnf[0];
                        n888ContratoServicos_HmlSemCnf = P00A511_n888ContratoServicos_HmlSemCnf[0];
                        A1454ContratoServicos_PrazoTpDias = P00A511_A1454ContratoServicos_PrazoTpDias[0];
                        n1454ContratoServicos_PrazoTpDias = P00A511_n1454ContratoServicos_PrazoTpDias[0];
                        A1152ContratoServicos_PrazoAnalise = P00A511_A1152ContratoServicos_PrazoAnalise[0];
                        n1152ContratoServicos_PrazoAnalise = P00A511_n1152ContratoServicos_PrazoAnalise[0];
                        A1153ContratoServicos_PrazoResposta = P00A511_A1153ContratoServicos_PrazoResposta[0];
                        n1153ContratoServicos_PrazoResposta = P00A511_n1153ContratoServicos_PrazoResposta[0];
                        A1181ContratoServicos_PrazoGarantia = P00A511_A1181ContratoServicos_PrazoGarantia[0];
                        n1181ContratoServicos_PrazoGarantia = P00A511_n1181ContratoServicos_PrazoGarantia[0];
                        A1182ContratoServicos_PrazoAtendeGarantia = P00A511_A1182ContratoServicos_PrazoAtendeGarantia[0];
                        n1182ContratoServicos_PrazoAtendeGarantia = P00A511_n1182ContratoServicos_PrazoAtendeGarantia[0];
                        A1224ContratoServicos_PrazoCorrecao = P00A511_A1224ContratoServicos_PrazoCorrecao[0];
                        n1224ContratoServicos_PrazoCorrecao = P00A511_n1224ContratoServicos_PrazoCorrecao[0];
                        A1225ContratoServicos_PrazoCorrecaoTipo = P00A511_A1225ContratoServicos_PrazoCorrecaoTipo[0];
                        n1225ContratoServicos_PrazoCorrecaoTipo = P00A511_n1225ContratoServicos_PrazoCorrecaoTipo[0];
                        A1649ContratoServicos_PrazoInicio = P00A511_A1649ContratoServicos_PrazoInicio[0];
                        n1649ContratoServicos_PrazoInicio = P00A511_n1649ContratoServicos_PrazoInicio[0];
                        A1190ContratoServicos_PrazoImediato = P00A511_A1190ContratoServicos_PrazoImediato[0];
                        n1190ContratoServicos_PrazoImediato = P00A511_n1190ContratoServicos_PrazoImediato[0];
                        A1191ContratoServicos_Produtividade = P00A511_A1191ContratoServicos_Produtividade[0];
                        n1191ContratoServicos_Produtividade = P00A511_n1191ContratoServicos_Produtividade[0];
                        A1217ContratoServicos_EspelhaAceite = P00A511_A1217ContratoServicos_EspelhaAceite[0];
                        n1217ContratoServicos_EspelhaAceite = P00A511_n1217ContratoServicos_EspelhaAceite[0];
                        A1266ContratoServicos_Momento = P00A511_A1266ContratoServicos_Momento[0];
                        n1266ContratoServicos_Momento = P00A511_n1266ContratoServicos_Momento[0];
                        A1325ContratoServicos_StatusPagFnc = P00A511_A1325ContratoServicos_StatusPagFnc[0];
                        n1325ContratoServicos_StatusPagFnc = P00A511_n1325ContratoServicos_StatusPagFnc[0];
                        A1340ContratoServicos_QntUntCns = P00A511_A1340ContratoServicos_QntUntCns[0];
                        n1340ContratoServicos_QntUntCns = P00A511_n1340ContratoServicos_QntUntCns[0];
                        A1341ContratoServicos_FatorCnvUndCnt = P00A511_A1341ContratoServicos_FatorCnvUndCnt[0];
                        n1341ContratoServicos_FatorCnvUndCnt = P00A511_n1341ContratoServicos_FatorCnvUndCnt[0];
                        A1397ContratoServicos_NaoRequerAtr = P00A511_A1397ContratoServicos_NaoRequerAtr[0];
                        n1397ContratoServicos_NaoRequerAtr = P00A511_n1397ContratoServicos_NaoRequerAtr[0];
                        A453Contrato_IndiceDivergencia = P00A511_A453Contrato_IndiceDivergencia[0];
                        A1455ContratoServicos_IndiceDivergencia = P00A511_A1455ContratoServicos_IndiceDivergencia[0];
                        n1455ContratoServicos_IndiceDivergencia = P00A511_n1455ContratoServicos_IndiceDivergencia[0];
                        A1516ContratoServicos_TmpEstAnl = P00A511_A1516ContratoServicos_TmpEstAnl[0];
                        n1516ContratoServicos_TmpEstAnl = P00A511_n1516ContratoServicos_TmpEstAnl[0];
                        A1501ContratoServicos_TmpEstExc = P00A511_A1501ContratoServicos_TmpEstExc[0];
                        n1501ContratoServicos_TmpEstExc = P00A511_n1501ContratoServicos_TmpEstExc[0];
                        A1502ContratoServicos_TmpEstCrr = P00A511_A1502ContratoServicos_TmpEstCrr[0];
                        n1502ContratoServicos_TmpEstCrr = P00A511_n1502ContratoServicos_TmpEstCrr[0];
                        A1531ContratoServicos_TipoHierarquia = P00A511_A1531ContratoServicos_TipoHierarquia[0];
                        n1531ContratoServicos_TipoHierarquia = P00A511_n1531ContratoServicos_TipoHierarquia[0];
                        A1537ContratoServicos_PercTmp = P00A511_A1537ContratoServicos_PercTmp[0];
                        n1537ContratoServicos_PercTmp = P00A511_n1537ContratoServicos_PercTmp[0];
                        A1538ContratoServicos_PercPgm = P00A511_A1538ContratoServicos_PercPgm[0];
                        n1538ContratoServicos_PercPgm = P00A511_n1538ContratoServicos_PercPgm[0];
                        A1539ContratoServicos_PercCnc = P00A511_A1539ContratoServicos_PercCnc[0];
                        n1539ContratoServicos_PercCnc = P00A511_n1539ContratoServicos_PercCnc[0];
                        A638ContratoServicos_Ativo = P00A511_A638ContratoServicos_Ativo[0];
                        A1723ContratoServicos_CodigoFiscal = P00A511_A1723ContratoServicos_CodigoFiscal[0];
                        n1723ContratoServicos_CodigoFiscal = P00A511_n1723ContratoServicos_CodigoFiscal[0];
                        A1799ContratoServicos_LimiteProposta = P00A511_A1799ContratoServicos_LimiteProposta[0];
                        n1799ContratoServicos_LimiteProposta = P00A511_n1799ContratoServicos_LimiteProposta[0];
                        A2074ContratoServicos_CalculoRmn = P00A511_A2074ContratoServicos_CalculoRmn[0];
                        n2074ContratoServicos_CalculoRmn = P00A511_n2074ContratoServicos_CalculoRmn[0];
                        A2094ContratoServicos_SolicitaGestorSistema = P00A511_A2094ContratoServicos_SolicitaGestorSistema[0];
                        A913ContratoServicos_PrazoTipo = P00A511_A913ContratoServicos_PrazoTipo[0];
                        n913ContratoServicos_PrazoTipo = P00A511_n913ContratoServicos_PrazoTipo[0];
                        A914ContratoServicos_PrazoDias = P00A511_A914ContratoServicos_PrazoDias[0];
                        n914ContratoServicos_PrazoDias = P00A511_n914ContratoServicos_PrazoDias[0];
                        A911ContratoServicos_Prazos = P00A511_A911ContratoServicos_Prazos[0];
                        n911ContratoServicos_Prazos = P00A511_n911ContratoServicos_Prazos[0];
                        A1377ContratoServicos_Indicadores = P00A511_A1377ContratoServicos_Indicadores[0];
                        n1377ContratoServicos_Indicadores = P00A511_n1377ContratoServicos_Indicadores[0];
                        A2073ContratoServicos_QtdRmn = P00A511_A2073ContratoServicos_QtdRmn[0];
                        n2073ContratoServicos_QtdRmn = P00A511_n2073ContratoServicos_QtdRmn[0];
                        A605Servico_Sigla = P00A511_A605Servico_Sigla[0];
                        A608Servico_Nome = P00A511_A608Servico_Nome[0];
                        A155Servico_Codigo = P00A511_A155Servico_Codigo[0];
                        A77Contrato_Numero = P00A511_A77Contrato_Numero[0];
                        A78Contrato_NumeroAta = P00A511_A78Contrato_NumeroAta[0];
                        n78Contrato_NumeroAta = P00A511_n78Contrato_NumeroAta[0];
                        A79Contrato_Ano = P00A511_A79Contrato_Ano[0];
                        A116Contrato_ValorUnidadeContratacao = P00A511_A116Contrato_ValorUnidadeContratacao[0];
                        A75Contrato_AreaTrabalhoCod = P00A511_A75Contrato_AreaTrabalhoCod[0];
                        A39Contratada_Codigo = P00A511_A39Contratada_Codigo[0];
                        A453Contrato_IndiceDivergencia = P00A511_A453Contrato_IndiceDivergencia[0];
                        A40Contratada_PessoaCod = P00A511_A40Contratada_PessoaCod[0];
                        A516Contratada_TipoFabrica = P00A511_A516Contratada_TipoFabrica[0];
                        A41Contratada_PessoaNom = P00A511_A41Contratada_PessoaNom[0];
                        n41Contratada_PessoaNom = P00A511_n41Contratada_PessoaNom[0];
                        A42Contratada_PessoaCNPJ = P00A511_A42Contratada_PessoaCNPJ[0];
                        n42Contratada_PessoaCNPJ = P00A511_n42Contratada_PessoaCNPJ[0];
                        A2132ContratoServicos_UndCntNome = P00A511_A2132ContratoServicos_UndCntNome[0];
                        n2132ContratoServicos_UndCntNome = P00A511_n2132ContratoServicos_UndCntNome[0];
                        A1712ContratoServicos_UndCntSgl = P00A511_A1712ContratoServicos_UndCntSgl[0];
                        n1712ContratoServicos_UndCntSgl = P00A511_n1712ContratoServicos_UndCntSgl[0];
                        A913ContratoServicos_PrazoTipo = P00A511_A913ContratoServicos_PrazoTipo[0];
                        n913ContratoServicos_PrazoTipo = P00A511_n913ContratoServicos_PrazoTipo[0];
                        A914ContratoServicos_PrazoDias = P00A511_A914ContratoServicos_PrazoDias[0];
                        n914ContratoServicos_PrazoDias = P00A511_n914ContratoServicos_PrazoDias[0];
                        A911ContratoServicos_Prazos = P00A511_A911ContratoServicos_Prazos[0];
                        n911ContratoServicos_Prazos = P00A511_n911ContratoServicos_Prazos[0];
                        A1377ContratoServicos_Indicadores = P00A511_A1377ContratoServicos_Indicadores[0];
                        n1377ContratoServicos_Indicadores = P00A511_n1377ContratoServicos_Indicadores[0];
                        A2073ContratoServicos_QtdRmn = P00A511_A2073ContratoServicos_QtdRmn[0];
                        n2073ContratoServicos_QtdRmn = P00A511_n2073ContratoServicos_QtdRmn[0];
                        A1061Servico_Tela = P00A511_A1061Servico_Tela[0];
                        n1061Servico_Tela = P00A511_n1061Servico_Tela[0];
                        A632Servico_Ativo = P00A511_A632Servico_Ativo[0];
                        A156Servico_Descricao = P00A511_A156Servico_Descricao[0];
                        n156Servico_Descricao = P00A511_n156Servico_Descricao[0];
                        A631Servico_Vinculado = P00A511_A631Servico_Vinculado[0];
                        n631Servico_Vinculado = P00A511_n631Servico_Vinculado[0];
                        A157ServicoGrupo_Codigo = P00A511_A157ServicoGrupo_Codigo[0];
                        A2092Servico_IsOrigemReferencia = P00A511_A2092Servico_IsOrigemReferencia[0];
                        A605Servico_Sigla = P00A511_A605Servico_Sigla[0];
                        A608Servico_Nome = P00A511_A608Servico_Nome[0];
                        A158ServicoGrupo_Descricao = P00A511_A158ServicoGrupo_Descricao[0];
                        A826ContratoServicos_ServicoSigla = A605Servico_Sigla;
                        A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
                        GXt_int1 = A1551Servico_Responsavel;
                        new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int1) ;
                        A1551Servico_Responsavel = GXt_int1;
                        GXt_int2 = A640Servico_Vinculados;
                        new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int2) ;
                        A640Servico_Vinculados = GXt_int2;
                        A827ContratoServicos_ServicoCod = A155Servico_Codigo;
                        AV32GXV2 = 1;
                        while ( AV32GXV2 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                        {
                           AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV32GXV2));
                           if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_Codigo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_Codigo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_Numero") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A77Contrato_Numero;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_NumeroAta") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A78Contrato_NumeroAta;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_Ano") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_ValorUnidadeContratacao") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_AreaTrabalhoCod") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_Codigo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_PessoaCod") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_PessoaNom") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A41Contratada_PessoaNom;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_PessoaCNPJ") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A42Contratada_PessoaCNPJ;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_TipoFabrica") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A516Contratada_TipoFabrica;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Codigo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_Alias") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1858ContratoServicos_Alias;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_ServicoCod") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A827ContratoServicos_ServicoCod), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Nome") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A608Servico_Nome;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Sigla") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A605Servico_Sigla;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Tela") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1061Servico_Tela;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Ativo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A632Servico_Ativo);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Identificacao") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2107Servico_Identificacao;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_ServicoSigla") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A826ContratoServicos_ServicoSigla;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Descricao") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A156Servico_Descricao;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Vinculado") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Vinculados") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Responsavel") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ServicoGrupo_Codigo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ServicoGrupo_Descricao") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A158ServicoGrupo_Descricao;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_IsOrigemReferencia") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_UnidadeContratada") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_UndCntNome") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2132ContratoServicos_UndCntNome;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_UndCntSgl") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1712ContratoServicos_UndCntSgl;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_QtdContratada") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A555Servico_QtdContratada), 10, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_VlrUnidadeContratada") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Percentual") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A558Servico_Percentual, 7, 3);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ServicoContrato_Faturamento") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A607ServicoContrato_Faturamento;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_LocalExec") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A639ContratoServicos_LocalExec;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_TipoVnc") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A868ContratoServicos_TipoVnc;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_HmlSemCnf") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A888ContratoServicos_HmlSemCnf);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_PrazoTipo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A913ContratoServicos_PrazoTipo;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_PrazoDias") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A914ContratoServicos_PrazoDias), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_PrazoTpDias") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1454ContratoServicos_PrazoTpDias;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_Prazos") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A911ContratoServicos_Prazos), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_Indicadores") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1377ContratoServicos_Indicadores), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_PrazoAnalise") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1152ContratoServicos_PrazoAnalise), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_PrazoResposta") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1153ContratoServicos_PrazoResposta), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_PrazoGarantia") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1181ContratoServicos_PrazoGarantia), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_PrazoAtendeGarantia") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_PrazoCorrecao") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_PrazoCorrecaoTipo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1225ContratoServicos_PrazoCorrecaoTipo;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_PrazoInicio") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_PrazoImediato") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1190ContratoServicos_PrazoImediato);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_Produtividade") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1191ContratoServicos_Produtividade, 14, 5);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_EspelhaAceite") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1217ContratoServicos_EspelhaAceite);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_Momento") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1266ContratoServicos_Momento;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_StatusPagFnc") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1325ContratoServicos_StatusPagFnc;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_QntUntCns") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1340ContratoServicos_QntUntCns, 9, 4);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_FatorCnvUndCnt") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1341ContratoServicos_FatorCnvUndCnt, 9, 4);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_NaoRequerAtr") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1397ContratoServicos_NaoRequerAtr);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_IndiceDivergencia") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_IndiceDivergencia") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1455ContratoServicos_IndiceDivergencia, 6, 2);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_TmpEstAnl") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1516ContratoServicos_TmpEstAnl), 8, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_TmpEstExc") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1501ContratoServicos_TmpEstExc), 8, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_TmpEstCrr") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1502ContratoServicos_TmpEstCrr), 8, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_TipoHierarquia") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1531ContratoServicos_TipoHierarquia), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_PercTmp") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1537ContratoServicos_PercTmp), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_PercPgm") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1538ContratoServicos_PercPgm), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_PercCnc") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1539ContratoServicos_PercCnc), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_Ativo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A638ContratoServicos_Ativo);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_CodigoFiscal") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1723ContratoServicos_CodigoFiscal;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_LimiteProposta") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1799ContratoServicos_LimiteProposta, 14, 5);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_CalculoRmn") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_QtdRmn") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_SolicitaGestorSistema") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2094ContratoServicos_SolicitaGestorSistema);
                           }
                           AV32GXV2 = (int)(AV32GXV2+1);
                        }
                        /* Exiting from a For First loop. */
                        if (true) break;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV11AuditingObjectRecordItem.gxTpr_Tablename, "ContratoServicosRmn") == 0 )
                  {
                     AV20CountKeyAttributes = 0;
                     AV33GXV3 = 1;
                     while ( AV33GXV3 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                     {
                        AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV33GXV3));
                        if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosRmn_Sequencial") == 0 )
                        {
                           AV22KeyContratoServicosRmn_Sequencial = AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue;
                           AV20CountKeyAttributes = (short)(AV20CountKeyAttributes+1);
                           if ( AV20CountKeyAttributes == 1 )
                           {
                              if (true) break;
                           }
                        }
                        AV33GXV3 = (int)(AV33GXV3+1);
                     }
                     AV34GXLvl1546 = 0;
                     /* Using cursor P00A513 */
                     pr_default.execute(4, new Object[] {A160ContratoServicos_Codigo});
                     while ( (pr_default.getStatus(4) != 101) )
                     {
                        A2069ContratoServicosRmn_Sequencial = P00A513_A2069ContratoServicosRmn_Sequencial[0];
                        A2070ContratoServicosRmn_Inicio = P00A513_A2070ContratoServicosRmn_Inicio[0];
                        A2071ContratoServicosRmn_Fim = P00A513_A2071ContratoServicosRmn_Fim[0];
                        A2072ContratoServicosRmn_Valor = P00A513_A2072ContratoServicosRmn_Valor[0];
                        if ( StringUtil.StrCmp(StringUtil.Str( (decimal)(A2069ContratoServicosRmn_Sequencial), 4, 0), AV22KeyContratoServicosRmn_Sequencial) == 0 )
                        {
                           AV34GXLvl1546 = 1;
                           AV11AuditingObjectRecordItem.gxTpr_Mode = "UPD";
                           AV21CountUpdatedContratoServicosRmn_Sequencial = (short)(AV21CountUpdatedContratoServicosRmn_Sequencial+1);
                           AV35GXV4 = 1;
                           while ( AV35GXV4 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                           {
                              AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV35GXV4));
                              if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosRmn_Sequencial") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2069ContratoServicosRmn_Sequencial), 4, 0);
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosRmn_Inicio") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A2070ContratoServicosRmn_Inicio, 14, 5);
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosRmn_Fim") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A2071ContratoServicosRmn_Fim, 14, 5);
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosRmn_Valor") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A2072ContratoServicosRmn_Valor, 14, 5);
                              }
                              AV35GXV4 = (int)(AV35GXV4+1);
                           }
                        }
                        pr_default.readNext(4);
                     }
                     pr_default.close(4);
                     if ( AV34GXLvl1546 == 0 )
                     {
                        AV11AuditingObjectRecordItem.gxTpr_Mode = "DLT";
                     }
                  }
                  AV30GXV1 = (int)(AV30GXV1+1);
               }
               if ( AV21CountUpdatedContratoServicosRmn_Sequencial < A40000ContratoServicos_QtdRmn )
               {
                  AV17AuditingObjectNewRecords = new wwpbaseobjects.SdtAuditingObject(context);
                  /* Using cursor P00A514 */
                  pr_default.execute(5, new Object[] {AV16ContratoServicos_Codigo});
                  while ( (pr_default.getStatus(5) != 101) )
                  {
                     A160ContratoServicos_Codigo = P00A514_A160ContratoServicos_Codigo[0];
                     A2069ContratoServicosRmn_Sequencial = P00A514_A2069ContratoServicosRmn_Sequencial[0];
                     A2070ContratoServicosRmn_Inicio = P00A514_A2070ContratoServicosRmn_Inicio[0];
                     A2071ContratoServicosRmn_Fim = P00A514_A2071ContratoServicosRmn_Fim[0];
                     A2072ContratoServicosRmn_Valor = P00A514_A2072ContratoServicosRmn_Valor[0];
                     AV22KeyContratoServicosRmn_Sequencial = StringUtil.Str( (decimal)(A2069ContratoServicosRmn_Sequencial), 4, 0);
                     AV23RecordExistsContratoServicosRmn_Sequencial = false;
                     AV37GXV5 = 1;
                     while ( AV37GXV5 <= AV10AuditingObject.gxTpr_Record.Count )
                     {
                        AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV37GXV5));
                        if ( StringUtil.StrCmp(AV11AuditingObjectRecordItem.gxTpr_Tablename, "ContratoServicosRmn") == 0 )
                        {
                           AV20CountKeyAttributes = 0;
                           AV38GXV6 = 1;
                           while ( AV38GXV6 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                           {
                              AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV38GXV6));
                              if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosRmn_Sequencial") == 0 )
                              {
                                 if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue, AV22KeyContratoServicosRmn_Sequencial) == 0 )
                                 {
                                    AV23RecordExistsContratoServicosRmn_Sequencial = true;
                                    AV20CountKeyAttributes = (short)(AV20CountKeyAttributes+1);
                                    if ( AV20CountKeyAttributes == 1 )
                                    {
                                       if (true) break;
                                    }
                                 }
                              }
                              AV38GXV6 = (int)(AV38GXV6+1);
                           }
                        }
                        AV37GXV5 = (int)(AV37GXV5+1);
                     }
                     if ( ! ( AV23RecordExistsContratoServicosRmn_Sequencial ) )
                     {
                        AV18AuditingObjectRecordItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
                        AV18AuditingObjectRecordItemNew.gxTpr_Tablename = "ContratoServicosRmn";
                        AV18AuditingObjectRecordItemNew.gxTpr_Mode = "INS";
                        AV17AuditingObjectNewRecords.gxTpr_Record.Add(AV18AuditingObjectRecordItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosRmn_Sequencial";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = true;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2069ContratoServicosRmn_Sequencial), 4, 0);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosRmn_Inicio";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = true;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( A2070ContratoServicosRmn_Inicio, 14, 5);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosRmn_Fim";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( A2071ContratoServicosRmn_Fim, 14, 5);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosRmn_Valor";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( A2072ContratoServicosRmn_Valor, 14, 5);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                     }
                     pr_default.readNext(5);
                  }
                  pr_default.close(5);
                  AV39GXV7 = 1;
                  while ( AV39GXV7 <= AV17AuditingObjectNewRecords.gxTpr_Record.Count )
                  {
                     AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV17AuditingObjectNewRecords.gxTpr_Record.Item(AV39GXV7));
                     AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
                     AV39GXV7 = (int)(AV39GXV7+1);
                  }
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00A55_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00A55_A74Contrato_Codigo = new int[1] ;
         P00A55_A77Contrato_Numero = new String[] {""} ;
         P00A55_A78Contrato_NumeroAta = new String[] {""} ;
         P00A55_n78Contrato_NumeroAta = new bool[] {false} ;
         P00A55_A79Contrato_Ano = new short[1] ;
         P00A55_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         P00A55_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P00A55_A39Contratada_Codigo = new int[1] ;
         P00A55_A40Contratada_PessoaCod = new int[1] ;
         P00A55_A41Contratada_PessoaNom = new String[] {""} ;
         P00A55_n41Contratada_PessoaNom = new bool[] {false} ;
         P00A55_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00A55_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00A55_A516Contratada_TipoFabrica = new String[] {""} ;
         P00A55_A1858ContratoServicos_Alias = new String[] {""} ;
         P00A55_n1858ContratoServicos_Alias = new bool[] {false} ;
         P00A55_A1061Servico_Tela = new String[] {""} ;
         P00A55_n1061Servico_Tela = new bool[] {false} ;
         P00A55_A632Servico_Ativo = new bool[] {false} ;
         P00A55_A156Servico_Descricao = new String[] {""} ;
         P00A55_n156Servico_Descricao = new bool[] {false} ;
         P00A55_A631Servico_Vinculado = new int[1] ;
         P00A55_n631Servico_Vinculado = new bool[] {false} ;
         P00A55_A157ServicoGrupo_Codigo = new int[1] ;
         P00A55_A158ServicoGrupo_Descricao = new String[] {""} ;
         P00A55_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         P00A55_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         P00A55_A2132ContratoServicos_UndCntNome = new String[] {""} ;
         P00A55_n2132ContratoServicos_UndCntNome = new bool[] {false} ;
         P00A55_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         P00A55_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         P00A55_A555Servico_QtdContratada = new long[1] ;
         P00A55_n555Servico_QtdContratada = new bool[] {false} ;
         P00A55_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         P00A55_A558Servico_Percentual = new decimal[1] ;
         P00A55_n558Servico_Percentual = new bool[] {false} ;
         P00A55_A607ServicoContrato_Faturamento = new String[] {""} ;
         P00A55_A639ContratoServicos_LocalExec = new String[] {""} ;
         P00A55_A868ContratoServicos_TipoVnc = new String[] {""} ;
         P00A55_n868ContratoServicos_TipoVnc = new bool[] {false} ;
         P00A55_A888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         P00A55_n888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         P00A55_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         P00A55_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         P00A55_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         P00A55_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         P00A55_A1153ContratoServicos_PrazoResposta = new short[1] ;
         P00A55_n1153ContratoServicos_PrazoResposta = new bool[] {false} ;
         P00A55_A1181ContratoServicos_PrazoGarantia = new short[1] ;
         P00A55_n1181ContratoServicos_PrazoGarantia = new bool[] {false} ;
         P00A55_A1182ContratoServicos_PrazoAtendeGarantia = new short[1] ;
         P00A55_n1182ContratoServicos_PrazoAtendeGarantia = new bool[] {false} ;
         P00A55_A1224ContratoServicos_PrazoCorrecao = new short[1] ;
         P00A55_n1224ContratoServicos_PrazoCorrecao = new bool[] {false} ;
         P00A55_A1225ContratoServicos_PrazoCorrecaoTipo = new String[] {""} ;
         P00A55_n1225ContratoServicos_PrazoCorrecaoTipo = new bool[] {false} ;
         P00A55_A1649ContratoServicos_PrazoInicio = new short[1] ;
         P00A55_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         P00A55_A1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         P00A55_n1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         P00A55_A1191ContratoServicos_Produtividade = new decimal[1] ;
         P00A55_n1191ContratoServicos_Produtividade = new bool[] {false} ;
         P00A55_A1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         P00A55_n1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         P00A55_A1266ContratoServicos_Momento = new String[] {""} ;
         P00A55_n1266ContratoServicos_Momento = new bool[] {false} ;
         P00A55_A1325ContratoServicos_StatusPagFnc = new String[] {""} ;
         P00A55_n1325ContratoServicos_StatusPagFnc = new bool[] {false} ;
         P00A55_A1340ContratoServicos_QntUntCns = new decimal[1] ;
         P00A55_n1340ContratoServicos_QntUntCns = new bool[] {false} ;
         P00A55_A1341ContratoServicos_FatorCnvUndCnt = new decimal[1] ;
         P00A55_n1341ContratoServicos_FatorCnvUndCnt = new bool[] {false} ;
         P00A55_A1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         P00A55_n1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         P00A55_A453Contrato_IndiceDivergencia = new decimal[1] ;
         P00A55_A1455ContratoServicos_IndiceDivergencia = new decimal[1] ;
         P00A55_n1455ContratoServicos_IndiceDivergencia = new bool[] {false} ;
         P00A55_A1516ContratoServicos_TmpEstAnl = new int[1] ;
         P00A55_n1516ContratoServicos_TmpEstAnl = new bool[] {false} ;
         P00A55_A1501ContratoServicos_TmpEstExc = new int[1] ;
         P00A55_n1501ContratoServicos_TmpEstExc = new bool[] {false} ;
         P00A55_A1502ContratoServicos_TmpEstCrr = new int[1] ;
         P00A55_n1502ContratoServicos_TmpEstCrr = new bool[] {false} ;
         P00A55_A1531ContratoServicos_TipoHierarquia = new short[1] ;
         P00A55_n1531ContratoServicos_TipoHierarquia = new bool[] {false} ;
         P00A55_A1537ContratoServicos_PercTmp = new short[1] ;
         P00A55_n1537ContratoServicos_PercTmp = new bool[] {false} ;
         P00A55_A1538ContratoServicos_PercPgm = new short[1] ;
         P00A55_n1538ContratoServicos_PercPgm = new bool[] {false} ;
         P00A55_A1539ContratoServicos_PercCnc = new short[1] ;
         P00A55_n1539ContratoServicos_PercCnc = new bool[] {false} ;
         P00A55_A638ContratoServicos_Ativo = new bool[] {false} ;
         P00A55_A1723ContratoServicos_CodigoFiscal = new String[] {""} ;
         P00A55_n1723ContratoServicos_CodigoFiscal = new bool[] {false} ;
         P00A55_A1799ContratoServicos_LimiteProposta = new decimal[1] ;
         P00A55_n1799ContratoServicos_LimiteProposta = new bool[] {false} ;
         P00A55_A2074ContratoServicos_CalculoRmn = new short[1] ;
         P00A55_n2074ContratoServicos_CalculoRmn = new bool[] {false} ;
         P00A55_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         P00A55_A913ContratoServicos_PrazoTipo = new String[] {""} ;
         P00A55_n913ContratoServicos_PrazoTipo = new bool[] {false} ;
         P00A55_A914ContratoServicos_PrazoDias = new short[1] ;
         P00A55_n914ContratoServicos_PrazoDias = new bool[] {false} ;
         P00A55_A911ContratoServicos_Prazos = new short[1] ;
         P00A55_n911ContratoServicos_Prazos = new bool[] {false} ;
         P00A55_A160ContratoServicos_Codigo = new int[1] ;
         P00A55_A1377ContratoServicos_Indicadores = new short[1] ;
         P00A55_n1377ContratoServicos_Indicadores = new bool[] {false} ;
         P00A55_A2073ContratoServicos_QtdRmn = new short[1] ;
         P00A55_n2073ContratoServicos_QtdRmn = new bool[] {false} ;
         P00A55_A605Servico_Sigla = new String[] {""} ;
         P00A55_A608Servico_Nome = new String[] {""} ;
         P00A55_A155Servico_Codigo = new int[1] ;
         A77Contrato_Numero = "";
         A78Contrato_NumeroAta = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A516Contratada_TipoFabrica = "";
         A1858ContratoServicos_Alias = "";
         A1061Servico_Tela = "";
         A156Servico_Descricao = "";
         A158ServicoGrupo_Descricao = "";
         A2132ContratoServicos_UndCntNome = "";
         A1712ContratoServicos_UndCntSgl = "";
         A607ServicoContrato_Faturamento = "";
         A639ContratoServicos_LocalExec = "";
         A868ContratoServicos_TipoVnc = "";
         A1454ContratoServicos_PrazoTpDias = "";
         A1225ContratoServicos_PrazoCorrecaoTipo = "";
         A1266ContratoServicos_Momento = "";
         A1325ContratoServicos_StatusPagFnc = "";
         A1723ContratoServicos_CodigoFiscal = "";
         A913ContratoServicos_PrazoTipo = "";
         A605Servico_Sigla = "";
         A608Servico_Nome = "";
         A826ContratoServicos_ServicoSigla = "";
         A2107Servico_Identificacao = "";
         AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         P00A56_A160ContratoServicos_Codigo = new int[1] ;
         P00A56_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         P00A56_A2070ContratoServicosRmn_Inicio = new decimal[1] ;
         P00A56_A2071ContratoServicosRmn_Fim = new decimal[1] ;
         P00A56_A2072ContratoServicosRmn_Valor = new decimal[1] ;
         P00A511_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00A511_A74Contrato_Codigo = new int[1] ;
         P00A511_A77Contrato_Numero = new String[] {""} ;
         P00A511_A78Contrato_NumeroAta = new String[] {""} ;
         P00A511_n78Contrato_NumeroAta = new bool[] {false} ;
         P00A511_A79Contrato_Ano = new short[1] ;
         P00A511_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         P00A511_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P00A511_A39Contratada_Codigo = new int[1] ;
         P00A511_A40Contratada_PessoaCod = new int[1] ;
         P00A511_A41Contratada_PessoaNom = new String[] {""} ;
         P00A511_n41Contratada_PessoaNom = new bool[] {false} ;
         P00A511_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00A511_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00A511_A516Contratada_TipoFabrica = new String[] {""} ;
         P00A511_A1858ContratoServicos_Alias = new String[] {""} ;
         P00A511_n1858ContratoServicos_Alias = new bool[] {false} ;
         P00A511_A1061Servico_Tela = new String[] {""} ;
         P00A511_n1061Servico_Tela = new bool[] {false} ;
         P00A511_A632Servico_Ativo = new bool[] {false} ;
         P00A511_A156Servico_Descricao = new String[] {""} ;
         P00A511_n156Servico_Descricao = new bool[] {false} ;
         P00A511_A631Servico_Vinculado = new int[1] ;
         P00A511_n631Servico_Vinculado = new bool[] {false} ;
         P00A511_A157ServicoGrupo_Codigo = new int[1] ;
         P00A511_A158ServicoGrupo_Descricao = new String[] {""} ;
         P00A511_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         P00A511_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         P00A511_A2132ContratoServicos_UndCntNome = new String[] {""} ;
         P00A511_n2132ContratoServicos_UndCntNome = new bool[] {false} ;
         P00A511_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         P00A511_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         P00A511_A555Servico_QtdContratada = new long[1] ;
         P00A511_n555Servico_QtdContratada = new bool[] {false} ;
         P00A511_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         P00A511_A558Servico_Percentual = new decimal[1] ;
         P00A511_n558Servico_Percentual = new bool[] {false} ;
         P00A511_A607ServicoContrato_Faturamento = new String[] {""} ;
         P00A511_A639ContratoServicos_LocalExec = new String[] {""} ;
         P00A511_A868ContratoServicos_TipoVnc = new String[] {""} ;
         P00A511_n868ContratoServicos_TipoVnc = new bool[] {false} ;
         P00A511_A888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         P00A511_n888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         P00A511_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         P00A511_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         P00A511_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         P00A511_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         P00A511_A1153ContratoServicos_PrazoResposta = new short[1] ;
         P00A511_n1153ContratoServicos_PrazoResposta = new bool[] {false} ;
         P00A511_A1181ContratoServicos_PrazoGarantia = new short[1] ;
         P00A511_n1181ContratoServicos_PrazoGarantia = new bool[] {false} ;
         P00A511_A1182ContratoServicos_PrazoAtendeGarantia = new short[1] ;
         P00A511_n1182ContratoServicos_PrazoAtendeGarantia = new bool[] {false} ;
         P00A511_A1224ContratoServicos_PrazoCorrecao = new short[1] ;
         P00A511_n1224ContratoServicos_PrazoCorrecao = new bool[] {false} ;
         P00A511_A1225ContratoServicos_PrazoCorrecaoTipo = new String[] {""} ;
         P00A511_n1225ContratoServicos_PrazoCorrecaoTipo = new bool[] {false} ;
         P00A511_A1649ContratoServicos_PrazoInicio = new short[1] ;
         P00A511_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         P00A511_A1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         P00A511_n1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         P00A511_A1191ContratoServicos_Produtividade = new decimal[1] ;
         P00A511_n1191ContratoServicos_Produtividade = new bool[] {false} ;
         P00A511_A1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         P00A511_n1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         P00A511_A1266ContratoServicos_Momento = new String[] {""} ;
         P00A511_n1266ContratoServicos_Momento = new bool[] {false} ;
         P00A511_A1325ContratoServicos_StatusPagFnc = new String[] {""} ;
         P00A511_n1325ContratoServicos_StatusPagFnc = new bool[] {false} ;
         P00A511_A1340ContratoServicos_QntUntCns = new decimal[1] ;
         P00A511_n1340ContratoServicos_QntUntCns = new bool[] {false} ;
         P00A511_A1341ContratoServicos_FatorCnvUndCnt = new decimal[1] ;
         P00A511_n1341ContratoServicos_FatorCnvUndCnt = new bool[] {false} ;
         P00A511_A1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         P00A511_n1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         P00A511_A453Contrato_IndiceDivergencia = new decimal[1] ;
         P00A511_A1455ContratoServicos_IndiceDivergencia = new decimal[1] ;
         P00A511_n1455ContratoServicos_IndiceDivergencia = new bool[] {false} ;
         P00A511_A1516ContratoServicos_TmpEstAnl = new int[1] ;
         P00A511_n1516ContratoServicos_TmpEstAnl = new bool[] {false} ;
         P00A511_A1501ContratoServicos_TmpEstExc = new int[1] ;
         P00A511_n1501ContratoServicos_TmpEstExc = new bool[] {false} ;
         P00A511_A1502ContratoServicos_TmpEstCrr = new int[1] ;
         P00A511_n1502ContratoServicos_TmpEstCrr = new bool[] {false} ;
         P00A511_A1531ContratoServicos_TipoHierarquia = new short[1] ;
         P00A511_n1531ContratoServicos_TipoHierarquia = new bool[] {false} ;
         P00A511_A1537ContratoServicos_PercTmp = new short[1] ;
         P00A511_n1537ContratoServicos_PercTmp = new bool[] {false} ;
         P00A511_A1538ContratoServicos_PercPgm = new short[1] ;
         P00A511_n1538ContratoServicos_PercPgm = new bool[] {false} ;
         P00A511_A1539ContratoServicos_PercCnc = new short[1] ;
         P00A511_n1539ContratoServicos_PercCnc = new bool[] {false} ;
         P00A511_A638ContratoServicos_Ativo = new bool[] {false} ;
         P00A511_A1723ContratoServicos_CodigoFiscal = new String[] {""} ;
         P00A511_n1723ContratoServicos_CodigoFiscal = new bool[] {false} ;
         P00A511_A1799ContratoServicos_LimiteProposta = new decimal[1] ;
         P00A511_n1799ContratoServicos_LimiteProposta = new bool[] {false} ;
         P00A511_A2074ContratoServicos_CalculoRmn = new short[1] ;
         P00A511_n2074ContratoServicos_CalculoRmn = new bool[] {false} ;
         P00A511_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         P00A511_A913ContratoServicos_PrazoTipo = new String[] {""} ;
         P00A511_n913ContratoServicos_PrazoTipo = new bool[] {false} ;
         P00A511_A914ContratoServicos_PrazoDias = new short[1] ;
         P00A511_n914ContratoServicos_PrazoDias = new bool[] {false} ;
         P00A511_A911ContratoServicos_Prazos = new short[1] ;
         P00A511_n911ContratoServicos_Prazos = new bool[] {false} ;
         P00A511_A160ContratoServicos_Codigo = new int[1] ;
         P00A511_A1377ContratoServicos_Indicadores = new short[1] ;
         P00A511_n1377ContratoServicos_Indicadores = new bool[] {false} ;
         P00A511_A2073ContratoServicos_QtdRmn = new short[1] ;
         P00A511_n2073ContratoServicos_QtdRmn = new bool[] {false} ;
         P00A511_A40000ContratoServicos_QtdRmn = new int[1] ;
         P00A511_n40000ContratoServicos_QtdRmn = new bool[] {false} ;
         P00A511_A605Servico_Sigla = new String[] {""} ;
         P00A511_A608Servico_Nome = new String[] {""} ;
         P00A511_A155Servico_Codigo = new int[1] ;
         P00A512_A160ContratoServicos_Codigo = new int[1] ;
         P00A512_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         P00A512_A2070ContratoServicosRmn_Inicio = new decimal[1] ;
         P00A512_A2071ContratoServicosRmn_Fim = new decimal[1] ;
         P00A512_A2072ContratoServicosRmn_Valor = new decimal[1] ;
         AV22KeyContratoServicosRmn_Sequencial = "";
         P00A513_A160ContratoServicos_Codigo = new int[1] ;
         P00A513_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         P00A513_A2070ContratoServicosRmn_Inicio = new decimal[1] ;
         P00A513_A2071ContratoServicosRmn_Fim = new decimal[1] ;
         P00A513_A2072ContratoServicosRmn_Valor = new decimal[1] ;
         AV17AuditingObjectNewRecords = new wwpbaseobjects.SdtAuditingObject(context);
         P00A514_A160ContratoServicos_Codigo = new int[1] ;
         P00A514_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         P00A514_A2070ContratoServicosRmn_Inicio = new decimal[1] ;
         P00A514_A2071ContratoServicosRmn_Fim = new decimal[1] ;
         P00A514_A2072ContratoServicosRmn_Valor = new decimal[1] ;
         AV18AuditingObjectRecordItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.loadauditcontratoservicos__default(),
            new Object[][] {
                new Object[] {
               P00A55_A903ContratoServicosPrazo_CntSrvCod, P00A55_A74Contrato_Codigo, P00A55_A77Contrato_Numero, P00A55_A78Contrato_NumeroAta, P00A55_n78Contrato_NumeroAta, P00A55_A79Contrato_Ano, P00A55_A116Contrato_ValorUnidadeContratacao, P00A55_A75Contrato_AreaTrabalhoCod, P00A55_A39Contratada_Codigo, P00A55_A40Contratada_PessoaCod,
               P00A55_A41Contratada_PessoaNom, P00A55_n41Contratada_PessoaNom, P00A55_A42Contratada_PessoaCNPJ, P00A55_n42Contratada_PessoaCNPJ, P00A55_A516Contratada_TipoFabrica, P00A55_A1858ContratoServicos_Alias, P00A55_n1858ContratoServicos_Alias, P00A55_A1061Servico_Tela, P00A55_n1061Servico_Tela, P00A55_A632Servico_Ativo,
               P00A55_A156Servico_Descricao, P00A55_n156Servico_Descricao, P00A55_A631Servico_Vinculado, P00A55_n631Servico_Vinculado, P00A55_A157ServicoGrupo_Codigo, P00A55_A158ServicoGrupo_Descricao, P00A55_A2092Servico_IsOrigemReferencia, P00A55_A1212ContratoServicos_UnidadeContratada, P00A55_A2132ContratoServicos_UndCntNome, P00A55_n2132ContratoServicos_UndCntNome,
               P00A55_A1712ContratoServicos_UndCntSgl, P00A55_n1712ContratoServicos_UndCntSgl, P00A55_A555Servico_QtdContratada, P00A55_n555Servico_QtdContratada, P00A55_A557Servico_VlrUnidadeContratada, P00A55_A558Servico_Percentual, P00A55_n558Servico_Percentual, P00A55_A607ServicoContrato_Faturamento, P00A55_A639ContratoServicos_LocalExec, P00A55_A868ContratoServicos_TipoVnc,
               P00A55_n868ContratoServicos_TipoVnc, P00A55_A888ContratoServicos_HmlSemCnf, P00A55_n888ContratoServicos_HmlSemCnf, P00A55_A1454ContratoServicos_PrazoTpDias, P00A55_n1454ContratoServicos_PrazoTpDias, P00A55_A1152ContratoServicos_PrazoAnalise, P00A55_n1152ContratoServicos_PrazoAnalise, P00A55_A1153ContratoServicos_PrazoResposta, P00A55_n1153ContratoServicos_PrazoResposta, P00A55_A1181ContratoServicos_PrazoGarantia,
               P00A55_n1181ContratoServicos_PrazoGarantia, P00A55_A1182ContratoServicos_PrazoAtendeGarantia, P00A55_n1182ContratoServicos_PrazoAtendeGarantia, P00A55_A1224ContratoServicos_PrazoCorrecao, P00A55_n1224ContratoServicos_PrazoCorrecao, P00A55_A1225ContratoServicos_PrazoCorrecaoTipo, P00A55_n1225ContratoServicos_PrazoCorrecaoTipo, P00A55_A1649ContratoServicos_PrazoInicio, P00A55_n1649ContratoServicos_PrazoInicio, P00A55_A1190ContratoServicos_PrazoImediato,
               P00A55_n1190ContratoServicos_PrazoImediato, P00A55_A1191ContratoServicos_Produtividade, P00A55_n1191ContratoServicos_Produtividade, P00A55_A1217ContratoServicos_EspelhaAceite, P00A55_n1217ContratoServicos_EspelhaAceite, P00A55_A1266ContratoServicos_Momento, P00A55_n1266ContratoServicos_Momento, P00A55_A1325ContratoServicos_StatusPagFnc, P00A55_n1325ContratoServicos_StatusPagFnc, P00A55_A1340ContratoServicos_QntUntCns,
               P00A55_n1340ContratoServicos_QntUntCns, P00A55_A1341ContratoServicos_FatorCnvUndCnt, P00A55_n1341ContratoServicos_FatorCnvUndCnt, P00A55_A1397ContratoServicos_NaoRequerAtr, P00A55_n1397ContratoServicos_NaoRequerAtr, P00A55_A453Contrato_IndiceDivergencia, P00A55_A1455ContratoServicos_IndiceDivergencia, P00A55_n1455ContratoServicos_IndiceDivergencia, P00A55_A1516ContratoServicos_TmpEstAnl, P00A55_n1516ContratoServicos_TmpEstAnl,
               P00A55_A1501ContratoServicos_TmpEstExc, P00A55_n1501ContratoServicos_TmpEstExc, P00A55_A1502ContratoServicos_TmpEstCrr, P00A55_n1502ContratoServicos_TmpEstCrr, P00A55_A1531ContratoServicos_TipoHierarquia, P00A55_n1531ContratoServicos_TipoHierarquia, P00A55_A1537ContratoServicos_PercTmp, P00A55_n1537ContratoServicos_PercTmp, P00A55_A1538ContratoServicos_PercPgm, P00A55_n1538ContratoServicos_PercPgm,
               P00A55_A1539ContratoServicos_PercCnc, P00A55_n1539ContratoServicos_PercCnc, P00A55_A638ContratoServicos_Ativo, P00A55_A1723ContratoServicos_CodigoFiscal, P00A55_n1723ContratoServicos_CodigoFiscal, P00A55_A1799ContratoServicos_LimiteProposta, P00A55_n1799ContratoServicos_LimiteProposta, P00A55_A2074ContratoServicos_CalculoRmn, P00A55_n2074ContratoServicos_CalculoRmn, P00A55_A2094ContratoServicos_SolicitaGestorSistema,
               P00A55_A913ContratoServicos_PrazoTipo, P00A55_n913ContratoServicos_PrazoTipo, P00A55_A914ContratoServicos_PrazoDias, P00A55_n914ContratoServicos_PrazoDias, P00A55_A911ContratoServicos_Prazos, P00A55_n911ContratoServicos_Prazos, P00A55_A160ContratoServicos_Codigo, P00A55_A1377ContratoServicos_Indicadores, P00A55_n1377ContratoServicos_Indicadores, P00A55_A2073ContratoServicos_QtdRmn,
               P00A55_n2073ContratoServicos_QtdRmn, P00A55_A605Servico_Sigla, P00A55_A608Servico_Nome, P00A55_A155Servico_Codigo
               }
               , new Object[] {
               P00A56_A160ContratoServicos_Codigo, P00A56_A2069ContratoServicosRmn_Sequencial, P00A56_A2070ContratoServicosRmn_Inicio, P00A56_A2071ContratoServicosRmn_Fim, P00A56_A2072ContratoServicosRmn_Valor
               }
               , new Object[] {
               P00A511_A903ContratoServicosPrazo_CntSrvCod, P00A511_A74Contrato_Codigo, P00A511_A77Contrato_Numero, P00A511_A78Contrato_NumeroAta, P00A511_n78Contrato_NumeroAta, P00A511_A79Contrato_Ano, P00A511_A116Contrato_ValorUnidadeContratacao, P00A511_A75Contrato_AreaTrabalhoCod, P00A511_A39Contratada_Codigo, P00A511_A40Contratada_PessoaCod,
               P00A511_A41Contratada_PessoaNom, P00A511_n41Contratada_PessoaNom, P00A511_A42Contratada_PessoaCNPJ, P00A511_n42Contratada_PessoaCNPJ, P00A511_A516Contratada_TipoFabrica, P00A511_A1858ContratoServicos_Alias, P00A511_n1858ContratoServicos_Alias, P00A511_A1061Servico_Tela, P00A511_n1061Servico_Tela, P00A511_A632Servico_Ativo,
               P00A511_A156Servico_Descricao, P00A511_n156Servico_Descricao, P00A511_A631Servico_Vinculado, P00A511_n631Servico_Vinculado, P00A511_A157ServicoGrupo_Codigo, P00A511_A158ServicoGrupo_Descricao, P00A511_A2092Servico_IsOrigemReferencia, P00A511_A1212ContratoServicos_UnidadeContratada, P00A511_A2132ContratoServicos_UndCntNome, P00A511_n2132ContratoServicos_UndCntNome,
               P00A511_A1712ContratoServicos_UndCntSgl, P00A511_n1712ContratoServicos_UndCntSgl, P00A511_A555Servico_QtdContratada, P00A511_n555Servico_QtdContratada, P00A511_A557Servico_VlrUnidadeContratada, P00A511_A558Servico_Percentual, P00A511_n558Servico_Percentual, P00A511_A607ServicoContrato_Faturamento, P00A511_A639ContratoServicos_LocalExec, P00A511_A868ContratoServicos_TipoVnc,
               P00A511_n868ContratoServicos_TipoVnc, P00A511_A888ContratoServicos_HmlSemCnf, P00A511_n888ContratoServicos_HmlSemCnf, P00A511_A1454ContratoServicos_PrazoTpDias, P00A511_n1454ContratoServicos_PrazoTpDias, P00A511_A1152ContratoServicos_PrazoAnalise, P00A511_n1152ContratoServicos_PrazoAnalise, P00A511_A1153ContratoServicos_PrazoResposta, P00A511_n1153ContratoServicos_PrazoResposta, P00A511_A1181ContratoServicos_PrazoGarantia,
               P00A511_n1181ContratoServicos_PrazoGarantia, P00A511_A1182ContratoServicos_PrazoAtendeGarantia, P00A511_n1182ContratoServicos_PrazoAtendeGarantia, P00A511_A1224ContratoServicos_PrazoCorrecao, P00A511_n1224ContratoServicos_PrazoCorrecao, P00A511_A1225ContratoServicos_PrazoCorrecaoTipo, P00A511_n1225ContratoServicos_PrazoCorrecaoTipo, P00A511_A1649ContratoServicos_PrazoInicio, P00A511_n1649ContratoServicos_PrazoInicio, P00A511_A1190ContratoServicos_PrazoImediato,
               P00A511_n1190ContratoServicos_PrazoImediato, P00A511_A1191ContratoServicos_Produtividade, P00A511_n1191ContratoServicos_Produtividade, P00A511_A1217ContratoServicos_EspelhaAceite, P00A511_n1217ContratoServicos_EspelhaAceite, P00A511_A1266ContratoServicos_Momento, P00A511_n1266ContratoServicos_Momento, P00A511_A1325ContratoServicos_StatusPagFnc, P00A511_n1325ContratoServicos_StatusPagFnc, P00A511_A1340ContratoServicos_QntUntCns,
               P00A511_n1340ContratoServicos_QntUntCns, P00A511_A1341ContratoServicos_FatorCnvUndCnt, P00A511_n1341ContratoServicos_FatorCnvUndCnt, P00A511_A1397ContratoServicos_NaoRequerAtr, P00A511_n1397ContratoServicos_NaoRequerAtr, P00A511_A453Contrato_IndiceDivergencia, P00A511_A1455ContratoServicos_IndiceDivergencia, P00A511_n1455ContratoServicos_IndiceDivergencia, P00A511_A1516ContratoServicos_TmpEstAnl, P00A511_n1516ContratoServicos_TmpEstAnl,
               P00A511_A1501ContratoServicos_TmpEstExc, P00A511_n1501ContratoServicos_TmpEstExc, P00A511_A1502ContratoServicos_TmpEstCrr, P00A511_n1502ContratoServicos_TmpEstCrr, P00A511_A1531ContratoServicos_TipoHierarquia, P00A511_n1531ContratoServicos_TipoHierarquia, P00A511_A1537ContratoServicos_PercTmp, P00A511_n1537ContratoServicos_PercTmp, P00A511_A1538ContratoServicos_PercPgm, P00A511_n1538ContratoServicos_PercPgm,
               P00A511_A1539ContratoServicos_PercCnc, P00A511_n1539ContratoServicos_PercCnc, P00A511_A638ContratoServicos_Ativo, P00A511_A1723ContratoServicos_CodigoFiscal, P00A511_n1723ContratoServicos_CodigoFiscal, P00A511_A1799ContratoServicos_LimiteProposta, P00A511_n1799ContratoServicos_LimiteProposta, P00A511_A2074ContratoServicos_CalculoRmn, P00A511_n2074ContratoServicos_CalculoRmn, P00A511_A2094ContratoServicos_SolicitaGestorSistema,
               P00A511_A913ContratoServicos_PrazoTipo, P00A511_n913ContratoServicos_PrazoTipo, P00A511_A914ContratoServicos_PrazoDias, P00A511_n914ContratoServicos_PrazoDias, P00A511_A911ContratoServicos_Prazos, P00A511_n911ContratoServicos_Prazos, P00A511_A160ContratoServicos_Codigo, P00A511_A1377ContratoServicos_Indicadores, P00A511_n1377ContratoServicos_Indicadores, P00A511_A2073ContratoServicos_QtdRmn,
               P00A511_n2073ContratoServicos_QtdRmn, P00A511_A40000ContratoServicos_QtdRmn, P00A511_n40000ContratoServicos_QtdRmn, P00A511_A605Servico_Sigla, P00A511_A608Servico_Nome, P00A511_A155Servico_Codigo
               }
               , new Object[] {
               P00A512_A160ContratoServicos_Codigo, P00A512_A2069ContratoServicosRmn_Sequencial, P00A512_A2070ContratoServicosRmn_Inicio, P00A512_A2071ContratoServicosRmn_Fim, P00A512_A2072ContratoServicosRmn_Valor
               }
               , new Object[] {
               P00A513_A160ContratoServicos_Codigo, P00A513_A2069ContratoServicosRmn_Sequencial, P00A513_A2070ContratoServicosRmn_Inicio, P00A513_A2071ContratoServicosRmn_Fim, P00A513_A2072ContratoServicosRmn_Valor
               }
               , new Object[] {
               P00A514_A160ContratoServicos_Codigo, P00A514_A2069ContratoServicosRmn_Sequencial, P00A514_A2070ContratoServicosRmn_Inicio, P00A514_A2071ContratoServicosRmn_Fim, P00A514_A2072ContratoServicosRmn_Valor
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A79Contrato_Ano ;
      private short A1152ContratoServicos_PrazoAnalise ;
      private short A1153ContratoServicos_PrazoResposta ;
      private short A1181ContratoServicos_PrazoGarantia ;
      private short A1182ContratoServicos_PrazoAtendeGarantia ;
      private short A1224ContratoServicos_PrazoCorrecao ;
      private short A1649ContratoServicos_PrazoInicio ;
      private short A1531ContratoServicos_TipoHierarquia ;
      private short A1537ContratoServicos_PercTmp ;
      private short A1538ContratoServicos_PercPgm ;
      private short A1539ContratoServicos_PercCnc ;
      private short A2074ContratoServicos_CalculoRmn ;
      private short A914ContratoServicos_PrazoDias ;
      private short A911ContratoServicos_Prazos ;
      private short A1377ContratoServicos_Indicadores ;
      private short A2073ContratoServicos_QtdRmn ;
      private short A640Servico_Vinculados ;
      private short A2069ContratoServicosRmn_Sequencial ;
      private short AV21CountUpdatedContratoServicosRmn_Sequencial ;
      private short GXt_int2 ;
      private short AV20CountKeyAttributes ;
      private short AV34GXLvl1546 ;
      private int AV16ContratoServicos_Codigo ;
      private int A74Contrato_Codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A631Servico_Vinculado ;
      private int A157ServicoGrupo_Codigo ;
      private int A1212ContratoServicos_UnidadeContratada ;
      private int A1516ContratoServicos_TmpEstAnl ;
      private int A1501ContratoServicos_TmpEstExc ;
      private int A1502ContratoServicos_TmpEstCrr ;
      private int A160ContratoServicos_Codigo ;
      private int A155Servico_Codigo ;
      private int A1551Servico_Responsavel ;
      private int A827ContratoServicos_ServicoCod ;
      private int A40000ContratoServicos_QtdRmn ;
      private int AV30GXV1 ;
      private int GXt_int1 ;
      private int AV32GXV2 ;
      private int AV33GXV3 ;
      private int AV35GXV4 ;
      private int AV37GXV5 ;
      private int AV38GXV6 ;
      private int AV39GXV7 ;
      private long A555Servico_QtdContratada ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private decimal A558Servico_Percentual ;
      private decimal A1191ContratoServicos_Produtividade ;
      private decimal A1340ContratoServicos_QntUntCns ;
      private decimal A1341ContratoServicos_FatorCnvUndCnt ;
      private decimal A453Contrato_IndiceDivergencia ;
      private decimal A1455ContratoServicos_IndiceDivergencia ;
      private decimal A1799ContratoServicos_LimiteProposta ;
      private decimal A2070ContratoServicosRmn_Inicio ;
      private decimal A2071ContratoServicosRmn_Fim ;
      private decimal A2072ContratoServicosRmn_Valor ;
      private String AV13SaveOldValues ;
      private String AV14ActualMode ;
      private String scmdbuf ;
      private String A77Contrato_Numero ;
      private String A78Contrato_NumeroAta ;
      private String A41Contratada_PessoaNom ;
      private String A516Contratada_TipoFabrica ;
      private String A1858ContratoServicos_Alias ;
      private String A1061Servico_Tela ;
      private String A2132ContratoServicos_UndCntNome ;
      private String A1712ContratoServicos_UndCntSgl ;
      private String A639ContratoServicos_LocalExec ;
      private String A868ContratoServicos_TipoVnc ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String A1225ContratoServicos_PrazoCorrecaoTipo ;
      private String A1266ContratoServicos_Momento ;
      private String A1325ContratoServicos_StatusPagFnc ;
      private String A1723ContratoServicos_CodigoFiscal ;
      private String A913ContratoServicos_PrazoTipo ;
      private String A605Servico_Sigla ;
      private String A608Servico_Nome ;
      private String A826ContratoServicos_ServicoSigla ;
      private bool returnInSub ;
      private bool n78Contrato_NumeroAta ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n1858ContratoServicos_Alias ;
      private bool n1061Servico_Tela ;
      private bool A632Servico_Ativo ;
      private bool n156Servico_Descricao ;
      private bool n631Servico_Vinculado ;
      private bool A2092Servico_IsOrigemReferencia ;
      private bool n2132ContratoServicos_UndCntNome ;
      private bool n1712ContratoServicos_UndCntSgl ;
      private bool n555Servico_QtdContratada ;
      private bool n558Servico_Percentual ;
      private bool n868ContratoServicos_TipoVnc ;
      private bool A888ContratoServicos_HmlSemCnf ;
      private bool n888ContratoServicos_HmlSemCnf ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private bool n1152ContratoServicos_PrazoAnalise ;
      private bool n1153ContratoServicos_PrazoResposta ;
      private bool n1181ContratoServicos_PrazoGarantia ;
      private bool n1182ContratoServicos_PrazoAtendeGarantia ;
      private bool n1224ContratoServicos_PrazoCorrecao ;
      private bool n1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool n1649ContratoServicos_PrazoInicio ;
      private bool A1190ContratoServicos_PrazoImediato ;
      private bool n1190ContratoServicos_PrazoImediato ;
      private bool n1191ContratoServicos_Produtividade ;
      private bool A1217ContratoServicos_EspelhaAceite ;
      private bool n1217ContratoServicos_EspelhaAceite ;
      private bool n1266ContratoServicos_Momento ;
      private bool n1325ContratoServicos_StatusPagFnc ;
      private bool n1340ContratoServicos_QntUntCns ;
      private bool n1341ContratoServicos_FatorCnvUndCnt ;
      private bool A1397ContratoServicos_NaoRequerAtr ;
      private bool n1397ContratoServicos_NaoRequerAtr ;
      private bool n1455ContratoServicos_IndiceDivergencia ;
      private bool n1516ContratoServicos_TmpEstAnl ;
      private bool n1501ContratoServicos_TmpEstExc ;
      private bool n1502ContratoServicos_TmpEstCrr ;
      private bool n1531ContratoServicos_TipoHierarquia ;
      private bool n1537ContratoServicos_PercTmp ;
      private bool n1538ContratoServicos_PercPgm ;
      private bool n1539ContratoServicos_PercCnc ;
      private bool A638ContratoServicos_Ativo ;
      private bool n1723ContratoServicos_CodigoFiscal ;
      private bool n1799ContratoServicos_LimiteProposta ;
      private bool n2074ContratoServicos_CalculoRmn ;
      private bool A2094ContratoServicos_SolicitaGestorSistema ;
      private bool n913ContratoServicos_PrazoTipo ;
      private bool n914ContratoServicos_PrazoDias ;
      private bool n911ContratoServicos_Prazos ;
      private bool n1377ContratoServicos_Indicadores ;
      private bool n2073ContratoServicos_QtdRmn ;
      private bool n40000ContratoServicos_QtdRmn ;
      private bool AV23RecordExistsContratoServicosRmn_Sequencial ;
      private String A156Servico_Descricao ;
      private String A42Contratada_PessoaCNPJ ;
      private String A158ServicoGrupo_Descricao ;
      private String A607ServicoContrato_Faturamento ;
      private String A2107Servico_Identificacao ;
      private String AV22KeyContratoServicosRmn_Sequencial ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ;
      private IDataStoreProvider pr_default ;
      private int[] P00A55_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] P00A55_A74Contrato_Codigo ;
      private String[] P00A55_A77Contrato_Numero ;
      private String[] P00A55_A78Contrato_NumeroAta ;
      private bool[] P00A55_n78Contrato_NumeroAta ;
      private short[] P00A55_A79Contrato_Ano ;
      private decimal[] P00A55_A116Contrato_ValorUnidadeContratacao ;
      private int[] P00A55_A75Contrato_AreaTrabalhoCod ;
      private int[] P00A55_A39Contratada_Codigo ;
      private int[] P00A55_A40Contratada_PessoaCod ;
      private String[] P00A55_A41Contratada_PessoaNom ;
      private bool[] P00A55_n41Contratada_PessoaNom ;
      private String[] P00A55_A42Contratada_PessoaCNPJ ;
      private bool[] P00A55_n42Contratada_PessoaCNPJ ;
      private String[] P00A55_A516Contratada_TipoFabrica ;
      private String[] P00A55_A1858ContratoServicos_Alias ;
      private bool[] P00A55_n1858ContratoServicos_Alias ;
      private String[] P00A55_A1061Servico_Tela ;
      private bool[] P00A55_n1061Servico_Tela ;
      private bool[] P00A55_A632Servico_Ativo ;
      private String[] P00A55_A156Servico_Descricao ;
      private bool[] P00A55_n156Servico_Descricao ;
      private int[] P00A55_A631Servico_Vinculado ;
      private bool[] P00A55_n631Servico_Vinculado ;
      private int[] P00A55_A157ServicoGrupo_Codigo ;
      private String[] P00A55_A158ServicoGrupo_Descricao ;
      private bool[] P00A55_A2092Servico_IsOrigemReferencia ;
      private int[] P00A55_A1212ContratoServicos_UnidadeContratada ;
      private String[] P00A55_A2132ContratoServicos_UndCntNome ;
      private bool[] P00A55_n2132ContratoServicos_UndCntNome ;
      private String[] P00A55_A1712ContratoServicos_UndCntSgl ;
      private bool[] P00A55_n1712ContratoServicos_UndCntSgl ;
      private long[] P00A55_A555Servico_QtdContratada ;
      private bool[] P00A55_n555Servico_QtdContratada ;
      private decimal[] P00A55_A557Servico_VlrUnidadeContratada ;
      private decimal[] P00A55_A558Servico_Percentual ;
      private bool[] P00A55_n558Servico_Percentual ;
      private String[] P00A55_A607ServicoContrato_Faturamento ;
      private String[] P00A55_A639ContratoServicos_LocalExec ;
      private String[] P00A55_A868ContratoServicos_TipoVnc ;
      private bool[] P00A55_n868ContratoServicos_TipoVnc ;
      private bool[] P00A55_A888ContratoServicos_HmlSemCnf ;
      private bool[] P00A55_n888ContratoServicos_HmlSemCnf ;
      private String[] P00A55_A1454ContratoServicos_PrazoTpDias ;
      private bool[] P00A55_n1454ContratoServicos_PrazoTpDias ;
      private short[] P00A55_A1152ContratoServicos_PrazoAnalise ;
      private bool[] P00A55_n1152ContratoServicos_PrazoAnalise ;
      private short[] P00A55_A1153ContratoServicos_PrazoResposta ;
      private bool[] P00A55_n1153ContratoServicos_PrazoResposta ;
      private short[] P00A55_A1181ContratoServicos_PrazoGarantia ;
      private bool[] P00A55_n1181ContratoServicos_PrazoGarantia ;
      private short[] P00A55_A1182ContratoServicos_PrazoAtendeGarantia ;
      private bool[] P00A55_n1182ContratoServicos_PrazoAtendeGarantia ;
      private short[] P00A55_A1224ContratoServicos_PrazoCorrecao ;
      private bool[] P00A55_n1224ContratoServicos_PrazoCorrecao ;
      private String[] P00A55_A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool[] P00A55_n1225ContratoServicos_PrazoCorrecaoTipo ;
      private short[] P00A55_A1649ContratoServicos_PrazoInicio ;
      private bool[] P00A55_n1649ContratoServicos_PrazoInicio ;
      private bool[] P00A55_A1190ContratoServicos_PrazoImediato ;
      private bool[] P00A55_n1190ContratoServicos_PrazoImediato ;
      private decimal[] P00A55_A1191ContratoServicos_Produtividade ;
      private bool[] P00A55_n1191ContratoServicos_Produtividade ;
      private bool[] P00A55_A1217ContratoServicos_EspelhaAceite ;
      private bool[] P00A55_n1217ContratoServicos_EspelhaAceite ;
      private String[] P00A55_A1266ContratoServicos_Momento ;
      private bool[] P00A55_n1266ContratoServicos_Momento ;
      private String[] P00A55_A1325ContratoServicos_StatusPagFnc ;
      private bool[] P00A55_n1325ContratoServicos_StatusPagFnc ;
      private decimal[] P00A55_A1340ContratoServicos_QntUntCns ;
      private bool[] P00A55_n1340ContratoServicos_QntUntCns ;
      private decimal[] P00A55_A1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] P00A55_n1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] P00A55_A1397ContratoServicos_NaoRequerAtr ;
      private bool[] P00A55_n1397ContratoServicos_NaoRequerAtr ;
      private decimal[] P00A55_A453Contrato_IndiceDivergencia ;
      private decimal[] P00A55_A1455ContratoServicos_IndiceDivergencia ;
      private bool[] P00A55_n1455ContratoServicos_IndiceDivergencia ;
      private int[] P00A55_A1516ContratoServicos_TmpEstAnl ;
      private bool[] P00A55_n1516ContratoServicos_TmpEstAnl ;
      private int[] P00A55_A1501ContratoServicos_TmpEstExc ;
      private bool[] P00A55_n1501ContratoServicos_TmpEstExc ;
      private int[] P00A55_A1502ContratoServicos_TmpEstCrr ;
      private bool[] P00A55_n1502ContratoServicos_TmpEstCrr ;
      private short[] P00A55_A1531ContratoServicos_TipoHierarquia ;
      private bool[] P00A55_n1531ContratoServicos_TipoHierarquia ;
      private short[] P00A55_A1537ContratoServicos_PercTmp ;
      private bool[] P00A55_n1537ContratoServicos_PercTmp ;
      private short[] P00A55_A1538ContratoServicos_PercPgm ;
      private bool[] P00A55_n1538ContratoServicos_PercPgm ;
      private short[] P00A55_A1539ContratoServicos_PercCnc ;
      private bool[] P00A55_n1539ContratoServicos_PercCnc ;
      private bool[] P00A55_A638ContratoServicos_Ativo ;
      private String[] P00A55_A1723ContratoServicos_CodigoFiscal ;
      private bool[] P00A55_n1723ContratoServicos_CodigoFiscal ;
      private decimal[] P00A55_A1799ContratoServicos_LimiteProposta ;
      private bool[] P00A55_n1799ContratoServicos_LimiteProposta ;
      private short[] P00A55_A2074ContratoServicos_CalculoRmn ;
      private bool[] P00A55_n2074ContratoServicos_CalculoRmn ;
      private bool[] P00A55_A2094ContratoServicos_SolicitaGestorSistema ;
      private String[] P00A55_A913ContratoServicos_PrazoTipo ;
      private bool[] P00A55_n913ContratoServicos_PrazoTipo ;
      private short[] P00A55_A914ContratoServicos_PrazoDias ;
      private bool[] P00A55_n914ContratoServicos_PrazoDias ;
      private short[] P00A55_A911ContratoServicos_Prazos ;
      private bool[] P00A55_n911ContratoServicos_Prazos ;
      private int[] P00A55_A160ContratoServicos_Codigo ;
      private short[] P00A55_A1377ContratoServicos_Indicadores ;
      private bool[] P00A55_n1377ContratoServicos_Indicadores ;
      private short[] P00A55_A2073ContratoServicos_QtdRmn ;
      private bool[] P00A55_n2073ContratoServicos_QtdRmn ;
      private String[] P00A55_A605Servico_Sigla ;
      private String[] P00A55_A608Servico_Nome ;
      private int[] P00A55_A155Servico_Codigo ;
      private int[] P00A56_A160ContratoServicos_Codigo ;
      private short[] P00A56_A2069ContratoServicosRmn_Sequencial ;
      private decimal[] P00A56_A2070ContratoServicosRmn_Inicio ;
      private decimal[] P00A56_A2071ContratoServicosRmn_Fim ;
      private decimal[] P00A56_A2072ContratoServicosRmn_Valor ;
      private int[] P00A511_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] P00A511_A74Contrato_Codigo ;
      private String[] P00A511_A77Contrato_Numero ;
      private String[] P00A511_A78Contrato_NumeroAta ;
      private bool[] P00A511_n78Contrato_NumeroAta ;
      private short[] P00A511_A79Contrato_Ano ;
      private decimal[] P00A511_A116Contrato_ValorUnidadeContratacao ;
      private int[] P00A511_A75Contrato_AreaTrabalhoCod ;
      private int[] P00A511_A39Contratada_Codigo ;
      private int[] P00A511_A40Contratada_PessoaCod ;
      private String[] P00A511_A41Contratada_PessoaNom ;
      private bool[] P00A511_n41Contratada_PessoaNom ;
      private String[] P00A511_A42Contratada_PessoaCNPJ ;
      private bool[] P00A511_n42Contratada_PessoaCNPJ ;
      private String[] P00A511_A516Contratada_TipoFabrica ;
      private String[] P00A511_A1858ContratoServicos_Alias ;
      private bool[] P00A511_n1858ContratoServicos_Alias ;
      private String[] P00A511_A1061Servico_Tela ;
      private bool[] P00A511_n1061Servico_Tela ;
      private bool[] P00A511_A632Servico_Ativo ;
      private String[] P00A511_A156Servico_Descricao ;
      private bool[] P00A511_n156Servico_Descricao ;
      private int[] P00A511_A631Servico_Vinculado ;
      private bool[] P00A511_n631Servico_Vinculado ;
      private int[] P00A511_A157ServicoGrupo_Codigo ;
      private String[] P00A511_A158ServicoGrupo_Descricao ;
      private bool[] P00A511_A2092Servico_IsOrigemReferencia ;
      private int[] P00A511_A1212ContratoServicos_UnidadeContratada ;
      private String[] P00A511_A2132ContratoServicos_UndCntNome ;
      private bool[] P00A511_n2132ContratoServicos_UndCntNome ;
      private String[] P00A511_A1712ContratoServicos_UndCntSgl ;
      private bool[] P00A511_n1712ContratoServicos_UndCntSgl ;
      private long[] P00A511_A555Servico_QtdContratada ;
      private bool[] P00A511_n555Servico_QtdContratada ;
      private decimal[] P00A511_A557Servico_VlrUnidadeContratada ;
      private decimal[] P00A511_A558Servico_Percentual ;
      private bool[] P00A511_n558Servico_Percentual ;
      private String[] P00A511_A607ServicoContrato_Faturamento ;
      private String[] P00A511_A639ContratoServicos_LocalExec ;
      private String[] P00A511_A868ContratoServicos_TipoVnc ;
      private bool[] P00A511_n868ContratoServicos_TipoVnc ;
      private bool[] P00A511_A888ContratoServicos_HmlSemCnf ;
      private bool[] P00A511_n888ContratoServicos_HmlSemCnf ;
      private String[] P00A511_A1454ContratoServicos_PrazoTpDias ;
      private bool[] P00A511_n1454ContratoServicos_PrazoTpDias ;
      private short[] P00A511_A1152ContratoServicos_PrazoAnalise ;
      private bool[] P00A511_n1152ContratoServicos_PrazoAnalise ;
      private short[] P00A511_A1153ContratoServicos_PrazoResposta ;
      private bool[] P00A511_n1153ContratoServicos_PrazoResposta ;
      private short[] P00A511_A1181ContratoServicos_PrazoGarantia ;
      private bool[] P00A511_n1181ContratoServicos_PrazoGarantia ;
      private short[] P00A511_A1182ContratoServicos_PrazoAtendeGarantia ;
      private bool[] P00A511_n1182ContratoServicos_PrazoAtendeGarantia ;
      private short[] P00A511_A1224ContratoServicos_PrazoCorrecao ;
      private bool[] P00A511_n1224ContratoServicos_PrazoCorrecao ;
      private String[] P00A511_A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool[] P00A511_n1225ContratoServicos_PrazoCorrecaoTipo ;
      private short[] P00A511_A1649ContratoServicos_PrazoInicio ;
      private bool[] P00A511_n1649ContratoServicos_PrazoInicio ;
      private bool[] P00A511_A1190ContratoServicos_PrazoImediato ;
      private bool[] P00A511_n1190ContratoServicos_PrazoImediato ;
      private decimal[] P00A511_A1191ContratoServicos_Produtividade ;
      private bool[] P00A511_n1191ContratoServicos_Produtividade ;
      private bool[] P00A511_A1217ContratoServicos_EspelhaAceite ;
      private bool[] P00A511_n1217ContratoServicos_EspelhaAceite ;
      private String[] P00A511_A1266ContratoServicos_Momento ;
      private bool[] P00A511_n1266ContratoServicos_Momento ;
      private String[] P00A511_A1325ContratoServicos_StatusPagFnc ;
      private bool[] P00A511_n1325ContratoServicos_StatusPagFnc ;
      private decimal[] P00A511_A1340ContratoServicos_QntUntCns ;
      private bool[] P00A511_n1340ContratoServicos_QntUntCns ;
      private decimal[] P00A511_A1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] P00A511_n1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] P00A511_A1397ContratoServicos_NaoRequerAtr ;
      private bool[] P00A511_n1397ContratoServicos_NaoRequerAtr ;
      private decimal[] P00A511_A453Contrato_IndiceDivergencia ;
      private decimal[] P00A511_A1455ContratoServicos_IndiceDivergencia ;
      private bool[] P00A511_n1455ContratoServicos_IndiceDivergencia ;
      private int[] P00A511_A1516ContratoServicos_TmpEstAnl ;
      private bool[] P00A511_n1516ContratoServicos_TmpEstAnl ;
      private int[] P00A511_A1501ContratoServicos_TmpEstExc ;
      private bool[] P00A511_n1501ContratoServicos_TmpEstExc ;
      private int[] P00A511_A1502ContratoServicos_TmpEstCrr ;
      private bool[] P00A511_n1502ContratoServicos_TmpEstCrr ;
      private short[] P00A511_A1531ContratoServicos_TipoHierarquia ;
      private bool[] P00A511_n1531ContratoServicos_TipoHierarquia ;
      private short[] P00A511_A1537ContratoServicos_PercTmp ;
      private bool[] P00A511_n1537ContratoServicos_PercTmp ;
      private short[] P00A511_A1538ContratoServicos_PercPgm ;
      private bool[] P00A511_n1538ContratoServicos_PercPgm ;
      private short[] P00A511_A1539ContratoServicos_PercCnc ;
      private bool[] P00A511_n1539ContratoServicos_PercCnc ;
      private bool[] P00A511_A638ContratoServicos_Ativo ;
      private String[] P00A511_A1723ContratoServicos_CodigoFiscal ;
      private bool[] P00A511_n1723ContratoServicos_CodigoFiscal ;
      private decimal[] P00A511_A1799ContratoServicos_LimiteProposta ;
      private bool[] P00A511_n1799ContratoServicos_LimiteProposta ;
      private short[] P00A511_A2074ContratoServicos_CalculoRmn ;
      private bool[] P00A511_n2074ContratoServicos_CalculoRmn ;
      private bool[] P00A511_A2094ContratoServicos_SolicitaGestorSistema ;
      private String[] P00A511_A913ContratoServicos_PrazoTipo ;
      private bool[] P00A511_n913ContratoServicos_PrazoTipo ;
      private short[] P00A511_A914ContratoServicos_PrazoDias ;
      private bool[] P00A511_n914ContratoServicos_PrazoDias ;
      private short[] P00A511_A911ContratoServicos_Prazos ;
      private bool[] P00A511_n911ContratoServicos_Prazos ;
      private int[] P00A511_A160ContratoServicos_Codigo ;
      private short[] P00A511_A1377ContratoServicos_Indicadores ;
      private bool[] P00A511_n1377ContratoServicos_Indicadores ;
      private short[] P00A511_A2073ContratoServicos_QtdRmn ;
      private bool[] P00A511_n2073ContratoServicos_QtdRmn ;
      private int[] P00A511_A40000ContratoServicos_QtdRmn ;
      private bool[] P00A511_n40000ContratoServicos_QtdRmn ;
      private String[] P00A511_A605Servico_Sigla ;
      private String[] P00A511_A608Servico_Nome ;
      private int[] P00A511_A155Servico_Codigo ;
      private int[] P00A512_A160ContratoServicos_Codigo ;
      private short[] P00A512_A2069ContratoServicosRmn_Sequencial ;
      private decimal[] P00A512_A2070ContratoServicosRmn_Inicio ;
      private decimal[] P00A512_A2071ContratoServicosRmn_Fim ;
      private decimal[] P00A512_A2072ContratoServicosRmn_Valor ;
      private int[] P00A513_A160ContratoServicos_Codigo ;
      private short[] P00A513_A2069ContratoServicosRmn_Sequencial ;
      private decimal[] P00A513_A2070ContratoServicosRmn_Inicio ;
      private decimal[] P00A513_A2071ContratoServicosRmn_Fim ;
      private decimal[] P00A513_A2072ContratoServicosRmn_Valor ;
      private int[] P00A514_A160ContratoServicos_Codigo ;
      private short[] P00A514_A2069ContratoServicosRmn_Sequencial ;
      private decimal[] P00A514_A2070ContratoServicosRmn_Inicio ;
      private decimal[] P00A514_A2071ContratoServicosRmn_Fim ;
      private decimal[] P00A514_A2072ContratoServicosRmn_Valor ;
      private wwpbaseobjects.SdtAuditingObject AV10AuditingObject ;
      private wwpbaseobjects.SdtAuditingObject AV17AuditingObjectNewRecords ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV11AuditingObjectRecordItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV18AuditingObjectRecordItemNew ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV12AuditingObjectRecordItemAttributeItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV19AuditingObjectRecordItemAttributeItemNew ;
   }

   public class loadauditcontratoservicos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00A55 ;
          prmP00A55 = new Object[] {
          new Object[] {"@AV16ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00A55 ;
          cmdBufferP00A55=" SELECT T6.[ContratoServicosPrazo_CntSrvCod], T1.[Contrato_Codigo], T2.[Contrato_Numero], T2.[Contrato_NumeroAta], T2.[Contrato_Ano], T2.[Contrato_ValorUnidadeContratacao], T2.[Contrato_AreaTrabalhoCod], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T3.[Contratada_TipoFabrica], T1.[ContratoServicos_Alias], T10.[Servico_Tela], T10.[Servico_Ativo], T10.[Servico_Descricao], T10.[Servico_Vinculado], T10.[ServicoGrupo_Codigo], T11.[ServicoGrupo_Descricao], T10.[Servico_IsOrigemReferencia], T1.[ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada, T5.[UnidadeMedicao_Nome] AS ContratoServicos_UndCntNome, T5.[UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl, T1.[Servico_QtdContratada], T1.[Servico_VlrUnidadeContratada], T1.[Servico_Percentual], T1.[ServicoContrato_Faturamento], T1.[ContratoServicos_LocalExec], T1.[ContratoServicos_TipoVnc], T1.[ContratoServicos_HmlSemCnf], T1.[ContratoServicos_PrazoTpDias], T1.[ContratoServicos_PrazoAnalise], T1.[ContratoServicos_PrazoResposta], T1.[ContratoServicos_PrazoGarantia], T1.[ContratoServicos_PrazoAtendeGarantia], T1.[ContratoServicos_PrazoCorrecao], T1.[ContratoServicos_PrazoCorrecaoTipo], T1.[ContratoServicos_PrazoInicio], T1.[ContratoServicos_PrazoImediato], T1.[ContratoServicos_Produtividade], T1.[ContratoServicos_EspelhaAceite], T1.[ContratoServicos_Momento], T1.[ContratoServicos_StatusPagFnc], T1.[ContratoServicos_QntUntCns], T1.[ContratoServicos_FatorCnvUndCnt], T1.[ContratoServicos_NaoRequerAtr], T2.[Contrato_IndiceDivergencia], T1.[ContratoServicos_IndiceDivergencia], T1.[ContratoServicos_TmpEstAnl], T1.[ContratoServicos_TmpEstExc], T1.[ContratoServicos_TmpEstCrr], T1.[ContratoServicos_TipoHierarquia], "
          + " T1.[ContratoServicos_PercTmp], T1.[ContratoServicos_PercPgm], T1.[ContratoServicos_PercCnc], T1.[ContratoServicos_Ativo], T1.[ContratoServicos_CodigoFiscal], T1.[ContratoServicos_LimiteProposta], T1.[ContratoServicos_CalculoRmn], T1.[ContratoServicos_SolicitaGestorSistema], COALESCE( T6.[ContratoServicosPrazo_Tipo], '') AS ContratoServicos_PrazoTipo, COALESCE( T6.[ContratoServicosPrazo_Dias], 0) AS ContratoServicos_PrazoDias, COALESCE( T7.[ContratoServicos_Prazos], 0) AS ContratoServicos_Prazos, T1.[ContratoServicos_Codigo], COALESCE( T8.[ContratoServicos_Indicadores], 0) AS ContratoServicos_Indicadores, COALESCE( T9.[ContratoServicos_QtdRmn], 0) AS ContratoServicos_QtdRmn, T10.[Servico_Sigla], T10.[Servico_Nome], T1.[Servico_Codigo] FROM (((((((((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) INNER JOIN [UnidadeMedicao] T5 WITH (NOLOCK) ON T5.[UnidadeMedicao_Codigo] = T1.[ContratoServicos_UnidadeContratada]) LEFT JOIN [ContratoServicosPrazo] T6 WITH (NOLOCK) ON T6.[ContratoServicosPrazo_CntSrvCod] = T1.[ContratoServicos_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_Prazos, T13.[ContratoServicos_Codigo] FROM [ContratoServicosPrazo] T12 WITH (NOLOCK),  [ContratoServicos] T13 WITH (NOLOCK) WHERE T12.[ContratoServicosPrazo_CntSrvCod] = T13.[ContratoServicos_Codigo] GROUP BY T13.[ContratoServicos_Codigo] ) T7 ON T7.[ContratoServicos_Codigo] = T1.[ContratoServicos_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_Indicadores, [ContratoServicosIndicador_CntSrvCod] FROM [ContratoServicosIndicador] WITH (NOLOCK)"
          + " GROUP BY [ContratoServicosIndicador_CntSrvCod] ) T8 ON T8.[ContratoServicosIndicador_CntSrvCod] = T1.[ContratoServicos_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_QtdRmn, [ContratoServicos_Codigo] FROM [ContratoServicosRmn] WITH (NOLOCK) GROUP BY [ContratoServicos_Codigo] ) T9 ON T9.[ContratoServicos_Codigo] = T1.[ContratoServicos_Codigo]) INNER JOIN [Servico] T10 WITH (NOLOCK) ON T10.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [ServicoGrupo] T11 WITH (NOLOCK) ON T11.[ServicoGrupo_Codigo] = T10.[ServicoGrupo_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @AV16ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo]" ;
          Object[] prmP00A56 ;
          prmP00A56 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00A511 ;
          prmP00A511 = new Object[] {
          new Object[] {"@AV16ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00A511 ;
          cmdBufferP00A511=" SELECT T6.[ContratoServicosPrazo_CntSrvCod], T1.[Contrato_Codigo], T2.[Contrato_Numero], T2.[Contrato_NumeroAta], T2.[Contrato_Ano], T2.[Contrato_ValorUnidadeContratacao], T2.[Contrato_AreaTrabalhoCod], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T3.[Contratada_TipoFabrica], T1.[ContratoServicos_Alias], T10.[Servico_Tela], T10.[Servico_Ativo], T10.[Servico_Descricao], T10.[Servico_Vinculado], T10.[ServicoGrupo_Codigo], T11.[ServicoGrupo_Descricao], T10.[Servico_IsOrigemReferencia], T1.[ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada, T5.[UnidadeMedicao_Nome] AS ContratoServicos_UndCntNome, T5.[UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl, T1.[Servico_QtdContratada], T1.[Servico_VlrUnidadeContratada], T1.[Servico_Percentual], T1.[ServicoContrato_Faturamento], T1.[ContratoServicos_LocalExec], T1.[ContratoServicos_TipoVnc], T1.[ContratoServicos_HmlSemCnf], T1.[ContratoServicos_PrazoTpDias], T1.[ContratoServicos_PrazoAnalise], T1.[ContratoServicos_PrazoResposta], T1.[ContratoServicos_PrazoGarantia], T1.[ContratoServicos_PrazoAtendeGarantia], T1.[ContratoServicos_PrazoCorrecao], T1.[ContratoServicos_PrazoCorrecaoTipo], T1.[ContratoServicos_PrazoInicio], T1.[ContratoServicos_PrazoImediato], T1.[ContratoServicos_Produtividade], T1.[ContratoServicos_EspelhaAceite], T1.[ContratoServicos_Momento], T1.[ContratoServicos_StatusPagFnc], T1.[ContratoServicos_QntUntCns], T1.[ContratoServicos_FatorCnvUndCnt], T1.[ContratoServicos_NaoRequerAtr], T2.[Contrato_IndiceDivergencia], T1.[ContratoServicos_IndiceDivergencia], T1.[ContratoServicos_TmpEstAnl], T1.[ContratoServicos_TmpEstExc], T1.[ContratoServicos_TmpEstCrr], T1.[ContratoServicos_TipoHierarquia], "
          + " T1.[ContratoServicos_PercTmp], T1.[ContratoServicos_PercPgm], T1.[ContratoServicos_PercCnc], T1.[ContratoServicos_Ativo], T1.[ContratoServicos_CodigoFiscal], T1.[ContratoServicos_LimiteProposta], T1.[ContratoServicos_CalculoRmn], T1.[ContratoServicos_SolicitaGestorSistema], COALESCE( T6.[ContratoServicosPrazo_Tipo], '') AS ContratoServicos_PrazoTipo, COALESCE( T6.[ContratoServicosPrazo_Dias], 0) AS ContratoServicos_PrazoDias, COALESCE( T7.[ContratoServicos_Prazos], 0) AS ContratoServicos_Prazos, T1.[ContratoServicos_Codigo], COALESCE( T8.[ContratoServicos_Indicadores], 0) AS ContratoServicos_Indicadores, COALESCE( T9.[ContratoServicos_QtdRmn], 0) AS ContratoServicos_QtdRmn, COALESCE( T12.[ContratoServicos_QtdRmn], 0) AS GXC1, T10.[Servico_Sigla], T10.[Servico_Nome], T1.[Servico_Codigo] FROM (((((((((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) INNER JOIN [UnidadeMedicao] T5 WITH (NOLOCK) ON T5.[UnidadeMedicao_Codigo] = T1.[ContratoServicos_UnidadeContratada]) LEFT JOIN [ContratoServicosPrazo] T6 WITH (NOLOCK) ON T6.[ContratoServicosPrazo_CntSrvCod] = T1.[ContratoServicos_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_Prazos, T14.[ContratoServicos_Codigo] FROM [ContratoServicosPrazo] T13 WITH (NOLOCK),  [ContratoServicos] T14 WITH (NOLOCK) WHERE T13.[ContratoServicosPrazo_CntSrvCod] = T14.[ContratoServicos_Codigo] GROUP BY T14.[ContratoServicos_Codigo] ) T7 ON T7.[ContratoServicos_Codigo] = T1.[ContratoServicos_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_Indicadores, [ContratoServicosIndicador_CntSrvCod]"
          + " FROM [ContratoServicosIndicador] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_CntSrvCod] ) T8 ON T8.[ContratoServicosIndicador_CntSrvCod] = T1.[ContratoServicos_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_QtdRmn, [ContratoServicos_Codigo] FROM [ContratoServicosRmn] WITH (NOLOCK) GROUP BY [ContratoServicos_Codigo] ) T9 ON T9.[ContratoServicos_Codigo] = T1.[ContratoServicos_Codigo]) INNER JOIN [Servico] T10 WITH (NOLOCK) ON T10.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [ServicoGrupo] T11 WITH (NOLOCK) ON T11.[ServicoGrupo_Codigo] = T10.[ServicoGrupo_Codigo]),  (SELECT COUNT(*) AS ContratoServicos_QtdRmn FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV16ContratoServicos_Codigo ) T12 WHERE T1.[ContratoServicos_Codigo] = @AV16ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo]" ;
          Object[] prmP00A512 ;
          prmP00A512 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00A513 ;
          prmP00A513 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00A514 ;
          prmP00A514 = new Object[] {
          new Object[] {"@AV16ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00A55", cmdBufferP00A55,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A55,1,0,true,true )
             ,new CursorDef("P00A56", "SELECT [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial], [ContratoServicosRmn_Inicio], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Valor] FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A56,100,0,false,false )
             ,new CursorDef("P00A511", cmdBufferP00A511,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A511,1,0,true,true )
             ,new CursorDef("P00A512", "SELECT [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial], [ContratoServicosRmn_Inicio], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Valor] FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A512,100,0,false,false )
             ,new CursorDef("P00A513", "SELECT [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial], [ContratoServicosRmn_Inicio], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Valor] FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A513,100,0,false,false )
             ,new CursorDef("P00A514", "SELECT [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial], [ContratoServicosRmn_Inicio], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Valor] FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV16ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A514,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((String[]) buf[10])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(10);
                ((String[]) buf[12])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(11);
                ((String[]) buf[14])[0] = rslt.getString(12, 1) ;
                ((String[]) buf[15])[0] = rslt.getString(13, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(13);
                ((String[]) buf[17])[0] = rslt.getString(14, 3) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(14);
                ((bool[]) buf[19])[0] = rslt.getBool(15) ;
                ((String[]) buf[20])[0] = rslt.getLongVarchar(16) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(16);
                ((int[]) buf[22])[0] = rslt.getInt(17) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(17);
                ((int[]) buf[24])[0] = rslt.getInt(18) ;
                ((String[]) buf[25])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[26])[0] = rslt.getBool(20) ;
                ((int[]) buf[27])[0] = rslt.getInt(21) ;
                ((String[]) buf[28])[0] = rslt.getString(22, 50) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(22);
                ((String[]) buf[30])[0] = rslt.getString(23, 15) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(23);
                ((long[]) buf[32])[0] = rslt.getLong(24) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(24);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(25) ;
                ((decimal[]) buf[35])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(26);
                ((String[]) buf[37])[0] = rslt.getVarchar(27) ;
                ((String[]) buf[38])[0] = rslt.getString(28, 1) ;
                ((String[]) buf[39])[0] = rslt.getString(29, 1) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(29);
                ((bool[]) buf[41])[0] = rslt.getBool(30) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(30);
                ((String[]) buf[43])[0] = rslt.getString(31, 1) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(31);
                ((short[]) buf[45])[0] = rslt.getShort(32) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(32);
                ((short[]) buf[47])[0] = rslt.getShort(33) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(33);
                ((short[]) buf[49])[0] = rslt.getShort(34) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(34);
                ((short[]) buf[51])[0] = rslt.getShort(35) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(35);
                ((short[]) buf[53])[0] = rslt.getShort(36) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(36);
                ((String[]) buf[55])[0] = rslt.getString(37, 1) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(37);
                ((short[]) buf[57])[0] = rslt.getShort(38) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(38);
                ((bool[]) buf[59])[0] = rslt.getBool(39) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(39);
                ((decimal[]) buf[61])[0] = rslt.getDecimal(40) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(40);
                ((bool[]) buf[63])[0] = rslt.getBool(41) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(41);
                ((String[]) buf[65])[0] = rslt.getString(42, 1) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(42);
                ((String[]) buf[67])[0] = rslt.getString(43, 1) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(43);
                ((decimal[]) buf[69])[0] = rslt.getDecimal(44) ;
                ((bool[]) buf[70])[0] = rslt.wasNull(44);
                ((decimal[]) buf[71])[0] = rslt.getDecimal(45) ;
                ((bool[]) buf[72])[0] = rslt.wasNull(45);
                ((bool[]) buf[73])[0] = rslt.getBool(46) ;
                ((bool[]) buf[74])[0] = rslt.wasNull(46);
                ((decimal[]) buf[75])[0] = rslt.getDecimal(47) ;
                ((decimal[]) buf[76])[0] = rslt.getDecimal(48) ;
                ((bool[]) buf[77])[0] = rslt.wasNull(48);
                ((int[]) buf[78])[0] = rslt.getInt(49) ;
                ((bool[]) buf[79])[0] = rslt.wasNull(49);
                ((int[]) buf[80])[0] = rslt.getInt(50) ;
                ((bool[]) buf[81])[0] = rslt.wasNull(50);
                ((int[]) buf[82])[0] = rslt.getInt(51) ;
                ((bool[]) buf[83])[0] = rslt.wasNull(51);
                ((short[]) buf[84])[0] = rslt.getShort(52) ;
                ((bool[]) buf[85])[0] = rslt.wasNull(52);
                ((short[]) buf[86])[0] = rslt.getShort(53) ;
                ((bool[]) buf[87])[0] = rslt.wasNull(53);
                ((short[]) buf[88])[0] = rslt.getShort(54) ;
                ((bool[]) buf[89])[0] = rslt.wasNull(54);
                ((short[]) buf[90])[0] = rslt.getShort(55) ;
                ((bool[]) buf[91])[0] = rslt.wasNull(55);
                ((bool[]) buf[92])[0] = rslt.getBool(56) ;
                ((String[]) buf[93])[0] = rslt.getString(57, 5) ;
                ((bool[]) buf[94])[0] = rslt.wasNull(57);
                ((decimal[]) buf[95])[0] = rslt.getDecimal(58) ;
                ((bool[]) buf[96])[0] = rslt.wasNull(58);
                ((short[]) buf[97])[0] = rslt.getShort(59) ;
                ((bool[]) buf[98])[0] = rslt.wasNull(59);
                ((bool[]) buf[99])[0] = rslt.getBool(60) ;
                ((String[]) buf[100])[0] = rslt.getString(61, 20) ;
                ((bool[]) buf[101])[0] = rslt.wasNull(61);
                ((short[]) buf[102])[0] = rslt.getShort(62) ;
                ((bool[]) buf[103])[0] = rslt.wasNull(62);
                ((short[]) buf[104])[0] = rslt.getShort(63) ;
                ((bool[]) buf[105])[0] = rslt.wasNull(63);
                ((int[]) buf[106])[0] = rslt.getInt(64) ;
                ((short[]) buf[107])[0] = rslt.getShort(65) ;
                ((bool[]) buf[108])[0] = rslt.wasNull(65);
                ((short[]) buf[109])[0] = rslt.getShort(66) ;
                ((bool[]) buf[110])[0] = rslt.wasNull(66);
                ((String[]) buf[111])[0] = rslt.getString(67, 15) ;
                ((String[]) buf[112])[0] = rslt.getString(68, 50) ;
                ((int[]) buf[113])[0] = rslt.getInt(69) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((String[]) buf[10])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(10);
                ((String[]) buf[12])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(11);
                ((String[]) buf[14])[0] = rslt.getString(12, 1) ;
                ((String[]) buf[15])[0] = rslt.getString(13, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(13);
                ((String[]) buf[17])[0] = rslt.getString(14, 3) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(14);
                ((bool[]) buf[19])[0] = rslt.getBool(15) ;
                ((String[]) buf[20])[0] = rslt.getLongVarchar(16) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(16);
                ((int[]) buf[22])[0] = rslt.getInt(17) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(17);
                ((int[]) buf[24])[0] = rslt.getInt(18) ;
                ((String[]) buf[25])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[26])[0] = rslt.getBool(20) ;
                ((int[]) buf[27])[0] = rslt.getInt(21) ;
                ((String[]) buf[28])[0] = rslt.getString(22, 50) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(22);
                ((String[]) buf[30])[0] = rslt.getString(23, 15) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(23);
                ((long[]) buf[32])[0] = rslt.getLong(24) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(24);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(25) ;
                ((decimal[]) buf[35])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(26);
                ((String[]) buf[37])[0] = rslt.getVarchar(27) ;
                ((String[]) buf[38])[0] = rslt.getString(28, 1) ;
                ((String[]) buf[39])[0] = rslt.getString(29, 1) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(29);
                ((bool[]) buf[41])[0] = rslt.getBool(30) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(30);
                ((String[]) buf[43])[0] = rslt.getString(31, 1) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(31);
                ((short[]) buf[45])[0] = rslt.getShort(32) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(32);
                ((short[]) buf[47])[0] = rslt.getShort(33) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(33);
                ((short[]) buf[49])[0] = rslt.getShort(34) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(34);
                ((short[]) buf[51])[0] = rslt.getShort(35) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(35);
                ((short[]) buf[53])[0] = rslt.getShort(36) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(36);
                ((String[]) buf[55])[0] = rslt.getString(37, 1) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(37);
                ((short[]) buf[57])[0] = rslt.getShort(38) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(38);
                ((bool[]) buf[59])[0] = rslt.getBool(39) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(39);
                ((decimal[]) buf[61])[0] = rslt.getDecimal(40) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(40);
                ((bool[]) buf[63])[0] = rslt.getBool(41) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(41);
                ((String[]) buf[65])[0] = rslt.getString(42, 1) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(42);
                ((String[]) buf[67])[0] = rslt.getString(43, 1) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(43);
                ((decimal[]) buf[69])[0] = rslt.getDecimal(44) ;
                ((bool[]) buf[70])[0] = rslt.wasNull(44);
                ((decimal[]) buf[71])[0] = rslt.getDecimal(45) ;
                ((bool[]) buf[72])[0] = rslt.wasNull(45);
                ((bool[]) buf[73])[0] = rslt.getBool(46) ;
                ((bool[]) buf[74])[0] = rslt.wasNull(46);
                ((decimal[]) buf[75])[0] = rslt.getDecimal(47) ;
                ((decimal[]) buf[76])[0] = rslt.getDecimal(48) ;
                ((bool[]) buf[77])[0] = rslt.wasNull(48);
                ((int[]) buf[78])[0] = rslt.getInt(49) ;
                ((bool[]) buf[79])[0] = rslt.wasNull(49);
                ((int[]) buf[80])[0] = rslt.getInt(50) ;
                ((bool[]) buf[81])[0] = rslt.wasNull(50);
                ((int[]) buf[82])[0] = rslt.getInt(51) ;
                ((bool[]) buf[83])[0] = rslt.wasNull(51);
                ((short[]) buf[84])[0] = rslt.getShort(52) ;
                ((bool[]) buf[85])[0] = rslt.wasNull(52);
                ((short[]) buf[86])[0] = rslt.getShort(53) ;
                ((bool[]) buf[87])[0] = rslt.wasNull(53);
                ((short[]) buf[88])[0] = rslt.getShort(54) ;
                ((bool[]) buf[89])[0] = rslt.wasNull(54);
                ((short[]) buf[90])[0] = rslt.getShort(55) ;
                ((bool[]) buf[91])[0] = rslt.wasNull(55);
                ((bool[]) buf[92])[0] = rslt.getBool(56) ;
                ((String[]) buf[93])[0] = rslt.getString(57, 5) ;
                ((bool[]) buf[94])[0] = rslt.wasNull(57);
                ((decimal[]) buf[95])[0] = rslt.getDecimal(58) ;
                ((bool[]) buf[96])[0] = rslt.wasNull(58);
                ((short[]) buf[97])[0] = rslt.getShort(59) ;
                ((bool[]) buf[98])[0] = rslt.wasNull(59);
                ((bool[]) buf[99])[0] = rslt.getBool(60) ;
                ((String[]) buf[100])[0] = rslt.getString(61, 20) ;
                ((bool[]) buf[101])[0] = rslt.wasNull(61);
                ((short[]) buf[102])[0] = rslt.getShort(62) ;
                ((bool[]) buf[103])[0] = rslt.wasNull(62);
                ((short[]) buf[104])[0] = rslt.getShort(63) ;
                ((bool[]) buf[105])[0] = rslt.wasNull(63);
                ((int[]) buf[106])[0] = rslt.getInt(64) ;
                ((short[]) buf[107])[0] = rslt.getShort(65) ;
                ((bool[]) buf[108])[0] = rslt.wasNull(65);
                ((short[]) buf[109])[0] = rslt.getShort(66) ;
                ((bool[]) buf[110])[0] = rslt.wasNull(66);
                ((int[]) buf[111])[0] = rslt.getInt(67) ;
                ((bool[]) buf[112])[0] = rslt.wasNull(67);
                ((String[]) buf[113])[0] = rslt.getString(68, 15) ;
                ((String[]) buf[114])[0] = rslt.getString(69, 50) ;
                ((int[]) buf[115])[0] = rslt.getInt(70) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
