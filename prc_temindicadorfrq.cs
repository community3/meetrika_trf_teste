/*
               File: PRC_TemIndicadorFrq
        Description: Tem Indicador de Frequencia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:13.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_temindicadorfrq : GXProcedure
   {
      public prc_temindicadorfrq( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_temindicadorfrq( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicosIndicador_CntSrvCod ,
                           out bool aP1_TemIndicadorDeFrequencia )
      {
         this.A1270ContratoServicosIndicador_CntSrvCod = aP0_ContratoServicosIndicador_CntSrvCod;
         this.AV8TemIndicadorDeFrequencia = false ;
         initialize();
         executePrivate();
         aP0_ContratoServicosIndicador_CntSrvCod=this.A1270ContratoServicosIndicador_CntSrvCod;
         aP1_TemIndicadorDeFrequencia=this.AV8TemIndicadorDeFrequencia;
      }

      public bool executeUdp( ref int aP0_ContratoServicosIndicador_CntSrvCod )
      {
         this.A1270ContratoServicosIndicador_CntSrvCod = aP0_ContratoServicosIndicador_CntSrvCod;
         this.AV8TemIndicadorDeFrequencia = false ;
         initialize();
         executePrivate();
         aP0_ContratoServicosIndicador_CntSrvCod=this.A1270ContratoServicosIndicador_CntSrvCod;
         aP1_TemIndicadorDeFrequencia=this.AV8TemIndicadorDeFrequencia;
         return AV8TemIndicadorDeFrequencia ;
      }

      public void executeSubmit( ref int aP0_ContratoServicosIndicador_CntSrvCod ,
                                 out bool aP1_TemIndicadorDeFrequencia )
      {
         prc_temindicadorfrq objprc_temindicadorfrq;
         objprc_temindicadorfrq = new prc_temindicadorfrq();
         objprc_temindicadorfrq.A1270ContratoServicosIndicador_CntSrvCod = aP0_ContratoServicosIndicador_CntSrvCod;
         objprc_temindicadorfrq.AV8TemIndicadorDeFrequencia = false ;
         objprc_temindicadorfrq.context.SetSubmitInitialConfig(context);
         objprc_temindicadorfrq.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_temindicadorfrq);
         aP0_ContratoServicosIndicador_CntSrvCod=this.A1270ContratoServicosIndicador_CntSrvCod;
         aP1_TemIndicadorDeFrequencia=this.AV8TemIndicadorDeFrequencia;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_temindicadorfrq)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00WQ2 */
         pr_default.execute(0, new Object[] {A1270ContratoServicosIndicador_CntSrvCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1308ContratoServicosIndicador_Tipo = P00WQ2_A1308ContratoServicosIndicador_Tipo[0];
            n1308ContratoServicosIndicador_Tipo = P00WQ2_n1308ContratoServicosIndicador_Tipo[0];
            A1269ContratoServicosIndicador_Codigo = P00WQ2_A1269ContratoServicosIndicador_Codigo[0];
            AV8TemIndicadorDeFrequencia = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00WQ2_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P00WQ2_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         P00WQ2_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         P00WQ2_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         A1308ContratoServicosIndicador_Tipo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_temindicadorfrq__default(),
            new Object[][] {
                new Object[] {
               P00WQ2_A1270ContratoServicosIndicador_CntSrvCod, P00WQ2_A1308ContratoServicosIndicador_Tipo, P00WQ2_n1308ContratoServicosIndicador_Tipo, P00WQ2_A1269ContratoServicosIndicador_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private String scmdbuf ;
      private String A1308ContratoServicosIndicador_Tipo ;
      private bool AV8TemIndicadorDeFrequencia ;
      private bool n1308ContratoServicosIndicador_Tipo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicosIndicador_CntSrvCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00WQ2_A1270ContratoServicosIndicador_CntSrvCod ;
      private String[] P00WQ2_A1308ContratoServicosIndicador_Tipo ;
      private bool[] P00WQ2_n1308ContratoServicosIndicador_Tipo ;
      private int[] P00WQ2_A1269ContratoServicosIndicador_Codigo ;
      private bool aP1_TemIndicadorDeFrequencia ;
   }

   public class prc_temindicadorfrq__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WQ2 ;
          prmP00WQ2 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WQ2", "SELECT TOP 1 [ContratoServicosIndicador_CntSrvCod], [ContratoServicosIndicador_Tipo], [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE ([ContratoServicosIndicador_CntSrvCod] = @ContratoServicosIndicador_CntSrvCod) AND ([ContratoServicosIndicador_Tipo] = 'FP') ORDER BY [ContratoServicosIndicador_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WQ2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
