/*
               File: PromptAgendaAtendimento
        Description: Selecione Agenda Atendimento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:39:51.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptagendaatendimento : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptagendaatendimento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptagendaatendimento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutAgendaAtendimento_CntSrcCod ,
                           ref DateTime aP1_InOutAgendaAtendimento_Data ,
                           ref int aP2_InOutAgendaAtendimento_CodDmn )
      {
         this.AV7InOutAgendaAtendimento_CntSrcCod = aP0_InOutAgendaAtendimento_CntSrcCod;
         this.AV8InOutAgendaAtendimento_Data = aP1_InOutAgendaAtendimento_Data;
         this.AV30InOutAgendaAtendimento_CodDmn = aP2_InOutAgendaAtendimento_CodDmn;
         executePrivate();
         aP0_InOutAgendaAtendimento_CntSrcCod=this.AV7InOutAgendaAtendimento_CntSrcCod;
         aP1_InOutAgendaAtendimento_Data=this.AV8InOutAgendaAtendimento_Data;
         aP2_InOutAgendaAtendimento_CodDmn=this.AV30InOutAgendaAtendimento_CodDmn;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_86 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_86_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_86_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16AgendaAtendimento_Data1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AgendaAtendimento_Data1", context.localUtil.Format(AV16AgendaAtendimento_Data1, "99/99/99"));
               AV17AgendaAtendimento_Data_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AgendaAtendimento_Data_To1", context.localUtil.Format(AV17AgendaAtendimento_Data_To1, "99/99/99"));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20AgendaAtendimento_Data2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AgendaAtendimento_Data2", context.localUtil.Format(AV20AgendaAtendimento_Data2, "99/99/99"));
               AV21AgendaAtendimento_Data_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AgendaAtendimento_Data_To2", context.localUtil.Format(AV21AgendaAtendimento_Data_To2, "99/99/99"));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24AgendaAtendimento_Data3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24AgendaAtendimento_Data3", context.localUtil.Format(AV24AgendaAtendimento_Data3, "99/99/99"));
               AV25AgendaAtendimento_Data_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AgendaAtendimento_Data_To3", context.localUtil.Format(AV25AgendaAtendimento_Data_To3, "99/99/99"));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV32TFAgendaAtendimento_CntSrcCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFAgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFAgendaAtendimento_CntSrcCod), 6, 0)));
               AV33TFAgendaAtendimento_CntSrcCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFAgendaAtendimento_CntSrcCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFAgendaAtendimento_CntSrcCod_To), 6, 0)));
               AV36TFAgendaAtendimento_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFAgendaAtendimento_Data", context.localUtil.Format(AV36TFAgendaAtendimento_Data, "99/99/99"));
               AV37TFAgendaAtendimento_Data_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFAgendaAtendimento_Data_To", context.localUtil.Format(AV37TFAgendaAtendimento_Data_To, "99/99/99"));
               AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace", AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace);
               AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace", AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV19DynamicFiltersSelector2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV23DynamicFiltersSelector3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFAgendaAtendimento_CntSrcCod, AV33TFAgendaAtendimento_CntSrcCod_To, AV36TFAgendaAtendimento_Data, AV37TFAgendaAtendimento_Data_To, AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace, AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "PromptAgendaAtendimento";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1209AgendaAtendimento_CodDmn), "ZZZZZ9");
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("promptagendaatendimento:[SendSecurityCheck value for]"+"AgendaAtendimento_CodDmn:"+context.localUtil.Format( (decimal)(A1209AgendaAtendimento_CodDmn), "ZZZZZ9"));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutAgendaAtendimento_CntSrcCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutAgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutAgendaAtendimento_CntSrcCod), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutAgendaAtendimento_Data = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutAgendaAtendimento_Data", context.localUtil.Format(AV8InOutAgendaAtendimento_Data, "99/99/99"));
                  AV30InOutAgendaAtendimento_CodDmn = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30InOutAgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30InOutAgendaAtendimento_CodDmn), 6, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAIE2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSIE2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEIE2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299395157");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptagendaatendimento.aspx") + "?" + UrlEncode("" +AV7InOutAgendaAtendimento_CntSrcCod) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV8InOutAgendaAtendimento_Data)) + "," + UrlEncode("" +AV30InOutAgendaAtendimento_CodDmn)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vAGENDAATENDIMENTO_DATA1", context.localUtil.Format(AV16AgendaAtendimento_Data1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vAGENDAATENDIMENTO_DATA_TO1", context.localUtil.Format(AV17AgendaAtendimento_Data_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vAGENDAATENDIMENTO_DATA2", context.localUtil.Format(AV20AgendaAtendimento_Data2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vAGENDAATENDIMENTO_DATA_TO2", context.localUtil.Format(AV21AgendaAtendimento_Data_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vAGENDAATENDIMENTO_DATA3", context.localUtil.Format(AV24AgendaAtendimento_Data3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vAGENDAATENDIMENTO_DATA_TO3", context.localUtil.Format(AV25AgendaAtendimento_Data_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAGENDAATENDIMENTO_CNTSRCCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFAgendaAtendimento_CntSrcCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAGENDAATENDIMENTO_CNTSRCCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33TFAgendaAtendimento_CntSrcCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAGENDAATENDIMENTO_DATA", context.localUtil.Format(AV36TFAgendaAtendimento_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAGENDAATENDIMENTO_DATA_TO", context.localUtil.Format(AV37TFAgendaAtendimento_Data_To, "99/99/99"));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_86", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_86), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV41DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV41DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAGENDAATENDIMENTO_CNTSRCCODTITLEFILTERDATA", AV31AgendaAtendimento_CntSrcCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAGENDAATENDIMENTO_CNTSRCCODTITLEFILTERDATA", AV31AgendaAtendimento_CntSrcCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAGENDAATENDIMENTO_DATATITLEFILTERDATA", AV35AgendaAtendimento_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAGENDAATENDIMENTO_DATATITLEFILTERDATA", AV35AgendaAtendimento_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTAGENDAATENDIMENTO_CNTSRCCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutAgendaAtendimento_CntSrcCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTAGENDAATENDIMENTO_DATA", context.localUtil.DToC( AV8InOutAgendaAtendimento_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vINOUTAGENDAATENDIMENTO_CODDMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30InOutAgendaAtendimento_CodDmn), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Caption", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Tooltip", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Cls", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Filteredtext_set", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Includesortasc", StringUtil.BoolToStr( Ddo_agendaatendimento_cntsrccod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_agendaatendimento_cntsrccod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Sortedstatus", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Includefilter", StringUtil.BoolToStr( Ddo_agendaatendimento_cntsrccod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Filtertype", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Filterisrange", StringUtil.BoolToStr( Ddo_agendaatendimento_cntsrccod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Includedatalist", StringUtil.BoolToStr( Ddo_agendaatendimento_cntsrccod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Sortasc", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Sortdsc", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Cleanfilter", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Rangefilterto", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Searchbuttontext", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Caption", StringUtil.RTrim( Ddo_agendaatendimento_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Tooltip", StringUtil.RTrim( Ddo_agendaatendimento_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Cls", StringUtil.RTrim( Ddo_agendaatendimento_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_agendaatendimento_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_agendaatendimento_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_agendaatendimento_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_agendaatendimento_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_agendaatendimento_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_agendaatendimento_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Sortedstatus", StringUtil.RTrim( Ddo_agendaatendimento_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Includefilter", StringUtil.BoolToStr( Ddo_agendaatendimento_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Filtertype", StringUtil.RTrim( Ddo_agendaatendimento_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_agendaatendimento_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_agendaatendimento_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Sortasc", StringUtil.RTrim( Ddo_agendaatendimento_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Sortdsc", StringUtil.RTrim( Ddo_agendaatendimento_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Cleanfilter", StringUtil.RTrim( Ddo_agendaatendimento_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_agendaatendimento_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Rangefilterto", StringUtil.RTrim( Ddo_agendaatendimento_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_agendaatendimento_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Activeeventkey", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Filteredtext_get", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_agendaatendimento_cntsrccod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Activeeventkey", StringUtil.RTrim( Ddo_agendaatendimento_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_agendaatendimento_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AGENDAATENDIMENTO_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_agendaatendimento_data_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "PromptAgendaAtendimento";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1209AgendaAtendimento_CodDmn), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("promptagendaatendimento:[SendSecurityCheck value for]"+"AgendaAtendimento_CodDmn:"+context.localUtil.Format( (decimal)(A1209AgendaAtendimento_CodDmn), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormIE2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptAgendaAtendimento" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Agenda Atendimento" ;
      }

      protected void WBIE0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_IE2( true) ;
         }
         else
         {
            wb_table1_2_IE2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_IE2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAgendaAtendimento_CodDmn_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1209AgendaAtendimento_CodDmn), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAgendaAtendimento_CodDmn_Jsonclick, 0, "Attribute", "", "", "", edtAgendaAtendimento_CodDmn_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_PromptAgendaAtendimento.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(95, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(96, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfagendaatendimento_cntsrccod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFAgendaAtendimento_CntSrcCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32TFAgendaAtendimento_CntSrcCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfagendaatendimento_cntsrccod_Jsonclick, 0, "Attribute", "", "", "", edtavTfagendaatendimento_cntsrccod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfagendaatendimento_cntsrccod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33TFAgendaAtendimento_CntSrcCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV33TFAgendaAtendimento_CntSrcCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfagendaatendimento_cntsrccod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfagendaatendimento_cntsrccod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptAgendaAtendimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfagendaatendimento_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfagendaatendimento_data_Internalname, context.localUtil.Format(AV36TFAgendaAtendimento_Data, "99/99/99"), context.localUtil.Format( AV36TFAgendaAtendimento_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfagendaatendimento_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfagendaatendimento_data_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavTfagendaatendimento_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfagendaatendimento_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfagendaatendimento_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfagendaatendimento_data_to_Internalname, context.localUtil.Format(AV37TFAgendaAtendimento_Data_To, "99/99/99"), context.localUtil.Format( AV37TFAgendaAtendimento_Data_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfagendaatendimento_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfagendaatendimento_data_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavTfagendaatendimento_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfagendaatendimento_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_agendaatendimento_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_agendaatendimento_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_agendaatendimento_dataauxdate_Internalname, context.localUtil.Format(AV38DDO_AgendaAtendimento_DataAuxDate, "99/99/99"), context.localUtil.Format( AV38DDO_AgendaAtendimento_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_agendaatendimento_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_agendaatendimento_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_agendaatendimento_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_agendaatendimento_dataauxdateto_Internalname, context.localUtil.Format(AV39DDO_AgendaAtendimento_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV39DDO_AgendaAtendimento_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_agendaatendimento_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_agendaatendimento_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AGENDAATENDIMENTO_CNTSRCCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_agendaatendimento_cntsrccodtitlecontrolidtoreplace_Internalname, AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", 0, edtavDdo_agendaatendimento_cntsrccodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptAgendaAtendimento.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AGENDAATENDIMENTO_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Internalname, AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", 0, edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptAgendaAtendimento.htm");
         }
         wbLoad = true;
      }

      protected void STARTIE2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Agenda Atendimento", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPIE0( ) ;
      }

      protected void WSIE2( )
      {
         STARTIE2( ) ;
         EVTIE2( ) ;
      }

      protected void EVTIE2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11IE2 */
                           E11IE2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_AGENDAATENDIMENTO_CNTSRCCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12IE2 */
                           E12IE2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_AGENDAATENDIMENTO_DATA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13IE2 */
                           E13IE2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14IE2 */
                           E14IE2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15IE2 */
                           E15IE2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16IE2 */
                           E16IE2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17IE2 */
                           E17IE2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18IE2 */
                           E18IE2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19IE2 */
                           E19IE2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20IE2 */
                           E20IE2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_86_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
                           SubsflControlProps_862( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV47Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A1183AgendaAtendimento_CntSrcCod = (int)(context.localUtil.CToN( cgiGet( edtAgendaAtendimento_CntSrcCod_Internalname), ",", "."));
                           A1184AgendaAtendimento_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtAgendaAtendimento_Data_Internalname), 0));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E21IE2 */
                                 E21IE2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E22IE2 */
                                 E22IE2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E23IE2 */
                                 E23IE2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Agendaatendimento_data1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA1"), 0) != AV16AgendaAtendimento_Data1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Agendaatendimento_data_to1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA_TO1"), 0) != AV17AgendaAtendimento_Data_To1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Agendaatendimento_data2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA2"), 0) != AV20AgendaAtendimento_Data2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Agendaatendimento_data_to2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA_TO2"), 0) != AV21AgendaAtendimento_Data_To2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Agendaatendimento_data3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA3"), 0) != AV24AgendaAtendimento_Data3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Agendaatendimento_data_to3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA_TO3"), 0) != AV25AgendaAtendimento_Data_To3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfagendaatendimento_cntsrccod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAGENDAATENDIMENTO_CNTSRCCOD"), ",", ".") != Convert.ToDecimal( AV32TFAgendaAtendimento_CntSrcCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfagendaatendimento_cntsrccod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAGENDAATENDIMENTO_CNTSRCCOD_TO"), ",", ".") != Convert.ToDecimal( AV33TFAgendaAtendimento_CntSrcCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfagendaatendimento_data Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFAGENDAATENDIMENTO_DATA"), 0) != AV36TFAgendaAtendimento_Data )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfagendaatendimento_data_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFAGENDAATENDIMENTO_DATA_TO"), 0) != AV37TFAgendaAtendimento_Data_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E24IE2 */
                                       E24IE2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEIE2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormIE2( ) ;
            }
         }
      }

      protected void PAIE2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("AGENDAATENDIMENTO_DATA", "Data", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("AGENDAATENDIMENTO_DATA", "Data", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("AGENDAATENDIMENTO_DATA", "Data", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_862( ) ;
         while ( nGXsfl_86_idx <= nRC_GXsfl_86 )
         {
            sendrow_862( ) ;
            nGXsfl_86_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_86_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_86_idx+1));
            sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
            SubsflControlProps_862( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       DateTime AV16AgendaAtendimento_Data1 ,
                                       DateTime AV17AgendaAtendimento_Data_To1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       DateTime AV20AgendaAtendimento_Data2 ,
                                       DateTime AV21AgendaAtendimento_Data_To2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       DateTime AV24AgendaAtendimento_Data3 ,
                                       DateTime AV25AgendaAtendimento_Data_To3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV32TFAgendaAtendimento_CntSrcCod ,
                                       int AV33TFAgendaAtendimento_CntSrcCod_To ,
                                       DateTime AV36TFAgendaAtendimento_Data ,
                                       DateTime AV37TFAgendaAtendimento_Data_To ,
                                       String AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace ,
                                       String AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFIE2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_AGENDAATENDIMENTO_CNTSRCCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1183AgendaAtendimento_CntSrcCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AGENDAATENDIMENTO_CNTSRCCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AGENDAATENDIMENTO_DATA", GetSecureSignedToken( "", A1184AgendaAtendimento_Data));
         GxWebStd.gx_hidden_field( context, "AGENDAATENDIMENTO_DATA", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFIE2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFIE2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 86;
         /* Execute user event: E22IE2 */
         E22IE2 ();
         nGXsfl_86_idx = 1;
         sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
         SubsflControlProps_862( ) ;
         nGXsfl_86_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_862( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16AgendaAtendimento_Data1 ,
                                                 AV17AgendaAtendimento_Data_To1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20AgendaAtendimento_Data2 ,
                                                 AV21AgendaAtendimento_Data_To2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24AgendaAtendimento_Data3 ,
                                                 AV25AgendaAtendimento_Data_To3 ,
                                                 AV32TFAgendaAtendimento_CntSrcCod ,
                                                 AV33TFAgendaAtendimento_CntSrcCod_To ,
                                                 AV36TFAgendaAtendimento_Data ,
                                                 AV37TFAgendaAtendimento_Data_To ,
                                                 A1184AgendaAtendimento_Data ,
                                                 A1183AgendaAtendimento_CntSrcCod ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00IE2 */
            pr_default.execute(0, new Object[] {AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV32TFAgendaAtendimento_CntSrcCod, AV33TFAgendaAtendimento_CntSrcCod_To, AV36TFAgendaAtendimento_Data, AV37TFAgendaAtendimento_Data_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_86_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1209AgendaAtendimento_CodDmn = H00IE2_A1209AgendaAtendimento_CodDmn[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
               A1184AgendaAtendimento_Data = H00IE2_A1184AgendaAtendimento_Data[0];
               A1183AgendaAtendimento_CntSrcCod = H00IE2_A1183AgendaAtendimento_CntSrcCod[0];
               /* Execute user event: E23IE2 */
               E23IE2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 86;
            WBIE0( ) ;
         }
         nGXsfl_86_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16AgendaAtendimento_Data1 ,
                                              AV17AgendaAtendimento_Data_To1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20AgendaAtendimento_Data2 ,
                                              AV21AgendaAtendimento_Data_To2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24AgendaAtendimento_Data3 ,
                                              AV25AgendaAtendimento_Data_To3 ,
                                              AV32TFAgendaAtendimento_CntSrcCod ,
                                              AV33TFAgendaAtendimento_CntSrcCod_To ,
                                              AV36TFAgendaAtendimento_Data ,
                                              AV37TFAgendaAtendimento_Data_To ,
                                              A1184AgendaAtendimento_Data ,
                                              A1183AgendaAtendimento_CntSrcCod ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00IE3 */
         pr_default.execute(1, new Object[] {AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV32TFAgendaAtendimento_CntSrcCod, AV33TFAgendaAtendimento_CntSrcCod_To, AV36TFAgendaAtendimento_Data, AV37TFAgendaAtendimento_Data_To});
         GRID_nRecordCount = H00IE3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV19DynamicFiltersSelector2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV23DynamicFiltersSelector3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFAgendaAtendimento_CntSrcCod, AV33TFAgendaAtendimento_CntSrcCod_To, AV36TFAgendaAtendimento_Data, AV37TFAgendaAtendimento_Data_To, AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace, AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV19DynamicFiltersSelector2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV23DynamicFiltersSelector3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFAgendaAtendimento_CntSrcCod, AV33TFAgendaAtendimento_CntSrcCod_To, AV36TFAgendaAtendimento_Data, AV37TFAgendaAtendimento_Data_To, AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace, AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV19DynamicFiltersSelector2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV23DynamicFiltersSelector3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFAgendaAtendimento_CntSrcCod, AV33TFAgendaAtendimento_CntSrcCod_To, AV36TFAgendaAtendimento_Data, AV37TFAgendaAtendimento_Data_To, AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace, AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV19DynamicFiltersSelector2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV23DynamicFiltersSelector3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFAgendaAtendimento_CntSrcCod, AV33TFAgendaAtendimento_CntSrcCod_To, AV36TFAgendaAtendimento_Data, AV37TFAgendaAtendimento_Data_To, AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace, AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV19DynamicFiltersSelector2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV23DynamicFiltersSelector3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFAgendaAtendimento_CntSrcCod, AV33TFAgendaAtendimento_CntSrcCod_To, AV36TFAgendaAtendimento_Data, AV37TFAgendaAtendimento_Data_To, AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace, AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace) ;
         }
         return (int)(0) ;
      }

      protected void STRUPIE0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E21IE2 */
         E21IE2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV41DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vAGENDAATENDIMENTO_CNTSRCCODTITLEFILTERDATA"), AV31AgendaAtendimento_CntSrcCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAGENDAATENDIMENTO_DATATITLEFILTERDATA"), AV35AgendaAtendimento_DataTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavAgendaatendimento_data1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Agenda Atendimento_Data1"}), 1, "vAGENDAATENDIMENTO_DATA1");
               GX_FocusControl = edtavAgendaatendimento_data1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16AgendaAtendimento_Data1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AgendaAtendimento_Data1", context.localUtil.Format(AV16AgendaAtendimento_Data1, "99/99/99"));
            }
            else
            {
               AV16AgendaAtendimento_Data1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAgendaatendimento_data1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AgendaAtendimento_Data1", context.localUtil.Format(AV16AgendaAtendimento_Data1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavAgendaatendimento_data_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Agenda Atendimento_Data_To1"}), 1, "vAGENDAATENDIMENTO_DATA_TO1");
               GX_FocusControl = edtavAgendaatendimento_data_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17AgendaAtendimento_Data_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AgendaAtendimento_Data_To1", context.localUtil.Format(AV17AgendaAtendimento_Data_To1, "99/99/99"));
            }
            else
            {
               AV17AgendaAtendimento_Data_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAgendaatendimento_data_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AgendaAtendimento_Data_To1", context.localUtil.Format(AV17AgendaAtendimento_Data_To1, "99/99/99"));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavAgendaatendimento_data2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Agenda Atendimento_Data2"}), 1, "vAGENDAATENDIMENTO_DATA2");
               GX_FocusControl = edtavAgendaatendimento_data2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20AgendaAtendimento_Data2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AgendaAtendimento_Data2", context.localUtil.Format(AV20AgendaAtendimento_Data2, "99/99/99"));
            }
            else
            {
               AV20AgendaAtendimento_Data2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAgendaatendimento_data2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AgendaAtendimento_Data2", context.localUtil.Format(AV20AgendaAtendimento_Data2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavAgendaatendimento_data_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Agenda Atendimento_Data_To2"}), 1, "vAGENDAATENDIMENTO_DATA_TO2");
               GX_FocusControl = edtavAgendaatendimento_data_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21AgendaAtendimento_Data_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AgendaAtendimento_Data_To2", context.localUtil.Format(AV21AgendaAtendimento_Data_To2, "99/99/99"));
            }
            else
            {
               AV21AgendaAtendimento_Data_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAgendaatendimento_data_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AgendaAtendimento_Data_To2", context.localUtil.Format(AV21AgendaAtendimento_Data_To2, "99/99/99"));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavAgendaatendimento_data3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Agenda Atendimento_Data3"}), 1, "vAGENDAATENDIMENTO_DATA3");
               GX_FocusControl = edtavAgendaatendimento_data3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24AgendaAtendimento_Data3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24AgendaAtendimento_Data3", context.localUtil.Format(AV24AgendaAtendimento_Data3, "99/99/99"));
            }
            else
            {
               AV24AgendaAtendimento_Data3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAgendaatendimento_data3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24AgendaAtendimento_Data3", context.localUtil.Format(AV24AgendaAtendimento_Data3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavAgendaatendimento_data_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Agenda Atendimento_Data_To3"}), 1, "vAGENDAATENDIMENTO_DATA_TO3");
               GX_FocusControl = edtavAgendaatendimento_data_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25AgendaAtendimento_Data_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AgendaAtendimento_Data_To3", context.localUtil.Format(AV25AgendaAtendimento_Data_To3, "99/99/99"));
            }
            else
            {
               AV25AgendaAtendimento_Data_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAgendaatendimento_data_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AgendaAtendimento_Data_To3", context.localUtil.Format(AV25AgendaAtendimento_Data_To3, "99/99/99"));
            }
            A1209AgendaAtendimento_CodDmn = (int)(context.localUtil.CToN( cgiGet( edtAgendaAtendimento_CodDmn_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_cntsrccod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_cntsrccod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAGENDAATENDIMENTO_CNTSRCCOD");
               GX_FocusControl = edtavTfagendaatendimento_cntsrccod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32TFAgendaAtendimento_CntSrcCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFAgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFAgendaAtendimento_CntSrcCod), 6, 0)));
            }
            else
            {
               AV32TFAgendaAtendimento_CntSrcCod = (int)(context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_cntsrccod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFAgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFAgendaAtendimento_CntSrcCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_cntsrccod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_cntsrccod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAGENDAATENDIMENTO_CNTSRCCOD_TO");
               GX_FocusControl = edtavTfagendaatendimento_cntsrccod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33TFAgendaAtendimento_CntSrcCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFAgendaAtendimento_CntSrcCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFAgendaAtendimento_CntSrcCod_To), 6, 0)));
            }
            else
            {
               AV33TFAgendaAtendimento_CntSrcCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_cntsrccod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFAgendaAtendimento_CntSrcCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFAgendaAtendimento_CntSrcCod_To), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfagendaatendimento_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFAgenda Atendimento_Data"}), 1, "vTFAGENDAATENDIMENTO_DATA");
               GX_FocusControl = edtavTfagendaatendimento_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFAgendaAtendimento_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFAgendaAtendimento_Data", context.localUtil.Format(AV36TFAgendaAtendimento_Data, "99/99/99"));
            }
            else
            {
               AV36TFAgendaAtendimento_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfagendaatendimento_data_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFAgendaAtendimento_Data", context.localUtil.Format(AV36TFAgendaAtendimento_Data, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfagendaatendimento_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFAgenda Atendimento_Data_To"}), 1, "vTFAGENDAATENDIMENTO_DATA_TO");
               GX_FocusControl = edtavTfagendaatendimento_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37TFAgendaAtendimento_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFAgendaAtendimento_Data_To", context.localUtil.Format(AV37TFAgendaAtendimento_Data_To, "99/99/99"));
            }
            else
            {
               AV37TFAgendaAtendimento_Data_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfagendaatendimento_data_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFAgendaAtendimento_Data_To", context.localUtil.Format(AV37TFAgendaAtendimento_Data_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_agendaatendimento_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Agenda Atendimento_Data Aux Date"}), 1, "vDDO_AGENDAATENDIMENTO_DATAAUXDATE");
               GX_FocusControl = edtavDdo_agendaatendimento_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38DDO_AgendaAtendimento_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DDO_AgendaAtendimento_DataAuxDate", context.localUtil.Format(AV38DDO_AgendaAtendimento_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV38DDO_AgendaAtendimento_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_agendaatendimento_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DDO_AgendaAtendimento_DataAuxDate", context.localUtil.Format(AV38DDO_AgendaAtendimento_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_agendaatendimento_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Agenda Atendimento_Data Aux Date To"}), 1, "vDDO_AGENDAATENDIMENTO_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_agendaatendimento_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39DDO_AgendaAtendimento_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DDO_AgendaAtendimento_DataAuxDateTo", context.localUtil.Format(AV39DDO_AgendaAtendimento_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV39DDO_AgendaAtendimento_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_agendaatendimento_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DDO_AgendaAtendimento_DataAuxDateTo", context.localUtil.Format(AV39DDO_AgendaAtendimento_DataAuxDateTo, "99/99/99"));
            }
            AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace = cgiGet( edtavDdo_agendaatendimento_cntsrccodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace", AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace);
            AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace = cgiGet( edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace", AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_86 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_86"), ",", "."));
            AV43GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV44GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_agendaatendimento_cntsrccod_Caption = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Caption");
            Ddo_agendaatendimento_cntsrccod_Tooltip = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Tooltip");
            Ddo_agendaatendimento_cntsrccod_Cls = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Cls");
            Ddo_agendaatendimento_cntsrccod_Filteredtext_set = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Filteredtext_set");
            Ddo_agendaatendimento_cntsrccod_Filteredtextto_set = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Filteredtextto_set");
            Ddo_agendaatendimento_cntsrccod_Dropdownoptionstype = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Dropdownoptionstype");
            Ddo_agendaatendimento_cntsrccod_Titlecontrolidtoreplace = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Titlecontrolidtoreplace");
            Ddo_agendaatendimento_cntsrccod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Includesortasc"));
            Ddo_agendaatendimento_cntsrccod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Includesortdsc"));
            Ddo_agendaatendimento_cntsrccod_Sortedstatus = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Sortedstatus");
            Ddo_agendaatendimento_cntsrccod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Includefilter"));
            Ddo_agendaatendimento_cntsrccod_Filtertype = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Filtertype");
            Ddo_agendaatendimento_cntsrccod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Filterisrange"));
            Ddo_agendaatendimento_cntsrccod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Includedatalist"));
            Ddo_agendaatendimento_cntsrccod_Sortasc = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Sortasc");
            Ddo_agendaatendimento_cntsrccod_Sortdsc = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Sortdsc");
            Ddo_agendaatendimento_cntsrccod_Cleanfilter = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Cleanfilter");
            Ddo_agendaatendimento_cntsrccod_Rangefilterfrom = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Rangefilterfrom");
            Ddo_agendaatendimento_cntsrccod_Rangefilterto = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Rangefilterto");
            Ddo_agendaatendimento_cntsrccod_Searchbuttontext = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Searchbuttontext");
            Ddo_agendaatendimento_data_Caption = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Caption");
            Ddo_agendaatendimento_data_Tooltip = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Tooltip");
            Ddo_agendaatendimento_data_Cls = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Cls");
            Ddo_agendaatendimento_data_Filteredtext_set = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Filteredtext_set");
            Ddo_agendaatendimento_data_Filteredtextto_set = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Filteredtextto_set");
            Ddo_agendaatendimento_data_Dropdownoptionstype = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Dropdownoptionstype");
            Ddo_agendaatendimento_data_Titlecontrolidtoreplace = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Titlecontrolidtoreplace");
            Ddo_agendaatendimento_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Includesortasc"));
            Ddo_agendaatendimento_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Includesortdsc"));
            Ddo_agendaatendimento_data_Sortedstatus = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Sortedstatus");
            Ddo_agendaatendimento_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Includefilter"));
            Ddo_agendaatendimento_data_Filtertype = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Filtertype");
            Ddo_agendaatendimento_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Filterisrange"));
            Ddo_agendaatendimento_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Includedatalist"));
            Ddo_agendaatendimento_data_Sortasc = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Sortasc");
            Ddo_agendaatendimento_data_Sortdsc = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Sortdsc");
            Ddo_agendaatendimento_data_Cleanfilter = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Cleanfilter");
            Ddo_agendaatendimento_data_Rangefilterfrom = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Rangefilterfrom");
            Ddo_agendaatendimento_data_Rangefilterto = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Rangefilterto");
            Ddo_agendaatendimento_data_Searchbuttontext = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_agendaatendimento_cntsrccod_Activeeventkey = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Activeeventkey");
            Ddo_agendaatendimento_cntsrccod_Filteredtext_get = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Filteredtext_get");
            Ddo_agendaatendimento_cntsrccod_Filteredtextto_get = cgiGet( "DDO_AGENDAATENDIMENTO_CNTSRCCOD_Filteredtextto_get");
            Ddo_agendaatendimento_data_Activeeventkey = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Activeeventkey");
            Ddo_agendaatendimento_data_Filteredtext_get = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Filteredtext_get");
            Ddo_agendaatendimento_data_Filteredtextto_get = cgiGet( "DDO_AGENDAATENDIMENTO_DATA_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "PromptAgendaAtendimento";
            A1209AgendaAtendimento_CodDmn = (int)(context.localUtil.CToN( cgiGet( edtAgendaAtendimento_CodDmn_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1209AgendaAtendimento_CodDmn), "ZZZZZ9");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("promptagendaatendimento:[SecurityCheckFailed value for]"+"AgendaAtendimento_CodDmn:"+context.localUtil.Format( (decimal)(A1209AgendaAtendimento_CodDmn), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA1"), 0) != AV16AgendaAtendimento_Data1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA_TO1"), 0) != AV17AgendaAtendimento_Data_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA2"), 0) != AV20AgendaAtendimento_Data2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA_TO2"), 0) != AV21AgendaAtendimento_Data_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA3"), 0) != AV24AgendaAtendimento_Data3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAGENDAATENDIMENTO_DATA_TO3"), 0) != AV25AgendaAtendimento_Data_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAGENDAATENDIMENTO_CNTSRCCOD"), ",", ".") != Convert.ToDecimal( AV32TFAgendaAtendimento_CntSrcCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAGENDAATENDIMENTO_CNTSRCCOD_TO"), ",", ".") != Convert.ToDecimal( AV33TFAgendaAtendimento_CntSrcCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFAGENDAATENDIMENTO_DATA"), 0) != AV36TFAgendaAtendimento_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFAGENDAATENDIMENTO_DATA_TO"), 0) != AV37TFAgendaAtendimento_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E21IE2 */
         E21IE2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21IE2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "AGENDAATENDIMENTO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "AGENDAATENDIMENTO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "AGENDAATENDIMENTO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfagendaatendimento_cntsrccod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfagendaatendimento_cntsrccod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfagendaatendimento_cntsrccod_Visible), 5, 0)));
         edtavTfagendaatendimento_cntsrccod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfagendaatendimento_cntsrccod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfagendaatendimento_cntsrccod_to_Visible), 5, 0)));
         edtavTfagendaatendimento_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfagendaatendimento_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfagendaatendimento_data_Visible), 5, 0)));
         edtavTfagendaatendimento_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfagendaatendimento_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfagendaatendimento_data_to_Visible), 5, 0)));
         Ddo_agendaatendimento_cntsrccod_Titlecontrolidtoreplace = subGrid_Internalname+"_AgendaAtendimento_CntSrcCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_cntsrccod_Internalname, "TitleControlIdToReplace", Ddo_agendaatendimento_cntsrccod_Titlecontrolidtoreplace);
         AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace = Ddo_agendaatendimento_cntsrccod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace", AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace);
         edtavDdo_agendaatendimento_cntsrccodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_agendaatendimento_cntsrccodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_agendaatendimento_cntsrccodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_agendaatendimento_data_Titlecontrolidtoreplace = subGrid_Internalname+"_AgendaAtendimento_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_data_Internalname, "TitleControlIdToReplace", Ddo_agendaatendimento_data_Titlecontrolidtoreplace);
         AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace = Ddo_agendaatendimento_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace", AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace);
         edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Agenda Atendimento";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         edtAgendaAtendimento_CodDmn_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_CodDmn_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_CodDmn_Visible), 5, 0)));
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Data", 0);
         cmbavOrderedby.addItem("2", "Agenda Atendimento_Cnt Src Cod", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV41DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV41DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E22IE2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV31AgendaAtendimento_CntSrcCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV35AgendaAtendimento_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         edtAgendaAtendimento_CntSrcCod_Titleformat = 2;
         edtAgendaAtendimento_CntSrcCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Agenda Atendimento_Cnt Src Cod", AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_CntSrcCod_Internalname, "Title", edtAgendaAtendimento_CntSrcCod_Title);
         edtAgendaAtendimento_Data_Titleformat = 2;
         edtAgendaAtendimento_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_Data_Internalname, "Title", edtAgendaAtendimento_Data_Title);
         AV43GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43GridCurrentPage), 10, 0)));
         AV44GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV31AgendaAtendimento_CntSrcCodTitleFilterData", AV31AgendaAtendimento_CntSrcCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35AgendaAtendimento_DataTitleFilterData", AV35AgendaAtendimento_DataTitleFilterData);
      }

      protected void E11IE2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV42PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV42PageToGo) ;
         }
      }

      protected void E12IE2( )
      {
         /* Ddo_agendaatendimento_cntsrccod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_agendaatendimento_cntsrccod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_agendaatendimento_cntsrccod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_cntsrccod_Internalname, "SortedStatus", Ddo_agendaatendimento_cntsrccod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_agendaatendimento_cntsrccod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_agendaatendimento_cntsrccod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_cntsrccod_Internalname, "SortedStatus", Ddo_agendaatendimento_cntsrccod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_agendaatendimento_cntsrccod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV32TFAgendaAtendimento_CntSrcCod = (int)(NumberUtil.Val( Ddo_agendaatendimento_cntsrccod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFAgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFAgendaAtendimento_CntSrcCod), 6, 0)));
            AV33TFAgendaAtendimento_CntSrcCod_To = (int)(NumberUtil.Val( Ddo_agendaatendimento_cntsrccod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFAgendaAtendimento_CntSrcCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFAgendaAtendimento_CntSrcCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13IE2( )
      {
         /* Ddo_agendaatendimento_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_agendaatendimento_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_agendaatendimento_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_data_Internalname, "SortedStatus", Ddo_agendaatendimento_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_agendaatendimento_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_agendaatendimento_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_data_Internalname, "SortedStatus", Ddo_agendaatendimento_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_agendaatendimento_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV36TFAgendaAtendimento_Data = context.localUtil.CToD( Ddo_agendaatendimento_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFAgendaAtendimento_Data", context.localUtil.Format(AV36TFAgendaAtendimento_Data, "99/99/99"));
            AV37TFAgendaAtendimento_Data_To = context.localUtil.CToD( Ddo_agendaatendimento_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFAgendaAtendimento_Data_To", context.localUtil.Format(AV37TFAgendaAtendimento_Data_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E23IE2( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV47Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 86;
         }
         sendrow_862( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_86_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(86, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E24IE2 */
         E24IE2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24IE2( )
      {
         /* Enter Routine */
         AV7InOutAgendaAtendimento_CntSrcCod = A1183AgendaAtendimento_CntSrcCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutAgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutAgendaAtendimento_CntSrcCod), 6, 0)));
         AV8InOutAgendaAtendimento_Data = A1184AgendaAtendimento_Data;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutAgendaAtendimento_Data", context.localUtil.Format(AV8InOutAgendaAtendimento_Data, "99/99/99"));
         AV30InOutAgendaAtendimento_CodDmn = A1209AgendaAtendimento_CodDmn;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30InOutAgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30InOutAgendaAtendimento_CodDmn), 6, 0)));
         context.setWebReturnParms(new Object[] {(int)AV7InOutAgendaAtendimento_CntSrcCod,context.localUtil.Format( AV8InOutAgendaAtendimento_Data, "99/99/99"),(int)AV30InOutAgendaAtendimento_CodDmn});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E14IE2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E19IE2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E15IE2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV19DynamicFiltersSelector2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV23DynamicFiltersSelector3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFAgendaAtendimento_CntSrcCod, AV33TFAgendaAtendimento_CntSrcCod_To, AV36TFAgendaAtendimento_Data, AV37TFAgendaAtendimento_Data_To, AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace, AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E20IE2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E16IE2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV19DynamicFiltersSelector2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV23DynamicFiltersSelector3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFAgendaAtendimento_CntSrcCod, AV33TFAgendaAtendimento_CntSrcCod_To, AV36TFAgendaAtendimento_Data, AV37TFAgendaAtendimento_Data_To, AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace, AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E17IE2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AgendaAtendimento_Data1, AV17AgendaAtendimento_Data_To1, AV19DynamicFiltersSelector2, AV20AgendaAtendimento_Data2, AV21AgendaAtendimento_Data_To2, AV23DynamicFiltersSelector3, AV24AgendaAtendimento_Data3, AV25AgendaAtendimento_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFAgendaAtendimento_CntSrcCod, AV33TFAgendaAtendimento_CntSrcCod_To, AV36TFAgendaAtendimento_Data, AV37TFAgendaAtendimento_Data_To, AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace, AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E18IE2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_agendaatendimento_cntsrccod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_cntsrccod_Internalname, "SortedStatus", Ddo_agendaatendimento_cntsrccod_Sortedstatus);
         Ddo_agendaatendimento_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_data_Internalname, "SortedStatus", Ddo_agendaatendimento_data_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_agendaatendimento_cntsrccod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_cntsrccod_Internalname, "SortedStatus", Ddo_agendaatendimento_cntsrccod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_agendaatendimento_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_data_Internalname, "SortedStatus", Ddo_agendaatendimento_data_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfiltersagendaatendimento_data1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersagendaatendimento_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersagendaatendimento_data1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AGENDAATENDIMENTO_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersagendaatendimento_data1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersagendaatendimento_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersagendaatendimento_data1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfiltersagendaatendimento_data2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersagendaatendimento_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersagendaatendimento_data2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AGENDAATENDIMENTO_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersagendaatendimento_data2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersagendaatendimento_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersagendaatendimento_data2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfiltersagendaatendimento_data3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersagendaatendimento_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersagendaatendimento_data3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AGENDAATENDIMENTO_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersagendaatendimento_data3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersagendaatendimento_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersagendaatendimento_data3_Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "AGENDAATENDIMENTO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20AgendaAtendimento_Data2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AgendaAtendimento_Data2", context.localUtil.Format(AV20AgendaAtendimento_Data2, "99/99/99"));
         AV21AgendaAtendimento_Data_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AgendaAtendimento_Data_To2", context.localUtil.Format(AV21AgendaAtendimento_Data_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "AGENDAATENDIMENTO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24AgendaAtendimento_Data3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24AgendaAtendimento_Data3", context.localUtil.Format(AV24AgendaAtendimento_Data3, "99/99/99"));
         AV25AgendaAtendimento_Data_To3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AgendaAtendimento_Data_To3", context.localUtil.Format(AV25AgendaAtendimento_Data_To3, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'CLEANFILTERS' Routine */
         AV32TFAgendaAtendimento_CntSrcCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFAgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFAgendaAtendimento_CntSrcCod), 6, 0)));
         Ddo_agendaatendimento_cntsrccod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_cntsrccod_Internalname, "FilteredText_set", Ddo_agendaatendimento_cntsrccod_Filteredtext_set);
         AV33TFAgendaAtendimento_CntSrcCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFAgendaAtendimento_CntSrcCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFAgendaAtendimento_CntSrcCod_To), 6, 0)));
         Ddo_agendaatendimento_cntsrccod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_cntsrccod_Internalname, "FilteredTextTo_set", Ddo_agendaatendimento_cntsrccod_Filteredtextto_set);
         AV36TFAgendaAtendimento_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFAgendaAtendimento_Data", context.localUtil.Format(AV36TFAgendaAtendimento_Data, "99/99/99"));
         Ddo_agendaatendimento_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_data_Internalname, "FilteredText_set", Ddo_agendaatendimento_data_Filteredtext_set);
         AV37TFAgendaAtendimento_Data_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFAgendaAtendimento_Data_To", context.localUtil.Format(AV37TFAgendaAtendimento_Data_To, "99/99/99"));
         Ddo_agendaatendimento_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_agendaatendimento_data_Internalname, "FilteredTextTo_set", Ddo_agendaatendimento_data_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "AGENDAATENDIMENTO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16AgendaAtendimento_Data1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AgendaAtendimento_Data1", context.localUtil.Format(AV16AgendaAtendimento_Data1, "99/99/99"));
         AV17AgendaAtendimento_Data_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AgendaAtendimento_Data_To1", context.localUtil.Format(AV17AgendaAtendimento_Data_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AGENDAATENDIMENTO_DATA") == 0 )
            {
               AV16AgendaAtendimento_Data1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AgendaAtendimento_Data1", context.localUtil.Format(AV16AgendaAtendimento_Data1, "99/99/99"));
               AV17AgendaAtendimento_Data_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AgendaAtendimento_Data_To1", context.localUtil.Format(AV17AgendaAtendimento_Data_To1, "99/99/99"));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AGENDAATENDIMENTO_DATA") == 0 )
               {
                  AV20AgendaAtendimento_Data2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AgendaAtendimento_Data2", context.localUtil.Format(AV20AgendaAtendimento_Data2, "99/99/99"));
                  AV21AgendaAtendimento_Data_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AgendaAtendimento_Data_To2", context.localUtil.Format(AV21AgendaAtendimento_Data_To2, "99/99/99"));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AGENDAATENDIMENTO_DATA") == 0 )
                  {
                     AV24AgendaAtendimento_Data3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24AgendaAtendimento_Data3", context.localUtil.Format(AV24AgendaAtendimento_Data3, "99/99/99"));
                     AV25AgendaAtendimento_Data_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AgendaAtendimento_Data_To3", context.localUtil.Format(AV25AgendaAtendimento_Data_To3, "99/99/99"));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AGENDAATENDIMENTO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV16AgendaAtendimento_Data1) && (DateTime.MinValue==AV17AgendaAtendimento_Data_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV16AgendaAtendimento_Data1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV17AgendaAtendimento_Data_To1, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AGENDAATENDIMENTO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV20AgendaAtendimento_Data2) && (DateTime.MinValue==AV21AgendaAtendimento_Data_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV20AgendaAtendimento_Data2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV21AgendaAtendimento_Data_To2, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AGENDAATENDIMENTO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV24AgendaAtendimento_Data3) && (DateTime.MinValue==AV25AgendaAtendimento_Data_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV24AgendaAtendimento_Data3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV25AgendaAtendimento_Data_To3, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_IE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_IE2( true) ;
         }
         else
         {
            wb_table2_5_IE2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_IE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_80_IE2( true) ;
         }
         else
         {
            wb_table3_80_IE2( false) ;
         }
         return  ;
      }

      protected void wb_table3_80_IE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_IE2e( true) ;
         }
         else
         {
            wb_table1_2_IE2e( false) ;
         }
      }

      protected void wb_table3_80_IE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_83_IE2( true) ;
         }
         else
         {
            wb_table4_83_IE2( false) ;
         }
         return  ;
      }

      protected void wb_table4_83_IE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_80_IE2e( true) ;
         }
         else
         {
            wb_table3_80_IE2e( false) ;
         }
      }

      protected void wb_table4_83_IE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"86\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAgendaAtendimento_CntSrcCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtAgendaAtendimento_CntSrcCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAgendaAtendimento_CntSrcCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAgendaAtendimento_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtAgendaAtendimento_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAgendaAtendimento_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAgendaAtendimento_CntSrcCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAgendaAtendimento_CntSrcCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAgendaAtendimento_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAgendaAtendimento_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 86 )
         {
            wbEnd = 0;
            nRC_GXsfl_86 = (short)(nGXsfl_86_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_83_IE2e( true) ;
         }
         else
         {
            wb_table4_83_IE2e( false) ;
         }
      }

      protected void wb_table2_5_IE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptAgendaAtendimento.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_IE2( true) ;
         }
         else
         {
            wb_table5_14_IE2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_IE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_IE2e( true) ;
         }
         else
         {
            wb_table2_5_IE2e( false) ;
         }
      }

      protected void wb_table5_14_IE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_IE2( true) ;
         }
         else
         {
            wb_table6_19_IE2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_IE2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_IE2e( true) ;
         }
         else
         {
            wb_table5_14_IE2e( false) ;
         }
      }

      protected void wb_table6_19_IE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e25ie1_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptAgendaAtendimento.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_IE2( true) ;
         }
         else
         {
            wb_table7_28_IE2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_IE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptAgendaAtendimento.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e26ie1_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_PromptAgendaAtendimento.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_47_IE2( true) ;
         }
         else
         {
            wb_table8_47_IE2( false) ;
         }
         return  ;
      }

      protected void wb_table8_47_IE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptAgendaAtendimento.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e27ie1_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_PromptAgendaAtendimento.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_66_IE2( true) ;
         }
         else
         {
            wb_table9_66_IE2( false) ;
         }
         return  ;
      }

      protected void wb_table9_66_IE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_IE2e( true) ;
         }
         else
         {
            wb_table6_19_IE2e( false) ;
         }
      }

      protected void wb_table9_66_IE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersagendaatendimento_data3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersagendaatendimento_data3_Internalname, tblTablemergeddynamicfiltersagendaatendimento_data3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAgendaatendimento_data3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAgendaatendimento_data3_Internalname, context.localUtil.Format(AV24AgendaAtendimento_Data3, "99/99/99"), context.localUtil.Format( AV24AgendaAtendimento_Data3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgendaatendimento_data3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavAgendaatendimento_data3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersagendaatendimento_data_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfiltersagendaatendimento_data_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAgendaatendimento_data_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAgendaatendimento_data_to3_Internalname, context.localUtil.Format(AV25AgendaAtendimento_Data_To3, "99/99/99"), context.localUtil.Format( AV25AgendaAtendimento_Data_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgendaatendimento_data_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavAgendaatendimento_data_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_66_IE2e( true) ;
         }
         else
         {
            wb_table9_66_IE2e( false) ;
         }
      }

      protected void wb_table8_47_IE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersagendaatendimento_data2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersagendaatendimento_data2_Internalname, tblTablemergeddynamicfiltersagendaatendimento_data2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAgendaatendimento_data2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAgendaatendimento_data2_Internalname, context.localUtil.Format(AV20AgendaAtendimento_Data2, "99/99/99"), context.localUtil.Format( AV20AgendaAtendimento_Data2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgendaatendimento_data2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavAgendaatendimento_data2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersagendaatendimento_data_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfiltersagendaatendimento_data_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAgendaatendimento_data_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAgendaatendimento_data_to2_Internalname, context.localUtil.Format(AV21AgendaAtendimento_Data_To2, "99/99/99"), context.localUtil.Format( AV21AgendaAtendimento_Data_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgendaatendimento_data_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavAgendaatendimento_data_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_47_IE2e( true) ;
         }
         else
         {
            wb_table8_47_IE2e( false) ;
         }
      }

      protected void wb_table7_28_IE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersagendaatendimento_data1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersagendaatendimento_data1_Internalname, tblTablemergeddynamicfiltersagendaatendimento_data1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAgendaatendimento_data1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAgendaatendimento_data1_Internalname, context.localUtil.Format(AV16AgendaAtendimento_Data1, "99/99/99"), context.localUtil.Format( AV16AgendaAtendimento_Data1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgendaatendimento_data1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavAgendaatendimento_data1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersagendaatendimento_data_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfiltersagendaatendimento_data_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAgendaatendimento_data_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAgendaatendimento_data_to1_Internalname, context.localUtil.Format(AV17AgendaAtendimento_Data_To1, "99/99/99"), context.localUtil.Format( AV17AgendaAtendimento_Data_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAgendaatendimento_data_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptAgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtavAgendaatendimento_data_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptAgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_IE2e( true) ;
         }
         else
         {
            wb_table7_28_IE2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutAgendaAtendimento_CntSrcCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutAgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutAgendaAtendimento_CntSrcCod), 6, 0)));
         AV8InOutAgendaAtendimento_Data = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutAgendaAtendimento_Data", context.localUtil.Format(AV8InOutAgendaAtendimento_Data, "99/99/99"));
         AV30InOutAgendaAtendimento_CodDmn = Convert.ToInt32(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30InOutAgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30InOutAgendaAtendimento_CodDmn), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAIE2( ) ;
         WSIE2( ) ;
         WEIE2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299395477");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptagendaatendimento.js", "?20205299395477");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_862( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_86_idx;
         edtAgendaAtendimento_CntSrcCod_Internalname = "AGENDAATENDIMENTO_CNTSRCCOD_"+sGXsfl_86_idx;
         edtAgendaAtendimento_Data_Internalname = "AGENDAATENDIMENTO_DATA_"+sGXsfl_86_idx;
      }

      protected void SubsflControlProps_fel_862( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_86_fel_idx;
         edtAgendaAtendimento_CntSrcCod_Internalname = "AGENDAATENDIMENTO_CNTSRCCOD_"+sGXsfl_86_fel_idx;
         edtAgendaAtendimento_Data_Internalname = "AGENDAATENDIMENTO_DATA_"+sGXsfl_86_fel_idx;
      }

      protected void sendrow_862( )
      {
         SubsflControlProps_862( ) ;
         WBIE0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_86_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_86_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_86_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 87,'',false,'',86)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV47Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV47Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_86_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAgendaAtendimento_CntSrcCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1183AgendaAtendimento_CntSrcCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAgendaAtendimento_CntSrcCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAgendaAtendimento_Data_Internalname,context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"),context.localUtil.Format( A1184AgendaAtendimento_Data, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAgendaAtendimento_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_AGENDAATENDIMENTO_CNTSRCCOD"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A1183AgendaAtendimento_CntSrcCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AGENDAATENDIMENTO_DATA"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, A1184AgendaAtendimento_Data));
            GridContainer.AddRow(GridRow);
            nGXsfl_86_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_86_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_86_idx+1));
            sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
            SubsflControlProps_862( ) ;
         }
         /* End function sendrow_862 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavAgendaatendimento_data1_Internalname = "vAGENDAATENDIMENTO_DATA1";
         lblDynamicfiltersagendaatendimento_data_rangemiddletext1_Internalname = "DYNAMICFILTERSAGENDAATENDIMENTO_DATA_RANGEMIDDLETEXT1";
         edtavAgendaatendimento_data_to1_Internalname = "vAGENDAATENDIMENTO_DATA_TO1";
         tblTablemergeddynamicfiltersagendaatendimento_data1_Internalname = "TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavAgendaatendimento_data2_Internalname = "vAGENDAATENDIMENTO_DATA2";
         lblDynamicfiltersagendaatendimento_data_rangemiddletext2_Internalname = "DYNAMICFILTERSAGENDAATENDIMENTO_DATA_RANGEMIDDLETEXT2";
         edtavAgendaatendimento_data_to2_Internalname = "vAGENDAATENDIMENTO_DATA_TO2";
         tblTablemergeddynamicfiltersagendaatendimento_data2_Internalname = "TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavAgendaatendimento_data3_Internalname = "vAGENDAATENDIMENTO_DATA3";
         lblDynamicfiltersagendaatendimento_data_rangemiddletext3_Internalname = "DYNAMICFILTERSAGENDAATENDIMENTO_DATA_RANGEMIDDLETEXT3";
         edtavAgendaatendimento_data_to3_Internalname = "vAGENDAATENDIMENTO_DATA_TO3";
         tblTablemergeddynamicfiltersagendaatendimento_data3_Internalname = "TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtAgendaAtendimento_CntSrcCod_Internalname = "AGENDAATENDIMENTO_CNTSRCCOD";
         edtAgendaAtendimento_Data_Internalname = "AGENDAATENDIMENTO_DATA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         edtAgendaAtendimento_CodDmn_Internalname = "AGENDAATENDIMENTO_CODDMN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfagendaatendimento_cntsrccod_Internalname = "vTFAGENDAATENDIMENTO_CNTSRCCOD";
         edtavTfagendaatendimento_cntsrccod_to_Internalname = "vTFAGENDAATENDIMENTO_CNTSRCCOD_TO";
         edtavTfagendaatendimento_data_Internalname = "vTFAGENDAATENDIMENTO_DATA";
         edtavTfagendaatendimento_data_to_Internalname = "vTFAGENDAATENDIMENTO_DATA_TO";
         edtavDdo_agendaatendimento_dataauxdate_Internalname = "vDDO_AGENDAATENDIMENTO_DATAAUXDATE";
         edtavDdo_agendaatendimento_dataauxdateto_Internalname = "vDDO_AGENDAATENDIMENTO_DATAAUXDATETO";
         divDdo_agendaatendimento_dataauxdates_Internalname = "DDO_AGENDAATENDIMENTO_DATAAUXDATES";
         Ddo_agendaatendimento_cntsrccod_Internalname = "DDO_AGENDAATENDIMENTO_CNTSRCCOD";
         edtavDdo_agendaatendimento_cntsrccodtitlecontrolidtoreplace_Internalname = "vDDO_AGENDAATENDIMENTO_CNTSRCCODTITLECONTROLIDTOREPLACE";
         Ddo_agendaatendimento_data_Internalname = "DDO_AGENDAATENDIMENTO_DATA";
         edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Internalname = "vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtAgendaAtendimento_Data_Jsonclick = "";
         edtAgendaAtendimento_CntSrcCod_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavAgendaatendimento_data_to1_Jsonclick = "";
         edtavAgendaatendimento_data1_Jsonclick = "";
         edtavAgendaatendimento_data_to2_Jsonclick = "";
         edtavAgendaatendimento_data2_Jsonclick = "";
         edtavAgendaatendimento_data_to3_Jsonclick = "";
         edtavAgendaatendimento_data3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtAgendaAtendimento_Data_Titleformat = 0;
         edtAgendaAtendimento_CntSrcCod_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         tblTablemergeddynamicfiltersagendaatendimento_data3_Visible = 1;
         tblTablemergeddynamicfiltersagendaatendimento_data2_Visible = 1;
         tblTablemergeddynamicfiltersagendaatendimento_data1_Visible = 1;
         edtAgendaAtendimento_Data_Title = "Data";
         edtAgendaAtendimento_CntSrcCod_Title = "Agenda Atendimento_Cnt Src Cod";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_agendaatendimento_cntsrccodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_agendaatendimento_dataauxdateto_Jsonclick = "";
         edtavDdo_agendaatendimento_dataauxdate_Jsonclick = "";
         edtavTfagendaatendimento_data_to_Jsonclick = "";
         edtavTfagendaatendimento_data_to_Visible = 1;
         edtavTfagendaatendimento_data_Jsonclick = "";
         edtavTfagendaatendimento_data_Visible = 1;
         edtavTfagendaatendimento_cntsrccod_to_Jsonclick = "";
         edtavTfagendaatendimento_cntsrccod_to_Visible = 1;
         edtavTfagendaatendimento_cntsrccod_Jsonclick = "";
         edtavTfagendaatendimento_cntsrccod_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtAgendaAtendimento_CodDmn_Jsonclick = "";
         edtAgendaAtendimento_CodDmn_Visible = 1;
         Ddo_agendaatendimento_data_Searchbuttontext = "Pesquisar";
         Ddo_agendaatendimento_data_Rangefilterto = "At�";
         Ddo_agendaatendimento_data_Rangefilterfrom = "Desde";
         Ddo_agendaatendimento_data_Cleanfilter = "Limpar pesquisa";
         Ddo_agendaatendimento_data_Sortdsc = "Ordenar de Z � A";
         Ddo_agendaatendimento_data_Sortasc = "Ordenar de A � Z";
         Ddo_agendaatendimento_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_agendaatendimento_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_data_Filtertype = "Date";
         Ddo_agendaatendimento_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_data_Titlecontrolidtoreplace = "";
         Ddo_agendaatendimento_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_agendaatendimento_data_Cls = "ColumnSettings";
         Ddo_agendaatendimento_data_Tooltip = "Op��es";
         Ddo_agendaatendimento_data_Caption = "";
         Ddo_agendaatendimento_cntsrccod_Searchbuttontext = "Pesquisar";
         Ddo_agendaatendimento_cntsrccod_Rangefilterto = "At�";
         Ddo_agendaatendimento_cntsrccod_Rangefilterfrom = "Desde";
         Ddo_agendaatendimento_cntsrccod_Cleanfilter = "Limpar pesquisa";
         Ddo_agendaatendimento_cntsrccod_Sortdsc = "Ordenar de Z � A";
         Ddo_agendaatendimento_cntsrccod_Sortasc = "Ordenar de A � Z";
         Ddo_agendaatendimento_cntsrccod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_agendaatendimento_cntsrccod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_cntsrccod_Filtertype = "Numeric";
         Ddo_agendaatendimento_cntsrccod_Includefilter = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_cntsrccod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_cntsrccod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_cntsrccod_Titlecontrolidtoreplace = "";
         Ddo_agendaatendimento_cntsrccod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_agendaatendimento_cntsrccod_Cls = "ColumnSettings";
         Ddo_agendaatendimento_cntsrccod_Tooltip = "Op��es";
         Ddo_agendaatendimento_cntsrccod_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Agenda Atendimento";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFAgendaAtendimento_CntSrcCod',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFAgendaAtendimento_CntSrcCod_To',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV37TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CNTSRCCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[{av:'AV31AgendaAtendimento_CntSrcCodTitleFilterData',fld:'vAGENDAATENDIMENTO_CNTSRCCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV35AgendaAtendimento_DataTitleFilterData',fld:'vAGENDAATENDIMENTO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'edtAgendaAtendimento_CntSrcCod_Titleformat',ctrl:'AGENDAATENDIMENTO_CNTSRCCOD',prop:'Titleformat'},{av:'edtAgendaAtendimento_CntSrcCod_Title',ctrl:'AGENDAATENDIMENTO_CNTSRCCOD',prop:'Title'},{av:'edtAgendaAtendimento_Data_Titleformat',ctrl:'AGENDAATENDIMENTO_DATA',prop:'Titleformat'},{av:'edtAgendaAtendimento_Data_Title',ctrl:'AGENDAATENDIMENTO_DATA',prop:'Title'},{av:'AV43GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV44GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11IE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFAgendaAtendimento_CntSrcCod',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFAgendaAtendimento_CntSrcCod_To',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV37TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CNTSRCCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_AGENDAATENDIMENTO_CNTSRCCOD.ONOPTIONCLICKED","{handler:'E12IE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFAgendaAtendimento_CntSrcCod',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFAgendaAtendimento_CntSrcCod_To',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV37TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CNTSRCCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_agendaatendimento_cntsrccod_Activeeventkey',ctrl:'DDO_AGENDAATENDIMENTO_CNTSRCCOD',prop:'ActiveEventKey'},{av:'Ddo_agendaatendimento_cntsrccod_Filteredtext_get',ctrl:'DDO_AGENDAATENDIMENTO_CNTSRCCOD',prop:'FilteredText_get'},{av:'Ddo_agendaatendimento_cntsrccod_Filteredtextto_get',ctrl:'DDO_AGENDAATENDIMENTO_CNTSRCCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_agendaatendimento_cntsrccod_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_CNTSRCCOD',prop:'SortedStatus'},{av:'AV32TFAgendaAtendimento_CntSrcCod',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFAgendaAtendimento_CntSrcCod_To',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_agendaatendimento_data_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AGENDAATENDIMENTO_DATA.ONOPTIONCLICKED","{handler:'E13IE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFAgendaAtendimento_CntSrcCod',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFAgendaAtendimento_CntSrcCod_To',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV37TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CNTSRCCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_agendaatendimento_data_Activeeventkey',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'ActiveEventKey'},{av:'Ddo_agendaatendimento_data_Filteredtext_get',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'FilteredText_get'},{av:'Ddo_agendaatendimento_data_Filteredtextto_get',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_agendaatendimento_data_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'SortedStatus'},{av:'AV36TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV37TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'Ddo_agendaatendimento_cntsrccod_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_CNTSRCCOD',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E23IE2',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E24IE2',iparms:[{av:'A1183AgendaAtendimento_CntSrcCod',fld:'AGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1184AgendaAtendimento_Data',fld:'AGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7InOutAgendaAtendimento_CntSrcCod',fld:'vINOUTAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'AV8InOutAgendaAtendimento_Data',fld:'vINOUTAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV30InOutAgendaAtendimento_CodDmn',fld:'vINOUTAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E14IE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFAgendaAtendimento_CntSrcCod',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFAgendaAtendimento_CntSrcCod_To',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV37TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CNTSRCCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E19IE2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E15IE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFAgendaAtendimento_CntSrcCod',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFAgendaAtendimento_CntSrcCod_To',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV37TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CNTSRCCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E25IE1',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfiltersagendaatendimento_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E20IE2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E16IE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFAgendaAtendimento_CntSrcCod',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFAgendaAtendimento_CntSrcCod_To',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV37TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CNTSRCCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E26IE1',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfiltersagendaatendimento_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E17IE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFAgendaAtendimento_CntSrcCod',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFAgendaAtendimento_CntSrcCod_To',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV37TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CNTSRCCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E27IE1',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfiltersagendaatendimento_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E18IE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFAgendaAtendimento_CntSrcCod',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFAgendaAtendimento_CntSrcCod_To',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV37TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_CNTSRCCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV32TFAgendaAtendimento_CntSrcCod',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_agendaatendimento_cntsrccod_Filteredtext_set',ctrl:'DDO_AGENDAATENDIMENTO_CNTSRCCOD',prop:'FilteredText_set'},{av:'AV33TFAgendaAtendimento_CntSrcCod_To',fld:'vTFAGENDAATENDIMENTO_CNTSRCCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_agendaatendimento_cntsrccod_Filteredtextto_set',ctrl:'DDO_AGENDAATENDIMENTO_CNTSRCCOD',prop:'FilteredTextTo_set'},{av:'AV36TFAgendaAtendimento_Data',fld:'vTFAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'Ddo_agendaatendimento_data_Filteredtext_set',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'FilteredText_set'},{av:'AV37TFAgendaAtendimento_Data_To',fld:'vTFAGENDAATENDIMENTO_DATA_TO',pic:'',nv:''},{av:'Ddo_agendaatendimento_data_Filteredtextto_set',ctrl:'DDO_AGENDAATENDIMENTO_DATA',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AgendaAtendimento_Data1',fld:'vAGENDAATENDIMENTO_DATA1',pic:'',nv:''},{av:'AV17AgendaAtendimento_Data_To1',fld:'vAGENDAATENDIMENTO_DATA_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfiltersagendaatendimento_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AgendaAtendimento_Data2',fld:'vAGENDAATENDIMENTO_DATA2',pic:'',nv:''},{av:'AV21AgendaAtendimento_Data_To2',fld:'vAGENDAATENDIMENTO_DATA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AgendaAtendimento_Data3',fld:'vAGENDAATENDIMENTO_DATA3',pic:'',nv:''},{av:'AV25AgendaAtendimento_Data_To3',fld:'vAGENDAATENDIMENTO_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersagendaatendimento_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAGENDAATENDIMENTO_DATA3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutAgendaAtendimento_Data = DateTime.MinValue;
         Gridpaginationbar_Selectedpage = "";
         Ddo_agendaatendimento_cntsrccod_Activeeventkey = "";
         Ddo_agendaatendimento_cntsrccod_Filteredtext_get = "";
         Ddo_agendaatendimento_cntsrccod_Filteredtextto_get = "";
         Ddo_agendaatendimento_data_Activeeventkey = "";
         Ddo_agendaatendimento_data_Filteredtext_get = "";
         Ddo_agendaatendimento_data_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16AgendaAtendimento_Data1 = DateTime.MinValue;
         AV17AgendaAtendimento_Data_To1 = DateTime.MinValue;
         AV19DynamicFiltersSelector2 = "";
         AV20AgendaAtendimento_Data2 = DateTime.MinValue;
         AV21AgendaAtendimento_Data_To2 = DateTime.MinValue;
         AV23DynamicFiltersSelector3 = "";
         AV24AgendaAtendimento_Data3 = DateTime.MinValue;
         AV25AgendaAtendimento_Data_To3 = DateTime.MinValue;
         AV36TFAgendaAtendimento_Data = DateTime.MinValue;
         AV37TFAgendaAtendimento_Data_To = DateTime.MinValue;
         AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace = "";
         AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace = "";
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV41DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV31AgendaAtendimento_CntSrcCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV35AgendaAtendimento_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         Ddo_agendaatendimento_cntsrccod_Filteredtext_set = "";
         Ddo_agendaatendimento_cntsrccod_Filteredtextto_set = "";
         Ddo_agendaatendimento_cntsrccod_Sortedstatus = "";
         Ddo_agendaatendimento_data_Filteredtext_set = "";
         Ddo_agendaatendimento_data_Filteredtextto_set = "";
         Ddo_agendaatendimento_data_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV38DDO_AgendaAtendimento_DataAuxDate = DateTime.MinValue;
         AV39DDO_AgendaAtendimento_DataAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV47Select_GXI = "";
         A1184AgendaAtendimento_Data = DateTime.MinValue;
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00IE2_A1209AgendaAtendimento_CodDmn = new int[1] ;
         H00IE2_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         H00IE2_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         H00IE3_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfiltersagendaatendimento_data_rangemiddletext3_Jsonclick = "";
         lblDynamicfiltersagendaatendimento_data_rangemiddletext2_Jsonclick = "";
         lblDynamicfiltersagendaatendimento_data_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptagendaatendimento__default(),
            new Object[][] {
                new Object[] {
               H00IE2_A1209AgendaAtendimento_CodDmn, H00IE2_A1184AgendaAtendimento_Data, H00IE2_A1183AgendaAtendimento_CntSrcCod
               }
               , new Object[] {
               H00IE3_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_86 ;
      private short nGXsfl_86_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_86_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtAgendaAtendimento_CntSrcCod_Titleformat ;
      private short edtAgendaAtendimento_Data_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutAgendaAtendimento_CntSrcCod ;
      private int AV30InOutAgendaAtendimento_CodDmn ;
      private int wcpOAV7InOutAgendaAtendimento_CntSrcCod ;
      private int wcpOAV30InOutAgendaAtendimento_CodDmn ;
      private int subGrid_Rows ;
      private int AV32TFAgendaAtendimento_CntSrcCod ;
      private int AV33TFAgendaAtendimento_CntSrcCod_To ;
      private int A1209AgendaAtendimento_CodDmn ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtAgendaAtendimento_CodDmn_Visible ;
      private int edtavTfagendaatendimento_cntsrccod_Visible ;
      private int edtavTfagendaatendimento_cntsrccod_to_Visible ;
      private int edtavTfagendaatendimento_data_Visible ;
      private int edtavTfagendaatendimento_data_to_Visible ;
      private int edtavDdo_agendaatendimento_cntsrccodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Visible ;
      private int A1183AgendaAtendimento_CntSrcCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV42PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfiltersagendaatendimento_data1_Visible ;
      private int tblTablemergeddynamicfiltersagendaatendimento_data2_Visible ;
      private int tblTablemergeddynamicfiltersagendaatendimento_data3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV43GridCurrentPage ;
      private long AV44GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_agendaatendimento_cntsrccod_Activeeventkey ;
      private String Ddo_agendaatendimento_cntsrccod_Filteredtext_get ;
      private String Ddo_agendaatendimento_cntsrccod_Filteredtextto_get ;
      private String Ddo_agendaatendimento_data_Activeeventkey ;
      private String Ddo_agendaatendimento_data_Filteredtext_get ;
      private String Ddo_agendaatendimento_data_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_86_idx="0001" ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_agendaatendimento_cntsrccod_Caption ;
      private String Ddo_agendaatendimento_cntsrccod_Tooltip ;
      private String Ddo_agendaatendimento_cntsrccod_Cls ;
      private String Ddo_agendaatendimento_cntsrccod_Filteredtext_set ;
      private String Ddo_agendaatendimento_cntsrccod_Filteredtextto_set ;
      private String Ddo_agendaatendimento_cntsrccod_Dropdownoptionstype ;
      private String Ddo_agendaatendimento_cntsrccod_Titlecontrolidtoreplace ;
      private String Ddo_agendaatendimento_cntsrccod_Sortedstatus ;
      private String Ddo_agendaatendimento_cntsrccod_Filtertype ;
      private String Ddo_agendaatendimento_cntsrccod_Sortasc ;
      private String Ddo_agendaatendimento_cntsrccod_Sortdsc ;
      private String Ddo_agendaatendimento_cntsrccod_Cleanfilter ;
      private String Ddo_agendaatendimento_cntsrccod_Rangefilterfrom ;
      private String Ddo_agendaatendimento_cntsrccod_Rangefilterto ;
      private String Ddo_agendaatendimento_cntsrccod_Searchbuttontext ;
      private String Ddo_agendaatendimento_data_Caption ;
      private String Ddo_agendaatendimento_data_Tooltip ;
      private String Ddo_agendaatendimento_data_Cls ;
      private String Ddo_agendaatendimento_data_Filteredtext_set ;
      private String Ddo_agendaatendimento_data_Filteredtextto_set ;
      private String Ddo_agendaatendimento_data_Dropdownoptionstype ;
      private String Ddo_agendaatendimento_data_Titlecontrolidtoreplace ;
      private String Ddo_agendaatendimento_data_Sortedstatus ;
      private String Ddo_agendaatendimento_data_Filtertype ;
      private String Ddo_agendaatendimento_data_Sortasc ;
      private String Ddo_agendaatendimento_data_Sortdsc ;
      private String Ddo_agendaatendimento_data_Cleanfilter ;
      private String Ddo_agendaatendimento_data_Rangefilterfrom ;
      private String Ddo_agendaatendimento_data_Rangefilterto ;
      private String Ddo_agendaatendimento_data_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String edtAgendaAtendimento_CodDmn_Internalname ;
      private String edtAgendaAtendimento_CodDmn_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfagendaatendimento_cntsrccod_Internalname ;
      private String edtavTfagendaatendimento_cntsrccod_Jsonclick ;
      private String edtavTfagendaatendimento_cntsrccod_to_Internalname ;
      private String edtavTfagendaatendimento_cntsrccod_to_Jsonclick ;
      private String edtavTfagendaatendimento_data_Internalname ;
      private String edtavTfagendaatendimento_data_Jsonclick ;
      private String edtavTfagendaatendimento_data_to_Internalname ;
      private String edtavTfagendaatendimento_data_to_Jsonclick ;
      private String divDdo_agendaatendimento_dataauxdates_Internalname ;
      private String edtavDdo_agendaatendimento_dataauxdate_Internalname ;
      private String edtavDdo_agendaatendimento_dataauxdate_Jsonclick ;
      private String edtavDdo_agendaatendimento_dataauxdateto_Internalname ;
      private String edtavDdo_agendaatendimento_dataauxdateto_Jsonclick ;
      private String edtavDdo_agendaatendimento_cntsrccodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_agendaatendimento_datatitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtAgendaAtendimento_CntSrcCod_Internalname ;
      private String edtAgendaAtendimento_Data_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavAgendaatendimento_data1_Internalname ;
      private String edtavAgendaatendimento_data_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavAgendaatendimento_data2_Internalname ;
      private String edtavAgendaatendimento_data_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavAgendaatendimento_data3_Internalname ;
      private String edtavAgendaatendimento_data_to3_Internalname ;
      private String hsh ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_agendaatendimento_cntsrccod_Internalname ;
      private String Ddo_agendaatendimento_data_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtAgendaAtendimento_CntSrcCod_Title ;
      private String edtAgendaAtendimento_Data_Title ;
      private String edtavSelect_Tooltiptext ;
      private String tblTablemergeddynamicfiltersagendaatendimento_data1_Internalname ;
      private String tblTablemergeddynamicfiltersagendaatendimento_data2_Internalname ;
      private String tblTablemergeddynamicfiltersagendaatendimento_data3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavAgendaatendimento_data3_Jsonclick ;
      private String lblDynamicfiltersagendaatendimento_data_rangemiddletext3_Internalname ;
      private String lblDynamicfiltersagendaatendimento_data_rangemiddletext3_Jsonclick ;
      private String edtavAgendaatendimento_data_to3_Jsonclick ;
      private String edtavAgendaatendimento_data2_Jsonclick ;
      private String lblDynamicfiltersagendaatendimento_data_rangemiddletext2_Internalname ;
      private String lblDynamicfiltersagendaatendimento_data_rangemiddletext2_Jsonclick ;
      private String edtavAgendaatendimento_data_to2_Jsonclick ;
      private String edtavAgendaatendimento_data1_Jsonclick ;
      private String lblDynamicfiltersagendaatendimento_data_rangemiddletext1_Internalname ;
      private String lblDynamicfiltersagendaatendimento_data_rangemiddletext1_Jsonclick ;
      private String edtavAgendaatendimento_data_to1_Jsonclick ;
      private String sGXsfl_86_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtAgendaAtendimento_CntSrcCod_Jsonclick ;
      private String edtAgendaAtendimento_Data_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV8InOutAgendaAtendimento_Data ;
      private DateTime wcpOAV8InOutAgendaAtendimento_Data ;
      private DateTime AV16AgendaAtendimento_Data1 ;
      private DateTime AV17AgendaAtendimento_Data_To1 ;
      private DateTime AV20AgendaAtendimento_Data2 ;
      private DateTime AV21AgendaAtendimento_Data_To2 ;
      private DateTime AV24AgendaAtendimento_Data3 ;
      private DateTime AV25AgendaAtendimento_Data_To3 ;
      private DateTime AV36TFAgendaAtendimento_Data ;
      private DateTime AV37TFAgendaAtendimento_Data_To ;
      private DateTime AV38DDO_AgendaAtendimento_DataAuxDate ;
      private DateTime AV39DDO_AgendaAtendimento_DataAuxDateTo ;
      private DateTime A1184AgendaAtendimento_Data ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool toggleJsOutput ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_agendaatendimento_cntsrccod_Includesortasc ;
      private bool Ddo_agendaatendimento_cntsrccod_Includesortdsc ;
      private bool Ddo_agendaatendimento_cntsrccod_Includefilter ;
      private bool Ddo_agendaatendimento_cntsrccod_Filterisrange ;
      private bool Ddo_agendaatendimento_cntsrccod_Includedatalist ;
      private bool Ddo_agendaatendimento_data_Includesortasc ;
      private bool Ddo_agendaatendimento_data_Includesortdsc ;
      private bool Ddo_agendaatendimento_data_Includefilter ;
      private bool Ddo_agendaatendimento_data_Filterisrange ;
      private bool Ddo_agendaatendimento_data_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV34ddo_AgendaAtendimento_CntSrcCodTitleControlIdToReplace ;
      private String AV40ddo_AgendaAtendimento_DataTitleControlIdToReplace ;
      private String AV47Select_GXI ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutAgendaAtendimento_CntSrcCod ;
      private DateTime aP1_InOutAgendaAtendimento_Data ;
      private int aP2_InOutAgendaAtendimento_CodDmn ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00IE2_A1209AgendaAtendimento_CodDmn ;
      private DateTime[] H00IE2_A1184AgendaAtendimento_Data ;
      private int[] H00IE2_A1183AgendaAtendimento_CntSrcCod ;
      private long[] H00IE3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV31AgendaAtendimento_CntSrcCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV35AgendaAtendimento_DataTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV41DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptagendaatendimento__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00IE2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV16AgendaAtendimento_Data1 ,
                                             DateTime AV17AgendaAtendimento_Data_To1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             DateTime AV20AgendaAtendimento_Data2 ,
                                             DateTime AV21AgendaAtendimento_Data_To2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             DateTime AV24AgendaAtendimento_Data3 ,
                                             DateTime AV25AgendaAtendimento_Data_To3 ,
                                             int AV32TFAgendaAtendimento_CntSrcCod ,
                                             int AV33TFAgendaAtendimento_CntSrcCod_To ,
                                             DateTime AV36TFAgendaAtendimento_Data ,
                                             DateTime AV37TFAgendaAtendimento_Data_To ,
                                             DateTime A1184AgendaAtendimento_Data ,
                                             int A1183AgendaAtendimento_CntSrcCod ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [15] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [AgendaAtendimento_CodDmn], [AgendaAtendimento_Data], [AgendaAtendimento_CntSrcCod]";
         sFromString = " FROM [AgendaAtendimento] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV16AgendaAtendimento_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_Data] >= @AV16AgendaAtendimento_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_Data] >= @AV16AgendaAtendimento_Data1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV17AgendaAtendimento_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_Data] <= @AV17AgendaAtendimento_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_Data] <= @AV17AgendaAtendimento_Data_To1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV20AgendaAtendimento_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_Data] >= @AV20AgendaAtendimento_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_Data] >= @AV20AgendaAtendimento_Data2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV21AgendaAtendimento_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_Data] <= @AV21AgendaAtendimento_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_Data] <= @AV21AgendaAtendimento_Data_To2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV24AgendaAtendimento_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_Data] >= @AV24AgendaAtendimento_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_Data] >= @AV24AgendaAtendimento_Data3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV25AgendaAtendimento_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_Data] <= @AV25AgendaAtendimento_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_Data] <= @AV25AgendaAtendimento_Data_To3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV32TFAgendaAtendimento_CntSrcCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_CntSrcCod] >= @AV32TFAgendaAtendimento_CntSrcCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_CntSrcCod] >= @AV32TFAgendaAtendimento_CntSrcCod)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV33TFAgendaAtendimento_CntSrcCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_CntSrcCod] <= @AV33TFAgendaAtendimento_CntSrcCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_CntSrcCod] <= @AV33TFAgendaAtendimento_CntSrcCod_To)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (DateTime.MinValue==AV36TFAgendaAtendimento_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_Data] >= @AV36TFAgendaAtendimento_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_Data] >= @AV36TFAgendaAtendimento_Data)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV37TFAgendaAtendimento_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_Data] <= @AV37TFAgendaAtendimento_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_Data] <= @AV37TFAgendaAtendimento_Data_To)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [AgendaAtendimento_Data]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [AgendaAtendimento_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [AgendaAtendimento_CntSrcCod]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [AgendaAtendimento_CntSrcCod] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [AgendaAtendimento_CntSrcCod], [AgendaAtendimento_Data], [AgendaAtendimento_CodDmn]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00IE3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV16AgendaAtendimento_Data1 ,
                                             DateTime AV17AgendaAtendimento_Data_To1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             DateTime AV20AgendaAtendimento_Data2 ,
                                             DateTime AV21AgendaAtendimento_Data_To2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             DateTime AV24AgendaAtendimento_Data3 ,
                                             DateTime AV25AgendaAtendimento_Data_To3 ,
                                             int AV32TFAgendaAtendimento_CntSrcCod ,
                                             int AV33TFAgendaAtendimento_CntSrcCod_To ,
                                             DateTime AV36TFAgendaAtendimento_Data ,
                                             DateTime AV37TFAgendaAtendimento_Data_To ,
                                             DateTime A1184AgendaAtendimento_Data ,
                                             int A1183AgendaAtendimento_CntSrcCod ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [10] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [AgendaAtendimento] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV16AgendaAtendimento_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_Data] >= @AV16AgendaAtendimento_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_Data] >= @AV16AgendaAtendimento_Data1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV17AgendaAtendimento_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_Data] <= @AV17AgendaAtendimento_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_Data] <= @AV17AgendaAtendimento_Data_To1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV20AgendaAtendimento_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_Data] >= @AV20AgendaAtendimento_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_Data] >= @AV20AgendaAtendimento_Data2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV21AgendaAtendimento_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_Data] <= @AV21AgendaAtendimento_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_Data] <= @AV21AgendaAtendimento_Data_To2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV24AgendaAtendimento_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_Data] >= @AV24AgendaAtendimento_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_Data] >= @AV24AgendaAtendimento_Data3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV25AgendaAtendimento_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_Data] <= @AV25AgendaAtendimento_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_Data] <= @AV25AgendaAtendimento_Data_To3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV32TFAgendaAtendimento_CntSrcCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_CntSrcCod] >= @AV32TFAgendaAtendimento_CntSrcCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_CntSrcCod] >= @AV32TFAgendaAtendimento_CntSrcCod)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV33TFAgendaAtendimento_CntSrcCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_CntSrcCod] <= @AV33TFAgendaAtendimento_CntSrcCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_CntSrcCod] <= @AV33TFAgendaAtendimento_CntSrcCod_To)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (DateTime.MinValue==AV36TFAgendaAtendimento_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_Data] >= @AV36TFAgendaAtendimento_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_Data] >= @AV36TFAgendaAtendimento_Data)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV37TFAgendaAtendimento_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AgendaAtendimento_Data] <= @AV37TFAgendaAtendimento_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([AgendaAtendimento_Data] <= @AV37TFAgendaAtendimento_Data_To)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00IE2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (int)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] );
               case 1 :
                     return conditional_H00IE3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (int)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00IE2 ;
          prmH00IE2 = new Object[] {
          new Object[] {"@AV16AgendaAtendimento_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17AgendaAtendimento_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20AgendaAtendimento_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21AgendaAtendimento_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24AgendaAtendimento_Data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25AgendaAtendimento_Data_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV32TFAgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV33TFAgendaAtendimento_CntSrcCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFAgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV37TFAgendaAtendimento_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00IE3 ;
          prmH00IE3 = new Object[] {
          new Object[] {"@AV16AgendaAtendimento_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17AgendaAtendimento_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20AgendaAtendimento_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21AgendaAtendimento_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24AgendaAtendimento_Data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25AgendaAtendimento_Data_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV32TFAgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV33TFAgendaAtendimento_CntSrcCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFAgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV37TFAgendaAtendimento_Data_To",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00IE2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IE2,11,0,true,false )
             ,new CursorDef("H00IE3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IE3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                return;
       }
    }

 }

}
