/*
               File: Prc_AutorizacaoConsumo_VerificaAtivo
        Description: Prc_Autorizacao Consumo_Verifica Ativo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:54.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_autorizacaoconsumo_verificaativo : GXProcedure
   {
      public prc_autorizacaoconsumo_verificaativo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_autorizacaoconsumo_verificaativo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo ,
                           int aP1_AutorizacaoConsumo_UnidadeMedicaoCod ,
                           out bool aP2_IsExisteAtivo )
      {
         this.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV10AutorizacaoConsumo_UnidadeMedicaoCod = aP1_AutorizacaoConsumo_UnidadeMedicaoCod;
         this.AV9IsExisteAtivo = false ;
         initialize();
         executePrivate();
         aP2_IsExisteAtivo=this.AV9IsExisteAtivo;
      }

      public bool executeUdp( int aP0_Contrato_Codigo ,
                              int aP1_AutorizacaoConsumo_UnidadeMedicaoCod )
      {
         this.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV10AutorizacaoConsumo_UnidadeMedicaoCod = aP1_AutorizacaoConsumo_UnidadeMedicaoCod;
         this.AV9IsExisteAtivo = false ;
         initialize();
         executePrivate();
         aP2_IsExisteAtivo=this.AV9IsExisteAtivo;
         return AV9IsExisteAtivo ;
      }

      public void executeSubmit( int aP0_Contrato_Codigo ,
                                 int aP1_AutorizacaoConsumo_UnidadeMedicaoCod ,
                                 out bool aP2_IsExisteAtivo )
      {
         prc_autorizacaoconsumo_verificaativo objprc_autorizacaoconsumo_verificaativo;
         objprc_autorizacaoconsumo_verificaativo = new prc_autorizacaoconsumo_verificaativo();
         objprc_autorizacaoconsumo_verificaativo.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_autorizacaoconsumo_verificaativo.AV10AutorizacaoConsumo_UnidadeMedicaoCod = aP1_AutorizacaoConsumo_UnidadeMedicaoCod;
         objprc_autorizacaoconsumo_verificaativo.AV9IsExisteAtivo = false ;
         objprc_autorizacaoconsumo_verificaativo.context.SetSubmitInitialConfig(context);
         objprc_autorizacaoconsumo_verificaativo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_autorizacaoconsumo_verificaativo);
         aP2_IsExisteAtivo=this.AV9IsExisteAtivo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_autorizacaoconsumo_verificaativo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9IsExisteAtivo = false;
         /* Using cursor P00D12 */
         pr_default.execute(0, new Object[] {AV10AutorizacaoConsumo_UnidadeMedicaoCod, AV8Contrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1777AutorizacaoConsumo_UnidadeMedicaoCod = P00D12_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0];
            A74Contrato_Codigo = P00D12_A74Contrato_Codigo[0];
            A1780AutorizacaoConsumo_Ativo = P00D12_A1780AutorizacaoConsumo_Ativo[0];
            A1774AutorizacaoConsumo_Codigo = P00D12_A1774AutorizacaoConsumo_Codigo[0];
            AV9IsExisteAtivo = true;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00D12_A1777AutorizacaoConsumo_UnidadeMedicaoCod = new int[1] ;
         P00D12_A74Contrato_Codigo = new int[1] ;
         P00D12_A1780AutorizacaoConsumo_Ativo = new bool[] {false} ;
         P00D12_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_autorizacaoconsumo_verificaativo__default(),
            new Object[][] {
                new Object[] {
               P00D12_A1777AutorizacaoConsumo_UnidadeMedicaoCod, P00D12_A74Contrato_Codigo, P00D12_A1780AutorizacaoConsumo_Ativo, P00D12_A1774AutorizacaoConsumo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Contrato_Codigo ;
      private int AV10AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int A74Contrato_Codigo ;
      private int A1774AutorizacaoConsumo_Codigo ;
      private String scmdbuf ;
      private bool AV9IsExisteAtivo ;
      private bool A1780AutorizacaoConsumo_Ativo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00D12_A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int[] P00D12_A74Contrato_Codigo ;
      private bool[] P00D12_A1780AutorizacaoConsumo_Ativo ;
      private int[] P00D12_A1774AutorizacaoConsumo_Codigo ;
      private bool aP2_IsExisteAtivo ;
   }

   public class prc_autorizacaoconsumo_verificaativo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00D12 ;
          prmP00D12 = new Object[] {
          new Object[] {"@AV10AutorizacaoConsumo_UnidadeMedicaoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00D12", "SELECT [AutorizacaoConsumo_UnidadeMedicaoCod], [Contrato_Codigo], [AutorizacaoConsumo_Ativo], [AutorizacaoConsumo_Codigo] FROM [AutorizacaoConsumo] WITH (NOLOCK) WHERE ([AutorizacaoConsumo_UnidadeMedicaoCod] = @AV10AutorizacaoConsumo_UnidadeMedicaoCod) AND ([AutorizacaoConsumo_Ativo] = 1) AND ([Contrato_Codigo] = @AV8Contrato_Codigo) ORDER BY [AutorizacaoConsumo_UnidadeMedicaoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00D12,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
