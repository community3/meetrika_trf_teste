/*
               File: PRC_RetornaImagemItemMenu
        Description: PRC_Retorna Imagem Item Menu
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:48.6
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_retornaimagemitemmenu : GXProcedure
   {
      public prc_retornaimagemitemmenu( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_retornaimagemitemmenu( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Menu_Codigo ,
                           out String aP1_Menu_Imagem )
      {
         this.AV8Menu_Codigo = aP0_Menu_Codigo;
         this.AV9Menu_Imagem = "" ;
         initialize();
         executePrivate();
         aP1_Menu_Imagem=this.AV9Menu_Imagem;
      }

      public String executeUdp( int aP0_Menu_Codigo )
      {
         this.AV8Menu_Codigo = aP0_Menu_Codigo;
         this.AV9Menu_Imagem = "" ;
         initialize();
         executePrivate();
         aP1_Menu_Imagem=this.AV9Menu_Imagem;
         return AV9Menu_Imagem ;
      }

      public void executeSubmit( int aP0_Menu_Codigo ,
                                 out String aP1_Menu_Imagem )
      {
         prc_retornaimagemitemmenu objprc_retornaimagemitemmenu;
         objprc_retornaimagemitemmenu = new prc_retornaimagemitemmenu();
         objprc_retornaimagemitemmenu.AV8Menu_Codigo = aP0_Menu_Codigo;
         objprc_retornaimagemitemmenu.AV9Menu_Imagem = "" ;
         objprc_retornaimagemitemmenu.context.SetSubmitInitialConfig(context);
         objprc_retornaimagemitemmenu.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_retornaimagemitemmenu);
         aP1_Menu_Imagem=this.AV9Menu_Imagem;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_retornaimagemitemmenu)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P001A2 */
         pr_default.execute(0, new Object[] {AV8Menu_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A277Menu_Codigo = P001A2_A277Menu_Codigo[0];
            A40000Menu_Imagem_GXI = P001A2_A40000Menu_Imagem_GXI[0];
            n40000Menu_Imagem_GXI = P001A2_n40000Menu_Imagem_GXI[0];
            A282Menu_Imagem = P001A2_A282Menu_Imagem[0];
            n282Menu_Imagem = P001A2_n282Menu_Imagem[0];
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) && String.IsNullOrEmpty(StringUtil.RTrim( A40000Menu_Imagem_GXI)) ) )
            {
               AV9Menu_Imagem = A282Menu_Imagem;
               AV13Menu_imagem_GXI = A40000Menu_Imagem_GXI;
            }
            else
            {
               AV9Menu_Imagem = context.GetImagePath( "eb70f502-bfaf-4c23-a9e3-b0df87cbbc7d", "", context.GetTheme( ));
               AV13Menu_imagem_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "eb70f502-bfaf-4c23-a9e3-b0df87cbbc7d", "", context.GetTheme( )));
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P001A2_A277Menu_Codigo = new int[1] ;
         P001A2_A40000Menu_Imagem_GXI = new String[] {""} ;
         P001A2_n40000Menu_Imagem_GXI = new bool[] {false} ;
         P001A2_A282Menu_Imagem = new String[] {""} ;
         P001A2_n282Menu_Imagem = new bool[] {false} ;
         A40000Menu_Imagem_GXI = "";
         A282Menu_Imagem = "";
         AV13Menu_imagem_GXI = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_retornaimagemitemmenu__default(),
            new Object[][] {
                new Object[] {
               P001A2_A277Menu_Codigo, P001A2_A40000Menu_Imagem_GXI, P001A2_n40000Menu_Imagem_GXI, P001A2_A282Menu_Imagem, P001A2_n282Menu_Imagem
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Menu_Codigo ;
      private int A277Menu_Codigo ;
      private String scmdbuf ;
      private bool n40000Menu_Imagem_GXI ;
      private bool n282Menu_Imagem ;
      private String A40000Menu_Imagem_GXI ;
      private String AV13Menu_imagem_GXI ;
      private String AV9Menu_Imagem ;
      private String A282Menu_Imagem ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P001A2_A277Menu_Codigo ;
      private String[] P001A2_A40000Menu_Imagem_GXI ;
      private bool[] P001A2_n40000Menu_Imagem_GXI ;
      private String[] P001A2_A282Menu_Imagem ;
      private bool[] P001A2_n282Menu_Imagem ;
      private String aP1_Menu_Imagem ;
   }

   public class prc_retornaimagemitemmenu__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP001A2 ;
          prmP001A2 = new Object[] {
          new Object[] {"@AV8Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P001A2", "SELECT [Menu_Codigo], [Menu_Imagem_GXI], [Menu_Imagem] FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @AV8Menu_Codigo ORDER BY [Menu_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001A2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getMultimediaUri(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getMultimediaFile(3, rslt.getVarchar(2)) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
