/*
               File: PRC_UPDUsuario_UserGamGuid
        Description: Update Usuario_User Gam Guid
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:21.2
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updusuario_usergamguid : GXProcedure
   {
      public prc_updusuario_usergamguid( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updusuario_usergamguid( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Usuario_Codigo ,
                           String aP1_GamGuid )
      {
         this.A1Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV8GamGuid = aP1_GamGuid;
         initialize();
         executePrivate();
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
      }

      public void executeSubmit( ref int aP0_Usuario_Codigo ,
                                 String aP1_GamGuid )
      {
         prc_updusuario_usergamguid objprc_updusuario_usergamguid;
         objprc_updusuario_usergamguid = new prc_updusuario_usergamguid();
         objprc_updusuario_usergamguid.A1Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_updusuario_usergamguid.AV8GamGuid = aP1_GamGuid;
         objprc_updusuario_usergamguid.context.SetSubmitInitialConfig(context);
         objprc_updusuario_usergamguid.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updusuario_usergamguid);
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updusuario_usergamguid)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00AO2 */
         pr_default.execute(0, new Object[] {A1Usuario_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A341Usuario_UserGamGuid = P00AO2_A341Usuario_UserGamGuid[0];
            A341Usuario_UserGamGuid = AV8GamGuid;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00AO3 */
            pr_default.execute(1, new Object[] {A341Usuario_UserGamGuid, A1Usuario_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("Usuario") ;
            if (true) break;
            /* Using cursor P00AO4 */
            pr_default.execute(2, new Object[] {A341Usuario_UserGamGuid, A1Usuario_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("Usuario") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_UPDUsuario_UserGamGuid");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00AO2_A1Usuario_Codigo = new int[1] ;
         P00AO2_A341Usuario_UserGamGuid = new String[] {""} ;
         A341Usuario_UserGamGuid = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updusuario_usergamguid__default(),
            new Object[][] {
                new Object[] {
               P00AO2_A1Usuario_Codigo, P00AO2_A341Usuario_UserGamGuid
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1Usuario_Codigo ;
      private String AV8GamGuid ;
      private String scmdbuf ;
      private String A341Usuario_UserGamGuid ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Usuario_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00AO2_A1Usuario_Codigo ;
      private String[] P00AO2_A341Usuario_UserGamGuid ;
   }

   public class prc_updusuario_usergamguid__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AO2 ;
          prmP00AO2 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AO3 ;
          prmP00AO3 = new Object[] {
          new Object[] {"@Usuario_UserGamGuid",SqlDbType.Char,40,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AO4 ;
          prmP00AO4 = new Object[] {
          new Object[] {"@Usuario_UserGamGuid",SqlDbType.Char,40,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AO2", "SELECT TOP 1 [Usuario_Codigo], [Usuario_UserGamGuid] FROM [Usuario] WITH (UPDLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ORDER BY [Usuario_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AO2,1,0,true,true )
             ,new CursorDef("P00AO3", "UPDATE [Usuario] SET [Usuario_UserGamGuid]=@Usuario_UserGamGuid  WHERE [Usuario_Codigo] = @Usuario_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AO3)
             ,new CursorDef("P00AO4", "UPDATE [Usuario] SET [Usuario_UserGamGuid]=@Usuario_UserGamGuid  WHERE [Usuario_Codigo] = @Usuario_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AO4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 40) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
