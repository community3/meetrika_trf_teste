/*
               File: NaoConformidade
        Description: N�o Conformidade
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:15:39.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class naoconformidade : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_14") == 0 )
         {
            A428NaoConformidade_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A428NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A428NaoConformidade_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_14( A428NaoConformidade_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_15") == 0 )
         {
            A2025NaoConformidade_TipoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2025NaoConformidade_TipoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2025NaoConformidade_TipoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2025NaoConformidade_TipoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_15( A2025NaoConformidade_TipoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV8NaoConformidade_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8NaoConformidade_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vNAOCONFORMIDADE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8NaoConformidade_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbNaoConformidade_Tipo.Name = "NAOCONFORMIDADE_TIPO";
         cmbNaoConformidade_Tipo.WebTags = "";
         cmbNaoConformidade_Tipo.addItem("1", "Demandas", 0);
         cmbNaoConformidade_Tipo.addItem("2", "Itens da Contagem", 0);
         cmbNaoConformidade_Tipo.addItem("3", "Contagens", 0);
         cmbNaoConformidade_Tipo.addItem("4", "Check List Demandas", 0);
         cmbNaoConformidade_Tipo.addItem("5", "Check List Contagens", 0);
         cmbNaoConformidade_Tipo.addItem("6", "Homologa��o", 0);
         if ( cmbNaoConformidade_Tipo.ItemCount > 0 )
         {
            A429NaoConformidade_Tipo = (short)(NumberUtil.Val( cmbNaoConformidade_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A429NaoConformidade_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0)));
         }
         cmbNaoConformidade_EhImpeditiva.Name = "NAOCONFORMIDADE_EHIMPEDITIVA";
         cmbNaoConformidade_EhImpeditiva.WebTags = "";
         cmbNaoConformidade_EhImpeditiva.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbNaoConformidade_EhImpeditiva.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbNaoConformidade_EhImpeditiva.ItemCount > 0 )
         {
            A1121NaoConformidade_EhImpeditiva = StringUtil.StrToBool( cmbNaoConformidade_EhImpeditiva.getValidValue(StringUtil.BoolToStr( A1121NaoConformidade_EhImpeditiva)));
            n1121NaoConformidade_EhImpeditiva = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1121NaoConformidade_EhImpeditiva", A1121NaoConformidade_EhImpeditiva);
         }
         chkNaoConformidade_Glosavel.Name = "NAOCONFORMIDADE_GLOSAVEL";
         chkNaoConformidade_Glosavel.WebTags = "";
         chkNaoConformidade_Glosavel.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkNaoConformidade_Glosavel_Internalname, "TitleCaption", chkNaoConformidade_Glosavel.Caption);
         chkNaoConformidade_Glosavel.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "N�o Conformidade", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtNaoConformidade_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public naoconformidade( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public naoconformidade( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_NaoConformidade_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV8NaoConformidade_Codigo = aP1_NaoConformidade_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbNaoConformidade_Tipo = new GXCombobox();
         cmbNaoConformidade_EhImpeditiva = new GXCombobox();
         chkNaoConformidade_Glosavel = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbNaoConformidade_Tipo.ItemCount > 0 )
         {
            A429NaoConformidade_Tipo = (short)(NumberUtil.Val( cmbNaoConformidade_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A429NaoConformidade_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0)));
         }
         if ( cmbNaoConformidade_EhImpeditiva.ItemCount > 0 )
         {
            A1121NaoConformidade_EhImpeditiva = StringUtil.StrToBool( cmbNaoConformidade_EhImpeditiva.getValidValue(StringUtil.BoolToStr( A1121NaoConformidade_EhImpeditiva)));
            n1121NaoConformidade_EhImpeditiva = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1121NaoConformidade_EhImpeditiva", A1121NaoConformidade_EhImpeditiva);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1Q65( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1Q65e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtNaoConformidade_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A426NaoConformidade_Codigo), 6, 0, ",", "")), ((edtNaoConformidade_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A426NaoConformidade_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A426NaoConformidade_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtNaoConformidade_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtNaoConformidade_Codigo_Visible, edtNaoConformidade_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_NaoConformidade.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1Q65( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1Q65( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1Q65e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_36_1Q65( true) ;
         }
         return  ;
      }

      protected void wb_table3_36_1Q65e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1Q65e( true) ;
         }
         else
         {
            wb_table1_2_1Q65e( false) ;
         }
      }

      protected void wb_table3_36_1Q65( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_NaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_NaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_NaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_36_1Q65e( true) ;
         }
         else
         {
            wb_table3_36_1Q65e( false) ;
         }
      }

      protected void wb_table2_5_1Q65( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1Q65( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1Q65e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1Q65e( true) ;
         }
         else
         {
            wb_table2_5_1Q65e( false) ;
         }
      }

      protected void wb_table4_13_1Q65( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknaoconformidade_nome_Internalname, "Nome", "", "", lblTextblocknaoconformidade_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtNaoConformidade_Nome_Internalname, StringUtil.RTrim( A427NaoConformidade_Nome), StringUtil.RTrim( context.localUtil.Format( A427NaoConformidade_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtNaoConformidade_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtNaoConformidade_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_NaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknaoconformidade_tipo_Internalname, "Tipo", "", "", lblTextblocknaoconformidade_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbNaoConformidade_Tipo, cmbNaoConformidade_Tipo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0)), 1, cmbNaoConformidade_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbNaoConformidade_Tipo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", true, "HLP_NaoConformidade.htm");
            cmbNaoConformidade_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbNaoConformidade_Tipo_Internalname, "Values", (String)(cmbNaoConformidade_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknaoconformidade_ehimpeditiva_Internalname, "� impeditiva?", "", "", lblTextblocknaoconformidade_ehimpeditiva_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbNaoConformidade_EhImpeditiva, cmbNaoConformidade_EhImpeditiva_Internalname, StringUtil.BoolToStr( A1121NaoConformidade_EhImpeditiva), 1, cmbNaoConformidade_EhImpeditiva_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbNaoConformidade_EhImpeditiva.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_NaoConformidade.htm");
            cmbNaoConformidade_EhImpeditiva.CurrentValue = StringUtil.BoolToStr( A1121NaoConformidade_EhImpeditiva);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbNaoConformidade_EhImpeditiva_Internalname, "Values", (String)(cmbNaoConformidade_EhImpeditiva.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknaoconformidade_glosavel_Internalname, "Glos�vel", "", "", lblTextblocknaoconformidade_glosavel_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkNaoConformidade_Glosavel_Internalname, StringUtil.BoolToStr( A2029NaoConformidade_Glosavel), "", "", 1, chkNaoConformidade_Glosavel.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(33, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1Q65e( true) ;
         }
         else
         {
            wb_table4_13_1Q65e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111Q2 */
         E111Q2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A427NaoConformidade_Nome = StringUtil.Upper( cgiGet( edtNaoConformidade_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A427NaoConformidade_Nome", A427NaoConformidade_Nome);
               cmbNaoConformidade_Tipo.CurrentValue = cgiGet( cmbNaoConformidade_Tipo_Internalname);
               A429NaoConformidade_Tipo = (short)(NumberUtil.Val( cgiGet( cmbNaoConformidade_Tipo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A429NaoConformidade_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0)));
               cmbNaoConformidade_EhImpeditiva.CurrentValue = cgiGet( cmbNaoConformidade_EhImpeditiva_Internalname);
               A1121NaoConformidade_EhImpeditiva = StringUtil.StrToBool( cgiGet( cmbNaoConformidade_EhImpeditiva_Internalname));
               n1121NaoConformidade_EhImpeditiva = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1121NaoConformidade_EhImpeditiva", A1121NaoConformidade_EhImpeditiva);
               n1121NaoConformidade_EhImpeditiva = ((false==A1121NaoConformidade_EhImpeditiva) ? true : false);
               A2029NaoConformidade_Glosavel = StringUtil.StrToBool( cgiGet( chkNaoConformidade_Glosavel_Internalname));
               n2029NaoConformidade_Glosavel = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2029NaoConformidade_Glosavel", A2029NaoConformidade_Glosavel);
               n2029NaoConformidade_Glosavel = ((false==A2029NaoConformidade_Glosavel) ? true : false);
               A426NaoConformidade_Codigo = (int)(context.localUtil.CToN( cgiGet( edtNaoConformidade_Codigo_Internalname), ",", "."));
               n426NaoConformidade_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
               /* Read saved values. */
               Z426NaoConformidade_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z426NaoConformidade_Codigo"), ",", "."));
               Z427NaoConformidade_Nome = cgiGet( "Z427NaoConformidade_Nome");
               Z429NaoConformidade_Tipo = (short)(context.localUtil.CToN( cgiGet( "Z429NaoConformidade_Tipo"), ",", "."));
               Z1121NaoConformidade_EhImpeditiva = StringUtil.StrToBool( cgiGet( "Z1121NaoConformidade_EhImpeditiva"));
               n1121NaoConformidade_EhImpeditiva = ((false==A1121NaoConformidade_EhImpeditiva) ? true : false);
               Z2029NaoConformidade_Glosavel = StringUtil.StrToBool( cgiGet( "Z2029NaoConformidade_Glosavel"));
               n2029NaoConformidade_Glosavel = ((false==A2029NaoConformidade_Glosavel) ? true : false);
               Z428NaoConformidade_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z428NaoConformidade_AreaTrabalhoCod"), ",", "."));
               Z2025NaoConformidade_TipoCod = (int)(context.localUtil.CToN( cgiGet( "Z2025NaoConformidade_TipoCod"), ",", "."));
               n2025NaoConformidade_TipoCod = ((0==A2025NaoConformidade_TipoCod) ? true : false);
               A428NaoConformidade_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z428NaoConformidade_AreaTrabalhoCod"), ",", "."));
               A2025NaoConformidade_TipoCod = (int)(context.localUtil.CToN( cgiGet( "Z2025NaoConformidade_TipoCod"), ",", "."));
               n2025NaoConformidade_TipoCod = false;
               n2025NaoConformidade_TipoCod = ((0==A2025NaoConformidade_TipoCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N428NaoConformidade_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N428NaoConformidade_AreaTrabalhoCod"), ",", "."));
               N2025NaoConformidade_TipoCod = (int)(context.localUtil.CToN( cgiGet( "N2025NaoConformidade_TipoCod"), ",", "."));
               n2025NaoConformidade_TipoCod = ((0==A2025NaoConformidade_TipoCod) ? true : false);
               AV8NaoConformidade_Codigo = (int)(context.localUtil.CToN( cgiGet( "vNAOCONFORMIDADE_CODIGO"), ",", "."));
               AV11Insert_NaoConformidade_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_NAOCONFORMIDADE_AREATRABALHOCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A428NaoConformidade_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "NAOCONFORMIDADE_AREATRABALHOCOD"), ",", "."));
               AV13Insert_NaoConformidade_TipoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_NAOCONFORMIDADE_TIPOCOD"), ",", "."));
               A2025NaoConformidade_TipoCod = (int)(context.localUtil.CToN( cgiGet( "NAOCONFORMIDADE_TIPOCOD"), ",", "."));
               n2025NaoConformidade_TipoCod = ((0==A2025NaoConformidade_TipoCod) ? true : false);
               AV15Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "NaoConformidade";
               A426NaoConformidade_Codigo = (int)(context.localUtil.CToN( cgiGet( edtNaoConformidade_Codigo_Internalname), ",", "."));
               n426NaoConformidade_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A426NaoConformidade_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A428NaoConformidade_AreaTrabalhoCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2025NaoConformidade_TipoCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A426NaoConformidade_Codigo != Z426NaoConformidade_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("naoconformidade:[SecurityCheckFailed value for]"+"NaoConformidade_Codigo:"+context.localUtil.Format( (decimal)(A426NaoConformidade_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("naoconformidade:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("naoconformidade:[SecurityCheckFailed value for]"+"NaoConformidade_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A428NaoConformidade_AreaTrabalhoCod), "ZZZZZ9"));
                  GXUtil.WriteLog("naoconformidade:[SecurityCheckFailed value for]"+"NaoConformidade_TipoCod:"+context.localUtil.Format( (decimal)(A2025NaoConformidade_TipoCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A426NaoConformidade_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n426NaoConformidade_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode65 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode65;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound65 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_1Q0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "NAOCONFORMIDADE_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtNaoConformidade_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111Q2 */
                           E111Q2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121Q2 */
                           E121Q2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E121Q2 */
            E121Q2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1Q65( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1Q65( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_1Q0( )
      {
         BeforeValidate1Q65( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1Q65( ) ;
            }
            else
            {
               CheckExtendedTable1Q65( ) ;
               CloseExtendedTableCursors1Q65( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption1Q0( )
      {
      }

      protected void E111Q2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV15Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV16GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            while ( AV16GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV16GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "NaoConformidade_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_NaoConformidade_AreaTrabalhoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_NaoConformidade_AreaTrabalhoCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "NaoConformidade_TipoCod") == 0 )
               {
                  AV13Insert_NaoConformidade_TipoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_NaoConformidade_TipoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_NaoConformidade_TipoCod), 6, 0)));
               }
               AV16GXV1 = (int)(AV16GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            }
         }
         edtNaoConformidade_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNaoConformidade_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtNaoConformidade_Codigo_Visible), 5, 0)));
      }

      protected void E121Q2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwnaoconformidade.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM1Q65( short GX_JID )
      {
         if ( ( GX_JID == 13 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z427NaoConformidade_Nome = T001Q3_A427NaoConformidade_Nome[0];
               Z429NaoConformidade_Tipo = T001Q3_A429NaoConformidade_Tipo[0];
               Z1121NaoConformidade_EhImpeditiva = T001Q3_A1121NaoConformidade_EhImpeditiva[0];
               Z2029NaoConformidade_Glosavel = T001Q3_A2029NaoConformidade_Glosavel[0];
               Z428NaoConformidade_AreaTrabalhoCod = T001Q3_A428NaoConformidade_AreaTrabalhoCod[0];
               Z2025NaoConformidade_TipoCod = T001Q3_A2025NaoConformidade_TipoCod[0];
            }
            else
            {
               Z427NaoConformidade_Nome = A427NaoConformidade_Nome;
               Z429NaoConformidade_Tipo = A429NaoConformidade_Tipo;
               Z1121NaoConformidade_EhImpeditiva = A1121NaoConformidade_EhImpeditiva;
               Z2029NaoConformidade_Glosavel = A2029NaoConformidade_Glosavel;
               Z428NaoConformidade_AreaTrabalhoCod = A428NaoConformidade_AreaTrabalhoCod;
               Z2025NaoConformidade_TipoCod = A2025NaoConformidade_TipoCod;
            }
         }
         if ( GX_JID == -13 )
         {
            Z426NaoConformidade_Codigo = A426NaoConformidade_Codigo;
            Z427NaoConformidade_Nome = A427NaoConformidade_Nome;
            Z429NaoConformidade_Tipo = A429NaoConformidade_Tipo;
            Z1121NaoConformidade_EhImpeditiva = A1121NaoConformidade_EhImpeditiva;
            Z2029NaoConformidade_Glosavel = A2029NaoConformidade_Glosavel;
            Z428NaoConformidade_AreaTrabalhoCod = A428NaoConformidade_AreaTrabalhoCod;
            Z2025NaoConformidade_TipoCod = A2025NaoConformidade_TipoCod;
         }
      }

      protected void standaloneNotModal( )
      {
         edtNaoConformidade_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNaoConformidade_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtNaoConformidade_Codigo_Enabled), 5, 0)));
         AV15Pgmname = "NaoConformidade";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Pgmname", AV15Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtNaoConformidade_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNaoConformidade_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtNaoConformidade_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV8NaoConformidade_Codigo) )
         {
            A426NaoConformidade_Codigo = AV8NaoConformidade_Codigo;
            n426NaoConformidade_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_NaoConformidade_TipoCod) )
         {
            A2025NaoConformidade_TipoCod = AV13Insert_NaoConformidade_TipoCod;
            n2025NaoConformidade_TipoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2025NaoConformidade_TipoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2025NaoConformidade_TipoCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_NaoConformidade_AreaTrabalhoCod) )
         {
            A428NaoConformidade_AreaTrabalhoCod = AV11Insert_NaoConformidade_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A428NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A428NaoConformidade_AreaTrabalhoCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A428NaoConformidade_AreaTrabalhoCod) && ( Gx_BScreen == 0 ) )
            {
               A428NaoConformidade_AreaTrabalhoCod = AV7WWPContext.gxTpr_Areatrabalho_codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A428NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A428NaoConformidade_AreaTrabalhoCod), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A2029NaoConformidade_Glosavel) && ( Gx_BScreen == 0 ) )
         {
            A2029NaoConformidade_Glosavel = false;
            n2029NaoConformidade_Glosavel = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2029NaoConformidade_Glosavel", A2029NaoConformidade_Glosavel);
         }
      }

      protected void Load1Q65( )
      {
         /* Using cursor T001Q6 */
         pr_default.execute(4, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound65 = 1;
            A427NaoConformidade_Nome = T001Q6_A427NaoConformidade_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A427NaoConformidade_Nome", A427NaoConformidade_Nome);
            A429NaoConformidade_Tipo = T001Q6_A429NaoConformidade_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A429NaoConformidade_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0)));
            A1121NaoConformidade_EhImpeditiva = T001Q6_A1121NaoConformidade_EhImpeditiva[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1121NaoConformidade_EhImpeditiva", A1121NaoConformidade_EhImpeditiva);
            n1121NaoConformidade_EhImpeditiva = T001Q6_n1121NaoConformidade_EhImpeditiva[0];
            A2029NaoConformidade_Glosavel = T001Q6_A2029NaoConformidade_Glosavel[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2029NaoConformidade_Glosavel", A2029NaoConformidade_Glosavel);
            n2029NaoConformidade_Glosavel = T001Q6_n2029NaoConformidade_Glosavel[0];
            A428NaoConformidade_AreaTrabalhoCod = T001Q6_A428NaoConformidade_AreaTrabalhoCod[0];
            A2025NaoConformidade_TipoCod = T001Q6_A2025NaoConformidade_TipoCod[0];
            n2025NaoConformidade_TipoCod = T001Q6_n2025NaoConformidade_TipoCod[0];
            ZM1Q65( -13) ;
         }
         pr_default.close(4);
         OnLoadActions1Q65( ) ;
      }

      protected void OnLoadActions1Q65( )
      {
      }

      protected void CheckExtendedTable1Q65( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A427NaoConformidade_Nome)) )
         {
            GX_msglist.addItem("N�o Conformidade � obrigat�rio.", 1, "NAOCONFORMIDADE_NOME");
            AnyError = 1;
            GX_FocusControl = edtNaoConformidade_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( A429NaoConformidade_Tipo == 1 ) || ( A429NaoConformidade_Tipo == 2 ) || ( A429NaoConformidade_Tipo == 3 ) || ( A429NaoConformidade_Tipo == 4 ) || ( A429NaoConformidade_Tipo == 5 ) || ( A429NaoConformidade_Tipo == 6 ) ) )
         {
            GX_msglist.addItem("Campo Tipo fora do intervalo", "OutOfRange", 1, "NAOCONFORMIDADE_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbNaoConformidade_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (0==A429NaoConformidade_Tipo) )
         {
            GX_msglist.addItem("Tipo � obrigat�rio.", 1, "NAOCONFORMIDADE_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbNaoConformidade_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T001Q4 */
         pr_default.execute(2, new Object[] {A428NaoConformidade_AreaTrabalhoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Nao Conformidade_Area de Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor T001Q5 */
         pr_default.execute(3, new Object[] {n2025NaoConformidade_TipoCod, A2025NaoConformidade_TipoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A2025NaoConformidade_TipoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Nao Conformidade_Tipo Cod'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors1Q65( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_14( int A428NaoConformidade_AreaTrabalhoCod )
      {
         /* Using cursor T001Q7 */
         pr_default.execute(5, new Object[] {A428NaoConformidade_AreaTrabalhoCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Nao Conformidade_Area de Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_15( int A2025NaoConformidade_TipoCod )
      {
         /* Using cursor T001Q8 */
         pr_default.execute(6, new Object[] {n2025NaoConformidade_TipoCod, A2025NaoConformidade_TipoCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A2025NaoConformidade_TipoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Nao Conformidade_Tipo Cod'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey1Q65( )
      {
         /* Using cursor T001Q9 */
         pr_default.execute(7, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound65 = 1;
         }
         else
         {
            RcdFound65 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001Q3 */
         pr_default.execute(1, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1Q65( 13) ;
            RcdFound65 = 1;
            A426NaoConformidade_Codigo = T001Q3_A426NaoConformidade_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
            n426NaoConformidade_Codigo = T001Q3_n426NaoConformidade_Codigo[0];
            A427NaoConformidade_Nome = T001Q3_A427NaoConformidade_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A427NaoConformidade_Nome", A427NaoConformidade_Nome);
            A429NaoConformidade_Tipo = T001Q3_A429NaoConformidade_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A429NaoConformidade_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0)));
            A1121NaoConformidade_EhImpeditiva = T001Q3_A1121NaoConformidade_EhImpeditiva[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1121NaoConformidade_EhImpeditiva", A1121NaoConformidade_EhImpeditiva);
            n1121NaoConformidade_EhImpeditiva = T001Q3_n1121NaoConformidade_EhImpeditiva[0];
            A2029NaoConformidade_Glosavel = T001Q3_A2029NaoConformidade_Glosavel[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2029NaoConformidade_Glosavel", A2029NaoConformidade_Glosavel);
            n2029NaoConformidade_Glosavel = T001Q3_n2029NaoConformidade_Glosavel[0];
            A428NaoConformidade_AreaTrabalhoCod = T001Q3_A428NaoConformidade_AreaTrabalhoCod[0];
            A2025NaoConformidade_TipoCod = T001Q3_A2025NaoConformidade_TipoCod[0];
            n2025NaoConformidade_TipoCod = T001Q3_n2025NaoConformidade_TipoCod[0];
            Z426NaoConformidade_Codigo = A426NaoConformidade_Codigo;
            sMode65 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1Q65( ) ;
            if ( AnyError == 1 )
            {
               RcdFound65 = 0;
               InitializeNonKey1Q65( ) ;
            }
            Gx_mode = sMode65;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound65 = 0;
            InitializeNonKey1Q65( ) ;
            sMode65 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode65;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1Q65( ) ;
         if ( RcdFound65 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound65 = 0;
         /* Using cursor T001Q10 */
         pr_default.execute(8, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T001Q10_A426NaoConformidade_Codigo[0] < A426NaoConformidade_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T001Q10_A426NaoConformidade_Codigo[0] > A426NaoConformidade_Codigo ) ) )
            {
               A426NaoConformidade_Codigo = T001Q10_A426NaoConformidade_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
               n426NaoConformidade_Codigo = T001Q10_n426NaoConformidade_Codigo[0];
               RcdFound65 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound65 = 0;
         /* Using cursor T001Q11 */
         pr_default.execute(9, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T001Q11_A426NaoConformidade_Codigo[0] > A426NaoConformidade_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T001Q11_A426NaoConformidade_Codigo[0] < A426NaoConformidade_Codigo ) ) )
            {
               A426NaoConformidade_Codigo = T001Q11_A426NaoConformidade_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
               n426NaoConformidade_Codigo = T001Q11_n426NaoConformidade_Codigo[0];
               RcdFound65 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1Q65( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtNaoConformidade_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1Q65( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound65 == 1 )
            {
               if ( A426NaoConformidade_Codigo != Z426NaoConformidade_Codigo )
               {
                  A426NaoConformidade_Codigo = Z426NaoConformidade_Codigo;
                  n426NaoConformidade_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "NAOCONFORMIDADE_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtNaoConformidade_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtNaoConformidade_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1Q65( ) ;
                  GX_FocusControl = edtNaoConformidade_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A426NaoConformidade_Codigo != Z426NaoConformidade_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtNaoConformidade_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1Q65( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "NAOCONFORMIDADE_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtNaoConformidade_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtNaoConformidade_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1Q65( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A426NaoConformidade_Codigo != Z426NaoConformidade_Codigo )
         {
            A426NaoConformidade_Codigo = Z426NaoConformidade_Codigo;
            n426NaoConformidade_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "NAOCONFORMIDADE_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtNaoConformidade_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtNaoConformidade_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1Q65( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001Q2 */
            pr_default.execute(0, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"NaoConformidade"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z427NaoConformidade_Nome, T001Q2_A427NaoConformidade_Nome[0]) != 0 ) || ( Z429NaoConformidade_Tipo != T001Q2_A429NaoConformidade_Tipo[0] ) || ( Z1121NaoConformidade_EhImpeditiva != T001Q2_A1121NaoConformidade_EhImpeditiva[0] ) || ( Z2029NaoConformidade_Glosavel != T001Q2_A2029NaoConformidade_Glosavel[0] ) || ( Z428NaoConformidade_AreaTrabalhoCod != T001Q2_A428NaoConformidade_AreaTrabalhoCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2025NaoConformidade_TipoCod != T001Q2_A2025NaoConformidade_TipoCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z427NaoConformidade_Nome, T001Q2_A427NaoConformidade_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("naoconformidade:[seudo value changed for attri]"+"NaoConformidade_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z427NaoConformidade_Nome);
                  GXUtil.WriteLogRaw("Current: ",T001Q2_A427NaoConformidade_Nome[0]);
               }
               if ( Z429NaoConformidade_Tipo != T001Q2_A429NaoConformidade_Tipo[0] )
               {
                  GXUtil.WriteLog("naoconformidade:[seudo value changed for attri]"+"NaoConformidade_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z429NaoConformidade_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T001Q2_A429NaoConformidade_Tipo[0]);
               }
               if ( Z1121NaoConformidade_EhImpeditiva != T001Q2_A1121NaoConformidade_EhImpeditiva[0] )
               {
                  GXUtil.WriteLog("naoconformidade:[seudo value changed for attri]"+"NaoConformidade_EhImpeditiva");
                  GXUtil.WriteLogRaw("Old: ",Z1121NaoConformidade_EhImpeditiva);
                  GXUtil.WriteLogRaw("Current: ",T001Q2_A1121NaoConformidade_EhImpeditiva[0]);
               }
               if ( Z2029NaoConformidade_Glosavel != T001Q2_A2029NaoConformidade_Glosavel[0] )
               {
                  GXUtil.WriteLog("naoconformidade:[seudo value changed for attri]"+"NaoConformidade_Glosavel");
                  GXUtil.WriteLogRaw("Old: ",Z2029NaoConformidade_Glosavel);
                  GXUtil.WriteLogRaw("Current: ",T001Q2_A2029NaoConformidade_Glosavel[0]);
               }
               if ( Z428NaoConformidade_AreaTrabalhoCod != T001Q2_A428NaoConformidade_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("naoconformidade:[seudo value changed for attri]"+"NaoConformidade_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z428NaoConformidade_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T001Q2_A428NaoConformidade_AreaTrabalhoCod[0]);
               }
               if ( Z2025NaoConformidade_TipoCod != T001Q2_A2025NaoConformidade_TipoCod[0] )
               {
                  GXUtil.WriteLog("naoconformidade:[seudo value changed for attri]"+"NaoConformidade_TipoCod");
                  GXUtil.WriteLogRaw("Old: ",Z2025NaoConformidade_TipoCod);
                  GXUtil.WriteLogRaw("Current: ",T001Q2_A2025NaoConformidade_TipoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"NaoConformidade"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1Q65( )
      {
         BeforeValidate1Q65( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1Q65( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1Q65( 0) ;
            CheckOptimisticConcurrency1Q65( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1Q65( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1Q65( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001Q12 */
                     pr_default.execute(10, new Object[] {A427NaoConformidade_Nome, A429NaoConformidade_Tipo, n1121NaoConformidade_EhImpeditiva, A1121NaoConformidade_EhImpeditiva, n2029NaoConformidade_Glosavel, A2029NaoConformidade_Glosavel, A428NaoConformidade_AreaTrabalhoCod, n2025NaoConformidade_TipoCod, A2025NaoConformidade_TipoCod});
                     A426NaoConformidade_Codigo = T001Q12_A426NaoConformidade_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
                     n426NaoConformidade_Codigo = T001Q12_n426NaoConformidade_Codigo[0];
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("NaoConformidade") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1Q0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1Q65( ) ;
            }
            EndLevel1Q65( ) ;
         }
         CloseExtendedTableCursors1Q65( ) ;
      }

      protected void Update1Q65( )
      {
         BeforeValidate1Q65( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1Q65( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1Q65( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1Q65( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1Q65( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001Q13 */
                     pr_default.execute(11, new Object[] {A427NaoConformidade_Nome, A429NaoConformidade_Tipo, n1121NaoConformidade_EhImpeditiva, A1121NaoConformidade_EhImpeditiva, n2029NaoConformidade_Glosavel, A2029NaoConformidade_Glosavel, A428NaoConformidade_AreaTrabalhoCod, n2025NaoConformidade_TipoCod, A2025NaoConformidade_TipoCod, n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("NaoConformidade") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"NaoConformidade"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1Q65( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1Q65( ) ;
         }
         CloseExtendedTableCursors1Q65( ) ;
      }

      protected void DeferredUpdate1Q65( )
      {
      }

      protected void delete( )
      {
         BeforeValidate1Q65( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1Q65( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1Q65( ) ;
            AfterConfirm1Q65( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1Q65( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001Q14 */
                  pr_default.execute(12, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("NaoConformidade") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode65 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1Q65( ) ;
         Gx_mode = sMode65;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1Q65( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T001Q15 */
            pr_default.execute(13, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Indicador"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
            /* Using cursor T001Q16 */
            pr_default.execute(14, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Ocorr�ncia"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
            /* Using cursor T001Q17 */
            pr_default.execute(15, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
            if ( (pr_default.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Nao Cnf"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(15);
            /* Using cursor T001Q18 */
            pr_default.execute(16, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
            if ( (pr_default.getStatus(16) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(16);
            /* Using cursor T001Q19 */
            pr_default.execute(17, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
            if ( (pr_default.getStatus(17) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(17);
            /* Using cursor T001Q20 */
            pr_default.execute(18, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
            if ( (pr_default.getStatus(18) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Chck Lst Log"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(18);
         }
      }

      protected void EndLevel1Q65( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1Q65( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "NaoConformidade");
            if ( AnyError == 0 )
            {
               ConfirmValues1Q0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "NaoConformidade");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1Q65( )
      {
         /* Scan By routine */
         /* Using cursor T001Q21 */
         pr_default.execute(19);
         RcdFound65 = 0;
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound65 = 1;
            A426NaoConformidade_Codigo = T001Q21_A426NaoConformidade_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
            n426NaoConformidade_Codigo = T001Q21_n426NaoConformidade_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1Q65( )
      {
         /* Scan next routine */
         pr_default.readNext(19);
         RcdFound65 = 0;
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound65 = 1;
            A426NaoConformidade_Codigo = T001Q21_A426NaoConformidade_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
            n426NaoConformidade_Codigo = T001Q21_n426NaoConformidade_Codigo[0];
         }
      }

      protected void ScanEnd1Q65( )
      {
         pr_default.close(19);
      }

      protected void AfterConfirm1Q65( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1Q65( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1Q65( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1Q65( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1Q65( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1Q65( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1Q65( )
      {
         edtNaoConformidade_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNaoConformidade_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtNaoConformidade_Nome_Enabled), 5, 0)));
         cmbNaoConformidade_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbNaoConformidade_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbNaoConformidade_Tipo.Enabled), 5, 0)));
         cmbNaoConformidade_EhImpeditiva.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbNaoConformidade_EhImpeditiva_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbNaoConformidade_EhImpeditiva.Enabled), 5, 0)));
         chkNaoConformidade_Glosavel.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkNaoConformidade_Glosavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkNaoConformidade_Glosavel.Enabled), 5, 0)));
         edtNaoConformidade_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNaoConformidade_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtNaoConformidade_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1Q0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216154041");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("naoconformidade.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV8NaoConformidade_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z426NaoConformidade_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z427NaoConformidade_Nome", StringUtil.RTrim( Z427NaoConformidade_Nome));
         GxWebStd.gx_hidden_field( context, "Z429NaoConformidade_Tipo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z429NaoConformidade_Tipo), 2, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1121NaoConformidade_EhImpeditiva", Z1121NaoConformidade_EhImpeditiva);
         GxWebStd.gx_boolean_hidden_field( context, "Z2029NaoConformidade_Glosavel", Z2029NaoConformidade_Glosavel);
         GxWebStd.gx_hidden_field( context, "Z428NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z428NaoConformidade_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2025NaoConformidade_TipoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2025NaoConformidade_TipoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N428NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A428NaoConformidade_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N2025NaoConformidade_TipoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2025NaoConformidade_TipoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vNAOCONFORMIDADE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8NaoConformidade_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_NAOCONFORMIDADE_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_NaoConformidade_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "NAOCONFORMIDADE_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A428NaoConformidade_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_NAOCONFORMIDADE_TIPOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_NaoConformidade_TipoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "NAOCONFORMIDADE_TIPOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2025NaoConformidade_TipoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV15Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vNAOCONFORMIDADE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8NaoConformidade_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "NaoConformidade";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A426NaoConformidade_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A428NaoConformidade_AreaTrabalhoCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2025NaoConformidade_TipoCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("naoconformidade:[SendSecurityCheck value for]"+"NaoConformidade_Codigo:"+context.localUtil.Format( (decimal)(A426NaoConformidade_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("naoconformidade:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("naoconformidade:[SendSecurityCheck value for]"+"NaoConformidade_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A428NaoConformidade_AreaTrabalhoCod), "ZZZZZ9"));
         GXUtil.WriteLog("naoconformidade:[SendSecurityCheck value for]"+"NaoConformidade_TipoCod:"+context.localUtil.Format( (decimal)(A2025NaoConformidade_TipoCod), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("naoconformidade.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV8NaoConformidade_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "NaoConformidade" ;
      }

      public override String GetPgmdesc( )
      {
         return "N�o Conformidade" ;
      }

      protected void InitializeNonKey1Q65( )
      {
         A2025NaoConformidade_TipoCod = 0;
         n2025NaoConformidade_TipoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2025NaoConformidade_TipoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2025NaoConformidade_TipoCod), 6, 0)));
         A427NaoConformidade_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A427NaoConformidade_Nome", A427NaoConformidade_Nome);
         A429NaoConformidade_Tipo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A429NaoConformidade_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0)));
         A1121NaoConformidade_EhImpeditiva = false;
         n1121NaoConformidade_EhImpeditiva = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1121NaoConformidade_EhImpeditiva", A1121NaoConformidade_EhImpeditiva);
         n1121NaoConformidade_EhImpeditiva = ((false==A1121NaoConformidade_EhImpeditiva) ? true : false);
         A428NaoConformidade_AreaTrabalhoCod = AV7WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A428NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A428NaoConformidade_AreaTrabalhoCod), 6, 0)));
         A2029NaoConformidade_Glosavel = false;
         n2029NaoConformidade_Glosavel = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2029NaoConformidade_Glosavel", A2029NaoConformidade_Glosavel);
         Z427NaoConformidade_Nome = "";
         Z429NaoConformidade_Tipo = 0;
         Z1121NaoConformidade_EhImpeditiva = false;
         Z2029NaoConformidade_Glosavel = false;
         Z428NaoConformidade_AreaTrabalhoCod = 0;
         Z2025NaoConformidade_TipoCod = 0;
      }

      protected void InitAll1Q65( )
      {
         A426NaoConformidade_Codigo = 0;
         n426NaoConformidade_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
         InitializeNonKey1Q65( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A428NaoConformidade_AreaTrabalhoCod = i428NaoConformidade_AreaTrabalhoCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A428NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A428NaoConformidade_AreaTrabalhoCod), 6, 0)));
         A2029NaoConformidade_Glosavel = i2029NaoConformidade_Glosavel;
         n2029NaoConformidade_Glosavel = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2029NaoConformidade_Glosavel", A2029NaoConformidade_Glosavel);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216154061");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("naoconformidade.js", "?20206216154061");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocknaoconformidade_nome_Internalname = "TEXTBLOCKNAOCONFORMIDADE_NOME";
         edtNaoConformidade_Nome_Internalname = "NAOCONFORMIDADE_NOME";
         lblTextblocknaoconformidade_tipo_Internalname = "TEXTBLOCKNAOCONFORMIDADE_TIPO";
         cmbNaoConformidade_Tipo_Internalname = "NAOCONFORMIDADE_TIPO";
         lblTextblocknaoconformidade_ehimpeditiva_Internalname = "TEXTBLOCKNAOCONFORMIDADE_EHIMPEDITIVA";
         cmbNaoConformidade_EhImpeditiva_Internalname = "NAOCONFORMIDADE_EHIMPEDITIVA";
         lblTextblocknaoconformidade_glosavel_Internalname = "TEXTBLOCKNAOCONFORMIDADE_GLOSAVEL";
         chkNaoConformidade_Glosavel_Internalname = "NAOCONFORMIDADE_GLOSAVEL";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtNaoConformidade_Codigo_Internalname = "NAOCONFORMIDADE_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "N�o Conformidade";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "N�o Conformidade";
         chkNaoConformidade_Glosavel.Enabled = 1;
         cmbNaoConformidade_EhImpeditiva_Jsonclick = "";
         cmbNaoConformidade_EhImpeditiva.Enabled = 1;
         cmbNaoConformidade_Tipo_Jsonclick = "";
         cmbNaoConformidade_Tipo.Enabled = 1;
         edtNaoConformidade_Nome_Jsonclick = "";
         edtNaoConformidade_Nome_Enabled = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtNaoConformidade_Codigo_Jsonclick = "";
         edtNaoConformidade_Codigo_Enabled = 0;
         edtNaoConformidade_Codigo_Visible = 1;
         chkNaoConformidade_Glosavel.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV8NaoConformidade_Codigo',fld:'vNAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E121Q2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z427NaoConformidade_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTextblocknaoconformidade_nome_Jsonclick = "";
         A427NaoConformidade_Nome = "";
         lblTextblocknaoconformidade_tipo_Jsonclick = "";
         lblTextblocknaoconformidade_ehimpeditiva_Jsonclick = "";
         lblTextblocknaoconformidade_glosavel_Jsonclick = "";
         AV15Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode65 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV7WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         T001Q6_A426NaoConformidade_Codigo = new int[1] ;
         T001Q6_n426NaoConformidade_Codigo = new bool[] {false} ;
         T001Q6_A427NaoConformidade_Nome = new String[] {""} ;
         T001Q6_A429NaoConformidade_Tipo = new short[1] ;
         T001Q6_A1121NaoConformidade_EhImpeditiva = new bool[] {false} ;
         T001Q6_n1121NaoConformidade_EhImpeditiva = new bool[] {false} ;
         T001Q6_A2029NaoConformidade_Glosavel = new bool[] {false} ;
         T001Q6_n2029NaoConformidade_Glosavel = new bool[] {false} ;
         T001Q6_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         T001Q6_A2025NaoConformidade_TipoCod = new int[1] ;
         T001Q6_n2025NaoConformidade_TipoCod = new bool[] {false} ;
         T001Q4_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         T001Q5_A2025NaoConformidade_TipoCod = new int[1] ;
         T001Q5_n2025NaoConformidade_TipoCod = new bool[] {false} ;
         T001Q7_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         T001Q8_A2025NaoConformidade_TipoCod = new int[1] ;
         T001Q8_n2025NaoConformidade_TipoCod = new bool[] {false} ;
         T001Q9_A426NaoConformidade_Codigo = new int[1] ;
         T001Q9_n426NaoConformidade_Codigo = new bool[] {false} ;
         T001Q3_A426NaoConformidade_Codigo = new int[1] ;
         T001Q3_n426NaoConformidade_Codigo = new bool[] {false} ;
         T001Q3_A427NaoConformidade_Nome = new String[] {""} ;
         T001Q3_A429NaoConformidade_Tipo = new short[1] ;
         T001Q3_A1121NaoConformidade_EhImpeditiva = new bool[] {false} ;
         T001Q3_n1121NaoConformidade_EhImpeditiva = new bool[] {false} ;
         T001Q3_A2029NaoConformidade_Glosavel = new bool[] {false} ;
         T001Q3_n2029NaoConformidade_Glosavel = new bool[] {false} ;
         T001Q3_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         T001Q3_A2025NaoConformidade_TipoCod = new int[1] ;
         T001Q3_n2025NaoConformidade_TipoCod = new bool[] {false} ;
         T001Q10_A426NaoConformidade_Codigo = new int[1] ;
         T001Q10_n426NaoConformidade_Codigo = new bool[] {false} ;
         T001Q11_A426NaoConformidade_Codigo = new int[1] ;
         T001Q11_n426NaoConformidade_Codigo = new bool[] {false} ;
         T001Q2_A426NaoConformidade_Codigo = new int[1] ;
         T001Q2_n426NaoConformidade_Codigo = new bool[] {false} ;
         T001Q2_A427NaoConformidade_Nome = new String[] {""} ;
         T001Q2_A429NaoConformidade_Tipo = new short[1] ;
         T001Q2_A1121NaoConformidade_EhImpeditiva = new bool[] {false} ;
         T001Q2_n1121NaoConformidade_EhImpeditiva = new bool[] {false} ;
         T001Q2_A2029NaoConformidade_Glosavel = new bool[] {false} ;
         T001Q2_n2029NaoConformidade_Glosavel = new bool[] {false} ;
         T001Q2_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         T001Q2_A2025NaoConformidade_TipoCod = new int[1] ;
         T001Q2_n2025NaoConformidade_TipoCod = new bool[] {false} ;
         T001Q12_A426NaoConformidade_Codigo = new int[1] ;
         T001Q12_n426NaoConformidade_Codigo = new bool[] {false} ;
         T001Q15_A2058Indicador_Codigo = new int[1] ;
         T001Q16_A294ContratoOcorrencia_Codigo = new int[1] ;
         T001Q17_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         T001Q18_A456ContagemResultado_Codigo = new int[1] ;
         T001Q18_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         T001Q18_A511ContagemResultado_HoraCnt = new String[] {""} ;
         T001Q19_A456ContagemResultado_Codigo = new int[1] ;
         T001Q20_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         T001Q21_A426NaoConformidade_Codigo = new int[1] ;
         T001Q21_n426NaoConformidade_Codigo = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.naoconformidade__default(),
            new Object[][] {
                new Object[] {
               T001Q2_A426NaoConformidade_Codigo, T001Q2_A427NaoConformidade_Nome, T001Q2_A429NaoConformidade_Tipo, T001Q2_A1121NaoConformidade_EhImpeditiva, T001Q2_n1121NaoConformidade_EhImpeditiva, T001Q2_A2029NaoConformidade_Glosavel, T001Q2_n2029NaoConformidade_Glosavel, T001Q2_A428NaoConformidade_AreaTrabalhoCod, T001Q2_A2025NaoConformidade_TipoCod, T001Q2_n2025NaoConformidade_TipoCod
               }
               , new Object[] {
               T001Q3_A426NaoConformidade_Codigo, T001Q3_A427NaoConformidade_Nome, T001Q3_A429NaoConformidade_Tipo, T001Q3_A1121NaoConformidade_EhImpeditiva, T001Q3_n1121NaoConformidade_EhImpeditiva, T001Q3_A2029NaoConformidade_Glosavel, T001Q3_n2029NaoConformidade_Glosavel, T001Q3_A428NaoConformidade_AreaTrabalhoCod, T001Q3_A2025NaoConformidade_TipoCod, T001Q3_n2025NaoConformidade_TipoCod
               }
               , new Object[] {
               T001Q4_A428NaoConformidade_AreaTrabalhoCod
               }
               , new Object[] {
               T001Q5_A2025NaoConformidade_TipoCod
               }
               , new Object[] {
               T001Q6_A426NaoConformidade_Codigo, T001Q6_A427NaoConformidade_Nome, T001Q6_A429NaoConformidade_Tipo, T001Q6_A1121NaoConformidade_EhImpeditiva, T001Q6_n1121NaoConformidade_EhImpeditiva, T001Q6_A2029NaoConformidade_Glosavel, T001Q6_n2029NaoConformidade_Glosavel, T001Q6_A428NaoConformidade_AreaTrabalhoCod, T001Q6_A2025NaoConformidade_TipoCod, T001Q6_n2025NaoConformidade_TipoCod
               }
               , new Object[] {
               T001Q7_A428NaoConformidade_AreaTrabalhoCod
               }
               , new Object[] {
               T001Q8_A2025NaoConformidade_TipoCod
               }
               , new Object[] {
               T001Q9_A426NaoConformidade_Codigo
               }
               , new Object[] {
               T001Q10_A426NaoConformidade_Codigo
               }
               , new Object[] {
               T001Q11_A426NaoConformidade_Codigo
               }
               , new Object[] {
               T001Q12_A426NaoConformidade_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001Q15_A2058Indicador_Codigo
               }
               , new Object[] {
               T001Q16_A294ContratoOcorrencia_Codigo
               }
               , new Object[] {
               T001Q17_A2024ContagemResultadoNaoCnf_Codigo
               }
               , new Object[] {
               T001Q18_A456ContagemResultado_Codigo, T001Q18_A473ContagemResultado_DataCnt, T001Q18_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               T001Q19_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T001Q20_A820ContagemResultadoChckLstLog_Codigo
               }
               , new Object[] {
               T001Q21_A426NaoConformidade_Codigo
               }
            }
         );
         Z2029NaoConformidade_Glosavel = false;
         n2029NaoConformidade_Glosavel = false;
         A2029NaoConformidade_Glosavel = false;
         n2029NaoConformidade_Glosavel = false;
         i2029NaoConformidade_Glosavel = false;
         n2029NaoConformidade_Glosavel = false;
         AV15Pgmname = "NaoConformidade";
         Z428NaoConformidade_AreaTrabalhoCod = AV7WWPContext.gxTpr_Areatrabalho_codigo;
         N428NaoConformidade_AreaTrabalhoCod = AV7WWPContext.gxTpr_Areatrabalho_codigo;
         i428NaoConformidade_AreaTrabalhoCod = AV7WWPContext.gxTpr_Areatrabalho_codigo;
         A428NaoConformidade_AreaTrabalhoCod = AV7WWPContext.gxTpr_Areatrabalho_codigo;
      }

      private short Z429NaoConformidade_Tipo ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A429NaoConformidade_Tipo ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound65 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int wcpOAV8NaoConformidade_Codigo ;
      private int Z426NaoConformidade_Codigo ;
      private int Z428NaoConformidade_AreaTrabalhoCod ;
      private int Z2025NaoConformidade_TipoCod ;
      private int N428NaoConformidade_AreaTrabalhoCod ;
      private int N2025NaoConformidade_TipoCod ;
      private int A428NaoConformidade_AreaTrabalhoCod ;
      private int A2025NaoConformidade_TipoCod ;
      private int AV8NaoConformidade_Codigo ;
      private int trnEnded ;
      private int A426NaoConformidade_Codigo ;
      private int edtNaoConformidade_Codigo_Enabled ;
      private int edtNaoConformidade_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int edtNaoConformidade_Nome_Enabled ;
      private int AV11Insert_NaoConformidade_AreaTrabalhoCod ;
      private int AV13Insert_NaoConformidade_TipoCod ;
      private int AV16GXV1 ;
      private int i428NaoConformidade_AreaTrabalhoCod ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z427NaoConformidade_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkNaoConformidade_Glosavel_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtNaoConformidade_Nome_Internalname ;
      private String edtNaoConformidade_Codigo_Internalname ;
      private String edtNaoConformidade_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocknaoconformidade_nome_Internalname ;
      private String lblTextblocknaoconformidade_nome_Jsonclick ;
      private String A427NaoConformidade_Nome ;
      private String edtNaoConformidade_Nome_Jsonclick ;
      private String lblTextblocknaoconformidade_tipo_Internalname ;
      private String lblTextblocknaoconformidade_tipo_Jsonclick ;
      private String cmbNaoConformidade_Tipo_Internalname ;
      private String cmbNaoConformidade_Tipo_Jsonclick ;
      private String lblTextblocknaoconformidade_ehimpeditiva_Internalname ;
      private String lblTextblocknaoconformidade_ehimpeditiva_Jsonclick ;
      private String cmbNaoConformidade_EhImpeditiva_Internalname ;
      private String cmbNaoConformidade_EhImpeditiva_Jsonclick ;
      private String lblTextblocknaoconformidade_glosavel_Internalname ;
      private String lblTextblocknaoconformidade_glosavel_Jsonclick ;
      private String AV15Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode65 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool Z1121NaoConformidade_EhImpeditiva ;
      private bool Z2029NaoConformidade_Glosavel ;
      private bool entryPointCalled ;
      private bool n2025NaoConformidade_TipoCod ;
      private bool toggleJsOutput ;
      private bool A1121NaoConformidade_EhImpeditiva ;
      private bool n1121NaoConformidade_EhImpeditiva ;
      private bool wbErr ;
      private bool A2029NaoConformidade_Glosavel ;
      private bool n2029NaoConformidade_Glosavel ;
      private bool n426NaoConformidade_Codigo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i2029NaoConformidade_Glosavel ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbNaoConformidade_Tipo ;
      private GXCombobox cmbNaoConformidade_EhImpeditiva ;
      private GXCheckbox chkNaoConformidade_Glosavel ;
      private IDataStoreProvider pr_default ;
      private int[] T001Q6_A426NaoConformidade_Codigo ;
      private bool[] T001Q6_n426NaoConformidade_Codigo ;
      private String[] T001Q6_A427NaoConformidade_Nome ;
      private short[] T001Q6_A429NaoConformidade_Tipo ;
      private bool[] T001Q6_A1121NaoConformidade_EhImpeditiva ;
      private bool[] T001Q6_n1121NaoConformidade_EhImpeditiva ;
      private bool[] T001Q6_A2029NaoConformidade_Glosavel ;
      private bool[] T001Q6_n2029NaoConformidade_Glosavel ;
      private int[] T001Q6_A428NaoConformidade_AreaTrabalhoCod ;
      private int[] T001Q6_A2025NaoConformidade_TipoCod ;
      private bool[] T001Q6_n2025NaoConformidade_TipoCod ;
      private int[] T001Q4_A428NaoConformidade_AreaTrabalhoCod ;
      private int[] T001Q5_A2025NaoConformidade_TipoCod ;
      private bool[] T001Q5_n2025NaoConformidade_TipoCod ;
      private int[] T001Q7_A428NaoConformidade_AreaTrabalhoCod ;
      private int[] T001Q8_A2025NaoConformidade_TipoCod ;
      private bool[] T001Q8_n2025NaoConformidade_TipoCod ;
      private int[] T001Q9_A426NaoConformidade_Codigo ;
      private bool[] T001Q9_n426NaoConformidade_Codigo ;
      private int[] T001Q3_A426NaoConformidade_Codigo ;
      private bool[] T001Q3_n426NaoConformidade_Codigo ;
      private String[] T001Q3_A427NaoConformidade_Nome ;
      private short[] T001Q3_A429NaoConformidade_Tipo ;
      private bool[] T001Q3_A1121NaoConformidade_EhImpeditiva ;
      private bool[] T001Q3_n1121NaoConformidade_EhImpeditiva ;
      private bool[] T001Q3_A2029NaoConformidade_Glosavel ;
      private bool[] T001Q3_n2029NaoConformidade_Glosavel ;
      private int[] T001Q3_A428NaoConformidade_AreaTrabalhoCod ;
      private int[] T001Q3_A2025NaoConformidade_TipoCod ;
      private bool[] T001Q3_n2025NaoConformidade_TipoCod ;
      private int[] T001Q10_A426NaoConformidade_Codigo ;
      private bool[] T001Q10_n426NaoConformidade_Codigo ;
      private int[] T001Q11_A426NaoConformidade_Codigo ;
      private bool[] T001Q11_n426NaoConformidade_Codigo ;
      private int[] T001Q2_A426NaoConformidade_Codigo ;
      private bool[] T001Q2_n426NaoConformidade_Codigo ;
      private String[] T001Q2_A427NaoConformidade_Nome ;
      private short[] T001Q2_A429NaoConformidade_Tipo ;
      private bool[] T001Q2_A1121NaoConformidade_EhImpeditiva ;
      private bool[] T001Q2_n1121NaoConformidade_EhImpeditiva ;
      private bool[] T001Q2_A2029NaoConformidade_Glosavel ;
      private bool[] T001Q2_n2029NaoConformidade_Glosavel ;
      private int[] T001Q2_A428NaoConformidade_AreaTrabalhoCod ;
      private int[] T001Q2_A2025NaoConformidade_TipoCod ;
      private bool[] T001Q2_n2025NaoConformidade_TipoCod ;
      private int[] T001Q12_A426NaoConformidade_Codigo ;
      private bool[] T001Q12_n426NaoConformidade_Codigo ;
      private int[] T001Q15_A2058Indicador_Codigo ;
      private int[] T001Q16_A294ContratoOcorrencia_Codigo ;
      private int[] T001Q17_A2024ContagemResultadoNaoCnf_Codigo ;
      private int[] T001Q18_A456ContagemResultado_Codigo ;
      private DateTime[] T001Q18_A473ContagemResultado_DataCnt ;
      private String[] T001Q18_A511ContagemResultado_HoraCnt ;
      private int[] T001Q19_A456ContagemResultado_Codigo ;
      private int[] T001Q20_A820ContagemResultadoChckLstLog_Codigo ;
      private int[] T001Q21_A426NaoConformidade_Codigo ;
      private bool[] T001Q21_n426NaoConformidade_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV7WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class naoconformidade__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001Q6 ;
          prmT001Q6 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q4 ;
          prmT001Q4 = new Object[] {
          new Object[] {"@NaoConformidade_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q5 ;
          prmT001Q5 = new Object[] {
          new Object[] {"@NaoConformidade_TipoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q7 ;
          prmT001Q7 = new Object[] {
          new Object[] {"@NaoConformidade_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q8 ;
          prmT001Q8 = new Object[] {
          new Object[] {"@NaoConformidade_TipoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q9 ;
          prmT001Q9 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q3 ;
          prmT001Q3 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q10 ;
          prmT001Q10 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q11 ;
          prmT001Q11 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q2 ;
          prmT001Q2 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q12 ;
          prmT001Q12 = new Object[] {
          new Object[] {"@NaoConformidade_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@NaoConformidade_Tipo",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@NaoConformidade_EhImpeditiva",SqlDbType.Bit,4,0} ,
          new Object[] {"@NaoConformidade_Glosavel",SqlDbType.Bit,4,0} ,
          new Object[] {"@NaoConformidade_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@NaoConformidade_TipoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q13 ;
          prmT001Q13 = new Object[] {
          new Object[] {"@NaoConformidade_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@NaoConformidade_Tipo",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@NaoConformidade_EhImpeditiva",SqlDbType.Bit,4,0} ,
          new Object[] {"@NaoConformidade_Glosavel",SqlDbType.Bit,4,0} ,
          new Object[] {"@NaoConformidade_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@NaoConformidade_TipoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q14 ;
          prmT001Q14 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q15 ;
          prmT001Q15 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q16 ;
          prmT001Q16 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q17 ;
          prmT001Q17 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q18 ;
          prmT001Q18 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q19 ;
          prmT001Q19 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q20 ;
          prmT001Q20 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Q21 ;
          prmT001Q21 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T001Q2", "SELECT [NaoConformidade_Codigo], [NaoConformidade_Nome], [NaoConformidade_Tipo], [NaoConformidade_EhImpeditiva], [NaoConformidade_Glosavel], [NaoConformidade_AreaTrabalhoCod] AS NaoConformidade_AreaTrabalhoCod, [NaoConformidade_TipoCod] AS NaoConformidade_TipoCod FROM [NaoConformidade] WITH (UPDLOCK) WHERE [NaoConformidade_Codigo] = @NaoConformidade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q2,1,0,true,false )
             ,new CursorDef("T001Q3", "SELECT [NaoConformidade_Codigo], [NaoConformidade_Nome], [NaoConformidade_Tipo], [NaoConformidade_EhImpeditiva], [NaoConformidade_Glosavel], [NaoConformidade_AreaTrabalhoCod] AS NaoConformidade_AreaTrabalhoCod, [NaoConformidade_TipoCod] AS NaoConformidade_TipoCod FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @NaoConformidade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q3,1,0,true,false )
             ,new CursorDef("T001Q4", "SELECT [AreaTrabalho_Codigo] AS NaoConformidade_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @NaoConformidade_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q4,1,0,true,false )
             ,new CursorDef("T001Q5", "SELECT [TipoNaoCnf_Codigo] AS NaoConformidade_TipoCod FROM [TipoNaoConformidade] WITH (NOLOCK) WHERE [TipoNaoCnf_Codigo] = @NaoConformidade_TipoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q5,1,0,true,false )
             ,new CursorDef("T001Q6", "SELECT TM1.[NaoConformidade_Codigo], TM1.[NaoConformidade_Nome], TM1.[NaoConformidade_Tipo], TM1.[NaoConformidade_EhImpeditiva], TM1.[NaoConformidade_Glosavel], TM1.[NaoConformidade_AreaTrabalhoCod] AS NaoConformidade_AreaTrabalhoCod, TM1.[NaoConformidade_TipoCod] AS NaoConformidade_TipoCod FROM [NaoConformidade] TM1 WITH (NOLOCK) WHERE TM1.[NaoConformidade_Codigo] = @NaoConformidade_Codigo ORDER BY TM1.[NaoConformidade_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q6,100,0,true,false )
             ,new CursorDef("T001Q7", "SELECT [AreaTrabalho_Codigo] AS NaoConformidade_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @NaoConformidade_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q7,1,0,true,false )
             ,new CursorDef("T001Q8", "SELECT [TipoNaoCnf_Codigo] AS NaoConformidade_TipoCod FROM [TipoNaoConformidade] WITH (NOLOCK) WHERE [TipoNaoCnf_Codigo] = @NaoConformidade_TipoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q8,1,0,true,false )
             ,new CursorDef("T001Q9", "SELECT [NaoConformidade_Codigo] FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @NaoConformidade_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q9,1,0,true,false )
             ,new CursorDef("T001Q10", "SELECT TOP 1 [NaoConformidade_Codigo] FROM [NaoConformidade] WITH (NOLOCK) WHERE ( [NaoConformidade_Codigo] > @NaoConformidade_Codigo) ORDER BY [NaoConformidade_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q10,1,0,true,true )
             ,new CursorDef("T001Q11", "SELECT TOP 1 [NaoConformidade_Codigo] FROM [NaoConformidade] WITH (NOLOCK) WHERE ( [NaoConformidade_Codigo] < @NaoConformidade_Codigo) ORDER BY [NaoConformidade_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q11,1,0,true,true )
             ,new CursorDef("T001Q12", "INSERT INTO [NaoConformidade]([NaoConformidade_Nome], [NaoConformidade_Tipo], [NaoConformidade_EhImpeditiva], [NaoConformidade_Glosavel], [NaoConformidade_AreaTrabalhoCod], [NaoConformidade_TipoCod]) VALUES(@NaoConformidade_Nome, @NaoConformidade_Tipo, @NaoConformidade_EhImpeditiva, @NaoConformidade_Glosavel, @NaoConformidade_AreaTrabalhoCod, @NaoConformidade_TipoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001Q12)
             ,new CursorDef("T001Q13", "UPDATE [NaoConformidade] SET [NaoConformidade_Nome]=@NaoConformidade_Nome, [NaoConformidade_Tipo]=@NaoConformidade_Tipo, [NaoConformidade_EhImpeditiva]=@NaoConformidade_EhImpeditiva, [NaoConformidade_Glosavel]=@NaoConformidade_Glosavel, [NaoConformidade_AreaTrabalhoCod]=@NaoConformidade_AreaTrabalhoCod, [NaoConformidade_TipoCod]=@NaoConformidade_TipoCod  WHERE [NaoConformidade_Codigo] = @NaoConformidade_Codigo", GxErrorMask.GX_NOMASK,prmT001Q13)
             ,new CursorDef("T001Q14", "DELETE FROM [NaoConformidade]  WHERE [NaoConformidade_Codigo] = @NaoConformidade_Codigo", GxErrorMask.GX_NOMASK,prmT001Q14)
             ,new CursorDef("T001Q15", "SELECT TOP 1 [Indicador_Codigo] FROM [Indicador] WITH (NOLOCK) WHERE [Indicador_NaoCnfCod] = @NaoConformidade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q15,1,0,true,true )
             ,new CursorDef("T001Q16", "SELECT TOP 1 [ContratoOcorrencia_Codigo] FROM [ContratoOcorrencia] WITH (NOLOCK) WHERE [ContratoOcorrencia_NaoCnfCod] = @NaoConformidade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q16,1,0,true,true )
             ,new CursorDef("T001Q17", "SELECT TOP 1 [ContagemResultadoNaoCnf_Codigo] FROM [ContagemResultadoNaoCnf] WITH (NOLOCK) WHERE [ContagemResultadoNaoCnf_NaoCnfCod] = @NaoConformidade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q17,1,0,true,true )
             ,new CursorDef("T001Q18", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_NaoCnfCntCod] = @NaoConformidade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q18,1,0,true,true )
             ,new CursorDef("T001Q19", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_NaoCnfDmnCod] = @NaoConformidade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q19,1,0,true,true )
             ,new CursorDef("T001Q20", "SELECT TOP 1 [ContagemResultadoChckLstLog_Codigo] FROM [ContagemResultadoChckLstLog] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @NaoConformidade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q20,1,0,true,true )
             ,new CursorDef("T001Q21", "SELECT [NaoConformidade_Codigo] FROM [NaoConformidade] WITH (NOLOCK) ORDER BY [NaoConformidade_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001Q21,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[5]);
                }
                stmt.SetParameter(5, (int)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[8]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[5]);
                }
                stmt.SetParameter(5, (int)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[10]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
