/*
               File: PromptGlosario
        Description: Selecione Glosario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:39:17.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptglosario : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptglosario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptglosario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutGlosario_Codigo )
      {
         this.AV32InOutGlosario_Codigo = aP0_InOutGlosario_Codigo;
         executePrivate();
         aP0_InOutGlosario_Codigo=this.AV32InOutGlosario_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_62 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_62_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_62_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV14DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
               AV16Glosario_Termo1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Glosario_Termo1", AV16Glosario_Termo1);
               AV18DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               AV20Glosario_Termo2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Glosario_Termo2", AV20Glosario_Termo2);
               AV22DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
               AV24Glosario_Termo3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Glosario_Termo3", AV24Glosario_Termo3);
               AV17DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV21DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
               AV13OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
               AV35TFGlosario_Termo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFGlosario_Termo", AV35TFGlosario_Termo);
               AV36TFGlosario_Termo_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFGlosario_Termo_Sel", AV36TFGlosario_Termo_Sel);
               AV37ddo_Glosario_TermoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Glosario_TermoTitleControlIdToReplace", AV37ddo_Glosario_TermoTitleControlIdToReplace);
               AV33Glosario_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Glosario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Glosario_AreaTrabalhoCod), 6, 0)));
               AV45Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV9GridState);
               AV26DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersIgnoreFirst", AV26DynamicFiltersIgnoreFirst);
               AV25DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV16Glosario_Termo1, AV18DynamicFiltersSelector2, AV20Glosario_Termo2, AV22DynamicFiltersSelector3, AV24Glosario_Termo3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV13OrderedDsc, AV35TFGlosario_Termo, AV36TFGlosario_Termo_Sel, AV37ddo_Glosario_TermoTitleControlIdToReplace, AV33Glosario_AreaTrabalhoCod, AV45Pgmname, AV9GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "PromptGlosario";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1347Glosario_Codigo), "ZZZZZ9");
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("promptglosario:[SendSecurityCheck value for]"+"Glosario_Codigo:"+context.localUtil.Format( (decimal)(A1347Glosario_Codigo), "ZZZZZ9"));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV32InOutGlosario_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32InOutGlosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32InOutGlosario_Codigo), 6, 0)));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAF92( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV45Pgmname = "PromptGlosario";
               context.Gx_err = 0;
               WSF92( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEF92( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823391768");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptglosario.aspx") + "?" + UrlEncode("" +AV32InOutGlosario_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV14DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vGLOSARIO_TERMO1", AV16Glosario_Termo1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV18DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vGLOSARIO_TERMO2", AV20Glosario_Termo2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV22DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vGLOSARIO_TERMO3", AV24Glosario_Termo3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV17DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV21DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV13OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGLOSARIO_TERMO", AV35TFGlosario_Termo);
         GxWebStd.gx_hidden_field( context, "GXH_vTFGLOSARIO_TERMO_SEL", AV36TFGlosario_Termo_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_62", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_62), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV38DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV38DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGLOSARIO_TERMOTITLEFILTERDATA", AV34Glosario_TermoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGLOSARIO_TERMOTITLEFILTERDATA", AV34Glosario_TermoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV45Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV9GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV9GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV26DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV25DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTGLOSARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32InOutGlosario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Caption", StringUtil.RTrim( Ddo_glosario_termo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Tooltip", StringUtil.RTrim( Ddo_glosario_termo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Cls", StringUtil.RTrim( Ddo_glosario_termo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Filteredtext_set", StringUtil.RTrim( Ddo_glosario_termo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Selectedvalue_set", StringUtil.RTrim( Ddo_glosario_termo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Dropdownoptionstype", StringUtil.RTrim( Ddo_glosario_termo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_glosario_termo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Includesortasc", StringUtil.BoolToStr( Ddo_glosario_termo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Includesortdsc", StringUtil.BoolToStr( Ddo_glosario_termo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Sortedstatus", StringUtil.RTrim( Ddo_glosario_termo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Includefilter", StringUtil.BoolToStr( Ddo_glosario_termo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Filtertype", StringUtil.RTrim( Ddo_glosario_termo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Filterisrange", StringUtil.BoolToStr( Ddo_glosario_termo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Includedatalist", StringUtil.BoolToStr( Ddo_glosario_termo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Datalisttype", StringUtil.RTrim( Ddo_glosario_termo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Datalistfixedvalues", StringUtil.RTrim( Ddo_glosario_termo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Datalistproc", StringUtil.RTrim( Ddo_glosario_termo_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_glosario_termo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Sortasc", StringUtil.RTrim( Ddo_glosario_termo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Sortdsc", StringUtil.RTrim( Ddo_glosario_termo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Loadingdata", StringUtil.RTrim( Ddo_glosario_termo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Cleanfilter", StringUtil.RTrim( Ddo_glosario_termo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Rangefilterfrom", StringUtil.RTrim( Ddo_glosario_termo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Rangefilterto", StringUtil.RTrim( Ddo_glosario_termo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Noresultsfound", StringUtil.RTrim( Ddo_glosario_termo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Searchbuttontext", StringUtil.RTrim( Ddo_glosario_termo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Activeeventkey", StringUtil.RTrim( Ddo_glosario_termo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Filteredtext_get", StringUtil.RTrim( Ddo_glosario_termo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Selectedvalue_get", StringUtil.RTrim( Ddo_glosario_termo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "PromptGlosario";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1347Glosario_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("promptglosario:[SendSecurityCheck value for]"+"Glosario_Codigo:"+context.localUtil.Format( (decimal)(A1347Glosario_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormF92( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptGlosario" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Glosario" ;
      }

      protected void WBF90( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_F92( true) ;
         }
         else
         {
            wb_table1_2_F92( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_F92e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGlosario_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1347Glosario_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1347Glosario_Codigo), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGlosario_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtGlosario_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_PromptGlosario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV17DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(70, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV21DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(71, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV13OrderedDsc), StringUtil.BoolToStr( AV13OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptGlosario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfglosario_termo_Internalname, AV35TFGlosario_Termo, StringUtil.RTrim( context.localUtil.Format( AV35TFGlosario_Termo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfglosario_termo_Jsonclick, 0, "Attribute", "", "", "", edtavTfglosario_termo_Visible, 1, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptGlosario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfglosario_termo_sel_Internalname, AV36TFGlosario_Termo_Sel, StringUtil.RTrim( context.localUtil.Format( AV36TFGlosario_Termo_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfglosario_termo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfglosario_termo_sel_Visible, 1, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptGlosario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_GLOSARIO_TERMOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_glosario_termotitlecontrolidtoreplace_Internalname, AV37ddo_Glosario_TermoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", 0, edtavDdo_glosario_termotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptGlosario.htm");
         }
         wbLoad = true;
      }

      protected void STARTF92( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Glosario", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPF90( ) ;
      }

      protected void WSF92( )
      {
         STARTF92( ) ;
         EVTF92( ) ;
      }

      protected void EVTF92( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11F92 */
                           E11F92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_GLOSARIO_TERMO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12F92 */
                           E12F92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13F92 */
                           E13F92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14F92 */
                           E14F92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15F92 */
                           E15F92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16F92 */
                           E16F92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17F92 */
                           E17F92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18F92 */
                           E18F92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19F92 */
                           E19F92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20F92 */
                           E20F92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21F92 */
                           E21F92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_62_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
                           SubsflControlProps_622( ) ;
                           AV27Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV27Select)) ? AV44Select_GXI : context.convertURL( context.PathToRelativeUrl( AV27Select))));
                           A858Glosario_Termo = cgiGet( edtGlosario_Termo_Internalname);
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E22F92 */
                                 E22F92 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E23F92 */
                                 E23F92 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E24F92 */
                                 E24F92 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV14DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Glosario_termo1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vGLOSARIO_TERMO1"), AV16Glosario_Termo1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Glosario_termo2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vGLOSARIO_TERMO2"), AV20Glosario_Termo2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV22DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Glosario_termo3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vGLOSARIO_TERMO3"), AV24Glosario_Termo3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV21DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfglosario_termo Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGLOSARIO_TERMO"), AV35TFGlosario_Termo) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfglosario_termo_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGLOSARIO_TERMO_SEL"), AV36TFGlosario_Termo_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E25F92 */
                                       E25F92 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEF92( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormF92( ) ;
            }
         }
      }

      protected void PAF92( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("GLOSARIO_TERMO", "Termo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV14DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV14DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("GLOSARIO_TERMO", "Termo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("GLOSARIO_TERMO", "Termo", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV22DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV22DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavGlosario_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_622( ) ;
         while ( nGXsfl_62_idx <= nRC_GXsfl_62 )
         {
            sendrow_622( ) ;
            nGXsfl_62_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_62_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_62_idx+1));
            sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
            SubsflControlProps_622( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String AV14DynamicFiltersSelector1 ,
                                       String AV16Glosario_Termo1 ,
                                       String AV18DynamicFiltersSelector2 ,
                                       String AV20Glosario_Termo2 ,
                                       String AV22DynamicFiltersSelector3 ,
                                       String AV24Glosario_Termo3 ,
                                       bool AV17DynamicFiltersEnabled2 ,
                                       bool AV21DynamicFiltersEnabled3 ,
                                       bool AV13OrderedDsc ,
                                       String AV35TFGlosario_Termo ,
                                       String AV36TFGlosario_Termo_Sel ,
                                       String AV37ddo_Glosario_TermoTitleControlIdToReplace ,
                                       int AV33Glosario_AreaTrabalhoCod ,
                                       String AV45Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV9GridState ,
                                       bool AV26DynamicFiltersIgnoreFirst ,
                                       bool AV25DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFF92( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_GLOSARIO_TERMO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A858Glosario_Termo, ""))));
         GxWebStd.gx_hidden_field( context, "GLOSARIO_TERMO", A858Glosario_Termo);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV14DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV14DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV22DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV22DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFF92( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV45Pgmname = "PromptGlosario";
         context.Gx_err = 0;
      }

      protected void RFF92( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 62;
         /* Execute user event: E23F92 */
         E23F92 ();
         nGXsfl_62_idx = 1;
         sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
         SubsflControlProps_622( ) ;
         nGXsfl_62_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_622( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV14DynamicFiltersSelector1 ,
                                                 AV16Glosario_Termo1 ,
                                                 AV17DynamicFiltersEnabled2 ,
                                                 AV18DynamicFiltersSelector2 ,
                                                 AV20Glosario_Termo2 ,
                                                 AV21DynamicFiltersEnabled3 ,
                                                 AV22DynamicFiltersSelector3 ,
                                                 AV24Glosario_Termo3 ,
                                                 AV36TFGlosario_Termo_Sel ,
                                                 AV35TFGlosario_Termo ,
                                                 A858Glosario_Termo ,
                                                 AV13OrderedDsc ,
                                                 A1346Glosario_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV16Glosario_Termo1 = StringUtil.Concat( StringUtil.RTrim( AV16Glosario_Termo1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Glosario_Termo1", AV16Glosario_Termo1);
            lV20Glosario_Termo2 = StringUtil.Concat( StringUtil.RTrim( AV20Glosario_Termo2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Glosario_Termo2", AV20Glosario_Termo2);
            lV24Glosario_Termo3 = StringUtil.Concat( StringUtil.RTrim( AV24Glosario_Termo3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Glosario_Termo3", AV24Glosario_Termo3);
            lV35TFGlosario_Termo = StringUtil.Concat( StringUtil.RTrim( AV35TFGlosario_Termo), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFGlosario_Termo", AV35TFGlosario_Termo);
            /* Using cursor H00F92 */
            pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV16Glosario_Termo1, lV20Glosario_Termo2, lV24Glosario_Termo3, lV35TFGlosario_Termo, AV36TFGlosario_Termo_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_62_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1346Glosario_AreaTrabalhoCod = H00F92_A1346Glosario_AreaTrabalhoCod[0];
               A1347Glosario_Codigo = H00F92_A1347Glosario_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1347Glosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1347Glosario_Codigo), 6, 0)));
               A858Glosario_Termo = H00F92_A858Glosario_Termo[0];
               /* Execute user event: E24F92 */
               E24F92 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 62;
            WBF90( ) ;
         }
         nGXsfl_62_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV14DynamicFiltersSelector1 ,
                                              AV16Glosario_Termo1 ,
                                              AV17DynamicFiltersEnabled2 ,
                                              AV18DynamicFiltersSelector2 ,
                                              AV20Glosario_Termo2 ,
                                              AV21DynamicFiltersEnabled3 ,
                                              AV22DynamicFiltersSelector3 ,
                                              AV24Glosario_Termo3 ,
                                              AV36TFGlosario_Termo_Sel ,
                                              AV35TFGlosario_Termo ,
                                              A858Glosario_Termo ,
                                              AV13OrderedDsc ,
                                              A1346Glosario_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV16Glosario_Termo1 = StringUtil.Concat( StringUtil.RTrim( AV16Glosario_Termo1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Glosario_Termo1", AV16Glosario_Termo1);
         lV20Glosario_Termo2 = StringUtil.Concat( StringUtil.RTrim( AV20Glosario_Termo2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Glosario_Termo2", AV20Glosario_Termo2);
         lV24Glosario_Termo3 = StringUtil.Concat( StringUtil.RTrim( AV24Glosario_Termo3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Glosario_Termo3", AV24Glosario_Termo3);
         lV35TFGlosario_Termo = StringUtil.Concat( StringUtil.RTrim( AV35TFGlosario_Termo), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFGlosario_Termo", AV35TFGlosario_Termo);
         /* Using cursor H00F93 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV16Glosario_Termo1, lV20Glosario_Termo2, lV24Glosario_Termo3, lV35TFGlosario_Termo, AV36TFGlosario_Termo_Sel});
         GRID_nRecordCount = H00F93_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV16Glosario_Termo1, AV18DynamicFiltersSelector2, AV20Glosario_Termo2, AV22DynamicFiltersSelector3, AV24Glosario_Termo3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV13OrderedDsc, AV35TFGlosario_Termo, AV36TFGlosario_Termo_Sel, AV37ddo_Glosario_TermoTitleControlIdToReplace, AV33Glosario_AreaTrabalhoCod, AV45Pgmname, AV9GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV16Glosario_Termo1, AV18DynamicFiltersSelector2, AV20Glosario_Termo2, AV22DynamicFiltersSelector3, AV24Glosario_Termo3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV13OrderedDsc, AV35TFGlosario_Termo, AV36TFGlosario_Termo_Sel, AV37ddo_Glosario_TermoTitleControlIdToReplace, AV33Glosario_AreaTrabalhoCod, AV45Pgmname, AV9GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV16Glosario_Termo1, AV18DynamicFiltersSelector2, AV20Glosario_Termo2, AV22DynamicFiltersSelector3, AV24Glosario_Termo3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV13OrderedDsc, AV35TFGlosario_Termo, AV36TFGlosario_Termo_Sel, AV37ddo_Glosario_TermoTitleControlIdToReplace, AV33Glosario_AreaTrabalhoCod, AV45Pgmname, AV9GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV16Glosario_Termo1, AV18DynamicFiltersSelector2, AV20Glosario_Termo2, AV22DynamicFiltersSelector3, AV24Glosario_Termo3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV13OrderedDsc, AV35TFGlosario_Termo, AV36TFGlosario_Termo_Sel, AV37ddo_Glosario_TermoTitleControlIdToReplace, AV33Glosario_AreaTrabalhoCod, AV45Pgmname, AV9GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV16Glosario_Termo1, AV18DynamicFiltersSelector2, AV20Glosario_Termo2, AV22DynamicFiltersSelector3, AV24Glosario_Termo3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV13OrderedDsc, AV35TFGlosario_Termo, AV36TFGlosario_Termo_Sel, AV37ddo_Glosario_TermoTitleControlIdToReplace, AV33Glosario_AreaTrabalhoCod, AV45Pgmname, AV9GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPF90( )
      {
         /* Before Start, stand alone formulas. */
         AV45Pgmname = "PromptGlosario";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E22F92 */
         E22F92 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV38DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vGLOSARIO_TERMOTITLEFILTERDATA"), AV34Glosario_TermoTitleFilterData);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavGlosario_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavGlosario_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vGLOSARIO_AREATRABALHOCOD");
               GX_FocusControl = edtavGlosario_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33Glosario_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Glosario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Glosario_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV33Glosario_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavGlosario_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Glosario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Glosario_AreaTrabalhoCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV14DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
            AV16Glosario_Termo1 = cgiGet( edtavGlosario_termo1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Glosario_Termo1", AV16Glosario_Termo1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV18DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            AV20Glosario_Termo2 = cgiGet( edtavGlosario_termo2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Glosario_Termo2", AV20Glosario_Termo2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV22DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
            AV24Glosario_Termo3 = cgiGet( edtavGlosario_termo3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Glosario_Termo3", AV24Glosario_Termo3);
            A1347Glosario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGlosario_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1347Glosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1347Glosario_Codigo), 6, 0)));
            AV17DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
            AV21DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
            AV13OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            AV35TFGlosario_Termo = cgiGet( edtavTfglosario_termo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFGlosario_Termo", AV35TFGlosario_Termo);
            AV36TFGlosario_Termo_Sel = cgiGet( edtavTfglosario_termo_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFGlosario_Termo_Sel", AV36TFGlosario_Termo_Sel);
            AV37ddo_Glosario_TermoTitleControlIdToReplace = cgiGet( edtavDdo_glosario_termotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Glosario_TermoTitleControlIdToReplace", AV37ddo_Glosario_TermoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_62 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_62"), ",", "."));
            AV40GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV41GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_glosario_termo_Caption = cgiGet( "DDO_GLOSARIO_TERMO_Caption");
            Ddo_glosario_termo_Tooltip = cgiGet( "DDO_GLOSARIO_TERMO_Tooltip");
            Ddo_glosario_termo_Cls = cgiGet( "DDO_GLOSARIO_TERMO_Cls");
            Ddo_glosario_termo_Filteredtext_set = cgiGet( "DDO_GLOSARIO_TERMO_Filteredtext_set");
            Ddo_glosario_termo_Selectedvalue_set = cgiGet( "DDO_GLOSARIO_TERMO_Selectedvalue_set");
            Ddo_glosario_termo_Dropdownoptionstype = cgiGet( "DDO_GLOSARIO_TERMO_Dropdownoptionstype");
            Ddo_glosario_termo_Titlecontrolidtoreplace = cgiGet( "DDO_GLOSARIO_TERMO_Titlecontrolidtoreplace");
            Ddo_glosario_termo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_GLOSARIO_TERMO_Includesortasc"));
            Ddo_glosario_termo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_GLOSARIO_TERMO_Includesortdsc"));
            Ddo_glosario_termo_Sortedstatus = cgiGet( "DDO_GLOSARIO_TERMO_Sortedstatus");
            Ddo_glosario_termo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_GLOSARIO_TERMO_Includefilter"));
            Ddo_glosario_termo_Filtertype = cgiGet( "DDO_GLOSARIO_TERMO_Filtertype");
            Ddo_glosario_termo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_GLOSARIO_TERMO_Filterisrange"));
            Ddo_glosario_termo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_GLOSARIO_TERMO_Includedatalist"));
            Ddo_glosario_termo_Datalisttype = cgiGet( "DDO_GLOSARIO_TERMO_Datalisttype");
            Ddo_glosario_termo_Datalistfixedvalues = cgiGet( "DDO_GLOSARIO_TERMO_Datalistfixedvalues");
            Ddo_glosario_termo_Datalistproc = cgiGet( "DDO_GLOSARIO_TERMO_Datalistproc");
            Ddo_glosario_termo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_GLOSARIO_TERMO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_glosario_termo_Sortasc = cgiGet( "DDO_GLOSARIO_TERMO_Sortasc");
            Ddo_glosario_termo_Sortdsc = cgiGet( "DDO_GLOSARIO_TERMO_Sortdsc");
            Ddo_glosario_termo_Loadingdata = cgiGet( "DDO_GLOSARIO_TERMO_Loadingdata");
            Ddo_glosario_termo_Cleanfilter = cgiGet( "DDO_GLOSARIO_TERMO_Cleanfilter");
            Ddo_glosario_termo_Rangefilterfrom = cgiGet( "DDO_GLOSARIO_TERMO_Rangefilterfrom");
            Ddo_glosario_termo_Rangefilterto = cgiGet( "DDO_GLOSARIO_TERMO_Rangefilterto");
            Ddo_glosario_termo_Noresultsfound = cgiGet( "DDO_GLOSARIO_TERMO_Noresultsfound");
            Ddo_glosario_termo_Searchbuttontext = cgiGet( "DDO_GLOSARIO_TERMO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_glosario_termo_Activeeventkey = cgiGet( "DDO_GLOSARIO_TERMO_Activeeventkey");
            Ddo_glosario_termo_Filteredtext_get = cgiGet( "DDO_GLOSARIO_TERMO_Filteredtext_get");
            Ddo_glosario_termo_Selectedvalue_get = cgiGet( "DDO_GLOSARIO_TERMO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "PromptGlosario";
            A1347Glosario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGlosario_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1347Glosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1347Glosario_Codigo), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1347Glosario_Codigo), "ZZZZZ9");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("promptglosario:[SecurityCheckFailed value for]"+"Glosario_Codigo:"+context.localUtil.Format( (decimal)(A1347Glosario_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV14DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vGLOSARIO_TERMO1"), AV16Glosario_Termo1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vGLOSARIO_TERMO2"), AV20Glosario_Termo2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV22DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vGLOSARIO_TERMO3"), AV24Glosario_Termo3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV21DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGLOSARIO_TERMO"), AV35TFGlosario_Termo) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGLOSARIO_TERMO_SEL"), AV36TFGlosario_Termo_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E22F92 */
         E22F92 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22F92( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV14DynamicFiltersSelector1 = "GLOSARIO_TERMO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV18DynamicFiltersSelector2 = "GLOSARIO_TERMO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersSelector3 = "GLOSARIO_TERMO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfglosario_termo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfglosario_termo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfglosario_termo_Visible), 5, 0)));
         edtavTfglosario_termo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfglosario_termo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfglosario_termo_sel_Visible), 5, 0)));
         Ddo_glosario_termo_Titlecontrolidtoreplace = subGrid_Internalname+"_Glosario_Termo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_termo_Internalname, "TitleControlIdToReplace", Ddo_glosario_termo_Titlecontrolidtoreplace);
         AV37ddo_Glosario_TermoTitleControlIdToReplace = Ddo_glosario_termo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Glosario_TermoTitleControlIdToReplace", AV37ddo_Glosario_TermoTitleControlIdToReplace);
         edtavDdo_glosario_termotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_glosario_termotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_glosario_termotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Glosario";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         edtGlosario_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGlosario_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV38DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV38DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E23F92( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV34Glosario_TermoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtGlosario_Termo_Titleformat = 2;
         edtGlosario_Termo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Termo", AV37ddo_Glosario_TermoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Termo_Internalname, "Title", edtGlosario_Termo_Title);
         AV40GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40GridCurrentPage), 10, 0)));
         AV41GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34Glosario_TermoTitleFilterData", AV34Glosario_TermoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9GridState", AV9GridState);
      }

      protected void E11F92( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV39PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV39PageToGo) ;
         }
      }

      protected void E12F92( )
      {
         /* Ddo_glosario_termo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_glosario_termo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_glosario_termo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_termo_Internalname, "SortedStatus", Ddo_glosario_termo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_glosario_termo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_glosario_termo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_termo_Internalname, "SortedStatus", Ddo_glosario_termo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_glosario_termo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFGlosario_Termo = Ddo_glosario_termo_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFGlosario_Termo", AV35TFGlosario_Termo);
            AV36TFGlosario_Termo_Sel = Ddo_glosario_termo_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFGlosario_Termo_Sel", AV36TFGlosario_Termo_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E24F92( )
      {
         /* Grid_Load Routine */
         AV27Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV27Select);
         AV44Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 62;
         }
         sendrow_622( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_62_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(62, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E25F92 */
         E25F92 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25F92( )
      {
         /* Enter Routine */
         AV32InOutGlosario_Codigo = A1347Glosario_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32InOutGlosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32InOutGlosario_Codigo), 6, 0)));
         context.setWebReturnParms(new Object[] {(int)AV32InOutGlosario_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E17F92( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV17DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E13F92( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV25DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         AV26DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersIgnoreFirst", AV26DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         AV26DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersIgnoreFirst", AV26DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV16Glosario_Termo1, AV18DynamicFiltersSelector2, AV20Glosario_Termo2, AV22DynamicFiltersSelector3, AV24Glosario_Termo3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV13OrderedDsc, AV35TFGlosario_Termo, AV36TFGlosario_Termo_Sel, AV37ddo_Glosario_TermoTitleControlIdToReplace, AV33Glosario_AreaTrabalhoCod, AV45Pgmname, AV9GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9GridState", AV9GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E18F92( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19F92( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV21DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E14F92( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV25DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV16Glosario_Termo1, AV18DynamicFiltersSelector2, AV20Glosario_Termo2, AV22DynamicFiltersSelector3, AV24Glosario_Termo3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV13OrderedDsc, AV35TFGlosario_Termo, AV36TFGlosario_Termo_Sel, AV37ddo_Glosario_TermoTitleControlIdToReplace, AV33Glosario_AreaTrabalhoCod, AV45Pgmname, AV9GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9GridState", AV9GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E20F92( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15F92( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV25DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         AV21DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV16Glosario_Termo1, AV18DynamicFiltersSelector2, AV20Glosario_Termo2, AV22DynamicFiltersSelector3, AV24Glosario_Termo3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV13OrderedDsc, AV35TFGlosario_Termo, AV36TFGlosario_Termo_Sel, AV37ddo_Glosario_TermoTitleControlIdToReplace, AV33Glosario_AreaTrabalhoCod, AV45Pgmname, AV9GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9GridState", AV9GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E21F92( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16F92( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9GridState", AV9GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S192( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         Ddo_glosario_termo_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_termo_Internalname, "SortedStatus", Ddo_glosario_termo_Sortedstatus);
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavGlosario_termo1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGlosario_termo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGlosario_termo1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "GLOSARIO_TERMO") == 0 )
         {
            edtavGlosario_termo1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGlosario_termo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGlosario_termo1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavGlosario_termo2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGlosario_termo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGlosario_termo2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "GLOSARIO_TERMO") == 0 )
         {
            edtavGlosario_termo2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGlosario_termo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGlosario_termo2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavGlosario_termo3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGlosario_termo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGlosario_termo3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV22DynamicFiltersSelector3, "GLOSARIO_TERMO") == 0 )
         {
            edtavGlosario_termo3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGlosario_termo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGlosario_termo3_Visible), 5, 0)));
         }
      }

      protected void S172( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         AV18DynamicFiltersSelector2 = "GLOSARIO_TERMO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         AV20Glosario_Termo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Glosario_Termo2", AV20Glosario_Termo2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
         AV22DynamicFiltersSelector3 = "GLOSARIO_TERMO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
         AV24Glosario_Termo3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Glosario_Termo3", AV24Glosario_Termo3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S182( )
      {
         /* 'CLEANFILTERS' Routine */
         AV33Glosario_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Glosario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Glosario_AreaTrabalhoCod), 6, 0)));
         AV35TFGlosario_Termo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFGlosario_Termo", AV35TFGlosario_Termo);
         Ddo_glosario_termo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_termo_Internalname, "FilteredText_set", Ddo_glosario_termo_Filteredtext_set);
         AV36TFGlosario_Termo_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFGlosario_Termo_Sel", AV36TFGlosario_Termo_Sel);
         Ddo_glosario_termo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_termo_Internalname, "SelectedValue_set", Ddo_glosario_termo_Selectedvalue_set);
         AV14DynamicFiltersSelector1 = "GLOSARIO_TERMO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
         AV16Glosario_Termo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Glosario_Termo1", AV16Glosario_Termo1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV9GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV9GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV11GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV9GridState.gxTpr_Dynamicfilters.Item(1));
            AV14DynamicFiltersSelector1 = AV11GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "GLOSARIO_TERMO") == 0 )
            {
               AV16Glosario_Termo1 = AV11GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Glosario_Termo1", AV16Glosario_Termo1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV9GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV17DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV11GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV9GridState.gxTpr_Dynamicfilters.Item(2));
               AV18DynamicFiltersSelector2 = AV11GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "GLOSARIO_TERMO") == 0 )
               {
                  AV20Glosario_Termo2 = AV11GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Glosario_Termo2", AV20Glosario_Termo2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV9GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV21DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
                  AV11GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV9GridState.gxTpr_Dynamicfilters.Item(3));
                  AV22DynamicFiltersSelector3 = AV11GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV22DynamicFiltersSelector3, "GLOSARIO_TERMO") == 0 )
                  {
                     AV24Glosario_Termo3 = AV11GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Glosario_Termo3", AV24Glosario_Termo3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV25DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV9GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV9GridState.gxTpr_Ordereddsc = AV13OrderedDsc;
         AV9GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV33Glosario_AreaTrabalhoCod) )
         {
            AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV10GridStateFilterValue.gxTpr_Name = "GLOSARIO_AREATRABALHOCOD";
            AV10GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV33Glosario_AreaTrabalhoCod), 6, 0);
            AV9GridState.gxTpr_Filtervalues.Add(AV10GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFGlosario_Termo)) )
         {
            AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV10GridStateFilterValue.gxTpr_Name = "TFGLOSARIO_TERMO";
            AV10GridStateFilterValue.gxTpr_Value = AV35TFGlosario_Termo;
            AV9GridState.gxTpr_Filtervalues.Add(AV10GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFGlosario_Termo_Sel)) )
         {
            AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV10GridStateFilterValue.gxTpr_Name = "TFGLOSARIO_TERMO_SEL";
            AV10GridStateFilterValue.gxTpr_Value = AV36TFGlosario_Termo_Sel;
            AV9GridState.gxTpr_Filtervalues.Add(AV10GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV45Pgmname+"GridState",  AV9GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S162( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV9GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV26DynamicFiltersIgnoreFirst )
         {
            AV11GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV11GridStateDynamicFilter.gxTpr_Selected = AV14DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "GLOSARIO_TERMO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV16Glosario_Termo1)) )
            {
               AV11GridStateDynamicFilter.gxTpr_Value = AV16Glosario_Termo1;
            }
            if ( AV25DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV11GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV9GridState.gxTpr_Dynamicfilters.Add(AV11GridStateDynamicFilter, 0);
            }
         }
         if ( AV17DynamicFiltersEnabled2 )
         {
            AV11GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV11GridStateDynamicFilter.gxTpr_Selected = AV18DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "GLOSARIO_TERMO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV20Glosario_Termo2)) )
            {
               AV11GridStateDynamicFilter.gxTpr_Value = AV20Glosario_Termo2;
            }
            if ( AV25DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV11GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV9GridState.gxTpr_Dynamicfilters.Add(AV11GridStateDynamicFilter, 0);
            }
         }
         if ( AV21DynamicFiltersEnabled3 )
         {
            AV11GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV11GridStateDynamicFilter.gxTpr_Selected = AV22DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV22DynamicFiltersSelector3, "GLOSARIO_TERMO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Glosario_Termo3)) )
            {
               AV11GridStateDynamicFilter.gxTpr_Value = AV24Glosario_Termo3;
            }
            if ( AV25DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV11GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV9GridState.gxTpr_Dynamicfilters.Add(AV11GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_F92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_F92( true) ;
         }
         else
         {
            wb_table2_5_F92( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_F92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_56_F92( true) ;
         }
         else
         {
            wb_table3_56_F92( false) ;
         }
         return  ;
      }

      protected void wb_table3_56_F92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_F92e( true) ;
         }
         else
         {
            wb_table1_2_F92e( false) ;
         }
      }

      protected void wb_table3_56_F92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_59_F92( true) ;
         }
         else
         {
            wb_table4_59_F92( false) ;
         }
         return  ;
      }

      protected void wb_table4_59_F92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_56_F92e( true) ;
         }
         else
         {
            wb_table3_56_F92e( false) ;
         }
      }

      protected void wb_table4_59_F92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"62\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtGlosario_Termo_Titleformat == 0 )
               {
                  context.SendWebValue( edtGlosario_Termo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtGlosario_Termo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV27Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A858Glosario_Termo);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtGlosario_Termo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtGlosario_Termo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 62 )
         {
            wbEnd = 0;
            nRC_GXsfl_62 = (short)(nGXsfl_62_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_59_F92e( true) ;
         }
         else
         {
            wb_table4_59_F92e( false) ;
         }
      }

      protected void wb_table2_5_F92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_8_F92( true) ;
         }
         else
         {
            wb_table5_8_F92( false) ;
         }
         return  ;
      }

      protected void wb_table5_8_F92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_F92e( true) ;
         }
         else
         {
            wb_table2_5_F92e( false) ;
         }
      }

      protected void wb_table5_8_F92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextglosario_areatrabalhocod_Internalname, "de Trabalho", "", "", lblFiltertextglosario_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGlosario_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33Glosario_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV33Glosario_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,15);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGlosario_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_17_F92( true) ;
         }
         else
         {
            wb_table6_17_F92( false) ;
         }
         return  ;
      }

      protected void wb_table6_17_F92e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_8_F92e( true) ;
         }
         else
         {
            wb_table5_8_F92e( false) ;
         }
      }

      protected void wb_table6_17_F92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV14DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "", true, "HLP_PromptGlosario.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGlosario_termo1_Internalname, AV16Glosario_Termo1, StringUtil.RTrim( context.localUtil.Format( AV16Glosario_Termo1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGlosario_termo1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGlosario_termo1_Visible, 1, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptGlosario.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV18DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_PromptGlosario.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGlosario_termo2_Internalname, AV20Glosario_Termo2, StringUtil.RTrim( context.localUtil.Format( AV20Glosario_Termo2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGlosario_termo2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGlosario_termo2_Visible, 1, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptGlosario.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV22DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_PromptGlosario.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGlosario_termo3_Internalname, AV24Glosario_Termo3, StringUtil.RTrim( context.localUtil.Format( AV24Glosario_Termo3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGlosario_termo3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGlosario_termo3_Visible, 1, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_17_F92e( true) ;
         }
         else
         {
            wb_table6_17_F92e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV32InOutGlosario_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32InOutGlosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32InOutGlosario_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAF92( ) ;
         WSF92( ) ;
         WEF92( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282339200");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptglosario.js", "?20204282339200");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_622( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_62_idx;
         edtGlosario_Termo_Internalname = "GLOSARIO_TERMO_"+sGXsfl_62_idx;
      }

      protected void SubsflControlProps_fel_622( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_62_fel_idx;
         edtGlosario_Termo_Internalname = "GLOSARIO_TERMO_"+sGXsfl_62_fel_idx;
      }

      protected void sendrow_622( )
      {
         SubsflControlProps_622( ) ;
         WBF90( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_62_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_62_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_62_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 63,'',false,'',62)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV27Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV27Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV44Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV27Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV27Select)) ? AV44Select_GXI : context.PathToRelativeUrl( AV27Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_62_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV27Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtGlosario_Termo_Internalname,(String)A858Glosario_Termo,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtGlosario_Termo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)255,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_GLOSARIO_TERMO"+"_"+sGXsfl_62_idx, GetSecureSignedToken( sGXsfl_62_idx, StringUtil.RTrim( context.localUtil.Format( A858Glosario_Termo, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_62_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_62_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_62_idx+1));
            sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
            SubsflControlProps_622( ) ;
         }
         /* End function sendrow_622 */
      }

      protected void init_default_properties( )
      {
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextglosario_areatrabalhocod_Internalname = "FILTERTEXTGLOSARIO_AREATRABALHOCOD";
         edtavGlosario_areatrabalhocod_Internalname = "vGLOSARIO_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavGlosario_termo1_Internalname = "vGLOSARIO_TERMO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavGlosario_termo2_Internalname = "vGLOSARIO_TERMO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavGlosario_termo3_Internalname = "vGLOSARIO_TERMO3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtGlosario_Termo_Internalname = "GLOSARIO_TERMO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         edtGlosario_Codigo_Internalname = "GLOSARIO_CODIGO";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         edtavTfglosario_termo_Internalname = "vTFGLOSARIO_TERMO";
         edtavTfglosario_termo_sel_Internalname = "vTFGLOSARIO_TERMO_SEL";
         Ddo_glosario_termo_Internalname = "DDO_GLOSARIO_TERMO";
         edtavDdo_glosario_termotitlecontrolidtoreplace_Internalname = "vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtGlosario_Termo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavGlosario_termo3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavGlosario_termo2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavGlosario_termo1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavGlosario_areatrabalhocod_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtGlosario_Termo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavGlosario_termo3_Visible = 1;
         edtavGlosario_termo2_Visible = 1;
         edtavGlosario_termo1_Visible = 1;
         edtGlosario_Termo_Title = "Termo";
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_glosario_termotitlecontrolidtoreplace_Visible = 1;
         edtavTfglosario_termo_sel_Jsonclick = "";
         edtavTfglosario_termo_sel_Visible = 1;
         edtavTfglosario_termo_Jsonclick = "";
         edtavTfglosario_termo_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtGlosario_Codigo_Jsonclick = "";
         edtGlosario_Codigo_Visible = 1;
         Ddo_glosario_termo_Searchbuttontext = "Pesquisar";
         Ddo_glosario_termo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_glosario_termo_Rangefilterto = "At�";
         Ddo_glosario_termo_Rangefilterfrom = "Desde";
         Ddo_glosario_termo_Cleanfilter = "Limpar pesquisa";
         Ddo_glosario_termo_Loadingdata = "Carregando dados...";
         Ddo_glosario_termo_Sortdsc = "Ordenar de Z � A";
         Ddo_glosario_termo_Sortasc = "Ordenar de A � Z";
         Ddo_glosario_termo_Datalistupdateminimumcharacters = 0;
         Ddo_glosario_termo_Datalistproc = "GetPromptGlosarioFilterData";
         Ddo_glosario_termo_Datalistfixedvalues = "";
         Ddo_glosario_termo_Datalisttype = "Dynamic";
         Ddo_glosario_termo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_glosario_termo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_glosario_termo_Filtertype = "Character";
         Ddo_glosario_termo_Includefilter = Convert.ToBoolean( -1);
         Ddo_glosario_termo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_glosario_termo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_glosario_termo_Titlecontrolidtoreplace = "";
         Ddo_glosario_termo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_glosario_termo_Cls = "ColumnSettings";
         Ddo_glosario_termo_Tooltip = "Op��es";
         Ddo_glosario_termo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Glosario";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV37ddo_Glosario_TermoTitleControlIdToReplace',fld:'vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV33Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV35TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV36TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''}],oparms:[{av:'AV34Glosario_TermoTitleFilterData',fld:'vGLOSARIO_TERMOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtGlosario_Termo_Titleformat',ctrl:'GLOSARIO_TERMO',prop:'Titleformat'},{av:'edtGlosario_Termo_Title',ctrl:'GLOSARIO_TERMO',prop:'Title'},{av:'AV40GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV41GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11F92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV35TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV36TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'AV37ddo_Glosario_TermoTitleControlIdToReplace',fld:'vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_GLOSARIO_TERMO.ONOPTIONCLICKED","{handler:'E12F92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV35TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV36TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'AV37ddo_Glosario_TermoTitleControlIdToReplace',fld:'vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_glosario_termo_Activeeventkey',ctrl:'DDO_GLOSARIO_TERMO',prop:'ActiveEventKey'},{av:'Ddo_glosario_termo_Filteredtext_get',ctrl:'DDO_GLOSARIO_TERMO',prop:'FilteredText_get'},{av:'Ddo_glosario_termo_Selectedvalue_get',ctrl:'DDO_GLOSARIO_TERMO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_glosario_termo_Sortedstatus',ctrl:'DDO_GLOSARIO_TERMO',prop:'SortedStatus'},{av:'AV35TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV36TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''}]}");
         setEventMetadata("GRID.LOAD","{handler:'E24F92',iparms:[],oparms:[{av:'AV27Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E25F92',iparms:[{av:'A1347Glosario_Codigo',fld:'GLOSARIO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV32InOutGlosario_Codigo',fld:'vINOUTGLOSARIO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E17F92',iparms:[],oparms:[{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E13F92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV35TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV36TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'AV37ddo_Glosario_TermoTitleControlIdToReplace',fld:'vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavGlosario_termo2_Visible',ctrl:'vGLOSARIO_TERMO2',prop:'Visible'},{av:'edtavGlosario_termo3_Visible',ctrl:'vGLOSARIO_TERMO3',prop:'Visible'},{av:'edtavGlosario_termo1_Visible',ctrl:'vGLOSARIO_TERMO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E18F92',iparms:[{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavGlosario_termo1_Visible',ctrl:'vGLOSARIO_TERMO1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E19F92',iparms:[],oparms:[{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E14F92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV35TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV36TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'AV37ddo_Glosario_TermoTitleControlIdToReplace',fld:'vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavGlosario_termo2_Visible',ctrl:'vGLOSARIO_TERMO2',prop:'Visible'},{av:'edtavGlosario_termo3_Visible',ctrl:'vGLOSARIO_TERMO3',prop:'Visible'},{av:'edtavGlosario_termo1_Visible',ctrl:'vGLOSARIO_TERMO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20F92',iparms:[{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavGlosario_termo2_Visible',ctrl:'vGLOSARIO_TERMO2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E15F92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV35TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV36TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'AV37ddo_Glosario_TermoTitleControlIdToReplace',fld:'vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavGlosario_termo2_Visible',ctrl:'vGLOSARIO_TERMO2',prop:'Visible'},{av:'edtavGlosario_termo3_Visible',ctrl:'vGLOSARIO_TERMO3',prop:'Visible'},{av:'edtavGlosario_termo1_Visible',ctrl:'vGLOSARIO_TERMO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E21F92',iparms:[{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavGlosario_termo3_Visible',ctrl:'vGLOSARIO_TERMO3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E16F92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV35TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV36TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'AV37ddo_Glosario_TermoTitleControlIdToReplace',fld:'vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV33Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV35TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'Ddo_glosario_termo_Filteredtext_set',ctrl:'DDO_GLOSARIO_TERMO',prop:'FilteredText_set'},{av:'AV36TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'Ddo_glosario_termo_Selectedvalue_set',ctrl:'DDO_GLOSARIO_TERMO',prop:'SelectedValue_set'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavGlosario_termo1_Visible',ctrl:'vGLOSARIO_TERMO1',prop:'Visible'},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavGlosario_termo2_Visible',ctrl:'vGLOSARIO_TERMO2',prop:'Visible'},{av:'edtavGlosario_termo3_Visible',ctrl:'vGLOSARIO_TERMO3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_glosario_termo_Activeeventkey = "";
         Ddo_glosario_termo_Filteredtext_get = "";
         Ddo_glosario_termo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV14DynamicFiltersSelector1 = "";
         AV16Glosario_Termo1 = "";
         AV18DynamicFiltersSelector2 = "";
         AV20Glosario_Termo2 = "";
         AV22DynamicFiltersSelector3 = "";
         AV24Glosario_Termo3 = "";
         AV35TFGlosario_Termo = "";
         AV36TFGlosario_Termo_Sel = "";
         AV37ddo_Glosario_TermoTitleControlIdToReplace = "";
         AV45Pgmname = "";
         AV9GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV38DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV34Glosario_TermoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_glosario_termo_Filteredtext_set = "";
         Ddo_glosario_termo_Selectedvalue_set = "";
         Ddo_glosario_termo_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV27Select = "";
         AV44Select_GXI = "";
         A858Glosario_Termo = "";
         GridContainer = new GXWebGrid( context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         lV16Glosario_Termo1 = "";
         lV20Glosario_Termo2 = "";
         lV24Glosario_Termo3 = "";
         lV35TFGlosario_Termo = "";
         H00F92_A1346Glosario_AreaTrabalhoCod = new int[1] ;
         H00F92_A1347Glosario_Codigo = new int[1] ;
         H00F92_A858Glosario_Termo = new String[] {""} ;
         H00F93_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV11GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFiltertextglosario_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptglosario__default(),
            new Object[][] {
                new Object[] {
               H00F92_A1346Glosario_AreaTrabalhoCod, H00F92_A1347Glosario_Codigo, H00F92_A858Glosario_Termo
               }
               , new Object[] {
               H00F93_AGRID_nRecordCount
               }
            }
         );
         AV45Pgmname = "PromptGlosario";
         /* GeneXus formulas. */
         AV45Pgmname = "PromptGlosario";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_62 ;
      private short nGXsfl_62_idx=1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_62_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtGlosario_Termo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV32InOutGlosario_Codigo ;
      private int wcpOAV32InOutGlosario_Codigo ;
      private int subGrid_Rows ;
      private int AV33Glosario_AreaTrabalhoCod ;
      private int A1347Glosario_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_glosario_termo_Datalistupdateminimumcharacters ;
      private int edtGlosario_Codigo_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfglosario_termo_Visible ;
      private int edtavTfglosario_termo_sel_Visible ;
      private int edtavDdo_glosario_termotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A1346Glosario_AreaTrabalhoCod ;
      private int AV39PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavGlosario_termo1_Visible ;
      private int edtavGlosario_termo2_Visible ;
      private int edtavGlosario_termo3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV40GridCurrentPage ;
      private long AV41GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_glosario_termo_Activeeventkey ;
      private String Ddo_glosario_termo_Filteredtext_get ;
      private String Ddo_glosario_termo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_62_idx="0001" ;
      private String AV45Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_glosario_termo_Caption ;
      private String Ddo_glosario_termo_Tooltip ;
      private String Ddo_glosario_termo_Cls ;
      private String Ddo_glosario_termo_Filteredtext_set ;
      private String Ddo_glosario_termo_Selectedvalue_set ;
      private String Ddo_glosario_termo_Dropdownoptionstype ;
      private String Ddo_glosario_termo_Titlecontrolidtoreplace ;
      private String Ddo_glosario_termo_Sortedstatus ;
      private String Ddo_glosario_termo_Filtertype ;
      private String Ddo_glosario_termo_Datalisttype ;
      private String Ddo_glosario_termo_Datalistfixedvalues ;
      private String Ddo_glosario_termo_Datalistproc ;
      private String Ddo_glosario_termo_Sortasc ;
      private String Ddo_glosario_termo_Sortdsc ;
      private String Ddo_glosario_termo_Loadingdata ;
      private String Ddo_glosario_termo_Cleanfilter ;
      private String Ddo_glosario_termo_Rangefilterfrom ;
      private String Ddo_glosario_termo_Rangefilterto ;
      private String Ddo_glosario_termo_Noresultsfound ;
      private String Ddo_glosario_termo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String edtGlosario_Codigo_Internalname ;
      private String edtGlosario_Codigo_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfglosario_termo_Internalname ;
      private String edtavTfglosario_termo_Jsonclick ;
      private String edtavTfglosario_termo_sel_Internalname ;
      private String edtavTfglosario_termo_sel_Jsonclick ;
      private String edtavDdo_glosario_termotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtGlosario_Termo_Internalname ;
      private String edtavGlosario_areatrabalhocod_Internalname ;
      private String scmdbuf ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavGlosario_termo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavGlosario_termo2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavGlosario_termo3_Internalname ;
      private String hsh ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_glosario_termo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtGlosario_Termo_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextglosario_areatrabalhocod_Internalname ;
      private String lblFiltertextglosario_areatrabalhocod_Jsonclick ;
      private String edtavGlosario_areatrabalhocod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavGlosario_termo1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavGlosario_termo2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavGlosario_termo3_Jsonclick ;
      private String sGXsfl_62_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtGlosario_Termo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV17DynamicFiltersEnabled2 ;
      private bool AV21DynamicFiltersEnabled3 ;
      private bool AV13OrderedDsc ;
      private bool AV26DynamicFiltersIgnoreFirst ;
      private bool AV25DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_glosario_termo_Includesortasc ;
      private bool Ddo_glosario_termo_Includesortdsc ;
      private bool Ddo_glosario_termo_Includefilter ;
      private bool Ddo_glosario_termo_Filterisrange ;
      private bool Ddo_glosario_termo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV27Select_IsBlob ;
      private String AV14DynamicFiltersSelector1 ;
      private String AV16Glosario_Termo1 ;
      private String AV18DynamicFiltersSelector2 ;
      private String AV20Glosario_Termo2 ;
      private String AV22DynamicFiltersSelector3 ;
      private String AV24Glosario_Termo3 ;
      private String AV35TFGlosario_Termo ;
      private String AV36TFGlosario_Termo_Sel ;
      private String AV37ddo_Glosario_TermoTitleControlIdToReplace ;
      private String AV44Select_GXI ;
      private String A858Glosario_Termo ;
      private String lV16Glosario_Termo1 ;
      private String lV20Glosario_Termo2 ;
      private String lV24Glosario_Termo3 ;
      private String lV35TFGlosario_Termo ;
      private String AV27Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutGlosario_Codigo ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00F92_A1346Glosario_AreaTrabalhoCod ;
      private int[] H00F92_A1347Glosario_Codigo ;
      private String[] H00F92_A858Glosario_Termo ;
      private long[] H00F93_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34Glosario_TermoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV9GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV10GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV11GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV38DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptglosario__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00F92( IGxContext context ,
                                             String AV14DynamicFiltersSelector1 ,
                                             String AV16Glosario_Termo1 ,
                                             bool AV17DynamicFiltersEnabled2 ,
                                             String AV18DynamicFiltersSelector2 ,
                                             String AV20Glosario_Termo2 ,
                                             bool AV21DynamicFiltersEnabled3 ,
                                             String AV22DynamicFiltersSelector3 ,
                                             String AV24Glosario_Termo3 ,
                                             String AV36TFGlosario_Termo_Sel ,
                                             String AV35TFGlosario_Termo ,
                                             String A858Glosario_Termo ,
                                             bool AV13OrderedDsc ,
                                             int A1346Glosario_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [11] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Glosario_AreaTrabalhoCod], [Glosario_Codigo], [Glosario_Termo]";
         sFromString = " FROM [Glosario] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([Glosario_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16Glosario_Termo1)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV16Glosario_Termo1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20Glosario_Termo2)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV20Glosario_Termo2)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV21DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector3, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Glosario_Termo3)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV24Glosario_Termo3)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV36TFGlosario_Termo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFGlosario_Termo)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like @lV35TFGlosario_Termo)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFGlosario_Termo_Sel)) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] = @AV36TFGlosario_Termo_Sel)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Glosario_Termo]";
         }
         else if ( AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Glosario_Termo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Glosario_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00F93( IGxContext context ,
                                             String AV14DynamicFiltersSelector1 ,
                                             String AV16Glosario_Termo1 ,
                                             bool AV17DynamicFiltersEnabled2 ,
                                             String AV18DynamicFiltersSelector2 ,
                                             String AV20Glosario_Termo2 ,
                                             bool AV21DynamicFiltersEnabled3 ,
                                             String AV22DynamicFiltersSelector3 ,
                                             String AV24Glosario_Termo3 ,
                                             String AV36TFGlosario_Termo_Sel ,
                                             String AV35TFGlosario_Termo ,
                                             String A858Glosario_Termo ,
                                             bool AV13OrderedDsc ,
                                             int A1346Glosario_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [6] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Glosario] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Glosario_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16Glosario_Termo1)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV16Glosario_Termo1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20Glosario_Termo2)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV20Glosario_Termo2)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV21DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector3, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Glosario_Termo3)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV24Glosario_Termo3)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV36TFGlosario_Termo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFGlosario_Termo)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like @lV35TFGlosario_Termo)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFGlosario_Termo_Sel)) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] = @AV36TFGlosario_Termo_Sel)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00F92(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] );
               case 1 :
                     return conditional_H00F93(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00F92 ;
          prmH00F92 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16Glosario_Termo1",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV20Glosario_Termo2",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV24Glosario_Termo3",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV35TFGlosario_Termo",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV36TFGlosario_Termo_Sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00F93 ;
          prmH00F93 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16Glosario_Termo1",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV20Glosario_Termo2",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV24Glosario_Termo3",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV35TFGlosario_Termo",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV36TFGlosario_Termo_Sel",SqlDbType.VarChar,255,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00F92", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00F92,11,0,true,false )
             ,new CursorDef("H00F93", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00F93,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

}
