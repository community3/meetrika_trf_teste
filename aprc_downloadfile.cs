/*
               File: PRC_DownloadFile
        Description: PRC_Download File
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:58.34
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_downloadfile : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV9Path = gxfirstwebparm;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV11ContentType = GetNextPar( );
                  AV10NomeArquivo = GetNextPar( );
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aprc_downloadfile( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_downloadfile( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_Path ,
                           String aP1_ContentType ,
                           String aP2_NomeArquivo )
      {
         this.AV9Path = aP0_Path;
         this.AV11ContentType = aP1_ContentType;
         this.AV10NomeArquivo = aP2_NomeArquivo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_Path ,
                                 String aP1_ContentType ,
                                 String aP2_NomeArquivo )
      {
         aprc_downloadfile objaprc_downloadfile;
         objaprc_downloadfile = new aprc_downloadfile();
         objaprc_downloadfile.AV9Path = aP0_Path;
         objaprc_downloadfile.AV11ContentType = aP1_ContentType;
         objaprc_downloadfile.AV10NomeArquivo = aP2_NomeArquivo;
         objaprc_downloadfile.context.SetSubmitInitialConfig(context);
         objaprc_downloadfile.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_downloadfile);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_downloadfile)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11ContentType)) )
         {
            if ( ! context.isAjaxRequest( ) )
            {
               AV8HttpResponse.AppendHeader("Content-type", "text/plain; charset=ISO-8859-1");
            }
         }
         else
         {
            if ( ! context.isAjaxRequest( ) )
            {
               AV8HttpResponse.AppendHeader("Content-type", AV11ContentType);
            }
         }
         if ( ! context.isAjaxRequest( ) )
         {
            AV8HttpResponse.AppendHeader("Content-Disposition", "attachment; filename="+StringUtil.StringReplace( AV10NomeArquivo, " ", " "));
         }
         AV8HttpResponse.AddFile(StringUtil.StringReplace( StringUtil.Lower( AV9Path), "http://", ""));
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV8HttpResponse = new GxHttpResponse( context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV9Path ;
      private String AV11ContentType ;
      private String AV10NomeArquivo ;
      private bool entryPointCalled ;
      private GxHttpResponse AV8HttpResponse ;
   }

}
