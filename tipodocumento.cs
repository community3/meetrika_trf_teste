/*
               File: TipoDocumento
        Description: Tipo de Documentos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:51:17.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tipodocumento : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7TipoDocumento_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7TipoDocumento_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTIPODOCUMENTO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7TipoDocumento_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         chkTipoDocumento_Ativo.Name = "TIPODOCUMENTO_ATIVO";
         chkTipoDocumento_Ativo.WebTags = "";
         chkTipoDocumento_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkTipoDocumento_Ativo_Internalname, "TitleCaption", chkTipoDocumento_Ativo.Caption);
         chkTipoDocumento_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Tipo de Documentos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtTipoDocumento_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public tipodocumento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public tipodocumento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_TipoDocumento_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7TipoDocumento_Codigo = aP1_TipoDocumento_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkTipoDocumento_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2885( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2885e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTipoDocumento_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ",", "")), ((edtTipoDocumento_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipoDocumento_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtTipoDocumento_Codigo_Visible, edtTipoDocumento_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_TipoDocumento.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2885( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2885( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2885e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_26_2885( true) ;
         }
         return  ;
      }

      protected void wb_table3_26_2885e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2885e( true) ;
         }
         else
         {
            wb_table1_2_2885e( false) ;
         }
      }

      protected void wb_table3_26_2885( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_26_2885e( true) ;
         }
         else
         {
            wb_table3_26_2885e( false) ;
         }
      }

      protected void wb_table2_5_2885( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2885( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2885e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2885e( true) ;
         }
         else
         {
            wb_table2_5_2885e( false) ;
         }
      }

      protected void wb_table4_13_2885( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipodocumento_nome_Internalname, "Nome", "", "", lblTextblocktipodocumento_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTipoDocumento_Nome_Internalname, StringUtil.RTrim( A646TipoDocumento_Nome), StringUtil.RTrim( context.localUtil.Format( A646TipoDocumento_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipoDocumento_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtTipoDocumento_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_TipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipodocumento_ativo_Internalname, "Ativo?", "", "", lblTextblocktipodocumento_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblocktipodocumento_ativo_Visible, 1, 0, "HLP_TipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkTipoDocumento_Ativo_Internalname, StringUtil.BoolToStr( A647TipoDocumento_Ativo), "", "", chkTipoDocumento_Ativo.Visible, chkTipoDocumento_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(23, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2885e( true) ;
         }
         else
         {
            wb_table4_13_2885e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11282 */
         E11282 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A646TipoDocumento_Nome = StringUtil.Upper( cgiGet( edtTipoDocumento_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A646TipoDocumento_Nome", A646TipoDocumento_Nome);
               A647TipoDocumento_Ativo = StringUtil.StrToBool( cgiGet( chkTipoDocumento_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A647TipoDocumento_Ativo", A647TipoDocumento_Ativo);
               A645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTipoDocumento_Codigo_Internalname), ",", "."));
               n645TipoDocumento_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
               /* Read saved values. */
               Z645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z645TipoDocumento_Codigo"), ",", "."));
               Z646TipoDocumento_Nome = cgiGet( "Z646TipoDocumento_Nome");
               Z647TipoDocumento_Ativo = StringUtil.StrToBool( cgiGet( "Z647TipoDocumento_Ativo"));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( "vTIPODOCUMENTO_CODIGO"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "TipoDocumento";
               A645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTipoDocumento_Codigo_Internalname), ",", "."));
               n645TipoDocumento_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A645TipoDocumento_Codigo != Z645TipoDocumento_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("tipodocumento:[SecurityCheckFailed value for]"+"TipoDocumento_Codigo:"+context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("tipodocumento:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A645TipoDocumento_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n645TipoDocumento_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode85 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode85;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound85 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_280( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "TIPODOCUMENTO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtTipoDocumento_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11282 */
                           E11282 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12282 */
                           E12282 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12282 */
            E12282 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2885( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2885( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_280( )
      {
         BeforeValidate2885( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2885( ) ;
            }
            else
            {
               CheckExtendedTable2885( ) ;
               CloseExtendedTableCursors2885( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption280( )
      {
      }

      protected void E11282( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtTipoDocumento_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoDocumento_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoDocumento_Codigo_Visible), 5, 0)));
      }

      protected void E12282( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwtipodocumento.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2885( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z646TipoDocumento_Nome = T00283_A646TipoDocumento_Nome[0];
               Z647TipoDocumento_Ativo = T00283_A647TipoDocumento_Ativo[0];
            }
            else
            {
               Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
               Z647TipoDocumento_Ativo = A647TipoDocumento_Ativo;
            }
         }
         if ( GX_JID == -6 )
         {
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
            Z647TipoDocumento_Ativo = A647TipoDocumento_Ativo;
         }
      }

      protected void standaloneNotModal( )
      {
         edtTipoDocumento_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoDocumento_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoDocumento_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtTipoDocumento_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoDocumento_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoDocumento_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7TipoDocumento_Codigo) )
         {
            A645TipoDocumento_Codigo = AV7TipoDocumento_Codigo;
            n645TipoDocumento_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         chkTipoDocumento_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkTipoDocumento_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkTipoDocumento_Ativo.Visible), 5, 0)));
         lblTextblocktipodocumento_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblocktipodocumento_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblocktipodocumento_ativo_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A647TipoDocumento_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A647TipoDocumento_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A647TipoDocumento_Ativo", A647TipoDocumento_Ativo);
         }
      }

      protected void Load2885( )
      {
         /* Using cursor T00284 */
         pr_default.execute(2, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound85 = 1;
            A646TipoDocumento_Nome = T00284_A646TipoDocumento_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A646TipoDocumento_Nome", A646TipoDocumento_Nome);
            A647TipoDocumento_Ativo = T00284_A647TipoDocumento_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A647TipoDocumento_Ativo", A647TipoDocumento_Ativo);
            ZM2885( -6) ;
         }
         pr_default.close(2);
         OnLoadActions2885( ) ;
      }

      protected void OnLoadActions2885( )
      {
      }

      protected void CheckExtendedTable2885( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
      }

      protected void CloseExtendedTableCursors2885( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey2885( )
      {
         /* Using cursor T00285 */
         pr_default.execute(3, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound85 = 1;
         }
         else
         {
            RcdFound85 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00283 */
         pr_default.execute(1, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2885( 6) ;
            RcdFound85 = 1;
            A645TipoDocumento_Codigo = T00283_A645TipoDocumento_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
            n645TipoDocumento_Codigo = T00283_n645TipoDocumento_Codigo[0];
            A646TipoDocumento_Nome = T00283_A646TipoDocumento_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A646TipoDocumento_Nome", A646TipoDocumento_Nome);
            A647TipoDocumento_Ativo = T00283_A647TipoDocumento_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A647TipoDocumento_Ativo", A647TipoDocumento_Ativo);
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            sMode85 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2885( ) ;
            if ( AnyError == 1 )
            {
               RcdFound85 = 0;
               InitializeNonKey2885( ) ;
            }
            Gx_mode = sMode85;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound85 = 0;
            InitializeNonKey2885( ) ;
            sMode85 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode85;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2885( ) ;
         if ( RcdFound85 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound85 = 0;
         /* Using cursor T00286 */
         pr_default.execute(4, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T00286_A645TipoDocumento_Codigo[0] < A645TipoDocumento_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T00286_A645TipoDocumento_Codigo[0] > A645TipoDocumento_Codigo ) ) )
            {
               A645TipoDocumento_Codigo = T00286_A645TipoDocumento_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
               n645TipoDocumento_Codigo = T00286_n645TipoDocumento_Codigo[0];
               RcdFound85 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound85 = 0;
         /* Using cursor T00287 */
         pr_default.execute(5, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T00287_A645TipoDocumento_Codigo[0] > A645TipoDocumento_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T00287_A645TipoDocumento_Codigo[0] < A645TipoDocumento_Codigo ) ) )
            {
               A645TipoDocumento_Codigo = T00287_A645TipoDocumento_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
               n645TipoDocumento_Codigo = T00287_n645TipoDocumento_Codigo[0];
               RcdFound85 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2885( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtTipoDocumento_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2885( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound85 == 1 )
            {
               if ( A645TipoDocumento_Codigo != Z645TipoDocumento_Codigo )
               {
                  A645TipoDocumento_Codigo = Z645TipoDocumento_Codigo;
                  n645TipoDocumento_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "TIPODOCUMENTO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtTipoDocumento_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtTipoDocumento_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2885( ) ;
                  GX_FocusControl = edtTipoDocumento_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A645TipoDocumento_Codigo != Z645TipoDocumento_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtTipoDocumento_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2885( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "TIPODOCUMENTO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtTipoDocumento_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtTipoDocumento_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2885( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A645TipoDocumento_Codigo != Z645TipoDocumento_Codigo )
         {
            A645TipoDocumento_Codigo = Z645TipoDocumento_Codigo;
            n645TipoDocumento_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "TIPODOCUMENTO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtTipoDocumento_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtTipoDocumento_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2885( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00282 */
            pr_default.execute(0, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"TipoDocumento"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z646TipoDocumento_Nome, T00282_A646TipoDocumento_Nome[0]) != 0 ) || ( Z647TipoDocumento_Ativo != T00282_A647TipoDocumento_Ativo[0] ) )
            {
               if ( StringUtil.StrCmp(Z646TipoDocumento_Nome, T00282_A646TipoDocumento_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("tipodocumento:[seudo value changed for attri]"+"TipoDocumento_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z646TipoDocumento_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00282_A646TipoDocumento_Nome[0]);
               }
               if ( Z647TipoDocumento_Ativo != T00282_A647TipoDocumento_Ativo[0] )
               {
                  GXUtil.WriteLog("tipodocumento:[seudo value changed for attri]"+"TipoDocumento_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z647TipoDocumento_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T00282_A647TipoDocumento_Ativo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"TipoDocumento"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2885( )
      {
         BeforeValidate2885( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2885( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2885( 0) ;
            CheckOptimisticConcurrency2885( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2885( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2885( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T00288 */
                     pr_default.execute(6, new Object[] {A646TipoDocumento_Nome, A647TipoDocumento_Ativo});
                     A645TipoDocumento_Codigo = T00288_A645TipoDocumento_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
                     n645TipoDocumento_Codigo = T00288_n645TipoDocumento_Codigo[0];
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("TipoDocumento") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption280( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2885( ) ;
            }
            EndLevel2885( ) ;
         }
         CloseExtendedTableCursors2885( ) ;
      }

      protected void Update2885( )
      {
         BeforeValidate2885( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2885( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2885( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2885( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2885( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T00289 */
                     pr_default.execute(7, new Object[] {A646TipoDocumento_Nome, A647TipoDocumento_Ativo, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("TipoDocumento") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"TipoDocumento"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2885( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2885( ) ;
         }
         CloseExtendedTableCursors2885( ) ;
      }

      protected void DeferredUpdate2885( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2885( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2885( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2885( ) ;
            AfterConfirm2885( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2885( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002810 */
                  pr_default.execute(8, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("TipoDocumento") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode85 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2885( ) ;
         Gx_mode = sMode85;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2885( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T002811 */
            pr_default.execute(9, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Anexos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor T002812 */
            pr_default.execute(10, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Anexos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
            /* Using cursor T002813 */
            pr_default.execute(11, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Anexos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor T002814 */
            pr_default.execute(12, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Anexos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor T002815 */
            pr_default.execute(13, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Arquivos Anexos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
            /* Using cursor T002816 */
            pr_default.execute(14, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Evidencias"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
         }
      }

      protected void EndLevel2885( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2885( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "TipoDocumento");
            if ( AnyError == 0 )
            {
               ConfirmValues280( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "TipoDocumento");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2885( )
      {
         /* Scan By routine */
         /* Using cursor T002817 */
         pr_default.execute(15);
         RcdFound85 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound85 = 1;
            A645TipoDocumento_Codigo = T002817_A645TipoDocumento_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
            n645TipoDocumento_Codigo = T002817_n645TipoDocumento_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2885( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound85 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound85 = 1;
            A645TipoDocumento_Codigo = T002817_A645TipoDocumento_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
            n645TipoDocumento_Codigo = T002817_n645TipoDocumento_Codigo[0];
         }
      }

      protected void ScanEnd2885( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm2885( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2885( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2885( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2885( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2885( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2885( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2885( )
      {
         edtTipoDocumento_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoDocumento_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoDocumento_Nome_Enabled), 5, 0)));
         chkTipoDocumento_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkTipoDocumento_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkTipoDocumento_Ativo.Enabled), 5, 0)));
         edtTipoDocumento_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoDocumento_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoDocumento_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues280( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205181251198");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("tipodocumento.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7TipoDocumento_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z645TipoDocumento_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z646TipoDocumento_Nome", StringUtil.RTrim( Z646TipoDocumento_Nome));
         GxWebStd.gx_boolean_hidden_field( context, "Z647TipoDocumento_Ativo", Z647TipoDocumento_Ativo);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vTIPODOCUMENTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7TipoDocumento_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vTIPODOCUMENTO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7TipoDocumento_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "TipoDocumento";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("tipodocumento:[SendSecurityCheck value for]"+"TipoDocumento_Codigo:"+context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("tipodocumento:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("tipodocumento.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7TipoDocumento_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "TipoDocumento" ;
      }

      public override String GetPgmdesc( )
      {
         return "Tipo de Documentos" ;
      }

      protected void InitializeNonKey2885( )
      {
         A646TipoDocumento_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A646TipoDocumento_Nome", A646TipoDocumento_Nome);
         A647TipoDocumento_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A647TipoDocumento_Ativo", A647TipoDocumento_Ativo);
         Z646TipoDocumento_Nome = "";
         Z647TipoDocumento_Ativo = false;
      }

      protected void InitAll2885( )
      {
         A645TipoDocumento_Codigo = 0;
         n645TipoDocumento_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
         InitializeNonKey2885( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A647TipoDocumento_Ativo = i647TipoDocumento_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A647TipoDocumento_Ativo", A647TipoDocumento_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812511932");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("tipodocumento.js", "?202051812511932");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocktipodocumento_nome_Internalname = "TEXTBLOCKTIPODOCUMENTO_NOME";
         edtTipoDocumento_Nome_Internalname = "TIPODOCUMENTO_NOME";
         lblTextblocktipodocumento_ativo_Internalname = "TEXTBLOCKTIPODOCUMENTO_ATIVO";
         chkTipoDocumento_Ativo_Internalname = "TIPODOCUMENTO_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtTipoDocumento_Codigo_Internalname = "TIPODOCUMENTO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Tipo de Documento";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Tipo de Documentos";
         chkTipoDocumento_Ativo.Enabled = 1;
         chkTipoDocumento_Ativo.Visible = 1;
         lblTextblocktipodocumento_ativo_Visible = 1;
         edtTipoDocumento_Nome_Jsonclick = "";
         edtTipoDocumento_Nome_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtTipoDocumento_Codigo_Jsonclick = "";
         edtTipoDocumento_Codigo_Enabled = 0;
         edtTipoDocumento_Codigo_Visible = 1;
         chkTipoDocumento_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7TipoDocumento_Codigo',fld:'vTIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12282',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z646TipoDocumento_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblocktipodocumento_nome_Jsonclick = "";
         A646TipoDocumento_Nome = "";
         lblTextblocktipodocumento_ativo_Jsonclick = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode85 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         T00284_A645TipoDocumento_Codigo = new int[1] ;
         T00284_n645TipoDocumento_Codigo = new bool[] {false} ;
         T00284_A646TipoDocumento_Nome = new String[] {""} ;
         T00284_A647TipoDocumento_Ativo = new bool[] {false} ;
         T00285_A645TipoDocumento_Codigo = new int[1] ;
         T00285_n645TipoDocumento_Codigo = new bool[] {false} ;
         T00283_A645TipoDocumento_Codigo = new int[1] ;
         T00283_n645TipoDocumento_Codigo = new bool[] {false} ;
         T00283_A646TipoDocumento_Nome = new String[] {""} ;
         T00283_A647TipoDocumento_Ativo = new bool[] {false} ;
         T00286_A645TipoDocumento_Codigo = new int[1] ;
         T00286_n645TipoDocumento_Codigo = new bool[] {false} ;
         T00287_A645TipoDocumento_Codigo = new int[1] ;
         T00287_n645TipoDocumento_Codigo = new bool[] {false} ;
         T00282_A645TipoDocumento_Codigo = new int[1] ;
         T00282_n645TipoDocumento_Codigo = new bool[] {false} ;
         T00282_A646TipoDocumento_Nome = new String[] {""} ;
         T00282_A647TipoDocumento_Ativo = new bool[] {false} ;
         T00288_A645TipoDocumento_Codigo = new int[1] ;
         T00288_n645TipoDocumento_Codigo = new bool[] {false} ;
         T002811_A648Projeto_Codigo = new int[1] ;
         T002811_A2163ProjetoAnexos_Codigo = new int[1] ;
         T002812_A709ReferenciaINM_Codigo = new int[1] ;
         T002812_A2134ReferenciaINMAnexos_Codigo = new int[1] ;
         T002813_A1106Anexo_Codigo = new int[1] ;
         T002814_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         T002815_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         T002815_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         T002816_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         T002817_A645TipoDocumento_Codigo = new int[1] ;
         T002817_n645TipoDocumento_Codigo = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tipodocumento__default(),
            new Object[][] {
                new Object[] {
               T00282_A645TipoDocumento_Codigo, T00282_A646TipoDocumento_Nome, T00282_A647TipoDocumento_Ativo
               }
               , new Object[] {
               T00283_A645TipoDocumento_Codigo, T00283_A646TipoDocumento_Nome, T00283_A647TipoDocumento_Ativo
               }
               , new Object[] {
               T00284_A645TipoDocumento_Codigo, T00284_A646TipoDocumento_Nome, T00284_A647TipoDocumento_Ativo
               }
               , new Object[] {
               T00285_A645TipoDocumento_Codigo
               }
               , new Object[] {
               T00286_A645TipoDocumento_Codigo
               }
               , new Object[] {
               T00287_A645TipoDocumento_Codigo
               }
               , new Object[] {
               T00288_A645TipoDocumento_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002811_A648Projeto_Codigo, T002811_A2163ProjetoAnexos_Codigo
               }
               , new Object[] {
               T002812_A709ReferenciaINM_Codigo, T002812_A2134ReferenciaINMAnexos_Codigo
               }
               , new Object[] {
               T002813_A1106Anexo_Codigo
               }
               , new Object[] {
               T002814_A1005RegrasContagemAnexo_Codigo
               }
               , new Object[] {
               T002815_A841LoteArquivoAnexo_LoteCod, T002815_A836LoteArquivoAnexo_Data
               }
               , new Object[] {
               T002816_A586ContagemResultadoEvidencia_Codigo
               }
               , new Object[] {
               T002817_A645TipoDocumento_Codigo
               }
            }
         );
         Z647TipoDocumento_Ativo = true;
         A647TipoDocumento_Ativo = true;
         i647TipoDocumento_Ativo = true;
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound85 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int wcpOAV7TipoDocumento_Codigo ;
      private int Z645TipoDocumento_Codigo ;
      private int AV7TipoDocumento_Codigo ;
      private int trnEnded ;
      private int A645TipoDocumento_Codigo ;
      private int edtTipoDocumento_Codigo_Enabled ;
      private int edtTipoDocumento_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtTipoDocumento_Nome_Enabled ;
      private int lblTextblocktipodocumento_ativo_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z646TipoDocumento_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkTipoDocumento_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtTipoDocumento_Nome_Internalname ;
      private String edtTipoDocumento_Codigo_Internalname ;
      private String edtTipoDocumento_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocktipodocumento_nome_Internalname ;
      private String lblTextblocktipodocumento_nome_Jsonclick ;
      private String A646TipoDocumento_Nome ;
      private String edtTipoDocumento_Nome_Jsonclick ;
      private String lblTextblocktipodocumento_ativo_Internalname ;
      private String lblTextblocktipodocumento_ativo_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode85 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool Z647TipoDocumento_Ativo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A647TipoDocumento_Ativo ;
      private bool n645TipoDocumento_Codigo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool i647TipoDocumento_Ativo ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkTipoDocumento_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] T00284_A645TipoDocumento_Codigo ;
      private bool[] T00284_n645TipoDocumento_Codigo ;
      private String[] T00284_A646TipoDocumento_Nome ;
      private bool[] T00284_A647TipoDocumento_Ativo ;
      private int[] T00285_A645TipoDocumento_Codigo ;
      private bool[] T00285_n645TipoDocumento_Codigo ;
      private int[] T00283_A645TipoDocumento_Codigo ;
      private bool[] T00283_n645TipoDocumento_Codigo ;
      private String[] T00283_A646TipoDocumento_Nome ;
      private bool[] T00283_A647TipoDocumento_Ativo ;
      private int[] T00286_A645TipoDocumento_Codigo ;
      private bool[] T00286_n645TipoDocumento_Codigo ;
      private int[] T00287_A645TipoDocumento_Codigo ;
      private bool[] T00287_n645TipoDocumento_Codigo ;
      private int[] T00282_A645TipoDocumento_Codigo ;
      private bool[] T00282_n645TipoDocumento_Codigo ;
      private String[] T00282_A646TipoDocumento_Nome ;
      private bool[] T00282_A647TipoDocumento_Ativo ;
      private int[] T00288_A645TipoDocumento_Codigo ;
      private bool[] T00288_n645TipoDocumento_Codigo ;
      private int[] T002811_A648Projeto_Codigo ;
      private int[] T002811_A2163ProjetoAnexos_Codigo ;
      private int[] T002812_A709ReferenciaINM_Codigo ;
      private int[] T002812_A2134ReferenciaINMAnexos_Codigo ;
      private int[] T002813_A1106Anexo_Codigo ;
      private int[] T002814_A1005RegrasContagemAnexo_Codigo ;
      private int[] T002815_A841LoteArquivoAnexo_LoteCod ;
      private DateTime[] T002815_A836LoteArquivoAnexo_Data ;
      private int[] T002816_A586ContagemResultadoEvidencia_Codigo ;
      private int[] T002817_A645TipoDocumento_Codigo ;
      private bool[] T002817_n645TipoDocumento_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class tipodocumento__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00284 ;
          prmT00284 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00285 ;
          prmT00285 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00283 ;
          prmT00283 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00286 ;
          prmT00286 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00287 ;
          prmT00287 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00282 ;
          prmT00282 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00288 ;
          prmT00288 = new Object[] {
          new Object[] {"@TipoDocumento_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Ativo",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT00289 ;
          prmT00289 = new Object[] {
          new Object[] {"@TipoDocumento_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002810 ;
          prmT002810 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002811 ;
          prmT002811 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002812 ;
          prmT002812 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002813 ;
          prmT002813 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002814 ;
          prmT002814 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002815 ;
          prmT002815 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002816 ;
          prmT002816 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002817 ;
          prmT002817 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T00282", "SELECT [TipoDocumento_Codigo], [TipoDocumento_Nome], [TipoDocumento_Ativo] FROM [TipoDocumento] WITH (UPDLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00282,1,0,true,false )
             ,new CursorDef("T00283", "SELECT [TipoDocumento_Codigo], [TipoDocumento_Nome], [TipoDocumento_Ativo] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00283,1,0,true,false )
             ,new CursorDef("T00284", "SELECT TM1.[TipoDocumento_Codigo], TM1.[TipoDocumento_Nome], TM1.[TipoDocumento_Ativo] FROM [TipoDocumento] TM1 WITH (NOLOCK) WHERE TM1.[TipoDocumento_Codigo] = @TipoDocumento_Codigo ORDER BY TM1.[TipoDocumento_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00284,100,0,true,false )
             ,new CursorDef("T00285", "SELECT [TipoDocumento_Codigo] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00285,1,0,true,false )
             ,new CursorDef("T00286", "SELECT TOP 1 [TipoDocumento_Codigo] FROM [TipoDocumento] WITH (NOLOCK) WHERE ( [TipoDocumento_Codigo] > @TipoDocumento_Codigo) ORDER BY [TipoDocumento_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00286,1,0,true,true )
             ,new CursorDef("T00287", "SELECT TOP 1 [TipoDocumento_Codigo] FROM [TipoDocumento] WITH (NOLOCK) WHERE ( [TipoDocumento_Codigo] < @TipoDocumento_Codigo) ORDER BY [TipoDocumento_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00287,1,0,true,true )
             ,new CursorDef("T00288", "INSERT INTO [TipoDocumento]([TipoDocumento_Nome], [TipoDocumento_Ativo]) VALUES(@TipoDocumento_Nome, @TipoDocumento_Ativo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT00288)
             ,new CursorDef("T00289", "UPDATE [TipoDocumento] SET [TipoDocumento_Nome]=@TipoDocumento_Nome, [TipoDocumento_Ativo]=@TipoDocumento_Ativo  WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo", GxErrorMask.GX_NOMASK,prmT00289)
             ,new CursorDef("T002810", "DELETE FROM [TipoDocumento]  WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo", GxErrorMask.GX_NOMASK,prmT002810)
             ,new CursorDef("T002811", "SELECT TOP 1 [Projeto_Codigo], [ProjetoAnexos_Codigo] FROM [ProjetoAnexos] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002811,1,0,true,true )
             ,new CursorDef("T002812", "SELECT TOP 1 [ReferenciaINM_Codigo], [ReferenciaINMAnexos_Codigo] FROM [ReferenciaINMAnexos] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002812,1,0,true,true )
             ,new CursorDef("T002813", "SELECT TOP 1 [Anexo_Codigo] FROM [Anexos] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002813,1,0,true,true )
             ,new CursorDef("T002814", "SELECT TOP 1 [RegrasContagemAnexo_Codigo] FROM [RegrasContagemAnexos] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002814,1,0,true,true )
             ,new CursorDef("T002815", "SELECT TOP 1 [LoteArquivoAnexo_LoteCod], [LoteArquivoAnexo_Data] FROM [LoteArquivoAnexo] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002815,1,0,true,true )
             ,new CursorDef("T002816", "SELECT TOP 1 [ContagemResultadoEvidencia_Codigo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002816,1,0,true,true )
             ,new CursorDef("T002817", "SELECT [TipoDocumento_Codigo] FROM [TipoDocumento] WITH (NOLOCK) ORDER BY [TipoDocumento_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002817,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[3]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
