/*
               File: WP_SelArquivo
        Description: Anexar arquivo:
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:39:3.81
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_selarquivo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_selarquivo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_selarquivo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavTipodocumento_codigo = new GXCombobox();
         cmbavArtefatos_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vTIPODOCUMENTO_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvTIPODOCUMENTO_CODIGOF42( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAF42( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTF42( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282339386");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_selarquivo.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_ARQUIVOEVIDENCIA", AV12Sdt_ArquivoEvidencia);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_ARQUIVOEVIDENCIA", AV12Sdt_ArquivoEvidencia);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV20WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV20WWPContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_CONTAGEMRESULTADOEVIDENCIAS", AV13Sdt_ContagemResultadoEvidencias);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_CONTAGEMRESULTADOEVIDENCIAS", AV13Sdt_ContagemResultadoEvidencias);
         }
         GXCCtlgxBlob = "vBLOB" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV5Blob);
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEF42( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTF42( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_selarquivo.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_SelArquivo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Anexar arquivo:" ;
      }

      protected void WBF40( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_F42( true) ;
         }
         else
         {
            wb_table1_2_F42( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_F42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "<p></p>") ;
         }
         wbLoad = true;
      }

      protected void STARTF42( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Anexar arquivo:", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPF40( ) ;
      }

      protected void WSF42( )
      {
         STARTF42( ) ;
         EVTF42( ) ;
      }

      protected void EVTF42( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11F42 */
                              E11F42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ENTER'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12F42 */
                              E12F42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13F42 */
                              E13F42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12F42 */
                                    E12F42 ();
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEF42( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAF42( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavTipodocumento_codigo.Name = "vTIPODOCUMENTO_CODIGO";
            dynavTipodocumento_codigo.WebTags = "";
            cmbavArtefatos_codigo.Name = "vARTEFATOS_CODIGO";
            cmbavArtefatos_codigo.WebTags = "";
            cmbavArtefatos_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavArtefatos_codigo.ItemCount > 0 )
            {
               AV21Artefatos_Codigo = (int)(NumberUtil.Val( cmbavArtefatos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21Artefatos_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Artefatos_Codigo), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavBlob_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvTIPODOCUMENTO_CODIGOF42( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvTIPODOCUMENTO_CODIGO_dataF42( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvTIPODOCUMENTO_CODIGO_htmlF42( )
      {
         int gxdynajaxvalue ;
         GXDLVvTIPODOCUMENTO_CODIGO_dataF42( ) ;
         gxdynajaxindex = 1;
         dynavTipodocumento_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavTipodocumento_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavTipodocumento_codigo.ItemCount > 0 )
         {
            AV14TipoDocumento_Codigo = (int)(NumberUtil.Val( dynavTipodocumento_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14TipoDocumento_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14TipoDocumento_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvTIPODOCUMENTO_CODIGO_dataF42( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00F42 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00F42_A645TipoDocumento_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00F42_A646TipoDocumento_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavTipodocumento_codigo.ItemCount > 0 )
         {
            AV14TipoDocumento_Codigo = (int)(NumberUtil.Val( dynavTipodocumento_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14TipoDocumento_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14TipoDocumento_Codigo), 6, 0)));
         }
         if ( cmbavArtefatos_codigo.ItemCount > 0 )
         {
            AV21Artefatos_Codigo = (int)(NumberUtil.Val( cmbavArtefatos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21Artefatos_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Artefatos_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFF42( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFF42( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E13F42 */
            E13F42 ();
            WBF40( ) ;
         }
      }

      protected void STRUPF40( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         GXVvTIPODOCUMENTO_CODIGO_htmlF42( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11F42 */
         E11F42 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV5Blob = cgiGet( edtavBlob_Internalname);
            AV19Link = cgiGet( edtavLink_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Link", AV19Link);
            dynavTipodocumento_codigo.CurrentValue = cgiGet( dynavTipodocumento_codigo_Internalname);
            AV14TipoDocumento_Codigo = (int)(NumberUtil.Val( cgiGet( dynavTipodocumento_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14TipoDocumento_Codigo), 6, 0)));
            cmbavArtefatos_codigo.CurrentValue = cgiGet( cmbavArtefatos_codigo_Internalname);
            AV21Artefatos_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavArtefatos_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Artefatos_Codigo), 6, 0)));
            AV8Descricao = cgiGet( edtavDescricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Descricao", AV8Descricao);
            AV11FileName = cgiGet( edtavFilename_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11FileName", AV11FileName);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV5Blob)) )
            {
               GXCCtlgxBlob = "vBLOB" + "_gxBlob";
               AV5Blob = cgiGet( GXCCtlgxBlob);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvTIPODOCUMENTO_CODIGO_htmlF42( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11F42 */
         E11F42 ();
         if (returnInSub) return;
      }

      protected void E11F42( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV20WWPContext) ;
         bttConfirm_Visible = (AV20WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttConfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttConfirm_Visible), 5, 0)));
         Form.Jscriptsrc.Add("http://code.jquery.com/jquery-latest.js\">") ;
         Form.Headerrawhtml = "<script type=\"text/javascript\">";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(document).ready(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vBLOB\").blur(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"var nome = $(\"#vBLOB\").val();";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vFILENAME\").val(nome);";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"</script>";
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         edtavFilename_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFilename_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFilename_Visible), 5, 0)));
         AV13Sdt_ContagemResultadoEvidencias.FromXml(AV15Websession.Get("ArquivosEvd"), "");
         /* Execute user subroutine: 'ARTEFATOS' */
         S112 ();
         if (returnInSub) return;
      }

      protected void E12F42( )
      {
         /* 'Enter' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV5Blob)) && String.IsNullOrEmpty(StringUtil.RTrim( AV19Link)) )
         {
            GX_msglist.addItem("Selecione um Arquivo ou informe um Link que deseja anexar!");
         }
         else if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5Blob)) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Link)) )
         {
            GX_msglist.addItem("Arquivo e Link n�o podem ser anexados na mesma a��o!");
         }
         else if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Link)) && (0==AV14TipoDocumento_Codigo) && (0==AV21Artefatos_Codigo) )
         {
            if ( cmbavArtefatos_codigo.ItemCount > 1 )
            {
               GX_msglist.addItem("Informe o Tipo de Documento ou Artefato associado ao Link a ser anexado!");
            }
            else
            {
               GX_msglist.addItem("Informe o Tipo de Documento associado ao Link a ser anexado!");
            }
         }
         else if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5Blob)) && (0==AV14TipoDocumento_Codigo) && (0==AV21Artefatos_Codigo) )
         {
            if ( cmbavArtefatos_codigo.ItemCount > 1 )
            {
               GX_msglist.addItem("Informe o Tipo de Documento ou Artefato associado ao arquivo a ser anexado!");
            }
            else
            {
               GX_msglist.addItem("Informe o Tipo de Documento associado ao arquivo a ser anexado!");
            }
         }
         else
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV5Blob)) )
            {
               AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq = AV19Link;
            }
            else
            {
               AV24Arquivo = AV5Blob;
               AV11FileName = StringUtil.Substring( AV11FileName, StringUtil.StringSearchRev( AV11FileName, "\\", -1)+1, 255);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11FileName", AV11FileName);
               AV9Ext = StringUtil.Trim( StringUtil.Substring( AV11FileName, StringUtil.StringSearchRev( AV11FileName, ".", -1)+1, 4));
               AV11FileName = StringUtil.Substring( AV11FileName, 1, StringUtil.StringSearchRev( AV11FileName, ".", -1)-1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11FileName", AV11FileName);
               AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_arquivo = AV24Arquivo;
               AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq = AV11FileName;
               AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_tipoarq = AV9Ext;
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Link)) )
            {
               AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_link = AV19Link;
            }
            AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_descricao = AV8Descricao;
            AV12Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_codigo = AV14TipoDocumento_Codigo;
            AV12Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_nome = StringUtil.Str( (decimal)(AV14TipoDocumento_Codigo), 6, 0);
            AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_owner = AV20WWPContext.gxTpr_Userid;
            AV12Sdt_ArquivoEvidencia.gxTpr_Artefatos_codigo = AV21Artefatos_Codigo;
            AV13Sdt_ContagemResultadoEvidencias.Add(AV12Sdt_ArquivoEvidencia, 0);
            AV12Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
            AV15Websession.Set("ArquivosEvd", AV13Sdt_ContagemResultadoEvidencias.ToXml(false, true, "SDT_ContagemResultadoEvidencias", "GxEv3Up14_MeetrikaVs3"));
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12Sdt_ArquivoEvidencia", AV12Sdt_ArquivoEvidencia);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV13Sdt_ContagemResultadoEvidencias", AV13Sdt_ContagemResultadoEvidencias);
      }

      protected void S112( )
      {
         /* 'ARTEFATOS' Routine */
         AV25Codigo = (int)(NumberUtil.Val( AV15Websession.Get("Codigo"), "."));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Codigo), 6, 0)));
         /* Using cursor H00F43 */
         pr_default.execute(1, new Object[] {AV25Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1772ContagemResultadoArtefato_OSCod = H00F43_A1772ContagemResultadoArtefato_OSCod[0];
            A1773ContagemResultadoArtefato_AnxCod = H00F43_A1773ContagemResultadoArtefato_AnxCod[0];
            n1773ContagemResultadoArtefato_AnxCod = H00F43_n1773ContagemResultadoArtefato_AnxCod[0];
            A1770ContagemResultadoArtefato_EvdCod = H00F43_A1770ContagemResultadoArtefato_EvdCod[0];
            n1770ContagemResultadoArtefato_EvdCod = H00F43_n1770ContagemResultadoArtefato_EvdCod[0];
            A1771ContagemResultadoArtefato_ArtefatoCod = H00F43_A1771ContagemResultadoArtefato_ArtefatoCod[0];
            A1751Artefatos_Descricao = H00F43_A1751Artefatos_Descricao[0];
            n1751Artefatos_Descricao = H00F43_n1751Artefatos_Descricao[0];
            A1751Artefatos_Descricao = H00F43_A1751Artefatos_Descricao[0];
            n1751Artefatos_Descricao = H00F43_n1751Artefatos_Descricao[0];
            AV22Anexado = (bool)((A1770ContagemResultadoArtefato_EvdCod>0)||(A1773ContagemResultadoArtefato_AnxCod>0));
            if ( ! AV22Anexado )
            {
               AV21Artefatos_Codigo = A1771ContagemResultadoArtefato_ArtefatoCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Artefatos_Codigo), 6, 0)));
               /* Execute user subroutine: 'MEMORYSEARCH' */
               S123 ();
               if ( returnInSub )
               {
                  pr_default.close(1);
                  returnInSub = true;
                  if (true) return;
               }
            }
            cmbavArtefatos_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1771ContagemResultadoArtefato_ArtefatoCod), 6, 0)), A1751Artefatos_Descricao+(AV22Anexado ? " -" : " ?"), 0);
            pr_default.readNext(1);
         }
         pr_default.close(1);
         AV21Artefatos_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Artefatos_Codigo), 6, 0)));
         lblTbartefato_Visible = ((cmbavArtefatos_codigo.ItemCount>1) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbartefato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbartefato_Visible), 5, 0)));
         cmbavArtefatos_codigo.Visible = ((cmbavArtefatos_codigo.ItemCount>1) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavArtefatos_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavArtefatos_codigo.Visible), 5, 0)));
      }

      protected void S123( )
      {
         /* 'MEMORYSEARCH' Routine */
         AV29GXV1 = 1;
         while ( AV29GXV1 <= AV13Sdt_ContagemResultadoEvidencias.Count )
         {
            AV12Sdt_ArquivoEvidencia = ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV13Sdt_ContagemResultadoEvidencias.Item(AV29GXV1));
            if ( AV12Sdt_ArquivoEvidencia.gxTpr_Artefatos_codigo == AV21Artefatos_Codigo )
            {
               AV22Anexado = true;
               if (true) break;
            }
            AV29GXV1 = (int)(AV29GXV1+1);
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E13F42( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_F42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "left", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left")+"\" class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='DataContentCell'>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"left\" colspan=\"4\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left;height:24px")+"\" class='DataContentCell'>") ;
            ClassString = "Image";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'',false,'',0)\"";
            edtavBlob_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5Blob)) )
            {
               gxblobfileaux.Source = AV5Blob;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavBlob_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavBlob_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV5Blob = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV5Blob));
                  edtavBlob_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV5Blob));
            }
            GxWebStd.gx_blob_field( context, edtavBlob_Internalname, StringUtil.RTrim( AV5Blob), context.PathToRelativeUrl( AV5Blob), (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Filetype)) ? AV5Blob : edtavBlob_Filetype)) : edtavBlob_Contenttype), false, "", edtavBlob_Parameters, 0, 1, 1, "", "", 0, 0, 0, "px", 18, "px", 0, 0, 0, edtavBlob_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,8);\"", "", "", "HLP_WP_SelArquivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:36px")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Link:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SelArquivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLink_Internalname, AV19Link, AV19Link, TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLink_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 2097152, 0, 1, 0, 1, 0, -1, true, "", "left", false, "HLP_WP_SelArquivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Tipo Documento:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SelArquivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavTipodocumento_codigo, dynavTipodocumento_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14TipoDocumento_Codigo), 6, 0)), 1, dynavTipodocumento_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WP_SelArquivo.htm");
            dynavTipodocumento_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14TipoDocumento_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodocumento_codigo_Internalname, "Values", (String)(dynavTipodocumento_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbartefato_Internalname, "Artefato:", "", "", lblTbartefato_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", lblTbartefato_Visible, 1, 0, "HLP_WP_SelArquivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavArtefatos_codigo, cmbavArtefatos_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21Artefatos_Codigo), 6, 0)), 1, cmbavArtefatos_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavArtefatos_codigo.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "", true, "HLP_WP_SelArquivo.htm");
            cmbavArtefatos_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21Artefatos_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavArtefatos_codigo_Internalname, "Values", (String)(cmbavArtefatos_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Descri��o:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SelArquivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDescricao_Internalname, AV8Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", 0, 1, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_WP_SelArquivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"4\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:6px")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFilename_Internalname, AV11FileName, StringUtil.RTrim( context.localUtil.Format( AV11FileName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFilename_Jsonclick, 0, "Attribute", "", "", "", edtavFilename_Visible, 1, 0, "text", "", 0, "px", 1, "px", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_SelArquivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"4\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:36px")+"\">") ;
            context.WriteHtmlText( "<p>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttConfirm_Internalname, "", "Confirmar", bttConfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttConfirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'ENTER\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_SelArquivo.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttCancel_Internalname, "", "Fechar", bttCancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_SelArquivo.htm");
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_SelArquivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_F42e( true) ;
         }
         else
         {
            wb_table1_2_F42e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAF42( ) ;
         WSF42( ) ;
         WEF42( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282339416");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_selarquivo.js", "?20204282339416");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         edtavBlob_Internalname = "vBLOB";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavLink_Internalname = "vLINK";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         dynavTipodocumento_codigo_Internalname = "vTIPODOCUMENTO_CODIGO";
         lblTbartefato_Internalname = "TBARTEFATO";
         cmbavArtefatos_codigo_Internalname = "vARTEFATOS_CODIGO";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavDescricao_Internalname = "vDESCRICAO";
         edtavFilename_Internalname = "vFILENAME";
         bttConfirm_Internalname = "CONFIRM";
         bttCancel_Internalname = "CANCEL";
         lblTbjava_Internalname = "TBJAVA";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         lblTbjava_Visible = 1;
         bttConfirm_Visible = 1;
         edtavFilename_Jsonclick = "";
         cmbavArtefatos_codigo_Jsonclick = "";
         lblTbartefato_Visible = 1;
         dynavTipodocumento_codigo_Jsonclick = "";
         edtavLink_Jsonclick = "";
         edtavBlob_Jsonclick = "";
         edtavBlob_Parameters = "";
         edtavBlob_Contenttype = "";
         edtavBlob_Filetype = "";
         cmbavArtefatos_codigo.Visible = 1;
         edtavFilename_Visible = 1;
         lblTbjava_Caption = "Script";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Anexar arquivo:";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'ENTER'","{handler:'E12F42',iparms:[{av:'AV5Blob',fld:'vBLOB',pic:'',nv:''},{av:'AV19Link',fld:'vLINK',pic:'',nv:''},{av:'AV14TipoDocumento_Codigo',fld:'vTIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12Sdt_ArquivoEvidencia',fld:'vSDT_ARQUIVOEVIDENCIA',pic:'',nv:null},{av:'AV11FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV8Descricao',fld:'vDESCRICAO',pic:'',nv:''},{av:'AV20WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV13Sdt_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',pic:'',nv:null}],oparms:[{av:'AV11FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV12Sdt_ArquivoEvidencia',fld:'vSDT_ARQUIVOEVIDENCIA',pic:'',nv:null},{av:'AV13Sdt_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV12Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
         AV20WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV13Sdt_ContagemResultadoEvidencias = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_MeetrikaVs3", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         GXCCtlgxBlob = "";
         AV5Blob = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00F42_A645TipoDocumento_Codigo = new int[1] ;
         H00F42_A646TipoDocumento_Nome = new String[] {""} ;
         H00F42_A647TipoDocumento_Ativo = new bool[] {false} ;
         AV19Link = "";
         AV8Descricao = "";
         AV11FileName = "";
         AV15Websession = context.GetSession();
         AV24Arquivo = "";
         AV9Ext = "";
         H00F43_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         H00F43_A1772ContagemResultadoArtefato_OSCod = new int[1] ;
         H00F43_A1773ContagemResultadoArtefato_AnxCod = new int[1] ;
         H00F43_n1773ContagemResultadoArtefato_AnxCod = new bool[] {false} ;
         H00F43_A1770ContagemResultadoArtefato_EvdCod = new int[1] ;
         H00F43_n1770ContagemResultadoArtefato_EvdCod = new bool[] {false} ;
         H00F43_A1771ContagemResultadoArtefato_ArtefatoCod = new int[1] ;
         H00F43_A1751Artefatos_Descricao = new String[] {""} ;
         H00F43_n1751Artefatos_Descricao = new bool[] {false} ;
         A1751Artefatos_Descricao = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         lblTextblock3_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTbartefato_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         bttConfirm_Jsonclick = "";
         bttCancel_Jsonclick = "";
         lblTbjava_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_selarquivo__default(),
            new Object[][] {
                new Object[] {
               H00F42_A645TipoDocumento_Codigo, H00F42_A646TipoDocumento_Nome, H00F42_A647TipoDocumento_Ativo
               }
               , new Object[] {
               H00F43_A1769ContagemResultadoArtefato_Codigo, H00F43_A1772ContagemResultadoArtefato_OSCod, H00F43_A1773ContagemResultadoArtefato_AnxCod, H00F43_n1773ContagemResultadoArtefato_AnxCod, H00F43_A1770ContagemResultadoArtefato_EvdCod, H00F43_n1770ContagemResultadoArtefato_EvdCod, H00F43_A1771ContagemResultadoArtefato_ArtefatoCod, H00F43_A1751Artefatos_Descricao, H00F43_n1751Artefatos_Descricao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV21Artefatos_Codigo ;
      private int gxdynajaxindex ;
      private int AV14TipoDocumento_Codigo ;
      private int bttConfirm_Visible ;
      private int lblTbjava_Visible ;
      private int edtavFilename_Visible ;
      private int AV25Codigo ;
      private int A1772ContagemResultadoArtefato_OSCod ;
      private int A1773ContagemResultadoArtefato_AnxCod ;
      private int A1770ContagemResultadoArtefato_EvdCod ;
      private int A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int lblTbartefato_Visible ;
      private int AV29GXV1 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXCCtlgxBlob ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavBlob_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavLink_Internalname ;
      private String dynavTipodocumento_codigo_Internalname ;
      private String cmbavArtefatos_codigo_Internalname ;
      private String edtavDescricao_Internalname ;
      private String edtavFilename_Internalname ;
      private String bttConfirm_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String AV24Arquivo ;
      private String AV9Ext ;
      private String lblTbartefato_Internalname ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String edtavBlob_Filetype ;
      private String edtavBlob_Contenttype ;
      private String edtavBlob_Parameters ;
      private String edtavBlob_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavLink_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String dynavTipodocumento_codigo_Jsonclick ;
      private String lblTbartefato_Jsonclick ;
      private String cmbavArtefatos_codigo_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavFilename_Jsonclick ;
      private String bttConfirm_Jsonclick ;
      private String bttCancel_Internalname ;
      private String bttCancel_Jsonclick ;
      private String lblTbjava_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1773ContagemResultadoArtefato_AnxCod ;
      private bool n1770ContagemResultadoArtefato_EvdCod ;
      private bool n1751Artefatos_Descricao ;
      private bool AV22Anexado ;
      private String AV19Link ;
      private String AV8Descricao ;
      private String AV11FileName ;
      private String A1751Artefatos_Descricao ;
      private String AV5Blob ;
      private IGxSession AV15Websession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxFile gxblobfileaux ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavTipodocumento_codigo ;
      private GXCombobox cmbavArtefatos_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00F42_A645TipoDocumento_Codigo ;
      private String[] H00F42_A646TipoDocumento_Nome ;
      private bool[] H00F42_A647TipoDocumento_Ativo ;
      private int[] H00F43_A1769ContagemResultadoArtefato_Codigo ;
      private int[] H00F43_A1772ContagemResultadoArtefato_OSCod ;
      private int[] H00F43_A1773ContagemResultadoArtefato_AnxCod ;
      private bool[] H00F43_n1773ContagemResultadoArtefato_AnxCod ;
      private int[] H00F43_A1770ContagemResultadoArtefato_EvdCod ;
      private bool[] H00F43_n1770ContagemResultadoArtefato_EvdCod ;
      private int[] H00F43_A1771ContagemResultadoArtefato_ArtefatoCod ;
      private String[] H00F43_A1751Artefatos_Descricao ;
      private bool[] H00F43_n1751Artefatos_Descricao ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV13Sdt_ContagemResultadoEvidencias ;
      private GXWebForm Form ;
      private SdtSDT_ContagemResultadoEvidencias_Arquivo AV12Sdt_ArquivoEvidencia ;
      private wwpbaseobjects.SdtWWPContext AV20WWPContext ;
   }

   public class wp_selarquivo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00F42 ;
          prmH00F42 = new Object[] {
          } ;
          Object[] prmH00F43 ;
          prmH00F43 = new Object[] {
          new Object[] {"@AV25Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00F42", "SELECT [TipoDocumento_Codigo], [TipoDocumento_Nome], [TipoDocumento_Ativo] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Ativo] = 1 ORDER BY [TipoDocumento_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00F42,0,0,true,false )
             ,new CursorDef("H00F43", "SELECT T1.[ContagemResultadoArtefato_Codigo], T1.[ContagemResultadoArtefato_OSCod], T1.[ContagemResultadoArtefato_AnxCod], T1.[ContagemResultadoArtefato_EvdCod], T1.[ContagemResultadoArtefato_ArtefatoCod] AS ContagemResultadoArtefato_ArtefatoCod, T2.[Artefatos_Descricao] FROM ([ContagemResultadoArtefato] T1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = T1.[ContagemResultadoArtefato_ArtefatoCod]) WHERE T1.[ContagemResultadoArtefato_OSCod] = @AV25Codigo ORDER BY T1.[ContagemResultadoArtefato_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00F43,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
