/*
               File: WP_BackLog
        Description: Back Log
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:25:45.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_backlog : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_backlog( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_backlog( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo )
      {
         this.AV7Codigo = aP0_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavUsuario_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vUSUARIO_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvUSUARIO_CODIGOMM2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Codigo), 6, 0)));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAMM2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTMM2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216254513");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_backlog.aspx") + "?" + UrlEncode("" +AV7Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV13WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV13WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vSTATUSDMN", StringUtil.RTrim( AV12StatusDmn));
         GxWebStd.gx_hidden_field( context, "vPRAZOENTREGA", context.localUtil.TToC( AV10PrazoEntrega, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "vRESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vDATAHORA", GetSecureSignedToken( "", context.localUtil.Format( AV5DataHora, "99/99/99 99:99")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEMM2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTMM2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_backlog.aspx") + "?" + UrlEncode("" +AV7Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "WP_BackLog" ;
      }

      public override String GetPgmdesc( )
      {
         return "Back Log" ;
      }

      protected void WBMM0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_MM2( true) ;
         }
         else
         {
            wb_table1_2_MM2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MM2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTMM2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Back Log", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPMM0( ) ;
      }

      protected void WSMM2( )
      {
         STARTMM2( ) ;
         EVTMM2( ) ;
      }

      protected void EVTMM2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11MM2 */
                              E11MM2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12MM2 */
                                    E12MM2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13MM2 */
                              E13MM2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14MM2 */
                              E14MM2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEMM2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAMM2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavUsuario_codigo.Name = "vUSUARIO_CODIGO";
            dynavUsuario_codigo.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavDatahora_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvUSUARIO_CODIGOMM2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvUSUARIO_CODIGO_dataMM2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvUSUARIO_CODIGO_htmlMM2( )
      {
         int gxdynajaxvalue ;
         GXDLVvUSUARIO_CODIGO_dataMM2( ) ;
         gxdynajaxindex = 1;
         dynavUsuario_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavUsuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavUsuario_codigo.ItemCount > 0 )
         {
            AV6Usuario_Codigo = (int)(NumberUtil.Val( dynavUsuario_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6Usuario_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Usuario_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvUSUARIO_CODIGO_dataMM2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00MM2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00MM2_A1Usuario_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00MM2_A58Usuario_PessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavUsuario_codigo.ItemCount > 0 )
         {
            AV6Usuario_Codigo = (int)(NumberUtil.Val( dynavUsuario_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6Usuario_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Usuario_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFMM2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavDatahora_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDatahora_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDatahora_Enabled), 5, 0)));
         dynavUsuario_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUsuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUsuario_codigo.Enabled), 5, 0)));
      }

      protected void RFMM2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E14MM2 */
            E14MM2 ();
            WBMM0( ) ;
         }
      }

      protected void STRUPMM0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavDatahora_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDatahora_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDatahora_Enabled), 5, 0)));
         dynavUsuario_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUsuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUsuario_codigo.Enabled), 5, 0)));
         GXVvUSUARIO_CODIGO_htmlMM2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11MM2 */
         E11MM2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( context.localUtil.VCDateTime( cgiGet( edtavDatahora_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data Hora"}), 1, "vDATAHORA");
               GX_FocusControl = edtavDatahora_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV5DataHora = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5DataHora", context.localUtil.TToC( AV5DataHora, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDATAHORA", GetSecureSignedToken( "", context.localUtil.Format( AV5DataHora, "99/99/99 99:99")));
            }
            else
            {
               AV5DataHora = context.localUtil.CToT( cgiGet( edtavDatahora_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5DataHora", context.localUtil.TToC( AV5DataHora, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDATAHORA", GetSecureSignedToken( "", context.localUtil.Format( AV5DataHora, "99/99/99 99:99")));
            }
            dynavUsuario_codigo.CurrentValue = cgiGet( dynavUsuario_codigo_Internalname);
            AV6Usuario_Codigo = (int)(NumberUtil.Val( cgiGet( dynavUsuario_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Usuario_Codigo), 6, 0)));
            AV9Nota = cgiGet( edtavNota_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Nota", AV9Nota);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvUSUARIO_CODIGO_htmlMM2( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11MM2 */
         E11MM2 ();
         if (returnInSub) return;
      }

      protected void E11MM2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV13WWPContext) ;
         AV6Usuario_Codigo = AV13WWPContext.gxTpr_Userid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Usuario_Codigo), 6, 0)));
         AV5DataHora = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5DataHora", context.localUtil.TToC( AV5DataHora, 8, 5, 0, 3, "/", ":", " "));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDATAHORA", GetSecureSignedToken( "", context.localUtil.Format( AV5DataHora, "99/99/99 99:99")));
         dynavUsuario_codigo.Enabled = (AV13WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUsuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUsuario_codigo.Enabled), 5, 0)));
         /* Using cursor H00MM3 */
         pr_default.execute(1, new Object[] {AV7Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A456ContagemResultado_Codigo = H00MM3_A456ContagemResultado_Codigo[0];
            A493ContagemResultado_DemandaFM = H00MM3_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = H00MM3_n493ContagemResultado_DemandaFM[0];
            A484ContagemResultado_StatusDmn = H00MM3_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00MM3_n484ContagemResultado_StatusDmn[0];
            A472ContagemResultado_DataEntrega = H00MM3_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = H00MM3_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = H00MM3_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = H00MM3_n912ContagemResultado_HoraEntrega[0];
            A890ContagemResultado_Responsavel = H00MM3_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = H00MM3_n890ContagemResultado_Responsavel[0];
            Form.Caption = "Envio pro Back Log da OS "+A493ContagemResultado_DemandaFM;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV12StatusDmn = A484ContagemResultado_StatusDmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12StatusDmn", AV12StatusDmn);
            AV10PrazoEntrega = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10PrazoEntrega", context.localUtil.TToC( AV10PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            AV10PrazoEntrega = DateTimeUtil.TAdd( AV10PrazoEntrega, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10PrazoEntrega", context.localUtil.TToC( AV10PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            AV10PrazoEntrega = DateTimeUtil.TAdd( AV10PrazoEntrega, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10PrazoEntrega", context.localUtil.TToC( AV10PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            AV14Responsavel = A890ContagemResultado_Responsavel;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Responsavel), 6, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         bttBtnenter_Visible = (AV13WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      public void GXEnter( )
      {
         /* Execute user event: E12MM2 */
         E12MM2 ();
         if (returnInSub) return;
      }

      protected void E12MM2( )
      {
         /* Enter Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV9Nota)) )
         {
            GX_msglist.addItem("Raz�o do env�o pro Back Log � obrigat�ria!");
         }
         else
         {
            new prc_inslogresponsavel(context ).execute( ref  AV7Codigo,  0,  "B",  "D",  AV13WWPContext.gxTpr_Userid,  0,  AV12StatusDmn,  "B",  AV9Nota,  AV10PrazoEntrega,  true) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12StatusDmn", AV12StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Nota", AV9Nota);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10PrazoEntrega", context.localUtil.TToC( AV10PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            new prc_alterastatusdmn(context ).execute( ref  AV7Codigo,  "B",  AV14Responsavel) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Responsavel), 6, 0)));
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13MM2( )
      {
         /* 'DoFechar' Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E14MM2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_MM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_MM2( true) ;
         }
         else
         {
            wb_table2_8_MM2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_MM2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table3_25_MM2( true) ;
         }
         else
         {
            wb_table3_25_MM2( false) ;
         }
         return  ;
      }

      protected void wb_table3_25_MM2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_BackLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MM2e( true) ;
         }
         else
         {
            wb_table1_2_MM2e( false) ;
         }
      }

      protected void wb_table3_25_MM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 15, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnenter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_BackLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_BackLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_25_MM2e( true) ;
         }
         else
         {
            wb_table3_25_MM2e( false) ;
         }
      }

      protected void wb_table2_8_MM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockdatahora_Internalname, "Data", "", "", lblTextblockdatahora_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_BackLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDatahora_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDatahora_Internalname, context.localUtil.TToC( AV5DataHora, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV5DataHora, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDatahora_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtavDatahora_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "DataHora", "right", false, "HLP_WP_BackLog.htm");
            GxWebStd.gx_bitmap( context, edtavDatahora_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavDatahora_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_BackLog.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_codigo_Internalname, "Usu�rio", "", "", lblTextblockusuario_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_BackLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavUsuario_codigo, dynavUsuario_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV6Usuario_Codigo), 6, 0)), 1, dynavUsuario_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavUsuario_codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,17);\"", "", true, "HLP_WP_BackLog.htm");
            dynavUsuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV6Usuario_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUsuario_codigo_Internalname, "Values", (String)(dynavUsuario_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknota_Internalname, "Raz�o", "", "", lblTextblocknota_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_BackLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='RequiredDataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNota_Internalname, AV9Nota, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", 0, 1, 1, 0, 120, "chr", 12, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_WP_BackLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_MM2e( true) ;
         }
         else
         {
            wb_table2_8_MM2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMM2( ) ;
         WSMM2( ) ;
         WEMM2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216254534");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_backlog.js", "?20206216254534");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockdatahora_Internalname = "TEXTBLOCKDATAHORA";
         edtavDatahora_Internalname = "vDATAHORA";
         lblTextblockusuario_codigo_Internalname = "TEXTBLOCKUSUARIO_CODIGO";
         dynavUsuario_codigo_Internalname = "vUSUARIO_CODIGO";
         lblTextblocknota_Internalname = "TEXTBLOCKNOTA";
         edtavNota_Internalname = "vNOTA";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtnfechar_Internalname = "BTNFECHAR";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         lblTbjava_Internalname = "TBJAVA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         dynavUsuario_codigo_Jsonclick = "";
         edtavDatahora_Jsonclick = "";
         edtavDatahora_Enabled = 1;
         bttBtnenter_Visible = 1;
         lblTbjava_Visible = 1;
         lblTbjava_Caption = "tbJava";
         dynavUsuario_codigo.Enabled = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Back Log";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E12MM2',iparms:[{av:'AV9Nota',fld:'vNOTA',pic:'',nv:''},{av:'AV7Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV12StatusDmn',fld:'vSTATUSDMN',pic:'',nv:''},{av:'AV10PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV14Responsavel',fld:'vRESPONSAVEL',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E13MM2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV13WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV12StatusDmn = "";
         AV10PrazoEntrega = (DateTime)(DateTime.MinValue);
         AV5DataHora = (DateTime)(DateTime.MinValue);
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00MM2_A57Usuario_PessoaCod = new int[1] ;
         H00MM2_A1Usuario_Codigo = new int[1] ;
         H00MM2_A58Usuario_PessoaNom = new String[] {""} ;
         H00MM2_n58Usuario_PessoaNom = new bool[] {false} ;
         H00MM2_A54Usuario_Ativo = new bool[] {false} ;
         AV9Nota = "";
         H00MM3_A456ContagemResultado_Codigo = new int[1] ;
         H00MM3_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00MM3_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00MM3_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00MM3_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00MM3_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00MM3_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00MM3_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         H00MM3_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         H00MM3_A890ContagemResultado_Responsavel = new int[1] ;
         H00MM3_n890ContagemResultado_Responsavel = new bool[] {false} ;
         A493ContagemResultado_DemandaFM = "";
         A484ContagemResultado_StatusDmn = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTbjava_Jsonclick = "";
         TempTags = "";
         bttBtnenter_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockdatahora_Jsonclick = "";
         lblTextblockusuario_codigo_Jsonclick = "";
         lblTextblocknota_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_backlog__default(),
            new Object[][] {
                new Object[] {
               H00MM2_A57Usuario_PessoaCod, H00MM2_A1Usuario_Codigo, H00MM2_A58Usuario_PessoaNom, H00MM2_n58Usuario_PessoaNom, H00MM2_A54Usuario_Ativo
               }
               , new Object[] {
               H00MM3_A456ContagemResultado_Codigo, H00MM3_A493ContagemResultado_DemandaFM, H00MM3_n493ContagemResultado_DemandaFM, H00MM3_A484ContagemResultado_StatusDmn, H00MM3_n484ContagemResultado_StatusDmn, H00MM3_A472ContagemResultado_DataEntrega, H00MM3_n472ContagemResultado_DataEntrega, H00MM3_A912ContagemResultado_HoraEntrega, H00MM3_n912ContagemResultado_HoraEntrega, H00MM3_A890ContagemResultado_Responsavel,
               H00MM3_n890ContagemResultado_Responsavel
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavDatahora_Enabled = 0;
         dynavUsuario_codigo.Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV7Codigo ;
      private int wcpOAV7Codigo ;
      private int AV14Responsavel ;
      private int gxdynajaxindex ;
      private int AV6Usuario_Codigo ;
      private int edtavDatahora_Enabled ;
      private int A456ContagemResultado_Codigo ;
      private int A890ContagemResultado_Responsavel ;
      private int bttBtnenter_Visible ;
      private int lblTbjava_Visible ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV12StatusDmn ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavDatahora_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String dynavUsuario_codigo_Internalname ;
      private String edtavNota_Internalname ;
      private String A484ContagemResultado_StatusDmn ;
      private String bttBtnenter_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTbjava_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String TempTags ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String lblTextblockdatahora_Internalname ;
      private String lblTextblockdatahora_Jsonclick ;
      private String edtavDatahora_Jsonclick ;
      private String lblTextblockusuario_codigo_Internalname ;
      private String lblTextblockusuario_codigo_Jsonclick ;
      private String dynavUsuario_codigo_Jsonclick ;
      private String lblTextblocknota_Internalname ;
      private String lblTextblocknota_Jsonclick ;
      private DateTime AV10PrazoEntrega ;
      private DateTime AV5DataHora ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n890ContagemResultado_Responsavel ;
      private String AV9Nota ;
      private String A493ContagemResultado_DemandaFM ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavUsuario_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00MM2_A57Usuario_PessoaCod ;
      private int[] H00MM2_A1Usuario_Codigo ;
      private String[] H00MM2_A58Usuario_PessoaNom ;
      private bool[] H00MM2_n58Usuario_PessoaNom ;
      private bool[] H00MM2_A54Usuario_Ativo ;
      private int[] H00MM3_A456ContagemResultado_Codigo ;
      private String[] H00MM3_A493ContagemResultado_DemandaFM ;
      private bool[] H00MM3_n493ContagemResultado_DemandaFM ;
      private String[] H00MM3_A484ContagemResultado_StatusDmn ;
      private bool[] H00MM3_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00MM3_A472ContagemResultado_DataEntrega ;
      private bool[] H00MM3_n472ContagemResultado_DataEntrega ;
      private DateTime[] H00MM3_A912ContagemResultado_HoraEntrega ;
      private bool[] H00MM3_n912ContagemResultado_HoraEntrega ;
      private int[] H00MM3_A890ContagemResultado_Responsavel ;
      private bool[] H00MM3_n890ContagemResultado_Responsavel ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV13WWPContext ;
   }

   public class wp_backlog__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00MM2 ;
          prmH00MM2 = new Object[] {
          } ;
          Object[] prmH00MM3 ;
          prmH00MM3 = new Object[] {
          new Object[] {"@AV7Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00MM2", "SELECT T2.[Pessoa_Codigo] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_Ativo] FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Ativo] = 1 ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MM2,0,0,true,false )
             ,new CursorDef("H00MM3", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_DemandaFM], [ContagemResultado_StatusDmn], [ContagemResultado_DataEntrega], [ContagemResultado_HoraEntrega], [ContagemResultado_Responsavel] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV7Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MM3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
