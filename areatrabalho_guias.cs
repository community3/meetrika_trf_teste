/*
               File: AreaTrabalho_Guias
        Description: Area de Trabalho x Guias
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:58:30.85
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class areatrabalho_guias : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A5AreaTrabalho_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A93Guia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A93Guia_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Area de Trabalho x Guias", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public areatrabalho_guias( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public areatrabalho_guias( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0J20( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0J20e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0J20( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0J20( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0J20e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Area de Trabalho x Guias", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_AreaTrabalho_Guias.htm");
            wb_table3_28_0J20( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_0J20e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0J20e( true) ;
         }
         else
         {
            wb_table1_2_0J20e( false) ;
         }
      }

      protected void wb_table3_28_0J20( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_0J20( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_0J20e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_AreaTrabalho_Guias.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_AreaTrabalho_Guias.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_AreaTrabalho_Guias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_0J20e( true) ;
         }
         else
         {
            wb_table3_28_0J20e( false) ;
         }
      }

      protected void wb_table4_34_0J20( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_codigo_Internalname, "C�digo Area de Trabalho", "", "", lblTextblockareatrabalho_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_AreaTrabalho_Guias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAreaTrabalho_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")), ((edtAreaTrabalho_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAreaTrabalho_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtAreaTrabalho_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AreaTrabalho_Guias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_descricao_Internalname, "�rea de Trabalho", "", "", lblTextblockareatrabalho_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_AreaTrabalho_Guias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAreaTrabalho_Descricao_Internalname, A6AreaTrabalho_Descricao, StringUtil.RTrim( context.localUtil.Format( A6AreaTrabalho_Descricao, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAreaTrabalho_Descricao_Jsonclick, 0, "Attribute", "", "", "", 1, edtAreaTrabalho_Descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_AreaTrabalho_Guias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockguia_codigo_Internalname, "C�digo", "", "", lblTextblockguia_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_AreaTrabalho_Guias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGuia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A93Guia_Codigo), 6, 0, ",", "")), ((edtGuia_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGuia_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtGuia_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AreaTrabalho_Guias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockguia_nome_Internalname, "Nome", "", "", lblTextblockguia_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_AreaTrabalho_Guias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGuia_Nome_Internalname, StringUtil.RTrim( A94Guia_Nome), StringUtil.RTrim( context.localUtil.Format( A94Guia_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGuia_Nome_Jsonclick, 0, "Attribute", "", "", "", 1, edtGuia_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_AreaTrabalho_Guias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockguia_versao_Internalname, "Vers�o", "", "", lblTextblockguia_versao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_AreaTrabalho_Guias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGuia_Versao_Internalname, StringUtil.RTrim( A95Guia_Versao), StringUtil.RTrim( context.localUtil.Format( A95Guia_Versao, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGuia_Versao_Jsonclick, 0, "Attribute", "", "", "", 1, edtGuia_Versao_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "VersaoGuia", "left", true, "HLP_AreaTrabalho_Guias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_0J20e( true) ;
         }
         else
         {
            wb_table4_34_0J20e( false) ;
         }
      }

      protected void wb_table2_5_0J20( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AreaTrabalho_Guias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AreaTrabalho_Guias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AreaTrabalho_Guias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AreaTrabalho_Guias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AreaTrabalho_Guias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AreaTrabalho_Guias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AreaTrabalho_Guias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AreaTrabalho_Guias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AreaTrabalho_Guias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AreaTrabalho_Guias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AreaTrabalho_Guias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AreaTrabalho_Guias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AreaTrabalho_Guias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AreaTrabalho_Guias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AreaTrabalho_Guias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AreaTrabalho_Guias.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0J20e( true) ;
         }
         else
         {
            wb_table2_5_0J20e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtAreaTrabalho_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAreaTrabalho_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "AREATRABALHO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A5AreaTrabalho_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               }
               else
               {
                  A5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               }
               A6AreaTrabalho_Descricao = StringUtil.Upper( cgiGet( edtAreaTrabalho_Descricao_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
               if ( ( ( context.localUtil.CToN( cgiGet( edtGuia_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtGuia_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "GUIA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtGuia_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A93Guia_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
               }
               else
               {
                  A93Guia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGuia_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
               }
               A94Guia_Nome = StringUtil.Upper( cgiGet( edtGuia_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
               A95Guia_Versao = cgiGet( edtGuia_Versao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A95Guia_Versao", A95Guia_Versao);
               /* Read saved values. */
               Z5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z5AreaTrabalho_Codigo"), ",", "."));
               Z93Guia_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z93Guia_Codigo"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
                  A93Guia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0J20( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes0J20( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption0J0( )
      {
      }

      protected void ZM0J20( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -1 )
         {
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            Z93Guia_Codigo = A93Guia_Codigo;
            Z6AreaTrabalho_Descricao = A6AreaTrabalho_Descricao;
            Z94Guia_Nome = A94Guia_Nome;
            Z95Guia_Versao = A95Guia_Versao;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load0J20( )
      {
         /* Using cursor T000J6 */
         pr_default.execute(4, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound20 = 1;
            A6AreaTrabalho_Descricao = T000J6_A6AreaTrabalho_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
            A94Guia_Nome = T000J6_A94Guia_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
            A95Guia_Versao = T000J6_A95Guia_Versao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A95Guia_Versao", A95Guia_Versao);
            ZM0J20( -1) ;
         }
         pr_default.close(4);
         OnLoadActions0J20( ) ;
      }

      protected void OnLoadActions0J20( )
      {
      }

      protected void CheckExtendedTable0J20( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T000J4 */
         pr_default.execute(2, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "AREATRABALHO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A6AreaTrabalho_Descricao = T000J4_A6AreaTrabalho_Descricao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
         pr_default.close(2);
         /* Using cursor T000J5 */
         pr_default.execute(3, new Object[] {A93Guia_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Guia'.", "ForeignKeyNotFound", 1, "GUIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtGuia_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A94Guia_Nome = T000J5_A94Guia_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
         A95Guia_Versao = T000J5_A95Guia_Versao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A95Guia_Versao", A95Guia_Versao);
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors0J20( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( int A5AreaTrabalho_Codigo )
      {
         /* Using cursor T000J7 */
         pr_default.execute(5, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "AREATRABALHO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A6AreaTrabalho_Descricao = T000J7_A6AreaTrabalho_Descricao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A6AreaTrabalho_Descricao)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_3( int A93Guia_Codigo )
      {
         /* Using cursor T000J8 */
         pr_default.execute(6, new Object[] {A93Guia_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Guia'.", "ForeignKeyNotFound", 1, "GUIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtGuia_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A94Guia_Nome = T000J8_A94Guia_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
         A95Guia_Versao = T000J8_A95Guia_Versao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A95Guia_Versao", A95Guia_Versao);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A94Guia_Nome))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A95Guia_Versao))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey0J20( )
      {
         /* Using cursor T000J9 */
         pr_default.execute(7, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound20 = 1;
         }
         else
         {
            RcdFound20 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000J3 */
         pr_default.execute(1, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0J20( 1) ;
            RcdFound20 = 1;
            A5AreaTrabalho_Codigo = T000J3_A5AreaTrabalho_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            A93Guia_Codigo = T000J3_A93Guia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            Z93Guia_Codigo = A93Guia_Codigo;
            sMode20 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load0J20( ) ;
            if ( AnyError == 1 )
            {
               RcdFound20 = 0;
               InitializeNonKey0J20( ) ;
            }
            Gx_mode = sMode20;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound20 = 0;
            InitializeNonKey0J20( ) ;
            sMode20 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode20;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0J20( ) ;
         if ( RcdFound20 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound20 = 0;
         /* Using cursor T000J10 */
         pr_default.execute(8, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T000J10_A5AreaTrabalho_Codigo[0] < A5AreaTrabalho_Codigo ) || ( T000J10_A5AreaTrabalho_Codigo[0] == A5AreaTrabalho_Codigo ) && ( T000J10_A93Guia_Codigo[0] < A93Guia_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T000J10_A5AreaTrabalho_Codigo[0] > A5AreaTrabalho_Codigo ) || ( T000J10_A5AreaTrabalho_Codigo[0] == A5AreaTrabalho_Codigo ) && ( T000J10_A93Guia_Codigo[0] > A93Guia_Codigo ) ) )
            {
               A5AreaTrabalho_Codigo = T000J10_A5AreaTrabalho_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               A93Guia_Codigo = T000J10_A93Guia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
               RcdFound20 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound20 = 0;
         /* Using cursor T000J11 */
         pr_default.execute(9, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T000J11_A5AreaTrabalho_Codigo[0] > A5AreaTrabalho_Codigo ) || ( T000J11_A5AreaTrabalho_Codigo[0] == A5AreaTrabalho_Codigo ) && ( T000J11_A93Guia_Codigo[0] > A93Guia_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T000J11_A5AreaTrabalho_Codigo[0] < A5AreaTrabalho_Codigo ) || ( T000J11_A5AreaTrabalho_Codigo[0] == A5AreaTrabalho_Codigo ) && ( T000J11_A93Guia_Codigo[0] < A93Guia_Codigo ) ) )
            {
               A5AreaTrabalho_Codigo = T000J11_A5AreaTrabalho_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               A93Guia_Codigo = T000J11_A93Guia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
               RcdFound20 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0J20( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0J20( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound20 == 1 )
            {
               if ( ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo ) || ( A93Guia_Codigo != Z93Guia_Codigo ) )
               {
                  A5AreaTrabalho_Codigo = Z5AreaTrabalho_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
                  A93Guia_Codigo = Z93Guia_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "AREATRABALHO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update0J20( ) ;
                  GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo ) || ( A93Guia_Codigo != Z93Guia_Codigo ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0J20( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "AREATRABALHO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0J20( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo ) || ( A93Guia_Codigo != Z93Guia_Codigo ) )
         {
            A5AreaTrabalho_Codigo = Z5AreaTrabalho_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            A93Guia_Codigo = Z93Guia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "AREATRABALHO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound20 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "AREATRABALHO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart0J20( ) ;
         if ( RcdFound20 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd0J20( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound20 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound20 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart0J20( ) ;
         if ( RcdFound20 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound20 != 0 )
            {
               ScanNext0J20( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd0J20( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency0J20( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000J2 */
            pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AreaTrabalho_Guias"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"AreaTrabalho_Guias"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0J20( )
      {
         BeforeValidate0J20( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0J20( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0J20( 0) ;
            CheckOptimisticConcurrency0J20( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0J20( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0J20( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000J12 */
                     pr_default.execute(10, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("AreaTrabalho_Guias") ;
                     if ( (pr_default.getStatus(10) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0J0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0J20( ) ;
            }
            EndLevel0J20( ) ;
         }
         CloseExtendedTableCursors0J20( ) ;
      }

      protected void Update0J20( )
      {
         BeforeValidate0J20( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0J20( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0J20( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0J20( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0J20( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [AreaTrabalho_Guias] */
                     DeferredUpdate0J20( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption0J0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0J20( ) ;
         }
         CloseExtendedTableCursors0J20( ) ;
      }

      protected void DeferredUpdate0J20( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate0J20( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0J20( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0J20( ) ;
            AfterConfirm0J20( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0J20( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000J13 */
                  pr_default.execute(11, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("AreaTrabalho_Guias") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound20 == 0 )
                        {
                           InitAll0J20( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption0J0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode20 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel0J20( ) ;
         Gx_mode = sMode20;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls0J20( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000J14 */
            pr_default.execute(12, new Object[] {A5AreaTrabalho_Codigo});
            A6AreaTrabalho_Descricao = T000J14_A6AreaTrabalho_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
            pr_default.close(12);
            /* Using cursor T000J15 */
            pr_default.execute(13, new Object[] {A93Guia_Codigo});
            A94Guia_Nome = T000J15_A94Guia_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
            A95Guia_Versao = T000J15_A95Guia_Versao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A95Guia_Versao", A95Guia_Versao);
            pr_default.close(13);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000J16 */
            pr_default.execute(14, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Guia"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
         }
      }

      protected void EndLevel0J20( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0J20( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(12);
            pr_default.close(13);
            context.CommitDataStores( "AreaTrabalho_Guias");
            if ( AnyError == 0 )
            {
               ConfirmValues0J0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(12);
            pr_default.close(13);
            context.RollbackDataStores( "AreaTrabalho_Guias");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0J20( )
      {
         /* Using cursor T000J17 */
         pr_default.execute(15);
         RcdFound20 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound20 = 1;
            A5AreaTrabalho_Codigo = T000J17_A5AreaTrabalho_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            A93Guia_Codigo = T000J17_A93Guia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0J20( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound20 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound20 = 1;
            A5AreaTrabalho_Codigo = T000J17_A5AreaTrabalho_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            A93Guia_Codigo = T000J17_A93Guia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd0J20( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm0J20( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0J20( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0J20( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0J20( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0J20( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0J20( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0J20( )
      {
         edtAreaTrabalho_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_Codigo_Enabled), 5, 0)));
         edtAreaTrabalho_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_Descricao_Enabled), 5, 0)));
         edtGuia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_Codigo_Enabled), 5, 0)));
         edtGuia_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_Nome_Enabled), 5, 0)));
         edtGuia_Versao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Versao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_Versao_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0J0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042822583194");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("areatrabalho_guias.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z93Guia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z93Guia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("areatrabalho_guias.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "AreaTrabalho_Guias" ;
      }

      public override String GetPgmdesc( )
      {
         return "Area de Trabalho x Guias" ;
      }

      protected void InitializeNonKey0J20( )
      {
         A6AreaTrabalho_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
         A94Guia_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
         A95Guia_Versao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A95Guia_Versao", A95Guia_Versao);
      }

      protected void InitAll0J20( )
      {
         A5AreaTrabalho_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         A93Guia_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
         InitializeNonKey0J20( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282258320");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("areatrabalho_guias.js", "?20204282258320");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockareatrabalho_codigo_Internalname = "TEXTBLOCKAREATRABALHO_CODIGO";
         edtAreaTrabalho_Codigo_Internalname = "AREATRABALHO_CODIGO";
         lblTextblockareatrabalho_descricao_Internalname = "TEXTBLOCKAREATRABALHO_DESCRICAO";
         edtAreaTrabalho_Descricao_Internalname = "AREATRABALHO_DESCRICAO";
         lblTextblockguia_codigo_Internalname = "TEXTBLOCKGUIA_CODIGO";
         edtGuia_Codigo_Internalname = "GUIA_CODIGO";
         lblTextblockguia_nome_Internalname = "TEXTBLOCKGUIA_NOME";
         edtGuia_Nome_Internalname = "GUIA_NOME";
         lblTextblockguia_versao_Internalname = "TEXTBLOCKGUIA_VERSAO";
         edtGuia_Versao_Internalname = "GUIA_VERSAO";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Area de Trabalho x Guias";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtGuia_Versao_Jsonclick = "";
         edtGuia_Versao_Enabled = 0;
         edtGuia_Nome_Jsonclick = "";
         edtGuia_Nome_Enabled = 0;
         edtGuia_Codigo_Jsonclick = "";
         edtGuia_Codigo_Enabled = 1;
         edtAreaTrabalho_Descricao_Jsonclick = "";
         edtAreaTrabalho_Descricao_Enabled = 0;
         edtAreaTrabalho_Codigo_Jsonclick = "";
         edtAreaTrabalho_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T000J14 */
         pr_default.execute(12, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "AREATRABALHO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A6AreaTrabalho_Descricao = T000J14_A6AreaTrabalho_Descricao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
         pr_default.close(12);
         /* Using cursor T000J15 */
         pr_default.execute(13, new Object[] {A93Guia_Codigo});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Guia'.", "ForeignKeyNotFound", 1, "GUIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtGuia_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A94Guia_Nome = T000J15_A94Guia_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
         A95Guia_Versao = T000J15_A95Guia_Versao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A95Guia_Versao", A95Guia_Versao);
         pr_default.close(13);
         if ( AnyError == 0 )
         {
            GX_FocusControl = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Areatrabalho_codigo( int GX_Parm1 ,
                                             String GX_Parm2 )
      {
         A5AreaTrabalho_Codigo = GX_Parm1;
         A6AreaTrabalho_Descricao = GX_Parm2;
         /* Using cursor T000J14 */
         pr_default.execute(12, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "AREATRABALHO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
         }
         A6AreaTrabalho_Descricao = T000J14_A6AreaTrabalho_Descricao[0];
         pr_default.close(12);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A6AreaTrabalho_Descricao = "";
         }
         isValidOutput.Add(A6AreaTrabalho_Descricao);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Guia_codigo( int GX_Parm1 ,
                                     int GX_Parm2 ,
                                     String GX_Parm3 ,
                                     String GX_Parm4 )
      {
         A5AreaTrabalho_Codigo = GX_Parm1;
         A93Guia_Codigo = GX_Parm2;
         A94Guia_Nome = GX_Parm3;
         A95Guia_Versao = GX_Parm4;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         /* Using cursor T000J15 */
         pr_default.execute(13, new Object[] {A93Guia_Codigo});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Guia'.", "ForeignKeyNotFound", 1, "GUIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtGuia_Codigo_Internalname;
         }
         A94Guia_Nome = T000J15_A94Guia_Nome[0];
         A95Guia_Versao = T000J15_A95Guia_Versao[0];
         pr_default.close(13);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A6AreaTrabalho_Descricao = "";
            A94Guia_Nome = "";
            A95Guia_Versao = "";
         }
         isValidOutput.Add(A6AreaTrabalho_Descricao);
         isValidOutput.Add(StringUtil.RTrim( A94Guia_Nome));
         isValidOutput.Add(StringUtil.RTrim( A95Guia_Versao));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z5AreaTrabalho_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z93Guia_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(Z6AreaTrabalho_Descricao);
         isValidOutput.Add(StringUtil.RTrim( Z94Guia_Nome));
         isValidOutput.Add(StringUtil.RTrim( Z95Guia_Versao));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockareatrabalho_codigo_Jsonclick = "";
         lblTextblockareatrabalho_descricao_Jsonclick = "";
         A6AreaTrabalho_Descricao = "";
         lblTextblockguia_codigo_Jsonclick = "";
         lblTextblockguia_nome_Jsonclick = "";
         A94Guia_Nome = "";
         lblTextblockguia_versao_Jsonclick = "";
         A95Guia_Versao = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z6AreaTrabalho_Descricao = "";
         Z94Guia_Nome = "";
         Z95Guia_Versao = "";
         T000J6_A6AreaTrabalho_Descricao = new String[] {""} ;
         T000J6_A94Guia_Nome = new String[] {""} ;
         T000J6_A95Guia_Versao = new String[] {""} ;
         T000J6_A5AreaTrabalho_Codigo = new int[1] ;
         T000J6_A93Guia_Codigo = new int[1] ;
         T000J4_A6AreaTrabalho_Descricao = new String[] {""} ;
         T000J5_A94Guia_Nome = new String[] {""} ;
         T000J5_A95Guia_Versao = new String[] {""} ;
         T000J7_A6AreaTrabalho_Descricao = new String[] {""} ;
         T000J8_A94Guia_Nome = new String[] {""} ;
         T000J8_A95Guia_Versao = new String[] {""} ;
         T000J9_A5AreaTrabalho_Codigo = new int[1] ;
         T000J9_A93Guia_Codigo = new int[1] ;
         T000J3_A5AreaTrabalho_Codigo = new int[1] ;
         T000J3_A93Guia_Codigo = new int[1] ;
         sMode20 = "";
         T000J10_A5AreaTrabalho_Codigo = new int[1] ;
         T000J10_A93Guia_Codigo = new int[1] ;
         T000J11_A5AreaTrabalho_Codigo = new int[1] ;
         T000J11_A93Guia_Codigo = new int[1] ;
         T000J2_A5AreaTrabalho_Codigo = new int[1] ;
         T000J2_A93Guia_Codigo = new int[1] ;
         T000J14_A6AreaTrabalho_Descricao = new String[] {""} ;
         T000J15_A94Guia_Nome = new String[] {""} ;
         T000J15_A95Guia_Versao = new String[] {""} ;
         T000J16_A93Guia_Codigo = new int[1] ;
         T000J17_A5AreaTrabalho_Codigo = new int[1] ;
         T000J17_A93Guia_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.areatrabalho_guias__default(),
            new Object[][] {
                new Object[] {
               T000J2_A5AreaTrabalho_Codigo, T000J2_A93Guia_Codigo
               }
               , new Object[] {
               T000J3_A5AreaTrabalho_Codigo, T000J3_A93Guia_Codigo
               }
               , new Object[] {
               T000J4_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               T000J5_A94Guia_Nome, T000J5_A95Guia_Versao
               }
               , new Object[] {
               T000J6_A6AreaTrabalho_Descricao, T000J6_A94Guia_Nome, T000J6_A95Guia_Versao, T000J6_A5AreaTrabalho_Codigo, T000J6_A93Guia_Codigo
               }
               , new Object[] {
               T000J7_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               T000J8_A94Guia_Nome, T000J8_A95Guia_Versao
               }
               , new Object[] {
               T000J9_A5AreaTrabalho_Codigo, T000J9_A93Guia_Codigo
               }
               , new Object[] {
               T000J10_A5AreaTrabalho_Codigo, T000J10_A93Guia_Codigo
               }
               , new Object[] {
               T000J11_A5AreaTrabalho_Codigo, T000J11_A93Guia_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000J14_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               T000J15_A94Guia_Nome, T000J15_A95Guia_Versao
               }
               , new Object[] {
               T000J16_A93Guia_Codigo
               }
               , new Object[] {
               T000J17_A5AreaTrabalho_Codigo, T000J17_A93Guia_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound20 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z5AreaTrabalho_Codigo ;
      private int Z93Guia_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A93Guia_Codigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtAreaTrabalho_Codigo_Enabled ;
      private int edtAreaTrabalho_Descricao_Enabled ;
      private int edtGuia_Codigo_Enabled ;
      private int edtGuia_Nome_Enabled ;
      private int edtGuia_Versao_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtAreaTrabalho_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockareatrabalho_codigo_Internalname ;
      private String lblTextblockareatrabalho_codigo_Jsonclick ;
      private String edtAreaTrabalho_Codigo_Jsonclick ;
      private String lblTextblockareatrabalho_descricao_Internalname ;
      private String lblTextblockareatrabalho_descricao_Jsonclick ;
      private String edtAreaTrabalho_Descricao_Internalname ;
      private String edtAreaTrabalho_Descricao_Jsonclick ;
      private String lblTextblockguia_codigo_Internalname ;
      private String lblTextblockguia_codigo_Jsonclick ;
      private String edtGuia_Codigo_Internalname ;
      private String edtGuia_Codigo_Jsonclick ;
      private String lblTextblockguia_nome_Internalname ;
      private String lblTextblockguia_nome_Jsonclick ;
      private String edtGuia_Nome_Internalname ;
      private String A94Guia_Nome ;
      private String edtGuia_Nome_Jsonclick ;
      private String lblTextblockguia_versao_Internalname ;
      private String lblTextblockguia_versao_Jsonclick ;
      private String edtGuia_Versao_Internalname ;
      private String A95Guia_Versao ;
      private String edtGuia_Versao_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z94Guia_Nome ;
      private String Z95Guia_Versao ;
      private String sMode20 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private String A6AreaTrabalho_Descricao ;
      private String Z6AreaTrabalho_Descricao ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] T000J6_A6AreaTrabalho_Descricao ;
      private String[] T000J6_A94Guia_Nome ;
      private String[] T000J6_A95Guia_Versao ;
      private int[] T000J6_A5AreaTrabalho_Codigo ;
      private int[] T000J6_A93Guia_Codigo ;
      private String[] T000J4_A6AreaTrabalho_Descricao ;
      private String[] T000J5_A94Guia_Nome ;
      private String[] T000J5_A95Guia_Versao ;
      private String[] T000J7_A6AreaTrabalho_Descricao ;
      private String[] T000J8_A94Guia_Nome ;
      private String[] T000J8_A95Guia_Versao ;
      private int[] T000J9_A5AreaTrabalho_Codigo ;
      private int[] T000J9_A93Guia_Codigo ;
      private int[] T000J3_A5AreaTrabalho_Codigo ;
      private int[] T000J3_A93Guia_Codigo ;
      private int[] T000J10_A5AreaTrabalho_Codigo ;
      private int[] T000J10_A93Guia_Codigo ;
      private int[] T000J11_A5AreaTrabalho_Codigo ;
      private int[] T000J11_A93Guia_Codigo ;
      private int[] T000J2_A5AreaTrabalho_Codigo ;
      private int[] T000J2_A93Guia_Codigo ;
      private String[] T000J14_A6AreaTrabalho_Descricao ;
      private String[] T000J15_A94Guia_Nome ;
      private String[] T000J15_A95Guia_Versao ;
      private int[] T000J16_A93Guia_Codigo ;
      private int[] T000J17_A5AreaTrabalho_Codigo ;
      private int[] T000J17_A93Guia_Codigo ;
      private GXWebForm Form ;
   }

   public class areatrabalho_guias__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000J6 ;
          prmT000J6 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000J4 ;
          prmT000J4 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000J5 ;
          prmT000J5 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000J7 ;
          prmT000J7 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000J8 ;
          prmT000J8 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000J9 ;
          prmT000J9 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000J3 ;
          prmT000J3 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000J10 ;
          prmT000J10 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000J11 ;
          prmT000J11 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000J2 ;
          prmT000J2 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000J12 ;
          prmT000J12 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000J13 ;
          prmT000J13 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000J16 ;
          prmT000J16 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000J17 ;
          prmT000J17 = new Object[] {
          } ;
          Object[] prmT000J14 ;
          prmT000J14 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000J15 ;
          prmT000J15 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000J2", "SELECT [AreaTrabalho_Codigo], [Guia_Codigo] FROM [AreaTrabalho_Guias] WITH (UPDLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo AND [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000J2,1,0,true,false )
             ,new CursorDef("T000J3", "SELECT [AreaTrabalho_Codigo], [Guia_Codigo] FROM [AreaTrabalho_Guias] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo AND [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000J3,1,0,true,false )
             ,new CursorDef("T000J4", "SELECT [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000J4,1,0,true,false )
             ,new CursorDef("T000J5", "SELECT [Guia_Nome], [Guia_Versao] FROM [Guia] WITH (NOLOCK) WHERE [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000J5,1,0,true,false )
             ,new CursorDef("T000J6", "SELECT T2.[AreaTrabalho_Descricao], T3.[Guia_Nome], T3.[Guia_Versao], TM1.[AreaTrabalho_Codigo], TM1.[Guia_Codigo] FROM (([AreaTrabalho_Guias] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[AreaTrabalho_Codigo]) INNER JOIN [Guia] T3 WITH (NOLOCK) ON T3.[Guia_Codigo] = TM1.[Guia_Codigo]) WHERE TM1.[AreaTrabalho_Codigo] = @AreaTrabalho_Codigo and TM1.[Guia_Codigo] = @Guia_Codigo ORDER BY TM1.[AreaTrabalho_Codigo], TM1.[Guia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000J6,100,0,true,false )
             ,new CursorDef("T000J7", "SELECT [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000J7,1,0,true,false )
             ,new CursorDef("T000J8", "SELECT [Guia_Nome], [Guia_Versao] FROM [Guia] WITH (NOLOCK) WHERE [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000J8,1,0,true,false )
             ,new CursorDef("T000J9", "SELECT [AreaTrabalho_Codigo], [Guia_Codigo] FROM [AreaTrabalho_Guias] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo AND [Guia_Codigo] = @Guia_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000J9,1,0,true,false )
             ,new CursorDef("T000J10", "SELECT TOP 1 [AreaTrabalho_Codigo], [Guia_Codigo] FROM [AreaTrabalho_Guias] WITH (NOLOCK) WHERE ( [AreaTrabalho_Codigo] > @AreaTrabalho_Codigo or [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo and [Guia_Codigo] > @Guia_Codigo) ORDER BY [AreaTrabalho_Codigo], [Guia_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000J10,1,0,true,true )
             ,new CursorDef("T000J11", "SELECT TOP 1 [AreaTrabalho_Codigo], [Guia_Codigo] FROM [AreaTrabalho_Guias] WITH (NOLOCK) WHERE ( [AreaTrabalho_Codigo] < @AreaTrabalho_Codigo or [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo and [Guia_Codigo] < @Guia_Codigo) ORDER BY [AreaTrabalho_Codigo] DESC, [Guia_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000J11,1,0,true,true )
             ,new CursorDef("T000J12", "INSERT INTO [AreaTrabalho_Guias]([AreaTrabalho_Codigo], [Guia_Codigo]) VALUES(@AreaTrabalho_Codigo, @Guia_Codigo)", GxErrorMask.GX_NOMASK,prmT000J12)
             ,new CursorDef("T000J13", "DELETE FROM [AreaTrabalho_Guias]  WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo AND [Guia_Codigo] = @Guia_Codigo", GxErrorMask.GX_NOMASK,prmT000J13)
             ,new CursorDef("T000J14", "SELECT [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000J14,1,0,true,false )
             ,new CursorDef("T000J15", "SELECT [Guia_Nome], [Guia_Versao] FROM [Guia] WITH (NOLOCK) WHERE [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000J15,1,0,true,false )
             ,new CursorDef("T000J16", "SELECT TOP 1 [Guia_Codigo] FROM [Guia] WITH (NOLOCK) WHERE [Guia_AreaTrabalhoCod] = @AreaTrabalho_Codigo AND [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000J16,1,0,true,true )
             ,new CursorDef("T000J17", "SELECT [AreaTrabalho_Codigo], [Guia_Codigo] FROM [AreaTrabalho_Guias] WITH (NOLOCK) ORDER BY [AreaTrabalho_Codigo], [Guia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000J17,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
