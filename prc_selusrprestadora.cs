/*
               File: PRC_SelUsrPrestadora
        Description: Seleciona Usuario da Prestadora
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:16.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_selusrprestadora : GXProcedure
   {
      public prc_selusrprestadora( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_selusrprestadora( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_AreaTrabalho_Codigo ,
                           ref bool aP1_Seleciona )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV8Seleciona = aP1_Seleciona;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_Seleciona=this.AV8Seleciona;
      }

      public bool executeUdp( ref int aP0_AreaTrabalho_Codigo )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV8Seleciona = aP1_Seleciona;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_Seleciona=this.AV8Seleciona;
         return AV8Seleciona ;
      }

      public void executeSubmit( ref int aP0_AreaTrabalho_Codigo ,
                                 ref bool aP1_Seleciona )
      {
         prc_selusrprestadora objprc_selusrprestadora;
         objprc_selusrprestadora = new prc_selusrprestadora();
         objprc_selusrprestadora.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_selusrprestadora.AV8Seleciona = aP1_Seleciona;
         objprc_selusrprestadora.context.SetSubmitInitialConfig(context);
         objprc_selusrprestadora.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_selusrprestadora);
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_Seleciona=this.AV8Seleciona;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_selusrprestadora)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00XL2 */
         pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2080AreaTrabalho_SelUsrPrestadora = P00XL2_A2080AreaTrabalho_SelUsrPrestadora[0];
            n2080AreaTrabalho_SelUsrPrestadora = P00XL2_n2080AreaTrabalho_SelUsrPrestadora[0];
            AV8Seleciona = A2080AreaTrabalho_SelUsrPrestadora;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00XL2_A5AreaTrabalho_Codigo = new int[1] ;
         P00XL2_A2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         P00XL2_n2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_selusrprestadora__default(),
            new Object[][] {
                new Object[] {
               P00XL2_A5AreaTrabalho_Codigo, P00XL2_A2080AreaTrabalho_SelUsrPrestadora, P00XL2_n2080AreaTrabalho_SelUsrPrestadora
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A5AreaTrabalho_Codigo ;
      private String scmdbuf ;
      private bool AV8Seleciona ;
      private bool A2080AreaTrabalho_SelUsrPrestadora ;
      private bool n2080AreaTrabalho_SelUsrPrestadora ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_AreaTrabalho_Codigo ;
      private bool aP1_Seleciona ;
      private IDataStoreProvider pr_default ;
      private int[] P00XL2_A5AreaTrabalho_Codigo ;
      private bool[] P00XL2_A2080AreaTrabalho_SelUsrPrestadora ;
      private bool[] P00XL2_n2080AreaTrabalho_SelUsrPrestadora ;
   }

   public class prc_selusrprestadora__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XL2 ;
          prmP00XL2 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XL2", "SELECT TOP 1 [AreaTrabalho_Codigo], [AreaTrabalho_SelUsrPrestadora] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XL2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
