/*
               File: GetWWContratoServicosPrioridadeFilterData
        Description: Get WWContrato Servicos Prioridade Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:25.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratoservicosprioridadefilterdata : GXProcedure
   {
      public getwwcontratoservicosprioridadefilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratoservicosprioridadefilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratoservicosprioridadefilterdata objgetwwcontratoservicosprioridadefilterdata;
         objgetwwcontratoservicosprioridadefilterdata = new getwwcontratoservicosprioridadefilterdata();
         objgetwwcontratoservicosprioridadefilterdata.AV18DDOName = aP0_DDOName;
         objgetwwcontratoservicosprioridadefilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetwwcontratoservicosprioridadefilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratoservicosprioridadefilterdata.AV22OptionsJson = "" ;
         objgetwwcontratoservicosprioridadefilterdata.AV25OptionsDescJson = "" ;
         objgetwwcontratoservicosprioridadefilterdata.AV27OptionIndexesJson = "" ;
         objgetwwcontratoservicosprioridadefilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratoservicosprioridadefilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratoservicosprioridadefilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratoservicosprioridadefilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOSERVICOSPRIORIDADE_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSPRIORIDADE_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("WWContratoServicosPrioridadeGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratoServicosPrioridadeGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("WWContratoServicosPrioridadeGridState"), "");
         }
         AV48GXV1 = 1;
         while ( AV48GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV48GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRIORIDADE_ORDEM") == 0 )
            {
               AV42TFContratoServicosPrioridade_Ordem = (short)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
               AV43TFContratoServicosPrioridade_Ordem_To = (short)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRIORIDADE_NOME") == 0 )
            {
               AV10TFContratoServicosPrioridade_Nome = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRIORIDADE_NOME_SEL") == 0 )
            {
               AV11TFContratoServicosPrioridade_Nome_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRIORIDADE_PERCVALORB") == 0 )
            {
               AV12TFContratoServicosPrioridade_PercValorB = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, ".");
               AV13TFContratoServicosPrioridade_PercValorB_To = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO") == 0 )
            {
               AV14TFContratoServicosPrioridade_PercPrazo = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, ".");
               AV15TFContratoServicosPrioridade_PercPrazo_To = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRIORIDADE_PESO") == 0 )
            {
               AV44TFContratoServicosPrioridade_Peso = (short)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
               AV45TFContratoServicosPrioridade_Peso_To = (short)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, "."));
            }
            AV48GXV1 = (int)(AV48GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 )
            {
               AV35ContratoServicosPrioridade_Nome1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV36DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV37DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 )
               {
                  AV38ContratoServicosPrioridade_Nome2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV39DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV40DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 )
                  {
                     AV41ContratoServicosPrioridade_Nome3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATOSERVICOSPRIORIDADE_NOMEOPTIONS' Routine */
         AV10TFContratoServicosPrioridade_Nome = AV16SearchTxt;
         AV11TFContratoServicosPrioridade_Nome_Sel = "";
         AV50WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV51WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 = AV35ContratoServicosPrioridade_Nome1;
         AV52WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 = AV36DynamicFiltersEnabled2;
         AV53WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 = AV37DynamicFiltersSelector2;
         AV54WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 = AV38ContratoServicosPrioridade_Nome2;
         AV55WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 = AV39DynamicFiltersEnabled3;
         AV56WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 = AV40DynamicFiltersSelector3;
         AV57WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 = AV41ContratoServicosPrioridade_Nome3;
         AV58WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem = AV42TFContratoServicosPrioridade_Ordem;
         AV59WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to = AV43TFContratoServicosPrioridade_Ordem_To;
         AV60WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome = AV10TFContratoServicosPrioridade_Nome;
         AV61WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel = AV11TFContratoServicosPrioridade_Nome_Sel;
         AV62WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb = AV12TFContratoServicosPrioridade_PercValorB;
         AV63WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to = AV13TFContratoServicosPrioridade_PercValorB_To;
         AV64WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo = AV14TFContratoServicosPrioridade_PercPrazo;
         AV65WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to = AV15TFContratoServicosPrioridade_PercPrazo_To;
         AV66WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso = AV44TFContratoServicosPrioridade_Peso;
         AV67WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to = AV45TFContratoServicosPrioridade_Peso_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV50WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 ,
                                              AV51WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 ,
                                              AV52WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 ,
                                              AV53WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 ,
                                              AV54WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 ,
                                              AV55WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 ,
                                              AV56WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 ,
                                              AV57WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 ,
                                              AV58WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem ,
                                              AV59WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to ,
                                              AV61WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel ,
                                              AV60WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome ,
                                              AV62WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb ,
                                              AV63WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to ,
                                              AV64WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo ,
                                              AV65WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to ,
                                              AV66WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso ,
                                              AV67WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to ,
                                              A1337ContratoServicosPrioridade_Nome ,
                                              A2066ContratoServicosPrioridade_Ordem ,
                                              A1338ContratoServicosPrioridade_PercValorB ,
                                              A1339ContratoServicosPrioridade_PercPrazo ,
                                              A2067ContratoServicosPrioridade_Peso },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV51WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 = StringUtil.PadR( StringUtil.RTrim( AV51WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1), 50, "%");
         lV54WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 = StringUtil.PadR( StringUtil.RTrim( AV54WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2), 50, "%");
         lV57WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 = StringUtil.PadR( StringUtil.RTrim( AV57WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3), 50, "%");
         lV60WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome = StringUtil.PadR( StringUtil.RTrim( AV60WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome), 50, "%");
         /* Using cursor P00QO2 */
         pr_default.execute(0, new Object[] {lV51WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1, lV54WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2, lV57WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3, AV58WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem, AV59WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to, lV60WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome, AV61WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel, AV62WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb, AV63WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to, AV64WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo, AV65WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to, AV66WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso, AV67WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKQO2 = false;
            A1337ContratoServicosPrioridade_Nome = P00QO2_A1337ContratoServicosPrioridade_Nome[0];
            A2067ContratoServicosPrioridade_Peso = P00QO2_A2067ContratoServicosPrioridade_Peso[0];
            n2067ContratoServicosPrioridade_Peso = P00QO2_n2067ContratoServicosPrioridade_Peso[0];
            A1339ContratoServicosPrioridade_PercPrazo = P00QO2_A1339ContratoServicosPrioridade_PercPrazo[0];
            n1339ContratoServicosPrioridade_PercPrazo = P00QO2_n1339ContratoServicosPrioridade_PercPrazo[0];
            A1338ContratoServicosPrioridade_PercValorB = P00QO2_A1338ContratoServicosPrioridade_PercValorB[0];
            n1338ContratoServicosPrioridade_PercValorB = P00QO2_n1338ContratoServicosPrioridade_PercValorB[0];
            A2066ContratoServicosPrioridade_Ordem = P00QO2_A2066ContratoServicosPrioridade_Ordem[0];
            n2066ContratoServicosPrioridade_Ordem = P00QO2_n2066ContratoServicosPrioridade_Ordem[0];
            A1336ContratoServicosPrioridade_Codigo = P00QO2_A1336ContratoServicosPrioridade_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00QO2_A1337ContratoServicosPrioridade_Nome[0], A1337ContratoServicosPrioridade_Nome) == 0 ) )
            {
               BRKQO2 = false;
               A1336ContratoServicosPrioridade_Codigo = P00QO2_A1336ContratoServicosPrioridade_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKQO2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1337ContratoServicosPrioridade_Nome)) )
            {
               AV20Option = A1337ContratoServicosPrioridade_Nome;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQO2 )
            {
               BRKQO2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratoServicosPrioridade_Nome = "";
         AV11TFContratoServicosPrioridade_Nome_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV35ContratoServicosPrioridade_Nome1 = "";
         AV37DynamicFiltersSelector2 = "";
         AV38ContratoServicosPrioridade_Nome2 = "";
         AV40DynamicFiltersSelector3 = "";
         AV41ContratoServicosPrioridade_Nome3 = "";
         AV50WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 = "";
         AV51WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 = "";
         AV53WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 = "";
         AV54WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 = "";
         AV56WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 = "";
         AV57WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 = "";
         AV60WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome = "";
         AV61WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel = "";
         scmdbuf = "";
         lV51WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 = "";
         lV54WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 = "";
         lV57WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 = "";
         lV60WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome = "";
         A1337ContratoServicosPrioridade_Nome = "";
         P00QO2_A1337ContratoServicosPrioridade_Nome = new String[] {""} ;
         P00QO2_A2067ContratoServicosPrioridade_Peso = new short[1] ;
         P00QO2_n2067ContratoServicosPrioridade_Peso = new bool[] {false} ;
         P00QO2_A1339ContratoServicosPrioridade_PercPrazo = new decimal[1] ;
         P00QO2_n1339ContratoServicosPrioridade_PercPrazo = new bool[] {false} ;
         P00QO2_A1338ContratoServicosPrioridade_PercValorB = new decimal[1] ;
         P00QO2_n1338ContratoServicosPrioridade_PercValorB = new bool[] {false} ;
         P00QO2_A2066ContratoServicosPrioridade_Ordem = new short[1] ;
         P00QO2_n2066ContratoServicosPrioridade_Ordem = new bool[] {false} ;
         P00QO2_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         AV20Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratoservicosprioridadefilterdata__default(),
            new Object[][] {
                new Object[] {
               P00QO2_A1337ContratoServicosPrioridade_Nome, P00QO2_A2067ContratoServicosPrioridade_Peso, P00QO2_n2067ContratoServicosPrioridade_Peso, P00QO2_A1339ContratoServicosPrioridade_PercPrazo, P00QO2_n1339ContratoServicosPrioridade_PercPrazo, P00QO2_A1338ContratoServicosPrioridade_PercValorB, P00QO2_n1338ContratoServicosPrioridade_PercValorB, P00QO2_A2066ContratoServicosPrioridade_Ordem, P00QO2_n2066ContratoServicosPrioridade_Ordem, P00QO2_A1336ContratoServicosPrioridade_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV42TFContratoServicosPrioridade_Ordem ;
      private short AV43TFContratoServicosPrioridade_Ordem_To ;
      private short AV44TFContratoServicosPrioridade_Peso ;
      private short AV45TFContratoServicosPrioridade_Peso_To ;
      private short AV58WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem ;
      private short AV59WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to ;
      private short AV66WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso ;
      private short AV67WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to ;
      private short A2066ContratoServicosPrioridade_Ordem ;
      private short A2067ContratoServicosPrioridade_Peso ;
      private int AV48GXV1 ;
      private int A1336ContratoServicosPrioridade_Codigo ;
      private long AV28count ;
      private decimal AV12TFContratoServicosPrioridade_PercValorB ;
      private decimal AV13TFContratoServicosPrioridade_PercValorB_To ;
      private decimal AV14TFContratoServicosPrioridade_PercPrazo ;
      private decimal AV15TFContratoServicosPrioridade_PercPrazo_To ;
      private decimal AV62WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb ;
      private decimal AV63WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to ;
      private decimal AV64WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo ;
      private decimal AV65WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to ;
      private decimal A1338ContratoServicosPrioridade_PercValorB ;
      private decimal A1339ContratoServicosPrioridade_PercPrazo ;
      private String AV10TFContratoServicosPrioridade_Nome ;
      private String AV11TFContratoServicosPrioridade_Nome_Sel ;
      private String AV35ContratoServicosPrioridade_Nome1 ;
      private String AV38ContratoServicosPrioridade_Nome2 ;
      private String AV41ContratoServicosPrioridade_Nome3 ;
      private String AV51WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 ;
      private String AV54WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 ;
      private String AV57WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 ;
      private String AV60WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome ;
      private String AV61WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel ;
      private String scmdbuf ;
      private String lV51WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 ;
      private String lV54WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 ;
      private String lV57WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 ;
      private String lV60WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome ;
      private String A1337ContratoServicosPrioridade_Nome ;
      private bool returnInSub ;
      private bool AV36DynamicFiltersEnabled2 ;
      private bool AV39DynamicFiltersEnabled3 ;
      private bool AV52WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 ;
      private bool AV55WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 ;
      private bool BRKQO2 ;
      private bool n2067ContratoServicosPrioridade_Peso ;
      private bool n1339ContratoServicosPrioridade_PercPrazo ;
      private bool n1338ContratoServicosPrioridade_PercValorB ;
      private bool n2066ContratoServicosPrioridade_Ordem ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV37DynamicFiltersSelector2 ;
      private String AV40DynamicFiltersSelector3 ;
      private String AV50WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 ;
      private String AV53WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 ;
      private String AV56WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00QO2_A1337ContratoServicosPrioridade_Nome ;
      private short[] P00QO2_A2067ContratoServicosPrioridade_Peso ;
      private bool[] P00QO2_n2067ContratoServicosPrioridade_Peso ;
      private decimal[] P00QO2_A1339ContratoServicosPrioridade_PercPrazo ;
      private bool[] P00QO2_n1339ContratoServicosPrioridade_PercPrazo ;
      private decimal[] P00QO2_A1338ContratoServicosPrioridade_PercValorB ;
      private bool[] P00QO2_n1338ContratoServicosPrioridade_PercValorB ;
      private short[] P00QO2_A2066ContratoServicosPrioridade_Ordem ;
      private bool[] P00QO2_n2066ContratoServicosPrioridade_Ordem ;
      private int[] P00QO2_A1336ContratoServicosPrioridade_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getwwcontratoservicosprioridadefilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00QO2( IGxContext context ,
                                             String AV50WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 ,
                                             String AV51WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 ,
                                             bool AV52WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 ,
                                             String AV53WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 ,
                                             String AV54WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 ,
                                             bool AV55WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 ,
                                             String AV56WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 ,
                                             String AV57WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 ,
                                             short AV58WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem ,
                                             short AV59WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to ,
                                             String AV61WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel ,
                                             String AV60WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome ,
                                             decimal AV62WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb ,
                                             decimal AV63WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to ,
                                             decimal AV64WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo ,
                                             decimal AV65WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to ,
                                             short AV66WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso ,
                                             short AV67WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to ,
                                             String A1337ContratoServicosPrioridade_Nome ,
                                             short A2066ContratoServicosPrioridade_Ordem ,
                                             decimal A1338ContratoServicosPrioridade_PercValorB ,
                                             decimal A1339ContratoServicosPrioridade_PercPrazo ,
                                             short A2067ContratoServicosPrioridade_Peso )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [13] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [ContratoServicosPrioridade_Nome], [ContratoServicosPrioridade_Peso], [ContratoServicosPrioridade_PercPrazo], [ContratoServicosPrioridade_PercValorB], [ContratoServicosPrioridade_Ordem], [ContratoServicosPrioridade_Codigo] FROM [ContratoServicosPrioridade] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV50WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Nome] like '%' + @lV51WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Nome] like '%' + @lV51WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( AV52WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV53WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Nome] like '%' + @lV54WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Nome] like '%' + @lV54WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV55WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV56WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Nome] like '%' + @lV57WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Nome] like '%' + @lV57WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 + '%')";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (0==AV58WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Ordem] >= @AV58WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Ordem] >= @AV58WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (0==AV59WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Ordem] <= @AV59WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Ordem] <= @AV59WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Nome] like @lV60WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Nome] like @lV60WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Nome] = @AV61WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Nome] = @AV61WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV62WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_PercValorB] >= @AV62WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_PercValorB] >= @AV62WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV63WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_PercValorB] <= @AV63WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_PercValorB] <= @AV63WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV64WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_PercPrazo] >= @AV64WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_PercPrazo] >= @AV64WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV65WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_PercPrazo] <= @AV65WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_PercPrazo] <= @AV65WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (0==AV66WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Peso] >= @AV66WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Peso] >= @AV66WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV67WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Peso] <= @AV67WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Peso] <= @AV67WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [ContratoServicosPrioridade_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00QO2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (decimal)dynConstraints[15] , (short)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (short)dynConstraints[22] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00QO2 ;
          prmP00QO2 = new Object[] {
          new Object[] {"@lV51WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV59WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@lV60WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV61WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV62WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV63WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV64WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV65WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV66WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV67WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00QO2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00QO2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[25]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratoservicosprioridadefilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratoservicosprioridadefilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratoservicosprioridadefilterdata") )
          {
             return  ;
          }
          getwwcontratoservicosprioridadefilterdata worker = new getwwcontratoservicosprioridadefilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
