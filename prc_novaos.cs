/*
               File: PRC_NovaOS
        Description: Nova OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:12.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_novaos : GXProcedure
   {
      public prc_novaos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_novaos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_ContratadaCod ,
                           int aP1_ContagemResultado_Responsavel ,
                           DateTime aP2_ContagemResultado_DataDmn ,
                           ref String aP3_ContagemResultado_Demanda ,
                           ref String aP4_ContagemResultado_DemandaFM ,
                           String aP5_ContagemResultado_Descricao ,
                           String aP6_ContagemResultado_Observacao ,
                           String aP7_ContagemResultado_Link ,
                           int aP8_ContagemResultado_OSVinculada ,
                           int aP9_UserId ,
                           int aP10_ContagemResultado_Servico ,
                           decimal aP11_ContagemResultado_ValorPF ,
                           int aP12_Sistema_Codigo ,
                           int aP13_Modulo_Codigo ,
                           int aP14_FuncaoUsuario_Codigo ,
                           ref DateTime aP15_PrazoEntrega ,
                           bool aP16_Contratante_OSAutomatica ,
                           decimal aP17_ContagemResultado_PFBFSImp ,
                           decimal aP18_ContagemResultado_PFLFSImp ,
                           int aP19_AgendaAtendimento_CntSrcCod ,
                           DateTime aP20_PrazoAnalise ,
                           String aP21_PrazoTipo ,
                           int aP22_RdmnIssueId ,
                           int aP23_RdmnProjectId ,
                           DateTime aP24_RdmnUpdated ,
                           int aP25_ContratadaOrigem ,
                           int aP26_ContadorFS ,
                           int aP27_Prioridade_Codigo ,
                           decimal aP28_ContagemResultado_CntSrvPrrPrz ,
                           decimal aP29_ContagemResultado_CntSrvPrrCst ,
                           short aP30_DiasComplexidade ,
                           short aP31_Evento ,
                           decimal aP32_ContagemResultado_QuantidadeSolicitada )
      {
         this.A490ContagemResultado_ContratadaCod = aP0_ContagemResultado_ContratadaCod;
         this.AV22ContagemResultado_Responsavel = aP1_ContagemResultado_Responsavel;
         this.A471ContagemResultado_DataDmn = aP2_ContagemResultado_DataDmn;
         this.AV26ContagemResultado_Demanda = aP3_ContagemResultado_Demanda;
         this.AV28ContagemResultado_DemandaFM = aP4_ContagemResultado_DemandaFM;
         this.A494ContagemResultado_Descricao = aP5_ContagemResultado_Descricao;
         this.A514ContagemResultado_Observacao = aP6_ContagemResultado_Observacao;
         this.A465ContagemResultado_Link = aP7_ContagemResultado_Link;
         this.AV18ContagemResultado_OSVinculada = aP8_ContagemResultado_OSVinculada;
         this.AV53UserId = aP9_UserId;
         this.A601ContagemResultado_Servico = aP10_ContagemResultado_Servico;
         this.A512ContagemResultado_ValorPF = aP11_ContagemResultado_ValorPF;
         this.AV21Sistema_Codigo = aP12_Sistema_Codigo;
         this.AV20Modulo_Codigo = aP13_Modulo_Codigo;
         this.AV24FuncaoUsuario_Codigo = aP14_FuncaoUsuario_Codigo;
         this.AV27PrazoEntrega = aP15_PrazoEntrega;
         this.AV29Contratante_OSAutomatica = aP16_Contratante_OSAutomatica;
         this.A798ContagemResultado_PFBFSImp = aP17_ContagemResultado_PFBFSImp;
         this.A799ContagemResultado_PFLFSImp = aP18_ContagemResultado_PFLFSImp;
         this.AV37AgendaAtendimento_CntSrcCod = aP19_AgendaAtendimento_CntSrcCod;
         this.AV39PrazoAnalise = aP20_PrazoAnalise;
         this.AV51PrazoTipo = aP21_PrazoTipo;
         this.AV56RdmnIssueId = aP22_RdmnIssueId;
         this.AV55RdmnProjectId = aP23_RdmnProjectId;
         this.AV57RdmnUpdated = aP24_RdmnUpdated;
         this.AV58ContratadaOrigem = aP25_ContratadaOrigem;
         this.AV59ContadorFS = aP26_ContadorFS;
         this.AV60Prioridade_Codigo = aP27_Prioridade_Codigo;
         this.AV61ContagemResultado_CntSrvPrrPrz = aP28_ContagemResultado_CntSrvPrrPrz;
         this.AV62ContagemResultado_CntSrvPrrCst = aP29_ContagemResultado_CntSrvPrrCst;
         this.AV71DiasComplexidade = aP30_DiasComplexidade;
         this.AV73Evento = aP31_Evento;
         this.AV88ContagemResultado_QuantidadeSolicitada = aP32_ContagemResultado_QuantidadeSolicitada;
         initialize();
         executePrivate();
         aP3_ContagemResultado_Demanda=this.AV26ContagemResultado_Demanda;
         aP4_ContagemResultado_DemandaFM=this.AV28ContagemResultado_DemandaFM;
         aP15_PrazoEntrega=this.AV27PrazoEntrega;
      }

      public void executeSubmit( int aP0_ContagemResultado_ContratadaCod ,
                                 int aP1_ContagemResultado_Responsavel ,
                                 DateTime aP2_ContagemResultado_DataDmn ,
                                 ref String aP3_ContagemResultado_Demanda ,
                                 ref String aP4_ContagemResultado_DemandaFM ,
                                 String aP5_ContagemResultado_Descricao ,
                                 String aP6_ContagemResultado_Observacao ,
                                 String aP7_ContagemResultado_Link ,
                                 int aP8_ContagemResultado_OSVinculada ,
                                 int aP9_UserId ,
                                 int aP10_ContagemResultado_Servico ,
                                 decimal aP11_ContagemResultado_ValorPF ,
                                 int aP12_Sistema_Codigo ,
                                 int aP13_Modulo_Codigo ,
                                 int aP14_FuncaoUsuario_Codigo ,
                                 ref DateTime aP15_PrazoEntrega ,
                                 bool aP16_Contratante_OSAutomatica ,
                                 decimal aP17_ContagemResultado_PFBFSImp ,
                                 decimal aP18_ContagemResultado_PFLFSImp ,
                                 int aP19_AgendaAtendimento_CntSrcCod ,
                                 DateTime aP20_PrazoAnalise ,
                                 String aP21_PrazoTipo ,
                                 int aP22_RdmnIssueId ,
                                 int aP23_RdmnProjectId ,
                                 DateTime aP24_RdmnUpdated ,
                                 int aP25_ContratadaOrigem ,
                                 int aP26_ContadorFS ,
                                 int aP27_Prioridade_Codigo ,
                                 decimal aP28_ContagemResultado_CntSrvPrrPrz ,
                                 decimal aP29_ContagemResultado_CntSrvPrrCst ,
                                 short aP30_DiasComplexidade ,
                                 short aP31_Evento ,
                                 decimal aP32_ContagemResultado_QuantidadeSolicitada )
      {
         prc_novaos objprc_novaos;
         objprc_novaos = new prc_novaos();
         objprc_novaos.A490ContagemResultado_ContratadaCod = aP0_ContagemResultado_ContratadaCod;
         objprc_novaos.AV22ContagemResultado_Responsavel = aP1_ContagemResultado_Responsavel;
         objprc_novaos.A471ContagemResultado_DataDmn = aP2_ContagemResultado_DataDmn;
         objprc_novaos.AV26ContagemResultado_Demanda = aP3_ContagemResultado_Demanda;
         objprc_novaos.AV28ContagemResultado_DemandaFM = aP4_ContagemResultado_DemandaFM;
         objprc_novaos.A494ContagemResultado_Descricao = aP5_ContagemResultado_Descricao;
         objprc_novaos.A514ContagemResultado_Observacao = aP6_ContagemResultado_Observacao;
         objprc_novaos.A465ContagemResultado_Link = aP7_ContagemResultado_Link;
         objprc_novaos.AV18ContagemResultado_OSVinculada = aP8_ContagemResultado_OSVinculada;
         objprc_novaos.AV53UserId = aP9_UserId;
         objprc_novaos.A601ContagemResultado_Servico = aP10_ContagemResultado_Servico;
         objprc_novaos.A512ContagemResultado_ValorPF = aP11_ContagemResultado_ValorPF;
         objprc_novaos.AV21Sistema_Codigo = aP12_Sistema_Codigo;
         objprc_novaos.AV20Modulo_Codigo = aP13_Modulo_Codigo;
         objprc_novaos.AV24FuncaoUsuario_Codigo = aP14_FuncaoUsuario_Codigo;
         objprc_novaos.AV27PrazoEntrega = aP15_PrazoEntrega;
         objprc_novaos.AV29Contratante_OSAutomatica = aP16_Contratante_OSAutomatica;
         objprc_novaos.A798ContagemResultado_PFBFSImp = aP17_ContagemResultado_PFBFSImp;
         objprc_novaos.A799ContagemResultado_PFLFSImp = aP18_ContagemResultado_PFLFSImp;
         objprc_novaos.AV37AgendaAtendimento_CntSrcCod = aP19_AgendaAtendimento_CntSrcCod;
         objprc_novaos.AV39PrazoAnalise = aP20_PrazoAnalise;
         objprc_novaos.AV51PrazoTipo = aP21_PrazoTipo;
         objprc_novaos.AV56RdmnIssueId = aP22_RdmnIssueId;
         objprc_novaos.AV55RdmnProjectId = aP23_RdmnProjectId;
         objprc_novaos.AV57RdmnUpdated = aP24_RdmnUpdated;
         objprc_novaos.AV58ContratadaOrigem = aP25_ContratadaOrigem;
         objprc_novaos.AV59ContadorFS = aP26_ContadorFS;
         objprc_novaos.AV60Prioridade_Codigo = aP27_Prioridade_Codigo;
         objprc_novaos.AV61ContagemResultado_CntSrvPrrPrz = aP28_ContagemResultado_CntSrvPrrPrz;
         objprc_novaos.AV62ContagemResultado_CntSrvPrrCst = aP29_ContagemResultado_CntSrvPrrCst;
         objprc_novaos.AV71DiasComplexidade = aP30_DiasComplexidade;
         objprc_novaos.AV73Evento = aP31_Evento;
         objprc_novaos.AV88ContagemResultado_QuantidadeSolicitada = aP32_ContagemResultado_QuantidadeSolicitada;
         objprc_novaos.context.SetSubmitInitialConfig(context);
         objprc_novaos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_novaos);
         aP3_ContagemResultado_Demanda=this.AV26ContagemResultado_Demanda;
         aP4_ContagemResultado_DemandaFM=this.AV28ContagemResultado_DemandaFM;
         aP15_PrazoEntrega=this.AV27PrazoEntrega;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_novaos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV29Contratante_OSAutomatica )
         {
            /* Using cursor P004R2 */
            pr_default.execute(0, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A39Contratada_Codigo = P004R2_A39Contratada_Codigo[0];
               A524Contratada_OS = P004R2_A524Contratada_OS[0];
               n524Contratada_OS = P004R2_n524Contratada_OS[0];
               A524Contratada_OS = (int)(A524Contratada_OS+1);
               n524Contratada_OS = false;
               AV28ContagemResultado_DemandaFM = StringUtil.Trim( StringUtil.Str( (decimal)(A524Contratada_OS), 8, 0));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P004R3 */
               pr_default.execute(1, new Object[] {n524Contratada_OS, A524Contratada_OS, A39Contratada_Codigo});
               pr_default.close(1);
               dsDefault.SmartCacheProvider.SetUpdated("Contratada") ;
               if (true) break;
               /* Using cursor P004R4 */
               pr_default.execute(2, new Object[] {n524Contratada_OS, A524Contratada_OS, A39Contratada_Codigo});
               pr_default.close(2);
               dsDefault.SmartCacheProvider.SetUpdated("Contratada") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            context.CommitDataStores( "PRC_NovaOS");
         }
         /* Using cursor P004R5 */
         pr_default.execute(3, new Object[] {AV37AgendaAtendimento_CntSrcCod});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A1212ContratoServicos_UnidadeContratada = P004R5_A1212ContratoServicos_UnidadeContratada[0];
            A74Contrato_Codigo = P004R5_A74Contrato_Codigo[0];
            A160ContratoServicos_Codigo = P004R5_A160ContratoServicos_Codigo[0];
            A1191ContratoServicos_Produtividade = P004R5_A1191ContratoServicos_Produtividade[0];
            n1191ContratoServicos_Produtividade = P004R5_n1191ContratoServicos_Produtividade[0];
            A1454ContratoServicos_PrazoTpDias = P004R5_A1454ContratoServicos_PrazoTpDias[0];
            n1454ContratoServicos_PrazoTpDias = P004R5_n1454ContratoServicos_PrazoTpDias[0];
            A1649ContratoServicos_PrazoInicio = P004R5_A1649ContratoServicos_PrazoInicio[0];
            n1649ContratoServicos_PrazoInicio = P004R5_n1649ContratoServicos_PrazoInicio[0];
            OV74PrazoInicio = AV74PrazoInicio;
            if ( ( A1191ContratoServicos_Produtividade > Convert.ToDecimal( 0 )) )
            {
               AV42ProdutividadeDia = A1191ContratoServicos_Produtividade;
            }
            else
            {
               /* Using cursor P004R6 */
               pr_default.execute(4, new Object[] {A74Contrato_Codigo, A1212ContratoServicos_UnidadeContratada});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A1207ContratoUnidades_ContratoCod = P004R6_A1207ContratoUnidades_ContratoCod[0];
                  A1204ContratoUnidades_UndMedCod = P004R6_A1204ContratoUnidades_UndMedCod[0];
                  A1208ContratoUnidades_Produtividade = P004R6_A1208ContratoUnidades_Produtividade[0];
                  n1208ContratoUnidades_Produtividade = P004R6_n1208ContratoUnidades_Produtividade[0];
                  AV42ProdutividadeDia = A1208ContratoUnidades_Produtividade;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(4);
            }
            AV72TipoDias = A1454ContratoServicos_PrazoTpDias;
            AV74PrazoInicio = A1649ContratoServicos_PrazoInicio;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
         /* Execute user subroutine: 'INICIOAGENDAMENTO' */
         S181 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51PrazoTipo)) && ( StringUtil.StrCmp(AV51PrazoTipo, "S") != 0 ) && (0==AV60Prioridade_Codigo) )
         {
            /* Execute user subroutine: 'CONFIRMODIADISPONIVEL' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else
         {
            if ( (0==AV60Prioridade_Codigo) )
            {
               AV40DiaDisponivel = AV27PrazoEntrega;
            }
            else
            {
               /* Execute user subroutine: 'PRAZOPRIORIDADE' */
               S171 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
               AV40DiaDisponivel = AV63PrazoPrioridade;
            }
         }
         AV9Usuario_Codigo = AV22ContagemResultado_Responsavel;
         if ( (0==AV9Usuario_Codigo) )
         {
            /* Using cursor P004R7 */
            pr_default.execute(5, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A66ContratadaUsuario_ContratadaCod = P004R7_A66ContratadaUsuario_ContratadaCod[0];
               A69ContratadaUsuario_UsuarioCod = P004R7_A69ContratadaUsuario_UsuarioCod[0];
               AV9Usuario_Codigo = A69ContratadaUsuario_UsuarioCod;
               AV54Qtd = (short)(AV54Qtd+1);
               pr_default.readNext(5);
            }
            pr_default.close(5);
            if ( AV54Qtd > 1 )
            {
               AV9Usuario_Codigo = 0;
            }
         }
         /* Execute user subroutine: 'INSERIRDEMANDA' */
         S121 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         new prc_newevidenciademanda(context ).execute( ref  AV30ContagemResultado_Codigo) ;
         AV43QtdUnd = A798ContagemResultado_PFBFSImp;
         if ( ( StringUtil.StrCmp(AV51PrazoTipo, "S") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( AV51PrazoTipo)) || ( AV60Prioridade_Codigo > 0 ) )
         {
            if ( (0==AV60Prioridade_Codigo) )
            {
               AV40DiaDisponivel = AV27PrazoEntrega;
            }
            else
            {
               AV40DiaDisponivel = AV63PrazoPrioridade;
            }
            AV48AgendaAtendimento_QtdUnd = AV43QtdUnd;
            /* Execute user subroutine: 'NEWAGENDAATENDIMENTO' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else
         {
            /* Execute user subroutine: 'AGENDAR' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         new prc_inslogresponsavel(context ).execute( ref  AV30ContagemResultado_Codigo,  AV9Usuario_Codigo,  "S",  "D",  AV53UserId,  0,  "",  "S",  "",  AV27PrazoEntrega,  false) ;
         if ( ( AV61ContagemResultado_CntSrvPrrPrz > Convert.ToDecimal( 0 )) )
         {
            new prc_inslogresponsavel(context ).execute( ref  AV30ContagemResultado_Codigo,  AV9Usuario_Codigo,  "PR",  "D",  AV53UserId,  0,  "S",  "S",  "",  AV63PrazoPrioridade,  false) ;
            AV27PrazoEntrega = AV63PrazoPrioridade;
         }
         new prc_disparoservicovinculado(context ).execute(  AV30ContagemResultado_Codigo,  AV53UserId) ;
         this.cleanup();
         if (true) return;
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'CONFIRMODIADISPONIVEL' Routine */
         GXt_int1 = AV47Dias;
         new prc_diasparaentrega(context ).execute( ref  AV37AgendaAtendimento_CntSrcCod,  A798ContagemResultado_PFBFSImp,  AV71DiasComplexidade, out  GXt_int1) ;
         AV47Dias = GXt_int1;
         GXt_dtime2 = AV40DiaDisponivel;
         new prc_adddiasuteis(context ).execute(  AV40DiaDisponivel,  AV47Dias,  AV72TipoDias, out  GXt_dtime2) ;
         AV40DiaDisponivel = GXt_dtime2;
         AV40DiaDisponivel = DateTimeUtil.TAdd( AV40DiaDisponivel, 3600*(DateTimeUtil.Hour( AV27PrazoEntrega)));
         AV40DiaDisponivel = DateTimeUtil.TAdd( AV40DiaDisponivel, 60*(DateTimeUtil.Minute( AV27PrazoEntrega)));
         if ( DateTimeUtil.ResetTime( AV27PrazoEntrega) < DateTimeUtil.ResetTime( AV40DiaDisponivel) )
         {
            AV27PrazoEntrega = AV40DiaDisponivel;
         }
         else
         {
            AV40DiaDisponivel = AV27PrazoEntrega;
         }
      }

      protected void S121( )
      {
         /* 'INSERIRDEMANDA' Routine */
         /*
            INSERT RECORD ON TABLE ContagemResultado

         */
         A457ContagemResultado_Demanda = AV26ContagemResultado_Demanda;
         n457ContagemResultado_Demanda = false;
         A493ContagemResultado_DemandaFM = AV28ContagemResultado_DemandaFM;
         n493ContagemResultado_DemandaFM = false;
         if ( (0==AV60Prioridade_Codigo) )
         {
            GXt_int1 = AV47Dias;
            new prc_diasuteisentre(context ).execute(  A471ContagemResultado_DataDmn,  DateTimeUtil.ResetTime( AV27PrazoEntrega),  AV72TipoDias, out  GXt_int1) ;
            AV47Dias = GXt_int1;
            A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV27PrazoEntrega);
            n472ContagemResultado_DataEntrega = false;
            A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(AV27PrazoEntrega);
            n912ContagemResultado_HoraEntrega = false;
            A1351ContagemResultado_DataPrevista = AV27PrazoEntrega;
            n1351ContagemResultado_DataPrevista = false;
            A1227ContagemResultado_PrazoInicialDias = AV47Dias;
            n1227ContagemResultado_PrazoInicialDias = false;
         }
         else
         {
            GXt_int1 = AV47Dias;
            new prc_diasuteisentre(context ).execute(  A471ContagemResultado_DataDmn,  DateTimeUtil.ResetTime( AV63PrazoPrioridade),  AV72TipoDias, out  GXt_int1) ;
            AV47Dias = GXt_int1;
            A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV63PrazoPrioridade);
            n472ContagemResultado_DataEntrega = false;
            A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(AV63PrazoPrioridade);
            n912ContagemResultado_HoraEntrega = false;
            A1351ContagemResultado_DataPrevista = AV63PrazoPrioridade;
            n1351ContagemResultado_DataPrevista = false;
            A1227ContagemResultado_PrazoInicialDias = AV47Dias;
            n1227ContagemResultado_PrazoInicialDias = false;
         }
         if ( (0==AV18ContagemResultado_OSVinculada) )
         {
            A602ContagemResultado_OSVinculada = 0;
            n602ContagemResultado_OSVinculada = false;
            n602ContagemResultado_OSVinculada = true;
         }
         else
         {
            A602ContagemResultado_OSVinculada = AV18ContagemResultado_OSVinculada;
            n602ContagemResultado_OSVinculada = false;
         }
         if ( (0==AV21Sistema_Codigo) )
         {
            A489ContagemResultado_SistemaCod = 0;
            n489ContagemResultado_SistemaCod = false;
            n489ContagemResultado_SistemaCod = true;
         }
         else
         {
            A489ContagemResultado_SistemaCod = AV21Sistema_Codigo;
            n489ContagemResultado_SistemaCod = false;
         }
         if ( (0==AV20Modulo_Codigo) )
         {
            A146Modulo_Codigo = 0;
            n146Modulo_Codigo = false;
            n146Modulo_Codigo = true;
         }
         else
         {
            A146Modulo_Codigo = AV20Modulo_Codigo;
            n146Modulo_Codigo = false;
         }
         if ( (0==AV24FuncaoUsuario_Codigo) )
         {
            A1044ContagemResultado_FncUsrCod = 0;
            n1044ContagemResultado_FncUsrCod = false;
            n1044ContagemResultado_FncUsrCod = true;
         }
         else
         {
            A1044ContagemResultado_FncUsrCod = AV24FuncaoUsuario_Codigo;
            n1044ContagemResultado_FncUsrCod = false;
         }
         A484ContagemResultado_StatusDmn = "S";
         n484ContagemResultado_StatusDmn = false;
         A1173ContagemResultado_OSManual = true;
         n1173ContagemResultado_OSManual = false;
         A598ContagemResultado_Baseline = false;
         n598ContagemResultado_Baseline = false;
         A508ContagemResultado_Owner = AV53UserId;
         A1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1350ContagemResultado_DataCadastro = false;
         A1389ContagemResultado_RdmnIssueId = AV56RdmnIssueId;
         n1389ContagemResultado_RdmnIssueId = false;
         A1390ContagemResultado_RdmnProjectId = AV55RdmnProjectId;
         n1390ContagemResultado_RdmnProjectId = false;
         A1392ContagemResultado_RdmnUpdated = AV57RdmnUpdated;
         n1392ContagemResultado_RdmnUpdated = false;
         A1515ContagemResultado_Evento = AV73Evento;
         n1515ContagemResultado_Evento = false;
         A1553ContagemResultado_CntSrvCod = AV37AgendaAtendimento_CntSrcCod;
         n1553ContagemResultado_CntSrvCod = false;
         if ( (0==AV60Prioridade_Codigo) )
         {
            A1443ContagemResultado_CntSrvPrrCod = 0;
            n1443ContagemResultado_CntSrvPrrCod = false;
            n1443ContagemResultado_CntSrvPrrCod = true;
         }
         else
         {
            A1443ContagemResultado_CntSrvPrrCod = AV60Prioridade_Codigo;
            n1443ContagemResultado_CntSrvPrrCod = false;
         }
         A1444ContagemResultado_CntSrvPrrPrz = AV61ContagemResultado_CntSrvPrrPrz;
         n1444ContagemResultado_CntSrvPrrPrz = false;
         A1445ContagemResultado_CntSrvPrrCst = AV62ContagemResultado_CntSrvPrrCst;
         n1445ContagemResultado_CntSrvPrrCst = false;
         if ( (0==AV58ContratadaOrigem) )
         {
            A805ContagemResultado_ContratadaOrigemCod = 0;
            n805ContagemResultado_ContratadaOrigemCod = false;
            n805ContagemResultado_ContratadaOrigemCod = true;
         }
         else
         {
            A805ContagemResultado_ContratadaOrigemCod = AV58ContratadaOrigem;
            n805ContagemResultado_ContratadaOrigemCod = false;
         }
         if ( (0==AV59ContadorFS) )
         {
            A454ContagemResultado_ContadorFSCod = 0;
            n454ContagemResultado_ContadorFSCod = false;
            n454ContagemResultado_ContadorFSCod = true;
         }
         else
         {
            A454ContagemResultado_ContadorFSCod = AV59ContadorFS;
            n454ContagemResultado_ContadorFSCod = false;
         }
         A468ContagemResultado_NaoCnfDmnCod = 0;
         n468ContagemResultado_NaoCnfDmnCod = false;
         n468ContagemResultado_NaoCnfDmnCod = true;
         A597ContagemResultado_LoteAceiteCod = 0;
         n597ContagemResultado_LoteAceiteCod = false;
         n597ContagemResultado_LoteAceiteCod = true;
         if ( (0==AV9Usuario_Codigo) )
         {
            A890ContagemResultado_Responsavel = 0;
            n890ContagemResultado_Responsavel = false;
            n890ContagemResultado_Responsavel = true;
         }
         else
         {
            A890ContagemResultado_Responsavel = AV9Usuario_Codigo;
            n890ContagemResultado_Responsavel = false;
         }
         A1043ContagemResultado_LiqLogCod = 0;
         n1043ContagemResultado_LiqLogCod = false;
         n1043ContagemResultado_LiqLogCod = true;
         A1583ContagemResultado_TipoRegistro = 1;
         A1636ContagemResultado_ServicoSS = 0;
         n1636ContagemResultado_ServicoSS = false;
         n1636ContagemResultado_ServicoSS = true;
         A2133ContagemResultado_QuantidadeSolicitada = AV88ContagemResultado_QuantidadeSolicitada;
         n2133ContagemResultado_QuantidadeSolicitada = false;
         A485ContagemResultado_EhValidacao = false;
         n485ContagemResultado_EhValidacao = false;
         A1452ContagemResultado_SS = 0;
         n1452ContagemResultado_SS = false;
         /* Using cursor P004R8 */
         pr_default.execute(6, new Object[] {n471ContagemResultado_DataDmn, A471ContagemResultado_DataDmn, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod, n457ContagemResultado_Demanda, A457ContagemResultado_Demanda, n465ContagemResultado_Link, A465ContagemResultado_Link, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n485ContagemResultado_EhValidacao, A485ContagemResultado_EhValidacao, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n493ContagemResultado_DemandaFM, A493ContagemResultado_DemandaFM, n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod, n146Modulo_Codigo, A146Modulo_Codigo, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, A508ContagemResultado_Owner, n512ContagemResultado_ValorPF, A512ContagemResultado_ValorPF, n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod, n598ContagemResultado_Baseline, A598ContagemResultado_Baseline, n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada, n798ContagemResultado_PFBFSImp, A798ContagemResultado_PFBFSImp, n799ContagemResultado_PFLFSImp, A799ContagemResultado_PFLFSImp, n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, n1044ContagemResultado_FncUsrCod, A1044ContagemResultado_FncUsrCod, n1173ContagemResultado_OSManual, A1173ContagemResultado_OSManual, n1227ContagemResultado_PrazoInicialDias, A1227ContagemResultado_PrazoInicialDias, n1350ContagemResultado_DataCadastro, A1350ContagemResultado_DataCadastro, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1389ContagemResultado_RdmnIssueId, A1389ContagemResultado_RdmnIssueId, n1390ContagemResultado_RdmnProjectId, A1390ContagemResultado_RdmnProjectId, n1392ContagemResultado_RdmnUpdated, A1392ContagemResultado_RdmnUpdated, n1443ContagemResultado_CntSrvPrrCod, A1443ContagemResultado_CntSrvPrrCod, n1444ContagemResultado_CntSrvPrrPrz, A1444ContagemResultado_CntSrvPrrPrz, n1445ContagemResultado_CntSrvPrrCst, A1445ContagemResultado_CntSrvPrrCst, n1452ContagemResultado_SS, A1452ContagemResultado_SS, n1515ContagemResultado_Evento, A1515ContagemResultado_Evento, n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, A1583ContagemResultado_TipoRegistro, n1636ContagemResultado_ServicoSS, A1636ContagemResultado_ServicoSS, n2133ContagemResultado_QuantidadeSolicitada, A2133ContagemResultado_QuantidadeSolicitada});
         A456ContagemResultado_Codigo = P004R8_A456ContagemResultado_Codigo[0];
         pr_default.close(6);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
         if ( (pr_default.getStatus(6) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         AV30ContagemResultado_Codigo = A456ContagemResultado_Codigo;
         /* Execute user subroutine: 'INSERTARTEFATOS' */
         S131 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'INSERTREQUISITOS' */
         S141 ();
         if (returnInSub) return;
         AV32WebSession.Set("LastContagemResultado_Codigo", StringUtil.Str( (decimal)(AV30ContagemResultado_Codigo), 6, 0));
         AV82DataInicio = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
         AV82DataInicio = DateTimeUtil.TAdd( AV82DataInicio, 3600*(DateTimeUtil.Hour( AV27PrazoEntrega)));
         AV82DataInicio = DateTimeUtil.TAdd( AV82DataInicio, 60*(DateTimeUtil.Minute( AV27PrazoEntrega)));
         new prc_novocicloexecucao(context ).execute(  AV30ContagemResultado_Codigo,  AV82DataInicio,  AV27PrazoEntrega,  AV72TipoDias,  true) ;
      }

      protected void S151( )
      {
         /* 'AGENDAR' Routine */
         if ( ( AV42ProdutividadeDia > Convert.ToDecimal( 0 )) )
         {
            AV40DiaDisponivel = AV75InicioAgendamento;
         }
         AV44AgendaAtendimento_Data = DateTimeUtil.ResetTime(AV40DiaDisponivel);
         /* Using cursor P004R9 */
         pr_default.execute(7, new Object[] {AV37AgendaAtendimento_CntSrcCod, AV44AgendaAtendimento_Data, n799ContagemResultado_PFLFSImp, A799ContagemResultado_PFLFSImp, n798ContagemResultado_PFBFSImp, A798ContagemResultado_PFBFSImp, n512ContagemResultado_ValorPF, A512ContagemResultado_ValorPF, n601ContagemResultado_Servico, A601ContagemResultado_Servico, n465ContagemResultado_Link, A465ContagemResultado_Link, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n471ContagemResultado_DataDmn, A471ContagemResultado_DataDmn, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A1209AgendaAtendimento_CodDmn = P004R9_A1209AgendaAtendimento_CodDmn[0];
            A1553ContagemResultado_CntSrvCod = P004R9_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P004R9_n1553ContagemResultado_CntSrvCod[0];
            A484ContagemResultado_StatusDmn = P004R9_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P004R9_n484ContagemResultado_StatusDmn[0];
            A1184AgendaAtendimento_Data = P004R9_A1184AgendaAtendimento_Data[0];
            A1183AgendaAtendimento_CntSrcCod = P004R9_A1183AgendaAtendimento_CntSrcCod[0];
            A1553ContagemResultado_CntSrvCod = P004R9_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P004R9_n1553ContagemResultado_CntSrvCod[0];
            A484ContagemResultado_StatusDmn = P004R9_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P004R9_n484ContagemResultado_StatusDmn[0];
            AV49PrimeiraDataAchada = A1184AgendaAtendimento_Data;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(7);
         }
         pr_default.close(7);
         while ( AV44AgendaAtendimento_Data < AV49PrimeiraDataAchada )
         {
            if ( (Convert.ToDecimal(0)==AV42ProdutividadeDia) || ( AV43QtdUnd <= AV42ProdutividadeDia ) )
            {
               AV48AgendaAtendimento_QtdUnd = AV43QtdUnd;
               AV43QtdUnd = 0;
               AV46Reservado = true;
               /* Execute user subroutine: 'NEWAGENDAATENDIMENTO' */
               S161 ();
               if (returnInSub) return;
               if (true) break;
            }
            else
            {
               AV43QtdUnd = (decimal)(AV43QtdUnd-AV42ProdutividadeDia);
               AV48AgendaAtendimento_QtdUnd = AV42ProdutividadeDia;
               /* Execute user subroutine: 'NEWAGENDAATENDIMENTO' */
               S161 ();
               if (returnInSub) return;
               GXt_dtime2 = AV40DiaDisponivel;
               new prc_adddiasuteis(context ).execute(  AV40DiaDisponivel,  1,  AV72TipoDias, out  GXt_dtime2) ;
               AV40DiaDisponivel = GXt_dtime2;
               AV44AgendaAtendimento_Data = DateTimeUtil.ResetTime(AV40DiaDisponivel);
            }
         }
         if ( ! AV46Reservado || ( AV43QtdUnd > Convert.ToDecimal( 0 )) )
         {
            /* Using cursor P004R11 */
            pr_default.execute(8, new Object[] {AV37AgendaAtendimento_CntSrcCod, AV44AgendaAtendimento_Data, n799ContagemResultado_PFLFSImp, A799ContagemResultado_PFLFSImp, n798ContagemResultado_PFBFSImp, A798ContagemResultado_PFBFSImp, n512ContagemResultado_ValorPF, A512ContagemResultado_ValorPF, n601ContagemResultado_Servico, A601ContagemResultado_Servico, n465ContagemResultado_Link, A465ContagemResultado_Link, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n471ContagemResultado_DataDmn, A471ContagemResultado_DataDmn, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
            while ( (pr_default.getStatus(8) != 101) )
            {
               A1209AgendaAtendimento_CodDmn = P004R11_A1209AgendaAtendimento_CodDmn[0];
               A1553ContagemResultado_CntSrvCod = P004R11_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P004R11_n1553ContagemResultado_CntSrvCod[0];
               A484ContagemResultado_StatusDmn = P004R11_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P004R11_n484ContagemResultado_StatusDmn[0];
               A1184AgendaAtendimento_Data = P004R11_A1184AgendaAtendimento_Data[0];
               A1183AgendaAtendimento_CntSrcCod = P004R11_A1183AgendaAtendimento_CntSrcCod[0];
               A40000GXC1 = P004R11_A40000GXC1[0];
               n40000GXC1 = P004R11_n40000GXC1[0];
               A1553ContagemResultado_CntSrvCod = P004R11_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P004R11_n1553ContagemResultado_CntSrvCod[0];
               A484ContagemResultado_StatusDmn = P004R11_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P004R11_n484ContagemResultado_StatusDmn[0];
               A40000GXC1 = P004R11_A40000GXC1[0];
               n40000GXC1 = P004R11_n40000GXC1[0];
               while ( DateTimeUtil.ResetTime( AV40DiaDisponivel) <= A1184AgendaAtendimento_Data )
               {
                  if ( DateTimeUtil.ResetTime( AV40DiaDisponivel) < A1184AgendaAtendimento_Data )
                  {
                     AV41TotalUndDoDia = 0;
                  }
                  else
                  {
                     AV41TotalUndDoDia = A40000GXC1;
                     AV40DiaDisponivel = DateTimeUtil.ResetTime( A1184AgendaAtendimento_Data ) ;
                     AV40DiaDisponivel = DateTimeUtil.TAdd( AV40DiaDisponivel, 3600*(DateTimeUtil.Hour( AV27PrazoEntrega)));
                     AV40DiaDisponivel = DateTimeUtil.TAdd( AV40DiaDisponivel, 60*(DateTimeUtil.Minute( AV27PrazoEntrega)));
                  }
                  if ( (Convert.ToDecimal(0)==AV42ProdutividadeDia) || ( AV41TotalUndDoDia < AV42ProdutividadeDia ) )
                  {
                     if ( (Convert.ToDecimal(0)==AV42ProdutividadeDia) || ( AV43QtdUnd <= AV42ProdutividadeDia - AV41TotalUndDoDia ) )
                     {
                        AV48AgendaAtendimento_QtdUnd = AV43QtdUnd;
                        AV46Reservado = true;
                        AV43QtdUnd = 0;
                        /* Execute user subroutine: 'NEWAGENDAATENDIMENTO' */
                        S161 ();
                        if ( returnInSub )
                        {
                           pr_default.close(8);
                           returnInSub = true;
                           if (true) return;
                        }
                        if (true) break;
                     }
                     else
                     {
                        AV43QtdUnd = (decimal)(AV43QtdUnd-(AV42ProdutividadeDia-AV41TotalUndDoDia));
                        AV48AgendaAtendimento_QtdUnd = (decimal)(AV42ProdutividadeDia-AV41TotalUndDoDia);
                        /* Execute user subroutine: 'NEWAGENDAATENDIMENTO' */
                        S161 ();
                        if ( returnInSub )
                        {
                           pr_default.close(8);
                           returnInSub = true;
                           if (true) return;
                        }
                        GXt_dtime2 = AV40DiaDisponivel;
                        new prc_adddiasuteis(context ).execute(  AV40DiaDisponivel,  1,  AV72TipoDias, out  GXt_dtime2) ;
                        AV40DiaDisponivel = GXt_dtime2;
                     }
                  }
                  else
                  {
                     GXt_dtime2 = AV40DiaDisponivel;
                     new prc_adddiasuteis(context ).execute(  AV40DiaDisponivel,  1,  AV72TipoDias, out  GXt_dtime2) ;
                     AV40DiaDisponivel = GXt_dtime2;
                  }
               }
               if ( (Convert.ToDecimal(0)==AV43QtdUnd) )
               {
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(8);
            }
            pr_default.close(8);
         }
         while ( ( AV43QtdUnd > Convert.ToDecimal( 0 )) || ( ! AV46Reservado && (Convert.ToDecimal(0)==A798ContagemResultado_PFBFSImp) ) )
         {
            if ( (Convert.ToDecimal(0)==AV42ProdutividadeDia) || ( AV43QtdUnd <= AV42ProdutividadeDia ) )
            {
               AV48AgendaAtendimento_QtdUnd = AV43QtdUnd;
               AV43QtdUnd = 0;
               AV46Reservado = true;
               /* Execute user subroutine: 'NEWAGENDAATENDIMENTO' */
               S161 ();
               if (returnInSub) return;
               if (true) break;
            }
            else
            {
               AV48AgendaAtendimento_QtdUnd = AV42ProdutividadeDia;
               AV43QtdUnd = (decimal)(AV43QtdUnd-AV42ProdutividadeDia);
               /* Execute user subroutine: 'NEWAGENDAATENDIMENTO' */
               S161 ();
               if (returnInSub) return;
               GXt_dtime2 = AV40DiaDisponivel;
               new prc_adddiasuteis(context ).execute(  AV40DiaDisponivel,  1,  AV72TipoDias, out  GXt_dtime2) ;
               AV40DiaDisponivel = GXt_dtime2;
               AV44AgendaAtendimento_Data = DateTimeUtil.ResetTime(AV40DiaDisponivel);
            }
         }
         if ( AV27PrazoEntrega < AV40DiaDisponivel )
         {
            AV27PrazoEntrega = AV40DiaDisponivel;
            /* Using cursor P004R12 */
            pr_default.execute(9, new Object[] {AV30ContagemResultado_Codigo, n471ContagemResultado_DataDmn, A471ContagemResultado_DataDmn, n465ContagemResultado_Link, A465ContagemResultado_Link, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, n512ContagemResultado_ValorPF, A512ContagemResultado_ValorPF, n798ContagemResultado_PFBFSImp, A798ContagemResultado_PFBFSImp, n799ContagemResultado_PFLFSImp, A799ContagemResultado_PFLFSImp, n601ContagemResultado_Servico, A601ContagemResultado_Servico});
            while ( (pr_default.getStatus(9) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P004R12_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P004R12_n1553ContagemResultado_CntSrvCod[0];
               A456ContagemResultado_Codigo = P004R12_A456ContagemResultado_Codigo[0];
               A472ContagemResultado_DataEntrega = P004R12_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P004R12_n472ContagemResultado_DataEntrega[0];
               A912ContagemResultado_HoraEntrega = P004R12_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P004R12_n912ContagemResultado_HoraEntrega[0];
               A1351ContagemResultado_DataPrevista = P004R12_A1351ContagemResultado_DataPrevista[0];
               n1351ContagemResultado_DataPrevista = P004R12_n1351ContagemResultado_DataPrevista[0];
               A1227ContagemResultado_PrazoInicialDias = P004R12_A1227ContagemResultado_PrazoInicialDias[0];
               n1227ContagemResultado_PrazoInicialDias = P004R12_n1227ContagemResultado_PrazoInicialDias[0];
               A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV40DiaDisponivel);
               n472ContagemResultado_DataEntrega = false;
               A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(AV40DiaDisponivel);
               n912ContagemResultado_HoraEntrega = false;
               A1351ContagemResultado_DataPrevista = AV40DiaDisponivel;
               n1351ContagemResultado_DataPrevista = false;
               GXt_int1 = A1227ContagemResultado_PrazoInicialDias;
               new prc_diasuteisentre(context ).execute(  A471ContagemResultado_DataDmn,  DateTimeUtil.ResetTime( AV40DiaDisponivel),  AV72TipoDias, out  GXt_int1) ;
               A1227ContagemResultado_PrazoInicialDias = GXt_int1;
               n1227ContagemResultado_PrazoInicialDias = false;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P004R13 */
               pr_default.execute(10, new Object[] {n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1227ContagemResultado_PrazoInicialDias, A1227ContagemResultado_PrazoInicialDias, A456ContagemResultado_Codigo});
               pr_default.close(10);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if (true) break;
               /* Using cursor P004R14 */
               pr_default.execute(11, new Object[] {n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1227ContagemResultado_PrazoInicialDias, A1227ContagemResultado_PrazoInicialDias, A456ContagemResultado_Codigo});
               pr_default.close(11);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(9);
         }
      }

      protected void S161( )
      {
         /* 'NEWAGENDAATENDIMENTO' Routine */
         /*
            INSERT RECORD ON TABLE AgendaAtendimento

         */
         A1183AgendaAtendimento_CntSrcCod = AV37AgendaAtendimento_CntSrcCod;
         A1184AgendaAtendimento_Data = DateTimeUtil.ResetTime(AV40DiaDisponivel);
         A1209AgendaAtendimento_CodDmn = AV30ContagemResultado_Codigo;
         A1186AgendaAtendimento_QtdUnd = AV48AgendaAtendimento_QtdUnd;
         /* Using cursor P004R15 */
         pr_default.execute(12, new Object[] {A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn, A1186AgendaAtendimento_QtdUnd});
         pr_default.close(12);
         dsDefault.SmartCacheProvider.SetUpdated("AgendaAtendimento") ;
         if ( (pr_default.getStatus(12) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
      }

      protected void S171( )
      {
         /* 'PRAZOPRIORIDADE' Routine */
         GXt_int1 = AV47Dias;
         new prc_diasparaentrega(context ).execute( ref  AV37AgendaAtendimento_CntSrcCod,  A798ContagemResultado_PFBFSImp,  AV71DiasComplexidade, out  GXt_int1) ;
         AV47Dias = GXt_int1;
         AV47Dias = (short)(AV47Dias*(AV61ContagemResultado_CntSrvPrrPrz/ (decimal)(100)));
         /* Using cursor P004R16 */
         pr_default.execute(13, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         while ( (pr_default.getStatus(13) != 101) )
         {
            A39Contratada_Codigo = P004R16_A39Contratada_Codigo[0];
            A52Contratada_AreaTrabalhoCod = P004R16_A52Contratada_AreaTrabalhoCod[0];
            new prc_getexpedientedocontratante(context ).execute( ref  A52Contratada_AreaTrabalhoCod, ref  AV67InicioExp, ref  AV68FimExp) ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(13);
         if ( (0==DateTimeUtil.Hour( AV67InicioExp)) )
         {
            AV67InicioExp = DateTimeUtil.ResetDate(DateTimeUtil.TAdd( AV67InicioExp, 3600*(8)));
         }
         if ( (0==DateTimeUtil.Hour( AV68FimExp)) )
         {
            AV68FimExp = DateTimeUtil.ResetDate(DateTimeUtil.TAdd( AV68FimExp, 3600*(18)));
         }
         AV64Horas = (decimal)(DateTimeUtil.Hour( AV68FimExp)-DateTimeUtil.Hour( AV67InicioExp));
         AV64Horas = (decimal)(AV64Horas*(AV61ContagemResultado_CntSrvPrrPrz/ (decimal)(100)));
         AV70Resto = (decimal)((AV64Horas-NumberUtil.Int( (long)(AV64Horas))));
         AV64Horas = (decimal)(DateTimeUtil.Hour( AV67InicioExp)+NumberUtil.Int( (long)(AV64Horas)));
         AV65Minutos = (short)(60*AV70Resto);
         AV65Minutos = (short)(DateTimeUtil.Minute( AV68FimExp)-AV65Minutos);
         GXt_dtime2 = AV63PrazoPrioridade;
         GXt_dtime3 = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
         new prc_adddiasuteis(context ).execute(  GXt_dtime3,  AV47Dias,  AV72TipoDias, out  GXt_dtime2) ;
         AV63PrazoPrioridade = GXt_dtime2;
         AV63PrazoPrioridade = DateTimeUtil.TAdd( AV63PrazoPrioridade, (int)(3600*(NumberUtil.Int( (long)(AV64Horas)))));
         AV63PrazoPrioridade = DateTimeUtil.TAdd( AV63PrazoPrioridade, 60*(AV65Minutos));
      }

      protected void S181( )
      {
         /* 'INICIOAGENDAMENTO' Routine */
         AV40DiaDisponivel = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
         GXt_int1 = AV76DiasAnalise;
         new prc_diasparaanalise(context ).execute( ref  AV37AgendaAtendimento_CntSrcCod,  A798ContagemResultado_PFBFSImp,  AV71DiasComplexidade, out  GXt_int1) ;
         AV76DiasAnalise = GXt_int1;
         if ( AV74PrazoInicio == 2 )
         {
            new prc_datadeinicioutil(context ).execute( ref  AV40DiaDisponivel) ;
         }
         else if ( (0==AV76DiasAnalise) && ( AV74PrazoInicio == 1 ) )
         {
            AV76DiasAnalise = 1;
         }
         GXt_dtime3 = AV75InicioAgendamento;
         new prc_adddiasuteis(context ).execute(  AV40DiaDisponivel,  AV76DiasAnalise,  AV72TipoDias, out  GXt_dtime3) ;
         AV75InicioAgendamento = GXt_dtime3;
      }

      protected void S131( )
      {
         /* 'INSERTARTEFATOS' Routine */
         new prc_insartefatosdaos(context ).execute( ref  AV30ContagemResultado_Codigo) ;
      }

      protected void S141( )
      {
         /* 'INSERTREQUISITOS' Routine */
         AV83SDT_Requisitos.FromXml(AV32WebSession.Get("Requisitos"), "");
         AV32WebSession.Remove("Requisitos");
         AV99GXV1 = 1;
         while ( AV99GXV1 <= AV83SDT_Requisitos.Count )
         {
            AV85SDT_RequisitoItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV83SDT_Requisitos.Item(AV99GXV1));
            if ( (0==AV85SDT_RequisitoItem.gxTpr_Requisito_codigo) )
            {
               AV87Requisito = new SdtRequisito(context);
               AV87Requisito.gxTpr_Requisito_identificador = AV85SDT_RequisitoItem.gxTpr_Requisito_identificador;
               AV87Requisito.gxTpr_Requisito_agrupador = AV85SDT_RequisitoItem.gxTpr_Requisito_agrupador;
               AV87Requisito.gxTpr_Requisito_ordem = AV85SDT_RequisitoItem.gxTpr_Requisito_ordem;
               AV87Requisito.gxTpr_Requisito_prioridade = AV85SDT_RequisitoItem.gxTpr_Requisito_prioridade;
               AV87Requisito.gxTpr_Requisito_status = AV85SDT_RequisitoItem.gxTpr_Requisito_status;
               AV87Requisito.gxTpr_Requisito_titulo = AV85SDT_RequisitoItem.gxTpr_Requisito_titulo;
               AV87Requisito.gxTpr_Requisito_descricao = AV85SDT_RequisitoItem.gxTpr_Requisito_descricao;
               if ( (0==AV85SDT_RequisitoItem.gxTpr_Requisito_tiporeqcod) )
               {
                  AV87Requisito.gxTv_SdtRequisito_Requisito_tiporeqcod_SetNull();
               }
               else
               {
                  AV87Requisito.gxTpr_Requisito_tiporeqcod = AV85SDT_RequisitoItem.gxTpr_Requisito_tiporeqcod;
               }
               AV87Requisito.gxTpr_Requisito_referenciatecnica = AV85SDT_RequisitoItem.gxTpr_Requisito_referenciatecnica;
               AV87Requisito.gxTpr_Requisito_restricao = AV85SDT_RequisitoItem.gxTpr_Requisito_restricao;
               AV87Requisito.gxTv_SdtRequisito_Requisito_reqcod_SetNull();
               AV87Requisito.gxTv_SdtRequisito_Proposta_codigo_SetNull();
               AV87Requisito.Save();
               AV86Requisito_Codigo = AV87Requisito.gxTpr_Requisito_codigo;
            }
            else
            {
               AV86Requisito_Codigo = AV85SDT_RequisitoItem.gxTpr_Requisito_codigo;
            }
            /*
               INSERT RECORD ON TABLE ContagemResultadoRequisito

            */
            A2003ContagemResultadoRequisito_OSCod = AV30ContagemResultado_Codigo;
            A2004ContagemResultadoRequisito_ReqCod = AV86Requisito_Codigo;
            A2006ContagemResultadoRequisito_Owner = (bool)(((0==AV85SDT_RequisitoItem.gxTpr_Requisito_codigo)));
            /* Using cursor P004R17 */
            pr_default.execute(14, new Object[] {A2003ContagemResultadoRequisito_OSCod, A2004ContagemResultadoRequisito_ReqCod, A2006ContagemResultadoRequisito_Owner});
            A2005ContagemResultadoRequisito_Codigo = P004R17_A2005ContagemResultadoRequisito_Codigo[0];
            pr_default.close(14);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoRequisito") ;
            if ( (pr_default.getStatus(14) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
            AV99GXV1 = (int)(AV99GXV1+1);
         }
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_NovaOS");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P004R2_A39Contratada_Codigo = new int[1] ;
         P004R2_A524Contratada_OS = new int[1] ;
         P004R2_n524Contratada_OS = new bool[] {false} ;
         P004R5_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         P004R5_A74Contrato_Codigo = new int[1] ;
         P004R5_A160ContratoServicos_Codigo = new int[1] ;
         P004R5_A1191ContratoServicos_Produtividade = new decimal[1] ;
         P004R5_n1191ContratoServicos_Produtividade = new bool[] {false} ;
         P004R5_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         P004R5_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         P004R5_A1649ContratoServicos_PrazoInicio = new short[1] ;
         P004R5_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         A1454ContratoServicos_PrazoTpDias = "";
         AV74PrazoInicio = 1;
         P004R6_A1207ContratoUnidades_ContratoCod = new int[1] ;
         P004R6_A1204ContratoUnidades_UndMedCod = new int[1] ;
         P004R6_A1208ContratoUnidades_Produtividade = new decimal[1] ;
         P004R6_n1208ContratoUnidades_Produtividade = new bool[] {false} ;
         AV72TipoDias = "";
         AV40DiaDisponivel = (DateTime)(DateTime.MinValue);
         AV63PrazoPrioridade = (DateTime)(DateTime.MinValue);
         P004R7_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P004R7_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         AV50Status = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A484ContagemResultado_StatusDmn = "";
         A1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         A1392ContagemResultado_RdmnUpdated = (DateTime)(DateTime.MinValue);
         P004R8_A456ContagemResultado_Codigo = new int[1] ;
         Gx_emsg = "";
         AV32WebSession = context.GetSession();
         AV82DataInicio = (DateTime)(DateTime.MinValue);
         AV75InicioAgendamento = (DateTime)(DateTime.MinValue);
         AV44AgendaAtendimento_Data = DateTime.MinValue;
         P004R9_A1209AgendaAtendimento_CodDmn = new int[1] ;
         P004R9_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P004R9_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P004R9_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         P004R9_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         P004R9_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         P004R9_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         P004R9_A512ContagemResultado_ValorPF = new decimal[1] ;
         P004R9_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P004R9_A601ContagemResultado_Servico = new int[1] ;
         P004R9_n601ContagemResultado_Servico = new bool[] {false} ;
         P004R9_A465ContagemResultado_Link = new String[] {""} ;
         P004R9_n465ContagemResultado_Link = new bool[] {false} ;
         P004R9_A514ContagemResultado_Observacao = new String[] {""} ;
         P004R9_n514ContagemResultado_Observacao = new bool[] {false} ;
         P004R9_A494ContagemResultado_Descricao = new String[] {""} ;
         P004R9_n494ContagemResultado_Descricao = new bool[] {false} ;
         P004R9_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P004R9_n471ContagemResultado_DataDmn = new bool[] {false} ;
         P004R9_A490ContagemResultado_ContratadaCod = new int[1] ;
         P004R9_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P004R9_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P004R9_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P004R9_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         P004R9_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         A1184AgendaAtendimento_Data = DateTime.MinValue;
         AV49PrimeiraDataAchada = DateTime.MinValue;
         P004R11_A1209AgendaAtendimento_CodDmn = new int[1] ;
         P004R11_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P004R11_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P004R11_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         P004R11_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         P004R11_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         P004R11_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         P004R11_A512ContagemResultado_ValorPF = new decimal[1] ;
         P004R11_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P004R11_A601ContagemResultado_Servico = new int[1] ;
         P004R11_n601ContagemResultado_Servico = new bool[] {false} ;
         P004R11_A465ContagemResultado_Link = new String[] {""} ;
         P004R11_n465ContagemResultado_Link = new bool[] {false} ;
         P004R11_A514ContagemResultado_Observacao = new String[] {""} ;
         P004R11_n514ContagemResultado_Observacao = new bool[] {false} ;
         P004R11_A494ContagemResultado_Descricao = new String[] {""} ;
         P004R11_n494ContagemResultado_Descricao = new bool[] {false} ;
         P004R11_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P004R11_n471ContagemResultado_DataDmn = new bool[] {false} ;
         P004R11_A490ContagemResultado_ContratadaCod = new int[1] ;
         P004R11_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P004R11_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P004R11_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P004R11_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         P004R11_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         P004R11_A40000GXC1 = new decimal[1] ;
         P004R11_n40000GXC1 = new bool[] {false} ;
         P004R12_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P004R12_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P004R12_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P004R12_n471ContagemResultado_DataDmn = new bool[] {false} ;
         P004R12_A465ContagemResultado_Link = new String[] {""} ;
         P004R12_n465ContagemResultado_Link = new bool[] {false} ;
         P004R12_A490ContagemResultado_ContratadaCod = new int[1] ;
         P004R12_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P004R12_A494ContagemResultado_Descricao = new String[] {""} ;
         P004R12_n494ContagemResultado_Descricao = new bool[] {false} ;
         P004R12_A514ContagemResultado_Observacao = new String[] {""} ;
         P004R12_n514ContagemResultado_Observacao = new bool[] {false} ;
         P004R12_A512ContagemResultado_ValorPF = new decimal[1] ;
         P004R12_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P004R12_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         P004R12_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         P004R12_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         P004R12_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         P004R12_A601ContagemResultado_Servico = new int[1] ;
         P004R12_n601ContagemResultado_Servico = new bool[] {false} ;
         P004R12_A456ContagemResultado_Codigo = new int[1] ;
         P004R12_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P004R12_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P004R12_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P004R12_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P004R12_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P004R12_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P004R12_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P004R12_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P004R16_A39Contratada_Codigo = new int[1] ;
         P004R16_A52Contratada_AreaTrabalhoCod = new int[1] ;
         AV67InicioExp = (DateTime)(DateTime.MinValue);
         AV68FimExp = (DateTime)(DateTime.MinValue);
         GXt_dtime2 = (DateTime)(DateTime.MinValue);
         GXt_dtime3 = (DateTime)(DateTime.MinValue);
         AV83SDT_Requisitos = new GxObjectCollection( context, "SDT_Requisitos.SDT_RequisitosItem", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Requisitos_SDT_RequisitosItem", "GeneXus.Programs");
         AV85SDT_RequisitoItem = new SdtSDT_Requisitos_SDT_RequisitosItem(context);
         AV87Requisito = new SdtRequisito(context);
         P004R17_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_novaos__default(),
            new Object[][] {
                new Object[] {
               P004R2_A39Contratada_Codigo, P004R2_A524Contratada_OS, P004R2_n524Contratada_OS
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P004R5_A1212ContratoServicos_UnidadeContratada, P004R5_A74Contrato_Codigo, P004R5_A160ContratoServicos_Codigo, P004R5_A1191ContratoServicos_Produtividade, P004R5_n1191ContratoServicos_Produtividade, P004R5_A1454ContratoServicos_PrazoTpDias, P004R5_n1454ContratoServicos_PrazoTpDias, P004R5_A1649ContratoServicos_PrazoInicio, P004R5_n1649ContratoServicos_PrazoInicio
               }
               , new Object[] {
               P004R6_A1207ContratoUnidades_ContratoCod, P004R6_A1204ContratoUnidades_UndMedCod, P004R6_A1208ContratoUnidades_Produtividade, P004R6_n1208ContratoUnidades_Produtividade
               }
               , new Object[] {
               P004R7_A66ContratadaUsuario_ContratadaCod, P004R7_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               P004R8_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P004R9_A1209AgendaAtendimento_CodDmn, P004R9_A1553ContagemResultado_CntSrvCod, P004R9_n1553ContagemResultado_CntSrvCod, P004R9_A799ContagemResultado_PFLFSImp, P004R9_n799ContagemResultado_PFLFSImp, P004R9_A798ContagemResultado_PFBFSImp, P004R9_n798ContagemResultado_PFBFSImp, P004R9_A512ContagemResultado_ValorPF, P004R9_n512ContagemResultado_ValorPF, P004R9_A601ContagemResultado_Servico,
               P004R9_n601ContagemResultado_Servico, P004R9_A465ContagemResultado_Link, P004R9_n465ContagemResultado_Link, P004R9_A514ContagemResultado_Observacao, P004R9_n514ContagemResultado_Observacao, P004R9_A494ContagemResultado_Descricao, P004R9_n494ContagemResultado_Descricao, P004R9_A471ContagemResultado_DataDmn, P004R9_n471ContagemResultado_DataDmn, P004R9_A490ContagemResultado_ContratadaCod,
               P004R9_n490ContagemResultado_ContratadaCod, P004R9_A484ContagemResultado_StatusDmn, P004R9_n484ContagemResultado_StatusDmn, P004R9_A1184AgendaAtendimento_Data, P004R9_A1183AgendaAtendimento_CntSrcCod
               }
               , new Object[] {
               P004R11_A1209AgendaAtendimento_CodDmn, P004R11_A1553ContagemResultado_CntSrvCod, P004R11_n1553ContagemResultado_CntSrvCod, P004R11_A799ContagemResultado_PFLFSImp, P004R11_n799ContagemResultado_PFLFSImp, P004R11_A798ContagemResultado_PFBFSImp, P004R11_n798ContagemResultado_PFBFSImp, P004R11_A512ContagemResultado_ValorPF, P004R11_n512ContagemResultado_ValorPF, P004R11_A601ContagemResultado_Servico,
               P004R11_n601ContagemResultado_Servico, P004R11_A465ContagemResultado_Link, P004R11_n465ContagemResultado_Link, P004R11_A514ContagemResultado_Observacao, P004R11_n514ContagemResultado_Observacao, P004R11_A494ContagemResultado_Descricao, P004R11_n494ContagemResultado_Descricao, P004R11_A471ContagemResultado_DataDmn, P004R11_n471ContagemResultado_DataDmn, P004R11_A490ContagemResultado_ContratadaCod,
               P004R11_n490ContagemResultado_ContratadaCod, P004R11_A484ContagemResultado_StatusDmn, P004R11_n484ContagemResultado_StatusDmn, P004R11_A1184AgendaAtendimento_Data, P004R11_A1183AgendaAtendimento_CntSrcCod, P004R11_A40000GXC1, P004R11_n40000GXC1
               }
               , new Object[] {
               P004R12_A1553ContagemResultado_CntSrvCod, P004R12_n1553ContagemResultado_CntSrvCod, P004R12_A471ContagemResultado_DataDmn, P004R12_A465ContagemResultado_Link, P004R12_n465ContagemResultado_Link, P004R12_A490ContagemResultado_ContratadaCod, P004R12_n490ContagemResultado_ContratadaCod, P004R12_A494ContagemResultado_Descricao, P004R12_n494ContagemResultado_Descricao, P004R12_A514ContagemResultado_Observacao,
               P004R12_n514ContagemResultado_Observacao, P004R12_A512ContagemResultado_ValorPF, P004R12_n512ContagemResultado_ValorPF, P004R12_A798ContagemResultado_PFBFSImp, P004R12_n798ContagemResultado_PFBFSImp, P004R12_A799ContagemResultado_PFLFSImp, P004R12_n799ContagemResultado_PFLFSImp, P004R12_A601ContagemResultado_Servico, P004R12_n601ContagemResultado_Servico, P004R12_A456ContagemResultado_Codigo,
               P004R12_A472ContagemResultado_DataEntrega, P004R12_n472ContagemResultado_DataEntrega, P004R12_A912ContagemResultado_HoraEntrega, P004R12_n912ContagemResultado_HoraEntrega, P004R12_A1351ContagemResultado_DataPrevista, P004R12_n1351ContagemResultado_DataPrevista, P004R12_A1227ContagemResultado_PrazoInicialDias, P004R12_n1227ContagemResultado_PrazoInicialDias
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P004R16_A39Contratada_Codigo, P004R16_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               P004R17_A2005ContagemResultadoRequisito_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV71DiasComplexidade ;
      private short AV73Evento ;
      private short A1649ContratoServicos_PrazoInicio ;
      private short OV74PrazoInicio ;
      private short AV74PrazoInicio ;
      private short AV54Qtd ;
      private short AV47Dias ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short A1515ContagemResultado_Evento ;
      private short A1583ContagemResultado_TipoRegistro ;
      private short AV65Minutos ;
      private short AV76DiasAnalise ;
      private short GXt_int1 ;
      private int A490ContagemResultado_ContratadaCod ;
      private int AV22ContagemResultado_Responsavel ;
      private int AV18ContagemResultado_OSVinculada ;
      private int AV53UserId ;
      private int A601ContagemResultado_Servico ;
      private int AV21Sistema_Codigo ;
      private int AV20Modulo_Codigo ;
      private int AV24FuncaoUsuario_Codigo ;
      private int AV37AgendaAtendimento_CntSrcCod ;
      private int AV56RdmnIssueId ;
      private int AV55RdmnProjectId ;
      private int AV58ContratadaOrigem ;
      private int AV59ContadorFS ;
      private int AV60Prioridade_Codigo ;
      private int A39Contratada_Codigo ;
      private int A524Contratada_OS ;
      private int A1212ContratoServicos_UnidadeContratada ;
      private int A74Contrato_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int A1207ContratoUnidades_ContratoCod ;
      private int A1204ContratoUnidades_UndMedCod ;
      private int AV9Usuario_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int AV30ContagemResultado_Codigo ;
      private int GX_INS69 ;
      private int A602ContagemResultado_OSVinculada ;
      private int A489ContagemResultado_SistemaCod ;
      private int A146Modulo_Codigo ;
      private int A1044ContagemResultado_FncUsrCod ;
      private int A508ContagemResultado_Owner ;
      private int A1389ContagemResultado_RdmnIssueId ;
      private int A1390ContagemResultado_RdmnProjectId ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1443ContagemResultado_CntSrvPrrCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A454ContagemResultado_ContadorFSCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A890ContagemResultado_Responsavel ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A1452ContagemResultado_SS ;
      private int A456ContagemResultado_Codigo ;
      private int A1209AgendaAtendimento_CodDmn ;
      private int A1183AgendaAtendimento_CntSrcCod ;
      private int GX_INS142 ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int AV99GXV1 ;
      private int AV86Requisito_Codigo ;
      private int GX_INS221 ;
      private int A2003ContagemResultadoRequisito_OSCod ;
      private int A2004ContagemResultadoRequisito_ReqCod ;
      private int A2005ContagemResultadoRequisito_Codigo ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A798ContagemResultado_PFBFSImp ;
      private decimal A799ContagemResultado_PFLFSImp ;
      private decimal AV61ContagemResultado_CntSrvPrrPrz ;
      private decimal AV62ContagemResultado_CntSrvPrrCst ;
      private decimal AV88ContagemResultado_QuantidadeSolicitada ;
      private decimal A1191ContratoServicos_Produtividade ;
      private decimal AV42ProdutividadeDia ;
      private decimal A1208ContratoUnidades_Produtividade ;
      private decimal AV43QtdUnd ;
      private decimal AV48AgendaAtendimento_QtdUnd ;
      private decimal A1444ContagemResultado_CntSrvPrrPrz ;
      private decimal A1445ContagemResultado_CntSrvPrrCst ;
      private decimal A2133ContagemResultado_QuantidadeSolicitada ;
      private decimal A40000GXC1 ;
      private decimal AV41TotalUndDoDia ;
      private decimal A1186AgendaAtendimento_QtdUnd ;
      private decimal AV64Horas ;
      private decimal AV70Resto ;
      private String AV51PrazoTipo ;
      private String scmdbuf ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String AV72TipoDias ;
      private String AV50Status ;
      private String A484ContagemResultado_StatusDmn ;
      private String Gx_emsg ;
      private DateTime AV27PrazoEntrega ;
      private DateTime AV39PrazoAnalise ;
      private DateTime AV57RdmnUpdated ;
      private DateTime AV40DiaDisponivel ;
      private DateTime AV63PrazoPrioridade ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A1350ContagemResultado_DataCadastro ;
      private DateTime A1392ContagemResultado_RdmnUpdated ;
      private DateTime AV82DataInicio ;
      private DateTime AV75InicioAgendamento ;
      private DateTime AV67InicioExp ;
      private DateTime AV68FimExp ;
      private DateTime GXt_dtime2 ;
      private DateTime GXt_dtime3 ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime AV44AgendaAtendimento_Data ;
      private DateTime A1184AgendaAtendimento_Data ;
      private DateTime AV49PrimeiraDataAchada ;
      private bool AV29Contratante_OSAutomatica ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n524Contratada_OS ;
      private bool n1191ContratoServicos_Produtividade ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private bool n1649ContratoServicos_PrazoInicio ;
      private bool n1208ContratoUnidades_Produtividade ;
      private bool returnInSub ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n146Modulo_Codigo ;
      private bool n1044ContagemResultado_FncUsrCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool A1173ContagemResultado_OSManual ;
      private bool n1173ContagemResultado_OSManual ;
      private bool A598ContagemResultado_Baseline ;
      private bool n598ContagemResultado_Baseline ;
      private bool n1350ContagemResultado_DataCadastro ;
      private bool n1389ContagemResultado_RdmnIssueId ;
      private bool n1390ContagemResultado_RdmnProjectId ;
      private bool n1392ContagemResultado_RdmnUpdated ;
      private bool n1515ContagemResultado_Evento ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1443ContagemResultado_CntSrvPrrCod ;
      private bool n1444ContagemResultado_CntSrvPrrPrz ;
      private bool n1445ContagemResultado_CntSrvPrrCst ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n454ContagemResultado_ContadorFSCod ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool n2133ContagemResultado_QuantidadeSolicitada ;
      private bool A485ContagemResultado_EhValidacao ;
      private bool n485ContagemResultado_EhValidacao ;
      private bool n1452ContagemResultado_SS ;
      private bool n471ContagemResultado_DataDmn ;
      private bool n465ContagemResultado_Link ;
      private bool n494ContagemResultado_Descricao ;
      private bool n514ContagemResultado_Observacao ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n798ContagemResultado_PFBFSImp ;
      private bool n799ContagemResultado_PFLFSImp ;
      private bool n601ContagemResultado_Servico ;
      private bool AV46Reservado ;
      private bool n40000GXC1 ;
      private bool A2006ContagemResultadoRequisito_Owner ;
      private String A514ContagemResultado_Observacao ;
      private String A465ContagemResultado_Link ;
      private String AV26ContagemResultado_Demanda ;
      private String AV28ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP3_ContagemResultado_Demanda ;
      private String aP4_ContagemResultado_DemandaFM ;
      private DateTime aP15_PrazoEntrega ;
      private IDataStoreProvider pr_default ;
      private int[] P004R2_A39Contratada_Codigo ;
      private int[] P004R2_A524Contratada_OS ;
      private bool[] P004R2_n524Contratada_OS ;
      private int[] P004R5_A1212ContratoServicos_UnidadeContratada ;
      private int[] P004R5_A74Contrato_Codigo ;
      private int[] P004R5_A160ContratoServicos_Codigo ;
      private decimal[] P004R5_A1191ContratoServicos_Produtividade ;
      private bool[] P004R5_n1191ContratoServicos_Produtividade ;
      private String[] P004R5_A1454ContratoServicos_PrazoTpDias ;
      private bool[] P004R5_n1454ContratoServicos_PrazoTpDias ;
      private short[] P004R5_A1649ContratoServicos_PrazoInicio ;
      private bool[] P004R5_n1649ContratoServicos_PrazoInicio ;
      private int[] P004R6_A1207ContratoUnidades_ContratoCod ;
      private int[] P004R6_A1204ContratoUnidades_UndMedCod ;
      private decimal[] P004R6_A1208ContratoUnidades_Produtividade ;
      private bool[] P004R6_n1208ContratoUnidades_Produtividade ;
      private int[] P004R7_A66ContratadaUsuario_ContratadaCod ;
      private int[] P004R7_A69ContratadaUsuario_UsuarioCod ;
      private int[] P004R8_A456ContagemResultado_Codigo ;
      private int[] P004R9_A1209AgendaAtendimento_CodDmn ;
      private int[] P004R9_A1553ContagemResultado_CntSrvCod ;
      private bool[] P004R9_n1553ContagemResultado_CntSrvCod ;
      private decimal[] P004R9_A799ContagemResultado_PFLFSImp ;
      private bool[] P004R9_n799ContagemResultado_PFLFSImp ;
      private decimal[] P004R9_A798ContagemResultado_PFBFSImp ;
      private bool[] P004R9_n798ContagemResultado_PFBFSImp ;
      private decimal[] P004R9_A512ContagemResultado_ValorPF ;
      private bool[] P004R9_n512ContagemResultado_ValorPF ;
      private int[] P004R9_A601ContagemResultado_Servico ;
      private bool[] P004R9_n601ContagemResultado_Servico ;
      private String[] P004R9_A465ContagemResultado_Link ;
      private bool[] P004R9_n465ContagemResultado_Link ;
      private String[] P004R9_A514ContagemResultado_Observacao ;
      private bool[] P004R9_n514ContagemResultado_Observacao ;
      private String[] P004R9_A494ContagemResultado_Descricao ;
      private bool[] P004R9_n494ContagemResultado_Descricao ;
      private DateTime[] P004R9_A471ContagemResultado_DataDmn ;
      private bool[] P004R9_n471ContagemResultado_DataDmn ;
      private int[] P004R9_A490ContagemResultado_ContratadaCod ;
      private bool[] P004R9_n490ContagemResultado_ContratadaCod ;
      private String[] P004R9_A484ContagemResultado_StatusDmn ;
      private bool[] P004R9_n484ContagemResultado_StatusDmn ;
      private DateTime[] P004R9_A1184AgendaAtendimento_Data ;
      private int[] P004R9_A1183AgendaAtendimento_CntSrcCod ;
      private int[] P004R11_A1209AgendaAtendimento_CodDmn ;
      private int[] P004R11_A1553ContagemResultado_CntSrvCod ;
      private bool[] P004R11_n1553ContagemResultado_CntSrvCod ;
      private decimal[] P004R11_A799ContagemResultado_PFLFSImp ;
      private bool[] P004R11_n799ContagemResultado_PFLFSImp ;
      private decimal[] P004R11_A798ContagemResultado_PFBFSImp ;
      private bool[] P004R11_n798ContagemResultado_PFBFSImp ;
      private decimal[] P004R11_A512ContagemResultado_ValorPF ;
      private bool[] P004R11_n512ContagemResultado_ValorPF ;
      private int[] P004R11_A601ContagemResultado_Servico ;
      private bool[] P004R11_n601ContagemResultado_Servico ;
      private String[] P004R11_A465ContagemResultado_Link ;
      private bool[] P004R11_n465ContagemResultado_Link ;
      private String[] P004R11_A514ContagemResultado_Observacao ;
      private bool[] P004R11_n514ContagemResultado_Observacao ;
      private String[] P004R11_A494ContagemResultado_Descricao ;
      private bool[] P004R11_n494ContagemResultado_Descricao ;
      private DateTime[] P004R11_A471ContagemResultado_DataDmn ;
      private bool[] P004R11_n471ContagemResultado_DataDmn ;
      private int[] P004R11_A490ContagemResultado_ContratadaCod ;
      private bool[] P004R11_n490ContagemResultado_ContratadaCod ;
      private String[] P004R11_A484ContagemResultado_StatusDmn ;
      private bool[] P004R11_n484ContagemResultado_StatusDmn ;
      private DateTime[] P004R11_A1184AgendaAtendimento_Data ;
      private int[] P004R11_A1183AgendaAtendimento_CntSrcCod ;
      private decimal[] P004R11_A40000GXC1 ;
      private bool[] P004R11_n40000GXC1 ;
      private int[] P004R12_A1553ContagemResultado_CntSrvCod ;
      private bool[] P004R12_n1553ContagemResultado_CntSrvCod ;
      private DateTime[] P004R12_A471ContagemResultado_DataDmn ;
      private bool[] P004R12_n471ContagemResultado_DataDmn ;
      private String[] P004R12_A465ContagemResultado_Link ;
      private bool[] P004R12_n465ContagemResultado_Link ;
      private int[] P004R12_A490ContagemResultado_ContratadaCod ;
      private bool[] P004R12_n490ContagemResultado_ContratadaCod ;
      private String[] P004R12_A494ContagemResultado_Descricao ;
      private bool[] P004R12_n494ContagemResultado_Descricao ;
      private String[] P004R12_A514ContagemResultado_Observacao ;
      private bool[] P004R12_n514ContagemResultado_Observacao ;
      private decimal[] P004R12_A512ContagemResultado_ValorPF ;
      private bool[] P004R12_n512ContagemResultado_ValorPF ;
      private decimal[] P004R12_A798ContagemResultado_PFBFSImp ;
      private bool[] P004R12_n798ContagemResultado_PFBFSImp ;
      private decimal[] P004R12_A799ContagemResultado_PFLFSImp ;
      private bool[] P004R12_n799ContagemResultado_PFLFSImp ;
      private int[] P004R12_A601ContagemResultado_Servico ;
      private bool[] P004R12_n601ContagemResultado_Servico ;
      private int[] P004R12_A456ContagemResultado_Codigo ;
      private DateTime[] P004R12_A472ContagemResultado_DataEntrega ;
      private bool[] P004R12_n472ContagemResultado_DataEntrega ;
      private DateTime[] P004R12_A912ContagemResultado_HoraEntrega ;
      private bool[] P004R12_n912ContagemResultado_HoraEntrega ;
      private DateTime[] P004R12_A1351ContagemResultado_DataPrevista ;
      private bool[] P004R12_n1351ContagemResultado_DataPrevista ;
      private short[] P004R12_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P004R12_n1227ContagemResultado_PrazoInicialDias ;
      private int[] P004R16_A39Contratada_Codigo ;
      private int[] P004R16_A52Contratada_AreaTrabalhoCod ;
      private int[] P004R17_A2005ContagemResultadoRequisito_Codigo ;
      private IGxSession AV32WebSession ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Requisitos_SDT_RequisitosItem ))]
      private IGxCollection AV83SDT_Requisitos ;
      private SdtSDT_Requisitos_SDT_RequisitosItem AV85SDT_RequisitoItem ;
      private SdtRequisito AV87Requisito ;
   }

   public class prc_novaos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004R2 ;
          prmP004R2 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004R3 ;
          prmP004R3 = new Object[] {
          new Object[] {"@Contratada_OS",SqlDbType.Int,8,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004R4 ;
          prmP004R4 = new Object[] {
          new Object[] {"@Contratada_OS",SqlDbType.Int,8,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004R5 ;
          prmP004R5 = new Object[] {
          new Object[] {"@AV37AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004R6 ;
          prmP004R6 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004R7 ;
          prmP004R7 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004R8 ;
          prmP004R8 = new Object[] {
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultado_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_EhValidacao",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Baseline",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_PFBFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_FncUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_OSManual",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_PrazoInicialDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_DataCadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_RdmnIssueId",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_RdmnProjectId",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_RdmnUpdated",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_CntSrvPrrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_CntSrvPrrPrz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_CntSrvPrrCst",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_Evento",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_TipoRegistro",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_ServicoSS",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_QuantidadeSolicitada",SqlDbType.Decimal,9,4}
          } ;
          String cmdBufferP004R8 ;
          cmdBufferP004R8=" INSERT INTO [ContagemResultado]([ContagemResultado_DataDmn], [ContagemResultado_DataEntrega], [ContagemResultado_ContadorFSCod], [ContagemResultado_Demanda], [ContagemResultado_Link], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_StatusDmn], [ContagemResultado_EhValidacao], [ContagemResultado_ContratadaCod], [ContagemResultado_DemandaFM], [ContagemResultado_Descricao], [ContagemResultado_SistemaCod], [Modulo_Codigo], [ContagemResultado_Observacao], [ContagemResultado_Owner], [ContagemResultado_ValorPF], [ContagemResultado_LoteAceiteCod], [ContagemResultado_Baseline], [ContagemResultado_OSVinculada], [ContagemResultado_PFBFSImp], [ContagemResultado_PFLFSImp], [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_Responsavel], [ContagemResultado_HoraEntrega], [ContagemResultado_LiqLogCod], [ContagemResultado_FncUsrCod], [ContagemResultado_OSManual], [ContagemResultado_PrazoInicialDias], [ContagemResultado_DataCadastro], [ContagemResultado_DataPrevista], [ContagemResultado_RdmnIssueId], [ContagemResultado_RdmnProjectId], [ContagemResultado_RdmnUpdated], [ContagemResultado_CntSrvPrrCod], [ContagemResultado_CntSrvPrrPrz], [ContagemResultado_CntSrvPrrCst], [ContagemResultado_SS], [ContagemResultado_Evento], [ContagemResultado_CntSrvCod], [ContagemResultado_TipoRegistro], [ContagemResultado_ServicoSS], [ContagemResultado_QuantidadeSolicitada], [ContagemResultado_Evidencia], [ContagemResultado_Agrupador], [ContagemResultado_GlsData], [ContagemResultado_GlsDescricao], [ContagemResultado_GlsValor], [ContagemResultado_GlsUser], [ContagemResultado_PBFinal], [ContagemResultado_PLFinal], [ContagemResultado_Custo], [ContagemResultado_PrazoMaisDias], [ContagemResultado_DataHomologacao], [ContagemResultado_DataExecucao], [ContagemResultado_TemDpnHmlg], [ContagemResultado_TmpEstExc], "
          + " [ContagemResultado_TmpEstCrr], [ContagemResultado_InicioExc], [ContagemResultado_FimExc], [ContagemResultado_InicioCrr], [ContagemResultado_FimCrr], [ContagemResultado_TmpEstAnl], [ContagemResultado_InicioAnl], [ContagemResultado_FimAnl], [ContagemResultado_ProjetoCod], [ContagemResultado_VlrAceite], [ContagemResultado_UOOwner], [ContagemResultado_Referencia], [ContagemResultado_Restricoes], [ContagemResultado_PrioridadePrevista], [ContagemResultado_Combinada], [ContagemResultado_Entrega], [ContagemResultado_DataInicio], [ContagemResultado_SemCusto], [ContagemResultado_VlrCnc], [ContagemResultado_PFCnc], [ContagemResultado_DataPrvPgm], [ContagemResultado_DataEntregaReal]) VALUES(@ContagemResultado_DataDmn, @ContagemResultado_DataEntrega, @ContagemResultado_ContadorFSCod, @ContagemResultado_Demanda, @ContagemResultado_Link, @ContagemResultado_NaoCnfDmnCod, @ContagemResultado_StatusDmn, @ContagemResultado_EhValidacao, @ContagemResultado_ContratadaCod, @ContagemResultado_DemandaFM, @ContagemResultado_Descricao, @ContagemResultado_SistemaCod, @Modulo_Codigo, @ContagemResultado_Observacao, @ContagemResultado_Owner, @ContagemResultado_ValorPF, @ContagemResultado_LoteAceiteCod, @ContagemResultado_Baseline, @ContagemResultado_OSVinculada, @ContagemResultado_PFBFSImp, @ContagemResultado_PFLFSImp, @ContagemResultado_ContratadaOrigemCod, @ContagemResultado_Responsavel, @ContagemResultado_HoraEntrega, @ContagemResultado_LiqLogCod, @ContagemResultado_FncUsrCod, @ContagemResultado_OSManual, @ContagemResultado_PrazoInicialDias, @ContagemResultado_DataCadastro, @ContagemResultado_DataPrevista, @ContagemResultado_RdmnIssueId, @ContagemResultado_RdmnProjectId, @ContagemResultado_RdmnUpdated, @ContagemResultado_CntSrvPrrCod, @ContagemResultado_CntSrvPrrPrz, @ContagemResultado_CntSrvPrrCst,"
          + " @ContagemResultado_SS, @ContagemResultado_Evento, @ContagemResultado_CntSrvCod, @ContagemResultado_TipoRegistro, @ContagemResultado_ServicoSS, @ContagemResultado_QuantidadeSolicitada, '', '', convert( DATETIME, '17530101', 112 ), '', convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(int, 0), '', '', '', convert(bit, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 )); SELECT SCOPE_IDENTITY()" ;
          Object[] prmP004R9 ;
          prmP004R9 = new Object[] {
          new Object[] {"@AV37AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44AgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_PFLFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Servico",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004R11 ;
          prmP004R11 = new Object[] {
          new Object[] {"@AV37AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44AgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_PFLFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Servico",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP004R11 ;
          cmdBufferP004R11=" SELECT DISTINCT NULL AS [AgendaAtendimento_CodDmn], NULL AS [ContagemResultado_CntSrvCod], NULL AS [ContagemResultado_PFLFSImp], NULL AS [ContagemResultado_PFBFSImp], NULL AS [ContagemResultado_ValorPF], NULL AS [ContagemResultado_Servico], NULL AS [ContagemResultado_Link], NULL AS [ContagemResultado_Observacao], NULL AS [ContagemResultado_Descricao], NULL AS [ContagemResultado_DataDmn], NULL AS [ContagemResultado_ContratadaCod], NULL AS [ContagemResultado_StatusDmn], [AgendaAtendimento_Data], [AgendaAtendimento_CntSrcCod], [GXC1] FROM ( SELECT TOP(100) PERCENT T1.[AgendaAtendimento_CodDmn] AS AgendaAtendimento_CodDmn, T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[ContagemResultado_PFLFSImp], T2.[ContagemResultado_PFBFSImp], T2.[ContagemResultado_ValorPF], T3.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContagemResultado_Link], T2.[ContagemResultado_Observacao], T2.[ContagemResultado_Descricao], T2.[ContagemResultado_DataDmn], T2.[ContagemResultado_ContratadaCod], T2.[ContagemResultado_StatusDmn], T1.[AgendaAtendimento_Data], T1.[AgendaAtendimento_CntSrcCod], COALESCE( T4.[GXC1], 0) AS GXC1 FROM ((([AgendaAtendimento] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[AgendaAtendimento_CodDmn]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN (SELECT SUM([AgendaAtendimento_QtdUnd]) AS GXC1, [AgendaAtendimento_CntSrcCod], [AgendaAtendimento_Data] FROM [AgendaAtendimento] WITH (NOLOCK) GROUP BY [AgendaAtendimento_CntSrcCod], [AgendaAtendimento_Data] ) T4 ON T4.[AgendaAtendimento_CntSrcCod] = T1.[AgendaAtendimento_CntSrcCod] AND T4.[AgendaAtendimento_Data] = T1.[AgendaAtendimento_Data]) WHERE (T1.[AgendaAtendimento_CntSrcCod] "
          + " = @AV37AgendaAtendimento_CntSrcCod and T1.[AgendaAtendimento_Data] >= @AV44AgendaAtendimento_Data) AND (T2.[ContagemResultado_PFLFSImp] = @ContagemResultado_PFLFSImp) AND (T2.[ContagemResultado_PFBFSImp] = @ContagemResultado_PFBFSImp) AND (T2.[ContagemResultado_ValorPF] = @ContagemResultado_ValorPF) AND (T3.[Servico_Codigo] = @ContagemResultado_Servico) AND (T2.[ContagemResultado_Link] = @ContagemResultado_Link) AND (T2.[ContagemResultado_Observacao] = @ContagemResultado_Observacao) AND (T2.[ContagemResultado_Descricao] = @ContagemResultado_Descricao) AND (T2.[ContagemResultado_DataDmn] = @ContagemResultado_DataDmn) AND (T2.[ContagemResultado_ContratadaCod] = @ContagemResultado_ContratadaCod) AND (T2.[ContagemResultado_StatusDmn] <> 'X') ORDER BY T1.[AgendaAtendimento_CntSrcCod]) DistinctT ORDER BY [AgendaAtendimento_CntSrcCod]" ;
          Object[] prmP004R12 ;
          prmP004R12 = new Object[] {
          new Object[] {"@AV30ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_PFBFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004R13 ;
          prmP004R13 = new Object[] {
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_PrazoInicialDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004R14 ;
          prmP004R14 = new Object[] {
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_PrazoInicialDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004R15 ;
          prmP004R15 = new Object[] {
          new Object[] {"@AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AgendaAtendimento_CodDmn",SqlDbType.Int,6,0} ,
          new Object[] {"@AgendaAtendimento_QtdUnd",SqlDbType.Decimal,14,5}
          } ;
          Object[] prmP004R16 ;
          prmP004R16 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004R17 ;
          prmP004R17 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoRequisito_ReqCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoRequisito_Owner",SqlDbType.Bit,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004R2", "SELECT TOP 1 [Contratada_Codigo], [Contratada_OS] FROM [Contratada] WITH (UPDLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ORDER BY [Contratada_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004R2,1,0,true,true )
             ,new CursorDef("P004R3", "UPDATE [Contratada] SET [Contratada_OS]=@Contratada_OS  WHERE [Contratada_Codigo] = @Contratada_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004R3)
             ,new CursorDef("P004R4", "UPDATE [Contratada] SET [Contratada_OS]=@Contratada_OS  WHERE [Contratada_Codigo] = @Contratada_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004R4)
             ,new CursorDef("P004R5", "SELECT TOP 1 [ContratoServicos_UnidadeContratada], [Contrato_Codigo], [ContratoServicos_Codigo], [ContratoServicos_Produtividade], [ContratoServicos_PrazoTpDias], [ContratoServicos_PrazoInicio] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV37AgendaAtendimento_CntSrcCod ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004R5,1,0,true,true )
             ,new CursorDef("P004R6", "SELECT TOP 1 [ContratoUnidades_ContratoCod], [ContratoUnidades_UndMedCod], [ContratoUnidades_Produtividade] FROM [ContratoUnidades] WITH (NOLOCK) WHERE [ContratoUnidades_ContratoCod] = @Contrato_Codigo and [ContratoUnidades_UndMedCod] = @ContratoServicos_UnidadeContratada ORDER BY [ContratoUnidades_ContratoCod], [ContratoUnidades_UndMedCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004R6,1,0,false,true )
             ,new CursorDef("P004R7", "SELECT [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @ContagemResultado_ContratadaCod ORDER BY [ContratadaUsuario_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004R7,100,0,false,false )
             ,new CursorDef("P004R8", cmdBufferP004R8, GxErrorMask.GX_NOMASK,prmP004R8)
             ,new CursorDef("P004R9", "SELECT TOP 1 T1.[AgendaAtendimento_CodDmn] AS AgendaAtendimento_CodDmn, T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[ContagemResultado_PFLFSImp], T2.[ContagemResultado_PFBFSImp], T2.[ContagemResultado_ValorPF], T3.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContagemResultado_Link], T2.[ContagemResultado_Observacao], T2.[ContagemResultado_Descricao], T2.[ContagemResultado_DataDmn], T2.[ContagemResultado_ContratadaCod], T2.[ContagemResultado_StatusDmn], T1.[AgendaAtendimento_Data], T1.[AgendaAtendimento_CntSrcCod] FROM (([AgendaAtendimento] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[AgendaAtendimento_CodDmn]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE (T1.[AgendaAtendimento_CntSrcCod] = @AV37AgendaAtendimento_CntSrcCod and T1.[AgendaAtendimento_Data] >= @AV44AgendaAtendimento_Data) AND (T2.[ContagemResultado_PFLFSImp] = @ContagemResultado_PFLFSImp) AND (T2.[ContagemResultado_PFBFSImp] = @ContagemResultado_PFBFSImp) AND (T2.[ContagemResultado_ValorPF] = @ContagemResultado_ValorPF) AND (T3.[Servico_Codigo] = @ContagemResultado_Servico) AND (T2.[ContagemResultado_Link] = @ContagemResultado_Link) AND (T2.[ContagemResultado_Observacao] = @ContagemResultado_Observacao) AND (T2.[ContagemResultado_Descricao] = @ContagemResultado_Descricao) AND (T2.[ContagemResultado_DataDmn] = @ContagemResultado_DataDmn) AND (T2.[ContagemResultado_ContratadaCod] = @ContagemResultado_ContratadaCod) AND (T2.[ContagemResultado_StatusDmn] <> 'X') ORDER BY T1.[AgendaAtendimento_CntSrcCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004R9,1,0,false,true )
             ,new CursorDef("P004R11", cmdBufferP004R11,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004R11,100,0,true,false )
             ,new CursorDef("P004R12", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_Link], T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_Observacao], T1.[ContagemResultado_ValorPF], T1.[ContagemResultado_PFBFSImp], T1.[ContagemResultado_PFLFSImp], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_PrazoInicialDias] FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE (T1.[ContagemResultado_Codigo] = @AV30ContagemResultado_Codigo) AND (T1.[ContagemResultado_DataDmn] = @ContagemResultado_DataDmn) AND (T1.[ContagemResultado_Link] = @ContagemResultado_Link) AND (T1.[ContagemResultado_ContratadaCod] = @ContagemResultado_ContratadaCod) AND (T1.[ContagemResultado_Descricao] = @ContagemResultado_Descricao) AND (T1.[ContagemResultado_Observacao] = @ContagemResultado_Observacao) AND (T1.[ContagemResultado_ValorPF] = @ContagemResultado_ValorPF) AND (T1.[ContagemResultado_PFBFSImp] = @ContagemResultado_PFBFSImp) AND (T1.[ContagemResultado_PFLFSImp] = @ContagemResultado_PFLFSImp) AND (T2.[Servico_Codigo] = @ContagemResultado_Servico) ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004R12,1,0,true,true )
             ,new CursorDef("P004R13", "UPDATE [ContagemResultado] SET [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_PrazoInicialDias]=@ContagemResultado_PrazoInicialDias  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004R13)
             ,new CursorDef("P004R14", "UPDATE [ContagemResultado] SET [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_PrazoInicialDias]=@ContagemResultado_PrazoInicialDias  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004R14)
             ,new CursorDef("P004R15", "INSERT INTO [AgendaAtendimento]([AgendaAtendimento_CntSrcCod], [AgendaAtendimento_Data], [AgendaAtendimento_CodDmn], [AgendaAtendimento_QtdUnd]) VALUES(@AgendaAtendimento_CntSrcCod, @AgendaAtendimento_Data, @AgendaAtendimento_CodDmn, @AgendaAtendimento_QtdUnd)", GxErrorMask.GX_NOMASK,prmP004R15)
             ,new CursorDef("P004R16", "SELECT TOP 1 [Contratada_Codigo], [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004R16,1,0,true,true )
             ,new CursorDef("P004R17", "INSERT INTO [ContagemResultadoRequisito]([ContagemResultadoRequisito_OSCod], [ContagemResultadoRequisito_ReqCod], [ContagemResultadoRequisito_Owner]) VALUES(@ContagemResultadoRequisito_OSCod, @ContagemResultadoRequisito_ReqCod, @ContagemResultadoRequisito_Owner); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP004R17)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[23])[0] = rslt.getGXDate(13) ;
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[23])[0] = rslt.getGXDate(13) ;
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((decimal[]) buf[25])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[22])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[24])[0] = rslt.getGXDateTime(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((short[]) buf[26])[0] = rslt.getShort(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(8, (bool)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 11 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[27]);
                }
                stmt.SetParameter(15, (int)parms[28]);
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 18 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(18, (bool)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 19 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(19, (int)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 20 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(20, (decimal)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 21 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(21, (decimal)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 24 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(24, (DateTime)parms[46]);
                }
                if ( (bool)parms[47] )
                {
                   stmt.setNull( 25 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(25, (int)parms[48]);
                }
                if ( (bool)parms[49] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[50]);
                }
                if ( (bool)parms[51] )
                {
                   stmt.setNull( 27 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(27, (bool)parms[52]);
                }
                if ( (bool)parms[53] )
                {
                   stmt.setNull( 28 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(28, (short)parms[54]);
                }
                if ( (bool)parms[55] )
                {
                   stmt.setNull( 29 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(29, (DateTime)parms[56]);
                }
                if ( (bool)parms[57] )
                {
                   stmt.setNull( 30 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(30, (DateTime)parms[58]);
                }
                if ( (bool)parms[59] )
                {
                   stmt.setNull( 31 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(31, (int)parms[60]);
                }
                if ( (bool)parms[61] )
                {
                   stmt.setNull( 32 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(32, (int)parms[62]);
                }
                if ( (bool)parms[63] )
                {
                   stmt.setNull( 33 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(33, (DateTime)parms[64]);
                }
                if ( (bool)parms[65] )
                {
                   stmt.setNull( 34 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(34, (int)parms[66]);
                }
                if ( (bool)parms[67] )
                {
                   stmt.setNull( 35 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(35, (decimal)parms[68]);
                }
                if ( (bool)parms[69] )
                {
                   stmt.setNull( 36 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(36, (decimal)parms[70]);
                }
                if ( (bool)parms[71] )
                {
                   stmt.setNull( 37 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(37, (int)parms[72]);
                }
                if ( (bool)parms[73] )
                {
                   stmt.setNull( 38 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(38, (short)parms[74]);
                }
                if ( (bool)parms[75] )
                {
                   stmt.setNull( 39 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(39, (int)parms[76]);
                }
                stmt.SetParameter(40, (short)parms[77]);
                if ( (bool)parms[78] )
                {
                   stmt.setNull( 41 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(41, (int)parms[79]);
                }
                if ( (bool)parms[80] )
                {
                   stmt.setNull( 42 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(42, (decimal)parms[81]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(10, (DateTime)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[19]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(10, (DateTime)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[19]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[18]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                return;
       }
    }

 }

}
