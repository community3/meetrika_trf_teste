/*
               File: GetPromptUsuarioPerfilFilterData
        Description: Get Prompt Usuario Perfil Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:4:47.51
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptusuarioperfilfilterdata : GXProcedure
   {
      public getpromptusuarioperfilfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptusuarioperfilfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptusuarioperfilfilterdata objgetpromptusuarioperfilfilterdata;
         objgetpromptusuarioperfilfilterdata = new getpromptusuarioperfilfilterdata();
         objgetpromptusuarioperfilfilterdata.AV20DDOName = aP0_DDOName;
         objgetpromptusuarioperfilfilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetpromptusuarioperfilfilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptusuarioperfilfilterdata.AV24OptionsJson = "" ;
         objgetpromptusuarioperfilfilterdata.AV27OptionsDescJson = "" ;
         objgetpromptusuarioperfilfilterdata.AV29OptionIndexesJson = "" ;
         objgetpromptusuarioperfilfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptusuarioperfilfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptusuarioperfilfilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptusuarioperfilfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_USUARIO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADUSUARIO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_USUARIO_PESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADUSUARIO_PESSOANOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_PERFIL_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADPERFIL_NOMEOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("PromptUsuarioPerfilGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptUsuarioPerfilGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("PromptUsuarioPerfilGridState"), "");
         }
         AV49GXV1 = 1;
         while ( AV49GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV49GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME") == 0 )
            {
               AV10TFUsuario_Nome = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME_SEL") == 0 )
            {
               AV11TFUsuario_Nome_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFUSUARIO_PESSOANOM") == 0 )
            {
               AV12TFUsuario_PessoaNom = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFUSUARIO_PESSOANOM_SEL") == 0 )
            {
               AV13TFUsuario_PessoaNom_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFPERFIL_NOME") == 0 )
            {
               AV14TFPerfil_Nome = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFPERFIL_NOME_SEL") == 0 )
            {
               AV15TFPerfil_Nome_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFPERFIL_TIPO_SEL") == 0 )
            {
               AV16TFPerfil_Tipo_SelsJson = AV34GridStateFilterValue.gxTpr_Value;
               AV17TFPerfil_Tipo_Sels.FromJSonString(AV16TFPerfil_Tipo_SelsJson);
            }
            AV49GXV1 = (int)(AV49GXV1+1);
         }
         if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(1));
            AV36DynamicFiltersSelector1 = AV35GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "PERFIL_NOME") == 0 )
            {
               AV37Perfil_Nome1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 )
            {
               AV38Usuario_PessoaNom1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV39DynamicFiltersEnabled2 = true;
               AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(2));
               AV40DynamicFiltersSelector2 = AV35GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "PERFIL_NOME") == 0 )
               {
                  AV41Perfil_Nome2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 )
               {
                  AV42Usuario_PessoaNom2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV43DynamicFiltersEnabled3 = true;
                  AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(3));
                  AV44DynamicFiltersSelector3 = AV35GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "PERFIL_NOME") == 0 )
                  {
                     AV45Perfil_Nome3 = AV35GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 )
                  {
                     AV46Usuario_PessoaNom3 = AV35GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADUSUARIO_NOMEOPTIONS' Routine */
         AV10TFUsuario_Nome = AV18SearchTxt;
         AV11TFUsuario_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A275Perfil_Tipo ,
                                              AV17TFPerfil_Tipo_Sels ,
                                              AV36DynamicFiltersSelector1 ,
                                              AV37Perfil_Nome1 ,
                                              AV38Usuario_PessoaNom1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV41Perfil_Nome2 ,
                                              AV42Usuario_PessoaNom2 ,
                                              AV43DynamicFiltersEnabled3 ,
                                              AV44DynamicFiltersSelector3 ,
                                              AV45Perfil_Nome3 ,
                                              AV46Usuario_PessoaNom3 ,
                                              AV11TFUsuario_Nome_Sel ,
                                              AV10TFUsuario_Nome ,
                                              AV13TFUsuario_PessoaNom_Sel ,
                                              AV12TFUsuario_PessoaNom ,
                                              AV15TFPerfil_Nome_Sel ,
                                              AV14TFPerfil_Nome ,
                                              AV17TFPerfil_Tipo_Sels.Count ,
                                              A4Perfil_Nome ,
                                              A58Usuario_PessoaNom ,
                                              A2Usuario_Nome },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV37Perfil_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV37Perfil_Nome1), 50, "%");
         lV38Usuario_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV38Usuario_PessoaNom1), 100, "%");
         lV41Perfil_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV41Perfil_Nome2), 50, "%");
         lV42Usuario_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV42Usuario_PessoaNom2), 100, "%");
         lV45Perfil_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV45Perfil_Nome3), 50, "%");
         lV46Usuario_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV46Usuario_PessoaNom3), 100, "%");
         lV10TFUsuario_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUsuario_Nome), 50, "%");
         lV12TFUsuario_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV12TFUsuario_PessoaNom), 100, "%");
         lV14TFPerfil_Nome = StringUtil.PadR( StringUtil.RTrim( AV14TFPerfil_Nome), 50, "%");
         /* Using cursor P00LG2 */
         pr_default.execute(0, new Object[] {lV37Perfil_Nome1, lV38Usuario_PessoaNom1, lV41Perfil_Nome2, lV42Usuario_PessoaNom2, lV45Perfil_Nome3, lV46Usuario_PessoaNom3, lV10TFUsuario_Nome, AV11TFUsuario_Nome_Sel, lV12TFUsuario_PessoaNom, AV13TFUsuario_PessoaNom_Sel, lV14TFPerfil_Nome, AV15TFPerfil_Nome_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKLG2 = false;
            A1Usuario_Codigo = P00LG2_A1Usuario_Codigo[0];
            A57Usuario_PessoaCod = P00LG2_A57Usuario_PessoaCod[0];
            A3Perfil_Codigo = P00LG2_A3Perfil_Codigo[0];
            A2Usuario_Nome = P00LG2_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LG2_n2Usuario_Nome[0];
            A275Perfil_Tipo = P00LG2_A275Perfil_Tipo[0];
            A58Usuario_PessoaNom = P00LG2_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00LG2_n58Usuario_PessoaNom[0];
            A4Perfil_Nome = P00LG2_A4Perfil_Nome[0];
            A57Usuario_PessoaCod = P00LG2_A57Usuario_PessoaCod[0];
            A2Usuario_Nome = P00LG2_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LG2_n2Usuario_Nome[0];
            A58Usuario_PessoaNom = P00LG2_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00LG2_n58Usuario_PessoaNom[0];
            A275Perfil_Tipo = P00LG2_A275Perfil_Tipo[0];
            A4Perfil_Nome = P00LG2_A4Perfil_Nome[0];
            AV30count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00LG2_A2Usuario_Nome[0], A2Usuario_Nome) == 0 ) )
            {
               BRKLG2 = false;
               A1Usuario_Codigo = P00LG2_A1Usuario_Codigo[0];
               A3Perfil_Codigo = P00LG2_A3Perfil_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKLG2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2Usuario_Nome)) )
            {
               AV22Option = A2Usuario_Nome;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLG2 )
            {
               BRKLG2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADUSUARIO_PESSOANOMOPTIONS' Routine */
         AV12TFUsuario_PessoaNom = AV18SearchTxt;
         AV13TFUsuario_PessoaNom_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A275Perfil_Tipo ,
                                              AV17TFPerfil_Tipo_Sels ,
                                              AV36DynamicFiltersSelector1 ,
                                              AV37Perfil_Nome1 ,
                                              AV38Usuario_PessoaNom1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV41Perfil_Nome2 ,
                                              AV42Usuario_PessoaNom2 ,
                                              AV43DynamicFiltersEnabled3 ,
                                              AV44DynamicFiltersSelector3 ,
                                              AV45Perfil_Nome3 ,
                                              AV46Usuario_PessoaNom3 ,
                                              AV11TFUsuario_Nome_Sel ,
                                              AV10TFUsuario_Nome ,
                                              AV13TFUsuario_PessoaNom_Sel ,
                                              AV12TFUsuario_PessoaNom ,
                                              AV15TFPerfil_Nome_Sel ,
                                              AV14TFPerfil_Nome ,
                                              AV17TFPerfil_Tipo_Sels.Count ,
                                              A4Perfil_Nome ,
                                              A58Usuario_PessoaNom ,
                                              A2Usuario_Nome },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV37Perfil_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV37Perfil_Nome1), 50, "%");
         lV38Usuario_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV38Usuario_PessoaNom1), 100, "%");
         lV41Perfil_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV41Perfil_Nome2), 50, "%");
         lV42Usuario_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV42Usuario_PessoaNom2), 100, "%");
         lV45Perfil_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV45Perfil_Nome3), 50, "%");
         lV46Usuario_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV46Usuario_PessoaNom3), 100, "%");
         lV10TFUsuario_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUsuario_Nome), 50, "%");
         lV12TFUsuario_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV12TFUsuario_PessoaNom), 100, "%");
         lV14TFPerfil_Nome = StringUtil.PadR( StringUtil.RTrim( AV14TFPerfil_Nome), 50, "%");
         /* Using cursor P00LG3 */
         pr_default.execute(1, new Object[] {lV37Perfil_Nome1, lV38Usuario_PessoaNom1, lV41Perfil_Nome2, lV42Usuario_PessoaNom2, lV45Perfil_Nome3, lV46Usuario_PessoaNom3, lV10TFUsuario_Nome, AV11TFUsuario_Nome_Sel, lV12TFUsuario_PessoaNom, AV13TFUsuario_PessoaNom_Sel, lV14TFPerfil_Nome, AV15TFPerfil_Nome_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKLG4 = false;
            A1Usuario_Codigo = P00LG3_A1Usuario_Codigo[0];
            A57Usuario_PessoaCod = P00LG3_A57Usuario_PessoaCod[0];
            A3Perfil_Codigo = P00LG3_A3Perfil_Codigo[0];
            A58Usuario_PessoaNom = P00LG3_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00LG3_n58Usuario_PessoaNom[0];
            A275Perfil_Tipo = P00LG3_A275Perfil_Tipo[0];
            A2Usuario_Nome = P00LG3_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LG3_n2Usuario_Nome[0];
            A4Perfil_Nome = P00LG3_A4Perfil_Nome[0];
            A57Usuario_PessoaCod = P00LG3_A57Usuario_PessoaCod[0];
            A2Usuario_Nome = P00LG3_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LG3_n2Usuario_Nome[0];
            A58Usuario_PessoaNom = P00LG3_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00LG3_n58Usuario_PessoaNom[0];
            A275Perfil_Tipo = P00LG3_A275Perfil_Tipo[0];
            A4Perfil_Nome = P00LG3_A4Perfil_Nome[0];
            AV30count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00LG3_A58Usuario_PessoaNom[0], A58Usuario_PessoaNom) == 0 ) )
            {
               BRKLG4 = false;
               A1Usuario_Codigo = P00LG3_A1Usuario_Codigo[0];
               A57Usuario_PessoaCod = P00LG3_A57Usuario_PessoaCod[0];
               A3Perfil_Codigo = P00LG3_A3Perfil_Codigo[0];
               A57Usuario_PessoaCod = P00LG3_A57Usuario_PessoaCod[0];
               AV30count = (long)(AV30count+1);
               BRKLG4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A58Usuario_PessoaNom)) )
            {
               AV22Option = A58Usuario_PessoaNom;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLG4 )
            {
               BRKLG4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADPERFIL_NOMEOPTIONS' Routine */
         AV14TFPerfil_Nome = AV18SearchTxt;
         AV15TFPerfil_Nome_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A275Perfil_Tipo ,
                                              AV17TFPerfil_Tipo_Sels ,
                                              AV36DynamicFiltersSelector1 ,
                                              AV37Perfil_Nome1 ,
                                              AV38Usuario_PessoaNom1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV41Perfil_Nome2 ,
                                              AV42Usuario_PessoaNom2 ,
                                              AV43DynamicFiltersEnabled3 ,
                                              AV44DynamicFiltersSelector3 ,
                                              AV45Perfil_Nome3 ,
                                              AV46Usuario_PessoaNom3 ,
                                              AV11TFUsuario_Nome_Sel ,
                                              AV10TFUsuario_Nome ,
                                              AV13TFUsuario_PessoaNom_Sel ,
                                              AV12TFUsuario_PessoaNom ,
                                              AV15TFPerfil_Nome_Sel ,
                                              AV14TFPerfil_Nome ,
                                              AV17TFPerfil_Tipo_Sels.Count ,
                                              A4Perfil_Nome ,
                                              A58Usuario_PessoaNom ,
                                              A2Usuario_Nome },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV37Perfil_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV37Perfil_Nome1), 50, "%");
         lV38Usuario_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV38Usuario_PessoaNom1), 100, "%");
         lV41Perfil_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV41Perfil_Nome2), 50, "%");
         lV42Usuario_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV42Usuario_PessoaNom2), 100, "%");
         lV45Perfil_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV45Perfil_Nome3), 50, "%");
         lV46Usuario_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV46Usuario_PessoaNom3), 100, "%");
         lV10TFUsuario_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUsuario_Nome), 50, "%");
         lV12TFUsuario_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV12TFUsuario_PessoaNom), 100, "%");
         lV14TFPerfil_Nome = StringUtil.PadR( StringUtil.RTrim( AV14TFPerfil_Nome), 50, "%");
         /* Using cursor P00LG4 */
         pr_default.execute(2, new Object[] {lV37Perfil_Nome1, lV38Usuario_PessoaNom1, lV41Perfil_Nome2, lV42Usuario_PessoaNom2, lV45Perfil_Nome3, lV46Usuario_PessoaNom3, lV10TFUsuario_Nome, AV11TFUsuario_Nome_Sel, lV12TFUsuario_PessoaNom, AV13TFUsuario_PessoaNom_Sel, lV14TFPerfil_Nome, AV15TFPerfil_Nome_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKLG6 = false;
            A1Usuario_Codigo = P00LG4_A1Usuario_Codigo[0];
            A57Usuario_PessoaCod = P00LG4_A57Usuario_PessoaCod[0];
            A3Perfil_Codigo = P00LG4_A3Perfil_Codigo[0];
            A275Perfil_Tipo = P00LG4_A275Perfil_Tipo[0];
            A2Usuario_Nome = P00LG4_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LG4_n2Usuario_Nome[0];
            A58Usuario_PessoaNom = P00LG4_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00LG4_n58Usuario_PessoaNom[0];
            A4Perfil_Nome = P00LG4_A4Perfil_Nome[0];
            A57Usuario_PessoaCod = P00LG4_A57Usuario_PessoaCod[0];
            A2Usuario_Nome = P00LG4_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LG4_n2Usuario_Nome[0];
            A58Usuario_PessoaNom = P00LG4_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00LG4_n58Usuario_PessoaNom[0];
            A275Perfil_Tipo = P00LG4_A275Perfil_Tipo[0];
            A4Perfil_Nome = P00LG4_A4Perfil_Nome[0];
            AV30count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00LG4_A3Perfil_Codigo[0] == A3Perfil_Codigo ) )
            {
               BRKLG6 = false;
               A1Usuario_Codigo = P00LG4_A1Usuario_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKLG6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A4Perfil_Nome)) )
            {
               AV22Option = A4Perfil_Nome;
               AV21InsertIndex = 1;
               while ( ( AV21InsertIndex <= AV23Options.Count ) && ( StringUtil.StrCmp(((String)AV23Options.Item(AV21InsertIndex)), AV22Option) < 0 ) )
               {
                  AV21InsertIndex = (int)(AV21InsertIndex+1);
               }
               AV23Options.Add(AV22Option, AV21InsertIndex);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), AV21InsertIndex);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLG6 )
            {
               BRKLG6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFUsuario_Nome = "";
         AV11TFUsuario_Nome_Sel = "";
         AV12TFUsuario_PessoaNom = "";
         AV13TFUsuario_PessoaNom_Sel = "";
         AV14TFPerfil_Nome = "";
         AV15TFPerfil_Nome_Sel = "";
         AV16TFPerfil_Tipo_SelsJson = "";
         AV17TFPerfil_Tipo_Sels = new GxSimpleCollection();
         AV35GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV36DynamicFiltersSelector1 = "";
         AV37Perfil_Nome1 = "";
         AV38Usuario_PessoaNom1 = "";
         AV40DynamicFiltersSelector2 = "";
         AV41Perfil_Nome2 = "";
         AV42Usuario_PessoaNom2 = "";
         AV44DynamicFiltersSelector3 = "";
         AV45Perfil_Nome3 = "";
         AV46Usuario_PessoaNom3 = "";
         scmdbuf = "";
         lV37Perfil_Nome1 = "";
         lV38Usuario_PessoaNom1 = "";
         lV41Perfil_Nome2 = "";
         lV42Usuario_PessoaNom2 = "";
         lV45Perfil_Nome3 = "";
         lV46Usuario_PessoaNom3 = "";
         lV10TFUsuario_Nome = "";
         lV12TFUsuario_PessoaNom = "";
         lV14TFPerfil_Nome = "";
         A4Perfil_Nome = "";
         A58Usuario_PessoaNom = "";
         A2Usuario_Nome = "";
         P00LG2_A1Usuario_Codigo = new int[1] ;
         P00LG2_A57Usuario_PessoaCod = new int[1] ;
         P00LG2_A3Perfil_Codigo = new int[1] ;
         P00LG2_A2Usuario_Nome = new String[] {""} ;
         P00LG2_n2Usuario_Nome = new bool[] {false} ;
         P00LG2_A275Perfil_Tipo = new short[1] ;
         P00LG2_A58Usuario_PessoaNom = new String[] {""} ;
         P00LG2_n58Usuario_PessoaNom = new bool[] {false} ;
         P00LG2_A4Perfil_Nome = new String[] {""} ;
         AV22Option = "";
         P00LG3_A1Usuario_Codigo = new int[1] ;
         P00LG3_A57Usuario_PessoaCod = new int[1] ;
         P00LG3_A3Perfil_Codigo = new int[1] ;
         P00LG3_A58Usuario_PessoaNom = new String[] {""} ;
         P00LG3_n58Usuario_PessoaNom = new bool[] {false} ;
         P00LG3_A275Perfil_Tipo = new short[1] ;
         P00LG3_A2Usuario_Nome = new String[] {""} ;
         P00LG3_n2Usuario_Nome = new bool[] {false} ;
         P00LG3_A4Perfil_Nome = new String[] {""} ;
         P00LG4_A1Usuario_Codigo = new int[1] ;
         P00LG4_A57Usuario_PessoaCod = new int[1] ;
         P00LG4_A3Perfil_Codigo = new int[1] ;
         P00LG4_A275Perfil_Tipo = new short[1] ;
         P00LG4_A2Usuario_Nome = new String[] {""} ;
         P00LG4_n2Usuario_Nome = new bool[] {false} ;
         P00LG4_A58Usuario_PessoaNom = new String[] {""} ;
         P00LG4_n58Usuario_PessoaNom = new bool[] {false} ;
         P00LG4_A4Perfil_Nome = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptusuarioperfilfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00LG2_A1Usuario_Codigo, P00LG2_A57Usuario_PessoaCod, P00LG2_A3Perfil_Codigo, P00LG2_A2Usuario_Nome, P00LG2_n2Usuario_Nome, P00LG2_A275Perfil_Tipo, P00LG2_A58Usuario_PessoaNom, P00LG2_n58Usuario_PessoaNom, P00LG2_A4Perfil_Nome
               }
               , new Object[] {
               P00LG3_A1Usuario_Codigo, P00LG3_A57Usuario_PessoaCod, P00LG3_A3Perfil_Codigo, P00LG3_A58Usuario_PessoaNom, P00LG3_n58Usuario_PessoaNom, P00LG3_A275Perfil_Tipo, P00LG3_A2Usuario_Nome, P00LG3_n2Usuario_Nome, P00LG3_A4Perfil_Nome
               }
               , new Object[] {
               P00LG4_A1Usuario_Codigo, P00LG4_A57Usuario_PessoaCod, P00LG4_A3Perfil_Codigo, P00LG4_A275Perfil_Tipo, P00LG4_A2Usuario_Nome, P00LG4_n2Usuario_Nome, P00LG4_A58Usuario_PessoaNom, P00LG4_n58Usuario_PessoaNom, P00LG4_A4Perfil_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A275Perfil_Tipo ;
      private int AV49GXV1 ;
      private int AV17TFPerfil_Tipo_Sels_Count ;
      private int A1Usuario_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int A3Perfil_Codigo ;
      private int AV21InsertIndex ;
      private long AV30count ;
      private String AV10TFUsuario_Nome ;
      private String AV11TFUsuario_Nome_Sel ;
      private String AV12TFUsuario_PessoaNom ;
      private String AV13TFUsuario_PessoaNom_Sel ;
      private String AV14TFPerfil_Nome ;
      private String AV15TFPerfil_Nome_Sel ;
      private String AV37Perfil_Nome1 ;
      private String AV38Usuario_PessoaNom1 ;
      private String AV41Perfil_Nome2 ;
      private String AV42Usuario_PessoaNom2 ;
      private String AV45Perfil_Nome3 ;
      private String AV46Usuario_PessoaNom3 ;
      private String scmdbuf ;
      private String lV37Perfil_Nome1 ;
      private String lV38Usuario_PessoaNom1 ;
      private String lV41Perfil_Nome2 ;
      private String lV42Usuario_PessoaNom2 ;
      private String lV45Perfil_Nome3 ;
      private String lV46Usuario_PessoaNom3 ;
      private String lV10TFUsuario_Nome ;
      private String lV12TFUsuario_PessoaNom ;
      private String lV14TFPerfil_Nome ;
      private String A4Perfil_Nome ;
      private String A58Usuario_PessoaNom ;
      private String A2Usuario_Nome ;
      private bool returnInSub ;
      private bool AV39DynamicFiltersEnabled2 ;
      private bool AV43DynamicFiltersEnabled3 ;
      private bool BRKLG2 ;
      private bool n2Usuario_Nome ;
      private bool n58Usuario_PessoaNom ;
      private bool BRKLG4 ;
      private bool BRKLG6 ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String AV16TFPerfil_Tipo_SelsJson ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV36DynamicFiltersSelector1 ;
      private String AV40DynamicFiltersSelector2 ;
      private String AV44DynamicFiltersSelector3 ;
      private String AV22Option ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00LG2_A1Usuario_Codigo ;
      private int[] P00LG2_A57Usuario_PessoaCod ;
      private int[] P00LG2_A3Perfil_Codigo ;
      private String[] P00LG2_A2Usuario_Nome ;
      private bool[] P00LG2_n2Usuario_Nome ;
      private short[] P00LG2_A275Perfil_Tipo ;
      private String[] P00LG2_A58Usuario_PessoaNom ;
      private bool[] P00LG2_n58Usuario_PessoaNom ;
      private String[] P00LG2_A4Perfil_Nome ;
      private int[] P00LG3_A1Usuario_Codigo ;
      private int[] P00LG3_A57Usuario_PessoaCod ;
      private int[] P00LG3_A3Perfil_Codigo ;
      private String[] P00LG3_A58Usuario_PessoaNom ;
      private bool[] P00LG3_n58Usuario_PessoaNom ;
      private short[] P00LG3_A275Perfil_Tipo ;
      private String[] P00LG3_A2Usuario_Nome ;
      private bool[] P00LG3_n2Usuario_Nome ;
      private String[] P00LG3_A4Perfil_Nome ;
      private int[] P00LG4_A1Usuario_Codigo ;
      private int[] P00LG4_A57Usuario_PessoaCod ;
      private int[] P00LG4_A3Perfil_Codigo ;
      private short[] P00LG4_A275Perfil_Tipo ;
      private String[] P00LG4_A2Usuario_Nome ;
      private bool[] P00LG4_n2Usuario_Nome ;
      private String[] P00LG4_A58Usuario_PessoaNom ;
      private bool[] P00LG4_n58Usuario_PessoaNom ;
      private String[] P00LG4_A4Perfil_Nome ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV17TFPerfil_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV35GridStateDynamicFilter ;
   }

   public class getpromptusuarioperfilfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00LG2( IGxContext context ,
                                             short A275Perfil_Tipo ,
                                             IGxCollection AV17TFPerfil_Tipo_Sels ,
                                             String AV36DynamicFiltersSelector1 ,
                                             String AV37Perfil_Nome1 ,
                                             String AV38Usuario_PessoaNom1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             String AV41Perfil_Nome2 ,
                                             String AV42Usuario_PessoaNom2 ,
                                             bool AV43DynamicFiltersEnabled3 ,
                                             String AV44DynamicFiltersSelector3 ,
                                             String AV45Perfil_Nome3 ,
                                             String AV46Usuario_PessoaNom3 ,
                                             String AV11TFUsuario_Nome_Sel ,
                                             String AV10TFUsuario_Nome ,
                                             String AV13TFUsuario_PessoaNom_Sel ,
                                             String AV12TFUsuario_PessoaNom ,
                                             String AV15TFPerfil_Nome_Sel ,
                                             String AV14TFPerfil_Nome ,
                                             int AV17TFPerfil_Tipo_Sels_Count ,
                                             String A4Perfil_Nome ,
                                             String A58Usuario_PessoaNom ,
                                             String A2Usuario_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [12] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Usuario_Codigo], T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Perfil_Codigo], T2.[Usuario_Nome], T4.[Perfil_Tipo], T3.[Pessoa_Nome] AS Usuario_PessoaNom, T4.[Perfil_Nome] FROM ((([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Usuario_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Perfil] T4 WITH (NOLOCK) ON T4.[Perfil_Codigo] = T1.[Perfil_Codigo])";
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Perfil_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV37Perfil_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV37Perfil_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Usuario_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV38Usuario_PessoaNom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV38Usuario_PessoaNom1 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Perfil_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV41Perfil_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV41Perfil_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Usuario_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV42Usuario_PessoaNom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV42Usuario_PessoaNom2 + '%')";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Perfil_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV45Perfil_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV45Perfil_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Usuario_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV46Usuario_PessoaNom3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV46Usuario_PessoaNom3 + '%')";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUsuario_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV10TFUsuario_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like @lV10TFUsuario_Nome)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] = @AV11TFUsuario_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] = @AV11TFUsuario_Nome_Sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFUsuario_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFUsuario_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV12TFUsuario_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV12TFUsuario_PessoaNom)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFUsuario_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV13TFUsuario_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV13TFUsuario_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFPerfil_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFPerfil_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like @lV14TFPerfil_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like @lV14TFPerfil_Nome)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFPerfil_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] = @AV15TFPerfil_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] = @AV15TFPerfil_Nome_Sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV17TFPerfil_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFPerfil_Tipo_Sels, "T4.[Perfil_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFPerfil_Tipo_Sels, "T4.[Perfil_Tipo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Usuario_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00LG3( IGxContext context ,
                                             short A275Perfil_Tipo ,
                                             IGxCollection AV17TFPerfil_Tipo_Sels ,
                                             String AV36DynamicFiltersSelector1 ,
                                             String AV37Perfil_Nome1 ,
                                             String AV38Usuario_PessoaNom1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             String AV41Perfil_Nome2 ,
                                             String AV42Usuario_PessoaNom2 ,
                                             bool AV43DynamicFiltersEnabled3 ,
                                             String AV44DynamicFiltersSelector3 ,
                                             String AV45Perfil_Nome3 ,
                                             String AV46Usuario_PessoaNom3 ,
                                             String AV11TFUsuario_Nome_Sel ,
                                             String AV10TFUsuario_Nome ,
                                             String AV13TFUsuario_PessoaNom_Sel ,
                                             String AV12TFUsuario_PessoaNom ,
                                             String AV15TFPerfil_Nome_Sel ,
                                             String AV14TFPerfil_Nome ,
                                             int AV17TFPerfil_Tipo_Sels_Count ,
                                             String A4Perfil_Nome ,
                                             String A58Usuario_PessoaNom ,
                                             String A2Usuario_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [12] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Usuario_Codigo], T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Perfil_Codigo], T3.[Pessoa_Nome] AS Usuario_PessoaNom, T4.[Perfil_Tipo], T2.[Usuario_Nome], T4.[Perfil_Nome] FROM ((([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Usuario_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Perfil] T4 WITH (NOLOCK) ON T4.[Perfil_Codigo] = T1.[Perfil_Codigo])";
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Perfil_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV37Perfil_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV37Perfil_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Usuario_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV38Usuario_PessoaNom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV38Usuario_PessoaNom1 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Perfil_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV41Perfil_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV41Perfil_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Usuario_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV42Usuario_PessoaNom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV42Usuario_PessoaNom2 + '%')";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Perfil_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV45Perfil_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV45Perfil_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Usuario_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV46Usuario_PessoaNom3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV46Usuario_PessoaNom3 + '%')";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUsuario_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV10TFUsuario_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like @lV10TFUsuario_Nome)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] = @AV11TFUsuario_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] = @AV11TFUsuario_Nome_Sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFUsuario_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFUsuario_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV12TFUsuario_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV12TFUsuario_PessoaNom)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFUsuario_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV13TFUsuario_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV13TFUsuario_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFPerfil_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFPerfil_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like @lV14TFPerfil_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like @lV14TFPerfil_Nome)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFPerfil_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] = @AV15TFPerfil_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] = @AV15TFPerfil_Nome_Sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV17TFPerfil_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFPerfil_Tipo_Sels, "T4.[Perfil_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFPerfil_Tipo_Sels, "T4.[Perfil_Tipo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00LG4( IGxContext context ,
                                             short A275Perfil_Tipo ,
                                             IGxCollection AV17TFPerfil_Tipo_Sels ,
                                             String AV36DynamicFiltersSelector1 ,
                                             String AV37Perfil_Nome1 ,
                                             String AV38Usuario_PessoaNom1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             String AV41Perfil_Nome2 ,
                                             String AV42Usuario_PessoaNom2 ,
                                             bool AV43DynamicFiltersEnabled3 ,
                                             String AV44DynamicFiltersSelector3 ,
                                             String AV45Perfil_Nome3 ,
                                             String AV46Usuario_PessoaNom3 ,
                                             String AV11TFUsuario_Nome_Sel ,
                                             String AV10TFUsuario_Nome ,
                                             String AV13TFUsuario_PessoaNom_Sel ,
                                             String AV12TFUsuario_PessoaNom ,
                                             String AV15TFPerfil_Nome_Sel ,
                                             String AV14TFPerfil_Nome ,
                                             int AV17TFPerfil_Tipo_Sels_Count ,
                                             String A4Perfil_Nome ,
                                             String A58Usuario_PessoaNom ,
                                             String A2Usuario_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [12] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Usuario_Codigo], T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Perfil_Codigo], T4.[Perfil_Tipo], T2.[Usuario_Nome], T3.[Pessoa_Nome] AS Usuario_PessoaNom, T4.[Perfil_Nome] FROM ((([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Usuario_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Perfil] T4 WITH (NOLOCK) ON T4.[Perfil_Codigo] = T1.[Perfil_Codigo])";
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Perfil_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV37Perfil_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV37Perfil_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Usuario_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV38Usuario_PessoaNom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV38Usuario_PessoaNom1 + '%')";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Perfil_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV41Perfil_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV41Perfil_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Usuario_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV42Usuario_PessoaNom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV42Usuario_PessoaNom2 + '%')";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Perfil_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV45Perfil_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV45Perfil_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Usuario_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV46Usuario_PessoaNom3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV46Usuario_PessoaNom3 + '%')";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUsuario_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV10TFUsuario_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like @lV10TFUsuario_Nome)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] = @AV11TFUsuario_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] = @AV11TFUsuario_Nome_Sel)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFUsuario_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFUsuario_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV12TFUsuario_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV12TFUsuario_PessoaNom)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFUsuario_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV13TFUsuario_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV13TFUsuario_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFPerfil_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFPerfil_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like @lV14TFPerfil_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like @lV14TFPerfil_Nome)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFPerfil_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] = @AV15TFPerfil_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] = @AV15TFPerfil_Nome_Sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV17TFPerfil_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFPerfil_Tipo_Sels, "T4.[Perfil_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFPerfil_Tipo_Sels, "T4.[Perfil_Tipo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Perfil_Codigo]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00LG2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] );
               case 1 :
                     return conditional_P00LG3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] );
               case 2 :
                     return conditional_P00LG4(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00LG2 ;
          prmP00LG2 = new Object[] {
          new Object[] {"@lV37Perfil_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV38Usuario_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV41Perfil_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Usuario_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV45Perfil_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46Usuario_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV10TFUsuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUsuario_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFUsuario_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFUsuario_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV14TFPerfil_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFPerfil_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00LG3 ;
          prmP00LG3 = new Object[] {
          new Object[] {"@lV37Perfil_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV38Usuario_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV41Perfil_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Usuario_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV45Perfil_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46Usuario_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV10TFUsuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUsuario_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFUsuario_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFUsuario_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV14TFPerfil_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFPerfil_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00LG4 ;
          prmP00LG4 = new Object[] {
          new Object[] {"@lV37Perfil_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV38Usuario_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV41Perfil_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Usuario_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV45Perfil_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46Usuario_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV10TFUsuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUsuario_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFUsuario_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFUsuario_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV14TFPerfil_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFPerfil_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00LG2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LG2,100,0,true,false )
             ,new CursorDef("P00LG3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LG3,100,0,true,false )
             ,new CursorDef("P00LG4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LG4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 50) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 50) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptusuarioperfilfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptusuarioperfilfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptusuarioperfilfilterdata") )
          {
             return  ;
          }
          getpromptusuarioperfilfilterdata worker = new getpromptusuarioperfilfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
