/*
               File: PRC_NaDivergenciaFicaCom
        Description: Na Divergencia Fica Com
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:38.48
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_nadivergenciaficacom : GXProcedure
   {
      public prc_nadivergenciaficacom( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_nadivergenciaficacom( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           int aP1_ContagemResultado_OSVinculada ,
                           String aP2_ContagemResultado_Demanda ,
                           int aP3_ContratadaOrigem_Codigo ,
                           int aP4_Servico_Codigo ,
                           int aP5_AreaTrabalho_Codigo ,
                           int aP6_UserId ,
                           String aP7_Parecer )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV9ContagemResultado_OSVinculada = aP1_ContagemResultado_OSVinculada;
         this.AV12ContagemResultado_Demanda = aP2_ContagemResultado_Demanda;
         this.AV13ContratadaOrigem_Codigo = aP3_ContratadaOrigem_Codigo;
         this.AV14Servico_Codigo = aP4_Servico_Codigo;
         this.AV17AreaTrabalho_Codigo = aP5_AreaTrabalho_Codigo;
         this.AV19UserId = aP6_UserId;
         this.AV28Parecer = aP7_Parecer;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 int aP1_ContagemResultado_OSVinculada ,
                                 String aP2_ContagemResultado_Demanda ,
                                 int aP3_ContratadaOrigem_Codigo ,
                                 int aP4_Servico_Codigo ,
                                 int aP5_AreaTrabalho_Codigo ,
                                 int aP6_UserId ,
                                 String aP7_Parecer )
      {
         prc_nadivergenciaficacom objprc_nadivergenciaficacom;
         objprc_nadivergenciaficacom = new prc_nadivergenciaficacom();
         objprc_nadivergenciaficacom.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_nadivergenciaficacom.AV9ContagemResultado_OSVinculada = aP1_ContagemResultado_OSVinculada;
         objprc_nadivergenciaficacom.AV12ContagemResultado_Demanda = aP2_ContagemResultado_Demanda;
         objprc_nadivergenciaficacom.AV13ContratadaOrigem_Codigo = aP3_ContratadaOrigem_Codigo;
         objprc_nadivergenciaficacom.AV14Servico_Codigo = aP4_Servico_Codigo;
         objprc_nadivergenciaficacom.AV17AreaTrabalho_Codigo = aP5_AreaTrabalho_Codigo;
         objprc_nadivergenciaficacom.AV19UserId = aP6_UserId;
         objprc_nadivergenciaficacom.AV28Parecer = aP7_Parecer;
         objprc_nadivergenciaficacom.context.SetSubmitInitialConfig(context);
         objprc_nadivergenciaficacom.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_nadivergenciaficacom);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_nadivergenciaficacom)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV20Prazo = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
         /* Using cursor P008M2 */
         pr_default.execute(0, new Object[] {AV8ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A456ContagemResultado_Codigo = P008M2_A456ContagemResultado_Codigo[0];
            A1593ContagemResultado_CntSrvTpVnc = P008M2_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P008M2_n1593ContagemResultado_CntSrvTpVnc[0];
            A490ContagemResultado_ContratadaCod = P008M2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P008M2_n490ContagemResultado_ContratadaCod[0];
            A1603ContagemResultado_CntCod = P008M2_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P008M2_n1603ContagemResultado_CntCod[0];
            A912ContagemResultado_HoraEntrega = P008M2_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P008M2_n912ContagemResultado_HoraEntrega[0];
            A1618ContagemResultado_PrzRsp = P008M2_A1618ContagemResultado_PrzRsp[0];
            n1618ContagemResultado_PrzRsp = P008M2_n1618ContagemResultado_PrzRsp[0];
            A1611ContagemResultado_PrzTpDias = P008M2_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P008M2_n1611ContagemResultado_PrzTpDias[0];
            A1553ContagemResultado_CntSrvCod = P008M2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P008M2_n1553ContagemResultado_CntSrvCod[0];
            A1593ContagemResultado_CntSrvTpVnc = P008M2_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P008M2_n1593ContagemResultado_CntSrvTpVnc[0];
            A1603ContagemResultado_CntCod = P008M2_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P008M2_n1603ContagemResultado_CntCod[0];
            A1618ContagemResultado_PrzRsp = P008M2_A1618ContagemResultado_PrzRsp[0];
            n1618ContagemResultado_PrzRsp = P008M2_n1618ContagemResultado_PrzRsp[0];
            A1611ContagemResultado_PrzTpDias = P008M2_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P008M2_n1611ContagemResultado_PrzTpDias[0];
            AV30ContagemResultado_CntSrvTpVnc = A1593ContagemResultado_CntSrvTpVnc;
            AV33Prestadora = A490ContagemResultado_ContratadaCod;
            AV35ContagemResultado_CntCod = A1603ContagemResultado_CntCod;
            AV20Prazo = DateTimeUtil.TAdd( AV20Prazo, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
            AV20Prazo = DateTimeUtil.TAdd( AV20Prazo, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
            AV25Dias = A1618ContagemResultado_PrzRsp;
            AV26TipoDias = A1611ContagemResultado_PrzTpDias;
            AV29ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( ( AV9ContagemResultado_OSVinculada > 0 ) && ( ( StringUtil.StrCmp(AV30ContagemResultado_CntSrvTpVnc, "C") == 0 ) || ( StringUtil.StrCmp(AV30ContagemResultado_CntSrvTpVnc, "A") == 0 ) ) )
         {
            /* Using cursor P008M3 */
            pr_default.execute(1, new Object[] {AV9ContagemResultado_OSVinculada, AV33Prestadora});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A456ContagemResultado_Codigo = P008M3_A456ContagemResultado_Codigo[0];
               A1636ContagemResultado_ServicoSS = P008M3_A1636ContagemResultado_ServicoSS[0];
               n1636ContagemResultado_ServicoSS = P008M3_n1636ContagemResultado_ServicoSS[0];
               A484ContagemResultado_StatusDmn = P008M3_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P008M3_n484ContagemResultado_StatusDmn[0];
               A490ContagemResultado_ContratadaCod = P008M3_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P008M3_n490ContagemResultado_ContratadaCod[0];
               A912ContagemResultado_HoraEntrega = P008M3_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P008M3_n912ContagemResultado_HoraEntrega[0];
               A601ContagemResultado_Servico = P008M3_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P008M3_n601ContagemResultado_Servico[0];
               A1611ContagemResultado_PrzTpDias = P008M3_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P008M3_n1611ContagemResultado_PrzTpDias[0];
               A1553ContagemResultado_CntSrvCod = P008M3_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P008M3_n1553ContagemResultado_CntSrvCod[0];
               A890ContagemResultado_Responsavel = P008M3_A890ContagemResultado_Responsavel[0];
               n890ContagemResultado_Responsavel = P008M3_n890ContagemResultado_Responsavel[0];
               A472ContagemResultado_DataEntrega = P008M3_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P008M3_n472ContagemResultado_DataEntrega[0];
               A1351ContagemResultado_DataPrevista = P008M3_A1351ContagemResultado_DataPrevista[0];
               n1351ContagemResultado_DataPrevista = P008M3_n1351ContagemResultado_DataPrevista[0];
               A1511ContagemResultado_InicioCrr = P008M3_A1511ContagemResultado_InicioCrr[0];
               n1511ContagemResultado_InicioCrr = P008M3_n1511ContagemResultado_InicioCrr[0];
               A601ContagemResultado_Servico = P008M3_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P008M3_n601ContagemResultado_Servico[0];
               A1611ContagemResultado_PrzTpDias = P008M3_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P008M3_n1611ContagemResultado_PrzTpDias[0];
               AV20Prazo = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
               AV20Prazo = DateTimeUtil.TAdd( AV20Prazo, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
               AV20Prazo = DateTimeUtil.TAdd( AV20Prazo, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
               AV22Contratada = A490ContagemResultado_ContratadaCod;
               AV23Servico = A601ContagemResultado_Servico;
               AV24Codigo = A456ContagemResultado_Codigo;
               AV26TipoDias = A1611ContagemResultado_PrzTpDias;
               AV29ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
               /* Execute user subroutine: 'ADDPRAZOCORRECAO' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(1);
                  this.cleanup();
                  if (true) return;
               }
               AV31OSVinculada = AV24Codigo;
               /* Execute user subroutine: 'RETORNAPARACONTRATADA' */
               S131 ();
               if ( returnInSub )
               {
                  pr_default.close(1);
                  this.cleanup();
                  if (true) return;
               }
               new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  AV16Responsavel,  "D",  "D",  AV19UserId,  0,  A484ContagemResultado_StatusDmn,  "A",  AV28Parecer,  AV20Prazo,  true) ;
               if ( AV16Responsavel > 0 )
               {
                  A890ContagemResultado_Responsavel = AV16Responsavel;
                  n890ContagemResultado_Responsavel = false;
               }
               A484ContagemResultado_StatusDmn = "A";
               n484ContagemResultado_StatusDmn = false;
               A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV20Prazo);
               n472ContagemResultado_DataEntrega = false;
               A1351ContagemResultado_DataPrevista = AV20Prazo;
               n1351ContagemResultado_DataPrevista = false;
               A1511ContagemResultado_InicioCrr = DateTimeUtil.ServerNow( context, "DEFAULT");
               n1511ContagemResultado_InicioCrr = false;
               /* Using cursor P008M4 */
               pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A483ContagemResultado_StatusCnt = P008M4_A483ContagemResultado_StatusCnt[0];
                  A511ContagemResultado_HoraCnt = P008M4_A511ContagemResultado_HoraCnt[0];
                  A473ContagemResultado_DataCnt = P008M4_A473ContagemResultado_DataCnt[0];
                  A483ContagemResultado_StatusCnt = 7;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  /* Using cursor P008M5 */
                  pr_default.execute(3, new Object[] {A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                  pr_default.close(3);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                  if (true) break;
                  /* Using cursor P008M6 */
                  pr_default.execute(4, new Object[] {A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                  pr_default.close(4);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                  pr_default.readNext(2);
               }
               pr_default.close(2);
               new prc_disparoservicovinculado(context ).execute(  A456ContagemResultado_Codigo,  AV19UserId) ;
               AV34TemVinculada = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P008M7 */
               pr_default.execute(5, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1511ContagemResultado_InicioCrr, A1511ContagemResultado_InicioCrr, A456ContagemResultado_Codigo});
               pr_default.close(5);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if (true) break;
               /* Using cursor P008M8 */
               pr_default.execute(6, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1511ContagemResultado_InicioCrr, A1511ContagemResultado_InicioCrr, A456ContagemResultado_Codigo});
               pr_default.close(6);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
         }
         else
         {
            /* Using cursor P008M9 */
            pr_default.execute(7, new Object[] {AV8ContagemResultado_Codigo, AV33Prestadora});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A456ContagemResultado_Codigo = P008M9_A456ContagemResultado_Codigo[0];
               A1636ContagemResultado_ServicoSS = P008M9_A1636ContagemResultado_ServicoSS[0];
               n1636ContagemResultado_ServicoSS = P008M9_n1636ContagemResultado_ServicoSS[0];
               A484ContagemResultado_StatusDmn = P008M9_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P008M9_n484ContagemResultado_StatusDmn[0];
               A1593ContagemResultado_CntSrvTpVnc = P008M9_A1593ContagemResultado_CntSrvTpVnc[0];
               n1593ContagemResultado_CntSrvTpVnc = P008M9_n1593ContagemResultado_CntSrvTpVnc[0];
               A490ContagemResultado_ContratadaCod = P008M9_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P008M9_n490ContagemResultado_ContratadaCod[0];
               A602ContagemResultado_OSVinculada = P008M9_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = P008M9_n602ContagemResultado_OSVinculada[0];
               A912ContagemResultado_HoraEntrega = P008M9_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P008M9_n912ContagemResultado_HoraEntrega[0];
               A601ContagemResultado_Servico = P008M9_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P008M9_n601ContagemResultado_Servico[0];
               A1611ContagemResultado_PrzTpDias = P008M9_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P008M9_n1611ContagemResultado_PrzTpDias[0];
               A1553ContagemResultado_CntSrvCod = P008M9_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P008M9_n1553ContagemResultado_CntSrvCod[0];
               A890ContagemResultado_Responsavel = P008M9_A890ContagemResultado_Responsavel[0];
               n890ContagemResultado_Responsavel = P008M9_n890ContagemResultado_Responsavel[0];
               A472ContagemResultado_DataEntrega = P008M9_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P008M9_n472ContagemResultado_DataEntrega[0];
               A1351ContagemResultado_DataPrevista = P008M9_A1351ContagemResultado_DataPrevista[0];
               n1351ContagemResultado_DataPrevista = P008M9_n1351ContagemResultado_DataPrevista[0];
               A1511ContagemResultado_InicioCrr = P008M9_A1511ContagemResultado_InicioCrr[0];
               n1511ContagemResultado_InicioCrr = P008M9_n1511ContagemResultado_InicioCrr[0];
               A1593ContagemResultado_CntSrvTpVnc = P008M9_A1593ContagemResultado_CntSrvTpVnc[0];
               n1593ContagemResultado_CntSrvTpVnc = P008M9_n1593ContagemResultado_CntSrvTpVnc[0];
               A601ContagemResultado_Servico = P008M9_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P008M9_n601ContagemResultado_Servico[0];
               A1611ContagemResultado_PrzTpDias = P008M9_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P008M9_n1611ContagemResultado_PrzTpDias[0];
               AV20Prazo = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
               AV20Prazo = DateTimeUtil.TAdd( AV20Prazo, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
               AV20Prazo = DateTimeUtil.TAdd( AV20Prazo, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
               AV22Contratada = A490ContagemResultado_ContratadaCod;
               AV23Servico = A601ContagemResultado_Servico;
               AV24Codigo = A456ContagemResultado_Codigo;
               AV26TipoDias = A1611ContagemResultado_PrzTpDias;
               AV29ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
               /* Execute user subroutine: 'ADDPRAZOCORRECAO' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(7);
                  this.cleanup();
                  if (true) return;
               }
               AV31OSVinculada = AV24Codigo;
               /* Execute user subroutine: 'RETORNAPARACONTRATADA' */
               S131 ();
               if ( returnInSub )
               {
                  pr_default.close(7);
                  this.cleanup();
                  if (true) return;
               }
               new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  AV16Responsavel,  "D",  "D",  AV19UserId,  0,  A484ContagemResultado_StatusDmn,  "A",  AV28Parecer,  AV20Prazo,  true) ;
               if ( AV16Responsavel > 0 )
               {
                  A890ContagemResultado_Responsavel = AV16Responsavel;
                  n890ContagemResultado_Responsavel = false;
               }
               A484ContagemResultado_StatusDmn = "A";
               n484ContagemResultado_StatusDmn = false;
               A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV20Prazo);
               n472ContagemResultado_DataEntrega = false;
               A1351ContagemResultado_DataPrevista = AV20Prazo;
               n1351ContagemResultado_DataPrevista = false;
               A1511ContagemResultado_InicioCrr = DateTimeUtil.ServerNow( context, "DEFAULT");
               n1511ContagemResultado_InicioCrr = false;
               /* Using cursor P008M10 */
               pr_default.execute(8, new Object[] {A456ContagemResultado_Codigo});
               while ( (pr_default.getStatus(8) != 101) )
               {
                  A483ContagemResultado_StatusCnt = P008M10_A483ContagemResultado_StatusCnt[0];
                  A511ContagemResultado_HoraCnt = P008M10_A511ContagemResultado_HoraCnt[0];
                  A473ContagemResultado_DataCnt = P008M10_A473ContagemResultado_DataCnt[0];
                  A483ContagemResultado_StatusCnt = 7;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  /* Using cursor P008M11 */
                  pr_default.execute(9, new Object[] {A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                  pr_default.close(9);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                  if (true) break;
                  /* Using cursor P008M12 */
                  pr_default.execute(10, new Object[] {A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                  pr_default.readNext(8);
               }
               pr_default.close(8);
               new prc_disparoservicovinculado(context ).execute(  A456ContagemResultado_Codigo,  AV19UserId) ;
               AV34TemVinculada = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P008M13 */
               pr_default.execute(11, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1511ContagemResultado_InicioCrr, A1511ContagemResultado_InicioCrr, A456ContagemResultado_Codigo});
               pr_default.close(11);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if (true) break;
               /* Using cursor P008M14 */
               pr_default.execute(12, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1511ContagemResultado_InicioCrr, A1511ContagemResultado_InicioCrr, A456ContagemResultado_Codigo});
               pr_default.close(12);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               pr_default.readNext(7);
            }
            pr_default.close(7);
         }
         if ( AV34TemVinculada )
         {
            /* Using cursor P008M15 */
            pr_default.execute(13, new Object[] {AV8ContagemResultado_Codigo});
            while ( (pr_default.getStatus(13) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P008M15_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P008M15_n1553ContagemResultado_CntSrvCod[0];
               A456ContagemResultado_Codigo = P008M15_A456ContagemResultado_Codigo[0];
               A484ContagemResultado_StatusDmn = P008M15_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P008M15_n484ContagemResultado_StatusDmn[0];
               A472ContagemResultado_DataEntrega = P008M15_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P008M15_n472ContagemResultado_DataEntrega[0];
               A912ContagemResultado_HoraEntrega = P008M15_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P008M15_n912ContagemResultado_HoraEntrega[0];
               A490ContagemResultado_ContratadaCod = P008M15_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P008M15_n490ContagemResultado_ContratadaCod[0];
               A601ContagemResultado_Servico = P008M15_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P008M15_n601ContagemResultado_Servico[0];
               A1611ContagemResultado_PrzTpDias = P008M15_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P008M15_n1611ContagemResultado_PrzTpDias[0];
               A601ContagemResultado_Servico = P008M15_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P008M15_n601ContagemResultado_Servico[0];
               A1611ContagemResultado_PrzTpDias = P008M15_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P008M15_n1611ContagemResultado_PrzTpDias[0];
               AV20Prazo = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
               AV20Prazo = DateTimeUtil.TAdd( AV20Prazo, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
               AV20Prazo = DateTimeUtil.TAdd( AV20Prazo, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
               AV22Contratada = A490ContagemResultado_ContratadaCod;
               AV23Servico = A601ContagemResultado_Servico;
               AV24Codigo = A456ContagemResultado_Codigo;
               AV26TipoDias = A1611ContagemResultado_PrzTpDias;
               /* Using cursor P008M16 */
               pr_default.execute(14, new Object[] {A456ContagemResultado_Codigo});
               while ( (pr_default.getStatus(14) != 101) )
               {
                  A892LogResponsavel_DemandaCod = P008M16_A892LogResponsavel_DemandaCod[0];
                  n892LogResponsavel_DemandaCod = P008M16_n892LogResponsavel_DemandaCod[0];
                  A894LogResponsavel_Acao = P008M16_A894LogResponsavel_Acao[0];
                  A1234LogResponsavel_NovoStatus = P008M16_A1234LogResponsavel_NovoStatus[0];
                  n1234LogResponsavel_NovoStatus = P008M16_n1234LogResponsavel_NovoStatus[0];
                  A1797LogResponsavel_Codigo = P008M16_A1797LogResponsavel_Codigo[0];
                  if ( StringUtil.StrCmp(A894LogResponsavel_Acao, "D") == 0 )
                  {
                     A1234LogResponsavel_NovoStatus = "B";
                     n1234LogResponsavel_NovoStatus = false;
                  }
                  else
                  {
                     new prc_inslogresponsavel(context ).execute( ref  AV8ContagemResultado_Codigo,  AV16Responsavel,  "D",  "D",  AV19UserId,  0,  A484ContagemResultado_StatusDmn,  "B",  AV28Parecer,  AV20Prazo,  true) ;
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  /* Using cursor P008M17 */
                  pr_default.execute(15, new Object[] {n1234LogResponsavel_NovoStatus, A1234LogResponsavel_NovoStatus, A1797LogResponsavel_Codigo});
                  pr_default.close(15);
                  dsDefault.SmartCacheProvider.SetUpdated("LogResponsavel") ;
                  if (true) break;
                  /* Using cursor P008M18 */
                  pr_default.execute(16, new Object[] {n1234LogResponsavel_NovoStatus, A1234LogResponsavel_NovoStatus, A1797LogResponsavel_Codigo});
                  pr_default.close(16);
                  dsDefault.SmartCacheProvider.SetUpdated("LogResponsavel") ;
                  pr_default.readNext(14);
               }
               pr_default.close(14);
               A484ContagemResultado_StatusDmn = "B";
               n484ContagemResultado_StatusDmn = false;
               new prc_disparoservicovinculado(context ).execute(  A456ContagemResultado_Codigo,  AV19UserId) ;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P008M19 */
               pr_default.execute(17, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
               pr_default.close(17);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if (true) break;
               /* Using cursor P008M20 */
               pr_default.execute(18, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
               pr_default.close(18);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(13);
         }
         else
         {
            if ( AV13ContratadaOrigem_Codigo > 0 )
            {
               /* Execute user subroutine: 'RETORNAPARAORIGEM' */
               S151 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
               /* Execute user subroutine: 'ADDPRAZOCORRECAO' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
            else
            {
               /* Execute user subroutine: 'RETORNAPARACONTRATANTE' */
               S161 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
               /* Execute user subroutine: 'ADDPRAZORESPOSTA' */
               S121 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
            /* Using cursor P008M21 */
            pr_default.execute(19, new Object[] {AV8ContagemResultado_Codigo});
            while ( (pr_default.getStatus(19) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P008M21_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P008M21_n1553ContagemResultado_CntSrvCod[0];
               A456ContagemResultado_Codigo = P008M21_A456ContagemResultado_Codigo[0];
               A484ContagemResultado_StatusDmn = P008M21_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P008M21_n484ContagemResultado_StatusDmn[0];
               A490ContagemResultado_ContratadaCod = P008M21_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P008M21_n490ContagemResultado_ContratadaCod[0];
               A601ContagemResultado_Servico = P008M21_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P008M21_n601ContagemResultado_Servico[0];
               A890ContagemResultado_Responsavel = P008M21_A890ContagemResultado_Responsavel[0];
               n890ContagemResultado_Responsavel = P008M21_n890ContagemResultado_Responsavel[0];
               A472ContagemResultado_DataEntrega = P008M21_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P008M21_n472ContagemResultado_DataEntrega[0];
               A1351ContagemResultado_DataPrevista = P008M21_A1351ContagemResultado_DataPrevista[0];
               n1351ContagemResultado_DataPrevista = P008M21_n1351ContagemResultado_DataPrevista[0];
               A1511ContagemResultado_InicioCrr = P008M21_A1511ContagemResultado_InicioCrr[0];
               n1511ContagemResultado_InicioCrr = P008M21_n1511ContagemResultado_InicioCrr[0];
               A601ContagemResultado_Servico = P008M21_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P008M21_n601ContagemResultado_Servico[0];
               AV22Contratada = A490ContagemResultado_ContratadaCod;
               AV23Servico = A601ContagemResultado_Servico;
               AV24Codigo = A456ContagemResultado_Codigo;
               AV27StatusDmn = A484ContagemResultado_StatusDmn;
               if ( AV16Responsavel > 0 )
               {
                  A890ContagemResultado_Responsavel = AV16Responsavel;
                  n890ContagemResultado_Responsavel = false;
               }
               A484ContagemResultado_StatusDmn = "A";
               n484ContagemResultado_StatusDmn = false;
               A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV20Prazo);
               n472ContagemResultado_DataEntrega = false;
               A1351ContagemResultado_DataPrevista = AV20Prazo;
               n1351ContagemResultado_DataPrevista = false;
               A1511ContagemResultado_InicioCrr = DateTimeUtil.ServerNow( context, "DEFAULT");
               n1511ContagemResultado_InicioCrr = false;
               /* Using cursor P008M22 */
               pr_default.execute(20, new Object[] {A456ContagemResultado_Codigo});
               while ( (pr_default.getStatus(20) != 101) )
               {
                  A892LogResponsavel_DemandaCod = P008M22_A892LogResponsavel_DemandaCod[0];
                  n892LogResponsavel_DemandaCod = P008M22_n892LogResponsavel_DemandaCod[0];
                  A894LogResponsavel_Acao = P008M22_A894LogResponsavel_Acao[0];
                  A891LogResponsavel_UsuarioCod = P008M22_A891LogResponsavel_UsuarioCod[0];
                  n891LogResponsavel_UsuarioCod = P008M22_n891LogResponsavel_UsuarioCod[0];
                  A896LogResponsavel_Owner = P008M22_A896LogResponsavel_Owner[0];
                  A1177LogResponsavel_Prazo = P008M22_A1177LogResponsavel_Prazo[0];
                  n1177LogResponsavel_Prazo = P008M22_n1177LogResponsavel_Prazo[0];
                  A1797LogResponsavel_Codigo = P008M22_A1797LogResponsavel_Codigo[0];
                  if ( StringUtil.StrCmp(A894LogResponsavel_Acao, "D") == 0 )
                  {
                     if ( (0==AV16Responsavel) )
                     {
                        A891LogResponsavel_UsuarioCod = 0;
                        n891LogResponsavel_UsuarioCod = false;
                        n891LogResponsavel_UsuarioCod = true;
                     }
                     else
                     {
                        A891LogResponsavel_UsuarioCod = AV16Responsavel;
                        n891LogResponsavel_UsuarioCod = false;
                     }
                     A896LogResponsavel_Owner = AV19UserId;
                     A1177LogResponsavel_Prazo = AV20Prazo;
                     n1177LogResponsavel_Prazo = false;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     /* Using cursor P008M23 */
                     pr_default.execute(21, new Object[] {n891LogResponsavel_UsuarioCod, A891LogResponsavel_UsuarioCod, A896LogResponsavel_Owner, n1177LogResponsavel_Prazo, A1177LogResponsavel_Prazo, A1797LogResponsavel_Codigo});
                     pr_default.close(21);
                     dsDefault.SmartCacheProvider.SetUpdated("LogResponsavel") ;
                     if (true) break;
                  }
                  else
                  {
                     new prc_inslogresponsavel(context ).execute( ref  AV8ContagemResultado_Codigo,  AV16Responsavel,  "D",  "D",  AV19UserId,  0,  AV27StatusDmn,  A484ContagemResultado_StatusDmn,  AV28Parecer,  AV20Prazo,  true) ;
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  /* Using cursor P008M24 */
                  pr_default.execute(22, new Object[] {n891LogResponsavel_UsuarioCod, A891LogResponsavel_UsuarioCod, A896LogResponsavel_Owner, n1177LogResponsavel_Prazo, A1177LogResponsavel_Prazo, A1797LogResponsavel_Codigo});
                  pr_default.close(22);
                  dsDefault.SmartCacheProvider.SetUpdated("LogResponsavel") ;
                  if (true) break;
                  /* Using cursor P008M25 */
                  pr_default.execute(23, new Object[] {n891LogResponsavel_UsuarioCod, A891LogResponsavel_UsuarioCod, A896LogResponsavel_Owner, n1177LogResponsavel_Prazo, A1177LogResponsavel_Prazo, A1797LogResponsavel_Codigo});
                  pr_default.close(23);
                  dsDefault.SmartCacheProvider.SetUpdated("LogResponsavel") ;
                  pr_default.readNext(20);
               }
               pr_default.close(20);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P008M26 */
               pr_default.execute(24, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1511ContagemResultado_InicioCrr, A1511ContagemResultado_InicioCrr, A456ContagemResultado_Codigo});
               pr_default.close(24);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if (true) break;
               /* Using cursor P008M27 */
               pr_default.execute(25, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1511ContagemResultado_InicioCrr, A1511ContagemResultado_InicioCrr, A456ContagemResultado_Codigo});
               pr_default.close(25);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(19);
            new prc_disparoservicovinculado(context ).execute(  AV8ContagemResultado_Codigo,  AV19UserId) ;
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'ADDPRAZOCORRECAO' Routine */
         GXt_int1 = AV25Dias;
         new prc_diasparacorrecao(context ).execute( ref  AV29ContratoServicos_Codigo,  AV24Codigo, out  GXt_int1) ;
         AV25Dias = GXt_int1;
         if ( AV25Dias > 0 )
         {
            GXt_dtime2 = AV20Prazo;
            new prc_adddiasuteis(context ).execute(  AV20Prazo,  AV25Dias,  AV26TipoDias, out  GXt_dtime2) ;
            AV20Prazo = GXt_dtime2;
         }
      }

      protected void S121( )
      {
         /* 'ADDPRAZORESPOSTA' Routine */
         if ( (0==AV25Dias) )
         {
            AV25Dias = 5;
         }
         GXt_dtime2 = AV20Prazo;
         new prc_adddiasuteis(context ).execute(  AV20Prazo,  AV25Dias,  AV26TipoDias, out  GXt_dtime2) ;
         AV20Prazo = GXt_dtime2;
      }

      protected void S131( )
      {
         /* 'RETORNAPARACONTRATADA' Routine */
         AV32Acoes = "NTV";
         pr_default.dynParam(26, new Object[]{ new Object[]{
                                              AV31OSVinculada ,
                                              A892LogResponsavel_DemandaCod ,
                                              AV8ContagemResultado_Codigo ,
                                              AV32Acoes ,
                                              A894LogResponsavel_Acao ,
                                              A1149LogResponsavel_OwnerEhContratante },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P008M28 */
         pr_default.execute(26, new Object[] {AV32Acoes, AV31OSVinculada, AV8ContagemResultado_Codigo});
         while ( (pr_default.getStatus(26) != 101) )
         {
            A894LogResponsavel_Acao = P008M28_A894LogResponsavel_Acao[0];
            A1797LogResponsavel_Codigo = P008M28_A1797LogResponsavel_Codigo[0];
            A896LogResponsavel_Owner = P008M28_A896LogResponsavel_Owner[0];
            A892LogResponsavel_DemandaCod = P008M28_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P008M28_n892LogResponsavel_DemandaCod[0];
            GXt_boolean3 = A1149LogResponsavel_OwnerEhContratante;
            new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean3) ;
            A1149LogResponsavel_OwnerEhContratante = GXt_boolean3;
            if ( ! A1149LogResponsavel_OwnerEhContratante )
            {
               /* Using cursor P008M29 */
               pr_default.execute(27, new Object[] {AV22Contratada, A896LogResponsavel_Owner});
               while ( (pr_default.getStatus(27) != 101) )
               {
                  A69ContratadaUsuario_UsuarioCod = P008M29_A69ContratadaUsuario_UsuarioCod[0];
                  A66ContratadaUsuario_ContratadaCod = P008M29_A66ContratadaUsuario_ContratadaCod[0];
                  AV16Responsavel = A896LogResponsavel_Owner;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(27);
               if ( AV16Responsavel > 0 )
               {
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
            }
            pr_default.readNext(26);
         }
         pr_default.close(26);
         if ( (0==AV16Responsavel) )
         {
            AV24Codigo = AV31OSVinculada;
            /* Execute user subroutine: 'SEMRESPONSAVEL' */
            S141 ();
            if (returnInSub) return;
         }
      }

      protected void S151( )
      {
         /* 'RETORNAPARAORIGEM' Routine */
         /* Using cursor P008M30 */
         pr_default.execute(28, new Object[] {AV8ContagemResultado_Codigo});
         while ( (pr_default.getStatus(28) != 101) )
         {
            A490ContagemResultado_ContratadaCod = P008M30_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P008M30_n490ContagemResultado_ContratadaCod[0];
            A1481Contratada_UsaOSistema = P008M30_A1481Contratada_UsaOSistema[0];
            n1481Contratada_UsaOSistema = P008M30_n1481Contratada_UsaOSistema[0];
            A1797LogResponsavel_Codigo = P008M30_A1797LogResponsavel_Codigo[0];
            A896LogResponsavel_Owner = P008M30_A896LogResponsavel_Owner[0];
            A891LogResponsavel_UsuarioCod = P008M30_A891LogResponsavel_UsuarioCod[0];
            n891LogResponsavel_UsuarioCod = P008M30_n891LogResponsavel_UsuarioCod[0];
            A892LogResponsavel_DemandaCod = P008M30_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P008M30_n892LogResponsavel_DemandaCod[0];
            A490ContagemResultado_ContratadaCod = P008M30_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P008M30_n490ContagemResultado_ContratadaCod[0];
            A1481Contratada_UsaOSistema = P008M30_A1481Contratada_UsaOSistema[0];
            n1481Contratada_UsaOSistema = P008M30_n1481Contratada_UsaOSistema[0];
            GXt_boolean3 = A1149LogResponsavel_OwnerEhContratante;
            new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean3) ;
            A1149LogResponsavel_OwnerEhContratante = GXt_boolean3;
            if ( ! A1149LogResponsavel_OwnerEhContratante )
            {
               GXt_boolean3 = A1148LogResponsavel_UsuarioEhContratante;
               new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A891LogResponsavel_UsuarioCod, out  GXt_boolean3) ;
               A1148LogResponsavel_UsuarioEhContratante = GXt_boolean3;
               if ( ! A1148LogResponsavel_UsuarioEhContratante )
               {
                  /* Using cursor P008M31 */
                  pr_default.execute(29, new Object[] {AV13ContratadaOrigem_Codigo, n1481Contratada_UsaOSistema, A1481Contratada_UsaOSistema, A896LogResponsavel_Owner, n891LogResponsavel_UsuarioCod, A891LogResponsavel_UsuarioCod});
                  while ( (pr_default.getStatus(29) != 101) )
                  {
                     A69ContratadaUsuario_UsuarioCod = P008M31_A69ContratadaUsuario_UsuarioCod[0];
                     A66ContratadaUsuario_ContratadaCod = P008M31_A66ContratadaUsuario_ContratadaCod[0];
                     AV16Responsavel = A69ContratadaUsuario_UsuarioCod;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(29);
                  }
                  pr_default.close(29);
                  if ( AV16Responsavel > 0 )
                  {
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
            }
            pr_default.readNext(28);
         }
         pr_default.close(28);
         if ( (0==AV16Responsavel) )
         {
            AV52GXLvl269 = 0;
            /* Using cursor P008M32 */
            pr_default.execute(30, new Object[] {AV13ContratadaOrigem_Codigo});
            while ( (pr_default.getStatus(30) != 101) )
            {
               A1481Contratada_UsaOSistema = P008M32_A1481Contratada_UsaOSistema[0];
               n1481Contratada_UsaOSistema = P008M32_n1481Contratada_UsaOSistema[0];
               A66ContratadaUsuario_ContratadaCod = P008M32_A66ContratadaUsuario_ContratadaCod[0];
               A69ContratadaUsuario_UsuarioCod = P008M32_A69ContratadaUsuario_UsuarioCod[0];
               A1481Contratada_UsaOSistema = P008M32_A1481Contratada_UsaOSistema[0];
               n1481Contratada_UsaOSistema = P008M32_n1481Contratada_UsaOSistema[0];
               AV52GXLvl269 = 1;
               AV16Responsavel = A69ContratadaUsuario_UsuarioCod;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(30);
            }
            pr_default.close(30);
            if ( AV52GXLvl269 == 0 )
            {
               /* Execute user subroutine: 'RETORNAPARACONTRATANTE' */
               S161 ();
               if (returnInSub) return;
            }
         }
      }

      protected void S161( )
      {
         /* 'RETORNAPARACONTRATANTE' Routine */
         AV53GXLvl283 = 0;
         /* Using cursor P008M33 */
         pr_default.execute(31, new Object[] {AV8ContagemResultado_Codigo});
         while ( (pr_default.getStatus(31) != 101) )
         {
            A1797LogResponsavel_Codigo = P008M33_A1797LogResponsavel_Codigo[0];
            A896LogResponsavel_Owner = P008M33_A896LogResponsavel_Owner[0];
            A892LogResponsavel_DemandaCod = P008M33_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P008M33_n892LogResponsavel_DemandaCod[0];
            GXt_boolean3 = A1149LogResponsavel_OwnerEhContratante;
            new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean3) ;
            A1149LogResponsavel_OwnerEhContratante = GXt_boolean3;
            if ( A1149LogResponsavel_OwnerEhContratante )
            {
               AV53GXLvl283 = 1;
               AV16Responsavel = A896LogResponsavel_Owner;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(31);
         }
         pr_default.close(31);
         if ( AV53GXLvl283 == 0 )
         {
            /* Using cursor P008M34 */
            pr_default.execute(32, new Object[] {AV35ContagemResultado_CntCod});
            while ( (pr_default.getStatus(32) != 101) )
            {
               A74Contrato_Codigo = P008M34_A74Contrato_Codigo[0];
               A160ContratoServicos_Codigo = P008M34_A160ContratoServicos_Codigo[0];
               AV55GXLvl292 = 0;
               /* Using cursor P008M35 */
               pr_default.execute(33, new Object[] {A74Contrato_Codigo});
               while ( (pr_default.getStatus(33) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = P008M35_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = P008M35_A1079ContratoGestor_UsuarioCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = P008M35_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = P008M35_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = P008M35_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = P008M35_n1446ContratoGestor_ContratadaAreaCod[0];
                  GXt_boolean3 = A1135ContratoGestor_UsuarioEhContratante;
                  new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean3) ;
                  A1135ContratoGestor_UsuarioEhContratante = GXt_boolean3;
                  if ( A1135ContratoGestor_UsuarioEhContratante )
                  {
                     AV55GXLvl292 = 1;
                     AV16Responsavel = A1079ContratoGestor_UsuarioCod;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
                  pr_default.readNext(33);
               }
               pr_default.close(33);
               if ( AV55GXLvl292 == 0 )
               {
                  /* Using cursor P008M36 */
                  pr_default.execute(34, new Object[] {AV17AreaTrabalho_Codigo});
                  while ( (pr_default.getStatus(34) != 101) )
                  {
                     A29Contratante_Codigo = P008M36_A29Contratante_Codigo[0];
                     n29Contratante_Codigo = P008M36_n29Contratante_Codigo[0];
                     A5AreaTrabalho_Codigo = P008M36_A5AreaTrabalho_Codigo[0];
                     /* Using cursor P008M37 */
                     pr_default.execute(35, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
                     while ( (pr_default.getStatus(35) != 101) )
                     {
                        A63ContratanteUsuario_ContratanteCod = P008M37_A63ContratanteUsuario_ContratanteCod[0];
                        A60ContratanteUsuario_UsuarioCod = P008M37_A60ContratanteUsuario_UsuarioCod[0];
                        AV16Responsavel = A60ContratanteUsuario_UsuarioCod;
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        pr_default.readNext(35);
                     }
                     pr_default.close(35);
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  pr_default.close(34);
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(32);
         }
      }

      protected void S141( )
      {
         /* 'SEMRESPONSAVEL' Routine */
         GXt_int1 = (short)(AV16Responsavel);
         new prc_gestorprepostocontrato(context ).execute( ref  AV24Codigo, out  GXt_int1) ;
         AV16Responsavel = GXt_int1;
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_NaDivergenciaFicaCom");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV20Prazo = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         P008M2_A456ContagemResultado_Codigo = new int[1] ;
         P008M2_A1593ContagemResultado_CntSrvTpVnc = new String[] {""} ;
         P008M2_n1593ContagemResultado_CntSrvTpVnc = new bool[] {false} ;
         P008M2_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008M2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008M2_A1603ContagemResultado_CntCod = new int[1] ;
         P008M2_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P008M2_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P008M2_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P008M2_A1618ContagemResultado_PrzRsp = new short[1] ;
         P008M2_n1618ContagemResultado_PrzRsp = new bool[] {false} ;
         P008M2_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P008M2_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P008M2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008M2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         A1593ContagemResultado_CntSrvTpVnc = "";
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A1611ContagemResultado_PrzTpDias = "";
         AV30ContagemResultado_CntSrvTpVnc = "";
         AV26TipoDias = "";
         P008M3_A456ContagemResultado_Codigo = new int[1] ;
         P008M3_A1636ContagemResultado_ServicoSS = new int[1] ;
         P008M3_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         P008M3_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008M3_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008M3_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008M3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008M3_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P008M3_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P008M3_A601ContagemResultado_Servico = new int[1] ;
         P008M3_n601ContagemResultado_Servico = new bool[] {false} ;
         P008M3_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P008M3_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P008M3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008M3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008M3_A890ContagemResultado_Responsavel = new int[1] ;
         P008M3_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P008M3_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P008M3_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P008M3_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P008M3_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P008M3_A1511ContagemResultado_InicioCrr = new DateTime[] {DateTime.MinValue} ;
         P008M3_n1511ContagemResultado_InicioCrr = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A1511ContagemResultado_InicioCrr = (DateTime)(DateTime.MinValue);
         P008M4_A456ContagemResultado_Codigo = new int[1] ;
         P008M4_A483ContagemResultado_StatusCnt = new short[1] ;
         P008M4_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P008M4_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         A511ContagemResultado_HoraCnt = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         P008M9_A456ContagemResultado_Codigo = new int[1] ;
         P008M9_A1636ContagemResultado_ServicoSS = new int[1] ;
         P008M9_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         P008M9_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008M9_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008M9_A1593ContagemResultado_CntSrvTpVnc = new String[] {""} ;
         P008M9_n1593ContagemResultado_CntSrvTpVnc = new bool[] {false} ;
         P008M9_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008M9_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008M9_A602ContagemResultado_OSVinculada = new int[1] ;
         P008M9_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P008M9_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P008M9_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P008M9_A601ContagemResultado_Servico = new int[1] ;
         P008M9_n601ContagemResultado_Servico = new bool[] {false} ;
         P008M9_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P008M9_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P008M9_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008M9_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008M9_A890ContagemResultado_Responsavel = new int[1] ;
         P008M9_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P008M9_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P008M9_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P008M9_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P008M9_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P008M9_A1511ContagemResultado_InicioCrr = new DateTime[] {DateTime.MinValue} ;
         P008M9_n1511ContagemResultado_InicioCrr = new bool[] {false} ;
         P008M10_A456ContagemResultado_Codigo = new int[1] ;
         P008M10_A483ContagemResultado_StatusCnt = new short[1] ;
         P008M10_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P008M10_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P008M15_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008M15_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008M15_A456ContagemResultado_Codigo = new int[1] ;
         P008M15_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008M15_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008M15_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P008M15_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P008M15_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P008M15_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P008M15_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008M15_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008M15_A601ContagemResultado_Servico = new int[1] ;
         P008M15_n601ContagemResultado_Servico = new bool[] {false} ;
         P008M15_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P008M15_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P008M16_A892LogResponsavel_DemandaCod = new int[1] ;
         P008M16_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P008M16_A894LogResponsavel_Acao = new String[] {""} ;
         P008M16_A1234LogResponsavel_NovoStatus = new String[] {""} ;
         P008M16_n1234LogResponsavel_NovoStatus = new bool[] {false} ;
         P008M16_A1797LogResponsavel_Codigo = new long[1] ;
         A894LogResponsavel_Acao = "";
         A1234LogResponsavel_NovoStatus = "";
         P008M21_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008M21_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008M21_A456ContagemResultado_Codigo = new int[1] ;
         P008M21_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008M21_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008M21_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008M21_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008M21_A601ContagemResultado_Servico = new int[1] ;
         P008M21_n601ContagemResultado_Servico = new bool[] {false} ;
         P008M21_A890ContagemResultado_Responsavel = new int[1] ;
         P008M21_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P008M21_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P008M21_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P008M21_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P008M21_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P008M21_A1511ContagemResultado_InicioCrr = new DateTime[] {DateTime.MinValue} ;
         P008M21_n1511ContagemResultado_InicioCrr = new bool[] {false} ;
         AV27StatusDmn = "";
         P008M22_A892LogResponsavel_DemandaCod = new int[1] ;
         P008M22_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P008M22_A894LogResponsavel_Acao = new String[] {""} ;
         P008M22_A891LogResponsavel_UsuarioCod = new int[1] ;
         P008M22_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         P008M22_A896LogResponsavel_Owner = new int[1] ;
         P008M22_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         P008M22_n1177LogResponsavel_Prazo = new bool[] {false} ;
         P008M22_A1797LogResponsavel_Codigo = new long[1] ;
         A1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         GXt_dtime2 = (DateTime)(DateTime.MinValue);
         AV32Acoes = "";
         P008M28_A894LogResponsavel_Acao = new String[] {""} ;
         P008M28_A1797LogResponsavel_Codigo = new long[1] ;
         P008M28_A896LogResponsavel_Owner = new int[1] ;
         P008M28_A892LogResponsavel_DemandaCod = new int[1] ;
         P008M28_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P008M29_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P008M29_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P008M30_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008M30_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008M30_A1481Contratada_UsaOSistema = new bool[] {false} ;
         P008M30_n1481Contratada_UsaOSistema = new bool[] {false} ;
         P008M30_A1797LogResponsavel_Codigo = new long[1] ;
         P008M30_A896LogResponsavel_Owner = new int[1] ;
         P008M30_A891LogResponsavel_UsuarioCod = new int[1] ;
         P008M30_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         P008M30_A892LogResponsavel_DemandaCod = new int[1] ;
         P008M30_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P008M31_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P008M31_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P008M32_A1481Contratada_UsaOSistema = new bool[] {false} ;
         P008M32_n1481Contratada_UsaOSistema = new bool[] {false} ;
         P008M32_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P008M32_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P008M33_A1797LogResponsavel_Codigo = new long[1] ;
         P008M33_A896LogResponsavel_Owner = new int[1] ;
         P008M33_A892LogResponsavel_DemandaCod = new int[1] ;
         P008M33_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P008M34_A74Contrato_Codigo = new int[1] ;
         P008M34_A160ContratoServicos_Codigo = new int[1] ;
         P008M35_A1078ContratoGestor_ContratoCod = new int[1] ;
         P008M35_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P008M35_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P008M35_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         P008M36_A29Contratante_Codigo = new int[1] ;
         P008M36_n29Contratante_Codigo = new bool[] {false} ;
         P008M36_A5AreaTrabalho_Codigo = new int[1] ;
         P008M37_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P008M37_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_nadivergenciaficacom__default(),
            new Object[][] {
                new Object[] {
               P008M2_A456ContagemResultado_Codigo, P008M2_A1593ContagemResultado_CntSrvTpVnc, P008M2_n1593ContagemResultado_CntSrvTpVnc, P008M2_A490ContagemResultado_ContratadaCod, P008M2_n490ContagemResultado_ContratadaCod, P008M2_A1603ContagemResultado_CntCod, P008M2_n1603ContagemResultado_CntCod, P008M2_A912ContagemResultado_HoraEntrega, P008M2_n912ContagemResultado_HoraEntrega, P008M2_A1618ContagemResultado_PrzRsp,
               P008M2_n1618ContagemResultado_PrzRsp, P008M2_A1611ContagemResultado_PrzTpDias, P008M2_n1611ContagemResultado_PrzTpDias, P008M2_A1553ContagemResultado_CntSrvCod, P008M2_n1553ContagemResultado_CntSrvCod
               }
               , new Object[] {
               P008M3_A456ContagemResultado_Codigo, P008M3_A1636ContagemResultado_ServicoSS, P008M3_n1636ContagemResultado_ServicoSS, P008M3_A484ContagemResultado_StatusDmn, P008M3_n484ContagemResultado_StatusDmn, P008M3_A490ContagemResultado_ContratadaCod, P008M3_n490ContagemResultado_ContratadaCod, P008M3_A912ContagemResultado_HoraEntrega, P008M3_n912ContagemResultado_HoraEntrega, P008M3_A601ContagemResultado_Servico,
               P008M3_n601ContagemResultado_Servico, P008M3_A1611ContagemResultado_PrzTpDias, P008M3_n1611ContagemResultado_PrzTpDias, P008M3_A1553ContagemResultado_CntSrvCod, P008M3_n1553ContagemResultado_CntSrvCod, P008M3_A890ContagemResultado_Responsavel, P008M3_n890ContagemResultado_Responsavel, P008M3_A472ContagemResultado_DataEntrega, P008M3_n472ContagemResultado_DataEntrega, P008M3_A1351ContagemResultado_DataPrevista,
               P008M3_n1351ContagemResultado_DataPrevista, P008M3_A1511ContagemResultado_InicioCrr, P008M3_n1511ContagemResultado_InicioCrr
               }
               , new Object[] {
               P008M4_A456ContagemResultado_Codigo, P008M4_A483ContagemResultado_StatusCnt, P008M4_A511ContagemResultado_HoraCnt, P008M4_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P008M9_A456ContagemResultado_Codigo, P008M9_A1636ContagemResultado_ServicoSS, P008M9_n1636ContagemResultado_ServicoSS, P008M9_A484ContagemResultado_StatusDmn, P008M9_n484ContagemResultado_StatusDmn, P008M9_A1593ContagemResultado_CntSrvTpVnc, P008M9_n1593ContagemResultado_CntSrvTpVnc, P008M9_A490ContagemResultado_ContratadaCod, P008M9_n490ContagemResultado_ContratadaCod, P008M9_A602ContagemResultado_OSVinculada,
               P008M9_n602ContagemResultado_OSVinculada, P008M9_A912ContagemResultado_HoraEntrega, P008M9_n912ContagemResultado_HoraEntrega, P008M9_A601ContagemResultado_Servico, P008M9_n601ContagemResultado_Servico, P008M9_A1611ContagemResultado_PrzTpDias, P008M9_n1611ContagemResultado_PrzTpDias, P008M9_A1553ContagemResultado_CntSrvCod, P008M9_n1553ContagemResultado_CntSrvCod, P008M9_A890ContagemResultado_Responsavel,
               P008M9_n890ContagemResultado_Responsavel, P008M9_A472ContagemResultado_DataEntrega, P008M9_n472ContagemResultado_DataEntrega, P008M9_A1351ContagemResultado_DataPrevista, P008M9_n1351ContagemResultado_DataPrevista, P008M9_A1511ContagemResultado_InicioCrr, P008M9_n1511ContagemResultado_InicioCrr
               }
               , new Object[] {
               P008M10_A456ContagemResultado_Codigo, P008M10_A483ContagemResultado_StatusCnt, P008M10_A511ContagemResultado_HoraCnt, P008M10_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P008M15_A1553ContagemResultado_CntSrvCod, P008M15_n1553ContagemResultado_CntSrvCod, P008M15_A456ContagemResultado_Codigo, P008M15_A484ContagemResultado_StatusDmn, P008M15_n484ContagemResultado_StatusDmn, P008M15_A472ContagemResultado_DataEntrega, P008M15_n472ContagemResultado_DataEntrega, P008M15_A912ContagemResultado_HoraEntrega, P008M15_n912ContagemResultado_HoraEntrega, P008M15_A490ContagemResultado_ContratadaCod,
               P008M15_n490ContagemResultado_ContratadaCod, P008M15_A601ContagemResultado_Servico, P008M15_n601ContagemResultado_Servico, P008M15_A1611ContagemResultado_PrzTpDias, P008M15_n1611ContagemResultado_PrzTpDias
               }
               , new Object[] {
               P008M16_A892LogResponsavel_DemandaCod, P008M16_n892LogResponsavel_DemandaCod, P008M16_A894LogResponsavel_Acao, P008M16_A1234LogResponsavel_NovoStatus, P008M16_n1234LogResponsavel_NovoStatus, P008M16_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P008M21_A1553ContagemResultado_CntSrvCod, P008M21_n1553ContagemResultado_CntSrvCod, P008M21_A456ContagemResultado_Codigo, P008M21_A484ContagemResultado_StatusDmn, P008M21_n484ContagemResultado_StatusDmn, P008M21_A490ContagemResultado_ContratadaCod, P008M21_n490ContagemResultado_ContratadaCod, P008M21_A601ContagemResultado_Servico, P008M21_n601ContagemResultado_Servico, P008M21_A890ContagemResultado_Responsavel,
               P008M21_n890ContagemResultado_Responsavel, P008M21_A472ContagemResultado_DataEntrega, P008M21_n472ContagemResultado_DataEntrega, P008M21_A1351ContagemResultado_DataPrevista, P008M21_n1351ContagemResultado_DataPrevista, P008M21_A1511ContagemResultado_InicioCrr, P008M21_n1511ContagemResultado_InicioCrr
               }
               , new Object[] {
               P008M22_A892LogResponsavel_DemandaCod, P008M22_n892LogResponsavel_DemandaCod, P008M22_A894LogResponsavel_Acao, P008M22_A891LogResponsavel_UsuarioCod, P008M22_n891LogResponsavel_UsuarioCod, P008M22_A896LogResponsavel_Owner, P008M22_A1177LogResponsavel_Prazo, P008M22_n1177LogResponsavel_Prazo, P008M22_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P008M28_A894LogResponsavel_Acao, P008M28_A1797LogResponsavel_Codigo, P008M28_A896LogResponsavel_Owner, P008M28_A892LogResponsavel_DemandaCod, P008M28_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               P008M29_A69ContratadaUsuario_UsuarioCod, P008M29_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               P008M30_A490ContagemResultado_ContratadaCod, P008M30_n490ContagemResultado_ContratadaCod, P008M30_A1481Contratada_UsaOSistema, P008M30_n1481Contratada_UsaOSistema, P008M30_A1797LogResponsavel_Codigo, P008M30_A896LogResponsavel_Owner, P008M30_A891LogResponsavel_UsuarioCod, P008M30_n891LogResponsavel_UsuarioCod, P008M30_A892LogResponsavel_DemandaCod, P008M30_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               P008M31_A69ContratadaUsuario_UsuarioCod, P008M31_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               P008M32_A1481Contratada_UsaOSistema, P008M32_n1481Contratada_UsaOSistema, P008M32_A66ContratadaUsuario_ContratadaCod, P008M32_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               P008M33_A1797LogResponsavel_Codigo, P008M33_A896LogResponsavel_Owner, P008M33_A892LogResponsavel_DemandaCod, P008M33_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               P008M34_A74Contrato_Codigo, P008M34_A160ContratoServicos_Codigo
               }
               , new Object[] {
               P008M35_A1078ContratoGestor_ContratoCod, P008M35_A1079ContratoGestor_UsuarioCod, P008M35_A1446ContratoGestor_ContratadaAreaCod, P008M35_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               P008M36_A29Contratante_Codigo, P008M36_n29Contratante_Codigo, P008M36_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               P008M37_A63ContratanteUsuario_ContratanteCod, P008M37_A60ContratanteUsuario_UsuarioCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1618ContagemResultado_PrzRsp ;
      private short AV25Dias ;
      private short A483ContagemResultado_StatusCnt ;
      private short AV52GXLvl269 ;
      private short AV53GXLvl283 ;
      private short AV55GXLvl292 ;
      private short GXt_int1 ;
      private int AV8ContagemResultado_Codigo ;
      private int AV9ContagemResultado_OSVinculada ;
      private int AV13ContratadaOrigem_Codigo ;
      private int AV14Servico_Codigo ;
      private int AV17AreaTrabalho_Codigo ;
      private int AV19UserId ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int AV33Prestadora ;
      private int AV35ContagemResultado_CntCod ;
      private int AV29ContratoServicos_Codigo ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A601ContagemResultado_Servico ;
      private int A890ContagemResultado_Responsavel ;
      private int AV22Contratada ;
      private int AV23Servico ;
      private int AV24Codigo ;
      private int AV31OSVinculada ;
      private int AV16Responsavel ;
      private int A602ContagemResultado_OSVinculada ;
      private int A892LogResponsavel_DemandaCod ;
      private int A891LogResponsavel_UsuarioCod ;
      private int A896LogResponsavel_Owner ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A74Contrato_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private long A1797LogResponsavel_Codigo ;
      private String scmdbuf ;
      private String A1593ContagemResultado_CntSrvTpVnc ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String AV30ContagemResultado_CntSrvTpVnc ;
      private String AV26TipoDias ;
      private String A484ContagemResultado_StatusDmn ;
      private String A511ContagemResultado_HoraCnt ;
      private String A894LogResponsavel_Acao ;
      private String A1234LogResponsavel_NovoStatus ;
      private String AV27StatusDmn ;
      private String AV32Acoes ;
      private DateTime AV20Prazo ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A1511ContagemResultado_InicioCrr ;
      private DateTime A1177LogResponsavel_Prazo ;
      private DateTime GXt_dtime2 ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool n1593ContagemResultado_CntSrvTpVnc ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n1618ContagemResultado_PrzRsp ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n601ContagemResultado_Servico ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n1511ContagemResultado_InicioCrr ;
      private bool returnInSub ;
      private bool AV34TemVinculada ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1234LogResponsavel_NovoStatus ;
      private bool n891LogResponsavel_UsuarioCod ;
      private bool n1177LogResponsavel_Prazo ;
      private bool A1149LogResponsavel_OwnerEhContratante ;
      private bool A1481Contratada_UsaOSistema ;
      private bool n1481Contratada_UsaOSistema ;
      private bool A1148LogResponsavel_UsuarioEhContratante ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool GXt_boolean3 ;
      private bool n29Contratante_Codigo ;
      private String AV28Parecer ;
      private String AV12ContagemResultado_Demanda ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P008M2_A456ContagemResultado_Codigo ;
      private String[] P008M2_A1593ContagemResultado_CntSrvTpVnc ;
      private bool[] P008M2_n1593ContagemResultado_CntSrvTpVnc ;
      private int[] P008M2_A490ContagemResultado_ContratadaCod ;
      private bool[] P008M2_n490ContagemResultado_ContratadaCod ;
      private int[] P008M2_A1603ContagemResultado_CntCod ;
      private bool[] P008M2_n1603ContagemResultado_CntCod ;
      private DateTime[] P008M2_A912ContagemResultado_HoraEntrega ;
      private bool[] P008M2_n912ContagemResultado_HoraEntrega ;
      private short[] P008M2_A1618ContagemResultado_PrzRsp ;
      private bool[] P008M2_n1618ContagemResultado_PrzRsp ;
      private String[] P008M2_A1611ContagemResultado_PrzTpDias ;
      private bool[] P008M2_n1611ContagemResultado_PrzTpDias ;
      private int[] P008M2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008M2_n1553ContagemResultado_CntSrvCod ;
      private int[] P008M3_A456ContagemResultado_Codigo ;
      private int[] P008M3_A1636ContagemResultado_ServicoSS ;
      private bool[] P008M3_n1636ContagemResultado_ServicoSS ;
      private String[] P008M3_A484ContagemResultado_StatusDmn ;
      private bool[] P008M3_n484ContagemResultado_StatusDmn ;
      private int[] P008M3_A490ContagemResultado_ContratadaCod ;
      private bool[] P008M3_n490ContagemResultado_ContratadaCod ;
      private DateTime[] P008M3_A912ContagemResultado_HoraEntrega ;
      private bool[] P008M3_n912ContagemResultado_HoraEntrega ;
      private int[] P008M3_A601ContagemResultado_Servico ;
      private bool[] P008M3_n601ContagemResultado_Servico ;
      private String[] P008M3_A1611ContagemResultado_PrzTpDias ;
      private bool[] P008M3_n1611ContagemResultado_PrzTpDias ;
      private int[] P008M3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008M3_n1553ContagemResultado_CntSrvCod ;
      private int[] P008M3_A890ContagemResultado_Responsavel ;
      private bool[] P008M3_n890ContagemResultado_Responsavel ;
      private DateTime[] P008M3_A472ContagemResultado_DataEntrega ;
      private bool[] P008M3_n472ContagemResultado_DataEntrega ;
      private DateTime[] P008M3_A1351ContagemResultado_DataPrevista ;
      private bool[] P008M3_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P008M3_A1511ContagemResultado_InicioCrr ;
      private bool[] P008M3_n1511ContagemResultado_InicioCrr ;
      private int[] P008M4_A456ContagemResultado_Codigo ;
      private short[] P008M4_A483ContagemResultado_StatusCnt ;
      private String[] P008M4_A511ContagemResultado_HoraCnt ;
      private DateTime[] P008M4_A473ContagemResultado_DataCnt ;
      private int[] P008M9_A456ContagemResultado_Codigo ;
      private int[] P008M9_A1636ContagemResultado_ServicoSS ;
      private bool[] P008M9_n1636ContagemResultado_ServicoSS ;
      private String[] P008M9_A484ContagemResultado_StatusDmn ;
      private bool[] P008M9_n484ContagemResultado_StatusDmn ;
      private String[] P008M9_A1593ContagemResultado_CntSrvTpVnc ;
      private bool[] P008M9_n1593ContagemResultado_CntSrvTpVnc ;
      private int[] P008M9_A490ContagemResultado_ContratadaCod ;
      private bool[] P008M9_n490ContagemResultado_ContratadaCod ;
      private int[] P008M9_A602ContagemResultado_OSVinculada ;
      private bool[] P008M9_n602ContagemResultado_OSVinculada ;
      private DateTime[] P008M9_A912ContagemResultado_HoraEntrega ;
      private bool[] P008M9_n912ContagemResultado_HoraEntrega ;
      private int[] P008M9_A601ContagemResultado_Servico ;
      private bool[] P008M9_n601ContagemResultado_Servico ;
      private String[] P008M9_A1611ContagemResultado_PrzTpDias ;
      private bool[] P008M9_n1611ContagemResultado_PrzTpDias ;
      private int[] P008M9_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008M9_n1553ContagemResultado_CntSrvCod ;
      private int[] P008M9_A890ContagemResultado_Responsavel ;
      private bool[] P008M9_n890ContagemResultado_Responsavel ;
      private DateTime[] P008M9_A472ContagemResultado_DataEntrega ;
      private bool[] P008M9_n472ContagemResultado_DataEntrega ;
      private DateTime[] P008M9_A1351ContagemResultado_DataPrevista ;
      private bool[] P008M9_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P008M9_A1511ContagemResultado_InicioCrr ;
      private bool[] P008M9_n1511ContagemResultado_InicioCrr ;
      private int[] P008M10_A456ContagemResultado_Codigo ;
      private short[] P008M10_A483ContagemResultado_StatusCnt ;
      private String[] P008M10_A511ContagemResultado_HoraCnt ;
      private DateTime[] P008M10_A473ContagemResultado_DataCnt ;
      private int[] P008M15_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008M15_n1553ContagemResultado_CntSrvCod ;
      private int[] P008M15_A456ContagemResultado_Codigo ;
      private String[] P008M15_A484ContagemResultado_StatusDmn ;
      private bool[] P008M15_n484ContagemResultado_StatusDmn ;
      private DateTime[] P008M15_A472ContagemResultado_DataEntrega ;
      private bool[] P008M15_n472ContagemResultado_DataEntrega ;
      private DateTime[] P008M15_A912ContagemResultado_HoraEntrega ;
      private bool[] P008M15_n912ContagemResultado_HoraEntrega ;
      private int[] P008M15_A490ContagemResultado_ContratadaCod ;
      private bool[] P008M15_n490ContagemResultado_ContratadaCod ;
      private int[] P008M15_A601ContagemResultado_Servico ;
      private bool[] P008M15_n601ContagemResultado_Servico ;
      private String[] P008M15_A1611ContagemResultado_PrzTpDias ;
      private bool[] P008M15_n1611ContagemResultado_PrzTpDias ;
      private int[] P008M16_A892LogResponsavel_DemandaCod ;
      private bool[] P008M16_n892LogResponsavel_DemandaCod ;
      private String[] P008M16_A894LogResponsavel_Acao ;
      private String[] P008M16_A1234LogResponsavel_NovoStatus ;
      private bool[] P008M16_n1234LogResponsavel_NovoStatus ;
      private long[] P008M16_A1797LogResponsavel_Codigo ;
      private int[] P008M21_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008M21_n1553ContagemResultado_CntSrvCod ;
      private int[] P008M21_A456ContagemResultado_Codigo ;
      private String[] P008M21_A484ContagemResultado_StatusDmn ;
      private bool[] P008M21_n484ContagemResultado_StatusDmn ;
      private int[] P008M21_A490ContagemResultado_ContratadaCod ;
      private bool[] P008M21_n490ContagemResultado_ContratadaCod ;
      private int[] P008M21_A601ContagemResultado_Servico ;
      private bool[] P008M21_n601ContagemResultado_Servico ;
      private int[] P008M21_A890ContagemResultado_Responsavel ;
      private bool[] P008M21_n890ContagemResultado_Responsavel ;
      private DateTime[] P008M21_A472ContagemResultado_DataEntrega ;
      private bool[] P008M21_n472ContagemResultado_DataEntrega ;
      private DateTime[] P008M21_A1351ContagemResultado_DataPrevista ;
      private bool[] P008M21_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P008M21_A1511ContagemResultado_InicioCrr ;
      private bool[] P008M21_n1511ContagemResultado_InicioCrr ;
      private int[] P008M22_A892LogResponsavel_DemandaCod ;
      private bool[] P008M22_n892LogResponsavel_DemandaCod ;
      private String[] P008M22_A894LogResponsavel_Acao ;
      private int[] P008M22_A891LogResponsavel_UsuarioCod ;
      private bool[] P008M22_n891LogResponsavel_UsuarioCod ;
      private int[] P008M22_A896LogResponsavel_Owner ;
      private DateTime[] P008M22_A1177LogResponsavel_Prazo ;
      private bool[] P008M22_n1177LogResponsavel_Prazo ;
      private long[] P008M22_A1797LogResponsavel_Codigo ;
      private String[] P008M28_A894LogResponsavel_Acao ;
      private long[] P008M28_A1797LogResponsavel_Codigo ;
      private int[] P008M28_A896LogResponsavel_Owner ;
      private int[] P008M28_A892LogResponsavel_DemandaCod ;
      private bool[] P008M28_n892LogResponsavel_DemandaCod ;
      private int[] P008M29_A69ContratadaUsuario_UsuarioCod ;
      private int[] P008M29_A66ContratadaUsuario_ContratadaCod ;
      private int[] P008M30_A490ContagemResultado_ContratadaCod ;
      private bool[] P008M30_n490ContagemResultado_ContratadaCod ;
      private bool[] P008M30_A1481Contratada_UsaOSistema ;
      private bool[] P008M30_n1481Contratada_UsaOSistema ;
      private long[] P008M30_A1797LogResponsavel_Codigo ;
      private int[] P008M30_A896LogResponsavel_Owner ;
      private int[] P008M30_A891LogResponsavel_UsuarioCod ;
      private bool[] P008M30_n891LogResponsavel_UsuarioCod ;
      private int[] P008M30_A892LogResponsavel_DemandaCod ;
      private bool[] P008M30_n892LogResponsavel_DemandaCod ;
      private int[] P008M31_A69ContratadaUsuario_UsuarioCod ;
      private int[] P008M31_A66ContratadaUsuario_ContratadaCod ;
      private bool[] P008M32_A1481Contratada_UsaOSistema ;
      private bool[] P008M32_n1481Contratada_UsaOSistema ;
      private int[] P008M32_A66ContratadaUsuario_ContratadaCod ;
      private int[] P008M32_A69ContratadaUsuario_UsuarioCod ;
      private long[] P008M33_A1797LogResponsavel_Codigo ;
      private int[] P008M33_A896LogResponsavel_Owner ;
      private int[] P008M33_A892LogResponsavel_DemandaCod ;
      private bool[] P008M33_n892LogResponsavel_DemandaCod ;
      private int[] P008M34_A74Contrato_Codigo ;
      private int[] P008M34_A160ContratoServicos_Codigo ;
      private int[] P008M35_A1078ContratoGestor_ContratoCod ;
      private int[] P008M35_A1079ContratoGestor_UsuarioCod ;
      private int[] P008M35_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P008M35_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] P008M36_A29Contratante_Codigo ;
      private bool[] P008M36_n29Contratante_Codigo ;
      private int[] P008M36_A5AreaTrabalho_Codigo ;
      private int[] P008M37_A63ContratanteUsuario_ContratanteCod ;
      private int[] P008M37_A60ContratanteUsuario_UsuarioCod ;
   }

   public class prc_nadivergenciaficacom__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P008M28( IGxContext context ,
                                              int AV31OSVinculada ,
                                              int A892LogResponsavel_DemandaCod ,
                                              int AV8ContagemResultado_Codigo ,
                                              String AV32Acoes ,
                                              String A894LogResponsavel_Acao ,
                                              bool A1149LogResponsavel_OwnerEhContratante )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [3] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT [LogResponsavel_Acao], [LogResponsavel_Codigo], [LogResponsavel_Owner], [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM [LogResponsavel] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ((CHARINDEX(RTRIM([LogResponsavel_Acao]), @AV32Acoes)) > 0)";
         if ( AV31OSVinculada > 0 )
         {
            sWhereString = sWhereString + " and ([LogResponsavel_DemandaCod] = @AV31OSVinculada)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( (0==AV31OSVinculada) )
         {
            sWhereString = sWhereString + " and ([LogResponsavel_DemandaCod] = @AV8ContagemResultado_Codigo)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 26 :
                     return conditional_P008M28(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new UpdateCursor(def[21])
         ,new UpdateCursor(def[22])
         ,new UpdateCursor(def[23])
         ,new UpdateCursor(def[24])
         ,new UpdateCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008M2 ;
          prmP008M2 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M3 ;
          prmP008M3 = new Object[] {
          new Object[] {"@AV9ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV33Prestadora",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M4 ;
          prmP008M4 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M5 ;
          prmP008M5 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP008M6 ;
          prmP008M6 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP008M7 ;
          prmP008M7 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_InicioCrr",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M8 ;
          prmP008M8 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_InicioCrr",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M9 ;
          prmP008M9 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV33Prestadora",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M10 ;
          prmP008M10 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M11 ;
          prmP008M11 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP008M12 ;
          prmP008M12 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP008M13 ;
          prmP008M13 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_InicioCrr",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M14 ;
          prmP008M14 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_InicioCrr",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M15 ;
          prmP008M15 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M16 ;
          prmP008M16 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M17 ;
          prmP008M17 = new Object[] {
          new Object[] {"@LogResponsavel_NovoStatus",SqlDbType.Char,1,0} ,
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmP008M18 ;
          prmP008M18 = new Object[] {
          new Object[] {"@LogResponsavel_NovoStatus",SqlDbType.Char,1,0} ,
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmP008M19 ;
          prmP008M19 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M20 ;
          prmP008M20 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M21 ;
          prmP008M21 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M22 ;
          prmP008M22 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M23 ;
          prmP008M23 = new Object[] {
          new Object[] {"@LogResponsavel_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmP008M24 ;
          prmP008M24 = new Object[] {
          new Object[] {"@LogResponsavel_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmP008M25 ;
          prmP008M25 = new Object[] {
          new Object[] {"@LogResponsavel_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmP008M26 ;
          prmP008M26 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_InicioCrr",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M27 ;
          prmP008M27 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_InicioCrr",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M29 ;
          prmP008M29 = new Object[] {
          new Object[] {"@AV22Contratada",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M30 ;
          prmP008M30 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M31 ;
          prmP008M31 = new Object[] {
          new Object[] {"@AV13ContratadaOrigem_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_UsaOSistema",SqlDbType.Bit,4,0} ,
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M32 ;
          prmP008M32 = new Object[] {
          new Object[] {"@AV13ContratadaOrigem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M33 ;
          prmP008M33 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M34 ;
          prmP008M34 = new Object[] {
          new Object[] {"@AV35ContagemResultado_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M35 ;
          prmP008M35 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M36 ;
          prmP008M36 = new Object[] {
          new Object[] {"@AV17AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M37 ;
          prmP008M37 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008M28 ;
          prmP008M28 = new Object[] {
          new Object[] {"@AV32Acoes",SqlDbType.Char,20,0} ,
          new Object[] {"@AV31OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008M2", "SELECT TOP 1 T1.[ContagemResultado_Codigo], T2.[ContratoServicos_TipoVnc] AS ContagemResultado_CntSrvTpVnc, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_HoraEntrega], T2.[ContratoServicos_PrazoResposta] AS ContagemResultado_PrzRsp, T2.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV8ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M2,1,0,false,true )
             ,new CursorDef("P008M3", "SELECT TOP 1 T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ServicoSS], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_HoraEntrega], T2.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_InicioCrr] FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE (T1.[ContagemResultado_Codigo] = @AV9ContagemResultado_OSVinculada) AND (T1.[ContagemResultado_ContratadaCod] <> @AV33Prestadora) AND (Not ( T1.[ContagemResultado_StatusDmn] = 'X' or T1.[ContagemResultado_StatusDmn] = 'D' or T1.[ContagemResultado_StatusDmn] = 'H' or T1.[ContagemResultado_StatusDmn] = 'O' or T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L')) AND (T1.[ContagemResultado_ServicoSS] IS NULL) ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M3,1,0,true,true )
             ,new CursorDef("P008M4", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_StatusCnt], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] DESC, [ContagemResultado_DataCnt] DESC, [ContagemResultado_HoraCnt] DESC ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M4,1,0,true,true )
             ,new CursorDef("P008M5", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M5)
             ,new CursorDef("P008M6", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M6)
             ,new CursorDef("P008M7", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_InicioCrr]=@ContagemResultado_InicioCrr  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M7)
             ,new CursorDef("P008M8", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_InicioCrr]=@ContagemResultado_InicioCrr  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M8)
             ,new CursorDef("P008M9", "SELECT TOP 1 T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ServicoSS], T1.[ContagemResultado_StatusDmn], T2.[ContratoServicos_TipoVnc] AS ContagemResultado_CntSrvTpVnc, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_HoraEntrega], T2.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_InicioCrr] FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE (T1.[ContagemResultado_OSVinculada] = @AV8ContagemResultado_Codigo) AND (T1.[ContagemResultado_ContratadaCod] <> @AV33Prestadora) AND (T2.[ContratoServicos_TipoVnc] = 'C' or T2.[ContratoServicos_TipoVnc] = 'A') AND (Not ( T1.[ContagemResultado_StatusDmn] = 'X' or T1.[ContagemResultado_StatusDmn] = 'D' or T1.[ContagemResultado_StatusDmn] = 'H' or T1.[ContagemResultado_StatusDmn] = 'O' or T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L')) AND (T1.[ContagemResultado_ServicoSS] IS NULL) ORDER BY T1.[ContagemResultado_OSVinculada] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M9,1,0,true,true )
             ,new CursorDef("P008M10", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_StatusCnt], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] DESC, [ContagemResultado_DataCnt] DESC, [ContagemResultado_HoraCnt] DESC ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M10,1,0,true,true )
             ,new CursorDef("P008M11", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M11)
             ,new CursorDef("P008M12", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M12)
             ,new CursorDef("P008M13", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_InicioCrr]=@ContagemResultado_InicioCrr  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M13)
             ,new CursorDef("P008M14", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_InicioCrr]=@ContagemResultado_InicioCrr  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M14)
             ,new CursorDef("P008M15", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV8ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M15,1,0,true,true )
             ,new CursorDef("P008M16", "SELECT TOP 1 [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, [LogResponsavel_Acao], [LogResponsavel_NovoStatus], [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (UPDLOCK) WHERE [LogResponsavel_DemandaCod] = @ContagemResultado_Codigo ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M16,1,0,true,true )
             ,new CursorDef("P008M17", "UPDATE [LogResponsavel] SET [LogResponsavel_NovoStatus]=@LogResponsavel_NovoStatus  WHERE [LogResponsavel_Codigo] = @LogResponsavel_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M17)
             ,new CursorDef("P008M18", "UPDATE [LogResponsavel] SET [LogResponsavel_NovoStatus]=@LogResponsavel_NovoStatus  WHERE [LogResponsavel_Codigo] = @LogResponsavel_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M18)
             ,new CursorDef("P008M19", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M19)
             ,new CursorDef("P008M20", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M20)
             ,new CursorDef("P008M21", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_InicioCrr] FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV8ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M21,1,0,true,true )
             ,new CursorDef("P008M22", "SELECT [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, [LogResponsavel_Acao], [LogResponsavel_UsuarioCod], [LogResponsavel_Owner], [LogResponsavel_Prazo], [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (UPDLOCK) WHERE [LogResponsavel_DemandaCod] = @ContagemResultado_Codigo ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M22,1,0,true,false )
             ,new CursorDef("P008M23", "UPDATE [LogResponsavel] SET [LogResponsavel_UsuarioCod]=@LogResponsavel_UsuarioCod, [LogResponsavel_Owner]=@LogResponsavel_Owner, [LogResponsavel_Prazo]=@LogResponsavel_Prazo  WHERE [LogResponsavel_Codigo] = @LogResponsavel_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M23)
             ,new CursorDef("P008M24", "UPDATE [LogResponsavel] SET [LogResponsavel_UsuarioCod]=@LogResponsavel_UsuarioCod, [LogResponsavel_Owner]=@LogResponsavel_Owner, [LogResponsavel_Prazo]=@LogResponsavel_Prazo  WHERE [LogResponsavel_Codigo] = @LogResponsavel_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M24)
             ,new CursorDef("P008M25", "UPDATE [LogResponsavel] SET [LogResponsavel_UsuarioCod]=@LogResponsavel_UsuarioCod, [LogResponsavel_Owner]=@LogResponsavel_Owner, [LogResponsavel_Prazo]=@LogResponsavel_Prazo  WHERE [LogResponsavel_Codigo] = @LogResponsavel_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M25)
             ,new CursorDef("P008M26", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_InicioCrr]=@ContagemResultado_InicioCrr  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M26)
             ,new CursorDef("P008M27", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_InicioCrr]=@ContagemResultado_InicioCrr  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008M27)
             ,new CursorDef("P008M28", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M28,100,0,true,false )
             ,new CursorDef("P008M29", "SELECT TOP 1 [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @AV22Contratada and [ContratadaUsuario_UsuarioCod] = @LogResponsavel_Owner ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M29,1,0,false,true )
             ,new CursorDef("P008M30", "SELECT T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T3.[Contratada_UsaOSistema], T1.[LogResponsavel_Codigo], T1.[LogResponsavel_Owner], T1.[LogResponsavel_UsuarioCod], T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM (([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) WHERE T1.[LogResponsavel_DemandaCod] = @AV8ContagemResultado_Codigo ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M30,100,0,true,false )
             ,new CursorDef("P008M31", "SELECT TOP 1 [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo FROM [ContratadaUsuario] WITH (NOLOCK) WHERE ([ContratadaUsuario_ContratadaCod] = @AV13ContratadaOrigem_Codigo) AND (@Contratada_UsaOSistema = 1) AND ([ContratadaUsuario_UsuarioCod] = @LogResponsavel_Owner or [ContratadaUsuario_UsuarioCod] = @LogResponsavel_UsuarioCod) ORDER BY [ContratadaUsuario_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M31,1,0,false,true )
             ,new CursorDef("P008M32", "SELECT TOP 1 T2.[Contratada_UsaOSistema], T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo, T1.[ContratadaUsuario_UsuarioCod] FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) WHERE (T1.[ContratadaUsuario_ContratadaCod] = @AV13ContratadaOrigem_Codigo) AND (T2.[Contratada_UsaOSistema] = 1) ORDER BY T1.[ContratadaUsuario_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M32,1,0,false,true )
             ,new CursorDef("P008M33", "SELECT [LogResponsavel_Codigo], [LogResponsavel_Owner], [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @AV8ContagemResultado_Codigo ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M33,100,0,true,false )
             ,new CursorDef("P008M34", "SELECT [Contrato_Codigo], [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV35ContagemResultado_CntCod ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M34,1,0,true,true )
             ,new CursorDef("P008M35", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M35,100,0,true,false )
             ,new CursorDef("P008M36", "SELECT TOP 1 [Contratante_Codigo], [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV17AreaTrabalho_Codigo ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M36,1,0,true,true )
             ,new CursorDef("P008M37", "SELECT TOP 1 [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod] FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @Contratante_Codigo ORDER BY [ContratanteUsuario_ContratanteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008M37,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((short[]) buf[9])[0] = rslt.getShort(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[19])[0] = rslt.getGXDateTime(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[21])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[23])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[25])[0] = rslt.getGXDateTime(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((long[]) buf[5])[0] = rslt.getLong(4) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[15])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((long[]) buf[8])[0] = rslt.getLong(6) ;
                return;
             case 26 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((long[]) buf[1])[0] = rslt.getLong(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((long[]) buf[4])[0] = rslt.getLong(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 31 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 10 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (long)parms[2]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (long)parms[2]);
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[4]);
                }
                stmt.SetParameter(4, (long)parms[5]);
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[4]);
                }
                stmt.SetParameter(4, (long)parms[5]);
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[4]);
                }
                stmt.SetParameter(4, (long)parms[5]);
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 26 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 34 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 35 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
