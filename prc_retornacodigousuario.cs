/*
               File: PRC_RetornaCodigoUsuario
        Description: PRC_Retorna Codigo Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:48.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_retornacodigousuario : GXProcedure
   {
      public prc_retornacodigousuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_retornacodigousuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out int aP0_Usuario_Codigo )
      {
         this.AV8Usuario_Codigo = 0 ;
         initialize();
         executePrivate();
         aP0_Usuario_Codigo=this.AV8Usuario_Codigo;
      }

      public int executeUdp( )
      {
         this.AV8Usuario_Codigo = 0 ;
         initialize();
         executePrivate();
         aP0_Usuario_Codigo=this.AV8Usuario_Codigo;
         return AV8Usuario_Codigo ;
      }

      public void executeSubmit( out int aP0_Usuario_Codigo )
      {
         prc_retornacodigousuario objprc_retornacodigousuario;
         objprc_retornacodigousuario = new prc_retornacodigousuario();
         objprc_retornacodigousuario.AV8Usuario_Codigo = 0 ;
         objprc_retornacodigousuario.context.SetSubmitInitialConfig(context);
         objprc_retornacodigousuario.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_retornacodigousuario);
         aP0_Usuario_Codigo=this.AV8Usuario_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_retornacodigousuario)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV12Udparg1 = new SdtGAMUser(context).getid();
         /* Using cursor P001B2 */
         pr_default.execute(0, new Object[] {AV12Udparg1});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A341Usuario_UserGamGuid = P001B2_A341Usuario_UserGamGuid[0];
            A54Usuario_Ativo = P001B2_A54Usuario_Ativo[0];
            A1Usuario_Codigo = P001B2_A1Usuario_Codigo[0];
            AV8Usuario_Codigo = A1Usuario_Codigo;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV12Udparg1 = "";
         scmdbuf = "";
         P001B2_A341Usuario_UserGamGuid = new String[] {""} ;
         P001B2_A54Usuario_Ativo = new bool[] {false} ;
         P001B2_A1Usuario_Codigo = new int[1] ;
         A341Usuario_UserGamGuid = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_retornacodigousuario__default(),
            new Object[][] {
                new Object[] {
               P001B2_A341Usuario_UserGamGuid, P001B2_A54Usuario_Ativo, P001B2_A1Usuario_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Usuario_Codigo ;
      private int A1Usuario_Codigo ;
      private String AV12Udparg1 ;
      private String scmdbuf ;
      private String A341Usuario_UserGamGuid ;
      private bool A54Usuario_Ativo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P001B2_A341Usuario_UserGamGuid ;
      private bool[] P001B2_A54Usuario_Ativo ;
      private int[] P001B2_A1Usuario_Codigo ;
      private int aP0_Usuario_Codigo ;
   }

   public class prc_retornacodigousuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP001B2 ;
          prmP001B2 = new Object[] {
          new Object[] {"@AV12Udparg1",SqlDbType.Char,40,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P001B2", "SELECT [Usuario_UserGamGuid], [Usuario_Ativo], [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE ([Usuario_UserGamGuid] = @AV12Udparg1) AND ([Usuario_Ativo] = 1) ORDER BY [Usuario_UserGamGuid] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001B2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 40) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
       }
    }

 }

}
