/*
               File: PRC_AgruparParaFaturamento
        Description: Agrupar Para Faturamento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/17/2020 1:56:27.83
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_agruparparafaturamento : GXProcedure
   {
      public prc_agruparparafaturamento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_agruparparafaturamento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           short aP1_ContagemResultado_StatusCnt ,
                           String aP2_GridStateXML ,
                           String aP3_Acao ,
                           String aP4_Lote_Nome )
      {
         this.AV45Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV38ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         this.AV60GridStateXML = aP2_GridStateXML;
         this.AV8Acao = aP3_Acao;
         this.AV71Lote_Nome = aP4_Lote_Nome;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 short aP1_ContagemResultado_StatusCnt ,
                                 String aP2_GridStateXML ,
                                 String aP3_Acao ,
                                 String aP4_Lote_Nome )
      {
         prc_agruparparafaturamento objprc_agruparparafaturamento;
         objprc_agruparparafaturamento = new prc_agruparparafaturamento();
         objprc_agruparparafaturamento.AV45Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objprc_agruparparafaturamento.AV38ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         objprc_agruparparafaturamento.AV60GridStateXML = aP2_GridStateXML;
         objprc_agruparparafaturamento.AV8Acao = aP3_Acao;
         objprc_agruparparafaturamento.AV71Lote_Nome = aP4_Lote_Nome;
         objprc_agruparparafaturamento.context.SetSubmitInitialConfig(context);
         objprc_agruparparafaturamento.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_agruparparafaturamento);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_agruparparafaturamento)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV66WWPContext) ;
         AV89Codigos.FromXml(AV90WebSession.Get("SdtCodigos"), "Collection");
         if ( AV89Codigos.Count == 0 )
         {
            AV58GridState.gxTpr_Dynamicfilters.FromXml(AV60GridStateXML, "");
            if ( AV58GridState.gxTpr_Dynamicfilters.Count >= 1 )
            {
               AV59GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV58GridState.gxTpr_Dynamicfilters.Item(1));
               AV49DynamicFiltersSelector1 = AV59GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 )
               {
                  AV17ContagemResultado_DataCnt1 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                  AV14ContagemResultado_DataCnt_To1 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV40ContagemResultado_StatusDmn1 = AV59GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV207DynamicFiltersOperator1 = AV59GridStateDynamicFilter.gxTpr_Operator;
                  AV27ContagemResultado_OsFsOsFm1 = AV59GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV283ContagemResultado_DataDmn1 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                  AV278ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
               {
                  AV294ContagemResultado_DataPrevista1 = context.localUtil.CToT( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                  AV289ContagemResultado_DataPrevista_To1 = context.localUtil.CToT( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICO") == 0 )
               {
                  AV82ContagemResultado_Servico1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
               {
                  AV126ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV31ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV10ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
               {
                  AV93ContagemResultado_ServicoGrupo1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_BASELINE") == 0 )
               {
                  AV86ContagemResultado_Baseline1 = AV59GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
               {
                  AV23ContagemResultado_NaoCnfDmnCod1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_PFBFM") == 0 )
               {
                  AV131ContagemResultado_PFBFM1 = NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, ".");
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_PFBFS") == 0 )
               {
                  AV136ContagemResultado_PFBFS1 = NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, ".");
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  AV143ContagemResultado_Agrupador1 = AV59GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  AV187ContagemResultado_Descricao1 = AV59GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_OWNER") == 0 )
               {
                  AV193ContagemResultado_Owner1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
               {
                  AV219ContagemResultado_TemPndHmlg1 = AV59GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "CONTAGEMRESULTADO_CNTCOD") == 0 )
               {
                  AV305ContagemResultado_CntCod1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV58GridState.gxTpr_Dynamicfilters.Count >= 2 )
               {
                  AV47DynamicFiltersEnabled2 = true;
                  AV59GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV58GridState.gxTpr_Dynamicfilters.Item(2));
                  AV50DynamicFiltersSelector2 = AV59GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 )
                  {
                     AV18ContagemResultado_DataCnt2 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                     AV15ContagemResultado_DataCnt_To2 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV41ContagemResultado_StatusDmn2 = AV59GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV208DynamicFiltersOperator2 = AV59GridStateDynamicFilter.gxTpr_Operator;
                     AV28ContagemResultado_OsFsOsFm2 = AV59GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV284ContagemResultado_DataDmn2 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                     AV279ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                  {
                     AV295ContagemResultado_DataPrevista2 = context.localUtil.CToT( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                     AV290ContagemResultado_DataPrevista_To2 = context.localUtil.CToT( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICO") == 0 )
                  {
                     AV84ContagemResultado_Servico2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                  {
                     AV127ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV32ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV11ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
                  {
                     AV94ContagemResultado_ServicoGrupo2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_BASELINE") == 0 )
                  {
                     AV87ContagemResultado_Baseline2 = AV59GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                  {
                     AV24ContagemResultado_NaoCnfDmnCod2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_PFBFM") == 0 )
                  {
                     AV132ContagemResultado_PFBFM2 = NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, ".");
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_PFBFS") == 0 )
                  {
                     AV137ContagemResultado_PFBFS2 = NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, ".");
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                  {
                     AV144ContagemResultado_Agrupador2 = AV59GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                  {
                     AV188ContagemResultado_Descricao2 = AV59GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_OWNER") == 0 )
                  {
                     AV194ContagemResultado_Owner2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
                  {
                     AV220ContagemResultado_TemPndHmlg2 = AV59GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                  {
                     AV306ContagemResultado_CntCod2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  if ( AV58GridState.gxTpr_Dynamicfilters.Count >= 3 )
                  {
                     AV48DynamicFiltersEnabled3 = true;
                     AV59GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV58GridState.gxTpr_Dynamicfilters.Item(3));
                     AV51DynamicFiltersSelector3 = AV59GridStateDynamicFilter.gxTpr_Selected;
                     if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 )
                     {
                        AV19ContagemResultado_DataCnt3 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                        AV16ContagemResultado_DataCnt_To3 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                     {
                        AV42ContagemResultado_StatusDmn3 = AV59GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                     {
                        AV209DynamicFiltersOperator3 = AV59GridStateDynamicFilter.gxTpr_Operator;
                        AV29ContagemResultado_OsFsOsFm3 = AV59GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                     {
                        AV285ContagemResultado_DataDmn3 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                        AV280ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                     {
                        AV296ContagemResultado_DataPrevista3 = context.localUtil.CToT( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                        AV291ContagemResultado_DataPrevista_To3 = context.localUtil.CToT( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICO") == 0 )
                     {
                        AV85ContagemResultado_Servico3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                     {
                        AV128ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                     {
                        AV33ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                     {
                        AV12ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
                     {
                        AV95ContagemResultado_ServicoGrupo3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_BASELINE") == 0 )
                     {
                        AV88ContagemResultado_Baseline3 = AV59GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                     {
                        AV25ContagemResultado_NaoCnfDmnCod3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_PFBFM") == 0 )
                     {
                        AV133ContagemResultado_PFBFM3 = NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, ".");
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_PFBFS") == 0 )
                     {
                        AV138ContagemResultado_PFBFS3 = NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, ".");
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                     {
                        AV145ContagemResultado_Agrupador3 = AV59GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                     {
                        AV189ContagemResultado_Descricao3 = AV59GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_OWNER") == 0 )
                     {
                        AV195ContagemResultado_Owner3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
                     {
                        AV221ContagemResultado_TemPndHmlg3 = AV59GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                     {
                        AV307ContagemResultado_CntCod3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     if ( AV58GridState.gxTpr_Dynamicfilters.Count >= 4 )
                     {
                        AV99DynamicFiltersEnabled4 = true;
                        AV59GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV58GridState.gxTpr_Dynamicfilters.Item(4));
                        AV101DynamicFiltersSelector4 = AV59GridStateDynamicFilter.gxTpr_Selected;
                        if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATACNT") == 0 )
                        {
                           AV108ContagemResultado_DataCnt4 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                           AV107ContagemResultado_DataCnt_To4 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                        {
                           AV117ContagemResultado_StatusDmn4 = AV59GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                        {
                           AV210DynamicFiltersOperator4 = AV59GridStateDynamicFilter.gxTpr_Operator;
                           AV112ContagemResultado_OsFsOsFm4 = AV59GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATADMN") == 0 )
                        {
                           AV286ContagemResultado_DataDmn4 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                           AV281ContagemResultado_DataDmn_To4 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                        {
                           AV297ContagemResultado_DataPrevista4 = context.localUtil.CToT( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                           AV292ContagemResultado_DataPrevista_To4 = context.localUtil.CToT( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_SERVICO") == 0 )
                        {
                           AV113ContagemResultado_Servico4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                        {
                           AV129ContagemResultado_ContadorFM4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                        {
                           AV115ContagemResultado_SistemaCod4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                        {
                           AV105ContagemResultado_ContratadaCod4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
                        {
                           AV114ContagemResultado_ServicoGrupo4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_BASELINE") == 0 )
                        {
                           AV103ContagemResultado_Baseline4 = AV59GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                        {
                           AV111ContagemResultado_NaoCnfDmnCod4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_PFBFM") == 0 )
                        {
                           AV134ContagemResultado_PFBFM4 = NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, ".");
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_PFBFS") == 0 )
                        {
                           AV139ContagemResultado_PFBFS4 = NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, ".");
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                        {
                           AV146ContagemResultado_Agrupador4 = AV59GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                        {
                           AV190ContagemResultado_Descricao4 = AV59GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_OWNER") == 0 )
                        {
                           AV196ContagemResultado_Owner4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
                        {
                           AV222ContagemResultado_TemPndHmlg4 = AV59GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV101DynamicFiltersSelector4, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                        {
                           AV308ContagemResultado_CntCod4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        if ( AV58GridState.gxTpr_Dynamicfilters.Count >= 5 )
                        {
                           AV100DynamicFiltersEnabled5 = true;
                           AV59GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV58GridState.gxTpr_Dynamicfilters.Item(5));
                           AV102DynamicFiltersSelector5 = AV59GridStateDynamicFilter.gxTpr_Selected;
                           if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATACNT") == 0 )
                           {
                              AV110ContagemResultado_DataCnt5 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                              AV109ContagemResultado_DataCnt_To5 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                           {
                              AV124ContagemResultado_StatusDmn5 = AV59GridStateDynamicFilter.gxTpr_Value;
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                           {
                              AV211DynamicFiltersOperator5 = AV59GridStateDynamicFilter.gxTpr_Operator;
                              AV119ContagemResultado_OsFsOsFm5 = AV59GridStateDynamicFilter.gxTpr_Value;
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATADMN") == 0 )
                           {
                              AV287ContagemResultado_DataDmn5 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                              AV282ContagemResultado_DataDmn_To5 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                           {
                              AV298ContagemResultado_DataPrevista5 = context.localUtil.CToT( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                              AV293ContagemResultado_DataPrevista_To5 = context.localUtil.CToT( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_SERVICO") == 0 )
                           {
                              AV120ContagemResultado_Servico5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                           {
                              AV130ContagemResultado_ContadorFM5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                           {
                              AV122ContagemResultado_SistemaCod5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                           {
                              AV106ContagemResultado_ContratadaCod5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
                           {
                              AV121ContagemResultado_ServicoGrupo5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_BASELINE") == 0 )
                           {
                              AV104ContagemResultado_Baseline5 = AV59GridStateDynamicFilter.gxTpr_Value;
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                           {
                              AV118ContagemResultado_NaoCnfDmnCod5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_PFBFM") == 0 )
                           {
                              AV135ContagemResultado_PFBFM5 = NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, ".");
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_PFBFS") == 0 )
                           {
                              AV140ContagemResultado_PFBFS5 = NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, ".");
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                           {
                              AV147ContagemResultado_Agrupador5 = AV59GridStateDynamicFilter.gxTpr_Value;
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                           {
                              AV191ContagemResultado_Descricao5 = AV59GridStateDynamicFilter.gxTpr_Value;
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_OWNER") == 0 )
                           {
                              AV197ContagemResultado_Owner5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
                           {
                              AV223ContagemResultado_TemPndHmlg5 = AV59GridStateDynamicFilter.gxTpr_Value;
                           }
                           else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector5, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                           {
                              AV309ContagemResultado_CntCod5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           }
                        }
                     }
                  }
               }
            }
         }
         if ( StringUtil.StrCmp(AV8Acao, "H") == 0 )
         {
            AV151StatusDemanda = "H";
            AV163Subject = "Demandas Homologadas (No reply)";
            AV148StatusLog = "H";
         }
         else if ( StringUtil.StrCmp(AV8Acao, "UH") == 0 )
         {
            AV151StatusDemanda = "R";
            AV163Subject = "Desfeita a homologa��o (No reply)";
            AV148StatusLog = "V";
         }
         else if ( StringUtil.StrCmp(AV8Acao, "O") == 0 )
         {
            AV151StatusDemanda = "O";
            AV163Subject = "Demandas Aceite, Lote " + AV71Lote_Nome + " (No reply)";
            AV148StatusLog = "O";
            AV198PrimeiroID = (int)(AV89Codigos.GetNumeric(1));
            /* Using cursor P00312 */
            pr_default.execute(0, new Object[] {AV198PrimeiroID});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P00312_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00312_n1553ContagemResultado_CntSrvCod[0];
               A456ContagemResultado_Codigo = P00312_A456ContagemResultado_Codigo[0];
               A490ContagemResultado_ContratadaCod = P00312_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00312_n490ContagemResultado_ContratadaCod[0];
               A1603ContagemResultado_CntCod = P00312_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00312_n1603ContagemResultado_CntCod[0];
               A1603ContagemResultado_CntCod = P00312_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00312_n1603ContagemResultado_CntCod[0];
               AV213Contratada_Codigo = A490ContagemResultado_ContratadaCod;
               AV184Contrato_Codigo = A1603ContagemResultado_CntCod;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            /* Using cursor P00315 */
            pr_default.execute(1, new Object[] {AV213Contratada_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A74Contrato_Codigo = P00315_A74Contrato_Codigo[0];
               n74Contrato_Codigo = P00315_n74Contrato_Codigo[0];
               A39Contratada_Codigo = P00315_A39Contratada_Codigo[0];
               A530Contratada_Lote = P00315_A530Contratada_Lote[0];
               n530Contratada_Lote = P00315_n530Contratada_Lote[0];
               A1451Contratada_SS = P00315_A1451Contratada_SS[0];
               n1451Contratada_SS = P00315_n1451Contratada_SS[0];
               A82Contrato_DataVigenciaInicio = P00315_A82Contrato_DataVigenciaInicio[0];
               A81Contrato_Quantidade = P00315_A81Contrato_Quantidade[0];
               A842Contrato_DataInicioTA = P00315_A842Contrato_DataInicioTA[0];
               n842Contrato_DataInicioTA = P00315_n842Contrato_DataInicioTA[0];
               A842Contrato_DataInicioTA = P00315_A842Contrato_DataInicioTA[0];
               n842Contrato_DataInicioTA = P00315_n842Contrato_DataInicioTA[0];
               A530Contratada_Lote = P00315_A530Contratada_Lote[0];
               n530Contratada_Lote = P00315_n530Contratada_Lote[0];
               A1451Contratada_SS = P00315_A1451Contratada_SS[0];
               n1451Contratada_SS = P00315_n1451Contratada_SS[0];
               AV46Contratada_Lote = (short)(A530Contratada_Lote+1);
               AV200Contratada_SS = A1451Contratada_SS;
               if ( P00315_n842Contrato_DataInicioTA[0] )
               {
                  AV98Lote_DataContrato = A82Contrato_DataVigenciaInicio;
               }
               else
               {
                  AV98Lote_DataContrato = A842Contrato_DataInicioTA;
               }
               AV304Contratado = (decimal)(A81Contrato_Quantidade);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            AV64ServerNow = DateTimeUtil.ServerNow( context, "DEFAULT");
            AV77Lote_Numero = StringUtil.Trim( StringUtil.Str( (decimal)(AV46Contratada_Lote), 4, 0)) + StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( AV64ServerNow)), 10, 0)), 2, "0") + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( AV64ServerNow)), 10, 0));
         }
         else if ( StringUtil.StrCmp(AV8Acao, "UO") == 0 )
         {
            AV151StatusDemanda = "H";
            AV163Subject = "Desfeito o Aceite (No reply)";
            AV148StatusLog = "H";
         }
         else if ( StringUtil.StrCmp(AV8Acao, "P") == 0 )
         {
            AV151StatusDemanda = "P";
            AV163Subject = "Demandas Faturadas, Lote " + AV71Lote_Nome + " (No reply)";
            AV148StatusLog = "P";
         }
         else if ( StringUtil.StrCmp(AV8Acao, "UP") == 0 )
         {
            AV151StatusDemanda = "O";
            AV163Subject = "Desfeito o Faturamento (No reply)";
            AV148StatusLog = "O";
         }
         else if ( StringUtil.StrCmp(AV8Acao, "L") == 0 )
         {
            AV151StatusDemanda = "L";
            AV163Subject = "Demandas Liquidadas, Lote " + AV71Lote_Nome + " (No reply)";
            AV148StatusLog = "Q";
         }
         else if ( StringUtil.StrCmp(AV8Acao, "UL") == 0 )
         {
            AV151StatusDemanda = "P";
            AV163Subject = "Desfeita a Liquida��o (No reply)";
            AV148StatusLog = "P";
         }
         if ( StringUtil.StrCmp(AV8Acao, "O") == 0 )
         {
            GXt_boolean1 = AV217Contratante_SSAutomatica;
            new prc_ssautomatica(context ).execute( ref  AV45Contratada_AreaTrabalhoCod, out  GXt_boolean1) ;
            AV217Contratante_SSAutomatica = GXt_boolean1;
            AV64ServerNow = DateTimeUtil.ServerNow( context, "DEFAULT");
            GXt_decimal2 = AV303Usado;
            new prc_usadodocontrato(context ).execute( ref  AV184Contrato_Codigo, out  GXt_decimal2) ;
            AV303Usado = GXt_decimal2;
            /*
               INSERT RECORD ON TABLE Lote

            */
            A562Lote_Numero = AV77Lote_Numero;
            A595Lote_AreaTrabalhoCod = AV45Contratada_AreaTrabalhoCod;
            A563Lote_Nome = AV71Lote_Nome;
            A564Lote_Data = AV64ServerNow;
            A559Lote_UserCod = AV66WWPContext.gxTpr_Userid;
            A565Lote_ValorPF = 0;
            A844Lote_DataContrato = AV98Lote_DataContrato;
            n844Lote_DataContrato = false;
            A2055Lote_CntUsado = AV303Usado;
            n2055Lote_CntUsado = false;
            A2056Lote_CntSaldo = (decimal)(AV304Contratado-AV303Usado);
            n2056Lote_CntSaldo = false;
            A2088Lote_ParecerFinal = AV90WebSession.Get("ParecerFinal");
            n2088Lote_ParecerFinal = false;
            A2087Lote_Comentarios = AV90WebSession.Get("Comentarios");
            n2087Lote_Comentarios = false;
            /* Using cursor P00316 */
            pr_default.execute(2, new Object[] {A562Lote_Numero, A595Lote_AreaTrabalhoCod, A563Lote_Nome, A564Lote_Data, A559Lote_UserCod, A565Lote_ValorPF, n844Lote_DataContrato, A844Lote_DataContrato, n2055Lote_CntUsado, A2055Lote_CntUsado, n2056Lote_CntSaldo, A2056Lote_CntSaldo, n2088Lote_ParecerFinal, A2088Lote_ParecerFinal, n2087Lote_Comentarios, A2087Lote_Comentarios});
            A596Lote_Codigo = P00316_A596Lote_Codigo[0];
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("Lote") ;
            if ( (pr_default.getStatus(2) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
            AV21ContagemResultado_LoteAceite = A596Lote_Codigo;
            AV90WebSession.Remove("ParecerFinal");
            AV90WebSession.Remove("Comentarios");
            /* Execute user subroutine: 'ACEITE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Using cursor P00317 */
            pr_default.execute(3, new Object[] {AV21ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A597ContagemResultado_LoteAceiteCod = P00317_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P00317_n597ContagemResultado_LoteAceiteCod[0];
               A456ContagemResultado_Codigo = P00317_A456ContagemResultado_Codigo[0];
               AV214Ok = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(3);
            }
            pr_default.close(3);
            if ( AV214Ok )
            {
               /* Using cursor P00318 */
               pr_default.execute(4, new Object[] {AV213Contratada_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A39Contratada_Codigo = P00318_A39Contratada_Codigo[0];
                  A1451Contratada_SS = P00318_A1451Contratada_SS[0];
                  n1451Contratada_SS = P00318_n1451Contratada_SS[0];
                  A530Contratada_Lote = P00318_A530Contratada_Lote[0];
                  n530Contratada_Lote = P00318_n530Contratada_Lote[0];
                  A1451Contratada_SS = AV200Contratada_SS;
                  n1451Contratada_SS = false;
                  A530Contratada_Lote = (short)(A530Contratada_Lote+1);
                  n530Contratada_Lote = false;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  /* Using cursor P00319 */
                  pr_default.execute(5, new Object[] {n1451Contratada_SS, A1451Contratada_SS, n530Contratada_Lote, A530Contratada_Lote, A39Contratada_Codigo});
                  pr_default.close(5);
                  dsDefault.SmartCacheProvider.SetUpdated("Contratada") ;
                  if (true) break;
                  /* Using cursor P003110 */
                  pr_default.execute(6, new Object[] {n1451Contratada_SS, A1451Contratada_SS, n530Contratada_Lote, A530Contratada_Lote, A39Contratada_Codigo});
                  pr_default.close(6);
                  dsDefault.SmartCacheProvider.SetUpdated("Contratada") ;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(4);
               AV150StatusAnterior = "H";
               AV151StatusDemanda = "O";
               new prc_newanexolote(context ).execute( ref  AV21ContagemResultado_LoteAceite) ;
               AV90WebSession.Remove("SdtCodigos");
               AV90WebSession.Remove("ArquivosEvd");
            }
            else
            {
               /* Using cursor P003111 */
               pr_default.execute(7, new Object[] {AV21ContagemResultado_LoteAceite});
               while ( (pr_default.getStatus(7) != 101) )
               {
                  A596Lote_Codigo = P003111_A596Lote_Codigo[0];
                  /* Using cursor P003112 */
                  pr_default.execute(8, new Object[] {A596Lote_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("Lote") ;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(7);
            }
         }
         else
         {
            /* Execute user subroutine: 'ALTERARSTATUS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV319GXV1 = 1;
         while ( AV319GXV1 <= AV89Codigos.Count )
         {
            AV149Codigo = (int)(AV89Codigos.GetNumeric(AV319GXV1));
            GXt_dtime3 = DateTimeUtil.ResetTime( DateTime.MinValue ) ;
            new prc_inslogresponsavel(context ).execute( ref  AV149Codigo,  0,  AV148StatusLog,  "D",  AV66WWPContext.gxTpr_Userid,  0,  AV150StatusAnterior,  AV151StatusDemanda,  "",  GXt_dtime3,  false) ;
            AV319GXV1 = (int)(AV319GXV1+1);
         }
         /* Execute user subroutine: 'ENVIARNOTIFICACAO' */
         S141 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(AV8Acao, "O") == 0 )
         {
            new prc_aceiteenviaremaillote(context ).execute(  AV21ContagemResultado_LoteAceite,  AV216ContagemResultado_VlrAceite,  AV91QtdeSelecionadas) ;
         }
         this.cleanup();
         if (true) return;
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'ACEITE' Routine */
         pr_default.dynParam(9, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV89Codigos },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor P003113 */
         pr_default.execute(9);
         while ( (pr_default.getStatus(9) != 101) )
         {
            A484ContagemResultado_StatusDmn = P003113_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P003113_n484ContagemResultado_StatusDmn[0];
            A597ContagemResultado_LoteAceiteCod = P003113_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P003113_n597ContagemResultado_LoteAceiteCod[0];
            A1452ContagemResultado_SS = P003113_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = P003113_n1452ContagemResultado_SS[0];
            A1051ContagemResultado_GlsValor = P003113_A1051ContagemResultado_GlsValor[0];
            n1051ContagemResultado_GlsValor = P003113_n1051ContagemResultado_GlsValor[0];
            A1559ContagemResultado_VlrAceite = P003113_A1559ContagemResultado_VlrAceite[0];
            n1559ContagemResultado_VlrAceite = P003113_n1559ContagemResultado_VlrAceite[0];
            A457ContagemResultado_Demanda = P003113_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P003113_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P003113_A456ContagemResultado_Codigo[0];
            A512ContagemResultado_ValorPF = P003113_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P003113_n512ContagemResultado_ValorPF[0];
            GXt_decimal2 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal2) ;
            A574ContagemResultado_PFFinal = GXt_decimal2;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
            A484ContagemResultado_StatusDmn = AV151StatusDemanda;
            n484ContagemResultado_StatusDmn = false;
            A597ContagemResultado_LoteAceiteCod = AV21ContagemResultado_LoteAceite;
            n597ContagemResultado_LoteAceiteCod = false;
            if ( P003113_n1452ContagemResultado_SS[0] )
            {
               A1452ContagemResultado_SS = 0;
               n1452ContagemResultado_SS = false;
            }
            AV216ContagemResultado_VlrAceite = 0;
            /* Using cursor P003114 */
            pr_default.execute(10, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(10) != 101) )
            {
               A1314ContagemResultadoIndicadores_DemandaCod = P003114_A1314ContagemResultadoIndicadores_DemandaCod[0];
               A1323ContagemResultadoIndicadores_Valor = P003114_A1323ContagemResultadoIndicadores_Valor[0];
               A1316ContagemResultadoIndicadores_LoteCod = P003114_A1316ContagemResultadoIndicadores_LoteCod[0];
               n1316ContagemResultadoIndicadores_LoteCod = P003114_n1316ContagemResultadoIndicadores_LoteCod[0];
               A1315ContagemResultadoIndicadores_IndicadorCod = P003114_A1315ContagemResultadoIndicadores_IndicadorCod[0];
               AV216ContagemResultado_VlrAceite = (decimal)(AV216ContagemResultado_VlrAceite+A1323ContagemResultadoIndicadores_Valor);
               A1316ContagemResultadoIndicadores_LoteCod = AV21ContagemResultado_LoteAceite;
               n1316ContagemResultadoIndicadores_LoteCod = false;
               /* Using cursor P003115 */
               pr_default.execute(11, new Object[] {n1316ContagemResultadoIndicadores_LoteCod, A1316ContagemResultadoIndicadores_LoteCod, A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod});
               pr_default.close(11);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoIndicadores") ;
               pr_default.readNext(10);
            }
            pr_default.close(10);
            A1559ContagemResultado_VlrAceite = (decimal)(A606ContagemResultado_ValorFinal-A1051ContagemResultado_GlsValor-AV216ContagemResultado_VlrAceite);
            n1559ContagemResultado_VlrAceite = false;
            AV157Demandas = AV157Demandas + StringUtil.Trim( A457ContagemResultado_Demanda) + ", ";
            AV91QtdeSelecionadas = (short)(AV91QtdeSelecionadas+1);
            BatchSize = 100;
            pr_default.initializeBatch( 12, BatchSize, this, "Executebatchp003116");
            /* Using cursor P003116 */
            pr_default.addRecord(12, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod, n1452ContagemResultado_SS, A1452ContagemResultado_SS, n1559ContagemResultado_VlrAceite, A1559ContagemResultado_VlrAceite, A456ContagemResultado_Codigo});
            if ( pr_default.recordCount(12) == pr_default.getBatchSize(12) )
            {
               Executebatchp003116( ) ;
            }
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            pr_default.readNext(9);
         }
         if ( pr_default.getBatchSize(12) > 0 )
         {
            Executebatchp003116( ) ;
         }
         pr_default.close(9);
         if ( AV217Contratante_SSAutomatica )
         {
            new prc_renumerass(context ).execute( ref  AV21ContagemResultado_LoteAceite, ref  AV200Contratada_SS) ;
         }
         if ( AV89Codigos.Count == 1 )
         {
            AV163Subject = StringUtil.StringReplace( AV163Subject, "Demandas A", "OS "+StringUtil.StringReplace( AV157Demandas, ",", " A"));
         }
      }

      protected void S121( )
      {
         /* 'ALTERARSTATUS' Routine */
         if ( AV89Codigos.Count == 0 )
         {
            AV324ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod = AV45Contratada_AreaTrabalhoCod;
            AV325ExtraWWContagemResultadoExtraSelectionDS_2_Contagemresultado_statuscnt = AV38ContagemResultado_StatusCnt;
            AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = AV49DynamicFiltersSelector1;
            AV327ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 = AV207DynamicFiltersOperator1;
            AV328ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 = AV17ContagemResultado_DataCnt1;
            AV329ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 = AV14ContagemResultado_DataCnt_To1;
            AV330ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 = AV40ContagemResultado_StatusDmn1;
            AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = AV27ContagemResultado_OsFsOsFm1;
            AV332ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 = AV283ContagemResultado_DataDmn1;
            AV333ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 = AV278ContagemResultado_DataDmn_To1;
            AV334ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 = DateTimeUtil.ResetTime(AV294ContagemResultado_DataPrevista1);
            AV335ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 = DateTimeUtil.ResetTime(AV289ContagemResultado_DataPrevista_To1);
            AV336ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 = AV82ContagemResultado_Servico1;
            AV337ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 = AV126ContagemResultado_ContadorFM1;
            AV338ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 = AV31ContagemResultado_SistemaCod1;
            AV339ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 = AV10ContagemResultado_ContratadaCod1;
            AV340ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 = AV93ContagemResultado_ServicoGrupo1;
            AV341ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 = AV86ContagemResultado_Baseline1;
            AV342ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 = AV23ContagemResultado_NaoCnfDmnCod1;
            AV343ExtraWWContagemResultadoExtraSelectionDS_20_Contagemresultado_pfbfm1 = AV131ContagemResultado_PFBFM1;
            AV344ExtraWWContagemResultadoExtraSelectionDS_21_Contagemresultado_pfbfs1 = AV136ContagemResultado_PFBFS1;
            AV345ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 = AV143ContagemResultado_Agrupador1;
            AV346ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = AV187ContagemResultado_Descricao1;
            AV347ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 = AV193ContagemResultado_Owner1;
            AV348ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 = AV219ContagemResultado_TemPndHmlg1;
            AV349ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 = AV305ContagemResultado_CntCod1;
            AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
            AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = AV50DynamicFiltersSelector2;
            AV352ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 = AV208DynamicFiltersOperator2;
            AV353ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 = AV18ContagemResultado_DataCnt2;
            AV354ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 = AV15ContagemResultado_DataCnt_To2;
            AV355ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 = AV41ContagemResultado_StatusDmn2;
            AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = AV28ContagemResultado_OsFsOsFm2;
            AV357ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 = AV284ContagemResultado_DataDmn2;
            AV358ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 = AV279ContagemResultado_DataDmn_To2;
            AV359ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 = DateTimeUtil.ResetTime(AV295ContagemResultado_DataPrevista2);
            AV360ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 = DateTimeUtil.ResetTime(AV290ContagemResultado_DataPrevista_To2);
            AV361ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 = AV84ContagemResultado_Servico2;
            AV362ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 = AV127ContagemResultado_ContadorFM2;
            AV363ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 = AV32ContagemResultado_SistemaCod2;
            AV364ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 = AV11ContagemResultado_ContratadaCod2;
            AV365ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 = AV94ContagemResultado_ServicoGrupo2;
            AV366ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 = AV87ContagemResultado_Baseline2;
            AV367ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 = AV24ContagemResultado_NaoCnfDmnCod2;
            AV368ExtraWWContagemResultadoExtraSelectionDS_45_Contagemresultado_pfbfm2 = AV132ContagemResultado_PFBFM2;
            AV369ExtraWWContagemResultadoExtraSelectionDS_46_Contagemresultado_pfbfs2 = AV137ContagemResultado_PFBFS2;
            AV370ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 = AV144ContagemResultado_Agrupador2;
            AV371ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = AV188ContagemResultado_Descricao2;
            AV372ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 = AV194ContagemResultado_Owner2;
            AV373ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 = AV220ContagemResultado_TemPndHmlg2;
            AV374ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 = AV306ContagemResultado_CntCod2;
            AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = AV48DynamicFiltersEnabled3;
            AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = AV51DynamicFiltersSelector3;
            AV377ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 = AV209DynamicFiltersOperator3;
            AV378ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 = AV19ContagemResultado_DataCnt3;
            AV379ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 = AV16ContagemResultado_DataCnt_To3;
            AV380ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 = AV42ContagemResultado_StatusDmn3;
            AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = AV29ContagemResultado_OsFsOsFm3;
            AV382ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 = AV285ContagemResultado_DataDmn3;
            AV383ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 = AV280ContagemResultado_DataDmn_To3;
            AV384ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 = DateTimeUtil.ResetTime(AV296ContagemResultado_DataPrevista3);
            AV385ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 = DateTimeUtil.ResetTime(AV291ContagemResultado_DataPrevista_To3);
            AV386ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 = AV85ContagemResultado_Servico3;
            AV387ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 = AV128ContagemResultado_ContadorFM3;
            AV388ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 = AV33ContagemResultado_SistemaCod3;
            AV389ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 = AV12ContagemResultado_ContratadaCod3;
            AV390ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 = AV95ContagemResultado_ServicoGrupo3;
            AV391ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 = AV88ContagemResultado_Baseline3;
            AV392ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 = AV25ContagemResultado_NaoCnfDmnCod3;
            AV393ExtraWWContagemResultadoExtraSelectionDS_70_Contagemresultado_pfbfm3 = AV133ContagemResultado_PFBFM3;
            AV394ExtraWWContagemResultadoExtraSelectionDS_71_Contagemresultado_pfbfs3 = AV138ContagemResultado_PFBFS3;
            AV395ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 = AV145ContagemResultado_Agrupador3;
            AV396ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = AV189ContagemResultado_Descricao3;
            AV397ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 = AV195ContagemResultado_Owner3;
            AV398ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 = AV221ContagemResultado_TemPndHmlg3;
            AV399ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 = AV307ContagemResultado_CntCod3;
            AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = AV99DynamicFiltersEnabled4;
            AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = AV101DynamicFiltersSelector4;
            AV402ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 = AV210DynamicFiltersOperator4;
            AV403ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 = AV108ContagemResultado_DataCnt4;
            AV404ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 = AV107ContagemResultado_DataCnt_To4;
            AV405ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 = AV117ContagemResultado_StatusDmn4;
            AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = AV112ContagemResultado_OsFsOsFm4;
            AV407ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 = AV286ContagemResultado_DataDmn4;
            AV408ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 = AV281ContagemResultado_DataDmn_To4;
            AV409ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 = DateTimeUtil.ResetTime(AV297ContagemResultado_DataPrevista4);
            AV410ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 = DateTimeUtil.ResetTime(AV292ContagemResultado_DataPrevista_To4);
            AV411ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 = AV113ContagemResultado_Servico4;
            AV412ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 = AV129ContagemResultado_ContadorFM4;
            AV413ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 = AV115ContagemResultado_SistemaCod4;
            AV414ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 = AV105ContagemResultado_ContratadaCod4;
            AV415ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 = AV114ContagemResultado_ServicoGrupo4;
            AV416ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 = AV103ContagemResultado_Baseline4;
            AV417ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 = AV111ContagemResultado_NaoCnfDmnCod4;
            AV418ExtraWWContagemResultadoExtraSelectionDS_95_Contagemresultado_pfbfm4 = AV134ContagemResultado_PFBFM4;
            AV419ExtraWWContagemResultadoExtraSelectionDS_96_Contagemresultado_pfbfs4 = AV139ContagemResultado_PFBFS4;
            AV420ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 = AV146ContagemResultado_Agrupador4;
            AV421ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = AV190ContagemResultado_Descricao4;
            AV422ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 = AV196ContagemResultado_Owner4;
            AV423ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 = AV222ContagemResultado_TemPndHmlg4;
            AV424ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 = AV308ContagemResultado_CntCod4;
            AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = AV100DynamicFiltersEnabled5;
            AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = AV102DynamicFiltersSelector5;
            AV427ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 = AV211DynamicFiltersOperator5;
            AV428ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 = AV110ContagemResultado_DataCnt5;
            AV429ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 = AV109ContagemResultado_DataCnt_To5;
            AV430ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 = AV124ContagemResultado_StatusDmn5;
            AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = AV119ContagemResultado_OsFsOsFm5;
            AV432ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 = AV287ContagemResultado_DataDmn5;
            AV433ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 = AV282ContagemResultado_DataDmn_To5;
            AV434ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 = DateTimeUtil.ResetTime(AV298ContagemResultado_DataPrevista5);
            AV435ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 = DateTimeUtil.ResetTime(AV293ContagemResultado_DataPrevista_To5);
            AV436ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 = AV120ContagemResultado_Servico5;
            AV437ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 = AV130ContagemResultado_ContadorFM5;
            AV438ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 = AV122ContagemResultado_SistemaCod5;
            AV439ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 = AV106ContagemResultado_ContratadaCod5;
            AV440ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 = AV121ContagemResultado_ServicoGrupo5;
            AV441ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 = AV104ContagemResultado_Baseline5;
            AV442ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 = AV118ContagemResultado_NaoCnfDmnCod5;
            AV443ExtraWWContagemResultadoExtraSelectionDS_120_Contagemresultado_pfbfm5 = AV135ContagemResultado_PFBFM5;
            AV444ExtraWWContagemResultadoExtraSelectionDS_121_Contagemresultado_pfbfs5 = AV140ContagemResultado_PFBFS5;
            AV445ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 = AV147ContagemResultado_Agrupador5;
            AV446ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = AV191ContagemResultado_Descricao5;
            AV447ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 = AV197ContagemResultado_Owner5;
            AV448ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 = AV223ContagemResultado_TemPndHmlg5;
            AV449ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 = AV309ContagemResultado_CntCod5;
            AV450ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = AV224TFContagemResultado_Agrupador;
            AV451ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel = AV225TFContagemResultado_Agrupador_Sel;
            AV452ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = AV233TFContagemResultado_DemandaFM;
            AV453ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel = AV234TFContagemResultado_DemandaFM_Sel;
            AV454ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = AV231TFContagemResultado_Demanda;
            AV455ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel = AV232TFContagemResultado_Demanda_Sel;
            AV456ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = AV235TFContagemResultado_Descricao;
            AV457ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel = AV236TFContagemResultado_Descricao_Sel;
            AV458ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista = AV301TFContagemResultado_DataPrevista;
            AV459ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to = AV302TFContagemResultado_DataPrevista_To;
            AV460ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn = AV299TFContagemResultado_DataDmn;
            AV461ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to = AV300TFContagemResultado_DataDmn_To;
            AV462ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt = AV229TFContagemResultado_DataUltCnt;
            AV463ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to = AV230TFContagemResultado_DataUltCnt_To;
            AV464ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = AV227TFContagemResultado_ContratadaSigla;
            AV465ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel = AV228TFContagemResultado_ContratadaSigla_Sel;
            AV466ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = AV241TFContagemResultado_SistemaCoord;
            AV467ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel = AV242TFContagemResultado_SistemaCoord_Sel;
            AV468ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels = AV245TFContagemResultado_StatusDmn_Sels;
            AV469ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel = AV226TFContagemResultado_Baseline_Sel;
            AV470ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = AV239TFContagemResultado_ServicoSigla;
            AV471ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel = AV240TFContagemResultado_ServicoSigla_Sel;
            AV472ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal = AV237TFContagemResultado_PFFinal;
            AV473ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to = AV238TFContagemResultado_PFFinal_To;
            AV474ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf = AV247TFContagemResultado_ValorPF;
            AV475ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to = AV248TFContagemResultado_ValorPF_To;
            pr_default.dynParam(13, new Object[]{ new Object[]{
                                                 A484ContagemResultado_StatusDmn ,
                                                 AV468ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels ,
                                                 AV66WWPContext.gxTpr_Contratada_codigo ,
                                                 AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 ,
                                                 AV330ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 ,
                                                 AV327ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 ,
                                                 AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ,
                                                 AV332ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 ,
                                                 AV333ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 ,
                                                 AV334ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 ,
                                                 AV335ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 ,
                                                 AV336ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 ,
                                                 AV338ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 ,
                                                 AV339ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 ,
                                                 AV324ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod ,
                                                 AV340ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 ,
                                                 AV341ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 ,
                                                 AV342ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 ,
                                                 AV345ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 ,
                                                 AV346ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ,
                                                 AV347ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 ,
                                                 AV349ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 ,
                                                 AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 ,
                                                 AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 ,
                                                 AV355ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 ,
                                                 AV352ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 ,
                                                 AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ,
                                                 AV357ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 ,
                                                 AV358ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 ,
                                                 AV359ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 ,
                                                 AV360ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 ,
                                                 AV361ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 ,
                                                 AV363ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 ,
                                                 AV364ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 ,
                                                 AV365ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 ,
                                                 AV366ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 ,
                                                 AV367ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 ,
                                                 AV370ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 ,
                                                 AV371ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ,
                                                 AV372ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 ,
                                                 AV374ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 ,
                                                 AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 ,
                                                 AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 ,
                                                 AV380ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 ,
                                                 AV377ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 ,
                                                 AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ,
                                                 AV382ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 ,
                                                 AV383ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 ,
                                                 AV384ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 ,
                                                 AV385ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 ,
                                                 AV386ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 ,
                                                 AV388ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 ,
                                                 AV389ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 ,
                                                 AV390ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 ,
                                                 AV391ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 ,
                                                 AV392ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 ,
                                                 AV395ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 ,
                                                 AV396ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ,
                                                 AV397ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 ,
                                                 AV399ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 ,
                                                 AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 ,
                                                 AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 ,
                                                 AV405ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 ,
                                                 AV402ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 ,
                                                 AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ,
                                                 AV407ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 ,
                                                 AV408ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 ,
                                                 AV409ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 ,
                                                 AV410ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 ,
                                                 AV411ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 ,
                                                 AV413ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 ,
                                                 AV414ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 ,
                                                 AV415ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 ,
                                                 AV416ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 ,
                                                 AV417ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 ,
                                                 AV420ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 ,
                                                 AV421ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ,
                                                 AV422ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 ,
                                                 AV424ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 ,
                                                 AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 ,
                                                 AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 ,
                                                 AV430ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 ,
                                                 AV427ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 ,
                                                 AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ,
                                                 AV432ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 ,
                                                 AV433ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 ,
                                                 AV434ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 ,
                                                 AV435ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 ,
                                                 AV436ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 ,
                                                 AV438ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 ,
                                                 AV439ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 ,
                                                 AV440ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 ,
                                                 AV441ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 ,
                                                 AV442ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 ,
                                                 AV445ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 ,
                                                 AV446ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ,
                                                 AV447ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 ,
                                                 AV449ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 ,
                                                 AV451ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel ,
                                                 AV450ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ,
                                                 AV453ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel ,
                                                 AV452ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ,
                                                 AV455ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel ,
                                                 AV454ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ,
                                                 AV457ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel ,
                                                 AV456ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ,
                                                 AV458ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista ,
                                                 AV459ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to ,
                                                 AV460ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn ,
                                                 AV461ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to ,
                                                 AV465ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel ,
                                                 AV464ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ,
                                                 AV467ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel ,
                                                 AV466ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ,
                                                 AV468ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels.Count ,
                                                 AV469ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel ,
                                                 AV471ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel ,
                                                 AV470ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ,
                                                 AV474ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf ,
                                                 AV475ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to ,
                                                 A490ContagemResultado_ContratadaCod ,
                                                 A457ContagemResultado_Demanda ,
                                                 A493ContagemResultado_DemandaFM ,
                                                 A471ContagemResultado_DataDmn ,
                                                 A1351ContagemResultado_DataPrevista ,
                                                 A601ContagemResultado_Servico ,
                                                 A489ContagemResultado_SistemaCod ,
                                                 A764ContagemResultado_ServicoGrupo ,
                                                 A598ContagemResultado_Baseline ,
                                                 A468ContagemResultado_NaoCnfDmnCod ,
                                                 A1046ContagemResultado_Agrupador ,
                                                 A494ContagemResultado_Descricao ,
                                                 A508ContagemResultado_Owner ,
                                                 A1603ContagemResultado_CntCod ,
                                                 A803ContagemResultado_ContratadaSigla ,
                                                 A515ContagemResultado_SistemaCoord ,
                                                 A801ContagemResultado_ServicoSigla ,
                                                 A512ContagemResultado_ValorPF ,
                                                 A531ContagemResultado_StatusUltCnt ,
                                                 A1854ContagemResultado_VlrCnc ,
                                                 AV328ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 ,
                                                 A566ContagemResultado_DataUltCnt ,
                                                 AV329ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 ,
                                                 AV337ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 ,
                                                 A584ContagemResultado_ContadorFM ,
                                                 A684ContagemResultado_PFBFSUltima ,
                                                 A682ContagemResultado_PFBFMUltima ,
                                                 A685ContagemResultado_PFLFSUltima ,
                                                 A683ContagemResultado_PFLFMUltima ,
                                                 AV348ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 ,
                                                 A1802ContagemResultado_TemPndHmlg ,
                                                 AV353ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 ,
                                                 AV354ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 ,
                                                 AV362ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 ,
                                                 AV373ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 ,
                                                 AV378ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 ,
                                                 AV379ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 ,
                                                 AV387ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 ,
                                                 AV398ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 ,
                                                 AV403ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 ,
                                                 AV404ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 ,
                                                 AV412ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 ,
                                                 AV423ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 ,
                                                 AV428ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 ,
                                                 AV429ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 ,
                                                 AV437ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 ,
                                                 AV448ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 ,
                                                 AV462ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt ,
                                                 AV463ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to ,
                                                 AV472ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ,
                                                 A574ContagemResultado_PFFinal ,
                                                 AV473ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ,
                                                 A517ContagemResultado_Ultima ,
                                                 A52Contratada_AreaTrabalhoCod ,
                                                 AV66WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
            lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
            lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
            lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
            lV346ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV346ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1), "%", "");
            lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
            lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
            lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
            lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
            lV371ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV371ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2), "%", "");
            lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
            lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
            lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
            lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
            lV396ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV396ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3), "%", "");
            lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
            lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
            lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
            lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
            lV421ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = StringUtil.Concat( StringUtil.RTrim( AV421ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4), "%", "");
            lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
            lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
            lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
            lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
            lV446ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = StringUtil.Concat( StringUtil.RTrim( AV446ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5), "%", "");
            lV450ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = StringUtil.PadR( StringUtil.RTrim( AV450ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador), 15, "%");
            lV452ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV452ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm), "%", "");
            lV454ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = StringUtil.Concat( StringUtil.RTrim( AV454ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda), "%", "");
            lV456ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = StringUtil.Concat( StringUtil.RTrim( AV456ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao), "%", "");
            lV464ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV464ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla), 15, "%");
            lV466ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = StringUtil.Concat( StringUtil.RTrim( AV466ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord), "%", "");
            lV470ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV470ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla), 15, "%");
            /* Using cursor P003118 */
            pr_default.execute(13, new Object[] {AV330ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, AV355ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, AV380ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, AV405ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, AV430ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV328ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1, AV328ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1, AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV329ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1, AV329ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1, AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV337ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1, AV337ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1, AV337ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1, AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV353ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2, AV353ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2, AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV354ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2, AV354ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2, AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV362ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2, AV362ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2, AV362ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2, AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV378ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3, AV378ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3, AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV379ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3, AV379ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3, AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV387ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3, AV387ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3, AV387ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3, AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV403ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4, AV403ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4, AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV404ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4, AV404ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4, AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV412ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4, AV412ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4, AV412ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4, AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV428ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5, AV428ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5, AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV429ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5, AV429ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5, AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV437ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5, AV437ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5, AV437ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5, AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV462ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt, AV462ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt, AV463ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to, AV463ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to, AV66WWPContext.gxTpr_Areatrabalho_codigo, AV66WWPContext.gxTpr_Contratada_codigo, AV330ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, AV332ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1, AV333ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1, AV334ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1, AV335ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1, AV336ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1, AV338ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1, AV339ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1, AV340ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1, AV342ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1, AV345ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1, lV346ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1, AV347ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1, AV349ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1, AV355ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, AV357ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2, AV358ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2, AV359ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2, AV360ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2,
            AV361ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2, AV363ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2, AV364ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2, AV365ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2, AV367ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2, AV370ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2, lV371ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2, AV372ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2, AV374ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2, AV380ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, AV382ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3, AV383ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3, AV384ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3, AV385ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3, AV386ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3, AV388ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3, AV389ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3, AV390ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3, AV392ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3, AV395ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3, lV396ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3, AV397ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3, AV399ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3, AV405ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, AV407ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4, AV408ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4, AV409ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4, AV410ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4, AV411ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4, AV413ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4, AV414ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4, AV415ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4, AV417ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4, AV420ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4, lV421ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4, AV422ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4, AV424ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4, AV430ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, AV432ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5, AV433ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5, AV434ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5, AV435ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5, AV436ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5, AV438ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5, AV439ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5, AV440ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5, AV442ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5, AV445ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5, lV446ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5, AV447ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5, AV449ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5, lV450ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador, AV451ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel, lV452ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm, AV453ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel, lV454ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda, AV455ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel, lV456ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao, AV457ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel, AV458ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista, AV459ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to, AV460ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn, AV461ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to, lV464ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla, AV465ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel, lV466ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord, AV467ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel, lV470ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla, AV471ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel, AV474ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf, AV475ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to});
            while ( (pr_default.getStatus(13) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P003118_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P003118_n1553ContagemResultado_CntSrvCod[0];
               A517ContagemResultado_Ultima = P003118_A517ContagemResultado_Ultima[0];
               A512ContagemResultado_ValorPF = P003118_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P003118_n512ContagemResultado_ValorPF[0];
               A801ContagemResultado_ServicoSigla = P003118_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = P003118_n801ContagemResultado_ServicoSigla[0];
               A515ContagemResultado_SistemaCoord = P003118_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P003118_n515ContagemResultado_SistemaCoord[0];
               A803ContagemResultado_ContratadaSigla = P003118_A803ContagemResultado_ContratadaSigla[0];
               n803ContagemResultado_ContratadaSigla = P003118_n803ContagemResultado_ContratadaSigla[0];
               A1603ContagemResultado_CntCod = P003118_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P003118_n1603ContagemResultado_CntCod[0];
               A494ContagemResultado_Descricao = P003118_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = P003118_n494ContagemResultado_Descricao[0];
               A1046ContagemResultado_Agrupador = P003118_A1046ContagemResultado_Agrupador[0];
               n1046ContagemResultado_Agrupador = P003118_n1046ContagemResultado_Agrupador[0];
               A468ContagemResultado_NaoCnfDmnCod = P003118_A468ContagemResultado_NaoCnfDmnCod[0];
               n468ContagemResultado_NaoCnfDmnCod = P003118_n468ContagemResultado_NaoCnfDmnCod[0];
               A598ContagemResultado_Baseline = P003118_A598ContagemResultado_Baseline[0];
               n598ContagemResultado_Baseline = P003118_n598ContagemResultado_Baseline[0];
               A764ContagemResultado_ServicoGrupo = P003118_A764ContagemResultado_ServicoGrupo[0];
               n764ContagemResultado_ServicoGrupo = P003118_n764ContagemResultado_ServicoGrupo[0];
               A489ContagemResultado_SistemaCod = P003118_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P003118_n489ContagemResultado_SistemaCod[0];
               A508ContagemResultado_Owner = P003118_A508ContagemResultado_Owner[0];
               A601ContagemResultado_Servico = P003118_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P003118_n601ContagemResultado_Servico[0];
               A1351ContagemResultado_DataPrevista = P003118_A1351ContagemResultado_DataPrevista[0];
               n1351ContagemResultado_DataPrevista = P003118_n1351ContagemResultado_DataPrevista[0];
               A471ContagemResultado_DataDmn = P003118_A471ContagemResultado_DataDmn[0];
               A493ContagemResultado_DemandaFM = P003118_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P003118_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = P003118_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P003118_n457ContagemResultado_Demanda[0];
               A484ContagemResultado_StatusDmn = P003118_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P003118_n484ContagemResultado_StatusDmn[0];
               A1854ContagemResultado_VlrCnc = P003118_A1854ContagemResultado_VlrCnc[0];
               n1854ContagemResultado_VlrCnc = P003118_n1854ContagemResultado_VlrCnc[0];
               A490ContagemResultado_ContratadaCod = P003118_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P003118_n490ContagemResultado_ContratadaCod[0];
               A52Contratada_AreaTrabalhoCod = P003118_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P003118_n52Contratada_AreaTrabalhoCod[0];
               A1348ContagemResultado_DataHomologacao = P003118_A1348ContagemResultado_DataHomologacao[0];
               n1348ContagemResultado_DataHomologacao = P003118_n1348ContagemResultado_DataHomologacao[0];
               A483ContagemResultado_StatusCnt = P003118_A483ContagemResultado_StatusCnt[0];
               A683ContagemResultado_PFLFMUltima = P003118_A683ContagemResultado_PFLFMUltima[0];
               A685ContagemResultado_PFLFSUltima = P003118_A685ContagemResultado_PFLFSUltima[0];
               A682ContagemResultado_PFBFMUltima = P003118_A682ContagemResultado_PFBFMUltima[0];
               A684ContagemResultado_PFBFSUltima = P003118_A684ContagemResultado_PFBFSUltima[0];
               A584ContagemResultado_ContadorFM = P003118_A584ContagemResultado_ContadorFM[0];
               A566ContagemResultado_DataUltCnt = P003118_A566ContagemResultado_DataUltCnt[0];
               A531ContagemResultado_StatusUltCnt = P003118_A531ContagemResultado_StatusUltCnt[0];
               A456ContagemResultado_Codigo = P003118_A456ContagemResultado_Codigo[0];
               A473ContagemResultado_DataCnt = P003118_A473ContagemResultado_DataCnt[0];
               A511ContagemResultado_HoraCnt = P003118_A511ContagemResultado_HoraCnt[0];
               A1553ContagemResultado_CntSrvCod = P003118_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P003118_n1553ContagemResultado_CntSrvCod[0];
               A512ContagemResultado_ValorPF = P003118_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P003118_n512ContagemResultado_ValorPF[0];
               A494ContagemResultado_Descricao = P003118_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = P003118_n494ContagemResultado_Descricao[0];
               A1046ContagemResultado_Agrupador = P003118_A1046ContagemResultado_Agrupador[0];
               n1046ContagemResultado_Agrupador = P003118_n1046ContagemResultado_Agrupador[0];
               A468ContagemResultado_NaoCnfDmnCod = P003118_A468ContagemResultado_NaoCnfDmnCod[0];
               n468ContagemResultado_NaoCnfDmnCod = P003118_n468ContagemResultado_NaoCnfDmnCod[0];
               A598ContagemResultado_Baseline = P003118_A598ContagemResultado_Baseline[0];
               n598ContagemResultado_Baseline = P003118_n598ContagemResultado_Baseline[0];
               A489ContagemResultado_SistemaCod = P003118_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P003118_n489ContagemResultado_SistemaCod[0];
               A508ContagemResultado_Owner = P003118_A508ContagemResultado_Owner[0];
               A1351ContagemResultado_DataPrevista = P003118_A1351ContagemResultado_DataPrevista[0];
               n1351ContagemResultado_DataPrevista = P003118_n1351ContagemResultado_DataPrevista[0];
               A471ContagemResultado_DataDmn = P003118_A471ContagemResultado_DataDmn[0];
               A493ContagemResultado_DemandaFM = P003118_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P003118_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = P003118_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P003118_n457ContagemResultado_Demanda[0];
               A484ContagemResultado_StatusDmn = P003118_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P003118_n484ContagemResultado_StatusDmn[0];
               A1854ContagemResultado_VlrCnc = P003118_A1854ContagemResultado_VlrCnc[0];
               n1854ContagemResultado_VlrCnc = P003118_n1854ContagemResultado_VlrCnc[0];
               A490ContagemResultado_ContratadaCod = P003118_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P003118_n490ContagemResultado_ContratadaCod[0];
               A1348ContagemResultado_DataHomologacao = P003118_A1348ContagemResultado_DataHomologacao[0];
               n1348ContagemResultado_DataHomologacao = P003118_n1348ContagemResultado_DataHomologacao[0];
               A1603ContagemResultado_CntCod = P003118_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P003118_n1603ContagemResultado_CntCod[0];
               A601ContagemResultado_Servico = P003118_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P003118_n601ContagemResultado_Servico[0];
               A801ContagemResultado_ServicoSigla = P003118_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = P003118_n801ContagemResultado_ServicoSigla[0];
               A764ContagemResultado_ServicoGrupo = P003118_A764ContagemResultado_ServicoGrupo[0];
               n764ContagemResultado_ServicoGrupo = P003118_n764ContagemResultado_ServicoGrupo[0];
               A515ContagemResultado_SistemaCoord = P003118_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P003118_n515ContagemResultado_SistemaCoord[0];
               A803ContagemResultado_ContratadaSigla = P003118_A803ContagemResultado_ContratadaSigla[0];
               n803ContagemResultado_ContratadaSigla = P003118_n803ContagemResultado_ContratadaSigla[0];
               A52Contratada_AreaTrabalhoCod = P003118_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P003118_n52Contratada_AreaTrabalhoCod[0];
               A683ContagemResultado_PFLFMUltima = P003118_A683ContagemResultado_PFLFMUltima[0];
               A685ContagemResultado_PFLFSUltima = P003118_A685ContagemResultado_PFLFSUltima[0];
               A682ContagemResultado_PFBFMUltima = P003118_A682ContagemResultado_PFBFMUltima[0];
               A684ContagemResultado_PFBFSUltima = P003118_A684ContagemResultado_PFBFSUltima[0];
               A584ContagemResultado_ContadorFM = P003118_A584ContagemResultado_ContadorFM[0];
               A566ContagemResultado_DataUltCnt = P003118_A566ContagemResultado_DataUltCnt[0];
               A531ContagemResultado_StatusUltCnt = P003118_A531ContagemResultado_StatusUltCnt[0];
               GXt_boolean1 = A1802ContagemResultado_TemPndHmlg;
               new prc_tempndparahomologar(context ).execute( ref  A456ContagemResultado_Codigo, out  GXt_boolean1) ;
               A1802ContagemResultado_TemPndHmlg = GXt_boolean1;
               if ( ! ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV348ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
               {
                  if ( ! ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV348ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                  {
                     if ( ! ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV373ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                     {
                        if ( ! ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV373ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                        {
                           if ( ! ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV398ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                           {
                              if ( ! ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV398ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                              {
                                 if ( ! ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV423ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                                 {
                                    if ( ! ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV423ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                                    {
                                       if ( ! ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV448ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                                       {
                                          if ( ! ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV448ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                                          {
                                             GXt_decimal2 = A574ContagemResultado_PFFinal;
                                             new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal2) ;
                                             A574ContagemResultado_PFFinal = GXt_decimal2;
                                             if ( (Convert.ToDecimal(0)==AV472ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal) || ( ( A574ContagemResultado_PFFinal >= AV472ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ) ) )
                                             {
                                                if ( (Convert.ToDecimal(0)==AV473ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to) || ( ( A574ContagemResultado_PFFinal <= AV473ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ) ) )
                                                {
                                                   AV149Codigo = A456ContagemResultado_Codigo;
                                                   AV168Validado = false;
                                                   if ( ( StringUtil.StrCmp(AV8Acao, "H") == 0 ) && ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "D") == 0 ) ) )
                                                   {
                                                      AV168Validado = true;
                                                      A1348ContagemResultado_DataHomologacao = DateTimeUtil.ServerNow( context, "DEFAULT");
                                                      n1348ContagemResultado_DataHomologacao = false;
                                                      /* Execute user subroutine: 'DLTNAOCNFCONTESTADA' */
                                                      S1310 ();
                                                      if ( returnInSub )
                                                      {
                                                         pr_default.close(13);
                                                         returnInSub = true;
                                                         if (true) return;
                                                      }
                                                      A483ContagemResultado_StatusCnt = 8;
                                                   }
                                                   else if ( ( StringUtil.StrCmp(AV8Acao, "UH") == 0 ) && ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "H") == 0 ) )
                                                   {
                                                      AV168Validado = true;
                                                      A1348ContagemResultado_DataHomologacao = (DateTime)(DateTime.MinValue);
                                                      n1348ContagemResultado_DataHomologacao = false;
                                                      n1348ContagemResultado_DataHomologacao = true;
                                                      A483ContagemResultado_StatusCnt = 5;
                                                   }
                                                   else if ( ( StringUtil.StrCmp(AV8Acao, "UO") == 0 ) && ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "O") == 0 ) )
                                                   {
                                                      AV168Validado = true;
                                                   }
                                                   if ( AV168Validado )
                                                   {
                                                      AV89Codigos.Add(A456ContagemResultado_Codigo, 0);
                                                      AV150StatusAnterior = A484ContagemResultado_StatusDmn;
                                                      AV157Demandas = AV157Demandas + StringUtil.Trim( A457ContagemResultado_Demanda) + ", ";
                                                      A484ContagemResultado_StatusDmn = AV151StatusDemanda;
                                                      n484ContagemResultado_StatusDmn = false;
                                                   }
                                                   BatchSize = 200;
                                                   pr_default.initializeBatch( 14, BatchSize, this, "Executebatchp003119");
                                                   /* Using cursor P003119 */
                                                   pr_default.addRecord(14, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, A456ContagemResultado_Codigo});
                                                   if ( pr_default.recordCount(14) == pr_default.getBatchSize(14) )
                                                   {
                                                      Executebatchp003119( ) ;
                                                   }
                                                   dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                                                   BatchSize = 200;
                                                   pr_default.initializeBatch( 15, BatchSize, this, "Executebatchp003120");
                                                   /* Using cursor P003120 */
                                                   pr_default.addRecord(15, new Object[] {A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                                                   if ( pr_default.recordCount(15) == pr_default.getBatchSize(15) )
                                                   {
                                                      Executebatchp003120( ) ;
                                                   }
                                                   dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                                                }
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
               pr_default.readNext(13);
            }
            if ( pr_default.getBatchSize(14) > 0 )
            {
               Executebatchp003119( ) ;
            }
            if ( pr_default.getBatchSize(15) > 0 )
            {
               Executebatchp003120( ) ;
            }
            pr_default.close(13);
         }
         else
         {
            pr_default.dynParam(16, new Object[]{ new Object[]{
                                                 A456ContagemResultado_Codigo ,
                                                 AV89Codigos },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor P003121 */
            pr_default.execute(16);
            while ( (pr_default.getStatus(16) != 101) )
            {
               A456ContagemResultado_Codigo = P003121_A456ContagemResultado_Codigo[0];
               A484ContagemResultado_StatusDmn = P003121_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P003121_n484ContagemResultado_StatusDmn[0];
               A457ContagemResultado_Demanda = P003121_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P003121_n457ContagemResultado_Demanda[0];
               A1348ContagemResultado_DataHomologacao = P003121_A1348ContagemResultado_DataHomologacao[0];
               n1348ContagemResultado_DataHomologacao = P003121_n1348ContagemResultado_DataHomologacao[0];
               AV150StatusAnterior = A484ContagemResultado_StatusDmn;
               AV157Demandas = AV157Demandas + StringUtil.Trim( A457ContagemResultado_Demanda) + ", ";
               A484ContagemResultado_StatusDmn = AV151StatusDemanda;
               n484ContagemResultado_StatusDmn = false;
               if ( StringUtil.StrCmp(AV8Acao, "H") == 0 )
               {
                  A1348ContagemResultado_DataHomologacao = DateTimeUtil.ServerNow( context, "DEFAULT");
                  n1348ContagemResultado_DataHomologacao = false;
                  AV149Codigo = A456ContagemResultado_Codigo;
                  /* Execute user subroutine: 'DLTNAOCNFCONTESTADA' */
                  S1310 ();
                  if ( returnInSub )
                  {
                     pr_default.close(16);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV311StatusContagem = 8;
               }
               else if ( StringUtil.StrCmp(AV8Acao, "UH") == 0 )
               {
                  A1348ContagemResultado_DataHomologacao = (DateTime)(DateTime.MinValue);
                  n1348ContagemResultado_DataHomologacao = false;
                  AV311StatusContagem = 5;
               }
               /* Optimized UPDATE. */
               /* Using cursor P003122 */
               pr_default.execute(17, new Object[] {AV311StatusContagem, A456ContagemResultado_Codigo});
               pr_default.close(17);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               /* End optimized UPDATE. */
               BatchSize = 200;
               pr_default.initializeBatch( 18, BatchSize, this, "Executebatchp003123");
               /* Using cursor P003123 */
               pr_default.addRecord(18, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, A456ContagemResultado_Codigo});
               if ( pr_default.recordCount(18) == pr_default.getBatchSize(18) )
               {
                  Executebatchp003123( ) ;
               }
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               pr_default.readNext(16);
            }
            if ( pr_default.getBatchSize(18) > 0 )
            {
               Executebatchp003123( ) ;
            }
            pr_default.close(16);
         }
         if ( AV89Codigos.Count == 1 )
         {
            if ( StringUtil.StringSearch( AV8Acao, "U", 1) == 0 )
            {
               AV163Subject = StringUtil.StringReplace( AV163Subject, "adas,", "ada,");
               AV163Subject = StringUtil.StringReplace( AV163Subject, "adas", "ada");
               AV163Subject = StringUtil.StringReplace( AV163Subject, "Demandas", "OS "+StringUtil.StringReplace( AV157Demandas, ",", ""));
            }
            else
            {
               AV163Subject = "OS " + StringUtil.StringReplace( AV157Demandas, ",", " ") + AV163Subject;
            }
         }
      }

      protected void S141( )
      {
         /* 'ENVIARNOTIFICACAO' Routine */
         pr_default.dynParam(19, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV89Codigos },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor P003124 */
         pr_default.execute(19);
         while ( (pr_default.getStatus(19) != 101) )
         {
            A490ContagemResultado_ContratadaCod = P003124_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P003124_n490ContagemResultado_ContratadaCod[0];
            A1553ContagemResultado_CntSrvCod = P003124_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P003124_n1553ContagemResultado_CntSrvCod[0];
            A1604ContagemResultado_CntPrpCod = P003124_A1604ContagemResultado_CntPrpCod[0];
            n1604ContagemResultado_CntPrpCod = P003124_n1604ContagemResultado_CntPrpCod[0];
            A1605ContagemResultado_CntPrpPesCod = P003124_A1605ContagemResultado_CntPrpPesCod[0];
            n1605ContagemResultado_CntPrpPesCod = P003124_n1605ContagemResultado_CntPrpPesCod[0];
            A456ContagemResultado_Codigo = P003124_A456ContagemResultado_Codigo[0];
            A1603ContagemResultado_CntCod = P003124_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P003124_n1603ContagemResultado_CntCod[0];
            A803ContagemResultado_ContratadaSigla = P003124_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P003124_n803ContagemResultado_ContratadaSigla[0];
            A1606ContagemResultado_CntPrpPesNom = P003124_A1606ContagemResultado_CntPrpPesNom[0];
            n1606ContagemResultado_CntPrpPesNom = P003124_n1606ContagemResultado_CntPrpPesNom[0];
            A803ContagemResultado_ContratadaSigla = P003124_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P003124_n803ContagemResultado_ContratadaSigla[0];
            A1603ContagemResultado_CntCod = P003124_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P003124_n1603ContagemResultado_CntCod[0];
            A1604ContagemResultado_CntPrpCod = P003124_A1604ContagemResultado_CntPrpCod[0];
            n1604ContagemResultado_CntPrpCod = P003124_n1604ContagemResultado_CntPrpCod[0];
            A1605ContagemResultado_CntPrpPesCod = P003124_A1605ContagemResultado_CntPrpPesCod[0];
            n1605ContagemResultado_CntPrpPesCod = P003124_n1605ContagemResultado_CntPrpPesCod[0];
            A1606ContagemResultado_CntPrpPesNom = P003124_A1606ContagemResultado_CntPrpPesNom[0];
            n1606ContagemResultado_CntPrpPesNom = P003124_n1606ContagemResultado_CntPrpPesNom[0];
            AV184Contrato_Codigo = A1603ContagemResultado_CntCod;
            AV158UserName = StringUtil.Trim( A1606ContagemResultado_CntPrpPesNom) + " da " + A803ContagemResultado_ContratadaSigla;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(19);
         }
         pr_default.close(19);
         if ( AV66WWPContext.gxTpr_Userehcontratante )
         {
            AV158UserName = AV66WWPContext.gxTpr_Username;
         }
         new prc_usuariosparanotificar(context ).execute(  AV184Contrato_Codigo,  AV151StatusDemanda) ;
         AV159Usuarios.FromXml(AV90WebSession.Get("Usuarios"), "Collection");
         AV90WebSession.Remove("Usuarios");
         if ( (0==AV159Usuarios.IndexOf((int)(AV66WWPContext.gxTpr_Userid))) )
         {
            /* Using cursor P003125 */
            pr_default.execute(20, new Object[] {AV66WWPContext.gxTpr_Userid});
            while ( (pr_default.getStatus(20) != 101) )
            {
               A1Usuario_Codigo = P003125_A1Usuario_Codigo[0];
               AV159Usuarios.Add(A1Usuario_Codigo, 0);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(20);
         }
         if ( AV159Usuarios.Count > 0 )
         {
            AV162EmailText = StringUtil.NewLine( ) + "Prezado(a)," + StringUtil.NewLine( );
            AV162EmailText = AV162EmailText + "As OS do Lote " + AV71Lote_Nome + " sob n�mero/s " + AV157Demandas + "foram de: " + gxdomainstatusdemanda.getDescription(context,AV150StatusAnterior) + " para " + gxdomainstatusdemanda.getDescription(context,AV151StatusDemanda) + ".";
            AV162EmailText = AV162EmailText + StringUtil.NewLine( ) + AV66WWPContext.gxTpr_Areatrabalho_descricao + ", usu�rio " + AV158UserName + StringUtil.NewLine( );
            AV162EmailText = AV162EmailText + StringUtil.NewLine( ) + StringUtil.NewLine( ) + "Esta mensagem pode conter informa��o confidencial e/ou privilegiada. Se voc� n�o for o destinat�rio ou a pessoa autorizada a receber esta mensagem, n�o pode usar, copiar ou divulgar as informa��es nela contidas ou tomar qualquer a��o baseada nessas informa��es. Se voc� recebeu esta mensagem por engano, por favor avise imediatamente o remetente, respondendo o e-mail e em seguida apague-o.";
            AV90WebSession.Set("DemandaCodigo", AV89Codigos.ToXml(false, true, "Collection", ""));
            new prc_enviaremail(context ).execute(  AV45Contratada_AreaTrabalhoCod,  AV159Usuarios,  AV163Subject,  AV162EmailText,  AV165Attachments, ref  AV164Resultado) ;
            AV90WebSession.Remove("DemandaCodigo");
         }
      }

      protected void S1310( )
      {
         /* 'DLTNAOCNFCONTESTADA' Routine */
         /* Using cursor P003127 */
         pr_default.execute(21, new Object[] {AV149Codigo});
         if ( (pr_default.getStatus(21) != 101) )
         {
            A40000GXC1 = P003127_A40000GXC1[0];
         }
         else
         {
            A40000GXC1 = 0;
         }
         pr_default.close(21);
         AV149Codigo = A40000GXC1;
         if ( AV149Codigo > 0 )
         {
            new prc_dltnaocnf(context ).execute( ref  AV149Codigo) ;
         }
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      protected void Executebatchp003116( )
      {
         /* Using cursor P003116 */
         pr_default.executeBatch(12);
         pr_default.close(12);
      }

      protected void Executebatchp003119( )
      {
         /* Using cursor P003119 */
         pr_default.executeBatch(14);
         pr_default.close(14);
      }

      protected void Executebatchp003120( )
      {
         /* Using cursor P003120 */
         pr_default.executeBatch(15);
         pr_default.close(15);
      }

      protected void Executebatchp003123( )
      {
         /* Using cursor P003123 */
         pr_default.executeBatch(18);
         pr_default.close(18);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_AgruparParaFaturamento");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(18);
         pr_default.close(14);
         pr_default.close(12);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         AV66WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV89Codigos = new GxSimpleCollection();
         AV90WebSession = context.GetSession();
         AV58GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV59GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV49DynamicFiltersSelector1 = "";
         AV17ContagemResultado_DataCnt1 = DateTime.MinValue;
         AV14ContagemResultado_DataCnt_To1 = DateTime.MinValue;
         AV40ContagemResultado_StatusDmn1 = "";
         AV27ContagemResultado_OsFsOsFm1 = "";
         AV283ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV278ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV294ContagemResultado_DataPrevista1 = (DateTime)(DateTime.MinValue);
         AV289ContagemResultado_DataPrevista_To1 = (DateTime)(DateTime.MinValue);
         AV86ContagemResultado_Baseline1 = "";
         AV143ContagemResultado_Agrupador1 = "";
         AV187ContagemResultado_Descricao1 = "";
         AV219ContagemResultado_TemPndHmlg1 = "";
         AV50DynamicFiltersSelector2 = "";
         AV18ContagemResultado_DataCnt2 = DateTime.MinValue;
         AV15ContagemResultado_DataCnt_To2 = DateTime.MinValue;
         AV41ContagemResultado_StatusDmn2 = "";
         AV28ContagemResultado_OsFsOsFm2 = "";
         AV284ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV279ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV295ContagemResultado_DataPrevista2 = (DateTime)(DateTime.MinValue);
         AV290ContagemResultado_DataPrevista_To2 = (DateTime)(DateTime.MinValue);
         AV87ContagemResultado_Baseline2 = "";
         AV144ContagemResultado_Agrupador2 = "";
         AV188ContagemResultado_Descricao2 = "";
         AV220ContagemResultado_TemPndHmlg2 = "";
         AV51DynamicFiltersSelector3 = "";
         AV19ContagemResultado_DataCnt3 = DateTime.MinValue;
         AV16ContagemResultado_DataCnt_To3 = DateTime.MinValue;
         AV42ContagemResultado_StatusDmn3 = "";
         AV29ContagemResultado_OsFsOsFm3 = "";
         AV285ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV280ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV296ContagemResultado_DataPrevista3 = (DateTime)(DateTime.MinValue);
         AV291ContagemResultado_DataPrevista_To3 = (DateTime)(DateTime.MinValue);
         AV88ContagemResultado_Baseline3 = "";
         AV145ContagemResultado_Agrupador3 = "";
         AV189ContagemResultado_Descricao3 = "";
         AV221ContagemResultado_TemPndHmlg3 = "";
         AV101DynamicFiltersSelector4 = "";
         AV108ContagemResultado_DataCnt4 = DateTime.MinValue;
         AV107ContagemResultado_DataCnt_To4 = DateTime.MinValue;
         AV117ContagemResultado_StatusDmn4 = "";
         AV112ContagemResultado_OsFsOsFm4 = "";
         AV286ContagemResultado_DataDmn4 = DateTime.MinValue;
         AV281ContagemResultado_DataDmn_To4 = DateTime.MinValue;
         AV297ContagemResultado_DataPrevista4 = (DateTime)(DateTime.MinValue);
         AV292ContagemResultado_DataPrevista_To4 = (DateTime)(DateTime.MinValue);
         AV103ContagemResultado_Baseline4 = "";
         AV146ContagemResultado_Agrupador4 = "";
         AV190ContagemResultado_Descricao4 = "";
         AV222ContagemResultado_TemPndHmlg4 = "";
         AV102DynamicFiltersSelector5 = "";
         AV110ContagemResultado_DataCnt5 = DateTime.MinValue;
         AV109ContagemResultado_DataCnt_To5 = DateTime.MinValue;
         AV124ContagemResultado_StatusDmn5 = "";
         AV119ContagemResultado_OsFsOsFm5 = "";
         AV287ContagemResultado_DataDmn5 = DateTime.MinValue;
         AV282ContagemResultado_DataDmn_To5 = DateTime.MinValue;
         AV298ContagemResultado_DataPrevista5 = (DateTime)(DateTime.MinValue);
         AV293ContagemResultado_DataPrevista_To5 = (DateTime)(DateTime.MinValue);
         AV104ContagemResultado_Baseline5 = "";
         AV147ContagemResultado_Agrupador5 = "";
         AV191ContagemResultado_Descricao5 = "";
         AV223ContagemResultado_TemPndHmlg5 = "";
         AV151StatusDemanda = "";
         AV163Subject = "";
         AV148StatusLog = "";
         scmdbuf = "";
         P00312_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00312_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00312_A456ContagemResultado_Codigo = new int[1] ;
         P00312_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00312_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00312_A1603ContagemResultado_CntCod = new int[1] ;
         P00312_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00315_A74Contrato_Codigo = new int[1] ;
         P00315_n74Contrato_Codigo = new bool[] {false} ;
         P00315_A39Contratada_Codigo = new int[1] ;
         P00315_A530Contratada_Lote = new short[1] ;
         P00315_n530Contratada_Lote = new bool[] {false} ;
         P00315_A1451Contratada_SS = new int[1] ;
         P00315_n1451Contratada_SS = new bool[] {false} ;
         P00315_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         P00315_A81Contrato_Quantidade = new int[1] ;
         P00315_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         P00315_n842Contrato_DataInicioTA = new bool[] {false} ;
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         A842Contrato_DataInicioTA = DateTime.MinValue;
         AV98Lote_DataContrato = DateTime.MinValue;
         AV64ServerNow = (DateTime)(DateTime.MinValue);
         AV77Lote_Numero = "";
         A562Lote_Numero = "";
         A563Lote_Nome = "";
         A564Lote_Data = (DateTime)(DateTime.MinValue);
         A844Lote_DataContrato = DateTime.MinValue;
         A2088Lote_ParecerFinal = "";
         A2087Lote_Comentarios = "";
         P00316_A596Lote_Codigo = new int[1] ;
         Gx_emsg = "";
         P00317_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00317_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00317_A456ContagemResultado_Codigo = new int[1] ;
         P00318_A39Contratada_Codigo = new int[1] ;
         P00318_A1451Contratada_SS = new int[1] ;
         P00318_n1451Contratada_SS = new bool[] {false} ;
         P00318_A530Contratada_Lote = new short[1] ;
         P00318_n530Contratada_Lote = new bool[] {false} ;
         AV150StatusAnterior = "";
         P003111_A596Lote_Codigo = new int[1] ;
         Gx_date = DateTime.MinValue;
         GXt_dtime3 = (DateTime)(DateTime.MinValue);
         P003113_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003113_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003113_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P003113_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P003113_A1452ContagemResultado_SS = new int[1] ;
         P003113_n1452ContagemResultado_SS = new bool[] {false} ;
         P003113_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P003113_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P003113_A1559ContagemResultado_VlrAceite = new decimal[1] ;
         P003113_n1559ContagemResultado_VlrAceite = new bool[] {false} ;
         P003113_A457ContagemResultado_Demanda = new String[] {""} ;
         P003113_n457ContagemResultado_Demanda = new bool[] {false} ;
         P003113_A456ContagemResultado_Codigo = new int[1] ;
         P003113_A512ContagemResultado_ValorPF = new decimal[1] ;
         P003113_n512ContagemResultado_ValorPF = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         A457ContagemResultado_Demanda = "";
         P003114_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         P003114_A1323ContagemResultadoIndicadores_Valor = new decimal[1] ;
         P003114_A1316ContagemResultadoIndicadores_LoteCod = new int[1] ;
         P003114_n1316ContagemResultadoIndicadores_LoteCod = new bool[] {false} ;
         P003114_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         AV157Demandas = "";
         P003116_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003116_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003116_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P003116_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P003116_A1452ContagemResultado_SS = new int[1] ;
         P003116_n1452ContagemResultado_SS = new bool[] {false} ;
         P003116_A1559ContagemResultado_VlrAceite = new decimal[1] ;
         P003116_n1559ContagemResultado_VlrAceite = new bool[] {false} ;
         P003116_A456ContagemResultado_Codigo = new int[1] ;
         AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = "";
         AV328ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 = DateTime.MinValue;
         AV329ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 = DateTime.MinValue;
         AV330ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 = "";
         AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = "";
         AV332ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV333ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV334ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 = DateTime.MinValue;
         AV335ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 = DateTime.MinValue;
         AV341ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 = "";
         AV345ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 = "";
         AV346ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = "";
         AV348ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 = "";
         AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = "";
         AV353ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 = DateTime.MinValue;
         AV354ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 = DateTime.MinValue;
         AV355ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 = "";
         AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = "";
         AV357ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV358ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV359ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 = DateTime.MinValue;
         AV360ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 = DateTime.MinValue;
         AV366ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 = "";
         AV370ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 = "";
         AV371ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = "";
         AV373ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 = "";
         AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = "";
         AV378ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 = DateTime.MinValue;
         AV379ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 = DateTime.MinValue;
         AV380ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 = "";
         AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = "";
         AV382ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV383ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV384ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 = DateTime.MinValue;
         AV385ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 = DateTime.MinValue;
         AV391ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 = "";
         AV395ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 = "";
         AV396ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = "";
         AV398ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 = "";
         AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = "";
         AV403ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 = DateTime.MinValue;
         AV404ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 = DateTime.MinValue;
         AV405ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 = "";
         AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = "";
         AV407ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 = DateTime.MinValue;
         AV408ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 = DateTime.MinValue;
         AV409ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 = DateTime.MinValue;
         AV410ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 = DateTime.MinValue;
         AV416ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 = "";
         AV420ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 = "";
         AV421ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = "";
         AV423ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 = "";
         AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = "";
         AV428ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 = DateTime.MinValue;
         AV429ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 = DateTime.MinValue;
         AV430ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 = "";
         AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = "";
         AV432ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 = DateTime.MinValue;
         AV433ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 = DateTime.MinValue;
         AV434ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 = DateTime.MinValue;
         AV435ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 = DateTime.MinValue;
         AV441ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 = "";
         AV445ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 = "";
         AV446ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = "";
         AV448ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 = "";
         AV450ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = "";
         AV224TFContagemResultado_Agrupador = "";
         AV451ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel = "";
         AV225TFContagemResultado_Agrupador_Sel = "";
         AV452ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = "";
         AV233TFContagemResultado_DemandaFM = "";
         AV453ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel = "";
         AV234TFContagemResultado_DemandaFM_Sel = "";
         AV454ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = "";
         AV231TFContagemResultado_Demanda = "";
         AV455ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel = "";
         AV232TFContagemResultado_Demanda_Sel = "";
         AV456ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = "";
         AV235TFContagemResultado_Descricao = "";
         AV457ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel = "";
         AV236TFContagemResultado_Descricao_Sel = "";
         AV458ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista = (DateTime)(DateTime.MinValue);
         AV301TFContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         AV459ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to = (DateTime)(DateTime.MinValue);
         AV302TFContagemResultado_DataPrevista_To = (DateTime)(DateTime.MinValue);
         AV460ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn = DateTime.MinValue;
         AV299TFContagemResultado_DataDmn = DateTime.MinValue;
         AV461ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to = DateTime.MinValue;
         AV300TFContagemResultado_DataDmn_To = DateTime.MinValue;
         AV462ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt = DateTime.MinValue;
         AV229TFContagemResultado_DataUltCnt = DateTime.MinValue;
         AV463ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to = DateTime.MinValue;
         AV230TFContagemResultado_DataUltCnt_To = DateTime.MinValue;
         AV464ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = "";
         AV227TFContagemResultado_ContratadaSigla = "";
         AV465ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel = "";
         AV228TFContagemResultado_ContratadaSigla_Sel = "";
         AV466ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = "";
         AV241TFContagemResultado_SistemaCoord = "";
         AV467ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel = "";
         AV242TFContagemResultado_SistemaCoord_Sel = "";
         AV468ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels = new GxSimpleCollection();
         AV245TFContagemResultado_StatusDmn_Sels = new GxSimpleCollection();
         AV470ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = "";
         AV239TFContagemResultado_ServicoSigla = "";
         AV471ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel = "";
         AV240TFContagemResultado_ServicoSigla_Sel = "";
         lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = "";
         lV346ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = "";
         lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = "";
         lV371ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = "";
         lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = "";
         lV396ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = "";
         lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = "";
         lV421ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = "";
         lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = "";
         lV446ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = "";
         lV450ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = "";
         lV452ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = "";
         lV454ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = "";
         lV456ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = "";
         lV464ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = "";
         lV466ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = "";
         lV470ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A1046ContagemResultado_Agrupador = "";
         A494ContagemResultado_Descricao = "";
         A803ContagemResultado_ContratadaSigla = "";
         A515ContagemResultado_SistemaCoord = "";
         A801ContagemResultado_ServicoSigla = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         P003118_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P003118_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P003118_A517ContagemResultado_Ultima = new bool[] {false} ;
         P003118_A512ContagemResultado_ValorPF = new decimal[1] ;
         P003118_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P003118_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P003118_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P003118_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P003118_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P003118_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P003118_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P003118_A1603ContagemResultado_CntCod = new int[1] ;
         P003118_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P003118_A494ContagemResultado_Descricao = new String[] {""} ;
         P003118_n494ContagemResultado_Descricao = new bool[] {false} ;
         P003118_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P003118_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P003118_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P003118_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P003118_A598ContagemResultado_Baseline = new bool[] {false} ;
         P003118_n598ContagemResultado_Baseline = new bool[] {false} ;
         P003118_A764ContagemResultado_ServicoGrupo = new int[1] ;
         P003118_n764ContagemResultado_ServicoGrupo = new bool[] {false} ;
         P003118_A489ContagemResultado_SistemaCod = new int[1] ;
         P003118_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P003118_A508ContagemResultado_Owner = new int[1] ;
         P003118_A601ContagemResultado_Servico = new int[1] ;
         P003118_n601ContagemResultado_Servico = new bool[] {false} ;
         P003118_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P003118_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P003118_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P003118_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P003118_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P003118_A457ContagemResultado_Demanda = new String[] {""} ;
         P003118_n457ContagemResultado_Demanda = new bool[] {false} ;
         P003118_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003118_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003118_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P003118_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P003118_A490ContagemResultado_ContratadaCod = new int[1] ;
         P003118_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P003118_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P003118_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P003118_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P003118_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         P003118_A483ContagemResultado_StatusCnt = new short[1] ;
         P003118_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P003118_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P003118_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P003118_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P003118_A584ContagemResultado_ContadorFM = new int[1] ;
         P003118_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P003118_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P003118_A456ContagemResultado_Codigo = new int[1] ;
         P003118_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P003118_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A1348ContagemResultado_DataHomologacao = (DateTime)(DateTime.MinValue);
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         P003119_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003119_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003119_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P003119_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         P003119_A456ContagemResultado_Codigo = new int[1] ;
         P003120_A483ContagemResultado_StatusCnt = new short[1] ;
         P003120_A456ContagemResultado_Codigo = new int[1] ;
         P003120_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P003120_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P003121_A456ContagemResultado_Codigo = new int[1] ;
         P003121_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003121_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003121_A457ContagemResultado_Demanda = new String[] {""} ;
         P003121_n457ContagemResultado_Demanda = new bool[] {false} ;
         P003121_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P003121_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         P003123_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003123_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003123_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P003123_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         P003123_A456ContagemResultado_Codigo = new int[1] ;
         P003124_A490ContagemResultado_ContratadaCod = new int[1] ;
         P003124_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P003124_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P003124_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P003124_A1604ContagemResultado_CntPrpCod = new int[1] ;
         P003124_n1604ContagemResultado_CntPrpCod = new bool[] {false} ;
         P003124_A1605ContagemResultado_CntPrpPesCod = new int[1] ;
         P003124_n1605ContagemResultado_CntPrpPesCod = new bool[] {false} ;
         P003124_A456ContagemResultado_Codigo = new int[1] ;
         P003124_A1603ContagemResultado_CntCod = new int[1] ;
         P003124_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P003124_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P003124_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P003124_A1606ContagemResultado_CntPrpPesNom = new String[] {""} ;
         P003124_n1606ContagemResultado_CntPrpPesNom = new bool[] {false} ;
         A1606ContagemResultado_CntPrpPesNom = "";
         AV158UserName = "";
         AV159Usuarios = new GxSimpleCollection();
         P003125_A1Usuario_Codigo = new int[1] ;
         AV162EmailText = "";
         AV165Attachments = new GxSimpleCollection();
         AV164Resultado = "";
         P003127_A40000GXC1 = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_agruparparafaturamento__default(),
            new Object[][] {
                new Object[] {
               P00312_A1553ContagemResultado_CntSrvCod, P00312_n1553ContagemResultado_CntSrvCod, P00312_A456ContagemResultado_Codigo, P00312_A490ContagemResultado_ContratadaCod, P00312_n490ContagemResultado_ContratadaCod, P00312_A1603ContagemResultado_CntCod, P00312_n1603ContagemResultado_CntCod
               }
               , new Object[] {
               P00315_A74Contrato_Codigo, P00315_A39Contratada_Codigo, P00315_A530Contratada_Lote, P00315_n530Contratada_Lote, P00315_A1451Contratada_SS, P00315_n1451Contratada_SS, P00315_A82Contrato_DataVigenciaInicio, P00315_A81Contrato_Quantidade, P00315_A842Contrato_DataInicioTA, P00315_n842Contrato_DataInicioTA
               }
               , new Object[] {
               P00316_A596Lote_Codigo
               }
               , new Object[] {
               P00317_A597ContagemResultado_LoteAceiteCod, P00317_n597ContagemResultado_LoteAceiteCod, P00317_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00318_A39Contratada_Codigo, P00318_A1451Contratada_SS, P00318_n1451Contratada_SS, P00318_A530Contratada_Lote, P00318_n530Contratada_Lote
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P003111_A596Lote_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               P003113_A484ContagemResultado_StatusDmn, P003113_n484ContagemResultado_StatusDmn, P003113_A597ContagemResultado_LoteAceiteCod, P003113_n597ContagemResultado_LoteAceiteCod, P003113_A1452ContagemResultado_SS, P003113_n1452ContagemResultado_SS, P003113_A1051ContagemResultado_GlsValor, P003113_n1051ContagemResultado_GlsValor, P003113_A1559ContagemResultado_VlrAceite, P003113_n1559ContagemResultado_VlrAceite,
               P003113_A457ContagemResultado_Demanda, P003113_n457ContagemResultado_Demanda, P003113_A456ContagemResultado_Codigo, P003113_A512ContagemResultado_ValorPF, P003113_n512ContagemResultado_ValorPF
               }
               , new Object[] {
               P003114_A1314ContagemResultadoIndicadores_DemandaCod, P003114_A1323ContagemResultadoIndicadores_Valor, P003114_A1316ContagemResultadoIndicadores_LoteCod, P003114_n1316ContagemResultadoIndicadores_LoteCod, P003114_A1315ContagemResultadoIndicadores_IndicadorCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P003118_A1553ContagemResultado_CntSrvCod, P003118_n1553ContagemResultado_CntSrvCod, P003118_A517ContagemResultado_Ultima, P003118_A512ContagemResultado_ValorPF, P003118_n512ContagemResultado_ValorPF, P003118_A801ContagemResultado_ServicoSigla, P003118_n801ContagemResultado_ServicoSigla, P003118_A515ContagemResultado_SistemaCoord, P003118_n515ContagemResultado_SistemaCoord, P003118_A803ContagemResultado_ContratadaSigla,
               P003118_n803ContagemResultado_ContratadaSigla, P003118_A1603ContagemResultado_CntCod, P003118_n1603ContagemResultado_CntCod, P003118_A494ContagemResultado_Descricao, P003118_n494ContagemResultado_Descricao, P003118_A1046ContagemResultado_Agrupador, P003118_n1046ContagemResultado_Agrupador, P003118_A468ContagemResultado_NaoCnfDmnCod, P003118_n468ContagemResultado_NaoCnfDmnCod, P003118_A598ContagemResultado_Baseline,
               P003118_n598ContagemResultado_Baseline, P003118_A764ContagemResultado_ServicoGrupo, P003118_n764ContagemResultado_ServicoGrupo, P003118_A489ContagemResultado_SistemaCod, P003118_n489ContagemResultado_SistemaCod, P003118_A508ContagemResultado_Owner, P003118_A601ContagemResultado_Servico, P003118_n601ContagemResultado_Servico, P003118_A1351ContagemResultado_DataPrevista, P003118_n1351ContagemResultado_DataPrevista,
               P003118_A471ContagemResultado_DataDmn, P003118_A493ContagemResultado_DemandaFM, P003118_n493ContagemResultado_DemandaFM, P003118_A457ContagemResultado_Demanda, P003118_n457ContagemResultado_Demanda, P003118_A484ContagemResultado_StatusDmn, P003118_n484ContagemResultado_StatusDmn, P003118_A1854ContagemResultado_VlrCnc, P003118_n1854ContagemResultado_VlrCnc, P003118_A490ContagemResultado_ContratadaCod,
               P003118_n490ContagemResultado_ContratadaCod, P003118_A52Contratada_AreaTrabalhoCod, P003118_n52Contratada_AreaTrabalhoCod, P003118_A1348ContagemResultado_DataHomologacao, P003118_n1348ContagemResultado_DataHomologacao, P003118_A483ContagemResultado_StatusCnt, P003118_A683ContagemResultado_PFLFMUltima, P003118_A685ContagemResultado_PFLFSUltima, P003118_A682ContagemResultado_PFBFMUltima, P003118_A684ContagemResultado_PFBFSUltima,
               P003118_A584ContagemResultado_ContadorFM, P003118_A566ContagemResultado_DataUltCnt, P003118_A531ContagemResultado_StatusUltCnt, P003118_A456ContagemResultado_Codigo, P003118_A473ContagemResultado_DataCnt, P003118_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P003121_A456ContagemResultado_Codigo, P003121_A484ContagemResultado_StatusDmn, P003121_n484ContagemResultado_StatusDmn, P003121_A457ContagemResultado_Demanda, P003121_n457ContagemResultado_Demanda, P003121_A1348ContagemResultado_DataHomologacao, P003121_n1348ContagemResultado_DataHomologacao
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P003124_A490ContagemResultado_ContratadaCod, P003124_n490ContagemResultado_ContratadaCod, P003124_A1553ContagemResultado_CntSrvCod, P003124_n1553ContagemResultado_CntSrvCod, P003124_A1604ContagemResultado_CntPrpCod, P003124_n1604ContagemResultado_CntPrpCod, P003124_A1605ContagemResultado_CntPrpPesCod, P003124_n1605ContagemResultado_CntPrpPesCod, P003124_A456ContagemResultado_Codigo, P003124_A1603ContagemResultado_CntCod,
               P003124_n1603ContagemResultado_CntCod, P003124_A803ContagemResultado_ContratadaSigla, P003124_n803ContagemResultado_ContratadaSigla, P003124_A1606ContagemResultado_CntPrpPesNom, P003124_n1606ContagemResultado_CntPrpPesNom
               }
               , new Object[] {
               P003125_A1Usuario_Codigo
               }
               , new Object[] {
               P003127_A40000GXC1
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short AV38ContagemResultado_StatusCnt ;
      private short AV207DynamicFiltersOperator1 ;
      private short AV208DynamicFiltersOperator2 ;
      private short AV209DynamicFiltersOperator3 ;
      private short AV210DynamicFiltersOperator4 ;
      private short AV211DynamicFiltersOperator5 ;
      private short A530Contratada_Lote ;
      private short AV46Contratada_Lote ;
      private short AV91QtdeSelecionadas ;
      private short AV325ExtraWWContagemResultadoExtraSelectionDS_2_Contagemresultado_statuscnt ;
      private short AV327ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 ;
      private short AV352ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 ;
      private short AV377ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 ;
      private short AV402ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 ;
      private short AV427ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 ;
      private short AV469ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel ;
      private short AV226TFContagemResultado_Baseline_Sel ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short A483ContagemResultado_StatusCnt ;
      private short AV311StatusContagem ;
      private int AV45Contratada_AreaTrabalhoCod ;
      private int AV82ContagemResultado_Servico1 ;
      private int AV126ContagemResultado_ContadorFM1 ;
      private int AV31ContagemResultado_SistemaCod1 ;
      private int AV10ContagemResultado_ContratadaCod1 ;
      private int AV93ContagemResultado_ServicoGrupo1 ;
      private int AV23ContagemResultado_NaoCnfDmnCod1 ;
      private int AV193ContagemResultado_Owner1 ;
      private int AV305ContagemResultado_CntCod1 ;
      private int AV84ContagemResultado_Servico2 ;
      private int AV127ContagemResultado_ContadorFM2 ;
      private int AV32ContagemResultado_SistemaCod2 ;
      private int AV11ContagemResultado_ContratadaCod2 ;
      private int AV94ContagemResultado_ServicoGrupo2 ;
      private int AV24ContagemResultado_NaoCnfDmnCod2 ;
      private int AV194ContagemResultado_Owner2 ;
      private int AV306ContagemResultado_CntCod2 ;
      private int AV85ContagemResultado_Servico3 ;
      private int AV128ContagemResultado_ContadorFM3 ;
      private int AV33ContagemResultado_SistemaCod3 ;
      private int AV12ContagemResultado_ContratadaCod3 ;
      private int AV95ContagemResultado_ServicoGrupo3 ;
      private int AV25ContagemResultado_NaoCnfDmnCod3 ;
      private int AV195ContagemResultado_Owner3 ;
      private int AV307ContagemResultado_CntCod3 ;
      private int AV113ContagemResultado_Servico4 ;
      private int AV129ContagemResultado_ContadorFM4 ;
      private int AV115ContagemResultado_SistemaCod4 ;
      private int AV105ContagemResultado_ContratadaCod4 ;
      private int AV114ContagemResultado_ServicoGrupo4 ;
      private int AV111ContagemResultado_NaoCnfDmnCod4 ;
      private int AV196ContagemResultado_Owner4 ;
      private int AV308ContagemResultado_CntCod4 ;
      private int AV120ContagemResultado_Servico5 ;
      private int AV130ContagemResultado_ContadorFM5 ;
      private int AV122ContagemResultado_SistemaCod5 ;
      private int AV106ContagemResultado_ContratadaCod5 ;
      private int AV121ContagemResultado_ServicoGrupo5 ;
      private int AV118ContagemResultado_NaoCnfDmnCod5 ;
      private int AV197ContagemResultado_Owner5 ;
      private int AV309ContagemResultado_CntCod5 ;
      private int AV198PrimeiroID ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1603ContagemResultado_CntCod ;
      private int AV213Contratada_Codigo ;
      private int AV184Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A1451Contratada_SS ;
      private int A81Contrato_Quantidade ;
      private int AV200Contratada_SS ;
      private int GX_INS78 ;
      private int A595Lote_AreaTrabalhoCod ;
      private int A559Lote_UserCod ;
      private int A596Lote_Codigo ;
      private int AV21ContagemResultado_LoteAceite ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int AV319GXV1 ;
      private int AV149Codigo ;
      private int A1452ContagemResultado_SS ;
      private int A1314ContagemResultadoIndicadores_DemandaCod ;
      private int A1316ContagemResultadoIndicadores_LoteCod ;
      private int A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int BatchSize ;
      private int AV324ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod ;
      private int AV336ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 ;
      private int AV337ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 ;
      private int AV338ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 ;
      private int AV339ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 ;
      private int AV340ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 ;
      private int AV342ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 ;
      private int AV347ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 ;
      private int AV349ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 ;
      private int AV361ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 ;
      private int AV362ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 ;
      private int AV363ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 ;
      private int AV364ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 ;
      private int AV365ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 ;
      private int AV367ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 ;
      private int AV372ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 ;
      private int AV374ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 ;
      private int AV386ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 ;
      private int AV387ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 ;
      private int AV388ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 ;
      private int AV389ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 ;
      private int AV390ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 ;
      private int AV392ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 ;
      private int AV397ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 ;
      private int AV399ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 ;
      private int AV411ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 ;
      private int AV412ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 ;
      private int AV413ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 ;
      private int AV414ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 ;
      private int AV415ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 ;
      private int AV417ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 ;
      private int AV422ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 ;
      private int AV424ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 ;
      private int AV436ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 ;
      private int AV437ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 ;
      private int AV438ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 ;
      private int AV439ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 ;
      private int AV440ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 ;
      private int AV442ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 ;
      private int AV447ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 ;
      private int AV449ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 ;
      private int AV66WWPContext_gxTpr_Contratada_codigo ;
      private int AV468ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels_Count ;
      private int AV66WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A601ContagemResultado_Servico ;
      private int A489ContagemResultado_SistemaCod ;
      private int A764ContagemResultado_ServicoGrupo ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A508ContagemResultado_Owner ;
      private int A584ContagemResultado_ContadorFM ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1604ContagemResultado_CntPrpCod ;
      private int A1605ContagemResultado_CntPrpPesCod ;
      private int A1Usuario_Codigo ;
      private int A40000GXC1 ;
      private decimal AV131ContagemResultado_PFBFM1 ;
      private decimal AV136ContagemResultado_PFBFS1 ;
      private decimal AV132ContagemResultado_PFBFM2 ;
      private decimal AV137ContagemResultado_PFBFS2 ;
      private decimal AV133ContagemResultado_PFBFM3 ;
      private decimal AV138ContagemResultado_PFBFS3 ;
      private decimal AV134ContagemResultado_PFBFM4 ;
      private decimal AV139ContagemResultado_PFBFS4 ;
      private decimal AV135ContagemResultado_PFBFM5 ;
      private decimal AV140ContagemResultado_PFBFS5 ;
      private decimal AV304Contratado ;
      private decimal AV303Usado ;
      private decimal A565Lote_ValorPF ;
      private decimal A2055Lote_CntUsado ;
      private decimal A2056Lote_CntSaldo ;
      private decimal AV216ContagemResultado_VlrAceite ;
      private decimal A1051ContagemResultado_GlsValor ;
      private decimal A1559ContagemResultado_VlrAceite ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal A606ContagemResultado_ValorFinal ;
      private decimal A1323ContagemResultadoIndicadores_Valor ;
      private decimal AV343ExtraWWContagemResultadoExtraSelectionDS_20_Contagemresultado_pfbfm1 ;
      private decimal AV344ExtraWWContagemResultadoExtraSelectionDS_21_Contagemresultado_pfbfs1 ;
      private decimal AV368ExtraWWContagemResultadoExtraSelectionDS_45_Contagemresultado_pfbfm2 ;
      private decimal AV369ExtraWWContagemResultadoExtraSelectionDS_46_Contagemresultado_pfbfs2 ;
      private decimal AV393ExtraWWContagemResultadoExtraSelectionDS_70_Contagemresultado_pfbfm3 ;
      private decimal AV394ExtraWWContagemResultadoExtraSelectionDS_71_Contagemresultado_pfbfs3 ;
      private decimal AV418ExtraWWContagemResultadoExtraSelectionDS_95_Contagemresultado_pfbfm4 ;
      private decimal AV419ExtraWWContagemResultadoExtraSelectionDS_96_Contagemresultado_pfbfs4 ;
      private decimal AV443ExtraWWContagemResultadoExtraSelectionDS_120_Contagemresultado_pfbfm5 ;
      private decimal AV444ExtraWWContagemResultadoExtraSelectionDS_121_Contagemresultado_pfbfs5 ;
      private decimal AV472ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ;
      private decimal AV237TFContagemResultado_PFFinal ;
      private decimal AV473ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ;
      private decimal AV238TFContagemResultado_PFFinal_To ;
      private decimal AV474ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf ;
      private decimal AV247TFContagemResultado_ValorPF ;
      private decimal AV475ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to ;
      private decimal AV248TFContagemResultado_ValorPF_To ;
      private decimal A1854ContagemResultado_VlrCnc ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal A683ContagemResultado_PFLFMUltima ;
      private decimal GXt_decimal2 ;
      private String AV8Acao ;
      private String AV71Lote_Nome ;
      private String AV40ContagemResultado_StatusDmn1 ;
      private String AV86ContagemResultado_Baseline1 ;
      private String AV143ContagemResultado_Agrupador1 ;
      private String AV219ContagemResultado_TemPndHmlg1 ;
      private String AV41ContagemResultado_StatusDmn2 ;
      private String AV87ContagemResultado_Baseline2 ;
      private String AV144ContagemResultado_Agrupador2 ;
      private String AV220ContagemResultado_TemPndHmlg2 ;
      private String AV42ContagemResultado_StatusDmn3 ;
      private String AV88ContagemResultado_Baseline3 ;
      private String AV145ContagemResultado_Agrupador3 ;
      private String AV221ContagemResultado_TemPndHmlg3 ;
      private String AV117ContagemResultado_StatusDmn4 ;
      private String AV103ContagemResultado_Baseline4 ;
      private String AV146ContagemResultado_Agrupador4 ;
      private String AV222ContagemResultado_TemPndHmlg4 ;
      private String AV124ContagemResultado_StatusDmn5 ;
      private String AV104ContagemResultado_Baseline5 ;
      private String AV147ContagemResultado_Agrupador5 ;
      private String AV223ContagemResultado_TemPndHmlg5 ;
      private String AV151StatusDemanda ;
      private String AV163Subject ;
      private String AV148StatusLog ;
      private String scmdbuf ;
      private String AV77Lote_Numero ;
      private String A562Lote_Numero ;
      private String A563Lote_Nome ;
      private String Gx_emsg ;
      private String AV150StatusAnterior ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV157Demandas ;
      private String AV330ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 ;
      private String AV341ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 ;
      private String AV345ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 ;
      private String AV348ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 ;
      private String AV355ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 ;
      private String AV366ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 ;
      private String AV370ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 ;
      private String AV373ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 ;
      private String AV380ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 ;
      private String AV391ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 ;
      private String AV395ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 ;
      private String AV398ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 ;
      private String AV405ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 ;
      private String AV416ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 ;
      private String AV420ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 ;
      private String AV423ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 ;
      private String AV430ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 ;
      private String AV441ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 ;
      private String AV445ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 ;
      private String AV448ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 ;
      private String AV450ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ;
      private String AV224TFContagemResultado_Agrupador ;
      private String AV451ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel ;
      private String AV225TFContagemResultado_Agrupador_Sel ;
      private String AV464ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ;
      private String AV227TFContagemResultado_ContratadaSigla ;
      private String AV465ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel ;
      private String AV228TFContagemResultado_ContratadaSigla_Sel ;
      private String AV470ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ;
      private String AV239TFContagemResultado_ServicoSigla ;
      private String AV471ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel ;
      private String AV240TFContagemResultado_ServicoSigla_Sel ;
      private String lV450ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ;
      private String lV464ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ;
      private String lV470ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ;
      private String A1046ContagemResultado_Agrupador ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A511ContagemResultado_HoraCnt ;
      private String A1606ContagemResultado_CntPrpPesNom ;
      private String AV158UserName ;
      private String AV162EmailText ;
      private String AV164Resultado ;
      private DateTime AV294ContagemResultado_DataPrevista1 ;
      private DateTime AV289ContagemResultado_DataPrevista_To1 ;
      private DateTime AV295ContagemResultado_DataPrevista2 ;
      private DateTime AV290ContagemResultado_DataPrevista_To2 ;
      private DateTime AV296ContagemResultado_DataPrevista3 ;
      private DateTime AV291ContagemResultado_DataPrevista_To3 ;
      private DateTime AV297ContagemResultado_DataPrevista4 ;
      private DateTime AV292ContagemResultado_DataPrevista_To4 ;
      private DateTime AV298ContagemResultado_DataPrevista5 ;
      private DateTime AV293ContagemResultado_DataPrevista_To5 ;
      private DateTime AV64ServerNow ;
      private DateTime A564Lote_Data ;
      private DateTime GXt_dtime3 ;
      private DateTime AV458ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista ;
      private DateTime AV301TFContagemResultado_DataPrevista ;
      private DateTime AV459ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to ;
      private DateTime AV302TFContagemResultado_DataPrevista_To ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A1348ContagemResultado_DataHomologacao ;
      private DateTime AV17ContagemResultado_DataCnt1 ;
      private DateTime AV14ContagemResultado_DataCnt_To1 ;
      private DateTime AV283ContagemResultado_DataDmn1 ;
      private DateTime AV278ContagemResultado_DataDmn_To1 ;
      private DateTime AV18ContagemResultado_DataCnt2 ;
      private DateTime AV15ContagemResultado_DataCnt_To2 ;
      private DateTime AV284ContagemResultado_DataDmn2 ;
      private DateTime AV279ContagemResultado_DataDmn_To2 ;
      private DateTime AV19ContagemResultado_DataCnt3 ;
      private DateTime AV16ContagemResultado_DataCnt_To3 ;
      private DateTime AV285ContagemResultado_DataDmn3 ;
      private DateTime AV280ContagemResultado_DataDmn_To3 ;
      private DateTime AV108ContagemResultado_DataCnt4 ;
      private DateTime AV107ContagemResultado_DataCnt_To4 ;
      private DateTime AV286ContagemResultado_DataDmn4 ;
      private DateTime AV281ContagemResultado_DataDmn_To4 ;
      private DateTime AV110ContagemResultado_DataCnt5 ;
      private DateTime AV109ContagemResultado_DataCnt_To5 ;
      private DateTime AV287ContagemResultado_DataDmn5 ;
      private DateTime AV282ContagemResultado_DataDmn_To5 ;
      private DateTime A82Contrato_DataVigenciaInicio ;
      private DateTime A842Contrato_DataInicioTA ;
      private DateTime AV98Lote_DataContrato ;
      private DateTime A844Lote_DataContrato ;
      private DateTime Gx_date ;
      private DateTime AV328ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 ;
      private DateTime AV329ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 ;
      private DateTime AV332ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 ;
      private DateTime AV333ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 ;
      private DateTime AV334ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 ;
      private DateTime AV335ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 ;
      private DateTime AV353ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 ;
      private DateTime AV354ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 ;
      private DateTime AV357ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 ;
      private DateTime AV358ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 ;
      private DateTime AV359ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 ;
      private DateTime AV360ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 ;
      private DateTime AV378ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 ;
      private DateTime AV379ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 ;
      private DateTime AV382ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 ;
      private DateTime AV383ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 ;
      private DateTime AV384ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 ;
      private DateTime AV385ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 ;
      private DateTime AV403ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 ;
      private DateTime AV404ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 ;
      private DateTime AV407ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 ;
      private DateTime AV408ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 ;
      private DateTime AV409ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 ;
      private DateTime AV410ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 ;
      private DateTime AV428ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 ;
      private DateTime AV429ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 ;
      private DateTime AV432ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 ;
      private DateTime AV433ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 ;
      private DateTime AV434ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 ;
      private DateTime AV435ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 ;
      private DateTime AV460ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn ;
      private DateTime AV299TFContagemResultado_DataDmn ;
      private DateTime AV461ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to ;
      private DateTime AV300TFContagemResultado_DataDmn_To ;
      private DateTime AV462ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt ;
      private DateTime AV229TFContagemResultado_DataUltCnt ;
      private DateTime AV463ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to ;
      private DateTime AV230TFContagemResultado_DataUltCnt_To ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool AV47DynamicFiltersEnabled2 ;
      private bool AV48DynamicFiltersEnabled3 ;
      private bool AV99DynamicFiltersEnabled4 ;
      private bool AV100DynamicFiltersEnabled5 ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n74Contrato_Codigo ;
      private bool n530Contratada_Lote ;
      private bool n1451Contratada_SS ;
      private bool n842Contrato_DataInicioTA ;
      private bool AV217Contratante_SSAutomatica ;
      private bool n844Lote_DataContrato ;
      private bool n2055Lote_CntUsado ;
      private bool n2056Lote_CntSaldo ;
      private bool n2088Lote_ParecerFinal ;
      private bool n2087Lote_Comentarios ;
      private bool returnInSub ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool AV214Ok ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1452ContagemResultado_SS ;
      private bool n1051ContagemResultado_GlsValor ;
      private bool n1559ContagemResultado_VlrAceite ;
      private bool n457ContagemResultado_Demanda ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n1316ContagemResultadoIndicadores_LoteCod ;
      private bool AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 ;
      private bool AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 ;
      private bool AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 ;
      private bool AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 ;
      private bool A598ContagemResultado_Baseline ;
      private bool A1802ContagemResultado_TemPndHmlg ;
      private bool A517ContagemResultado_Ultima ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n494ContagemResultado_Descricao ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n598ContagemResultado_Baseline ;
      private bool n764ContagemResultado_ServicoGrupo ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n1854ContagemResultado_VlrCnc ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n1348ContagemResultado_DataHomologacao ;
      private bool GXt_boolean1 ;
      private bool AV168Validado ;
      private bool n1604ContagemResultado_CntPrpCod ;
      private bool n1605ContagemResultado_CntPrpPesCod ;
      private bool n1606ContagemResultado_CntPrpPesNom ;
      private String AV60GridStateXML ;
      private String A2088Lote_ParecerFinal ;
      private String A2087Lote_Comentarios ;
      private String AV49DynamicFiltersSelector1 ;
      private String AV27ContagemResultado_OsFsOsFm1 ;
      private String AV187ContagemResultado_Descricao1 ;
      private String AV50DynamicFiltersSelector2 ;
      private String AV28ContagemResultado_OsFsOsFm2 ;
      private String AV188ContagemResultado_Descricao2 ;
      private String AV51DynamicFiltersSelector3 ;
      private String AV29ContagemResultado_OsFsOsFm3 ;
      private String AV189ContagemResultado_Descricao3 ;
      private String AV101DynamicFiltersSelector4 ;
      private String AV112ContagemResultado_OsFsOsFm4 ;
      private String AV190ContagemResultado_Descricao4 ;
      private String AV102DynamicFiltersSelector5 ;
      private String AV119ContagemResultado_OsFsOsFm5 ;
      private String AV191ContagemResultado_Descricao5 ;
      private String A457ContagemResultado_Demanda ;
      private String AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 ;
      private String AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ;
      private String AV346ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ;
      private String AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 ;
      private String AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ;
      private String AV371ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ;
      private String AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 ;
      private String AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ;
      private String AV396ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ;
      private String AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 ;
      private String AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ;
      private String AV421ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ;
      private String AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 ;
      private String AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ;
      private String AV446ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ;
      private String AV452ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ;
      private String AV233TFContagemResultado_DemandaFM ;
      private String AV453ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel ;
      private String AV234TFContagemResultado_DemandaFM_Sel ;
      private String AV454ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ;
      private String AV231TFContagemResultado_Demanda ;
      private String AV455ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel ;
      private String AV232TFContagemResultado_Demanda_Sel ;
      private String AV456ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ;
      private String AV235TFContagemResultado_Descricao ;
      private String AV457ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel ;
      private String AV236TFContagemResultado_Descricao_Sel ;
      private String AV466ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ;
      private String AV241TFContagemResultado_SistemaCoord ;
      private String AV467ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel ;
      private String AV242TFContagemResultado_SistemaCoord_Sel ;
      private String lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ;
      private String lV346ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ;
      private String lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ;
      private String lV371ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ;
      private String lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ;
      private String lV396ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ;
      private String lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ;
      private String lV421ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ;
      private String lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ;
      private String lV446ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ;
      private String lV452ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ;
      private String lV454ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ;
      private String lV456ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ;
      private String lV466ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A515ContagemResultado_SistemaCoord ;
      private IGxSession AV90WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00312_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00312_n1553ContagemResultado_CntSrvCod ;
      private int[] P00312_A456ContagemResultado_Codigo ;
      private int[] P00312_A490ContagemResultado_ContratadaCod ;
      private bool[] P00312_n490ContagemResultado_ContratadaCod ;
      private int[] P00312_A1603ContagemResultado_CntCod ;
      private bool[] P00312_n1603ContagemResultado_CntCod ;
      private int[] P00315_A74Contrato_Codigo ;
      private bool[] P00315_n74Contrato_Codigo ;
      private int[] P00315_A39Contratada_Codigo ;
      private short[] P00315_A530Contratada_Lote ;
      private bool[] P00315_n530Contratada_Lote ;
      private int[] P00315_A1451Contratada_SS ;
      private bool[] P00315_n1451Contratada_SS ;
      private DateTime[] P00315_A82Contrato_DataVigenciaInicio ;
      private int[] P00315_A81Contrato_Quantidade ;
      private DateTime[] P00315_A842Contrato_DataInicioTA ;
      private bool[] P00315_n842Contrato_DataInicioTA ;
      private int[] P00316_A596Lote_Codigo ;
      private int[] P00317_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00317_n597ContagemResultado_LoteAceiteCod ;
      private int[] P00317_A456ContagemResultado_Codigo ;
      private int[] P00318_A39Contratada_Codigo ;
      private int[] P00318_A1451Contratada_SS ;
      private bool[] P00318_n1451Contratada_SS ;
      private short[] P00318_A530Contratada_Lote ;
      private bool[] P00318_n530Contratada_Lote ;
      private int[] P003111_A596Lote_Codigo ;
      private String[] P003113_A484ContagemResultado_StatusDmn ;
      private bool[] P003113_n484ContagemResultado_StatusDmn ;
      private int[] P003113_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P003113_n597ContagemResultado_LoteAceiteCod ;
      private int[] P003113_A1452ContagemResultado_SS ;
      private bool[] P003113_n1452ContagemResultado_SS ;
      private decimal[] P003113_A1051ContagemResultado_GlsValor ;
      private bool[] P003113_n1051ContagemResultado_GlsValor ;
      private decimal[] P003113_A1559ContagemResultado_VlrAceite ;
      private bool[] P003113_n1559ContagemResultado_VlrAceite ;
      private String[] P003113_A457ContagemResultado_Demanda ;
      private bool[] P003113_n457ContagemResultado_Demanda ;
      private int[] P003113_A456ContagemResultado_Codigo ;
      private decimal[] P003113_A512ContagemResultado_ValorPF ;
      private bool[] P003113_n512ContagemResultado_ValorPF ;
      private int[] P003114_A1314ContagemResultadoIndicadores_DemandaCod ;
      private decimal[] P003114_A1323ContagemResultadoIndicadores_Valor ;
      private int[] P003114_A1316ContagemResultadoIndicadores_LoteCod ;
      private bool[] P003114_n1316ContagemResultadoIndicadores_LoteCod ;
      private int[] P003114_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private String[] P003116_A484ContagemResultado_StatusDmn ;
      private bool[] P003116_n484ContagemResultado_StatusDmn ;
      private int[] P003116_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P003116_n597ContagemResultado_LoteAceiteCod ;
      private int[] P003116_A1452ContagemResultado_SS ;
      private bool[] P003116_n1452ContagemResultado_SS ;
      private decimal[] P003116_A1559ContagemResultado_VlrAceite ;
      private bool[] P003116_n1559ContagemResultado_VlrAceite ;
      private int[] P003116_A456ContagemResultado_Codigo ;
      private int[] P003118_A1553ContagemResultado_CntSrvCod ;
      private bool[] P003118_n1553ContagemResultado_CntSrvCod ;
      private bool[] P003118_A517ContagemResultado_Ultima ;
      private decimal[] P003118_A512ContagemResultado_ValorPF ;
      private bool[] P003118_n512ContagemResultado_ValorPF ;
      private String[] P003118_A801ContagemResultado_ServicoSigla ;
      private bool[] P003118_n801ContagemResultado_ServicoSigla ;
      private String[] P003118_A515ContagemResultado_SistemaCoord ;
      private bool[] P003118_n515ContagemResultado_SistemaCoord ;
      private String[] P003118_A803ContagemResultado_ContratadaSigla ;
      private bool[] P003118_n803ContagemResultado_ContratadaSigla ;
      private int[] P003118_A1603ContagemResultado_CntCod ;
      private bool[] P003118_n1603ContagemResultado_CntCod ;
      private String[] P003118_A494ContagemResultado_Descricao ;
      private bool[] P003118_n494ContagemResultado_Descricao ;
      private String[] P003118_A1046ContagemResultado_Agrupador ;
      private bool[] P003118_n1046ContagemResultado_Agrupador ;
      private int[] P003118_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P003118_n468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P003118_A598ContagemResultado_Baseline ;
      private bool[] P003118_n598ContagemResultado_Baseline ;
      private int[] P003118_A764ContagemResultado_ServicoGrupo ;
      private bool[] P003118_n764ContagemResultado_ServicoGrupo ;
      private int[] P003118_A489ContagemResultado_SistemaCod ;
      private bool[] P003118_n489ContagemResultado_SistemaCod ;
      private int[] P003118_A508ContagemResultado_Owner ;
      private int[] P003118_A601ContagemResultado_Servico ;
      private bool[] P003118_n601ContagemResultado_Servico ;
      private DateTime[] P003118_A1351ContagemResultado_DataPrevista ;
      private bool[] P003118_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P003118_A471ContagemResultado_DataDmn ;
      private String[] P003118_A493ContagemResultado_DemandaFM ;
      private bool[] P003118_n493ContagemResultado_DemandaFM ;
      private String[] P003118_A457ContagemResultado_Demanda ;
      private bool[] P003118_n457ContagemResultado_Demanda ;
      private String[] P003118_A484ContagemResultado_StatusDmn ;
      private bool[] P003118_n484ContagemResultado_StatusDmn ;
      private decimal[] P003118_A1854ContagemResultado_VlrCnc ;
      private bool[] P003118_n1854ContagemResultado_VlrCnc ;
      private int[] P003118_A490ContagemResultado_ContratadaCod ;
      private bool[] P003118_n490ContagemResultado_ContratadaCod ;
      private int[] P003118_A52Contratada_AreaTrabalhoCod ;
      private bool[] P003118_n52Contratada_AreaTrabalhoCod ;
      private DateTime[] P003118_A1348ContagemResultado_DataHomologacao ;
      private bool[] P003118_n1348ContagemResultado_DataHomologacao ;
      private short[] P003118_A483ContagemResultado_StatusCnt ;
      private decimal[] P003118_A683ContagemResultado_PFLFMUltima ;
      private decimal[] P003118_A685ContagemResultado_PFLFSUltima ;
      private decimal[] P003118_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P003118_A684ContagemResultado_PFBFSUltima ;
      private int[] P003118_A584ContagemResultado_ContadorFM ;
      private DateTime[] P003118_A566ContagemResultado_DataUltCnt ;
      private short[] P003118_A531ContagemResultado_StatusUltCnt ;
      private int[] P003118_A456ContagemResultado_Codigo ;
      private DateTime[] P003118_A473ContagemResultado_DataCnt ;
      private String[] P003118_A511ContagemResultado_HoraCnt ;
      private String[] P003119_A484ContagemResultado_StatusDmn ;
      private bool[] P003119_n484ContagemResultado_StatusDmn ;
      private DateTime[] P003119_A1348ContagemResultado_DataHomologacao ;
      private bool[] P003119_n1348ContagemResultado_DataHomologacao ;
      private int[] P003119_A456ContagemResultado_Codigo ;
      private short[] P003120_A483ContagemResultado_StatusCnt ;
      private int[] P003120_A456ContagemResultado_Codigo ;
      private DateTime[] P003120_A473ContagemResultado_DataCnt ;
      private String[] P003120_A511ContagemResultado_HoraCnt ;
      private int[] P003121_A456ContagemResultado_Codigo ;
      private String[] P003121_A484ContagemResultado_StatusDmn ;
      private bool[] P003121_n484ContagemResultado_StatusDmn ;
      private String[] P003121_A457ContagemResultado_Demanda ;
      private bool[] P003121_n457ContagemResultado_Demanda ;
      private DateTime[] P003121_A1348ContagemResultado_DataHomologacao ;
      private bool[] P003121_n1348ContagemResultado_DataHomologacao ;
      private String[] P003123_A484ContagemResultado_StatusDmn ;
      private bool[] P003123_n484ContagemResultado_StatusDmn ;
      private DateTime[] P003123_A1348ContagemResultado_DataHomologacao ;
      private bool[] P003123_n1348ContagemResultado_DataHomologacao ;
      private int[] P003123_A456ContagemResultado_Codigo ;
      private int[] P003124_A490ContagemResultado_ContratadaCod ;
      private bool[] P003124_n490ContagemResultado_ContratadaCod ;
      private int[] P003124_A1553ContagemResultado_CntSrvCod ;
      private bool[] P003124_n1553ContagemResultado_CntSrvCod ;
      private int[] P003124_A1604ContagemResultado_CntPrpCod ;
      private bool[] P003124_n1604ContagemResultado_CntPrpCod ;
      private int[] P003124_A1605ContagemResultado_CntPrpPesCod ;
      private bool[] P003124_n1605ContagemResultado_CntPrpPesCod ;
      private int[] P003124_A456ContagemResultado_Codigo ;
      private int[] P003124_A1603ContagemResultado_CntCod ;
      private bool[] P003124_n1603ContagemResultado_CntCod ;
      private String[] P003124_A803ContagemResultado_ContratadaSigla ;
      private bool[] P003124_n803ContagemResultado_ContratadaSigla ;
      private String[] P003124_A1606ContagemResultado_CntPrpPesNom ;
      private bool[] P003124_n1606ContagemResultado_CntPrpPesNom ;
      private int[] P003125_A1Usuario_Codigo ;
      private int[] P003127_A40000GXC1 ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV89Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV159Usuarios ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV468ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV245TFContagemResultado_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV165Attachments ;
      private wwpbaseobjects.SdtWWPGridState AV58GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV59GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV66WWPContext ;
   }

   public class prc_agruparparafaturamento__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P003113( IGxContext context ,
                                              int A456ContagemResultado_Codigo ,
                                              IGxCollection AV89Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_StatusDmn], [ContagemResultado_LoteAceiteCod], [ContagemResultado_SS], [ContagemResultado_GlsValor], [ContagemResultado_VlrAceite], [ContagemResultado_Demanda], [ContagemResultado_Codigo], [ContagemResultado_ValorPF] FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV89Codigos, "[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_Codigo]";
         GXv_Object4[0] = scmdbuf;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P003118( IGxContext context ,
                                              String A484ContagemResultado_StatusDmn ,
                                              IGxCollection AV468ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels ,
                                              int AV66WWPContext_gxTpr_Contratada_codigo ,
                                              String AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 ,
                                              String AV330ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 ,
                                              short AV327ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 ,
                                              String AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ,
                                              DateTime AV332ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 ,
                                              DateTime AV333ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 ,
                                              DateTime AV334ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 ,
                                              DateTime AV335ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 ,
                                              int AV336ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 ,
                                              int AV338ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 ,
                                              int AV339ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 ,
                                              int AV324ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod ,
                                              int AV340ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 ,
                                              String AV341ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 ,
                                              int AV342ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 ,
                                              String AV345ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 ,
                                              String AV346ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ,
                                              int AV347ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 ,
                                              int AV349ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 ,
                                              bool AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 ,
                                              String AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 ,
                                              String AV355ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 ,
                                              short AV352ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 ,
                                              String AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ,
                                              DateTime AV357ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 ,
                                              DateTime AV358ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 ,
                                              DateTime AV359ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 ,
                                              DateTime AV360ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 ,
                                              int AV361ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 ,
                                              int AV363ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 ,
                                              int AV364ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 ,
                                              int AV365ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 ,
                                              String AV366ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 ,
                                              int AV367ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 ,
                                              String AV370ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 ,
                                              String AV371ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ,
                                              int AV372ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 ,
                                              int AV374ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 ,
                                              bool AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 ,
                                              String AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 ,
                                              String AV380ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 ,
                                              short AV377ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 ,
                                              String AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ,
                                              DateTime AV382ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 ,
                                              DateTime AV383ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 ,
                                              DateTime AV384ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 ,
                                              DateTime AV385ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 ,
                                              int AV386ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 ,
                                              int AV388ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 ,
                                              int AV389ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 ,
                                              int AV390ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 ,
                                              String AV391ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 ,
                                              int AV392ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 ,
                                              String AV395ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 ,
                                              String AV396ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ,
                                              int AV397ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 ,
                                              int AV399ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 ,
                                              bool AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 ,
                                              String AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 ,
                                              String AV405ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 ,
                                              short AV402ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 ,
                                              String AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ,
                                              DateTime AV407ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 ,
                                              DateTime AV408ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 ,
                                              DateTime AV409ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 ,
                                              DateTime AV410ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 ,
                                              int AV411ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 ,
                                              int AV413ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 ,
                                              int AV414ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 ,
                                              int AV415ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 ,
                                              String AV416ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 ,
                                              int AV417ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 ,
                                              String AV420ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 ,
                                              String AV421ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ,
                                              int AV422ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 ,
                                              int AV424ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 ,
                                              bool AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 ,
                                              String AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 ,
                                              String AV430ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 ,
                                              short AV427ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 ,
                                              String AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ,
                                              DateTime AV432ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 ,
                                              DateTime AV433ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 ,
                                              DateTime AV434ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 ,
                                              DateTime AV435ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 ,
                                              int AV436ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 ,
                                              int AV438ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 ,
                                              int AV439ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 ,
                                              int AV440ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 ,
                                              String AV441ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 ,
                                              int AV442ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 ,
                                              String AV445ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 ,
                                              String AV446ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ,
                                              int AV447ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 ,
                                              int AV449ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 ,
                                              String AV451ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel ,
                                              String AV450ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ,
                                              String AV453ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel ,
                                              String AV452ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ,
                                              String AV455ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel ,
                                              String AV454ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ,
                                              String AV457ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel ,
                                              String AV456ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ,
                                              DateTime AV458ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista ,
                                              DateTime AV459ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to ,
                                              DateTime AV460ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn ,
                                              DateTime AV461ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to ,
                                              String AV465ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel ,
                                              String AV464ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ,
                                              String AV467ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel ,
                                              String AV466ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ,
                                              int AV468ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels_Count ,
                                              short AV469ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel ,
                                              String AV471ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel ,
                                              String AV470ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ,
                                              decimal AV474ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf ,
                                              decimal AV475ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              String A457ContagemResultado_Demanda ,
                                              String A493ContagemResultado_DemandaFM ,
                                              DateTime A471ContagemResultado_DataDmn ,
                                              DateTime A1351ContagemResultado_DataPrevista ,
                                              int A601ContagemResultado_Servico ,
                                              int A489ContagemResultado_SistemaCod ,
                                              int A764ContagemResultado_ServicoGrupo ,
                                              bool A598ContagemResultado_Baseline ,
                                              int A468ContagemResultado_NaoCnfDmnCod ,
                                              String A1046ContagemResultado_Agrupador ,
                                              String A494ContagemResultado_Descricao ,
                                              int A508ContagemResultado_Owner ,
                                              int A1603ContagemResultado_CntCod ,
                                              String A803ContagemResultado_ContratadaSigla ,
                                              String A515ContagemResultado_SistemaCoord ,
                                              String A801ContagemResultado_ServicoSigla ,
                                              decimal A512ContagemResultado_ValorPF ,
                                              short A531ContagemResultado_StatusUltCnt ,
                                              decimal A1854ContagemResultado_VlrCnc ,
                                              DateTime AV328ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 ,
                                              DateTime A566ContagemResultado_DataUltCnt ,
                                              DateTime AV329ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 ,
                                              int AV337ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 ,
                                              int A584ContagemResultado_ContadorFM ,
                                              decimal A684ContagemResultado_PFBFSUltima ,
                                              decimal A682ContagemResultado_PFBFMUltima ,
                                              decimal A685ContagemResultado_PFLFSUltima ,
                                              decimal A683ContagemResultado_PFLFMUltima ,
                                              String AV348ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 ,
                                              bool A1802ContagemResultado_TemPndHmlg ,
                                              DateTime AV353ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 ,
                                              DateTime AV354ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 ,
                                              int AV362ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 ,
                                              String AV373ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 ,
                                              DateTime AV378ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 ,
                                              DateTime AV379ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 ,
                                              int AV387ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 ,
                                              String AV398ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 ,
                                              DateTime AV403ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 ,
                                              DateTime AV404ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 ,
                                              int AV412ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 ,
                                              String AV423ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 ,
                                              DateTime AV428ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 ,
                                              DateTime AV429ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 ,
                                              int AV437ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 ,
                                              String AV448ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 ,
                                              DateTime AV462ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt ,
                                              DateTime AV463ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to ,
                                              decimal AV472ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ,
                                              decimal A574ContagemResultado_PFFinal ,
                                              decimal AV473ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ,
                                              bool A517ContagemResultado_Ultima ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int AV66WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [211] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Ultima], T2.[ContagemResultado_ValorPF], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T5.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T6.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T2.[ContagemResultado_Descricao], T2.[ContagemResultado_Agrupador], T2.[ContagemResultado_NaoCnfDmnCod], T2.[ContagemResultado_Baseline], T4.[ServicoGrupo_Codigo] AS ContagemResultado_ServicoGrupo, T2.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[ContagemResultado_Owner], T3.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContagemResultado_DataPrevista], T2.[ContagemResultado_DataDmn], T2.[ContagemResultado_DemandaFM], T2.[ContagemResultado_Demanda], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_VlrCnc], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Contratada_AreaTrabalhoCod], T2.[ContagemResultado_DataHomologacao], T1.[ContagemResultado_StatusCnt], COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T7.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM (((((([ContagemResultadoContagens]";
         scmdbuf = scmdbuf + " T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T2.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( Not ( @AV330ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 = 'N' or @AV355ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 = 'N' or @AV380ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 = 'N' or @AV405ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 = 'N' or @AV430ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 = 'N')) or ( COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) = 5 or T2.[ContagemResultado_VlrCnc] > 0 or COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) = 8))";
         scmdbuf = scmdbuf + " and (Not ( @AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV328ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV328ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV329ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV329ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV337ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV337ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV337ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (@AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 <> 'CONTAGEMRESULTADO_PFBFM' or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (@AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 <> 'CONTAGEMRESULTADO_PFBFS' or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV353ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV353ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV354ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV354ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV362ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV362ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV362ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV378ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV378ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV379ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV379ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV387ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV387ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV387ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3)))";
         scmdbuf = scmdbuf + " and (Not ( @AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV403ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV403ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4))";
         scmdbuf = scmdbuf + " and (Not ( @AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV404ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV404ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4))";
         scmdbuf = scmdbuf + " and (Not ( @AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV412ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV412ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV412ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4)))";
         scmdbuf = scmdbuf + " and (Not ( @AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV428ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV428ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5))";
         scmdbuf = scmdbuf + " and (Not ( @AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV429ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV429ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5))";
         scmdbuf = scmdbuf + " and (Not ( @AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV437ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV437ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV437ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5)))";
         scmdbuf = scmdbuf + " and (Not ( @AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and ((@AV462ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV462ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV463ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV463ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Ultima] = 1)";
         scmdbuf = scmdbuf + " and (T6.[Contratada_AreaTrabalhoCod] = @AV66WWPC_1Areatrabalho_codigo)";
         if ( AV66WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV66WWPC_2Contratada_codigo)";
         }
         else
         {
            GXv_int6[90] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV330ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1)) && ! ( StringUtil.StrCmp(AV330ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV330ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int6[91] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV330ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L' or T2.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV327ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] = @AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int6[92] = 1;
            GXv_int6[93] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV327ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] like @lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int6[94] = 1;
            GXv_int6[95] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV327ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] like '%' + @lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int6[96] = 1;
            GXv_int6[97] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV332ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV332ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int6[98] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV333ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV333ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int6[99] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV334ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV334ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1)";
         }
         else
         {
            GXv_int6[100] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV335ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV335ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1))";
         }
         else
         {
            GXv_int6[101] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV336ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV336ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int6[102] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV338ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV338ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int6[103] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV339ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 > 0 ) && ( AV324ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV339ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int6[104] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV340ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV340ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1)";
         }
         else
         {
            GXv_int6[105] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV341ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV341ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV342ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV342ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1)";
         }
         else
         {
            GXv_int6[106] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV345ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV345ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int6[107] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV346ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV346ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 + '%')";
         }
         else
         {
            GXv_int6[108] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV347ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Owner] = @AV347ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1)";
         }
         else
         {
            GXv_int6[109] = 1;
         }
         if ( ( StringUtil.StrCmp(AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV349ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV349ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1)";
         }
         else
         {
            GXv_int6[110] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV355ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2)) && ! ( StringUtil.StrCmp(AV355ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV355ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int6[111] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV355ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L' or T2.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV352ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] = @AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int6[112] = 1;
            GXv_int6[113] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV352ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] like @lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int6[114] = 1;
            GXv_int6[115] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV352ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] like '%' + @lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int6[116] = 1;
            GXv_int6[117] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV357ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV357ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int6[118] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV358ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV358ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int6[119] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV359ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV359ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2)";
         }
         else
         {
            GXv_int6[120] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV360ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV360ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2))";
         }
         else
         {
            GXv_int6[121] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV361ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV361ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int6[122] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV363ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV363ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int6[123] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV364ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 > 0 ) && ( AV324ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV364ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int6[124] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV365ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV365ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2)";
         }
         else
         {
            GXv_int6[125] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV366ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV366ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV367ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV367ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2)";
         }
         else
         {
            GXv_int6[126] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV370ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV370ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int6[127] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV371ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV371ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 + '%')";
         }
         else
         {
            GXv_int6[128] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV372ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Owner] = @AV372ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2)";
         }
         else
         {
            GXv_int6[129] = 1;
         }
         if ( AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV374ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV374ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2)";
         }
         else
         {
            GXv_int6[130] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV380ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3)) && ! ( StringUtil.StrCmp(AV380ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV380ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int6[131] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV380ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L' or T2.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV377ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] = @AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int6[132] = 1;
            GXv_int6[133] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV377ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] like @lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int6[134] = 1;
            GXv_int6[135] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV377ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] like '%' + @lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int6[136] = 1;
            GXv_int6[137] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV382ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV382ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int6[138] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV383ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV383ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int6[139] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV384ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV384ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3)";
         }
         else
         {
            GXv_int6[140] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV385ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV385ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3))";
         }
         else
         {
            GXv_int6[141] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV386ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV386ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int6[142] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV388ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV388ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int6[143] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV389ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 > 0 ) && ( AV324ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV389ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int6[144] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV390ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV390ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3)";
         }
         else
         {
            GXv_int6[145] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV391ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV391ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV392ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV392ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3)";
         }
         else
         {
            GXv_int6[146] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV395ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV395ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int6[147] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV396ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV396ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 + '%')";
         }
         else
         {
            GXv_int6[148] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV397ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Owner] = @AV397ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3)";
         }
         else
         {
            GXv_int6[149] = 1;
         }
         if ( AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV399ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV399ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3)";
         }
         else
         {
            GXv_int6[150] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV405ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4)) && ! ( StringUtil.StrCmp(AV405ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV405ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4)";
         }
         else
         {
            GXv_int6[151] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV405ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L' or T2.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV402ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 or T2.[ContagemResultado_DemandaFM] = @AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int6[152] = 1;
            GXv_int6[153] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV402ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 or T2.[ContagemResultado_DemandaFM] like @lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int6[154] = 1;
            GXv_int6[155] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV402ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 or T2.[ContagemResultado_DemandaFM] like '%' + @lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int6[156] = 1;
            GXv_int6[157] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV407ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV407ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4)";
         }
         else
         {
            GXv_int6[158] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV408ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV408ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4)";
         }
         else
         {
            GXv_int6[159] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV409ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV409ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4)";
         }
         else
         {
            GXv_int6[160] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV410ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV410ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4))";
         }
         else
         {
            GXv_int6[161] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV411ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV411ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4)";
         }
         else
         {
            GXv_int6[162] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV413ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV413ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4)";
         }
         else
         {
            GXv_int6[163] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV414ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 > 0 ) && ( AV324ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV414ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4)";
         }
         else
         {
            GXv_int6[164] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV415ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV415ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4)";
         }
         else
         {
            GXv_int6[165] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV416ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV416ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV417ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV417ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4)";
         }
         else
         {
            GXv_int6[166] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV420ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV420ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4)";
         }
         else
         {
            GXv_int6[167] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV421ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV421ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 + '%')";
         }
         else
         {
            GXv_int6[168] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV422ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Owner] = @AV422ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4)";
         }
         else
         {
            GXv_int6[169] = 1;
         }
         if ( AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV424ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV424ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4)";
         }
         else
         {
            GXv_int6[170] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV430ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5)) && ! ( StringUtil.StrCmp(AV430ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV430ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5)";
         }
         else
         {
            GXv_int6[171] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV430ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L' or T2.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV427ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 or T2.[ContagemResultado_DemandaFM] = @AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int6[172] = 1;
            GXv_int6[173] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV427ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 or T2.[ContagemResultado_DemandaFM] like @lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int6[174] = 1;
            GXv_int6[175] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV427ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 or T2.[ContagemResultado_DemandaFM] like '%' + @lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int6[176] = 1;
            GXv_int6[177] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV432ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV432ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5)";
         }
         else
         {
            GXv_int6[178] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV433ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV433ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5)";
         }
         else
         {
            GXv_int6[179] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV434ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV434ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5)";
         }
         else
         {
            GXv_int6[180] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV435ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV435ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5))";
         }
         else
         {
            GXv_int6[181] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV436ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV436ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5)";
         }
         else
         {
            GXv_int6[182] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV438ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV438ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5)";
         }
         else
         {
            GXv_int6[183] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV439ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 > 0 ) && ( AV324ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV439ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5)";
         }
         else
         {
            GXv_int6[184] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV440ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV440ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5)";
         }
         else
         {
            GXv_int6[185] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV441ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV441ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV442ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV442ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5)";
         }
         else
         {
            GXv_int6[186] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV445ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV445ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5)";
         }
         else
         {
            GXv_int6[187] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV446ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV446ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 + '%')";
         }
         else
         {
            GXv_int6[188] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV447ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Owner] = @AV447ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5)";
         }
         else
         {
            GXv_int6[189] = 1;
         }
         if ( AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV449ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV449ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5)";
         }
         else
         {
            GXv_int6[190] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV451ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV450ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like @lV450ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador)";
         }
         else
         {
            GXv_int6[191] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV451ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV451ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel)";
         }
         else
         {
            GXv_int6[192] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV453ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV452ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV452ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm)";
         }
         else
         {
            GXv_int6[193] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV453ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV453ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel)";
         }
         else
         {
            GXv_int6[194] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV455ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV454ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV454ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda)";
         }
         else
         {
            GXv_int6[195] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV455ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV455ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel)";
         }
         else
         {
            GXv_int6[196] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV457ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV456ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like @lV456ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao)";
         }
         else
         {
            GXv_int6[197] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV457ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] = @AV457ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel)";
         }
         else
         {
            GXv_int6[198] = 1;
         }
         if ( ! (DateTime.MinValue==AV458ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV458ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista)";
         }
         else
         {
            GXv_int6[199] = 1;
         }
         if ( ! (DateTime.MinValue==AV459ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] <= @AV459ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to)";
         }
         else
         {
            GXv_int6[200] = 1;
         }
         if ( ! (DateTime.MinValue==AV460ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV460ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int6[201] = 1;
         }
         if ( ! (DateTime.MinValue==AV461ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV461ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int6[202] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV465ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV464ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] like @lV464ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int6[203] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV465ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] = @AV465ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int6[204] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV467ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV466ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Coordenacao] like @lV466ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord)";
         }
         else
         {
            GXv_int6[205] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV467ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Coordenacao] = @AV467ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel)";
         }
         else
         {
            GXv_int6[206] = 1;
         }
         if ( AV468ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV468ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels, "T2.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV469ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel == 1 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV469ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel == 2 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 0)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV471ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV470ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] like @lV470ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int6[207] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV471ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] = @AV471ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int6[208] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV474ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ValorPF] >= @AV474ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf)";
         }
         else
         {
            GXv_int6[209] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV475ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ValorPF] <= @AV475ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to)";
         }
         else
         {
            GXv_int6[210] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      protected Object[] conditional_P003121( IGxContext context ,
                                              int A456ContagemResultado_Codigo ,
                                              IGxCollection AV89Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_Codigo], [ContagemResultado_StatusDmn], [ContagemResultado_Demanda], [ContagemResultado_DataHomologacao] FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV89Codigos, "[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_Codigo]";
         GXv_Object8[0] = scmdbuf;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P003124( IGxContext context ,
                                              int A456ContagemResultado_Codigo ,
                                              IGxCollection AV89Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Contrato_PrepostoCod] AS ContagemResultado_CntPrpCod, T5.[Usuario_PessoaCod] AS ContagemResultado_CntPrpPesCod, T1.[ContagemResultado_Codigo], T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T2.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T6.[Pessoa_Nome] AS ContagemResultado_CntPrpPesNom FROM ((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo]) LEFT JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T4.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV89Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo]";
         GXv_Object10[0] = scmdbuf;
         return GXv_Object10 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 9 :
                     return conditional_P003113(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 13 :
                     return conditional_P003118(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (bool)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (short)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (bool)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (short)dynConstraints[44] , (String)dynConstraints[45] , (DateTime)dynConstraints[46] , (DateTime)dynConstraints[47] , (DateTime)dynConstraints[48] , (DateTime)dynConstraints[49] , (int)dynConstraints[50] , (int)dynConstraints[51] , (int)dynConstraints[52] , (int)dynConstraints[53] , (String)dynConstraints[54] , (int)dynConstraints[55] , (String)dynConstraints[56] , (String)dynConstraints[57] , (int)dynConstraints[58] , (int)dynConstraints[59] , (bool)dynConstraints[60] , (String)dynConstraints[61] , (String)dynConstraints[62] , (short)dynConstraints[63] , (String)dynConstraints[64] , (DateTime)dynConstraints[65] , (DateTime)dynConstraints[66] , (DateTime)dynConstraints[67] , (DateTime)dynConstraints[68] , (int)dynConstraints[69] , (int)dynConstraints[70] , (int)dynConstraints[71] , (int)dynConstraints[72] , (String)dynConstraints[73] , (int)dynConstraints[74] , (String)dynConstraints[75] , (String)dynConstraints[76] , (int)dynConstraints[77] , (int)dynConstraints[78] , (bool)dynConstraints[79] , (String)dynConstraints[80] , (String)dynConstraints[81] , (short)dynConstraints[82] , (String)dynConstraints[83] , (DateTime)dynConstraints[84] , (DateTime)dynConstraints[85] , (DateTime)dynConstraints[86] , (DateTime)dynConstraints[87] , (int)dynConstraints[88] , (int)dynConstraints[89] , (int)dynConstraints[90] , (int)dynConstraints[91] , (String)dynConstraints[92] , (int)dynConstraints[93] , (String)dynConstraints[94] , (String)dynConstraints[95] , (int)dynConstraints[96] , (int)dynConstraints[97] , (String)dynConstraints[98] , (String)dynConstraints[99] , (String)dynConstraints[100] , (String)dynConstraints[101] , (String)dynConstraints[102] , (String)dynConstraints[103] , (String)dynConstraints[104] , (String)dynConstraints[105] , (DateTime)dynConstraints[106] , (DateTime)dynConstraints[107] , (DateTime)dynConstraints[108] , (DateTime)dynConstraints[109] , (String)dynConstraints[110] , (String)dynConstraints[111] , (String)dynConstraints[112] , (String)dynConstraints[113] , (int)dynConstraints[114] , (short)dynConstraints[115] , (String)dynConstraints[116] , (String)dynConstraints[117] , (decimal)dynConstraints[118] , (decimal)dynConstraints[119] , (int)dynConstraints[120] , (String)dynConstraints[121] , (String)dynConstraints[122] , (DateTime)dynConstraints[123] , (DateTime)dynConstraints[124] , (int)dynConstraints[125] , (int)dynConstraints[126] , (int)dynConstraints[127] , (bool)dynConstraints[128] , (int)dynConstraints[129] , (String)dynConstraints[130] , (String)dynConstraints[131] , (int)dynConstraints[132] , (int)dynConstraints[133] , (String)dynConstraints[134] , (String)dynConstraints[135] , (String)dynConstraints[136] , (decimal)dynConstraints[137] , (short)dynConstraints[138] , (decimal)dynConstraints[139] , (DateTime)dynConstraints[140] , (DateTime)dynConstraints[141] , (DateTime)dynConstraints[142] , (int)dynConstraints[143] , (int)dynConstraints[144] , (decimal)dynConstraints[145] , (decimal)dynConstraints[146] , (decimal)dynConstraints[147] , (decimal)dynConstraints[148] , (String)dynConstraints[149] , (bool)dynConstraints[150] , (DateTime)dynConstraints[151] , (DateTime)dynConstraints[152] , (int)dynConstraints[153] , (String)dynConstraints[154] , (DateTime)dynConstraints[155] , (DateTime)dynConstraints[156] , (int)dynConstraints[157] , (String)dynConstraints[158] , (DateTime)dynConstraints[159] , (DateTime)dynConstraints[160] , (int)dynConstraints[161] , (String)dynConstraints[162] , (DateTime)dynConstraints[163] , (DateTime)dynConstraints[164] , (int)dynConstraints[165] , (String)dynConstraints[166] , (DateTime)dynConstraints[167] , (DateTime)dynConstraints[168] , (decimal)dynConstraints[169] , (decimal)dynConstraints[170] , (decimal)dynConstraints[171] , (bool)dynConstraints[172] , (int)dynConstraints[173] , (int)dynConstraints[174] );
               case 16 :
                     return conditional_P003121(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 19 :
                     return conditional_P003124(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new BatchUpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new BatchUpdateCursor(def[14])
         ,new BatchUpdateCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new BatchUpdateCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00312 ;
          prmP00312 = new Object[] {
          new Object[] {"@AV198PrimeiroID",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00315 ;
          prmP00315 = new Object[] {
          new Object[] {"@AV213Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00316 ;
          prmP00316 = new Object[] {
          new Object[] {"@Lote_Numero",SqlDbType.Char,10,0} ,
          new Object[] {"@Lote_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Lote_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Lote_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Lote_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Lote_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Lote_DataContrato",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_CntUsado",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Lote_CntSaldo",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Lote_ParecerFinal",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Lote_Comentarios",SqlDbType.VarChar,2097152,0}
          } ;
          Object[] prmP00317 ;
          prmP00317 = new Object[] {
          new Object[] {"@AV21ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00318 ;
          prmP00318 = new Object[] {
          new Object[] {"@AV213Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00319 ;
          prmP00319 = new Object[] {
          new Object[] {"@Contratada_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@Contratada_Lote",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003110 ;
          prmP003110 = new Object[] {
          new Object[] {"@Contratada_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@Contratada_Lote",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003111 ;
          prmP003111 = new Object[] {
          new Object[] {"@AV21ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003112 ;
          prmP003112 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003114 ;
          prmP003114 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003115 ;
          prmP003115 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003116 ;
          prmP003116 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_VlrAceite",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003119 ;
          prmP003119 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003120 ;
          prmP003120 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP003122 ;
          prmP003122 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003123 ;
          prmP003123 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003125 ;
          prmP003125 = new Object[] {
          new Object[] {"@AV66WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP003127 ;
          prmP003127 = new Object[] {
          new Object[] {"@AV149Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003113 ;
          prmP003113 = new Object[] {
          } ;
          Object[] prmP003118 ;
          prmP003118 = new Object[] {
          new Object[] {"@AV330ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV355ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV380ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV405ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV430ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV328ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV328ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV329ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV329ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV337ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV337ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV337ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV326ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV353ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV353ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV354ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV354ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV362ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV362ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV362ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV350ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV351ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV378ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV378ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV379ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV379ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV387ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV387ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV387ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV375ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV376ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV403ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV403ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV404ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV404ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV412ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV412ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV412ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV400ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV401ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV428ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV428ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV429ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV429ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV437ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV437ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV437ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV425ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV426ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV462ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV462ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV463ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV463ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV66WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV66WWPC_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV330ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV331ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV332ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV333ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV334ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV335ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV336ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV338ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV339ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV340ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV342ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV345ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV346ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV347ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV349ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV355ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV356ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV357ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV358ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV359ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV360ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV361ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV363ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV364ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV365ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV367ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV370ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV371ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV372ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV374ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV380ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV381ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV382ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV383ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV384ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV385ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV386ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV388ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV389ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV390ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV392ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV395ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV396ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV397ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV399ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV405ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV406ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV407ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV408ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV409ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV410ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV411ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV413ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV414ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV415ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV417ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV420ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4",SqlDbType.Char,15,0} ,
          new Object[] {"@lV421ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV422ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV424ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV430ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV431ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV432ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV433ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV434ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV435ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV436ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV438ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV439ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV440ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV442ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV445ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5",SqlDbType.Char,15,0} ,
          new Object[] {"@lV446ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV447ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV449ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV450ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV451ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV452ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV453ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV454ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV455ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV456ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV457ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV458ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV459ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV460ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV461ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV464ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV465ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV466ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV467ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV470ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV471ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV474ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV475ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP003121 ;
          prmP003121 = new Object[] {
          } ;
          Object[] prmP003124 ;
          prmP003124 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00312", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV198PrimeiroID ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00312,1,0,false,true )
             ,new CursorDef("P00315", "SELECT TOP 1 T1.[Contrato_Codigo], T1.[Contratada_Codigo], T3.[Contratada_Lote], T3.[Contratada_SS], T1.[Contrato_DataVigenciaInicio], T1.[Contrato_Quantidade], COALESCE( T2.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA FROM (([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT T4.[ContratoTermoAditivo_DataInicio], T4.[Contrato_Codigo], T4.[ContratoTermoAditivo_Codigo], T5.[GXC5] AS GXC5 FROM ([ContratoTermoAditivo] T4 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC5, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T5 ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo]) WHERE T4.[ContratoTermoAditivo_Codigo] = T5.[GXC5] ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[Contratada_Codigo]) WHERE T1.[Contratada_Codigo] = @AV213Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00315,1,0,false,true )
             ,new CursorDef("P00316", "INSERT INTO [Lote]([Lote_Numero], [Lote_AreaTrabalhoCod], [Lote_Nome], [Lote_Data], [Lote_UserCod], [Lote_ValorPF], [Lote_DataContrato], [Lote_CntUsado], [Lote_CntSaldo], [Lote_ParecerFinal], [Lote_Comentarios], [Lote_NFe], [Lote_DataNfe], [Lote_LiqData], [Lote_LiqBanco], [Lote_LiqValor], [Lote_NFeArq], [Lote_NFeNomeArq], [Lote_NFeTipoArq], [Lote_PrevPagamento], [Lote_NFeDataProtocolo], [Lote_GlsData], [Lote_GlsDescricao], [Lote_GlsValor], [Lote_GlsUser], [Lote_GLSigned], [Lote_OSSigned], [Lote_TASigned]) VALUES(@Lote_Numero, @Lote_AreaTrabalhoCod, @Lote_Nome, @Lote_Data, @Lote_UserCod, @Lote_ValorPF, @Lote_DataContrato, @Lote_CntUsado, @Lote_CntSaldo, @Lote_ParecerFinal, @Lote_Comentarios, convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), '', convert(int, 0), CONVERT(varbinary(1), ''), '', '', convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), '', convert(int, 0), convert(int, 0), convert(bit, 0), convert(bit, 0), convert(bit, 0)); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00316)
             ,new CursorDef("P00317", "SELECT TOP 1 [ContagemResultado_LoteAceiteCod], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @AV21ContagemResultado_LoteAceite ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00317,1,0,false,true )
             ,new CursorDef("P00318", "SELECT TOP 1 [Contratada_Codigo], [Contratada_SS], [Contratada_Lote] FROM [Contratada] WITH (UPDLOCK) WHERE [Contratada_Codigo] = @AV213Contratada_Codigo ORDER BY [Contratada_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00318,1,0,true,true )
             ,new CursorDef("P00319", "UPDATE [Contratada] SET [Contratada_SS]=@Contratada_SS, [Contratada_Lote]=@Contratada_Lote  WHERE [Contratada_Codigo] = @Contratada_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00319)
             ,new CursorDef("P003110", "UPDATE [Contratada] SET [Contratada_SS]=@Contratada_SS, [Contratada_Lote]=@Contratada_Lote  WHERE [Contratada_Codigo] = @Contratada_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003110)
             ,new CursorDef("P003111", "SELECT TOP 1 [Lote_Codigo] FROM [Lote] WITH (UPDLOCK) WHERE [Lote_Codigo] = @AV21ContagemResultado_LoteAceite ORDER BY [Lote_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003111,1,0,true,true )
             ,new CursorDef("P003112", "DELETE FROM [Lote]  WHERE [Lote_Codigo] = @Lote_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003112)
             ,new CursorDef("P003113", "scmdbuf",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003113,1,0,true,false )
             ,new CursorDef("P003114", "SELECT [ContagemResultadoIndicadores_DemandaCod], [ContagemResultadoIndicadores_Valor], [ContagemResultadoIndicadores_LoteCod], [ContagemResultadoIndicadores_IndicadorCod] FROM [ContagemResultadoIndicadores] WITH (UPDLOCK) WHERE [ContagemResultadoIndicadores_DemandaCod] = @ContagemResultado_Codigo ORDER BY [ContagemResultadoIndicadores_DemandaCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003114,1,0,true,false )
             ,new CursorDef("P003115", "UPDATE [ContagemResultadoIndicadores] SET [ContagemResultadoIndicadores_LoteCod]=@ContagemResultadoIndicadores_LoteCod  WHERE [ContagemResultadoIndicadores_DemandaCod] = @ContagemResultadoIndicadores_DemandaCod AND [ContagemResultadoIndicadores_IndicadorCod] = @ContagemResultadoIndicadores_IndicadorCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003115)
             ,new CursorDef("P003116", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_LoteAceiteCod]=@ContagemResultado_LoteAceiteCod, [ContagemResultado_SS]=@ContagemResultado_SS, [ContagemResultado_VlrAceite]=@ContagemResultado_VlrAceite  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003116)
             ,new CursorDef("P003118", "scmdbuf",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003118,1,0,true,false )
             ,new CursorDef("P003119", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_DataHomologacao]=@ContagemResultado_DataHomologacao  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003119)
             ,new CursorDef("P003120", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003120)
             ,new CursorDef("P003121", "scmdbuf",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003121,1,0,true,false )
             ,new CursorDef("P003122", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_Ultima] = 1)", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003122)
             ,new CursorDef("P003123", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_DataHomologacao]=@ContagemResultado_DataHomologacao  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003123)
             ,new CursorDef("P003124", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003124,1,0,false,true )
             ,new CursorDef("P003125", "SELECT TOP 1 [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV66WWPContext__Userid ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003125,1,0,false,true )
             ,new CursorDef("P003127", "SELECT COALESCE( T1.[GXC1], 0) AS GXC1 FROM (SELECT MAX([ContagemResultadoNaoCnf_Codigo]) AS GXC1 FROM [ContagemResultadoNaoCnf] WITH (NOLOCK) WHERE ([ContagemResultadoNaoCnf_Contestada] = 1) AND ([ContagemResultadoNaoCnf_OSCod] = @AV149Codigo) ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003127,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((decimal[]) buf[13])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((bool[]) buf[19])[0] = rslt.getBool(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[28])[0] = rslt.getGXDateTime(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[30])[0] = rslt.getGXDate(17) ;
                ((String[]) buf[31])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                ((String[]) buf[33])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((String[]) buf[35])[0] = rslt.getString(20, 1) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((decimal[]) buf[37])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                ((int[]) buf[39])[0] = rslt.getInt(22) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(22);
                ((int[]) buf[41])[0] = rslt.getInt(23) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(23);
                ((DateTime[]) buf[43])[0] = rslt.getGXDateTime(24) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(24);
                ((short[]) buf[45])[0] = rslt.getShort(25) ;
                ((decimal[]) buf[46])[0] = rslt.getDecimal(26) ;
                ((decimal[]) buf[47])[0] = rslt.getDecimal(27) ;
                ((decimal[]) buf[48])[0] = rslt.getDecimal(28) ;
                ((decimal[]) buf[49])[0] = rslt.getDecimal(29) ;
                ((int[]) buf[50])[0] = rslt.getInt(30) ;
                ((DateTime[]) buf[51])[0] = rslt.getGXDate(31) ;
                ((short[]) buf[52])[0] = rslt.getShort(32) ;
                ((int[]) buf[53])[0] = rslt.getInt(33) ;
                ((DateTime[]) buf[54])[0] = rslt.getGXDate(34) ;
                ((String[]) buf[55])[0] = rslt.getString(35, 5) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameterDatetime(4, (DateTime)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(7, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 11 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[15]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                return;
             case 13 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[211]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[212]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[213]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[214]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[215]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[216]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[217]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[218]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[219]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[220]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[221]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[222]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[223]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[224]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[225]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[227]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[228]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[229]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[230]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[231]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[232]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[233]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[234]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[235]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[236]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[237]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[238]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[239]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[240]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[241]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[242]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[243]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[245]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[246]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[247]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[248]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[249]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[250]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[251]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[252]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[253]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[254]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[255]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[256]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[257]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[258]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[259]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[260]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[261]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[262]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[263]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[264]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[265]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[266]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[267]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[268]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[269]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[270]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[271]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[272]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[273]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[274]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[275]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[276]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[277]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[278]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[279]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[280]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[281]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[282]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[283]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[284]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[285]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[286]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[287]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[288]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[289]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[290]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[291]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[292]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[293]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[294]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[295]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[296]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[297]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[298]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[299]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[300]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[301]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[302]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[303]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[304]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[305]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[306]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[307]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[308]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[309]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[310]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[311]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[312]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[313]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[314]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[315]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[316]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[317]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[318]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[319]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[320]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[321]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[322]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[323]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[324]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[325]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[326]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[327]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[328]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[329]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[330]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[331]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[332]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[333]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[334]);
                }
                if ( (short)parms[124] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[335]);
                }
                if ( (short)parms[125] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[336]);
                }
                if ( (short)parms[126] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[337]);
                }
                if ( (short)parms[127] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[338]);
                }
                if ( (short)parms[128] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[339]);
                }
                if ( (short)parms[129] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[340]);
                }
                if ( (short)parms[130] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[341]);
                }
                if ( (short)parms[131] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[342]);
                }
                if ( (short)parms[132] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[343]);
                }
                if ( (short)parms[133] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[344]);
                }
                if ( (short)parms[134] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[345]);
                }
                if ( (short)parms[135] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[346]);
                }
                if ( (short)parms[136] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[347]);
                }
                if ( (short)parms[137] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[348]);
                }
                if ( (short)parms[138] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[349]);
                }
                if ( (short)parms[139] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[350]);
                }
                if ( (short)parms[140] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[351]);
                }
                if ( (short)parms[141] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[352]);
                }
                if ( (short)parms[142] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[353]);
                }
                if ( (short)parms[143] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[354]);
                }
                if ( (short)parms[144] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[355]);
                }
                if ( (short)parms[145] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[356]);
                }
                if ( (short)parms[146] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[357]);
                }
                if ( (short)parms[147] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[358]);
                }
                if ( (short)parms[148] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[359]);
                }
                if ( (short)parms[149] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[360]);
                }
                if ( (short)parms[150] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[361]);
                }
                if ( (short)parms[151] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[362]);
                }
                if ( (short)parms[152] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[363]);
                }
                if ( (short)parms[153] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[364]);
                }
                if ( (short)parms[154] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[365]);
                }
                if ( (short)parms[155] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[366]);
                }
                if ( (short)parms[156] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[367]);
                }
                if ( (short)parms[157] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[368]);
                }
                if ( (short)parms[158] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[369]);
                }
                if ( (short)parms[159] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[370]);
                }
                if ( (short)parms[160] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[371]);
                }
                if ( (short)parms[161] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[372]);
                }
                if ( (short)parms[162] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[373]);
                }
                if ( (short)parms[163] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[374]);
                }
                if ( (short)parms[164] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[375]);
                }
                if ( (short)parms[165] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[376]);
                }
                if ( (short)parms[166] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[377]);
                }
                if ( (short)parms[167] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[378]);
                }
                if ( (short)parms[168] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[379]);
                }
                if ( (short)parms[169] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[380]);
                }
                if ( (short)parms[170] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[381]);
                }
                if ( (short)parms[171] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[382]);
                }
                if ( (short)parms[172] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[383]);
                }
                if ( (short)parms[173] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[384]);
                }
                if ( (short)parms[174] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[385]);
                }
                if ( (short)parms[175] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[386]);
                }
                if ( (short)parms[176] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[387]);
                }
                if ( (short)parms[177] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[388]);
                }
                if ( (short)parms[178] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[389]);
                }
                if ( (short)parms[179] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[390]);
                }
                if ( (short)parms[180] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[391]);
                }
                if ( (short)parms[181] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[392]);
                }
                if ( (short)parms[182] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[393]);
                }
                if ( (short)parms[183] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[394]);
                }
                if ( (short)parms[184] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[395]);
                }
                if ( (short)parms[185] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[396]);
                }
                if ( (short)parms[186] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[397]);
                }
                if ( (short)parms[187] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[398]);
                }
                if ( (short)parms[188] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[399]);
                }
                if ( (short)parms[189] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[400]);
                }
                if ( (short)parms[190] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[401]);
                }
                if ( (short)parms[191] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[402]);
                }
                if ( (short)parms[192] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[403]);
                }
                if ( (short)parms[193] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[404]);
                }
                if ( (short)parms[194] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[405]);
                }
                if ( (short)parms[195] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[406]);
                }
                if ( (short)parms[196] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[407]);
                }
                if ( (short)parms[197] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[408]);
                }
                if ( (short)parms[198] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[409]);
                }
                if ( (short)parms[199] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[410]);
                }
                if ( (short)parms[200] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[411]);
                }
                if ( (short)parms[201] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[412]);
                }
                if ( (short)parms[202] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[413]);
                }
                if ( (short)parms[203] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[414]);
                }
                if ( (short)parms[204] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[415]);
                }
                if ( (short)parms[205] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[416]);
                }
                if ( (short)parms[206] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[417]);
                }
                if ( (short)parms[207] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[418]);
                }
                if ( (short)parms[208] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[419]);
                }
                if ( (short)parms[209] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[420]);
                }
                if ( (short)parms[210] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[421]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 15 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 17 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 20 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
