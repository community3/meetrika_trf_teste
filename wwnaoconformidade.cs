/*
               File: WWNaoConformidade
        Description:  N�o Conformidade
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:2:8.51
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwnaoconformidade : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwnaoconformidade( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwnaoconformidade( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavNaoconformidade_tipo1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavNaoconformidade_tipo2 = new GXCombobox();
         cmbNaoConformidade_Tipo = new GXCombobox();
         cmbNaoConformidade_EhImpeditiva = new GXCombobox();
         chkNaoConformidade_Glosavel = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_67 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_67_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_67_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV16DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
               AV17NaoConformidade_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NaoConformidade_Nome1", AV17NaoConformidade_Nome1);
               AV18NaoConformidade_Tipo1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NaoConformidade_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0)));
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21NaoConformidade_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21NaoConformidade_Nome2", AV21NaoConformidade_Nome2);
               AV22NaoConformidade_Tipo2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NaoConformidade_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0)));
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV31TFNaoConformidade_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFNaoConformidade_Nome", AV31TFNaoConformidade_Nome);
               AV32TFNaoConformidade_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFNaoConformidade_Nome_Sel", AV32TFNaoConformidade_Nome_Sel);
               AV39TFNaoConformidade_EhImpeditiva_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFNaoConformidade_EhImpeditiva_Sel", StringUtil.Str( (decimal)(AV39TFNaoConformidade_EhImpeditiva_Sel), 1, 0));
               AV46TFNaoConformidade_Glosavel_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFNaoConformidade_Glosavel_Sel", StringUtil.Str( (decimal)(AV46TFNaoConformidade_Glosavel_Sel), 1, 0));
               AV33ddo_NaoConformidade_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_NaoConformidade_NomeTitleControlIdToReplace", AV33ddo_NaoConformidade_NomeTitleControlIdToReplace);
               AV37ddo_NaoConformidade_TipoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_NaoConformidade_TipoTitleControlIdToReplace", AV37ddo_NaoConformidade_TipoTitleControlIdToReplace);
               AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace", AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace);
               AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace", AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace);
               AV15NaoConformidade_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15NaoConformidade_AreaTrabalhoCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV36TFNaoConformidade_Tipo_Sels);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV65Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV24DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
               AV23DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
               A426NaoConformidade_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, AV20DynamicFiltersSelector2, AV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, AV19DynamicFiltersEnabled2, AV31TFNaoConformidade_Nome, AV32TFNaoConformidade_Nome_Sel, AV39TFNaoConformidade_EhImpeditiva_Sel, AV46TFNaoConformidade_Glosavel_Sel, AV33ddo_NaoConformidade_NomeTitleControlIdToReplace, AV37ddo_NaoConformidade_TipoTitleControlIdToReplace, AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace, AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace, AV15NaoConformidade_AreaTrabalhoCod, AV36TFNaoConformidade_Tipo_Sels, AV6WWPContext, AV65Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A426NaoConformidade_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAAL2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTAL2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020518132880");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwnaoconformidade.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vNAOCONFORMIDADE_NOME1", StringUtil.RTrim( AV17NaoConformidade_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vNAOCONFORMIDADE_TIPO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18NaoConformidade_Tipo1), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vNAOCONFORMIDADE_NOME2", StringUtil.RTrim( AV21NaoConformidade_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vNAOCONFORMIDADE_TIPO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22NaoConformidade_Tipo2), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNAOCONFORMIDADE_NOME", StringUtil.RTrim( AV31TFNaoConformidade_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNAOCONFORMIDADE_NOME_SEL", StringUtil.RTrim( AV32TFNaoConformidade_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFNaoConformidade_EhImpeditiva_Sel), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNAOCONFORMIDADE_GLOSAVEL_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46TFNaoConformidade_Glosavel_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_67", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_67), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV41DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV41DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNAOCONFORMIDADE_NOMETITLEFILTERDATA", AV30NaoConformidade_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNAOCONFORMIDADE_NOMETITLEFILTERDATA", AV30NaoConformidade_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNAOCONFORMIDADE_TIPOTITLEFILTERDATA", AV34NaoConformidade_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNAOCONFORMIDADE_TIPOTITLEFILTERDATA", AV34NaoConformidade_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNAOCONFORMIDADE_EHIMPEDITIVATITLEFILTERDATA", AV38NaoConformidade_EhImpeditivaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNAOCONFORMIDADE_EHIMPEDITIVATITLEFILTERDATA", AV38NaoConformidade_EhImpeditivaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNAOCONFORMIDADE_GLOSAVELTITLEFILTERDATA", AV45NaoConformidade_GlosavelTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNAOCONFORMIDADE_GLOSAVELTITLEFILTERDATA", AV45NaoConformidade_GlosavelTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFNAOCONFORMIDADE_TIPO_SELS", AV36TFNaoConformidade_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFNAOCONFORMIDADE_TIPO_SELS", AV36TFNaoConformidade_Tipo_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV65Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV24DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV23DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Caption", StringUtil.RTrim( Ddo_naoconformidade_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Tooltip", StringUtil.RTrim( Ddo_naoconformidade_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Cls", StringUtil.RTrim( Ddo_naoconformidade_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_naoconformidade_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_naoconformidade_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_naoconformidade_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_naoconformidade_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_naoconformidade_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_naoconformidade_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Sortedstatus", StringUtil.RTrim( Ddo_naoconformidade_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Includefilter", StringUtil.BoolToStr( Ddo_naoconformidade_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Filtertype", StringUtil.RTrim( Ddo_naoconformidade_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_naoconformidade_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_naoconformidade_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Datalisttype", StringUtil.RTrim( Ddo_naoconformidade_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Datalistproc", StringUtil.RTrim( Ddo_naoconformidade_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_naoconformidade_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Sortasc", StringUtil.RTrim( Ddo_naoconformidade_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Sortdsc", StringUtil.RTrim( Ddo_naoconformidade_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Loadingdata", StringUtil.RTrim( Ddo_naoconformidade_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Cleanfilter", StringUtil.RTrim( Ddo_naoconformidade_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Noresultsfound", StringUtil.RTrim( Ddo_naoconformidade_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_naoconformidade_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Caption", StringUtil.RTrim( Ddo_naoconformidade_tipo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Tooltip", StringUtil.RTrim( Ddo_naoconformidade_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Cls", StringUtil.RTrim( Ddo_naoconformidade_tipo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_naoconformidade_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_naoconformidade_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_naoconformidade_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_naoconformidade_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_naoconformidade_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_naoconformidade_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_naoconformidade_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_naoconformidade_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Datalisttype", StringUtil.RTrim( Ddo_naoconformidade_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_naoconformidade_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_naoconformidade_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Sortasc", StringUtil.RTrim( Ddo_naoconformidade_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Sortdsc", StringUtil.RTrim( Ddo_naoconformidade_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_naoconformidade_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_naoconformidade_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Caption", StringUtil.RTrim( Ddo_naoconformidade_ehimpeditiva_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Tooltip", StringUtil.RTrim( Ddo_naoconformidade_ehimpeditiva_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Cls", StringUtil.RTrim( Ddo_naoconformidade_ehimpeditiva_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Selectedvalue_set", StringUtil.RTrim( Ddo_naoconformidade_ehimpeditiva_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Dropdownoptionstype", StringUtil.RTrim( Ddo_naoconformidade_ehimpeditiva_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_naoconformidade_ehimpeditiva_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Includesortasc", StringUtil.BoolToStr( Ddo_naoconformidade_ehimpeditiva_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Includesortdsc", StringUtil.BoolToStr( Ddo_naoconformidade_ehimpeditiva_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Sortedstatus", StringUtil.RTrim( Ddo_naoconformidade_ehimpeditiva_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Includefilter", StringUtil.BoolToStr( Ddo_naoconformidade_ehimpeditiva_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Includedatalist", StringUtil.BoolToStr( Ddo_naoconformidade_ehimpeditiva_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Datalisttype", StringUtil.RTrim( Ddo_naoconformidade_ehimpeditiva_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Datalistfixedvalues", StringUtil.RTrim( Ddo_naoconformidade_ehimpeditiva_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Sortasc", StringUtil.RTrim( Ddo_naoconformidade_ehimpeditiva_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Sortdsc", StringUtil.RTrim( Ddo_naoconformidade_ehimpeditiva_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Cleanfilter", StringUtil.RTrim( Ddo_naoconformidade_ehimpeditiva_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Searchbuttontext", StringUtil.RTrim( Ddo_naoconformidade_ehimpeditiva_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Caption", StringUtil.RTrim( Ddo_naoconformidade_glosavel_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Tooltip", StringUtil.RTrim( Ddo_naoconformidade_glosavel_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Cls", StringUtil.RTrim( Ddo_naoconformidade_glosavel_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Selectedvalue_set", StringUtil.RTrim( Ddo_naoconformidade_glosavel_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Dropdownoptionstype", StringUtil.RTrim( Ddo_naoconformidade_glosavel_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_naoconformidade_glosavel_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Includesortasc", StringUtil.BoolToStr( Ddo_naoconformidade_glosavel_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Includesortdsc", StringUtil.BoolToStr( Ddo_naoconformidade_glosavel_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Sortedstatus", StringUtil.RTrim( Ddo_naoconformidade_glosavel_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Includefilter", StringUtil.BoolToStr( Ddo_naoconformidade_glosavel_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Includedatalist", StringUtil.BoolToStr( Ddo_naoconformidade_glosavel_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Datalisttype", StringUtil.RTrim( Ddo_naoconformidade_glosavel_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Datalistfixedvalues", StringUtil.RTrim( Ddo_naoconformidade_glosavel_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Sortasc", StringUtil.RTrim( Ddo_naoconformidade_glosavel_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Sortdsc", StringUtil.RTrim( Ddo_naoconformidade_glosavel_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Cleanfilter", StringUtil.RTrim( Ddo_naoconformidade_glosavel_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Searchbuttontext", StringUtil.RTrim( Ddo_naoconformidade_glosavel_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Activeeventkey", StringUtil.RTrim( Ddo_naoconformidade_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_naoconformidade_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_naoconformidade_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_naoconformidade_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_naoconformidade_tipo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Activeeventkey", StringUtil.RTrim( Ddo_naoconformidade_ehimpeditiva_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Selectedvalue_get", StringUtil.RTrim( Ddo_naoconformidade_ehimpeditiva_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Activeeventkey", StringUtil.RTrim( Ddo_naoconformidade_glosavel_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_GLOSAVEL_Selectedvalue_get", StringUtil.RTrim( Ddo_naoconformidade_glosavel_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEAL2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTAL2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwnaoconformidade.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWNaoConformidade" ;
      }

      public override String GetPgmdesc( )
      {
         return " N�o Conformidade" ;
      }

      protected void WBAL0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_AL2( true) ;
         }
         else
         {
            wb_table1_2_AL2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_AL2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_67_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(80, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_67_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnaoconformidade_nome_Internalname, StringUtil.RTrim( AV31TFNaoConformidade_Nome), StringUtil.RTrim( context.localUtil.Format( AV31TFNaoConformidade_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnaoconformidade_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfnaoconformidade_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWNaoConformidade.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_67_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnaoconformidade_nome_sel_Internalname, StringUtil.RTrim( AV32TFNaoConformidade_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV32TFNaoConformidade_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnaoconformidade_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfnaoconformidade_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWNaoConformidade.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_67_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnaoconformidade_ehimpeditiva_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFNaoConformidade_EhImpeditiva_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV39TFNaoConformidade_EhImpeditiva_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnaoconformidade_ehimpeditiva_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfnaoconformidade_ehimpeditiva_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWNaoConformidade.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_67_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnaoconformidade_glosavel_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46TFNaoConformidade_Glosavel_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV46TFNaoConformidade_Glosavel_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnaoconformidade_glosavel_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfnaoconformidade_glosavel_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWNaoConformidade.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_NAOCONFORMIDADE_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_67_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Internalname, AV33ddo_NaoConformidade_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,86);\"", 0, edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWNaoConformidade.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_NAOCONFORMIDADE_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_67_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Internalname, AV37ddo_NaoConformidade_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,88);\"", 0, edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWNaoConformidade.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_NAOCONFORMIDADE_EHIMPEDITIVAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_67_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_naoconformidade_ehimpeditivatitlecontrolidtoreplace_Internalname, AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"", 0, edtavDdo_naoconformidade_ehimpeditivatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWNaoConformidade.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_NAOCONFORMIDADE_GLOSAVELContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_67_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_naoconformidade_glosaveltitlecontrolidtoreplace_Internalname, AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", 0, edtavDdo_naoconformidade_glosaveltitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWNaoConformidade.htm");
         }
         wbLoad = true;
      }

      protected void STARTAL2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " N�o Conformidade", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPAL0( ) ;
      }

      protected void WSAL2( )
      {
         STARTAL2( ) ;
         EVTAL2( ) ;
      }

      protected void EVTAL2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11AL2 */
                              E11AL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_NAOCONFORMIDADE_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12AL2 */
                              E12AL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_NAOCONFORMIDADE_TIPO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13AL2 */
                              E13AL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_NAOCONFORMIDADE_EHIMPEDITIVA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14AL2 */
                              E14AL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_NAOCONFORMIDADE_GLOSAVEL.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15AL2 */
                              E15AL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16AL2 */
                              E16AL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17AL2 */
                              E17AL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18AL2 */
                              E18AL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19AL2 */
                              E19AL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20AL2 */
                              E20AL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21AL2 */
                              E21AL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22AL2 */
                              E22AL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23AL2 */
                              E23AL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_67_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_67_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_67_idx), 4, 0)), 4, "0");
                              SubsflControlProps_672( ) ;
                              AV25Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)) ? AV63Update_GXI : context.convertURL( context.PathToRelativeUrl( AV25Update))));
                              AV26Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)) ? AV64Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV26Delete))));
                              A428NaoConformidade_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtNaoConformidade_AreaTrabalhoCod_Internalname), ",", "."));
                              A426NaoConformidade_Codigo = (int)(context.localUtil.CToN( cgiGet( edtNaoConformidade_Codigo_Internalname), ",", "."));
                              A427NaoConformidade_Nome = StringUtil.Upper( cgiGet( edtNaoConformidade_Nome_Internalname));
                              cmbNaoConformidade_Tipo.Name = cmbNaoConformidade_Tipo_Internalname;
                              cmbNaoConformidade_Tipo.CurrentValue = cgiGet( cmbNaoConformidade_Tipo_Internalname);
                              A429NaoConformidade_Tipo = (short)(NumberUtil.Val( cgiGet( cmbNaoConformidade_Tipo_Internalname), "."));
                              cmbNaoConformidade_EhImpeditiva.Name = cmbNaoConformidade_EhImpeditiva_Internalname;
                              cmbNaoConformidade_EhImpeditiva.CurrentValue = cgiGet( cmbNaoConformidade_EhImpeditiva_Internalname);
                              A1121NaoConformidade_EhImpeditiva = StringUtil.StrToBool( cgiGet( cmbNaoConformidade_EhImpeditiva_Internalname));
                              n1121NaoConformidade_EhImpeditiva = false;
                              A2029NaoConformidade_Glosavel = StringUtil.StrToBool( cgiGet( chkNaoConformidade_Glosavel_Internalname));
                              n2029NaoConformidade_Glosavel = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E24AL2 */
                                    E24AL2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25AL2 */
                                    E25AL2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26AL2 */
                                    E26AL2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Naoconformidade_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vNAOCONFORMIDADE_NOME1"), AV17NaoConformidade_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Naoconformidade_tipo1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vNAOCONFORMIDADE_TIPO1"), ",", ".") != Convert.ToDecimal( AV18NaoConformidade_Tipo1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Naoconformidade_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vNAOCONFORMIDADE_NOME2"), AV21NaoConformidade_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Naoconformidade_tipo2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vNAOCONFORMIDADE_TIPO2"), ",", ".") != Convert.ToDecimal( AV22NaoConformidade_Tipo2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfnaoconformidade_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFNAOCONFORMIDADE_NOME"), AV31TFNaoConformidade_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfnaoconformidade_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFNAOCONFORMIDADE_NOME_SEL"), AV32TFNaoConformidade_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfnaoconformidade_ehimpeditiva_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL"), ",", ".") != Convert.ToDecimal( AV39TFNaoConformidade_EhImpeditiva_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfnaoconformidade_glosavel_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFNAOCONFORMIDADE_GLOSAVEL_SEL"), ",", ".") != Convert.ToDecimal( AV46TFNaoConformidade_Glosavel_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEAL2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAAL2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("NAOCONFORMIDADE_NOME", "Conformidade", 0);
            cmbavDynamicfiltersselector1.addItem("NAOCONFORMIDADE_TIPO", "Tipo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavNaoconformidade_tipo1.Name = "vNAOCONFORMIDADE_TIPO1";
            cmbavNaoconformidade_tipo1.WebTags = "";
            cmbavNaoconformidade_tipo1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "Todos", 0);
            cmbavNaoconformidade_tipo1.addItem("1", "Demandas", 0);
            cmbavNaoconformidade_tipo1.addItem("2", "Itens da Contagem", 0);
            cmbavNaoconformidade_tipo1.addItem("3", "Contagens", 0);
            cmbavNaoconformidade_tipo1.addItem("4", "Check List Demandas", 0);
            cmbavNaoconformidade_tipo1.addItem("5", "Check List Contagens", 0);
            cmbavNaoconformidade_tipo1.addItem("6", "Homologa��o", 0);
            if ( cmbavNaoconformidade_tipo1.ItemCount > 0 )
            {
               AV18NaoConformidade_Tipo1 = (short)(NumberUtil.Val( cmbavNaoconformidade_tipo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NaoConformidade_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("NAOCONFORMIDADE_NOME", "Conformidade", 0);
            cmbavDynamicfiltersselector2.addItem("NAOCONFORMIDADE_TIPO", "Tipo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavNaoconformidade_tipo2.Name = "vNAOCONFORMIDADE_TIPO2";
            cmbavNaoconformidade_tipo2.WebTags = "";
            cmbavNaoconformidade_tipo2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "Todos", 0);
            cmbavNaoconformidade_tipo2.addItem("1", "Demandas", 0);
            cmbavNaoconformidade_tipo2.addItem("2", "Itens da Contagem", 0);
            cmbavNaoconformidade_tipo2.addItem("3", "Contagens", 0);
            cmbavNaoconformidade_tipo2.addItem("4", "Check List Demandas", 0);
            cmbavNaoconformidade_tipo2.addItem("5", "Check List Contagens", 0);
            cmbavNaoconformidade_tipo2.addItem("6", "Homologa��o", 0);
            if ( cmbavNaoconformidade_tipo2.ItemCount > 0 )
            {
               AV22NaoConformidade_Tipo2 = (short)(NumberUtil.Val( cmbavNaoconformidade_tipo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NaoConformidade_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0)));
            }
            GXCCtl = "NAOCONFORMIDADE_TIPO_" + sGXsfl_67_idx;
            cmbNaoConformidade_Tipo.Name = GXCCtl;
            cmbNaoConformidade_Tipo.WebTags = "";
            cmbNaoConformidade_Tipo.addItem("1", "Demandas", 0);
            cmbNaoConformidade_Tipo.addItem("2", "Itens da Contagem", 0);
            cmbNaoConformidade_Tipo.addItem("3", "Contagens", 0);
            cmbNaoConformidade_Tipo.addItem("4", "Check List Demandas", 0);
            cmbNaoConformidade_Tipo.addItem("5", "Check List Contagens", 0);
            cmbNaoConformidade_Tipo.addItem("6", "Homologa��o", 0);
            if ( cmbNaoConformidade_Tipo.ItemCount > 0 )
            {
               A429NaoConformidade_Tipo = (short)(NumberUtil.Val( cmbNaoConformidade_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0))), "."));
            }
            GXCCtl = "NAOCONFORMIDADE_EHIMPEDITIVA_" + sGXsfl_67_idx;
            cmbNaoConformidade_EhImpeditiva.Name = GXCCtl;
            cmbNaoConformidade_EhImpeditiva.WebTags = "";
            cmbNaoConformidade_EhImpeditiva.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbNaoConformidade_EhImpeditiva.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbNaoConformidade_EhImpeditiva.ItemCount > 0 )
            {
               A1121NaoConformidade_EhImpeditiva = StringUtil.StrToBool( cmbNaoConformidade_EhImpeditiva.getValidValue(StringUtil.BoolToStr( A1121NaoConformidade_EhImpeditiva)));
               n1121NaoConformidade_EhImpeditiva = false;
            }
            GXCCtl = "NAOCONFORMIDADE_GLOSAVEL_" + sGXsfl_67_idx;
            chkNaoConformidade_Glosavel.Name = GXCCtl;
            chkNaoConformidade_Glosavel.WebTags = "";
            chkNaoConformidade_Glosavel.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkNaoConformidade_Glosavel_Internalname, "TitleCaption", chkNaoConformidade_Glosavel.Caption);
            chkNaoConformidade_Glosavel.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_672( ) ;
         while ( nGXsfl_67_idx <= nRC_GXsfl_67 )
         {
            sendrow_672( ) ;
            nGXsfl_67_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_67_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_67_idx+1));
            sGXsfl_67_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_67_idx), 4, 0)), 4, "0");
            SubsflControlProps_672( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       String AV17NaoConformidade_Nome1 ,
                                       short AV18NaoConformidade_Tipo1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       String AV21NaoConformidade_Nome2 ,
                                       short AV22NaoConformidade_Tipo2 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       String AV31TFNaoConformidade_Nome ,
                                       String AV32TFNaoConformidade_Nome_Sel ,
                                       short AV39TFNaoConformidade_EhImpeditiva_Sel ,
                                       short AV46TFNaoConformidade_Glosavel_Sel ,
                                       String AV33ddo_NaoConformidade_NomeTitleControlIdToReplace ,
                                       String AV37ddo_NaoConformidade_TipoTitleControlIdToReplace ,
                                       String AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace ,
                                       String AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace ,
                                       int AV15NaoConformidade_AreaTrabalhoCod ,
                                       IGxCollection AV36TFNaoConformidade_Tipo_Sels ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV65Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV24DynamicFiltersIgnoreFirst ,
                                       bool AV23DynamicFiltersRemoving ,
                                       int A426NaoConformidade_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFAL2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A428NaoConformidade_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "NAOCONFORMIDADE_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A428NaoConformidade_AreaTrabalhoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A426NaoConformidade_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "NAOCONFORMIDADE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A426NaoConformidade_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A427NaoConformidade_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "NAOCONFORMIDADE_NOME", StringUtil.RTrim( A427NaoConformidade_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_TIPO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A429NaoConformidade_Tipo), "Z9")));
         GxWebStd.gx_hidden_field( context, "NAOCONFORMIDADE_TIPO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A429NaoConformidade_Tipo), 2, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_EHIMPEDITIVA", GetSecureSignedToken( "", A1121NaoConformidade_EhImpeditiva));
         GxWebStd.gx_hidden_field( context, "NAOCONFORMIDADE_EHIMPEDITIVA", StringUtil.BoolToStr( A1121NaoConformidade_EhImpeditiva));
         GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_GLOSAVEL", GetSecureSignedToken( "", A2029NaoConformidade_Glosavel));
         GxWebStd.gx_hidden_field( context, "NAOCONFORMIDADE_GLOSAVEL", StringUtil.BoolToStr( A2029NaoConformidade_Glosavel));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavNaoconformidade_tipo1.ItemCount > 0 )
         {
            AV18NaoConformidade_Tipo1 = (short)(NumberUtil.Val( cmbavNaoconformidade_tipo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NaoConformidade_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavNaoconformidade_tipo2.ItemCount > 0 )
         {
            AV22NaoConformidade_Tipo2 = (short)(NumberUtil.Val( cmbavNaoconformidade_tipo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NaoConformidade_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFAL2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV65Pgmname = "WWNaoConformidade";
         context.Gx_err = 0;
      }

      protected void RFAL2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 67;
         /* Execute user event: E25AL2 */
         E25AL2 ();
         nGXsfl_67_idx = 1;
         sGXsfl_67_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_67_idx), 4, 0)), 4, "0");
         SubsflControlProps_672( ) ;
         nGXsfl_67_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_672( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A429NaoConformidade_Tipo ,
                                                 AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels ,
                                                 AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1 ,
                                                 AV52WWNaoConformidadeDS_3_Naoconformidade_nome1 ,
                                                 AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1 ,
                                                 AV54WWNaoConformidadeDS_5_Dynamicfiltersenabled2 ,
                                                 AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2 ,
                                                 AV56WWNaoConformidadeDS_7_Naoconformidade_nome2 ,
                                                 AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2 ,
                                                 AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel ,
                                                 AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome ,
                                                 AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels.Count ,
                                                 AV61WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel ,
                                                 AV62WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel ,
                                                 A427NaoConformidade_Nome ,
                                                 A1121NaoConformidade_EhImpeditiva ,
                                                 A2029NaoConformidade_Glosavel ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A428NaoConformidade_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV52WWNaoConformidadeDS_3_Naoconformidade_nome1 = StringUtil.PadR( StringUtil.RTrim( AV52WWNaoConformidadeDS_3_Naoconformidade_nome1), 50, "%");
            lV56WWNaoConformidadeDS_7_Naoconformidade_nome2 = StringUtil.PadR( StringUtil.RTrim( AV56WWNaoConformidadeDS_7_Naoconformidade_nome2), 50, "%");
            lV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome = StringUtil.PadR( StringUtil.RTrim( AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome), 50, "%");
            /* Using cursor H00AL2 */
            pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV52WWNaoConformidadeDS_3_Naoconformidade_nome1, AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1, lV56WWNaoConformidadeDS_7_Naoconformidade_nome2, AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2, lV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome, AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_67_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A2029NaoConformidade_Glosavel = H00AL2_A2029NaoConformidade_Glosavel[0];
               n2029NaoConformidade_Glosavel = H00AL2_n2029NaoConformidade_Glosavel[0];
               A1121NaoConformidade_EhImpeditiva = H00AL2_A1121NaoConformidade_EhImpeditiva[0];
               n1121NaoConformidade_EhImpeditiva = H00AL2_n1121NaoConformidade_EhImpeditiva[0];
               A429NaoConformidade_Tipo = H00AL2_A429NaoConformidade_Tipo[0];
               A427NaoConformidade_Nome = H00AL2_A427NaoConformidade_Nome[0];
               A426NaoConformidade_Codigo = H00AL2_A426NaoConformidade_Codigo[0];
               A428NaoConformidade_AreaTrabalhoCod = H00AL2_A428NaoConformidade_AreaTrabalhoCod[0];
               /* Execute user event: E26AL2 */
               E26AL2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 67;
            WBAL0( ) ;
         }
         nGXsfl_67_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV50WWNaoConformidadeDS_1_Naoconformidade_areatrabalhocod = AV15NaoConformidade_AreaTrabalhoCod;
         AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV52WWNaoConformidadeDS_3_Naoconformidade_nome1 = AV17NaoConformidade_Nome1;
         AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1 = AV18NaoConformidade_Tipo1;
         AV54WWNaoConformidadeDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV56WWNaoConformidadeDS_7_Naoconformidade_nome2 = AV21NaoConformidade_Nome2;
         AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2 = AV22NaoConformidade_Tipo2;
         AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome = AV31TFNaoConformidade_Nome;
         AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel = AV32TFNaoConformidade_Nome_Sel;
         AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels = AV36TFNaoConformidade_Tipo_Sels;
         AV61WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel = AV39TFNaoConformidade_EhImpeditiva_Sel;
         AV62WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel = AV46TFNaoConformidade_Glosavel_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A429NaoConformidade_Tipo ,
                                              AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels ,
                                              AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1 ,
                                              AV52WWNaoConformidadeDS_3_Naoconformidade_nome1 ,
                                              AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1 ,
                                              AV54WWNaoConformidadeDS_5_Dynamicfiltersenabled2 ,
                                              AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2 ,
                                              AV56WWNaoConformidadeDS_7_Naoconformidade_nome2 ,
                                              AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2 ,
                                              AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel ,
                                              AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome ,
                                              AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels.Count ,
                                              AV61WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel ,
                                              AV62WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel ,
                                              A427NaoConformidade_Nome ,
                                              A1121NaoConformidade_EhImpeditiva ,
                                              A2029NaoConformidade_Glosavel ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A428NaoConformidade_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV52WWNaoConformidadeDS_3_Naoconformidade_nome1 = StringUtil.PadR( StringUtil.RTrim( AV52WWNaoConformidadeDS_3_Naoconformidade_nome1), 50, "%");
         lV56WWNaoConformidadeDS_7_Naoconformidade_nome2 = StringUtil.PadR( StringUtil.RTrim( AV56WWNaoConformidadeDS_7_Naoconformidade_nome2), 50, "%");
         lV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome = StringUtil.PadR( StringUtil.RTrim( AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome), 50, "%");
         /* Using cursor H00AL3 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV52WWNaoConformidadeDS_3_Naoconformidade_nome1, AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1, lV56WWNaoConformidadeDS_7_Naoconformidade_nome2, AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2, lV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome, AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel});
         GRID_nRecordCount = H00AL3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV50WWNaoConformidadeDS_1_Naoconformidade_areatrabalhocod = AV15NaoConformidade_AreaTrabalhoCod;
         AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV52WWNaoConformidadeDS_3_Naoconformidade_nome1 = AV17NaoConformidade_Nome1;
         AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1 = AV18NaoConformidade_Tipo1;
         AV54WWNaoConformidadeDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV56WWNaoConformidadeDS_7_Naoconformidade_nome2 = AV21NaoConformidade_Nome2;
         AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2 = AV22NaoConformidade_Tipo2;
         AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome = AV31TFNaoConformidade_Nome;
         AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel = AV32TFNaoConformidade_Nome_Sel;
         AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels = AV36TFNaoConformidade_Tipo_Sels;
         AV61WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel = AV39TFNaoConformidade_EhImpeditiva_Sel;
         AV62WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel = AV46TFNaoConformidade_Glosavel_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, AV20DynamicFiltersSelector2, AV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, AV19DynamicFiltersEnabled2, AV31TFNaoConformidade_Nome, AV32TFNaoConformidade_Nome_Sel, AV39TFNaoConformidade_EhImpeditiva_Sel, AV46TFNaoConformidade_Glosavel_Sel, AV33ddo_NaoConformidade_NomeTitleControlIdToReplace, AV37ddo_NaoConformidade_TipoTitleControlIdToReplace, AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace, AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace, AV15NaoConformidade_AreaTrabalhoCod, AV36TFNaoConformidade_Tipo_Sels, AV6WWPContext, AV65Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A426NaoConformidade_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV50WWNaoConformidadeDS_1_Naoconformidade_areatrabalhocod = AV15NaoConformidade_AreaTrabalhoCod;
         AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV52WWNaoConformidadeDS_3_Naoconformidade_nome1 = AV17NaoConformidade_Nome1;
         AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1 = AV18NaoConformidade_Tipo1;
         AV54WWNaoConformidadeDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV56WWNaoConformidadeDS_7_Naoconformidade_nome2 = AV21NaoConformidade_Nome2;
         AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2 = AV22NaoConformidade_Tipo2;
         AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome = AV31TFNaoConformidade_Nome;
         AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel = AV32TFNaoConformidade_Nome_Sel;
         AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels = AV36TFNaoConformidade_Tipo_Sels;
         AV61WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel = AV39TFNaoConformidade_EhImpeditiva_Sel;
         AV62WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel = AV46TFNaoConformidade_Glosavel_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, AV20DynamicFiltersSelector2, AV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, AV19DynamicFiltersEnabled2, AV31TFNaoConformidade_Nome, AV32TFNaoConformidade_Nome_Sel, AV39TFNaoConformidade_EhImpeditiva_Sel, AV46TFNaoConformidade_Glosavel_Sel, AV33ddo_NaoConformidade_NomeTitleControlIdToReplace, AV37ddo_NaoConformidade_TipoTitleControlIdToReplace, AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace, AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace, AV15NaoConformidade_AreaTrabalhoCod, AV36TFNaoConformidade_Tipo_Sels, AV6WWPContext, AV65Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A426NaoConformidade_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV50WWNaoConformidadeDS_1_Naoconformidade_areatrabalhocod = AV15NaoConformidade_AreaTrabalhoCod;
         AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV52WWNaoConformidadeDS_3_Naoconformidade_nome1 = AV17NaoConformidade_Nome1;
         AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1 = AV18NaoConformidade_Tipo1;
         AV54WWNaoConformidadeDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV56WWNaoConformidadeDS_7_Naoconformidade_nome2 = AV21NaoConformidade_Nome2;
         AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2 = AV22NaoConformidade_Tipo2;
         AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome = AV31TFNaoConformidade_Nome;
         AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel = AV32TFNaoConformidade_Nome_Sel;
         AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels = AV36TFNaoConformidade_Tipo_Sels;
         AV61WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel = AV39TFNaoConformidade_EhImpeditiva_Sel;
         AV62WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel = AV46TFNaoConformidade_Glosavel_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, AV20DynamicFiltersSelector2, AV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, AV19DynamicFiltersEnabled2, AV31TFNaoConformidade_Nome, AV32TFNaoConformidade_Nome_Sel, AV39TFNaoConformidade_EhImpeditiva_Sel, AV46TFNaoConformidade_Glosavel_Sel, AV33ddo_NaoConformidade_NomeTitleControlIdToReplace, AV37ddo_NaoConformidade_TipoTitleControlIdToReplace, AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace, AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace, AV15NaoConformidade_AreaTrabalhoCod, AV36TFNaoConformidade_Tipo_Sels, AV6WWPContext, AV65Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A426NaoConformidade_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV50WWNaoConformidadeDS_1_Naoconformidade_areatrabalhocod = AV15NaoConformidade_AreaTrabalhoCod;
         AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV52WWNaoConformidadeDS_3_Naoconformidade_nome1 = AV17NaoConformidade_Nome1;
         AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1 = AV18NaoConformidade_Tipo1;
         AV54WWNaoConformidadeDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV56WWNaoConformidadeDS_7_Naoconformidade_nome2 = AV21NaoConformidade_Nome2;
         AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2 = AV22NaoConformidade_Tipo2;
         AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome = AV31TFNaoConformidade_Nome;
         AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel = AV32TFNaoConformidade_Nome_Sel;
         AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels = AV36TFNaoConformidade_Tipo_Sels;
         AV61WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel = AV39TFNaoConformidade_EhImpeditiva_Sel;
         AV62WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel = AV46TFNaoConformidade_Glosavel_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, AV20DynamicFiltersSelector2, AV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, AV19DynamicFiltersEnabled2, AV31TFNaoConformidade_Nome, AV32TFNaoConformidade_Nome_Sel, AV39TFNaoConformidade_EhImpeditiva_Sel, AV46TFNaoConformidade_Glosavel_Sel, AV33ddo_NaoConformidade_NomeTitleControlIdToReplace, AV37ddo_NaoConformidade_TipoTitleControlIdToReplace, AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace, AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace, AV15NaoConformidade_AreaTrabalhoCod, AV36TFNaoConformidade_Tipo_Sels, AV6WWPContext, AV65Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A426NaoConformidade_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV50WWNaoConformidadeDS_1_Naoconformidade_areatrabalhocod = AV15NaoConformidade_AreaTrabalhoCod;
         AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV52WWNaoConformidadeDS_3_Naoconformidade_nome1 = AV17NaoConformidade_Nome1;
         AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1 = AV18NaoConformidade_Tipo1;
         AV54WWNaoConformidadeDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV56WWNaoConformidadeDS_7_Naoconformidade_nome2 = AV21NaoConformidade_Nome2;
         AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2 = AV22NaoConformidade_Tipo2;
         AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome = AV31TFNaoConformidade_Nome;
         AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel = AV32TFNaoConformidade_Nome_Sel;
         AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels = AV36TFNaoConformidade_Tipo_Sels;
         AV61WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel = AV39TFNaoConformidade_EhImpeditiva_Sel;
         AV62WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel = AV46TFNaoConformidade_Glosavel_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, AV20DynamicFiltersSelector2, AV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, AV19DynamicFiltersEnabled2, AV31TFNaoConformidade_Nome, AV32TFNaoConformidade_Nome_Sel, AV39TFNaoConformidade_EhImpeditiva_Sel, AV46TFNaoConformidade_Glosavel_Sel, AV33ddo_NaoConformidade_NomeTitleControlIdToReplace, AV37ddo_NaoConformidade_TipoTitleControlIdToReplace, AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace, AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace, AV15NaoConformidade_AreaTrabalhoCod, AV36TFNaoConformidade_Tipo_Sels, AV6WWPContext, AV65Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A426NaoConformidade_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPAL0( )
      {
         /* Before Start, stand alone formulas. */
         AV65Pgmname = "WWNaoConformidade";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E24AL2 */
         E24AL2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV41DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vNAOCONFORMIDADE_NOMETITLEFILTERDATA"), AV30NaoConformidade_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vNAOCONFORMIDADE_TIPOTITLEFILTERDATA"), AV34NaoConformidade_TipoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vNAOCONFORMIDADE_EHIMPEDITIVATITLEFILTERDATA"), AV38NaoConformidade_EhImpeditivaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vNAOCONFORMIDADE_GLOSAVELTITLEFILTERDATA"), AV45NaoConformidade_GlosavelTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavNaoconformidade_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavNaoconformidade_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vNAOCONFORMIDADE_AREATRABALHOCOD");
               GX_FocusControl = edtavNaoconformidade_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV15NaoConformidade_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15NaoConformidade_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV15NaoConformidade_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavNaoconformidade_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15NaoConformidade_AreaTrabalhoCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            AV17NaoConformidade_Nome1 = StringUtil.Upper( cgiGet( edtavNaoconformidade_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NaoConformidade_Nome1", AV17NaoConformidade_Nome1);
            cmbavNaoconformidade_tipo1.Name = cmbavNaoconformidade_tipo1_Internalname;
            cmbavNaoconformidade_tipo1.CurrentValue = cgiGet( cmbavNaoconformidade_tipo1_Internalname);
            AV18NaoConformidade_Tipo1 = (short)(NumberUtil.Val( cgiGet( cmbavNaoconformidade_tipo1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NaoConformidade_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0)));
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            AV21NaoConformidade_Nome2 = StringUtil.Upper( cgiGet( edtavNaoconformidade_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21NaoConformidade_Nome2", AV21NaoConformidade_Nome2);
            cmbavNaoconformidade_tipo2.Name = cmbavNaoconformidade_tipo2_Internalname;
            cmbavNaoconformidade_tipo2.CurrentValue = cgiGet( cmbavNaoconformidade_tipo2_Internalname);
            AV22NaoConformidade_Tipo2 = (short)(NumberUtil.Val( cgiGet( cmbavNaoconformidade_tipo2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NaoConformidade_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0)));
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV31TFNaoConformidade_Nome = StringUtil.Upper( cgiGet( edtavTfnaoconformidade_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFNaoConformidade_Nome", AV31TFNaoConformidade_Nome);
            AV32TFNaoConformidade_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfnaoconformidade_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFNaoConformidade_Nome_Sel", AV32TFNaoConformidade_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnaoconformidade_ehimpeditiva_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnaoconformidade_ehimpeditiva_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL");
               GX_FocusControl = edtavTfnaoconformidade_ehimpeditiva_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFNaoConformidade_EhImpeditiva_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFNaoConformidade_EhImpeditiva_Sel", StringUtil.Str( (decimal)(AV39TFNaoConformidade_EhImpeditiva_Sel), 1, 0));
            }
            else
            {
               AV39TFNaoConformidade_EhImpeditiva_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfnaoconformidade_ehimpeditiva_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFNaoConformidade_EhImpeditiva_Sel", StringUtil.Str( (decimal)(AV39TFNaoConformidade_EhImpeditiva_Sel), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnaoconformidade_glosavel_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnaoconformidade_glosavel_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNAOCONFORMIDADE_GLOSAVEL_SEL");
               GX_FocusControl = edtavTfnaoconformidade_glosavel_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46TFNaoConformidade_Glosavel_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFNaoConformidade_Glosavel_Sel", StringUtil.Str( (decimal)(AV46TFNaoConformidade_Glosavel_Sel), 1, 0));
            }
            else
            {
               AV46TFNaoConformidade_Glosavel_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfnaoconformidade_glosavel_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFNaoConformidade_Glosavel_Sel", StringUtil.Str( (decimal)(AV46TFNaoConformidade_Glosavel_Sel), 1, 0));
            }
            AV33ddo_NaoConformidade_NomeTitleControlIdToReplace = cgiGet( edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_NaoConformidade_NomeTitleControlIdToReplace", AV33ddo_NaoConformidade_NomeTitleControlIdToReplace);
            AV37ddo_NaoConformidade_TipoTitleControlIdToReplace = cgiGet( edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_NaoConformidade_TipoTitleControlIdToReplace", AV37ddo_NaoConformidade_TipoTitleControlIdToReplace);
            AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace = cgiGet( edtavDdo_naoconformidade_ehimpeditivatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace", AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace);
            AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace = cgiGet( edtavDdo_naoconformidade_glosaveltitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace", AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_67 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_67"), ",", "."));
            AV43GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV44GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_naoconformidade_nome_Caption = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Caption");
            Ddo_naoconformidade_nome_Tooltip = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Tooltip");
            Ddo_naoconformidade_nome_Cls = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Cls");
            Ddo_naoconformidade_nome_Filteredtext_set = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Filteredtext_set");
            Ddo_naoconformidade_nome_Selectedvalue_set = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Selectedvalue_set");
            Ddo_naoconformidade_nome_Dropdownoptionstype = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Dropdownoptionstype");
            Ddo_naoconformidade_nome_Titlecontrolidtoreplace = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Titlecontrolidtoreplace");
            Ddo_naoconformidade_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_NOME_Includesortasc"));
            Ddo_naoconformidade_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_NOME_Includesortdsc"));
            Ddo_naoconformidade_nome_Sortedstatus = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Sortedstatus");
            Ddo_naoconformidade_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_NOME_Includefilter"));
            Ddo_naoconformidade_nome_Filtertype = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Filtertype");
            Ddo_naoconformidade_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_NOME_Filterisrange"));
            Ddo_naoconformidade_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_NOME_Includedatalist"));
            Ddo_naoconformidade_nome_Datalisttype = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Datalisttype");
            Ddo_naoconformidade_nome_Datalistproc = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Datalistproc");
            Ddo_naoconformidade_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_NAOCONFORMIDADE_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_naoconformidade_nome_Sortasc = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Sortasc");
            Ddo_naoconformidade_nome_Sortdsc = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Sortdsc");
            Ddo_naoconformidade_nome_Loadingdata = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Loadingdata");
            Ddo_naoconformidade_nome_Cleanfilter = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Cleanfilter");
            Ddo_naoconformidade_nome_Noresultsfound = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Noresultsfound");
            Ddo_naoconformidade_nome_Searchbuttontext = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Searchbuttontext");
            Ddo_naoconformidade_tipo_Caption = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Caption");
            Ddo_naoconformidade_tipo_Tooltip = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Tooltip");
            Ddo_naoconformidade_tipo_Cls = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Cls");
            Ddo_naoconformidade_tipo_Selectedvalue_set = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Selectedvalue_set");
            Ddo_naoconformidade_tipo_Dropdownoptionstype = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Dropdownoptionstype");
            Ddo_naoconformidade_tipo_Titlecontrolidtoreplace = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Titlecontrolidtoreplace");
            Ddo_naoconformidade_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Includesortasc"));
            Ddo_naoconformidade_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Includesortdsc"));
            Ddo_naoconformidade_tipo_Sortedstatus = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Sortedstatus");
            Ddo_naoconformidade_tipo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Includefilter"));
            Ddo_naoconformidade_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Includedatalist"));
            Ddo_naoconformidade_tipo_Datalisttype = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Datalisttype");
            Ddo_naoconformidade_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Allowmultipleselection"));
            Ddo_naoconformidade_tipo_Datalistfixedvalues = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Datalistfixedvalues");
            Ddo_naoconformidade_tipo_Sortasc = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Sortasc");
            Ddo_naoconformidade_tipo_Sortdsc = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Sortdsc");
            Ddo_naoconformidade_tipo_Cleanfilter = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Cleanfilter");
            Ddo_naoconformidade_tipo_Searchbuttontext = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Searchbuttontext");
            Ddo_naoconformidade_ehimpeditiva_Caption = cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Caption");
            Ddo_naoconformidade_ehimpeditiva_Tooltip = cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Tooltip");
            Ddo_naoconformidade_ehimpeditiva_Cls = cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Cls");
            Ddo_naoconformidade_ehimpeditiva_Selectedvalue_set = cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Selectedvalue_set");
            Ddo_naoconformidade_ehimpeditiva_Dropdownoptionstype = cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Dropdownoptionstype");
            Ddo_naoconformidade_ehimpeditiva_Titlecontrolidtoreplace = cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Titlecontrolidtoreplace");
            Ddo_naoconformidade_ehimpeditiva_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Includesortasc"));
            Ddo_naoconformidade_ehimpeditiva_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Includesortdsc"));
            Ddo_naoconformidade_ehimpeditiva_Sortedstatus = cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Sortedstatus");
            Ddo_naoconformidade_ehimpeditiva_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Includefilter"));
            Ddo_naoconformidade_ehimpeditiva_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Includedatalist"));
            Ddo_naoconformidade_ehimpeditiva_Datalisttype = cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Datalisttype");
            Ddo_naoconformidade_ehimpeditiva_Datalistfixedvalues = cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Datalistfixedvalues");
            Ddo_naoconformidade_ehimpeditiva_Sortasc = cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Sortasc");
            Ddo_naoconformidade_ehimpeditiva_Sortdsc = cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Sortdsc");
            Ddo_naoconformidade_ehimpeditiva_Cleanfilter = cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Cleanfilter");
            Ddo_naoconformidade_ehimpeditiva_Searchbuttontext = cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Searchbuttontext");
            Ddo_naoconformidade_glosavel_Caption = cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Caption");
            Ddo_naoconformidade_glosavel_Tooltip = cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Tooltip");
            Ddo_naoconformidade_glosavel_Cls = cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Cls");
            Ddo_naoconformidade_glosavel_Selectedvalue_set = cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Selectedvalue_set");
            Ddo_naoconformidade_glosavel_Dropdownoptionstype = cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Dropdownoptionstype");
            Ddo_naoconformidade_glosavel_Titlecontrolidtoreplace = cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Titlecontrolidtoreplace");
            Ddo_naoconformidade_glosavel_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Includesortasc"));
            Ddo_naoconformidade_glosavel_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Includesortdsc"));
            Ddo_naoconformidade_glosavel_Sortedstatus = cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Sortedstatus");
            Ddo_naoconformidade_glosavel_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Includefilter"));
            Ddo_naoconformidade_glosavel_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Includedatalist"));
            Ddo_naoconformidade_glosavel_Datalisttype = cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Datalisttype");
            Ddo_naoconformidade_glosavel_Datalistfixedvalues = cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Datalistfixedvalues");
            Ddo_naoconformidade_glosavel_Sortasc = cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Sortasc");
            Ddo_naoconformidade_glosavel_Sortdsc = cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Sortdsc");
            Ddo_naoconformidade_glosavel_Cleanfilter = cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Cleanfilter");
            Ddo_naoconformidade_glosavel_Searchbuttontext = cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_naoconformidade_nome_Activeeventkey = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Activeeventkey");
            Ddo_naoconformidade_nome_Filteredtext_get = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Filteredtext_get");
            Ddo_naoconformidade_nome_Selectedvalue_get = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Selectedvalue_get");
            Ddo_naoconformidade_tipo_Activeeventkey = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Activeeventkey");
            Ddo_naoconformidade_tipo_Selectedvalue_get = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Selectedvalue_get");
            Ddo_naoconformidade_ehimpeditiva_Activeeventkey = cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Activeeventkey");
            Ddo_naoconformidade_ehimpeditiva_Selectedvalue_get = cgiGet( "DDO_NAOCONFORMIDADE_EHIMPEDITIVA_Selectedvalue_get");
            Ddo_naoconformidade_glosavel_Activeeventkey = cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Activeeventkey");
            Ddo_naoconformidade_glosavel_Selectedvalue_get = cgiGet( "DDO_NAOCONFORMIDADE_GLOSAVEL_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vNAOCONFORMIDADE_NOME1"), AV17NaoConformidade_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vNAOCONFORMIDADE_TIPO1"), ",", ".") != Convert.ToDecimal( AV18NaoConformidade_Tipo1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vNAOCONFORMIDADE_NOME2"), AV21NaoConformidade_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vNAOCONFORMIDADE_TIPO2"), ",", ".") != Convert.ToDecimal( AV22NaoConformidade_Tipo2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFNAOCONFORMIDADE_NOME"), AV31TFNaoConformidade_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFNAOCONFORMIDADE_NOME_SEL"), AV32TFNaoConformidade_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL"), ",", ".") != Convert.ToDecimal( AV39TFNaoConformidade_EhImpeditiva_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFNAOCONFORMIDADE_GLOSAVEL_SEL"), ",", ".") != Convert.ToDecimal( AV46TFNaoConformidade_Glosavel_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E24AL2 */
         E24AL2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24AL2( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.0 - Data: 09/04/2020 23:40", 0) ;
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV18NaoConformidade_Tipo1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NaoConformidade_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0)));
         AV16DynamicFiltersSelector1 = "NAOCONFORMIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22NaoConformidade_Tipo2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NaoConformidade_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0)));
         AV20DynamicFiltersSelector2 = "NAOCONFORMIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfnaoconformidade_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnaoconformidade_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnaoconformidade_nome_Visible), 5, 0)));
         edtavTfnaoconformidade_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnaoconformidade_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnaoconformidade_nome_sel_Visible), 5, 0)));
         edtavTfnaoconformidade_ehimpeditiva_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnaoconformidade_ehimpeditiva_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnaoconformidade_ehimpeditiva_sel_Visible), 5, 0)));
         edtavTfnaoconformidade_glosavel_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnaoconformidade_glosavel_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnaoconformidade_glosavel_sel_Visible), 5, 0)));
         Ddo_naoconformidade_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_NaoConformidade_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_nome_Internalname, "TitleControlIdToReplace", Ddo_naoconformidade_nome_Titlecontrolidtoreplace);
         AV33ddo_NaoConformidade_NomeTitleControlIdToReplace = Ddo_naoconformidade_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_NaoConformidade_NomeTitleControlIdToReplace", AV33ddo_NaoConformidade_NomeTitleControlIdToReplace);
         edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_naoconformidade_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_NaoConformidade_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_tipo_Internalname, "TitleControlIdToReplace", Ddo_naoconformidade_tipo_Titlecontrolidtoreplace);
         AV37ddo_NaoConformidade_TipoTitleControlIdToReplace = Ddo_naoconformidade_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_NaoConformidade_TipoTitleControlIdToReplace", AV37ddo_NaoConformidade_TipoTitleControlIdToReplace);
         edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_naoconformidade_ehimpeditiva_Titlecontrolidtoreplace = subGrid_Internalname+"_NaoConformidade_EhImpeditiva";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_ehimpeditiva_Internalname, "TitleControlIdToReplace", Ddo_naoconformidade_ehimpeditiva_Titlecontrolidtoreplace);
         AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace = Ddo_naoconformidade_ehimpeditiva_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace", AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace);
         edtavDdo_naoconformidade_ehimpeditivatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_naoconformidade_ehimpeditivatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_naoconformidade_ehimpeditivatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_naoconformidade_glosavel_Titlecontrolidtoreplace = subGrid_Internalname+"_NaoConformidade_Glosavel";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_glosavel_Internalname, "TitleControlIdToReplace", Ddo_naoconformidade_glosavel_Titlecontrolidtoreplace);
         AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace = Ddo_naoconformidade_glosavel_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace", AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace);
         edtavDdo_naoconformidade_glosaveltitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_naoconformidade_glosaveltitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_naoconformidade_glosaveltitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " N�o Conformidade";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Conformidade", 0);
         cmbavOrderedby.addItem("2", "Tipo", 0);
         cmbavOrderedby.addItem("3", "� impeditiva?", 0);
         cmbavOrderedby.addItem("4", "Glos�vel", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV41DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV41DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E25AL2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV30NaoConformidade_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34NaoConformidade_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38NaoConformidade_EhImpeditivaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45NaoConformidade_GlosavelTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtNaoConformidade_Nome_Titleformat = 2;
         edtNaoConformidade_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV33ddo_NaoConformidade_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNaoConformidade_Nome_Internalname, "Title", edtNaoConformidade_Nome_Title);
         cmbNaoConformidade_Tipo_Titleformat = 2;
         cmbNaoConformidade_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV37ddo_NaoConformidade_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbNaoConformidade_Tipo_Internalname, "Title", cmbNaoConformidade_Tipo.Title.Text);
         cmbNaoConformidade_EhImpeditiva_Titleformat = 2;
         cmbNaoConformidade_EhImpeditiva.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "� impeditiva?", AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbNaoConformidade_EhImpeditiva_Internalname, "Title", cmbNaoConformidade_EhImpeditiva.Title.Text);
         chkNaoConformidade_Glosavel_Titleformat = 2;
         chkNaoConformidade_Glosavel.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Glos�vel", AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkNaoConformidade_Glosavel_Internalname, "Title", chkNaoConformidade_Glosavel.Title.Text);
         AV43GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43GridCurrentPage), 10, 0)));
         AV44GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44GridPageCount), 10, 0)));
         AV50WWNaoConformidadeDS_1_Naoconformidade_areatrabalhocod = AV15NaoConformidade_AreaTrabalhoCod;
         AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV52WWNaoConformidadeDS_3_Naoconformidade_nome1 = AV17NaoConformidade_Nome1;
         AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1 = AV18NaoConformidade_Tipo1;
         AV54WWNaoConformidadeDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV56WWNaoConformidadeDS_7_Naoconformidade_nome2 = AV21NaoConformidade_Nome2;
         AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2 = AV22NaoConformidade_Tipo2;
         AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome = AV31TFNaoConformidade_Nome;
         AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel = AV32TFNaoConformidade_Nome_Sel;
         AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels = AV36TFNaoConformidade_Tipo_Sels;
         AV61WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel = AV39TFNaoConformidade_EhImpeditiva_Sel;
         AV62WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel = AV46TFNaoConformidade_Glosavel_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30NaoConformidade_NomeTitleFilterData", AV30NaoConformidade_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34NaoConformidade_TipoTitleFilterData", AV34NaoConformidade_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38NaoConformidade_EhImpeditivaTitleFilterData", AV38NaoConformidade_EhImpeditivaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV45NaoConformidade_GlosavelTitleFilterData", AV45NaoConformidade_GlosavelTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11AL2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV42PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV42PageToGo) ;
         }
      }

      protected void E12AL2( )
      {
         /* Ddo_naoconformidade_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_naoconformidade_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_naoconformidade_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_nome_Internalname, "SortedStatus", Ddo_naoconformidade_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_naoconformidade_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_naoconformidade_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_nome_Internalname, "SortedStatus", Ddo_naoconformidade_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_naoconformidade_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV31TFNaoConformidade_Nome = Ddo_naoconformidade_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFNaoConformidade_Nome", AV31TFNaoConformidade_Nome);
            AV32TFNaoConformidade_Nome_Sel = Ddo_naoconformidade_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFNaoConformidade_Nome_Sel", AV32TFNaoConformidade_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13AL2( )
      {
         /* Ddo_naoconformidade_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_naoconformidade_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_naoconformidade_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_tipo_Internalname, "SortedStatus", Ddo_naoconformidade_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_naoconformidade_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_naoconformidade_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_tipo_Internalname, "SortedStatus", Ddo_naoconformidade_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_naoconformidade_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFNaoConformidade_Tipo_SelsJson = Ddo_naoconformidade_tipo_Selectedvalue_get;
            AV36TFNaoConformidade_Tipo_Sels.FromJSonString(StringUtil.StringReplace( AV35TFNaoConformidade_Tipo_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36TFNaoConformidade_Tipo_Sels", AV36TFNaoConformidade_Tipo_Sels);
      }

      protected void E14AL2( )
      {
         /* Ddo_naoconformidade_ehimpeditiva_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_naoconformidade_ehimpeditiva_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_naoconformidade_ehimpeditiva_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_ehimpeditiva_Internalname, "SortedStatus", Ddo_naoconformidade_ehimpeditiva_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_naoconformidade_ehimpeditiva_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_naoconformidade_ehimpeditiva_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_ehimpeditiva_Internalname, "SortedStatus", Ddo_naoconformidade_ehimpeditiva_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_naoconformidade_ehimpeditiva_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFNaoConformidade_EhImpeditiva_Sel = (short)(NumberUtil.Val( Ddo_naoconformidade_ehimpeditiva_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFNaoConformidade_EhImpeditiva_Sel", StringUtil.Str( (decimal)(AV39TFNaoConformidade_EhImpeditiva_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15AL2( )
      {
         /* Ddo_naoconformidade_glosavel_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_naoconformidade_glosavel_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_naoconformidade_glosavel_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_glosavel_Internalname, "SortedStatus", Ddo_naoconformidade_glosavel_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_naoconformidade_glosavel_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_naoconformidade_glosavel_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_glosavel_Internalname, "SortedStatus", Ddo_naoconformidade_glosavel_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_naoconformidade_glosavel_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV46TFNaoConformidade_Glosavel_Sel = (short)(NumberUtil.Val( Ddo_naoconformidade_glosavel_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFNaoConformidade_Glosavel_Sel", StringUtil.Str( (decimal)(AV46TFNaoConformidade_Glosavel_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E26AL2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavUpdate_Link = formatLink("naoconformidade.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A426NaoConformidade_Codigo);
            AV25Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV25Update);
            AV63Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV25Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV25Update);
            AV63Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavDelete_Link = formatLink("naoconformidade.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A426NaoConformidade_Codigo);
            AV26Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV26Delete);
            AV64Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV26Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV26Delete);
            AV64Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtNaoConformidade_Nome_Link = formatLink("viewnaoconformidade.aspx") + "?" + UrlEncode("" +A426NaoConformidade_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 67;
         }
         sendrow_672( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_67_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(67, GridRow);
         }
      }

      protected void E16AL2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E21AL2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E17AL2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, AV20DynamicFiltersSelector2, AV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, AV19DynamicFiltersEnabled2, AV31TFNaoConformidade_Nome, AV32TFNaoConformidade_Nome_Sel, AV39TFNaoConformidade_EhImpeditiva_Sel, AV46TFNaoConformidade_Glosavel_Sel, AV33ddo_NaoConformidade_NomeTitleControlIdToReplace, AV37ddo_NaoConformidade_TipoTitleControlIdToReplace, AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace, AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace, AV15NaoConformidade_AreaTrabalhoCod, AV36TFNaoConformidade_Tipo_Sels, AV6WWPContext, AV65Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A426NaoConformidade_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavNaoconformidade_tipo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo1_Internalname, "Values", cmbavNaoconformidade_tipo1.ToJavascriptSource());
         cmbavNaoconformidade_tipo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo2_Internalname, "Values", cmbavNaoconformidade_tipo2.ToJavascriptSource());
      }

      protected void E22AL2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18AL2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, AV20DynamicFiltersSelector2, AV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, AV19DynamicFiltersEnabled2, AV31TFNaoConformidade_Nome, AV32TFNaoConformidade_Nome_Sel, AV39TFNaoConformidade_EhImpeditiva_Sel, AV46TFNaoConformidade_Glosavel_Sel, AV33ddo_NaoConformidade_NomeTitleControlIdToReplace, AV37ddo_NaoConformidade_TipoTitleControlIdToReplace, AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace, AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace, AV15NaoConformidade_AreaTrabalhoCod, AV36TFNaoConformidade_Tipo_Sels, AV6WWPContext, AV65Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A426NaoConformidade_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavNaoconformidade_tipo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo1_Internalname, "Values", cmbavNaoconformidade_tipo1.ToJavascriptSource());
         cmbavNaoconformidade_tipo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo2_Internalname, "Values", cmbavNaoconformidade_tipo2.ToJavascriptSource());
      }

      protected void E23AL2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19AL2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36TFNaoConformidade_Tipo_Sels", AV36TFNaoConformidade_Tipo_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavNaoconformidade_tipo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo1_Internalname, "Values", cmbavNaoconformidade_tipo1.ToJavascriptSource());
         cmbavNaoconformidade_tipo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo2_Internalname, "Values", cmbavNaoconformidade_tipo2.ToJavascriptSource());
      }

      protected void E20AL2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("naoconformidade.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_naoconformidade_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_nome_Internalname, "SortedStatus", Ddo_naoconformidade_nome_Sortedstatus);
         Ddo_naoconformidade_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_tipo_Internalname, "SortedStatus", Ddo_naoconformidade_tipo_Sortedstatus);
         Ddo_naoconformidade_ehimpeditiva_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_ehimpeditiva_Internalname, "SortedStatus", Ddo_naoconformidade_ehimpeditiva_Sortedstatus);
         Ddo_naoconformidade_glosavel_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_glosavel_Internalname, "SortedStatus", Ddo_naoconformidade_glosavel_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_naoconformidade_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_nome_Internalname, "SortedStatus", Ddo_naoconformidade_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_naoconformidade_tipo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_tipo_Internalname, "SortedStatus", Ddo_naoconformidade_tipo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_naoconformidade_ehimpeditiva_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_ehimpeditiva_Internalname, "SortedStatus", Ddo_naoconformidade_ehimpeditiva_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_naoconformidade_glosavel_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_glosavel_Internalname, "SortedStatus", Ddo_naoconformidade_glosavel_Sortedstatus);
         }
      }

      protected void S162( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavNaoconformidade_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNaoconformidade_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNaoconformidade_nome1_Visible), 5, 0)));
         cmbavNaoconformidade_tipo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavNaoconformidade_tipo1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "NAOCONFORMIDADE_NOME") == 0 )
         {
            edtavNaoconformidade_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNaoconformidade_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNaoconformidade_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "NAOCONFORMIDADE_TIPO") == 0 )
         {
            cmbavNaoconformidade_tipo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavNaoconformidade_tipo1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavNaoconformidade_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNaoconformidade_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNaoconformidade_nome2_Visible), 5, 0)));
         cmbavNaoconformidade_tipo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavNaoconformidade_tipo2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "NAOCONFORMIDADE_NOME") == 0 )
         {
            edtavNaoconformidade_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNaoconformidade_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNaoconformidade_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "NAOCONFORMIDADE_TIPO") == 0 )
         {
            cmbavNaoconformidade_tipo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavNaoconformidade_tipo2.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "NAOCONFORMIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21NaoConformidade_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21NaoConformidade_Nome2", AV21NaoConformidade_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV15NaoConformidade_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15NaoConformidade_AreaTrabalhoCod), 6, 0)));
         AV31TFNaoConformidade_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFNaoConformidade_Nome", AV31TFNaoConformidade_Nome);
         Ddo_naoconformidade_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_nome_Internalname, "FilteredText_set", Ddo_naoconformidade_nome_Filteredtext_set);
         AV32TFNaoConformidade_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFNaoConformidade_Nome_Sel", AV32TFNaoConformidade_Nome_Sel);
         Ddo_naoconformidade_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_nome_Internalname, "SelectedValue_set", Ddo_naoconformidade_nome_Selectedvalue_set);
         AV36TFNaoConformidade_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_naoconformidade_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_tipo_Internalname, "SelectedValue_set", Ddo_naoconformidade_tipo_Selectedvalue_set);
         AV39TFNaoConformidade_EhImpeditiva_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFNaoConformidade_EhImpeditiva_Sel", StringUtil.Str( (decimal)(AV39TFNaoConformidade_EhImpeditiva_Sel), 1, 0));
         Ddo_naoconformidade_ehimpeditiva_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_ehimpeditiva_Internalname, "SelectedValue_set", Ddo_naoconformidade_ehimpeditiva_Selectedvalue_set);
         AV46TFNaoConformidade_Glosavel_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFNaoConformidade_Glosavel_Sel", StringUtil.Str( (decimal)(AV46TFNaoConformidade_Glosavel_Sel), 1, 0));
         Ddo_naoconformidade_glosavel_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_glosavel_Internalname, "SelectedValue_set", Ddo_naoconformidade_glosavel_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "NAOCONFORMIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17NaoConformidade_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NaoConformidade_Nome1", AV17NaoConformidade_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get(AV65Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV65Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV27Session.Get(AV65Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV66GXV1 = 1;
         while ( AV66GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV66GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "NAOCONFORMIDADE_AREATRABALHOCOD") == 0 )
            {
               AV15NaoConformidade_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15NaoConformidade_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFNAOCONFORMIDADE_NOME") == 0 )
            {
               AV31TFNaoConformidade_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFNaoConformidade_Nome", AV31TFNaoConformidade_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFNaoConformidade_Nome)) )
               {
                  Ddo_naoconformidade_nome_Filteredtext_set = AV31TFNaoConformidade_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_nome_Internalname, "FilteredText_set", Ddo_naoconformidade_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFNAOCONFORMIDADE_NOME_SEL") == 0 )
            {
               AV32TFNaoConformidade_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFNaoConformidade_Nome_Sel", AV32TFNaoConformidade_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFNaoConformidade_Nome_Sel)) )
               {
                  Ddo_naoconformidade_nome_Selectedvalue_set = AV32TFNaoConformidade_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_nome_Internalname, "SelectedValue_set", Ddo_naoconformidade_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFNAOCONFORMIDADE_TIPO_SEL") == 0 )
            {
               AV35TFNaoConformidade_Tipo_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV36TFNaoConformidade_Tipo_Sels.FromJSonString(AV35TFNaoConformidade_Tipo_SelsJson);
               if ( ! ( AV36TFNaoConformidade_Tipo_Sels.Count == 0 ) )
               {
                  Ddo_naoconformidade_tipo_Selectedvalue_set = AV35TFNaoConformidade_Tipo_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_tipo_Internalname, "SelectedValue_set", Ddo_naoconformidade_tipo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFNAOCONFORMIDADE_EHIMPEDITIVA_SEL") == 0 )
            {
               AV39TFNaoConformidade_EhImpeditiva_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFNaoConformidade_EhImpeditiva_Sel", StringUtil.Str( (decimal)(AV39TFNaoConformidade_EhImpeditiva_Sel), 1, 0));
               if ( ! (0==AV39TFNaoConformidade_EhImpeditiva_Sel) )
               {
                  Ddo_naoconformidade_ehimpeditiva_Selectedvalue_set = StringUtil.Str( (decimal)(AV39TFNaoConformidade_EhImpeditiva_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_ehimpeditiva_Internalname, "SelectedValue_set", Ddo_naoconformidade_ehimpeditiva_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFNAOCONFORMIDADE_GLOSAVEL_SEL") == 0 )
            {
               AV46TFNaoConformidade_Glosavel_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFNaoConformidade_Glosavel_Sel", StringUtil.Str( (decimal)(AV46TFNaoConformidade_Glosavel_Sel), 1, 0));
               if ( ! (0==AV46TFNaoConformidade_Glosavel_Sel) )
               {
                  Ddo_naoconformidade_glosavel_Selectedvalue_set = StringUtil.Str( (decimal)(AV46TFNaoConformidade_Glosavel_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_glosavel_Internalname, "SelectedValue_set", Ddo_naoconformidade_glosavel_Selectedvalue_set);
               }
            }
            AV66GXV1 = (int)(AV66GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "NAOCONFORMIDADE_NOME") == 0 )
            {
               AV17NaoConformidade_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NaoConformidade_Nome1", AV17NaoConformidade_Nome1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "NAOCONFORMIDADE_TIPO") == 0 )
            {
               AV18NaoConformidade_Tipo1 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NaoConformidade_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "NAOCONFORMIDADE_NOME") == 0 )
               {
                  AV21NaoConformidade_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21NaoConformidade_Nome2", AV21NaoConformidade_Nome2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "NAOCONFORMIDADE_TIPO") == 0 )
               {
                  AV22NaoConformidade_Tipo2 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NaoConformidade_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV23DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV27Session.Get(AV65Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV15NaoConformidade_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "NAOCONFORMIDADE_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV15NaoConformidade_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFNaoConformidade_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNAOCONFORMIDADE_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV31TFNaoConformidade_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFNaoConformidade_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNAOCONFORMIDADE_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV32TFNaoConformidade_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV36TFNaoConformidade_Tipo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNAOCONFORMIDADE_TIPO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV36TFNaoConformidade_Tipo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV39TFNaoConformidade_EhImpeditiva_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNAOCONFORMIDADE_EHIMPEDITIVA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV39TFNaoConformidade_EhImpeditiva_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV46TFNaoConformidade_Glosavel_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNAOCONFORMIDADE_GLOSAVEL_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV46TFNaoConformidade_Glosavel_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV65Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV24DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "NAOCONFORMIDADE_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17NaoConformidade_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17NaoConformidade_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "NAOCONFORMIDADE_TIPO") == 0 ) && ! (0==AV18NaoConformidade_Tipo1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0);
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "NAOCONFORMIDADE_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21NaoConformidade_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21NaoConformidade_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "NAOCONFORMIDADE_TIPO") == 0 ) && ! (0==AV22NaoConformidade_Tipo2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0);
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV65Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "NaoConformidade";
         AV27Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_AL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_AL2( true) ;
         }
         else
         {
            wb_table2_8_AL2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_AL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_61_AL2( true) ;
         }
         else
         {
            wb_table3_61_AL2( false) ;
         }
         return  ;
      }

      protected void wb_table3_61_AL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_AL2e( true) ;
         }
         else
         {
            wb_table1_2_AL2e( false) ;
         }
      }

      protected void wb_table3_61_AL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_64_AL2( true) ;
         }
         else
         {
            wb_table4_64_AL2( false) ;
         }
         return  ;
      }

      protected void wb_table4_64_AL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_61_AL2e( true) ;
         }
         else
         {
            wb_table3_61_AL2e( false) ;
         }
      }

      protected void wb_table4_64_AL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"67\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "de Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Conformidade") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtNaoConformidade_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtNaoConformidade_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtNaoConformidade_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbNaoConformidade_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbNaoConformidade_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbNaoConformidade_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbNaoConformidade_EhImpeditiva_Titleformat == 0 )
               {
                  context.SendWebValue( cmbNaoConformidade_EhImpeditiva.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbNaoConformidade_EhImpeditiva.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkNaoConformidade_Glosavel_Titleformat == 0 )
               {
                  context.SendWebValue( chkNaoConformidade_Glosavel.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkNaoConformidade_Glosavel.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV25Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV26Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A428NaoConformidade_AreaTrabalhoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A426NaoConformidade_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A427NaoConformidade_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtNaoConformidade_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtNaoConformidade_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtNaoConformidade_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A429NaoConformidade_Tipo), 2, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbNaoConformidade_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbNaoConformidade_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1121NaoConformidade_EhImpeditiva));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbNaoConformidade_EhImpeditiva.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbNaoConformidade_EhImpeditiva_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A2029NaoConformidade_Glosavel));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkNaoConformidade_Glosavel.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkNaoConformidade_Glosavel_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 67 )
         {
            wbEnd = 0;
            nRC_GXsfl_67 = (short)(nGXsfl_67_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_64_AL2e( true) ;
         }
         else
         {
            wb_table4_64_AL2e( false) ;
         }
      }

      protected void wb_table2_8_AL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNaoconformidadetitle_Internalname, "N�o Conformidades", "", "", lblNaoconformidadetitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_AL2( true) ;
         }
         else
         {
            wb_table5_13_AL2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_AL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_67_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWNaoConformidade.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_67_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_AL2( true) ;
         }
         else
         {
            wb_table6_23_AL2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_AL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_AL2e( true) ;
         }
         else
         {
            wb_table2_8_AL2e( false) ;
         }
      }

      protected void wb_table6_23_AL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextnaoconformidade_areatrabalhocod_Internalname, "de Trabalho", "", "", lblFiltertextnaoconformidade_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_67_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNaoconformidade_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15NaoConformidade_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV15NaoConformidade_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNaoconformidade_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_32_AL2( true) ;
         }
         else
         {
            wb_table7_32_AL2( false) ;
         }
         return  ;
      }

      protected void wb_table7_32_AL2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_AL2e( true) ;
         }
         else
         {
            wb_table6_23_AL2e( false) ;
         }
      }

      protected void wb_table7_32_AL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_67_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_WWNaoConformidade.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_67_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNaoconformidade_nome1_Internalname, StringUtil.RTrim( AV17NaoConformidade_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17NaoConformidade_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNaoconformidade_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavNaoconformidade_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWNaoConformidade.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_67_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavNaoconformidade_tipo1, cmbavNaoconformidade_tipo1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0)), 1, cmbavNaoconformidade_tipo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavNaoconformidade_tipo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "", true, "HLP_WWNaoConformidade.htm");
            cmbavNaoconformidade_tipo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo1_Internalname, "Values", (String)(cmbavNaoconformidade_tipo1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_67_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WWNaoConformidade.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_67_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNaoconformidade_nome2_Internalname, StringUtil.RTrim( AV21NaoConformidade_Nome2), StringUtil.RTrim( context.localUtil.Format( AV21NaoConformidade_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNaoconformidade_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavNaoconformidade_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWNaoConformidade.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_67_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavNaoconformidade_tipo2, cmbavNaoconformidade_tipo2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0)), 1, cmbavNaoconformidade_tipo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavNaoconformidade_tipo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "", true, "HLP_WWNaoConformidade.htm");
            cmbavNaoconformidade_tipo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo2_Internalname, "Values", (String)(cmbavNaoconformidade_tipo2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_32_AL2e( true) ;
         }
         else
         {
            wb_table7_32_AL2e( false) ;
         }
      }

      protected void wb_table5_13_AL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_AL2e( true) ;
         }
         else
         {
            wb_table5_13_AL2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAAL2( ) ;
         WSAL2( ) ;
         WEAL2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205181321629");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwnaoconformidade.js", "?20205181321629");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_672( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_67_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_67_idx;
         edtNaoConformidade_AreaTrabalhoCod_Internalname = "NAOCONFORMIDADE_AREATRABALHOCOD_"+sGXsfl_67_idx;
         edtNaoConformidade_Codigo_Internalname = "NAOCONFORMIDADE_CODIGO_"+sGXsfl_67_idx;
         edtNaoConformidade_Nome_Internalname = "NAOCONFORMIDADE_NOME_"+sGXsfl_67_idx;
         cmbNaoConformidade_Tipo_Internalname = "NAOCONFORMIDADE_TIPO_"+sGXsfl_67_idx;
         cmbNaoConformidade_EhImpeditiva_Internalname = "NAOCONFORMIDADE_EHIMPEDITIVA_"+sGXsfl_67_idx;
         chkNaoConformidade_Glosavel_Internalname = "NAOCONFORMIDADE_GLOSAVEL_"+sGXsfl_67_idx;
      }

      protected void SubsflControlProps_fel_672( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_67_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_67_fel_idx;
         edtNaoConformidade_AreaTrabalhoCod_Internalname = "NAOCONFORMIDADE_AREATRABALHOCOD_"+sGXsfl_67_fel_idx;
         edtNaoConformidade_Codigo_Internalname = "NAOCONFORMIDADE_CODIGO_"+sGXsfl_67_fel_idx;
         edtNaoConformidade_Nome_Internalname = "NAOCONFORMIDADE_NOME_"+sGXsfl_67_fel_idx;
         cmbNaoConformidade_Tipo_Internalname = "NAOCONFORMIDADE_TIPO_"+sGXsfl_67_fel_idx;
         cmbNaoConformidade_EhImpeditiva_Internalname = "NAOCONFORMIDADE_EHIMPEDITIVA_"+sGXsfl_67_fel_idx;
         chkNaoConformidade_Glosavel_Internalname = "NAOCONFORMIDADE_GLOSAVEL_"+sGXsfl_67_fel_idx;
      }

      protected void sendrow_672( )
      {
         SubsflControlProps_672( ) ;
         WBAL0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_67_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_67_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_67_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV25Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV25Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV63Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)) ? AV63Update_GXI : context.PathToRelativeUrl( AV25Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV25Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV26Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV64Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)) ? AV64Delete_GXI : context.PathToRelativeUrl( AV26Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV26Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNaoConformidade_AreaTrabalhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A428NaoConformidade_AreaTrabalhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A428NaoConformidade_AreaTrabalhoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNaoConformidade_AreaTrabalhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)67,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNaoConformidade_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A426NaoConformidade_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A426NaoConformidade_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNaoConformidade_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)67,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNaoConformidade_Nome_Internalname,StringUtil.RTrim( A427NaoConformidade_Nome),StringUtil.RTrim( context.localUtil.Format( A427NaoConformidade_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtNaoConformidade_Nome_Link,(String)"",(String)"",(String)"",(String)edtNaoConformidade_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)67,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_67_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "NAOCONFORMIDADE_TIPO_" + sGXsfl_67_idx;
               cmbNaoConformidade_Tipo.Name = GXCCtl;
               cmbNaoConformidade_Tipo.WebTags = "";
               cmbNaoConformidade_Tipo.addItem("1", "Demandas", 0);
               cmbNaoConformidade_Tipo.addItem("2", "Itens da Contagem", 0);
               cmbNaoConformidade_Tipo.addItem("3", "Contagens", 0);
               cmbNaoConformidade_Tipo.addItem("4", "Check List Demandas", 0);
               cmbNaoConformidade_Tipo.addItem("5", "Check List Contagens", 0);
               cmbNaoConformidade_Tipo.addItem("6", "Homologa��o", 0);
               if ( cmbNaoConformidade_Tipo.ItemCount > 0 )
               {
                  A429NaoConformidade_Tipo = (short)(NumberUtil.Val( cmbNaoConformidade_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0))), "."));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbNaoConformidade_Tipo,(String)cmbNaoConformidade_Tipo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0)),(short)1,(String)cmbNaoConformidade_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbNaoConformidade_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbNaoConformidade_Tipo_Internalname, "Values", (String)(cmbNaoConformidade_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_67_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "NAOCONFORMIDADE_EHIMPEDITIVA_" + sGXsfl_67_idx;
               cmbNaoConformidade_EhImpeditiva.Name = GXCCtl;
               cmbNaoConformidade_EhImpeditiva.WebTags = "";
               cmbNaoConformidade_EhImpeditiva.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               cmbNaoConformidade_EhImpeditiva.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               if ( cmbNaoConformidade_EhImpeditiva.ItemCount > 0 )
               {
                  A1121NaoConformidade_EhImpeditiva = StringUtil.StrToBool( cmbNaoConformidade_EhImpeditiva.getValidValue(StringUtil.BoolToStr( A1121NaoConformidade_EhImpeditiva)));
                  n1121NaoConformidade_EhImpeditiva = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbNaoConformidade_EhImpeditiva,(String)cmbNaoConformidade_EhImpeditiva_Internalname,StringUtil.BoolToStr( A1121NaoConformidade_EhImpeditiva),(short)1,(String)cmbNaoConformidade_EhImpeditiva_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbNaoConformidade_EhImpeditiva.CurrentValue = StringUtil.BoolToStr( A1121NaoConformidade_EhImpeditiva);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbNaoConformidade_EhImpeditiva_Internalname, "Values", (String)(cmbNaoConformidade_EhImpeditiva.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkNaoConformidade_Glosavel_Internalname,StringUtil.BoolToStr( A2029NaoConformidade_Glosavel),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_AREATRABALHOCOD"+"_"+sGXsfl_67_idx, GetSecureSignedToken( sGXsfl_67_idx, context.localUtil.Format( (decimal)(A428NaoConformidade_AreaTrabalhoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_CODIGO"+"_"+sGXsfl_67_idx, GetSecureSignedToken( sGXsfl_67_idx, context.localUtil.Format( (decimal)(A426NaoConformidade_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_NOME"+"_"+sGXsfl_67_idx, GetSecureSignedToken( sGXsfl_67_idx, StringUtil.RTrim( context.localUtil.Format( A427NaoConformidade_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_TIPO"+"_"+sGXsfl_67_idx, GetSecureSignedToken( sGXsfl_67_idx, context.localUtil.Format( (decimal)(A429NaoConformidade_Tipo), "Z9")));
            GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_EHIMPEDITIVA"+"_"+sGXsfl_67_idx, GetSecureSignedToken( sGXsfl_67_idx, A1121NaoConformidade_EhImpeditiva));
            GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_GLOSAVEL"+"_"+sGXsfl_67_idx, GetSecureSignedToken( sGXsfl_67_idx, A2029NaoConformidade_Glosavel));
            GridContainer.AddRow(GridRow);
            nGXsfl_67_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_67_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_67_idx+1));
            sGXsfl_67_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_67_idx), 4, 0)), 4, "0");
            SubsflControlProps_672( ) ;
         }
         /* End function sendrow_672 */
      }

      protected void init_default_properties( )
      {
         lblNaoconformidadetitle_Internalname = "NAOCONFORMIDADETITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextnaoconformidade_areatrabalhocod_Internalname = "FILTERTEXTNAOCONFORMIDADE_AREATRABALHOCOD";
         edtavNaoconformidade_areatrabalhocod_Internalname = "vNAOCONFORMIDADE_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavNaoconformidade_nome1_Internalname = "vNAOCONFORMIDADE_NOME1";
         cmbavNaoconformidade_tipo1_Internalname = "vNAOCONFORMIDADE_TIPO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavNaoconformidade_nome2_Internalname = "vNAOCONFORMIDADE_NOME2";
         cmbavNaoconformidade_tipo2_Internalname = "vNAOCONFORMIDADE_TIPO2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtNaoConformidade_AreaTrabalhoCod_Internalname = "NAOCONFORMIDADE_AREATRABALHOCOD";
         edtNaoConformidade_Codigo_Internalname = "NAOCONFORMIDADE_CODIGO";
         edtNaoConformidade_Nome_Internalname = "NAOCONFORMIDADE_NOME";
         cmbNaoConformidade_Tipo_Internalname = "NAOCONFORMIDADE_TIPO";
         cmbNaoConformidade_EhImpeditiva_Internalname = "NAOCONFORMIDADE_EHIMPEDITIVA";
         chkNaoConformidade_Glosavel_Internalname = "NAOCONFORMIDADE_GLOSAVEL";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         edtavTfnaoconformidade_nome_Internalname = "vTFNAOCONFORMIDADE_NOME";
         edtavTfnaoconformidade_nome_sel_Internalname = "vTFNAOCONFORMIDADE_NOME_SEL";
         edtavTfnaoconformidade_ehimpeditiva_sel_Internalname = "vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL";
         edtavTfnaoconformidade_glosavel_sel_Internalname = "vTFNAOCONFORMIDADE_GLOSAVEL_SEL";
         Ddo_naoconformidade_nome_Internalname = "DDO_NAOCONFORMIDADE_NOME";
         edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Internalname = "vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE";
         Ddo_naoconformidade_tipo_Internalname = "DDO_NAOCONFORMIDADE_TIPO";
         edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Internalname = "vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE";
         Ddo_naoconformidade_ehimpeditiva_Internalname = "DDO_NAOCONFORMIDADE_EHIMPEDITIVA";
         edtavDdo_naoconformidade_ehimpeditivatitlecontrolidtoreplace_Internalname = "vDDO_NAOCONFORMIDADE_EHIMPEDITIVATITLECONTROLIDTOREPLACE";
         Ddo_naoconformidade_glosavel_Internalname = "DDO_NAOCONFORMIDADE_GLOSAVEL";
         edtavDdo_naoconformidade_glosaveltitlecontrolidtoreplace_Internalname = "vDDO_NAOCONFORMIDADE_GLOSAVELTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbNaoConformidade_EhImpeditiva_Jsonclick = "";
         cmbNaoConformidade_Tipo_Jsonclick = "";
         edtNaoConformidade_Nome_Jsonclick = "";
         edtNaoConformidade_Codigo_Jsonclick = "";
         edtNaoConformidade_AreaTrabalhoCod_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         cmbavNaoconformidade_tipo2_Jsonclick = "";
         edtavNaoconformidade_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavNaoconformidade_tipo1_Jsonclick = "";
         edtavNaoconformidade_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavNaoconformidade_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtNaoConformidade_Nome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         chkNaoConformidade_Glosavel_Titleformat = 0;
         cmbNaoConformidade_EhImpeditiva_Titleformat = 0;
         cmbNaoConformidade_Tipo_Titleformat = 0;
         edtNaoConformidade_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavNaoconformidade_tipo2.Visible = 1;
         edtavNaoconformidade_nome2_Visible = 1;
         cmbavNaoconformidade_tipo1.Visible = 1;
         edtavNaoconformidade_nome1_Visible = 1;
         chkNaoConformidade_Glosavel.Title.Text = "Glos�vel";
         cmbNaoConformidade_EhImpeditiva.Title.Text = "� impeditiva?";
         cmbNaoConformidade_Tipo.Title.Text = "Tipo";
         edtNaoConformidade_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         chkNaoConformidade_Glosavel.Caption = "";
         edtavDdo_naoconformidade_glosaveltitlecontrolidtoreplace_Visible = 1;
         edtavDdo_naoconformidade_ehimpeditivatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfnaoconformidade_glosavel_sel_Jsonclick = "";
         edtavTfnaoconformidade_glosavel_sel_Visible = 1;
         edtavTfnaoconformidade_ehimpeditiva_sel_Jsonclick = "";
         edtavTfnaoconformidade_ehimpeditiva_sel_Visible = 1;
         edtavTfnaoconformidade_nome_sel_Jsonclick = "";
         edtavTfnaoconformidade_nome_sel_Visible = 1;
         edtavTfnaoconformidade_nome_Jsonclick = "";
         edtavTfnaoconformidade_nome_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_naoconformidade_glosavel_Searchbuttontext = "Pesquisar";
         Ddo_naoconformidade_glosavel_Cleanfilter = "Limpar pesquisa";
         Ddo_naoconformidade_glosavel_Sortdsc = "Ordenar de Z � A";
         Ddo_naoconformidade_glosavel_Sortasc = "Ordenar de A � Z";
         Ddo_naoconformidade_glosavel_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_naoconformidade_glosavel_Datalisttype = "FixedValues";
         Ddo_naoconformidade_glosavel_Includedatalist = Convert.ToBoolean( -1);
         Ddo_naoconformidade_glosavel_Includefilter = Convert.ToBoolean( 0);
         Ddo_naoconformidade_glosavel_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_naoconformidade_glosavel_Includesortasc = Convert.ToBoolean( -1);
         Ddo_naoconformidade_glosavel_Titlecontrolidtoreplace = "";
         Ddo_naoconformidade_glosavel_Dropdownoptionstype = "GridTitleSettings";
         Ddo_naoconformidade_glosavel_Cls = "ColumnSettings";
         Ddo_naoconformidade_glosavel_Tooltip = "Op��es";
         Ddo_naoconformidade_glosavel_Caption = "";
         Ddo_naoconformidade_ehimpeditiva_Searchbuttontext = "Pesquisar";
         Ddo_naoconformidade_ehimpeditiva_Cleanfilter = "Limpar pesquisa";
         Ddo_naoconformidade_ehimpeditiva_Sortdsc = "Ordenar de Z � A";
         Ddo_naoconformidade_ehimpeditiva_Sortasc = "Ordenar de A � Z";
         Ddo_naoconformidade_ehimpeditiva_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_naoconformidade_ehimpeditiva_Datalisttype = "FixedValues";
         Ddo_naoconformidade_ehimpeditiva_Includedatalist = Convert.ToBoolean( -1);
         Ddo_naoconformidade_ehimpeditiva_Includefilter = Convert.ToBoolean( 0);
         Ddo_naoconformidade_ehimpeditiva_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_naoconformidade_ehimpeditiva_Includesortasc = Convert.ToBoolean( -1);
         Ddo_naoconformidade_ehimpeditiva_Titlecontrolidtoreplace = "";
         Ddo_naoconformidade_ehimpeditiva_Dropdownoptionstype = "GridTitleSettings";
         Ddo_naoconformidade_ehimpeditiva_Cls = "ColumnSettings";
         Ddo_naoconformidade_ehimpeditiva_Tooltip = "Op��es";
         Ddo_naoconformidade_ehimpeditiva_Caption = "";
         Ddo_naoconformidade_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_naoconformidade_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_naoconformidade_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_naoconformidade_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_naoconformidade_tipo_Datalistfixedvalues = "1:Demandas,2:Itens da Contagem,3:Contagens,4:Check List Demandas,5:Check List Contagens,6:Homologa��o";
         Ddo_naoconformidade_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_naoconformidade_tipo_Datalisttype = "FixedValues";
         Ddo_naoconformidade_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_naoconformidade_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_naoconformidade_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_naoconformidade_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_naoconformidade_tipo_Titlecontrolidtoreplace = "";
         Ddo_naoconformidade_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_naoconformidade_tipo_Cls = "ColumnSettings";
         Ddo_naoconformidade_tipo_Tooltip = "Op��es";
         Ddo_naoconformidade_tipo_Caption = "";
         Ddo_naoconformidade_nome_Searchbuttontext = "Pesquisar";
         Ddo_naoconformidade_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_naoconformidade_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_naoconformidade_nome_Loadingdata = "Carregando dados...";
         Ddo_naoconformidade_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_naoconformidade_nome_Sortasc = "Ordenar de A � Z";
         Ddo_naoconformidade_nome_Datalistupdateminimumcharacters = 0;
         Ddo_naoconformidade_nome_Datalistproc = "GetWWNaoConformidadeFilterData";
         Ddo_naoconformidade_nome_Datalisttype = "Dynamic";
         Ddo_naoconformidade_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_naoconformidade_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_naoconformidade_nome_Filtertype = "Character";
         Ddo_naoconformidade_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_naoconformidade_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_naoconformidade_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_naoconformidade_nome_Titlecontrolidtoreplace = "";
         Ddo_naoconformidade_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_naoconformidade_nome_Cls = "ColumnSettings";
         Ddo_naoconformidade_nome_Tooltip = "Op��es";
         Ddo_naoconformidade_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " N�o Conformidade";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV33ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_EHIMPEDITIVATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_GLOSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV31TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV32TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV36TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV39TFNaoConformidade_EhImpeditiva_Sel',fld:'vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL',pic:'9',nv:0},{av:'AV46TFNaoConformidade_Glosavel_Sel',fld:'vTFNAOCONFORMIDADE_GLOSAVEL_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV30NaoConformidade_NomeTitleFilterData',fld:'vNAOCONFORMIDADE_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV34NaoConformidade_TipoTitleFilterData',fld:'vNAOCONFORMIDADE_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV38NaoConformidade_EhImpeditivaTitleFilterData',fld:'vNAOCONFORMIDADE_EHIMPEDITIVATITLEFILTERDATA',pic:'',nv:null},{av:'AV45NaoConformidade_GlosavelTitleFilterData',fld:'vNAOCONFORMIDADE_GLOSAVELTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtNaoConformidade_Nome_Titleformat',ctrl:'NAOCONFORMIDADE_NOME',prop:'Titleformat'},{av:'edtNaoConformidade_Nome_Title',ctrl:'NAOCONFORMIDADE_NOME',prop:'Title'},{av:'cmbNaoConformidade_Tipo'},{av:'cmbNaoConformidade_EhImpeditiva'},{av:'chkNaoConformidade_Glosavel_Titleformat',ctrl:'NAOCONFORMIDADE_GLOSAVEL',prop:'Titleformat'},{av:'chkNaoConformidade_Glosavel.Title.Text',ctrl:'NAOCONFORMIDADE_GLOSAVEL',prop:'Title'},{av:'AV43GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV44GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11AL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV31TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV32TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV39TFNaoConformidade_EhImpeditiva_Sel',fld:'vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL',pic:'9',nv:0},{av:'AV46TFNaoConformidade_Glosavel_Sel',fld:'vTFNAOCONFORMIDADE_GLOSAVEL_SEL',pic:'9',nv:0},{av:'AV33ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_EHIMPEDITIVATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_GLOSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_NAOCONFORMIDADE_NOME.ONOPTIONCLICKED","{handler:'E12AL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV31TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV32TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV39TFNaoConformidade_EhImpeditiva_Sel',fld:'vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL',pic:'9',nv:0},{av:'AV46TFNaoConformidade_Glosavel_Sel',fld:'vTFNAOCONFORMIDADE_GLOSAVEL_SEL',pic:'9',nv:0},{av:'AV33ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_EHIMPEDITIVATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_GLOSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_naoconformidade_nome_Activeeventkey',ctrl:'DDO_NAOCONFORMIDADE_NOME',prop:'ActiveEventKey'},{av:'Ddo_naoconformidade_nome_Filteredtext_get',ctrl:'DDO_NAOCONFORMIDADE_NOME',prop:'FilteredText_get'},{av:'Ddo_naoconformidade_nome_Selectedvalue_get',ctrl:'DDO_NAOCONFORMIDADE_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_naoconformidade_nome_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_NOME',prop:'SortedStatus'},{av:'AV31TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV32TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_naoconformidade_tipo_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_TIPO',prop:'SortedStatus'},{av:'Ddo_naoconformidade_ehimpeditiva_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_EHIMPEDITIVA',prop:'SortedStatus'},{av:'Ddo_naoconformidade_glosavel_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_GLOSAVEL',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NAOCONFORMIDADE_TIPO.ONOPTIONCLICKED","{handler:'E13AL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV31TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV32TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV39TFNaoConformidade_EhImpeditiva_Sel',fld:'vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL',pic:'9',nv:0},{av:'AV46TFNaoConformidade_Glosavel_Sel',fld:'vTFNAOCONFORMIDADE_GLOSAVEL_SEL',pic:'9',nv:0},{av:'AV33ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_EHIMPEDITIVATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_GLOSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_naoconformidade_tipo_Activeeventkey',ctrl:'DDO_NAOCONFORMIDADE_TIPO',prop:'ActiveEventKey'},{av:'Ddo_naoconformidade_tipo_Selectedvalue_get',ctrl:'DDO_NAOCONFORMIDADE_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_naoconformidade_tipo_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_TIPO',prop:'SortedStatus'},{av:'AV36TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'Ddo_naoconformidade_nome_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_NOME',prop:'SortedStatus'},{av:'Ddo_naoconformidade_ehimpeditiva_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_EHIMPEDITIVA',prop:'SortedStatus'},{av:'Ddo_naoconformidade_glosavel_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_GLOSAVEL',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NAOCONFORMIDADE_EHIMPEDITIVA.ONOPTIONCLICKED","{handler:'E14AL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV31TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV32TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV39TFNaoConformidade_EhImpeditiva_Sel',fld:'vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL',pic:'9',nv:0},{av:'AV46TFNaoConformidade_Glosavel_Sel',fld:'vTFNAOCONFORMIDADE_GLOSAVEL_SEL',pic:'9',nv:0},{av:'AV33ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_EHIMPEDITIVATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_GLOSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_naoconformidade_ehimpeditiva_Activeeventkey',ctrl:'DDO_NAOCONFORMIDADE_EHIMPEDITIVA',prop:'ActiveEventKey'},{av:'Ddo_naoconformidade_ehimpeditiva_Selectedvalue_get',ctrl:'DDO_NAOCONFORMIDADE_EHIMPEDITIVA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_naoconformidade_ehimpeditiva_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_EHIMPEDITIVA',prop:'SortedStatus'},{av:'AV39TFNaoConformidade_EhImpeditiva_Sel',fld:'vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL',pic:'9',nv:0},{av:'Ddo_naoconformidade_nome_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_NOME',prop:'SortedStatus'},{av:'Ddo_naoconformidade_tipo_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_TIPO',prop:'SortedStatus'},{av:'Ddo_naoconformidade_glosavel_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_GLOSAVEL',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NAOCONFORMIDADE_GLOSAVEL.ONOPTIONCLICKED","{handler:'E15AL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV31TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV32TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV39TFNaoConformidade_EhImpeditiva_Sel',fld:'vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL',pic:'9',nv:0},{av:'AV46TFNaoConformidade_Glosavel_Sel',fld:'vTFNAOCONFORMIDADE_GLOSAVEL_SEL',pic:'9',nv:0},{av:'AV33ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_EHIMPEDITIVATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_GLOSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_naoconformidade_glosavel_Activeeventkey',ctrl:'DDO_NAOCONFORMIDADE_GLOSAVEL',prop:'ActiveEventKey'},{av:'Ddo_naoconformidade_glosavel_Selectedvalue_get',ctrl:'DDO_NAOCONFORMIDADE_GLOSAVEL',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_naoconformidade_glosavel_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_GLOSAVEL',prop:'SortedStatus'},{av:'AV46TFNaoConformidade_Glosavel_Sel',fld:'vTFNAOCONFORMIDADE_GLOSAVEL_SEL',pic:'9',nv:0},{av:'Ddo_naoconformidade_nome_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_NOME',prop:'SortedStatus'},{av:'Ddo_naoconformidade_tipo_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_TIPO',prop:'SortedStatus'},{av:'Ddo_naoconformidade_ehimpeditiva_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_EHIMPEDITIVA',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E26AL2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV25Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV26Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtNaoConformidade_Nome_Link',ctrl:'NAOCONFORMIDADE_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E16AL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV31TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV32TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV39TFNaoConformidade_EhImpeditiva_Sel',fld:'vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL',pic:'9',nv:0},{av:'AV46TFNaoConformidade_Glosavel_Sel',fld:'vTFNAOCONFORMIDADE_GLOSAVEL_SEL',pic:'9',nv:0},{av:'AV33ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_EHIMPEDITIVATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_GLOSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E21AL2',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E17AL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV31TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV32TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV39TFNaoConformidade_EhImpeditiva_Sel',fld:'vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL',pic:'9',nv:0},{av:'AV46TFNaoConformidade_Glosavel_Sel',fld:'vTFNAOCONFORMIDADE_GLOSAVEL_SEL',pic:'9',nv:0},{av:'AV33ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_EHIMPEDITIVATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_GLOSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'edtavNaoconformidade_nome2_Visible',ctrl:'vNAOCONFORMIDADE_NOME2',prop:'Visible'},{av:'cmbavNaoconformidade_tipo2'},{av:'edtavNaoconformidade_nome1_Visible',ctrl:'vNAOCONFORMIDADE_NOME1',prop:'Visible'},{av:'cmbavNaoconformidade_tipo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E22AL2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavNaoconformidade_nome1_Visible',ctrl:'vNAOCONFORMIDADE_NOME1',prop:'Visible'},{av:'cmbavNaoconformidade_tipo1'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E18AL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV31TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV32TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV39TFNaoConformidade_EhImpeditiva_Sel',fld:'vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL',pic:'9',nv:0},{av:'AV46TFNaoConformidade_Glosavel_Sel',fld:'vTFNAOCONFORMIDADE_GLOSAVEL_SEL',pic:'9',nv:0},{av:'AV33ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_EHIMPEDITIVATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_GLOSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'edtavNaoconformidade_nome2_Visible',ctrl:'vNAOCONFORMIDADE_NOME2',prop:'Visible'},{av:'cmbavNaoconformidade_tipo2'},{av:'edtavNaoconformidade_nome1_Visible',ctrl:'vNAOCONFORMIDADE_NOME1',prop:'Visible'},{av:'cmbavNaoconformidade_tipo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E23AL2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavNaoconformidade_nome2_Visible',ctrl:'vNAOCONFORMIDADE_NOME2',prop:'Visible'},{av:'cmbavNaoconformidade_tipo2'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E19AL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV31TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV32TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV39TFNaoConformidade_EhImpeditiva_Sel',fld:'vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL',pic:'9',nv:0},{av:'AV46TFNaoConformidade_Glosavel_Sel',fld:'vTFNAOCONFORMIDADE_GLOSAVEL_SEL',pic:'9',nv:0},{av:'AV33ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_EHIMPEDITIVATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_GLOSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV31TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'Ddo_naoconformidade_nome_Filteredtext_set',ctrl:'DDO_NAOCONFORMIDADE_NOME',prop:'FilteredText_set'},{av:'AV32TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_naoconformidade_nome_Selectedvalue_set',ctrl:'DDO_NAOCONFORMIDADE_NOME',prop:'SelectedValue_set'},{av:'AV36TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'Ddo_naoconformidade_tipo_Selectedvalue_set',ctrl:'DDO_NAOCONFORMIDADE_TIPO',prop:'SelectedValue_set'},{av:'AV39TFNaoConformidade_EhImpeditiva_Sel',fld:'vTFNAOCONFORMIDADE_EHIMPEDITIVA_SEL',pic:'9',nv:0},{av:'Ddo_naoconformidade_ehimpeditiva_Selectedvalue_set',ctrl:'DDO_NAOCONFORMIDADE_EHIMPEDITIVA',prop:'SelectedValue_set'},{av:'AV46TFNaoConformidade_Glosavel_Sel',fld:'vTFNAOCONFORMIDADE_GLOSAVEL_SEL',pic:'9',nv:0},{av:'Ddo_naoconformidade_glosavel_Selectedvalue_set',ctrl:'DDO_NAOCONFORMIDADE_GLOSAVEL',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavNaoconformidade_nome1_Visible',ctrl:'vNAOCONFORMIDADE_NOME1',prop:'Visible'},{av:'cmbavNaoconformidade_tipo1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'edtavNaoconformidade_nome2_Visible',ctrl:'vNAOCONFORMIDADE_NOME2',prop:'Visible'},{av:'cmbavNaoconformidade_tipo2'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E20AL2',iparms:[{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_naoconformidade_nome_Activeeventkey = "";
         Ddo_naoconformidade_nome_Filteredtext_get = "";
         Ddo_naoconformidade_nome_Selectedvalue_get = "";
         Ddo_naoconformidade_tipo_Activeeventkey = "";
         Ddo_naoconformidade_tipo_Selectedvalue_get = "";
         Ddo_naoconformidade_ehimpeditiva_Activeeventkey = "";
         Ddo_naoconformidade_ehimpeditiva_Selectedvalue_get = "";
         Ddo_naoconformidade_glosavel_Activeeventkey = "";
         Ddo_naoconformidade_glosavel_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16DynamicFiltersSelector1 = "";
         AV17NaoConformidade_Nome1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV21NaoConformidade_Nome2 = "";
         AV31TFNaoConformidade_Nome = "";
         AV32TFNaoConformidade_Nome_Sel = "";
         AV33ddo_NaoConformidade_NomeTitleControlIdToReplace = "";
         AV37ddo_NaoConformidade_TipoTitleControlIdToReplace = "";
         AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace = "";
         AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace = "";
         AV36TFNaoConformidade_Tipo_Sels = new GxSimpleCollection();
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV65Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV41DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV30NaoConformidade_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34NaoConformidade_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38NaoConformidade_EhImpeditivaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45NaoConformidade_GlosavelTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_naoconformidade_nome_Filteredtext_set = "";
         Ddo_naoconformidade_nome_Selectedvalue_set = "";
         Ddo_naoconformidade_nome_Sortedstatus = "";
         Ddo_naoconformidade_tipo_Selectedvalue_set = "";
         Ddo_naoconformidade_tipo_Sortedstatus = "";
         Ddo_naoconformidade_ehimpeditiva_Selectedvalue_set = "";
         Ddo_naoconformidade_ehimpeditiva_Sortedstatus = "";
         Ddo_naoconformidade_glosavel_Selectedvalue_set = "";
         Ddo_naoconformidade_glosavel_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV25Update = "";
         AV63Update_GXI = "";
         AV26Delete = "";
         AV64Delete_GXI = "";
         A427NaoConformidade_Nome = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV52WWNaoConformidadeDS_3_Naoconformidade_nome1 = "";
         lV56WWNaoConformidadeDS_7_Naoconformidade_nome2 = "";
         lV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome = "";
         AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1 = "";
         AV52WWNaoConformidadeDS_3_Naoconformidade_nome1 = "";
         AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2 = "";
         AV56WWNaoConformidadeDS_7_Naoconformidade_nome2 = "";
         AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel = "";
         AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome = "";
         H00AL2_A2029NaoConformidade_Glosavel = new bool[] {false} ;
         H00AL2_n2029NaoConformidade_Glosavel = new bool[] {false} ;
         H00AL2_A1121NaoConformidade_EhImpeditiva = new bool[] {false} ;
         H00AL2_n1121NaoConformidade_EhImpeditiva = new bool[] {false} ;
         H00AL2_A429NaoConformidade_Tipo = new short[1] ;
         H00AL2_A427NaoConformidade_Nome = new String[] {""} ;
         H00AL2_A426NaoConformidade_Codigo = new int[1] ;
         H00AL2_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00AL3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV35TFNaoConformidade_Tipo_SelsJson = "";
         GridRow = new GXWebRow();
         imgInsert_Link = "";
         AV27Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblNaoconformidadetitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblFiltertextnaoconformidade_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwnaoconformidade__default(),
            new Object[][] {
                new Object[] {
               H00AL2_A2029NaoConformidade_Glosavel, H00AL2_n2029NaoConformidade_Glosavel, H00AL2_A1121NaoConformidade_EhImpeditiva, H00AL2_n1121NaoConformidade_EhImpeditiva, H00AL2_A429NaoConformidade_Tipo, H00AL2_A427NaoConformidade_Nome, H00AL2_A426NaoConformidade_Codigo, H00AL2_A428NaoConformidade_AreaTrabalhoCod
               }
               , new Object[] {
               H00AL3_AGRID_nRecordCount
               }
            }
         );
         AV65Pgmname = "WWNaoConformidade";
         /* GeneXus formulas. */
         AV65Pgmname = "WWNaoConformidade";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_67 ;
      private short nGXsfl_67_idx=1 ;
      private short AV13OrderedBy ;
      private short AV18NaoConformidade_Tipo1 ;
      private short AV22NaoConformidade_Tipo2 ;
      private short AV39TFNaoConformidade_EhImpeditiva_Sel ;
      private short AV46TFNaoConformidade_Glosavel_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A429NaoConformidade_Tipo ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_67_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1 ;
      private short AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2 ;
      private short AV61WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel ;
      private short AV62WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel ;
      private short edtNaoConformidade_Nome_Titleformat ;
      private short cmbNaoConformidade_Tipo_Titleformat ;
      private short cmbNaoConformidade_EhImpeditiva_Titleformat ;
      private short chkNaoConformidade_Glosavel_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV15NaoConformidade_AreaTrabalhoCod ;
      private int A426NaoConformidade_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_naoconformidade_nome_Datalistupdateminimumcharacters ;
      private int edtavTfnaoconformidade_nome_Visible ;
      private int edtavTfnaoconformidade_nome_sel_Visible ;
      private int edtavTfnaoconformidade_ehimpeditiva_sel_Visible ;
      private int edtavTfnaoconformidade_glosavel_sel_Visible ;
      private int edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_naoconformidade_ehimpeditivatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_naoconformidade_glosaveltitlecontrolidtoreplace_Visible ;
      private int A428NaoConformidade_AreaTrabalhoCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels_Count ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int AV50WWNaoConformidadeDS_1_Naoconformidade_areatrabalhocod ;
      private int edtavOrdereddsc_Visible ;
      private int AV42PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgInsert_Enabled ;
      private int edtavNaoconformidade_nome1_Visible ;
      private int edtavNaoconformidade_nome2_Visible ;
      private int AV66GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV43GridCurrentPage ;
      private long AV44GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_naoconformidade_nome_Activeeventkey ;
      private String Ddo_naoconformidade_nome_Filteredtext_get ;
      private String Ddo_naoconformidade_nome_Selectedvalue_get ;
      private String Ddo_naoconformidade_tipo_Activeeventkey ;
      private String Ddo_naoconformidade_tipo_Selectedvalue_get ;
      private String Ddo_naoconformidade_ehimpeditiva_Activeeventkey ;
      private String Ddo_naoconformidade_ehimpeditiva_Selectedvalue_get ;
      private String Ddo_naoconformidade_glosavel_Activeeventkey ;
      private String Ddo_naoconformidade_glosavel_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_67_idx="0001" ;
      private String AV17NaoConformidade_Nome1 ;
      private String AV21NaoConformidade_Nome2 ;
      private String AV31TFNaoConformidade_Nome ;
      private String AV32TFNaoConformidade_Nome_Sel ;
      private String AV65Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_naoconformidade_nome_Caption ;
      private String Ddo_naoconformidade_nome_Tooltip ;
      private String Ddo_naoconformidade_nome_Cls ;
      private String Ddo_naoconformidade_nome_Filteredtext_set ;
      private String Ddo_naoconformidade_nome_Selectedvalue_set ;
      private String Ddo_naoconformidade_nome_Dropdownoptionstype ;
      private String Ddo_naoconformidade_nome_Titlecontrolidtoreplace ;
      private String Ddo_naoconformidade_nome_Sortedstatus ;
      private String Ddo_naoconformidade_nome_Filtertype ;
      private String Ddo_naoconformidade_nome_Datalisttype ;
      private String Ddo_naoconformidade_nome_Datalistproc ;
      private String Ddo_naoconformidade_nome_Sortasc ;
      private String Ddo_naoconformidade_nome_Sortdsc ;
      private String Ddo_naoconformidade_nome_Loadingdata ;
      private String Ddo_naoconformidade_nome_Cleanfilter ;
      private String Ddo_naoconformidade_nome_Noresultsfound ;
      private String Ddo_naoconformidade_nome_Searchbuttontext ;
      private String Ddo_naoconformidade_tipo_Caption ;
      private String Ddo_naoconformidade_tipo_Tooltip ;
      private String Ddo_naoconformidade_tipo_Cls ;
      private String Ddo_naoconformidade_tipo_Selectedvalue_set ;
      private String Ddo_naoconformidade_tipo_Dropdownoptionstype ;
      private String Ddo_naoconformidade_tipo_Titlecontrolidtoreplace ;
      private String Ddo_naoconformidade_tipo_Sortedstatus ;
      private String Ddo_naoconformidade_tipo_Datalisttype ;
      private String Ddo_naoconformidade_tipo_Datalistfixedvalues ;
      private String Ddo_naoconformidade_tipo_Sortasc ;
      private String Ddo_naoconformidade_tipo_Sortdsc ;
      private String Ddo_naoconformidade_tipo_Cleanfilter ;
      private String Ddo_naoconformidade_tipo_Searchbuttontext ;
      private String Ddo_naoconformidade_ehimpeditiva_Caption ;
      private String Ddo_naoconformidade_ehimpeditiva_Tooltip ;
      private String Ddo_naoconformidade_ehimpeditiva_Cls ;
      private String Ddo_naoconformidade_ehimpeditiva_Selectedvalue_set ;
      private String Ddo_naoconformidade_ehimpeditiva_Dropdownoptionstype ;
      private String Ddo_naoconformidade_ehimpeditiva_Titlecontrolidtoreplace ;
      private String Ddo_naoconformidade_ehimpeditiva_Sortedstatus ;
      private String Ddo_naoconformidade_ehimpeditiva_Datalisttype ;
      private String Ddo_naoconformidade_ehimpeditiva_Datalistfixedvalues ;
      private String Ddo_naoconformidade_ehimpeditiva_Sortasc ;
      private String Ddo_naoconformidade_ehimpeditiva_Sortdsc ;
      private String Ddo_naoconformidade_ehimpeditiva_Cleanfilter ;
      private String Ddo_naoconformidade_ehimpeditiva_Searchbuttontext ;
      private String Ddo_naoconformidade_glosavel_Caption ;
      private String Ddo_naoconformidade_glosavel_Tooltip ;
      private String Ddo_naoconformidade_glosavel_Cls ;
      private String Ddo_naoconformidade_glosavel_Selectedvalue_set ;
      private String Ddo_naoconformidade_glosavel_Dropdownoptionstype ;
      private String Ddo_naoconformidade_glosavel_Titlecontrolidtoreplace ;
      private String Ddo_naoconformidade_glosavel_Sortedstatus ;
      private String Ddo_naoconformidade_glosavel_Datalisttype ;
      private String Ddo_naoconformidade_glosavel_Datalistfixedvalues ;
      private String Ddo_naoconformidade_glosavel_Sortasc ;
      private String Ddo_naoconformidade_glosavel_Sortdsc ;
      private String Ddo_naoconformidade_glosavel_Cleanfilter ;
      private String Ddo_naoconformidade_glosavel_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfnaoconformidade_nome_Internalname ;
      private String edtavTfnaoconformidade_nome_Jsonclick ;
      private String edtavTfnaoconformidade_nome_sel_Internalname ;
      private String edtavTfnaoconformidade_nome_sel_Jsonclick ;
      private String edtavTfnaoconformidade_ehimpeditiva_sel_Internalname ;
      private String edtavTfnaoconformidade_ehimpeditiva_sel_Jsonclick ;
      private String edtavTfnaoconformidade_glosavel_sel_Internalname ;
      private String edtavTfnaoconformidade_glosavel_sel_Jsonclick ;
      private String edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_naoconformidade_ehimpeditivatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_naoconformidade_glosaveltitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtNaoConformidade_AreaTrabalhoCod_Internalname ;
      private String edtNaoConformidade_Codigo_Internalname ;
      private String A427NaoConformidade_Nome ;
      private String edtNaoConformidade_Nome_Internalname ;
      private String cmbNaoConformidade_Tipo_Internalname ;
      private String cmbNaoConformidade_EhImpeditiva_Internalname ;
      private String chkNaoConformidade_Glosavel_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV52WWNaoConformidadeDS_3_Naoconformidade_nome1 ;
      private String lV56WWNaoConformidadeDS_7_Naoconformidade_nome2 ;
      private String lV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome ;
      private String AV52WWNaoConformidadeDS_3_Naoconformidade_nome1 ;
      private String AV56WWNaoConformidadeDS_7_Naoconformidade_nome2 ;
      private String AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel ;
      private String AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavNaoconformidade_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavNaoconformidade_nome1_Internalname ;
      private String cmbavNaoconformidade_tipo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavNaoconformidade_nome2_Internalname ;
      private String cmbavNaoconformidade_tipo2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_naoconformidade_nome_Internalname ;
      private String Ddo_naoconformidade_tipo_Internalname ;
      private String Ddo_naoconformidade_ehimpeditiva_Internalname ;
      private String Ddo_naoconformidade_glosavel_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtNaoConformidade_Nome_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtNaoConformidade_Nome_Link ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblNaoconformidadetitle_Internalname ;
      private String lblNaoconformidadetitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextnaoconformidade_areatrabalhocod_Internalname ;
      private String lblFiltertextnaoconformidade_areatrabalhocod_Jsonclick ;
      private String edtavNaoconformidade_areatrabalhocod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavNaoconformidade_nome1_Jsonclick ;
      private String cmbavNaoconformidade_tipo1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavNaoconformidade_nome2_Jsonclick ;
      private String cmbavNaoconformidade_tipo2_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_67_fel_idx="0001" ;
      private String ROClassString ;
      private String edtNaoConformidade_AreaTrabalhoCod_Jsonclick ;
      private String edtNaoConformidade_Codigo_Jsonclick ;
      private String edtNaoConformidade_Nome_Jsonclick ;
      private String cmbNaoConformidade_Tipo_Jsonclick ;
      private String cmbNaoConformidade_EhImpeditiva_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersIgnoreFirst ;
      private bool AV23DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_naoconformidade_nome_Includesortasc ;
      private bool Ddo_naoconformidade_nome_Includesortdsc ;
      private bool Ddo_naoconformidade_nome_Includefilter ;
      private bool Ddo_naoconformidade_nome_Filterisrange ;
      private bool Ddo_naoconformidade_nome_Includedatalist ;
      private bool Ddo_naoconformidade_tipo_Includesortasc ;
      private bool Ddo_naoconformidade_tipo_Includesortdsc ;
      private bool Ddo_naoconformidade_tipo_Includefilter ;
      private bool Ddo_naoconformidade_tipo_Includedatalist ;
      private bool Ddo_naoconformidade_tipo_Allowmultipleselection ;
      private bool Ddo_naoconformidade_ehimpeditiva_Includesortasc ;
      private bool Ddo_naoconformidade_ehimpeditiva_Includesortdsc ;
      private bool Ddo_naoconformidade_ehimpeditiva_Includefilter ;
      private bool Ddo_naoconformidade_ehimpeditiva_Includedatalist ;
      private bool Ddo_naoconformidade_glosavel_Includesortasc ;
      private bool Ddo_naoconformidade_glosavel_Includesortdsc ;
      private bool Ddo_naoconformidade_glosavel_Includefilter ;
      private bool Ddo_naoconformidade_glosavel_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A1121NaoConformidade_EhImpeditiva ;
      private bool n1121NaoConformidade_EhImpeditiva ;
      private bool A2029NaoConformidade_Glosavel ;
      private bool n2029NaoConformidade_Glosavel ;
      private bool AV54WWNaoConformidadeDS_5_Dynamicfiltersenabled2 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV25Update_IsBlob ;
      private bool AV26Delete_IsBlob ;
      private String AV35TFNaoConformidade_Tipo_SelsJson ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV33ddo_NaoConformidade_NomeTitleControlIdToReplace ;
      private String AV37ddo_NaoConformidade_TipoTitleControlIdToReplace ;
      private String AV40ddo_NaoConformidade_EhImpeditivaTitleControlIdToReplace ;
      private String AV47ddo_NaoConformidade_GlosavelTitleControlIdToReplace ;
      private String AV63Update_GXI ;
      private String AV64Delete_GXI ;
      private String AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1 ;
      private String AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2 ;
      private String AV25Update ;
      private String AV26Delete ;
      private String imgInsert_Bitmap ;
      private IGxSession AV27Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavNaoconformidade_tipo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavNaoconformidade_tipo2 ;
      private GXCombobox cmbNaoConformidade_Tipo ;
      private GXCombobox cmbNaoConformidade_EhImpeditiva ;
      private GXCheckbox chkNaoConformidade_Glosavel ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private bool[] H00AL2_A2029NaoConformidade_Glosavel ;
      private bool[] H00AL2_n2029NaoConformidade_Glosavel ;
      private bool[] H00AL2_A1121NaoConformidade_EhImpeditiva ;
      private bool[] H00AL2_n1121NaoConformidade_EhImpeditiva ;
      private short[] H00AL2_A429NaoConformidade_Tipo ;
      private String[] H00AL2_A427NaoConformidade_Nome ;
      private int[] H00AL2_A426NaoConformidade_Codigo ;
      private int[] H00AL2_A428NaoConformidade_AreaTrabalhoCod ;
      private long[] H00AL3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV36TFNaoConformidade_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30NaoConformidade_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34NaoConformidade_TipoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38NaoConformidade_EhImpeditivaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV45NaoConformidade_GlosavelTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV41DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwnaoconformidade__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00AL2( IGxContext context ,
                                             short A429NaoConformidade_Tipo ,
                                             IGxCollection AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels ,
                                             String AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1 ,
                                             String AV52WWNaoConformidadeDS_3_Naoconformidade_nome1 ,
                                             short AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1 ,
                                             bool AV54WWNaoConformidadeDS_5_Dynamicfiltersenabled2 ,
                                             String AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2 ,
                                             String AV56WWNaoConformidadeDS_7_Naoconformidade_nome2 ,
                                             short AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2 ,
                                             String AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel ,
                                             String AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome ,
                                             int AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels_Count ,
                                             short AV61WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel ,
                                             short AV62WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel ,
                                             String A427NaoConformidade_Nome ,
                                             bool A1121NaoConformidade_EhImpeditiva ,
                                             bool A2029NaoConformidade_Glosavel ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A428NaoConformidade_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [12] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [NaoConformidade_Glosavel], [NaoConformidade_EhImpeditiva], [NaoConformidade_Tipo], [NaoConformidade_Nome], [NaoConformidade_Codigo], [NaoConformidade_AreaTrabalhoCod]";
         sFromString = " FROM [NaoConformidade] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([NaoConformidade_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1, "NAOCONFORMIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWNaoConformidadeDS_3_Naoconformidade_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] like '%' + @lV52WWNaoConformidadeDS_3_Naoconformidade_nome1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1, "NAOCONFORMIDADE_TIPO") == 0 ) && ( ! (0==AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Tipo] = @AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV54WWNaoConformidadeDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2, "NAOCONFORMIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWNaoConformidadeDS_7_Naoconformidade_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] like '%' + @lV56WWNaoConformidadeDS_7_Naoconformidade_nome2)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV54WWNaoConformidadeDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2, "NAOCONFORMIDADE_TIPO") == 0 ) && ( ! (0==AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Tipo] = @AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome)) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] like @lV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel)) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] = @AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels, "[NaoConformidade_Tipo] IN (", ")") + ")";
         }
         if ( AV61WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel == 1 )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_EhImpeditiva] = 1)";
         }
         if ( AV61WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel == 2 )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_EhImpeditiva] = 0)";
         }
         if ( AV62WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel == 1 )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Glosavel] = 1)";
         }
         if ( AV62WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel == 2 )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Glosavel] = 0)";
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [NaoConformidade_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [NaoConformidade_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [NaoConformidade_Tipo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [NaoConformidade_Tipo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [NaoConformidade_EhImpeditiva]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [NaoConformidade_EhImpeditiva] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [NaoConformidade_Glosavel]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [NaoConformidade_Glosavel] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [NaoConformidade_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00AL3( IGxContext context ,
                                             short A429NaoConformidade_Tipo ,
                                             IGxCollection AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels ,
                                             String AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1 ,
                                             String AV52WWNaoConformidadeDS_3_Naoconformidade_nome1 ,
                                             short AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1 ,
                                             bool AV54WWNaoConformidadeDS_5_Dynamicfiltersenabled2 ,
                                             String AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2 ,
                                             String AV56WWNaoConformidadeDS_7_Naoconformidade_nome2 ,
                                             short AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2 ,
                                             String AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel ,
                                             String AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome ,
                                             int AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels_Count ,
                                             short AV61WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel ,
                                             short AV62WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel ,
                                             String A427NaoConformidade_Nome ,
                                             bool A1121NaoConformidade_EhImpeditiva ,
                                             bool A2029NaoConformidade_Glosavel ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A428NaoConformidade_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [7] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [NaoConformidade] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([NaoConformidade_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1, "NAOCONFORMIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWNaoConformidadeDS_3_Naoconformidade_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] like '%' + @lV52WWNaoConformidadeDS_3_Naoconformidade_nome1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV51WWNaoConformidadeDS_2_Dynamicfiltersselector1, "NAOCONFORMIDADE_TIPO") == 0 ) && ( ! (0==AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Tipo] = @AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV54WWNaoConformidadeDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2, "NAOCONFORMIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWNaoConformidadeDS_7_Naoconformidade_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] like '%' + @lV56WWNaoConformidadeDS_7_Naoconformidade_nome2)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV54WWNaoConformidadeDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWNaoConformidadeDS_6_Dynamicfiltersselector2, "NAOCONFORMIDADE_TIPO") == 0 ) && ( ! (0==AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Tipo] = @AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome)) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] like @lV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel)) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] = @AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV60WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels, "[NaoConformidade_Tipo] IN (", ")") + ")";
         }
         if ( AV61WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel == 1 )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_EhImpeditiva] = 1)";
         }
         if ( AV61WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel == 2 )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_EhImpeditiva] = 0)";
         }
         if ( AV62WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel == 1 )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Glosavel] = 1)";
         }
         if ( AV62WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel == 2 )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Glosavel] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00AL2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (bool)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] );
               case 1 :
                     return conditional_H00AL3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (bool)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00AL2 ;
          prmH00AL2 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV52WWNaoConformidadeDS_3_Naoconformidade_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV56WWNaoConformidadeDS_7_Naoconformidade_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00AL3 ;
          prmH00AL3 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV52WWNaoConformidadeDS_3_Naoconformidade_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV53WWNaoConformidadeDS_4_Naoconformidade_tipo1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV56WWNaoConformidadeDS_7_Naoconformidade_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57WWNaoConformidadeDS_8_Naoconformidade_tipo2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV58WWNaoConformidadeDS_9_Tfnaoconformidade_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV59WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00AL2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AL2,11,0,true,false )
             ,new CursorDef("H00AL3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AL3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

}
