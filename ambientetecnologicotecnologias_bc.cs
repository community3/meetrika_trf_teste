/*
               File: AmbienteTecnologicoTecnologias_BC
        Description: Ambiente Tecnologico Tecnologias
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:55:15.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class ambientetecnologicotecnologias_bc : GXHttpHandler, IGxSilentTrn
   {
      public ambientetecnologicotecnologias_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public ambientetecnologicotecnologias_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow1K57( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey1K57( ) ;
         standaloneModal( ) ;
         AddRow1K57( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z351AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
               Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_1K0( )
      {
         BeforeValidate1K57( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1K57( ) ;
            }
            else
            {
               CheckExtendedTable1K57( ) ;
               if ( AnyError == 0 )
               {
                  ZM1K57( 3) ;
                  ZM1K57( 4) ;
               }
               CloseExtendedTableCursors1K57( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM1K57( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z354AmbienteTecnologicoTecnologias_Ativo = A354AmbienteTecnologicoTecnologias_Ativo;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z352AmbienteTecnologico_Descricao = A352AmbienteTecnologico_Descricao;
            Z728AmbienteTecnologico_AreaTrabalhoCod = A728AmbienteTecnologico_AreaTrabalhoCod;
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            Z132Tecnologia_Nome = A132Tecnologia_Nome;
         }
         if ( GX_JID == -2 )
         {
            Z354AmbienteTecnologicoTecnologias_Ativo = A354AmbienteTecnologicoTecnologias_Ativo;
            Z351AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            Z352AmbienteTecnologico_Descricao = A352AmbienteTecnologico_Descricao;
            Z728AmbienteTecnologico_AreaTrabalhoCod = A728AmbienteTecnologico_AreaTrabalhoCod;
            Z132Tecnologia_Nome = A132Tecnologia_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A354AmbienteTecnologicoTecnologias_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A354AmbienteTecnologicoTecnologias_Ativo = true;
         }
      }

      protected void Load1K57( )
      {
         /* Using cursor BC001K6 */
         pr_default.execute(4, new Object[] {A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound57 = 1;
            A352AmbienteTecnologico_Descricao = BC001K6_A352AmbienteTecnologico_Descricao[0];
            A132Tecnologia_Nome = BC001K6_A132Tecnologia_Nome[0];
            A354AmbienteTecnologicoTecnologias_Ativo = BC001K6_A354AmbienteTecnologicoTecnologias_Ativo[0];
            A728AmbienteTecnologico_AreaTrabalhoCod = BC001K6_A728AmbienteTecnologico_AreaTrabalhoCod[0];
            ZM1K57( -2) ;
         }
         pr_default.close(4);
         OnLoadActions1K57( ) ;
      }

      protected void OnLoadActions1K57( )
      {
      }

      protected void CheckExtendedTable1K57( )
      {
         standaloneModal( ) ;
         /* Using cursor BC001K4 */
         pr_default.execute(2, new Object[] {A351AmbienteTecnologico_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T62'.", "ForeignKeyNotFound", 1, "AMBIENTETECNOLOGICO_CODIGO");
            AnyError = 1;
         }
         A352AmbienteTecnologico_Descricao = BC001K4_A352AmbienteTecnologico_Descricao[0];
         A728AmbienteTecnologico_AreaTrabalhoCod = BC001K4_A728AmbienteTecnologico_AreaTrabalhoCod[0];
         pr_default.close(2);
         /* Using cursor BC001K5 */
         pr_default.execute(3, new Object[] {A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tecnologia'.", "ForeignKeyNotFound", 1, "TECNOLOGIA_CODIGO");
            AnyError = 1;
         }
         A132Tecnologia_Nome = BC001K5_A132Tecnologia_Nome[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors1K57( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1K57( )
      {
         /* Using cursor BC001K7 */
         pr_default.execute(5, new Object[] {A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound57 = 1;
         }
         else
         {
            RcdFound57 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC001K3 */
         pr_default.execute(1, new Object[] {A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1K57( 2) ;
            RcdFound57 = 1;
            A354AmbienteTecnologicoTecnologias_Ativo = BC001K3_A354AmbienteTecnologicoTecnologias_Ativo[0];
            A351AmbienteTecnologico_Codigo = BC001K3_A351AmbienteTecnologico_Codigo[0];
            A131Tecnologia_Codigo = BC001K3_A131Tecnologia_Codigo[0];
            Z351AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            sMode57 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load1K57( ) ;
            if ( AnyError == 1 )
            {
               RcdFound57 = 0;
               InitializeNonKey1K57( ) ;
            }
            Gx_mode = sMode57;
         }
         else
         {
            RcdFound57 = 0;
            InitializeNonKey1K57( ) ;
            sMode57 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode57;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1K57( ) ;
         if ( RcdFound57 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_1K0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency1K57( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC001K2 */
            pr_default.execute(0, new Object[] {A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AmbienteTecnologicoTecnologias"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z354AmbienteTecnologicoTecnologias_Ativo != BC001K2_A354AmbienteTecnologicoTecnologias_Ativo[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"AmbienteTecnologicoTecnologias"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1K57( )
      {
         BeforeValidate1K57( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1K57( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1K57( 0) ;
            CheckOptimisticConcurrency1K57( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1K57( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1K57( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001K8 */
                     pr_default.execute(6, new Object[] {A354AmbienteTecnologicoTecnologias_Ativo, A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("AmbienteTecnologicoTecnologias") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1K57( ) ;
            }
            EndLevel1K57( ) ;
         }
         CloseExtendedTableCursors1K57( ) ;
      }

      protected void Update1K57( )
      {
         BeforeValidate1K57( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1K57( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1K57( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1K57( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1K57( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001K9 */
                     pr_default.execute(7, new Object[] {A354AmbienteTecnologicoTecnologias_Ativo, A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("AmbienteTecnologicoTecnologias") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AmbienteTecnologicoTecnologias"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1K57( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1K57( ) ;
         }
         CloseExtendedTableCursors1K57( ) ;
      }

      protected void DeferredUpdate1K57( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate1K57( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1K57( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1K57( ) ;
            AfterConfirm1K57( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1K57( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC001K10 */
                  pr_default.execute(8, new Object[] {A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("AmbienteTecnologicoTecnologias") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode57 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel1K57( ) ;
         Gx_mode = sMode57;
      }

      protected void OnDeleteControls1K57( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC001K11 */
            pr_default.execute(9, new Object[] {A351AmbienteTecnologico_Codigo});
            A352AmbienteTecnologico_Descricao = BC001K11_A352AmbienteTecnologico_Descricao[0];
            A728AmbienteTecnologico_AreaTrabalhoCod = BC001K11_A728AmbienteTecnologico_AreaTrabalhoCod[0];
            pr_default.close(9);
            /* Using cursor BC001K12 */
            pr_default.execute(10, new Object[] {A131Tecnologia_Codigo});
            A132Tecnologia_Nome = BC001K12_A132Tecnologia_Nome[0];
            pr_default.close(10);
         }
      }

      protected void EndLevel1K57( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1K57( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart1K57( )
      {
         /* Using cursor BC001K13 */
         pr_default.execute(11, new Object[] {A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
         RcdFound57 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound57 = 1;
            A352AmbienteTecnologico_Descricao = BC001K13_A352AmbienteTecnologico_Descricao[0];
            A132Tecnologia_Nome = BC001K13_A132Tecnologia_Nome[0];
            A354AmbienteTecnologicoTecnologias_Ativo = BC001K13_A354AmbienteTecnologicoTecnologias_Ativo[0];
            A351AmbienteTecnologico_Codigo = BC001K13_A351AmbienteTecnologico_Codigo[0];
            A131Tecnologia_Codigo = BC001K13_A131Tecnologia_Codigo[0];
            A728AmbienteTecnologico_AreaTrabalhoCod = BC001K13_A728AmbienteTecnologico_AreaTrabalhoCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext1K57( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound57 = 0;
         ScanKeyLoad1K57( ) ;
      }

      protected void ScanKeyLoad1K57( )
      {
         sMode57 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound57 = 1;
            A352AmbienteTecnologico_Descricao = BC001K13_A352AmbienteTecnologico_Descricao[0];
            A132Tecnologia_Nome = BC001K13_A132Tecnologia_Nome[0];
            A354AmbienteTecnologicoTecnologias_Ativo = BC001K13_A354AmbienteTecnologicoTecnologias_Ativo[0];
            A351AmbienteTecnologico_Codigo = BC001K13_A351AmbienteTecnologico_Codigo[0];
            A131Tecnologia_Codigo = BC001K13_A131Tecnologia_Codigo[0];
            A728AmbienteTecnologico_AreaTrabalhoCod = BC001K13_A728AmbienteTecnologico_AreaTrabalhoCod[0];
         }
         Gx_mode = sMode57;
      }

      protected void ScanKeyEnd1K57( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm1K57( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1K57( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1K57( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1K57( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1K57( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1K57( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1K57( )
      {
      }

      protected void AddRow1K57( )
      {
         VarsToRow57( bcAmbienteTecnologicoTecnologias) ;
      }

      protected void ReadRow1K57( )
      {
         RowToVars57( bcAmbienteTecnologicoTecnologias, 1) ;
      }

      protected void InitializeNonKey1K57( )
      {
         A352AmbienteTecnologico_Descricao = "";
         A728AmbienteTecnologico_AreaTrabalhoCod = 0;
         A132Tecnologia_Nome = "";
         A354AmbienteTecnologicoTecnologias_Ativo = true;
         Z354AmbienteTecnologicoTecnologias_Ativo = false;
      }

      protected void InitAll1K57( )
      {
         A351AmbienteTecnologico_Codigo = 0;
         A131Tecnologia_Codigo = 0;
         InitializeNonKey1K57( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A354AmbienteTecnologicoTecnologias_Ativo = i354AmbienteTecnologicoTecnologias_Ativo;
      }

      public void VarsToRow57( SdtAmbienteTecnologicoTecnologias obj57 )
      {
         obj57.gxTpr_Mode = Gx_mode;
         obj57.gxTpr_Ambientetecnologico_descricao = A352AmbienteTecnologico_Descricao;
         obj57.gxTpr_Ambientetecnologico_areatrabalhocod = A728AmbienteTecnologico_AreaTrabalhoCod;
         obj57.gxTpr_Tecnologia_nome = A132Tecnologia_Nome;
         obj57.gxTpr_Ambientetecnologicotecnologias_ativo = A354AmbienteTecnologicoTecnologias_Ativo;
         obj57.gxTpr_Ambientetecnologico_codigo = A351AmbienteTecnologico_Codigo;
         obj57.gxTpr_Tecnologia_codigo = A131Tecnologia_Codigo;
         obj57.gxTpr_Ambientetecnologico_codigo_Z = Z351AmbienteTecnologico_Codigo;
         obj57.gxTpr_Ambientetecnologico_descricao_Z = Z352AmbienteTecnologico_Descricao;
         obj57.gxTpr_Ambientetecnologico_areatrabalhocod_Z = Z728AmbienteTecnologico_AreaTrabalhoCod;
         obj57.gxTpr_Tecnologia_codigo_Z = Z131Tecnologia_Codigo;
         obj57.gxTpr_Tecnologia_nome_Z = Z132Tecnologia_Nome;
         obj57.gxTpr_Ambientetecnologicotecnologias_ativo_Z = Z354AmbienteTecnologicoTecnologias_Ativo;
         obj57.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow57( SdtAmbienteTecnologicoTecnologias obj57 )
      {
         obj57.gxTpr_Ambientetecnologico_codigo = A351AmbienteTecnologico_Codigo;
         obj57.gxTpr_Tecnologia_codigo = A131Tecnologia_Codigo;
         return  ;
      }

      public void RowToVars57( SdtAmbienteTecnologicoTecnologias obj57 ,
                               int forceLoad )
      {
         Gx_mode = obj57.gxTpr_Mode;
         A352AmbienteTecnologico_Descricao = obj57.gxTpr_Ambientetecnologico_descricao;
         A728AmbienteTecnologico_AreaTrabalhoCod = obj57.gxTpr_Ambientetecnologico_areatrabalhocod;
         A132Tecnologia_Nome = obj57.gxTpr_Tecnologia_nome;
         A354AmbienteTecnologicoTecnologias_Ativo = obj57.gxTpr_Ambientetecnologicotecnologias_ativo;
         A351AmbienteTecnologico_Codigo = obj57.gxTpr_Ambientetecnologico_codigo;
         A131Tecnologia_Codigo = obj57.gxTpr_Tecnologia_codigo;
         Z351AmbienteTecnologico_Codigo = obj57.gxTpr_Ambientetecnologico_codigo_Z;
         Z352AmbienteTecnologico_Descricao = obj57.gxTpr_Ambientetecnologico_descricao_Z;
         Z728AmbienteTecnologico_AreaTrabalhoCod = obj57.gxTpr_Ambientetecnologico_areatrabalhocod_Z;
         Z131Tecnologia_Codigo = obj57.gxTpr_Tecnologia_codigo_Z;
         Z132Tecnologia_Nome = obj57.gxTpr_Tecnologia_nome_Z;
         Z354AmbienteTecnologicoTecnologias_Ativo = obj57.gxTpr_Ambientetecnologicotecnologias_ativo_Z;
         Gx_mode = obj57.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A351AmbienteTecnologico_Codigo = (int)getParm(obj,0);
         A131Tecnologia_Codigo = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey1K57( ) ;
         ScanKeyStart1K57( ) ;
         if ( RcdFound57 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001K11 */
            pr_default.execute(9, new Object[] {A351AmbienteTecnologico_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe ' T62'.", "ForeignKeyNotFound", 1, "AMBIENTETECNOLOGICO_CODIGO");
               AnyError = 1;
            }
            A352AmbienteTecnologico_Descricao = BC001K11_A352AmbienteTecnologico_Descricao[0];
            A728AmbienteTecnologico_AreaTrabalhoCod = BC001K11_A728AmbienteTecnologico_AreaTrabalhoCod[0];
            pr_default.close(9);
            /* Using cursor BC001K12 */
            pr_default.execute(10, new Object[] {A131Tecnologia_Codigo});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Tecnologia'.", "ForeignKeyNotFound", 1, "TECNOLOGIA_CODIGO");
               AnyError = 1;
            }
            A132Tecnologia_Nome = BC001K12_A132Tecnologia_Nome[0];
            pr_default.close(10);
         }
         else
         {
            Gx_mode = "UPD";
            Z351AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
         }
         ZM1K57( -2) ;
         OnLoadActions1K57( ) ;
         AddRow1K57( ) ;
         ScanKeyEnd1K57( ) ;
         if ( RcdFound57 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars57( bcAmbienteTecnologicoTecnologias, 0) ;
         ScanKeyStart1K57( ) ;
         if ( RcdFound57 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001K11 */
            pr_default.execute(9, new Object[] {A351AmbienteTecnologico_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe ' T62'.", "ForeignKeyNotFound", 1, "AMBIENTETECNOLOGICO_CODIGO");
               AnyError = 1;
            }
            A352AmbienteTecnologico_Descricao = BC001K11_A352AmbienteTecnologico_Descricao[0];
            A728AmbienteTecnologico_AreaTrabalhoCod = BC001K11_A728AmbienteTecnologico_AreaTrabalhoCod[0];
            pr_default.close(9);
            /* Using cursor BC001K12 */
            pr_default.execute(10, new Object[] {A131Tecnologia_Codigo});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Tecnologia'.", "ForeignKeyNotFound", 1, "TECNOLOGIA_CODIGO");
               AnyError = 1;
            }
            A132Tecnologia_Nome = BC001K12_A132Tecnologia_Nome[0];
            pr_default.close(10);
         }
         else
         {
            Gx_mode = "UPD";
            Z351AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
         }
         ZM1K57( -2) ;
         OnLoadActions1K57( ) ;
         AddRow1K57( ) ;
         ScanKeyEnd1K57( ) ;
         if ( RcdFound57 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars57( bcAmbienteTecnologicoTecnologias, 0) ;
         nKeyPressed = 1;
         GetKey1K57( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert1K57( ) ;
         }
         else
         {
            if ( RcdFound57 == 1 )
            {
               if ( ( A351AmbienteTecnologico_Codigo != Z351AmbienteTecnologico_Codigo ) || ( A131Tecnologia_Codigo != Z131Tecnologia_Codigo ) )
               {
                  A351AmbienteTecnologico_Codigo = Z351AmbienteTecnologico_Codigo;
                  A131Tecnologia_Codigo = Z131Tecnologia_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update1K57( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A351AmbienteTecnologico_Codigo != Z351AmbienteTecnologico_Codigo ) || ( A131Tecnologia_Codigo != Z131Tecnologia_Codigo ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1K57( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1K57( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow57( bcAmbienteTecnologicoTecnologias) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars57( bcAmbienteTecnologicoTecnologias, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey1K57( ) ;
         if ( RcdFound57 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A351AmbienteTecnologico_Codigo != Z351AmbienteTecnologico_Codigo ) || ( A131Tecnologia_Codigo != Z131Tecnologia_Codigo ) )
            {
               A351AmbienteTecnologico_Codigo = Z351AmbienteTecnologico_Codigo;
               A131Tecnologia_Codigo = Z131Tecnologia_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A351AmbienteTecnologico_Codigo != Z351AmbienteTecnologico_Codigo ) || ( A131Tecnologia_Codigo != Z131Tecnologia_Codigo ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         pr_default.close(10);
         context.RollbackDataStores( "AmbienteTecnologicoTecnologias_BC");
         VarsToRow57( bcAmbienteTecnologicoTecnologias) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcAmbienteTecnologicoTecnologias.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcAmbienteTecnologicoTecnologias.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcAmbienteTecnologicoTecnologias )
         {
            bcAmbienteTecnologicoTecnologias = (SdtAmbienteTecnologicoTecnologias)(sdt);
            if ( StringUtil.StrCmp(bcAmbienteTecnologicoTecnologias.gxTpr_Mode, "") == 0 )
            {
               bcAmbienteTecnologicoTecnologias.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow57( bcAmbienteTecnologicoTecnologias) ;
            }
            else
            {
               RowToVars57( bcAmbienteTecnologicoTecnologias, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcAmbienteTecnologicoTecnologias.gxTpr_Mode, "") == 0 )
            {
               bcAmbienteTecnologicoTecnologias.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars57( bcAmbienteTecnologicoTecnologias, 1) ;
         return  ;
      }

      public SdtAmbienteTecnologicoTecnologias AmbienteTecnologicoTecnologias_BC
      {
         get {
            return bcAmbienteTecnologicoTecnologias ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
         pr_default.close(10);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z352AmbienteTecnologico_Descricao = "";
         A352AmbienteTecnologico_Descricao = "";
         Z132Tecnologia_Nome = "";
         A132Tecnologia_Nome = "";
         BC001K6_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         BC001K6_A132Tecnologia_Nome = new String[] {""} ;
         BC001K6_A354AmbienteTecnologicoTecnologias_Ativo = new bool[] {false} ;
         BC001K6_A351AmbienteTecnologico_Codigo = new int[1] ;
         BC001K6_A131Tecnologia_Codigo = new int[1] ;
         BC001K6_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         BC001K4_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         BC001K4_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         BC001K5_A132Tecnologia_Nome = new String[] {""} ;
         BC001K7_A351AmbienteTecnologico_Codigo = new int[1] ;
         BC001K7_A131Tecnologia_Codigo = new int[1] ;
         BC001K3_A354AmbienteTecnologicoTecnologias_Ativo = new bool[] {false} ;
         BC001K3_A351AmbienteTecnologico_Codigo = new int[1] ;
         BC001K3_A131Tecnologia_Codigo = new int[1] ;
         sMode57 = "";
         BC001K2_A354AmbienteTecnologicoTecnologias_Ativo = new bool[] {false} ;
         BC001K2_A351AmbienteTecnologico_Codigo = new int[1] ;
         BC001K2_A131Tecnologia_Codigo = new int[1] ;
         BC001K11_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         BC001K11_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         BC001K12_A132Tecnologia_Nome = new String[] {""} ;
         BC001K13_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         BC001K13_A132Tecnologia_Nome = new String[] {""} ;
         BC001K13_A354AmbienteTecnologicoTecnologias_Ativo = new bool[] {false} ;
         BC001K13_A351AmbienteTecnologico_Codigo = new int[1] ;
         BC001K13_A131Tecnologia_Codigo = new int[1] ;
         BC001K13_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.ambientetecnologicotecnologias_bc__default(),
            new Object[][] {
                new Object[] {
               BC001K2_A354AmbienteTecnologicoTecnologias_Ativo, BC001K2_A351AmbienteTecnologico_Codigo, BC001K2_A131Tecnologia_Codigo
               }
               , new Object[] {
               BC001K3_A354AmbienteTecnologicoTecnologias_Ativo, BC001K3_A351AmbienteTecnologico_Codigo, BC001K3_A131Tecnologia_Codigo
               }
               , new Object[] {
               BC001K4_A352AmbienteTecnologico_Descricao, BC001K4_A728AmbienteTecnologico_AreaTrabalhoCod
               }
               , new Object[] {
               BC001K5_A132Tecnologia_Nome
               }
               , new Object[] {
               BC001K6_A352AmbienteTecnologico_Descricao, BC001K6_A132Tecnologia_Nome, BC001K6_A354AmbienteTecnologicoTecnologias_Ativo, BC001K6_A351AmbienteTecnologico_Codigo, BC001K6_A131Tecnologia_Codigo, BC001K6_A728AmbienteTecnologico_AreaTrabalhoCod
               }
               , new Object[] {
               BC001K7_A351AmbienteTecnologico_Codigo, BC001K7_A131Tecnologia_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC001K11_A352AmbienteTecnologico_Descricao, BC001K11_A728AmbienteTecnologico_AreaTrabalhoCod
               }
               , new Object[] {
               BC001K12_A132Tecnologia_Nome
               }
               , new Object[] {
               BC001K13_A352AmbienteTecnologico_Descricao, BC001K13_A132Tecnologia_Nome, BC001K13_A354AmbienteTecnologicoTecnologias_Ativo, BC001K13_A351AmbienteTecnologico_Codigo, BC001K13_A131Tecnologia_Codigo, BC001K13_A728AmbienteTecnologico_AreaTrabalhoCod
               }
            }
         );
         Z354AmbienteTecnologicoTecnologias_Ativo = true;
         A354AmbienteTecnologicoTecnologias_Ativo = true;
         i354AmbienteTecnologicoTecnologias_Ativo = true;
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short RcdFound57 ;
      private int trnEnded ;
      private int Z351AmbienteTecnologico_Codigo ;
      private int A351AmbienteTecnologico_Codigo ;
      private int Z131Tecnologia_Codigo ;
      private int A131Tecnologia_Codigo ;
      private int Z728AmbienteTecnologico_AreaTrabalhoCod ;
      private int A728AmbienteTecnologico_AreaTrabalhoCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z132Tecnologia_Nome ;
      private String A132Tecnologia_Nome ;
      private String sMode57 ;
      private bool Z354AmbienteTecnologicoTecnologias_Ativo ;
      private bool A354AmbienteTecnologicoTecnologias_Ativo ;
      private bool i354AmbienteTecnologicoTecnologias_Ativo ;
      private String Z352AmbienteTecnologico_Descricao ;
      private String A352AmbienteTecnologico_Descricao ;
      private SdtAmbienteTecnologicoTecnologias bcAmbienteTecnologicoTecnologias ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC001K6_A352AmbienteTecnologico_Descricao ;
      private String[] BC001K6_A132Tecnologia_Nome ;
      private bool[] BC001K6_A354AmbienteTecnologicoTecnologias_Ativo ;
      private int[] BC001K6_A351AmbienteTecnologico_Codigo ;
      private int[] BC001K6_A131Tecnologia_Codigo ;
      private int[] BC001K6_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private String[] BC001K4_A352AmbienteTecnologico_Descricao ;
      private int[] BC001K4_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private String[] BC001K5_A132Tecnologia_Nome ;
      private int[] BC001K7_A351AmbienteTecnologico_Codigo ;
      private int[] BC001K7_A131Tecnologia_Codigo ;
      private bool[] BC001K3_A354AmbienteTecnologicoTecnologias_Ativo ;
      private int[] BC001K3_A351AmbienteTecnologico_Codigo ;
      private int[] BC001K3_A131Tecnologia_Codigo ;
      private bool[] BC001K2_A354AmbienteTecnologicoTecnologias_Ativo ;
      private int[] BC001K2_A351AmbienteTecnologico_Codigo ;
      private int[] BC001K2_A131Tecnologia_Codigo ;
      private String[] BC001K11_A352AmbienteTecnologico_Descricao ;
      private int[] BC001K11_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private String[] BC001K12_A132Tecnologia_Nome ;
      private String[] BC001K13_A352AmbienteTecnologico_Descricao ;
      private String[] BC001K13_A132Tecnologia_Nome ;
      private bool[] BC001K13_A354AmbienteTecnologicoTecnologias_Ativo ;
      private int[] BC001K13_A351AmbienteTecnologico_Codigo ;
      private int[] BC001K13_A131Tecnologia_Codigo ;
      private int[] BC001K13_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class ambientetecnologicotecnologias_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC001K6 ;
          prmBC001K6 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001K4 ;
          prmBC001K4 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001K5 ;
          prmBC001K5 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001K7 ;
          prmBC001K7 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001K3 ;
          prmBC001K3 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001K2 ;
          prmBC001K2 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001K8 ;
          prmBC001K8 = new Object[] {
          new Object[] {"@AmbienteTecnologicoTecnologias_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001K9 ;
          prmBC001K9 = new Object[] {
          new Object[] {"@AmbienteTecnologicoTecnologias_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001K10 ;
          prmBC001K10 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001K13 ;
          prmBC001K13 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001K11 ;
          prmBC001K11 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001K12 ;
          prmBC001K12 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC001K2", "SELECT [AmbienteTecnologicoTecnologias_Ativo], [AmbienteTecnologico_Codigo], [Tecnologia_Codigo] FROM [AmbienteTecnologicoTecnologias] WITH (UPDLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo AND [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001K2,1,0,true,false )
             ,new CursorDef("BC001K3", "SELECT [AmbienteTecnologicoTecnologias_Ativo], [AmbienteTecnologico_Codigo], [Tecnologia_Codigo] FROM [AmbienteTecnologicoTecnologias] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo AND [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001K3,1,0,true,false )
             ,new CursorDef("BC001K4", "SELECT [AmbienteTecnologico_Descricao], [AmbienteTecnologico_AreaTrabalhoCod] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001K4,1,0,true,false )
             ,new CursorDef("BC001K5", "SELECT [Tecnologia_Nome] FROM [Tecnologia] WITH (NOLOCK) WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001K5,1,0,true,false )
             ,new CursorDef("BC001K6", "SELECT T2.[AmbienteTecnologico_Descricao], T3.[Tecnologia_Nome], TM1.[AmbienteTecnologicoTecnologias_Ativo], TM1.[AmbienteTecnologico_Codigo], TM1.[Tecnologia_Codigo], T2.[AmbienteTecnologico_AreaTrabalhoCod] FROM (([AmbienteTecnologicoTecnologias] TM1 WITH (NOLOCK) INNER JOIN [AmbienteTecnologico] T2 WITH (NOLOCK) ON T2.[AmbienteTecnologico_Codigo] = TM1.[AmbienteTecnologico_Codigo]) INNER JOIN [Tecnologia] T3 WITH (NOLOCK) ON T3.[Tecnologia_Codigo] = TM1.[Tecnologia_Codigo]) WHERE TM1.[AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo and TM1.[Tecnologia_Codigo] = @Tecnologia_Codigo ORDER BY TM1.[AmbienteTecnologico_Codigo], TM1.[Tecnologia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001K6,100,0,true,false )
             ,new CursorDef("BC001K7", "SELECT [AmbienteTecnologico_Codigo], [Tecnologia_Codigo] FROM [AmbienteTecnologicoTecnologias] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo AND [Tecnologia_Codigo] = @Tecnologia_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001K7,1,0,true,false )
             ,new CursorDef("BC001K8", "INSERT INTO [AmbienteTecnologicoTecnologias]([AmbienteTecnologicoTecnologias_Ativo], [AmbienteTecnologico_Codigo], [Tecnologia_Codigo]) VALUES(@AmbienteTecnologicoTecnologias_Ativo, @AmbienteTecnologico_Codigo, @Tecnologia_Codigo)", GxErrorMask.GX_NOMASK,prmBC001K8)
             ,new CursorDef("BC001K9", "UPDATE [AmbienteTecnologicoTecnologias] SET [AmbienteTecnologicoTecnologias_Ativo]=@AmbienteTecnologicoTecnologias_Ativo  WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo AND [Tecnologia_Codigo] = @Tecnologia_Codigo", GxErrorMask.GX_NOMASK,prmBC001K9)
             ,new CursorDef("BC001K10", "DELETE FROM [AmbienteTecnologicoTecnologias]  WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo AND [Tecnologia_Codigo] = @Tecnologia_Codigo", GxErrorMask.GX_NOMASK,prmBC001K10)
             ,new CursorDef("BC001K11", "SELECT [AmbienteTecnologico_Descricao], [AmbienteTecnologico_AreaTrabalhoCod] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001K11,1,0,true,false )
             ,new CursorDef("BC001K12", "SELECT [Tecnologia_Nome] FROM [Tecnologia] WITH (NOLOCK) WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001K12,1,0,true,false )
             ,new CursorDef("BC001K13", "SELECT T2.[AmbienteTecnologico_Descricao], T3.[Tecnologia_Nome], TM1.[AmbienteTecnologicoTecnologias_Ativo], TM1.[AmbienteTecnologico_Codigo], TM1.[Tecnologia_Codigo], T2.[AmbienteTecnologico_AreaTrabalhoCod] FROM (([AmbienteTecnologicoTecnologias] TM1 WITH (NOLOCK) INNER JOIN [AmbienteTecnologico] T2 WITH (NOLOCK) ON T2.[AmbienteTecnologico_Codigo] = TM1.[AmbienteTecnologico_Codigo]) INNER JOIN [Tecnologia] T3 WITH (NOLOCK) ON T3.[Tecnologia_Codigo] = TM1.[Tecnologia_Codigo]) WHERE TM1.[AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo and TM1.[Tecnologia_Codigo] = @Tecnologia_Codigo ORDER BY TM1.[AmbienteTecnologico_Codigo], TM1.[Tecnologia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001K13,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 7 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
