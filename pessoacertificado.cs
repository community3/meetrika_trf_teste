/*
               File: PessoaCertificado
        Description: Certificados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:56:5.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class pessoacertificado : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A2011PessoaCertificado_PesCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2011PessoaCertificado_PesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2011PessoaCertificado_PesCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A2011PessoaCertificado_PesCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7PessoaCertificado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7PessoaCertificado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7PessoaCertificado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPESSOACERTIFICADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7PessoaCertificado_Codigo), "ZZZZZ9")));
               AV13PessoaCertificado_PesCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13PessoaCertificado_PesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13PessoaCertificado_PesCod), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Certificados", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtPessoaCertificado_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public pessoacertificado( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public pessoacertificado( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_PessoaCertificado_Codigo ,
                           ref int aP2_PessoaCertificado_PesCod )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7PessoaCertificado_Codigo = aP1_PessoaCertificado_Codigo;
         this.AV13PessoaCertificado_PesCod = aP2_PessoaCertificado_PesCod;
         executePrivate();
         aP2_PessoaCertificado_PesCod=this.AV13PessoaCertificado_PesCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_50222( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_50222e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoaCertificado_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2010PessoaCertificado_Codigo), 6, 0, ",", "")), ((edtPessoaCertificado_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2010PessoaCertificado_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A2010PessoaCertificado_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoaCertificado_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtPessoaCertificado_Codigo_Visible, edtPessoaCertificado_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_PessoaCertificado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPessoaCertificado_PesCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2011PessoaCertificado_PesCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A2011PessoaCertificado_PesCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoaCertificado_PesCod_Jsonclick, 0, "Attribute", "", "", "", edtPessoaCertificado_PesCod_Visible, edtPessoaCertificado_PesCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_PessoaCertificado.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_50222( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_50222( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_50222e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_31_50222( true) ;
         }
         return  ;
      }

      protected void wb_table3_31_50222e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_50222e( true) ;
         }
         else
         {
            wb_table1_2_50222e( false) ;
         }
      }

      protected void wb_table3_31_50222( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_PessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_PessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_PessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_31_50222e( true) ;
         }
         else
         {
            wb_table3_31_50222e( false) ;
         }
      }

      protected void wb_table2_5_50222( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_50222( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_50222e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_50222e( true) ;
         }
         else
         {
            wb_table2_5_50222e( false) ;
         }
      }

      protected void wb_table4_13_50222( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoacertificado_nome_Internalname, "Nome", "", "", lblTextblockpessoacertificado_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPessoaCertificado_Nome_Internalname, StringUtil.RTrim( A2013PessoaCertificado_Nome), StringUtil.RTrim( context.localUtil.Format( A2013PessoaCertificado_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoaCertificado_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtPessoaCertificado_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_PessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoacertificado_emissao_Internalname, "Emiss�o", "", "", lblTextblockpessoacertificado_emissao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtPessoaCertificado_Emissao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtPessoaCertificado_Emissao_Internalname, context.localUtil.Format(A2012PessoaCertificado_Emissao, "99/99/99"), context.localUtil.Format( A2012PessoaCertificado_Emissao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoaCertificado_Emissao_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtPessoaCertificado_Emissao_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtPessoaCertificado_Emissao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtPessoaCertificado_Emissao_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoacertificado_validade_Internalname, "Validade", "", "", lblTextblockpessoacertificado_validade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtPessoaCertificado_Validade_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtPessoaCertificado_Validade_Internalname, context.localUtil.Format(A2014PessoaCertificado_Validade, "99/99/99"), context.localUtil.Format( A2014PessoaCertificado_Validade, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoaCertificado_Validade_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtPessoaCertificado_Validade_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtPessoaCertificado_Validade_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtPessoaCertificado_Validade_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_50222e( true) ;
         }
         else
         {
            wb_table4_13_50222e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11502 */
         E11502 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A2013PessoaCertificado_Nome = StringUtil.Upper( cgiGet( edtPessoaCertificado_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2013PessoaCertificado_Nome", A2013PessoaCertificado_Nome);
               if ( context.localUtil.VCDate( cgiGet( edtPessoaCertificado_Emissao_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Emiss�o"}), 1, "PESSOACERTIFICADO_EMISSAO");
                  AnyError = 1;
                  GX_FocusControl = edtPessoaCertificado_Emissao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2012PessoaCertificado_Emissao = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2012PessoaCertificado_Emissao", context.localUtil.Format(A2012PessoaCertificado_Emissao, "99/99/99"));
               }
               else
               {
                  A2012PessoaCertificado_Emissao = context.localUtil.CToD( cgiGet( edtPessoaCertificado_Emissao_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2012PessoaCertificado_Emissao", context.localUtil.Format(A2012PessoaCertificado_Emissao, "99/99/99"));
               }
               if ( context.localUtil.VCDate( cgiGet( edtPessoaCertificado_Validade_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Validade"}), 1, "PESSOACERTIFICADO_VALIDADE");
                  AnyError = 1;
                  GX_FocusControl = edtPessoaCertificado_Validade_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2014PessoaCertificado_Validade = DateTime.MinValue;
                  n2014PessoaCertificado_Validade = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2014PessoaCertificado_Validade", context.localUtil.Format(A2014PessoaCertificado_Validade, "99/99/99"));
               }
               else
               {
                  A2014PessoaCertificado_Validade = context.localUtil.CToD( cgiGet( edtPessoaCertificado_Validade_Internalname), 2);
                  n2014PessoaCertificado_Validade = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2014PessoaCertificado_Validade", context.localUtil.Format(A2014PessoaCertificado_Validade, "99/99/99"));
               }
               n2014PessoaCertificado_Validade = ((DateTime.MinValue==A2014PessoaCertificado_Validade) ? true : false);
               A2010PessoaCertificado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPessoaCertificado_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2010PessoaCertificado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2010PessoaCertificado_Codigo), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtPessoaCertificado_PesCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtPessoaCertificado_PesCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PESSOACERTIFICADO_PESCOD");
                  AnyError = 1;
                  GX_FocusControl = edtPessoaCertificado_PesCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2011PessoaCertificado_PesCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2011PessoaCertificado_PesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2011PessoaCertificado_PesCod), 6, 0)));
               }
               else
               {
                  A2011PessoaCertificado_PesCod = (int)(context.localUtil.CToN( cgiGet( edtPessoaCertificado_PesCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2011PessoaCertificado_PesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2011PessoaCertificado_PesCod), 6, 0)));
               }
               /* Read saved values. */
               Z2010PessoaCertificado_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z2010PessoaCertificado_Codigo"), ",", "."));
               Z2012PessoaCertificado_Emissao = context.localUtil.CToD( cgiGet( "Z2012PessoaCertificado_Emissao"), 0);
               Z2013PessoaCertificado_Nome = cgiGet( "Z2013PessoaCertificado_Nome");
               Z2014PessoaCertificado_Validade = context.localUtil.CToD( cgiGet( "Z2014PessoaCertificado_Validade"), 0);
               n2014PessoaCertificado_Validade = ((DateTime.MinValue==A2014PessoaCertificado_Validade) ? true : false);
               Z2011PessoaCertificado_PesCod = (int)(context.localUtil.CToN( cgiGet( "Z2011PessoaCertificado_PesCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N2011PessoaCertificado_PesCod = (int)(context.localUtil.CToN( cgiGet( "N2011PessoaCertificado_PesCod"), ",", "."));
               AV7PessoaCertificado_Codigo = (int)(context.localUtil.CToN( cgiGet( "vPESSOACERTIFICADO_CODIGO"), ",", "."));
               AV11Insert_PessoaCertificado_PesCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PESSOACERTIFICADO_PESCOD"), ",", "."));
               AV13PessoaCertificado_PesCod = (int)(context.localUtil.CToN( cgiGet( "vPESSOACERTIFICADO_PESCOD"), ",", "."));
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "PessoaCertificado";
               A2010PessoaCertificado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPessoaCertificado_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2010PessoaCertificado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2010PessoaCertificado_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2010PessoaCertificado_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A2010PessoaCertificado_Codigo != Z2010PessoaCertificado_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("pessoacertificado:[SecurityCheckFailed value for]"+"PessoaCertificado_Codigo:"+context.localUtil.Format( (decimal)(A2010PessoaCertificado_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("pessoacertificado:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A2010PessoaCertificado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2010PessoaCertificado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2010PessoaCertificado_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode222 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode222;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound222 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_500( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "PESSOACERTIFICADO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtPessoaCertificado_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11502 */
                           E11502 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12502 */
                           E12502 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12502 */
            E12502 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll50222( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes50222( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_500( )
      {
         BeforeValidate50222( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls50222( ) ;
            }
            else
            {
               CheckExtendedTable50222( ) ;
               CloseExtendedTableCursors50222( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption500( )
      {
      }

      protected void E11502( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "PessoaCertificado_PesCod") == 0 )
               {
                  AV11Insert_PessoaCertificado_PesCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_PessoaCertificado_PesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_PessoaCertificado_PesCod), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
         edtPessoaCertificado_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoaCertificado_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoaCertificado_Codigo_Visible), 5, 0)));
         edtPessoaCertificado_PesCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoaCertificado_PesCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoaCertificado_PesCod_Visible), 5, 0)));
      }

      protected void E12502( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwpessoacertificado.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)AV13PessoaCertificado_PesCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM50222( short GX_JID )
      {
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2012PessoaCertificado_Emissao = T00503_A2012PessoaCertificado_Emissao[0];
               Z2013PessoaCertificado_Nome = T00503_A2013PessoaCertificado_Nome[0];
               Z2014PessoaCertificado_Validade = T00503_A2014PessoaCertificado_Validade[0];
               Z2011PessoaCertificado_PesCod = T00503_A2011PessoaCertificado_PesCod[0];
            }
            else
            {
               Z2012PessoaCertificado_Emissao = A2012PessoaCertificado_Emissao;
               Z2013PessoaCertificado_Nome = A2013PessoaCertificado_Nome;
               Z2014PessoaCertificado_Validade = A2014PessoaCertificado_Validade;
               Z2011PessoaCertificado_PesCod = A2011PessoaCertificado_PesCod;
            }
         }
         if ( GX_JID == -11 )
         {
            Z2010PessoaCertificado_Codigo = A2010PessoaCertificado_Codigo;
            Z2012PessoaCertificado_Emissao = A2012PessoaCertificado_Emissao;
            Z2013PessoaCertificado_Nome = A2013PessoaCertificado_Nome;
            Z2014PessoaCertificado_Validade = A2014PessoaCertificado_Validade;
            Z2011PessoaCertificado_PesCod = A2011PessoaCertificado_PesCod;
         }
      }

      protected void standaloneNotModal( )
      {
         edtPessoaCertificado_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoaCertificado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoaCertificado_Codigo_Enabled), 5, 0)));
         AV14Pgmname = "PessoaCertificado";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         edtPessoaCertificado_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoaCertificado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoaCertificado_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7PessoaCertificado_Codigo) )
         {
            A2010PessoaCertificado_Codigo = AV7PessoaCertificado_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2010PessoaCertificado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2010PessoaCertificado_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_PessoaCertificado_PesCod) )
         {
            edtPessoaCertificado_PesCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoaCertificado_PesCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoaCertificado_PesCod_Enabled), 5, 0)));
         }
         else
         {
            edtPessoaCertificado_PesCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoaCertificado_PesCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoaCertificado_PesCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_PessoaCertificado_PesCod) )
         {
            A2011PessoaCertificado_PesCod = AV11Insert_PessoaCertificado_PesCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2011PessoaCertificado_PesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2011PessoaCertificado_PesCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               A2011PessoaCertificado_PesCod = AV13PessoaCertificado_PesCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2011PessoaCertificado_PesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2011PessoaCertificado_PesCod), 6, 0)));
            }
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load50222( )
      {
         /* Using cursor T00505 */
         pr_default.execute(3, new Object[] {A2010PessoaCertificado_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound222 = 1;
            A2012PessoaCertificado_Emissao = T00505_A2012PessoaCertificado_Emissao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2012PessoaCertificado_Emissao", context.localUtil.Format(A2012PessoaCertificado_Emissao, "99/99/99"));
            A2013PessoaCertificado_Nome = T00505_A2013PessoaCertificado_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2013PessoaCertificado_Nome", A2013PessoaCertificado_Nome);
            A2014PessoaCertificado_Validade = T00505_A2014PessoaCertificado_Validade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2014PessoaCertificado_Validade", context.localUtil.Format(A2014PessoaCertificado_Validade, "99/99/99"));
            n2014PessoaCertificado_Validade = T00505_n2014PessoaCertificado_Validade[0];
            A2011PessoaCertificado_PesCod = T00505_A2011PessoaCertificado_PesCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2011PessoaCertificado_PesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2011PessoaCertificado_PesCod), 6, 0)));
            ZM50222( -11) ;
         }
         pr_default.close(3);
         OnLoadActions50222( ) ;
      }

      protected void OnLoadActions50222( )
      {
      }

      protected void CheckExtendedTable50222( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T00504 */
         pr_default.execute(2, new Object[] {A2011PessoaCertificado_PesCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Pessoa Certificado_Pessoa'.", "ForeignKeyNotFound", 1, "PESSOACERTIFICADO_PESCOD");
            AnyError = 1;
            GX_FocusControl = edtPessoaCertificado_PesCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         if ( ! ( (DateTime.MinValue==A2012PessoaCertificado_Emissao) || ( A2012PessoaCertificado_Emissao >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Emiss�o fora do intervalo", "OutOfRange", 1, "PESSOACERTIFICADO_EMISSAO");
            AnyError = 1;
            GX_FocusControl = edtPessoaCertificado_Emissao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( A2014PessoaCertificado_Validade < A2012PessoaCertificado_Emissao )
         {
            GX_msglist.addItem("A data de Validade deve ser maior que a data de Emiss�o!", 1, "PESSOACERTIFICADO_EMISSAO");
            AnyError = 1;
            GX_FocusControl = edtPessoaCertificado_Emissao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A2014PessoaCertificado_Validade) || ( A2014PessoaCertificado_Validade >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Validade fora do intervalo", "OutOfRange", 1, "PESSOACERTIFICADO_VALIDADE");
            AnyError = 1;
            GX_FocusControl = edtPessoaCertificado_Validade_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors50222( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_12( int A2011PessoaCertificado_PesCod )
      {
         /* Using cursor T00506 */
         pr_default.execute(4, new Object[] {A2011PessoaCertificado_PesCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Pessoa Certificado_Pessoa'.", "ForeignKeyNotFound", 1, "PESSOACERTIFICADO_PESCOD");
            AnyError = 1;
            GX_FocusControl = edtPessoaCertificado_PesCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey50222( )
      {
         /* Using cursor T00507 */
         pr_default.execute(5, new Object[] {A2010PessoaCertificado_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound222 = 1;
         }
         else
         {
            RcdFound222 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00503 */
         pr_default.execute(1, new Object[] {A2010PessoaCertificado_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM50222( 11) ;
            RcdFound222 = 1;
            A2010PessoaCertificado_Codigo = T00503_A2010PessoaCertificado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2010PessoaCertificado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2010PessoaCertificado_Codigo), 6, 0)));
            A2012PessoaCertificado_Emissao = T00503_A2012PessoaCertificado_Emissao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2012PessoaCertificado_Emissao", context.localUtil.Format(A2012PessoaCertificado_Emissao, "99/99/99"));
            A2013PessoaCertificado_Nome = T00503_A2013PessoaCertificado_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2013PessoaCertificado_Nome", A2013PessoaCertificado_Nome);
            A2014PessoaCertificado_Validade = T00503_A2014PessoaCertificado_Validade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2014PessoaCertificado_Validade", context.localUtil.Format(A2014PessoaCertificado_Validade, "99/99/99"));
            n2014PessoaCertificado_Validade = T00503_n2014PessoaCertificado_Validade[0];
            A2011PessoaCertificado_PesCod = T00503_A2011PessoaCertificado_PesCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2011PessoaCertificado_PesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2011PessoaCertificado_PesCod), 6, 0)));
            Z2010PessoaCertificado_Codigo = A2010PessoaCertificado_Codigo;
            sMode222 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load50222( ) ;
            if ( AnyError == 1 )
            {
               RcdFound222 = 0;
               InitializeNonKey50222( ) ;
            }
            Gx_mode = sMode222;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound222 = 0;
            InitializeNonKey50222( ) ;
            sMode222 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode222;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey50222( ) ;
         if ( RcdFound222 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound222 = 0;
         /* Using cursor T00508 */
         pr_default.execute(6, new Object[] {A2010PessoaCertificado_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T00508_A2010PessoaCertificado_Codigo[0] < A2010PessoaCertificado_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T00508_A2010PessoaCertificado_Codigo[0] > A2010PessoaCertificado_Codigo ) ) )
            {
               A2010PessoaCertificado_Codigo = T00508_A2010PessoaCertificado_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2010PessoaCertificado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2010PessoaCertificado_Codigo), 6, 0)));
               RcdFound222 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound222 = 0;
         /* Using cursor T00509 */
         pr_default.execute(7, new Object[] {A2010PessoaCertificado_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T00509_A2010PessoaCertificado_Codigo[0] > A2010PessoaCertificado_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T00509_A2010PessoaCertificado_Codigo[0] < A2010PessoaCertificado_Codigo ) ) )
            {
               A2010PessoaCertificado_Codigo = T00509_A2010PessoaCertificado_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2010PessoaCertificado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2010PessoaCertificado_Codigo), 6, 0)));
               RcdFound222 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey50222( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtPessoaCertificado_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert50222( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound222 == 1 )
            {
               if ( A2010PessoaCertificado_Codigo != Z2010PessoaCertificado_Codigo )
               {
                  A2010PessoaCertificado_Codigo = Z2010PessoaCertificado_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2010PessoaCertificado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2010PessoaCertificado_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "PESSOACERTIFICADO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtPessoaCertificado_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtPessoaCertificado_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update50222( ) ;
                  GX_FocusControl = edtPessoaCertificado_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A2010PessoaCertificado_Codigo != Z2010PessoaCertificado_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtPessoaCertificado_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert50222( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "PESSOACERTIFICADO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtPessoaCertificado_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtPessoaCertificado_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert50222( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A2010PessoaCertificado_Codigo != Z2010PessoaCertificado_Codigo )
         {
            A2010PessoaCertificado_Codigo = Z2010PessoaCertificado_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2010PessoaCertificado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2010PessoaCertificado_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "PESSOACERTIFICADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtPessoaCertificado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtPessoaCertificado_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency50222( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00502 */
            pr_default.execute(0, new Object[] {A2010PessoaCertificado_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"PessoaCertificado"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z2012PessoaCertificado_Emissao != T00502_A2012PessoaCertificado_Emissao[0] ) || ( StringUtil.StrCmp(Z2013PessoaCertificado_Nome, T00502_A2013PessoaCertificado_Nome[0]) != 0 ) || ( Z2014PessoaCertificado_Validade != T00502_A2014PessoaCertificado_Validade[0] ) || ( Z2011PessoaCertificado_PesCod != T00502_A2011PessoaCertificado_PesCod[0] ) )
            {
               if ( Z2012PessoaCertificado_Emissao != T00502_A2012PessoaCertificado_Emissao[0] )
               {
                  GXUtil.WriteLog("pessoacertificado:[seudo value changed for attri]"+"PessoaCertificado_Emissao");
                  GXUtil.WriteLogRaw("Old: ",Z2012PessoaCertificado_Emissao);
                  GXUtil.WriteLogRaw("Current: ",T00502_A2012PessoaCertificado_Emissao[0]);
               }
               if ( StringUtil.StrCmp(Z2013PessoaCertificado_Nome, T00502_A2013PessoaCertificado_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("pessoacertificado:[seudo value changed for attri]"+"PessoaCertificado_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z2013PessoaCertificado_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00502_A2013PessoaCertificado_Nome[0]);
               }
               if ( Z2014PessoaCertificado_Validade != T00502_A2014PessoaCertificado_Validade[0] )
               {
                  GXUtil.WriteLog("pessoacertificado:[seudo value changed for attri]"+"PessoaCertificado_Validade");
                  GXUtil.WriteLogRaw("Old: ",Z2014PessoaCertificado_Validade);
                  GXUtil.WriteLogRaw("Current: ",T00502_A2014PessoaCertificado_Validade[0]);
               }
               if ( Z2011PessoaCertificado_PesCod != T00502_A2011PessoaCertificado_PesCod[0] )
               {
                  GXUtil.WriteLog("pessoacertificado:[seudo value changed for attri]"+"PessoaCertificado_PesCod");
                  GXUtil.WriteLogRaw("Old: ",Z2011PessoaCertificado_PesCod);
                  GXUtil.WriteLogRaw("Current: ",T00502_A2011PessoaCertificado_PesCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"PessoaCertificado"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert50222( )
      {
         BeforeValidate50222( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable50222( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM50222( 0) ;
            CheckOptimisticConcurrency50222( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm50222( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert50222( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005010 */
                     pr_default.execute(8, new Object[] {A2012PessoaCertificado_Emissao, A2013PessoaCertificado_Nome, n2014PessoaCertificado_Validade, A2014PessoaCertificado_Validade, A2011PessoaCertificado_PesCod});
                     A2010PessoaCertificado_Codigo = T005010_A2010PessoaCertificado_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2010PessoaCertificado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2010PessoaCertificado_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("PessoaCertificado") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption500( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load50222( ) ;
            }
            EndLevel50222( ) ;
         }
         CloseExtendedTableCursors50222( ) ;
      }

      protected void Update50222( )
      {
         BeforeValidate50222( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable50222( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency50222( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm50222( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate50222( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005011 */
                     pr_default.execute(9, new Object[] {A2012PessoaCertificado_Emissao, A2013PessoaCertificado_Nome, n2014PessoaCertificado_Validade, A2014PessoaCertificado_Validade, A2011PessoaCertificado_PesCod, A2010PessoaCertificado_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("PessoaCertificado") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"PessoaCertificado"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate50222( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel50222( ) ;
         }
         CloseExtendedTableCursors50222( ) ;
      }

      protected void DeferredUpdate50222( )
      {
      }

      protected void delete( )
      {
         BeforeValidate50222( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency50222( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls50222( ) ;
            AfterConfirm50222( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete50222( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T005012 */
                  pr_default.execute(10, new Object[] {A2010PessoaCertificado_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("PessoaCertificado") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode222 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel50222( ) ;
         Gx_mode = sMode222;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls50222( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel50222( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete50222( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "PessoaCertificado");
            if ( AnyError == 0 )
            {
               ConfirmValues500( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "PessoaCertificado");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart50222( )
      {
         /* Scan By routine */
         /* Using cursor T005013 */
         pr_default.execute(11);
         RcdFound222 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound222 = 1;
            A2010PessoaCertificado_Codigo = T005013_A2010PessoaCertificado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2010PessoaCertificado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2010PessoaCertificado_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext50222( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound222 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound222 = 1;
            A2010PessoaCertificado_Codigo = T005013_A2010PessoaCertificado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2010PessoaCertificado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2010PessoaCertificado_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd50222( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm50222( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert50222( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate50222( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete50222( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete50222( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate50222( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes50222( )
      {
         edtPessoaCertificado_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoaCertificado_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoaCertificado_Nome_Enabled), 5, 0)));
         edtPessoaCertificado_Emissao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoaCertificado_Emissao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoaCertificado_Emissao_Enabled), 5, 0)));
         edtPessoaCertificado_Validade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoaCertificado_Validade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoaCertificado_Validade_Enabled), 5, 0)));
         edtPessoaCertificado_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoaCertificado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoaCertificado_Codigo_Enabled), 5, 0)));
         edtPessoaCertificado_PesCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoaCertificado_PesCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoaCertificado_PesCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues500( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205181256620");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("pessoacertificado.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7PessoaCertificado_Codigo) + "," + UrlEncode("" +AV13PessoaCertificado_PesCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z2010PessoaCertificado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2010PessoaCertificado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2012PessoaCertificado_Emissao", context.localUtil.DToC( Z2012PessoaCertificado_Emissao, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z2013PessoaCertificado_Nome", StringUtil.RTrim( Z2013PessoaCertificado_Nome));
         GxWebStd.gx_hidden_field( context, "Z2014PessoaCertificado_Validade", context.localUtil.DToC( Z2014PessoaCertificado_Validade, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z2011PessoaCertificado_PesCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2011PessoaCertificado_PesCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N2011PessoaCertificado_PesCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2011PessoaCertificado_PesCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vPESSOACERTIFICADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7PessoaCertificado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PESSOACERTIFICADO_PESCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_PessoaCertificado_PesCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPESSOACERTIFICADO_PESCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13PessoaCertificado_PesCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPESSOACERTIFICADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7PessoaCertificado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "PessoaCertificado";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2010PessoaCertificado_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("pessoacertificado:[SendSecurityCheck value for]"+"PessoaCertificado_Codigo:"+context.localUtil.Format( (decimal)(A2010PessoaCertificado_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("pessoacertificado:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("pessoacertificado.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7PessoaCertificado_Codigo) + "," + UrlEncode("" +AV13PessoaCertificado_PesCod) ;
      }

      public override String GetPgmname( )
      {
         return "PessoaCertificado" ;
      }

      public override String GetPgmdesc( )
      {
         return "Certificados" ;
      }

      protected void InitializeNonKey50222( )
      {
         A2011PessoaCertificado_PesCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2011PessoaCertificado_PesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2011PessoaCertificado_PesCod), 6, 0)));
         A2012PessoaCertificado_Emissao = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2012PessoaCertificado_Emissao", context.localUtil.Format(A2012PessoaCertificado_Emissao, "99/99/99"));
         A2013PessoaCertificado_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2013PessoaCertificado_Nome", A2013PessoaCertificado_Nome);
         A2014PessoaCertificado_Validade = DateTime.MinValue;
         n2014PessoaCertificado_Validade = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2014PessoaCertificado_Validade", context.localUtil.Format(A2014PessoaCertificado_Validade, "99/99/99"));
         n2014PessoaCertificado_Validade = ((DateTime.MinValue==A2014PessoaCertificado_Validade) ? true : false);
         Z2012PessoaCertificado_Emissao = DateTime.MinValue;
         Z2013PessoaCertificado_Nome = "";
         Z2014PessoaCertificado_Validade = DateTime.MinValue;
         Z2011PessoaCertificado_PesCod = 0;
      }

      protected void InitAll50222( )
      {
         A2010PessoaCertificado_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2010PessoaCertificado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2010PessoaCertificado_Codigo), 6, 0)));
         InitializeNonKey50222( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2011PessoaCertificado_PesCod = i2011PessoaCertificado_PesCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2011PessoaCertificado_PesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2011PessoaCertificado_PesCod), 6, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205181256637");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("pessoacertificado.js", "?20205181256637");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockpessoacertificado_nome_Internalname = "TEXTBLOCKPESSOACERTIFICADO_NOME";
         edtPessoaCertificado_Nome_Internalname = "PESSOACERTIFICADO_NOME";
         lblTextblockpessoacertificado_emissao_Internalname = "TEXTBLOCKPESSOACERTIFICADO_EMISSAO";
         edtPessoaCertificado_Emissao_Internalname = "PESSOACERTIFICADO_EMISSAO";
         lblTextblockpessoacertificado_validade_Internalname = "TEXTBLOCKPESSOACERTIFICADO_VALIDADE";
         edtPessoaCertificado_Validade_Internalname = "PESSOACERTIFICADO_VALIDADE";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtPessoaCertificado_Codigo_Internalname = "PESSOACERTIFICADO_CODIGO";
         edtPessoaCertificado_PesCod_Internalname = "PESSOACERTIFICADO_PESCOD";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Certificado";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Certificados";
         edtPessoaCertificado_Validade_Jsonclick = "";
         edtPessoaCertificado_Validade_Enabled = 1;
         edtPessoaCertificado_Emissao_Jsonclick = "";
         edtPessoaCertificado_Emissao_Enabled = 1;
         edtPessoaCertificado_Nome_Jsonclick = "";
         edtPessoaCertificado_Nome_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtPessoaCertificado_PesCod_Jsonclick = "";
         edtPessoaCertificado_PesCod_Enabled = 1;
         edtPessoaCertificado_PesCod_Visible = 1;
         edtPessoaCertificado_Codigo_Jsonclick = "";
         edtPessoaCertificado_Codigo_Enabled = 0;
         edtPessoaCertificado_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Pessoacertificado_pescod( int GX_Parm1 )
      {
         A2011PessoaCertificado_PesCod = GX_Parm1;
         /* Using cursor T005014 */
         pr_default.execute(12, new Object[] {A2011PessoaCertificado_PesCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Pessoa Certificado_Pessoa'.", "ForeignKeyNotFound", 1, "PESSOACERTIFICADO_PESCOD");
            AnyError = 1;
            GX_FocusControl = edtPessoaCertificado_PesCod_Internalname;
         }
         pr_default.close(12);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7PessoaCertificado_Codigo',fld:'vPESSOACERTIFICADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV13PessoaCertificado_PesCod',fld:'vPESSOACERTIFICADO_PESCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12502',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z2012PessoaCertificado_Emissao = DateTime.MinValue;
         Z2013PessoaCertificado_Nome = "";
         Z2014PessoaCertificado_Validade = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockpessoacertificado_nome_Jsonclick = "";
         A2013PessoaCertificado_Nome = "";
         lblTextblockpessoacertificado_emissao_Jsonclick = "";
         A2012PessoaCertificado_Emissao = DateTime.MinValue;
         lblTextblockpessoacertificado_validade_Jsonclick = "";
         A2014PessoaCertificado_Validade = DateTime.MinValue;
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode222 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         T00505_A2010PessoaCertificado_Codigo = new int[1] ;
         T00505_A2012PessoaCertificado_Emissao = new DateTime[] {DateTime.MinValue} ;
         T00505_A2013PessoaCertificado_Nome = new String[] {""} ;
         T00505_A2014PessoaCertificado_Validade = new DateTime[] {DateTime.MinValue} ;
         T00505_n2014PessoaCertificado_Validade = new bool[] {false} ;
         T00505_A2011PessoaCertificado_PesCod = new int[1] ;
         T00504_A2011PessoaCertificado_PesCod = new int[1] ;
         T00506_A2011PessoaCertificado_PesCod = new int[1] ;
         T00507_A2010PessoaCertificado_Codigo = new int[1] ;
         T00503_A2010PessoaCertificado_Codigo = new int[1] ;
         T00503_A2012PessoaCertificado_Emissao = new DateTime[] {DateTime.MinValue} ;
         T00503_A2013PessoaCertificado_Nome = new String[] {""} ;
         T00503_A2014PessoaCertificado_Validade = new DateTime[] {DateTime.MinValue} ;
         T00503_n2014PessoaCertificado_Validade = new bool[] {false} ;
         T00503_A2011PessoaCertificado_PesCod = new int[1] ;
         T00508_A2010PessoaCertificado_Codigo = new int[1] ;
         T00509_A2010PessoaCertificado_Codigo = new int[1] ;
         T00502_A2010PessoaCertificado_Codigo = new int[1] ;
         T00502_A2012PessoaCertificado_Emissao = new DateTime[] {DateTime.MinValue} ;
         T00502_A2013PessoaCertificado_Nome = new String[] {""} ;
         T00502_A2014PessoaCertificado_Validade = new DateTime[] {DateTime.MinValue} ;
         T00502_n2014PessoaCertificado_Validade = new bool[] {false} ;
         T00502_A2011PessoaCertificado_PesCod = new int[1] ;
         T005010_A2010PessoaCertificado_Codigo = new int[1] ;
         T005013_A2010PessoaCertificado_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T005014_A2011PessoaCertificado_PesCod = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.pessoacertificado__default(),
            new Object[][] {
                new Object[] {
               T00502_A2010PessoaCertificado_Codigo, T00502_A2012PessoaCertificado_Emissao, T00502_A2013PessoaCertificado_Nome, T00502_A2014PessoaCertificado_Validade, T00502_n2014PessoaCertificado_Validade, T00502_A2011PessoaCertificado_PesCod
               }
               , new Object[] {
               T00503_A2010PessoaCertificado_Codigo, T00503_A2012PessoaCertificado_Emissao, T00503_A2013PessoaCertificado_Nome, T00503_A2014PessoaCertificado_Validade, T00503_n2014PessoaCertificado_Validade, T00503_A2011PessoaCertificado_PesCod
               }
               , new Object[] {
               T00504_A2011PessoaCertificado_PesCod
               }
               , new Object[] {
               T00505_A2010PessoaCertificado_Codigo, T00505_A2012PessoaCertificado_Emissao, T00505_A2013PessoaCertificado_Nome, T00505_A2014PessoaCertificado_Validade, T00505_n2014PessoaCertificado_Validade, T00505_A2011PessoaCertificado_PesCod
               }
               , new Object[] {
               T00506_A2011PessoaCertificado_PesCod
               }
               , new Object[] {
               T00507_A2010PessoaCertificado_Codigo
               }
               , new Object[] {
               T00508_A2010PessoaCertificado_Codigo
               }
               , new Object[] {
               T00509_A2010PessoaCertificado_Codigo
               }
               , new Object[] {
               T005010_A2010PessoaCertificado_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T005013_A2010PessoaCertificado_Codigo
               }
               , new Object[] {
               T005014_A2011PessoaCertificado_PesCod
               }
            }
         );
         AV14Pgmname = "PessoaCertificado";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound222 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7PessoaCertificado_Codigo ;
      private int wcpOAV13PessoaCertificado_PesCod ;
      private int Z2010PessoaCertificado_Codigo ;
      private int Z2011PessoaCertificado_PesCod ;
      private int N2011PessoaCertificado_PesCod ;
      private int A2011PessoaCertificado_PesCod ;
      private int AV7PessoaCertificado_Codigo ;
      private int AV13PessoaCertificado_PesCod ;
      private int trnEnded ;
      private int A2010PessoaCertificado_Codigo ;
      private int edtPessoaCertificado_Codigo_Enabled ;
      private int edtPessoaCertificado_Codigo_Visible ;
      private int edtPessoaCertificado_PesCod_Visible ;
      private int edtPessoaCertificado_PesCod_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtPessoaCertificado_Nome_Enabled ;
      private int edtPessoaCertificado_Emissao_Enabled ;
      private int edtPessoaCertificado_Validade_Enabled ;
      private int AV11Insert_PessoaCertificado_PesCod ;
      private int AV15GXV1 ;
      private int i2011PessoaCertificado_PesCod ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z2013PessoaCertificado_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtPessoaCertificado_Nome_Internalname ;
      private String edtPessoaCertificado_Codigo_Internalname ;
      private String edtPessoaCertificado_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtPessoaCertificado_PesCod_Internalname ;
      private String edtPessoaCertificado_PesCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockpessoacertificado_nome_Internalname ;
      private String lblTextblockpessoacertificado_nome_Jsonclick ;
      private String A2013PessoaCertificado_Nome ;
      private String edtPessoaCertificado_Nome_Jsonclick ;
      private String lblTextblockpessoacertificado_emissao_Internalname ;
      private String lblTextblockpessoacertificado_emissao_Jsonclick ;
      private String edtPessoaCertificado_Emissao_Internalname ;
      private String edtPessoaCertificado_Emissao_Jsonclick ;
      private String lblTextblockpessoacertificado_validade_Internalname ;
      private String lblTextblockpessoacertificado_validade_Jsonclick ;
      private String edtPessoaCertificado_Validade_Internalname ;
      private String edtPessoaCertificado_Validade_Jsonclick ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode222 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z2012PessoaCertificado_Emissao ;
      private DateTime Z2014PessoaCertificado_Validade ;
      private DateTime A2012PessoaCertificado_Emissao ;
      private DateTime A2014PessoaCertificado_Validade ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n2014PessoaCertificado_Validade ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_PessoaCertificado_PesCod ;
      private IDataStoreProvider pr_default ;
      private int[] T00505_A2010PessoaCertificado_Codigo ;
      private DateTime[] T00505_A2012PessoaCertificado_Emissao ;
      private String[] T00505_A2013PessoaCertificado_Nome ;
      private DateTime[] T00505_A2014PessoaCertificado_Validade ;
      private bool[] T00505_n2014PessoaCertificado_Validade ;
      private int[] T00505_A2011PessoaCertificado_PesCod ;
      private int[] T00504_A2011PessoaCertificado_PesCod ;
      private int[] T00506_A2011PessoaCertificado_PesCod ;
      private int[] T00507_A2010PessoaCertificado_Codigo ;
      private int[] T00503_A2010PessoaCertificado_Codigo ;
      private DateTime[] T00503_A2012PessoaCertificado_Emissao ;
      private String[] T00503_A2013PessoaCertificado_Nome ;
      private DateTime[] T00503_A2014PessoaCertificado_Validade ;
      private bool[] T00503_n2014PessoaCertificado_Validade ;
      private int[] T00503_A2011PessoaCertificado_PesCod ;
      private int[] T00508_A2010PessoaCertificado_Codigo ;
      private int[] T00509_A2010PessoaCertificado_Codigo ;
      private int[] T00502_A2010PessoaCertificado_Codigo ;
      private DateTime[] T00502_A2012PessoaCertificado_Emissao ;
      private String[] T00502_A2013PessoaCertificado_Nome ;
      private DateTime[] T00502_A2014PessoaCertificado_Validade ;
      private bool[] T00502_n2014PessoaCertificado_Validade ;
      private int[] T00502_A2011PessoaCertificado_PesCod ;
      private int[] T005010_A2010PessoaCertificado_Codigo ;
      private int[] T005013_A2010PessoaCertificado_Codigo ;
      private int[] T005014_A2011PessoaCertificado_PesCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class pessoacertificado__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00505 ;
          prmT00505 = new Object[] {
          new Object[] {"@PessoaCertificado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00504 ;
          prmT00504 = new Object[] {
          new Object[] {"@PessoaCertificado_PesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00506 ;
          prmT00506 = new Object[] {
          new Object[] {"@PessoaCertificado_PesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00507 ;
          prmT00507 = new Object[] {
          new Object[] {"@PessoaCertificado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00503 ;
          prmT00503 = new Object[] {
          new Object[] {"@PessoaCertificado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00508 ;
          prmT00508 = new Object[] {
          new Object[] {"@PessoaCertificado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00509 ;
          prmT00509 = new Object[] {
          new Object[] {"@PessoaCertificado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00502 ;
          prmT00502 = new Object[] {
          new Object[] {"@PessoaCertificado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005010 ;
          prmT005010 = new Object[] {
          new Object[] {"@PessoaCertificado_Emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@PessoaCertificado_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@PessoaCertificado_Validade",SqlDbType.DateTime,8,0} ,
          new Object[] {"@PessoaCertificado_PesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005011 ;
          prmT005011 = new Object[] {
          new Object[] {"@PessoaCertificado_Emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@PessoaCertificado_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@PessoaCertificado_Validade",SqlDbType.DateTime,8,0} ,
          new Object[] {"@PessoaCertificado_PesCod",SqlDbType.Int,6,0} ,
          new Object[] {"@PessoaCertificado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005012 ;
          prmT005012 = new Object[] {
          new Object[] {"@PessoaCertificado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005013 ;
          prmT005013 = new Object[] {
          } ;
          Object[] prmT005014 ;
          prmT005014 = new Object[] {
          new Object[] {"@PessoaCertificado_PesCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00502", "SELECT [PessoaCertificado_Codigo], [PessoaCertificado_Emissao], [PessoaCertificado_Nome], [PessoaCertificado_Validade], [PessoaCertificado_PesCod] AS PessoaCertificado_PesCod FROM [PessoaCertificado] WITH (UPDLOCK) WHERE [PessoaCertificado_Codigo] = @PessoaCertificado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00502,1,0,true,false )
             ,new CursorDef("T00503", "SELECT [PessoaCertificado_Codigo], [PessoaCertificado_Emissao], [PessoaCertificado_Nome], [PessoaCertificado_Validade], [PessoaCertificado_PesCod] AS PessoaCertificado_PesCod FROM [PessoaCertificado] WITH (NOLOCK) WHERE [PessoaCertificado_Codigo] = @PessoaCertificado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00503,1,0,true,false )
             ,new CursorDef("T00504", "SELECT [Pessoa_Codigo] AS PessoaCertificado_PesCod FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @PessoaCertificado_PesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00504,1,0,true,false )
             ,new CursorDef("T00505", "SELECT TM1.[PessoaCertificado_Codigo], TM1.[PessoaCertificado_Emissao], TM1.[PessoaCertificado_Nome], TM1.[PessoaCertificado_Validade], TM1.[PessoaCertificado_PesCod] AS PessoaCertificado_PesCod FROM [PessoaCertificado] TM1 WITH (NOLOCK) WHERE TM1.[PessoaCertificado_Codigo] = @PessoaCertificado_Codigo ORDER BY TM1.[PessoaCertificado_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00505,100,0,true,false )
             ,new CursorDef("T00506", "SELECT [Pessoa_Codigo] AS PessoaCertificado_PesCod FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @PessoaCertificado_PesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00506,1,0,true,false )
             ,new CursorDef("T00507", "SELECT [PessoaCertificado_Codigo] FROM [PessoaCertificado] WITH (NOLOCK) WHERE [PessoaCertificado_Codigo] = @PessoaCertificado_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00507,1,0,true,false )
             ,new CursorDef("T00508", "SELECT TOP 1 [PessoaCertificado_Codigo] FROM [PessoaCertificado] WITH (NOLOCK) WHERE ( [PessoaCertificado_Codigo] > @PessoaCertificado_Codigo) ORDER BY [PessoaCertificado_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00508,1,0,true,true )
             ,new CursorDef("T00509", "SELECT TOP 1 [PessoaCertificado_Codigo] FROM [PessoaCertificado] WITH (NOLOCK) WHERE ( [PessoaCertificado_Codigo] < @PessoaCertificado_Codigo) ORDER BY [PessoaCertificado_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00509,1,0,true,true )
             ,new CursorDef("T005010", "INSERT INTO [PessoaCertificado]([PessoaCertificado_Emissao], [PessoaCertificado_Nome], [PessoaCertificado_Validade], [PessoaCertificado_PesCod]) VALUES(@PessoaCertificado_Emissao, @PessoaCertificado_Nome, @PessoaCertificado_Validade, @PessoaCertificado_PesCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT005010)
             ,new CursorDef("T005011", "UPDATE [PessoaCertificado] SET [PessoaCertificado_Emissao]=@PessoaCertificado_Emissao, [PessoaCertificado_Nome]=@PessoaCertificado_Nome, [PessoaCertificado_Validade]=@PessoaCertificado_Validade, [PessoaCertificado_PesCod]=@PessoaCertificado_PesCod  WHERE [PessoaCertificado_Codigo] = @PessoaCertificado_Codigo", GxErrorMask.GX_NOMASK,prmT005011)
             ,new CursorDef("T005012", "DELETE FROM [PessoaCertificado]  WHERE [PessoaCertificado_Codigo] = @PessoaCertificado_Codigo", GxErrorMask.GX_NOMASK,prmT005012)
             ,new CursorDef("T005013", "SELECT [PessoaCertificado_Codigo] FROM [PessoaCertificado] WITH (NOLOCK) ORDER BY [PessoaCertificado_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT005013,100,0,true,false )
             ,new CursorDef("T005014", "SELECT [Pessoa_Codigo] AS PessoaCertificado_PesCod FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @PessoaCertificado_PesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005014,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[3]);
                }
                stmt.SetParameter(4, (int)parms[4]);
                return;
             case 9 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[3]);
                }
                stmt.SetParameter(4, (int)parms[4]);
                stmt.SetParameter(5, (int)parms[5]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
