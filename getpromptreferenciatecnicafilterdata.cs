/*
               File: GetPromptReferenciaTecnicaFilterData
        Description: Get Prompt Referencia Tecnica Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:29.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptreferenciatecnicafilterdata : GXProcedure
   {
      public getpromptreferenciatecnicafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptreferenciatecnicafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
         return AV35OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptreferenciatecnicafilterdata objgetpromptreferenciatecnicafilterdata;
         objgetpromptreferenciatecnicafilterdata = new getpromptreferenciatecnicafilterdata();
         objgetpromptreferenciatecnicafilterdata.AV26DDOName = aP0_DDOName;
         objgetpromptreferenciatecnicafilterdata.AV24SearchTxt = aP1_SearchTxt;
         objgetpromptreferenciatecnicafilterdata.AV25SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptreferenciatecnicafilterdata.AV30OptionsJson = "" ;
         objgetpromptreferenciatecnicafilterdata.AV33OptionsDescJson = "" ;
         objgetpromptreferenciatecnicafilterdata.AV35OptionIndexesJson = "" ;
         objgetpromptreferenciatecnicafilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptreferenciatecnicafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptreferenciatecnicafilterdata);
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptreferenciatecnicafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV29Options = (IGxCollection)(new GxSimpleCollection());
         AV32OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV34OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_REFERENCIATECNICA_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADREFERENCIATECNICA_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_REFERENCIATECNICA_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADREFERENCIATECNICA_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_GUIA_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADGUIA_NOMEOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV30OptionsJson = AV29Options.ToJSonString(false);
         AV33OptionsDescJson = AV32OptionsDesc.ToJSonString(false);
         AV35OptionIndexesJson = AV34OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get("PromptReferenciaTecnicaGridState"), "") == 0 )
         {
            AV39GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptReferenciaTecnicaGridState"), "");
         }
         else
         {
            AV39GridState.FromXml(AV37Session.Get("PromptReferenciaTecnicaGridState"), "");
         }
         AV58GXV1 = 1;
         while ( AV58GXV1 <= AV39GridState.gxTpr_Filtervalues.Count )
         {
            AV40GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV39GridState.gxTpr_Filtervalues.Item(AV58GXV1));
            if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFREFERENCIATECNICA_CODIGO") == 0 )
            {
               AV10TFReferenciaTecnica_Codigo = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV11TFReferenciaTecnica_Codigo_To = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFREFERENCIATECNICA_NOME") == 0 )
            {
               AV12TFReferenciaTecnica_Nome = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFREFERENCIATECNICA_NOME_SEL") == 0 )
            {
               AV13TFReferenciaTecnica_Nome_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFREFERENCIATECNICA_DESCRICAO") == 0 )
            {
               AV14TFReferenciaTecnica_Descricao = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFREFERENCIATECNICA_DESCRICAO_SEL") == 0 )
            {
               AV15TFReferenciaTecnica_Descricao_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFREFERENCIATECNICA_UNIDADE_SEL") == 0 )
            {
               AV16TFReferenciaTecnica_Unidade_SelsJson = AV40GridStateFilterValue.gxTpr_Value;
               AV17TFReferenciaTecnica_Unidade_Sels.FromJSonString(AV16TFReferenciaTecnica_Unidade_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFREFERENCIATECNICA_VALOR") == 0 )
            {
               AV18TFReferenciaTecnica_Valor = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, ".");
               AV19TFReferenciaTecnica_Valor_To = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFGUIA_CODIGO") == 0 )
            {
               AV20TFGuia_Codigo = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV21TFGuia_Codigo_To = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFGUIA_NOME") == 0 )
            {
               AV22TFGuia_Nome = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFGUIA_NOME_SEL") == 0 )
            {
               AV23TFGuia_Nome_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            AV58GXV1 = (int)(AV58GXV1+1);
         }
         if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(1));
            AV42DynamicFiltersSelector1 = AV41GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "REFERENCIATECNICA_NOME") == 0 )
            {
               AV43DynamicFiltersOperator1 = AV41GridStateDynamicFilter.gxTpr_Operator;
               AV44ReferenciaTecnica_Nome1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "GUIA_NOME") == 0 )
            {
               AV43DynamicFiltersOperator1 = AV41GridStateDynamicFilter.gxTpr_Operator;
               AV45Guia_Nome1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV46DynamicFiltersEnabled2 = true;
               AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(2));
               AV47DynamicFiltersSelector2 = AV41GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "REFERENCIATECNICA_NOME") == 0 )
               {
                  AV48DynamicFiltersOperator2 = AV41GridStateDynamicFilter.gxTpr_Operator;
                  AV49ReferenciaTecnica_Nome2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "GUIA_NOME") == 0 )
               {
                  AV48DynamicFiltersOperator2 = AV41GridStateDynamicFilter.gxTpr_Operator;
                  AV50Guia_Nome2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV51DynamicFiltersEnabled3 = true;
                  AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(3));
                  AV52DynamicFiltersSelector3 = AV41GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "REFERENCIATECNICA_NOME") == 0 )
                  {
                     AV53DynamicFiltersOperator3 = AV41GridStateDynamicFilter.gxTpr_Operator;
                     AV54ReferenciaTecnica_Nome3 = AV41GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "GUIA_NOME") == 0 )
                  {
                     AV53DynamicFiltersOperator3 = AV41GridStateDynamicFilter.gxTpr_Operator;
                     AV55Guia_Nome3 = AV41GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADREFERENCIATECNICA_NOMEOPTIONS' Routine */
         AV12TFReferenciaTecnica_Nome = AV24SearchTxt;
         AV13TFReferenciaTecnica_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A114ReferenciaTecnica_Unidade ,
                                              AV17TFReferenciaTecnica_Unidade_Sels ,
                                              AV42DynamicFiltersSelector1 ,
                                              AV43DynamicFiltersOperator1 ,
                                              AV44ReferenciaTecnica_Nome1 ,
                                              AV45Guia_Nome1 ,
                                              AV46DynamicFiltersEnabled2 ,
                                              AV47DynamicFiltersSelector2 ,
                                              AV48DynamicFiltersOperator2 ,
                                              AV49ReferenciaTecnica_Nome2 ,
                                              AV50Guia_Nome2 ,
                                              AV51DynamicFiltersEnabled3 ,
                                              AV52DynamicFiltersSelector3 ,
                                              AV53DynamicFiltersOperator3 ,
                                              AV54ReferenciaTecnica_Nome3 ,
                                              AV55Guia_Nome3 ,
                                              AV10TFReferenciaTecnica_Codigo ,
                                              AV11TFReferenciaTecnica_Codigo_To ,
                                              AV13TFReferenciaTecnica_Nome_Sel ,
                                              AV12TFReferenciaTecnica_Nome ,
                                              AV15TFReferenciaTecnica_Descricao_Sel ,
                                              AV14TFReferenciaTecnica_Descricao ,
                                              AV17TFReferenciaTecnica_Unidade_Sels.Count ,
                                              AV18TFReferenciaTecnica_Valor ,
                                              AV19TFReferenciaTecnica_Valor_To ,
                                              AV20TFGuia_Codigo ,
                                              AV21TFGuia_Codigo_To ,
                                              AV23TFGuia_Nome_Sel ,
                                              AV22TFGuia_Nome ,
                                              A98ReferenciaTecnica_Nome ,
                                              A94Guia_Nome ,
                                              A97ReferenciaTecnica_Codigo ,
                                              A99ReferenciaTecnica_Descricao ,
                                              A100ReferenciaTecnica_Valor ,
                                              A93Guia_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.INT
                                              }
         });
         lV44ReferenciaTecnica_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV44ReferenciaTecnica_Nome1), 50, "%");
         lV44ReferenciaTecnica_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV44ReferenciaTecnica_Nome1), 50, "%");
         lV45Guia_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV45Guia_Nome1), 50, "%");
         lV45Guia_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV45Guia_Nome1), 50, "%");
         lV49ReferenciaTecnica_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV49ReferenciaTecnica_Nome2), 50, "%");
         lV49ReferenciaTecnica_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV49ReferenciaTecnica_Nome2), 50, "%");
         lV50Guia_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV50Guia_Nome2), 50, "%");
         lV50Guia_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV50Guia_Nome2), 50, "%");
         lV54ReferenciaTecnica_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV54ReferenciaTecnica_Nome3), 50, "%");
         lV54ReferenciaTecnica_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV54ReferenciaTecnica_Nome3), 50, "%");
         lV55Guia_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV55Guia_Nome3), 50, "%");
         lV55Guia_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV55Guia_Nome3), 50, "%");
         lV12TFReferenciaTecnica_Nome = StringUtil.PadR( StringUtil.RTrim( AV12TFReferenciaTecnica_Nome), 50, "%");
         lV14TFReferenciaTecnica_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFReferenciaTecnica_Descricao), "%", "");
         lV22TFGuia_Nome = StringUtil.PadR( StringUtil.RTrim( AV22TFGuia_Nome), 50, "%");
         /* Using cursor P00UN2 */
         pr_default.execute(0, new Object[] {lV44ReferenciaTecnica_Nome1, lV44ReferenciaTecnica_Nome1, lV45Guia_Nome1, lV45Guia_Nome1, lV49ReferenciaTecnica_Nome2, lV49ReferenciaTecnica_Nome2, lV50Guia_Nome2, lV50Guia_Nome2, lV54ReferenciaTecnica_Nome3, lV54ReferenciaTecnica_Nome3, lV55Guia_Nome3, lV55Guia_Nome3, AV10TFReferenciaTecnica_Codigo, AV11TFReferenciaTecnica_Codigo_To, lV12TFReferenciaTecnica_Nome, AV13TFReferenciaTecnica_Nome_Sel, lV14TFReferenciaTecnica_Descricao, AV15TFReferenciaTecnica_Descricao_Sel, AV18TFReferenciaTecnica_Valor, AV19TFReferenciaTecnica_Valor_To, AV20TFGuia_Codigo, AV21TFGuia_Codigo_To, lV22TFGuia_Nome, AV23TFGuia_Nome_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUN2 = false;
            A98ReferenciaTecnica_Nome = P00UN2_A98ReferenciaTecnica_Nome[0];
            A93Guia_Codigo = P00UN2_A93Guia_Codigo[0];
            A100ReferenciaTecnica_Valor = P00UN2_A100ReferenciaTecnica_Valor[0];
            A114ReferenciaTecnica_Unidade = P00UN2_A114ReferenciaTecnica_Unidade[0];
            A99ReferenciaTecnica_Descricao = P00UN2_A99ReferenciaTecnica_Descricao[0];
            A97ReferenciaTecnica_Codigo = P00UN2_A97ReferenciaTecnica_Codigo[0];
            A94Guia_Nome = P00UN2_A94Guia_Nome[0];
            A94Guia_Nome = P00UN2_A94Guia_Nome[0];
            AV36count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00UN2_A98ReferenciaTecnica_Nome[0], A98ReferenciaTecnica_Nome) == 0 ) )
            {
               BRKUN2 = false;
               A97ReferenciaTecnica_Codigo = P00UN2_A97ReferenciaTecnica_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKUN2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A98ReferenciaTecnica_Nome)) )
            {
               AV28Option = A98ReferenciaTecnica_Nome;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUN2 )
            {
               BRKUN2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADREFERENCIATECNICA_DESCRICAOOPTIONS' Routine */
         AV14TFReferenciaTecnica_Descricao = AV24SearchTxt;
         AV15TFReferenciaTecnica_Descricao_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A114ReferenciaTecnica_Unidade ,
                                              AV17TFReferenciaTecnica_Unidade_Sels ,
                                              AV42DynamicFiltersSelector1 ,
                                              AV43DynamicFiltersOperator1 ,
                                              AV44ReferenciaTecnica_Nome1 ,
                                              AV45Guia_Nome1 ,
                                              AV46DynamicFiltersEnabled2 ,
                                              AV47DynamicFiltersSelector2 ,
                                              AV48DynamicFiltersOperator2 ,
                                              AV49ReferenciaTecnica_Nome2 ,
                                              AV50Guia_Nome2 ,
                                              AV51DynamicFiltersEnabled3 ,
                                              AV52DynamicFiltersSelector3 ,
                                              AV53DynamicFiltersOperator3 ,
                                              AV54ReferenciaTecnica_Nome3 ,
                                              AV55Guia_Nome3 ,
                                              AV10TFReferenciaTecnica_Codigo ,
                                              AV11TFReferenciaTecnica_Codigo_To ,
                                              AV13TFReferenciaTecnica_Nome_Sel ,
                                              AV12TFReferenciaTecnica_Nome ,
                                              AV15TFReferenciaTecnica_Descricao_Sel ,
                                              AV14TFReferenciaTecnica_Descricao ,
                                              AV17TFReferenciaTecnica_Unidade_Sels.Count ,
                                              AV18TFReferenciaTecnica_Valor ,
                                              AV19TFReferenciaTecnica_Valor_To ,
                                              AV20TFGuia_Codigo ,
                                              AV21TFGuia_Codigo_To ,
                                              AV23TFGuia_Nome_Sel ,
                                              AV22TFGuia_Nome ,
                                              A98ReferenciaTecnica_Nome ,
                                              A94Guia_Nome ,
                                              A97ReferenciaTecnica_Codigo ,
                                              A99ReferenciaTecnica_Descricao ,
                                              A100ReferenciaTecnica_Valor ,
                                              A93Guia_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.INT
                                              }
         });
         lV44ReferenciaTecnica_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV44ReferenciaTecnica_Nome1), 50, "%");
         lV44ReferenciaTecnica_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV44ReferenciaTecnica_Nome1), 50, "%");
         lV45Guia_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV45Guia_Nome1), 50, "%");
         lV45Guia_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV45Guia_Nome1), 50, "%");
         lV49ReferenciaTecnica_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV49ReferenciaTecnica_Nome2), 50, "%");
         lV49ReferenciaTecnica_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV49ReferenciaTecnica_Nome2), 50, "%");
         lV50Guia_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV50Guia_Nome2), 50, "%");
         lV50Guia_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV50Guia_Nome2), 50, "%");
         lV54ReferenciaTecnica_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV54ReferenciaTecnica_Nome3), 50, "%");
         lV54ReferenciaTecnica_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV54ReferenciaTecnica_Nome3), 50, "%");
         lV55Guia_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV55Guia_Nome3), 50, "%");
         lV55Guia_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV55Guia_Nome3), 50, "%");
         lV12TFReferenciaTecnica_Nome = StringUtil.PadR( StringUtil.RTrim( AV12TFReferenciaTecnica_Nome), 50, "%");
         lV14TFReferenciaTecnica_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFReferenciaTecnica_Descricao), "%", "");
         lV22TFGuia_Nome = StringUtil.PadR( StringUtil.RTrim( AV22TFGuia_Nome), 50, "%");
         /* Using cursor P00UN3 */
         pr_default.execute(1, new Object[] {lV44ReferenciaTecnica_Nome1, lV44ReferenciaTecnica_Nome1, lV45Guia_Nome1, lV45Guia_Nome1, lV49ReferenciaTecnica_Nome2, lV49ReferenciaTecnica_Nome2, lV50Guia_Nome2, lV50Guia_Nome2, lV54ReferenciaTecnica_Nome3, lV54ReferenciaTecnica_Nome3, lV55Guia_Nome3, lV55Guia_Nome3, AV10TFReferenciaTecnica_Codigo, AV11TFReferenciaTecnica_Codigo_To, lV12TFReferenciaTecnica_Nome, AV13TFReferenciaTecnica_Nome_Sel, lV14TFReferenciaTecnica_Descricao, AV15TFReferenciaTecnica_Descricao_Sel, AV18TFReferenciaTecnica_Valor, AV19TFReferenciaTecnica_Valor_To, AV20TFGuia_Codigo, AV21TFGuia_Codigo_To, lV22TFGuia_Nome, AV23TFGuia_Nome_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKUN4 = false;
            A99ReferenciaTecnica_Descricao = P00UN3_A99ReferenciaTecnica_Descricao[0];
            A93Guia_Codigo = P00UN3_A93Guia_Codigo[0];
            A100ReferenciaTecnica_Valor = P00UN3_A100ReferenciaTecnica_Valor[0];
            A114ReferenciaTecnica_Unidade = P00UN3_A114ReferenciaTecnica_Unidade[0];
            A97ReferenciaTecnica_Codigo = P00UN3_A97ReferenciaTecnica_Codigo[0];
            A94Guia_Nome = P00UN3_A94Guia_Nome[0];
            A98ReferenciaTecnica_Nome = P00UN3_A98ReferenciaTecnica_Nome[0];
            A94Guia_Nome = P00UN3_A94Guia_Nome[0];
            AV36count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00UN3_A99ReferenciaTecnica_Descricao[0], A99ReferenciaTecnica_Descricao) == 0 ) )
            {
               BRKUN4 = false;
               A97ReferenciaTecnica_Codigo = P00UN3_A97ReferenciaTecnica_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKUN4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A99ReferenciaTecnica_Descricao)) )
            {
               AV28Option = A99ReferenciaTecnica_Descricao;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUN4 )
            {
               BRKUN4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADGUIA_NOMEOPTIONS' Routine */
         AV22TFGuia_Nome = AV24SearchTxt;
         AV23TFGuia_Nome_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A114ReferenciaTecnica_Unidade ,
                                              AV17TFReferenciaTecnica_Unidade_Sels ,
                                              AV42DynamicFiltersSelector1 ,
                                              AV43DynamicFiltersOperator1 ,
                                              AV44ReferenciaTecnica_Nome1 ,
                                              AV45Guia_Nome1 ,
                                              AV46DynamicFiltersEnabled2 ,
                                              AV47DynamicFiltersSelector2 ,
                                              AV48DynamicFiltersOperator2 ,
                                              AV49ReferenciaTecnica_Nome2 ,
                                              AV50Guia_Nome2 ,
                                              AV51DynamicFiltersEnabled3 ,
                                              AV52DynamicFiltersSelector3 ,
                                              AV53DynamicFiltersOperator3 ,
                                              AV54ReferenciaTecnica_Nome3 ,
                                              AV55Guia_Nome3 ,
                                              AV10TFReferenciaTecnica_Codigo ,
                                              AV11TFReferenciaTecnica_Codigo_To ,
                                              AV13TFReferenciaTecnica_Nome_Sel ,
                                              AV12TFReferenciaTecnica_Nome ,
                                              AV15TFReferenciaTecnica_Descricao_Sel ,
                                              AV14TFReferenciaTecnica_Descricao ,
                                              AV17TFReferenciaTecnica_Unidade_Sels.Count ,
                                              AV18TFReferenciaTecnica_Valor ,
                                              AV19TFReferenciaTecnica_Valor_To ,
                                              AV20TFGuia_Codigo ,
                                              AV21TFGuia_Codigo_To ,
                                              AV23TFGuia_Nome_Sel ,
                                              AV22TFGuia_Nome ,
                                              A98ReferenciaTecnica_Nome ,
                                              A94Guia_Nome ,
                                              A97ReferenciaTecnica_Codigo ,
                                              A99ReferenciaTecnica_Descricao ,
                                              A100ReferenciaTecnica_Valor ,
                                              A93Guia_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.INT
                                              }
         });
         lV44ReferenciaTecnica_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV44ReferenciaTecnica_Nome1), 50, "%");
         lV44ReferenciaTecnica_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV44ReferenciaTecnica_Nome1), 50, "%");
         lV45Guia_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV45Guia_Nome1), 50, "%");
         lV45Guia_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV45Guia_Nome1), 50, "%");
         lV49ReferenciaTecnica_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV49ReferenciaTecnica_Nome2), 50, "%");
         lV49ReferenciaTecnica_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV49ReferenciaTecnica_Nome2), 50, "%");
         lV50Guia_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV50Guia_Nome2), 50, "%");
         lV50Guia_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV50Guia_Nome2), 50, "%");
         lV54ReferenciaTecnica_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV54ReferenciaTecnica_Nome3), 50, "%");
         lV54ReferenciaTecnica_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV54ReferenciaTecnica_Nome3), 50, "%");
         lV55Guia_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV55Guia_Nome3), 50, "%");
         lV55Guia_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV55Guia_Nome3), 50, "%");
         lV12TFReferenciaTecnica_Nome = StringUtil.PadR( StringUtil.RTrim( AV12TFReferenciaTecnica_Nome), 50, "%");
         lV14TFReferenciaTecnica_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFReferenciaTecnica_Descricao), "%", "");
         lV22TFGuia_Nome = StringUtil.PadR( StringUtil.RTrim( AV22TFGuia_Nome), 50, "%");
         /* Using cursor P00UN4 */
         pr_default.execute(2, new Object[] {lV44ReferenciaTecnica_Nome1, lV44ReferenciaTecnica_Nome1, lV45Guia_Nome1, lV45Guia_Nome1, lV49ReferenciaTecnica_Nome2, lV49ReferenciaTecnica_Nome2, lV50Guia_Nome2, lV50Guia_Nome2, lV54ReferenciaTecnica_Nome3, lV54ReferenciaTecnica_Nome3, lV55Guia_Nome3, lV55Guia_Nome3, AV10TFReferenciaTecnica_Codigo, AV11TFReferenciaTecnica_Codigo_To, lV12TFReferenciaTecnica_Nome, AV13TFReferenciaTecnica_Nome_Sel, lV14TFReferenciaTecnica_Descricao, AV15TFReferenciaTecnica_Descricao_Sel, AV18TFReferenciaTecnica_Valor, AV19TFReferenciaTecnica_Valor_To, AV20TFGuia_Codigo, AV21TFGuia_Codigo_To, lV22TFGuia_Nome, AV23TFGuia_Nome_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKUN6 = false;
            A93Guia_Codigo = P00UN4_A93Guia_Codigo[0];
            A100ReferenciaTecnica_Valor = P00UN4_A100ReferenciaTecnica_Valor[0];
            A114ReferenciaTecnica_Unidade = P00UN4_A114ReferenciaTecnica_Unidade[0];
            A99ReferenciaTecnica_Descricao = P00UN4_A99ReferenciaTecnica_Descricao[0];
            A97ReferenciaTecnica_Codigo = P00UN4_A97ReferenciaTecnica_Codigo[0];
            A94Guia_Nome = P00UN4_A94Guia_Nome[0];
            A98ReferenciaTecnica_Nome = P00UN4_A98ReferenciaTecnica_Nome[0];
            A94Guia_Nome = P00UN4_A94Guia_Nome[0];
            AV36count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00UN4_A93Guia_Codigo[0] == A93Guia_Codigo ) )
            {
               BRKUN6 = false;
               A97ReferenciaTecnica_Codigo = P00UN4_A97ReferenciaTecnica_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKUN6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A94Guia_Nome)) )
            {
               AV28Option = A94Guia_Nome;
               AV27InsertIndex = 1;
               while ( ( AV27InsertIndex <= AV29Options.Count ) && ( StringUtil.StrCmp(((String)AV29Options.Item(AV27InsertIndex)), AV28Option) < 0 ) )
               {
                  AV27InsertIndex = (int)(AV27InsertIndex+1);
               }
               AV29Options.Add(AV28Option, AV27InsertIndex);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), AV27InsertIndex);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUN6 )
            {
               BRKUN6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV29Options = new GxSimpleCollection();
         AV32OptionsDesc = new GxSimpleCollection();
         AV34OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV37Session = context.GetSession();
         AV39GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV40GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFReferenciaTecnica_Nome = "";
         AV13TFReferenciaTecnica_Nome_Sel = "";
         AV14TFReferenciaTecnica_Descricao = "";
         AV15TFReferenciaTecnica_Descricao_Sel = "";
         AV16TFReferenciaTecnica_Unidade_SelsJson = "";
         AV17TFReferenciaTecnica_Unidade_Sels = new GxSimpleCollection();
         AV22TFGuia_Nome = "";
         AV23TFGuia_Nome_Sel = "";
         AV41GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV42DynamicFiltersSelector1 = "";
         AV44ReferenciaTecnica_Nome1 = "";
         AV45Guia_Nome1 = "";
         AV47DynamicFiltersSelector2 = "";
         AV49ReferenciaTecnica_Nome2 = "";
         AV50Guia_Nome2 = "";
         AV52DynamicFiltersSelector3 = "";
         AV54ReferenciaTecnica_Nome3 = "";
         AV55Guia_Nome3 = "";
         scmdbuf = "";
         lV44ReferenciaTecnica_Nome1 = "";
         lV45Guia_Nome1 = "";
         lV49ReferenciaTecnica_Nome2 = "";
         lV50Guia_Nome2 = "";
         lV54ReferenciaTecnica_Nome3 = "";
         lV55Guia_Nome3 = "";
         lV12TFReferenciaTecnica_Nome = "";
         lV14TFReferenciaTecnica_Descricao = "";
         lV22TFGuia_Nome = "";
         A98ReferenciaTecnica_Nome = "";
         A94Guia_Nome = "";
         A99ReferenciaTecnica_Descricao = "";
         P00UN2_A98ReferenciaTecnica_Nome = new String[] {""} ;
         P00UN2_A93Guia_Codigo = new int[1] ;
         P00UN2_A100ReferenciaTecnica_Valor = new decimal[1] ;
         P00UN2_A114ReferenciaTecnica_Unidade = new short[1] ;
         P00UN2_A99ReferenciaTecnica_Descricao = new String[] {""} ;
         P00UN2_A97ReferenciaTecnica_Codigo = new int[1] ;
         P00UN2_A94Guia_Nome = new String[] {""} ;
         AV28Option = "";
         P00UN3_A99ReferenciaTecnica_Descricao = new String[] {""} ;
         P00UN3_A93Guia_Codigo = new int[1] ;
         P00UN3_A100ReferenciaTecnica_Valor = new decimal[1] ;
         P00UN3_A114ReferenciaTecnica_Unidade = new short[1] ;
         P00UN3_A97ReferenciaTecnica_Codigo = new int[1] ;
         P00UN3_A94Guia_Nome = new String[] {""} ;
         P00UN3_A98ReferenciaTecnica_Nome = new String[] {""} ;
         P00UN4_A93Guia_Codigo = new int[1] ;
         P00UN4_A100ReferenciaTecnica_Valor = new decimal[1] ;
         P00UN4_A114ReferenciaTecnica_Unidade = new short[1] ;
         P00UN4_A99ReferenciaTecnica_Descricao = new String[] {""} ;
         P00UN4_A97ReferenciaTecnica_Codigo = new int[1] ;
         P00UN4_A94Guia_Nome = new String[] {""} ;
         P00UN4_A98ReferenciaTecnica_Nome = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptreferenciatecnicafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UN2_A98ReferenciaTecnica_Nome, P00UN2_A93Guia_Codigo, P00UN2_A100ReferenciaTecnica_Valor, P00UN2_A114ReferenciaTecnica_Unidade, P00UN2_A99ReferenciaTecnica_Descricao, P00UN2_A97ReferenciaTecnica_Codigo, P00UN2_A94Guia_Nome
               }
               , new Object[] {
               P00UN3_A99ReferenciaTecnica_Descricao, P00UN3_A93Guia_Codigo, P00UN3_A100ReferenciaTecnica_Valor, P00UN3_A114ReferenciaTecnica_Unidade, P00UN3_A97ReferenciaTecnica_Codigo, P00UN3_A94Guia_Nome, P00UN3_A98ReferenciaTecnica_Nome
               }
               , new Object[] {
               P00UN4_A93Guia_Codigo, P00UN4_A100ReferenciaTecnica_Valor, P00UN4_A114ReferenciaTecnica_Unidade, P00UN4_A99ReferenciaTecnica_Descricao, P00UN4_A97ReferenciaTecnica_Codigo, P00UN4_A94Guia_Nome, P00UN4_A98ReferenciaTecnica_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV43DynamicFiltersOperator1 ;
      private short AV48DynamicFiltersOperator2 ;
      private short AV53DynamicFiltersOperator3 ;
      private short A114ReferenciaTecnica_Unidade ;
      private int AV58GXV1 ;
      private int AV10TFReferenciaTecnica_Codigo ;
      private int AV11TFReferenciaTecnica_Codigo_To ;
      private int AV20TFGuia_Codigo ;
      private int AV21TFGuia_Codigo_To ;
      private int AV17TFReferenciaTecnica_Unidade_Sels_Count ;
      private int A97ReferenciaTecnica_Codigo ;
      private int A93Guia_Codigo ;
      private int AV27InsertIndex ;
      private long AV36count ;
      private decimal AV18TFReferenciaTecnica_Valor ;
      private decimal AV19TFReferenciaTecnica_Valor_To ;
      private decimal A100ReferenciaTecnica_Valor ;
      private String AV12TFReferenciaTecnica_Nome ;
      private String AV13TFReferenciaTecnica_Nome_Sel ;
      private String AV22TFGuia_Nome ;
      private String AV23TFGuia_Nome_Sel ;
      private String AV44ReferenciaTecnica_Nome1 ;
      private String AV45Guia_Nome1 ;
      private String AV49ReferenciaTecnica_Nome2 ;
      private String AV50Guia_Nome2 ;
      private String AV54ReferenciaTecnica_Nome3 ;
      private String AV55Guia_Nome3 ;
      private String scmdbuf ;
      private String lV44ReferenciaTecnica_Nome1 ;
      private String lV45Guia_Nome1 ;
      private String lV49ReferenciaTecnica_Nome2 ;
      private String lV50Guia_Nome2 ;
      private String lV54ReferenciaTecnica_Nome3 ;
      private String lV55Guia_Nome3 ;
      private String lV12TFReferenciaTecnica_Nome ;
      private String lV22TFGuia_Nome ;
      private String A98ReferenciaTecnica_Nome ;
      private String A94Guia_Nome ;
      private bool returnInSub ;
      private bool AV46DynamicFiltersEnabled2 ;
      private bool AV51DynamicFiltersEnabled3 ;
      private bool BRKUN2 ;
      private bool BRKUN4 ;
      private bool BRKUN6 ;
      private String AV35OptionIndexesJson ;
      private String AV30OptionsJson ;
      private String AV33OptionsDescJson ;
      private String AV16TFReferenciaTecnica_Unidade_SelsJson ;
      private String A99ReferenciaTecnica_Descricao ;
      private String AV26DDOName ;
      private String AV24SearchTxt ;
      private String AV25SearchTxtTo ;
      private String AV14TFReferenciaTecnica_Descricao ;
      private String AV15TFReferenciaTecnica_Descricao_Sel ;
      private String AV42DynamicFiltersSelector1 ;
      private String AV47DynamicFiltersSelector2 ;
      private String AV52DynamicFiltersSelector3 ;
      private String lV14TFReferenciaTecnica_Descricao ;
      private String AV28Option ;
      private IGxSession AV37Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00UN2_A98ReferenciaTecnica_Nome ;
      private int[] P00UN2_A93Guia_Codigo ;
      private decimal[] P00UN2_A100ReferenciaTecnica_Valor ;
      private short[] P00UN2_A114ReferenciaTecnica_Unidade ;
      private String[] P00UN2_A99ReferenciaTecnica_Descricao ;
      private int[] P00UN2_A97ReferenciaTecnica_Codigo ;
      private String[] P00UN2_A94Guia_Nome ;
      private String[] P00UN3_A99ReferenciaTecnica_Descricao ;
      private int[] P00UN3_A93Guia_Codigo ;
      private decimal[] P00UN3_A100ReferenciaTecnica_Valor ;
      private short[] P00UN3_A114ReferenciaTecnica_Unidade ;
      private int[] P00UN3_A97ReferenciaTecnica_Codigo ;
      private String[] P00UN3_A94Guia_Nome ;
      private String[] P00UN3_A98ReferenciaTecnica_Nome ;
      private int[] P00UN4_A93Guia_Codigo ;
      private decimal[] P00UN4_A100ReferenciaTecnica_Valor ;
      private short[] P00UN4_A114ReferenciaTecnica_Unidade ;
      private String[] P00UN4_A99ReferenciaTecnica_Descricao ;
      private int[] P00UN4_A97ReferenciaTecnica_Codigo ;
      private String[] P00UN4_A94Guia_Nome ;
      private String[] P00UN4_A98ReferenciaTecnica_Nome ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV17TFReferenciaTecnica_Unidade_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV39GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV40GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV41GridStateDynamicFilter ;
   }

   public class getpromptreferenciatecnicafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UN2( IGxContext context ,
                                             short A114ReferenciaTecnica_Unidade ,
                                             IGxCollection AV17TFReferenciaTecnica_Unidade_Sels ,
                                             String AV42DynamicFiltersSelector1 ,
                                             short AV43DynamicFiltersOperator1 ,
                                             String AV44ReferenciaTecnica_Nome1 ,
                                             String AV45Guia_Nome1 ,
                                             bool AV46DynamicFiltersEnabled2 ,
                                             String AV47DynamicFiltersSelector2 ,
                                             short AV48DynamicFiltersOperator2 ,
                                             String AV49ReferenciaTecnica_Nome2 ,
                                             String AV50Guia_Nome2 ,
                                             bool AV51DynamicFiltersEnabled3 ,
                                             String AV52DynamicFiltersSelector3 ,
                                             short AV53DynamicFiltersOperator3 ,
                                             String AV54ReferenciaTecnica_Nome3 ,
                                             String AV55Guia_Nome3 ,
                                             int AV10TFReferenciaTecnica_Codigo ,
                                             int AV11TFReferenciaTecnica_Codigo_To ,
                                             String AV13TFReferenciaTecnica_Nome_Sel ,
                                             String AV12TFReferenciaTecnica_Nome ,
                                             String AV15TFReferenciaTecnica_Descricao_Sel ,
                                             String AV14TFReferenciaTecnica_Descricao ,
                                             int AV17TFReferenciaTecnica_Unidade_Sels_Count ,
                                             decimal AV18TFReferenciaTecnica_Valor ,
                                             decimal AV19TFReferenciaTecnica_Valor_To ,
                                             int AV20TFGuia_Codigo ,
                                             int AV21TFGuia_Codigo_To ,
                                             String AV23TFGuia_Nome_Sel ,
                                             String AV22TFGuia_Nome ,
                                             String A98ReferenciaTecnica_Nome ,
                                             String A94Guia_Nome ,
                                             int A97ReferenciaTecnica_Codigo ,
                                             String A99ReferenciaTecnica_Descricao ,
                                             decimal A100ReferenciaTecnica_Valor ,
                                             int A93Guia_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [24] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ReferenciaTecnica_Nome], T1.[Guia_Codigo], T1.[ReferenciaTecnica_Valor], T1.[ReferenciaTecnica_Unidade], T1.[ReferenciaTecnica_Descricao], T1.[ReferenciaTecnica_Codigo], T2.[Guia_Nome] FROM ([ReferenciaTecnica] T1 WITH (NOLOCK) INNER JOIN [Guia] T2 WITH (NOLOCK) ON T2.[Guia_Codigo] = T1.[Guia_Codigo])";
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "REFERENCIATECNICA_NOME") == 0 ) && ( AV43DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ReferenciaTecnica_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV44ReferenciaTecnica_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV44ReferenciaTecnica_Nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "REFERENCIATECNICA_NOME") == 0 ) && ( AV43DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ReferenciaTecnica_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV44ReferenciaTecnica_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV44ReferenciaTecnica_Nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "GUIA_NOME") == 0 ) && ( AV43DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Guia_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV45Guia_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV45Guia_Nome1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "GUIA_NOME") == 0 ) && ( AV43DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Guia_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV45Guia_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV45Guia_Nome1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV46DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "REFERENCIATECNICA_NOME") == 0 ) && ( AV48DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ReferenciaTecnica_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV49ReferenciaTecnica_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV49ReferenciaTecnica_Nome2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV46DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "REFERENCIATECNICA_NOME") == 0 ) && ( AV48DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ReferenciaTecnica_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV49ReferenciaTecnica_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV49ReferenciaTecnica_Nome2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV46DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "GUIA_NOME") == 0 ) && ( AV48DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50Guia_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV50Guia_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV50Guia_Nome2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV46DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "GUIA_NOME") == 0 ) && ( AV48DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50Guia_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV50Guia_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV50Guia_Nome2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "REFERENCIATECNICA_NOME") == 0 ) && ( AV53DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ReferenciaTecnica_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV54ReferenciaTecnica_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV54ReferenciaTecnica_Nome3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "REFERENCIATECNICA_NOME") == 0 ) && ( AV53DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ReferenciaTecnica_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV54ReferenciaTecnica_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV54ReferenciaTecnica_Nome3)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "GUIA_NOME") == 0 ) && ( AV53DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Guia_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV55Guia_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV55Guia_Nome3)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "GUIA_NOME") == 0 ) && ( AV53DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Guia_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV55Guia_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV55Guia_Nome3)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV10TFReferenciaTecnica_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Codigo] >= @AV10TFReferenciaTecnica_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Codigo] >= @AV10TFReferenciaTecnica_Codigo)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (0==AV11TFReferenciaTecnica_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Codigo] <= @AV11TFReferenciaTecnica_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Codigo] <= @AV11TFReferenciaTecnica_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFReferenciaTecnica_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFReferenciaTecnica_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV12TFReferenciaTecnica_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV12TFReferenciaTecnica_Nome)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFReferenciaTecnica_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] = @AV13TFReferenciaTecnica_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] = @AV13TFReferenciaTecnica_Nome_Sel)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFReferenciaTecnica_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFReferenciaTecnica_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Descricao] like @lV14TFReferenciaTecnica_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Descricao] like @lV14TFReferenciaTecnica_Descricao)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFReferenciaTecnica_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Descricao] = @AV15TFReferenciaTecnica_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Descricao] = @AV15TFReferenciaTecnica_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV17TFReferenciaTecnica_Unidade_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFReferenciaTecnica_Unidade_Sels, "T1.[ReferenciaTecnica_Unidade] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFReferenciaTecnica_Unidade_Sels, "T1.[ReferenciaTecnica_Unidade] IN (", ")") + ")";
            }
         }
         if ( ! (Convert.ToDecimal(0)==AV18TFReferenciaTecnica_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Valor] >= @AV18TFReferenciaTecnica_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Valor] >= @AV18TFReferenciaTecnica_Valor)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV19TFReferenciaTecnica_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Valor] <= @AV19TFReferenciaTecnica_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Valor] <= @AV19TFReferenciaTecnica_Valor_To)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (0==AV20TFGuia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] >= @AV20TFGuia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] >= @AV20TFGuia_Codigo)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (0==AV21TFGuia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] <= @AV21TFGuia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] <= @AV21TFGuia_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFGuia_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFGuia_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV22TFGuia_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV22TFGuia_Nome)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFGuia_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] = @AV23TFGuia_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] = @AV23TFGuia_Nome_Sel)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ReferenciaTecnica_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00UN3( IGxContext context ,
                                             short A114ReferenciaTecnica_Unidade ,
                                             IGxCollection AV17TFReferenciaTecnica_Unidade_Sels ,
                                             String AV42DynamicFiltersSelector1 ,
                                             short AV43DynamicFiltersOperator1 ,
                                             String AV44ReferenciaTecnica_Nome1 ,
                                             String AV45Guia_Nome1 ,
                                             bool AV46DynamicFiltersEnabled2 ,
                                             String AV47DynamicFiltersSelector2 ,
                                             short AV48DynamicFiltersOperator2 ,
                                             String AV49ReferenciaTecnica_Nome2 ,
                                             String AV50Guia_Nome2 ,
                                             bool AV51DynamicFiltersEnabled3 ,
                                             String AV52DynamicFiltersSelector3 ,
                                             short AV53DynamicFiltersOperator3 ,
                                             String AV54ReferenciaTecnica_Nome3 ,
                                             String AV55Guia_Nome3 ,
                                             int AV10TFReferenciaTecnica_Codigo ,
                                             int AV11TFReferenciaTecnica_Codigo_To ,
                                             String AV13TFReferenciaTecnica_Nome_Sel ,
                                             String AV12TFReferenciaTecnica_Nome ,
                                             String AV15TFReferenciaTecnica_Descricao_Sel ,
                                             String AV14TFReferenciaTecnica_Descricao ,
                                             int AV17TFReferenciaTecnica_Unidade_Sels_Count ,
                                             decimal AV18TFReferenciaTecnica_Valor ,
                                             decimal AV19TFReferenciaTecnica_Valor_To ,
                                             int AV20TFGuia_Codigo ,
                                             int AV21TFGuia_Codigo_To ,
                                             String AV23TFGuia_Nome_Sel ,
                                             String AV22TFGuia_Nome ,
                                             String A98ReferenciaTecnica_Nome ,
                                             String A94Guia_Nome ,
                                             int A97ReferenciaTecnica_Codigo ,
                                             String A99ReferenciaTecnica_Descricao ,
                                             decimal A100ReferenciaTecnica_Valor ,
                                             int A93Guia_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [24] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ReferenciaTecnica_Descricao], T1.[Guia_Codigo], T1.[ReferenciaTecnica_Valor], T1.[ReferenciaTecnica_Unidade], T1.[ReferenciaTecnica_Codigo], T2.[Guia_Nome], T1.[ReferenciaTecnica_Nome] FROM ([ReferenciaTecnica] T1 WITH (NOLOCK) INNER JOIN [Guia] T2 WITH (NOLOCK) ON T2.[Guia_Codigo] = T1.[Guia_Codigo])";
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "REFERENCIATECNICA_NOME") == 0 ) && ( AV43DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ReferenciaTecnica_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV44ReferenciaTecnica_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV44ReferenciaTecnica_Nome1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "REFERENCIATECNICA_NOME") == 0 ) && ( AV43DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ReferenciaTecnica_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV44ReferenciaTecnica_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV44ReferenciaTecnica_Nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "GUIA_NOME") == 0 ) && ( AV43DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Guia_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV45Guia_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV45Guia_Nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "GUIA_NOME") == 0 ) && ( AV43DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Guia_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV45Guia_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV45Guia_Nome1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV46DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "REFERENCIATECNICA_NOME") == 0 ) && ( AV48DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ReferenciaTecnica_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV49ReferenciaTecnica_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV49ReferenciaTecnica_Nome2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV46DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "REFERENCIATECNICA_NOME") == 0 ) && ( AV48DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ReferenciaTecnica_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV49ReferenciaTecnica_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV49ReferenciaTecnica_Nome2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV46DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "GUIA_NOME") == 0 ) && ( AV48DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50Guia_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV50Guia_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV50Guia_Nome2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV46DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "GUIA_NOME") == 0 ) && ( AV48DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50Guia_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV50Guia_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV50Guia_Nome2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "REFERENCIATECNICA_NOME") == 0 ) && ( AV53DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ReferenciaTecnica_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV54ReferenciaTecnica_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV54ReferenciaTecnica_Nome3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "REFERENCIATECNICA_NOME") == 0 ) && ( AV53DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ReferenciaTecnica_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV54ReferenciaTecnica_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV54ReferenciaTecnica_Nome3)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "GUIA_NOME") == 0 ) && ( AV53DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Guia_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV55Guia_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV55Guia_Nome3)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "GUIA_NOME") == 0 ) && ( AV53DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Guia_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV55Guia_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV55Guia_Nome3)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV10TFReferenciaTecnica_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Codigo] >= @AV10TFReferenciaTecnica_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Codigo] >= @AV10TFReferenciaTecnica_Codigo)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV11TFReferenciaTecnica_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Codigo] <= @AV11TFReferenciaTecnica_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Codigo] <= @AV11TFReferenciaTecnica_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFReferenciaTecnica_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFReferenciaTecnica_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV12TFReferenciaTecnica_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV12TFReferenciaTecnica_Nome)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFReferenciaTecnica_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] = @AV13TFReferenciaTecnica_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] = @AV13TFReferenciaTecnica_Nome_Sel)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFReferenciaTecnica_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFReferenciaTecnica_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Descricao] like @lV14TFReferenciaTecnica_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Descricao] like @lV14TFReferenciaTecnica_Descricao)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFReferenciaTecnica_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Descricao] = @AV15TFReferenciaTecnica_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Descricao] = @AV15TFReferenciaTecnica_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV17TFReferenciaTecnica_Unidade_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFReferenciaTecnica_Unidade_Sels, "T1.[ReferenciaTecnica_Unidade] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFReferenciaTecnica_Unidade_Sels, "T1.[ReferenciaTecnica_Unidade] IN (", ")") + ")";
            }
         }
         if ( ! (Convert.ToDecimal(0)==AV18TFReferenciaTecnica_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Valor] >= @AV18TFReferenciaTecnica_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Valor] >= @AV18TFReferenciaTecnica_Valor)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV19TFReferenciaTecnica_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Valor] <= @AV19TFReferenciaTecnica_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Valor] <= @AV19TFReferenciaTecnica_Valor_To)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! (0==AV20TFGuia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] >= @AV20TFGuia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] >= @AV20TFGuia_Codigo)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! (0==AV21TFGuia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] <= @AV21TFGuia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] <= @AV21TFGuia_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFGuia_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFGuia_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV22TFGuia_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV22TFGuia_Nome)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFGuia_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] = @AV23TFGuia_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] = @AV23TFGuia_Nome_Sel)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ReferenciaTecnica_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00UN4( IGxContext context ,
                                             short A114ReferenciaTecnica_Unidade ,
                                             IGxCollection AV17TFReferenciaTecnica_Unidade_Sels ,
                                             String AV42DynamicFiltersSelector1 ,
                                             short AV43DynamicFiltersOperator1 ,
                                             String AV44ReferenciaTecnica_Nome1 ,
                                             String AV45Guia_Nome1 ,
                                             bool AV46DynamicFiltersEnabled2 ,
                                             String AV47DynamicFiltersSelector2 ,
                                             short AV48DynamicFiltersOperator2 ,
                                             String AV49ReferenciaTecnica_Nome2 ,
                                             String AV50Guia_Nome2 ,
                                             bool AV51DynamicFiltersEnabled3 ,
                                             String AV52DynamicFiltersSelector3 ,
                                             short AV53DynamicFiltersOperator3 ,
                                             String AV54ReferenciaTecnica_Nome3 ,
                                             String AV55Guia_Nome3 ,
                                             int AV10TFReferenciaTecnica_Codigo ,
                                             int AV11TFReferenciaTecnica_Codigo_To ,
                                             String AV13TFReferenciaTecnica_Nome_Sel ,
                                             String AV12TFReferenciaTecnica_Nome ,
                                             String AV15TFReferenciaTecnica_Descricao_Sel ,
                                             String AV14TFReferenciaTecnica_Descricao ,
                                             int AV17TFReferenciaTecnica_Unidade_Sels_Count ,
                                             decimal AV18TFReferenciaTecnica_Valor ,
                                             decimal AV19TFReferenciaTecnica_Valor_To ,
                                             int AV20TFGuia_Codigo ,
                                             int AV21TFGuia_Codigo_To ,
                                             String AV23TFGuia_Nome_Sel ,
                                             String AV22TFGuia_Nome ,
                                             String A98ReferenciaTecnica_Nome ,
                                             String A94Guia_Nome ,
                                             int A97ReferenciaTecnica_Codigo ,
                                             String A99ReferenciaTecnica_Descricao ,
                                             decimal A100ReferenciaTecnica_Valor ,
                                             int A93Guia_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [24] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Guia_Codigo], T1.[ReferenciaTecnica_Valor], T1.[ReferenciaTecnica_Unidade], T1.[ReferenciaTecnica_Descricao], T1.[ReferenciaTecnica_Codigo], T2.[Guia_Nome], T1.[ReferenciaTecnica_Nome] FROM ([ReferenciaTecnica] T1 WITH (NOLOCK) INNER JOIN [Guia] T2 WITH (NOLOCK) ON T2.[Guia_Codigo] = T1.[Guia_Codigo])";
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "REFERENCIATECNICA_NOME") == 0 ) && ( AV43DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ReferenciaTecnica_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV44ReferenciaTecnica_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV44ReferenciaTecnica_Nome1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "REFERENCIATECNICA_NOME") == 0 ) && ( AV43DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ReferenciaTecnica_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV44ReferenciaTecnica_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV44ReferenciaTecnica_Nome1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "GUIA_NOME") == 0 ) && ( AV43DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Guia_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV45Guia_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV45Guia_Nome1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "GUIA_NOME") == 0 ) && ( AV43DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Guia_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV45Guia_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV45Guia_Nome1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV46DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "REFERENCIATECNICA_NOME") == 0 ) && ( AV48DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ReferenciaTecnica_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV49ReferenciaTecnica_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV49ReferenciaTecnica_Nome2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV46DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "REFERENCIATECNICA_NOME") == 0 ) && ( AV48DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ReferenciaTecnica_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV49ReferenciaTecnica_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV49ReferenciaTecnica_Nome2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV46DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "GUIA_NOME") == 0 ) && ( AV48DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50Guia_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV50Guia_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV50Guia_Nome2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV46DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "GUIA_NOME") == 0 ) && ( AV48DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50Guia_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV50Guia_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV50Guia_Nome2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "REFERENCIATECNICA_NOME") == 0 ) && ( AV53DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ReferenciaTecnica_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV54ReferenciaTecnica_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV54ReferenciaTecnica_Nome3)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "REFERENCIATECNICA_NOME") == 0 ) && ( AV53DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ReferenciaTecnica_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV54ReferenciaTecnica_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV54ReferenciaTecnica_Nome3)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "GUIA_NOME") == 0 ) && ( AV53DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Guia_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV55Guia_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV55Guia_Nome3)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "GUIA_NOME") == 0 ) && ( AV53DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Guia_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV55Guia_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV55Guia_Nome3)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! (0==AV10TFReferenciaTecnica_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Codigo] >= @AV10TFReferenciaTecnica_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Codigo] >= @AV10TFReferenciaTecnica_Codigo)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! (0==AV11TFReferenciaTecnica_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Codigo] <= @AV11TFReferenciaTecnica_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Codigo] <= @AV11TFReferenciaTecnica_Codigo_To)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFReferenciaTecnica_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFReferenciaTecnica_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV12TFReferenciaTecnica_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV12TFReferenciaTecnica_Nome)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFReferenciaTecnica_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] = @AV13TFReferenciaTecnica_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] = @AV13TFReferenciaTecnica_Nome_Sel)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFReferenciaTecnica_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFReferenciaTecnica_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Descricao] like @lV14TFReferenciaTecnica_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Descricao] like @lV14TFReferenciaTecnica_Descricao)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFReferenciaTecnica_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Descricao] = @AV15TFReferenciaTecnica_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Descricao] = @AV15TFReferenciaTecnica_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( AV17TFReferenciaTecnica_Unidade_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFReferenciaTecnica_Unidade_Sels, "T1.[ReferenciaTecnica_Unidade] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFReferenciaTecnica_Unidade_Sels, "T1.[ReferenciaTecnica_Unidade] IN (", ")") + ")";
            }
         }
         if ( ! (Convert.ToDecimal(0)==AV18TFReferenciaTecnica_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Valor] >= @AV18TFReferenciaTecnica_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Valor] >= @AV18TFReferenciaTecnica_Valor)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV19TFReferenciaTecnica_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Valor] <= @AV19TFReferenciaTecnica_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Valor] <= @AV19TFReferenciaTecnica_Valor_To)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( ! (0==AV20TFGuia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] >= @AV20TFGuia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] >= @AV20TFGuia_Codigo)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! (0==AV21TFGuia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] <= @AV21TFGuia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] <= @AV21TFGuia_Codigo_To)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFGuia_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFGuia_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV22TFGuia_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV22TFGuia_Nome)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFGuia_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] = @AV23TFGuia_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] = @AV23TFGuia_Nome_Sel)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Guia_Codigo]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UN2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (decimal)dynConstraints[33] , (int)dynConstraints[34] );
               case 1 :
                     return conditional_P00UN3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (decimal)dynConstraints[33] , (int)dynConstraints[34] );
               case 2 :
                     return conditional_P00UN4(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (decimal)dynConstraints[33] , (int)dynConstraints[34] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UN2 ;
          prmP00UN2 = new Object[] {
          new Object[] {"@lV44ReferenciaTecnica_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV44ReferenciaTecnica_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV45Guia_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV45Guia_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49ReferenciaTecnica_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49ReferenciaTecnica_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50Guia_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50Guia_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54ReferenciaTecnica_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54ReferenciaTecnica_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55Guia_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55Guia_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV10TFReferenciaTecnica_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFReferenciaTecnica_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFReferenciaTecnica_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFReferenciaTecnica_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14TFReferenciaTecnica_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV15TFReferenciaTecnica_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV18TFReferenciaTecnica_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV19TFReferenciaTecnica_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV20TFGuia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFGuia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFGuia_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV23TFGuia_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00UN3 ;
          prmP00UN3 = new Object[] {
          new Object[] {"@lV44ReferenciaTecnica_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV44ReferenciaTecnica_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV45Guia_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV45Guia_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49ReferenciaTecnica_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49ReferenciaTecnica_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50Guia_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50Guia_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54ReferenciaTecnica_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54ReferenciaTecnica_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55Guia_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55Guia_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV10TFReferenciaTecnica_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFReferenciaTecnica_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFReferenciaTecnica_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFReferenciaTecnica_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14TFReferenciaTecnica_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV15TFReferenciaTecnica_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV18TFReferenciaTecnica_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV19TFReferenciaTecnica_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV20TFGuia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFGuia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFGuia_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV23TFGuia_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00UN4 ;
          prmP00UN4 = new Object[] {
          new Object[] {"@lV44ReferenciaTecnica_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV44ReferenciaTecnica_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV45Guia_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV45Guia_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49ReferenciaTecnica_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49ReferenciaTecnica_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50Guia_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50Guia_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54ReferenciaTecnica_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54ReferenciaTecnica_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55Guia_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55Guia_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV10TFReferenciaTecnica_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFReferenciaTecnica_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFReferenciaTecnica_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFReferenciaTecnica_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14TFReferenciaTecnica_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV15TFReferenciaTecnica_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV18TFReferenciaTecnica_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV19TFReferenciaTecnica_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV20TFGuia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFGuia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFGuia_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV23TFGuia_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UN2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UN2,100,0,true,false )
             ,new CursorDef("P00UN3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UN3,100,0,true,false )
             ,new CursorDef("P00UN4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UN4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 50) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 50) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptreferenciatecnicafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptreferenciatecnicafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptreferenciatecnicafilterdata") )
          {
             return  ;
          }
          getpromptreferenciatecnicafilterdata worker = new getpromptreferenciatecnicafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
