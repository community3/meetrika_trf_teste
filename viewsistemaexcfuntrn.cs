/*
               File: ViewSistemaExcFunTrn
        Description: Confirmar Exclus�o Fun�oes de Transa��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:4:4.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class viewsistemaexcfuntrn : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public viewsistemaexcfuntrn( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public viewsistemaexcfuntrn( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Tipo ,
                           ref int aP1_FuncaoAPF_Codigo ,
                           ref int aP2_FuncaoAPF_SistemaCod )
      {
         this.AV10Tipo = aP0_Tipo;
         this.AV6FuncaoAPF_Codigo = aP1_FuncaoAPF_Codigo;
         this.AV7FuncaoAPF_SistemaCod = aP2_FuncaoAPF_SistemaCod;
         executePrivate();
         aP0_Tipo=this.AV10Tipo;
         aP1_FuncaoAPF_Codigo=this.AV6FuncaoAPF_Codigo;
         aP2_FuncaoAPF_SistemaCod=this.AV7FuncaoAPF_SistemaCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV10Tipo = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Tipo", AV10Tipo);
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV6FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6FuncaoAPF_Codigo), 6, 0)));
                  AV7FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_SistemaCod), 6, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAT12( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTT12( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042904426");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("viewsistemaexcfuntrn.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV10Tipo)) + "," + UrlEncode("" +AV6FuncaoAPF_Codigo) + "," + UrlEncode("" +AV7FuncaoAPF_SistemaCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vFUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMENSAGEMSISTEMA", AV15MensagemSistema);
         GxWebStd.gx_hidden_field( context, "vTIPO", StringUtil.RTrim( AV10Tipo));
         GxWebStd.gx_hidden_field( context, "gxhash_vTOTALREGSATRIBUTOS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11TotalRegsAtributos), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTOTALREGVINC", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12TotalRegVinc), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTOTREGSEVIDENCIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13TotRegsEvidencias), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ViewSistemaExcFunTrn";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV11TotalRegsAtributos), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV12TotalRegVinc), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV13TotRegsEvidencias), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("viewsistemaexcfuntrn:[SendSecurityCheck value for]"+"TotalRegsAtributos:"+context.localUtil.Format( (decimal)(AV11TotalRegsAtributos), "ZZZZZ9"));
         GXUtil.WriteLog("viewsistemaexcfuntrn:[SendSecurityCheck value for]"+"TotalRegVinc:"+context.localUtil.Format( (decimal)(AV12TotalRegVinc), "ZZZZZ9"));
         GXUtil.WriteLog("viewsistemaexcfuntrn:[SendSecurityCheck value for]"+"TotRegsEvidencias:"+context.localUtil.Format( (decimal)(AV13TotRegsEvidencias), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WET12( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTT12( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("viewsistemaexcfuntrn.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV10Tipo)) + "," + UrlEncode("" +AV6FuncaoAPF_Codigo) + "," + UrlEncode("" +AV7FuncaoAPF_SistemaCod) ;
      }

      public override String GetPgmname( )
      {
         return "ViewSistemaExcFunTrn" ;
      }

      public override String GetPgmdesc( )
      {
         return "Confirmar Exclus�o Fun�oes de Transa��o" ;
      }

      protected void WBT10( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_T12( true) ;
         }
         else
         {
            wb_table1_2_T12( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_T12e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTT12( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Confirmar Exclus�o Fun�oes de Transa��o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPT10( ) ;
      }

      protected void WST12( )
      {
         STARTT12( ) ;
         EVTT12( ) ;
      }

      protected void EVTT12( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11T12 */
                              E11T12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12T12 */
                              E12T12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCONFIRMAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13T12 */
                              E13T12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14T12 */
                              E14T12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WET12( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAT12( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavTotalregsatributos_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFT12( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavTotalregsatributos_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalregsatributos_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalregsatributos_Enabled), 5, 0)));
         edtavTotalregvinc_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalregvinc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalregvinc_Enabled), 5, 0)));
         edtavTotregsevidencias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotregsevidencias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotregsevidencias_Enabled), 5, 0)));
      }

      protected void RFT12( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E12T12 */
         E12T12 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E14T12 */
            E14T12 ();
            WBT10( ) ;
         }
      }

      protected void STRUPT10( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavTotalregsatributos_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalregsatributos_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalregsatributos_Enabled), 5, 0)));
         edtavTotalregvinc_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalregvinc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalregvinc_Enabled), 5, 0)));
         edtavTotregsevidencias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotregsevidencias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotregsevidencias_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11T12 */
         E11T12 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTotalregsatributos_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTotalregsatributos_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTOTALREGSATRIBUTOS");
               GX_FocusControl = edtavTotalregsatributos_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV11TotalRegsAtributos = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11TotalRegsAtributos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11TotalRegsAtributos), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTALREGSATRIBUTOS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11TotalRegsAtributos), "ZZZZZ9")));
            }
            else
            {
               AV11TotalRegsAtributos = (int)(context.localUtil.CToN( cgiGet( edtavTotalregsatributos_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11TotalRegsAtributos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11TotalRegsAtributos), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTALREGSATRIBUTOS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11TotalRegsAtributos), "ZZZZZ9")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTotalregvinc_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTotalregvinc_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTOTALREGVINC");
               GX_FocusControl = edtavTotalregvinc_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV12TotalRegVinc = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12TotalRegVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12TotalRegVinc), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTALREGVINC", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12TotalRegVinc), "ZZZZZ9")));
            }
            else
            {
               AV12TotalRegVinc = (int)(context.localUtil.CToN( cgiGet( edtavTotalregvinc_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12TotalRegVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12TotalRegVinc), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTALREGVINC", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12TotalRegVinc), "ZZZZZ9")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTotregsevidencias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTotregsevidencias_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTOTREGSEVIDENCIAS");
               GX_FocusControl = edtavTotregsevidencias_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13TotRegsEvidencias = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TotRegsEvidencias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13TotRegsEvidencias), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTREGSEVIDENCIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13TotRegsEvidencias), "ZZZZZ9")));
            }
            else
            {
               AV13TotRegsEvidencias = (int)(context.localUtil.CToN( cgiGet( edtavTotregsevidencias_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TotRegsEvidencias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13TotRegsEvidencias), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTREGSEVIDENCIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13TotRegsEvidencias), "ZZZZZ9")));
            }
            /* Read saved values. */
            Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
            Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
            Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
            Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
            Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
            Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
            Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
            Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
            Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
            Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "ViewSistemaExcFunTrn";
            AV11TotalRegsAtributos = (int)(context.localUtil.CToN( cgiGet( edtavTotalregsatributos_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11TotalRegsAtributos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11TotalRegsAtributos), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTALREGSATRIBUTOS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11TotalRegsAtributos), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV11TotalRegsAtributos), "ZZZZZ9");
            AV12TotalRegVinc = (int)(context.localUtil.CToN( cgiGet( edtavTotalregvinc_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12TotalRegVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12TotalRegVinc), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTALREGVINC", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12TotalRegVinc), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV12TotalRegVinc), "ZZZZZ9");
            AV13TotRegsEvidencias = (int)(context.localUtil.CToN( cgiGet( edtavTotregsevidencias_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TotRegsEvidencias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13TotRegsEvidencias), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTREGSEVIDENCIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13TotRegsEvidencias), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV13TotRegsEvidencias), "ZZZZZ9");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("viewsistemaexcfuntrn:[SecurityCheckFailed value for]"+"TotalRegsAtributos:"+context.localUtil.Format( (decimal)(AV11TotalRegsAtributos), "ZZZZZ9"));
               GXUtil.WriteLog("viewsistemaexcfuntrn:[SecurityCheckFailed value for]"+"TotalRegVinc:"+context.localUtil.Format( (decimal)(AV12TotalRegVinc), "ZZZZZ9"));
               GXUtil.WriteLog("viewsistemaexcfuntrn:[SecurityCheckFailed value for]"+"TotRegsEvidencias:"+context.localUtil.Format( (decimal)(AV13TotRegsEvidencias), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11T12 */
         E11T12 ();
         if (returnInSub) return;
      }

      protected void E11T12( )
      {
         /* Start Routine */
         AV13TotRegsEvidencias = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TotRegsEvidencias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13TotRegsEvidencias), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTREGSEVIDENCIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13TotRegsEvidencias), "ZZZZZ9")));
         AV11TotalRegsAtributos = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11TotalRegsAtributos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11TotalRegsAtributos), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTALREGSATRIBUTOS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11TotalRegsAtributos), "ZZZZZ9")));
         AV12TotalRegVinc = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12TotalRegVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12TotalRegVinc), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTALREGVINC", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12TotalRegVinc), "ZZZZZ9")));
         /* Optimized group. */
         /* Using cursor H00T12 */
         pr_default.execute(0, new Object[] {AV6FuncaoAPF_Codigo, AV7FuncaoAPF_SistemaCod});
         cV13TotRegsEvidencias = H00T12_AV13TotRegsEvidencias[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "cV13TotRegsEvidencias", StringUtil.LTrim( StringUtil.Str( (decimal)(cV13TotRegsEvidencias), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTREGSEVIDENCIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(cV13TotRegsEvidencias), "ZZZZZ9")));
         pr_default.close(0);
         AV13TotRegsEvidencias = (int)(AV13TotRegsEvidencias+cV13TotRegsEvidencias*1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TotRegsEvidencias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13TotRegsEvidencias), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTREGSEVIDENCIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13TotRegsEvidencias), "ZZZZZ9")));
         /* End optimized group. */
         /* Optimized group. */
         /* Using cursor H00T13 */
         pr_default.execute(1, new Object[] {AV6FuncaoAPF_Codigo, AV7FuncaoAPF_SistemaCod});
         cV11TotalRegsAtributos = H00T13_AV11TotalRegsAtributos[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "cV11TotalRegsAtributos", StringUtil.LTrim( StringUtil.Str( (decimal)(cV11TotalRegsAtributos), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTALREGSATRIBUTOS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(cV11TotalRegsAtributos), "ZZZZZ9")));
         pr_default.close(1);
         AV11TotalRegsAtributos = (int)(AV11TotalRegsAtributos+cV11TotalRegsAtributos*1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11TotalRegsAtributos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11TotalRegsAtributos), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTALREGSATRIBUTOS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11TotalRegsAtributos), "ZZZZZ9")));
         /* End optimized group. */
         /* Optimized group. */
         /* Using cursor H00T14 */
         pr_default.execute(2, new Object[] {AV6FuncaoAPF_Codigo, AV7FuncaoAPF_SistemaCod});
         cV12TotalRegVinc = H00T14_AV12TotalRegVinc[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "cV12TotalRegVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(cV12TotalRegVinc), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTALREGVINC", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(cV12TotalRegVinc), "ZZZZZ9")));
         pr_default.close(2);
         AV12TotalRegVinc = (int)(AV12TotalRegVinc+cV12TotalRegVinc*1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12TotalRegVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12TotalRegVinc), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTOTALREGVINC", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12TotalRegVinc), "ZZZZZ9")));
         /* End optimized group. */
      }

      protected void E12T12( )
      {
         /* Refresh Routine */
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S112 ();
         if (returnInSub) return;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV5Context) ;
      }

      protected void E13T12( )
      {
         /* 'DoConfirmar' Routine */
         AV10Tipo = "TRANSACOES";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Tipo", AV10Tipo);
         AV14Mensagem = "";
         new pr_sistemaexcfundadostrn(context ).execute( ref  AV6FuncaoAPF_Codigo, ref  AV7FuncaoAPF_SistemaCod, ref  AV10Tipo, ref  AV15MensagemSistema) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6FuncaoAPF_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_SistemaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Tipo", AV10Tipo);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15MensagemSistema", AV15MensagemSistema);
         GX_msglist.addItem(AV15MensagemSistema);
         context.setWebReturnParms(new Object[] {(String)AV10Tipo,(int)AV6FuncaoAPF_Codigo,(int)AV7FuncaoAPF_SistemaCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S112( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( ( ( AV13TotRegsEvidencias + AV11TotalRegsAtributos + AV12TotalRegVinc ) > 0 ) ) )
         {
            bttBtnconfirmar_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirmar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconfirmar_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'SHOW MESSAGES' Routine */
         AV21GXV1 = 1;
         while ( AV21GXV1 <= AV9Messages.Count )
         {
            AV8Message = ((SdtMessages_Message)AV9Messages.Item(AV21GXV1));
            GX_msglist.addItem(AV8Message.gxTpr_Description);
            AV21GXV1 = (int)(AV21GXV1+1);
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E14T12( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_T12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktitle_Internalname, "Verifique Abaixo os Dados a Excluir", "", "", lblTextblocktitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ViewSistemaExcFunTrn.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table2_8_T12( true) ;
         }
         else
         {
            wb_table2_8_T12( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_T12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayout_unnamedtable1_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtable1_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblMsgalerta_Internalname, "<strong>Aten��o</strong></br></br>Ap�s confirma��o n�o tem como reverter esta a��o.", "", "", lblMsgalerta_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWPTab", 0, "", 1, 1, 2, "HLP_ViewSistemaExcFunTrn.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_59_T12( true) ;
         }
         else
         {
            wb_table3_59_T12( false) ;
         }
         return  ;
      }

      protected void wb_table3_59_T12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_T12e( true) ;
         }
         else
         {
            wb_table1_2_T12e( false) ;
         }
      }

      protected void wb_table3_59_T12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirmar_Internalname, "", "Confirmar", bttBtnconfirmar_Jsonclick, 5, "Confirme Exclus�o dos Dados", "", StyleString, ClassString, 1, bttBtnconfirmar_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOCONFIRMAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ViewSistemaExcFunTrn.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ViewSistemaExcFunTrn.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_59_T12e( true) ;
         }
         else
         {
            wb_table3_59_T12e( false) ;
         }
      }

      protected void wb_table2_8_T12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_16_T12( true) ;
         }
         else
         {
            wb_table4_16_T12( false) ;
         }
         return  ;
      }

      protected void wb_table4_16_T12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_T12e( true) ;
         }
         else
         {
            wb_table2_8_T12e( false) ;
         }
      }

      protected void wb_table4_16_T12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td rowspan=\"3\" >") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayout_dados_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divDados_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 DataContentCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtabletotalregsatributos_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 MergeLabelCell", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktotalregsatributos_Internalname, "Total Regs. Atributos", "", "", lblTextblocktotalregsatributos_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_ViewSistemaExcFunTrn.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-4", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavTotalregsatributos_Internalname, "Total Regs Atributos", "col-sm-3 BootstrapAttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTotalregsatributos_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11TotalRegsAtributos), 6, 0, ",", "")), ((edtavTotalregsatributos_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV11TotalRegsAtributos), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV11TotalRegsAtributos), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTotalregsatributos_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavTotalregsatributos_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ViewSistemaExcFunTrn.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 DataContentCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtabletotalregvinc_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 MergeLabelCell", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktotalregvinc_Internalname, "Total Regs. Vinculados", "", "", lblTextblocktotalregvinc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_ViewSistemaExcFunTrn.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-4", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavTotalregvinc_Internalname, "Total Reg Vinc", "col-sm-3 BootstrapAttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTotalregvinc_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12TotalRegVinc), 6, 0, ",", "")), ((edtavTotalregvinc_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12TotalRegVinc), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV12TotalRegVinc), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTotalregvinc_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavTotalregvinc_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ViewSistemaExcFunTrn.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 DataContentCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtabletotregsevidencias_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 MergeLabelCell", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktotregsevidencias_Internalname, "Total Regs. Evid�ncias", "", "", lblTextblocktotregsevidencias_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_ViewSistemaExcFunTrn.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-4", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavTotregsevidencias_Internalname, "Tot Regs Evidencias", "col-sm-3 BootstrapAttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTotregsevidencias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13TotRegsEvidencias), 6, 0, ",", "")), ((edtavTotregsevidencias_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13TotRegsEvidencias), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV13TotRegsEvidencias), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTotregsevidencias_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavTotregsevidencias_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ViewSistemaExcFunTrn.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_T12e( true) ;
         }
         else
         {
            wb_table4_16_T12e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV10Tipo = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Tipo", AV10Tipo);
         AV6FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6FuncaoAPF_Codigo), 6, 0)));
         AV7FuncaoAPF_SistemaCod = Convert.ToInt32(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_SistemaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAT12( ) ;
         WST12( ) ;
         WET12( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204290455");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("viewsistemaexcfuntrn.js", "?20204290456");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocktitle_Internalname = "TEXTBLOCKTITLE";
         lblTextblocktotalregsatributos_Internalname = "TEXTBLOCKTOTALREGSATRIBUTOS";
         div_Internalname = "";
         edtavTotalregsatributos_Internalname = "vTOTALREGSATRIBUTOS";
         div_Internalname = "";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtabletotalregsatributos_Internalname = "UNNAMEDTABLETOTALREGSATRIBUTOS";
         div_Internalname = "";
         div_Internalname = "";
         lblTextblocktotalregvinc_Internalname = "TEXTBLOCKTOTALREGVINC";
         div_Internalname = "";
         edtavTotalregvinc_Internalname = "vTOTALREGVINC";
         div_Internalname = "";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtabletotalregvinc_Internalname = "UNNAMEDTABLETOTALREGVINC";
         div_Internalname = "";
         div_Internalname = "";
         lblTextblocktotregsevidencias_Internalname = "TEXTBLOCKTOTREGSEVIDENCIAS";
         div_Internalname = "";
         edtavTotregsevidencias_Internalname = "vTOTREGSEVIDENCIAS";
         div_Internalname = "";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtabletotregsevidencias_Internalname = "UNNAMEDTABLETOTREGSEVIDENCIAS";
         div_Internalname = "";
         div_Internalname = "";
         divDados_Internalname = "DADOS";
         div_Internalname = "";
         divLayout_dados_Internalname = "LAYOUT_DADOS";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         lblMsgalerta_Internalname = "MSGALERTA";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         div_Internalname = "";
         divLayout_unnamedtable1_Internalname = "LAYOUT_UNNAMEDTABLE1";
         bttBtnconfirmar_Internalname = "BTNCONFIRMAR";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavTotregsevidencias_Jsonclick = "";
         edtavTotregsevidencias_Enabled = 1;
         edtavTotalregvinc_Jsonclick = "";
         edtavTotalregvinc_Enabled = 1;
         edtavTotalregsatributos_Jsonclick = "";
         edtavTotalregsatributos_Enabled = 1;
         bttBtnconfirmar_Enabled = 1;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Registros";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Confirmar Exclus�o Fun�oes de Transa��o";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV13TotRegsEvidencias',fld:'vTOTREGSEVIDENCIAS',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV11TotalRegsAtributos',fld:'vTOTALREGSATRIBUTOS',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV12TotalRegVinc',fld:'vTOTALREGVINC',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{ctrl:'BTNCONFIRMAR',prop:'Enabled'}]}");
         setEventMetadata("'DOCONFIRMAR'","{handler:'E13T12',iparms:[{av:'AV6FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV15MensagemSistema',fld:'vMENSAGEMSISTEMA',pic:'',nv:''}],oparms:[{av:'AV10Tipo',fld:'vTIPO',pic:'',nv:''},{av:'AV15MensagemSistema',fld:'vMENSAGEMSISTEMA',pic:'',nv:''},{av:'AV7FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV6FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV10Tipo = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV15MensagemSistema = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         hsh = "";
         scmdbuf = "";
         H00T12_AV13TotRegsEvidencias = new int[1] ;
         H00T13_AV11TotalRegsAtributos = new int[1] ;
         H00T14_AV12TotalRegVinc = new int[1] ;
         AV5Context = new wwpbaseobjects.SdtWWPContext(context);
         AV14Mensagem = "";
         AV9Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV8Message = new SdtMessages_Message(context);
         sStyleString = "";
         lblTextblocktitle_Jsonclick = "";
         lblMsgalerta_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtnconfirmar_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblTextblocktotalregsatributos_Jsonclick = "";
         lblTextblocktotalregvinc_Jsonclick = "";
         lblTextblocktotregsevidencias_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.viewsistemaexcfuntrn__default(),
            new Object[][] {
                new Object[] {
               H00T12_AV13TotRegsEvidencias
               }
               , new Object[] {
               H00T13_AV11TotalRegsAtributos
               }
               , new Object[] {
               H00T14_AV12TotalRegVinc
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavTotalregsatributos_Enabled = 0;
         edtavTotalregvinc_Enabled = 0;
         edtavTotregsevidencias_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV6FuncaoAPF_Codigo ;
      private int AV7FuncaoAPF_SistemaCod ;
      private int wcpOAV6FuncaoAPF_Codigo ;
      private int wcpOAV7FuncaoAPF_SistemaCod ;
      private int AV11TotalRegsAtributos ;
      private int AV12TotalRegVinc ;
      private int AV13TotRegsEvidencias ;
      private int edtavTotalregsatributos_Enabled ;
      private int edtavTotalregvinc_Enabled ;
      private int edtavTotregsevidencias_Enabled ;
      private int cV13TotRegsEvidencias ;
      private int cV11TotalRegsAtributos ;
      private int cV12TotalRegVinc ;
      private int bttBtnconfirmar_Enabled ;
      private int AV21GXV1 ;
      private int idxLst ;
      private String AV10Tipo ;
      private String wcpOAV10Tipo ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavTotalregsatributos_Internalname ;
      private String edtavTotalregvinc_Internalname ;
      private String edtavTotregsevidencias_Internalname ;
      private String hsh ;
      private String scmdbuf ;
      private String bttBtnconfirmar_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTextblocktitle_Internalname ;
      private String lblTextblocktitle_Jsonclick ;
      private String divLayout_unnamedtable1_Internalname ;
      private String divUnnamedtable1_Internalname ;
      private String lblMsgalerta_Internalname ;
      private String lblMsgalerta_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnconfirmar_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String divLayout_dados_Internalname ;
      private String divDados_Internalname ;
      private String divUnnamedtabletotalregsatributos_Internalname ;
      private String lblTextblocktotalregsatributos_Internalname ;
      private String lblTextblocktotalregsatributos_Jsonclick ;
      private String edtavTotalregsatributos_Jsonclick ;
      private String divUnnamedtabletotalregvinc_Internalname ;
      private String lblTextblocktotalregvinc_Internalname ;
      private String lblTextblocktotalregvinc_Jsonclick ;
      private String edtavTotalregvinc_Jsonclick ;
      private String divUnnamedtabletotregsevidencias_Internalname ;
      private String lblTextblocktotregsevidencias_Internalname ;
      private String lblTextblocktotregsevidencias_Jsonclick ;
      private String edtavTotregsevidencias_Jsonclick ;
      private String div_Internalname ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String AV15MensagemSistema ;
      private String AV14Mensagem ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Tipo ;
      private int aP1_FuncaoAPF_Codigo ;
      private int aP2_FuncaoAPF_SistemaCod ;
      private IDataStoreProvider pr_default ;
      private int[] H00T12_AV13TotRegsEvidencias ;
      private int[] H00T13_AV11TotalRegsAtributos ;
      private int[] H00T14_AV12TotalRegVinc ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV9Messages ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV5Context ;
      private SdtMessages_Message AV8Message ;
   }

   public class viewsistemaexcfuntrn__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00T12 ;
          prmH00T12 = new Object[] {
          new Object[] {"@AV6FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7FuncaoAPF_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T13 ;
          prmH00T13 = new Object[] {
          new Object[] {"@AV6FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7FuncaoAPF_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T14 ;
          prmH00T14 = new Object[] {
          new Object[] {"@AV6FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7FuncaoAPF_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00T12", "SELECT COUNT(*) FROM ([FuncaoAPFEvidencia] T1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo]) WHERE (T1.[FuncaoAPF_Codigo] = @AV6FuncaoAPF_Codigo) AND (T2.[FuncaoAPF_SistemaCod] = @AV7FuncaoAPF_SistemaCod) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T12,1,0,true,false )
             ,new CursorDef("H00T13", "SELECT COUNT(*) FROM (([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) WHERE (T1.[FuncaoAPF_Codigo] = @AV6FuncaoAPF_Codigo) AND (T3.[Tabela_SistemaCod] = @AV7FuncaoAPF_SistemaCod) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T13,1,0,true,false )
             ,new CursorDef("H00T14", "SELECT COUNT(*) FROM [FuncoesAPF] WITH (NOLOCK) WHERE ([FuncaoAPF_Codigo] = @AV6FuncaoAPF_Codigo) AND ([FuncaoAPF_SistemaCod] = @AV7FuncaoAPF_SistemaCod) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T14,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
