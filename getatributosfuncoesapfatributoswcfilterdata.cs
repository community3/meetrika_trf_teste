/*
               File: GetAtributosFuncoesAPFAtributosWCFilterData
        Description: Get Atributos Funcoes APFAtributos WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:30.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getatributosfuncoesapfatributoswcfilterdata : GXProcedure
   {
      public getatributosfuncoesapfatributoswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getatributosfuncoesapfatributoswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getatributosfuncoesapfatributoswcfilterdata objgetatributosfuncoesapfatributoswcfilterdata;
         objgetatributosfuncoesapfatributoswcfilterdata = new getatributosfuncoesapfatributoswcfilterdata();
         objgetatributosfuncoesapfatributoswcfilterdata.AV18DDOName = aP0_DDOName;
         objgetatributosfuncoesapfatributoswcfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetatributosfuncoesapfatributoswcfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetatributosfuncoesapfatributoswcfilterdata.AV22OptionsJson = "" ;
         objgetatributosfuncoesapfatributoswcfilterdata.AV25OptionsDescJson = "" ;
         objgetatributosfuncoesapfatributoswcfilterdata.AV27OptionIndexesJson = "" ;
         objgetatributosfuncoesapfatributoswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetatributosfuncoesapfatributoswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetatributosfuncoesapfatributoswcfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getatributosfuncoesapfatributoswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_FUNCAOAPF_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPF_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("AtributosFuncoesAPFAtributosWCGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "AtributosFuncoesAPFAtributosWCGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("AtributosFuncoesAPFAtributosWCGridState"), "");
         }
         AV40GXV1 = 1;
         while ( AV40GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV40GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_NOME") == 0 )
            {
               AV10TFFuncaoAPF_Nome = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_NOME_SEL") == 0 )
            {
               AV11TFFuncaoAPF_Nome_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_COMPLEXIDADE_SEL") == 0 )
            {
               AV12TFFuncaoAPF_Complexidade_SelsJson = AV32GridStateFilterValue.gxTpr_Value;
               AV13TFFuncaoAPF_Complexidade_Sels.FromJSonString(AV12TFFuncaoAPF_Complexidade_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_PF") == 0 )
            {
               AV14TFFuncaoAPF_PF = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, ".");
               AV15TFFuncaoAPF_PF_To = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "PARM_&FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD") == 0 )
            {
               AV37FuncaoAPFAtributos_AtributosCod = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            AV40GXV1 = (int)(AV40GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV36FuncaoAPF_Nome1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAOAPF_NOMEOPTIONS' Routine */
         AV10TFFuncaoAPF_Nome = AV16SearchTxt;
         AV11TFFuncaoAPF_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A185FuncaoAPF_Complexidade ,
                                              AV13TFFuncaoAPF_Complexidade_Sels ,
                                              AV34DynamicFiltersSelector1 ,
                                              AV35DynamicFiltersOperator1 ,
                                              AV36FuncaoAPF_Nome1 ,
                                              AV11TFFuncaoAPF_Nome_Sel ,
                                              AV10TFFuncaoAPF_Nome ,
                                              A166FuncaoAPF_Nome ,
                                              AV13TFFuncaoAPF_Complexidade_Sels.Count ,
                                              AV14TFFuncaoAPF_PF ,
                                              A386FuncaoAPF_PF ,
                                              AV15TFFuncaoAPF_PF_To ,
                                              AV37FuncaoAPFAtributos_AtributosCod ,
                                              A364FuncaoAPFAtributos_AtributosCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV36FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV36FuncaoAPF_Nome1), "%", "");
         lV36FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV36FuncaoAPF_Nome1), "%", "");
         lV10TFFuncaoAPF_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFFuncaoAPF_Nome), "%", "");
         /* Using cursor P00UP2 */
         pr_default.execute(0, new Object[] {AV37FuncaoAPFAtributos_AtributosCod, AV13TFFuncaoAPF_Complexidade_Sels.Count, lV36FuncaoAPF_Nome1, lV36FuncaoAPF_Nome1, lV10TFFuncaoAPF_Nome, AV11TFFuncaoAPF_Nome_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUP2 = false;
            A364FuncaoAPFAtributos_AtributosCod = P00UP2_A364FuncaoAPFAtributos_AtributosCod[0];
            A166FuncaoAPF_Nome = P00UP2_A166FuncaoAPF_Nome[0];
            A165FuncaoAPF_Codigo = P00UP2_A165FuncaoAPF_Codigo[0];
            A166FuncaoAPF_Nome = P00UP2_A166FuncaoAPF_Nome[0];
            GXt_char1 = A185FuncaoAPF_Complexidade;
            new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char1) ;
            A185FuncaoAPF_Complexidade = GXt_char1;
            if ( ( AV13TFFuncaoAPF_Complexidade_Sels.Count <= 0 ) || ( (AV13TFFuncaoAPF_Complexidade_Sels.IndexOf(StringUtil.RTrim( A185FuncaoAPF_Complexidade))>0) ) )
            {
               GXt_decimal2 = A386FuncaoAPF_PF;
               new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal2) ;
               A386FuncaoAPF_PF = GXt_decimal2;
               if ( (Convert.ToDecimal(0)==AV14TFFuncaoAPF_PF) || ( ( A386FuncaoAPF_PF >= AV14TFFuncaoAPF_PF ) ) )
               {
                  if ( (Convert.ToDecimal(0)==AV15TFFuncaoAPF_PF_To) || ( ( A386FuncaoAPF_PF <= AV15TFFuncaoAPF_PF_To ) ) )
                  {
                     AV28count = 0;
                     while ( (pr_default.getStatus(0) != 101) && ( P00UP2_A364FuncaoAPFAtributos_AtributosCod[0] == A364FuncaoAPFAtributos_AtributosCod ) && ( P00UP2_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) )
                     {
                        BRKUP2 = false;
                        AV28count = (long)(AV28count+1);
                        BRKUP2 = true;
                        pr_default.readNext(0);
                     }
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A166FuncaoAPF_Nome)) )
                     {
                        AV20Option = A166FuncaoAPF_Nome;
                        AV19InsertIndex = 1;
                        while ( ( AV19InsertIndex <= AV21Options.Count ) && ( StringUtil.StrCmp(((String)AV21Options.Item(AV19InsertIndex)), AV20Option) < 0 ) )
                        {
                           AV19InsertIndex = (int)(AV19InsertIndex+1);
                        }
                        AV21Options.Add(AV20Option, AV19InsertIndex);
                        AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), AV19InsertIndex);
                     }
                     if ( AV21Options.Count == 50 )
                     {
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                     }
                  }
               }
            }
            if ( ! BRKUP2 )
            {
               BRKUP2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFFuncaoAPF_Nome = "";
         AV11TFFuncaoAPF_Nome_Sel = "";
         AV12TFFuncaoAPF_Complexidade_SelsJson = "";
         AV13TFFuncaoAPF_Complexidade_Sels = new GxSimpleCollection();
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV36FuncaoAPF_Nome1 = "";
         scmdbuf = "";
         lV36FuncaoAPF_Nome1 = "";
         lV10TFFuncaoAPF_Nome = "";
         A185FuncaoAPF_Complexidade = "";
         A166FuncaoAPF_Nome = "";
         P00UP2_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         P00UP2_A166FuncaoAPF_Nome = new String[] {""} ;
         P00UP2_A165FuncaoAPF_Codigo = new int[1] ;
         GXt_char1 = "";
         AV20Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getatributosfuncoesapfatributoswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UP2_A364FuncaoAPFAtributos_AtributosCod, P00UP2_A166FuncaoAPF_Nome, P00UP2_A165FuncaoAPF_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV35DynamicFiltersOperator1 ;
      private int AV40GXV1 ;
      private int AV37FuncaoAPFAtributos_AtributosCod ;
      private int AV13TFFuncaoAPF_Complexidade_Sels_Count ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int A165FuncaoAPF_Codigo ;
      private int AV19InsertIndex ;
      private long AV28count ;
      private decimal AV14TFFuncaoAPF_PF ;
      private decimal AV15TFFuncaoAPF_PF_To ;
      private decimal A386FuncaoAPF_PF ;
      private decimal GXt_decimal2 ;
      private String scmdbuf ;
      private String A185FuncaoAPF_Complexidade ;
      private String GXt_char1 ;
      private bool returnInSub ;
      private bool BRKUP2 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV12TFFuncaoAPF_Complexidade_SelsJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV10TFFuncaoAPF_Nome ;
      private String AV11TFFuncaoAPF_Nome_Sel ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV36FuncaoAPF_Nome1 ;
      private String lV36FuncaoAPF_Nome1 ;
      private String lV10TFFuncaoAPF_Nome ;
      private String A166FuncaoAPF_Nome ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00UP2_A364FuncaoAPFAtributos_AtributosCod ;
      private String[] P00UP2_A166FuncaoAPF_Nome ;
      private int[] P00UP2_A165FuncaoAPF_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFFuncaoAPF_Complexidade_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getatributosfuncoesapfatributoswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UP2( IGxContext context ,
                                             String A185FuncaoAPF_Complexidade ,
                                             IGxCollection AV13TFFuncaoAPF_Complexidade_Sels ,
                                             String AV34DynamicFiltersSelector1 ,
                                             short AV35DynamicFiltersOperator1 ,
                                             String AV36FuncaoAPF_Nome1 ,
                                             String AV11TFFuncaoAPF_Nome_Sel ,
                                             String AV10TFFuncaoAPF_Nome ,
                                             String A166FuncaoAPF_Nome ,
                                             int AV13TFFuncaoAPF_Complexidade_Sels_Count ,
                                             decimal AV14TFFuncaoAPF_PF ,
                                             decimal A386FuncaoAPF_PF ,
                                             decimal AV15TFFuncaoAPF_PF_To ,
                                             int AV37FuncaoAPFAtributos_AtributosCod ,
                                             int A364FuncaoAPFAtributos_AtributosCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [6] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoAPFAtributos_AtributosCod], T2.[FuncaoAPF_Nome], T1.[FuncaoAPF_Codigo] FROM ([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[FuncaoAPFAtributos_AtributosCod] = @AV37FuncaoAPFAtributos_AtributosCod)";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36FuncaoAPF_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV36FuncaoAPF_Nome1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36FuncaoAPF_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like '%' + @lV36FuncaoAPF_Nome1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPF_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncaoAPF_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV10TFFuncaoAPF_Nome)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPF_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] = @AV11TFFuncaoAPF_Nome_Sel)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPFAtributos_AtributosCod], T1.[FuncaoAPF_Codigo]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UP2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (decimal)dynConstraints[9] , (decimal)dynConstraints[10] , (decimal)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UP2 ;
          prmP00UP2 = new Object[] {
          new Object[] {"@AV37FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFFuCount",SqlDbType.Int,9,0} ,
          new Object[] {"@lV36FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV36FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV10TFFuncaoAPF_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV11TFFuncaoAPF_Nome_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UP2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UP2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getatributosfuncoesapfatributoswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getatributosfuncoesapfatributoswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getatributosfuncoesapfatributoswcfilterdata") )
          {
             return  ;
          }
          getatributosfuncoesapfatributoswcfilterdata worker = new getatributosfuncoesapfatributoswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
