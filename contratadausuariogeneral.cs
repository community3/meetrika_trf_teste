/*
               File: ContratadaUsuarioGeneral
        Description: Contratada Usuario General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:10:17.25
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratadausuariogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratadausuariogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratadausuariogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratadaUsuario_ContratadaCod ,
                           int aP1_ContratadaUsuario_UsuarioCod )
      {
         this.A66ContratadaUsuario_ContratadaCod = aP0_ContratadaUsuario_ContratadaCod;
         this.A69ContratadaUsuario_UsuarioCod = aP1_ContratadaUsuario_UsuarioCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A66ContratadaUsuario_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
                  A69ContratadaUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A66ContratadaUsuario_ContratadaCod,(int)A69ContratadaUsuario_UsuarioCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA8W2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "ContratadaUsuarioGeneral";
               context.Gx_err = 0;
               /* Using cursor H008W2 */
               pr_default.execute(0, new Object[] {A66ContratadaUsuario_ContratadaCod});
               A67ContratadaUsuario_ContratadaPessoaCod = H008W2_A67ContratadaUsuario_ContratadaPessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A67ContratadaUsuario_ContratadaPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), 6, 0)));
               n67ContratadaUsuario_ContratadaPessoaCod = H008W2_n67ContratadaUsuario_ContratadaPessoaCod[0];
               pr_default.close(0);
               /* Using cursor H008W3 */
               pr_default.execute(1, new Object[] {n67ContratadaUsuario_ContratadaPessoaCod, A67ContratadaUsuario_ContratadaPessoaCod});
               A68ContratadaUsuario_ContratadaPessoaNom = H008W3_A68ContratadaUsuario_ContratadaPessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A68ContratadaUsuario_ContratadaPessoaNom", A68ContratadaUsuario_ContratadaPessoaNom);
               n68ContratadaUsuario_ContratadaPessoaNom = H008W3_n68ContratadaUsuario_ContratadaPessoaNom[0];
               pr_default.close(1);
               /* Using cursor H008W4 */
               pr_default.execute(2, new Object[] {A69ContratadaUsuario_UsuarioCod});
               A70ContratadaUsuario_UsuarioPessoaCod = H008W4_A70ContratadaUsuario_UsuarioPessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A70ContratadaUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0)));
               n70ContratadaUsuario_UsuarioPessoaCod = H008W4_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A2Usuario_Nome = H008W4_A2Usuario_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2Usuario_Nome", A2Usuario_Nome);
               n2Usuario_Nome = H008W4_n2Usuario_Nome[0];
               pr_default.close(2);
               /* Using cursor H008W5 */
               pr_default.execute(3, new Object[] {n70ContratadaUsuario_UsuarioPessoaCod, A70ContratadaUsuario_UsuarioPessoaCod});
               A71ContratadaUsuario_UsuarioPessoaNom = H008W5_A71ContratadaUsuario_UsuarioPessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A71ContratadaUsuario_UsuarioPessoaNom", A71ContratadaUsuario_UsuarioPessoaNom);
               n71ContratadaUsuario_UsuarioPessoaNom = H008W5_n71ContratadaUsuario_UsuarioPessoaNom[0];
               pr_default.close(3);
               WS8W2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contratada Usuario General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202052118101733");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratadausuariogeneral.aspx") + "?" + UrlEncode("" +A66ContratadaUsuario_ContratadaCod) + "," + UrlEncode("" +A69ContratadaUsuario_UsuarioCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm8W2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratadausuariogeneral.js", "?202052118101735");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratadaUsuarioGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contratada Usuario General" ;
      }

      protected void WB8W0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratadausuariogeneral.aspx");
            }
            wb_table1_2_8W2( true) ;
         }
         else
         {
            wb_table1_2_8W2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_8W2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratadaUsuario_ContratadaPessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratadaUsuario_ContratadaPessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtContratadaUsuario_ContratadaPessoaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratadaUsuarioGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratadaUsuario_UsuarioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A69ContratadaUsuario_UsuarioCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratadaUsuario_UsuarioCod_Jsonclick, 0, "Attribute", "", "", "", edtContratadaUsuario_UsuarioCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratadaUsuarioGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratadaUsuario_UsuarioPessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratadaUsuario_UsuarioPessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtContratadaUsuario_UsuarioPessoaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratadaUsuarioGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START8W2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contratada Usuario General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP8W0( ) ;
            }
         }
      }

      protected void WS8W2( )
      {
         START8W2( ) ;
         EVT8W2( ) ;
      }

      protected void EVT8W2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8W0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8W0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E118W2 */
                                    E118W2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8W0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E128W2 */
                                    E128W2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8W0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E138W2 */
                                    E138W2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8W0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E148W2 */
                                    E148W2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8W0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8W0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE8W2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm8W2( ) ;
            }
         }
      }

      protected void PA8W2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF8W2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "ContratadaUsuarioGeneral";
         context.Gx_err = 0;
      }

      protected void RF8W2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H008W6 */
            pr_default.execute(4, new Object[] {A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
            while ( (pr_default.getStatus(4) != 101) )
            {
               /* Execute user event: E128W2 */
               E128W2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(4);
            WB8W0( ) ;
         }
      }

      protected void STRUP8W0( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "ContratadaUsuarioGeneral";
         context.Gx_err = 0;
         /* Using cursor H008W7 */
         pr_default.execute(5, new Object[] {A66ContratadaUsuario_ContratadaCod});
         A67ContratadaUsuario_ContratadaPessoaCod = H008W7_A67ContratadaUsuario_ContratadaPessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A67ContratadaUsuario_ContratadaPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), 6, 0)));
         n67ContratadaUsuario_ContratadaPessoaCod = H008W7_n67ContratadaUsuario_ContratadaPessoaCod[0];
         pr_default.close(5);
         /* Using cursor H008W8 */
         pr_default.execute(6, new Object[] {n67ContratadaUsuario_ContratadaPessoaCod, A67ContratadaUsuario_ContratadaPessoaCod});
         A68ContratadaUsuario_ContratadaPessoaNom = H008W8_A68ContratadaUsuario_ContratadaPessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A68ContratadaUsuario_ContratadaPessoaNom", A68ContratadaUsuario_ContratadaPessoaNom);
         n68ContratadaUsuario_ContratadaPessoaNom = H008W8_n68ContratadaUsuario_ContratadaPessoaNom[0];
         pr_default.close(6);
         /* Using cursor H008W9 */
         pr_default.execute(7, new Object[] {A69ContratadaUsuario_UsuarioCod});
         A70ContratadaUsuario_UsuarioPessoaCod = H008W9_A70ContratadaUsuario_UsuarioPessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A70ContratadaUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0)));
         n70ContratadaUsuario_UsuarioPessoaCod = H008W9_n70ContratadaUsuario_UsuarioPessoaCod[0];
         A2Usuario_Nome = H008W9_A2Usuario_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2Usuario_Nome", A2Usuario_Nome);
         n2Usuario_Nome = H008W9_n2Usuario_Nome[0];
         pr_default.close(7);
         /* Using cursor H008W10 */
         pr_default.execute(8, new Object[] {n70ContratadaUsuario_UsuarioPessoaCod, A70ContratadaUsuario_UsuarioPessoaCod});
         A71ContratadaUsuario_UsuarioPessoaNom = H008W10_A71ContratadaUsuario_UsuarioPessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A71ContratadaUsuario_UsuarioPessoaNom", A71ContratadaUsuario_UsuarioPessoaNom);
         n71ContratadaUsuario_UsuarioPessoaNom = H008W10_n71ContratadaUsuario_UsuarioPessoaNom[0];
         pr_default.close(8);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E118W2 */
         E118W2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A2Usuario_Nome = StringUtil.Upper( cgiGet( edtUsuario_Nome_Internalname));
            n2Usuario_Nome = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2Usuario_Nome", A2Usuario_Nome);
            A71ContratadaUsuario_UsuarioPessoaNom = StringUtil.Upper( cgiGet( edtContratadaUsuario_UsuarioPessoaNom_Internalname));
            n71ContratadaUsuario_UsuarioPessoaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A71ContratadaUsuario_UsuarioPessoaNom", A71ContratadaUsuario_UsuarioPessoaNom);
            A68ContratadaUsuario_ContratadaPessoaNom = StringUtil.Upper( cgiGet( edtContratadaUsuario_ContratadaPessoaNom_Internalname));
            n68ContratadaUsuario_ContratadaPessoaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A68ContratadaUsuario_ContratadaPessoaNom", A68ContratadaUsuario_ContratadaPessoaNom);
            A67ContratadaUsuario_ContratadaPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratadaUsuario_ContratadaPessoaCod_Internalname), ",", "."));
            n67ContratadaUsuario_ContratadaPessoaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A67ContratadaUsuario_ContratadaPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), 6, 0)));
            A70ContratadaUsuario_UsuarioPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratadaUsuario_UsuarioPessoaCod_Internalname), ",", "."));
            n70ContratadaUsuario_UsuarioPessoaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A70ContratadaUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0)));
            /* Read saved values. */
            wcpOA66ContratadaUsuario_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA66ContratadaUsuario_ContratadaCod"), ",", "."));
            wcpOA69ContratadaUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA69ContratadaUsuario_UsuarioCod"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E118W2 */
         E118W2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E118W2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E128W2( )
      {
         /* Load Routine */
         edtUsuario_Nome_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A69ContratadaUsuario_UsuarioCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtUsuario_Nome_Internalname, "Link", edtUsuario_Nome_Link);
         edtContratadaUsuario_UsuarioPessoaNom_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A69ContratadaUsuario_UsuarioCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratadaUsuario_UsuarioPessoaNom_Internalname, "Link", edtContratadaUsuario_UsuarioPessoaNom_Link);
         edtContratadaUsuario_ContratadaPessoaNom_Link = formatLink("viewcontratada.aspx") + "?" + UrlEncode("" +A66ContratadaUsuario_ContratadaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratadaUsuario_ContratadaPessoaNom_Internalname, "Link", edtContratadaUsuario_ContratadaPessoaNom_Link);
         edtContratadaUsuario_ContratadaPessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratadaUsuario_ContratadaPessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratadaUsuario_ContratadaPessoaCod_Visible), 5, 0)));
         edtContratadaUsuario_UsuarioCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratadaUsuario_UsuarioCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratadaUsuario_UsuarioCod_Visible), 5, 0)));
         edtContratadaUsuario_UsuarioPessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratadaUsuario_UsuarioPessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratadaUsuario_UsuarioPessoaCod_Visible), 5, 0)));
      }

      protected void E138W2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contratadausuario.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A66ContratadaUsuario_ContratadaCod) + "," + UrlEncode("" +A69ContratadaUsuario_UsuarioCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E148W2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contratadausuario.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A66ContratadaUsuario_ContratadaCod) + "," + UrlEncode("" +A69ContratadaUsuario_UsuarioCod);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = false;
         AV9TrnContext.gxTpr_Callerurl = AV12HTTPRequest.ScriptName+"?"+AV12HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContratadaUsuario";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratadaUsuario_ContratadaCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratadaUsuario_ContratadaCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratadaUsuario_UsuarioCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV8ContratadaUsuario_UsuarioCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV11Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_8W2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_8W2( true) ;
         }
         else
         {
            wb_table2_8_8W2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_8W2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_26_8W2( true) ;
         }
         else
         {
            wb_table3_26_8W2( false) ;
         }
         return  ;
      }

      protected void wb_table3_26_8W2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_8W2e( true) ;
         }
         else
         {
            wb_table1_2_8W2e( false) ;
         }
      }

      protected void wb_table3_26_8W2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratadaUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratadaUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_26_8W2e( true) ;
         }
         else
         {
            wb_table3_26_8W2e( false) ;
         }
      }

      protected void wb_table2_8_8W2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_nome_Internalname, "Usu�rio", "", "", lblTextblockusuario_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_Nome_Internalname, StringUtil.RTrim( A2Usuario_Nome), StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtUsuario_Nome_Link, "", "", "", edtUsuario_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratadaUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratadausuario_usuariopessoanom_Internalname, "Pessoa", "", "", lblTextblockcontratadausuario_usuariopessoanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratadaUsuario_UsuarioPessoaNom_Internalname, StringUtil.RTrim( A71ContratadaUsuario_UsuarioPessoaNom), StringUtil.RTrim( context.localUtil.Format( A71ContratadaUsuario_UsuarioPessoaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContratadaUsuario_UsuarioPessoaNom_Link, "", "", "", edtContratadaUsuario_UsuarioPessoaNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratadaUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratadausuario_contratadapessoanom_Internalname, "Contratada", "", "", lblTextblockcontratadausuario_contratadapessoanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratadaUsuario_ContratadaPessoaNom_Internalname, StringUtil.RTrim( A68ContratadaUsuario_ContratadaPessoaNom), StringUtil.RTrim( context.localUtil.Format( A68ContratadaUsuario_ContratadaPessoaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContratadaUsuario_ContratadaPessoaNom_Link, "", "", "", edtContratadaUsuario_ContratadaPessoaNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratadaUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_8W2e( true) ;
         }
         else
         {
            wb_table2_8_8W2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A66ContratadaUsuario_ContratadaCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
         A69ContratadaUsuario_UsuarioCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA8W2( ) ;
         WS8W2( ) ;
         WE8W2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA66ContratadaUsuario_ContratadaCod = (String)((String)getParm(obj,0));
         sCtrlA69ContratadaUsuario_UsuarioCod = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA8W2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratadausuariogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA8W2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A66ContratadaUsuario_ContratadaCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
            A69ContratadaUsuario_UsuarioCod = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
         }
         wcpOA66ContratadaUsuario_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA66ContratadaUsuario_ContratadaCod"), ",", "."));
         wcpOA69ContratadaUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA69ContratadaUsuario_UsuarioCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A66ContratadaUsuario_ContratadaCod != wcpOA66ContratadaUsuario_ContratadaCod ) || ( A69ContratadaUsuario_UsuarioCod != wcpOA69ContratadaUsuario_UsuarioCod ) ) )
         {
            setjustcreated();
         }
         wcpOA66ContratadaUsuario_ContratadaCod = A66ContratadaUsuario_ContratadaCod;
         wcpOA69ContratadaUsuario_UsuarioCod = A69ContratadaUsuario_UsuarioCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA66ContratadaUsuario_ContratadaCod = cgiGet( sPrefix+"A66ContratadaUsuario_ContratadaCod_CTRL");
         if ( StringUtil.Len( sCtrlA66ContratadaUsuario_ContratadaCod) > 0 )
         {
            A66ContratadaUsuario_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA66ContratadaUsuario_ContratadaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
         }
         else
         {
            A66ContratadaUsuario_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A66ContratadaUsuario_ContratadaCod_PARM"), ",", "."));
         }
         sCtrlA69ContratadaUsuario_UsuarioCod = cgiGet( sPrefix+"A69ContratadaUsuario_UsuarioCod_CTRL");
         if ( StringUtil.Len( sCtrlA69ContratadaUsuario_UsuarioCod) > 0 )
         {
            A69ContratadaUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA69ContratadaUsuario_UsuarioCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
         }
         else
         {
            A69ContratadaUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A69ContratadaUsuario_UsuarioCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA8W2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS8W2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS8W2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A66ContratadaUsuario_ContratadaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA66ContratadaUsuario_ContratadaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A66ContratadaUsuario_ContratadaCod_CTRL", StringUtil.RTrim( sCtrlA66ContratadaUsuario_ContratadaCod));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A69ContratadaUsuario_UsuarioCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA69ContratadaUsuario_UsuarioCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A69ContratadaUsuario_UsuarioCod_CTRL", StringUtil.RTrim( sCtrlA69ContratadaUsuario_UsuarioCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE8W2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202052118101762");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratadausuariogeneral.js", "?202052118101763");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockusuario_nome_Internalname = sPrefix+"TEXTBLOCKUSUARIO_NOME";
         edtUsuario_Nome_Internalname = sPrefix+"USUARIO_NOME";
         lblTextblockcontratadausuario_usuariopessoanom_Internalname = sPrefix+"TEXTBLOCKCONTRATADAUSUARIO_USUARIOPESSOANOM";
         edtContratadaUsuario_UsuarioPessoaNom_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOPESSOANOM";
         lblTextblockcontratadausuario_contratadapessoanom_Internalname = sPrefix+"TEXTBLOCKCONTRATADAUSUARIO_CONTRATADAPESSOANOM";
         edtContratadaUsuario_ContratadaPessoaNom_Internalname = sPrefix+"CONTRATADAUSUARIO_CONTRATADAPESSOANOM";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContratadaUsuario_ContratadaPessoaCod_Internalname = sPrefix+"CONTRATADAUSUARIO_CONTRATADAPESSOACOD";
         edtContratadaUsuario_UsuarioCod_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOCOD";
         edtContratadaUsuario_UsuarioPessoaCod_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOPESSOACOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratadaUsuario_ContratadaPessoaNom_Jsonclick = "";
         edtContratadaUsuario_UsuarioPessoaNom_Jsonclick = "";
         edtUsuario_Nome_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtContratadaUsuario_ContratadaPessoaNom_Link = "";
         edtContratadaUsuario_UsuarioPessoaNom_Link = "";
         edtUsuario_Nome_Link = "";
         edtContratadaUsuario_UsuarioPessoaCod_Jsonclick = "";
         edtContratadaUsuario_UsuarioPessoaCod_Visible = 1;
         edtContratadaUsuario_UsuarioCod_Jsonclick = "";
         edtContratadaUsuario_UsuarioCod_Visible = 1;
         edtContratadaUsuario_ContratadaPessoaCod_Jsonclick = "";
         edtContratadaUsuario_ContratadaPessoaCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E138W2',iparms:[{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E148W2',iparms:[{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         scmdbuf = "";
         H008W2_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         H008W2_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         H008W3_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         H008W3_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         A68ContratadaUsuario_ContratadaPessoaNom = "";
         H008W4_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H008W4_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H008W4_A2Usuario_Nome = new String[] {""} ;
         H008W4_n2Usuario_Nome = new bool[] {false} ;
         A2Usuario_Nome = "";
         H008W5_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H008W5_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         H008W6_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H008W6_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H008W6_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H008W6_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H008W6_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         H008W6_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         H008W6_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         H008W6_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         H008W6_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H008W6_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H008W6_A2Usuario_Nome = new String[] {""} ;
         H008W6_n2Usuario_Nome = new bool[] {false} ;
         H008W7_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         H008W7_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         H008W8_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         H008W8_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         H008W9_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H008W9_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H008W9_A2Usuario_Nome = new String[] {""} ;
         H008W9_n2Usuario_Nome = new bool[] {false} ;
         H008W10_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H008W10_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV12HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV11Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockusuario_nome_Jsonclick = "";
         lblTextblockcontratadausuario_usuariopessoanom_Jsonclick = "";
         lblTextblockcontratadausuario_contratadapessoanom_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA66ContratadaUsuario_ContratadaCod = "";
         sCtrlA69ContratadaUsuario_UsuarioCod = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratadausuariogeneral__default(),
            new Object[][] {
                new Object[] {
               H008W2_A67ContratadaUsuario_ContratadaPessoaCod, H008W2_n67ContratadaUsuario_ContratadaPessoaCod
               }
               , new Object[] {
               H008W3_A68ContratadaUsuario_ContratadaPessoaNom, H008W3_n68ContratadaUsuario_ContratadaPessoaNom
               }
               , new Object[] {
               H008W4_A70ContratadaUsuario_UsuarioPessoaCod, H008W4_n70ContratadaUsuario_UsuarioPessoaCod, H008W4_A2Usuario_Nome, H008W4_n2Usuario_Nome
               }
               , new Object[] {
               H008W5_A71ContratadaUsuario_UsuarioPessoaNom, H008W5_n71ContratadaUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H008W6_A66ContratadaUsuario_ContratadaCod, H008W6_A69ContratadaUsuario_UsuarioCod, H008W6_A70ContratadaUsuario_UsuarioPessoaCod, H008W6_n70ContratadaUsuario_UsuarioPessoaCod, H008W6_A67ContratadaUsuario_ContratadaPessoaCod, H008W6_n67ContratadaUsuario_ContratadaPessoaCod, H008W6_A68ContratadaUsuario_ContratadaPessoaNom, H008W6_n68ContratadaUsuario_ContratadaPessoaNom, H008W6_A71ContratadaUsuario_UsuarioPessoaNom, H008W6_n71ContratadaUsuario_UsuarioPessoaNom,
               H008W6_A2Usuario_Nome, H008W6_n2Usuario_Nome
               }
               , new Object[] {
               H008W7_A67ContratadaUsuario_ContratadaPessoaCod, H008W7_n67ContratadaUsuario_ContratadaPessoaCod
               }
               , new Object[] {
               H008W8_A68ContratadaUsuario_ContratadaPessoaNom, H008W8_n68ContratadaUsuario_ContratadaPessoaNom
               }
               , new Object[] {
               H008W9_A70ContratadaUsuario_UsuarioPessoaCod, H008W9_n70ContratadaUsuario_UsuarioPessoaCod, H008W9_A2Usuario_Nome, H008W9_n2Usuario_Nome
               }
               , new Object[] {
               H008W10_A71ContratadaUsuario_UsuarioPessoaNom, H008W10_n71ContratadaUsuario_UsuarioPessoaNom
               }
            }
         );
         AV15Pgmname = "ContratadaUsuarioGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "ContratadaUsuarioGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int wcpOA66ContratadaUsuario_ContratadaCod ;
      private int wcpOA69ContratadaUsuario_UsuarioCod ;
      private int A67ContratadaUsuario_ContratadaPessoaCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int edtContratadaUsuario_ContratadaPessoaCod_Visible ;
      private int edtContratadaUsuario_UsuarioCod_Visible ;
      private int edtContratadaUsuario_UsuarioPessoaCod_Visible ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7ContratadaUsuario_ContratadaCod ;
      private int AV8ContratadaUsuario_UsuarioCod ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String scmdbuf ;
      private String A68ContratadaUsuario_ContratadaPessoaNom ;
      private String A2Usuario_Nome ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtContratadaUsuario_ContratadaPessoaCod_Internalname ;
      private String edtContratadaUsuario_ContratadaPessoaCod_Jsonclick ;
      private String edtContratadaUsuario_UsuarioCod_Internalname ;
      private String edtContratadaUsuario_UsuarioCod_Jsonclick ;
      private String edtContratadaUsuario_UsuarioPessoaCod_Internalname ;
      private String edtContratadaUsuario_UsuarioPessoaCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtUsuario_Nome_Internalname ;
      private String edtContratadaUsuario_UsuarioPessoaNom_Internalname ;
      private String edtContratadaUsuario_ContratadaPessoaNom_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String edtUsuario_Nome_Link ;
      private String edtContratadaUsuario_UsuarioPessoaNom_Link ;
      private String edtContratadaUsuario_ContratadaPessoaNom_Link ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockusuario_nome_Internalname ;
      private String lblTextblockusuario_nome_Jsonclick ;
      private String edtUsuario_Nome_Jsonclick ;
      private String lblTextblockcontratadausuario_usuariopessoanom_Internalname ;
      private String lblTextblockcontratadausuario_usuariopessoanom_Jsonclick ;
      private String edtContratadaUsuario_UsuarioPessoaNom_Jsonclick ;
      private String lblTextblockcontratadausuario_contratadapessoanom_Internalname ;
      private String lblTextblockcontratadausuario_contratadapessoanom_Jsonclick ;
      private String edtContratadaUsuario_ContratadaPessoaNom_Jsonclick ;
      private String sCtrlA66ContratadaUsuario_ContratadaCod ;
      private String sCtrlA69ContratadaUsuario_UsuarioCod ;
      private bool entryPointCalled ;
      private bool n67ContratadaUsuario_ContratadaPessoaCod ;
      private bool n68ContratadaUsuario_ContratadaPessoaNom ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n2Usuario_Nome ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H008W2_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] H008W2_n67ContratadaUsuario_ContratadaPessoaCod ;
      private String[] H008W3_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] H008W3_n68ContratadaUsuario_ContratadaPessoaNom ;
      private int[] H008W4_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H008W4_n70ContratadaUsuario_UsuarioPessoaCod ;
      private String[] H008W4_A2Usuario_Nome ;
      private bool[] H008W4_n2Usuario_Nome ;
      private String[] H008W5_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H008W5_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H008W6_A66ContratadaUsuario_ContratadaCod ;
      private int[] H008W6_A69ContratadaUsuario_UsuarioCod ;
      private int[] H008W6_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H008W6_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H008W6_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] H008W6_n67ContratadaUsuario_ContratadaPessoaCod ;
      private String[] H008W6_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] H008W6_n68ContratadaUsuario_ContratadaPessoaNom ;
      private String[] H008W6_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H008W6_n71ContratadaUsuario_UsuarioPessoaNom ;
      private String[] H008W6_A2Usuario_Nome ;
      private bool[] H008W6_n2Usuario_Nome ;
      private int[] H008W7_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] H008W7_n67ContratadaUsuario_ContratadaPessoaCod ;
      private String[] H008W8_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] H008W8_n68ContratadaUsuario_ContratadaPessoaNom ;
      private int[] H008W9_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H008W9_n70ContratadaUsuario_UsuarioPessoaCod ;
      private String[] H008W9_A2Usuario_Nome ;
      private bool[] H008W9_n2Usuario_Nome ;
      private String[] H008W10_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H008W10_n71ContratadaUsuario_UsuarioPessoaNom ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV12HTTPRequest ;
      private IGxSession AV11Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
   }

   public class contratadausuariogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH008W2 ;
          prmH008W2 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008W3 ;
          prmH008W3 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008W4 ;
          prmH008W4 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008W5 ;
          prmH008W5 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008W6 ;
          prmH008W6 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008W7 ;
          prmH008W7 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008W8 ;
          prmH008W8 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008W9 ;
          prmH008W9 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008W10 ;
          prmH008W10 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H008W2", "SELECT [Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratadaUsuario_ContratadaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008W2,1,0,true,true )
             ,new CursorDef("H008W3", "SELECT [Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratadaUsuario_ContratadaPessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008W3,1,0,true,true )
             ,new CursorDef("H008W4", "SELECT [Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, [Usuario_Nome] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratadaUsuario_UsuarioCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008W4,1,0,true,true )
             ,new CursorDef("H008W5", "SELECT [Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratadaUsuario_UsuarioPessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008W5,1,0,true,true )
             ,new CursorDef("H008W6", "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T3.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom, T5.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T4.[Usuario_Nome] FROM (((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) WHERE T1.[ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod and T1.[ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008W6,1,0,true,true )
             ,new CursorDef("H008W7", "SELECT [Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratadaUsuario_ContratadaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008W7,1,0,true,true )
             ,new CursorDef("H008W8", "SELECT [Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratadaUsuario_ContratadaPessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008W8,1,0,true,true )
             ,new CursorDef("H008W9", "SELECT [Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, [Usuario_Nome] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratadaUsuario_UsuarioCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008W9,1,0,true,true )
             ,new CursorDef("H008W10", "SELECT [Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratadaUsuario_UsuarioPessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008W10,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
