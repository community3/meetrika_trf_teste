/*
               File: REL_RelatorioGerencialContagemPDF
        Description: Stub for REL_RelatorioGerencialContagemPDF
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:45:16.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_relatoriogerencialcontagempdf : GXProcedure
   {
      public rel_relatoriogerencialcontagempdf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public rel_relatoriogerencialcontagempdf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( DateTime aP0_ContagemResultado_DataDmnIn ,
                           DateTime aP1_ContagemResultado_DataDmnFim ,
                           int aP2_ContagemResultado_ContratadaOrigemCod ,
                           int aP3_ContagemResultado_CntSrvCod ,
                           String aP4_ContagemResultado_StatusDmn )
      {
         this.AV2ContagemResultado_DataDmnIn = aP0_ContagemResultado_DataDmnIn;
         this.AV3ContagemResultado_DataDmnFim = aP1_ContagemResultado_DataDmnFim;
         this.AV4ContagemResultado_ContratadaOrigemCod = aP2_ContagemResultado_ContratadaOrigemCod;
         this.AV5ContagemResultado_CntSrvCod = aP3_ContagemResultado_CntSrvCod;
         this.AV6ContagemResultado_StatusDmn = aP4_ContagemResultado_StatusDmn;
         initialize();
         executePrivate();
      }

      public void executeSubmit( DateTime aP0_ContagemResultado_DataDmnIn ,
                                 DateTime aP1_ContagemResultado_DataDmnFim ,
                                 int aP2_ContagemResultado_ContratadaOrigemCod ,
                                 int aP3_ContagemResultado_CntSrvCod ,
                                 String aP4_ContagemResultado_StatusDmn )
      {
         rel_relatoriogerencialcontagempdf objrel_relatoriogerencialcontagempdf;
         objrel_relatoriogerencialcontagempdf = new rel_relatoriogerencialcontagempdf();
         objrel_relatoriogerencialcontagempdf.AV2ContagemResultado_DataDmnIn = aP0_ContagemResultado_DataDmnIn;
         objrel_relatoriogerencialcontagempdf.AV3ContagemResultado_DataDmnFim = aP1_ContagemResultado_DataDmnFim;
         objrel_relatoriogerencialcontagempdf.AV4ContagemResultado_ContratadaOrigemCod = aP2_ContagemResultado_ContratadaOrigemCod;
         objrel_relatoriogerencialcontagempdf.AV5ContagemResultado_CntSrvCod = aP3_ContagemResultado_CntSrvCod;
         objrel_relatoriogerencialcontagempdf.AV6ContagemResultado_StatusDmn = aP4_ContagemResultado_StatusDmn;
         objrel_relatoriogerencialcontagempdf.context.SetSubmitInitialConfig(context);
         objrel_relatoriogerencialcontagempdf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_relatoriogerencialcontagempdf);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_relatoriogerencialcontagempdf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(DateTime)AV2ContagemResultado_DataDmnIn,(DateTime)AV3ContagemResultado_DataDmnFim,(int)AV4ContagemResultado_ContratadaOrigemCod,(int)AV5ContagemResultado_CntSrvCod,(String)AV6ContagemResultado_StatusDmn} ;
         ClassLoader.Execute("arel_relatoriogerencialcontagempdf","GeneXus.Programs.arel_relatoriogerencialcontagempdf", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 5 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV4ContagemResultado_ContratadaOrigemCod ;
      private int AV5ContagemResultado_CntSrvCod ;
      private String AV6ContagemResultado_StatusDmn ;
      private DateTime AV2ContagemResultado_DataDmnIn ;
      private DateTime AV3ContagemResultado_DataDmnFim ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
