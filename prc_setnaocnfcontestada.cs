/*
               File: PRC_SetNaoCnfContestada
        Description: Set Nao Cnf Contestada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:13.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_setnaocnfcontestada : GXProcedure
   {
      public prc_setnaocnfcontestada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_setnaocnfcontestada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultadoNaoCnf_OSCod )
      {
         this.A2020ContagemResultadoNaoCnf_OSCod = aP0_ContagemResultadoNaoCnf_OSCod;
         initialize();
         executePrivate();
         aP0_ContagemResultadoNaoCnf_OSCod=this.A2020ContagemResultadoNaoCnf_OSCod;
      }

      public int executeUdp( )
      {
         this.A2020ContagemResultadoNaoCnf_OSCod = aP0_ContagemResultadoNaoCnf_OSCod;
         initialize();
         executePrivate();
         aP0_ContagemResultadoNaoCnf_OSCod=this.A2020ContagemResultadoNaoCnf_OSCod;
         return A2020ContagemResultadoNaoCnf_OSCod ;
      }

      public void executeSubmit( ref int aP0_ContagemResultadoNaoCnf_OSCod )
      {
         prc_setnaocnfcontestada objprc_setnaocnfcontestada;
         objprc_setnaocnfcontestada = new prc_setnaocnfcontestada();
         objprc_setnaocnfcontestada.A2020ContagemResultadoNaoCnf_OSCod = aP0_ContagemResultadoNaoCnf_OSCod;
         objprc_setnaocnfcontestada.context.SetSubmitInitialConfig(context);
         objprc_setnaocnfcontestada.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_setnaocnfcontestada);
         aP0_ContagemResultadoNaoCnf_OSCod=this.A2020ContagemResultadoNaoCnf_OSCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_setnaocnfcontestada)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00WS2 */
         pr_default.execute(0, new Object[] {A2020ContagemResultadoNaoCnf_OSCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2024ContagemResultadoNaoCnf_Codigo = P00WS2_A2024ContagemResultadoNaoCnf_Codigo[0];
            AV8Codigo = A2024ContagemResultadoNaoCnf_Codigo;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         /* Using cursor P00WS3 */
         pr_default.execute(1, new Object[] {AV8Codigo, A2020ContagemResultadoNaoCnf_OSCod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A2024ContagemResultadoNaoCnf_Codigo = P00WS3_A2024ContagemResultadoNaoCnf_Codigo[0];
            A2057ContagemResultadoNaoCnf_Contestada = P00WS3_A2057ContagemResultadoNaoCnf_Contestada[0];
            n2057ContagemResultadoNaoCnf_Contestada = P00WS3_n2057ContagemResultadoNaoCnf_Contestada[0];
            A2057ContagemResultadoNaoCnf_Contestada = true;
            n2057ContagemResultadoNaoCnf_Contestada = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00WS4 */
            pr_default.execute(2, new Object[] {n2057ContagemResultadoNaoCnf_Contestada, A2057ContagemResultadoNaoCnf_Contestada, A2024ContagemResultadoNaoCnf_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNaoCnf") ;
            if (true) break;
            /* Using cursor P00WS5 */
            pr_default.execute(3, new Object[] {n2057ContagemResultadoNaoCnf_Contestada, A2057ContagemResultadoNaoCnf_Contestada, A2024ContagemResultadoNaoCnf_Codigo});
            pr_default.close(3);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNaoCnf") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00WS2_A2020ContagemResultadoNaoCnf_OSCod = new int[1] ;
         P00WS2_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         P00WS3_A2020ContagemResultadoNaoCnf_OSCod = new int[1] ;
         P00WS3_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         P00WS3_A2057ContagemResultadoNaoCnf_Contestada = new bool[] {false} ;
         P00WS3_n2057ContagemResultadoNaoCnf_Contestada = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_setnaocnfcontestada__default(),
            new Object[][] {
                new Object[] {
               P00WS2_A2020ContagemResultadoNaoCnf_OSCod, P00WS2_A2024ContagemResultadoNaoCnf_Codigo
               }
               , new Object[] {
               P00WS3_A2020ContagemResultadoNaoCnf_OSCod, P00WS3_A2024ContagemResultadoNaoCnf_Codigo, P00WS3_A2057ContagemResultadoNaoCnf_Contestada, P00WS3_n2057ContagemResultadoNaoCnf_Contestada
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A2020ContagemResultadoNaoCnf_OSCod ;
      private int A2024ContagemResultadoNaoCnf_Codigo ;
      private int AV8Codigo ;
      private String scmdbuf ;
      private bool A2057ContagemResultadoNaoCnf_Contestada ;
      private bool n2057ContagemResultadoNaoCnf_Contestada ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultadoNaoCnf_OSCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00WS2_A2020ContagemResultadoNaoCnf_OSCod ;
      private int[] P00WS2_A2024ContagemResultadoNaoCnf_Codigo ;
      private int[] P00WS3_A2020ContagemResultadoNaoCnf_OSCod ;
      private int[] P00WS3_A2024ContagemResultadoNaoCnf_Codigo ;
      private bool[] P00WS3_A2057ContagemResultadoNaoCnf_Contestada ;
      private bool[] P00WS3_n2057ContagemResultadoNaoCnf_Contestada ;
   }

   public class prc_setnaocnfcontestada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WS2 ;
          prmP00WS2 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WS3 ;
          prmP00WS3 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WS4 ;
          prmP00WS4 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_Contestada",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WS5 ;
          prmP00WS5 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_Contestada",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WS2", "SELECT [ContagemResultadoNaoCnf_OSCod], [ContagemResultadoNaoCnf_Codigo] FROM [ContagemResultadoNaoCnf] WITH (NOLOCK) WHERE [ContagemResultadoNaoCnf_OSCod] = @ContagemResultadoNaoCnf_OSCod ORDER BY [ContagemResultadoNaoCnf_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WS2,100,0,false,false )
             ,new CursorDef("P00WS3", "SELECT TOP 1 [ContagemResultadoNaoCnf_OSCod], [ContagemResultadoNaoCnf_Codigo], [ContagemResultadoNaoCnf_Contestada] FROM [ContagemResultadoNaoCnf] WITH (UPDLOCK) WHERE ([ContagemResultadoNaoCnf_Codigo] = @AV8Codigo) AND ([ContagemResultadoNaoCnf_OSCod] = @ContagemResultadoNaoCnf_OSCod) ORDER BY [ContagemResultadoNaoCnf_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WS3,1,0,true,true )
             ,new CursorDef("P00WS4", "UPDATE [ContagemResultadoNaoCnf] SET [ContagemResultadoNaoCnf_Contestada]=@ContagemResultadoNaoCnf_Contestada  WHERE [ContagemResultadoNaoCnf_Codigo] = @ContagemResultadoNaoCnf_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00WS4)
             ,new CursorDef("P00WS5", "UPDATE [ContagemResultadoNaoCnf] SET [ContagemResultadoNaoCnf_Contestada]=@ContagemResultadoNaoCnf_Contestada  WHERE [ContagemResultadoNaoCnf_Codigo] = @ContagemResultadoNaoCnf_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00WS5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
