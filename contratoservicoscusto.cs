/*
               File: ContratoServicosCusto
        Description: Custo do Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:31:34.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicoscusto : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTRATOSERVICOSCUSTO_USUARIOCOD") == 0 )
         {
            A1478ContratoServicosCusto_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1478ContratoServicosCusto_ContratadaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1478ContratoServicosCusto_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1478ContratoServicosCusto_ContratadaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACONTRATOSERVICOSCUSTO_USUARIOCOD3T174( A1478ContratoServicosCusto_ContratadaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
         {
            A1471ContratoServicosCusto_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1471ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_13( A1471ContratoServicosCusto_CntSrvCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_16") == 0 )
         {
            A1479ContratoServicosCusto_CntCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1479ContratoServicosCusto_CntCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1479ContratoServicosCusto_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1479ContratoServicosCusto_CntCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_16( A1479ContratoServicosCusto_CntCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_14") == 0 )
         {
            A1472ContratoServicosCusto_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1472ContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_14( A1472ContratoServicosCusto_UsuarioCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_15") == 0 )
         {
            A1476ContratoServicosCusto_UsrPesCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1476ContratoServicosCusto_UsrPesCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1476ContratoServicosCusto_UsrPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1476ContratoServicosCusto_UsrPesCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_15( A1476ContratoServicosCusto_UsrPesCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoServicosCusto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosCusto_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOSCUSTO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicosCusto_Codigo), "ZZZZZ9")));
               AV14ContratoServicosCusto_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ContratoServicosCusto_CntSrvCod), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynContratoServicosCusto_UsuarioCod.Name = "CONTRATOSERVICOSCUSTO_USUARIOCOD";
         dynContratoServicosCusto_UsuarioCod.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Custo do Servi�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynContratoServicosCusto_UsuarioCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratoservicoscusto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoservicoscusto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoServicosCusto_Codigo ,
                           ref int aP2_ContratoServicosCusto_CntSrvCod )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoServicosCusto_Codigo = aP1_ContratoServicosCusto_Codigo;
         this.AV14ContratoServicosCusto_CntSrvCod = aP2_ContratoServicosCusto_CntSrvCod;
         executePrivate();
         aP2_ContratoServicosCusto_CntSrvCod=this.AV14ContratoServicosCusto_CntSrvCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynContratoServicosCusto_UsuarioCod = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynContratoServicosCusto_UsuarioCod.ItemCount > 0 )
         {
            A1472ContratoServicosCusto_UsuarioCod = (int)(NumberUtil.Val( dynContratoServicosCusto_UsuarioCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1472ContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3T174( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3T174e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosCusto_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0, ",", "")), ((edtContratoServicosCusto_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1473ContratoServicosCusto_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1473ContratoServicosCusto_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosCusto_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosCusto_Codigo_Visible, edtContratoServicosCusto_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosCusto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosCusto_CntSrvCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A1471ContratoServicosCusto_CntSrvCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosCusto_CntSrvCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosCusto_CntSrvCod_Visible, edtContratoServicosCusto_CntSrvCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosCusto.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosCusto_ContratadaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1478ContratoServicosCusto_ContratadaCod), 6, 0, ",", "")), ((edtContratoServicosCusto_ContratadaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1478ContratoServicosCusto_ContratadaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1478ContratoServicosCusto_ContratadaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosCusto_ContratadaCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosCusto_ContratadaCod_Visible, edtContratoServicosCusto_ContratadaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosCusto.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3T174( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3T174( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3T174e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table3_29_3T174( true) ;
         }
         return  ;
      }

      protected void wb_table3_29_3T174e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_ContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3T174e( true) ;
         }
         else
         {
            wb_table1_2_3T174e( false) ;
         }
      }

      protected void wb_table3_29_3T174( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_29_3T174e( true) ;
         }
         else
         {
            wb_table3_29_3T174e( false) ;
         }
      }

      protected void wb_table2_5_3T174( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_3T174( true) ;
         }
         return  ;
      }

      protected void wb_table4_11_3T174e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3T174e( true) ;
         }
         else
         {
            wb_table2_5_3T174e( false) ;
         }
      }

      protected void wb_table4_11_3T174( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicoscusto_usuariocod_Internalname, "Colaborador", "", "", lblTextblockcontratoservicoscusto_usuariocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContratoServicosCusto_UsuarioCod, dynContratoServicosCusto_UsuarioCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0)), 1, dynContratoServicosCusto_UsuarioCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContratoServicosCusto_UsuarioCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "", true, "HLP_ContratoServicosCusto.htm");
            dynContratoServicosCusto_UsuarioCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoServicosCusto_UsuarioCod_Internalname, "Values", (String)(dynContratoServicosCusto_UsuarioCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicoscusto_cstuntprdnrm_Internalname, "Normal R$", "", "", lblTextblockcontratoservicoscusto_cstuntprdnrm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosCusto_CstUntPrdNrm_Internalname, StringUtil.LTrim( StringUtil.NToC( A1474ContratoServicosCusto_CstUntPrdNrm, 18, 5, ",", "")), ((edtContratoServicosCusto_CstUntPrdNrm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1474ContratoServicosCusto_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1474ContratoServicosCusto_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosCusto_CstUntPrdNrm_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosCusto_CstUntPrdNrm_Enabled, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicoscusto_cstuntprdext_Internalname, "Extra   R$", "", "", lblTextblockcontratoservicoscusto_cstuntprdext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosCusto_CstUntPrdExt_Internalname, StringUtil.LTrim( StringUtil.NToC( A1475ContratoServicosCusto_CstUntPrdExt, 18, 5, ",", "")), ((edtContratoServicosCusto_CstUntPrdExt_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1475ContratoServicosCusto_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1475ContratoServicosCusto_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosCusto_CstUntPrdExt_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosCusto_CstUntPrdExt_Enabled, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_3T174e( true) ;
         }
         else
         {
            wb_table4_11_3T174e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113T2 */
         E113T2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynContratoServicosCusto_UsuarioCod.CurrentValue = cgiGet( dynContratoServicosCusto_UsuarioCod_Internalname);
               A1472ContratoServicosCusto_UsuarioCod = (int)(NumberUtil.Val( cgiGet( dynContratoServicosCusto_UsuarioCod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1472ContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CstUntPrdNrm_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CstUntPrdNrm_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosCusto_CstUntPrdNrm_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1474ContratoServicosCusto_CstUntPrdNrm = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1474ContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( A1474ContratoServicosCusto_CstUntPrdNrm, 18, 5)));
               }
               else
               {
                  A1474ContratoServicosCusto_CstUntPrdNrm = context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CstUntPrdNrm_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1474ContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( A1474ContratoServicosCusto_CstUntPrdNrm, 18, 5)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CstUntPrdExt_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CstUntPrdExt_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosCusto_CstUntPrdExt_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1475ContratoServicosCusto_CstUntPrdExt = 0;
                  n1475ContratoServicosCusto_CstUntPrdExt = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1475ContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( A1475ContratoServicosCusto_CstUntPrdExt, 18, 5)));
               }
               else
               {
                  A1475ContratoServicosCusto_CstUntPrdExt = context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CstUntPrdExt_Internalname), ",", ".");
                  n1475ContratoServicosCusto_CstUntPrdExt = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1475ContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( A1475ContratoServicosCusto_CstUntPrdExt, 18, 5)));
               }
               n1475ContratoServicosCusto_CstUntPrdExt = ((Convert.ToDecimal(0)==A1475ContratoServicosCusto_CstUntPrdExt) ? true : false);
               A1473ContratoServicosCusto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosCusto_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CntSrvCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CntSrvCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSCUSTO_CNTSRVCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosCusto_CntSrvCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1471ContratoServicosCusto_CntSrvCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1471ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0)));
               }
               else
               {
                  A1471ContratoServicosCusto_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CntSrvCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1471ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0)));
               }
               A1478ContratoServicosCusto_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosCusto_ContratadaCod_Internalname), ",", "."));
               n1478ContratoServicosCusto_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1478ContratoServicosCusto_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1478ContratoServicosCusto_ContratadaCod), 6, 0)));
               /* Read saved values. */
               Z1473ContratoServicosCusto_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1473ContratoServicosCusto_Codigo"), ",", "."));
               Z1474ContratoServicosCusto_CstUntPrdNrm = context.localUtil.CToN( cgiGet( "Z1474ContratoServicosCusto_CstUntPrdNrm"), ",", ".");
               Z1475ContratoServicosCusto_CstUntPrdExt = context.localUtil.CToN( cgiGet( "Z1475ContratoServicosCusto_CstUntPrdExt"), ",", ".");
               n1475ContratoServicosCusto_CstUntPrdExt = ((Convert.ToDecimal(0)==A1475ContratoServicosCusto_CstUntPrdExt) ? true : false);
               Z1471ContratoServicosCusto_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "Z1471ContratoServicosCusto_CntSrvCod"), ",", "."));
               Z1472ContratoServicosCusto_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "Z1472ContratoServicosCusto_UsuarioCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N1471ContratoServicosCusto_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "N1471ContratoServicosCusto_CntSrvCod"), ",", "."));
               N1472ContratoServicosCusto_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "N1472ContratoServicosCusto_UsuarioCod"), ",", "."));
               AV7ContratoServicosCusto_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOSERVICOSCUSTO_CODIGO"), ",", "."));
               AV11Insert_ContratoServicosCusto_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATOSERVICOSCUSTO_CNTSRVCOD"), ",", "."));
               AV14ContratoServicosCusto_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOSERVICOSCUSTO_CNTSRVCOD"), ",", "."));
               AV12Insert_ContratoServicosCusto_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATOSERVICOSCUSTO_USUARIOCOD"), ",", "."));
               A1479ContratoServicosCusto_CntCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSCUSTO_CNTCOD"), ",", "."));
               A1476ContratoServicosCusto_UsrPesCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSCUSTO_USRPESCOD"), ",", "."));
               A1477ContratoServicosCusto_UsrPesNom = cgiGet( "CONTRATOSERVICOSCUSTO_USRPESNOM");
               n1477ContratoServicosCusto_UsrPesNom = false;
               AV16Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoServicosCusto";
               A1473ContratoServicosCusto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosCusto_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1473ContratoServicosCusto_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1473ContratoServicosCusto_Codigo != Z1473ContratoServicosCusto_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratoservicoscusto:[SecurityCheckFailed value for]"+"ContratoServicosCusto_Codigo:"+context.localUtil.Format( (decimal)(A1473ContratoServicosCusto_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("contratoservicoscusto:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1473ContratoServicosCusto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode174 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode174;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound174 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_3T0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTRATOSERVICOSCUSTO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtContratoServicosCusto_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113T2 */
                           E113T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123T2 */
                           E123T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E123T2 */
            E123T2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3T174( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes3T174( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_3T0( )
      {
         BeforeValidate3T174( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3T174( ) ;
            }
            else
            {
               CheckExtendedTable3T174( ) ;
               CloseExtendedTableCursors3T174( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption3T0( )
      {
      }

      protected void E113T2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV16Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV17GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GXV1), 8, 0)));
            while ( AV17GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV17GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "ContratoServicosCusto_CntSrvCod") == 0 )
               {
                  AV11Insert_ContratoServicosCusto_CntSrvCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ContratoServicosCusto_CntSrvCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "ContratoServicosCusto_UsuarioCod") == 0 )
               {
                  AV12Insert_ContratoServicosCusto_UsuarioCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_ContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_ContratoServicosCusto_UsuarioCod), 6, 0)));
               }
               AV17GXV1 = (int)(AV17GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GXV1), 8, 0)));
            }
         }
         edtContratoServicosCusto_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosCusto_Codigo_Visible), 5, 0)));
         edtContratoServicosCusto_CntSrvCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_CntSrvCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosCusto_CntSrvCod_Visible), 5, 0)));
         edtContratoServicosCusto_ContratadaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_ContratadaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosCusto_ContratadaCod_Visible), 5, 0)));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      protected void E123T2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratoservicoscusto.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)AV14ContratoServicosCusto_CntSrvCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM3T174( short GX_JID )
      {
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1474ContratoServicosCusto_CstUntPrdNrm = T003T3_A1474ContratoServicosCusto_CstUntPrdNrm[0];
               Z1475ContratoServicosCusto_CstUntPrdExt = T003T3_A1475ContratoServicosCusto_CstUntPrdExt[0];
               Z1471ContratoServicosCusto_CntSrvCod = T003T3_A1471ContratoServicosCusto_CntSrvCod[0];
               Z1472ContratoServicosCusto_UsuarioCod = T003T3_A1472ContratoServicosCusto_UsuarioCod[0];
            }
            else
            {
               Z1474ContratoServicosCusto_CstUntPrdNrm = A1474ContratoServicosCusto_CstUntPrdNrm;
               Z1475ContratoServicosCusto_CstUntPrdExt = A1475ContratoServicosCusto_CstUntPrdExt;
               Z1471ContratoServicosCusto_CntSrvCod = A1471ContratoServicosCusto_CntSrvCod;
               Z1472ContratoServicosCusto_UsuarioCod = A1472ContratoServicosCusto_UsuarioCod;
            }
         }
         if ( GX_JID == -12 )
         {
            Z1473ContratoServicosCusto_Codigo = A1473ContratoServicosCusto_Codigo;
            Z1474ContratoServicosCusto_CstUntPrdNrm = A1474ContratoServicosCusto_CstUntPrdNrm;
            Z1475ContratoServicosCusto_CstUntPrdExt = A1475ContratoServicosCusto_CstUntPrdExt;
            Z1471ContratoServicosCusto_CntSrvCod = A1471ContratoServicosCusto_CntSrvCod;
            Z1472ContratoServicosCusto_UsuarioCod = A1472ContratoServicosCusto_UsuarioCod;
            Z1479ContratoServicosCusto_CntCod = A1479ContratoServicosCusto_CntCod;
            Z1478ContratoServicosCusto_ContratadaCod = A1478ContratoServicosCusto_ContratadaCod;
            Z1476ContratoServicosCusto_UsrPesCod = A1476ContratoServicosCusto_UsrPesCod;
            Z1477ContratoServicosCusto_UsrPesNom = A1477ContratoServicosCusto_UsrPesNom;
         }
      }

      protected void standaloneNotModal( )
      {
         edtContratoServicosCusto_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosCusto_Codigo_Enabled), 5, 0)));
         AV16Pgmname = "ContratoServicosCusto";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Pgmname", AV16Pgmname);
         edtContratoServicosCusto_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosCusto_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoServicosCusto_Codigo) )
         {
            A1473ContratoServicosCusto_Codigo = AV7ContratoServicosCusto_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContratoServicosCusto_CntSrvCod) )
         {
            edtContratoServicosCusto_CntSrvCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_CntSrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosCusto_CntSrvCod_Enabled), 5, 0)));
         }
         else
         {
            edtContratoServicosCusto_CntSrvCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_CntSrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosCusto_CntSrvCod_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_ContratoServicosCusto_UsuarioCod) )
         {
            dynContratoServicosCusto_UsuarioCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoServicosCusto_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoServicosCusto_UsuarioCod.Enabled), 5, 0)));
         }
         else
         {
            dynContratoServicosCusto_UsuarioCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoServicosCusto_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoServicosCusto_UsuarioCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_ContratoServicosCusto_UsuarioCod) )
         {
            A1472ContratoServicosCusto_UsuarioCod = AV12Insert_ContratoServicosCusto_UsuarioCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1472ContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContratoServicosCusto_CntSrvCod) )
         {
            A1471ContratoServicosCusto_CntSrvCod = AV11Insert_ContratoServicosCusto_CntSrvCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1471ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0)));
         }
         else
         {
            if ( ! (0==AV14ContratoServicosCusto_CntSrvCod) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               A1471ContratoServicosCusto_CntSrvCod = AV14ContratoServicosCusto_CntSrvCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1471ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0)));
            }
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T003T5 */
            pr_default.execute(3, new Object[] {A1472ContratoServicosCusto_UsuarioCod});
            A1476ContratoServicosCusto_UsrPesCod = T003T5_A1476ContratoServicosCusto_UsrPesCod[0];
            n1476ContratoServicosCusto_UsrPesCod = T003T5_n1476ContratoServicosCusto_UsrPesCod[0];
            pr_default.close(3);
            /* Using cursor T003T6 */
            pr_default.execute(4, new Object[] {n1476ContratoServicosCusto_UsrPesCod, A1476ContratoServicosCusto_UsrPesCod});
            A1477ContratoServicosCusto_UsrPesNom = T003T6_A1477ContratoServicosCusto_UsrPesNom[0];
            n1477ContratoServicosCusto_UsrPesNom = T003T6_n1477ContratoServicosCusto_UsrPesNom[0];
            pr_default.close(4);
            /* Using cursor T003T4 */
            pr_default.execute(2, new Object[] {A1471ContratoServicosCusto_CntSrvCod});
            A1479ContratoServicosCusto_CntCod = T003T4_A1479ContratoServicosCusto_CntCod[0];
            n1479ContratoServicosCusto_CntCod = T003T4_n1479ContratoServicosCusto_CntCod[0];
            pr_default.close(2);
            /* Using cursor T003T7 */
            pr_default.execute(5, new Object[] {n1479ContratoServicosCusto_CntCod, A1479ContratoServicosCusto_CntCod});
            if ( (pr_default.getStatus(5) != 101) )
            {
               A1478ContratoServicosCusto_ContratadaCod = T003T7_A1478ContratoServicosCusto_ContratadaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1478ContratoServicosCusto_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1478ContratoServicosCusto_ContratadaCod), 6, 0)));
               n1478ContratoServicosCusto_ContratadaCod = T003T7_n1478ContratoServicosCusto_ContratadaCod[0];
            }
            else
            {
               A1478ContratoServicosCusto_ContratadaCod = 0;
               n1478ContratoServicosCusto_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1478ContratoServicosCusto_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1478ContratoServicosCusto_ContratadaCod), 6, 0)));
            }
            pr_default.close(5);
            GXACONTRATOSERVICOSCUSTO_USUARIOCOD_html3T174( A1478ContratoServicosCusto_ContratadaCod) ;
         }
      }

      protected void Load3T174( )
      {
         /* Using cursor T003T8 */
         pr_default.execute(6, new Object[] {A1473ContratoServicosCusto_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound174 = 1;
            A1477ContratoServicosCusto_UsrPesNom = T003T8_A1477ContratoServicosCusto_UsrPesNom[0];
            n1477ContratoServicosCusto_UsrPesNom = T003T8_n1477ContratoServicosCusto_UsrPesNom[0];
            A1474ContratoServicosCusto_CstUntPrdNrm = T003T8_A1474ContratoServicosCusto_CstUntPrdNrm[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1474ContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( A1474ContratoServicosCusto_CstUntPrdNrm, 18, 5)));
            A1475ContratoServicosCusto_CstUntPrdExt = T003T8_A1475ContratoServicosCusto_CstUntPrdExt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1475ContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( A1475ContratoServicosCusto_CstUntPrdExt, 18, 5)));
            n1475ContratoServicosCusto_CstUntPrdExt = T003T8_n1475ContratoServicosCusto_CstUntPrdExt[0];
            A1471ContratoServicosCusto_CntSrvCod = T003T8_A1471ContratoServicosCusto_CntSrvCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1471ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0)));
            A1472ContratoServicosCusto_UsuarioCod = T003T8_A1472ContratoServicosCusto_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1472ContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0)));
            A1479ContratoServicosCusto_CntCod = T003T8_A1479ContratoServicosCusto_CntCod[0];
            n1479ContratoServicosCusto_CntCod = T003T8_n1479ContratoServicosCusto_CntCod[0];
            A1476ContratoServicosCusto_UsrPesCod = T003T8_A1476ContratoServicosCusto_UsrPesCod[0];
            n1476ContratoServicosCusto_UsrPesCod = T003T8_n1476ContratoServicosCusto_UsrPesCod[0];
            A1478ContratoServicosCusto_ContratadaCod = T003T8_A1478ContratoServicosCusto_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1478ContratoServicosCusto_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1478ContratoServicosCusto_ContratadaCod), 6, 0)));
            n1478ContratoServicosCusto_ContratadaCod = T003T8_n1478ContratoServicosCusto_ContratadaCod[0];
            ZM3T174( -12) ;
         }
         pr_default.close(6);
         OnLoadActions3T174( ) ;
      }

      protected void OnLoadActions3T174( )
      {
         GXACONTRATOSERVICOSCUSTO_USUARIOCOD_html3T174( A1478ContratoServicosCusto_ContratadaCod) ;
      }

      protected void CheckExtendedTable3T174( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T003T4 */
         pr_default.execute(2, new Object[] {A1471ContratoServicosCusto_CntSrvCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servi�o do contrato'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSCUSTO_CNTSRVCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosCusto_CntSrvCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1479ContratoServicosCusto_CntCod = T003T4_A1479ContratoServicosCusto_CntCod[0];
         n1479ContratoServicosCusto_CntCod = T003T4_n1479ContratoServicosCusto_CntCod[0];
         pr_default.close(2);
         /* Using cursor T003T7 */
         pr_default.execute(5, new Object[] {n1479ContratoServicosCusto_CntCod, A1479ContratoServicosCusto_CntCod});
         if ( (pr_default.getStatus(5) != 101) )
         {
            A1478ContratoServicosCusto_ContratadaCod = T003T7_A1478ContratoServicosCusto_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1478ContratoServicosCusto_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1478ContratoServicosCusto_ContratadaCod), 6, 0)));
            n1478ContratoServicosCusto_ContratadaCod = T003T7_n1478ContratoServicosCusto_ContratadaCod[0];
         }
         else
         {
            A1478ContratoServicosCusto_ContratadaCod = 0;
            n1478ContratoServicosCusto_ContratadaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1478ContratoServicosCusto_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1478ContratoServicosCusto_ContratadaCod), 6, 0)));
         }
         pr_default.close(5);
         GXACONTRATOSERVICOSCUSTO_USUARIOCOD_html3T174( A1478ContratoServicosCusto_ContratadaCod) ;
         /* Using cursor T003T5 */
         pr_default.execute(3, new Object[] {A1472ContratoServicosCusto_UsuarioCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usu�rio'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSCUSTO_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = dynContratoServicosCusto_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1476ContratoServicosCusto_UsrPesCod = T003T5_A1476ContratoServicosCusto_UsrPesCod[0];
         n1476ContratoServicosCusto_UsrPesCod = T003T5_n1476ContratoServicosCusto_UsrPesCod[0];
         pr_default.close(3);
         /* Using cursor T003T6 */
         pr_default.execute(4, new Object[] {n1476ContratoServicosCusto_UsrPesCod, A1476ContratoServicosCusto_UsrPesCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1477ContratoServicosCusto_UsrPesNom = T003T6_A1477ContratoServicosCusto_UsrPesNom[0];
         n1477ContratoServicosCusto_UsrPesNom = T003T6_n1477ContratoServicosCusto_UsrPesNom[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors3T174( )
      {
         pr_default.close(2);
         pr_default.close(5);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_13( int A1471ContratoServicosCusto_CntSrvCod )
      {
         /* Using cursor T003T9 */
         pr_default.execute(7, new Object[] {A1471ContratoServicosCusto_CntSrvCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servi�o do contrato'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSCUSTO_CNTSRVCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosCusto_CntSrvCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1479ContratoServicosCusto_CntCod = T003T9_A1479ContratoServicosCusto_CntCod[0];
         n1479ContratoServicosCusto_CntCod = T003T9_n1479ContratoServicosCusto_CntCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1479ContratoServicosCusto_CntCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_16( int A1479ContratoServicosCusto_CntCod )
      {
         /* Using cursor T003T10 */
         pr_default.execute(8, new Object[] {n1479ContratoServicosCusto_CntCod, A1479ContratoServicosCusto_CntCod});
         if ( (pr_default.getStatus(8) != 101) )
         {
            A1478ContratoServicosCusto_ContratadaCod = T003T10_A1478ContratoServicosCusto_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1478ContratoServicosCusto_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1478ContratoServicosCusto_ContratadaCod), 6, 0)));
            n1478ContratoServicosCusto_ContratadaCod = T003T10_n1478ContratoServicosCusto_ContratadaCod[0];
         }
         else
         {
            A1478ContratoServicosCusto_ContratadaCod = 0;
            n1478ContratoServicosCusto_ContratadaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1478ContratoServicosCusto_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1478ContratoServicosCusto_ContratadaCod), 6, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1478ContratoServicosCusto_ContratadaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_14( int A1472ContratoServicosCusto_UsuarioCod )
      {
         /* Using cursor T003T11 */
         pr_default.execute(9, new Object[] {A1472ContratoServicosCusto_UsuarioCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usu�rio'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSCUSTO_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = dynContratoServicosCusto_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1476ContratoServicosCusto_UsrPesCod = T003T11_A1476ContratoServicosCusto_UsrPesCod[0];
         n1476ContratoServicosCusto_UsrPesCod = T003T11_n1476ContratoServicosCusto_UsrPesCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1476ContratoServicosCusto_UsrPesCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_15( int A1476ContratoServicosCusto_UsrPesCod )
      {
         /* Using cursor T003T12 */
         pr_default.execute(10, new Object[] {n1476ContratoServicosCusto_UsrPesCod, A1476ContratoServicosCusto_UsrPesCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1477ContratoServicosCusto_UsrPesNom = T003T12_A1477ContratoServicosCusto_UsrPesNom[0];
         n1477ContratoServicosCusto_UsrPesNom = T003T12_n1477ContratoServicosCusto_UsrPesNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1477ContratoServicosCusto_UsrPesNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void GetKey3T174( )
      {
         /* Using cursor T003T13 */
         pr_default.execute(11, new Object[] {A1473ContratoServicosCusto_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound174 = 1;
         }
         else
         {
            RcdFound174 = 0;
         }
         pr_default.close(11);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003T3 */
         pr_default.execute(1, new Object[] {A1473ContratoServicosCusto_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3T174( 12) ;
            RcdFound174 = 1;
            A1473ContratoServicosCusto_Codigo = T003T3_A1473ContratoServicosCusto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
            A1474ContratoServicosCusto_CstUntPrdNrm = T003T3_A1474ContratoServicosCusto_CstUntPrdNrm[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1474ContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( A1474ContratoServicosCusto_CstUntPrdNrm, 18, 5)));
            A1475ContratoServicosCusto_CstUntPrdExt = T003T3_A1475ContratoServicosCusto_CstUntPrdExt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1475ContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( A1475ContratoServicosCusto_CstUntPrdExt, 18, 5)));
            n1475ContratoServicosCusto_CstUntPrdExt = T003T3_n1475ContratoServicosCusto_CstUntPrdExt[0];
            A1471ContratoServicosCusto_CntSrvCod = T003T3_A1471ContratoServicosCusto_CntSrvCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1471ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0)));
            A1472ContratoServicosCusto_UsuarioCod = T003T3_A1472ContratoServicosCusto_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1472ContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0)));
            Z1473ContratoServicosCusto_Codigo = A1473ContratoServicosCusto_Codigo;
            sMode174 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3T174( ) ;
            if ( AnyError == 1 )
            {
               RcdFound174 = 0;
               InitializeNonKey3T174( ) ;
            }
            Gx_mode = sMode174;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound174 = 0;
            InitializeNonKey3T174( ) ;
            sMode174 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode174;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3T174( ) ;
         if ( RcdFound174 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound174 = 0;
         /* Using cursor T003T14 */
         pr_default.execute(12, new Object[] {A1473ContratoServicosCusto_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T003T14_A1473ContratoServicosCusto_Codigo[0] < A1473ContratoServicosCusto_Codigo ) ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T003T14_A1473ContratoServicosCusto_Codigo[0] > A1473ContratoServicosCusto_Codigo ) ) )
            {
               A1473ContratoServicosCusto_Codigo = T003T14_A1473ContratoServicosCusto_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
               RcdFound174 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void move_previous( )
      {
         RcdFound174 = 0;
         /* Using cursor T003T15 */
         pr_default.execute(13, new Object[] {A1473ContratoServicosCusto_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            while ( (pr_default.getStatus(13) != 101) && ( ( T003T15_A1473ContratoServicosCusto_Codigo[0] > A1473ContratoServicosCusto_Codigo ) ) )
            {
               pr_default.readNext(13);
            }
            if ( (pr_default.getStatus(13) != 101) && ( ( T003T15_A1473ContratoServicosCusto_Codigo[0] < A1473ContratoServicosCusto_Codigo ) ) )
            {
               A1473ContratoServicosCusto_Codigo = T003T15_A1473ContratoServicosCusto_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
               RcdFound174 = 1;
            }
         }
         pr_default.close(13);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3T174( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynContratoServicosCusto_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3T174( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound174 == 1 )
            {
               if ( A1473ContratoServicosCusto_Codigo != Z1473ContratoServicosCusto_Codigo )
               {
                  A1473ContratoServicosCusto_Codigo = Z1473ContratoServicosCusto_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATOSERVICOSCUSTO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosCusto_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynContratoServicosCusto_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update3T174( ) ;
                  GX_FocusControl = dynContratoServicosCusto_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1473ContratoServicosCusto_Codigo != Z1473ContratoServicosCusto_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = dynContratoServicosCusto_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3T174( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOSERVICOSCUSTO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContratoServicosCusto_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = dynContratoServicosCusto_UsuarioCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3T174( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1473ContratoServicosCusto_Codigo != Z1473ContratoServicosCusto_Codigo )
         {
            A1473ContratoServicosCusto_Codigo = Z1473ContratoServicosCusto_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATOSERVICOSCUSTO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosCusto_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynContratoServicosCusto_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency3T174( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003T2 */
            pr_default.execute(0, new Object[] {A1473ContratoServicosCusto_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosCusto"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1474ContratoServicosCusto_CstUntPrdNrm != T003T2_A1474ContratoServicosCusto_CstUntPrdNrm[0] ) || ( Z1475ContratoServicosCusto_CstUntPrdExt != T003T2_A1475ContratoServicosCusto_CstUntPrdExt[0] ) || ( Z1471ContratoServicosCusto_CntSrvCod != T003T2_A1471ContratoServicosCusto_CntSrvCod[0] ) || ( Z1472ContratoServicosCusto_UsuarioCod != T003T2_A1472ContratoServicosCusto_UsuarioCod[0] ) )
            {
               if ( Z1474ContratoServicosCusto_CstUntPrdNrm != T003T2_A1474ContratoServicosCusto_CstUntPrdNrm[0] )
               {
                  GXUtil.WriteLog("contratoservicoscusto:[seudo value changed for attri]"+"ContratoServicosCusto_CstUntPrdNrm");
                  GXUtil.WriteLogRaw("Old: ",Z1474ContratoServicosCusto_CstUntPrdNrm);
                  GXUtil.WriteLogRaw("Current: ",T003T2_A1474ContratoServicosCusto_CstUntPrdNrm[0]);
               }
               if ( Z1475ContratoServicosCusto_CstUntPrdExt != T003T2_A1475ContratoServicosCusto_CstUntPrdExt[0] )
               {
                  GXUtil.WriteLog("contratoservicoscusto:[seudo value changed for attri]"+"ContratoServicosCusto_CstUntPrdExt");
                  GXUtil.WriteLogRaw("Old: ",Z1475ContratoServicosCusto_CstUntPrdExt);
                  GXUtil.WriteLogRaw("Current: ",T003T2_A1475ContratoServicosCusto_CstUntPrdExt[0]);
               }
               if ( Z1471ContratoServicosCusto_CntSrvCod != T003T2_A1471ContratoServicosCusto_CntSrvCod[0] )
               {
                  GXUtil.WriteLog("contratoservicoscusto:[seudo value changed for attri]"+"ContratoServicosCusto_CntSrvCod");
                  GXUtil.WriteLogRaw("Old: ",Z1471ContratoServicosCusto_CntSrvCod);
                  GXUtil.WriteLogRaw("Current: ",T003T2_A1471ContratoServicosCusto_CntSrvCod[0]);
               }
               if ( Z1472ContratoServicosCusto_UsuarioCod != T003T2_A1472ContratoServicosCusto_UsuarioCod[0] )
               {
                  GXUtil.WriteLog("contratoservicoscusto:[seudo value changed for attri]"+"ContratoServicosCusto_UsuarioCod");
                  GXUtil.WriteLogRaw("Old: ",Z1472ContratoServicosCusto_UsuarioCod);
                  GXUtil.WriteLogRaw("Current: ",T003T2_A1472ContratoServicosCusto_UsuarioCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosCusto"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3T174( )
      {
         BeforeValidate3T174( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3T174( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3T174( 0) ;
            CheckOptimisticConcurrency3T174( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3T174( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3T174( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003T16 */
                     pr_default.execute(14, new Object[] {A1474ContratoServicosCusto_CstUntPrdNrm, n1475ContratoServicosCusto_CstUntPrdExt, A1475ContratoServicosCusto_CstUntPrdExt, A1471ContratoServicosCusto_CntSrvCod, A1472ContratoServicosCusto_UsuarioCod});
                     A1473ContratoServicosCusto_Codigo = T003T16_A1473ContratoServicosCusto_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosCusto") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3T0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3T174( ) ;
            }
            EndLevel3T174( ) ;
         }
         CloseExtendedTableCursors3T174( ) ;
      }

      protected void Update3T174( )
      {
         BeforeValidate3T174( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3T174( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3T174( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3T174( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3T174( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003T17 */
                     pr_default.execute(15, new Object[] {A1474ContratoServicosCusto_CstUntPrdNrm, n1475ContratoServicosCusto_CstUntPrdExt, A1475ContratoServicosCusto_CstUntPrdExt, A1471ContratoServicosCusto_CntSrvCod, A1472ContratoServicosCusto_UsuarioCod, A1473ContratoServicosCusto_Codigo});
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosCusto") ;
                     if ( (pr_default.getStatus(15) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosCusto"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3T174( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3T174( ) ;
         }
         CloseExtendedTableCursors3T174( ) ;
      }

      protected void DeferredUpdate3T174( )
      {
      }

      protected void delete( )
      {
         BeforeValidate3T174( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3T174( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3T174( ) ;
            AfterConfirm3T174( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3T174( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003T18 */
                  pr_default.execute(16, new Object[] {A1473ContratoServicosCusto_Codigo});
                  pr_default.close(16);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosCusto") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode174 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3T174( ) ;
         Gx_mode = sMode174;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3T174( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003T19 */
            pr_default.execute(17, new Object[] {A1471ContratoServicosCusto_CntSrvCod});
            A1479ContratoServicosCusto_CntCod = T003T19_A1479ContratoServicosCusto_CntCod[0];
            n1479ContratoServicosCusto_CntCod = T003T19_n1479ContratoServicosCusto_CntCod[0];
            pr_default.close(17);
            /* Using cursor T003T20 */
            pr_default.execute(18, new Object[] {n1479ContratoServicosCusto_CntCod, A1479ContratoServicosCusto_CntCod});
            if ( (pr_default.getStatus(18) != 101) )
            {
               A1478ContratoServicosCusto_ContratadaCod = T003T20_A1478ContratoServicosCusto_ContratadaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1478ContratoServicosCusto_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1478ContratoServicosCusto_ContratadaCod), 6, 0)));
               n1478ContratoServicosCusto_ContratadaCod = T003T20_n1478ContratoServicosCusto_ContratadaCod[0];
            }
            else
            {
               A1478ContratoServicosCusto_ContratadaCod = 0;
               n1478ContratoServicosCusto_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1478ContratoServicosCusto_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1478ContratoServicosCusto_ContratadaCod), 6, 0)));
            }
            pr_default.close(18);
            GXACONTRATOSERVICOSCUSTO_USUARIOCOD_html3T174( A1478ContratoServicosCusto_ContratadaCod) ;
            /* Using cursor T003T21 */
            pr_default.execute(19, new Object[] {A1472ContratoServicosCusto_UsuarioCod});
            A1476ContratoServicosCusto_UsrPesCod = T003T21_A1476ContratoServicosCusto_UsrPesCod[0];
            n1476ContratoServicosCusto_UsrPesCod = T003T21_n1476ContratoServicosCusto_UsrPesCod[0];
            pr_default.close(19);
            /* Using cursor T003T22 */
            pr_default.execute(20, new Object[] {n1476ContratoServicosCusto_UsrPesCod, A1476ContratoServicosCusto_UsrPesCod});
            A1477ContratoServicosCusto_UsrPesNom = T003T22_A1477ContratoServicosCusto_UsrPesNom[0];
            n1477ContratoServicosCusto_UsrPesNom = T003T22_n1477ContratoServicosCusto_UsrPesNom[0];
            pr_default.close(20);
         }
      }

      protected void EndLevel3T174( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3T174( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(17);
            pr_default.close(19);
            pr_default.close(20);
            pr_default.close(18);
            context.CommitDataStores( "ContratoServicosCusto");
            if ( AnyError == 0 )
            {
               ConfirmValues3T0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(17);
            pr_default.close(19);
            pr_default.close(20);
            pr_default.close(18);
            context.RollbackDataStores( "ContratoServicosCusto");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3T174( )
      {
         /* Scan By routine */
         /* Using cursor T003T23 */
         pr_default.execute(21);
         RcdFound174 = 0;
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound174 = 1;
            A1473ContratoServicosCusto_Codigo = T003T23_A1473ContratoServicosCusto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3T174( )
      {
         /* Scan next routine */
         pr_default.readNext(21);
         RcdFound174 = 0;
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound174 = 1;
            A1473ContratoServicosCusto_Codigo = T003T23_A1473ContratoServicosCusto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3T174( )
      {
         pr_default.close(21);
      }

      protected void AfterConfirm3T174( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3T174( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3T174( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3T174( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3T174( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3T174( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3T174( )
      {
         dynContratoServicosCusto_UsuarioCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoServicosCusto_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoServicosCusto_UsuarioCod.Enabled), 5, 0)));
         edtContratoServicosCusto_CstUntPrdNrm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_CstUntPrdNrm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosCusto_CstUntPrdNrm_Enabled), 5, 0)));
         edtContratoServicosCusto_CstUntPrdExt_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_CstUntPrdExt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosCusto_CstUntPrdExt_Enabled), 5, 0)));
         edtContratoServicosCusto_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosCusto_Codigo_Enabled), 5, 0)));
         edtContratoServicosCusto_CntSrvCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_CntSrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosCusto_CntSrvCod_Enabled), 5, 0)));
         edtContratoServicosCusto_ContratadaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_ContratadaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosCusto_ContratadaCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3T0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299313578");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicoscusto.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoServicosCusto_Codigo) + "," + UrlEncode("" +AV14ContratoServicosCusto_CntSrvCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1473ContratoServicosCusto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1474ContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.NToC( Z1474ContratoServicosCusto_CstUntPrdNrm, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1475ContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.NToC( Z1475ContratoServicosCusto_CstUntPrdExt, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1471ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1471ContratoServicosCusto_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1472ContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1472ContratoServicosCusto_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N1471ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N1472ContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOSCUSTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicosCusto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATOSERVICOSCUSTO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_ContratoServicosCusto_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOSCUSTO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14ContratoServicosCusto_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATOSERVICOSCUSTO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_ContratoServicosCusto_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSCUSTO_CNTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1479ContratoServicosCusto_CntCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSCUSTO_USRPESCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1476ContratoServicosCusto_UsrPesCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSCUSTO_USRPESNOM", StringUtil.RTrim( A1477ContratoServicosCusto_UsrPesNom));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV16Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOSERVICOSCUSTO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicosCusto_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoServicosCusto";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1473ContratoServicosCusto_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoservicoscusto:[SendSecurityCheck value for]"+"ContratoServicosCusto_Codigo:"+context.localUtil.Format( (decimal)(A1473ContratoServicosCusto_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("contratoservicoscusto:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratoservicoscusto.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoServicosCusto_Codigo) + "," + UrlEncode("" +AV14ContratoServicosCusto_CntSrvCod) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosCusto" ;
      }

      public override String GetPgmdesc( )
      {
         return "Custo do Servi�o" ;
      }

      protected void InitializeNonKey3T174( )
      {
         A1471ContratoServicosCusto_CntSrvCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1471ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0)));
         A1472ContratoServicosCusto_UsuarioCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1472ContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0)));
         A1478ContratoServicosCusto_ContratadaCod = 0;
         n1478ContratoServicosCusto_ContratadaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1478ContratoServicosCusto_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1478ContratoServicosCusto_ContratadaCod), 6, 0)));
         A1479ContratoServicosCusto_CntCod = 0;
         n1479ContratoServicosCusto_CntCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1479ContratoServicosCusto_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1479ContratoServicosCusto_CntCod), 6, 0)));
         A1476ContratoServicosCusto_UsrPesCod = 0;
         n1476ContratoServicosCusto_UsrPesCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1476ContratoServicosCusto_UsrPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1476ContratoServicosCusto_UsrPesCod), 6, 0)));
         A1477ContratoServicosCusto_UsrPesNom = "";
         n1477ContratoServicosCusto_UsrPesNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1477ContratoServicosCusto_UsrPesNom", A1477ContratoServicosCusto_UsrPesNom);
         A1474ContratoServicosCusto_CstUntPrdNrm = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1474ContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( A1474ContratoServicosCusto_CstUntPrdNrm, 18, 5)));
         A1475ContratoServicosCusto_CstUntPrdExt = 0;
         n1475ContratoServicosCusto_CstUntPrdExt = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1475ContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( A1475ContratoServicosCusto_CstUntPrdExt, 18, 5)));
         n1475ContratoServicosCusto_CstUntPrdExt = ((Convert.ToDecimal(0)==A1475ContratoServicosCusto_CstUntPrdExt) ? true : false);
         Z1474ContratoServicosCusto_CstUntPrdNrm = 0;
         Z1475ContratoServicosCusto_CstUntPrdExt = 0;
         Z1471ContratoServicosCusto_CntSrvCod = 0;
         Z1472ContratoServicosCusto_UsuarioCod = 0;
      }

      protected void InitAll3T174( )
      {
         A1473ContratoServicosCusto_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
         InitializeNonKey3T174( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1471ContratoServicosCusto_CntSrvCod = i1471ContratoServicosCusto_CntSrvCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1471ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0)));
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299313588");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratoservicoscusto.js", "?20205299313589");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratoservicoscusto_usuariocod_Internalname = "TEXTBLOCKCONTRATOSERVICOSCUSTO_USUARIOCOD";
         dynContratoServicosCusto_UsuarioCod_Internalname = "CONTRATOSERVICOSCUSTO_USUARIOCOD";
         lblTextblockcontratoservicoscusto_cstuntprdnrm_Internalname = "TEXTBLOCKCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
         edtContratoServicosCusto_CstUntPrdNrm_Internalname = "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
         lblTextblockcontratoservicoscusto_cstuntprdext_Internalname = "TEXTBLOCKCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT";
         edtContratoServicosCusto_CstUntPrdExt_Internalname = "CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblTbjava_Internalname = "TBJAVA";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContratoServicosCusto_Codigo_Internalname = "CONTRATOSERVICOSCUSTO_CODIGO";
         edtContratoServicosCusto_CntSrvCod_Internalname = "CONTRATOSERVICOSCUSTO_CNTSRVCOD";
         edtContratoServicosCusto_ContratadaCod_Internalname = "CONTRATOSERVICOSCUSTO_CONTRATADACOD";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Custo do Servi�o";
         edtContratoServicosCusto_CstUntPrdExt_Jsonclick = "";
         edtContratoServicosCusto_CstUntPrdExt_Enabled = 1;
         edtContratoServicosCusto_CstUntPrdNrm_Jsonclick = "";
         edtContratoServicosCusto_CstUntPrdNrm_Enabled = 1;
         dynContratoServicosCusto_UsuarioCod_Jsonclick = "";
         dynContratoServicosCusto_UsuarioCod.Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         lblTbjava_Caption = "tbJava";
         lblTbjava_Visible = 1;
         edtContratoServicosCusto_ContratadaCod_Jsonclick = "";
         edtContratoServicosCusto_ContratadaCod_Enabled = 0;
         edtContratoServicosCusto_ContratadaCod_Visible = 1;
         edtContratoServicosCusto_CntSrvCod_Jsonclick = "";
         edtContratoServicosCusto_CntSrvCod_Enabled = 1;
         edtContratoServicosCusto_CntSrvCod_Visible = 1;
         edtContratoServicosCusto_Codigo_Jsonclick = "";
         edtContratoServicosCusto_Codigo_Enabled = 0;
         edtContratoServicosCusto_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         GXACONTRATOSERVICOSCUSTO_USUARIOCOD_html3T174( A1478ContratoServicosCusto_ContratadaCod) ;
         /* End function dynload_actions */
      }

      protected void GXDLACONTRATOSERVICOSCUSTO_USUARIOCOD3T174( int A1478ContratoServicosCusto_ContratadaCod )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTRATOSERVICOSCUSTO_USUARIOCOD_data3T174( A1478ContratoServicosCusto_ContratadaCod) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTRATOSERVICOSCUSTO_USUARIOCOD_html3T174( int A1478ContratoServicosCusto_ContratadaCod )
      {
         int gxdynajaxvalue ;
         GXDLACONTRATOSERVICOSCUSTO_USUARIOCOD_data3T174( A1478ContratoServicosCusto_ContratadaCod) ;
         gxdynajaxindex = 1;
         dynContratoServicosCusto_UsuarioCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContratoServicosCusto_UsuarioCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTRATOSERVICOSCUSTO_USUARIOCOD_data3T174( int A1478ContratoServicosCusto_ContratadaCod )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T003T24 */
         pr_default.execute(22, new Object[] {n1478ContratoServicosCusto_ContratadaCod, A1478ContratoServicosCusto_ContratadaCod});
         while ( (pr_default.getStatus(22) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T003T24_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T003T24_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(22);
         }
         pr_default.close(22);
      }

      public void Valid_Contratoservicoscusto_usuariocod( GXCombobox dynGX_Parm1 ,
                                                          int GX_Parm2 ,
                                                          String GX_Parm3 )
      {
         dynContratoServicosCusto_UsuarioCod = dynGX_Parm1;
         A1472ContratoServicosCusto_UsuarioCod = (int)(NumberUtil.Val( dynContratoServicosCusto_UsuarioCod.CurrentValue, "."));
         A1476ContratoServicosCusto_UsrPesCod = GX_Parm2;
         n1476ContratoServicosCusto_UsrPesCod = false;
         A1477ContratoServicosCusto_UsrPesNom = GX_Parm3;
         n1477ContratoServicosCusto_UsrPesNom = false;
         /* Using cursor T003T25 */
         pr_default.execute(23, new Object[] {A1472ContratoServicosCusto_UsuarioCod});
         if ( (pr_default.getStatus(23) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usu�rio'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSCUSTO_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = dynContratoServicosCusto_UsuarioCod_Internalname;
         }
         A1476ContratoServicosCusto_UsrPesCod = T003T25_A1476ContratoServicosCusto_UsrPesCod[0];
         n1476ContratoServicosCusto_UsrPesCod = T003T25_n1476ContratoServicosCusto_UsrPesCod[0];
         pr_default.close(23);
         /* Using cursor T003T26 */
         pr_default.execute(24, new Object[] {n1476ContratoServicosCusto_UsrPesCod, A1476ContratoServicosCusto_UsrPesCod});
         if ( (pr_default.getStatus(24) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1477ContratoServicosCusto_UsrPesNom = T003T26_A1477ContratoServicosCusto_UsrPesNom[0];
         n1477ContratoServicosCusto_UsrPesNom = T003T26_n1477ContratoServicosCusto_UsrPesNom[0];
         pr_default.close(24);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1476ContratoServicosCusto_UsrPesCod = 0;
            n1476ContratoServicosCusto_UsrPesCod = false;
            A1477ContratoServicosCusto_UsrPesNom = "";
            n1477ContratoServicosCusto_UsrPesNom = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1476ContratoServicosCusto_UsrPesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1477ContratoServicosCusto_UsrPesNom));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratoservicoscusto_cntsrvcod( int GX_Parm1 ,
                                                         int GX_Parm2 ,
                                                         int GX_Parm3 ,
                                                         GXCombobox dynGX_Parm4 )
      {
         A1471ContratoServicosCusto_CntSrvCod = GX_Parm1;
         A1479ContratoServicosCusto_CntCod = GX_Parm2;
         n1479ContratoServicosCusto_CntCod = false;
         A1478ContratoServicosCusto_ContratadaCod = GX_Parm3;
         n1478ContratoServicosCusto_ContratadaCod = false;
         dynContratoServicosCusto_UsuarioCod = dynGX_Parm4;
         A1472ContratoServicosCusto_UsuarioCod = (int)(NumberUtil.Val( dynContratoServicosCusto_UsuarioCod.CurrentValue, "."));
         /* Using cursor T003T27 */
         pr_default.execute(25, new Object[] {A1471ContratoServicosCusto_CntSrvCod});
         if ( (pr_default.getStatus(25) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servi�o do contrato'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSCUSTO_CNTSRVCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosCusto_CntSrvCod_Internalname;
         }
         A1479ContratoServicosCusto_CntCod = T003T27_A1479ContratoServicosCusto_CntCod[0];
         n1479ContratoServicosCusto_CntCod = T003T27_n1479ContratoServicosCusto_CntCod[0];
         pr_default.close(25);
         /* Using cursor T003T28 */
         pr_default.execute(26, new Object[] {n1479ContratoServicosCusto_CntCod, A1479ContratoServicosCusto_CntCod});
         if ( (pr_default.getStatus(26) != 101) )
         {
            A1478ContratoServicosCusto_ContratadaCod = T003T28_A1478ContratoServicosCusto_ContratadaCod[0];
            n1478ContratoServicosCusto_ContratadaCod = T003T28_n1478ContratoServicosCusto_ContratadaCod[0];
         }
         else
         {
            A1478ContratoServicosCusto_ContratadaCod = 0;
            n1478ContratoServicosCusto_ContratadaCod = false;
         }
         pr_default.close(26);
         GXACONTRATOSERVICOSCUSTO_USUARIOCOD_html3T174( A1478ContratoServicosCusto_ContratadaCod) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1479ContratoServicosCusto_CntCod = 0;
            n1479ContratoServicosCusto_CntCod = false;
            A1478ContratoServicosCusto_ContratadaCod = 0;
            n1478ContratoServicosCusto_ContratadaCod = false;
         }
         dynContratoServicosCusto_UsuarioCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1479ContratoServicosCusto_CntCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1478ContratoServicosCusto_ContratadaCod), 6, 0, ".", "")));
         if ( dynContratoServicosCusto_UsuarioCod.ItemCount > 0 )
         {
            A1472ContratoServicosCusto_UsuarioCod = (int)(NumberUtil.Val( dynContratoServicosCusto_UsuarioCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0))), "."));
         }
         dynContratoServicosCusto_UsuarioCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0));
         isValidOutput.Add(dynContratoServicosCusto_UsuarioCod);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratoservicoscusto_contratadacod( int GX_Parm1 ,
                                                             GXCombobox dynGX_Parm2 )
      {
         A1478ContratoServicosCusto_ContratadaCod = GX_Parm1;
         n1478ContratoServicosCusto_ContratadaCod = false;
         dynContratoServicosCusto_UsuarioCod = dynGX_Parm2;
         A1472ContratoServicosCusto_UsuarioCod = (int)(NumberUtil.Val( dynContratoServicosCusto_UsuarioCod.CurrentValue, "."));
         GXACONTRATOSERVICOSCUSTO_USUARIOCOD_html3T174( A1478ContratoServicosCusto_ContratadaCod) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         dynContratoServicosCusto_UsuarioCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0));
         if ( dynContratoServicosCusto_UsuarioCod.ItemCount > 0 )
         {
            A1472ContratoServicosCusto_UsuarioCod = (int)(NumberUtil.Val( dynContratoServicosCusto_UsuarioCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0))), "."));
         }
         dynContratoServicosCusto_UsuarioCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0));
         isValidOutput.Add(dynContratoServicosCusto_UsuarioCod);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoServicosCusto_Codigo',fld:'vCONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV14ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E123T2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(25);
         pr_default.close(17);
         pr_default.close(23);
         pr_default.close(19);
         pr_default.close(24);
         pr_default.close(20);
         pr_default.close(26);
         pr_default.close(18);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         lblTbjava_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontratoservicoscusto_usuariocod_Jsonclick = "";
         lblTextblockcontratoservicoscusto_cstuntprdnrm_Jsonclick = "";
         lblTextblockcontratoservicoscusto_cstuntprdext_Jsonclick = "";
         A1477ContratoServicosCusto_UsrPesNom = "";
         AV16Pgmname = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode174 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1477ContratoServicosCusto_UsrPesNom = "";
         T003T5_A1476ContratoServicosCusto_UsrPesCod = new int[1] ;
         T003T5_n1476ContratoServicosCusto_UsrPesCod = new bool[] {false} ;
         T003T6_A1477ContratoServicosCusto_UsrPesNom = new String[] {""} ;
         T003T6_n1477ContratoServicosCusto_UsrPesNom = new bool[] {false} ;
         T003T4_A1479ContratoServicosCusto_CntCod = new int[1] ;
         T003T4_n1479ContratoServicosCusto_CntCod = new bool[] {false} ;
         T003T7_A1478ContratoServicosCusto_ContratadaCod = new int[1] ;
         T003T7_n1478ContratoServicosCusto_ContratadaCod = new bool[] {false} ;
         T003T8_A74Contrato_Codigo = new int[1] ;
         T003T8_A1473ContratoServicosCusto_Codigo = new int[1] ;
         T003T8_A1477ContratoServicosCusto_UsrPesNom = new String[] {""} ;
         T003T8_n1477ContratoServicosCusto_UsrPesNom = new bool[] {false} ;
         T003T8_A1474ContratoServicosCusto_CstUntPrdNrm = new decimal[1] ;
         T003T8_A1475ContratoServicosCusto_CstUntPrdExt = new decimal[1] ;
         T003T8_n1475ContratoServicosCusto_CstUntPrdExt = new bool[] {false} ;
         T003T8_A1471ContratoServicosCusto_CntSrvCod = new int[1] ;
         T003T8_A1472ContratoServicosCusto_UsuarioCod = new int[1] ;
         T003T8_A1479ContratoServicosCusto_CntCod = new int[1] ;
         T003T8_n1479ContratoServicosCusto_CntCod = new bool[] {false} ;
         T003T8_A1476ContratoServicosCusto_UsrPesCod = new int[1] ;
         T003T8_n1476ContratoServicosCusto_UsrPesCod = new bool[] {false} ;
         T003T8_A1478ContratoServicosCusto_ContratadaCod = new int[1] ;
         T003T8_n1478ContratoServicosCusto_ContratadaCod = new bool[] {false} ;
         T003T9_A1479ContratoServicosCusto_CntCod = new int[1] ;
         T003T9_n1479ContratoServicosCusto_CntCod = new bool[] {false} ;
         T003T10_A1478ContratoServicosCusto_ContratadaCod = new int[1] ;
         T003T10_n1478ContratoServicosCusto_ContratadaCod = new bool[] {false} ;
         T003T11_A1476ContratoServicosCusto_UsrPesCod = new int[1] ;
         T003T11_n1476ContratoServicosCusto_UsrPesCod = new bool[] {false} ;
         T003T12_A1477ContratoServicosCusto_UsrPesNom = new String[] {""} ;
         T003T12_n1477ContratoServicosCusto_UsrPesNom = new bool[] {false} ;
         T003T13_A1473ContratoServicosCusto_Codigo = new int[1] ;
         T003T3_A1473ContratoServicosCusto_Codigo = new int[1] ;
         T003T3_A1474ContratoServicosCusto_CstUntPrdNrm = new decimal[1] ;
         T003T3_A1475ContratoServicosCusto_CstUntPrdExt = new decimal[1] ;
         T003T3_n1475ContratoServicosCusto_CstUntPrdExt = new bool[] {false} ;
         T003T3_A1471ContratoServicosCusto_CntSrvCod = new int[1] ;
         T003T3_A1472ContratoServicosCusto_UsuarioCod = new int[1] ;
         T003T14_A1473ContratoServicosCusto_Codigo = new int[1] ;
         T003T15_A1473ContratoServicosCusto_Codigo = new int[1] ;
         T003T2_A1473ContratoServicosCusto_Codigo = new int[1] ;
         T003T2_A1474ContratoServicosCusto_CstUntPrdNrm = new decimal[1] ;
         T003T2_A1475ContratoServicosCusto_CstUntPrdExt = new decimal[1] ;
         T003T2_n1475ContratoServicosCusto_CstUntPrdExt = new bool[] {false} ;
         T003T2_A1471ContratoServicosCusto_CntSrvCod = new int[1] ;
         T003T2_A1472ContratoServicosCusto_UsuarioCod = new int[1] ;
         T003T16_A1473ContratoServicosCusto_Codigo = new int[1] ;
         T003T19_A1479ContratoServicosCusto_CntCod = new int[1] ;
         T003T19_n1479ContratoServicosCusto_CntCod = new bool[] {false} ;
         T003T20_A1478ContratoServicosCusto_ContratadaCod = new int[1] ;
         T003T20_n1478ContratoServicosCusto_ContratadaCod = new bool[] {false} ;
         T003T21_A1476ContratoServicosCusto_UsrPesCod = new int[1] ;
         T003T21_n1476ContratoServicosCusto_UsrPesCod = new bool[] {false} ;
         T003T22_A1477ContratoServicosCusto_UsrPesNom = new String[] {""} ;
         T003T22_n1477ContratoServicosCusto_UsrPesNom = new bool[] {false} ;
         T003T23_A1473ContratoServicosCusto_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T003T24_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         T003T24_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         T003T24_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         T003T24_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         T003T24_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         T003T24_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         T003T24_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         T003T24_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         T003T25_A1476ContratoServicosCusto_UsrPesCod = new int[1] ;
         T003T25_n1476ContratoServicosCusto_UsrPesCod = new bool[] {false} ;
         T003T26_A1477ContratoServicosCusto_UsrPesNom = new String[] {""} ;
         T003T26_n1477ContratoServicosCusto_UsrPesNom = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T003T27_A1479ContratoServicosCusto_CntCod = new int[1] ;
         T003T27_n1479ContratoServicosCusto_CntCod = new bool[] {false} ;
         T003T28_A1478ContratoServicosCusto_ContratadaCod = new int[1] ;
         T003T28_n1478ContratoServicosCusto_ContratadaCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicoscusto__default(),
            new Object[][] {
                new Object[] {
               T003T2_A1473ContratoServicosCusto_Codigo, T003T2_A1474ContratoServicosCusto_CstUntPrdNrm, T003T2_A1475ContratoServicosCusto_CstUntPrdExt, T003T2_n1475ContratoServicosCusto_CstUntPrdExt, T003T2_A1471ContratoServicosCusto_CntSrvCod, T003T2_A1472ContratoServicosCusto_UsuarioCod
               }
               , new Object[] {
               T003T3_A1473ContratoServicosCusto_Codigo, T003T3_A1474ContratoServicosCusto_CstUntPrdNrm, T003T3_A1475ContratoServicosCusto_CstUntPrdExt, T003T3_n1475ContratoServicosCusto_CstUntPrdExt, T003T3_A1471ContratoServicosCusto_CntSrvCod, T003T3_A1472ContratoServicosCusto_UsuarioCod
               }
               , new Object[] {
               T003T4_A1479ContratoServicosCusto_CntCod, T003T4_n1479ContratoServicosCusto_CntCod
               }
               , new Object[] {
               T003T5_A1476ContratoServicosCusto_UsrPesCod, T003T5_n1476ContratoServicosCusto_UsrPesCod
               }
               , new Object[] {
               T003T6_A1477ContratoServicosCusto_UsrPesNom, T003T6_n1477ContratoServicosCusto_UsrPesNom
               }
               , new Object[] {
               T003T7_A1478ContratoServicosCusto_ContratadaCod, T003T7_n1478ContratoServicosCusto_ContratadaCod
               }
               , new Object[] {
               T003T8_A74Contrato_Codigo, T003T8_A1473ContratoServicosCusto_Codigo, T003T8_A1477ContratoServicosCusto_UsrPesNom, T003T8_n1477ContratoServicosCusto_UsrPesNom, T003T8_A1474ContratoServicosCusto_CstUntPrdNrm, T003T8_A1475ContratoServicosCusto_CstUntPrdExt, T003T8_n1475ContratoServicosCusto_CstUntPrdExt, T003T8_A1471ContratoServicosCusto_CntSrvCod, T003T8_A1472ContratoServicosCusto_UsuarioCod, T003T8_A1479ContratoServicosCusto_CntCod,
               T003T8_n1479ContratoServicosCusto_CntCod, T003T8_A1476ContratoServicosCusto_UsrPesCod, T003T8_n1476ContratoServicosCusto_UsrPesCod, T003T8_A1478ContratoServicosCusto_ContratadaCod, T003T8_n1478ContratoServicosCusto_ContratadaCod
               }
               , new Object[] {
               T003T9_A1479ContratoServicosCusto_CntCod, T003T9_n1479ContratoServicosCusto_CntCod
               }
               , new Object[] {
               T003T10_A1478ContratoServicosCusto_ContratadaCod, T003T10_n1478ContratoServicosCusto_ContratadaCod
               }
               , new Object[] {
               T003T11_A1476ContratoServicosCusto_UsrPesCod, T003T11_n1476ContratoServicosCusto_UsrPesCod
               }
               , new Object[] {
               T003T12_A1477ContratoServicosCusto_UsrPesNom, T003T12_n1477ContratoServicosCusto_UsrPesNom
               }
               , new Object[] {
               T003T13_A1473ContratoServicosCusto_Codigo
               }
               , new Object[] {
               T003T14_A1473ContratoServicosCusto_Codigo
               }
               , new Object[] {
               T003T15_A1473ContratoServicosCusto_Codigo
               }
               , new Object[] {
               T003T16_A1473ContratoServicosCusto_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003T19_A1479ContratoServicosCusto_CntCod, T003T19_n1479ContratoServicosCusto_CntCod
               }
               , new Object[] {
               T003T20_A1478ContratoServicosCusto_ContratadaCod, T003T20_n1478ContratoServicosCusto_ContratadaCod
               }
               , new Object[] {
               T003T21_A1476ContratoServicosCusto_UsrPesCod, T003T21_n1476ContratoServicosCusto_UsrPesCod
               }
               , new Object[] {
               T003T22_A1477ContratoServicosCusto_UsrPesNom, T003T22_n1477ContratoServicosCusto_UsrPesNom
               }
               , new Object[] {
               T003T23_A1473ContratoServicosCusto_Codigo
               }
               , new Object[] {
               T003T24_A70ContratadaUsuario_UsuarioPessoaCod, T003T24_n70ContratadaUsuario_UsuarioPessoaCod, T003T24_A69ContratadaUsuario_UsuarioCod, T003T24_A71ContratadaUsuario_UsuarioPessoaNom, T003T24_n71ContratadaUsuario_UsuarioPessoaNom, T003T24_A66ContratadaUsuario_ContratadaCod, T003T24_A1394ContratadaUsuario_UsuarioAtivo, T003T24_n1394ContratadaUsuario_UsuarioAtivo
               }
               , new Object[] {
               T003T25_A1476ContratoServicosCusto_UsrPesCod, T003T25_n1476ContratoServicosCusto_UsrPesCod
               }
               , new Object[] {
               T003T26_A1477ContratoServicosCusto_UsrPesNom, T003T26_n1477ContratoServicosCusto_UsrPesNom
               }
               , new Object[] {
               T003T27_A1479ContratoServicosCusto_CntCod, T003T27_n1479ContratoServicosCusto_CntCod
               }
               , new Object[] {
               T003T28_A1478ContratoServicosCusto_ContratadaCod, T003T28_n1478ContratoServicosCusto_ContratadaCod
               }
            }
         );
         AV16Pgmname = "ContratoServicosCusto";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound174 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7ContratoServicosCusto_Codigo ;
      private int wcpOAV14ContratoServicosCusto_CntSrvCod ;
      private int Z1473ContratoServicosCusto_Codigo ;
      private int Z1471ContratoServicosCusto_CntSrvCod ;
      private int Z1472ContratoServicosCusto_UsuarioCod ;
      private int N1471ContratoServicosCusto_CntSrvCod ;
      private int N1472ContratoServicosCusto_UsuarioCod ;
      private int A1478ContratoServicosCusto_ContratadaCod ;
      private int A1471ContratoServicosCusto_CntSrvCod ;
      private int A1479ContratoServicosCusto_CntCod ;
      private int A1472ContratoServicosCusto_UsuarioCod ;
      private int A1476ContratoServicosCusto_UsrPesCod ;
      private int AV7ContratoServicosCusto_Codigo ;
      private int AV14ContratoServicosCusto_CntSrvCod ;
      private int trnEnded ;
      private int A1473ContratoServicosCusto_Codigo ;
      private int edtContratoServicosCusto_Codigo_Enabled ;
      private int edtContratoServicosCusto_Codigo_Visible ;
      private int edtContratoServicosCusto_CntSrvCod_Visible ;
      private int edtContratoServicosCusto_CntSrvCod_Enabled ;
      private int edtContratoServicosCusto_ContratadaCod_Enabled ;
      private int edtContratoServicosCusto_ContratadaCod_Visible ;
      private int lblTbjava_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContratoServicosCusto_CstUntPrdNrm_Enabled ;
      private int edtContratoServicosCusto_CstUntPrdExt_Enabled ;
      private int AV11Insert_ContratoServicosCusto_CntSrvCod ;
      private int AV12Insert_ContratoServicosCusto_UsuarioCod ;
      private int AV17GXV1 ;
      private int Z1479ContratoServicosCusto_CntCod ;
      private int Z1478ContratoServicosCusto_ContratadaCod ;
      private int Z1476ContratoServicosCusto_UsrPesCod ;
      private int i1471ContratoServicosCusto_CntSrvCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal Z1474ContratoServicosCusto_CstUntPrdNrm ;
      private decimal Z1475ContratoServicosCusto_CstUntPrdExt ;
      private decimal A1474ContratoServicosCusto_CstUntPrdNrm ;
      private decimal A1475ContratoServicosCusto_CstUntPrdExt ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynContratoServicosCusto_UsuarioCod_Internalname ;
      private String edtContratoServicosCusto_Codigo_Internalname ;
      private String edtContratoServicosCusto_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtContratoServicosCusto_CntSrvCod_Internalname ;
      private String edtContratoServicosCusto_CntSrvCod_Jsonclick ;
      private String edtContratoServicosCusto_ContratadaCod_Internalname ;
      private String edtContratoServicosCusto_ContratadaCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratoservicoscusto_usuariocod_Internalname ;
      private String lblTextblockcontratoservicoscusto_usuariocod_Jsonclick ;
      private String dynContratoServicosCusto_UsuarioCod_Jsonclick ;
      private String lblTextblockcontratoservicoscusto_cstuntprdnrm_Internalname ;
      private String lblTextblockcontratoservicoscusto_cstuntprdnrm_Jsonclick ;
      private String edtContratoServicosCusto_CstUntPrdNrm_Internalname ;
      private String edtContratoServicosCusto_CstUntPrdNrm_Jsonclick ;
      private String lblTextblockcontratoservicoscusto_cstuntprdext_Internalname ;
      private String lblTextblockcontratoservicoscusto_cstuntprdext_Jsonclick ;
      private String edtContratoServicosCusto_CstUntPrdExt_Internalname ;
      private String edtContratoServicosCusto_CstUntPrdExt_Jsonclick ;
      private String A1477ContratoServicosCusto_UsrPesNom ;
      private String AV16Pgmname ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode174 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1477ContratoServicosCusto_UsrPesNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool n1478ContratoServicosCusto_ContratadaCod ;
      private bool n1479ContratoServicosCusto_CntCod ;
      private bool n1476ContratoServicosCusto_UsrPesCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1475ContratoServicosCusto_CstUntPrdExt ;
      private bool n1477ContratoServicosCusto_UsrPesNom ;
      private bool returnInSub ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_ContratoServicosCusto_CntSrvCod ;
      private GXCombobox dynContratoServicosCusto_UsuarioCod ;
      private IDataStoreProvider pr_default ;
      private int[] T003T5_A1476ContratoServicosCusto_UsrPesCod ;
      private bool[] T003T5_n1476ContratoServicosCusto_UsrPesCod ;
      private String[] T003T6_A1477ContratoServicosCusto_UsrPesNom ;
      private bool[] T003T6_n1477ContratoServicosCusto_UsrPesNom ;
      private int[] T003T4_A1479ContratoServicosCusto_CntCod ;
      private bool[] T003T4_n1479ContratoServicosCusto_CntCod ;
      private int[] T003T7_A1478ContratoServicosCusto_ContratadaCod ;
      private bool[] T003T7_n1478ContratoServicosCusto_ContratadaCod ;
      private int[] T003T8_A74Contrato_Codigo ;
      private int[] T003T8_A1473ContratoServicosCusto_Codigo ;
      private String[] T003T8_A1477ContratoServicosCusto_UsrPesNom ;
      private bool[] T003T8_n1477ContratoServicosCusto_UsrPesNom ;
      private decimal[] T003T8_A1474ContratoServicosCusto_CstUntPrdNrm ;
      private decimal[] T003T8_A1475ContratoServicosCusto_CstUntPrdExt ;
      private bool[] T003T8_n1475ContratoServicosCusto_CstUntPrdExt ;
      private int[] T003T8_A1471ContratoServicosCusto_CntSrvCod ;
      private int[] T003T8_A1472ContratoServicosCusto_UsuarioCod ;
      private int[] T003T8_A1479ContratoServicosCusto_CntCod ;
      private bool[] T003T8_n1479ContratoServicosCusto_CntCod ;
      private int[] T003T8_A1476ContratoServicosCusto_UsrPesCod ;
      private bool[] T003T8_n1476ContratoServicosCusto_UsrPesCod ;
      private int[] T003T8_A1478ContratoServicosCusto_ContratadaCod ;
      private bool[] T003T8_n1478ContratoServicosCusto_ContratadaCod ;
      private int[] T003T9_A1479ContratoServicosCusto_CntCod ;
      private bool[] T003T9_n1479ContratoServicosCusto_CntCod ;
      private int[] T003T10_A1478ContratoServicosCusto_ContratadaCod ;
      private bool[] T003T10_n1478ContratoServicosCusto_ContratadaCod ;
      private int[] T003T11_A1476ContratoServicosCusto_UsrPesCod ;
      private bool[] T003T11_n1476ContratoServicosCusto_UsrPesCod ;
      private String[] T003T12_A1477ContratoServicosCusto_UsrPesNom ;
      private bool[] T003T12_n1477ContratoServicosCusto_UsrPesNom ;
      private int[] T003T13_A1473ContratoServicosCusto_Codigo ;
      private int[] T003T3_A1473ContratoServicosCusto_Codigo ;
      private decimal[] T003T3_A1474ContratoServicosCusto_CstUntPrdNrm ;
      private decimal[] T003T3_A1475ContratoServicosCusto_CstUntPrdExt ;
      private bool[] T003T3_n1475ContratoServicosCusto_CstUntPrdExt ;
      private int[] T003T3_A1471ContratoServicosCusto_CntSrvCod ;
      private int[] T003T3_A1472ContratoServicosCusto_UsuarioCod ;
      private int[] T003T14_A1473ContratoServicosCusto_Codigo ;
      private int[] T003T15_A1473ContratoServicosCusto_Codigo ;
      private int[] T003T2_A1473ContratoServicosCusto_Codigo ;
      private decimal[] T003T2_A1474ContratoServicosCusto_CstUntPrdNrm ;
      private decimal[] T003T2_A1475ContratoServicosCusto_CstUntPrdExt ;
      private bool[] T003T2_n1475ContratoServicosCusto_CstUntPrdExt ;
      private int[] T003T2_A1471ContratoServicosCusto_CntSrvCod ;
      private int[] T003T2_A1472ContratoServicosCusto_UsuarioCod ;
      private int[] T003T16_A1473ContratoServicosCusto_Codigo ;
      private int[] T003T19_A1479ContratoServicosCusto_CntCod ;
      private bool[] T003T19_n1479ContratoServicosCusto_CntCod ;
      private int[] T003T20_A1478ContratoServicosCusto_ContratadaCod ;
      private bool[] T003T20_n1478ContratoServicosCusto_ContratadaCod ;
      private int[] T003T21_A1476ContratoServicosCusto_UsrPesCod ;
      private bool[] T003T21_n1476ContratoServicosCusto_UsrPesCod ;
      private String[] T003T22_A1477ContratoServicosCusto_UsrPesNom ;
      private bool[] T003T22_n1477ContratoServicosCusto_UsrPesNom ;
      private int[] T003T23_A1473ContratoServicosCusto_Codigo ;
      private int[] T003T24_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] T003T24_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] T003T24_A69ContratadaUsuario_UsuarioCod ;
      private String[] T003T24_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] T003T24_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] T003T24_A66ContratadaUsuario_ContratadaCod ;
      private bool[] T003T24_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] T003T24_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] T003T25_A1476ContratoServicosCusto_UsrPesCod ;
      private bool[] T003T25_n1476ContratoServicosCusto_UsrPesCod ;
      private String[] T003T26_A1477ContratoServicosCusto_UsrPesNom ;
      private bool[] T003T26_n1477ContratoServicosCusto_UsrPesNom ;
      private int[] T003T27_A1479ContratoServicosCusto_CntCod ;
      private bool[] T003T27_n1479ContratoServicosCusto_CntCod ;
      private int[] T003T28_A1478ContratoServicosCusto_ContratadaCod ;
      private bool[] T003T28_n1478ContratoServicosCusto_ContratadaCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class contratoservicoscusto__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003T8 ;
          prmT003T8 = new Object[] {
          new Object[] {"@ContratoServicosCusto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T4 ;
          prmT003T4 = new Object[] {
          new Object[] {"@ContratoServicosCusto_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T7 ;
          prmT003T7 = new Object[] {
          new Object[] {"@ContratoServicosCusto_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T5 ;
          prmT003T5 = new Object[] {
          new Object[] {"@ContratoServicosCusto_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T6 ;
          prmT003T6 = new Object[] {
          new Object[] {"@ContratoServicosCusto_UsrPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T9 ;
          prmT003T9 = new Object[] {
          new Object[] {"@ContratoServicosCusto_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T10 ;
          prmT003T10 = new Object[] {
          new Object[] {"@ContratoServicosCusto_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T11 ;
          prmT003T11 = new Object[] {
          new Object[] {"@ContratoServicosCusto_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T12 ;
          prmT003T12 = new Object[] {
          new Object[] {"@ContratoServicosCusto_UsrPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T13 ;
          prmT003T13 = new Object[] {
          new Object[] {"@ContratoServicosCusto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T3 ;
          prmT003T3 = new Object[] {
          new Object[] {"@ContratoServicosCusto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T14 ;
          prmT003T14 = new Object[] {
          new Object[] {"@ContratoServicosCusto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T15 ;
          prmT003T15 = new Object[] {
          new Object[] {"@ContratoServicosCusto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T2 ;
          prmT003T2 = new Object[] {
          new Object[] {"@ContratoServicosCusto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T16 ;
          prmT003T16 = new Object[] {
          new Object[] {"@ContratoServicosCusto_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratoServicosCusto_CstUntPrdExt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratoServicosCusto_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosCusto_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T17 ;
          prmT003T17 = new Object[] {
          new Object[] {"@ContratoServicosCusto_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratoServicosCusto_CstUntPrdExt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratoServicosCusto_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosCusto_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosCusto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T18 ;
          prmT003T18 = new Object[] {
          new Object[] {"@ContratoServicosCusto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T19 ;
          prmT003T19 = new Object[] {
          new Object[] {"@ContratoServicosCusto_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T20 ;
          prmT003T20 = new Object[] {
          new Object[] {"@ContratoServicosCusto_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T21 ;
          prmT003T21 = new Object[] {
          new Object[] {"@ContratoServicosCusto_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T22 ;
          prmT003T22 = new Object[] {
          new Object[] {"@ContratoServicosCusto_UsrPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T23 ;
          prmT003T23 = new Object[] {
          } ;
          Object[] prmT003T24 ;
          prmT003T24 = new Object[] {
          new Object[] {"@ContratoServicosCusto_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T25 ;
          prmT003T25 = new Object[] {
          new Object[] {"@ContratoServicosCusto_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T26 ;
          prmT003T26 = new Object[] {
          new Object[] {"@ContratoServicosCusto_UsrPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T27 ;
          prmT003T27 = new Object[] {
          new Object[] {"@ContratoServicosCusto_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003T28 ;
          prmT003T28 = new Object[] {
          new Object[] {"@ContratoServicosCusto_CntCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003T2", "SELECT [ContratoServicosCusto_Codigo], [ContratoServicosCusto_CstUntPrdNrm], [ContratoServicosCusto_CstUntPrdExt], [ContratoServicosCusto_CntSrvCod] AS ContratoServicosCusto_CntSrvCod, [ContratoServicosCusto_UsuarioCod] AS ContratoServicosCusto_UsuarioCod FROM [ContratoServicosCusto] WITH (UPDLOCK) WHERE [ContratoServicosCusto_Codigo] = @ContratoServicosCusto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T2,1,0,true,false )
             ,new CursorDef("T003T3", "SELECT [ContratoServicosCusto_Codigo], [ContratoServicosCusto_CstUntPrdNrm], [ContratoServicosCusto_CstUntPrdExt], [ContratoServicosCusto_CntSrvCod] AS ContratoServicosCusto_CntSrvCod, [ContratoServicosCusto_UsuarioCod] AS ContratoServicosCusto_UsuarioCod FROM [ContratoServicosCusto] WITH (NOLOCK) WHERE [ContratoServicosCusto_Codigo] = @ContratoServicosCusto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T3,1,0,true,false )
             ,new CursorDef("T003T4", "SELECT [Contrato_Codigo] AS ContratoServicosCusto_CntCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosCusto_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T4,1,0,true,false )
             ,new CursorDef("T003T5", "SELECT [Usuario_PessoaCod] AS ContratoServicosCusto_UsrPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratoServicosCusto_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T5,1,0,true,false )
             ,new CursorDef("T003T6", "SELECT [Pessoa_Nome] AS ContratoServicosCusto_UsrPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratoServicosCusto_UsrPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T6,1,0,true,false )
             ,new CursorDef("T003T7", "SELECT COALESCE( [Contratada_Codigo], 0) AS ContratoServicosCusto_ContratadaCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoServicosCusto_CntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T7,1,0,true,false )
             ,new CursorDef("T003T8", "SELECT T3.[Contrato_Codigo], TM1.[ContratoServicosCusto_Codigo], T5.[Pessoa_Nome] AS ContratoServicosCusto_UsrPesNom, TM1.[ContratoServicosCusto_CstUntPrdNrm], TM1.[ContratoServicosCusto_CstUntPrdExt], TM1.[ContratoServicosCusto_CntSrvCod] AS ContratoServicosCusto_CntSrvCod, TM1.[ContratoServicosCusto_UsuarioCod] AS ContratoServicosCusto_UsuarioCod, T2.[Contrato_Codigo] AS ContratoServicosCusto_CntCod, T4.[Usuario_PessoaCod] AS ContratoServicosCusto_UsrPesCod, COALESCE( T3.[Contratada_Codigo], 0) AS ContratoServicosCusto_ContratadaCod FROM (((([ContratoServicosCusto] TM1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = TM1.[ContratoServicosCusto_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = TM1.[ContratoServicosCusto_UsuarioCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) WHERE TM1.[ContratoServicosCusto_Codigo] = @ContratoServicosCusto_Codigo ORDER BY TM1.[ContratoServicosCusto_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003T8,100,0,true,false )
             ,new CursorDef("T003T9", "SELECT [Contrato_Codigo] AS ContratoServicosCusto_CntCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosCusto_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T9,1,0,true,false )
             ,new CursorDef("T003T10", "SELECT COALESCE( [Contratada_Codigo], 0) AS ContratoServicosCusto_ContratadaCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoServicosCusto_CntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T10,1,0,true,false )
             ,new CursorDef("T003T11", "SELECT [Usuario_PessoaCod] AS ContratoServicosCusto_UsrPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratoServicosCusto_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T11,1,0,true,false )
             ,new CursorDef("T003T12", "SELECT [Pessoa_Nome] AS ContratoServicosCusto_UsrPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratoServicosCusto_UsrPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T12,1,0,true,false )
             ,new CursorDef("T003T13", "SELECT [ContratoServicosCusto_Codigo] FROM [ContratoServicosCusto] WITH (NOLOCK) WHERE [ContratoServicosCusto_Codigo] = @ContratoServicosCusto_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003T13,1,0,true,false )
             ,new CursorDef("T003T14", "SELECT TOP 1 [ContratoServicosCusto_Codigo] FROM [ContratoServicosCusto] WITH (NOLOCK) WHERE ( [ContratoServicosCusto_Codigo] > @ContratoServicosCusto_Codigo) ORDER BY [ContratoServicosCusto_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003T14,1,0,true,true )
             ,new CursorDef("T003T15", "SELECT TOP 1 [ContratoServicosCusto_Codigo] FROM [ContratoServicosCusto] WITH (NOLOCK) WHERE ( [ContratoServicosCusto_Codigo] < @ContratoServicosCusto_Codigo) ORDER BY [ContratoServicosCusto_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003T15,1,0,true,true )
             ,new CursorDef("T003T16", "INSERT INTO [ContratoServicosCusto]([ContratoServicosCusto_CstUntPrdNrm], [ContratoServicosCusto_CstUntPrdExt], [ContratoServicosCusto_CntSrvCod], [ContratoServicosCusto_UsuarioCod]) VALUES(@ContratoServicosCusto_CstUntPrdNrm, @ContratoServicosCusto_CstUntPrdExt, @ContratoServicosCusto_CntSrvCod, @ContratoServicosCusto_UsuarioCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003T16)
             ,new CursorDef("T003T17", "UPDATE [ContratoServicosCusto] SET [ContratoServicosCusto_CstUntPrdNrm]=@ContratoServicosCusto_CstUntPrdNrm, [ContratoServicosCusto_CstUntPrdExt]=@ContratoServicosCusto_CstUntPrdExt, [ContratoServicosCusto_CntSrvCod]=@ContratoServicosCusto_CntSrvCod, [ContratoServicosCusto_UsuarioCod]=@ContratoServicosCusto_UsuarioCod  WHERE [ContratoServicosCusto_Codigo] = @ContratoServicosCusto_Codigo", GxErrorMask.GX_NOMASK,prmT003T17)
             ,new CursorDef("T003T18", "DELETE FROM [ContratoServicosCusto]  WHERE [ContratoServicosCusto_Codigo] = @ContratoServicosCusto_Codigo", GxErrorMask.GX_NOMASK,prmT003T18)
             ,new CursorDef("T003T19", "SELECT [Contrato_Codigo] AS ContratoServicosCusto_CntCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosCusto_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T19,1,0,true,false )
             ,new CursorDef("T003T20", "SELECT COALESCE( [Contratada_Codigo], 0) AS ContratoServicosCusto_ContratadaCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoServicosCusto_CntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T20,1,0,true,false )
             ,new CursorDef("T003T21", "SELECT [Usuario_PessoaCod] AS ContratoServicosCusto_UsrPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratoServicosCusto_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T21,1,0,true,false )
             ,new CursorDef("T003T22", "SELECT [Pessoa_Nome] AS ContratoServicosCusto_UsrPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratoServicosCusto_UsrPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T22,1,0,true,false )
             ,new CursorDef("T003T23", "SELECT [ContratoServicosCusto_Codigo] FROM [ContratoServicosCusto] WITH (NOLOCK) ORDER BY [ContratoServicosCusto_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003T23,100,0,true,false )
             ,new CursorDef("T003T24", "SELECT T3.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_ContratadaCod], T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE (T2.[Usuario_Ativo] = 1) AND (T1.[ContratadaUsuario_ContratadaCod] = @ContratoServicosCusto_ContratadaCod) ORDER BY T3.[Pessoa_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T24,0,0,true,false )
             ,new CursorDef("T003T25", "SELECT [Usuario_PessoaCod] AS ContratoServicosCusto_UsrPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratoServicosCusto_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T25,1,0,true,false )
             ,new CursorDef("T003T26", "SELECT [Pessoa_Nome] AS ContratoServicosCusto_UsrPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratoServicosCusto_UsrPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T26,1,0,true,false )
             ,new CursorDef("T003T27", "SELECT [Contrato_Codigo] AS ContratoServicosCusto_CntCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosCusto_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T27,1,0,true,false )
             ,new CursorDef("T003T28", "SELECT COALESCE( [Contratada_Codigo], 0) AS ContratoServicosCusto_ContratadaCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoServicosCusto_CntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003T28,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 24 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (decimal)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                stmt.SetParameter(4, (int)parms[4]);
                return;
             case 15 :
                stmt.SetParameter(1, (decimal)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                stmt.SetParameter(4, (int)parms[4]);
                stmt.SetParameter(5, (int)parms[5]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
