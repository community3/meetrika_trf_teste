/*
               File: ProjetoEstimativas
        Description: Projeto Estimativas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:49:43.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class projetoestimativas : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public projetoestimativas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public projetoestimativas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Projeto_Codigo )
      {
         this.A648Projeto_Codigo = aP0_Projeto_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A648Projeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A648Projeto_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAGU2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV22Pgmname = "ProjetoEstimativas";
               context.Gx_err = 0;
               edtavVariavel1_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel1_Enabled), 5, 0)));
               edtavVariavel2_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel2_Enabled), 5, 0)));
               edtavVariavel3_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel3_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel3_Enabled), 5, 0)));
               edtavVariavel4_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel4_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel4_Enabled), 5, 0)));
               edtavVariavel5_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel5_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel5_Enabled), 5, 0)));
               edtavVariavel6_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel6_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel6_Enabled), 5, 0)));
               edtavVariavel7_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel7_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel7_Enabled), 5, 0)));
               edtavVariavel8_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel8_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel8_Enabled), 5, 0)));
               WSGU2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Projeto Estimativas") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042822494323");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("projetoestimativas.aspx") + "?" + UrlEncode("" +A648Projeto_Codigo)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA648Projeto_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA648Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"PROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vVARIAVEL1", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV12Variavel1, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vVARIAVEL2", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV13Variavel2, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vVARIAVEL3", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV14Variavel3, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vVARIAVEL4", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV15Variavel4, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vVARIAVEL5", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV16Variavel5, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vVARIAVEL6", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV17Variavel6, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vVARIAVEL7", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV18Variavel7, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vVARIAVEL8", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV19Variavel8, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormGU2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("projetoestimativas.js", "?202042822494325");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ProjetoEstimativas" ;
      }

      public override String GetPgmdesc( )
      {
         return "Projeto Estimativas" ;
      }

      protected void WBGU0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "projetoestimativas.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_GU2( true) ;
         }
         else
         {
            wb_table1_2_GU2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_GU2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTGU2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Projeto Estimativas", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPGU0( ) ;
            }
         }
      }

      protected void WSGU2( )
      {
         STARTGU2( ) ;
         EVTGU2( ) ;
      }

      protected void EVTGU2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11GU2 */
                                    E11GU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12GU2 */
                                    E12GU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavVariavel1_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEGU2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormGU2( ) ;
            }
         }
      }

      protected void PAGU2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavVariavel1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFGU2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV22Pgmname = "ProjetoEstimativas";
         context.Gx_err = 0;
         edtavVariavel1_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel1_Enabled), 5, 0)));
         edtavVariavel2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel2_Enabled), 5, 0)));
         edtavVariavel3_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel3_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel3_Enabled), 5, 0)));
         edtavVariavel4_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel4_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel4_Enabled), 5, 0)));
         edtavVariavel5_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel5_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel5_Enabled), 5, 0)));
         edtavVariavel6_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel6_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel6_Enabled), 5, 0)));
         edtavVariavel7_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel7_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel7_Enabled), 5, 0)));
         edtavVariavel8_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel8_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel8_Enabled), 5, 0)));
      }

      protected void RFGU2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E12GU2 */
            E12GU2 ();
            WBGU0( ) ;
         }
      }

      protected void STRUPGU0( )
      {
         /* Before Start, stand alone formulas. */
         AV22Pgmname = "ProjetoEstimativas";
         context.Gx_err = 0;
         edtavVariavel1_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel1_Enabled), 5, 0)));
         edtavVariavel2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel2_Enabled), 5, 0)));
         edtavVariavel3_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel3_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel3_Enabled), 5, 0)));
         edtavVariavel4_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel4_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel4_Enabled), 5, 0)));
         edtavVariavel5_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel5_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel5_Enabled), 5, 0)));
         edtavVariavel6_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel6_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel6_Enabled), 5, 0)));
         edtavVariavel7_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel7_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel7_Enabled), 5, 0)));
         edtavVariavel8_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavVariavel8_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavel8_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11GU2 */
         E11GU2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            wcpOA648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA648Projeto_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11GU2 */
         E11GU2 ();
         if (returnInSub) return;
      }

      protected void E11GU2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if (returnInSub) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E12GU2( )
      {
         /* Load Routine */
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV22Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Projeto";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Projeto_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Projeto_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_GU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavel1_Internalname, "Vari�vel", "", "", lblTextblockvariavel1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoEstimativas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 7,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVariavel1_Internalname, StringUtil.LTrim( StringUtil.NToC( AV12Variavel1, 18, 5, ",", "")), ((edtavVariavel1_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV12Variavel1, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV12Variavel1, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,7);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVariavel1_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavVariavel1_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, false, "Valor", "right", false, "HLP_ProjetoEstimativas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavel2_Internalname, "Vari�vel", "", "", lblTextblockvariavel2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoEstimativas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVariavel2_Internalname, StringUtil.LTrim( StringUtil.NToC( AV13Variavel2, 18, 5, ",", "")), ((edtavVariavel2_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV13Variavel2, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV13Variavel2, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,12);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVariavel2_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavVariavel2_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, false, "Valor", "right", false, "HLP_ProjetoEstimativas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavel3_Internalname, "Vari�vel", "", "", lblTextblockvariavel3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoEstimativas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVariavel3_Internalname, StringUtil.LTrim( StringUtil.NToC( AV14Variavel3, 18, 5, ",", "")), ((edtavVariavel3_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV14Variavel3, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV14Variavel3, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,17);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVariavel3_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavVariavel3_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, false, "Valor", "right", false, "HLP_ProjetoEstimativas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavel4_Internalname, "Vari�vel", "", "", lblTextblockvariavel4_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoEstimativas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVariavel4_Internalname, StringUtil.LTrim( StringUtil.NToC( AV15Variavel4, 18, 5, ",", "")), ((edtavVariavel4_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV15Variavel4, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV15Variavel4, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,22);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVariavel4_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavVariavel4_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, false, "Valor", "right", false, "HLP_ProjetoEstimativas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavel5_Internalname, "Vari�vel", "", "", lblTextblockvariavel5_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoEstimativas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVariavel5_Internalname, StringUtil.LTrim( StringUtil.NToC( AV16Variavel5, 18, 5, ",", "")), ((edtavVariavel5_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV16Variavel5, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV16Variavel5, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,27);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVariavel5_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavVariavel5_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, false, "Valor", "right", false, "HLP_ProjetoEstimativas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavel6_Internalname, "Vari�vel", "", "", lblTextblockvariavel6_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoEstimativas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVariavel6_Internalname, StringUtil.LTrim( StringUtil.NToC( AV17Variavel6, 18, 5, ",", "")), ((edtavVariavel6_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV17Variavel6, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV17Variavel6, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,32);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVariavel6_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavVariavel6_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, false, "Valor", "right", false, "HLP_ProjetoEstimativas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavel7_Internalname, "Vari�vel", "", "", lblTextblockvariavel7_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoEstimativas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVariavel7_Internalname, StringUtil.LTrim( StringUtil.NToC( AV18Variavel7, 18, 5, ",", "")), ((edtavVariavel7_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV18Variavel7, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV18Variavel7, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,37);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVariavel7_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavVariavel7_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, false, "Valor", "right", false, "HLP_ProjetoEstimativas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavel8_Internalname, "Vari�vel", "", "", lblTextblockvariavel8_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoEstimativas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVariavel8_Internalname, StringUtil.LTrim( StringUtil.NToC( AV19Variavel8, 18, 5, ",", "")), ((edtavVariavel8_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV19Variavel8, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV19Variavel8, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,42);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVariavel8_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavVariavel8_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, false, "Valor", "right", false, "HLP_ProjetoEstimativas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_GU2e( true) ;
         }
         else
         {
            wb_table1_2_GU2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A648Projeto_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAGU2( ) ;
         WSGU2( ) ;
         WEGU2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA648Projeto_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAGU2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "projetoestimativas");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAGU2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A648Projeto_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         }
         wcpOA648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA648Projeto_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A648Projeto_Codigo != wcpOA648Projeto_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA648Projeto_Codigo = A648Projeto_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA648Projeto_Codigo = cgiGet( sPrefix+"A648Projeto_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA648Projeto_Codigo) > 0 )
         {
            A648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA648Projeto_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         }
         else
         {
            A648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A648Projeto_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAGU2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSGU2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSGU2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A648Projeto_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA648Projeto_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A648Projeto_Codigo_CTRL", StringUtil.RTrim( sCtrlA648Projeto_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEGU2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042822494350");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("projetoestimativas.js", "?202042822494350");
         }
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockvariavel1_Internalname = sPrefix+"TEXTBLOCKVARIAVEL1";
         edtavVariavel1_Internalname = sPrefix+"vVARIAVEL1";
         lblTextblockvariavel2_Internalname = sPrefix+"TEXTBLOCKVARIAVEL2";
         edtavVariavel2_Internalname = sPrefix+"vVARIAVEL2";
         lblTextblockvariavel3_Internalname = sPrefix+"TEXTBLOCKVARIAVEL3";
         edtavVariavel3_Internalname = sPrefix+"vVARIAVEL3";
         lblTextblockvariavel4_Internalname = sPrefix+"TEXTBLOCKVARIAVEL4";
         edtavVariavel4_Internalname = sPrefix+"vVARIAVEL4";
         lblTextblockvariavel5_Internalname = sPrefix+"TEXTBLOCKVARIAVEL5";
         edtavVariavel5_Internalname = sPrefix+"vVARIAVEL5";
         lblTextblockvariavel6_Internalname = sPrefix+"TEXTBLOCKVARIAVEL6";
         edtavVariavel6_Internalname = sPrefix+"vVARIAVEL6";
         lblTextblockvariavel7_Internalname = sPrefix+"TEXTBLOCKVARIAVEL7";
         edtavVariavel7_Internalname = sPrefix+"vVARIAVEL7";
         lblTextblockvariavel8_Internalname = sPrefix+"TEXTBLOCKVARIAVEL8";
         edtavVariavel8_Internalname = sPrefix+"vVARIAVEL8";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavVariavel8_Jsonclick = "";
         edtavVariavel8_Enabled = 1;
         edtavVariavel7_Jsonclick = "";
         edtavVariavel7_Enabled = 1;
         edtavVariavel6_Jsonclick = "";
         edtavVariavel6_Enabled = 1;
         edtavVariavel5_Jsonclick = "";
         edtavVariavel5_Enabled = 1;
         edtavVariavel4_Jsonclick = "";
         edtavVariavel4_Enabled = 1;
         edtavVariavel3_Jsonclick = "";
         edtavVariavel3_Enabled = 1;
         edtavVariavel2_Jsonclick = "";
         edtavVariavel2_Enabled = 1;
         edtavVariavel1_Jsonclick = "";
         edtavVariavel1_Enabled = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV22Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         lblTextblockvariavel1_Jsonclick = "";
         TempTags = "";
         lblTextblockvariavel2_Jsonclick = "";
         lblTextblockvariavel3_Jsonclick = "";
         lblTextblockvariavel4_Jsonclick = "";
         lblTextblockvariavel5_Jsonclick = "";
         lblTextblockvariavel6_Jsonclick = "";
         lblTextblockvariavel7_Jsonclick = "";
         lblTextblockvariavel8_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA648Projeto_Codigo = "";
         AV22Pgmname = "ProjetoEstimativas";
         /* GeneXus formulas. */
         AV22Pgmname = "ProjetoEstimativas";
         context.Gx_err = 0;
         edtavVariavel1_Enabled = 0;
         edtavVariavel2_Enabled = 0;
         edtavVariavel3_Enabled = 0;
         edtavVariavel4_Enabled = 0;
         edtavVariavel5_Enabled = 0;
         edtavVariavel6_Enabled = 0;
         edtavVariavel7_Enabled = 0;
         edtavVariavel8_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short nGXWrapped ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private int A648Projeto_Codigo ;
      private int wcpOA648Projeto_Codigo ;
      private int edtavVariavel1_Enabled ;
      private int edtavVariavel2_Enabled ;
      private int edtavVariavel3_Enabled ;
      private int edtavVariavel4_Enabled ;
      private int edtavVariavel5_Enabled ;
      private int edtavVariavel6_Enabled ;
      private int edtavVariavel7_Enabled ;
      private int edtavVariavel8_Enabled ;
      private int AV7Projeto_Codigo ;
      private int idxLst ;
      private decimal AV12Variavel1 ;
      private decimal AV13Variavel2 ;
      private decimal AV14Variavel3 ;
      private decimal AV15Variavel4 ;
      private decimal AV16Variavel5 ;
      private decimal AV17Variavel6 ;
      private decimal AV18Variavel7 ;
      private decimal AV19Variavel8 ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV22Pgmname ;
      private String edtavVariavel1_Internalname ;
      private String edtavVariavel2_Internalname ;
      private String edtavVariavel3_Internalname ;
      private String edtavVariavel4_Internalname ;
      private String edtavVariavel5_Internalname ;
      private String edtavVariavel6_Internalname ;
      private String edtavVariavel7_Internalname ;
      private String edtavVariavel8_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockvariavel1_Internalname ;
      private String lblTextblockvariavel1_Jsonclick ;
      private String TempTags ;
      private String edtavVariavel1_Jsonclick ;
      private String lblTextblockvariavel2_Internalname ;
      private String lblTextblockvariavel2_Jsonclick ;
      private String edtavVariavel2_Jsonclick ;
      private String lblTextblockvariavel3_Internalname ;
      private String lblTextblockvariavel3_Jsonclick ;
      private String edtavVariavel3_Jsonclick ;
      private String lblTextblockvariavel4_Internalname ;
      private String lblTextblockvariavel4_Jsonclick ;
      private String edtavVariavel4_Jsonclick ;
      private String lblTextblockvariavel5_Internalname ;
      private String lblTextblockvariavel5_Jsonclick ;
      private String edtavVariavel5_Jsonclick ;
      private String lblTextblockvariavel6_Internalname ;
      private String lblTextblockvariavel6_Jsonclick ;
      private String edtavVariavel6_Jsonclick ;
      private String lblTextblockvariavel7_Internalname ;
      private String lblTextblockvariavel7_Jsonclick ;
      private String edtavVariavel7_Jsonclick ;
      private String lblTextblockvariavel8_Internalname ;
      private String lblTextblockvariavel8_Jsonclick ;
      private String edtavVariavel8_Jsonclick ;
      private String sCtrlA648Projeto_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

}
