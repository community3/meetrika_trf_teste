/*
               File: GetPromptTipodeContaFilterData
        Description: Get Prompt Tipode Conta Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:12.52
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getprompttipodecontafilterdata : GXProcedure
   {
      public getprompttipodecontafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getprompttipodecontafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getprompttipodecontafilterdata objgetprompttipodecontafilterdata;
         objgetprompttipodecontafilterdata = new getprompttipodecontafilterdata();
         objgetprompttipodecontafilterdata.AV18DDOName = aP0_DDOName;
         objgetprompttipodecontafilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetprompttipodecontafilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetprompttipodecontafilterdata.AV22OptionsJson = "" ;
         objgetprompttipodecontafilterdata.AV25OptionsDescJson = "" ;
         objgetprompttipodecontafilterdata.AV27OptionIndexesJson = "" ;
         objgetprompttipodecontafilterdata.context.SetSubmitInitialConfig(context);
         objgetprompttipodecontafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetprompttipodecontafilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getprompttipodecontafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_TIPODECONTA_CODIGO") == 0 )
         {
            /* Execute user subroutine: 'LOADTIPODECONTA_CODIGOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_TIPODECONTA_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADTIPODECONTA_NOMEOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("PromptTipodeContaGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptTipodeContaGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("PromptTipodeContaGridState"), "");
         }
         AV50GXV1 = 1;
         while ( AV50GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV50GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_CODIGO") == 0 )
            {
               AV10TFTipodeConta_Codigo = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_CODIGO_SEL") == 0 )
            {
               AV11TFTipodeConta_Codigo_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_NOME") == 0 )
            {
               AV12TFTipodeConta_Nome = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_NOME_SEL") == 0 )
            {
               AV13TFTipodeConta_Nome_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_TIPO_SEL") == 0 )
            {
               AV14TFTipodeConta_Tipo_SelsJson = AV32GridStateFilterValue.gxTpr_Value;
               AV15TFTipodeConta_Tipo_Sels.FromJSonString(AV14TFTipodeConta_Tipo_SelsJson);
            }
            AV50GXV1 = (int)(AV50GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "TIPODECONTA_CODIGO") == 0 )
            {
               AV35TipodeConta_Codigo1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "TIPODECONTA_NOME") == 0 )
            {
               AV36TipodeConta_Nome1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "TIPODECONTA_CONTAPAICOD") == 0 )
            {
               AV37TipoDeConta_ContaPaiCod1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV38DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV39DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "TIPODECONTA_CODIGO") == 0 )
               {
                  AV40TipodeConta_Codigo2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "TIPODECONTA_NOME") == 0 )
               {
                  AV41TipodeConta_Nome2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "TIPODECONTA_CONTAPAICOD") == 0 )
               {
                  AV42TipoDeConta_ContaPaiCod2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV43DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV44DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "TIPODECONTA_CODIGO") == 0 )
                  {
                     AV45TipodeConta_Codigo3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "TIPODECONTA_NOME") == 0 )
                  {
                     AV46TipodeConta_Nome3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "TIPODECONTA_CONTAPAICOD") == 0 )
                  {
                     AV47TipoDeConta_ContaPaiCod3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADTIPODECONTA_CODIGOOPTIONS' Routine */
         AV10TFTipodeConta_Codigo = AV16SearchTxt;
         AV11TFTipodeConta_Codigo_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A873TipodeConta_Tipo ,
                                              AV15TFTipodeConta_Tipo_Sels ,
                                              AV34DynamicFiltersSelector1 ,
                                              AV35TipodeConta_Codigo1 ,
                                              AV36TipodeConta_Nome1 ,
                                              AV37TipoDeConta_ContaPaiCod1 ,
                                              AV38DynamicFiltersEnabled2 ,
                                              AV39DynamicFiltersSelector2 ,
                                              AV40TipodeConta_Codigo2 ,
                                              AV41TipodeConta_Nome2 ,
                                              AV42TipoDeConta_ContaPaiCod2 ,
                                              AV43DynamicFiltersEnabled3 ,
                                              AV44DynamicFiltersSelector3 ,
                                              AV45TipodeConta_Codigo3 ,
                                              AV46TipodeConta_Nome3 ,
                                              AV47TipoDeConta_ContaPaiCod3 ,
                                              AV11TFTipodeConta_Codigo_Sel ,
                                              AV10TFTipodeConta_Codigo ,
                                              AV13TFTipodeConta_Nome_Sel ,
                                              AV12TFTipodeConta_Nome ,
                                              AV15TFTipodeConta_Tipo_Sels.Count ,
                                              A870TipodeConta_Codigo ,
                                              A871TipodeConta_Nome ,
                                              A885TipoDeConta_ContaPaiCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV35TipodeConta_Codigo1 = StringUtil.PadR( StringUtil.RTrim( AV35TipodeConta_Codigo1), 20, "%");
         lV36TipodeConta_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36TipodeConta_Nome1), 50, "%");
         lV37TipoDeConta_ContaPaiCod1 = StringUtil.PadR( StringUtil.RTrim( AV37TipoDeConta_ContaPaiCod1), 20, "%");
         lV40TipodeConta_Codigo2 = StringUtil.PadR( StringUtil.RTrim( AV40TipodeConta_Codigo2), 20, "%");
         lV41TipodeConta_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV41TipodeConta_Nome2), 50, "%");
         lV42TipoDeConta_ContaPaiCod2 = StringUtil.PadR( StringUtil.RTrim( AV42TipoDeConta_ContaPaiCod2), 20, "%");
         lV45TipodeConta_Codigo3 = StringUtil.PadR( StringUtil.RTrim( AV45TipodeConta_Codigo3), 20, "%");
         lV46TipodeConta_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV46TipodeConta_Nome3), 50, "%");
         lV47TipoDeConta_ContaPaiCod3 = StringUtil.PadR( StringUtil.RTrim( AV47TipoDeConta_ContaPaiCod3), 20, "%");
         lV10TFTipodeConta_Codigo = StringUtil.PadR( StringUtil.RTrim( AV10TFTipodeConta_Codigo), 20, "%");
         lV12TFTipodeConta_Nome = StringUtil.PadR( StringUtil.RTrim( AV12TFTipodeConta_Nome), 50, "%");
         /* Using cursor P00P02 */
         pr_default.execute(0, new Object[] {lV35TipodeConta_Codigo1, lV36TipodeConta_Nome1, lV37TipoDeConta_ContaPaiCod1, lV40TipodeConta_Codigo2, lV41TipodeConta_Nome2, lV42TipoDeConta_ContaPaiCod2, lV45TipodeConta_Codigo3, lV46TipodeConta_Nome3, lV47TipoDeConta_ContaPaiCod3, lV10TFTipodeConta_Codigo, AV11TFTipodeConta_Codigo_Sel, lV12TFTipodeConta_Nome, AV13TFTipodeConta_Nome_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A873TipodeConta_Tipo = P00P02_A873TipodeConta_Tipo[0];
            A885TipoDeConta_ContaPaiCod = P00P02_A885TipoDeConta_ContaPaiCod[0];
            n885TipoDeConta_ContaPaiCod = P00P02_n885TipoDeConta_ContaPaiCod[0];
            A871TipodeConta_Nome = P00P02_A871TipodeConta_Nome[0];
            A870TipodeConta_Codigo = P00P02_A870TipodeConta_Codigo[0];
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A870TipodeConta_Codigo)) )
            {
               AV20Option = A870TipodeConta_Codigo;
               AV21Options.Add(AV20Option, 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADTIPODECONTA_NOMEOPTIONS' Routine */
         AV12TFTipodeConta_Nome = AV16SearchTxt;
         AV13TFTipodeConta_Nome_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A873TipodeConta_Tipo ,
                                              AV15TFTipodeConta_Tipo_Sels ,
                                              AV34DynamicFiltersSelector1 ,
                                              AV35TipodeConta_Codigo1 ,
                                              AV36TipodeConta_Nome1 ,
                                              AV37TipoDeConta_ContaPaiCod1 ,
                                              AV38DynamicFiltersEnabled2 ,
                                              AV39DynamicFiltersSelector2 ,
                                              AV40TipodeConta_Codigo2 ,
                                              AV41TipodeConta_Nome2 ,
                                              AV42TipoDeConta_ContaPaiCod2 ,
                                              AV43DynamicFiltersEnabled3 ,
                                              AV44DynamicFiltersSelector3 ,
                                              AV45TipodeConta_Codigo3 ,
                                              AV46TipodeConta_Nome3 ,
                                              AV47TipoDeConta_ContaPaiCod3 ,
                                              AV11TFTipodeConta_Codigo_Sel ,
                                              AV10TFTipodeConta_Codigo ,
                                              AV13TFTipodeConta_Nome_Sel ,
                                              AV12TFTipodeConta_Nome ,
                                              AV15TFTipodeConta_Tipo_Sels.Count ,
                                              A870TipodeConta_Codigo ,
                                              A871TipodeConta_Nome ,
                                              A885TipoDeConta_ContaPaiCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV35TipodeConta_Codigo1 = StringUtil.PadR( StringUtil.RTrim( AV35TipodeConta_Codigo1), 20, "%");
         lV36TipodeConta_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36TipodeConta_Nome1), 50, "%");
         lV37TipoDeConta_ContaPaiCod1 = StringUtil.PadR( StringUtil.RTrim( AV37TipoDeConta_ContaPaiCod1), 20, "%");
         lV40TipodeConta_Codigo2 = StringUtil.PadR( StringUtil.RTrim( AV40TipodeConta_Codigo2), 20, "%");
         lV41TipodeConta_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV41TipodeConta_Nome2), 50, "%");
         lV42TipoDeConta_ContaPaiCod2 = StringUtil.PadR( StringUtil.RTrim( AV42TipoDeConta_ContaPaiCod2), 20, "%");
         lV45TipodeConta_Codigo3 = StringUtil.PadR( StringUtil.RTrim( AV45TipodeConta_Codigo3), 20, "%");
         lV46TipodeConta_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV46TipodeConta_Nome3), 50, "%");
         lV47TipoDeConta_ContaPaiCod3 = StringUtil.PadR( StringUtil.RTrim( AV47TipoDeConta_ContaPaiCod3), 20, "%");
         lV10TFTipodeConta_Codigo = StringUtil.PadR( StringUtil.RTrim( AV10TFTipodeConta_Codigo), 20, "%");
         lV12TFTipodeConta_Nome = StringUtil.PadR( StringUtil.RTrim( AV12TFTipodeConta_Nome), 50, "%");
         /* Using cursor P00P03 */
         pr_default.execute(1, new Object[] {lV35TipodeConta_Codigo1, lV36TipodeConta_Nome1, lV37TipoDeConta_ContaPaiCod1, lV40TipodeConta_Codigo2, lV41TipodeConta_Nome2, lV42TipoDeConta_ContaPaiCod2, lV45TipodeConta_Codigo3, lV46TipodeConta_Nome3, lV47TipoDeConta_ContaPaiCod3, lV10TFTipodeConta_Codigo, AV11TFTipodeConta_Codigo_Sel, lV12TFTipodeConta_Nome, AV13TFTipodeConta_Nome_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKP03 = false;
            A871TipodeConta_Nome = P00P03_A871TipodeConta_Nome[0];
            A873TipodeConta_Tipo = P00P03_A873TipodeConta_Tipo[0];
            A885TipoDeConta_ContaPaiCod = P00P03_A885TipoDeConta_ContaPaiCod[0];
            n885TipoDeConta_ContaPaiCod = P00P03_n885TipoDeConta_ContaPaiCod[0];
            A870TipodeConta_Codigo = P00P03_A870TipodeConta_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00P03_A871TipodeConta_Nome[0], A871TipodeConta_Nome) == 0 ) )
            {
               BRKP03 = false;
               A870TipodeConta_Codigo = P00P03_A870TipodeConta_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKP03 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A871TipodeConta_Nome)) )
            {
               AV20Option = A871TipodeConta_Nome;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKP03 )
            {
               BRKP03 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFTipodeConta_Codigo = "";
         AV11TFTipodeConta_Codigo_Sel = "";
         AV12TFTipodeConta_Nome = "";
         AV13TFTipodeConta_Nome_Sel = "";
         AV14TFTipodeConta_Tipo_SelsJson = "";
         AV15TFTipodeConta_Tipo_Sels = new GxSimpleCollection();
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV35TipodeConta_Codigo1 = "";
         AV36TipodeConta_Nome1 = "";
         AV37TipoDeConta_ContaPaiCod1 = "";
         AV39DynamicFiltersSelector2 = "";
         AV40TipodeConta_Codigo2 = "";
         AV41TipodeConta_Nome2 = "";
         AV42TipoDeConta_ContaPaiCod2 = "";
         AV44DynamicFiltersSelector3 = "";
         AV45TipodeConta_Codigo3 = "";
         AV46TipodeConta_Nome3 = "";
         AV47TipoDeConta_ContaPaiCod3 = "";
         scmdbuf = "";
         lV35TipodeConta_Codigo1 = "";
         lV36TipodeConta_Nome1 = "";
         lV37TipoDeConta_ContaPaiCod1 = "";
         lV40TipodeConta_Codigo2 = "";
         lV41TipodeConta_Nome2 = "";
         lV42TipoDeConta_ContaPaiCod2 = "";
         lV45TipodeConta_Codigo3 = "";
         lV46TipodeConta_Nome3 = "";
         lV47TipoDeConta_ContaPaiCod3 = "";
         lV10TFTipodeConta_Codigo = "";
         lV12TFTipodeConta_Nome = "";
         A873TipodeConta_Tipo = "";
         A870TipodeConta_Codigo = "";
         A871TipodeConta_Nome = "";
         A885TipoDeConta_ContaPaiCod = "";
         P00P02_A873TipodeConta_Tipo = new String[] {""} ;
         P00P02_A885TipoDeConta_ContaPaiCod = new String[] {""} ;
         P00P02_n885TipoDeConta_ContaPaiCod = new bool[] {false} ;
         P00P02_A871TipodeConta_Nome = new String[] {""} ;
         P00P02_A870TipodeConta_Codigo = new String[] {""} ;
         AV20Option = "";
         P00P03_A871TipodeConta_Nome = new String[] {""} ;
         P00P03_A873TipodeConta_Tipo = new String[] {""} ;
         P00P03_A885TipoDeConta_ContaPaiCod = new String[] {""} ;
         P00P03_n885TipoDeConta_ContaPaiCod = new bool[] {false} ;
         P00P03_A870TipodeConta_Codigo = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getprompttipodecontafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00P02_A873TipodeConta_Tipo, P00P02_A885TipoDeConta_ContaPaiCod, P00P02_n885TipoDeConta_ContaPaiCod, P00P02_A871TipodeConta_Nome, P00P02_A870TipodeConta_Codigo
               }
               , new Object[] {
               P00P03_A871TipodeConta_Nome, P00P03_A873TipodeConta_Tipo, P00P03_A885TipoDeConta_ContaPaiCod, P00P03_n885TipoDeConta_ContaPaiCod, P00P03_A870TipodeConta_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV50GXV1 ;
      private int AV15TFTipodeConta_Tipo_Sels_Count ;
      private long AV28count ;
      private String AV10TFTipodeConta_Codigo ;
      private String AV11TFTipodeConta_Codigo_Sel ;
      private String AV12TFTipodeConta_Nome ;
      private String AV13TFTipodeConta_Nome_Sel ;
      private String AV35TipodeConta_Codigo1 ;
      private String AV36TipodeConta_Nome1 ;
      private String AV37TipoDeConta_ContaPaiCod1 ;
      private String AV40TipodeConta_Codigo2 ;
      private String AV41TipodeConta_Nome2 ;
      private String AV42TipoDeConta_ContaPaiCod2 ;
      private String AV45TipodeConta_Codigo3 ;
      private String AV46TipodeConta_Nome3 ;
      private String AV47TipoDeConta_ContaPaiCod3 ;
      private String scmdbuf ;
      private String lV35TipodeConta_Codigo1 ;
      private String lV36TipodeConta_Nome1 ;
      private String lV37TipoDeConta_ContaPaiCod1 ;
      private String lV40TipodeConta_Codigo2 ;
      private String lV41TipodeConta_Nome2 ;
      private String lV42TipoDeConta_ContaPaiCod2 ;
      private String lV45TipodeConta_Codigo3 ;
      private String lV46TipodeConta_Nome3 ;
      private String lV47TipoDeConta_ContaPaiCod3 ;
      private String lV10TFTipodeConta_Codigo ;
      private String lV12TFTipodeConta_Nome ;
      private String A873TipodeConta_Tipo ;
      private String A870TipodeConta_Codigo ;
      private String A871TipodeConta_Nome ;
      private String A885TipoDeConta_ContaPaiCod ;
      private bool returnInSub ;
      private bool AV38DynamicFiltersEnabled2 ;
      private bool AV43DynamicFiltersEnabled3 ;
      private bool n885TipoDeConta_ContaPaiCod ;
      private bool BRKP03 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV14TFTipodeConta_Tipo_SelsJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV39DynamicFiltersSelector2 ;
      private String AV44DynamicFiltersSelector3 ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00P02_A873TipodeConta_Tipo ;
      private String[] P00P02_A885TipoDeConta_ContaPaiCod ;
      private bool[] P00P02_n885TipoDeConta_ContaPaiCod ;
      private String[] P00P02_A871TipodeConta_Nome ;
      private String[] P00P02_A870TipodeConta_Codigo ;
      private String[] P00P03_A871TipodeConta_Nome ;
      private String[] P00P03_A873TipodeConta_Tipo ;
      private String[] P00P03_A885TipoDeConta_ContaPaiCod ;
      private bool[] P00P03_n885TipoDeConta_ContaPaiCod ;
      private String[] P00P03_A870TipodeConta_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV15TFTipodeConta_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getprompttipodecontafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00P02( IGxContext context ,
                                             String A873TipodeConta_Tipo ,
                                             IGxCollection AV15TFTipodeConta_Tipo_Sels ,
                                             String AV34DynamicFiltersSelector1 ,
                                             String AV35TipodeConta_Codigo1 ,
                                             String AV36TipodeConta_Nome1 ,
                                             String AV37TipoDeConta_ContaPaiCod1 ,
                                             bool AV38DynamicFiltersEnabled2 ,
                                             String AV39DynamicFiltersSelector2 ,
                                             String AV40TipodeConta_Codigo2 ,
                                             String AV41TipodeConta_Nome2 ,
                                             String AV42TipoDeConta_ContaPaiCod2 ,
                                             bool AV43DynamicFiltersEnabled3 ,
                                             String AV44DynamicFiltersSelector3 ,
                                             String AV45TipodeConta_Codigo3 ,
                                             String AV46TipodeConta_Nome3 ,
                                             String AV47TipoDeConta_ContaPaiCod3 ,
                                             String AV11TFTipodeConta_Codigo_Sel ,
                                             String AV10TFTipodeConta_Codigo ,
                                             String AV13TFTipodeConta_Nome_Sel ,
                                             String AV12TFTipodeConta_Nome ,
                                             int AV15TFTipodeConta_Tipo_Sels_Count ,
                                             String A870TipodeConta_Codigo ,
                                             String A871TipodeConta_Nome ,
                                             String A885TipoDeConta_ContaPaiCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [13] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT DISTINCT NULL AS [TipodeConta_Tipo], NULL AS [TipoDeConta_ContaPaiCod], NULL AS [TipodeConta_Nome], [TipodeConta_Codigo] FROM ( SELECT TOP(100) PERCENT [TipodeConta_Tipo], [TipoDeConta_ContaPaiCod], [TipodeConta_Nome], [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TipodeConta_Codigo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV35TipodeConta_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV35TipodeConta_Codigo1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TipodeConta_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV36TipodeConta_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV36TipodeConta_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TipoDeConta_ContaPaiCod1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV37TipoDeConta_ContaPaiCod1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV37TipoDeConta_ContaPaiCod1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TipodeConta_Codigo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV40TipodeConta_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV40TipodeConta_Codigo2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TipodeConta_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV41TipodeConta_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV41TipodeConta_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TipoDeConta_ContaPaiCod2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV42TipoDeConta_ContaPaiCod2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV42TipoDeConta_ContaPaiCod2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TipodeConta_Codigo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV45TipodeConta_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV45TipodeConta_Codigo3)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TipodeConta_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV46TipodeConta_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV46TipodeConta_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TipoDeConta_ContaPaiCod3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV47TipoDeConta_ContaPaiCod3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV47TipoDeConta_ContaPaiCod3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTipodeConta_Codigo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFTipodeConta_Codigo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like @lV10TFTipodeConta_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like @lV10TFTipodeConta_Codigo)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTipodeConta_Codigo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] = @AV11TFTipodeConta_Codigo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] = @AV11TFTipodeConta_Codigo_Sel)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFTipodeConta_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFTipodeConta_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like @lV12TFTipodeConta_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like @lV12TFTipodeConta_Nome)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFTipodeConta_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] = @AV13TFTipodeConta_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] = @AV13TFTipodeConta_Nome_Sel)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV15TFTipodeConta_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV15TFTipodeConta_Tipo_Sels, "[TipodeConta_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV15TFTipodeConta_Tipo_Sels, "[TipodeConta_Tipo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [TipodeConta_Codigo]";
         scmdbuf = scmdbuf + ") DistinctT";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00P03( IGxContext context ,
                                             String A873TipodeConta_Tipo ,
                                             IGxCollection AV15TFTipodeConta_Tipo_Sels ,
                                             String AV34DynamicFiltersSelector1 ,
                                             String AV35TipodeConta_Codigo1 ,
                                             String AV36TipodeConta_Nome1 ,
                                             String AV37TipoDeConta_ContaPaiCod1 ,
                                             bool AV38DynamicFiltersEnabled2 ,
                                             String AV39DynamicFiltersSelector2 ,
                                             String AV40TipodeConta_Codigo2 ,
                                             String AV41TipodeConta_Nome2 ,
                                             String AV42TipoDeConta_ContaPaiCod2 ,
                                             bool AV43DynamicFiltersEnabled3 ,
                                             String AV44DynamicFiltersSelector3 ,
                                             String AV45TipodeConta_Codigo3 ,
                                             String AV46TipodeConta_Nome3 ,
                                             String AV47TipoDeConta_ContaPaiCod3 ,
                                             String AV11TFTipodeConta_Codigo_Sel ,
                                             String AV10TFTipodeConta_Codigo ,
                                             String AV13TFTipodeConta_Nome_Sel ,
                                             String AV12TFTipodeConta_Nome ,
                                             int AV15TFTipodeConta_Tipo_Sels_Count ,
                                             String A870TipodeConta_Codigo ,
                                             String A871TipodeConta_Nome ,
                                             String A885TipoDeConta_ContaPaiCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [13] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [TipodeConta_Nome], [TipodeConta_Tipo], [TipoDeConta_ContaPaiCod], [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TipodeConta_Codigo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV35TipodeConta_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV35TipodeConta_Codigo1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TipodeConta_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV36TipodeConta_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV36TipodeConta_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TipoDeConta_ContaPaiCod1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV37TipoDeConta_ContaPaiCod1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV37TipoDeConta_ContaPaiCod1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TipodeConta_Codigo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV40TipodeConta_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV40TipodeConta_Codigo2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TipodeConta_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV41TipodeConta_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV41TipodeConta_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TipoDeConta_ContaPaiCod2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV42TipoDeConta_ContaPaiCod2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV42TipoDeConta_ContaPaiCod2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TipodeConta_Codigo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV45TipodeConta_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV45TipodeConta_Codigo3)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TipodeConta_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV46TipodeConta_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV46TipodeConta_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TipoDeConta_ContaPaiCod3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV47TipoDeConta_ContaPaiCod3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV47TipoDeConta_ContaPaiCod3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTipodeConta_Codigo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFTipodeConta_Codigo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like @lV10TFTipodeConta_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like @lV10TFTipodeConta_Codigo)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTipodeConta_Codigo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] = @AV11TFTipodeConta_Codigo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] = @AV11TFTipodeConta_Codigo_Sel)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFTipodeConta_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFTipodeConta_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like @lV12TFTipodeConta_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like @lV12TFTipodeConta_Nome)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFTipodeConta_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] = @AV13TFTipodeConta_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] = @AV13TFTipodeConta_Nome_Sel)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV15TFTipodeConta_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV15TFTipodeConta_Tipo_Sels, "[TipodeConta_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV15TFTipodeConta_Tipo_Sels, "[TipodeConta_Tipo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [TipodeConta_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00P02(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] );
               case 1 :
                     return conditional_P00P03(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00P02 ;
          prmP00P02 = new Object[] {
          new Object[] {"@lV35TipodeConta_Codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV36TipodeConta_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV37TipoDeConta_ContaPaiCod1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV40TipodeConta_Codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV41TipodeConta_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42TipoDeConta_ContaPaiCod2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV45TipodeConta_Codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV46TipodeConta_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47TipoDeConta_ContaPaiCod3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV10TFTipodeConta_Codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFTipodeConta_Codigo_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV12TFTipodeConta_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFTipodeConta_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00P03 ;
          prmP00P03 = new Object[] {
          new Object[] {"@lV35TipodeConta_Codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV36TipodeConta_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV37TipoDeConta_ContaPaiCod1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV40TipodeConta_Codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV41TipodeConta_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42TipoDeConta_ContaPaiCod2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV45TipodeConta_Codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV46TipodeConta_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47TipoDeConta_ContaPaiCod3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV10TFTipodeConta_Codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFTipodeConta_Codigo_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV12TFTipodeConta_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFTipodeConta_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00P02", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00P02,100,0,false,false )
             ,new CursorDef("P00P03", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00P03,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getprompttipodecontafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getprompttipodecontafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getprompttipodecontafilterdata") )
          {
             return  ;
          }
          getprompttipodecontafilterdata worker = new getprompttipodecontafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
