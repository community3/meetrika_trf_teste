/*
               File: AutorizacaoConsumo
        Description: Autoriza��o de Consumo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:56:17.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class autorizacaoconsumo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_30") == 0 )
         {
            A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_30( A74Contrato_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_31") == 0 )
         {
            A1777AutorizacaoConsumo_UnidadeMedicaoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1777AutorizacaoConsumo_UnidadeMedicaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_31( A1777AutorizacaoConsumo_UnidadeMedicaoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7AutorizacaoConsumo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AutorizacaoConsumo_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAUTORIZACAOCONSUMO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7AutorizacaoConsumo_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynAutorizacaoConsumo_UnidadeMedicaoCod.Name = "AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD";
         dynAutorizacaoConsumo_UnidadeMedicaoCod.WebTags = "";
         dynAutorizacaoConsumo_UnidadeMedicaoCod.removeAllItems();
         /* Using cursor T004H6 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            dynAutorizacaoConsumo_UnidadeMedicaoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T004H6_A1197UnidadeMedicao_Codigo[0]), 6, 0)), T004H6_A1198UnidadeMedicao_Nome[0], 0);
            pr_default.readNext(4);
         }
         pr_default.close(4);
         if ( dynAutorizacaoConsumo_UnidadeMedicaoCod.ItemCount > 0 )
         {
            A1777AutorizacaoConsumo_UnidadeMedicaoCod = (int)(NumberUtil.Val( dynAutorizacaoConsumo_UnidadeMedicaoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1777AutorizacaoConsumo_UnidadeMedicaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Autoriza��o de Consumo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContrato_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public autorizacaoconsumo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public autorizacaoconsumo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_AutorizacaoConsumo_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7AutorizacaoConsumo_Codigo = aP1_AutorizacaoConsumo_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynAutorizacaoConsumo_UnidadeMedicaoCod = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynAutorizacaoConsumo_UnidadeMedicaoCod.ItemCount > 0 )
         {
            A1777AutorizacaoConsumo_UnidadeMedicaoCod = (int)(NumberUtil.Val( dynAutorizacaoConsumo_UnidadeMedicaoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1777AutorizacaoConsumo_UnidadeMedicaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4H197( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4H197e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname, StringUtil.RTrim( A1778AutorizacaoConsumo_UnidadeMedicaoNom), StringUtil.RTrim( context.localUtil.Format( A1778AutorizacaoConsumo_UnidadeMedicaoNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAutorizacaoConsumo_UnidadeMedicaoNom_Jsonclick, 0, "Attribute", "", "", "", edtAutorizacaoConsumo_UnidadeMedicaoNom_Visible, edtAutorizacaoConsumo_UnidadeMedicaoNom_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_AutorizacaoConsumo.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4H197( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4H197( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4H197e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_62_4H197( true) ;
         }
         return  ;
      }

      protected void wb_table3_62_4H197e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4H197e( true) ;
         }
         else
         {
            wb_table1_2_4H197e( false) ;
         }
      }

      protected void wb_table3_62_4H197( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_62_4H197e( true) ;
         }
         else
         {
            wb_table3_62_4H197e( false) ;
         }
      }

      protected void wb_table2_5_4H197( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_4H197( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_4H197e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4H197e( true) ;
         }
         else
         {
            wb_table2_5_4H197e( false) ;
         }
      }

      protected void wb_table4_13_4H197( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockautorizacaoconsumo_codigo_Internalname, "C�digo", "", "", lblTextblockautorizacaoconsumo_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAutorizacaoConsumo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0, ",", "")), ((edtAutorizacaoConsumo_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1774AutorizacaoConsumo_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1774AutorizacaoConsumo_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAutorizacaoConsumo_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAutorizacaoConsumo_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_codigo_Internalname, "Contrato", "", "", lblTextblockcontrato_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_23_4H197( true) ;
         }
         return  ;
      }

      protected void wb_table5_23_4H197e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockautorizacaoconsumo_vigenciainicio_Internalname, "In�cio da Vig�ncia", "", "", lblTextblockautorizacaoconsumo_vigenciainicio_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtAutorizacaoConsumo_VigenciaInicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtAutorizacaoConsumo_VigenciaInicio_Internalname, context.localUtil.Format(A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"), context.localUtil.Format( A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAutorizacaoConsumo_VigenciaInicio_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtAutorizacaoConsumo_VigenciaInicio_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtAutorizacaoConsumo_VigenciaInicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtAutorizacaoConsumo_VigenciaInicio_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockautorizacaoconsumo_vigenciafim_Internalname, "Fim da Vig�ncia", "", "", lblTextblockautorizacaoconsumo_vigenciafim_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtAutorizacaoConsumo_VigenciaFim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtAutorizacaoConsumo_VigenciaFim_Internalname, context.localUtil.Format(A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"), context.localUtil.Format( A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAutorizacaoConsumo_VigenciaFim_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtAutorizacaoConsumo_VigenciaFim_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtAutorizacaoConsumo_VigenciaFim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtAutorizacaoConsumo_VigenciaFim_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockautorizacaoconsumo_unidademedicaocod_Internalname, "Unidade de Medi��o", "", "", lblTextblockautorizacaoconsumo_unidademedicaocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynAutorizacaoConsumo_UnidadeMedicaoCod, dynAutorizacaoConsumo_UnidadeMedicaoCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0)), 1, dynAutorizacaoConsumo_UnidadeMedicaoCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynAutorizacaoConsumo_UnidadeMedicaoCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_AutorizacaoConsumo.htm");
            dynAutorizacaoConsumo_UnidadeMedicaoCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAutorizacaoConsumo_UnidadeMedicaoCod_Internalname, "Values", (String)(dynAutorizacaoConsumo_UnidadeMedicaoCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockautorizacaoconsumo_quantidade_Internalname, "Quantidade", "", "", lblTextblockautorizacaoconsumo_quantidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAutorizacaoConsumo_Quantidade_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0, ",", "")), ((edtAutorizacaoConsumo_Quantidade_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1779AutorizacaoConsumo_Quantidade), "ZZ9")) : context.localUtil.Format( (decimal)(A1779AutorizacaoConsumo_Quantidade), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAutorizacaoConsumo_Quantidade_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAutorizacaoConsumo_Quantidade_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockautorizacaoconsumo_valor_Internalname, "Valor", "", "", lblTextblockautorizacaoconsumo_valor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAutorizacaoConsumo_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A1782AutorizacaoConsumo_Valor, 18, 5, ",", "")), ((edtAutorizacaoConsumo_Valor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1782AutorizacaoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1782AutorizacaoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAutorizacaoConsumo_Valor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAutorizacaoConsumo_Valor_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_4H197e( true) ;
         }
         else
         {
            wb_table4_13_4H197e( false) ;
         }
      }

      protected void wb_table5_23_4H197( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontrato_codigo_Internalname, tblTablemergedcontrato_codigo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContrato_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datavigenciainicio_Internalname, "Vig�ncia Inicio", "", "", lblTextblockcontrato_datavigenciainicio_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContrato_DataVigenciaInicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataVigenciaInicio_Internalname, context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"), context.localUtil.Format( A82Contrato_DataVigenciaInicio, "99/99/99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataVigenciaInicio_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContrato_DataVigenciaInicio_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_AutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataVigenciaInicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContrato_DataVigenciaInicio_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datavigenciatermino_Internalname, "Vig�ncia T�rmino", "", "", lblTextblockcontrato_datavigenciatermino_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContrato_DataVigenciaTermino_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataVigenciaTermino_Internalname, context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"), context.localUtil.Format( A83Contrato_DataVigenciaTermino, "99/99/99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataVigenciaTermino_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContrato_DataVigenciaTermino_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_AutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataVigenciaTermino_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContrato_DataVigenciaTermino_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_AutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_23_4H197e( true) ;
         }
         else
         {
            wb_table5_23_4H197e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E114H2 */
         E114H2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1774AutorizacaoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A74Contrato_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               }
               else
               {
                  A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               }
               A82Contrato_DataVigenciaInicio = context.localUtil.CToD( cgiGet( edtContrato_DataVigenciaInicio_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
               A83Contrato_DataVigenciaTermino = context.localUtil.CToD( cgiGet( edtContrato_DataVigenciaTermino_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
               if ( context.localUtil.VCDate( cgiGet( edtAutorizacaoConsumo_VigenciaInicio_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Autorizacao Consumo_Vigencia Inicio"}), 1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO");
                  AnyError = 1;
                  GX_FocusControl = edtAutorizacaoConsumo_VigenciaInicio_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1775AutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1775AutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"));
               }
               else
               {
                  A1775AutorizacaoConsumo_VigenciaInicio = context.localUtil.CToD( cgiGet( edtAutorizacaoConsumo_VigenciaInicio_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1775AutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"));
               }
               if ( context.localUtil.VCDate( cgiGet( edtAutorizacaoConsumo_VigenciaFim_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Autorizacao Consumo_Vigencia Fim"}), 1, "AUTORIZACAOCONSUMO_VIGENCIAFIM");
                  AnyError = 1;
                  GX_FocusControl = edtAutorizacaoConsumo_VigenciaFim_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1776AutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1776AutorizacaoConsumo_VigenciaFim", context.localUtil.Format(A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"));
               }
               else
               {
                  A1776AutorizacaoConsumo_VigenciaFim = context.localUtil.CToD( cgiGet( edtAutorizacaoConsumo_VigenciaFim_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1776AutorizacaoConsumo_VigenciaFim", context.localUtil.Format(A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"));
               }
               dynAutorizacaoConsumo_UnidadeMedicaoCod.CurrentValue = cgiGet( dynAutorizacaoConsumo_UnidadeMedicaoCod_Internalname);
               A1777AutorizacaoConsumo_UnidadeMedicaoCod = (int)(NumberUtil.Val( cgiGet( dynAutorizacaoConsumo_UnidadeMedicaoCod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1777AutorizacaoConsumo_UnidadeMedicaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_Quantidade_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_Quantidade_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "AUTORIZACAOCONSUMO_QUANTIDADE");
                  AnyError = 1;
                  GX_FocusControl = edtAutorizacaoConsumo_Quantidade_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1779AutorizacaoConsumo_Quantidade = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1779AutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0)));
               }
               else
               {
                  A1779AutorizacaoConsumo_Quantidade = (short)(context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_Quantidade_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1779AutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_Valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_Valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "AUTORIZACAOCONSUMO_VALOR");
                  AnyError = 1;
                  GX_FocusControl = edtAutorizacaoConsumo_Valor_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1782AutorizacaoConsumo_Valor = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1782AutorizacaoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( A1782AutorizacaoConsumo_Valor, 18, 5)));
               }
               else
               {
                  A1782AutorizacaoConsumo_Valor = context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_Valor_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1782AutorizacaoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( A1782AutorizacaoConsumo_Valor, 18, 5)));
               }
               A1778AutorizacaoConsumo_UnidadeMedicaoNom = StringUtil.Upper( cgiGet( edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname));
               n1778AutorizacaoConsumo_UnidadeMedicaoNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1778AutorizacaoConsumo_UnidadeMedicaoNom", A1778AutorizacaoConsumo_UnidadeMedicaoNom);
               /* Read saved values. */
               Z1774AutorizacaoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1774AutorizacaoConsumo_Codigo"), ",", "."));
               Z1775AutorizacaoConsumo_VigenciaInicio = context.localUtil.CToD( cgiGet( "Z1775AutorizacaoConsumo_VigenciaInicio"), 0);
               Z1776AutorizacaoConsumo_VigenciaFim = context.localUtil.CToD( cgiGet( "Z1776AutorizacaoConsumo_VigenciaFim"), 0);
               Z1779AutorizacaoConsumo_Quantidade = (short)(context.localUtil.CToN( cgiGet( "Z1779AutorizacaoConsumo_Quantidade"), ",", "."));
               Z1782AutorizacaoConsumo_Valor = context.localUtil.CToN( cgiGet( "Z1782AutorizacaoConsumo_Valor"), ",", ".");
               Z1780AutorizacaoConsumo_Ativo = StringUtil.StrToBool( cgiGet( "Z1780AutorizacaoConsumo_Ativo"));
               Z1787AutorizacaoConsumo_Status = cgiGet( "Z1787AutorizacaoConsumo_Status");
               Z74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z74Contrato_Codigo"), ",", "."));
               Z1777AutorizacaoConsumo_UnidadeMedicaoCod = (int)(context.localUtil.CToN( cgiGet( "Z1777AutorizacaoConsumo_UnidadeMedicaoCod"), ",", "."));
               A1780AutorizacaoConsumo_Ativo = StringUtil.StrToBool( cgiGet( "Z1780AutorizacaoConsumo_Ativo"));
               A1787AutorizacaoConsumo_Status = cgiGet( "Z1787AutorizacaoConsumo_Status");
               O1779AutorizacaoConsumo_Quantidade = (short)(context.localUtil.CToN( cgiGet( "O1779AutorizacaoConsumo_Quantidade"), ",", "."));
               O1782AutorizacaoConsumo_Valor = context.localUtil.CToN( cgiGet( "O1782AutorizacaoConsumo_Valor"), ",", ".");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "N74Contrato_Codigo"), ",", "."));
               N1777AutorizacaoConsumo_UnidadeMedicaoCod = (int)(context.localUtil.CToN( cgiGet( "N1777AutorizacaoConsumo_UnidadeMedicaoCod"), ",", "."));
               AV7AutorizacaoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( "vAUTORIZACAOCONSUMO_CODIGO"), ",", "."));
               AV11Insert_Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATO_CODIGO"), ",", "."));
               AV13Insert_AutorizacaoConsumo_UnidadeMedicaoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A1780AutorizacaoConsumo_Ativo = StringUtil.StrToBool( cgiGet( "AUTORIZACAOCONSUMO_ATIVO"));
               A1787AutorizacaoConsumo_Status = cgiGet( "AUTORIZACAOCONSUMO_STATUS");
               A1788AutorizacaoConsumo_unidadeMedicaoSig = cgiGet( "AUTORIZACAOCONSUMO_UNIDADEMEDICAOSIG");
               n1788AutorizacaoConsumo_unidadeMedicaoSig = false;
               AV15Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "AutorizacaoConsumo";
               A1774AutorizacaoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1774AutorizacaoConsumo_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1780AutorizacaoConsumo_Ativo);
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1787AutorizacaoConsumo_Status, ""));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1774AutorizacaoConsumo_Codigo != Z1774AutorizacaoConsumo_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("autorizacaoconsumo:[SecurityCheckFailed value for]"+"AutorizacaoConsumo_Codigo:"+context.localUtil.Format( (decimal)(A1774AutorizacaoConsumo_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("autorizacaoconsumo:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("autorizacaoconsumo:[SecurityCheckFailed value for]"+"AutorizacaoConsumo_Ativo:"+StringUtil.BoolToStr( A1780AutorizacaoConsumo_Ativo));
                  GXUtil.WriteLog("autorizacaoconsumo:[SecurityCheckFailed value for]"+"AutorizacaoConsumo_Status:"+StringUtil.RTrim( context.localUtil.Format( A1787AutorizacaoConsumo_Status, "")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1774AutorizacaoConsumo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode197 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode197;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound197 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_4H0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "AUTORIZACAOCONSUMO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtAutorizacaoConsumo_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E114H2 */
                           E114H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E124H2 */
                           E124H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E124H2 */
            E124H2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4H197( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes4H197( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_4H0( )
      {
         BeforeValidate4H197( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4H197( ) ;
            }
            else
            {
               CheckExtendedTable4H197( ) ;
               CloseExtendedTableCursors4H197( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption4H0( )
      {
      }

      protected void E114H2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV15Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV16GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            while ( AV16GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV16GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Contrato_Codigo") == 0 )
               {
                  AV11Insert_Contrato_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contrato_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "AutorizacaoConsumo_UnidadeMedicaoCod") == 0 )
               {
                  AV13Insert_AutorizacaoConsumo_UnidadeMedicaoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_AutorizacaoConsumo_UnidadeMedicaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0)));
               }
               AV16GXV1 = (int)(AV16GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            }
         }
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAutorizacaoConsumo_UnidadeMedicaoNom_Visible), 5, 0)));
      }

      protected void E124H2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwautorizacaoconsumo.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM4H197( short GX_JID )
      {
         if ( ( GX_JID == 29 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1775AutorizacaoConsumo_VigenciaInicio = T004H3_A1775AutorizacaoConsumo_VigenciaInicio[0];
               Z1776AutorizacaoConsumo_VigenciaFim = T004H3_A1776AutorizacaoConsumo_VigenciaFim[0];
               Z1779AutorizacaoConsumo_Quantidade = T004H3_A1779AutorizacaoConsumo_Quantidade[0];
               Z1782AutorizacaoConsumo_Valor = T004H3_A1782AutorizacaoConsumo_Valor[0];
               Z1780AutorizacaoConsumo_Ativo = T004H3_A1780AutorizacaoConsumo_Ativo[0];
               Z1787AutorizacaoConsumo_Status = T004H3_A1787AutorizacaoConsumo_Status[0];
               Z74Contrato_Codigo = T004H3_A74Contrato_Codigo[0];
               Z1777AutorizacaoConsumo_UnidadeMedicaoCod = T004H3_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0];
            }
            else
            {
               Z1775AutorizacaoConsumo_VigenciaInicio = A1775AutorizacaoConsumo_VigenciaInicio;
               Z1776AutorizacaoConsumo_VigenciaFim = A1776AutorizacaoConsumo_VigenciaFim;
               Z1779AutorizacaoConsumo_Quantidade = A1779AutorizacaoConsumo_Quantidade;
               Z1782AutorizacaoConsumo_Valor = A1782AutorizacaoConsumo_Valor;
               Z1780AutorizacaoConsumo_Ativo = A1780AutorizacaoConsumo_Ativo;
               Z1787AutorizacaoConsumo_Status = A1787AutorizacaoConsumo_Status;
               Z74Contrato_Codigo = A74Contrato_Codigo;
               Z1777AutorizacaoConsumo_UnidadeMedicaoCod = A1777AutorizacaoConsumo_UnidadeMedicaoCod;
            }
         }
         if ( GX_JID == -29 )
         {
            Z1774AutorizacaoConsumo_Codigo = A1774AutorizacaoConsumo_Codigo;
            Z1775AutorizacaoConsumo_VigenciaInicio = A1775AutorizacaoConsumo_VigenciaInicio;
            Z1776AutorizacaoConsumo_VigenciaFim = A1776AutorizacaoConsumo_VigenciaFim;
            Z1779AutorizacaoConsumo_Quantidade = A1779AutorizacaoConsumo_Quantidade;
            Z1782AutorizacaoConsumo_Valor = A1782AutorizacaoConsumo_Valor;
            Z1780AutorizacaoConsumo_Ativo = A1780AutorizacaoConsumo_Ativo;
            Z1787AutorizacaoConsumo_Status = A1787AutorizacaoConsumo_Status;
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z1777AutorizacaoConsumo_UnidadeMedicaoCod = A1777AutorizacaoConsumo_UnidadeMedicaoCod;
            Z83Contrato_DataVigenciaTermino = A83Contrato_DataVigenciaTermino;
            Z82Contrato_DataVigenciaInicio = A82Contrato_DataVigenciaInicio;
            Z1778AutorizacaoConsumo_UnidadeMedicaoNom = A1778AutorizacaoConsumo_UnidadeMedicaoNom;
            Z1788AutorizacaoConsumo_unidadeMedicaoSig = A1788AutorizacaoConsumo_unidadeMedicaoSig;
         }
      }

      protected void standaloneNotModal( )
      {
         edtAutorizacaoConsumo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAutorizacaoConsumo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAutorizacaoConsumo_Codigo_Enabled), 5, 0)));
         AV15Pgmname = "AutorizacaoConsumo";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Pgmname", AV15Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtAutorizacaoConsumo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAutorizacaoConsumo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAutorizacaoConsumo_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7AutorizacaoConsumo_Codigo) )
         {
            A1774AutorizacaoConsumo_Codigo = AV7AutorizacaoConsumo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
         {
            edtContrato_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtContrato_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_AutorizacaoConsumo_UnidadeMedicaoCod) )
         {
            dynAutorizacaoConsumo_UnidadeMedicaoCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAutorizacaoConsumo_UnidadeMedicaoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynAutorizacaoConsumo_UnidadeMedicaoCod.Enabled), 5, 0)));
         }
         else
         {
            dynAutorizacaoConsumo_UnidadeMedicaoCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAutorizacaoConsumo_UnidadeMedicaoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynAutorizacaoConsumo_UnidadeMedicaoCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_AutorizacaoConsumo_UnidadeMedicaoCod) )
         {
            A1777AutorizacaoConsumo_UnidadeMedicaoCod = AV13Insert_AutorizacaoConsumo_UnidadeMedicaoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1777AutorizacaoConsumo_UnidadeMedicaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
         {
            A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1780AutorizacaoConsumo_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A1780AutorizacaoConsumo_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1780AutorizacaoConsumo_Ativo", A1780AutorizacaoConsumo_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A1787AutorizacaoConsumo_Status)) && ( Gx_BScreen == 0 ) )
         {
            A1787AutorizacaoConsumo_Status = "CRI";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1787AutorizacaoConsumo_Status", A1787AutorizacaoConsumo_Status);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T004H5 */
            pr_default.execute(3, new Object[] {A1777AutorizacaoConsumo_UnidadeMedicaoCod});
            A1778AutorizacaoConsumo_UnidadeMedicaoNom = T004H5_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1778AutorizacaoConsumo_UnidadeMedicaoNom", A1778AutorizacaoConsumo_UnidadeMedicaoNom);
            n1778AutorizacaoConsumo_UnidadeMedicaoNom = T004H5_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            A1788AutorizacaoConsumo_unidadeMedicaoSig = T004H5_A1788AutorizacaoConsumo_unidadeMedicaoSig[0];
            n1788AutorizacaoConsumo_unidadeMedicaoSig = T004H5_n1788AutorizacaoConsumo_unidadeMedicaoSig[0];
            pr_default.close(3);
            /* Using cursor T004H4 */
            pr_default.execute(2, new Object[] {A74Contrato_Codigo});
            A83Contrato_DataVigenciaTermino = T004H4_A83Contrato_DataVigenciaTermino[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
            A82Contrato_DataVigenciaInicio = T004H4_A82Contrato_DataVigenciaInicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
            pr_default.close(2);
         }
      }

      protected void Load4H197( )
      {
         /* Using cursor T004H7 */
         pr_default.execute(5, new Object[] {A1774AutorizacaoConsumo_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound197 = 1;
            A83Contrato_DataVigenciaTermino = T004H7_A83Contrato_DataVigenciaTermino[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
            A82Contrato_DataVigenciaInicio = T004H7_A82Contrato_DataVigenciaInicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
            A1775AutorizacaoConsumo_VigenciaInicio = T004H7_A1775AutorizacaoConsumo_VigenciaInicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1775AutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"));
            A1776AutorizacaoConsumo_VigenciaFim = T004H7_A1776AutorizacaoConsumo_VigenciaFim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1776AutorizacaoConsumo_VigenciaFim", context.localUtil.Format(A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"));
            A1778AutorizacaoConsumo_UnidadeMedicaoNom = T004H7_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1778AutorizacaoConsumo_UnidadeMedicaoNom", A1778AutorizacaoConsumo_UnidadeMedicaoNom);
            n1778AutorizacaoConsumo_UnidadeMedicaoNom = T004H7_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            A1788AutorizacaoConsumo_unidadeMedicaoSig = T004H7_A1788AutorizacaoConsumo_unidadeMedicaoSig[0];
            n1788AutorizacaoConsumo_unidadeMedicaoSig = T004H7_n1788AutorizacaoConsumo_unidadeMedicaoSig[0];
            A1779AutorizacaoConsumo_Quantidade = T004H7_A1779AutorizacaoConsumo_Quantidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1779AutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0)));
            A1782AutorizacaoConsumo_Valor = T004H7_A1782AutorizacaoConsumo_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1782AutorizacaoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( A1782AutorizacaoConsumo_Valor, 18, 5)));
            A1780AutorizacaoConsumo_Ativo = T004H7_A1780AutorizacaoConsumo_Ativo[0];
            A1787AutorizacaoConsumo_Status = T004H7_A1787AutorizacaoConsumo_Status[0];
            A74Contrato_Codigo = T004H7_A74Contrato_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            A1777AutorizacaoConsumo_UnidadeMedicaoCod = T004H7_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1777AutorizacaoConsumo_UnidadeMedicaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0)));
            ZM4H197( -29) ;
         }
         pr_default.close(5);
         OnLoadActions4H197( ) ;
      }

      protected void OnLoadActions4H197( )
      {
      }

      protected void CheckExtendedTable4H197( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T004H4 */
         pr_default.execute(2, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "CONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContrato_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A83Contrato_DataVigenciaTermino = T004H4_A83Contrato_DataVigenciaTermino[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
         A82Contrato_DataVigenciaInicio = T004H4_A82Contrato_DataVigenciaInicio[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
         pr_default.close(2);
         if ( A1782AutorizacaoConsumo_Valor > new prc_contratounidademedicao_saldo(context).executeUdp(  A74Contrato_Codigo,  A1777AutorizacaoConsumo_UnidadeMedicaoCod) )
         {
            GX_msglist.addItem("Valor da autorizacao de consumo n�o pode ser maior que o saldo do contrato!", 1, "CONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContrato_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( true /* After */ && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && new prc_autorizacaoconsumo_verificaativo(context).executeUdp(  A74Contrato_Codigo,  A1777AutorizacaoConsumo_UnidadeMedicaoCod) )
         {
            GX_msglist.addItem("J� existe uma autoriza��o de consumo ativa para esse contrato!", 1, "CONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContrato_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A1775AutorizacaoConsumo_VigenciaInicio) || ( A1775AutorizacaoConsumo_VigenciaInicio >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Autorizacao Consumo_Vigencia Inicio fora do intervalo", "OutOfRange", 1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_VigenciaInicio_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (DateTime.MinValue==A1775AutorizacaoConsumo_VigenciaInicio) )
         {
            GX_msglist.addItem("Autorizacao Consumo_Vigencia Inicio � obrigat�rio.", 1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_VigenciaInicio_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( true /* After */ && ( A1775AutorizacaoConsumo_VigenciaInicio < A82Contrato_DataVigenciaInicio ) )
         {
            GX_msglist.addItem("Vig�ncia da Autoriza��o de Consumo nao pode ser menor que a vig�ncia do contrato!", 1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_VigenciaInicio_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A1776AutorizacaoConsumo_VigenciaFim) || ( A1776AutorizacaoConsumo_VigenciaFim >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Autorizacao Consumo_Vigencia Fim fora do intervalo", "OutOfRange", 1, "AUTORIZACAOCONSUMO_VIGENCIAFIM");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_VigenciaFim_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (DateTime.MinValue==A1776AutorizacaoConsumo_VigenciaFim) )
         {
            GX_msglist.addItem("Autorizacao Consumo_Vigencia Fim � obrigat�rio.", 1, "AUTORIZACAOCONSUMO_VIGENCIAFIM");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_VigenciaFim_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( true /* After */ && ( A1776AutorizacaoConsumo_VigenciaFim > A83Contrato_DataVigenciaTermino ) )
         {
            GX_msglist.addItem("Vig�ncia da Autoriza��o de Consumo nao pode ser maior que a vig�ncia do contrato!", 1, "AUTORIZACAOCONSUMO_VIGENCIAFIM");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_VigenciaFim_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( true /* After */ && ( A1775AutorizacaoConsumo_VigenciaInicio > A1776AutorizacaoConsumo_VigenciaFim ) )
         {
            GX_msglist.addItem("Data de in�cio da vig�ncia n�o pode ser maior que a data de t�rmino da vig�ncia!", 1, "AUTORIZACAOCONSUMO_VIGENCIAFIM");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_VigenciaFim_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( true /* After */ && ( A1776AutorizacaoConsumo_VigenciaFim < A1776AutorizacaoConsumo_VigenciaFim ) )
         {
            GX_msglist.addItem("Data de t�rmino da vig�ncia n�o pode ser menor que a data de in�cio da vig�ncia!", 1, "AUTORIZACAOCONSUMO_VIGENCIAFIM");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_VigenciaFim_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T004H5 */
         pr_default.execute(3, new Object[] {A1777AutorizacaoConsumo_UnidadeMedicaoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Autorizacao Consumo Unidade Medicao'.", "ForeignKeyNotFound", 1, "AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD");
            AnyError = 1;
            GX_FocusControl = dynAutorizacaoConsumo_UnidadeMedicaoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1778AutorizacaoConsumo_UnidadeMedicaoNom = T004H5_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1778AutorizacaoConsumo_UnidadeMedicaoNom", A1778AutorizacaoConsumo_UnidadeMedicaoNom);
         n1778AutorizacaoConsumo_UnidadeMedicaoNom = T004H5_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
         A1788AutorizacaoConsumo_unidadeMedicaoSig = T004H5_A1788AutorizacaoConsumo_unidadeMedicaoSig[0];
         n1788AutorizacaoConsumo_unidadeMedicaoSig = T004H5_n1788AutorizacaoConsumo_unidadeMedicaoSig[0];
         pr_default.close(3);
         if ( (0==A1777AutorizacaoConsumo_UnidadeMedicaoCod) )
         {
            GX_msglist.addItem("Unidade de Medi��o � obrigat�rio.", 1, "AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD");
            AnyError = 1;
            GX_FocusControl = dynAutorizacaoConsumo_UnidadeMedicaoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (0==A1779AutorizacaoConsumo_Quantidade) )
         {
            GX_msglist.addItem("Quantidade � obrigat�rio.", 1, "AUTORIZACAOCONSUMO_QUANTIDADE");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_Quantidade_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( true /* After */ && ( O1779AutorizacaoConsumo_Quantidade != A1779AutorizacaoConsumo_Quantidade ) && ( StringUtil.StrCmp(A1787AutorizacaoConsumo_Status, "AUT") == 0 ) && ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            GX_msglist.addItem("N�o � permitido alterar a quantidade ap�s a autoriza��o da Autoriza��o de Consumo!", 1, "AUTORIZACAOCONSUMO_QUANTIDADE");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_Quantidade_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (Convert.ToDecimal(0)==A1782AutorizacaoConsumo_Valor) )
         {
            GX_msglist.addItem("Autorizacao Consumo_Valor � obrigat�rio.", 1, "AUTORIZACAOCONSUMO_VALOR");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_Valor_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( true /* After */ && ( A1782AutorizacaoConsumo_Valor > new prc_contratounidademedicao_saldo(context).executeUdp(  A74Contrato_Codigo,  A1777AutorizacaoConsumo_UnidadeMedicaoCod) ) )
         {
            GX_msglist.addItem("Valor da Autoriza��o de consumo n�o pode ser maior que o Saldo do Contrato!", 1, "AUTORIZACAOCONSUMO_VALOR");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_Valor_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( true /* After */ && ( O1782AutorizacaoConsumo_Valor != A1782AutorizacaoConsumo_Valor ) && ( StringUtil.StrCmp(A1787AutorizacaoConsumo_Status, "AUT") == 0 ) && ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            GX_msglist.addItem("N�o � permitido alterar o valor ap�s a autoriza��o da Autoriza��o de Consumo!", 1, "AUTORIZACAOCONSUMO_VALOR");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_Valor_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors4H197( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_30( int A74Contrato_Codigo )
      {
         /* Using cursor T004H8 */
         pr_default.execute(6, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "CONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContrato_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A83Contrato_DataVigenciaTermino = T004H8_A83Contrato_DataVigenciaTermino[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
         A82Contrato_DataVigenciaInicio = T004H8_A82Contrato_DataVigenciaInicio[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"))+"\""+","+"\""+GXUtil.EncodeJSConstant( context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void gxLoad_31( int A1777AutorizacaoConsumo_UnidadeMedicaoCod )
      {
         /* Using cursor T004H9 */
         pr_default.execute(7, new Object[] {A1777AutorizacaoConsumo_UnidadeMedicaoCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Autorizacao Consumo Unidade Medicao'.", "ForeignKeyNotFound", 1, "AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD");
            AnyError = 1;
            GX_FocusControl = dynAutorizacaoConsumo_UnidadeMedicaoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1778AutorizacaoConsumo_UnidadeMedicaoNom = T004H9_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1778AutorizacaoConsumo_UnidadeMedicaoNom", A1778AutorizacaoConsumo_UnidadeMedicaoNom);
         n1778AutorizacaoConsumo_UnidadeMedicaoNom = T004H9_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
         A1788AutorizacaoConsumo_unidadeMedicaoSig = T004H9_A1788AutorizacaoConsumo_unidadeMedicaoSig[0];
         n1788AutorizacaoConsumo_unidadeMedicaoSig = T004H9_n1788AutorizacaoConsumo_unidadeMedicaoSig[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1778AutorizacaoConsumo_UnidadeMedicaoNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1788AutorizacaoConsumo_unidadeMedicaoSig))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void GetKey4H197( )
      {
         /* Using cursor T004H10 */
         pr_default.execute(8, new Object[] {A1774AutorizacaoConsumo_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound197 = 1;
         }
         else
         {
            RcdFound197 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004H3 */
         pr_default.execute(1, new Object[] {A1774AutorizacaoConsumo_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4H197( 29) ;
            RcdFound197 = 1;
            A1774AutorizacaoConsumo_Codigo = T004H3_A1774AutorizacaoConsumo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
            A1775AutorizacaoConsumo_VigenciaInicio = T004H3_A1775AutorizacaoConsumo_VigenciaInicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1775AutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"));
            A1776AutorizacaoConsumo_VigenciaFim = T004H3_A1776AutorizacaoConsumo_VigenciaFim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1776AutorizacaoConsumo_VigenciaFim", context.localUtil.Format(A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"));
            A1779AutorizacaoConsumo_Quantidade = T004H3_A1779AutorizacaoConsumo_Quantidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1779AutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0)));
            A1782AutorizacaoConsumo_Valor = T004H3_A1782AutorizacaoConsumo_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1782AutorizacaoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( A1782AutorizacaoConsumo_Valor, 18, 5)));
            A1780AutorizacaoConsumo_Ativo = T004H3_A1780AutorizacaoConsumo_Ativo[0];
            A1787AutorizacaoConsumo_Status = T004H3_A1787AutorizacaoConsumo_Status[0];
            A74Contrato_Codigo = T004H3_A74Contrato_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            A1777AutorizacaoConsumo_UnidadeMedicaoCod = T004H3_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1777AutorizacaoConsumo_UnidadeMedicaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0)));
            O1779AutorizacaoConsumo_Quantidade = A1779AutorizacaoConsumo_Quantidade;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1779AutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0)));
            O1782AutorizacaoConsumo_Valor = A1782AutorizacaoConsumo_Valor;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1782AutorizacaoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( A1782AutorizacaoConsumo_Valor, 18, 5)));
            Z1774AutorizacaoConsumo_Codigo = A1774AutorizacaoConsumo_Codigo;
            sMode197 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load4H197( ) ;
            if ( AnyError == 1 )
            {
               RcdFound197 = 0;
               InitializeNonKey4H197( ) ;
            }
            Gx_mode = sMode197;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound197 = 0;
            InitializeNonKey4H197( ) ;
            sMode197 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode197;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4H197( ) ;
         if ( RcdFound197 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound197 = 0;
         /* Using cursor T004H11 */
         pr_default.execute(9, new Object[] {A1774AutorizacaoConsumo_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T004H11_A1774AutorizacaoConsumo_Codigo[0] < A1774AutorizacaoConsumo_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T004H11_A1774AutorizacaoConsumo_Codigo[0] > A1774AutorizacaoConsumo_Codigo ) ) )
            {
               A1774AutorizacaoConsumo_Codigo = T004H11_A1774AutorizacaoConsumo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
               RcdFound197 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void move_previous( )
      {
         RcdFound197 = 0;
         /* Using cursor T004H12 */
         pr_default.execute(10, new Object[] {A1774AutorizacaoConsumo_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T004H12_A1774AutorizacaoConsumo_Codigo[0] > A1774AutorizacaoConsumo_Codigo ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T004H12_A1774AutorizacaoConsumo_Codigo[0] < A1774AutorizacaoConsumo_Codigo ) ) )
            {
               A1774AutorizacaoConsumo_Codigo = T004H12_A1774AutorizacaoConsumo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
               RcdFound197 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4H197( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContrato_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4H197( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound197 == 1 )
            {
               if ( A1774AutorizacaoConsumo_Codigo != Z1774AutorizacaoConsumo_Codigo )
               {
                  A1774AutorizacaoConsumo_Codigo = Z1774AutorizacaoConsumo_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "AUTORIZACAOCONSUMO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtAutorizacaoConsumo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContrato_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update4H197( ) ;
                  GX_FocusControl = edtContrato_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1774AutorizacaoConsumo_Codigo != Z1774AutorizacaoConsumo_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtContrato_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4H197( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "AUTORIZACAOCONSUMO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtAutorizacaoConsumo_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContrato_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4H197( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1774AutorizacaoConsumo_Codigo != Z1774AutorizacaoConsumo_Codigo )
         {
            A1774AutorizacaoConsumo_Codigo = Z1774AutorizacaoConsumo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "AUTORIZACAOCONSUMO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContrato_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency4H197( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004H2 */
            pr_default.execute(0, new Object[] {A1774AutorizacaoConsumo_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AutorizacaoConsumo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z1775AutorizacaoConsumo_VigenciaInicio != T004H2_A1775AutorizacaoConsumo_VigenciaInicio[0] ) || ( Z1776AutorizacaoConsumo_VigenciaFim != T004H2_A1776AutorizacaoConsumo_VigenciaFim[0] ) || ( Z1779AutorizacaoConsumo_Quantidade != T004H2_A1779AutorizacaoConsumo_Quantidade[0] ) || ( Z1782AutorizacaoConsumo_Valor != T004H2_A1782AutorizacaoConsumo_Valor[0] ) || ( Z1780AutorizacaoConsumo_Ativo != T004H2_A1780AutorizacaoConsumo_Ativo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z1787AutorizacaoConsumo_Status, T004H2_A1787AutorizacaoConsumo_Status[0]) != 0 ) || ( Z74Contrato_Codigo != T004H2_A74Contrato_Codigo[0] ) || ( Z1777AutorizacaoConsumo_UnidadeMedicaoCod != T004H2_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0] ) )
            {
               if ( Z1775AutorizacaoConsumo_VigenciaInicio != T004H2_A1775AutorizacaoConsumo_VigenciaInicio[0] )
               {
                  GXUtil.WriteLog("autorizacaoconsumo:[seudo value changed for attri]"+"AutorizacaoConsumo_VigenciaInicio");
                  GXUtil.WriteLogRaw("Old: ",Z1775AutorizacaoConsumo_VigenciaInicio);
                  GXUtil.WriteLogRaw("Current: ",T004H2_A1775AutorizacaoConsumo_VigenciaInicio[0]);
               }
               if ( Z1776AutorizacaoConsumo_VigenciaFim != T004H2_A1776AutorizacaoConsumo_VigenciaFim[0] )
               {
                  GXUtil.WriteLog("autorizacaoconsumo:[seudo value changed for attri]"+"AutorizacaoConsumo_VigenciaFim");
                  GXUtil.WriteLogRaw("Old: ",Z1776AutorizacaoConsumo_VigenciaFim);
                  GXUtil.WriteLogRaw("Current: ",T004H2_A1776AutorizacaoConsumo_VigenciaFim[0]);
               }
               if ( Z1779AutorizacaoConsumo_Quantidade != T004H2_A1779AutorizacaoConsumo_Quantidade[0] )
               {
                  GXUtil.WriteLog("autorizacaoconsumo:[seudo value changed for attri]"+"AutorizacaoConsumo_Quantidade");
                  GXUtil.WriteLogRaw("Old: ",Z1779AutorizacaoConsumo_Quantidade);
                  GXUtil.WriteLogRaw("Current: ",T004H2_A1779AutorizacaoConsumo_Quantidade[0]);
               }
               if ( Z1782AutorizacaoConsumo_Valor != T004H2_A1782AutorizacaoConsumo_Valor[0] )
               {
                  GXUtil.WriteLog("autorizacaoconsumo:[seudo value changed for attri]"+"AutorizacaoConsumo_Valor");
                  GXUtil.WriteLogRaw("Old: ",Z1782AutorizacaoConsumo_Valor);
                  GXUtil.WriteLogRaw("Current: ",T004H2_A1782AutorizacaoConsumo_Valor[0]);
               }
               if ( Z1780AutorizacaoConsumo_Ativo != T004H2_A1780AutorizacaoConsumo_Ativo[0] )
               {
                  GXUtil.WriteLog("autorizacaoconsumo:[seudo value changed for attri]"+"AutorizacaoConsumo_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z1780AutorizacaoConsumo_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T004H2_A1780AutorizacaoConsumo_Ativo[0]);
               }
               if ( StringUtil.StrCmp(Z1787AutorizacaoConsumo_Status, T004H2_A1787AutorizacaoConsumo_Status[0]) != 0 )
               {
                  GXUtil.WriteLog("autorizacaoconsumo:[seudo value changed for attri]"+"AutorizacaoConsumo_Status");
                  GXUtil.WriteLogRaw("Old: ",Z1787AutorizacaoConsumo_Status);
                  GXUtil.WriteLogRaw("Current: ",T004H2_A1787AutorizacaoConsumo_Status[0]);
               }
               if ( Z74Contrato_Codigo != T004H2_A74Contrato_Codigo[0] )
               {
                  GXUtil.WriteLog("autorizacaoconsumo:[seudo value changed for attri]"+"Contrato_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z74Contrato_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T004H2_A74Contrato_Codigo[0]);
               }
               if ( Z1777AutorizacaoConsumo_UnidadeMedicaoCod != T004H2_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0] )
               {
                  GXUtil.WriteLog("autorizacaoconsumo:[seudo value changed for attri]"+"AutorizacaoConsumo_UnidadeMedicaoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1777AutorizacaoConsumo_UnidadeMedicaoCod);
                  GXUtil.WriteLogRaw("Current: ",T004H2_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"AutorizacaoConsumo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4H197( )
      {
         BeforeValidate4H197( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4H197( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4H197( 0) ;
            CheckOptimisticConcurrency4H197( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4H197( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4H197( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004H13 */
                     pr_default.execute(11, new Object[] {A1775AutorizacaoConsumo_VigenciaInicio, A1776AutorizacaoConsumo_VigenciaFim, A1779AutorizacaoConsumo_Quantidade, A1782AutorizacaoConsumo_Valor, A1780AutorizacaoConsumo_Ativo, A1787AutorizacaoConsumo_Status, A74Contrato_Codigo, A1777AutorizacaoConsumo_UnidadeMedicaoCod});
                     A1774AutorizacaoConsumo_Codigo = T004H13_A1774AutorizacaoConsumo_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("AutorizacaoConsumo") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4H0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4H197( ) ;
            }
            EndLevel4H197( ) ;
         }
         CloseExtendedTableCursors4H197( ) ;
      }

      protected void Update4H197( )
      {
         BeforeValidate4H197( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4H197( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4H197( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4H197( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4H197( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004H14 */
                     pr_default.execute(12, new Object[] {A1775AutorizacaoConsumo_VigenciaInicio, A1776AutorizacaoConsumo_VigenciaFim, A1779AutorizacaoConsumo_Quantidade, A1782AutorizacaoConsumo_Valor, A1780AutorizacaoConsumo_Ativo, A1787AutorizacaoConsumo_Status, A74Contrato_Codigo, A1777AutorizacaoConsumo_UnidadeMedicaoCod, A1774AutorizacaoConsumo_Codigo});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("AutorizacaoConsumo") ;
                     if ( (pr_default.getStatus(12) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AutorizacaoConsumo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4H197( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4H197( ) ;
         }
         CloseExtendedTableCursors4H197( ) ;
      }

      protected void DeferredUpdate4H197( )
      {
      }

      protected void delete( )
      {
         BeforeValidate4H197( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4H197( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4H197( ) ;
            AfterConfirm4H197( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4H197( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004H15 */
                  pr_default.execute(13, new Object[] {A1774AutorizacaoConsumo_Codigo});
                  pr_default.close(13);
                  dsDefault.SmartCacheProvider.SetUpdated("AutorizacaoConsumo") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode197 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel4H197( ) ;
         Gx_mode = sMode197;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls4H197( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            if ( true /* After */ && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && new prc_autorizacaoconsumo_verificaativo(context).executeUdp(  A74Contrato_Codigo,  A1777AutorizacaoConsumo_UnidadeMedicaoCod) )
            {
               GX_msglist.addItem("J� existe uma autoriza��o de consumo ativa para esse contrato!", 1, "CONTRATO_CODIGO");
               AnyError = 1;
               GX_FocusControl = edtContrato_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            if ( true /* After */ && ( O1779AutorizacaoConsumo_Quantidade != A1779AutorizacaoConsumo_Quantidade ) && ( StringUtil.StrCmp(A1787AutorizacaoConsumo_Status, "AUT") == 0 ) && ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               GX_msglist.addItem("N�o � permitido alterar a quantidade ap�s a autoriza��o da Autoriza��o de Consumo!", 1, "AUTORIZACAOCONSUMO_QUANTIDADE");
               AnyError = 1;
               GX_FocusControl = edtAutorizacaoConsumo_Quantidade_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            if ( true /* After */ && ( O1782AutorizacaoConsumo_Valor != A1782AutorizacaoConsumo_Valor ) && ( StringUtil.StrCmp(A1787AutorizacaoConsumo_Status, "AUT") == 0 ) && ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               GX_msglist.addItem("N�o � permitido alterar o valor ap�s a autoriza��o da Autoriza��o de Consumo!", 1, "AUTORIZACAOCONSUMO_VALOR");
               AnyError = 1;
               GX_FocusControl = edtAutorizacaoConsumo_Valor_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            /* Using cursor T004H16 */
            pr_default.execute(14, new Object[] {A74Contrato_Codigo});
            A83Contrato_DataVigenciaTermino = T004H16_A83Contrato_DataVigenciaTermino[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
            A82Contrato_DataVigenciaInicio = T004H16_A82Contrato_DataVigenciaInicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
            pr_default.close(14);
            /* Using cursor T004H17 */
            pr_default.execute(15, new Object[] {A1777AutorizacaoConsumo_UnidadeMedicaoCod});
            A1778AutorizacaoConsumo_UnidadeMedicaoNom = T004H17_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1778AutorizacaoConsumo_UnidadeMedicaoNom", A1778AutorizacaoConsumo_UnidadeMedicaoNom);
            n1778AutorizacaoConsumo_UnidadeMedicaoNom = T004H17_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            A1788AutorizacaoConsumo_unidadeMedicaoSig = T004H17_A1788AutorizacaoConsumo_unidadeMedicaoSig[0];
            n1788AutorizacaoConsumo_unidadeMedicaoSig = T004H17_n1788AutorizacaoConsumo_unidadeMedicaoSig[0];
            pr_default.close(15);
         }
      }

      protected void EndLevel4H197( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4H197( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(14);
            pr_default.close(15);
            context.CommitDataStores( "AutorizacaoConsumo");
            if ( AnyError == 0 )
            {
               ConfirmValues4H0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(14);
            pr_default.close(15);
            context.RollbackDataStores( "AutorizacaoConsumo");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4H197( )
      {
         /* Scan By routine */
         /* Using cursor T004H18 */
         pr_default.execute(16);
         RcdFound197 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound197 = 1;
            A1774AutorizacaoConsumo_Codigo = T004H18_A1774AutorizacaoConsumo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4H197( )
      {
         /* Scan next routine */
         pr_default.readNext(16);
         RcdFound197 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound197 = 1;
            A1774AutorizacaoConsumo_Codigo = T004H18_A1774AutorizacaoConsumo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd4H197( )
      {
         pr_default.close(16);
      }

      protected void AfterConfirm4H197( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4H197( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4H197( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4H197( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4H197( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4H197( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4H197( )
      {
         edtAutorizacaoConsumo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAutorizacaoConsumo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAutorizacaoConsumo_Codigo_Enabled), 5, 0)));
         edtContrato_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Enabled), 5, 0)));
         edtContrato_DataVigenciaInicio_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_DataVigenciaInicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_DataVigenciaInicio_Enabled), 5, 0)));
         edtContrato_DataVigenciaTermino_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_DataVigenciaTermino_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_DataVigenciaTermino_Enabled), 5, 0)));
         edtAutorizacaoConsumo_VigenciaInicio_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAutorizacaoConsumo_VigenciaInicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAutorizacaoConsumo_VigenciaInicio_Enabled), 5, 0)));
         edtAutorizacaoConsumo_VigenciaFim_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAutorizacaoConsumo_VigenciaFim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAutorizacaoConsumo_VigenciaFim_Enabled), 5, 0)));
         dynAutorizacaoConsumo_UnidadeMedicaoCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAutorizacaoConsumo_UnidadeMedicaoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynAutorizacaoConsumo_UnidadeMedicaoCod.Enabled), 5, 0)));
         edtAutorizacaoConsumo_Quantidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAutorizacaoConsumo_Quantidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAutorizacaoConsumo_Quantidade_Enabled), 5, 0)));
         edtAutorizacaoConsumo_Valor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAutorizacaoConsumo_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAutorizacaoConsumo_Valor_Enabled), 5, 0)));
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAutorizacaoConsumo_UnidadeMedicaoNom_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4H0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020562356199");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("autorizacaoconsumo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7AutorizacaoConsumo_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1774AutorizacaoConsumo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1775AutorizacaoConsumo_VigenciaInicio", context.localUtil.DToC( Z1775AutorizacaoConsumo_VigenciaInicio, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1776AutorizacaoConsumo_VigenciaFim", context.localUtil.DToC( Z1776AutorizacaoConsumo_VigenciaFim, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1779AutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1779AutorizacaoConsumo_Quantidade), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1782AutorizacaoConsumo_Valor", StringUtil.LTrim( StringUtil.NToC( Z1782AutorizacaoConsumo_Valor, 18, 5, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1780AutorizacaoConsumo_Ativo", Z1780AutorizacaoConsumo_Ativo);
         GxWebStd.gx_hidden_field( context, "Z1787AutorizacaoConsumo_Status", StringUtil.RTrim( Z1787AutorizacaoConsumo_Status));
         GxWebStd.gx_hidden_field( context, "Z74Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1777AutorizacaoConsumo_UnidadeMedicaoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O1779AutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.NToC( (decimal)(O1779AutorizacaoConsumo_Quantidade), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O1782AutorizacaoConsumo_Valor", StringUtil.LTrim( StringUtil.NToC( O1782AutorizacaoConsumo_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N74Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N1777AutorizacaoConsumo_UnidadeMedicaoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vAUTORIZACAOCONSUMO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7AutorizacaoConsumo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "AUTORIZACAOCONSUMO_ATIVO", A1780AutorizacaoConsumo_Ativo);
         GxWebStd.gx_hidden_field( context, "AUTORIZACAOCONSUMO_STATUS", StringUtil.RTrim( A1787AutorizacaoConsumo_Status));
         GxWebStd.gx_hidden_field( context, "AUTORIZACAOCONSUMO_UNIDADEMEDICAOSIG", StringUtil.RTrim( A1788AutorizacaoConsumo_unidadeMedicaoSig));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV15Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vAUTORIZACAOCONSUMO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7AutorizacaoConsumo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "AutorizacaoConsumo";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1774AutorizacaoConsumo_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1780AutorizacaoConsumo_Ativo);
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1787AutorizacaoConsumo_Status, ""));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("autorizacaoconsumo:[SendSecurityCheck value for]"+"AutorizacaoConsumo_Codigo:"+context.localUtil.Format( (decimal)(A1774AutorizacaoConsumo_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("autorizacaoconsumo:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("autorizacaoconsumo:[SendSecurityCheck value for]"+"AutorizacaoConsumo_Ativo:"+StringUtil.BoolToStr( A1780AutorizacaoConsumo_Ativo));
         GXUtil.WriteLog("autorizacaoconsumo:[SendSecurityCheck value for]"+"AutorizacaoConsumo_Status:"+StringUtil.RTrim( context.localUtil.Format( A1787AutorizacaoConsumo_Status, "")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("autorizacaoconsumo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7AutorizacaoConsumo_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "AutorizacaoConsumo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Autoriza��o de Consumo" ;
      }

      protected void InitializeNonKey4H197( )
      {
         A74Contrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         A1777AutorizacaoConsumo_UnidadeMedicaoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1777AutorizacaoConsumo_UnidadeMedicaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0)));
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
         A1775AutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1775AutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"));
         A1776AutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1776AutorizacaoConsumo_VigenciaFim", context.localUtil.Format(A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"));
         A1778AutorizacaoConsumo_UnidadeMedicaoNom = "";
         n1778AutorizacaoConsumo_UnidadeMedicaoNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1778AutorizacaoConsumo_UnidadeMedicaoNom", A1778AutorizacaoConsumo_UnidadeMedicaoNom);
         A1788AutorizacaoConsumo_unidadeMedicaoSig = "";
         n1788AutorizacaoConsumo_unidadeMedicaoSig = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1788AutorizacaoConsumo_unidadeMedicaoSig", A1788AutorizacaoConsumo_unidadeMedicaoSig);
         A1779AutorizacaoConsumo_Quantidade = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1779AutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0)));
         A1782AutorizacaoConsumo_Valor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1782AutorizacaoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( A1782AutorizacaoConsumo_Valor, 18, 5)));
         A1780AutorizacaoConsumo_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1780AutorizacaoConsumo_Ativo", A1780AutorizacaoConsumo_Ativo);
         A1787AutorizacaoConsumo_Status = "CRI";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1787AutorizacaoConsumo_Status", A1787AutorizacaoConsumo_Status);
         O1779AutorizacaoConsumo_Quantidade = A1779AutorizacaoConsumo_Quantidade;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1779AutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0)));
         O1782AutorizacaoConsumo_Valor = A1782AutorizacaoConsumo_Valor;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1782AutorizacaoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( A1782AutorizacaoConsumo_Valor, 18, 5)));
         Z1775AutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
         Z1776AutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
         Z1779AutorizacaoConsumo_Quantidade = 0;
         Z1782AutorizacaoConsumo_Valor = 0;
         Z1780AutorizacaoConsumo_Ativo = false;
         Z1787AutorizacaoConsumo_Status = "";
         Z74Contrato_Codigo = 0;
         Z1777AutorizacaoConsumo_UnidadeMedicaoCod = 0;
      }

      protected void InitAll4H197( )
      {
         A1774AutorizacaoConsumo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
         InitializeNonKey4H197( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1780AutorizacaoConsumo_Ativo = i1780AutorizacaoConsumo_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1780AutorizacaoConsumo_Ativo", A1780AutorizacaoConsumo_Ativo);
         A1787AutorizacaoConsumo_Status = i1787AutorizacaoConsumo_Status;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1787AutorizacaoConsumo_Status", A1787AutorizacaoConsumo_Status);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205623561934");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("autorizacaoconsumo.js", "?20205623561934");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockautorizacaoconsumo_codigo_Internalname = "TEXTBLOCKAUTORIZACAOCONSUMO_CODIGO";
         edtAutorizacaoConsumo_Codigo_Internalname = "AUTORIZACAOCONSUMO_CODIGO";
         lblTextblockcontrato_codigo_Internalname = "TEXTBLOCKCONTRATO_CODIGO";
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO";
         lblTextblockcontrato_datavigenciainicio_Internalname = "TEXTBLOCKCONTRATO_DATAVIGENCIAINICIO";
         edtContrato_DataVigenciaInicio_Internalname = "CONTRATO_DATAVIGENCIAINICIO";
         lblTextblockcontrato_datavigenciatermino_Internalname = "TEXTBLOCKCONTRATO_DATAVIGENCIATERMINO";
         edtContrato_DataVigenciaTermino_Internalname = "CONTRATO_DATAVIGENCIATERMINO";
         tblTablemergedcontrato_codigo_Internalname = "TABLEMERGEDCONTRATO_CODIGO";
         lblTextblockautorizacaoconsumo_vigenciainicio_Internalname = "TEXTBLOCKAUTORIZACAOCONSUMO_VIGENCIAINICIO";
         edtAutorizacaoConsumo_VigenciaInicio_Internalname = "AUTORIZACAOCONSUMO_VIGENCIAINICIO";
         lblTextblockautorizacaoconsumo_vigenciafim_Internalname = "TEXTBLOCKAUTORIZACAOCONSUMO_VIGENCIAFIM";
         edtAutorizacaoConsumo_VigenciaFim_Internalname = "AUTORIZACAOCONSUMO_VIGENCIAFIM";
         lblTextblockautorizacaoconsumo_unidademedicaocod_Internalname = "TEXTBLOCKAUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD";
         dynAutorizacaoConsumo_UnidadeMedicaoCod_Internalname = "AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD";
         lblTextblockautorizacaoconsumo_quantidade_Internalname = "TEXTBLOCKAUTORIZACAOCONSUMO_QUANTIDADE";
         edtAutorizacaoConsumo_Quantidade_Internalname = "AUTORIZACAOCONSUMO_QUANTIDADE";
         lblTextblockautorizacaoconsumo_valor_Internalname = "TEXTBLOCKAUTORIZACAOCONSUMO_VALOR";
         edtAutorizacaoConsumo_Valor_Internalname = "AUTORIZACAOCONSUMO_VALOR";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname = "AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Autoriza��o de Consumo";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Autoriza��o de Consumo";
         edtContrato_DataVigenciaTermino_Jsonclick = "";
         edtContrato_DataVigenciaTermino_Enabled = 0;
         edtContrato_DataVigenciaInicio_Jsonclick = "";
         edtContrato_DataVigenciaInicio_Enabled = 0;
         edtContrato_Codigo_Jsonclick = "";
         edtContrato_Codigo_Enabled = 1;
         edtAutorizacaoConsumo_Valor_Jsonclick = "";
         edtAutorizacaoConsumo_Valor_Enabled = 1;
         edtAutorizacaoConsumo_Quantidade_Jsonclick = "";
         edtAutorizacaoConsumo_Quantidade_Enabled = 1;
         dynAutorizacaoConsumo_UnidadeMedicaoCod_Jsonclick = "";
         dynAutorizacaoConsumo_UnidadeMedicaoCod.Enabled = 1;
         edtAutorizacaoConsumo_VigenciaFim_Jsonclick = "";
         edtAutorizacaoConsumo_VigenciaFim_Enabled = 1;
         edtAutorizacaoConsumo_VigenciaInicio_Jsonclick = "";
         edtAutorizacaoConsumo_VigenciaInicio_Enabled = 1;
         edtAutorizacaoConsumo_Codigo_Jsonclick = "";
         edtAutorizacaoConsumo_Codigo_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Jsonclick = "";
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Enabled = 0;
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAAUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD4H1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAAUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD_data4H1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAAUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD_html4H1( )
      {
         int gxdynajaxvalue ;
         GXDLAAUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD_data4H1( ) ;
         gxdynajaxindex = 1;
         dynAutorizacaoConsumo_UnidadeMedicaoCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynAutorizacaoConsumo_UnidadeMedicaoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAAUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD_data4H1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T004H19 */
         pr_default.execute(17);
         while ( (pr_default.getStatus(17) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T004H19_A1197UnidadeMedicao_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T004H19_A1198UnidadeMedicao_Nome[0]));
            pr_default.readNext(17);
         }
         pr_default.close(17);
      }

      public void Valid_Contrato_codigo( String GX_Parm1 ,
                                         int GX_Parm2 ,
                                         DateTime GX_Parm3 ,
                                         DateTime GX_Parm4 )
      {
         Gx_mode = GX_Parm1;
         A74Contrato_Codigo = GX_Parm2;
         A83Contrato_DataVigenciaTermino = GX_Parm3;
         A82Contrato_DataVigenciaInicio = GX_Parm4;
         /* Using cursor T004H16 */
         pr_default.execute(14, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "CONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContrato_Codigo_Internalname;
         }
         A83Contrato_DataVigenciaTermino = T004H16_A83Contrato_DataVigenciaTermino[0];
         A82Contrato_DataVigenciaInicio = T004H16_A82Contrato_DataVigenciaInicio[0];
         pr_default.close(14);
         if ( true /* After */ && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && new prc_autorizacaoconsumo_verificaativo(context).executeUdp(  A74Contrato_Codigo,  A1777AutorizacaoConsumo_UnidadeMedicaoCod) )
         {
            GX_msglist.addItem("J� existe uma autoriza��o de consumo ativa para esse contrato!", 1, "CONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContrato_Codigo_Internalname;
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A83Contrato_DataVigenciaTermino = DateTime.MinValue;
            A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         }
         isValidOutput.Add(context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
         isValidOutput.Add(context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Autorizacaoconsumo_unidademedicaocod( GXCombobox dynGX_Parm1 ,
                                                              String GX_Parm2 ,
                                                              String GX_Parm3 )
      {
         dynAutorizacaoConsumo_UnidadeMedicaoCod = dynGX_Parm1;
         A1777AutorizacaoConsumo_UnidadeMedicaoCod = (int)(NumberUtil.Val( dynAutorizacaoConsumo_UnidadeMedicaoCod.CurrentValue, "."));
         A1778AutorizacaoConsumo_UnidadeMedicaoNom = GX_Parm2;
         n1778AutorizacaoConsumo_UnidadeMedicaoNom = false;
         A1788AutorizacaoConsumo_unidadeMedicaoSig = GX_Parm3;
         n1788AutorizacaoConsumo_unidadeMedicaoSig = false;
         /* Using cursor T004H17 */
         pr_default.execute(15, new Object[] {A1777AutorizacaoConsumo_UnidadeMedicaoCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Autorizacao Consumo Unidade Medicao'.", "ForeignKeyNotFound", 1, "AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD");
            AnyError = 1;
            GX_FocusControl = dynAutorizacaoConsumo_UnidadeMedicaoCod_Internalname;
         }
         A1778AutorizacaoConsumo_UnidadeMedicaoNom = T004H17_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
         n1778AutorizacaoConsumo_UnidadeMedicaoNom = T004H17_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
         A1788AutorizacaoConsumo_unidadeMedicaoSig = T004H17_A1788AutorizacaoConsumo_unidadeMedicaoSig[0];
         n1788AutorizacaoConsumo_unidadeMedicaoSig = T004H17_n1788AutorizacaoConsumo_unidadeMedicaoSig[0];
         pr_default.close(15);
         if ( (0==A1777AutorizacaoConsumo_UnidadeMedicaoCod) )
         {
            GX_msglist.addItem("Unidade de Medi��o � obrigat�rio.", 1, "AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD");
            AnyError = 1;
            GX_FocusControl = dynAutorizacaoConsumo_UnidadeMedicaoCod_Internalname;
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1778AutorizacaoConsumo_UnidadeMedicaoNom = "";
            n1778AutorizacaoConsumo_UnidadeMedicaoNom = false;
            A1788AutorizacaoConsumo_unidadeMedicaoSig = "";
            n1788AutorizacaoConsumo_unidadeMedicaoSig = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A1778AutorizacaoConsumo_UnidadeMedicaoNom));
         isValidOutput.Add(StringUtil.RTrim( A1788AutorizacaoConsumo_unidadeMedicaoSig));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Autorizacaoconsumo_valor( String GX_Parm1 ,
                                                  decimal GX_Parm2 ,
                                                  decimal GX_Parm3 ,
                                                  int GX_Parm4 ,
                                                  GXCombobox dynGX_Parm5 )
      {
         Gx_mode = GX_Parm1;
         O1782AutorizacaoConsumo_Valor = GX_Parm2;
         A1782AutorizacaoConsumo_Valor = GX_Parm3;
         A74Contrato_Codigo = GX_Parm4;
         dynAutorizacaoConsumo_UnidadeMedicaoCod = dynGX_Parm5;
         A1777AutorizacaoConsumo_UnidadeMedicaoCod = (int)(NumberUtil.Val( dynAutorizacaoConsumo_UnidadeMedicaoCod.CurrentValue, "."));
         if ( (Convert.ToDecimal(0)==A1782AutorizacaoConsumo_Valor) )
         {
            GX_msglist.addItem("Autorizacao Consumo_Valor � obrigat�rio.", 1, "AUTORIZACAOCONSUMO_VALOR");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_Valor_Internalname;
         }
         if ( A1782AutorizacaoConsumo_Valor > new prc_contratounidademedicao_saldo(context).executeUdp(  A74Contrato_Codigo,  A1777AutorizacaoConsumo_UnidadeMedicaoCod) )
         {
            GX_msglist.addItem("Valor da autorizacao de consumo n�o pode ser maior que o saldo do contrato!", 1, "AUTORIZACAOCONSUMO_VALOR");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_Valor_Internalname;
         }
         if ( true /* After */ && ( A1782AutorizacaoConsumo_Valor > new prc_contratounidademedicao_saldo(context).executeUdp(  A74Contrato_Codigo,  A1777AutorizacaoConsumo_UnidadeMedicaoCod) ) )
         {
            GX_msglist.addItem("Valor da Autoriza��o de consumo n�o pode ser maior que o Saldo do Contrato!", 1, "AUTORIZACAOCONSUMO_VALOR");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_Valor_Internalname;
         }
         if ( true /* After */ && ( O1782AutorizacaoConsumo_Valor != A1782AutorizacaoConsumo_Valor ) && ( StringUtil.StrCmp(A1787AutorizacaoConsumo_Status, "AUT") == 0 ) && ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            GX_msglist.addItem("N�o � permitido alterar o valor ap�s a autoriza��o da Autoriza��o de Consumo!", 1, "AUTORIZACAOCONSUMO_VALOR");
            AnyError = 1;
            GX_FocusControl = edtAutorizacaoConsumo_Valor_Internalname;
         }
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7AutorizacaoConsumo_Codigo',fld:'vAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E124H2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(14);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1775AutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
         Z1776AutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
         Z1787AutorizacaoConsumo_Status = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         T004H6_A1197UnidadeMedicao_Codigo = new int[1] ;
         T004H6_A1198UnidadeMedicao_Nome = new String[] {""} ;
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         A1778AutorizacaoConsumo_UnidadeMedicaoNom = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockautorizacaoconsumo_codigo_Jsonclick = "";
         lblTextblockcontrato_codigo_Jsonclick = "";
         lblTextblockautorizacaoconsumo_vigenciainicio_Jsonclick = "";
         A1775AutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
         lblTextblockautorizacaoconsumo_vigenciafim_Jsonclick = "";
         A1776AutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
         lblTextblockautorizacaoconsumo_unidademedicaocod_Jsonclick = "";
         lblTextblockautorizacaoconsumo_quantidade_Jsonclick = "";
         lblTextblockautorizacaoconsumo_valor_Jsonclick = "";
         lblTextblockcontrato_datavigenciainicio_Jsonclick = "";
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         lblTextblockcontrato_datavigenciatermino_Jsonclick = "";
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         A1787AutorizacaoConsumo_Status = "";
         A1788AutorizacaoConsumo_unidadeMedicaoSig = "";
         AV15Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode197 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z83Contrato_DataVigenciaTermino = DateTime.MinValue;
         Z82Contrato_DataVigenciaInicio = DateTime.MinValue;
         Z1778AutorizacaoConsumo_UnidadeMedicaoNom = "";
         Z1788AutorizacaoConsumo_unidadeMedicaoSig = "";
         T004H5_A1778AutorizacaoConsumo_UnidadeMedicaoNom = new String[] {""} ;
         T004H5_n1778AutorizacaoConsumo_UnidadeMedicaoNom = new bool[] {false} ;
         T004H5_A1788AutorizacaoConsumo_unidadeMedicaoSig = new String[] {""} ;
         T004H5_n1788AutorizacaoConsumo_unidadeMedicaoSig = new bool[] {false} ;
         T004H4_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         T004H4_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         T004H7_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         T004H7_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         T004H7_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         T004H7_A1775AutorizacaoConsumo_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         T004H7_A1776AutorizacaoConsumo_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         T004H7_A1778AutorizacaoConsumo_UnidadeMedicaoNom = new String[] {""} ;
         T004H7_n1778AutorizacaoConsumo_UnidadeMedicaoNom = new bool[] {false} ;
         T004H7_A1788AutorizacaoConsumo_unidadeMedicaoSig = new String[] {""} ;
         T004H7_n1788AutorizacaoConsumo_unidadeMedicaoSig = new bool[] {false} ;
         T004H7_A1779AutorizacaoConsumo_Quantidade = new short[1] ;
         T004H7_A1782AutorizacaoConsumo_Valor = new decimal[1] ;
         T004H7_A1780AutorizacaoConsumo_Ativo = new bool[] {false} ;
         T004H7_A1787AutorizacaoConsumo_Status = new String[] {""} ;
         T004H7_A74Contrato_Codigo = new int[1] ;
         T004H7_A1777AutorizacaoConsumo_UnidadeMedicaoCod = new int[1] ;
         T004H8_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         T004H8_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         T004H9_A1778AutorizacaoConsumo_UnidadeMedicaoNom = new String[] {""} ;
         T004H9_n1778AutorizacaoConsumo_UnidadeMedicaoNom = new bool[] {false} ;
         T004H9_A1788AutorizacaoConsumo_unidadeMedicaoSig = new String[] {""} ;
         T004H9_n1788AutorizacaoConsumo_unidadeMedicaoSig = new bool[] {false} ;
         T004H10_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         T004H3_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         T004H3_A1775AutorizacaoConsumo_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         T004H3_A1776AutorizacaoConsumo_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         T004H3_A1779AutorizacaoConsumo_Quantidade = new short[1] ;
         T004H3_A1782AutorizacaoConsumo_Valor = new decimal[1] ;
         T004H3_A1780AutorizacaoConsumo_Ativo = new bool[] {false} ;
         T004H3_A1787AutorizacaoConsumo_Status = new String[] {""} ;
         T004H3_A74Contrato_Codigo = new int[1] ;
         T004H3_A1777AutorizacaoConsumo_UnidadeMedicaoCod = new int[1] ;
         T004H11_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         T004H12_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         T004H2_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         T004H2_A1775AutorizacaoConsumo_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         T004H2_A1776AutorizacaoConsumo_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         T004H2_A1779AutorizacaoConsumo_Quantidade = new short[1] ;
         T004H2_A1782AutorizacaoConsumo_Valor = new decimal[1] ;
         T004H2_A1780AutorizacaoConsumo_Ativo = new bool[] {false} ;
         T004H2_A1787AutorizacaoConsumo_Status = new String[] {""} ;
         T004H2_A74Contrato_Codigo = new int[1] ;
         T004H2_A1777AutorizacaoConsumo_UnidadeMedicaoCod = new int[1] ;
         T004H13_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         T004H16_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         T004H16_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         T004H17_A1778AutorizacaoConsumo_UnidadeMedicaoNom = new String[] {""} ;
         T004H17_n1778AutorizacaoConsumo_UnidadeMedicaoNom = new bool[] {false} ;
         T004H17_A1788AutorizacaoConsumo_unidadeMedicaoSig = new String[] {""} ;
         T004H17_n1788AutorizacaoConsumo_unidadeMedicaoSig = new bool[] {false} ;
         T004H18_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i1787AutorizacaoConsumo_Status = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T004H19_A1197UnidadeMedicao_Codigo = new int[1] ;
         T004H19_A1198UnidadeMedicao_Nome = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.autorizacaoconsumo__default(),
            new Object[][] {
                new Object[] {
               T004H2_A1774AutorizacaoConsumo_Codigo, T004H2_A1775AutorizacaoConsumo_VigenciaInicio, T004H2_A1776AutorizacaoConsumo_VigenciaFim, T004H2_A1779AutorizacaoConsumo_Quantidade, T004H2_A1782AutorizacaoConsumo_Valor, T004H2_A1780AutorizacaoConsumo_Ativo, T004H2_A1787AutorizacaoConsumo_Status, T004H2_A74Contrato_Codigo, T004H2_A1777AutorizacaoConsumo_UnidadeMedicaoCod
               }
               , new Object[] {
               T004H3_A1774AutorizacaoConsumo_Codigo, T004H3_A1775AutorizacaoConsumo_VigenciaInicio, T004H3_A1776AutorizacaoConsumo_VigenciaFim, T004H3_A1779AutorizacaoConsumo_Quantidade, T004H3_A1782AutorizacaoConsumo_Valor, T004H3_A1780AutorizacaoConsumo_Ativo, T004H3_A1787AutorizacaoConsumo_Status, T004H3_A74Contrato_Codigo, T004H3_A1777AutorizacaoConsumo_UnidadeMedicaoCod
               }
               , new Object[] {
               T004H4_A83Contrato_DataVigenciaTermino, T004H4_A82Contrato_DataVigenciaInicio
               }
               , new Object[] {
               T004H5_A1778AutorizacaoConsumo_UnidadeMedicaoNom, T004H5_n1778AutorizacaoConsumo_UnidadeMedicaoNom, T004H5_A1788AutorizacaoConsumo_unidadeMedicaoSig, T004H5_n1788AutorizacaoConsumo_unidadeMedicaoSig
               }
               , new Object[] {
               T004H6_A1197UnidadeMedicao_Codigo, T004H6_A1198UnidadeMedicao_Nome
               }
               , new Object[] {
               T004H7_A1774AutorizacaoConsumo_Codigo, T004H7_A83Contrato_DataVigenciaTermino, T004H7_A82Contrato_DataVigenciaInicio, T004H7_A1775AutorizacaoConsumo_VigenciaInicio, T004H7_A1776AutorizacaoConsumo_VigenciaFim, T004H7_A1778AutorizacaoConsumo_UnidadeMedicaoNom, T004H7_n1778AutorizacaoConsumo_UnidadeMedicaoNom, T004H7_A1788AutorizacaoConsumo_unidadeMedicaoSig, T004H7_n1788AutorizacaoConsumo_unidadeMedicaoSig, T004H7_A1779AutorizacaoConsumo_Quantidade,
               T004H7_A1782AutorizacaoConsumo_Valor, T004H7_A1780AutorizacaoConsumo_Ativo, T004H7_A1787AutorizacaoConsumo_Status, T004H7_A74Contrato_Codigo, T004H7_A1777AutorizacaoConsumo_UnidadeMedicaoCod
               }
               , new Object[] {
               T004H8_A83Contrato_DataVigenciaTermino, T004H8_A82Contrato_DataVigenciaInicio
               }
               , new Object[] {
               T004H9_A1778AutorizacaoConsumo_UnidadeMedicaoNom, T004H9_n1778AutorizacaoConsumo_UnidadeMedicaoNom, T004H9_A1788AutorizacaoConsumo_unidadeMedicaoSig, T004H9_n1788AutorizacaoConsumo_unidadeMedicaoSig
               }
               , new Object[] {
               T004H10_A1774AutorizacaoConsumo_Codigo
               }
               , new Object[] {
               T004H11_A1774AutorizacaoConsumo_Codigo
               }
               , new Object[] {
               T004H12_A1774AutorizacaoConsumo_Codigo
               }
               , new Object[] {
               T004H13_A1774AutorizacaoConsumo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004H16_A83Contrato_DataVigenciaTermino, T004H16_A82Contrato_DataVigenciaInicio
               }
               , new Object[] {
               T004H17_A1778AutorizacaoConsumo_UnidadeMedicaoNom, T004H17_n1778AutorizacaoConsumo_UnidadeMedicaoNom, T004H17_A1788AutorizacaoConsumo_unidadeMedicaoSig, T004H17_n1788AutorizacaoConsumo_unidadeMedicaoSig
               }
               , new Object[] {
               T004H18_A1774AutorizacaoConsumo_Codigo
               }
               , new Object[] {
               T004H19_A1197UnidadeMedicao_Codigo, T004H19_A1198UnidadeMedicao_Nome
               }
            }
         );
         AV15Pgmname = "AutorizacaoConsumo";
         Z1787AutorizacaoConsumo_Status = "CRI";
         A1787AutorizacaoConsumo_Status = "CRI";
         i1787AutorizacaoConsumo_Status = "CRI";
         Z1780AutorizacaoConsumo_Ativo = true;
         A1780AutorizacaoConsumo_Ativo = true;
         i1780AutorizacaoConsumo_Ativo = true;
      }

      private short Z1779AutorizacaoConsumo_Quantidade ;
      private short O1779AutorizacaoConsumo_Quantidade ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A1779AutorizacaoConsumo_Quantidade ;
      private short Gx_BScreen ;
      private short RcdFound197 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7AutorizacaoConsumo_Codigo ;
      private int Z1774AutorizacaoConsumo_Codigo ;
      private int Z74Contrato_Codigo ;
      private int Z1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int N74Contrato_Codigo ;
      private int N1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int A74Contrato_Codigo ;
      private int A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int AV7AutorizacaoConsumo_Codigo ;
      private int trnEnded ;
      private int edtAutorizacaoConsumo_UnidadeMedicaoNom_Visible ;
      private int edtAutorizacaoConsumo_UnidadeMedicaoNom_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int A1774AutorizacaoConsumo_Codigo ;
      private int edtAutorizacaoConsumo_Codigo_Enabled ;
      private int edtAutorizacaoConsumo_VigenciaInicio_Enabled ;
      private int edtAutorizacaoConsumo_VigenciaFim_Enabled ;
      private int edtAutorizacaoConsumo_Quantidade_Enabled ;
      private int edtAutorizacaoConsumo_Valor_Enabled ;
      private int edtContrato_Codigo_Enabled ;
      private int edtContrato_DataVigenciaInicio_Enabled ;
      private int edtContrato_DataVigenciaTermino_Enabled ;
      private int AV11Insert_Contrato_Codigo ;
      private int AV13Insert_AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int AV16GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal Z1782AutorizacaoConsumo_Valor ;
      private decimal O1782AutorizacaoConsumo_Valor ;
      private decimal A1782AutorizacaoConsumo_Valor ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z1787AutorizacaoConsumo_Status ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContrato_Codigo_Internalname ;
      private String edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname ;
      private String A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private String edtAutorizacaoConsumo_UnidadeMedicaoNom_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockautorizacaoconsumo_codigo_Internalname ;
      private String lblTextblockautorizacaoconsumo_codigo_Jsonclick ;
      private String edtAutorizacaoConsumo_Codigo_Internalname ;
      private String edtAutorizacaoConsumo_Codigo_Jsonclick ;
      private String lblTextblockcontrato_codigo_Internalname ;
      private String lblTextblockcontrato_codigo_Jsonclick ;
      private String lblTextblockautorizacaoconsumo_vigenciainicio_Internalname ;
      private String lblTextblockautorizacaoconsumo_vigenciainicio_Jsonclick ;
      private String edtAutorizacaoConsumo_VigenciaInicio_Internalname ;
      private String edtAutorizacaoConsumo_VigenciaInicio_Jsonclick ;
      private String lblTextblockautorizacaoconsumo_vigenciafim_Internalname ;
      private String lblTextblockautorizacaoconsumo_vigenciafim_Jsonclick ;
      private String edtAutorizacaoConsumo_VigenciaFim_Internalname ;
      private String edtAutorizacaoConsumo_VigenciaFim_Jsonclick ;
      private String lblTextblockautorizacaoconsumo_unidademedicaocod_Internalname ;
      private String lblTextblockautorizacaoconsumo_unidademedicaocod_Jsonclick ;
      private String dynAutorizacaoConsumo_UnidadeMedicaoCod_Internalname ;
      private String dynAutorizacaoConsumo_UnidadeMedicaoCod_Jsonclick ;
      private String lblTextblockautorizacaoconsumo_quantidade_Internalname ;
      private String lblTextblockautorizacaoconsumo_quantidade_Jsonclick ;
      private String edtAutorizacaoConsumo_Quantidade_Internalname ;
      private String edtAutorizacaoConsumo_Quantidade_Jsonclick ;
      private String lblTextblockautorizacaoconsumo_valor_Internalname ;
      private String lblTextblockautorizacaoconsumo_valor_Jsonclick ;
      private String edtAutorizacaoConsumo_Valor_Internalname ;
      private String edtAutorizacaoConsumo_Valor_Jsonclick ;
      private String tblTablemergedcontrato_codigo_Internalname ;
      private String edtContrato_Codigo_Jsonclick ;
      private String lblTextblockcontrato_datavigenciainicio_Internalname ;
      private String lblTextblockcontrato_datavigenciainicio_Jsonclick ;
      private String edtContrato_DataVigenciaInicio_Internalname ;
      private String edtContrato_DataVigenciaInicio_Jsonclick ;
      private String lblTextblockcontrato_datavigenciatermino_Internalname ;
      private String lblTextblockcontrato_datavigenciatermino_Jsonclick ;
      private String edtContrato_DataVigenciaTermino_Internalname ;
      private String edtContrato_DataVigenciaTermino_Jsonclick ;
      private String A1787AutorizacaoConsumo_Status ;
      private String A1788AutorizacaoConsumo_unidadeMedicaoSig ;
      private String AV15Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode197 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private String Z1788AutorizacaoConsumo_unidadeMedicaoSig ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String i1787AutorizacaoConsumo_Status ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private DateTime Z1775AutorizacaoConsumo_VigenciaInicio ;
      private DateTime Z1776AutorizacaoConsumo_VigenciaFim ;
      private DateTime A1775AutorizacaoConsumo_VigenciaInicio ;
      private DateTime A1776AutorizacaoConsumo_VigenciaFim ;
      private DateTime A82Contrato_DataVigenciaInicio ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime Z83Contrato_DataVigenciaTermino ;
      private DateTime Z82Contrato_DataVigenciaInicio ;
      private bool Z1780AutorizacaoConsumo_Ativo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private bool A1780AutorizacaoConsumo_Ativo ;
      private bool n1788AutorizacaoConsumo_unidadeMedicaoSig ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i1780AutorizacaoConsumo_Ativo ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IDataStoreProvider pr_default ;
      private int[] T004H6_A1197UnidadeMedicao_Codigo ;
      private String[] T004H6_A1198UnidadeMedicao_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynAutorizacaoConsumo_UnidadeMedicaoCod ;
      private String[] T004H5_A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private bool[] T004H5_n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private String[] T004H5_A1788AutorizacaoConsumo_unidadeMedicaoSig ;
      private bool[] T004H5_n1788AutorizacaoConsumo_unidadeMedicaoSig ;
      private DateTime[] T004H4_A83Contrato_DataVigenciaTermino ;
      private DateTime[] T004H4_A82Contrato_DataVigenciaInicio ;
      private int[] T004H7_A1774AutorizacaoConsumo_Codigo ;
      private DateTime[] T004H7_A83Contrato_DataVigenciaTermino ;
      private DateTime[] T004H7_A82Contrato_DataVigenciaInicio ;
      private DateTime[] T004H7_A1775AutorizacaoConsumo_VigenciaInicio ;
      private DateTime[] T004H7_A1776AutorizacaoConsumo_VigenciaFim ;
      private String[] T004H7_A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private bool[] T004H7_n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private String[] T004H7_A1788AutorizacaoConsumo_unidadeMedicaoSig ;
      private bool[] T004H7_n1788AutorizacaoConsumo_unidadeMedicaoSig ;
      private short[] T004H7_A1779AutorizacaoConsumo_Quantidade ;
      private decimal[] T004H7_A1782AutorizacaoConsumo_Valor ;
      private bool[] T004H7_A1780AutorizacaoConsumo_Ativo ;
      private String[] T004H7_A1787AutorizacaoConsumo_Status ;
      private int[] T004H7_A74Contrato_Codigo ;
      private int[] T004H7_A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private DateTime[] T004H8_A83Contrato_DataVigenciaTermino ;
      private DateTime[] T004H8_A82Contrato_DataVigenciaInicio ;
      private String[] T004H9_A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private bool[] T004H9_n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private String[] T004H9_A1788AutorizacaoConsumo_unidadeMedicaoSig ;
      private bool[] T004H9_n1788AutorizacaoConsumo_unidadeMedicaoSig ;
      private int[] T004H10_A1774AutorizacaoConsumo_Codigo ;
      private int[] T004H3_A1774AutorizacaoConsumo_Codigo ;
      private DateTime[] T004H3_A1775AutorizacaoConsumo_VigenciaInicio ;
      private DateTime[] T004H3_A1776AutorizacaoConsumo_VigenciaFim ;
      private short[] T004H3_A1779AutorizacaoConsumo_Quantidade ;
      private decimal[] T004H3_A1782AutorizacaoConsumo_Valor ;
      private bool[] T004H3_A1780AutorizacaoConsumo_Ativo ;
      private String[] T004H3_A1787AutorizacaoConsumo_Status ;
      private int[] T004H3_A74Contrato_Codigo ;
      private int[] T004H3_A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int[] T004H11_A1774AutorizacaoConsumo_Codigo ;
      private int[] T004H12_A1774AutorizacaoConsumo_Codigo ;
      private int[] T004H2_A1774AutorizacaoConsumo_Codigo ;
      private DateTime[] T004H2_A1775AutorizacaoConsumo_VigenciaInicio ;
      private DateTime[] T004H2_A1776AutorizacaoConsumo_VigenciaFim ;
      private short[] T004H2_A1779AutorizacaoConsumo_Quantidade ;
      private decimal[] T004H2_A1782AutorizacaoConsumo_Valor ;
      private bool[] T004H2_A1780AutorizacaoConsumo_Ativo ;
      private String[] T004H2_A1787AutorizacaoConsumo_Status ;
      private int[] T004H2_A74Contrato_Codigo ;
      private int[] T004H2_A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int[] T004H13_A1774AutorizacaoConsumo_Codigo ;
      private DateTime[] T004H16_A83Contrato_DataVigenciaTermino ;
      private DateTime[] T004H16_A82Contrato_DataVigenciaInicio ;
      private String[] T004H17_A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private bool[] T004H17_n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private String[] T004H17_A1788AutorizacaoConsumo_unidadeMedicaoSig ;
      private bool[] T004H17_n1788AutorizacaoConsumo_unidadeMedicaoSig ;
      private int[] T004H18_A1774AutorizacaoConsumo_Codigo ;
      private int[] T004H19_A1197UnidadeMedicao_Codigo ;
      private String[] T004H19_A1198UnidadeMedicao_Nome ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class autorizacaoconsumo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004H6 ;
          prmT004H6 = new Object[] {
          } ;
          Object[] prmT004H7 ;
          prmT004H7 = new Object[] {
          new Object[] {"@AutorizacaoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004H4 ;
          prmT004H4 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004H5 ;
          prmT004H5 = new Object[] {
          new Object[] {"@AutorizacaoConsumo_UnidadeMedicaoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004H8 ;
          prmT004H8 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004H9 ;
          prmT004H9 = new Object[] {
          new Object[] {"@AutorizacaoConsumo_UnidadeMedicaoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004H10 ;
          prmT004H10 = new Object[] {
          new Object[] {"@AutorizacaoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004H3 ;
          prmT004H3 = new Object[] {
          new Object[] {"@AutorizacaoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004H11 ;
          prmT004H11 = new Object[] {
          new Object[] {"@AutorizacaoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004H12 ;
          prmT004H12 = new Object[] {
          new Object[] {"@AutorizacaoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004H2 ;
          prmT004H2 = new Object[] {
          new Object[] {"@AutorizacaoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004H13 ;
          prmT004H13 = new Object[] {
          new Object[] {"@AutorizacaoConsumo_VigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AutorizacaoConsumo_VigenciaFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AutorizacaoConsumo_Quantidade",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AutorizacaoConsumo_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AutorizacaoConsumo_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@AutorizacaoConsumo_Status",SqlDbType.Char,3,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AutorizacaoConsumo_UnidadeMedicaoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004H14 ;
          prmT004H14 = new Object[] {
          new Object[] {"@AutorizacaoConsumo_VigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AutorizacaoConsumo_VigenciaFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AutorizacaoConsumo_Quantidade",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AutorizacaoConsumo_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AutorizacaoConsumo_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@AutorizacaoConsumo_Status",SqlDbType.Char,3,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AutorizacaoConsumo_UnidadeMedicaoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AutorizacaoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004H15 ;
          prmT004H15 = new Object[] {
          new Object[] {"@AutorizacaoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004H18 ;
          prmT004H18 = new Object[] {
          } ;
          Object[] prmT004H19 ;
          prmT004H19 = new Object[] {
          } ;
          Object[] prmT004H16 ;
          prmT004H16 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004H17 ;
          prmT004H17 = new Object[] {
          new Object[] {"@AutorizacaoConsumo_UnidadeMedicaoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T004H2", "SELECT [AutorizacaoConsumo_Codigo], [AutorizacaoConsumo_VigenciaInicio], [AutorizacaoConsumo_VigenciaFim], [AutorizacaoConsumo_Quantidade], [AutorizacaoConsumo_Valor], [AutorizacaoConsumo_Ativo], [AutorizacaoConsumo_Status], [Contrato_Codigo], [AutorizacaoConsumo_UnidadeMedicaoCod] AS AutorizacaoConsumo_UnidadeMedicaoCod FROM [AutorizacaoConsumo] WITH (UPDLOCK) WHERE [AutorizacaoConsumo_Codigo] = @AutorizacaoConsumo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004H2,1,0,true,false )
             ,new CursorDef("T004H3", "SELECT [AutorizacaoConsumo_Codigo], [AutorizacaoConsumo_VigenciaInicio], [AutorizacaoConsumo_VigenciaFim], [AutorizacaoConsumo_Quantidade], [AutorizacaoConsumo_Valor], [AutorizacaoConsumo_Ativo], [AutorizacaoConsumo_Status], [Contrato_Codigo], [AutorizacaoConsumo_UnidadeMedicaoCod] AS AutorizacaoConsumo_UnidadeMedicaoCod FROM [AutorizacaoConsumo] WITH (NOLOCK) WHERE [AutorizacaoConsumo_Codigo] = @AutorizacaoConsumo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004H3,1,0,true,false )
             ,new CursorDef("T004H4", "SELECT [Contrato_DataVigenciaTermino], [Contrato_DataVigenciaInicio] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004H4,1,0,true,false )
             ,new CursorDef("T004H5", "SELECT [UnidadeMedicao_Nome] AS AutorizacaoConsumo_UnidadeMedicaoNom, [UnidadeMedicao_Sigla] AS AutorizacaoConsumo_unidadeMedicaoSig FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @AutorizacaoConsumo_UnidadeMedicaoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004H5,1,0,true,false )
             ,new CursorDef("T004H6", "SELECT [UnidadeMedicao_Codigo], [UnidadeMedicao_Nome] FROM [UnidadeMedicao] WITH (NOLOCK) ORDER BY [UnidadeMedicao_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004H6,0,0,true,false )
             ,new CursorDef("T004H7", "SELECT TM1.[AutorizacaoConsumo_Codigo], T2.[Contrato_DataVigenciaTermino], T2.[Contrato_DataVigenciaInicio], TM1.[AutorizacaoConsumo_VigenciaInicio], TM1.[AutorizacaoConsumo_VigenciaFim], T3.[UnidadeMedicao_Nome] AS AutorizacaoConsumo_UnidadeMedicaoNom, T3.[UnidadeMedicao_Sigla] AS AutorizacaoConsumo_unidadeMedicaoSig, TM1.[AutorizacaoConsumo_Quantidade], TM1.[AutorizacaoConsumo_Valor], TM1.[AutorizacaoConsumo_Ativo], TM1.[AutorizacaoConsumo_Status], TM1.[Contrato_Codigo], TM1.[AutorizacaoConsumo_UnidadeMedicaoCod] AS AutorizacaoConsumo_UnidadeMedicaoCod FROM (([AutorizacaoConsumo] TM1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = TM1.[Contrato_Codigo]) INNER JOIN [UnidadeMedicao] T3 WITH (NOLOCK) ON T3.[UnidadeMedicao_Codigo] = TM1.[AutorizacaoConsumo_UnidadeMedicaoCod]) WHERE TM1.[AutorizacaoConsumo_Codigo] = @AutorizacaoConsumo_Codigo ORDER BY TM1.[AutorizacaoConsumo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004H7,100,0,true,false )
             ,new CursorDef("T004H8", "SELECT [Contrato_DataVigenciaTermino], [Contrato_DataVigenciaInicio] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004H8,1,0,true,false )
             ,new CursorDef("T004H9", "SELECT [UnidadeMedicao_Nome] AS AutorizacaoConsumo_UnidadeMedicaoNom, [UnidadeMedicao_Sigla] AS AutorizacaoConsumo_unidadeMedicaoSig FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @AutorizacaoConsumo_UnidadeMedicaoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004H9,1,0,true,false )
             ,new CursorDef("T004H10", "SELECT [AutorizacaoConsumo_Codigo] FROM [AutorizacaoConsumo] WITH (NOLOCK) WHERE [AutorizacaoConsumo_Codigo] = @AutorizacaoConsumo_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004H10,1,0,true,false )
             ,new CursorDef("T004H11", "SELECT TOP 1 [AutorizacaoConsumo_Codigo] FROM [AutorizacaoConsumo] WITH (NOLOCK) WHERE ( [AutorizacaoConsumo_Codigo] > @AutorizacaoConsumo_Codigo) ORDER BY [AutorizacaoConsumo_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004H11,1,0,true,true )
             ,new CursorDef("T004H12", "SELECT TOP 1 [AutorizacaoConsumo_Codigo] FROM [AutorizacaoConsumo] WITH (NOLOCK) WHERE ( [AutorizacaoConsumo_Codigo] < @AutorizacaoConsumo_Codigo) ORDER BY [AutorizacaoConsumo_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004H12,1,0,true,true )
             ,new CursorDef("T004H13", "INSERT INTO [AutorizacaoConsumo]([AutorizacaoConsumo_VigenciaInicio], [AutorizacaoConsumo_VigenciaFim], [AutorizacaoConsumo_Quantidade], [AutorizacaoConsumo_Valor], [AutorizacaoConsumo_Ativo], [AutorizacaoConsumo_Status], [Contrato_Codigo], [AutorizacaoConsumo_UnidadeMedicaoCod]) VALUES(@AutorizacaoConsumo_VigenciaInicio, @AutorizacaoConsumo_VigenciaFim, @AutorizacaoConsumo_Quantidade, @AutorizacaoConsumo_Valor, @AutorizacaoConsumo_Ativo, @AutorizacaoConsumo_Status, @Contrato_Codigo, @AutorizacaoConsumo_UnidadeMedicaoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT004H13)
             ,new CursorDef("T004H14", "UPDATE [AutorizacaoConsumo] SET [AutorizacaoConsumo_VigenciaInicio]=@AutorizacaoConsumo_VigenciaInicio, [AutorizacaoConsumo_VigenciaFim]=@AutorizacaoConsumo_VigenciaFim, [AutorizacaoConsumo_Quantidade]=@AutorizacaoConsumo_Quantidade, [AutorizacaoConsumo_Valor]=@AutorizacaoConsumo_Valor, [AutorizacaoConsumo_Ativo]=@AutorizacaoConsumo_Ativo, [AutorizacaoConsumo_Status]=@AutorizacaoConsumo_Status, [Contrato_Codigo]=@Contrato_Codigo, [AutorizacaoConsumo_UnidadeMedicaoCod]=@AutorizacaoConsumo_UnidadeMedicaoCod  WHERE [AutorizacaoConsumo_Codigo] = @AutorizacaoConsumo_Codigo", GxErrorMask.GX_NOMASK,prmT004H14)
             ,new CursorDef("T004H15", "DELETE FROM [AutorizacaoConsumo]  WHERE [AutorizacaoConsumo_Codigo] = @AutorizacaoConsumo_Codigo", GxErrorMask.GX_NOMASK,prmT004H15)
             ,new CursorDef("T004H16", "SELECT [Contrato_DataVigenciaTermino], [Contrato_DataVigenciaInicio] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004H16,1,0,true,false )
             ,new CursorDef("T004H17", "SELECT [UnidadeMedicao_Nome] AS AutorizacaoConsumo_UnidadeMedicaoNom, [UnidadeMedicao_Sigla] AS AutorizacaoConsumo_unidadeMedicaoSig FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @AutorizacaoConsumo_UnidadeMedicaoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004H17,1,0,true,false )
             ,new CursorDef("T004H18", "SELECT [AutorizacaoConsumo_Codigo] FROM [AutorizacaoConsumo] WITH (NOLOCK) ORDER BY [AutorizacaoConsumo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004H18,100,0,true,false )
             ,new CursorDef("T004H19", "SELECT [UnidadeMedicao_Codigo], [UnidadeMedicao_Nome] FROM [UnidadeMedicao] WITH (NOLOCK) ORDER BY [UnidadeMedicao_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004H19,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[5])[0] = rslt.getBool(6) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 3) ;
                ((int[]) buf[7])[0] = rslt.getInt(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[5])[0] = rslt.getBool(6) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 3) ;
                ((int[]) buf[7])[0] = rslt.getInt(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                return;
             case 2 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((short[]) buf[9])[0] = rslt.getShort(8) ;
                ((decimal[]) buf[10])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[11])[0] = rslt.getBool(10) ;
                ((String[]) buf[12])[0] = rslt.getString(11, 3) ;
                ((int[]) buf[13])[0] = rslt.getInt(12) ;
                ((int[]) buf[14])[0] = rslt.getInt(13) ;
                return;
             case 6 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (bool)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                stmt.SetParameter(7, (int)parms[6]);
                stmt.SetParameter(8, (int)parms[7]);
                return;
             case 12 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (bool)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                stmt.SetParameter(7, (int)parms[6]);
                stmt.SetParameter(8, (int)parms[7]);
                stmt.SetParameter(9, (int)parms[8]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
