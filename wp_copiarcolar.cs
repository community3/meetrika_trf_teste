/*
               File: WP_CopiarColar
        Description: Copiar Colar Tabelas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:43:26.41
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_copiarcolar : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_copiarcolar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_copiarcolar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Sistema_Codigo ,
                           ref String aP1_Entidade )
      {
         this.AV13Sistema_Codigo = aP0_Sistema_Codigo;
         this.AV17Entidade = aP1_Entidade;
         executePrivate();
         aP1_Entidade=this.AV17Entidade;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavModulo_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vMODULO_CODIGO") == 0 )
            {
               AV13Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Sistema_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13Sistema_Codigo), "ZZZZZ9")));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvMODULO_CODIGO9X2( AV13Sistema_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV13Sistema_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Sistema_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13Sistema_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV17Entidade = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Entidade", AV17Entidade);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA9X2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START9X2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020548432660");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_copiarcolar.aspx") + "?" + UrlEncode("" +AV13Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV17Entidade))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vENTIDADE", StringUtil.RTrim( AV17Entidade));
         GxWebStd.gx_hidden_field( context, "TABELA_NOME", StringUtil.RTrim( A173Tabela_Nome));
         GxWebStd.gx_hidden_field( context, "TABELA_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUDPARG1", AV24Udparg1);
         GxWebStd.gx_hidden_field( context, "vNOMEAINSERIR", StringUtil.RTrim( AV19NomeAInserir));
         GxWebStd.gx_hidden_field( context, "SISTEMA_NOME", A416Sistema_Nome);
         GxWebStd.gx_hidden_field( context, "SISTEMA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A135Sistema_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV16WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV16WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vUDPARG2", AV26Udparg2);
         GxWebStd.gx_hidden_field( context, "vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13Sistema_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE9X2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT9X2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_copiarcolar.aspx") + "?" + UrlEncode("" +AV13Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV17Entidade)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_CopiarColar" ;
      }

      public override String GetPgmdesc( )
      {
         return "Copiar Colar Tabelas" ;
      }

      protected void WB9X0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_9X2( true) ;
         }
         else
         {
            wb_table1_2_9X2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_9X2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table2_7_9X2( true) ;
         }
         else
         {
            wb_table2_7_9X2( false) ;
         }
         return  ;
      }

      protected void wb_table2_7_9X2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START9X2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Copiar Colar Tabelas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP9X0( ) ;
      }

      protected void WS9X2( )
      {
         START9X2( ) ;
         EVT9X2( ) ;
      }

      protected void EVT9X2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E119X2 */
                              E119X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'GRAVAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E129X2 */
                              E129X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E139X2 */
                              E139X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E149X2 */
                              E149X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE9X2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA9X2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavModulo_codigo.Name = "vMODULO_CODIGO";
            dynavModulo_codigo.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavModulo_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvMODULO_CODIGO9X2( int AV13Sistema_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvMODULO_CODIGO_data9X2( AV13Sistema_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvMODULO_CODIGO_html9X2( int AV13Sistema_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvMODULO_CODIGO_data9X2( AV13Sistema_Codigo) ;
         gxdynajaxindex = 1;
         dynavModulo_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavModulo_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavModulo_codigo.ItemCount > 0 )
         {
            AV10Modulo_Codigo = (int)(NumberUtil.Val( dynavModulo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10Modulo_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Modulo_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvMODULO_CODIGO_data9X2( int AV13Sistema_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H009X2 */
         pr_default.execute(0, new Object[] {AV13Sistema_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H009X2_A146Modulo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H009X2_A143Modulo_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavModulo_codigo.ItemCount > 0 )
         {
            AV10Modulo_Codigo = (int)(NumberUtil.Val( dynavModulo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10Modulo_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Modulo_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9X2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF9X2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E149X2 */
            E149X2 ();
            WB9X0( ) ;
         }
      }

      protected void STRUP9X0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         GXVvMODULO_CODIGO_html9X2( AV13Sistema_Codigo) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E119X2 */
         E119X2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavModulo_codigo.CurrentValue = cgiGet( dynavModulo_codigo_Internalname);
            AV10Modulo_Codigo = (int)(NumberUtil.Val( cgiGet( dynavModulo_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Modulo_Codigo), 6, 0)));
            AV6Dados = cgiGet( edtavDados_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Dados", AV6Dados);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavInseridas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavInseridas_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vINSERIDAS");
               GX_FocusControl = edtavInseridas_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8Inseridas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Inseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Inseridas), 4, 0)));
            }
            else
            {
               AV8Inseridas = (short)(context.localUtil.CToN( cgiGet( edtavInseridas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Inseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Inseridas), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavJacadastradas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavJacadastradas_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vJACADASTRADAS");
               GX_FocusControl = edtavJacadastradas_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9JaCadastradas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9JaCadastradas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9JaCadastradas), 4, 0)));
            }
            else
            {
               AV9JaCadastradas = (short)(context.localUtil.CToN( cgiGet( edtavJacadastradas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9JaCadastradas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9JaCadastradas), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavNaoinseridas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavNaoinseridas_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vNAOINSERIDAS");
               GX_FocusControl = edtavNaoinseridas_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV11NaoInseridas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11NaoInseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11NaoInseridas), 4, 0)));
            }
            else
            {
               AV11NaoInseridas = (short)(context.localUtil.CToN( cgiGet( edtavNaoinseridas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11NaoInseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11NaoInseridas), 4, 0)));
            }
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvMODULO_CODIGO_html9X2( AV13Sistema_Codigo) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E119X2 */
         E119X2 ();
         if (returnInSub) return;
      }

      protected void E119X2( )
      {
         /* Start Routine */
         tblTblresultado_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblresultado_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblresultado_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV17Entidade, "Tab") == 0 )
         {
            /* Using cursor H009X3 */
            pr_default.execute(1, new Object[] {AV13Sistema_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A127Sistema_Codigo = H009X3_A127Sistema_Codigo[0];
               A416Sistema_Nome = H009X3_A416Sistema_Nome[0];
               lblTbsistemanome_Caption = StringUtil.Trim( A416Sistema_Nome);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbsistemanome_Internalname, "Caption", lblTbsistemanome_Caption);
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
         }
         else
         {
            tblTable2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTable2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTable2_Visible), 5, 0)));
            lblTextblock1_Caption = "Sistemas para inserir:";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblock1_Internalname, "Caption", lblTextblock1_Caption);
            lblTextblock2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblock2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblock2_Visible), 5, 0)));
            dynavModulo_codigo.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavModulo_codigo.Visible), 5, 0)));
         }
      }

      protected void E129X2( )
      {
         /* 'Gravar' Routine */
         AV7i = 0;
         AV19NomeAInserir = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NomeAInserir", AV19NomeAInserir);
         AV8Inseridas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Inseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Inseridas), 4, 0)));
         AV9JaCadastradas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9JaCadastradas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9JaCadastradas), 4, 0)));
         AV11NaoInseridas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11NaoInseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11NaoInseridas), 4, 0)));
         AV7i = 1;
         while ( AV7i <= StringUtil.Len( StringUtil.Trim( AV6Dados)) )
         {
            AV12s = (short)(AV7i+1);
            AV5Char = StringUtil.Substring( AV6Dados, AV7i, 1);
            if ( StringUtil.StrCmp(AV5Char, " ") == 0 )
            {
               if ( StringUtil.Asc( StringUtil.Substring( AV6Dados, AV12s, 1)) > 32 )
               {
                  if ( StringUtil.Len( AV19NomeAInserir) > 1 )
                  {
                     AV19NomeAInserir = AV19NomeAInserir + "_";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NomeAInserir", AV19NomeAInserir);
                  }
               }
               else
               {
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19NomeAInserir)) )
                  {
                     /* Execute user subroutine: 'NEW' */
                     S112 ();
                     if (returnInSub) return;
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV5Char, "�") == 0 )
            {
               AV19NomeAInserir = AV19NomeAInserir + "o";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NomeAInserir", AV19NomeAInserir);
            }
            else if ( StringUtil.StrCmp(AV5Char, "�") == 0 )
            {
               AV19NomeAInserir = AV19NomeAInserir + "a";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NomeAInserir", AV19NomeAInserir);
            }
            else if ( ( StringUtil.StrCmp(AV5Char, ",") == 0 ) || ( StringUtil.StrCmp(AV5Char, ";") == 0 ) || ( StringUtil.StrCmp(AV5Char, StringUtil.Chr( 9)) == 0 ) || ( StringUtil.StrCmp(AV5Char, StringUtil.Chr( 10)) == 0 ) || ( StringUtil.StrCmp(AV5Char, StringUtil.Chr( 13)) == 0 ) )
            {
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19NomeAInserir)) )
               {
                  /* Execute user subroutine: 'NEW' */
                  S112 ();
                  if (returnInSub) return;
               }
            }
            else if ( AV7i == StringUtil.Len( StringUtil.Trim( AV6Dados)) )
            {
               AV19NomeAInserir = AV19NomeAInserir + AV5Char;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NomeAInserir", AV19NomeAInserir);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19NomeAInserir)) )
               {
                  /* Execute user subroutine: 'NEW' */
                  S112 ();
                  if (returnInSub) return;
               }
            }
            else
            {
               AV19NomeAInserir = AV19NomeAInserir + AV5Char;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NomeAInserir", AV19NomeAInserir);
            }
            AV7i = (short)(AV7i+1);
         }
         tblTblresultado_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblresultado_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblresultado_Visible), 5, 0)));
      }

      protected void S112( )
      {
         /* 'NEW' Routine */
         if ( StringUtil.StrCmp(AV17Entidade, "Tab") == 0 )
         {
            /* Execute user subroutine: 'NEWTABELA' */
            S122 ();
            if (returnInSub) return;
         }
         else if ( StringUtil.StrCmp(AV17Entidade, "Sis") == 0 )
         {
            /* Execute user subroutine: 'NEWSISTEMA' */
            S132 ();
            if (returnInSub) return;
         }
      }

      protected void S122( )
      {
         /* 'NEWTABELA' Routine */
         AV23GXLvl72 = 0;
         AV24Udparg1 = new prc_padronizastring(context).executeUdp(  AV19NomeAInserir);
         /* Using cursor H009X4 */
         pr_default.execute(2, new Object[] {AV13Sistema_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A190Tabela_SistemaCod = H009X4_A190Tabela_SistemaCod[0];
            A173Tabela_Nome = H009X4_A173Tabela_Nome[0];
            if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A173Tabela_Nome), AV24Udparg1) == 0 )
            {
               AV23GXLvl72 = 1;
               AV9JaCadastradas = (short)(AV9JaCadastradas+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9JaCadastradas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9JaCadastradas), 4, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
         if ( AV23GXLvl72 == 0 )
         {
            AV14Tabela = new SdtTabela(context);
            AV14Tabela.gxTpr_Tabela_nome = StringUtil.Upper( AV19NomeAInserir);
            AV14Tabela.gxTpr_Tabela_sistemacod = AV13Sistema_Codigo;
            AV14Tabela.gxTpr_Tabela_modulocod = AV10Modulo_Codigo;
            AV14Tabela.Save();
            if ( AV14Tabela.Success() )
            {
               context.CommitDataStores( "WP_CopiarColar");
               AV8Inseridas = (short)(AV8Inseridas+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Inseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Inseridas), 4, 0)));
            }
            else
            {
               AV11NaoInseridas = (short)(AV11NaoInseridas+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11NaoInseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11NaoInseridas), 4, 0)));
               context.RollbackDataStores( "WP_CopiarColar");
            }
         }
         AV19NomeAInserir = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NomeAInserir", AV19NomeAInserir);
      }

      protected void S132( )
      {
         /* 'NEWSISTEMA' Routine */
         AV25GXLvl95 = 0;
         AV26Udparg2 = new prc_padronizastring(context).executeUdp(  AV19NomeAInserir);
         /* Using cursor H009X5 */
         pr_default.execute(3, new Object[] {AV16WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A135Sistema_AreaTrabalhoCod = H009X5_A135Sistema_AreaTrabalhoCod[0];
            A416Sistema_Nome = H009X5_A416Sistema_Nome[0];
            if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A416Sistema_Nome), AV26Udparg2) == 0 )
            {
               AV25GXLvl95 = 1;
               AV9JaCadastradas = (short)(AV9JaCadastradas+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9JaCadastradas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9JaCadastradas), 4, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(3);
         }
         pr_default.close(3);
         if ( AV25GXLvl95 == 0 )
         {
            AV18Sistema = new SdtSistema(context);
            AV18Sistema.gxTpr_Sistema_nome = StringUtil.Upper( AV19NomeAInserir);
            AV18Sistema.gxTpr_Sistema_sigla = StringUtil.Substring( AV18Sistema.gxTpr_Sistema_nome, 1, 15);
            AV18Sistema.gxTpr_Sistema_areatrabalhocod = AV16WWPContext.gxTpr_Areatrabalho_codigo;
            AV18Sistema.gxTv_SdtSistema_Ambientetecnologico_codigo_SetNull();
            AV18Sistema.gxTv_SdtSistema_Metodologia_codigo_SetNull();
            AV18Sistema.Save();
            if ( AV18Sistema.Success() )
            {
               context.CommitDataStores( "WP_CopiarColar");
               AV8Inseridas = (short)(AV8Inseridas+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Inseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Inseridas), 4, 0)));
            }
            else
            {
               AV11NaoInseridas = (short)(AV11NaoInseridas+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11NaoInseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11NaoInseridas), 4, 0)));
               context.RollbackDataStores( "WP_CopiarColar");
            }
         }
         AV19NomeAInserir = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NomeAInserir", AV19NomeAInserir);
      }

      protected void E139X2( )
      {
         /* 'DoFechar' Routine */
         if ( StringUtil.StrCmp(AV17Entidade, "Tab") == 0 )
         {
            context.wjLoc = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV13Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            context.wjLocDisableFrm = 1;
         }
         else if ( StringUtil.StrCmp(AV17Entidade, "Sis") == 0 )
         {
            context.wjLoc = formatLink("wwsistema.aspx") ;
            context.wjLocDisableFrm = 1;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E149X2( )
      {
         /* Load Routine */
      }

      protected void wb_table2_7_9X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, lblTextblock1_Caption, "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CopiarColar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "M�dulo:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTextblock2_Visible, 1, 0, "HLP_WP_CopiarColar.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavModulo_codigo, dynavModulo_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV10Modulo_Codigo), 6, 0)), 1, dynavModulo_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavModulo_codigo.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"", "", true, "HLP_WP_CopiarColar.htm");
            dynavModulo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10Modulo_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Values", (String)(dynavModulo_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDados_Internalname, AV6Dados, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,17);\"", 1, 1, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "4000", -1, "", "", -1, true, "", "HLP_WP_CopiarColar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table3_19_9X2( true) ;
         }
         else
         {
            wb_table3_19_9X2( false) ;
         }
         return  ;
      }

      protected void wb_table3_19_9X2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttGravar_Internalname, "", "Gravar", bttGravar_Jsonclick, 5, "Gravar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'GRAVAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_CopiarColar.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_CopiarColar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_7_9X2e( true) ;
         }
         else
         {
            wb_table2_7_9X2e( false) ;
         }
      }

      protected void wb_table3_19_9X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblresultado_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblresultado_Internalname, tblTblresultado_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Inseridas:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CopiarColar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavInseridas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Inseridas), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8Inseridas), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,24);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavInseridas_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_CopiarColar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Procuradas:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CopiarColar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavJacadastradas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9JaCadastradas), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9JaCadastradas), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,29);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavJacadastradas_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_CopiarColar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "Erros:", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CopiarColar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNaoinseridas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11NaoInseridas), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV11NaoInseridas), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNaoinseridas_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_CopiarColar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_19_9X2e( true) ;
         }
         else
         {
            wb_table3_19_9X2e( false) ;
         }
      }

      protected void wb_table1_2_9X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTable2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "Sistema:", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CopiarColar.htm");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbsistemanome_Internalname, lblTbsistemanome_Caption, "", "", lblTbsistemanome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:bold; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CopiarColar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_9X2e( true) ;
         }
         else
         {
            wb_table1_2_9X2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV13Sistema_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Sistema_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13Sistema_Codigo), "ZZZZZ9")));
         AV17Entidade = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Entidade", AV17Entidade);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9X2( ) ;
         WS9X2( ) ;
         WE9X2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202054843271");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_copiarcolar.js", "?202054843271");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock6_Internalname = "TEXTBLOCK6";
         lblTbsistemanome_Internalname = "TBSISTEMANOME";
         tblTable2_Internalname = "TABLE2";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         dynavModulo_codigo_Internalname = "vMODULO_CODIGO";
         edtavDados_Internalname = "vDADOS";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavInseridas_Internalname = "vINSERIDAS";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavJacadastradas_Internalname = "vJACADASTRADAS";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         edtavNaoinseridas_Internalname = "vNAOINSERIDAS";
         tblTblresultado_Internalname = "TBLRESULTADO";
         bttGravar_Internalname = "GRAVAR";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavNaoinseridas_Jsonclick = "";
         edtavJacadastradas_Jsonclick = "";
         edtavInseridas_Jsonclick = "";
         dynavModulo_codigo_Jsonclick = "";
         lblTextblock2_Visible = 1;
         dynavModulo_codigo.Visible = 1;
         lblTextblock1_Caption = "Tabelas para inserir:";
         tblTable2_Visible = 1;
         lblTbsistemanome_Caption = "Nome do Sistema";
         tblTblresultado_Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Copiar Colar Tabelas";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'GRAVAR'","{handler:'E129X2',iparms:[{av:'AV6Dados',fld:'vDADOS',pic:'',nv:''},{av:'AV17Entidade',fld:'vENTIDADE',pic:'@!',nv:''},{av:'A173Tabela_Nome',fld:'TABELA_NOME',pic:'@!',nv:''},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV13Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV24Udparg1',fld:'vUDPARG1',pic:'',nv:''},{av:'AV9JaCadastradas',fld:'vJACADASTRADAS',pic:'ZZZ9',nv:0},{av:'AV19NomeAInserir',fld:'vNOMEAINSERIR',pic:'@!',nv:''},{av:'AV10Modulo_Codigo',fld:'vMODULO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8Inseridas',fld:'vINSERIDAS',pic:'ZZZ9',nv:0},{av:'AV11NaoInseridas',fld:'vNAOINSERIDAS',pic:'ZZZ9',nv:0},{av:'A416Sistema_Nome',fld:'SISTEMA_NOME',pic:'@!',nv:''},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV26Udparg2',fld:'vUDPARG2',pic:'',nv:''}],oparms:[{av:'AV19NomeAInserir',fld:'vNOMEAINSERIR',pic:'@!',nv:''},{av:'AV8Inseridas',fld:'vINSERIDAS',pic:'ZZZ9',nv:0},{av:'AV9JaCadastradas',fld:'vJACADASTRADAS',pic:'ZZZ9',nv:0},{av:'AV11NaoInseridas',fld:'vNAOINSERIDAS',pic:'ZZZ9',nv:0},{av:'tblTblresultado_Visible',ctrl:'TBLRESULTADO',prop:'Visible'}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E139X2',iparms:[{av:'AV17Entidade',fld:'vENTIDADE',pic:'@!',nv:''},{av:'AV13Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV17Entidade = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A173Tabela_Nome = "";
         AV24Udparg1 = "";
         AV19NomeAInserir = "";
         A416Sistema_Nome = "";
         AV16WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV26Udparg2 = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H009X2_A146Modulo_Codigo = new int[1] ;
         H009X2_A143Modulo_Nome = new String[] {""} ;
         H009X2_A130Sistema_Ativo = new bool[] {false} ;
         H009X2_A127Sistema_Codigo = new int[1] ;
         AV6Dados = "";
         H009X3_A127Sistema_Codigo = new int[1] ;
         H009X3_A416Sistema_Nome = new String[] {""} ;
         AV5Char = "";
         H009X4_A172Tabela_Codigo = new int[1] ;
         H009X4_A190Tabela_SistemaCod = new int[1] ;
         H009X4_A173Tabela_Nome = new String[] {""} ;
         AV14Tabela = new SdtTabela(context);
         H009X5_A127Sistema_Codigo = new int[1] ;
         H009X5_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H009X5_A416Sistema_Nome = new String[] {""} ;
         AV18Sistema = new SdtSistema(context);
         sStyleString = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttGravar_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         lblTextblock6_Jsonclick = "";
         lblTbsistemanome_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_copiarcolar__default(),
            new Object[][] {
                new Object[] {
               H009X2_A146Modulo_Codigo, H009X2_A143Modulo_Nome, H009X2_A130Sistema_Ativo, H009X2_A127Sistema_Codigo
               }
               , new Object[] {
               H009X3_A127Sistema_Codigo, H009X3_A416Sistema_Nome
               }
               , new Object[] {
               H009X4_A172Tabela_Codigo, H009X4_A190Tabela_SistemaCod, H009X4_A173Tabela_Nome
               }
               , new Object[] {
               H009X5_A127Sistema_Codigo, H009X5_A135Sistema_AreaTrabalhoCod, H009X5_A416Sistema_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV8Inseridas ;
      private short AV9JaCadastradas ;
      private short AV11NaoInseridas ;
      private short AV7i ;
      private short AV12s ;
      private short AV23GXLvl72 ;
      private short AV25GXLvl95 ;
      private short nGXWrapped ;
      private int AV13Sistema_Codigo ;
      private int wcpOAV13Sistema_Codigo ;
      private int A190Tabela_SistemaCod ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int gxdynajaxindex ;
      private int AV10Modulo_Codigo ;
      private int tblTblresultado_Visible ;
      private int A127Sistema_Codigo ;
      private int tblTable2_Visible ;
      private int lblTextblock2_Visible ;
      private int idxLst ;
      private String AV17Entidade ;
      private String wcpOAV17Entidade ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A173Tabela_Nome ;
      private String AV19NomeAInserir ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String dynavModulo_codigo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavDados_Internalname ;
      private String edtavInseridas_Internalname ;
      private String edtavJacadastradas_Internalname ;
      private String edtavNaoinseridas_Internalname ;
      private String tblTblresultado_Internalname ;
      private String lblTbsistemanome_Caption ;
      private String lblTbsistemanome_Internalname ;
      private String tblTable2_Internalname ;
      private String lblTextblock1_Caption ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock2_Internalname ;
      private String AV5Char ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String lblTextblock2_Jsonclick ;
      private String TempTags ;
      private String dynavModulo_codigo_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String bttGravar_Internalname ;
      private String bttGravar_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavInseridas_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String edtavJacadastradas_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String edtavNaoinseridas_Jsonclick ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String lblTbsistemanome_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String AV24Udparg1 ;
      private String A416Sistema_Nome ;
      private String AV26Udparg2 ;
      private String AV6Dados ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP1_Entidade ;
      private GXCombobox dynavModulo_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H009X2_A146Modulo_Codigo ;
      private String[] H009X2_A143Modulo_Nome ;
      private bool[] H009X2_A130Sistema_Ativo ;
      private int[] H009X2_A127Sistema_Codigo ;
      private int[] H009X3_A127Sistema_Codigo ;
      private String[] H009X3_A416Sistema_Nome ;
      private int[] H009X4_A172Tabela_Codigo ;
      private int[] H009X4_A190Tabela_SistemaCod ;
      private String[] H009X4_A173Tabela_Nome ;
      private int[] H009X5_A127Sistema_Codigo ;
      private int[] H009X5_A135Sistema_AreaTrabalhoCod ;
      private String[] H009X5_A416Sistema_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV16WWPContext ;
      private SdtSistema AV18Sistema ;
      private SdtTabela AV14Tabela ;
   }

   public class wp_copiarcolar__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009X2 ;
          prmH009X2 = new Object[] {
          new Object[] {"@AV13Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009X3 ;
          prmH009X3 = new Object[] {
          new Object[] {"@AV13Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009X4 ;
          prmH009X4 = new Object[] {
          new Object[] {"@AV13Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009X5 ;
          prmH009X5 = new Object[] {
          new Object[] {"@AV16WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009X2", "SELECT T1.[Modulo_Codigo], T1.[Modulo_Nome], T2.[Sistema_Ativo], T1.[Sistema_Codigo] FROM ([Modulo] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Sistema_Codigo]) WHERE (T2.[Sistema_Ativo] = 1) AND (T1.[Sistema_Codigo] = @AV13Sistema_Codigo) ORDER BY T1.[Modulo_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009X2,0,0,true,false )
             ,new CursorDef("H009X3", "SELECT [Sistema_Codigo], [Sistema_Nome] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV13Sistema_Codigo ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009X3,1,0,false,true )
             ,new CursorDef("H009X4", "SELECT [Tabela_Codigo], [Tabela_SistemaCod], [Tabela_Nome] FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_SistemaCod] = @AV13Sistema_Codigo ORDER BY [Tabela_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009X4,100,0,true,false )
             ,new CursorDef("H009X5", "SELECT [Sistema_Codigo], [Sistema_AreaTrabalhoCod], [Sistema_Nome] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_AreaTrabalhoCod] = @AV16WWPC_1Areatrabalho_codigo ORDER BY [Sistema_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009X5,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
