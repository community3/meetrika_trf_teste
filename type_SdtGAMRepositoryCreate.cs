/*
               File: type_SdtGAMRepositoryCreate
        Description: GAMRepositoryCreate
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:52.81
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMRepositoryCreate : GxUserType, IGxExternalObject
   {
      public SdtGAMRepositoryCreate( )
      {
         initialize();
      }

      public SdtGAMRepositoryCreate( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMRepositoryCreate_externalReference == null )
         {
            GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
         }
         returntostring = "";
         returntostring = (String)(GAMRepositoryCreate_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Guid
      {
         get {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            return GAMRepositoryCreate_externalReference.GUID ;
         }

         set {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            GAMRepositoryCreate_externalReference.GUID = value;
         }

      }

      public String gxTpr_Namespace
      {
         get {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            return GAMRepositoryCreate_externalReference.NameSpace ;
         }

         set {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            GAMRepositoryCreate_externalReference.NameSpace = value;
         }

      }

      public bool gxTpr_Canregisterusers
      {
         get {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            return GAMRepositoryCreate_externalReference.CanRegisterUsers ;
         }

         set {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            GAMRepositoryCreate_externalReference.CanRegisterUsers = value;
         }

      }

      public bool gxTpr_Allowoauthaccess
      {
         get {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            return GAMRepositoryCreate_externalReference.AllowOauthAccess ;
         }

         set {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            GAMRepositoryCreate_externalReference.AllowOauthAccess = value;
         }

      }

      public bool gxTpr_Giveanonymoussession
      {
         get {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            return GAMRepositoryCreate_externalReference.GiveAnonymousSession ;
         }

         set {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            GAMRepositoryCreate_externalReference.GiveAnonymousSession = value;
         }

      }

      public String gxTpr_Name
      {
         get {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            return GAMRepositoryCreate_externalReference.Name ;
         }

         set {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            GAMRepositoryCreate_externalReference.Name = value;
         }

      }

      public String gxTpr_Description
      {
         get {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            return GAMRepositoryCreate_externalReference.Description ;
         }

         set {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            GAMRepositoryCreate_externalReference.Description = value;
         }

      }

      public String gxTpr_Administratorusername
      {
         get {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            return GAMRepositoryCreate_externalReference.AdministratorUserName ;
         }

         set {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            GAMRepositoryCreate_externalReference.AdministratorUserName = value;
         }

      }

      public String gxTpr_Administratoruserpassword
      {
         get {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            return GAMRepositoryCreate_externalReference.AdministratorUserPassword ;
         }

         set {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            GAMRepositoryCreate_externalReference.AdministratorUserPassword = value;
         }

      }

      public String gxTpr_Connectionusername
      {
         get {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            return GAMRepositoryCreate_externalReference.ConnectionUserName ;
         }

         set {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            GAMRepositoryCreate_externalReference.ConnectionUserName = value;
         }

      }

      public String gxTpr_Connectionuserpassword
      {
         get {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            return GAMRepositoryCreate_externalReference.ConnectionUserPassword ;
         }

         set {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            GAMRepositoryCreate_externalReference.ConnectionUserPassword = value;
         }

      }

      public bool gxTpr_Allowuseexistingnamespace
      {
         get {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            return GAMRepositoryCreate_externalReference.AllowUseExistingNamespace ;
         }

         set {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            GAMRepositoryCreate_externalReference.AllowUseExistingNamespace = value;
         }

      }

      public SdtGAMApplication gxTpr_Application
      {
         get {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            SdtGAMApplication intValue ;
            intValue = new SdtGAMApplication(context);
            Artech.Security.GAMApplication externalParm0 ;
            externalParm0 = GAMRepositoryCreate_externalReference.Application;
            intValue.ExternalInstance = externalParm0;
            return intValue ;
         }

         set {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            SdtGAMApplication intValue ;
            Artech.Security.GAMApplication externalParm1 ;
            intValue = value;
            externalParm1 = (Artech.Security.GAMApplication)(intValue.ExternalInstance);
            GAMRepositoryCreate_externalReference.Application = externalParm1;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMRepositoryCreate_externalReference == null )
            {
               GAMRepositoryCreate_externalReference = new Artech.Security.GAMRepositoryCreate(context);
            }
            return GAMRepositoryCreate_externalReference ;
         }

         set {
            GAMRepositoryCreate_externalReference = (Artech.Security.GAMRepositoryCreate)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMRepositoryCreate GAMRepositoryCreate_externalReference=null ;
   }

}
