/*
               File: MetodologiaFasesGeneral
        Description: Metodologia Fases General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:58:47.38
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class metodologiafasesgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public metodologiafasesgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public metodologiafasesgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_MetodologiaFases_Codigo )
      {
         this.A147MetodologiaFases_Codigo = aP0_MetodologiaFases_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A147MetodologiaFases_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A147MetodologiaFases_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A147MetodologiaFases_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A147MetodologiaFases_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA7Z2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "MetodologiaFasesGeneral";
               context.Gx_err = 0;
               WS7Z2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Metodologia Fases General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042822584743");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("metodologiafasesgeneral.aspx") + "?" + UrlEncode("" +A147MetodologiaFases_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA147MetodologiaFases_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA147MetodologiaFases_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"METODOLOGIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A137Metodologia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_METODOLOGIAFASES_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A148MetodologiaFases_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_METODOLOGIAFASES_PERCENTUAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( A149MetodologiaFases_Percentual, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_METODOLOGIAFASES_PRAZO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2135MetodologiaFases_Prazo, "ZZZ9.99")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm7Z2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("metodologiafasesgeneral.js", "?202042822584745");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "MetodologiaFasesGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Metodologia Fases General" ;
      }

      protected void WB7Z0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "metodologiafasesgeneral.aspx");
            }
            wb_table1_2_7Z2( true) ;
         }
         else
         {
            wb_table1_2_7Z2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_7Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMetodologiaFases_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A147MetodologiaFases_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A147MetodologiaFases_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMetodologiaFases_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtMetodologiaFases_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_MetodologiaFasesGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START7Z2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Metodologia Fases General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP7Z0( ) ;
            }
         }
      }

      protected void WS7Z2( )
      {
         START7Z2( ) ;
         EVT7Z2( ) ;
      }

      protected void EVT7Z2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7Z0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7Z0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E117Z2 */
                                    E117Z2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7Z0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E127Z2 */
                                    E127Z2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7Z0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E137Z2 */
                                    E137Z2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7Z0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E147Z2 */
                                    E147Z2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7Z0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7Z0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE7Z2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm7Z2( ) ;
            }
         }
      }

      protected void PA7Z2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF7Z2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "MetodologiaFasesGeneral";
         context.Gx_err = 0;
      }

      protected void RF7Z2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H007Z2 */
            pr_default.execute(0, new Object[] {A147MetodologiaFases_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A137Metodologia_Codigo = H007Z2_A137Metodologia_Codigo[0];
               A2135MetodologiaFases_Prazo = H007Z2_A2135MetodologiaFases_Prazo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2135MetodologiaFases_Prazo", StringUtil.LTrim( StringUtil.Str( A2135MetodologiaFases_Prazo, 7, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_METODOLOGIAFASES_PRAZO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2135MetodologiaFases_Prazo, "ZZZ9.99")));
               n2135MetodologiaFases_Prazo = H007Z2_n2135MetodologiaFases_Prazo[0];
               A149MetodologiaFases_Percentual = H007Z2_A149MetodologiaFases_Percentual[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A149MetodologiaFases_Percentual", StringUtil.LTrim( StringUtil.Str( A149MetodologiaFases_Percentual, 6, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_METODOLOGIAFASES_PERCENTUAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( A149MetodologiaFases_Percentual, "ZZ9.99")));
               A148MetodologiaFases_Nome = H007Z2_A148MetodologiaFases_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A148MetodologiaFases_Nome", A148MetodologiaFases_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_METODOLOGIAFASES_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A148MetodologiaFases_Nome, "@!"))));
               A138Metodologia_Descricao = H007Z2_A138Metodologia_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A138Metodologia_Descricao", A138Metodologia_Descricao);
               A138Metodologia_Descricao = H007Z2_A138Metodologia_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A138Metodologia_Descricao", A138Metodologia_Descricao);
               /* Execute user event: E127Z2 */
               E127Z2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB7Z0( ) ;
         }
      }

      protected void STRUP7Z0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "MetodologiaFasesGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E117Z2 */
         E117Z2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A138Metodologia_Descricao = StringUtil.Upper( cgiGet( edtMetodologia_Descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A138Metodologia_Descricao", A138Metodologia_Descricao);
            A148MetodologiaFases_Nome = StringUtil.Upper( cgiGet( edtMetodologiaFases_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A148MetodologiaFases_Nome", A148MetodologiaFases_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_METODOLOGIAFASES_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A148MetodologiaFases_Nome, "@!"))));
            A149MetodologiaFases_Percentual = context.localUtil.CToN( cgiGet( edtMetodologiaFases_Percentual_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A149MetodologiaFases_Percentual", StringUtil.LTrim( StringUtil.Str( A149MetodologiaFases_Percentual, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_METODOLOGIAFASES_PERCENTUAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( A149MetodologiaFases_Percentual, "ZZ9.99")));
            A2135MetodologiaFases_Prazo = context.localUtil.CToN( cgiGet( edtMetodologiaFases_Prazo_Internalname), ",", ".");
            n2135MetodologiaFases_Prazo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2135MetodologiaFases_Prazo", StringUtil.LTrim( StringUtil.Str( A2135MetodologiaFases_Prazo, 7, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_METODOLOGIAFASES_PRAZO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2135MetodologiaFases_Prazo, "ZZZ9.99")));
            /* Read saved values. */
            wcpOA147MetodologiaFases_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA147MetodologiaFases_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E117Z2 */
         E117Z2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E117Z2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E127Z2( )
      {
         /* Load Routine */
         edtMetodologia_Descricao_Link = formatLink("viewmetodologia.aspx") + "?" + UrlEncode("" +A137Metodologia_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtMetodologia_Descricao_Internalname, "Link", edtMetodologia_Descricao_Link);
         edtMetodologiaFases_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtMetodologiaFases_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMetodologiaFases_Codigo_Visible), 5, 0)));
      }

      protected void E137Z2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("metodologiafases.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A147MetodologiaFases_Codigo) + "," + UrlEncode("" +A137Metodologia_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E147Z2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("metodologiafases.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A147MetodologiaFases_Codigo) + "," + UrlEncode("" +A137Metodologia_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "MetodologiaFases";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "MetodologiaFases_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7MetodologiaFases_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_7Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_7Z2( true) ;
         }
         else
         {
            wb_table2_8_7Z2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_7Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_31_7Z2( true) ;
         }
         else
         {
            wb_table3_31_7Z2( false) ;
         }
         return  ;
      }

      protected void wb_table3_31_7Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_7Z2e( true) ;
         }
         else
         {
            wb_table1_2_7Z2e( false) ;
         }
      }

      protected void wb_table3_31_7Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_MetodologiaFasesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_MetodologiaFasesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_31_7Z2e( true) ;
         }
         else
         {
            wb_table3_31_7Z2e( false) ;
         }
      }

      protected void wb_table2_8_7Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmetodologia_descricao_Internalname, "Metodología", "", "", lblTextblockmetodologia_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_MetodologiaFasesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMetodologia_Descricao_Internalname, A138Metodologia_Descricao, StringUtil.RTrim( context.localUtil.Format( A138Metodologia_Descricao, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtMetodologia_Descricao_Link, "", "", "", edtMetodologia_Descricao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_MetodologiaFasesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmetodologiafases_nome_Internalname, "Nome", "", "", lblTextblockmetodologiafases_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_MetodologiaFasesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMetodologiaFases_Nome_Internalname, StringUtil.RTrim( A148MetodologiaFases_Nome), StringUtil.RTrim( context.localUtil.Format( A148MetodologiaFases_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMetodologiaFases_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_MetodologiaFasesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmetodologiafases_percentual_Internalname, "Percentual", "", "", lblTextblockmetodologiafases_percentual_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_MetodologiaFasesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMetodologiaFases_Percentual_Internalname, StringUtil.LTrim( StringUtil.NToC( A149MetodologiaFases_Percentual, 6, 2, ",", "")), context.localUtil.Format( A149MetodologiaFases_Percentual, "ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMetodologiaFases_Percentual_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_MetodologiaFasesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmetodologiafases_prazo_Internalname, "Prazo", "", "", lblTextblockmetodologiafases_prazo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_MetodologiaFasesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMetodologiaFases_Prazo_Internalname, StringUtil.LTrim( StringUtil.NToC( A2135MetodologiaFases_Prazo, 7, 2, ",", "")), context.localUtil.Format( A2135MetodologiaFases_Prazo, "ZZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMetodologiaFases_Prazo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 7, "chr", 1, "row", 7, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_MetodologiaFasesGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_7Z2e( true) ;
         }
         else
         {
            wb_table2_8_7Z2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A147MetodologiaFases_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A147MetodologiaFases_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A147MetodologiaFases_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA7Z2( ) ;
         WS7Z2( ) ;
         WE7Z2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA147MetodologiaFases_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA7Z2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "metodologiafasesgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA7Z2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A147MetodologiaFases_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A147MetodologiaFases_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A147MetodologiaFases_Codigo), 6, 0)));
         }
         wcpOA147MetodologiaFases_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA147MetodologiaFases_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A147MetodologiaFases_Codigo != wcpOA147MetodologiaFases_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA147MetodologiaFases_Codigo = A147MetodologiaFases_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA147MetodologiaFases_Codigo = cgiGet( sPrefix+"A147MetodologiaFases_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA147MetodologiaFases_Codigo) > 0 )
         {
            A147MetodologiaFases_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA147MetodologiaFases_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A147MetodologiaFases_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A147MetodologiaFases_Codigo), 6, 0)));
         }
         else
         {
            A147MetodologiaFases_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A147MetodologiaFases_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA7Z2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS7Z2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS7Z2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A147MetodologiaFases_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A147MetodologiaFases_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA147MetodologiaFases_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A147MetodologiaFases_Codigo_CTRL", StringUtil.RTrim( sCtrlA147MetodologiaFases_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE7Z2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042822584775");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("metodologiafasesgeneral.js", "?202042822584775");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockmetodologia_descricao_Internalname = sPrefix+"TEXTBLOCKMETODOLOGIA_DESCRICAO";
         edtMetodologia_Descricao_Internalname = sPrefix+"METODOLOGIA_DESCRICAO";
         lblTextblockmetodologiafases_nome_Internalname = sPrefix+"TEXTBLOCKMETODOLOGIAFASES_NOME";
         edtMetodologiaFases_Nome_Internalname = sPrefix+"METODOLOGIAFASES_NOME";
         lblTextblockmetodologiafases_percentual_Internalname = sPrefix+"TEXTBLOCKMETODOLOGIAFASES_PERCENTUAL";
         edtMetodologiaFases_Percentual_Internalname = sPrefix+"METODOLOGIAFASES_PERCENTUAL";
         lblTextblockmetodologiafases_prazo_Internalname = sPrefix+"TEXTBLOCKMETODOLOGIAFASES_PRAZO";
         edtMetodologiaFases_Prazo_Internalname = sPrefix+"METODOLOGIAFASES_PRAZO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtMetodologiaFases_Codigo_Internalname = sPrefix+"METODOLOGIAFASES_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtMetodologiaFases_Prazo_Jsonclick = "";
         edtMetodologiaFases_Percentual_Jsonclick = "";
         edtMetodologiaFases_Nome_Jsonclick = "";
         edtMetodologia_Descricao_Jsonclick = "";
         edtMetodologia_Descricao_Link = "";
         edtMetodologiaFases_Codigo_Jsonclick = "";
         edtMetodologiaFases_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E137Z2',iparms:[{av:'A147MetodologiaFases_Codigo',fld:'METODOLOGIAFASES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E147Z2',iparms:[{av:'A147MetodologiaFases_Codigo',fld:'METODOLOGIAFASES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A148MetodologiaFases_Nome = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H007Z2_A147MetodologiaFases_Codigo = new int[1] ;
         H007Z2_A137Metodologia_Codigo = new int[1] ;
         H007Z2_A2135MetodologiaFases_Prazo = new decimal[1] ;
         H007Z2_n2135MetodologiaFases_Prazo = new bool[] {false} ;
         H007Z2_A149MetodologiaFases_Percentual = new decimal[1] ;
         H007Z2_A148MetodologiaFases_Nome = new String[] {""} ;
         H007Z2_A138Metodologia_Descricao = new String[] {""} ;
         A138Metodologia_Descricao = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockmetodologia_descricao_Jsonclick = "";
         lblTextblockmetodologiafases_nome_Jsonclick = "";
         lblTextblockmetodologiafases_percentual_Jsonclick = "";
         lblTextblockmetodologiafases_prazo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA147MetodologiaFases_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.metodologiafasesgeneral__default(),
            new Object[][] {
                new Object[] {
               H007Z2_A147MetodologiaFases_Codigo, H007Z2_A137Metodologia_Codigo, H007Z2_A2135MetodologiaFases_Prazo, H007Z2_n2135MetodologiaFases_Prazo, H007Z2_A149MetodologiaFases_Percentual, H007Z2_A148MetodologiaFases_Nome, H007Z2_A138Metodologia_Descricao
               }
            }
         );
         AV14Pgmname = "MetodologiaFasesGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "MetodologiaFasesGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A147MetodologiaFases_Codigo ;
      private int wcpOA147MetodologiaFases_Codigo ;
      private int A137Metodologia_Codigo ;
      private int edtMetodologiaFases_Codigo_Visible ;
      private int AV7MetodologiaFases_Codigo ;
      private int idxLst ;
      private decimal A149MetodologiaFases_Percentual ;
      private decimal A2135MetodologiaFases_Prazo ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A148MetodologiaFases_Nome ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtMetodologiaFases_Codigo_Internalname ;
      private String edtMetodologiaFases_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtMetodologia_Descricao_Internalname ;
      private String edtMetodologiaFases_Nome_Internalname ;
      private String edtMetodologiaFases_Percentual_Internalname ;
      private String edtMetodologiaFases_Prazo_Internalname ;
      private String edtMetodologia_Descricao_Link ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockmetodologia_descricao_Internalname ;
      private String lblTextblockmetodologia_descricao_Jsonclick ;
      private String edtMetodologia_Descricao_Jsonclick ;
      private String lblTextblockmetodologiafases_nome_Internalname ;
      private String lblTextblockmetodologiafases_nome_Jsonclick ;
      private String edtMetodologiaFases_Nome_Jsonclick ;
      private String lblTextblockmetodologiafases_percentual_Internalname ;
      private String lblTextblockmetodologiafases_percentual_Jsonclick ;
      private String edtMetodologiaFases_Percentual_Jsonclick ;
      private String lblTextblockmetodologiafases_prazo_Internalname ;
      private String lblTextblockmetodologiafases_prazo_Jsonclick ;
      private String edtMetodologiaFases_Prazo_Jsonclick ;
      private String sCtrlA147MetodologiaFases_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2135MetodologiaFases_Prazo ;
      private bool returnInSub ;
      private String A138Metodologia_Descricao ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H007Z2_A147MetodologiaFases_Codigo ;
      private int[] H007Z2_A137Metodologia_Codigo ;
      private decimal[] H007Z2_A2135MetodologiaFases_Prazo ;
      private bool[] H007Z2_n2135MetodologiaFases_Prazo ;
      private decimal[] H007Z2_A149MetodologiaFases_Percentual ;
      private String[] H007Z2_A148MetodologiaFases_Nome ;
      private String[] H007Z2_A138Metodologia_Descricao ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class metodologiafasesgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH007Z2 ;
          prmH007Z2 = new Object[] {
          new Object[] {"@MetodologiaFases_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H007Z2", "SELECT T1.[MetodologiaFases_Codigo], T1.[Metodologia_Codigo], T1.[MetodologiaFases_Prazo], T1.[MetodologiaFases_Percentual], T1.[MetodologiaFases_Nome], T2.[Metodologia_Descricao] FROM ([MetodologiaFases] T1 WITH (NOLOCK) INNER JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo]) WHERE T1.[MetodologiaFases_Codigo] = @MetodologiaFases_Codigo ORDER BY T1.[MetodologiaFases_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007Z2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
