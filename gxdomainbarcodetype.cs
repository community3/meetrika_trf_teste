/*
               File: BarcodeType
        Description: BarcodeType
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 21:56:34.69
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomainbarcodetype
   {
      private static Hashtable domain = new Hashtable();
      static gxdomainbarcodetype ()
      {
         domain["UPC-E"] = "UPC-E Code";
         domain["Code39"] = "Code 39 Code";
         domain["Code39Mod43"] = "Code 39 mod 43 Code";
         domain["EAN-13"] = "EAN-13 Code";
         domain["EAN-8"] = "EAN-8 Code";
         domain["Code93"] = "Code 93 Code";
         domain["Code128"] = "Code 128 Code";
         domain["Interleaved2of5"] = "Interleaved 2 of 5 Code";
         domain["ITF-14"] = "ITF-14 Code";
         domain["PDF417"] = "PDF417 Code";
         domain["QR"] = "QR Code";
         domain["Aztec"] = "Aztec Code";
         domain["DataMatrix"] = "Data Matrix Code";
      }

      public static string getDescription( IGxContext context ,
                                           String key )
      {
         string rtkey ;
         rtkey = StringUtil.Trim( (String)(key));
         return (string)domain[rtkey] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (String key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
