/*
               File: ProjetoDesenvolvimentoGeneral
        Description: Projeto Desenvolvimento General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:2:59.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class projetodesenvolvimentogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public projetodesenvolvimentogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public projetodesenvolvimentogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ProjetoDesenvolvimento_ProjetoCod ,
                           int aP1_ProjetoDesenvolvimento_SistemaCod )
      {
         this.A669ProjetoDesenvolvimento_ProjetoCod = aP0_ProjetoDesenvolvimento_ProjetoCod;
         this.A670ProjetoDesenvolvimento_SistemaCod = aP1_ProjetoDesenvolvimento_SistemaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A669ProjetoDesenvolvimento_ProjetoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A669ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0)));
                  A670ProjetoDesenvolvimento_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A669ProjetoDesenvolvimento_ProjetoCod,(int)A670ProjetoDesenvolvimento_SistemaCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PADN2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV18Pgmname = "ProjetoDesenvolvimentoGeneral";
               context.Gx_err = 0;
               WSDN2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Projeto Desenvolvimento General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282325910");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("projetodesenvolvimentogeneral.aspx") + "?" + UrlEncode("" +A669ProjetoDesenvolvimento_ProjetoCod) + "," + UrlEncode("" +A670ProjetoDesenvolvimento_SistemaCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA669ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA669ProjetoDesenvolvimento_ProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA670ProjetoDesenvolvimento_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSISTEMA_TIPO", StringUtil.RTrim( AV13Sistema_Tipo));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormDN2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("projetodesenvolvimentogeneral.js", "?20204282325912");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ProjetoDesenvolvimentoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Projeto Desenvolvimento General" ;
      }

      protected void WBDN0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "projetodesenvolvimentogeneral.aspx");
            }
            wb_table1_2_DN2( true) ;
         }
         else
         {
            wb_table1_2_DN2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DN2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTDN2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Projeto Desenvolvimento General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPDN0( ) ;
            }
         }
      }

      protected void WSDN2( )
      {
         STARTDN2( ) ;
         EVTDN2( ) ;
      }

      protected void EVTDN2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11DN2 */
                                    E11DN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12DN2 */
                                    E12DN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13DN2 */
                                    E13DN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14DN2 */
                                    E14DN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEDN2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormDN2( ) ;
            }
         }
      }

      protected void PADN2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDN2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV18Pgmname = "ProjetoDesenvolvimentoGeneral";
         context.Gx_err = 0;
      }

      protected void RFDN2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00DN2 */
            pr_default.execute(0, new Object[] {A669ProjetoDesenvolvimento_ProjetoCod, A670ProjetoDesenvolvimento_SistemaCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               /* Execute user event: E12DN2 */
               E12DN2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBDN0( ) ;
         }
      }

      protected void STRUPDN0( )
      {
         /* Before Start, stand alone formulas. */
         AV18Pgmname = "ProjetoDesenvolvimentoGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11DN2 */
         E11DN2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            wcpOA669ProjetoDesenvolvimento_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA669ProjetoDesenvolvimento_ProjetoCod"), ",", "."));
            wcpOA670ProjetoDesenvolvimento_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA670ProjetoDesenvolvimento_SistemaCod"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11DN2 */
         E11DN2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11DN2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12DN2( )
      {
         /* Load Routine */
      }

      protected void E13DN2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("projetodesenvolvimento.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A669ProjetoDesenvolvimento_ProjetoCod) + "," + UrlEncode("" +A670ProjetoDesenvolvimento_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(AV13Sistema_Tipo));
         context.wjLocDisableFrm = 1;
      }

      protected void E14DN2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("projetodesenvolvimento.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A669ProjetoDesenvolvimento_ProjetoCod) + "," + UrlEncode("" +A670ProjetoDesenvolvimento_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(AV13Sistema_Tipo));
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV18Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = false;
         AV9TrnContext.gxTpr_Callerurl = AV12HTTPRequest.ScriptName+"?"+AV12HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ProjetoDesenvolvimento";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ProjetoDesenvolvimento_ProjetoCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV14ProjetoDesenvolvimento_ProjetoCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ProjetoDesenvolvimento_SistemaCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV15ProjetoDesenvolvimento_SistemaCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV11Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_DN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_DN2( true) ;
         }
         else
         {
            wb_table2_8_DN2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_DN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_21_DN2( true) ;
         }
         else
         {
            wb_table3_21_DN2( false) ;
         }
         return  ;
      }

      protected void wb_table3_21_DN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DN2e( true) ;
         }
         else
         {
            wb_table1_2_DN2e( false) ;
         }
      }

      protected void wb_table3_21_DN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ProjetoDesenvolvimentoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ProjetoDesenvolvimentoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_21_DN2e( true) ;
         }
         else
         {
            wb_table3_21_DN2e( false) ;
         }
      }

      protected void wb_table2_8_DN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojetodesenvolvimento_projetocod_Internalname, "Projeto", "", "", lblTextblockprojetodesenvolvimento_projetocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoDesenvolvimentoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjetoDesenvolvimento_ProjetoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjetoDesenvolvimento_ProjetoCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ProjetoDesenvolvimentoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojetodesenvolvimento_sistemacod_Internalname, "Sistema", "", "", lblTextblockprojetodesenvolvimento_sistemacod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoDesenvolvimentoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjetoDesenvolvimento_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjetoDesenvolvimento_SistemaCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ProjetoDesenvolvimentoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_DN2e( true) ;
         }
         else
         {
            wb_table2_8_DN2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A669ProjetoDesenvolvimento_ProjetoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A669ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0)));
         A670ProjetoDesenvolvimento_SistemaCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADN2( ) ;
         WSDN2( ) ;
         WEDN2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA669ProjetoDesenvolvimento_ProjetoCod = (String)((String)getParm(obj,0));
         sCtrlA670ProjetoDesenvolvimento_SistemaCod = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PADN2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "projetodesenvolvimentogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PADN2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A669ProjetoDesenvolvimento_ProjetoCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A669ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0)));
            A670ProjetoDesenvolvimento_SistemaCod = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
         }
         wcpOA669ProjetoDesenvolvimento_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA669ProjetoDesenvolvimento_ProjetoCod"), ",", "."));
         wcpOA670ProjetoDesenvolvimento_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA670ProjetoDesenvolvimento_SistemaCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A669ProjetoDesenvolvimento_ProjetoCod != wcpOA669ProjetoDesenvolvimento_ProjetoCod ) || ( A670ProjetoDesenvolvimento_SistemaCod != wcpOA670ProjetoDesenvolvimento_SistemaCod ) ) )
         {
            setjustcreated();
         }
         wcpOA669ProjetoDesenvolvimento_ProjetoCod = A669ProjetoDesenvolvimento_ProjetoCod;
         wcpOA670ProjetoDesenvolvimento_SistemaCod = A670ProjetoDesenvolvimento_SistemaCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA669ProjetoDesenvolvimento_ProjetoCod = cgiGet( sPrefix+"A669ProjetoDesenvolvimento_ProjetoCod_CTRL");
         if ( StringUtil.Len( sCtrlA669ProjetoDesenvolvimento_ProjetoCod) > 0 )
         {
            A669ProjetoDesenvolvimento_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA669ProjetoDesenvolvimento_ProjetoCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A669ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0)));
         }
         else
         {
            A669ProjetoDesenvolvimento_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A669ProjetoDesenvolvimento_ProjetoCod_PARM"), ",", "."));
         }
         sCtrlA670ProjetoDesenvolvimento_SistemaCod = cgiGet( sPrefix+"A670ProjetoDesenvolvimento_SistemaCod_CTRL");
         if ( StringUtil.Len( sCtrlA670ProjetoDesenvolvimento_SistemaCod) > 0 )
         {
            A670ProjetoDesenvolvimento_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA670ProjetoDesenvolvimento_SistemaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A670ProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0)));
         }
         else
         {
            A670ProjetoDesenvolvimento_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A670ProjetoDesenvolvimento_SistemaCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PADN2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSDN2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSDN2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A669ProjetoDesenvolvimento_ProjetoCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA669ProjetoDesenvolvimento_ProjetoCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A669ProjetoDesenvolvimento_ProjetoCod_CTRL", StringUtil.RTrim( sCtrlA669ProjetoDesenvolvimento_ProjetoCod));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A670ProjetoDesenvolvimento_SistemaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA670ProjetoDesenvolvimento_SistemaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A670ProjetoDesenvolvimento_SistemaCod_CTRL", StringUtil.RTrim( sCtrlA670ProjetoDesenvolvimento_SistemaCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEDN2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282325935");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("projetodesenvolvimentogeneral.js", "?20204282325935");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockprojetodesenvolvimento_projetocod_Internalname = sPrefix+"TEXTBLOCKPROJETODESENVOLVIMENTO_PROJETOCOD";
         edtProjetoDesenvolvimento_ProjetoCod_Internalname = sPrefix+"PROJETODESENVOLVIMENTO_PROJETOCOD";
         lblTextblockprojetodesenvolvimento_sistemacod_Internalname = sPrefix+"TEXTBLOCKPROJETODESENVOLVIMENTO_SISTEMACOD";
         edtProjetoDesenvolvimento_SistemaCod_Internalname = sPrefix+"PROJETODESENVOLVIMENTO_SISTEMACOD";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtProjetoDesenvolvimento_SistemaCod_Jsonclick = "";
         edtProjetoDesenvolvimento_ProjetoCod_Jsonclick = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13DN2',iparms:[{av:'A669ProjetoDesenvolvimento_ProjetoCod',fld:'PROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A670ProjetoDesenvolvimento_SistemaCod',fld:'PROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV13Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''}],oparms:[{av:'AV13Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''}]}");
         setEventMetadata("'DODELETE'","{handler:'E14DN2',iparms:[{av:'A669ProjetoDesenvolvimento_ProjetoCod',fld:'PROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A670ProjetoDesenvolvimento_SistemaCod',fld:'PROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV13Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''}],oparms:[{av:'AV13Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV18Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV13Sistema_Tipo = "A";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00DN2_A669ProjetoDesenvolvimento_ProjetoCod = new int[1] ;
         H00DN2_A670ProjetoDesenvolvimento_SistemaCod = new int[1] ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV12HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV11Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockprojetodesenvolvimento_projetocod_Jsonclick = "";
         lblTextblockprojetodesenvolvimento_sistemacod_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA669ProjetoDesenvolvimento_ProjetoCod = "";
         sCtrlA670ProjetoDesenvolvimento_SistemaCod = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.projetodesenvolvimentogeneral__default(),
            new Object[][] {
                new Object[] {
               H00DN2_A669ProjetoDesenvolvimento_ProjetoCod, H00DN2_A670ProjetoDesenvolvimento_SistemaCod
               }
            }
         );
         AV18Pgmname = "ProjetoDesenvolvimentoGeneral";
         /* GeneXus formulas. */
         AV18Pgmname = "ProjetoDesenvolvimentoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A669ProjetoDesenvolvimento_ProjetoCod ;
      private int A670ProjetoDesenvolvimento_SistemaCod ;
      private int wcpOA669ProjetoDesenvolvimento_ProjetoCod ;
      private int wcpOA670ProjetoDesenvolvimento_SistemaCod ;
      private int AV14ProjetoDesenvolvimento_ProjetoCod ;
      private int AV15ProjetoDesenvolvimento_SistemaCod ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV18Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV13Sistema_Tipo ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockprojetodesenvolvimento_projetocod_Internalname ;
      private String lblTextblockprojetodesenvolvimento_projetocod_Jsonclick ;
      private String edtProjetoDesenvolvimento_ProjetoCod_Internalname ;
      private String edtProjetoDesenvolvimento_ProjetoCod_Jsonclick ;
      private String lblTextblockprojetodesenvolvimento_sistemacod_Internalname ;
      private String lblTextblockprojetodesenvolvimento_sistemacod_Jsonclick ;
      private String edtProjetoDesenvolvimento_SistemaCod_Internalname ;
      private String edtProjetoDesenvolvimento_SistemaCod_Jsonclick ;
      private String sCtrlA669ProjetoDesenvolvimento_ProjetoCod ;
      private String sCtrlA670ProjetoDesenvolvimento_SistemaCod ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00DN2_A669ProjetoDesenvolvimento_ProjetoCod ;
      private int[] H00DN2_A670ProjetoDesenvolvimento_SistemaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV12HTTPRequest ;
      private IGxSession AV11Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
   }

   public class projetodesenvolvimentogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DN2 ;
          prmH00DN2 = new Object[] {
          new Object[] {"@ProjetoDesenvolvimento_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoDesenvolvimento_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DN2", "SELECT [ProjetoDesenvolvimento_ProjetoCod], [ProjetoDesenvolvimento_SistemaCod] FROM [ProjetoDesenvolvimento] WITH (NOLOCK) WHERE [ProjetoDesenvolvimento_ProjetoCod] = @ProjetoDesenvolvimento_ProjetoCod and [ProjetoDesenvolvimento_SistemaCod] = @ProjetoDesenvolvimento_SistemaCod ORDER BY [ProjetoDesenvolvimento_ProjetoCod], [ProjetoDesenvolvimento_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DN2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
