/*
               File: type_SdtGxMap
        Description: GxMap
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:56.81
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "GxMap" )]
   [XmlType(TypeName =  "GxMap" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtGxMap_Point ))]
   [Serializable]
   public class SdtGxMap : GxUserType
   {
      public SdtGxMap( )
      {
         /* Constructor for serialization */
      }

      public SdtGxMap( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtGxMap deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtGxMap)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtGxMap obj ;
         obj = this;
         obj.gxTpr_Points = deserialized.gxTpr_Points;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Points") )
               {
                  if ( gxTv_SdtGxMap_Points == null )
                  {
                     gxTv_SdtGxMap_Points = new GxObjectCollection( context, "GxMap.Point", "GxEv3Up14_MeetrikaVs3", "SdtGxMap_Point", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtGxMap_Points.readxmlcollection(oReader, "Points", "Point");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "GxMap";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         if ( gxTv_SdtGxMap_Points != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtGxMap_Points.writexmlcollection(oWriter, "Points", sNameSpace1, "Point", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         if ( gxTv_SdtGxMap_Points != null )
         {
            AddObjectProperty("Points", gxTv_SdtGxMap_Points, false);
         }
         return  ;
      }

      public class gxTv_SdtGxMap_Points_SdtGxMap_Point_80compatibility:SdtGxMap_Point {}
      [  SoapElement( ElementName = "Points" )]
      [  XmlArray( ElementName = "Points"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtGxMap_Point ), ElementName= "Point"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtGxMap_Points_SdtGxMap_Point_80compatibility ), ElementName= "GxMap.Point"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Points_GxObjectCollection
      {
         get {
            if ( gxTv_SdtGxMap_Points == null )
            {
               gxTv_SdtGxMap_Points = new GxObjectCollection( context, "GxMap.Point", "GxEv3Up14_MeetrikaVs3", "SdtGxMap_Point", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtGxMap_Points ;
         }

         set {
            if ( gxTv_SdtGxMap_Points == null )
            {
               gxTv_SdtGxMap_Points = new GxObjectCollection( context, "GxMap.Point", "GxEv3Up14_MeetrikaVs3", "SdtGxMap_Point", "GeneXus.Programs");
            }
            gxTv_SdtGxMap_Points = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Points
      {
         get {
            if ( gxTv_SdtGxMap_Points == null )
            {
               gxTv_SdtGxMap_Points = new GxObjectCollection( context, "GxMap.Point", "GxEv3Up14_MeetrikaVs3", "SdtGxMap_Point", "GeneXus.Programs");
            }
            return gxTv_SdtGxMap_Points ;
         }

         set {
            gxTv_SdtGxMap_Points = value;
         }

      }

      public void gxTv_SdtGxMap_Points_SetNull( )
      {
         gxTv_SdtGxMap_Points = null;
         return  ;
      }

      public bool gxTv_SdtGxMap_Points_IsNull( )
      {
         if ( gxTv_SdtGxMap_Points == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( SdtGxMap_Point ))]
      protected IGxCollection gxTv_SdtGxMap_Points=null ;
   }

   [DataContract(Name = @"GxMap", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtGxMap_RESTInterface : GxGenericCollectionItem<SdtGxMap>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtGxMap_RESTInterface( ) : base()
      {
      }

      public SdtGxMap_RESTInterface( SdtGxMap psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Points" , Order = 0 )]
      public GxGenericCollection<SdtGxMap_Point_RESTInterface> gxTpr_Points
      {
         get {
            return new GxGenericCollection<SdtGxMap_Point_RESTInterface>(sdt.gxTpr_Points) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Points);
         }

      }

      public SdtGxMap sdt
      {
         get {
            return (SdtGxMap)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtGxMap() ;
         }
      }

   }

}
