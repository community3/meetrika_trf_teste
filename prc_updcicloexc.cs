/*
               File: PRC_UpdCicloExc
        Description: Update Ciclo Execu��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:14:16.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updcicloexc : GXProcedure
   {
      public prc_updcicloexc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updcicloexc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           DateTime aP1_Prazo )
      {
         this.AV8Codigo = aP0_Codigo;
         this.AV9Prazo = aP1_Prazo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Codigo ,
                                 DateTime aP1_Prazo )
      {
         prc_updcicloexc objprc_updcicloexc;
         objprc_updcicloexc = new prc_updcicloexc();
         objprc_updcicloexc.AV8Codigo = aP0_Codigo;
         objprc_updcicloexc.AV9Prazo = aP1_Prazo;
         objprc_updcicloexc.context.SetSubmitInitialConfig(context);
         objprc_updcicloexc.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updcicloexc);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updcicloexc)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00VT2 */
         pr_default.execute(0, new Object[] {AV8Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00VT2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00VT2_n1553ContagemResultado_CntSrvCod[0];
            A1407ContagemResultadoExecucao_Fim = P00VT2_A1407ContagemResultadoExecucao_Fim[0];
            n1407ContagemResultadoExecucao_Fim = P00VT2_n1407ContagemResultadoExecucao_Fim[0];
            A1404ContagemResultadoExecucao_OSCod = P00VT2_A1404ContagemResultadoExecucao_OSCod[0];
            A1408ContagemResultadoExecucao_Prevista = P00VT2_A1408ContagemResultadoExecucao_Prevista[0];
            n1408ContagemResultadoExecucao_Prevista = P00VT2_n1408ContagemResultadoExecucao_Prevista[0];
            A1611ContagemResultado_PrzTpDias = P00VT2_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P00VT2_n1611ContagemResultado_PrzTpDias[0];
            A1406ContagemResultadoExecucao_Inicio = P00VT2_A1406ContagemResultadoExecucao_Inicio[0];
            A1411ContagemResultadoExecucao_PrazoDias = P00VT2_A1411ContagemResultadoExecucao_PrazoDias[0];
            n1411ContagemResultadoExecucao_PrazoDias = P00VT2_n1411ContagemResultadoExecucao_PrazoDias[0];
            A1405ContagemResultadoExecucao_Codigo = P00VT2_A1405ContagemResultadoExecucao_Codigo[0];
            A1553ContagemResultado_CntSrvCod = P00VT2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00VT2_n1553ContagemResultado_CntSrvCod[0];
            A1611ContagemResultado_PrzTpDias = P00VT2_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P00VT2_n1611ContagemResultado_PrzTpDias[0];
            A1408ContagemResultadoExecucao_Prevista = AV9Prazo;
            n1408ContagemResultadoExecucao_Prevista = false;
            GXt_int1 = A1411ContagemResultadoExecucao_PrazoDias;
            new prc_diasuteisentre(context ).execute(  DateTimeUtil.ResetTime( A1406ContagemResultadoExecucao_Inicio),  DateTimeUtil.ResetTime( AV9Prazo),  A1611ContagemResultado_PrzTpDias, out  GXt_int1) ;
            A1411ContagemResultadoExecucao_PrazoDias = GXt_int1;
            n1411ContagemResultadoExecucao_PrazoDias = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00VT3 */
            pr_default.execute(1, new Object[] {n1408ContagemResultadoExecucao_Prevista, A1408ContagemResultadoExecucao_Prevista, n1411ContagemResultadoExecucao_PrazoDias, A1411ContagemResultadoExecucao_PrazoDias, A1405ContagemResultadoExecucao_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
            if (true) break;
            /* Using cursor P00VT4 */
            pr_default.execute(2, new Object[] {n1408ContagemResultadoExecucao_Prevista, A1408ContagemResultadoExecucao_Prevista, n1411ContagemResultadoExecucao_PrazoDias, A1411ContagemResultadoExecucao_PrazoDias, A1405ContagemResultadoExecucao_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00VT2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00VT2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00VT2_A1407ContagemResultadoExecucao_Fim = new DateTime[] {DateTime.MinValue} ;
         P00VT2_n1407ContagemResultadoExecucao_Fim = new bool[] {false} ;
         P00VT2_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         P00VT2_A1408ContagemResultadoExecucao_Prevista = new DateTime[] {DateTime.MinValue} ;
         P00VT2_n1408ContagemResultadoExecucao_Prevista = new bool[] {false} ;
         P00VT2_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P00VT2_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P00VT2_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         P00VT2_A1411ContagemResultadoExecucao_PrazoDias = new short[1] ;
         P00VT2_n1411ContagemResultadoExecucao_PrazoDias = new bool[] {false} ;
         P00VT2_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         A1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         A1408ContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         A1611ContagemResultado_PrzTpDias = "";
         A1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updcicloexc__default(),
            new Object[][] {
                new Object[] {
               P00VT2_A1553ContagemResultado_CntSrvCod, P00VT2_n1553ContagemResultado_CntSrvCod, P00VT2_A1407ContagemResultadoExecucao_Fim, P00VT2_n1407ContagemResultadoExecucao_Fim, P00VT2_A1404ContagemResultadoExecucao_OSCod, P00VT2_A1408ContagemResultadoExecucao_Prevista, P00VT2_n1408ContagemResultadoExecucao_Prevista, P00VT2_A1611ContagemResultado_PrzTpDias, P00VT2_n1611ContagemResultado_PrzTpDias, P00VT2_A1406ContagemResultadoExecucao_Inicio,
               P00VT2_A1411ContagemResultadoExecucao_PrazoDias, P00VT2_n1411ContagemResultadoExecucao_PrazoDias, P00VT2_A1405ContagemResultadoExecucao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1411ContagemResultadoExecucao_PrazoDias ;
      private short GXt_int1 ;
      private int AV8Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1404ContagemResultadoExecucao_OSCod ;
      private int A1405ContagemResultadoExecucao_Codigo ;
      private String scmdbuf ;
      private String A1611ContagemResultado_PrzTpDias ;
      private DateTime AV9Prazo ;
      private DateTime A1407ContagemResultadoExecucao_Fim ;
      private DateTime A1408ContagemResultadoExecucao_Prevista ;
      private DateTime A1406ContagemResultadoExecucao_Inicio ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1407ContagemResultadoExecucao_Fim ;
      private bool n1408ContagemResultadoExecucao_Prevista ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n1411ContagemResultadoExecucao_PrazoDias ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00VT2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00VT2_n1553ContagemResultado_CntSrvCod ;
      private DateTime[] P00VT2_A1407ContagemResultadoExecucao_Fim ;
      private bool[] P00VT2_n1407ContagemResultadoExecucao_Fim ;
      private int[] P00VT2_A1404ContagemResultadoExecucao_OSCod ;
      private DateTime[] P00VT2_A1408ContagemResultadoExecucao_Prevista ;
      private bool[] P00VT2_n1408ContagemResultadoExecucao_Prevista ;
      private String[] P00VT2_A1611ContagemResultado_PrzTpDias ;
      private bool[] P00VT2_n1611ContagemResultado_PrzTpDias ;
      private DateTime[] P00VT2_A1406ContagemResultadoExecucao_Inicio ;
      private short[] P00VT2_A1411ContagemResultadoExecucao_PrazoDias ;
      private bool[] P00VT2_n1411ContagemResultadoExecucao_PrazoDias ;
      private int[] P00VT2_A1405ContagemResultadoExecucao_Codigo ;
   }

   public class prc_updcicloexc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VT2 ;
          prmP00VT2 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VT3 ;
          prmP00VT3 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Prevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_PrazoDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VT4 ;
          prmP00VT4 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Prevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_PrazoDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VT2", "SELECT TOP 1 T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultadoExecucao_Fim], T1.[ContagemResultadoExecucao_OSCod] AS ContagemResultadoExecucao_OSCod, T1.[ContagemResultadoExecucao_Prevista], T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[ContagemResultadoExecucao_Inicio], T1.[ContagemResultadoExecucao_PrazoDias], T1.[ContagemResultadoExecucao_Codigo] FROM (([ContagemResultadoExecucao] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoExecucao_OSCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE (T1.[ContagemResultadoExecucao_OSCod] = @AV8Codigo) AND ((T1.[ContagemResultadoExecucao_Fim] = convert( DATETIME, '17530101', 112 ))) ORDER BY T1.[ContagemResultadoExecucao_OSCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VT2,1,0,true,true )
             ,new CursorDef("P00VT3", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Prevista]=@ContagemResultadoExecucao_Prevista, [ContagemResultadoExecucao_PrazoDias]=@ContagemResultadoExecucao_PrazoDias  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VT3)
             ,new CursorDef("P00VT4", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Prevista]=@ContagemResultadoExecucao_Prevista, [ContagemResultadoExecucao_PrazoDias]=@ContagemResultadoExecucao_PrazoDias  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VT4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(6) ;
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
       }
    }

 }

}
