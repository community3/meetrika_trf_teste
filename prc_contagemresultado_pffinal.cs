/*
               File: PRC_ContagemResultado_PFFinal
        Description: Contagem Resultado_PFFinal
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:11:45.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contagemresultado_pffinal : GXProcedure
   {
      public prc_contagemresultado_pffinal( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contagemresultado_pffinal( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           out decimal aP1_ContagemResultado_PFFinal )
      {
         this.AV23Codigo = aP0_Codigo;
         this.AV9ContagemResultado_PFFinal = 0 ;
         initialize();
         executePrivate();
         aP1_ContagemResultado_PFFinal=this.AV9ContagemResultado_PFFinal;
      }

      public decimal executeUdp( int aP0_Codigo )
      {
         this.AV23Codigo = aP0_Codigo;
         this.AV9ContagemResultado_PFFinal = 0 ;
         initialize();
         executePrivate();
         aP1_ContagemResultado_PFFinal=this.AV9ContagemResultado_PFFinal;
         return AV9ContagemResultado_PFFinal ;
      }

      public void executeSubmit( int aP0_Codigo ,
                                 out decimal aP1_ContagemResultado_PFFinal )
      {
         prc_contagemresultado_pffinal objprc_contagemresultado_pffinal;
         objprc_contagemresultado_pffinal = new prc_contagemresultado_pffinal();
         objprc_contagemresultado_pffinal.AV23Codigo = aP0_Codigo;
         objprc_contagemresultado_pffinal.AV9ContagemResultado_PFFinal = 0 ;
         objprc_contagemresultado_pffinal.context.SetSubmitInitialConfig(context);
         objprc_contagemresultado_pffinal.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contagemresultado_pffinal);
         aP1_ContagemResultado_PFFinal=this.AV9ContagemResultado_PFFinal;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contagemresultado_pffinal)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00412 */
         pr_default.execute(0, new Object[] {AV23Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A490ContagemResultado_ContratadaCod = P00412_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00412_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00412_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00412_n52Contratada_AreaTrabalhoCod[0];
            A456ContagemResultado_Codigo = P00412_A456ContagemResultado_Codigo[0];
            A602ContagemResultado_OSVinculada = P00412_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00412_n602ContagemResultado_OSVinculada[0];
            A1593ContagemResultado_CntSrvTpVnc = P00412_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P00412_n1593ContagemResultado_CntSrvTpVnc[0];
            A1326ContagemResultado_ContratadaTipoFab = P00412_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00412_n1326ContagemResultado_ContratadaTipoFab[0];
            A484ContagemResultado_StatusDmn = P00412_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00412_n484ContagemResultado_StatusDmn[0];
            A1854ContagemResultado_VlrCnc = P00412_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P00412_n1854ContagemResultado_VlrCnc[0];
            A601ContagemResultado_Servico = P00412_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00412_n601ContagemResultado_Servico[0];
            A459ContagemResultado_PFLFS = P00412_A459ContagemResultado_PFLFS[0];
            n459ContagemResultado_PFLFS = P00412_n459ContagemResultado_PFLFS[0];
            A458ContagemResultado_PFBFS = P00412_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = P00412_n458ContagemResultado_PFBFS[0];
            A1592Contratada_AreaTrbClcPFnl = P00412_A1592Contratada_AreaTrbClcPFnl[0];
            n1592Contratada_AreaTrbClcPFnl = P00412_n1592Contratada_AreaTrbClcPFnl[0];
            A460ContagemResultado_PFBFM = P00412_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = P00412_n460ContagemResultado_PFBFM[0];
            A800ContagemResultado_Deflator = P00412_A800ContagemResultado_Deflator[0];
            n800ContagemResultado_Deflator = P00412_n800ContagemResultado_Deflator[0];
            A1594ContagemResultado_CntSrvFtrm = P00412_A1594ContagemResultado_CntSrvFtrm[0];
            n1594ContagemResultado_CntSrvFtrm = P00412_n1594ContagemResultado_CntSrvFtrm[0];
            A461ContagemResultado_PFLFM = P00412_A461ContagemResultado_PFLFM[0];
            n461ContagemResultado_PFLFM = P00412_n461ContagemResultado_PFLFM[0];
            A2074ContratoServicos_CalculoRmn = P00412_A2074ContratoServicos_CalculoRmn[0];
            n2074ContratoServicos_CalculoRmn = P00412_n2074ContratoServicos_CalculoRmn[0];
            A1553ContagemResultado_CntSrvCod = P00412_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00412_n1553ContagemResultado_CntSrvCod[0];
            A511ContagemResultado_HoraCnt = P00412_A511ContagemResultado_HoraCnt[0];
            A473ContagemResultado_DataCnt = P00412_A473ContagemResultado_DataCnt[0];
            A490ContagemResultado_ContratadaCod = P00412_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00412_n490ContagemResultado_ContratadaCod[0];
            A602ContagemResultado_OSVinculada = P00412_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00412_n602ContagemResultado_OSVinculada[0];
            A484ContagemResultado_StatusDmn = P00412_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00412_n484ContagemResultado_StatusDmn[0];
            A1854ContagemResultado_VlrCnc = P00412_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P00412_n1854ContagemResultado_VlrCnc[0];
            A1553ContagemResultado_CntSrvCod = P00412_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00412_n1553ContagemResultado_CntSrvCod[0];
            A52Contratada_AreaTrabalhoCod = P00412_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00412_n52Contratada_AreaTrabalhoCod[0];
            A1326ContagemResultado_ContratadaTipoFab = P00412_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00412_n1326ContagemResultado_ContratadaTipoFab[0];
            A1592Contratada_AreaTrbClcPFnl = P00412_A1592Contratada_AreaTrbClcPFnl[0];
            n1592Contratada_AreaTrbClcPFnl = P00412_n1592Contratada_AreaTrbClcPFnl[0];
            A1593ContagemResultado_CntSrvTpVnc = P00412_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P00412_n1593ContagemResultado_CntSrvTpVnc[0];
            A601ContagemResultado_Servico = P00412_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00412_n601ContagemResultado_Servico[0];
            A1594ContagemResultado_CntSrvFtrm = P00412_A1594ContagemResultado_CntSrvFtrm[0];
            n1594ContagemResultado_CntSrvFtrm = P00412_n1594ContagemResultado_CntSrvFtrm[0];
            A2074ContratoServicos_CalculoRmn = P00412_A2074ContratoServicos_CalculoRmn[0];
            n2074ContratoServicos_CalculoRmn = P00412_n2074ContratoServicos_CalculoRmn[0];
            OV15TipoFabrica = AV15TipoFabrica;
            AV16OSVinculada = A602ContagemResultado_OSVinculada;
            AV14ContratoServicos_TipoVnc = A1593ContagemResultado_CntSrvTpVnc;
            AV15TipoFabrica = A1326ContagemResultado_ContratadaTipoFab;
            if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
            {
               if ( ( A1854ContagemResultado_VlrCnc > Convert.ToDecimal( 0 )) )
               {
                  AV9ContagemResultado_PFFinal = A1854ContagemResultado_VlrCnc;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
            }
            if ( (0==A601ContagemResultado_Servico) )
            {
               if ( StringUtil.StrCmp(AV15TipoFabrica, "S") == 0 )
               {
                  AV9ContagemResultado_PFFinal = A459ContagemResultado_PFLFS;
               }
               else
               {
                  if ( ( A458ContagemResultado_PFBFS > Convert.ToDecimal( 0 )) )
                  {
                     if ( StringUtil.StrCmp(A1592Contratada_AreaTrbClcPFnl, "MB") == 0 )
                     {
                        AV9ContagemResultado_PFFinal = ((A458ContagemResultado_PFBFS<A460ContagemResultado_PFBFM) ? A458ContagemResultado_PFBFS : A460ContagemResultado_PFBFM);
                     }
                     else if ( StringUtil.StrCmp(A1592Contratada_AreaTrbClcPFnl, "BM") == 0 )
                     {
                     }
                  }
                  else
                  {
                     AV9ContagemResultado_PFFinal = A460ContagemResultado_PFBFM;
                  }
               }
            }
            else
            {
               if ( ( A800ContagemResultado_Deflator > Convert.ToDecimal( 0 )) )
               {
                  AV19dEFLATOR = A800ContagemResultado_Deflator;
               }
               else
               {
                  AV19dEFLATOR = (decimal)(1);
               }
               if ( StringUtil.StrCmp(AV15TipoFabrica, "S") == 0 )
               {
                  if ( StringUtil.StrCmp(A1594ContagemResultado_CntSrvFtrm, "B") == 0 )
                  {
                     AV9ContagemResultado_PFFinal = A458ContagemResultado_PFBFS;
                  }
                  else if ( StringUtil.StrCmp(A1594ContagemResultado_CntSrvFtrm, "L") == 0 )
                  {
                     AV9ContagemResultado_PFFinal = A459ContagemResultado_PFLFS;
                  }
                  AV9ContagemResultado_PFFinal = (decimal)(AV9ContagemResultado_PFFinal*AV19dEFLATOR);
               }
               else
               {
                  if ( StringUtil.StrCmp(A1594ContagemResultado_CntSrvFtrm, "B") == 0 )
                  {
                     if ( ! (Convert.ToDecimal(0)==A458ContagemResultado_PFBFS) && ( StringUtil.StrCmp(A1592Contratada_AreaTrbClcPFnl, "MB") == 0 ) )
                     {
                        AV9ContagemResultado_PFFinal = ((A458ContagemResultado_PFBFS<A460ContagemResultado_PFBFM) ? A458ContagemResultado_PFBFS : A460ContagemResultado_PFBFM);
                     }
                     else if ( (Convert.ToDecimal(0)==A458ContagemResultado_PFBFS) || ( StringUtil.StrCmp(A1592Contratada_AreaTrbClcPFnl, "BM") == 0 ) )
                     {
                        AV9ContagemResultado_PFFinal = A460ContagemResultado_PFBFM;
                     }
                     AV9ContagemResultado_PFFinal = (decimal)(AV9ContagemResultado_PFFinal*AV19dEFLATOR);
                  }
                  else
                  {
                     AV9ContagemResultado_PFFinal = A461ContagemResultado_PFLFM;
                  }
               }
               if ( A2074ContratoServicos_CalculoRmn > 0 )
               {
                  GXt_decimal1 = AV9ContagemResultado_PFFinal;
                  new prc_getvalorremuneracao(context ).execute( ref  A1553ContagemResultado_CntSrvCod,  AV9ContagemResultado_PFFinal, out  GXt_decimal1) ;
                  AV9ContagemResultado_PFFinal = GXt_decimal1;
               }
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( ( AV16OSVinculada > 0 ) && ( ( StringUtil.StrCmp(AV14ContratoServicos_TipoVnc, "D") == 0 ) || ( StringUtil.StrCmp(AV14ContratoServicos_TipoVnc, "A") == 0 ) ) )
         {
            GXt_decimal1 = AV17JaRecebido;
            new prc_jarecebido(context ).execute( ref  AV16OSVinculada, out  GXt_decimal1) ;
            AV17JaRecebido = GXt_decimal1;
            AV9ContagemResultado_PFFinal = (decimal)(AV9ContagemResultado_PFFinal-AV17JaRecebido);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00412_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00412_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00412_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00412_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00412_A456ContagemResultado_Codigo = new int[1] ;
         P00412_A602ContagemResultado_OSVinculada = new int[1] ;
         P00412_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00412_A1593ContagemResultado_CntSrvTpVnc = new String[] {""} ;
         P00412_n1593ContagemResultado_CntSrvTpVnc = new bool[] {false} ;
         P00412_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P00412_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P00412_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00412_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00412_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P00412_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P00412_A601ContagemResultado_Servico = new int[1] ;
         P00412_n601ContagemResultado_Servico = new bool[] {false} ;
         P00412_A459ContagemResultado_PFLFS = new decimal[1] ;
         P00412_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P00412_A458ContagemResultado_PFBFS = new decimal[1] ;
         P00412_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P00412_A1592Contratada_AreaTrbClcPFnl = new String[] {""} ;
         P00412_n1592Contratada_AreaTrbClcPFnl = new bool[] {false} ;
         P00412_A460ContagemResultado_PFBFM = new decimal[1] ;
         P00412_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P00412_A800ContagemResultado_Deflator = new decimal[1] ;
         P00412_n800ContagemResultado_Deflator = new bool[] {false} ;
         P00412_A1594ContagemResultado_CntSrvFtrm = new String[] {""} ;
         P00412_n1594ContagemResultado_CntSrvFtrm = new bool[] {false} ;
         P00412_A461ContagemResultado_PFLFM = new decimal[1] ;
         P00412_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P00412_A2074ContratoServicos_CalculoRmn = new short[1] ;
         P00412_n2074ContratoServicos_CalculoRmn = new bool[] {false} ;
         P00412_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00412_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00412_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P00412_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         A1593ContagemResultado_CntSrvTpVnc = "";
         A1326ContagemResultado_ContratadaTipoFab = "";
         A484ContagemResultado_StatusDmn = "";
         A1592Contratada_AreaTrbClcPFnl = "";
         A1594ContagemResultado_CntSrvFtrm = "";
         A511ContagemResultado_HoraCnt = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         OV15TipoFabrica = "";
         AV15TipoFabrica = "S";
         AV14ContratoServicos_TipoVnc = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contagemresultado_pffinal__default(),
            new Object[][] {
                new Object[] {
               P00412_A490ContagemResultado_ContratadaCod, P00412_n490ContagemResultado_ContratadaCod, P00412_A52Contratada_AreaTrabalhoCod, P00412_n52Contratada_AreaTrabalhoCod, P00412_A456ContagemResultado_Codigo, P00412_A602ContagemResultado_OSVinculada, P00412_n602ContagemResultado_OSVinculada, P00412_A1593ContagemResultado_CntSrvTpVnc, P00412_n1593ContagemResultado_CntSrvTpVnc, P00412_A1326ContagemResultado_ContratadaTipoFab,
               P00412_n1326ContagemResultado_ContratadaTipoFab, P00412_A484ContagemResultado_StatusDmn, P00412_n484ContagemResultado_StatusDmn, P00412_A1854ContagemResultado_VlrCnc, P00412_n1854ContagemResultado_VlrCnc, P00412_A601ContagemResultado_Servico, P00412_n601ContagemResultado_Servico, P00412_A459ContagemResultado_PFLFS, P00412_n459ContagemResultado_PFLFS, P00412_A458ContagemResultado_PFBFS,
               P00412_n458ContagemResultado_PFBFS, P00412_A1592Contratada_AreaTrbClcPFnl, P00412_n1592Contratada_AreaTrbClcPFnl, P00412_A460ContagemResultado_PFBFM, P00412_n460ContagemResultado_PFBFM, P00412_A800ContagemResultado_Deflator, P00412_n800ContagemResultado_Deflator, P00412_A1594ContagemResultado_CntSrvFtrm, P00412_n1594ContagemResultado_CntSrvFtrm, P00412_A461ContagemResultado_PFLFM,
               P00412_n461ContagemResultado_PFLFM, P00412_A2074ContratoServicos_CalculoRmn, P00412_n2074ContratoServicos_CalculoRmn, P00412_A1553ContagemResultado_CntSrvCod, P00412_n1553ContagemResultado_CntSrvCod, P00412_A511ContagemResultado_HoraCnt, P00412_A473ContagemResultado_DataCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A2074ContratoServicos_CalculoRmn ;
      private int AV23Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A456ContagemResultado_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int A601ContagemResultado_Servico ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int AV16OSVinculada ;
      private decimal AV9ContagemResultado_PFFinal ;
      private decimal A1854ContagemResultado_VlrCnc ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A800ContagemResultado_Deflator ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal AV19dEFLATOR ;
      private decimal AV17JaRecebido ;
      private decimal GXt_decimal1 ;
      private String scmdbuf ;
      private String A1593ContagemResultado_CntSrvTpVnc ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1592Contratada_AreaTrbClcPFnl ;
      private String A511ContagemResultado_HoraCnt ;
      private String OV15TipoFabrica ;
      private String AV15TipoFabrica ;
      private String AV14ContratoServicos_TipoVnc ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n1593ContagemResultado_CntSrvTpVnc ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1854ContagemResultado_VlrCnc ;
      private bool n601ContagemResultado_Servico ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n1592Contratada_AreaTrbClcPFnl ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n800ContagemResultado_Deflator ;
      private bool n1594ContagemResultado_CntSrvFtrm ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n2074ContratoServicos_CalculoRmn ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private String A1594ContagemResultado_CntSrvFtrm ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00412_A490ContagemResultado_ContratadaCod ;
      private bool[] P00412_n490ContagemResultado_ContratadaCod ;
      private int[] P00412_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00412_n52Contratada_AreaTrabalhoCod ;
      private int[] P00412_A456ContagemResultado_Codigo ;
      private int[] P00412_A602ContagemResultado_OSVinculada ;
      private bool[] P00412_n602ContagemResultado_OSVinculada ;
      private String[] P00412_A1593ContagemResultado_CntSrvTpVnc ;
      private bool[] P00412_n1593ContagemResultado_CntSrvTpVnc ;
      private String[] P00412_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P00412_n1326ContagemResultado_ContratadaTipoFab ;
      private String[] P00412_A484ContagemResultado_StatusDmn ;
      private bool[] P00412_n484ContagemResultado_StatusDmn ;
      private decimal[] P00412_A1854ContagemResultado_VlrCnc ;
      private bool[] P00412_n1854ContagemResultado_VlrCnc ;
      private int[] P00412_A601ContagemResultado_Servico ;
      private bool[] P00412_n601ContagemResultado_Servico ;
      private decimal[] P00412_A459ContagemResultado_PFLFS ;
      private bool[] P00412_n459ContagemResultado_PFLFS ;
      private decimal[] P00412_A458ContagemResultado_PFBFS ;
      private bool[] P00412_n458ContagemResultado_PFBFS ;
      private String[] P00412_A1592Contratada_AreaTrbClcPFnl ;
      private bool[] P00412_n1592Contratada_AreaTrbClcPFnl ;
      private decimal[] P00412_A460ContagemResultado_PFBFM ;
      private bool[] P00412_n460ContagemResultado_PFBFM ;
      private decimal[] P00412_A800ContagemResultado_Deflator ;
      private bool[] P00412_n800ContagemResultado_Deflator ;
      private String[] P00412_A1594ContagemResultado_CntSrvFtrm ;
      private bool[] P00412_n1594ContagemResultado_CntSrvFtrm ;
      private decimal[] P00412_A461ContagemResultado_PFLFM ;
      private bool[] P00412_n461ContagemResultado_PFLFM ;
      private short[] P00412_A2074ContratoServicos_CalculoRmn ;
      private bool[] P00412_n2074ContratoServicos_CalculoRmn ;
      private int[] P00412_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00412_n1553ContagemResultado_CntSrvCod ;
      private String[] P00412_A511ContagemResultado_HoraCnt ;
      private DateTime[] P00412_A473ContagemResultado_DataCnt ;
      private decimal aP1_ContagemResultado_PFFinal ;
   }

   public class prc_contagemresultado_pffinal__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00412 ;
          prmP00412 = new Object[] {
          new Object[] {"@AV23Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00412", "SELECT T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T3.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[ContagemResultado_Codigo], T2.[ContagemResultado_OSVinculada], T5.[ContratoServicos_TipoVnc] AS ContagemResultado_CntSrvTpVnc, T3.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_VlrCnc], T5.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_PFLFS], T1.[ContagemResultado_PFBFS], T4.[AreaTrabalho_CalculoPFinal] AS Contratada_AreaTrbClcPFnl, T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_Deflator], T5.[ServicoContrato_Faturamento] AS ContagemResultado_CntSrvFtrm, T1.[ContagemResultado_PFLFM], T5.[ContratoServicos_CalculoRmn], T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_HoraCnt], T1.[ContagemResultado_DataCnt] FROM (((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Contratada_AreaTrabalhoCod]) LEFT JOIN [ContratoServicos] T5 WITH (NOLOCK) ON T5.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV23Codigo ORDER BY T1.[ContagemResultado_Codigo] DESC, T1.[ContagemResultado_DataCnt] DESC, T1.[ContagemResultado_HoraCnt] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00412,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getString(12, 2) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((decimal[]) buf[23])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((decimal[]) buf[25])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((String[]) buf[27])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((decimal[]) buf[29])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((short[]) buf[31])[0] = rslt.getShort(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((int[]) buf[33])[0] = rslt.getInt(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((String[]) buf[35])[0] = rslt.getString(19, 5) ;
                ((DateTime[]) buf[36])[0] = rslt.getGXDate(20) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
