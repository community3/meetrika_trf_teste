/*
               File: type_SdtContratante
        Description: Contratante
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:29:6.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Contratante" )]
   [XmlType(TypeName =  "Contratante" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtContratante : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratante( )
      {
         /* Constructor for serialization */
         gxTv_SdtContratante_Contratante_cnpj = "";
         gxTv_SdtContratante_Contratante_razaosocial = "";
         gxTv_SdtContratante_Contratante_nomefantasia = "";
         gxTv_SdtContratante_Contratante_ie = "";
         gxTv_SdtContratante_Contratante_website = "";
         gxTv_SdtContratante_Contratante_email = "";
         gxTv_SdtContratante_Contratante_telefone = "";
         gxTv_SdtContratante_Contratante_ramal = "";
         gxTv_SdtContratante_Contratante_fax = "";
         gxTv_SdtContratante_Contratante_agencianome = "";
         gxTv_SdtContratante_Contratante_agencianro = "";
         gxTv_SdtContratante_Contratante_banconome = "";
         gxTv_SdtContratante_Contratante_banconro = "";
         gxTv_SdtContratante_Contratante_contacorrente = "";
         gxTv_SdtContratante_Municipio_nome = "";
         gxTv_SdtContratante_Estado_uf = "";
         gxTv_SdtContratante_Contratante_emailsdahost = "";
         gxTv_SdtContratante_Contratante_emailsdauser = "";
         gxTv_SdtContratante_Contratante_emailsdapass = "";
         gxTv_SdtContratante_Contratante_emailsdakey = "";
         gxTv_SdtContratante_Contratante_logoarquivo = "";
         gxTv_SdtContratante_Contratante_logonomearq = "";
         gxTv_SdtContratante_Contratante_logotipoarq = "";
         gxTv_SdtContratante_Contratante_iniciodoexpediente = (DateTime)(DateTime.MinValue);
         gxTv_SdtContratante_Contratante_fimdoexpediente = (DateTime)(DateTime.MinValue);
         gxTv_SdtContratante_Contratante_ssstatusplanejamento = "";
         gxTv_SdtContratante_Contratante_ttlrltgerencial = "";
         gxTv_SdtContratante_Contratante_sigla = "";
         gxTv_SdtContratante_Mode = "";
         gxTv_SdtContratante_Contratante_cnpj_Z = "";
         gxTv_SdtContratante_Contratante_razaosocial_Z = "";
         gxTv_SdtContratante_Contratante_nomefantasia_Z = "";
         gxTv_SdtContratante_Contratante_ie_Z = "";
         gxTv_SdtContratante_Contratante_website_Z = "";
         gxTv_SdtContratante_Contratante_email_Z = "";
         gxTv_SdtContratante_Contratante_telefone_Z = "";
         gxTv_SdtContratante_Contratante_ramal_Z = "";
         gxTv_SdtContratante_Contratante_fax_Z = "";
         gxTv_SdtContratante_Contratante_agencianome_Z = "";
         gxTv_SdtContratante_Contratante_agencianro_Z = "";
         gxTv_SdtContratante_Contratante_banconome_Z = "";
         gxTv_SdtContratante_Contratante_banconro_Z = "";
         gxTv_SdtContratante_Contratante_contacorrente_Z = "";
         gxTv_SdtContratante_Municipio_nome_Z = "";
         gxTv_SdtContratante_Estado_uf_Z = "";
         gxTv_SdtContratante_Contratante_emailsdahost_Z = "";
         gxTv_SdtContratante_Contratante_emailsdauser_Z = "";
         gxTv_SdtContratante_Contratante_emailsdapass_Z = "";
         gxTv_SdtContratante_Contratante_emailsdakey_Z = "";
         gxTv_SdtContratante_Contratante_logonomearq_Z = "";
         gxTv_SdtContratante_Contratante_logotipoarq_Z = "";
         gxTv_SdtContratante_Contratante_iniciodoexpediente_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContratante_Contratante_fimdoexpediente_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContratante_Contratante_ssstatusplanejamento_Z = "";
         gxTv_SdtContratante_Contratante_ttlrltgerencial_Z = "";
         gxTv_SdtContratante_Contratante_sigla_Z = "";
      }

      public SdtContratante( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV29Contratante_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV29Contratante_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Contratante_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Contratante");
         metadata.Set("BT", "Contratante");
         metadata.Set("PK", "[ \"Contratante_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Contratante_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Municipio_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Pessoa_Codigo\" ],\"FKMap\":[ \"Contratante_PessoaCod-Pessoa_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_pessoacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_cnpj_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_razaosocial_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_nomefantasia_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ie_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_website_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_email_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_telefone_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ramal_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_fax_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_agencianome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_agencianro_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_banconome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_banconro_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_contacorrente_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Municipio_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Municipio_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Estado_uf_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdahost_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdauser_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdapass_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdakey_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdaaut_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdaport_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdasec_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_osautomatica_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ssautomatica_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_existeconferencia_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_logonomearq_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_logotipoarq_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_iniciodoexpediente_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_fimdoexpediente_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_servicosspadrao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_retornass_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ssstatusplanejamento_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ssprestadoraunica_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_propostarqrsrv_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_propostarqrprz_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_propostarqresf_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_propostarqrcnt_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_propostanvlcnt_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_osgeraos_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_osgeratrp_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_osgerath_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_osgeratrd_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_osgeratr_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_oshmlgcompnd_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ativocirculante_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_passivocirculante_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_patrimonioliquido_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_receitabruta_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_usaosistema_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ttlrltgerencial_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_przaortr_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_przactrtr_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_requerorigem_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_selecionaresponsavelos_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_exibepf_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_sigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_cnpj_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_razaosocial_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_website_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_email_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ramal_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_fax_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_agencianome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_agencianro_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_banconome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_banconro_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_contacorrente_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Municipio_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdahost_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdauser_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdapass_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdakey_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdaaut_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdaport_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdasec_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ssautomatica_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_logoarquivo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_logonomearq_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_logotipoarq_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_iniciodoexpediente_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_fimdoexpediente_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_servicosspadrao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_retornass_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ssstatusplanejamento_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ssprestadoraunica_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_propostarqrsrv_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_propostarqrprz_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_propostarqresf_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_propostarqrcnt_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_propostanvlcnt_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_osgeraos_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_osgeratrp_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_osgerath_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_osgeratrd_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_osgeratr_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_oshmlgcompnd_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ativocirculante_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_passivocirculante_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_patrimonioliquido_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_receitabruta_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_usaosistema_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ttlrltgerencial_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_przaortr_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_przactrtr_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_requerorigem_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_sigla_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContratante deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContratante)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContratante obj ;
         obj = this;
         obj.gxTpr_Contratante_codigo = deserialized.gxTpr_Contratante_codigo;
         obj.gxTpr_Contratante_pessoacod = deserialized.gxTpr_Contratante_pessoacod;
         obj.gxTpr_Contratante_cnpj = deserialized.gxTpr_Contratante_cnpj;
         obj.gxTpr_Contratante_razaosocial = deserialized.gxTpr_Contratante_razaosocial;
         obj.gxTpr_Contratante_nomefantasia = deserialized.gxTpr_Contratante_nomefantasia;
         obj.gxTpr_Contratante_ie = deserialized.gxTpr_Contratante_ie;
         obj.gxTpr_Contratante_website = deserialized.gxTpr_Contratante_website;
         obj.gxTpr_Contratante_email = deserialized.gxTpr_Contratante_email;
         obj.gxTpr_Contratante_telefone = deserialized.gxTpr_Contratante_telefone;
         obj.gxTpr_Contratante_ramal = deserialized.gxTpr_Contratante_ramal;
         obj.gxTpr_Contratante_fax = deserialized.gxTpr_Contratante_fax;
         obj.gxTpr_Contratante_agencianome = deserialized.gxTpr_Contratante_agencianome;
         obj.gxTpr_Contratante_agencianro = deserialized.gxTpr_Contratante_agencianro;
         obj.gxTpr_Contratante_banconome = deserialized.gxTpr_Contratante_banconome;
         obj.gxTpr_Contratante_banconro = deserialized.gxTpr_Contratante_banconro;
         obj.gxTpr_Contratante_contacorrente = deserialized.gxTpr_Contratante_contacorrente;
         obj.gxTpr_Municipio_codigo = deserialized.gxTpr_Municipio_codigo;
         obj.gxTpr_Municipio_nome = deserialized.gxTpr_Municipio_nome;
         obj.gxTpr_Estado_uf = deserialized.gxTpr_Estado_uf;
         obj.gxTpr_Contratante_ativo = deserialized.gxTpr_Contratante_ativo;
         obj.gxTpr_Contratante_emailsdahost = deserialized.gxTpr_Contratante_emailsdahost;
         obj.gxTpr_Contratante_emailsdauser = deserialized.gxTpr_Contratante_emailsdauser;
         obj.gxTpr_Contratante_emailsdapass = deserialized.gxTpr_Contratante_emailsdapass;
         obj.gxTpr_Contratante_emailsdakey = deserialized.gxTpr_Contratante_emailsdakey;
         obj.gxTpr_Contratante_emailsdaaut = deserialized.gxTpr_Contratante_emailsdaaut;
         obj.gxTpr_Contratante_emailsdaport = deserialized.gxTpr_Contratante_emailsdaport;
         obj.gxTpr_Contratante_emailsdasec = deserialized.gxTpr_Contratante_emailsdasec;
         obj.gxTpr_Contratante_osautomatica = deserialized.gxTpr_Contratante_osautomatica;
         obj.gxTpr_Contratante_ssautomatica = deserialized.gxTpr_Contratante_ssautomatica;
         obj.gxTpr_Contratante_existeconferencia = deserialized.gxTpr_Contratante_existeconferencia;
         obj.gxTpr_Contratante_logoarquivo = deserialized.gxTpr_Contratante_logoarquivo;
         obj.gxTpr_Contratante_logonomearq = deserialized.gxTpr_Contratante_logonomearq;
         obj.gxTpr_Contratante_logotipoarq = deserialized.gxTpr_Contratante_logotipoarq;
         obj.gxTpr_Contratante_iniciodoexpediente = deserialized.gxTpr_Contratante_iniciodoexpediente;
         obj.gxTpr_Contratante_fimdoexpediente = deserialized.gxTpr_Contratante_fimdoexpediente;
         obj.gxTpr_Contratante_servicosspadrao = deserialized.gxTpr_Contratante_servicosspadrao;
         obj.gxTpr_Contratante_retornass = deserialized.gxTpr_Contratante_retornass;
         obj.gxTpr_Contratante_ssstatusplanejamento = deserialized.gxTpr_Contratante_ssstatusplanejamento;
         obj.gxTpr_Contratante_ssprestadoraunica = deserialized.gxTpr_Contratante_ssprestadoraunica;
         obj.gxTpr_Contratante_propostarqrsrv = deserialized.gxTpr_Contratante_propostarqrsrv;
         obj.gxTpr_Contratante_propostarqrprz = deserialized.gxTpr_Contratante_propostarqrprz;
         obj.gxTpr_Contratante_propostarqresf = deserialized.gxTpr_Contratante_propostarqresf;
         obj.gxTpr_Contratante_propostarqrcnt = deserialized.gxTpr_Contratante_propostarqrcnt;
         obj.gxTpr_Contratante_propostanvlcnt = deserialized.gxTpr_Contratante_propostanvlcnt;
         obj.gxTpr_Contratante_osgeraos = deserialized.gxTpr_Contratante_osgeraos;
         obj.gxTpr_Contratante_osgeratrp = deserialized.gxTpr_Contratante_osgeratrp;
         obj.gxTpr_Contratante_osgerath = deserialized.gxTpr_Contratante_osgerath;
         obj.gxTpr_Contratante_osgeratrd = deserialized.gxTpr_Contratante_osgeratrd;
         obj.gxTpr_Contratante_osgeratr = deserialized.gxTpr_Contratante_osgeratr;
         obj.gxTpr_Contratante_oshmlgcompnd = deserialized.gxTpr_Contratante_oshmlgcompnd;
         obj.gxTpr_Contratante_ativocirculante = deserialized.gxTpr_Contratante_ativocirculante;
         obj.gxTpr_Contratante_passivocirculante = deserialized.gxTpr_Contratante_passivocirculante;
         obj.gxTpr_Contratante_patrimonioliquido = deserialized.gxTpr_Contratante_patrimonioliquido;
         obj.gxTpr_Contratante_receitabruta = deserialized.gxTpr_Contratante_receitabruta;
         obj.gxTpr_Contratante_usaosistema = deserialized.gxTpr_Contratante_usaosistema;
         obj.gxTpr_Contratante_ttlrltgerencial = deserialized.gxTpr_Contratante_ttlrltgerencial;
         obj.gxTpr_Contratante_przaortr = deserialized.gxTpr_Contratante_przaortr;
         obj.gxTpr_Contratante_przactrtr = deserialized.gxTpr_Contratante_przactrtr;
         obj.gxTpr_Contratante_requerorigem = deserialized.gxTpr_Contratante_requerorigem;
         obj.gxTpr_Contratante_selecionaresponsavelos = deserialized.gxTpr_Contratante_selecionaresponsavelos;
         obj.gxTpr_Contratante_exibepf = deserialized.gxTpr_Contratante_exibepf;
         obj.gxTpr_Contratante_sigla = deserialized.gxTpr_Contratante_sigla;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contratante_codigo_Z = deserialized.gxTpr_Contratante_codigo_Z;
         obj.gxTpr_Contratante_pessoacod_Z = deserialized.gxTpr_Contratante_pessoacod_Z;
         obj.gxTpr_Contratante_cnpj_Z = deserialized.gxTpr_Contratante_cnpj_Z;
         obj.gxTpr_Contratante_razaosocial_Z = deserialized.gxTpr_Contratante_razaosocial_Z;
         obj.gxTpr_Contratante_nomefantasia_Z = deserialized.gxTpr_Contratante_nomefantasia_Z;
         obj.gxTpr_Contratante_ie_Z = deserialized.gxTpr_Contratante_ie_Z;
         obj.gxTpr_Contratante_website_Z = deserialized.gxTpr_Contratante_website_Z;
         obj.gxTpr_Contratante_email_Z = deserialized.gxTpr_Contratante_email_Z;
         obj.gxTpr_Contratante_telefone_Z = deserialized.gxTpr_Contratante_telefone_Z;
         obj.gxTpr_Contratante_ramal_Z = deserialized.gxTpr_Contratante_ramal_Z;
         obj.gxTpr_Contratante_fax_Z = deserialized.gxTpr_Contratante_fax_Z;
         obj.gxTpr_Contratante_agencianome_Z = deserialized.gxTpr_Contratante_agencianome_Z;
         obj.gxTpr_Contratante_agencianro_Z = deserialized.gxTpr_Contratante_agencianro_Z;
         obj.gxTpr_Contratante_banconome_Z = deserialized.gxTpr_Contratante_banconome_Z;
         obj.gxTpr_Contratante_banconro_Z = deserialized.gxTpr_Contratante_banconro_Z;
         obj.gxTpr_Contratante_contacorrente_Z = deserialized.gxTpr_Contratante_contacorrente_Z;
         obj.gxTpr_Municipio_codigo_Z = deserialized.gxTpr_Municipio_codigo_Z;
         obj.gxTpr_Municipio_nome_Z = deserialized.gxTpr_Municipio_nome_Z;
         obj.gxTpr_Estado_uf_Z = deserialized.gxTpr_Estado_uf_Z;
         obj.gxTpr_Contratante_ativo_Z = deserialized.gxTpr_Contratante_ativo_Z;
         obj.gxTpr_Contratante_emailsdahost_Z = deserialized.gxTpr_Contratante_emailsdahost_Z;
         obj.gxTpr_Contratante_emailsdauser_Z = deserialized.gxTpr_Contratante_emailsdauser_Z;
         obj.gxTpr_Contratante_emailsdapass_Z = deserialized.gxTpr_Contratante_emailsdapass_Z;
         obj.gxTpr_Contratante_emailsdakey_Z = deserialized.gxTpr_Contratante_emailsdakey_Z;
         obj.gxTpr_Contratante_emailsdaaut_Z = deserialized.gxTpr_Contratante_emailsdaaut_Z;
         obj.gxTpr_Contratante_emailsdaport_Z = deserialized.gxTpr_Contratante_emailsdaport_Z;
         obj.gxTpr_Contratante_emailsdasec_Z = deserialized.gxTpr_Contratante_emailsdasec_Z;
         obj.gxTpr_Contratante_osautomatica_Z = deserialized.gxTpr_Contratante_osautomatica_Z;
         obj.gxTpr_Contratante_ssautomatica_Z = deserialized.gxTpr_Contratante_ssautomatica_Z;
         obj.gxTpr_Contratante_existeconferencia_Z = deserialized.gxTpr_Contratante_existeconferencia_Z;
         obj.gxTpr_Contratante_logonomearq_Z = deserialized.gxTpr_Contratante_logonomearq_Z;
         obj.gxTpr_Contratante_logotipoarq_Z = deserialized.gxTpr_Contratante_logotipoarq_Z;
         obj.gxTpr_Contratante_iniciodoexpediente_Z = deserialized.gxTpr_Contratante_iniciodoexpediente_Z;
         obj.gxTpr_Contratante_fimdoexpediente_Z = deserialized.gxTpr_Contratante_fimdoexpediente_Z;
         obj.gxTpr_Contratante_servicosspadrao_Z = deserialized.gxTpr_Contratante_servicosspadrao_Z;
         obj.gxTpr_Contratante_retornass_Z = deserialized.gxTpr_Contratante_retornass_Z;
         obj.gxTpr_Contratante_ssstatusplanejamento_Z = deserialized.gxTpr_Contratante_ssstatusplanejamento_Z;
         obj.gxTpr_Contratante_ssprestadoraunica_Z = deserialized.gxTpr_Contratante_ssprestadoraunica_Z;
         obj.gxTpr_Contratante_propostarqrsrv_Z = deserialized.gxTpr_Contratante_propostarqrsrv_Z;
         obj.gxTpr_Contratante_propostarqrprz_Z = deserialized.gxTpr_Contratante_propostarqrprz_Z;
         obj.gxTpr_Contratante_propostarqresf_Z = deserialized.gxTpr_Contratante_propostarqresf_Z;
         obj.gxTpr_Contratante_propostarqrcnt_Z = deserialized.gxTpr_Contratante_propostarqrcnt_Z;
         obj.gxTpr_Contratante_propostanvlcnt_Z = deserialized.gxTpr_Contratante_propostanvlcnt_Z;
         obj.gxTpr_Contratante_osgeraos_Z = deserialized.gxTpr_Contratante_osgeraos_Z;
         obj.gxTpr_Contratante_osgeratrp_Z = deserialized.gxTpr_Contratante_osgeratrp_Z;
         obj.gxTpr_Contratante_osgerath_Z = deserialized.gxTpr_Contratante_osgerath_Z;
         obj.gxTpr_Contratante_osgeratrd_Z = deserialized.gxTpr_Contratante_osgeratrd_Z;
         obj.gxTpr_Contratante_osgeratr_Z = deserialized.gxTpr_Contratante_osgeratr_Z;
         obj.gxTpr_Contratante_oshmlgcompnd_Z = deserialized.gxTpr_Contratante_oshmlgcompnd_Z;
         obj.gxTpr_Contratante_ativocirculante_Z = deserialized.gxTpr_Contratante_ativocirculante_Z;
         obj.gxTpr_Contratante_passivocirculante_Z = deserialized.gxTpr_Contratante_passivocirculante_Z;
         obj.gxTpr_Contratante_patrimonioliquido_Z = deserialized.gxTpr_Contratante_patrimonioliquido_Z;
         obj.gxTpr_Contratante_receitabruta_Z = deserialized.gxTpr_Contratante_receitabruta_Z;
         obj.gxTpr_Contratante_usaosistema_Z = deserialized.gxTpr_Contratante_usaosistema_Z;
         obj.gxTpr_Contratante_ttlrltgerencial_Z = deserialized.gxTpr_Contratante_ttlrltgerencial_Z;
         obj.gxTpr_Contratante_przaortr_Z = deserialized.gxTpr_Contratante_przaortr_Z;
         obj.gxTpr_Contratante_przactrtr_Z = deserialized.gxTpr_Contratante_przactrtr_Z;
         obj.gxTpr_Contratante_requerorigem_Z = deserialized.gxTpr_Contratante_requerorigem_Z;
         obj.gxTpr_Contratante_selecionaresponsavelos_Z = deserialized.gxTpr_Contratante_selecionaresponsavelos_Z;
         obj.gxTpr_Contratante_exibepf_Z = deserialized.gxTpr_Contratante_exibepf_Z;
         obj.gxTpr_Contratante_sigla_Z = deserialized.gxTpr_Contratante_sigla_Z;
         obj.gxTpr_Contratante_codigo_N = deserialized.gxTpr_Contratante_codigo_N;
         obj.gxTpr_Contratante_cnpj_N = deserialized.gxTpr_Contratante_cnpj_N;
         obj.gxTpr_Contratante_razaosocial_N = deserialized.gxTpr_Contratante_razaosocial_N;
         obj.gxTpr_Contratante_website_N = deserialized.gxTpr_Contratante_website_N;
         obj.gxTpr_Contratante_email_N = deserialized.gxTpr_Contratante_email_N;
         obj.gxTpr_Contratante_ramal_N = deserialized.gxTpr_Contratante_ramal_N;
         obj.gxTpr_Contratante_fax_N = deserialized.gxTpr_Contratante_fax_N;
         obj.gxTpr_Contratante_agencianome_N = deserialized.gxTpr_Contratante_agencianome_N;
         obj.gxTpr_Contratante_agencianro_N = deserialized.gxTpr_Contratante_agencianro_N;
         obj.gxTpr_Contratante_banconome_N = deserialized.gxTpr_Contratante_banconome_N;
         obj.gxTpr_Contratante_banconro_N = deserialized.gxTpr_Contratante_banconro_N;
         obj.gxTpr_Contratante_contacorrente_N = deserialized.gxTpr_Contratante_contacorrente_N;
         obj.gxTpr_Municipio_codigo_N = deserialized.gxTpr_Municipio_codigo_N;
         obj.gxTpr_Contratante_emailsdahost_N = deserialized.gxTpr_Contratante_emailsdahost_N;
         obj.gxTpr_Contratante_emailsdauser_N = deserialized.gxTpr_Contratante_emailsdauser_N;
         obj.gxTpr_Contratante_emailsdapass_N = deserialized.gxTpr_Contratante_emailsdapass_N;
         obj.gxTpr_Contratante_emailsdakey_N = deserialized.gxTpr_Contratante_emailsdakey_N;
         obj.gxTpr_Contratante_emailsdaaut_N = deserialized.gxTpr_Contratante_emailsdaaut_N;
         obj.gxTpr_Contratante_emailsdaport_N = deserialized.gxTpr_Contratante_emailsdaport_N;
         obj.gxTpr_Contratante_emailsdasec_N = deserialized.gxTpr_Contratante_emailsdasec_N;
         obj.gxTpr_Contratante_ssautomatica_N = deserialized.gxTpr_Contratante_ssautomatica_N;
         obj.gxTpr_Contratante_logoarquivo_N = deserialized.gxTpr_Contratante_logoarquivo_N;
         obj.gxTpr_Contratante_logonomearq_N = deserialized.gxTpr_Contratante_logonomearq_N;
         obj.gxTpr_Contratante_logotipoarq_N = deserialized.gxTpr_Contratante_logotipoarq_N;
         obj.gxTpr_Contratante_iniciodoexpediente_N = deserialized.gxTpr_Contratante_iniciodoexpediente_N;
         obj.gxTpr_Contratante_fimdoexpediente_N = deserialized.gxTpr_Contratante_fimdoexpediente_N;
         obj.gxTpr_Contratante_servicosspadrao_N = deserialized.gxTpr_Contratante_servicosspadrao_N;
         obj.gxTpr_Contratante_retornass_N = deserialized.gxTpr_Contratante_retornass_N;
         obj.gxTpr_Contratante_ssstatusplanejamento_N = deserialized.gxTpr_Contratante_ssstatusplanejamento_N;
         obj.gxTpr_Contratante_ssprestadoraunica_N = deserialized.gxTpr_Contratante_ssprestadoraunica_N;
         obj.gxTpr_Contratante_propostarqrsrv_N = deserialized.gxTpr_Contratante_propostarqrsrv_N;
         obj.gxTpr_Contratante_propostarqrprz_N = deserialized.gxTpr_Contratante_propostarqrprz_N;
         obj.gxTpr_Contratante_propostarqresf_N = deserialized.gxTpr_Contratante_propostarqresf_N;
         obj.gxTpr_Contratante_propostarqrcnt_N = deserialized.gxTpr_Contratante_propostarqrcnt_N;
         obj.gxTpr_Contratante_propostanvlcnt_N = deserialized.gxTpr_Contratante_propostanvlcnt_N;
         obj.gxTpr_Contratante_osgeraos_N = deserialized.gxTpr_Contratante_osgeraos_N;
         obj.gxTpr_Contratante_osgeratrp_N = deserialized.gxTpr_Contratante_osgeratrp_N;
         obj.gxTpr_Contratante_osgerath_N = deserialized.gxTpr_Contratante_osgerath_N;
         obj.gxTpr_Contratante_osgeratrd_N = deserialized.gxTpr_Contratante_osgeratrd_N;
         obj.gxTpr_Contratante_osgeratr_N = deserialized.gxTpr_Contratante_osgeratr_N;
         obj.gxTpr_Contratante_oshmlgcompnd_N = deserialized.gxTpr_Contratante_oshmlgcompnd_N;
         obj.gxTpr_Contratante_ativocirculante_N = deserialized.gxTpr_Contratante_ativocirculante_N;
         obj.gxTpr_Contratante_passivocirculante_N = deserialized.gxTpr_Contratante_passivocirculante_N;
         obj.gxTpr_Contratante_patrimonioliquido_N = deserialized.gxTpr_Contratante_patrimonioliquido_N;
         obj.gxTpr_Contratante_receitabruta_N = deserialized.gxTpr_Contratante_receitabruta_N;
         obj.gxTpr_Contratante_usaosistema_N = deserialized.gxTpr_Contratante_usaosistema_N;
         obj.gxTpr_Contratante_ttlrltgerencial_N = deserialized.gxTpr_Contratante_ttlrltgerencial_N;
         obj.gxTpr_Contratante_przaortr_N = deserialized.gxTpr_Contratante_przaortr_N;
         obj.gxTpr_Contratante_przactrtr_N = deserialized.gxTpr_Contratante_przactrtr_N;
         obj.gxTpr_Contratante_requerorigem_N = deserialized.gxTpr_Contratante_requerorigem_N;
         obj.gxTpr_Contratante_sigla_N = deserialized.gxTpr_Contratante_sigla_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Codigo") )
               {
                  gxTv_SdtContratante_Contratante_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PessoaCod") )
               {
                  gxTv_SdtContratante_Contratante_pessoacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_CNPJ") )
               {
                  gxTv_SdtContratante_Contratante_cnpj = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_RazaoSocial") )
               {
                  gxTv_SdtContratante_Contratante_razaosocial = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_NomeFantasia") )
               {
                  gxTv_SdtContratante_Contratante_nomefantasia = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_IE") )
               {
                  gxTv_SdtContratante_Contratante_ie = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_WebSite") )
               {
                  gxTv_SdtContratante_Contratante_website = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Email") )
               {
                  gxTv_SdtContratante_Contratante_email = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Telefone") )
               {
                  gxTv_SdtContratante_Contratante_telefone = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Ramal") )
               {
                  gxTv_SdtContratante_Contratante_ramal = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Fax") )
               {
                  gxTv_SdtContratante_Contratante_fax = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_AgenciaNome") )
               {
                  gxTv_SdtContratante_Contratante_agencianome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_AgenciaNro") )
               {
                  gxTv_SdtContratante_Contratante_agencianro = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_BancoNome") )
               {
                  gxTv_SdtContratante_Contratante_banconome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_BancoNro") )
               {
                  gxTv_SdtContratante_Contratante_banconro = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_ContaCorrente") )
               {
                  gxTv_SdtContratante_Contratante_contacorrente = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Municipio_Codigo") )
               {
                  gxTv_SdtContratante_Municipio_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Municipio_Nome") )
               {
                  gxTv_SdtContratante_Municipio_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Estado_UF") )
               {
                  gxTv_SdtContratante_Estado_uf = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Ativo") )
               {
                  gxTv_SdtContratante_Contratante_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaHost") )
               {
                  gxTv_SdtContratante_Contratante_emailsdahost = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaUser") )
               {
                  gxTv_SdtContratante_Contratante_emailsdauser = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaPass") )
               {
                  gxTv_SdtContratante_Contratante_emailsdapass = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaKey") )
               {
                  gxTv_SdtContratante_Contratante_emailsdakey = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaAut") )
               {
                  gxTv_SdtContratante_Contratante_emailsdaaut = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaPort") )
               {
                  gxTv_SdtContratante_Contratante_emailsdaport = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaSec") )
               {
                  gxTv_SdtContratante_Contratante_emailsdasec = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSAutomatica") )
               {
                  gxTv_SdtContratante_Contratante_osautomatica = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_SSAutomatica") )
               {
                  gxTv_SdtContratante_Contratante_ssautomatica = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_ExisteConferencia") )
               {
                  gxTv_SdtContratante_Contratante_existeconferencia = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_LogoArquivo") )
               {
                  gxTv_SdtContratante_Contratante_logoarquivo=context.FileFromBase64( oReader.Value) ;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_LogoNomeArq") )
               {
                  gxTv_SdtContratante_Contratante_logonomearq = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_LogoTipoArq") )
               {
                  gxTv_SdtContratante_Contratante_logotipoarq = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_InicioDoExpediente") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContratante_Contratante_iniciodoexpediente = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContratante_Contratante_iniciodoexpediente = DateTimeUtil.ResetDate(context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), "."))));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_FimDoExpediente") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContratante_Contratante_fimdoexpediente = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContratante_Contratante_fimdoexpediente = DateTimeUtil.ResetDate(context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), "."))));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_ServicoSSPadrao") )
               {
                  gxTv_SdtContratante_Contratante_servicosspadrao = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_RetornaSS") )
               {
                  gxTv_SdtContratante_Contratante_retornass = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_SSStatusPlanejamento") )
               {
                  gxTv_SdtContratante_Contratante_ssstatusplanejamento = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_SSPrestadoraUnica") )
               {
                  gxTv_SdtContratante_Contratante_ssprestadoraunica = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PropostaRqrSrv") )
               {
                  gxTv_SdtContratante_Contratante_propostarqrsrv = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PropostaRqrPrz") )
               {
                  gxTv_SdtContratante_Contratante_propostarqrprz = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PropostaRqrEsf") )
               {
                  gxTv_SdtContratante_Contratante_propostarqresf = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PropostaRqrCnt") )
               {
                  gxTv_SdtContratante_Contratante_propostarqrcnt = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PropostaNvlCnt") )
               {
                  gxTv_SdtContratante_Contratante_propostanvlcnt = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSGeraOS") )
               {
                  gxTv_SdtContratante_Contratante_osgeraos = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSGeraTRP") )
               {
                  gxTv_SdtContratante_Contratante_osgeratrp = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSGeraTH") )
               {
                  gxTv_SdtContratante_Contratante_osgerath = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSGeraTRD") )
               {
                  gxTv_SdtContratante_Contratante_osgeratrd = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSGeraTR") )
               {
                  gxTv_SdtContratante_Contratante_osgeratr = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSHmlgComPnd") )
               {
                  gxTv_SdtContratante_Contratante_oshmlgcompnd = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_AtivoCirculante") )
               {
                  gxTv_SdtContratante_Contratante_ativocirculante = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PassivoCirculante") )
               {
                  gxTv_SdtContratante_Contratante_passivocirculante = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PatrimonioLiquido") )
               {
                  gxTv_SdtContratante_Contratante_patrimonioliquido = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_ReceitaBruta") )
               {
                  gxTv_SdtContratante_Contratante_receitabruta = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_UsaOSistema") )
               {
                  gxTv_SdtContratante_Contratante_usaosistema = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_TtlRltGerencial") )
               {
                  gxTv_SdtContratante_Contratante_ttlrltgerencial = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PrzAoRtr") )
               {
                  gxTv_SdtContratante_Contratante_przaortr = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PrzActRtr") )
               {
                  gxTv_SdtContratante_Contratante_przactrtr = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_RequerOrigem") )
               {
                  gxTv_SdtContratante_Contratante_requerorigem = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_SelecionaResponsavelOS") )
               {
                  gxTv_SdtContratante_Contratante_selecionaresponsavelos = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_ExibePF") )
               {
                  gxTv_SdtContratante_Contratante_exibepf = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Sigla") )
               {
                  gxTv_SdtContratante_Contratante_sigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContratante_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContratante_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Codigo_Z") )
               {
                  gxTv_SdtContratante_Contratante_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PessoaCod_Z") )
               {
                  gxTv_SdtContratante_Contratante_pessoacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_CNPJ_Z") )
               {
                  gxTv_SdtContratante_Contratante_cnpj_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_RazaoSocial_Z") )
               {
                  gxTv_SdtContratante_Contratante_razaosocial_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_NomeFantasia_Z") )
               {
                  gxTv_SdtContratante_Contratante_nomefantasia_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_IE_Z") )
               {
                  gxTv_SdtContratante_Contratante_ie_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_WebSite_Z") )
               {
                  gxTv_SdtContratante_Contratante_website_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Email_Z") )
               {
                  gxTv_SdtContratante_Contratante_email_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Telefone_Z") )
               {
                  gxTv_SdtContratante_Contratante_telefone_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Ramal_Z") )
               {
                  gxTv_SdtContratante_Contratante_ramal_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Fax_Z") )
               {
                  gxTv_SdtContratante_Contratante_fax_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_AgenciaNome_Z") )
               {
                  gxTv_SdtContratante_Contratante_agencianome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_AgenciaNro_Z") )
               {
                  gxTv_SdtContratante_Contratante_agencianro_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_BancoNome_Z") )
               {
                  gxTv_SdtContratante_Contratante_banconome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_BancoNro_Z") )
               {
                  gxTv_SdtContratante_Contratante_banconro_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_ContaCorrente_Z") )
               {
                  gxTv_SdtContratante_Contratante_contacorrente_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Municipio_Codigo_Z") )
               {
                  gxTv_SdtContratante_Municipio_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Municipio_Nome_Z") )
               {
                  gxTv_SdtContratante_Municipio_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Estado_UF_Z") )
               {
                  gxTv_SdtContratante_Estado_uf_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Ativo_Z") )
               {
                  gxTv_SdtContratante_Contratante_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaHost_Z") )
               {
                  gxTv_SdtContratante_Contratante_emailsdahost_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaUser_Z") )
               {
                  gxTv_SdtContratante_Contratante_emailsdauser_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaPass_Z") )
               {
                  gxTv_SdtContratante_Contratante_emailsdapass_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaKey_Z") )
               {
                  gxTv_SdtContratante_Contratante_emailsdakey_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaAut_Z") )
               {
                  gxTv_SdtContratante_Contratante_emailsdaaut_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaPort_Z") )
               {
                  gxTv_SdtContratante_Contratante_emailsdaport_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaSec_Z") )
               {
                  gxTv_SdtContratante_Contratante_emailsdasec_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSAutomatica_Z") )
               {
                  gxTv_SdtContratante_Contratante_osautomatica_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_SSAutomatica_Z") )
               {
                  gxTv_SdtContratante_Contratante_ssautomatica_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_ExisteConferencia_Z") )
               {
                  gxTv_SdtContratante_Contratante_existeconferencia_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_LogoNomeArq_Z") )
               {
                  gxTv_SdtContratante_Contratante_logonomearq_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_LogoTipoArq_Z") )
               {
                  gxTv_SdtContratante_Contratante_logotipoarq_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_InicioDoExpediente_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContratante_Contratante_iniciodoexpediente_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContratante_Contratante_iniciodoexpediente_Z = DateTimeUtil.ResetDate(context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), "."))));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_FimDoExpediente_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContratante_Contratante_fimdoexpediente_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContratante_Contratante_fimdoexpediente_Z = DateTimeUtil.ResetDate(context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), "."))));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_ServicoSSPadrao_Z") )
               {
                  gxTv_SdtContratante_Contratante_servicosspadrao_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_RetornaSS_Z") )
               {
                  gxTv_SdtContratante_Contratante_retornass_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_SSStatusPlanejamento_Z") )
               {
                  gxTv_SdtContratante_Contratante_ssstatusplanejamento_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_SSPrestadoraUnica_Z") )
               {
                  gxTv_SdtContratante_Contratante_ssprestadoraunica_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PropostaRqrSrv_Z") )
               {
                  gxTv_SdtContratante_Contratante_propostarqrsrv_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PropostaRqrPrz_Z") )
               {
                  gxTv_SdtContratante_Contratante_propostarqrprz_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PropostaRqrEsf_Z") )
               {
                  gxTv_SdtContratante_Contratante_propostarqresf_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PropostaRqrCnt_Z") )
               {
                  gxTv_SdtContratante_Contratante_propostarqrcnt_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PropostaNvlCnt_Z") )
               {
                  gxTv_SdtContratante_Contratante_propostanvlcnt_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSGeraOS_Z") )
               {
                  gxTv_SdtContratante_Contratante_osgeraos_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSGeraTRP_Z") )
               {
                  gxTv_SdtContratante_Contratante_osgeratrp_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSGeraTH_Z") )
               {
                  gxTv_SdtContratante_Contratante_osgerath_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSGeraTRD_Z") )
               {
                  gxTv_SdtContratante_Contratante_osgeratrd_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSGeraTR_Z") )
               {
                  gxTv_SdtContratante_Contratante_osgeratr_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSHmlgComPnd_Z") )
               {
                  gxTv_SdtContratante_Contratante_oshmlgcompnd_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_AtivoCirculante_Z") )
               {
                  gxTv_SdtContratante_Contratante_ativocirculante_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PassivoCirculante_Z") )
               {
                  gxTv_SdtContratante_Contratante_passivocirculante_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PatrimonioLiquido_Z") )
               {
                  gxTv_SdtContratante_Contratante_patrimonioliquido_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_ReceitaBruta_Z") )
               {
                  gxTv_SdtContratante_Contratante_receitabruta_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_UsaOSistema_Z") )
               {
                  gxTv_SdtContratante_Contratante_usaosistema_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_TtlRltGerencial_Z") )
               {
                  gxTv_SdtContratante_Contratante_ttlrltgerencial_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PrzAoRtr_Z") )
               {
                  gxTv_SdtContratante_Contratante_przaortr_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PrzActRtr_Z") )
               {
                  gxTv_SdtContratante_Contratante_przactrtr_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_RequerOrigem_Z") )
               {
                  gxTv_SdtContratante_Contratante_requerorigem_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_SelecionaResponsavelOS_Z") )
               {
                  gxTv_SdtContratante_Contratante_selecionaresponsavelos_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_ExibePF_Z") )
               {
                  gxTv_SdtContratante_Contratante_exibepf_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Sigla_Z") )
               {
                  gxTv_SdtContratante_Contratante_sigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Codigo_N") )
               {
                  gxTv_SdtContratante_Contratante_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_CNPJ_N") )
               {
                  gxTv_SdtContratante_Contratante_cnpj_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_RazaoSocial_N") )
               {
                  gxTv_SdtContratante_Contratante_razaosocial_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_WebSite_N") )
               {
                  gxTv_SdtContratante_Contratante_website_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Email_N") )
               {
                  gxTv_SdtContratante_Contratante_email_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Ramal_N") )
               {
                  gxTv_SdtContratante_Contratante_ramal_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Fax_N") )
               {
                  gxTv_SdtContratante_Contratante_fax_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_AgenciaNome_N") )
               {
                  gxTv_SdtContratante_Contratante_agencianome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_AgenciaNro_N") )
               {
                  gxTv_SdtContratante_Contratante_agencianro_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_BancoNome_N") )
               {
                  gxTv_SdtContratante_Contratante_banconome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_BancoNro_N") )
               {
                  gxTv_SdtContratante_Contratante_banconro_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_ContaCorrente_N") )
               {
                  gxTv_SdtContratante_Contratante_contacorrente_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Municipio_Codigo_N") )
               {
                  gxTv_SdtContratante_Municipio_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaHost_N") )
               {
                  gxTv_SdtContratante_Contratante_emailsdahost_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaUser_N") )
               {
                  gxTv_SdtContratante_Contratante_emailsdauser_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaPass_N") )
               {
                  gxTv_SdtContratante_Contratante_emailsdapass_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaKey_N") )
               {
                  gxTv_SdtContratante_Contratante_emailsdakey_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaAut_N") )
               {
                  gxTv_SdtContratante_Contratante_emailsdaaut_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaPort_N") )
               {
                  gxTv_SdtContratante_Contratante_emailsdaport_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaSec_N") )
               {
                  gxTv_SdtContratante_Contratante_emailsdasec_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_SSAutomatica_N") )
               {
                  gxTv_SdtContratante_Contratante_ssautomatica_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_LogoArquivo_N") )
               {
                  gxTv_SdtContratante_Contratante_logoarquivo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_LogoNomeArq_N") )
               {
                  gxTv_SdtContratante_Contratante_logonomearq_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_LogoTipoArq_N") )
               {
                  gxTv_SdtContratante_Contratante_logotipoarq_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_InicioDoExpediente_N") )
               {
                  gxTv_SdtContratante_Contratante_iniciodoexpediente_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_FimDoExpediente_N") )
               {
                  gxTv_SdtContratante_Contratante_fimdoexpediente_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_ServicoSSPadrao_N") )
               {
                  gxTv_SdtContratante_Contratante_servicosspadrao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_RetornaSS_N") )
               {
                  gxTv_SdtContratante_Contratante_retornass_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_SSStatusPlanejamento_N") )
               {
                  gxTv_SdtContratante_Contratante_ssstatusplanejamento_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_SSPrestadoraUnica_N") )
               {
                  gxTv_SdtContratante_Contratante_ssprestadoraunica_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PropostaRqrSrv_N") )
               {
                  gxTv_SdtContratante_Contratante_propostarqrsrv_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PropostaRqrPrz_N") )
               {
                  gxTv_SdtContratante_Contratante_propostarqrprz_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PropostaRqrEsf_N") )
               {
                  gxTv_SdtContratante_Contratante_propostarqresf_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PropostaRqrCnt_N") )
               {
                  gxTv_SdtContratante_Contratante_propostarqrcnt_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PropostaNvlCnt_N") )
               {
                  gxTv_SdtContratante_Contratante_propostanvlcnt_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSGeraOS_N") )
               {
                  gxTv_SdtContratante_Contratante_osgeraos_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSGeraTRP_N") )
               {
                  gxTv_SdtContratante_Contratante_osgeratrp_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSGeraTH_N") )
               {
                  gxTv_SdtContratante_Contratante_osgerath_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSGeraTRD_N") )
               {
                  gxTv_SdtContratante_Contratante_osgeratrd_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSGeraTR_N") )
               {
                  gxTv_SdtContratante_Contratante_osgeratr_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_OSHmlgComPnd_N") )
               {
                  gxTv_SdtContratante_Contratante_oshmlgcompnd_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_AtivoCirculante_N") )
               {
                  gxTv_SdtContratante_Contratante_ativocirculante_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PassivoCirculante_N") )
               {
                  gxTv_SdtContratante_Contratante_passivocirculante_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PatrimonioLiquido_N") )
               {
                  gxTv_SdtContratante_Contratante_patrimonioliquido_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_ReceitaBruta_N") )
               {
                  gxTv_SdtContratante_Contratante_receitabruta_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_UsaOSistema_N") )
               {
                  gxTv_SdtContratante_Contratante_usaosistema_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_TtlRltGerencial_N") )
               {
                  gxTv_SdtContratante_Contratante_ttlrltgerencial_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PrzAoRtr_N") )
               {
                  gxTv_SdtContratante_Contratante_przaortr_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PrzActRtr_N") )
               {
                  gxTv_SdtContratante_Contratante_przactrtr_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_RequerOrigem_N") )
               {
                  gxTv_SdtContratante_Contratante_requerorigem_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Sigla_N") )
               {
                  gxTv_SdtContratante_Contratante_sigla_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Contratante";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Contratante_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_PessoaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_pessoacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_CNPJ", StringUtil.RTrim( gxTv_SdtContratante_Contratante_cnpj));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_RazaoSocial", StringUtil.RTrim( gxTv_SdtContratante_Contratante_razaosocial));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_NomeFantasia", StringUtil.RTrim( gxTv_SdtContratante_Contratante_nomefantasia));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_IE", StringUtil.RTrim( gxTv_SdtContratante_Contratante_ie));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_WebSite", StringUtil.RTrim( gxTv_SdtContratante_Contratante_website));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_Email", StringUtil.RTrim( gxTv_SdtContratante_Contratante_email));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_Telefone", StringUtil.RTrim( gxTv_SdtContratante_Contratante_telefone));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_Ramal", StringUtil.RTrim( gxTv_SdtContratante_Contratante_ramal));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_Fax", StringUtil.RTrim( gxTv_SdtContratante_Contratante_fax));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_AgenciaNome", StringUtil.RTrim( gxTv_SdtContratante_Contratante_agencianome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_AgenciaNro", StringUtil.RTrim( gxTv_SdtContratante_Contratante_agencianro));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_BancoNome", StringUtil.RTrim( gxTv_SdtContratante_Contratante_banconome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_BancoNro", StringUtil.RTrim( gxTv_SdtContratante_Contratante_banconro));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_ContaCorrente", StringUtil.RTrim( gxTv_SdtContratante_Contratante_contacorrente));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Municipio_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Municipio_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Municipio_Nome", StringUtil.RTrim( gxTv_SdtContratante_Municipio_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Estado_UF", StringUtil.RTrim( gxTv_SdtContratante_Estado_uf));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_EmailSdaHost", StringUtil.RTrim( gxTv_SdtContratante_Contratante_emailsdahost));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_EmailSdaUser", StringUtil.RTrim( gxTv_SdtContratante_Contratante_emailsdauser));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_EmailSdaPass", StringUtil.RTrim( gxTv_SdtContratante_Contratante_emailsdapass));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_EmailSdaKey", StringUtil.RTrim( gxTv_SdtContratante_Contratante_emailsdakey));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_EmailSdaAut", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_emailsdaaut)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_EmailSdaPort", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_emailsdaport), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_EmailSdaSec", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_emailsdasec), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_OSAutomatica", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_osautomatica)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_SSAutomatica", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_ssautomatica)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_ExisteConferencia", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_existeconferencia)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_LogoArquivo", context.FileToBase64( gxTv_SdtContratante_Contratante_logoarquivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_LogoNomeArq", StringUtil.RTrim( gxTv_SdtContratante_Contratante_logonomearq));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_LogoTipoArq", StringUtil.RTrim( gxTv_SdtContratante_Contratante_logotipoarq));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtContratante_Contratante_iniciodoexpediente) )
         {
            oWriter.WriteStartElement("Contratante_InicioDoExpediente");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContratante_Contratante_iniciodoexpediente)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContratante_Contratante_iniciodoexpediente)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContratante_Contratante_iniciodoexpediente)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContratante_Contratante_iniciodoexpediente)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContratante_Contratante_iniciodoexpediente)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContratante_Contratante_iniciodoexpediente)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Contratante_InicioDoExpediente", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtContratante_Contratante_fimdoexpediente) )
         {
            oWriter.WriteStartElement("Contratante_FimDoExpediente");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContratante_Contratante_fimdoexpediente)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContratante_Contratante_fimdoexpediente)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContratante_Contratante_fimdoexpediente)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContratante_Contratante_fimdoexpediente)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContratante_Contratante_fimdoexpediente)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContratante_Contratante_fimdoexpediente)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Contratante_FimDoExpediente", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("Contratante_ServicoSSPadrao", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_servicosspadrao), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_RetornaSS", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_retornass)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_SSStatusPlanejamento", StringUtil.RTrim( gxTv_SdtContratante_Contratante_ssstatusplanejamento));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_SSPrestadoraUnica", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_ssprestadoraunica)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_PropostaRqrSrv", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_propostarqrsrv)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_PropostaRqrPrz", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_propostarqrprz)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_PropostaRqrEsf", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_propostarqresf)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_PropostaRqrCnt", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_propostarqrcnt)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_PropostaNvlCnt", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_propostanvlcnt), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_OSGeraOS", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_osgeraos)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_OSGeraTRP", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_osgeratrp)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_OSGeraTH", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_osgerath)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_OSGeraTRD", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_osgeratrd)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_OSGeraTR", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_osgeratr)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_OSHmlgComPnd", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_oshmlgcompnd)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_AtivoCirculante", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratante_Contratante_ativocirculante, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_PassivoCirculante", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratante_Contratante_passivocirculante, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_PatrimonioLiquido", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratante_Contratante_patrimonioliquido, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_ReceitaBruta", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratante_Contratante_receitabruta, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_UsaOSistema", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_usaosistema)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_TtlRltGerencial", StringUtil.RTrim( gxTv_SdtContratante_Contratante_ttlrltgerencial));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_PrzAoRtr", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_przaortr), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_PrzActRtr", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_przactrtr), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_RequerOrigem", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_requerorigem)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_SelecionaResponsavelOS", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_selecionaresponsavelos)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_ExibePF", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_exibepf)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratante_Sigla", StringUtil.RTrim( gxTv_SdtContratante_Contratante_sigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContratante_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PessoaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_pessoacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_CNPJ_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_cnpj_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_RazaoSocial_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_razaosocial_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_NomeFantasia_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_nomefantasia_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_IE_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_ie_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_WebSite_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_website_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_Email_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_email_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_Telefone_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_telefone_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_Ramal_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_ramal_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_Fax_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_fax_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_AgenciaNome_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_agencianome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_AgenciaNro_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_agencianro_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_BancoNome_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_banconome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_BancoNro_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_banconro_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_ContaCorrente_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_contacorrente_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Municipio_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Municipio_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Municipio_Nome_Z", StringUtil.RTrim( gxTv_SdtContratante_Municipio_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Estado_UF_Z", StringUtil.RTrim( gxTv_SdtContratante_Estado_uf_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_EmailSdaHost_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_emailsdahost_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_EmailSdaUser_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_emailsdauser_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_EmailSdaPass_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_emailsdapass_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_EmailSdaKey_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_emailsdakey_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_EmailSdaAut_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_emailsdaaut_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_EmailSdaPort_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_emailsdaport_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_EmailSdaSec_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_emailsdasec_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_OSAutomatica_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_osautomatica_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_SSAutomatica_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_ssautomatica_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_ExisteConferencia_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_existeconferencia_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_LogoNomeArq_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_logonomearq_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_LogoTipoArq_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_logotipoarq_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtContratante_Contratante_iniciodoexpediente_Z) )
            {
               oWriter.WriteStartElement("Contratante_InicioDoExpediente_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContratante_Contratante_iniciodoexpediente_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContratante_Contratante_iniciodoexpediente_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContratante_Contratante_iniciodoexpediente_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContratante_Contratante_iniciodoexpediente_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContratante_Contratante_iniciodoexpediente_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContratante_Contratante_iniciodoexpediente_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Contratante_InicioDoExpediente_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            if ( (DateTime.MinValue==gxTv_SdtContratante_Contratante_fimdoexpediente_Z) )
            {
               oWriter.WriteStartElement("Contratante_FimDoExpediente_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContratante_Contratante_fimdoexpediente_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContratante_Contratante_fimdoexpediente_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContratante_Contratante_fimdoexpediente_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContratante_Contratante_fimdoexpediente_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContratante_Contratante_fimdoexpediente_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContratante_Contratante_fimdoexpediente_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Contratante_FimDoExpediente_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("Contratante_ServicoSSPadrao_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_servicosspadrao_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_RetornaSS_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_retornass_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_SSStatusPlanejamento_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_ssstatusplanejamento_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_SSPrestadoraUnica_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_ssprestadoraunica_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PropostaRqrSrv_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_propostarqrsrv_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PropostaRqrPrz_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_propostarqrprz_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PropostaRqrEsf_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_propostarqresf_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PropostaRqrCnt_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_propostarqrcnt_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PropostaNvlCnt_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_propostanvlcnt_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_OSGeraOS_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_osgeraos_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_OSGeraTRP_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_osgeratrp_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_OSGeraTH_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_osgerath_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_OSGeraTRD_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_osgeratrd_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_OSGeraTR_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_osgeratr_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_OSHmlgComPnd_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_oshmlgcompnd_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_AtivoCirculante_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratante_Contratante_ativocirculante_Z, 18, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PassivoCirculante_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratante_Contratante_passivocirculante_Z, 18, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PatrimonioLiquido_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratante_Contratante_patrimonioliquido_Z, 18, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_ReceitaBruta_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratante_Contratante_receitabruta_Z, 18, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_UsaOSistema_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_usaosistema_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_TtlRltGerencial_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_ttlrltgerencial_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PrzAoRtr_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_przaortr_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PrzActRtr_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_przactrtr_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_RequerOrigem_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_requerorigem_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_SelecionaResponsavelOS_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_selecionaresponsavelos_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_ExibePF_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratante_Contratante_exibepf_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_Sigla_Z", StringUtil.RTrim( gxTv_SdtContratante_Contratante_sigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_CNPJ_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_cnpj_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_RazaoSocial_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_razaosocial_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_WebSite_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_website_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_Email_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_email_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_Ramal_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_ramal_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_Fax_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_fax_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_AgenciaNome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_agencianome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_AgenciaNro_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_agencianro_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_BancoNome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_banconome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_BancoNro_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_banconro_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_ContaCorrente_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_contacorrente_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Municipio_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Municipio_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_EmailSdaHost_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_emailsdahost_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_EmailSdaUser_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_emailsdauser_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_EmailSdaPass_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_emailsdapass_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_EmailSdaKey_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_emailsdakey_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_EmailSdaAut_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_emailsdaaut_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_EmailSdaPort_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_emailsdaport_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_EmailSdaSec_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_emailsdasec_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_SSAutomatica_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_ssautomatica_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_LogoArquivo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_logoarquivo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_LogoNomeArq_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_logonomearq_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_LogoTipoArq_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_logotipoarq_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_InicioDoExpediente_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_iniciodoexpediente_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_FimDoExpediente_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_fimdoexpediente_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_ServicoSSPadrao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_servicosspadrao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_RetornaSS_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_retornass_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_SSStatusPlanejamento_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_ssstatusplanejamento_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_SSPrestadoraUnica_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_ssprestadoraunica_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PropostaRqrSrv_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_propostarqrsrv_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PropostaRqrPrz_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_propostarqrprz_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PropostaRqrEsf_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_propostarqresf_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PropostaRqrCnt_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_propostarqrcnt_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PropostaNvlCnt_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_propostanvlcnt_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_OSGeraOS_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_osgeraos_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_OSGeraTRP_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_osgeratrp_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_OSGeraTH_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_osgerath_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_OSGeraTRD_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_osgeratrd_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_OSGeraTR_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_osgeratr_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_OSHmlgComPnd_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_oshmlgcompnd_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_AtivoCirculante_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_ativocirculante_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PassivoCirculante_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_passivocirculante_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PatrimonioLiquido_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_patrimonioliquido_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_ReceitaBruta_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_receitabruta_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_UsaOSistema_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_usaosistema_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_TtlRltGerencial_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_ttlrltgerencial_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PrzAoRtr_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_przaortr_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_PrzActRtr_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_przactrtr_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_RequerOrigem_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_requerorigem_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratante_Sigla_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratante_Contratante_sigla_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Contratante_Codigo", gxTv_SdtContratante_Contratante_codigo, false);
         AddObjectProperty("Contratante_PessoaCod", gxTv_SdtContratante_Contratante_pessoacod, false);
         AddObjectProperty("Contratante_CNPJ", gxTv_SdtContratante_Contratante_cnpj, false);
         AddObjectProperty("Contratante_RazaoSocial", gxTv_SdtContratante_Contratante_razaosocial, false);
         AddObjectProperty("Contratante_NomeFantasia", gxTv_SdtContratante_Contratante_nomefantasia, false);
         AddObjectProperty("Contratante_IE", gxTv_SdtContratante_Contratante_ie, false);
         AddObjectProperty("Contratante_WebSite", gxTv_SdtContratante_Contratante_website, false);
         AddObjectProperty("Contratante_Email", gxTv_SdtContratante_Contratante_email, false);
         AddObjectProperty("Contratante_Telefone", gxTv_SdtContratante_Contratante_telefone, false);
         AddObjectProperty("Contratante_Ramal", gxTv_SdtContratante_Contratante_ramal, false);
         AddObjectProperty("Contratante_Fax", gxTv_SdtContratante_Contratante_fax, false);
         AddObjectProperty("Contratante_AgenciaNome", gxTv_SdtContratante_Contratante_agencianome, false);
         AddObjectProperty("Contratante_AgenciaNro", gxTv_SdtContratante_Contratante_agencianro, false);
         AddObjectProperty("Contratante_BancoNome", gxTv_SdtContratante_Contratante_banconome, false);
         AddObjectProperty("Contratante_BancoNro", gxTv_SdtContratante_Contratante_banconro, false);
         AddObjectProperty("Contratante_ContaCorrente", gxTv_SdtContratante_Contratante_contacorrente, false);
         AddObjectProperty("Municipio_Codigo", gxTv_SdtContratante_Municipio_codigo, false);
         AddObjectProperty("Municipio_Nome", gxTv_SdtContratante_Municipio_nome, false);
         AddObjectProperty("Estado_UF", gxTv_SdtContratante_Estado_uf, false);
         AddObjectProperty("Contratante_Ativo", gxTv_SdtContratante_Contratante_ativo, false);
         AddObjectProperty("Contratante_EmailSdaHost", gxTv_SdtContratante_Contratante_emailsdahost, false);
         AddObjectProperty("Contratante_EmailSdaUser", gxTv_SdtContratante_Contratante_emailsdauser, false);
         AddObjectProperty("Contratante_EmailSdaPass", gxTv_SdtContratante_Contratante_emailsdapass, false);
         AddObjectProperty("Contratante_EmailSdaKey", gxTv_SdtContratante_Contratante_emailsdakey, false);
         AddObjectProperty("Contratante_EmailSdaAut", gxTv_SdtContratante_Contratante_emailsdaaut, false);
         AddObjectProperty("Contratante_EmailSdaPort", gxTv_SdtContratante_Contratante_emailsdaport, false);
         AddObjectProperty("Contratante_EmailSdaSec", gxTv_SdtContratante_Contratante_emailsdasec, false);
         AddObjectProperty("Contratante_OSAutomatica", gxTv_SdtContratante_Contratante_osautomatica, false);
         AddObjectProperty("Contratante_SSAutomatica", gxTv_SdtContratante_Contratante_ssautomatica, false);
         AddObjectProperty("Contratante_ExisteConferencia", gxTv_SdtContratante_Contratante_existeconferencia, false);
         AddObjectProperty("Contratante_LogoArquivo", gxTv_SdtContratante_Contratante_logoarquivo, false);
         AddObjectProperty("Contratante_LogoNomeArq", gxTv_SdtContratante_Contratante_logonomearq, false);
         AddObjectProperty("Contratante_LogoTipoArq", gxTv_SdtContratante_Contratante_logotipoarq, false);
         datetime_STZ = gxTv_SdtContratante_Contratante_iniciodoexpediente;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Contratante_InicioDoExpediente", sDateCnv, false);
         datetime_STZ = gxTv_SdtContratante_Contratante_fimdoexpediente;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Contratante_FimDoExpediente", sDateCnv, false);
         AddObjectProperty("Contratante_ServicoSSPadrao", gxTv_SdtContratante_Contratante_servicosspadrao, false);
         AddObjectProperty("Contratante_RetornaSS", gxTv_SdtContratante_Contratante_retornass, false);
         AddObjectProperty("Contratante_SSStatusPlanejamento", gxTv_SdtContratante_Contratante_ssstatusplanejamento, false);
         AddObjectProperty("Contratante_SSPrestadoraUnica", gxTv_SdtContratante_Contratante_ssprestadoraunica, false);
         AddObjectProperty("Contratante_PropostaRqrSrv", gxTv_SdtContratante_Contratante_propostarqrsrv, false);
         AddObjectProperty("Contratante_PropostaRqrPrz", gxTv_SdtContratante_Contratante_propostarqrprz, false);
         AddObjectProperty("Contratante_PropostaRqrEsf", gxTv_SdtContratante_Contratante_propostarqresf, false);
         AddObjectProperty("Contratante_PropostaRqrCnt", gxTv_SdtContratante_Contratante_propostarqrcnt, false);
         AddObjectProperty("Contratante_PropostaNvlCnt", gxTv_SdtContratante_Contratante_propostanvlcnt, false);
         AddObjectProperty("Contratante_OSGeraOS", gxTv_SdtContratante_Contratante_osgeraos, false);
         AddObjectProperty("Contratante_OSGeraTRP", gxTv_SdtContratante_Contratante_osgeratrp, false);
         AddObjectProperty("Contratante_OSGeraTH", gxTv_SdtContratante_Contratante_osgerath, false);
         AddObjectProperty("Contratante_OSGeraTRD", gxTv_SdtContratante_Contratante_osgeratrd, false);
         AddObjectProperty("Contratante_OSGeraTR", gxTv_SdtContratante_Contratante_osgeratr, false);
         AddObjectProperty("Contratante_OSHmlgComPnd", gxTv_SdtContratante_Contratante_oshmlgcompnd, false);
         AddObjectProperty("Contratante_AtivoCirculante", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContratante_Contratante_ativocirculante, 18, 5)), false);
         AddObjectProperty("Contratante_PassivoCirculante", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContratante_Contratante_passivocirculante, 18, 5)), false);
         AddObjectProperty("Contratante_PatrimonioLiquido", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContratante_Contratante_patrimonioliquido, 18, 5)), false);
         AddObjectProperty("Contratante_ReceitaBruta", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContratante_Contratante_receitabruta, 18, 5)), false);
         AddObjectProperty("Contratante_UsaOSistema", gxTv_SdtContratante_Contratante_usaosistema, false);
         AddObjectProperty("Contratante_TtlRltGerencial", gxTv_SdtContratante_Contratante_ttlrltgerencial, false);
         AddObjectProperty("Contratante_PrzAoRtr", gxTv_SdtContratante_Contratante_przaortr, false);
         AddObjectProperty("Contratante_PrzActRtr", gxTv_SdtContratante_Contratante_przactrtr, false);
         AddObjectProperty("Contratante_RequerOrigem", gxTv_SdtContratante_Contratante_requerorigem, false);
         AddObjectProperty("Contratante_SelecionaResponsavelOS", gxTv_SdtContratante_Contratante_selecionaresponsavelos, false);
         AddObjectProperty("Contratante_ExibePF", gxTv_SdtContratante_Contratante_exibepf, false);
         AddObjectProperty("Contratante_Sigla", gxTv_SdtContratante_Contratante_sigla, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContratante_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContratante_Initialized, false);
            AddObjectProperty("Contratante_Codigo_Z", gxTv_SdtContratante_Contratante_codigo_Z, false);
            AddObjectProperty("Contratante_PessoaCod_Z", gxTv_SdtContratante_Contratante_pessoacod_Z, false);
            AddObjectProperty("Contratante_CNPJ_Z", gxTv_SdtContratante_Contratante_cnpj_Z, false);
            AddObjectProperty("Contratante_RazaoSocial_Z", gxTv_SdtContratante_Contratante_razaosocial_Z, false);
            AddObjectProperty("Contratante_NomeFantasia_Z", gxTv_SdtContratante_Contratante_nomefantasia_Z, false);
            AddObjectProperty("Contratante_IE_Z", gxTv_SdtContratante_Contratante_ie_Z, false);
            AddObjectProperty("Contratante_WebSite_Z", gxTv_SdtContratante_Contratante_website_Z, false);
            AddObjectProperty("Contratante_Email_Z", gxTv_SdtContratante_Contratante_email_Z, false);
            AddObjectProperty("Contratante_Telefone_Z", gxTv_SdtContratante_Contratante_telefone_Z, false);
            AddObjectProperty("Contratante_Ramal_Z", gxTv_SdtContratante_Contratante_ramal_Z, false);
            AddObjectProperty("Contratante_Fax_Z", gxTv_SdtContratante_Contratante_fax_Z, false);
            AddObjectProperty("Contratante_AgenciaNome_Z", gxTv_SdtContratante_Contratante_agencianome_Z, false);
            AddObjectProperty("Contratante_AgenciaNro_Z", gxTv_SdtContratante_Contratante_agencianro_Z, false);
            AddObjectProperty("Contratante_BancoNome_Z", gxTv_SdtContratante_Contratante_banconome_Z, false);
            AddObjectProperty("Contratante_BancoNro_Z", gxTv_SdtContratante_Contratante_banconro_Z, false);
            AddObjectProperty("Contratante_ContaCorrente_Z", gxTv_SdtContratante_Contratante_contacorrente_Z, false);
            AddObjectProperty("Municipio_Codigo_Z", gxTv_SdtContratante_Municipio_codigo_Z, false);
            AddObjectProperty("Municipio_Nome_Z", gxTv_SdtContratante_Municipio_nome_Z, false);
            AddObjectProperty("Estado_UF_Z", gxTv_SdtContratante_Estado_uf_Z, false);
            AddObjectProperty("Contratante_Ativo_Z", gxTv_SdtContratante_Contratante_ativo_Z, false);
            AddObjectProperty("Contratante_EmailSdaHost_Z", gxTv_SdtContratante_Contratante_emailsdahost_Z, false);
            AddObjectProperty("Contratante_EmailSdaUser_Z", gxTv_SdtContratante_Contratante_emailsdauser_Z, false);
            AddObjectProperty("Contratante_EmailSdaPass_Z", gxTv_SdtContratante_Contratante_emailsdapass_Z, false);
            AddObjectProperty("Contratante_EmailSdaKey_Z", gxTv_SdtContratante_Contratante_emailsdakey_Z, false);
            AddObjectProperty("Contratante_EmailSdaAut_Z", gxTv_SdtContratante_Contratante_emailsdaaut_Z, false);
            AddObjectProperty("Contratante_EmailSdaPort_Z", gxTv_SdtContratante_Contratante_emailsdaport_Z, false);
            AddObjectProperty("Contratante_EmailSdaSec_Z", gxTv_SdtContratante_Contratante_emailsdasec_Z, false);
            AddObjectProperty("Contratante_OSAutomatica_Z", gxTv_SdtContratante_Contratante_osautomatica_Z, false);
            AddObjectProperty("Contratante_SSAutomatica_Z", gxTv_SdtContratante_Contratante_ssautomatica_Z, false);
            AddObjectProperty("Contratante_ExisteConferencia_Z", gxTv_SdtContratante_Contratante_existeconferencia_Z, false);
            AddObjectProperty("Contratante_LogoNomeArq_Z", gxTv_SdtContratante_Contratante_logonomearq_Z, false);
            AddObjectProperty("Contratante_LogoTipoArq_Z", gxTv_SdtContratante_Contratante_logotipoarq_Z, false);
            datetime_STZ = gxTv_SdtContratante_Contratante_iniciodoexpediente_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Contratante_InicioDoExpediente_Z", sDateCnv, false);
            datetime_STZ = gxTv_SdtContratante_Contratante_fimdoexpediente_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Contratante_FimDoExpediente_Z", sDateCnv, false);
            AddObjectProperty("Contratante_ServicoSSPadrao_Z", gxTv_SdtContratante_Contratante_servicosspadrao_Z, false);
            AddObjectProperty("Contratante_RetornaSS_Z", gxTv_SdtContratante_Contratante_retornass_Z, false);
            AddObjectProperty("Contratante_SSStatusPlanejamento_Z", gxTv_SdtContratante_Contratante_ssstatusplanejamento_Z, false);
            AddObjectProperty("Contratante_SSPrestadoraUnica_Z", gxTv_SdtContratante_Contratante_ssprestadoraunica_Z, false);
            AddObjectProperty("Contratante_PropostaRqrSrv_Z", gxTv_SdtContratante_Contratante_propostarqrsrv_Z, false);
            AddObjectProperty("Contratante_PropostaRqrPrz_Z", gxTv_SdtContratante_Contratante_propostarqrprz_Z, false);
            AddObjectProperty("Contratante_PropostaRqrEsf_Z", gxTv_SdtContratante_Contratante_propostarqresf_Z, false);
            AddObjectProperty("Contratante_PropostaRqrCnt_Z", gxTv_SdtContratante_Contratante_propostarqrcnt_Z, false);
            AddObjectProperty("Contratante_PropostaNvlCnt_Z", gxTv_SdtContratante_Contratante_propostanvlcnt_Z, false);
            AddObjectProperty("Contratante_OSGeraOS_Z", gxTv_SdtContratante_Contratante_osgeraos_Z, false);
            AddObjectProperty("Contratante_OSGeraTRP_Z", gxTv_SdtContratante_Contratante_osgeratrp_Z, false);
            AddObjectProperty("Contratante_OSGeraTH_Z", gxTv_SdtContratante_Contratante_osgerath_Z, false);
            AddObjectProperty("Contratante_OSGeraTRD_Z", gxTv_SdtContratante_Contratante_osgeratrd_Z, false);
            AddObjectProperty("Contratante_OSGeraTR_Z", gxTv_SdtContratante_Contratante_osgeratr_Z, false);
            AddObjectProperty("Contratante_OSHmlgComPnd_Z", gxTv_SdtContratante_Contratante_oshmlgcompnd_Z, false);
            AddObjectProperty("Contratante_AtivoCirculante_Z", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContratante_Contratante_ativocirculante_Z, 18, 5)), false);
            AddObjectProperty("Contratante_PassivoCirculante_Z", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContratante_Contratante_passivocirculante_Z, 18, 5)), false);
            AddObjectProperty("Contratante_PatrimonioLiquido_Z", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContratante_Contratante_patrimonioliquido_Z, 18, 5)), false);
            AddObjectProperty("Contratante_ReceitaBruta_Z", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContratante_Contratante_receitabruta_Z, 18, 5)), false);
            AddObjectProperty("Contratante_UsaOSistema_Z", gxTv_SdtContratante_Contratante_usaosistema_Z, false);
            AddObjectProperty("Contratante_TtlRltGerencial_Z", gxTv_SdtContratante_Contratante_ttlrltgerencial_Z, false);
            AddObjectProperty("Contratante_PrzAoRtr_Z", gxTv_SdtContratante_Contratante_przaortr_Z, false);
            AddObjectProperty("Contratante_PrzActRtr_Z", gxTv_SdtContratante_Contratante_przactrtr_Z, false);
            AddObjectProperty("Contratante_RequerOrigem_Z", gxTv_SdtContratante_Contratante_requerorigem_Z, false);
            AddObjectProperty("Contratante_SelecionaResponsavelOS_Z", gxTv_SdtContratante_Contratante_selecionaresponsavelos_Z, false);
            AddObjectProperty("Contratante_ExibePF_Z", gxTv_SdtContratante_Contratante_exibepf_Z, false);
            AddObjectProperty("Contratante_Sigla_Z", gxTv_SdtContratante_Contratante_sigla_Z, false);
            AddObjectProperty("Contratante_Codigo_N", gxTv_SdtContratante_Contratante_codigo_N, false);
            AddObjectProperty("Contratante_CNPJ_N", gxTv_SdtContratante_Contratante_cnpj_N, false);
            AddObjectProperty("Contratante_RazaoSocial_N", gxTv_SdtContratante_Contratante_razaosocial_N, false);
            AddObjectProperty("Contratante_WebSite_N", gxTv_SdtContratante_Contratante_website_N, false);
            AddObjectProperty("Contratante_Email_N", gxTv_SdtContratante_Contratante_email_N, false);
            AddObjectProperty("Contratante_Ramal_N", gxTv_SdtContratante_Contratante_ramal_N, false);
            AddObjectProperty("Contratante_Fax_N", gxTv_SdtContratante_Contratante_fax_N, false);
            AddObjectProperty("Contratante_AgenciaNome_N", gxTv_SdtContratante_Contratante_agencianome_N, false);
            AddObjectProperty("Contratante_AgenciaNro_N", gxTv_SdtContratante_Contratante_agencianro_N, false);
            AddObjectProperty("Contratante_BancoNome_N", gxTv_SdtContratante_Contratante_banconome_N, false);
            AddObjectProperty("Contratante_BancoNro_N", gxTv_SdtContratante_Contratante_banconro_N, false);
            AddObjectProperty("Contratante_ContaCorrente_N", gxTv_SdtContratante_Contratante_contacorrente_N, false);
            AddObjectProperty("Municipio_Codigo_N", gxTv_SdtContratante_Municipio_codigo_N, false);
            AddObjectProperty("Contratante_EmailSdaHost_N", gxTv_SdtContratante_Contratante_emailsdahost_N, false);
            AddObjectProperty("Contratante_EmailSdaUser_N", gxTv_SdtContratante_Contratante_emailsdauser_N, false);
            AddObjectProperty("Contratante_EmailSdaPass_N", gxTv_SdtContratante_Contratante_emailsdapass_N, false);
            AddObjectProperty("Contratante_EmailSdaKey_N", gxTv_SdtContratante_Contratante_emailsdakey_N, false);
            AddObjectProperty("Contratante_EmailSdaAut_N", gxTv_SdtContratante_Contratante_emailsdaaut_N, false);
            AddObjectProperty("Contratante_EmailSdaPort_N", gxTv_SdtContratante_Contratante_emailsdaport_N, false);
            AddObjectProperty("Contratante_EmailSdaSec_N", gxTv_SdtContratante_Contratante_emailsdasec_N, false);
            AddObjectProperty("Contratante_SSAutomatica_N", gxTv_SdtContratante_Contratante_ssautomatica_N, false);
            AddObjectProperty("Contratante_LogoArquivo_N", gxTv_SdtContratante_Contratante_logoarquivo_N, false);
            AddObjectProperty("Contratante_LogoNomeArq_N", gxTv_SdtContratante_Contratante_logonomearq_N, false);
            AddObjectProperty("Contratante_LogoTipoArq_N", gxTv_SdtContratante_Contratante_logotipoarq_N, false);
            AddObjectProperty("Contratante_InicioDoExpediente_N", gxTv_SdtContratante_Contratante_iniciodoexpediente_N, false);
            AddObjectProperty("Contratante_FimDoExpediente_N", gxTv_SdtContratante_Contratante_fimdoexpediente_N, false);
            AddObjectProperty("Contratante_ServicoSSPadrao_N", gxTv_SdtContratante_Contratante_servicosspadrao_N, false);
            AddObjectProperty("Contratante_RetornaSS_N", gxTv_SdtContratante_Contratante_retornass_N, false);
            AddObjectProperty("Contratante_SSStatusPlanejamento_N", gxTv_SdtContratante_Contratante_ssstatusplanejamento_N, false);
            AddObjectProperty("Contratante_SSPrestadoraUnica_N", gxTv_SdtContratante_Contratante_ssprestadoraunica_N, false);
            AddObjectProperty("Contratante_PropostaRqrSrv_N", gxTv_SdtContratante_Contratante_propostarqrsrv_N, false);
            AddObjectProperty("Contratante_PropostaRqrPrz_N", gxTv_SdtContratante_Contratante_propostarqrprz_N, false);
            AddObjectProperty("Contratante_PropostaRqrEsf_N", gxTv_SdtContratante_Contratante_propostarqresf_N, false);
            AddObjectProperty("Contratante_PropostaRqrCnt_N", gxTv_SdtContratante_Contratante_propostarqrcnt_N, false);
            AddObjectProperty("Contratante_PropostaNvlCnt_N", gxTv_SdtContratante_Contratante_propostanvlcnt_N, false);
            AddObjectProperty("Contratante_OSGeraOS_N", gxTv_SdtContratante_Contratante_osgeraos_N, false);
            AddObjectProperty("Contratante_OSGeraTRP_N", gxTv_SdtContratante_Contratante_osgeratrp_N, false);
            AddObjectProperty("Contratante_OSGeraTH_N", gxTv_SdtContratante_Contratante_osgerath_N, false);
            AddObjectProperty("Contratante_OSGeraTRD_N", gxTv_SdtContratante_Contratante_osgeratrd_N, false);
            AddObjectProperty("Contratante_OSGeraTR_N", gxTv_SdtContratante_Contratante_osgeratr_N, false);
            AddObjectProperty("Contratante_OSHmlgComPnd_N", gxTv_SdtContratante_Contratante_oshmlgcompnd_N, false);
            AddObjectProperty("Contratante_AtivoCirculante_N", gxTv_SdtContratante_Contratante_ativocirculante_N, false);
            AddObjectProperty("Contratante_PassivoCirculante_N", gxTv_SdtContratante_Contratante_passivocirculante_N, false);
            AddObjectProperty("Contratante_PatrimonioLiquido_N", gxTv_SdtContratante_Contratante_patrimonioliquido_N, false);
            AddObjectProperty("Contratante_ReceitaBruta_N", gxTv_SdtContratante_Contratante_receitabruta_N, false);
            AddObjectProperty("Contratante_UsaOSistema_N", gxTv_SdtContratante_Contratante_usaosistema_N, false);
            AddObjectProperty("Contratante_TtlRltGerencial_N", gxTv_SdtContratante_Contratante_ttlrltgerencial_N, false);
            AddObjectProperty("Contratante_PrzAoRtr_N", gxTv_SdtContratante_Contratante_przaortr_N, false);
            AddObjectProperty("Contratante_PrzActRtr_N", gxTv_SdtContratante_Contratante_przactrtr_N, false);
            AddObjectProperty("Contratante_RequerOrigem_N", gxTv_SdtContratante_Contratante_requerorigem_N, false);
            AddObjectProperty("Contratante_Sigla_N", gxTv_SdtContratante_Contratante_sigla_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Contratante_Codigo" )]
      [  XmlElement( ElementName = "Contratante_Codigo"   )]
      public int gxTpr_Contratante_codigo
      {
         get {
            return gxTv_SdtContratante_Contratante_codigo ;
         }

         set {
            if ( gxTv_SdtContratante_Contratante_codigo != value )
            {
               gxTv_SdtContratante_Mode = "INS";
               this.gxTv_SdtContratante_Contratante_codigo_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_pessoacod_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_cnpj_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_razaosocial_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_nomefantasia_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_ie_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_website_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_email_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_telefone_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_ramal_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_fax_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_agencianome_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_agencianro_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_banconome_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_banconro_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_contacorrente_Z_SetNull( );
               this.gxTv_SdtContratante_Municipio_codigo_Z_SetNull( );
               this.gxTv_SdtContratante_Municipio_nome_Z_SetNull( );
               this.gxTv_SdtContratante_Estado_uf_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_ativo_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_emailsdahost_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_emailsdauser_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_emailsdapass_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_emailsdakey_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_emailsdaaut_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_emailsdaport_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_emailsdasec_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_osautomatica_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_ssautomatica_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_existeconferencia_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_logonomearq_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_logotipoarq_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_iniciodoexpediente_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_fimdoexpediente_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_servicosspadrao_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_retornass_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_ssstatusplanejamento_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_ssprestadoraunica_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_propostarqrsrv_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_propostarqrprz_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_propostarqresf_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_propostarqrcnt_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_propostanvlcnt_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_osgeraos_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_osgeratrp_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_osgerath_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_osgeratrd_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_osgeratr_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_oshmlgcompnd_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_ativocirculante_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_passivocirculante_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_patrimonioliquido_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_receitabruta_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_usaosistema_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_ttlrltgerencial_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_przaortr_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_przactrtr_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_requerorigem_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_selecionaresponsavelos_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_exibepf_Z_SetNull( );
               this.gxTv_SdtContratante_Contratante_sigla_Z_SetNull( );
            }
            gxTv_SdtContratante_Contratante_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_PessoaCod" )]
      [  XmlElement( ElementName = "Contratante_PessoaCod"   )]
      public int gxTpr_Contratante_pessoacod
      {
         get {
            return gxTv_SdtContratante_Contratante_pessoacod ;
         }

         set {
            gxTv_SdtContratante_Contratante_pessoacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_CNPJ" )]
      [  XmlElement( ElementName = "Contratante_CNPJ"   )]
      public String gxTpr_Contratante_cnpj
      {
         get {
            return gxTv_SdtContratante_Contratante_cnpj ;
         }

         set {
            gxTv_SdtContratante_Contratante_cnpj_N = 0;
            gxTv_SdtContratante_Contratante_cnpj = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_cnpj_SetNull( )
      {
         gxTv_SdtContratante_Contratante_cnpj_N = 1;
         gxTv_SdtContratante_Contratante_cnpj = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_cnpj_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_RazaoSocial" )]
      [  XmlElement( ElementName = "Contratante_RazaoSocial"   )]
      public String gxTpr_Contratante_razaosocial
      {
         get {
            return gxTv_SdtContratante_Contratante_razaosocial ;
         }

         set {
            gxTv_SdtContratante_Contratante_razaosocial_N = 0;
            gxTv_SdtContratante_Contratante_razaosocial = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_razaosocial_SetNull( )
      {
         gxTv_SdtContratante_Contratante_razaosocial_N = 1;
         gxTv_SdtContratante_Contratante_razaosocial = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_razaosocial_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_NomeFantasia" )]
      [  XmlElement( ElementName = "Contratante_NomeFantasia"   )]
      public String gxTpr_Contratante_nomefantasia
      {
         get {
            return gxTv_SdtContratante_Contratante_nomefantasia ;
         }

         set {
            gxTv_SdtContratante_Contratante_nomefantasia = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_IE" )]
      [  XmlElement( ElementName = "Contratante_IE"   )]
      public String gxTpr_Contratante_ie
      {
         get {
            return gxTv_SdtContratante_Contratante_ie ;
         }

         set {
            gxTv_SdtContratante_Contratante_ie = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_WebSite" )]
      [  XmlElement( ElementName = "Contratante_WebSite"   )]
      public String gxTpr_Contratante_website
      {
         get {
            return gxTv_SdtContratante_Contratante_website ;
         }

         set {
            gxTv_SdtContratante_Contratante_website_N = 0;
            gxTv_SdtContratante_Contratante_website = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_website_SetNull( )
      {
         gxTv_SdtContratante_Contratante_website_N = 1;
         gxTv_SdtContratante_Contratante_website = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_website_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Email" )]
      [  XmlElement( ElementName = "Contratante_Email"   )]
      public String gxTpr_Contratante_email
      {
         get {
            return gxTv_SdtContratante_Contratante_email ;
         }

         set {
            gxTv_SdtContratante_Contratante_email_N = 0;
            gxTv_SdtContratante_Contratante_email = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_email_SetNull( )
      {
         gxTv_SdtContratante_Contratante_email_N = 1;
         gxTv_SdtContratante_Contratante_email = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_email_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Telefone" )]
      [  XmlElement( ElementName = "Contratante_Telefone"   )]
      public String gxTpr_Contratante_telefone
      {
         get {
            return gxTv_SdtContratante_Contratante_telefone ;
         }

         set {
            gxTv_SdtContratante_Contratante_telefone = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_Ramal" )]
      [  XmlElement( ElementName = "Contratante_Ramal"   )]
      public String gxTpr_Contratante_ramal
      {
         get {
            return gxTv_SdtContratante_Contratante_ramal ;
         }

         set {
            gxTv_SdtContratante_Contratante_ramal_N = 0;
            gxTv_SdtContratante_Contratante_ramal = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_ramal_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ramal_N = 1;
         gxTv_SdtContratante_Contratante_ramal = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ramal_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Fax" )]
      [  XmlElement( ElementName = "Contratante_Fax"   )]
      public String gxTpr_Contratante_fax
      {
         get {
            return gxTv_SdtContratante_Contratante_fax ;
         }

         set {
            gxTv_SdtContratante_Contratante_fax_N = 0;
            gxTv_SdtContratante_Contratante_fax = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_fax_SetNull( )
      {
         gxTv_SdtContratante_Contratante_fax_N = 1;
         gxTv_SdtContratante_Contratante_fax = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_fax_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_AgenciaNome" )]
      [  XmlElement( ElementName = "Contratante_AgenciaNome"   )]
      public String gxTpr_Contratante_agencianome
      {
         get {
            return gxTv_SdtContratante_Contratante_agencianome ;
         }

         set {
            gxTv_SdtContratante_Contratante_agencianome_N = 0;
            gxTv_SdtContratante_Contratante_agencianome = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_agencianome_SetNull( )
      {
         gxTv_SdtContratante_Contratante_agencianome_N = 1;
         gxTv_SdtContratante_Contratante_agencianome = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_agencianome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_AgenciaNro" )]
      [  XmlElement( ElementName = "Contratante_AgenciaNro"   )]
      public String gxTpr_Contratante_agencianro
      {
         get {
            return gxTv_SdtContratante_Contratante_agencianro ;
         }

         set {
            gxTv_SdtContratante_Contratante_agencianro_N = 0;
            gxTv_SdtContratante_Contratante_agencianro = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_agencianro_SetNull( )
      {
         gxTv_SdtContratante_Contratante_agencianro_N = 1;
         gxTv_SdtContratante_Contratante_agencianro = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_agencianro_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_BancoNome" )]
      [  XmlElement( ElementName = "Contratante_BancoNome"   )]
      public String gxTpr_Contratante_banconome
      {
         get {
            return gxTv_SdtContratante_Contratante_banconome ;
         }

         set {
            gxTv_SdtContratante_Contratante_banconome_N = 0;
            gxTv_SdtContratante_Contratante_banconome = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_banconome_SetNull( )
      {
         gxTv_SdtContratante_Contratante_banconome_N = 1;
         gxTv_SdtContratante_Contratante_banconome = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_banconome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_BancoNro" )]
      [  XmlElement( ElementName = "Contratante_BancoNro"   )]
      public String gxTpr_Contratante_banconro
      {
         get {
            return gxTv_SdtContratante_Contratante_banconro ;
         }

         set {
            gxTv_SdtContratante_Contratante_banconro_N = 0;
            gxTv_SdtContratante_Contratante_banconro = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_banconro_SetNull( )
      {
         gxTv_SdtContratante_Contratante_banconro_N = 1;
         gxTv_SdtContratante_Contratante_banconro = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_banconro_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_ContaCorrente" )]
      [  XmlElement( ElementName = "Contratante_ContaCorrente"   )]
      public String gxTpr_Contratante_contacorrente
      {
         get {
            return gxTv_SdtContratante_Contratante_contacorrente ;
         }

         set {
            gxTv_SdtContratante_Contratante_contacorrente_N = 0;
            gxTv_SdtContratante_Contratante_contacorrente = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_contacorrente_SetNull( )
      {
         gxTv_SdtContratante_Contratante_contacorrente_N = 1;
         gxTv_SdtContratante_Contratante_contacorrente = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_contacorrente_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Municipio_Codigo" )]
      [  XmlElement( ElementName = "Municipio_Codigo"   )]
      public int gxTpr_Municipio_codigo
      {
         get {
            return gxTv_SdtContratante_Municipio_codigo ;
         }

         set {
            gxTv_SdtContratante_Municipio_codigo_N = 0;
            gxTv_SdtContratante_Municipio_codigo = (int)(value);
         }

      }

      public void gxTv_SdtContratante_Municipio_codigo_SetNull( )
      {
         gxTv_SdtContratante_Municipio_codigo_N = 1;
         gxTv_SdtContratante_Municipio_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Municipio_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Municipio_Nome" )]
      [  XmlElement( ElementName = "Municipio_Nome"   )]
      public String gxTpr_Municipio_nome
      {
         get {
            return gxTv_SdtContratante_Municipio_nome ;
         }

         set {
            gxTv_SdtContratante_Municipio_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Estado_UF" )]
      [  XmlElement( ElementName = "Estado_UF"   )]
      public String gxTpr_Estado_uf
      {
         get {
            return gxTv_SdtContratante_Estado_uf ;
         }

         set {
            gxTv_SdtContratante_Estado_uf = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_Ativo" )]
      [  XmlElement( ElementName = "Contratante_Ativo"   )]
      public bool gxTpr_Contratante_ativo
      {
         get {
            return gxTv_SdtContratante_Contratante_ativo ;
         }

         set {
            gxTv_SdtContratante_Contratante_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Contratante_EmailSdaHost" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaHost"   )]
      public String gxTpr_Contratante_emailsdahost
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdahost ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdahost_N = 0;
            gxTv_SdtContratante_Contratante_emailsdahost = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdahost_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdahost_N = 1;
         gxTv_SdtContratante_Contratante_emailsdahost = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdahost_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaUser" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaUser"   )]
      public String gxTpr_Contratante_emailsdauser
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdauser ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdauser_N = 0;
            gxTv_SdtContratante_Contratante_emailsdauser = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdauser_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdauser_N = 1;
         gxTv_SdtContratante_Contratante_emailsdauser = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdauser_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaPass" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaPass"   )]
      public String gxTpr_Contratante_emailsdapass
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdapass ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdapass_N = 0;
            gxTv_SdtContratante_Contratante_emailsdapass = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdapass_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdapass_N = 1;
         gxTv_SdtContratante_Contratante_emailsdapass = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdapass_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaKey" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaKey"   )]
      public String gxTpr_Contratante_emailsdakey
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdakey ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdakey_N = 0;
            gxTv_SdtContratante_Contratante_emailsdakey = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdakey_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdakey_N = 1;
         gxTv_SdtContratante_Contratante_emailsdakey = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdakey_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaAut" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaAut"   )]
      public bool gxTpr_Contratante_emailsdaaut
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdaaut ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdaaut_N = 0;
            gxTv_SdtContratante_Contratante_emailsdaaut = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdaaut_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdaaut_N = 1;
         gxTv_SdtContratante_Contratante_emailsdaaut = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdaaut_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaPort" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaPort"   )]
      public short gxTpr_Contratante_emailsdaport
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdaport ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdaport_N = 0;
            gxTv_SdtContratante_Contratante_emailsdaport = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdaport_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdaport_N = 1;
         gxTv_SdtContratante_Contratante_emailsdaport = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdaport_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaSec" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaSec"   )]
      public short gxTpr_Contratante_emailsdasec
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdasec ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdasec_N = 0;
            gxTv_SdtContratante_Contratante_emailsdasec = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdasec_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdasec_N = 1;
         gxTv_SdtContratante_Contratante_emailsdasec = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdasec_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSAutomatica" )]
      [  XmlElement( ElementName = "Contratante_OSAutomatica"   )]
      public bool gxTpr_Contratante_osautomatica
      {
         get {
            return gxTv_SdtContratante_Contratante_osautomatica ;
         }

         set {
            gxTv_SdtContratante_Contratante_osautomatica = value;
         }

      }

      [  SoapElement( ElementName = "Contratante_SSAutomatica" )]
      [  XmlElement( ElementName = "Contratante_SSAutomatica"   )]
      public bool gxTpr_Contratante_ssautomatica
      {
         get {
            return gxTv_SdtContratante_Contratante_ssautomatica ;
         }

         set {
            gxTv_SdtContratante_Contratante_ssautomatica_N = 0;
            gxTv_SdtContratante_Contratante_ssautomatica = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_ssautomatica_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ssautomatica_N = 1;
         gxTv_SdtContratante_Contratante_ssautomatica = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ssautomatica_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_ExisteConferencia" )]
      [  XmlElement( ElementName = "Contratante_ExisteConferencia"   )]
      public bool gxTpr_Contratante_existeconferencia
      {
         get {
            return gxTv_SdtContratante_Contratante_existeconferencia ;
         }

         set {
            gxTv_SdtContratante_Contratante_existeconferencia = value;
         }

      }

      [  SoapElement( ElementName = "Contratante_LogoArquivo" )]
      [  XmlElement( ElementName = "Contratante_LogoArquivo"   )]
      [GxUpload()]
      public byte[] gxTpr_Contratante_logoarquivo_Blob
      {
         get {
            IGxContext context = this.context == null ? new GxContext() : this.context;
            return context.FileToByteArray( gxTv_SdtContratante_Contratante_logoarquivo) ;
         }

         set {
            gxTv_SdtContratante_Contratante_logoarquivo_N = 0;
            IGxContext context = this.context == null ? new GxContext() : this.context;
            gxTv_SdtContratante_Contratante_logoarquivo=context.FileFromByteArray( value) ;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      [GxUpload()]
      public String gxTpr_Contratante_logoarquivo
      {
         get {
            return gxTv_SdtContratante_Contratante_logoarquivo ;
         }

         set {
            gxTv_SdtContratante_Contratante_logoarquivo_N = 0;
            gxTv_SdtContratante_Contratante_logoarquivo = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_logoarquivo_SetBlob( String blob ,
                                                                       String fileName ,
                                                                       String fileType )
      {
         gxTv_SdtContratante_Contratante_logoarquivo = blob;
         gxTv_SdtContratante_Contratante_logonomearq = fileName;
         gxTv_SdtContratante_Contratante_logotipoarq = fileType;
         return  ;
      }

      public void gxTv_SdtContratante_Contratante_logoarquivo_SetNull( )
      {
         gxTv_SdtContratante_Contratante_logoarquivo_N = 1;
         gxTv_SdtContratante_Contratante_logoarquivo = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_logoarquivo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_LogoNomeArq" )]
      [  XmlElement( ElementName = "Contratante_LogoNomeArq"   )]
      public String gxTpr_Contratante_logonomearq
      {
         get {
            return gxTv_SdtContratante_Contratante_logonomearq ;
         }

         set {
            gxTv_SdtContratante_Contratante_logonomearq_N = 0;
            gxTv_SdtContratante_Contratante_logonomearq = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_logonomearq_SetNull( )
      {
         gxTv_SdtContratante_Contratante_logonomearq_N = 1;
         gxTv_SdtContratante_Contratante_logonomearq = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_logonomearq_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_LogoTipoArq" )]
      [  XmlElement( ElementName = "Contratante_LogoTipoArq"   )]
      public String gxTpr_Contratante_logotipoarq
      {
         get {
            return gxTv_SdtContratante_Contratante_logotipoarq ;
         }

         set {
            gxTv_SdtContratante_Contratante_logotipoarq_N = 0;
            gxTv_SdtContratante_Contratante_logotipoarq = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_logotipoarq_SetNull( )
      {
         gxTv_SdtContratante_Contratante_logotipoarq_N = 1;
         gxTv_SdtContratante_Contratante_logotipoarq = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_logotipoarq_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_InicioDoExpediente" )]
      [  XmlElement( ElementName = "Contratante_InicioDoExpediente"  , IsNullable=true )]
      public string gxTpr_Contratante_iniciodoexpediente_Nullable
      {
         get {
            if ( gxTv_SdtContratante_Contratante_iniciodoexpediente == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContratante_Contratante_iniciodoexpediente).value ;
         }

         set {
            gxTv_SdtContratante_Contratante_iniciodoexpediente_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContratante_Contratante_iniciodoexpediente = DateTime.MinValue;
            else
               gxTv_SdtContratante_Contratante_iniciodoexpediente = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contratante_iniciodoexpediente
      {
         get {
            return gxTv_SdtContratante_Contratante_iniciodoexpediente ;
         }

         set {
            gxTv_SdtContratante_Contratante_iniciodoexpediente_N = 0;
            gxTv_SdtContratante_Contratante_iniciodoexpediente = (DateTime)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_iniciodoexpediente_SetNull( )
      {
         gxTv_SdtContratante_Contratante_iniciodoexpediente_N = 1;
         gxTv_SdtContratante_Contratante_iniciodoexpediente = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_iniciodoexpediente_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_FimDoExpediente" )]
      [  XmlElement( ElementName = "Contratante_FimDoExpediente"  , IsNullable=true )]
      public string gxTpr_Contratante_fimdoexpediente_Nullable
      {
         get {
            if ( gxTv_SdtContratante_Contratante_fimdoexpediente == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContratante_Contratante_fimdoexpediente).value ;
         }

         set {
            gxTv_SdtContratante_Contratante_fimdoexpediente_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContratante_Contratante_fimdoexpediente = DateTime.MinValue;
            else
               gxTv_SdtContratante_Contratante_fimdoexpediente = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contratante_fimdoexpediente
      {
         get {
            return gxTv_SdtContratante_Contratante_fimdoexpediente ;
         }

         set {
            gxTv_SdtContratante_Contratante_fimdoexpediente_N = 0;
            gxTv_SdtContratante_Contratante_fimdoexpediente = (DateTime)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_fimdoexpediente_SetNull( )
      {
         gxTv_SdtContratante_Contratante_fimdoexpediente_N = 1;
         gxTv_SdtContratante_Contratante_fimdoexpediente = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_fimdoexpediente_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_ServicoSSPadrao" )]
      [  XmlElement( ElementName = "Contratante_ServicoSSPadrao"   )]
      public int gxTpr_Contratante_servicosspadrao
      {
         get {
            return gxTv_SdtContratante_Contratante_servicosspadrao ;
         }

         set {
            gxTv_SdtContratante_Contratante_servicosspadrao_N = 0;
            gxTv_SdtContratante_Contratante_servicosspadrao = (int)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_servicosspadrao_SetNull( )
      {
         gxTv_SdtContratante_Contratante_servicosspadrao_N = 1;
         gxTv_SdtContratante_Contratante_servicosspadrao = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_servicosspadrao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_RetornaSS" )]
      [  XmlElement( ElementName = "Contratante_RetornaSS"   )]
      public bool gxTpr_Contratante_retornass
      {
         get {
            return gxTv_SdtContratante_Contratante_retornass ;
         }

         set {
            gxTv_SdtContratante_Contratante_retornass_N = 0;
            gxTv_SdtContratante_Contratante_retornass = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_retornass_SetNull( )
      {
         gxTv_SdtContratante_Contratante_retornass_N = 1;
         gxTv_SdtContratante_Contratante_retornass = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_retornass_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_SSStatusPlanejamento" )]
      [  XmlElement( ElementName = "Contratante_SSStatusPlanejamento"   )]
      public String gxTpr_Contratante_ssstatusplanejamento
      {
         get {
            return gxTv_SdtContratante_Contratante_ssstatusplanejamento ;
         }

         set {
            gxTv_SdtContratante_Contratante_ssstatusplanejamento_N = 0;
            gxTv_SdtContratante_Contratante_ssstatusplanejamento = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_ssstatusplanejamento_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ssstatusplanejamento_N = 1;
         gxTv_SdtContratante_Contratante_ssstatusplanejamento = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ssstatusplanejamento_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_SSPrestadoraUnica" )]
      [  XmlElement( ElementName = "Contratante_SSPrestadoraUnica"   )]
      public bool gxTpr_Contratante_ssprestadoraunica
      {
         get {
            return gxTv_SdtContratante_Contratante_ssprestadoraunica ;
         }

         set {
            gxTv_SdtContratante_Contratante_ssprestadoraunica_N = 0;
            gxTv_SdtContratante_Contratante_ssprestadoraunica = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_ssprestadoraunica_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ssprestadoraunica_N = 1;
         gxTv_SdtContratante_Contratante_ssprestadoraunica = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ssprestadoraunica_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PropostaRqrSrv" )]
      [  XmlElement( ElementName = "Contratante_PropostaRqrSrv"   )]
      public bool gxTpr_Contratante_propostarqrsrv
      {
         get {
            return gxTv_SdtContratante_Contratante_propostarqrsrv ;
         }

         set {
            gxTv_SdtContratante_Contratante_propostarqrsrv_N = 0;
            gxTv_SdtContratante_Contratante_propostarqrsrv = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_propostarqrsrv_SetNull( )
      {
         gxTv_SdtContratante_Contratante_propostarqrsrv_N = 1;
         gxTv_SdtContratante_Contratante_propostarqrsrv = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_propostarqrsrv_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PropostaRqrPrz" )]
      [  XmlElement( ElementName = "Contratante_PropostaRqrPrz"   )]
      public bool gxTpr_Contratante_propostarqrprz
      {
         get {
            return gxTv_SdtContratante_Contratante_propostarqrprz ;
         }

         set {
            gxTv_SdtContratante_Contratante_propostarqrprz_N = 0;
            gxTv_SdtContratante_Contratante_propostarqrprz = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_propostarqrprz_SetNull( )
      {
         gxTv_SdtContratante_Contratante_propostarqrprz_N = 1;
         gxTv_SdtContratante_Contratante_propostarqrprz = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_propostarqrprz_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PropostaRqrEsf" )]
      [  XmlElement( ElementName = "Contratante_PropostaRqrEsf"   )]
      public bool gxTpr_Contratante_propostarqresf
      {
         get {
            return gxTv_SdtContratante_Contratante_propostarqresf ;
         }

         set {
            gxTv_SdtContratante_Contratante_propostarqresf_N = 0;
            gxTv_SdtContratante_Contratante_propostarqresf = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_propostarqresf_SetNull( )
      {
         gxTv_SdtContratante_Contratante_propostarqresf_N = 1;
         gxTv_SdtContratante_Contratante_propostarqresf = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_propostarqresf_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PropostaRqrCnt" )]
      [  XmlElement( ElementName = "Contratante_PropostaRqrCnt"   )]
      public bool gxTpr_Contratante_propostarqrcnt
      {
         get {
            return gxTv_SdtContratante_Contratante_propostarqrcnt ;
         }

         set {
            gxTv_SdtContratante_Contratante_propostarqrcnt_N = 0;
            gxTv_SdtContratante_Contratante_propostarqrcnt = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_propostarqrcnt_SetNull( )
      {
         gxTv_SdtContratante_Contratante_propostarqrcnt_N = 1;
         gxTv_SdtContratante_Contratante_propostarqrcnt = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_propostarqrcnt_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PropostaNvlCnt" )]
      [  XmlElement( ElementName = "Contratante_PropostaNvlCnt"   )]
      public short gxTpr_Contratante_propostanvlcnt
      {
         get {
            return gxTv_SdtContratante_Contratante_propostanvlcnt ;
         }

         set {
            gxTv_SdtContratante_Contratante_propostanvlcnt_N = 0;
            gxTv_SdtContratante_Contratante_propostanvlcnt = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_propostanvlcnt_SetNull( )
      {
         gxTv_SdtContratante_Contratante_propostanvlcnt_N = 1;
         gxTv_SdtContratante_Contratante_propostanvlcnt = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_propostanvlcnt_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSGeraOS" )]
      [  XmlElement( ElementName = "Contratante_OSGeraOS"   )]
      public bool gxTpr_Contratante_osgeraos
      {
         get {
            return gxTv_SdtContratante_Contratante_osgeraos ;
         }

         set {
            gxTv_SdtContratante_Contratante_osgeraos_N = 0;
            gxTv_SdtContratante_Contratante_osgeraos = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_osgeraos_SetNull( )
      {
         gxTv_SdtContratante_Contratante_osgeraos_N = 1;
         gxTv_SdtContratante_Contratante_osgeraos = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_osgeraos_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSGeraTRP" )]
      [  XmlElement( ElementName = "Contratante_OSGeraTRP"   )]
      public bool gxTpr_Contratante_osgeratrp
      {
         get {
            return gxTv_SdtContratante_Contratante_osgeratrp ;
         }

         set {
            gxTv_SdtContratante_Contratante_osgeratrp_N = 0;
            gxTv_SdtContratante_Contratante_osgeratrp = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_osgeratrp_SetNull( )
      {
         gxTv_SdtContratante_Contratante_osgeratrp_N = 1;
         gxTv_SdtContratante_Contratante_osgeratrp = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_osgeratrp_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSGeraTH" )]
      [  XmlElement( ElementName = "Contratante_OSGeraTH"   )]
      public bool gxTpr_Contratante_osgerath
      {
         get {
            return gxTv_SdtContratante_Contratante_osgerath ;
         }

         set {
            gxTv_SdtContratante_Contratante_osgerath_N = 0;
            gxTv_SdtContratante_Contratante_osgerath = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_osgerath_SetNull( )
      {
         gxTv_SdtContratante_Contratante_osgerath_N = 1;
         gxTv_SdtContratante_Contratante_osgerath = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_osgerath_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSGeraTRD" )]
      [  XmlElement( ElementName = "Contratante_OSGeraTRD"   )]
      public bool gxTpr_Contratante_osgeratrd
      {
         get {
            return gxTv_SdtContratante_Contratante_osgeratrd ;
         }

         set {
            gxTv_SdtContratante_Contratante_osgeratrd_N = 0;
            gxTv_SdtContratante_Contratante_osgeratrd = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_osgeratrd_SetNull( )
      {
         gxTv_SdtContratante_Contratante_osgeratrd_N = 1;
         gxTv_SdtContratante_Contratante_osgeratrd = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_osgeratrd_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSGeraTR" )]
      [  XmlElement( ElementName = "Contratante_OSGeraTR"   )]
      public bool gxTpr_Contratante_osgeratr
      {
         get {
            return gxTv_SdtContratante_Contratante_osgeratr ;
         }

         set {
            gxTv_SdtContratante_Contratante_osgeratr_N = 0;
            gxTv_SdtContratante_Contratante_osgeratr = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_osgeratr_SetNull( )
      {
         gxTv_SdtContratante_Contratante_osgeratr_N = 1;
         gxTv_SdtContratante_Contratante_osgeratr = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_osgeratr_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSHmlgComPnd" )]
      [  XmlElement( ElementName = "Contratante_OSHmlgComPnd"   )]
      public bool gxTpr_Contratante_oshmlgcompnd
      {
         get {
            return gxTv_SdtContratante_Contratante_oshmlgcompnd ;
         }

         set {
            gxTv_SdtContratante_Contratante_oshmlgcompnd_N = 0;
            gxTv_SdtContratante_Contratante_oshmlgcompnd = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_oshmlgcompnd_SetNull( )
      {
         gxTv_SdtContratante_Contratante_oshmlgcompnd_N = 1;
         gxTv_SdtContratante_Contratante_oshmlgcompnd = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_oshmlgcompnd_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_AtivoCirculante" )]
      [  XmlElement( ElementName = "Contratante_AtivoCirculante"   )]
      public double gxTpr_Contratante_ativocirculante_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratante_Contratante_ativocirculante) ;
         }

         set {
            gxTv_SdtContratante_Contratante_ativocirculante_N = 0;
            gxTv_SdtContratante_Contratante_ativocirculante = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratante_ativocirculante
      {
         get {
            return gxTv_SdtContratante_Contratante_ativocirculante ;
         }

         set {
            gxTv_SdtContratante_Contratante_ativocirculante_N = 0;
            gxTv_SdtContratante_Contratante_ativocirculante = (decimal)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_ativocirculante_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ativocirculante_N = 1;
         gxTv_SdtContratante_Contratante_ativocirculante = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ativocirculante_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PassivoCirculante" )]
      [  XmlElement( ElementName = "Contratante_PassivoCirculante"   )]
      public double gxTpr_Contratante_passivocirculante_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratante_Contratante_passivocirculante) ;
         }

         set {
            gxTv_SdtContratante_Contratante_passivocirculante_N = 0;
            gxTv_SdtContratante_Contratante_passivocirculante = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratante_passivocirculante
      {
         get {
            return gxTv_SdtContratante_Contratante_passivocirculante ;
         }

         set {
            gxTv_SdtContratante_Contratante_passivocirculante_N = 0;
            gxTv_SdtContratante_Contratante_passivocirculante = (decimal)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_passivocirculante_SetNull( )
      {
         gxTv_SdtContratante_Contratante_passivocirculante_N = 1;
         gxTv_SdtContratante_Contratante_passivocirculante = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_passivocirculante_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PatrimonioLiquido" )]
      [  XmlElement( ElementName = "Contratante_PatrimonioLiquido"   )]
      public double gxTpr_Contratante_patrimonioliquido_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratante_Contratante_patrimonioliquido) ;
         }

         set {
            gxTv_SdtContratante_Contratante_patrimonioliquido_N = 0;
            gxTv_SdtContratante_Contratante_patrimonioliquido = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratante_patrimonioliquido
      {
         get {
            return gxTv_SdtContratante_Contratante_patrimonioliquido ;
         }

         set {
            gxTv_SdtContratante_Contratante_patrimonioliquido_N = 0;
            gxTv_SdtContratante_Contratante_patrimonioliquido = (decimal)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_patrimonioliquido_SetNull( )
      {
         gxTv_SdtContratante_Contratante_patrimonioliquido_N = 1;
         gxTv_SdtContratante_Contratante_patrimonioliquido = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_patrimonioliquido_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_ReceitaBruta" )]
      [  XmlElement( ElementName = "Contratante_ReceitaBruta"   )]
      public double gxTpr_Contratante_receitabruta_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratante_Contratante_receitabruta) ;
         }

         set {
            gxTv_SdtContratante_Contratante_receitabruta_N = 0;
            gxTv_SdtContratante_Contratante_receitabruta = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratante_receitabruta
      {
         get {
            return gxTv_SdtContratante_Contratante_receitabruta ;
         }

         set {
            gxTv_SdtContratante_Contratante_receitabruta_N = 0;
            gxTv_SdtContratante_Contratante_receitabruta = (decimal)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_receitabruta_SetNull( )
      {
         gxTv_SdtContratante_Contratante_receitabruta_N = 1;
         gxTv_SdtContratante_Contratante_receitabruta = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_receitabruta_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_UsaOSistema" )]
      [  XmlElement( ElementName = "Contratante_UsaOSistema"   )]
      public bool gxTpr_Contratante_usaosistema
      {
         get {
            return gxTv_SdtContratante_Contratante_usaosistema ;
         }

         set {
            gxTv_SdtContratante_Contratante_usaosistema_N = 0;
            gxTv_SdtContratante_Contratante_usaosistema = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_usaosistema_SetNull( )
      {
         gxTv_SdtContratante_Contratante_usaosistema_N = 1;
         gxTv_SdtContratante_Contratante_usaosistema = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_usaosistema_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_TtlRltGerencial" )]
      [  XmlElement( ElementName = "Contratante_TtlRltGerencial"   )]
      public String gxTpr_Contratante_ttlrltgerencial
      {
         get {
            return gxTv_SdtContratante_Contratante_ttlrltgerencial ;
         }

         set {
            gxTv_SdtContratante_Contratante_ttlrltgerencial_N = 0;
            gxTv_SdtContratante_Contratante_ttlrltgerencial = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_ttlrltgerencial_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ttlrltgerencial_N = 1;
         gxTv_SdtContratante_Contratante_ttlrltgerencial = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ttlrltgerencial_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PrzAoRtr" )]
      [  XmlElement( ElementName = "Contratante_PrzAoRtr"   )]
      public short gxTpr_Contratante_przaortr
      {
         get {
            return gxTv_SdtContratante_Contratante_przaortr ;
         }

         set {
            gxTv_SdtContratante_Contratante_przaortr_N = 0;
            gxTv_SdtContratante_Contratante_przaortr = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_przaortr_SetNull( )
      {
         gxTv_SdtContratante_Contratante_przaortr_N = 1;
         gxTv_SdtContratante_Contratante_przaortr = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_przaortr_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PrzActRtr" )]
      [  XmlElement( ElementName = "Contratante_PrzActRtr"   )]
      public short gxTpr_Contratante_przactrtr
      {
         get {
            return gxTv_SdtContratante_Contratante_przactrtr ;
         }

         set {
            gxTv_SdtContratante_Contratante_przactrtr_N = 0;
            gxTv_SdtContratante_Contratante_przactrtr = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_przactrtr_SetNull( )
      {
         gxTv_SdtContratante_Contratante_przactrtr_N = 1;
         gxTv_SdtContratante_Contratante_przactrtr = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_przactrtr_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_RequerOrigem" )]
      [  XmlElement( ElementName = "Contratante_RequerOrigem"   )]
      public bool gxTpr_Contratante_requerorigem
      {
         get {
            return gxTv_SdtContratante_Contratante_requerorigem ;
         }

         set {
            gxTv_SdtContratante_Contratante_requerorigem_N = 0;
            gxTv_SdtContratante_Contratante_requerorigem = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_requerorigem_SetNull( )
      {
         gxTv_SdtContratante_Contratante_requerorigem_N = 1;
         gxTv_SdtContratante_Contratante_requerorigem = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_requerorigem_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_SelecionaResponsavelOS" )]
      [  XmlElement( ElementName = "Contratante_SelecionaResponsavelOS"   )]
      public bool gxTpr_Contratante_selecionaresponsavelos
      {
         get {
            return gxTv_SdtContratante_Contratante_selecionaresponsavelos ;
         }

         set {
            gxTv_SdtContratante_Contratante_selecionaresponsavelos = value;
         }

      }

      [  SoapElement( ElementName = "Contratante_ExibePF" )]
      [  XmlElement( ElementName = "Contratante_ExibePF"   )]
      public bool gxTpr_Contratante_exibepf
      {
         get {
            return gxTv_SdtContratante_Contratante_exibepf ;
         }

         set {
            gxTv_SdtContratante_Contratante_exibepf = value;
         }

      }

      [  SoapElement( ElementName = "Contratante_Sigla" )]
      [  XmlElement( ElementName = "Contratante_Sigla"   )]
      public String gxTpr_Contratante_sigla
      {
         get {
            return gxTv_SdtContratante_Contratante_sigla ;
         }

         set {
            gxTv_SdtContratante_Contratante_sigla_N = 0;
            gxTv_SdtContratante_Contratante_sigla = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_sigla_SetNull( )
      {
         gxTv_SdtContratante_Contratante_sigla_N = 1;
         gxTv_SdtContratante_Contratante_sigla = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_sigla_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContratante_Mode ;
         }

         set {
            gxTv_SdtContratante_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Mode_SetNull( )
      {
         gxTv_SdtContratante_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContratante_Initialized ;
         }

         set {
            gxTv_SdtContratante_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Initialized_SetNull( )
      {
         gxTv_SdtContratante_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Codigo_Z" )]
      [  XmlElement( ElementName = "Contratante_Codigo_Z"   )]
      public int gxTpr_Contratante_codigo_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_codigo_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_codigo_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PessoaCod_Z" )]
      [  XmlElement( ElementName = "Contratante_PessoaCod_Z"   )]
      public int gxTpr_Contratante_pessoacod_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_pessoacod_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_pessoacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_pessoacod_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_pessoacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_pessoacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_CNPJ_Z" )]
      [  XmlElement( ElementName = "Contratante_CNPJ_Z"   )]
      public String gxTpr_Contratante_cnpj_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_cnpj_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_cnpj_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_cnpj_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_cnpj_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_cnpj_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_RazaoSocial_Z" )]
      [  XmlElement( ElementName = "Contratante_RazaoSocial_Z"   )]
      public String gxTpr_Contratante_razaosocial_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_razaosocial_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_razaosocial_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_razaosocial_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_razaosocial_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_razaosocial_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_NomeFantasia_Z" )]
      [  XmlElement( ElementName = "Contratante_NomeFantasia_Z"   )]
      public String gxTpr_Contratante_nomefantasia_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_nomefantasia_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_nomefantasia_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_nomefantasia_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_nomefantasia_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_nomefantasia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_IE_Z" )]
      [  XmlElement( ElementName = "Contratante_IE_Z"   )]
      public String gxTpr_Contratante_ie_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_ie_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_ie_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_ie_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ie_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ie_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_WebSite_Z" )]
      [  XmlElement( ElementName = "Contratante_WebSite_Z"   )]
      public String gxTpr_Contratante_website_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_website_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_website_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_website_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_website_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_website_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Email_Z" )]
      [  XmlElement( ElementName = "Contratante_Email_Z"   )]
      public String gxTpr_Contratante_email_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_email_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_email_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_email_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_email_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_email_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Telefone_Z" )]
      [  XmlElement( ElementName = "Contratante_Telefone_Z"   )]
      public String gxTpr_Contratante_telefone_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_telefone_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_telefone_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_telefone_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_telefone_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_telefone_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Ramal_Z" )]
      [  XmlElement( ElementName = "Contratante_Ramal_Z"   )]
      public String gxTpr_Contratante_ramal_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_ramal_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_ramal_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_ramal_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ramal_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ramal_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Fax_Z" )]
      [  XmlElement( ElementName = "Contratante_Fax_Z"   )]
      public String gxTpr_Contratante_fax_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_fax_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_fax_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_fax_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_fax_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_fax_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_AgenciaNome_Z" )]
      [  XmlElement( ElementName = "Contratante_AgenciaNome_Z"   )]
      public String gxTpr_Contratante_agencianome_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_agencianome_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_agencianome_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_agencianome_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_agencianome_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_agencianome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_AgenciaNro_Z" )]
      [  XmlElement( ElementName = "Contratante_AgenciaNro_Z"   )]
      public String gxTpr_Contratante_agencianro_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_agencianro_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_agencianro_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_agencianro_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_agencianro_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_agencianro_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_BancoNome_Z" )]
      [  XmlElement( ElementName = "Contratante_BancoNome_Z"   )]
      public String gxTpr_Contratante_banconome_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_banconome_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_banconome_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_banconome_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_banconome_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_banconome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_BancoNro_Z" )]
      [  XmlElement( ElementName = "Contratante_BancoNro_Z"   )]
      public String gxTpr_Contratante_banconro_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_banconro_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_banconro_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_banconro_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_banconro_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_banconro_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_ContaCorrente_Z" )]
      [  XmlElement( ElementName = "Contratante_ContaCorrente_Z"   )]
      public String gxTpr_Contratante_contacorrente_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_contacorrente_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_contacorrente_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_contacorrente_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_contacorrente_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_contacorrente_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Municipio_Codigo_Z" )]
      [  XmlElement( ElementName = "Municipio_Codigo_Z"   )]
      public int gxTpr_Municipio_codigo_Z
      {
         get {
            return gxTv_SdtContratante_Municipio_codigo_Z ;
         }

         set {
            gxTv_SdtContratante_Municipio_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratante_Municipio_codigo_Z_SetNull( )
      {
         gxTv_SdtContratante_Municipio_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Municipio_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Municipio_Nome_Z" )]
      [  XmlElement( ElementName = "Municipio_Nome_Z"   )]
      public String gxTpr_Municipio_nome_Z
      {
         get {
            return gxTv_SdtContratante_Municipio_nome_Z ;
         }

         set {
            gxTv_SdtContratante_Municipio_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Municipio_nome_Z_SetNull( )
      {
         gxTv_SdtContratante_Municipio_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Municipio_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Estado_UF_Z" )]
      [  XmlElement( ElementName = "Estado_UF_Z"   )]
      public String gxTpr_Estado_uf_Z
      {
         get {
            return gxTv_SdtContratante_Estado_uf_Z ;
         }

         set {
            gxTv_SdtContratante_Estado_uf_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Estado_uf_Z_SetNull( )
      {
         gxTv_SdtContratante_Estado_uf_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Estado_uf_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Ativo_Z" )]
      [  XmlElement( ElementName = "Contratante_Ativo_Z"   )]
      public bool gxTpr_Contratante_ativo_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_ativo_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_ativo_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_ativo_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaHost_Z" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaHost_Z"   )]
      public String gxTpr_Contratante_emailsdahost_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdahost_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdahost_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdahost_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdahost_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdahost_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaUser_Z" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaUser_Z"   )]
      public String gxTpr_Contratante_emailsdauser_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdauser_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdauser_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdauser_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdauser_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdauser_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaPass_Z" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaPass_Z"   )]
      public String gxTpr_Contratante_emailsdapass_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdapass_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdapass_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdapass_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdapass_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdapass_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaKey_Z" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaKey_Z"   )]
      public String gxTpr_Contratante_emailsdakey_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdakey_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdakey_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdakey_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdakey_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdakey_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaAut_Z" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaAut_Z"   )]
      public bool gxTpr_Contratante_emailsdaaut_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdaaut_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdaaut_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdaaut_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdaaut_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdaaut_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaPort_Z" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaPort_Z"   )]
      public short gxTpr_Contratante_emailsdaport_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdaport_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdaport_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdaport_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdaport_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdaport_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaSec_Z" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaSec_Z"   )]
      public short gxTpr_Contratante_emailsdasec_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdasec_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdasec_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdasec_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdasec_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdasec_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSAutomatica_Z" )]
      [  XmlElement( ElementName = "Contratante_OSAutomatica_Z"   )]
      public bool gxTpr_Contratante_osautomatica_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_osautomatica_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_osautomatica_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_osautomatica_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_osautomatica_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_osautomatica_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_SSAutomatica_Z" )]
      [  XmlElement( ElementName = "Contratante_SSAutomatica_Z"   )]
      public bool gxTpr_Contratante_ssautomatica_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_ssautomatica_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_ssautomatica_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_ssautomatica_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ssautomatica_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ssautomatica_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_ExisteConferencia_Z" )]
      [  XmlElement( ElementName = "Contratante_ExisteConferencia_Z"   )]
      public bool gxTpr_Contratante_existeconferencia_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_existeconferencia_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_existeconferencia_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_existeconferencia_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_existeconferencia_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_existeconferencia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_LogoNomeArq_Z" )]
      [  XmlElement( ElementName = "Contratante_LogoNomeArq_Z"   )]
      public String gxTpr_Contratante_logonomearq_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_logonomearq_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_logonomearq_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_logonomearq_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_logonomearq_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_logonomearq_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_LogoTipoArq_Z" )]
      [  XmlElement( ElementName = "Contratante_LogoTipoArq_Z"   )]
      public String gxTpr_Contratante_logotipoarq_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_logotipoarq_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_logotipoarq_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_logotipoarq_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_logotipoarq_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_logotipoarq_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_InicioDoExpediente_Z" )]
      [  XmlElement( ElementName = "Contratante_InicioDoExpediente_Z"  , IsNullable=true )]
      public string gxTpr_Contratante_iniciodoexpediente_Z_Nullable
      {
         get {
            if ( gxTv_SdtContratante_Contratante_iniciodoexpediente_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContratante_Contratante_iniciodoexpediente_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContratante_Contratante_iniciodoexpediente_Z = DateTime.MinValue;
            else
               gxTv_SdtContratante_Contratante_iniciodoexpediente_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contratante_iniciodoexpediente_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_iniciodoexpediente_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_iniciodoexpediente_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_iniciodoexpediente_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_iniciodoexpediente_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_iniciodoexpediente_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_FimDoExpediente_Z" )]
      [  XmlElement( ElementName = "Contratante_FimDoExpediente_Z"  , IsNullable=true )]
      public string gxTpr_Contratante_fimdoexpediente_Z_Nullable
      {
         get {
            if ( gxTv_SdtContratante_Contratante_fimdoexpediente_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContratante_Contratante_fimdoexpediente_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContratante_Contratante_fimdoexpediente_Z = DateTime.MinValue;
            else
               gxTv_SdtContratante_Contratante_fimdoexpediente_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contratante_fimdoexpediente_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_fimdoexpediente_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_fimdoexpediente_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_fimdoexpediente_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_fimdoexpediente_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_fimdoexpediente_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_ServicoSSPadrao_Z" )]
      [  XmlElement( ElementName = "Contratante_ServicoSSPadrao_Z"   )]
      public int gxTpr_Contratante_servicosspadrao_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_servicosspadrao_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_servicosspadrao_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_servicosspadrao_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_servicosspadrao_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_servicosspadrao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_RetornaSS_Z" )]
      [  XmlElement( ElementName = "Contratante_RetornaSS_Z"   )]
      public bool gxTpr_Contratante_retornass_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_retornass_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_retornass_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_retornass_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_retornass_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_retornass_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_SSStatusPlanejamento_Z" )]
      [  XmlElement( ElementName = "Contratante_SSStatusPlanejamento_Z"   )]
      public String gxTpr_Contratante_ssstatusplanejamento_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_ssstatusplanejamento_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_ssstatusplanejamento_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_ssstatusplanejamento_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ssstatusplanejamento_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ssstatusplanejamento_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_SSPrestadoraUnica_Z" )]
      [  XmlElement( ElementName = "Contratante_SSPrestadoraUnica_Z"   )]
      public bool gxTpr_Contratante_ssprestadoraunica_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_ssprestadoraunica_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_ssprestadoraunica_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_ssprestadoraunica_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ssprestadoraunica_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ssprestadoraunica_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PropostaRqrSrv_Z" )]
      [  XmlElement( ElementName = "Contratante_PropostaRqrSrv_Z"   )]
      public bool gxTpr_Contratante_propostarqrsrv_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_propostarqrsrv_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_propostarqrsrv_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_propostarqrsrv_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_propostarqrsrv_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_propostarqrsrv_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PropostaRqrPrz_Z" )]
      [  XmlElement( ElementName = "Contratante_PropostaRqrPrz_Z"   )]
      public bool gxTpr_Contratante_propostarqrprz_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_propostarqrprz_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_propostarqrprz_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_propostarqrprz_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_propostarqrprz_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_propostarqrprz_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PropostaRqrEsf_Z" )]
      [  XmlElement( ElementName = "Contratante_PropostaRqrEsf_Z"   )]
      public bool gxTpr_Contratante_propostarqresf_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_propostarqresf_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_propostarqresf_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_propostarqresf_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_propostarqresf_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_propostarqresf_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PropostaRqrCnt_Z" )]
      [  XmlElement( ElementName = "Contratante_PropostaRqrCnt_Z"   )]
      public bool gxTpr_Contratante_propostarqrcnt_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_propostarqrcnt_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_propostarqrcnt_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_propostarqrcnt_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_propostarqrcnt_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_propostarqrcnt_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PropostaNvlCnt_Z" )]
      [  XmlElement( ElementName = "Contratante_PropostaNvlCnt_Z"   )]
      public short gxTpr_Contratante_propostanvlcnt_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_propostanvlcnt_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_propostanvlcnt_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_propostanvlcnt_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_propostanvlcnt_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_propostanvlcnt_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSGeraOS_Z" )]
      [  XmlElement( ElementName = "Contratante_OSGeraOS_Z"   )]
      public bool gxTpr_Contratante_osgeraos_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_osgeraos_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_osgeraos_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_osgeraos_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_osgeraos_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_osgeraos_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSGeraTRP_Z" )]
      [  XmlElement( ElementName = "Contratante_OSGeraTRP_Z"   )]
      public bool gxTpr_Contratante_osgeratrp_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_osgeratrp_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_osgeratrp_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_osgeratrp_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_osgeratrp_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_osgeratrp_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSGeraTH_Z" )]
      [  XmlElement( ElementName = "Contratante_OSGeraTH_Z"   )]
      public bool gxTpr_Contratante_osgerath_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_osgerath_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_osgerath_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_osgerath_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_osgerath_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_osgerath_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSGeraTRD_Z" )]
      [  XmlElement( ElementName = "Contratante_OSGeraTRD_Z"   )]
      public bool gxTpr_Contratante_osgeratrd_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_osgeratrd_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_osgeratrd_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_osgeratrd_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_osgeratrd_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_osgeratrd_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSGeraTR_Z" )]
      [  XmlElement( ElementName = "Contratante_OSGeraTR_Z"   )]
      public bool gxTpr_Contratante_osgeratr_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_osgeratr_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_osgeratr_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_osgeratr_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_osgeratr_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_osgeratr_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSHmlgComPnd_Z" )]
      [  XmlElement( ElementName = "Contratante_OSHmlgComPnd_Z"   )]
      public bool gxTpr_Contratante_oshmlgcompnd_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_oshmlgcompnd_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_oshmlgcompnd_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_oshmlgcompnd_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_oshmlgcompnd_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_oshmlgcompnd_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_AtivoCirculante_Z" )]
      [  XmlElement( ElementName = "Contratante_AtivoCirculante_Z"   )]
      public double gxTpr_Contratante_ativocirculante_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratante_Contratante_ativocirculante_Z) ;
         }

         set {
            gxTv_SdtContratante_Contratante_ativocirculante_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratante_ativocirculante_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_ativocirculante_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_ativocirculante_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_ativocirculante_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ativocirculante_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ativocirculante_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PassivoCirculante_Z" )]
      [  XmlElement( ElementName = "Contratante_PassivoCirculante_Z"   )]
      public double gxTpr_Contratante_passivocirculante_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratante_Contratante_passivocirculante_Z) ;
         }

         set {
            gxTv_SdtContratante_Contratante_passivocirculante_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratante_passivocirculante_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_passivocirculante_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_passivocirculante_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_passivocirculante_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_passivocirculante_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_passivocirculante_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PatrimonioLiquido_Z" )]
      [  XmlElement( ElementName = "Contratante_PatrimonioLiquido_Z"   )]
      public double gxTpr_Contratante_patrimonioliquido_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratante_Contratante_patrimonioliquido_Z) ;
         }

         set {
            gxTv_SdtContratante_Contratante_patrimonioliquido_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratante_patrimonioliquido_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_patrimonioliquido_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_patrimonioliquido_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_patrimonioliquido_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_patrimonioliquido_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_patrimonioliquido_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_ReceitaBruta_Z" )]
      [  XmlElement( ElementName = "Contratante_ReceitaBruta_Z"   )]
      public double gxTpr_Contratante_receitabruta_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratante_Contratante_receitabruta_Z) ;
         }

         set {
            gxTv_SdtContratante_Contratante_receitabruta_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratante_receitabruta_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_receitabruta_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_receitabruta_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_receitabruta_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_receitabruta_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_receitabruta_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_UsaOSistema_Z" )]
      [  XmlElement( ElementName = "Contratante_UsaOSistema_Z"   )]
      public bool gxTpr_Contratante_usaosistema_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_usaosistema_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_usaosistema_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_usaosistema_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_usaosistema_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_usaosistema_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_TtlRltGerencial_Z" )]
      [  XmlElement( ElementName = "Contratante_TtlRltGerencial_Z"   )]
      public String gxTpr_Contratante_ttlrltgerencial_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_ttlrltgerencial_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_ttlrltgerencial_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_ttlrltgerencial_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ttlrltgerencial_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ttlrltgerencial_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PrzAoRtr_Z" )]
      [  XmlElement( ElementName = "Contratante_PrzAoRtr_Z"   )]
      public short gxTpr_Contratante_przaortr_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_przaortr_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_przaortr_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_przaortr_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_przaortr_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_przaortr_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PrzActRtr_Z" )]
      [  XmlElement( ElementName = "Contratante_PrzActRtr_Z"   )]
      public short gxTpr_Contratante_przactrtr_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_przactrtr_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_przactrtr_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_przactrtr_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_przactrtr_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_przactrtr_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_RequerOrigem_Z" )]
      [  XmlElement( ElementName = "Contratante_RequerOrigem_Z"   )]
      public bool gxTpr_Contratante_requerorigem_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_requerorigem_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_requerorigem_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_requerorigem_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_requerorigem_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_requerorigem_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_SelecionaResponsavelOS_Z" )]
      [  XmlElement( ElementName = "Contratante_SelecionaResponsavelOS_Z"   )]
      public bool gxTpr_Contratante_selecionaresponsavelos_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_selecionaresponsavelos_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_selecionaresponsavelos_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_selecionaresponsavelos_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_selecionaresponsavelos_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_selecionaresponsavelos_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_ExibePF_Z" )]
      [  XmlElement( ElementName = "Contratante_ExibePF_Z"   )]
      public bool gxTpr_Contratante_exibepf_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_exibepf_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_exibepf_Z = value;
         }

      }

      public void gxTv_SdtContratante_Contratante_exibepf_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_exibepf_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_exibepf_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Sigla_Z" )]
      [  XmlElement( ElementName = "Contratante_Sigla_Z"   )]
      public String gxTpr_Contratante_sigla_Z
      {
         get {
            return gxTv_SdtContratante_Contratante_sigla_Z ;
         }

         set {
            gxTv_SdtContratante_Contratante_sigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_sigla_Z_SetNull( )
      {
         gxTv_SdtContratante_Contratante_sigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_sigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Codigo_N" )]
      [  XmlElement( ElementName = "Contratante_Codigo_N"   )]
      public short gxTpr_Contratante_codigo_N
      {
         get {
            return gxTv_SdtContratante_Contratante_codigo_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_codigo_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_CNPJ_N" )]
      [  XmlElement( ElementName = "Contratante_CNPJ_N"   )]
      public short gxTpr_Contratante_cnpj_N
      {
         get {
            return gxTv_SdtContratante_Contratante_cnpj_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_cnpj_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_cnpj_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_cnpj_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_cnpj_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_RazaoSocial_N" )]
      [  XmlElement( ElementName = "Contratante_RazaoSocial_N"   )]
      public short gxTpr_Contratante_razaosocial_N
      {
         get {
            return gxTv_SdtContratante_Contratante_razaosocial_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_razaosocial_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_razaosocial_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_razaosocial_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_razaosocial_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_WebSite_N" )]
      [  XmlElement( ElementName = "Contratante_WebSite_N"   )]
      public short gxTpr_Contratante_website_N
      {
         get {
            return gxTv_SdtContratante_Contratante_website_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_website_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_website_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_website_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_website_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Email_N" )]
      [  XmlElement( ElementName = "Contratante_Email_N"   )]
      public short gxTpr_Contratante_email_N
      {
         get {
            return gxTv_SdtContratante_Contratante_email_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_email_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_email_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_email_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_email_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Ramal_N" )]
      [  XmlElement( ElementName = "Contratante_Ramal_N"   )]
      public short gxTpr_Contratante_ramal_N
      {
         get {
            return gxTv_SdtContratante_Contratante_ramal_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_ramal_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_ramal_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ramal_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ramal_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Fax_N" )]
      [  XmlElement( ElementName = "Contratante_Fax_N"   )]
      public short gxTpr_Contratante_fax_N
      {
         get {
            return gxTv_SdtContratante_Contratante_fax_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_fax_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_fax_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_fax_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_fax_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_AgenciaNome_N" )]
      [  XmlElement( ElementName = "Contratante_AgenciaNome_N"   )]
      public short gxTpr_Contratante_agencianome_N
      {
         get {
            return gxTv_SdtContratante_Contratante_agencianome_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_agencianome_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_agencianome_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_agencianome_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_agencianome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_AgenciaNro_N" )]
      [  XmlElement( ElementName = "Contratante_AgenciaNro_N"   )]
      public short gxTpr_Contratante_agencianro_N
      {
         get {
            return gxTv_SdtContratante_Contratante_agencianro_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_agencianro_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_agencianro_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_agencianro_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_agencianro_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_BancoNome_N" )]
      [  XmlElement( ElementName = "Contratante_BancoNome_N"   )]
      public short gxTpr_Contratante_banconome_N
      {
         get {
            return gxTv_SdtContratante_Contratante_banconome_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_banconome_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_banconome_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_banconome_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_banconome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_BancoNro_N" )]
      [  XmlElement( ElementName = "Contratante_BancoNro_N"   )]
      public short gxTpr_Contratante_banconro_N
      {
         get {
            return gxTv_SdtContratante_Contratante_banconro_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_banconro_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_banconro_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_banconro_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_banconro_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_ContaCorrente_N" )]
      [  XmlElement( ElementName = "Contratante_ContaCorrente_N"   )]
      public short gxTpr_Contratante_contacorrente_N
      {
         get {
            return gxTv_SdtContratante_Contratante_contacorrente_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_contacorrente_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_contacorrente_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_contacorrente_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_contacorrente_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Municipio_Codigo_N" )]
      [  XmlElement( ElementName = "Municipio_Codigo_N"   )]
      public short gxTpr_Municipio_codigo_N
      {
         get {
            return gxTv_SdtContratante_Municipio_codigo_N ;
         }

         set {
            gxTv_SdtContratante_Municipio_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Municipio_codigo_N_SetNull( )
      {
         gxTv_SdtContratante_Municipio_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Municipio_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaHost_N" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaHost_N"   )]
      public short gxTpr_Contratante_emailsdahost_N
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdahost_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdahost_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdahost_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdahost_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdahost_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaUser_N" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaUser_N"   )]
      public short gxTpr_Contratante_emailsdauser_N
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdauser_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdauser_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdauser_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdauser_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdauser_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaPass_N" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaPass_N"   )]
      public short gxTpr_Contratante_emailsdapass_N
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdapass_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdapass_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdapass_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdapass_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdapass_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaKey_N" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaKey_N"   )]
      public short gxTpr_Contratante_emailsdakey_N
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdakey_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdakey_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdakey_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdakey_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdakey_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaAut_N" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaAut_N"   )]
      public short gxTpr_Contratante_emailsdaaut_N
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdaaut_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdaaut_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdaaut_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdaaut_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdaaut_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaPort_N" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaPort_N"   )]
      public short gxTpr_Contratante_emailsdaport_N
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdaport_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdaport_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdaport_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdaport_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdaport_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaSec_N" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaSec_N"   )]
      public short gxTpr_Contratante_emailsdasec_N
      {
         get {
            return gxTv_SdtContratante_Contratante_emailsdasec_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_emailsdasec_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_emailsdasec_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_emailsdasec_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_emailsdasec_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_SSAutomatica_N" )]
      [  XmlElement( ElementName = "Contratante_SSAutomatica_N"   )]
      public short gxTpr_Contratante_ssautomatica_N
      {
         get {
            return gxTv_SdtContratante_Contratante_ssautomatica_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_ssautomatica_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_ssautomatica_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ssautomatica_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ssautomatica_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_LogoArquivo_N" )]
      [  XmlElement( ElementName = "Contratante_LogoArquivo_N"   )]
      public short gxTpr_Contratante_logoarquivo_N
      {
         get {
            return gxTv_SdtContratante_Contratante_logoarquivo_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_logoarquivo_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_logoarquivo_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_logoarquivo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_logoarquivo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_LogoNomeArq_N" )]
      [  XmlElement( ElementName = "Contratante_LogoNomeArq_N"   )]
      public short gxTpr_Contratante_logonomearq_N
      {
         get {
            return gxTv_SdtContratante_Contratante_logonomearq_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_logonomearq_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_logonomearq_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_logonomearq_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_logonomearq_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_LogoTipoArq_N" )]
      [  XmlElement( ElementName = "Contratante_LogoTipoArq_N"   )]
      public short gxTpr_Contratante_logotipoarq_N
      {
         get {
            return gxTv_SdtContratante_Contratante_logotipoarq_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_logotipoarq_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_logotipoarq_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_logotipoarq_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_logotipoarq_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_InicioDoExpediente_N" )]
      [  XmlElement( ElementName = "Contratante_InicioDoExpediente_N"   )]
      public short gxTpr_Contratante_iniciodoexpediente_N
      {
         get {
            return gxTv_SdtContratante_Contratante_iniciodoexpediente_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_iniciodoexpediente_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_iniciodoexpediente_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_iniciodoexpediente_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_iniciodoexpediente_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_FimDoExpediente_N" )]
      [  XmlElement( ElementName = "Contratante_FimDoExpediente_N"   )]
      public short gxTpr_Contratante_fimdoexpediente_N
      {
         get {
            return gxTv_SdtContratante_Contratante_fimdoexpediente_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_fimdoexpediente_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_fimdoexpediente_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_fimdoexpediente_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_fimdoexpediente_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_ServicoSSPadrao_N" )]
      [  XmlElement( ElementName = "Contratante_ServicoSSPadrao_N"   )]
      public short gxTpr_Contratante_servicosspadrao_N
      {
         get {
            return gxTv_SdtContratante_Contratante_servicosspadrao_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_servicosspadrao_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_servicosspadrao_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_servicosspadrao_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_servicosspadrao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_RetornaSS_N" )]
      [  XmlElement( ElementName = "Contratante_RetornaSS_N"   )]
      public short gxTpr_Contratante_retornass_N
      {
         get {
            return gxTv_SdtContratante_Contratante_retornass_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_retornass_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_retornass_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_retornass_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_retornass_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_SSStatusPlanejamento_N" )]
      [  XmlElement( ElementName = "Contratante_SSStatusPlanejamento_N"   )]
      public short gxTpr_Contratante_ssstatusplanejamento_N
      {
         get {
            return gxTv_SdtContratante_Contratante_ssstatusplanejamento_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_ssstatusplanejamento_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_ssstatusplanejamento_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ssstatusplanejamento_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ssstatusplanejamento_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_SSPrestadoraUnica_N" )]
      [  XmlElement( ElementName = "Contratante_SSPrestadoraUnica_N"   )]
      public short gxTpr_Contratante_ssprestadoraunica_N
      {
         get {
            return gxTv_SdtContratante_Contratante_ssprestadoraunica_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_ssprestadoraunica_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_ssprestadoraunica_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ssprestadoraunica_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ssprestadoraunica_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PropostaRqrSrv_N" )]
      [  XmlElement( ElementName = "Contratante_PropostaRqrSrv_N"   )]
      public short gxTpr_Contratante_propostarqrsrv_N
      {
         get {
            return gxTv_SdtContratante_Contratante_propostarqrsrv_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_propostarqrsrv_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_propostarqrsrv_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_propostarqrsrv_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_propostarqrsrv_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PropostaRqrPrz_N" )]
      [  XmlElement( ElementName = "Contratante_PropostaRqrPrz_N"   )]
      public short gxTpr_Contratante_propostarqrprz_N
      {
         get {
            return gxTv_SdtContratante_Contratante_propostarqrprz_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_propostarqrprz_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_propostarqrprz_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_propostarqrprz_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_propostarqrprz_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PropostaRqrEsf_N" )]
      [  XmlElement( ElementName = "Contratante_PropostaRqrEsf_N"   )]
      public short gxTpr_Contratante_propostarqresf_N
      {
         get {
            return gxTv_SdtContratante_Contratante_propostarqresf_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_propostarqresf_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_propostarqresf_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_propostarqresf_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_propostarqresf_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PropostaRqrCnt_N" )]
      [  XmlElement( ElementName = "Contratante_PropostaRqrCnt_N"   )]
      public short gxTpr_Contratante_propostarqrcnt_N
      {
         get {
            return gxTv_SdtContratante_Contratante_propostarqrcnt_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_propostarqrcnt_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_propostarqrcnt_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_propostarqrcnt_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_propostarqrcnt_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PropostaNvlCnt_N" )]
      [  XmlElement( ElementName = "Contratante_PropostaNvlCnt_N"   )]
      public short gxTpr_Contratante_propostanvlcnt_N
      {
         get {
            return gxTv_SdtContratante_Contratante_propostanvlcnt_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_propostanvlcnt_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_propostanvlcnt_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_propostanvlcnt_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_propostanvlcnt_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSGeraOS_N" )]
      [  XmlElement( ElementName = "Contratante_OSGeraOS_N"   )]
      public short gxTpr_Contratante_osgeraos_N
      {
         get {
            return gxTv_SdtContratante_Contratante_osgeraos_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_osgeraos_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_osgeraos_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_osgeraos_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_osgeraos_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSGeraTRP_N" )]
      [  XmlElement( ElementName = "Contratante_OSGeraTRP_N"   )]
      public short gxTpr_Contratante_osgeratrp_N
      {
         get {
            return gxTv_SdtContratante_Contratante_osgeratrp_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_osgeratrp_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_osgeratrp_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_osgeratrp_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_osgeratrp_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSGeraTH_N" )]
      [  XmlElement( ElementName = "Contratante_OSGeraTH_N"   )]
      public short gxTpr_Contratante_osgerath_N
      {
         get {
            return gxTv_SdtContratante_Contratante_osgerath_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_osgerath_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_osgerath_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_osgerath_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_osgerath_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSGeraTRD_N" )]
      [  XmlElement( ElementName = "Contratante_OSGeraTRD_N"   )]
      public short gxTpr_Contratante_osgeratrd_N
      {
         get {
            return gxTv_SdtContratante_Contratante_osgeratrd_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_osgeratrd_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_osgeratrd_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_osgeratrd_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_osgeratrd_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSGeraTR_N" )]
      [  XmlElement( ElementName = "Contratante_OSGeraTR_N"   )]
      public short gxTpr_Contratante_osgeratr_N
      {
         get {
            return gxTv_SdtContratante_Contratante_osgeratr_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_osgeratr_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_osgeratr_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_osgeratr_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_osgeratr_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_OSHmlgComPnd_N" )]
      [  XmlElement( ElementName = "Contratante_OSHmlgComPnd_N"   )]
      public short gxTpr_Contratante_oshmlgcompnd_N
      {
         get {
            return gxTv_SdtContratante_Contratante_oshmlgcompnd_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_oshmlgcompnd_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_oshmlgcompnd_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_oshmlgcompnd_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_oshmlgcompnd_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_AtivoCirculante_N" )]
      [  XmlElement( ElementName = "Contratante_AtivoCirculante_N"   )]
      public short gxTpr_Contratante_ativocirculante_N
      {
         get {
            return gxTv_SdtContratante_Contratante_ativocirculante_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_ativocirculante_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_ativocirculante_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ativocirculante_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ativocirculante_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PassivoCirculante_N" )]
      [  XmlElement( ElementName = "Contratante_PassivoCirculante_N"   )]
      public short gxTpr_Contratante_passivocirculante_N
      {
         get {
            return gxTv_SdtContratante_Contratante_passivocirculante_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_passivocirculante_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_passivocirculante_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_passivocirculante_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_passivocirculante_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PatrimonioLiquido_N" )]
      [  XmlElement( ElementName = "Contratante_PatrimonioLiquido_N"   )]
      public short gxTpr_Contratante_patrimonioliquido_N
      {
         get {
            return gxTv_SdtContratante_Contratante_patrimonioliquido_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_patrimonioliquido_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_patrimonioliquido_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_patrimonioliquido_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_patrimonioliquido_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_ReceitaBruta_N" )]
      [  XmlElement( ElementName = "Contratante_ReceitaBruta_N"   )]
      public short gxTpr_Contratante_receitabruta_N
      {
         get {
            return gxTv_SdtContratante_Contratante_receitabruta_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_receitabruta_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_receitabruta_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_receitabruta_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_receitabruta_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_UsaOSistema_N" )]
      [  XmlElement( ElementName = "Contratante_UsaOSistema_N"   )]
      public short gxTpr_Contratante_usaosistema_N
      {
         get {
            return gxTv_SdtContratante_Contratante_usaosistema_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_usaosistema_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_usaosistema_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_usaosistema_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_usaosistema_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_TtlRltGerencial_N" )]
      [  XmlElement( ElementName = "Contratante_TtlRltGerencial_N"   )]
      public short gxTpr_Contratante_ttlrltgerencial_N
      {
         get {
            return gxTv_SdtContratante_Contratante_ttlrltgerencial_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_ttlrltgerencial_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_ttlrltgerencial_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_ttlrltgerencial_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_ttlrltgerencial_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PrzAoRtr_N" )]
      [  XmlElement( ElementName = "Contratante_PrzAoRtr_N"   )]
      public short gxTpr_Contratante_przaortr_N
      {
         get {
            return gxTv_SdtContratante_Contratante_przaortr_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_przaortr_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_przaortr_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_przaortr_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_przaortr_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PrzActRtr_N" )]
      [  XmlElement( ElementName = "Contratante_PrzActRtr_N"   )]
      public short gxTpr_Contratante_przactrtr_N
      {
         get {
            return gxTv_SdtContratante_Contratante_przactrtr_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_przactrtr_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_przactrtr_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_przactrtr_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_przactrtr_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_RequerOrigem_N" )]
      [  XmlElement( ElementName = "Contratante_RequerOrigem_N"   )]
      public short gxTpr_Contratante_requerorigem_N
      {
         get {
            return gxTv_SdtContratante_Contratante_requerorigem_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_requerorigem_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_requerorigem_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_requerorigem_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_requerorigem_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Sigla_N" )]
      [  XmlElement( ElementName = "Contratante_Sigla_N"   )]
      public short gxTpr_Contratante_sigla_N
      {
         get {
            return gxTv_SdtContratante_Contratante_sigla_N ;
         }

         set {
            gxTv_SdtContratante_Contratante_sigla_N = (short)(value);
         }

      }

      public void gxTv_SdtContratante_Contratante_sigla_N_SetNull( )
      {
         gxTv_SdtContratante_Contratante_sigla_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratante_Contratante_sigla_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContratante_Contratante_cnpj = "";
         gxTv_SdtContratante_Contratante_razaosocial = "";
         gxTv_SdtContratante_Contratante_nomefantasia = "";
         gxTv_SdtContratante_Contratante_ie = "";
         gxTv_SdtContratante_Contratante_website = "";
         gxTv_SdtContratante_Contratante_email = "";
         gxTv_SdtContratante_Contratante_telefone = "";
         gxTv_SdtContratante_Contratante_ramal = "";
         gxTv_SdtContratante_Contratante_fax = "";
         gxTv_SdtContratante_Contratante_agencianome = "";
         gxTv_SdtContratante_Contratante_agencianro = "";
         gxTv_SdtContratante_Contratante_banconome = "";
         gxTv_SdtContratante_Contratante_banconro = "";
         gxTv_SdtContratante_Contratante_contacorrente = "";
         gxTv_SdtContratante_Municipio_nome = "";
         gxTv_SdtContratante_Estado_uf = "";
         gxTv_SdtContratante_Contratante_emailsdahost = "";
         gxTv_SdtContratante_Contratante_emailsdauser = "";
         gxTv_SdtContratante_Contratante_emailsdapass = "";
         gxTv_SdtContratante_Contratante_emailsdakey = "";
         gxTv_SdtContratante_Contratante_logoarquivo = "";
         gxTv_SdtContratante_Contratante_logonomearq = "";
         gxTv_SdtContratante_Contratante_logotipoarq = "";
         gxTv_SdtContratante_Contratante_iniciodoexpediente = (DateTime)(DateTime.MinValue);
         gxTv_SdtContratante_Contratante_fimdoexpediente = (DateTime)(DateTime.MinValue);
         gxTv_SdtContratante_Contratante_ssstatusplanejamento = "B";
         gxTv_SdtContratante_Contratante_ttlrltgerencial = "Relatório Gerencial";
         gxTv_SdtContratante_Contratante_sigla = "";
         gxTv_SdtContratante_Mode = "";
         gxTv_SdtContratante_Contratante_cnpj_Z = "";
         gxTv_SdtContratante_Contratante_razaosocial_Z = "";
         gxTv_SdtContratante_Contratante_nomefantasia_Z = "";
         gxTv_SdtContratante_Contratante_ie_Z = "";
         gxTv_SdtContratante_Contratante_website_Z = "";
         gxTv_SdtContratante_Contratante_email_Z = "";
         gxTv_SdtContratante_Contratante_telefone_Z = "";
         gxTv_SdtContratante_Contratante_ramal_Z = "";
         gxTv_SdtContratante_Contratante_fax_Z = "";
         gxTv_SdtContratante_Contratante_agencianome_Z = "";
         gxTv_SdtContratante_Contratante_agencianro_Z = "";
         gxTv_SdtContratante_Contratante_banconome_Z = "";
         gxTv_SdtContratante_Contratante_banconro_Z = "";
         gxTv_SdtContratante_Contratante_contacorrente_Z = "";
         gxTv_SdtContratante_Municipio_nome_Z = "";
         gxTv_SdtContratante_Estado_uf_Z = "";
         gxTv_SdtContratante_Contratante_emailsdahost_Z = "";
         gxTv_SdtContratante_Contratante_emailsdauser_Z = "";
         gxTv_SdtContratante_Contratante_emailsdapass_Z = "";
         gxTv_SdtContratante_Contratante_emailsdakey_Z = "";
         gxTv_SdtContratante_Contratante_logonomearq_Z = "";
         gxTv_SdtContratante_Contratante_logotipoarq_Z = "";
         gxTv_SdtContratante_Contratante_iniciodoexpediente_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContratante_Contratante_fimdoexpediente_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContratante_Contratante_ssstatusplanejamento_Z = "";
         gxTv_SdtContratante_Contratante_ttlrltgerencial_Z = "";
         gxTv_SdtContratante_Contratante_sigla_Z = "";
         gxTv_SdtContratante_Contratante_ativo = true;
         gxTv_SdtContratante_Contratante_emailsdaaut = false;
         gxTv_SdtContratante_Contratante_propostarqrsrv = true;
         gxTv_SdtContratante_Contratante_propostanvlcnt = 0;
         gxTv_SdtContratante_Contratante_usaosistema = false;
         gxTv_SdtContratante_Contratante_requerorigem = true;
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contratante", "GeneXus.Programs.contratante_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContratante_Contratante_emailsdaport ;
      private short gxTv_SdtContratante_Contratante_emailsdasec ;
      private short gxTv_SdtContratante_Contratante_propostanvlcnt ;
      private short gxTv_SdtContratante_Contratante_przaortr ;
      private short gxTv_SdtContratante_Contratante_przactrtr ;
      private short gxTv_SdtContratante_Initialized ;
      private short gxTv_SdtContratante_Contratante_emailsdaport_Z ;
      private short gxTv_SdtContratante_Contratante_emailsdasec_Z ;
      private short gxTv_SdtContratante_Contratante_propostanvlcnt_Z ;
      private short gxTv_SdtContratante_Contratante_przaortr_Z ;
      private short gxTv_SdtContratante_Contratante_przactrtr_Z ;
      private short gxTv_SdtContratante_Contratante_codigo_N ;
      private short gxTv_SdtContratante_Contratante_cnpj_N ;
      private short gxTv_SdtContratante_Contratante_razaosocial_N ;
      private short gxTv_SdtContratante_Contratante_website_N ;
      private short gxTv_SdtContratante_Contratante_email_N ;
      private short gxTv_SdtContratante_Contratante_ramal_N ;
      private short gxTv_SdtContratante_Contratante_fax_N ;
      private short gxTv_SdtContratante_Contratante_agencianome_N ;
      private short gxTv_SdtContratante_Contratante_agencianro_N ;
      private short gxTv_SdtContratante_Contratante_banconome_N ;
      private short gxTv_SdtContratante_Contratante_banconro_N ;
      private short gxTv_SdtContratante_Contratante_contacorrente_N ;
      private short gxTv_SdtContratante_Municipio_codigo_N ;
      private short gxTv_SdtContratante_Contratante_emailsdahost_N ;
      private short gxTv_SdtContratante_Contratante_emailsdauser_N ;
      private short gxTv_SdtContratante_Contratante_emailsdapass_N ;
      private short gxTv_SdtContratante_Contratante_emailsdakey_N ;
      private short gxTv_SdtContratante_Contratante_emailsdaaut_N ;
      private short gxTv_SdtContratante_Contratante_emailsdaport_N ;
      private short gxTv_SdtContratante_Contratante_emailsdasec_N ;
      private short gxTv_SdtContratante_Contratante_ssautomatica_N ;
      private short gxTv_SdtContratante_Contratante_logoarquivo_N ;
      private short gxTv_SdtContratante_Contratante_logonomearq_N ;
      private short gxTv_SdtContratante_Contratante_logotipoarq_N ;
      private short gxTv_SdtContratante_Contratante_iniciodoexpediente_N ;
      private short gxTv_SdtContratante_Contratante_fimdoexpediente_N ;
      private short gxTv_SdtContratante_Contratante_servicosspadrao_N ;
      private short gxTv_SdtContratante_Contratante_retornass_N ;
      private short gxTv_SdtContratante_Contratante_ssstatusplanejamento_N ;
      private short gxTv_SdtContratante_Contratante_ssprestadoraunica_N ;
      private short gxTv_SdtContratante_Contratante_propostarqrsrv_N ;
      private short gxTv_SdtContratante_Contratante_propostarqrprz_N ;
      private short gxTv_SdtContratante_Contratante_propostarqresf_N ;
      private short gxTv_SdtContratante_Contratante_propostarqrcnt_N ;
      private short gxTv_SdtContratante_Contratante_propostanvlcnt_N ;
      private short gxTv_SdtContratante_Contratante_osgeraos_N ;
      private short gxTv_SdtContratante_Contratante_osgeratrp_N ;
      private short gxTv_SdtContratante_Contratante_osgerath_N ;
      private short gxTv_SdtContratante_Contratante_osgeratrd_N ;
      private short gxTv_SdtContratante_Contratante_osgeratr_N ;
      private short gxTv_SdtContratante_Contratante_oshmlgcompnd_N ;
      private short gxTv_SdtContratante_Contratante_ativocirculante_N ;
      private short gxTv_SdtContratante_Contratante_passivocirculante_N ;
      private short gxTv_SdtContratante_Contratante_patrimonioliquido_N ;
      private short gxTv_SdtContratante_Contratante_receitabruta_N ;
      private short gxTv_SdtContratante_Contratante_usaosistema_N ;
      private short gxTv_SdtContratante_Contratante_ttlrltgerencial_N ;
      private short gxTv_SdtContratante_Contratante_przaortr_N ;
      private short gxTv_SdtContratante_Contratante_przactrtr_N ;
      private short gxTv_SdtContratante_Contratante_requerorigem_N ;
      private short gxTv_SdtContratante_Contratante_sigla_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContratante_Contratante_codigo ;
      private int gxTv_SdtContratante_Contratante_pessoacod ;
      private int gxTv_SdtContratante_Municipio_codigo ;
      private int gxTv_SdtContratante_Contratante_servicosspadrao ;
      private int gxTv_SdtContratante_Contratante_codigo_Z ;
      private int gxTv_SdtContratante_Contratante_pessoacod_Z ;
      private int gxTv_SdtContratante_Municipio_codigo_Z ;
      private int gxTv_SdtContratante_Contratante_servicosspadrao_Z ;
      private decimal gxTv_SdtContratante_Contratante_ativocirculante ;
      private decimal gxTv_SdtContratante_Contratante_passivocirculante ;
      private decimal gxTv_SdtContratante_Contratante_patrimonioliquido ;
      private decimal gxTv_SdtContratante_Contratante_receitabruta ;
      private decimal gxTv_SdtContratante_Contratante_ativocirculante_Z ;
      private decimal gxTv_SdtContratante_Contratante_passivocirculante_Z ;
      private decimal gxTv_SdtContratante_Contratante_patrimonioliquido_Z ;
      private decimal gxTv_SdtContratante_Contratante_receitabruta_Z ;
      private String gxTv_SdtContratante_Contratante_razaosocial ;
      private String gxTv_SdtContratante_Contratante_nomefantasia ;
      private String gxTv_SdtContratante_Contratante_ie ;
      private String gxTv_SdtContratante_Contratante_telefone ;
      private String gxTv_SdtContratante_Contratante_ramal ;
      private String gxTv_SdtContratante_Contratante_fax ;
      private String gxTv_SdtContratante_Contratante_agencianome ;
      private String gxTv_SdtContratante_Contratante_agencianro ;
      private String gxTv_SdtContratante_Contratante_banconome ;
      private String gxTv_SdtContratante_Contratante_banconro ;
      private String gxTv_SdtContratante_Contratante_contacorrente ;
      private String gxTv_SdtContratante_Municipio_nome ;
      private String gxTv_SdtContratante_Estado_uf ;
      private String gxTv_SdtContratante_Contratante_emailsdakey ;
      private String gxTv_SdtContratante_Contratante_logonomearq ;
      private String gxTv_SdtContratante_Contratante_logotipoarq ;
      private String gxTv_SdtContratante_Contratante_ssstatusplanejamento ;
      private String gxTv_SdtContratante_Contratante_sigla ;
      private String gxTv_SdtContratante_Mode ;
      private String gxTv_SdtContratante_Contratante_razaosocial_Z ;
      private String gxTv_SdtContratante_Contratante_nomefantasia_Z ;
      private String gxTv_SdtContratante_Contratante_ie_Z ;
      private String gxTv_SdtContratante_Contratante_telefone_Z ;
      private String gxTv_SdtContratante_Contratante_ramal_Z ;
      private String gxTv_SdtContratante_Contratante_fax_Z ;
      private String gxTv_SdtContratante_Contratante_agencianome_Z ;
      private String gxTv_SdtContratante_Contratante_agencianro_Z ;
      private String gxTv_SdtContratante_Contratante_banconome_Z ;
      private String gxTv_SdtContratante_Contratante_banconro_Z ;
      private String gxTv_SdtContratante_Contratante_contacorrente_Z ;
      private String gxTv_SdtContratante_Municipio_nome_Z ;
      private String gxTv_SdtContratante_Estado_uf_Z ;
      private String gxTv_SdtContratante_Contratante_emailsdakey_Z ;
      private String gxTv_SdtContratante_Contratante_logonomearq_Z ;
      private String gxTv_SdtContratante_Contratante_logotipoarq_Z ;
      private String gxTv_SdtContratante_Contratante_ssstatusplanejamento_Z ;
      private String gxTv_SdtContratante_Contratante_sigla_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtContratante_Contratante_iniciodoexpediente ;
      private DateTime gxTv_SdtContratante_Contratante_fimdoexpediente ;
      private DateTime gxTv_SdtContratante_Contratante_iniciodoexpediente_Z ;
      private DateTime gxTv_SdtContratante_Contratante_fimdoexpediente_Z ;
      private DateTime datetime_STZ ;
      private bool gxTv_SdtContratante_Contratante_ativo ;
      private bool gxTv_SdtContratante_Contratante_emailsdaaut ;
      private bool gxTv_SdtContratante_Contratante_osautomatica ;
      private bool gxTv_SdtContratante_Contratante_ssautomatica ;
      private bool gxTv_SdtContratante_Contratante_existeconferencia ;
      private bool gxTv_SdtContratante_Contratante_retornass ;
      private bool gxTv_SdtContratante_Contratante_ssprestadoraunica ;
      private bool gxTv_SdtContratante_Contratante_propostarqrsrv ;
      private bool gxTv_SdtContratante_Contratante_propostarqrprz ;
      private bool gxTv_SdtContratante_Contratante_propostarqresf ;
      private bool gxTv_SdtContratante_Contratante_propostarqrcnt ;
      private bool gxTv_SdtContratante_Contratante_osgeraos ;
      private bool gxTv_SdtContratante_Contratante_osgeratrp ;
      private bool gxTv_SdtContratante_Contratante_osgerath ;
      private bool gxTv_SdtContratante_Contratante_osgeratrd ;
      private bool gxTv_SdtContratante_Contratante_osgeratr ;
      private bool gxTv_SdtContratante_Contratante_oshmlgcompnd ;
      private bool gxTv_SdtContratante_Contratante_usaosistema ;
      private bool gxTv_SdtContratante_Contratante_requerorigem ;
      private bool gxTv_SdtContratante_Contratante_selecionaresponsavelos ;
      private bool gxTv_SdtContratante_Contratante_exibepf ;
      private bool gxTv_SdtContratante_Contratante_ativo_Z ;
      private bool gxTv_SdtContratante_Contratante_emailsdaaut_Z ;
      private bool gxTv_SdtContratante_Contratante_osautomatica_Z ;
      private bool gxTv_SdtContratante_Contratante_ssautomatica_Z ;
      private bool gxTv_SdtContratante_Contratante_existeconferencia_Z ;
      private bool gxTv_SdtContratante_Contratante_retornass_Z ;
      private bool gxTv_SdtContratante_Contratante_ssprestadoraunica_Z ;
      private bool gxTv_SdtContratante_Contratante_propostarqrsrv_Z ;
      private bool gxTv_SdtContratante_Contratante_propostarqrprz_Z ;
      private bool gxTv_SdtContratante_Contratante_propostarqresf_Z ;
      private bool gxTv_SdtContratante_Contratante_propostarqrcnt_Z ;
      private bool gxTv_SdtContratante_Contratante_osgeraos_Z ;
      private bool gxTv_SdtContratante_Contratante_osgeratrp_Z ;
      private bool gxTv_SdtContratante_Contratante_osgerath_Z ;
      private bool gxTv_SdtContratante_Contratante_osgeratrd_Z ;
      private bool gxTv_SdtContratante_Contratante_osgeratr_Z ;
      private bool gxTv_SdtContratante_Contratante_oshmlgcompnd_Z ;
      private bool gxTv_SdtContratante_Contratante_usaosistema_Z ;
      private bool gxTv_SdtContratante_Contratante_requerorigem_Z ;
      private bool gxTv_SdtContratante_Contratante_selecionaresponsavelos_Z ;
      private bool gxTv_SdtContratante_Contratante_exibepf_Z ;
      private String gxTv_SdtContratante_Contratante_cnpj ;
      private String gxTv_SdtContratante_Contratante_website ;
      private String gxTv_SdtContratante_Contratante_email ;
      private String gxTv_SdtContratante_Contratante_emailsdahost ;
      private String gxTv_SdtContratante_Contratante_emailsdauser ;
      private String gxTv_SdtContratante_Contratante_emailsdapass ;
      private String gxTv_SdtContratante_Contratante_ttlrltgerencial ;
      private String gxTv_SdtContratante_Contratante_cnpj_Z ;
      private String gxTv_SdtContratante_Contratante_website_Z ;
      private String gxTv_SdtContratante_Contratante_email_Z ;
      private String gxTv_SdtContratante_Contratante_emailsdahost_Z ;
      private String gxTv_SdtContratante_Contratante_emailsdauser_Z ;
      private String gxTv_SdtContratante_Contratante_emailsdapass_Z ;
      private String gxTv_SdtContratante_Contratante_ttlrltgerencial_Z ;
      private String gxTv_SdtContratante_Contratante_logoarquivo ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Contratante", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtContratante_RESTInterface : GxGenericCollectionItem<SdtContratante>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratante_RESTInterface( ) : base()
      {
      }

      public SdtContratante_RESTInterface( SdtContratante psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Contratante_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratante_codigo
      {
         get {
            return sdt.gxTpr_Contratante_codigo ;
         }

         set {
            sdt.gxTpr_Contratante_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratante_PessoaCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratante_pessoacod
      {
         get {
            return sdt.gxTpr_Contratante_pessoacod ;
         }

         set {
            sdt.gxTpr_Contratante_pessoacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratante_CNPJ" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contratante_cnpj
      {
         get {
            return sdt.gxTpr_Contratante_cnpj ;
         }

         set {
            sdt.gxTpr_Contratante_cnpj = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_RazaoSocial" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Contratante_razaosocial
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_razaosocial) ;
         }

         set {
            sdt.gxTpr_Contratante_razaosocial = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_NomeFantasia" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Contratante_nomefantasia
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_nomefantasia) ;
         }

         set {
            sdt.gxTpr_Contratante_nomefantasia = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_IE" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Contratante_ie
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_ie) ;
         }

         set {
            sdt.gxTpr_Contratante_ie = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_WebSite" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Contratante_website
      {
         get {
            return sdt.gxTpr_Contratante_website ;
         }

         set {
            sdt.gxTpr_Contratante_website = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_Email" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Contratante_email
      {
         get {
            return sdt.gxTpr_Contratante_email ;
         }

         set {
            sdt.gxTpr_Contratante_email = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_Telefone" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Contratante_telefone
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_telefone) ;
         }

         set {
            sdt.gxTpr_Contratante_telefone = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_Ramal" , Order = 9 )]
      [GxSeudo()]
      public String gxTpr_Contratante_ramal
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_ramal) ;
         }

         set {
            sdt.gxTpr_Contratante_ramal = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_Fax" , Order = 10 )]
      [GxSeudo()]
      public String gxTpr_Contratante_fax
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_fax) ;
         }

         set {
            sdt.gxTpr_Contratante_fax = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_AgenciaNome" , Order = 11 )]
      [GxSeudo()]
      public String gxTpr_Contratante_agencianome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_agencianome) ;
         }

         set {
            sdt.gxTpr_Contratante_agencianome = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_AgenciaNro" , Order = 12 )]
      [GxSeudo()]
      public String gxTpr_Contratante_agencianro
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_agencianro) ;
         }

         set {
            sdt.gxTpr_Contratante_agencianro = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_BancoNome" , Order = 13 )]
      [GxSeudo()]
      public String gxTpr_Contratante_banconome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_banconome) ;
         }

         set {
            sdt.gxTpr_Contratante_banconome = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_BancoNro" , Order = 14 )]
      [GxSeudo()]
      public String gxTpr_Contratante_banconro
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_banconro) ;
         }

         set {
            sdt.gxTpr_Contratante_banconro = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_ContaCorrente" , Order = 15 )]
      [GxSeudo()]
      public String gxTpr_Contratante_contacorrente
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_contacorrente) ;
         }

         set {
            sdt.gxTpr_Contratante_contacorrente = (String)(value);
         }

      }

      [DataMember( Name = "Municipio_Codigo" , Order = 16 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Municipio_codigo
      {
         get {
            return sdt.gxTpr_Municipio_codigo ;
         }

         set {
            sdt.gxTpr_Municipio_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Municipio_Nome" , Order = 17 )]
      [GxSeudo()]
      public String gxTpr_Municipio_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Municipio_nome) ;
         }

         set {
            sdt.gxTpr_Municipio_nome = (String)(value);
         }

      }

      [DataMember( Name = "Estado_UF" , Order = 18 )]
      [GxSeudo()]
      public String gxTpr_Estado_uf
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Estado_uf) ;
         }

         set {
            sdt.gxTpr_Estado_uf = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_Ativo" , Order = 19 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_ativo
      {
         get {
            return sdt.gxTpr_Contratante_ativo ;
         }

         set {
            sdt.gxTpr_Contratante_ativo = value;
         }

      }

      [DataMember( Name = "Contratante_EmailSdaHost" , Order = 20 )]
      [GxSeudo()]
      public String gxTpr_Contratante_emailsdahost
      {
         get {
            return sdt.gxTpr_Contratante_emailsdahost ;
         }

         set {
            sdt.gxTpr_Contratante_emailsdahost = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_EmailSdaUser" , Order = 21 )]
      [GxSeudo()]
      public String gxTpr_Contratante_emailsdauser
      {
         get {
            return sdt.gxTpr_Contratante_emailsdauser ;
         }

         set {
            sdt.gxTpr_Contratante_emailsdauser = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_EmailSdaPass" , Order = 22 )]
      [GxSeudo()]
      public String gxTpr_Contratante_emailsdapass
      {
         get {
            return sdt.gxTpr_Contratante_emailsdapass ;
         }

         set {
            sdt.gxTpr_Contratante_emailsdapass = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_EmailSdaKey" , Order = 23 )]
      [GxSeudo()]
      public String gxTpr_Contratante_emailsdakey
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_emailsdakey) ;
         }

         set {
            sdt.gxTpr_Contratante_emailsdakey = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_EmailSdaAut" , Order = 24 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_emailsdaaut
      {
         get {
            return sdt.gxTpr_Contratante_emailsdaaut ;
         }

         set {
            sdt.gxTpr_Contratante_emailsdaaut = value;
         }

      }

      [DataMember( Name = "Contratante_EmailSdaPort" , Order = 25 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratante_emailsdaport
      {
         get {
            return sdt.gxTpr_Contratante_emailsdaport ;
         }

         set {
            sdt.gxTpr_Contratante_emailsdaport = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratante_EmailSdaSec" , Order = 26 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratante_emailsdasec
      {
         get {
            return sdt.gxTpr_Contratante_emailsdasec ;
         }

         set {
            sdt.gxTpr_Contratante_emailsdasec = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratante_OSAutomatica" , Order = 27 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_osautomatica
      {
         get {
            return sdt.gxTpr_Contratante_osautomatica ;
         }

         set {
            sdt.gxTpr_Contratante_osautomatica = value;
         }

      }

      [DataMember( Name = "Contratante_SSAutomatica" , Order = 28 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_ssautomatica
      {
         get {
            return sdt.gxTpr_Contratante_ssautomatica ;
         }

         set {
            sdt.gxTpr_Contratante_ssautomatica = value;
         }

      }

      [DataMember( Name = "Contratante_ExisteConferencia" , Order = 29 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_existeconferencia
      {
         get {
            return sdt.gxTpr_Contratante_existeconferencia ;
         }

         set {
            sdt.gxTpr_Contratante_existeconferencia = value;
         }

      }

      [DataMember( Name = "Contratante_LogoArquivo" , Order = 30 )]
      [GxUpload()]
      public String gxTpr_Contratante_logoarquivo
      {
         get {
            return PathUtil.RelativePath( sdt.gxTpr_Contratante_logoarquivo) ;
         }

         set {
            sdt.gxTpr_Contratante_logoarquivo = value;
         }

      }

      [DataMember( Name = "Contratante_LogoNomeArq" , Order = 31 )]
      public String gxTpr_Contratante_logonomearq
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_logonomearq) ;
         }

         set {
            sdt.gxTpr_Contratante_logonomearq = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_LogoTipoArq" , Order = 32 )]
      public String gxTpr_Contratante_logotipoarq
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_logotipoarq) ;
         }

         set {
            sdt.gxTpr_Contratante_logotipoarq = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_InicioDoExpediente" , Order = 33 )]
      [GxSeudo()]
      public String gxTpr_Contratante_iniciodoexpediente
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contratante_iniciodoexpediente) ;
         }

         set {
            GXt_dtime1 = DateTimeUtil.ResetDate(DateTimeUtil.CToT2( (String)(value)));
            sdt.gxTpr_Contratante_iniciodoexpediente = GXt_dtime1;
         }

      }

      [DataMember( Name = "Contratante_FimDoExpediente" , Order = 34 )]
      [GxSeudo()]
      public String gxTpr_Contratante_fimdoexpediente
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contratante_fimdoexpediente) ;
         }

         set {
            GXt_dtime1 = DateTimeUtil.ResetDate(DateTimeUtil.CToT2( (String)(value)));
            sdt.gxTpr_Contratante_fimdoexpediente = GXt_dtime1;
         }

      }

      [DataMember( Name = "Contratante_ServicoSSPadrao" , Order = 35 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratante_servicosspadrao
      {
         get {
            return sdt.gxTpr_Contratante_servicosspadrao ;
         }

         set {
            sdt.gxTpr_Contratante_servicosspadrao = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratante_RetornaSS" , Order = 36 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_retornass
      {
         get {
            return sdt.gxTpr_Contratante_retornass ;
         }

         set {
            sdt.gxTpr_Contratante_retornass = value;
         }

      }

      [DataMember( Name = "Contratante_SSStatusPlanejamento" , Order = 37 )]
      [GxSeudo()]
      public String gxTpr_Contratante_ssstatusplanejamento
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_ssstatusplanejamento) ;
         }

         set {
            sdt.gxTpr_Contratante_ssstatusplanejamento = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_SSPrestadoraUnica" , Order = 38 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_ssprestadoraunica
      {
         get {
            return sdt.gxTpr_Contratante_ssprestadoraunica ;
         }

         set {
            sdt.gxTpr_Contratante_ssprestadoraunica = value;
         }

      }

      [DataMember( Name = "Contratante_PropostaRqrSrv" , Order = 39 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_propostarqrsrv
      {
         get {
            return sdt.gxTpr_Contratante_propostarqrsrv ;
         }

         set {
            sdt.gxTpr_Contratante_propostarqrsrv = value;
         }

      }

      [DataMember( Name = "Contratante_PropostaRqrPrz" , Order = 40 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_propostarqrprz
      {
         get {
            return sdt.gxTpr_Contratante_propostarqrprz ;
         }

         set {
            sdt.gxTpr_Contratante_propostarqrprz = value;
         }

      }

      [DataMember( Name = "Contratante_PropostaRqrEsf" , Order = 41 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_propostarqresf
      {
         get {
            return sdt.gxTpr_Contratante_propostarqresf ;
         }

         set {
            sdt.gxTpr_Contratante_propostarqresf = value;
         }

      }

      [DataMember( Name = "Contratante_PropostaRqrCnt" , Order = 42 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_propostarqrcnt
      {
         get {
            return sdt.gxTpr_Contratante_propostarqrcnt ;
         }

         set {
            sdt.gxTpr_Contratante_propostarqrcnt = value;
         }

      }

      [DataMember( Name = "Contratante_PropostaNvlCnt" , Order = 43 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratante_propostanvlcnt
      {
         get {
            return sdt.gxTpr_Contratante_propostanvlcnt ;
         }

         set {
            sdt.gxTpr_Contratante_propostanvlcnt = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratante_OSGeraOS" , Order = 44 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_osgeraos
      {
         get {
            return sdt.gxTpr_Contratante_osgeraos ;
         }

         set {
            sdt.gxTpr_Contratante_osgeraos = value;
         }

      }

      [DataMember( Name = "Contratante_OSGeraTRP" , Order = 45 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_osgeratrp
      {
         get {
            return sdt.gxTpr_Contratante_osgeratrp ;
         }

         set {
            sdt.gxTpr_Contratante_osgeratrp = value;
         }

      }

      [DataMember( Name = "Contratante_OSGeraTH" , Order = 46 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_osgerath
      {
         get {
            return sdt.gxTpr_Contratante_osgerath ;
         }

         set {
            sdt.gxTpr_Contratante_osgerath = value;
         }

      }

      [DataMember( Name = "Contratante_OSGeraTRD" , Order = 47 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_osgeratrd
      {
         get {
            return sdt.gxTpr_Contratante_osgeratrd ;
         }

         set {
            sdt.gxTpr_Contratante_osgeratrd = value;
         }

      }

      [DataMember( Name = "Contratante_OSGeraTR" , Order = 48 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_osgeratr
      {
         get {
            return sdt.gxTpr_Contratante_osgeratr ;
         }

         set {
            sdt.gxTpr_Contratante_osgeratr = value;
         }

      }

      [DataMember( Name = "Contratante_OSHmlgComPnd" , Order = 49 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_oshmlgcompnd
      {
         get {
            return sdt.gxTpr_Contratante_oshmlgcompnd ;
         }

         set {
            sdt.gxTpr_Contratante_oshmlgcompnd = value;
         }

      }

      [DataMember( Name = "Contratante_AtivoCirculante" , Order = 50 )]
      [GxSeudo()]
      public String gxTpr_Contratante_ativocirculante
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contratante_ativocirculante, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contratante_ativocirculante = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Contratante_PassivoCirculante" , Order = 51 )]
      [GxSeudo()]
      public String gxTpr_Contratante_passivocirculante
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contratante_passivocirculante, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contratante_passivocirculante = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Contratante_PatrimonioLiquido" , Order = 52 )]
      [GxSeudo()]
      public String gxTpr_Contratante_patrimonioliquido
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contratante_patrimonioliquido, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contratante_patrimonioliquido = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Contratante_ReceitaBruta" , Order = 53 )]
      [GxSeudo()]
      public String gxTpr_Contratante_receitabruta
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contratante_receitabruta, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contratante_receitabruta = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Contratante_UsaOSistema" , Order = 54 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_usaosistema
      {
         get {
            return sdt.gxTpr_Contratante_usaosistema ;
         }

         set {
            sdt.gxTpr_Contratante_usaosistema = value;
         }

      }

      [DataMember( Name = "Contratante_TtlRltGerencial" , Order = 55 )]
      [GxSeudo()]
      public String gxTpr_Contratante_ttlrltgerencial
      {
         get {
            return sdt.gxTpr_Contratante_ttlrltgerencial ;
         }

         set {
            sdt.gxTpr_Contratante_ttlrltgerencial = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_PrzAoRtr" , Order = 56 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratante_przaortr
      {
         get {
            return sdt.gxTpr_Contratante_przaortr ;
         }

         set {
            sdt.gxTpr_Contratante_przaortr = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratante_PrzActRtr" , Order = 57 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratante_przactrtr
      {
         get {
            return sdt.gxTpr_Contratante_przactrtr ;
         }

         set {
            sdt.gxTpr_Contratante_przactrtr = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratante_RequerOrigem" , Order = 58 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_requerorigem
      {
         get {
            return sdt.gxTpr_Contratante_requerorigem ;
         }

         set {
            sdt.gxTpr_Contratante_requerorigem = value;
         }

      }

      [DataMember( Name = "Contratante_SelecionaResponsavelOS" , Order = 59 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_selecionaresponsavelos
      {
         get {
            return sdt.gxTpr_Contratante_selecionaresponsavelos ;
         }

         set {
            sdt.gxTpr_Contratante_selecionaresponsavelos = value;
         }

      }

      [DataMember( Name = "Contratante_ExibePF" , Order = 60 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_exibepf
      {
         get {
            return sdt.gxTpr_Contratante_exibepf ;
         }

         set {
            sdt.gxTpr_Contratante_exibepf = value;
         }

      }

      [DataMember( Name = "Contratante_Sigla" , Order = 61 )]
      [GxSeudo()]
      public String gxTpr_Contratante_sigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_sigla) ;
         }

         set {
            sdt.gxTpr_Contratante_sigla = (String)(value);
         }

      }

      public SdtContratante sdt
      {
         get {
            return (SdtContratante)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContratante() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 176 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
      private DateTime GXt_dtime1 ;
   }

}
