/*
               File: GetWWProjetoFilterData
        Description: Get WWProjeto Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/15/2020 20:47:58.61
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwprojetofilterdata : GXProcedure
   {
      public getwwprojetofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwprojetofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV25DDOName = aP0_DDOName;
         this.AV23SearchTxt = aP1_SearchTxt;
         this.AV24SearchTxtTo = aP2_SearchTxtTo;
         this.AV29OptionsJson = "" ;
         this.AV32OptionsDescJson = "" ;
         this.AV34OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV29OptionsJson;
         aP4_OptionsDescJson=this.AV32OptionsDescJson;
         aP5_OptionIndexesJson=this.AV34OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV25DDOName = aP0_DDOName;
         this.AV23SearchTxt = aP1_SearchTxt;
         this.AV24SearchTxtTo = aP2_SearchTxtTo;
         this.AV29OptionsJson = "" ;
         this.AV32OptionsDescJson = "" ;
         this.AV34OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV29OptionsJson;
         aP4_OptionsDescJson=this.AV32OptionsDescJson;
         aP5_OptionIndexesJson=this.AV34OptionIndexesJson;
         return AV34OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwprojetofilterdata objgetwwprojetofilterdata;
         objgetwwprojetofilterdata = new getwwprojetofilterdata();
         objgetwwprojetofilterdata.AV25DDOName = aP0_DDOName;
         objgetwwprojetofilterdata.AV23SearchTxt = aP1_SearchTxt;
         objgetwwprojetofilterdata.AV24SearchTxtTo = aP2_SearchTxtTo;
         objgetwwprojetofilterdata.AV29OptionsJson = "" ;
         objgetwwprojetofilterdata.AV32OptionsDescJson = "" ;
         objgetwwprojetofilterdata.AV34OptionIndexesJson = "" ;
         objgetwwprojetofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwprojetofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwprojetofilterdata);
         aP3_OptionsJson=this.AV29OptionsJson;
         aP4_OptionsDescJson=this.AV32OptionsDescJson;
         aP5_OptionIndexesJson=this.AV34OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwprojetofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV28Options = (IGxCollection)(new GxSimpleCollection());
         AV31OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV33OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV25DDOName), "DDO_PROJETO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADPROJETO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV25DDOName), "DDO_PROJETO_SIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADPROJETO_SIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV29OptionsJson = AV28Options.ToJSonString(false);
         AV32OptionsDescJson = AV31OptionsDesc.ToJSonString(false);
         AV34OptionIndexesJson = AV33OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV36Session.Get("WWProjetoGridState"), "") == 0 )
         {
            AV38GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWProjetoGridState"), "");
         }
         else
         {
            AV38GridState.FromXml(AV36Session.Get("WWProjetoGridState"), "");
         }
         AV63GXV1 = 1;
         while ( AV63GXV1 <= AV38GridState.gxTpr_Filtervalues.Count )
         {
            AV39GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV38GridState.gxTpr_Filtervalues.Item(AV63GXV1));
            if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFPROJETO_NOME") == 0 )
            {
               AV10TFProjeto_Nome = AV39GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFPROJETO_NOME_SEL") == 0 )
            {
               AV11TFProjeto_Nome_Sel = AV39GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFPROJETO_SIGLA") == 0 )
            {
               AV12TFProjeto_Sigla = AV39GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFPROJETO_SIGLA_SEL") == 0 )
            {
               AV13TFProjeto_Sigla_Sel = AV39GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFPROJETO_PRAZO") == 0 )
            {
               AV14TFProjeto_Prazo = (short)(NumberUtil.Val( AV39GridStateFilterValue.gxTpr_Value, "."));
               AV15TFProjeto_Prazo_To = (short)(NumberUtil.Val( AV39GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFPROJETO_CUSTO") == 0 )
            {
               AV16TFProjeto_Custo = NumberUtil.Val( AV39GridStateFilterValue.gxTpr_Value, ".");
               AV17TFProjeto_Custo_To = NumberUtil.Val( AV39GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFPROJETO_INCREMENTAL_SEL") == 0 )
            {
               AV18TFProjeto_Incremental_Sel = (short)(NumberUtil.Val( AV39GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFPROJETO_ESFORCO") == 0 )
            {
               AV19TFProjeto_Esforco = (short)(NumberUtil.Val( AV39GridStateFilterValue.gxTpr_Value, "."));
               AV20TFProjeto_Esforco_To = (short)(NumberUtil.Val( AV39GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFPROJETO_STATUS_SEL") == 0 )
            {
               AV21TFProjeto_Status_SelsJson = AV39GridStateFilterValue.gxTpr_Value;
               AV22TFProjeto_Status_Sels.FromJSonString(AV21TFProjeto_Status_SelsJson);
            }
            AV63GXV1 = (int)(AV63GXV1+1);
         }
         if ( AV38GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV40GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV38GridState.gxTpr_Dynamicfilters.Item(1));
            AV41DynamicFiltersSelector1 = AV40GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "PROJETO_SIGLA") == 0 )
            {
               AV42Projeto_Sigla1 = AV40GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "PROJETO_NOME") == 0 )
            {
               AV43Projeto_Nome1 = AV40GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "PROJETO_TIPOCONTAGEM") == 0 )
            {
               AV44Projeto_TipoContagem1 = AV40GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "PROJETO_TECNICACONTAGEM") == 0 )
            {
               AV45Projeto_TecnicaContagem1 = AV40GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "PROJETO_STATUS") == 0 )
            {
               AV46Projeto_Status1 = AV40GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV38GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV47DynamicFiltersEnabled2 = true;
               AV40GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV38GridState.gxTpr_Dynamicfilters.Item(2));
               AV48DynamicFiltersSelector2 = AV40GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV48DynamicFiltersSelector2, "PROJETO_SIGLA") == 0 )
               {
                  AV49Projeto_Sigla2 = AV40GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector2, "PROJETO_NOME") == 0 )
               {
                  AV50Projeto_Nome2 = AV40GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector2, "PROJETO_TIPOCONTAGEM") == 0 )
               {
                  AV51Projeto_TipoContagem2 = AV40GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector2, "PROJETO_TECNICACONTAGEM") == 0 )
               {
                  AV52Projeto_TecnicaContagem2 = AV40GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector2, "PROJETO_STATUS") == 0 )
               {
                  AV53Projeto_Status2 = AV40GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV38GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV54DynamicFiltersEnabled3 = true;
                  AV40GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV38GridState.gxTpr_Dynamicfilters.Item(3));
                  AV55DynamicFiltersSelector3 = AV40GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV55DynamicFiltersSelector3, "PROJETO_SIGLA") == 0 )
                  {
                     AV56Projeto_Sigla3 = AV40GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector3, "PROJETO_NOME") == 0 )
                  {
                     AV57Projeto_Nome3 = AV40GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector3, "PROJETO_TIPOCONTAGEM") == 0 )
                  {
                     AV58Projeto_TipoContagem3 = AV40GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector3, "PROJETO_TECNICACONTAGEM") == 0 )
                  {
                     AV59Projeto_TecnicaContagem3 = AV40GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector3, "PROJETO_STATUS") == 0 )
                  {
                     AV60Projeto_Status3 = AV40GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADPROJETO_NOMEOPTIONS' Routine */
         AV10TFProjeto_Nome = AV23SearchTxt;
         AV11TFProjeto_Nome_Sel = "";
         AV65WWProjetoDS_1_Dynamicfiltersselector1 = AV41DynamicFiltersSelector1;
         AV66WWProjetoDS_2_Projeto_sigla1 = AV42Projeto_Sigla1;
         AV67WWProjetoDS_3_Projeto_nome1 = AV43Projeto_Nome1;
         AV68WWProjetoDS_4_Projeto_tipocontagem1 = AV44Projeto_TipoContagem1;
         AV69WWProjetoDS_5_Projeto_tecnicacontagem1 = AV45Projeto_TecnicaContagem1;
         AV70WWProjetoDS_6_Projeto_status1 = AV46Projeto_Status1;
         AV71WWProjetoDS_7_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV72WWProjetoDS_8_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV73WWProjetoDS_9_Projeto_sigla2 = AV49Projeto_Sigla2;
         AV74WWProjetoDS_10_Projeto_nome2 = AV50Projeto_Nome2;
         AV75WWProjetoDS_11_Projeto_tipocontagem2 = AV51Projeto_TipoContagem2;
         AV76WWProjetoDS_12_Projeto_tecnicacontagem2 = AV52Projeto_TecnicaContagem2;
         AV77WWProjetoDS_13_Projeto_status2 = AV53Projeto_Status2;
         AV78WWProjetoDS_14_Dynamicfiltersenabled3 = AV54DynamicFiltersEnabled3;
         AV79WWProjetoDS_15_Dynamicfiltersselector3 = AV55DynamicFiltersSelector3;
         AV80WWProjetoDS_16_Projeto_sigla3 = AV56Projeto_Sigla3;
         AV81WWProjetoDS_17_Projeto_nome3 = AV57Projeto_Nome3;
         AV82WWProjetoDS_18_Projeto_tipocontagem3 = AV58Projeto_TipoContagem3;
         AV83WWProjetoDS_19_Projeto_tecnicacontagem3 = AV59Projeto_TecnicaContagem3;
         AV84WWProjetoDS_20_Projeto_status3 = AV60Projeto_Status3;
         AV85WWProjetoDS_21_Tfprojeto_nome = AV10TFProjeto_Nome;
         AV86WWProjetoDS_22_Tfprojeto_nome_sel = AV11TFProjeto_Nome_Sel;
         AV87WWProjetoDS_23_Tfprojeto_sigla = AV12TFProjeto_Sigla;
         AV88WWProjetoDS_24_Tfprojeto_sigla_sel = AV13TFProjeto_Sigla_Sel;
         AV89WWProjetoDS_25_Tfprojeto_prazo = AV14TFProjeto_Prazo;
         AV90WWProjetoDS_26_Tfprojeto_prazo_to = AV15TFProjeto_Prazo_To;
         AV91WWProjetoDS_27_Tfprojeto_custo = AV16TFProjeto_Custo;
         AV92WWProjetoDS_28_Tfprojeto_custo_to = AV17TFProjeto_Custo_To;
         AV93WWProjetoDS_29_Tfprojeto_incremental_sel = AV18TFProjeto_Incremental_Sel;
         AV94WWProjetoDS_30_Tfprojeto_esforco = AV19TFProjeto_Esforco;
         AV95WWProjetoDS_31_Tfprojeto_esforco_to = AV20TFProjeto_Esforco_To;
         AV96WWProjetoDS_32_Tfprojeto_status_sels = AV22TFProjeto_Status_Sels;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A658Projeto_Status ,
                                              AV96WWProjetoDS_32_Tfprojeto_status_sels ,
                                              AV65WWProjetoDS_1_Dynamicfiltersselector1 ,
                                              AV66WWProjetoDS_2_Projeto_sigla1 ,
                                              AV67WWProjetoDS_3_Projeto_nome1 ,
                                              AV68WWProjetoDS_4_Projeto_tipocontagem1 ,
                                              AV69WWProjetoDS_5_Projeto_tecnicacontagem1 ,
                                              AV70WWProjetoDS_6_Projeto_status1 ,
                                              AV71WWProjetoDS_7_Dynamicfiltersenabled2 ,
                                              AV72WWProjetoDS_8_Dynamicfiltersselector2 ,
                                              AV73WWProjetoDS_9_Projeto_sigla2 ,
                                              AV74WWProjetoDS_10_Projeto_nome2 ,
                                              AV75WWProjetoDS_11_Projeto_tipocontagem2 ,
                                              AV76WWProjetoDS_12_Projeto_tecnicacontagem2 ,
                                              AV77WWProjetoDS_13_Projeto_status2 ,
                                              AV78WWProjetoDS_14_Dynamicfiltersenabled3 ,
                                              AV79WWProjetoDS_15_Dynamicfiltersselector3 ,
                                              AV80WWProjetoDS_16_Projeto_sigla3 ,
                                              AV81WWProjetoDS_17_Projeto_nome3 ,
                                              AV82WWProjetoDS_18_Projeto_tipocontagem3 ,
                                              AV83WWProjetoDS_19_Projeto_tecnicacontagem3 ,
                                              AV84WWProjetoDS_20_Projeto_status3 ,
                                              AV86WWProjetoDS_22_Tfprojeto_nome_sel ,
                                              AV85WWProjetoDS_21_Tfprojeto_nome ,
                                              AV88WWProjetoDS_24_Tfprojeto_sigla_sel ,
                                              AV87WWProjetoDS_23_Tfprojeto_sigla ,
                                              AV89WWProjetoDS_25_Tfprojeto_prazo ,
                                              AV90WWProjetoDS_26_Tfprojeto_prazo_to ,
                                              AV91WWProjetoDS_27_Tfprojeto_custo ,
                                              AV92WWProjetoDS_28_Tfprojeto_custo_to ,
                                              AV93WWProjetoDS_29_Tfprojeto_incremental_sel ,
                                              AV94WWProjetoDS_30_Tfprojeto_esforco ,
                                              AV95WWProjetoDS_31_Tfprojeto_esforco_to ,
                                              AV96WWProjetoDS_32_Tfprojeto_status_sels.Count ,
                                              A650Projeto_Sigla ,
                                              A649Projeto_Nome ,
                                              A651Projeto_TipoContagem ,
                                              A652Projeto_TecnicaContagem ,
                                              A656Projeto_Prazo ,
                                              A655Projeto_Custo ,
                                              A1232Projeto_Incremental ,
                                              A657Projeto_Esforco },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         lV66WWProjetoDS_2_Projeto_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV66WWProjetoDS_2_Projeto_sigla1), 15, "%");
         lV67WWProjetoDS_3_Projeto_nome1 = StringUtil.PadR( StringUtil.RTrim( AV67WWProjetoDS_3_Projeto_nome1), 50, "%");
         lV73WWProjetoDS_9_Projeto_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV73WWProjetoDS_9_Projeto_sigla2), 15, "%");
         lV74WWProjetoDS_10_Projeto_nome2 = StringUtil.PadR( StringUtil.RTrim( AV74WWProjetoDS_10_Projeto_nome2), 50, "%");
         lV80WWProjetoDS_16_Projeto_sigla3 = StringUtil.PadR( StringUtil.RTrim( AV80WWProjetoDS_16_Projeto_sigla3), 15, "%");
         lV81WWProjetoDS_17_Projeto_nome3 = StringUtil.PadR( StringUtil.RTrim( AV81WWProjetoDS_17_Projeto_nome3), 50, "%");
         lV85WWProjetoDS_21_Tfprojeto_nome = StringUtil.PadR( StringUtil.RTrim( AV85WWProjetoDS_21_Tfprojeto_nome), 50, "%");
         lV87WWProjetoDS_23_Tfprojeto_sigla = StringUtil.PadR( StringUtil.RTrim( AV87WWProjetoDS_23_Tfprojeto_sigla), 15, "%");
         /* Using cursor P00O43 */
         pr_default.execute(0, new Object[] {lV66WWProjetoDS_2_Projeto_sigla1, lV67WWProjetoDS_3_Projeto_nome1, AV68WWProjetoDS_4_Projeto_tipocontagem1, AV69WWProjetoDS_5_Projeto_tecnicacontagem1, AV70WWProjetoDS_6_Projeto_status1, lV73WWProjetoDS_9_Projeto_sigla2, lV74WWProjetoDS_10_Projeto_nome2, AV75WWProjetoDS_11_Projeto_tipocontagem2, AV76WWProjetoDS_12_Projeto_tecnicacontagem2, AV77WWProjetoDS_13_Projeto_status2, lV80WWProjetoDS_16_Projeto_sigla3, lV81WWProjetoDS_17_Projeto_nome3, AV82WWProjetoDS_18_Projeto_tipocontagem3, AV83WWProjetoDS_19_Projeto_tecnicacontagem3, AV84WWProjetoDS_20_Projeto_status3, lV85WWProjetoDS_21_Tfprojeto_nome, AV86WWProjetoDS_22_Tfprojeto_nome_sel, lV87WWProjetoDS_23_Tfprojeto_sigla, AV88WWProjetoDS_24_Tfprojeto_sigla_sel, AV89WWProjetoDS_25_Tfprojeto_prazo, AV90WWProjetoDS_26_Tfprojeto_prazo_to, AV91WWProjetoDS_27_Tfprojeto_custo, AV92WWProjetoDS_28_Tfprojeto_custo_to, AV94WWProjetoDS_30_Tfprojeto_esforco, AV95WWProjetoDS_31_Tfprojeto_esforco_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKO42 = false;
            A649Projeto_Nome = P00O43_A649Projeto_Nome[0];
            A1232Projeto_Incremental = P00O43_A1232Projeto_Incremental[0];
            n1232Projeto_Incremental = P00O43_n1232Projeto_Incremental[0];
            A658Projeto_Status = P00O43_A658Projeto_Status[0];
            A652Projeto_TecnicaContagem = P00O43_A652Projeto_TecnicaContagem[0];
            A651Projeto_TipoContagem = P00O43_A651Projeto_TipoContagem[0];
            A650Projeto_Sigla = P00O43_A650Projeto_Sigla[0];
            A648Projeto_Codigo = P00O43_A648Projeto_Codigo[0];
            A657Projeto_Esforco = P00O43_A657Projeto_Esforco[0];
            A655Projeto_Custo = P00O43_A655Projeto_Custo[0];
            A656Projeto_Prazo = P00O43_A656Projeto_Prazo[0];
            A657Projeto_Esforco = P00O43_A657Projeto_Esforco[0];
            A655Projeto_Custo = P00O43_A655Projeto_Custo[0];
            A656Projeto_Prazo = P00O43_A656Projeto_Prazo[0];
            AV35count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00O43_A649Projeto_Nome[0], A649Projeto_Nome) == 0 ) )
            {
               BRKO42 = false;
               A648Projeto_Codigo = P00O43_A648Projeto_Codigo[0];
               AV35count = (long)(AV35count+1);
               BRKO42 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A649Projeto_Nome)) )
            {
               AV27Option = A649Projeto_Nome;
               AV28Options.Add(AV27Option, 0);
               AV33OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV35count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV28Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKO42 )
            {
               BRKO42 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADPROJETO_SIGLAOPTIONS' Routine */
         AV12TFProjeto_Sigla = AV23SearchTxt;
         AV13TFProjeto_Sigla_Sel = "";
         AV65WWProjetoDS_1_Dynamicfiltersselector1 = AV41DynamicFiltersSelector1;
         AV66WWProjetoDS_2_Projeto_sigla1 = AV42Projeto_Sigla1;
         AV67WWProjetoDS_3_Projeto_nome1 = AV43Projeto_Nome1;
         AV68WWProjetoDS_4_Projeto_tipocontagem1 = AV44Projeto_TipoContagem1;
         AV69WWProjetoDS_5_Projeto_tecnicacontagem1 = AV45Projeto_TecnicaContagem1;
         AV70WWProjetoDS_6_Projeto_status1 = AV46Projeto_Status1;
         AV71WWProjetoDS_7_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV72WWProjetoDS_8_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV73WWProjetoDS_9_Projeto_sigla2 = AV49Projeto_Sigla2;
         AV74WWProjetoDS_10_Projeto_nome2 = AV50Projeto_Nome2;
         AV75WWProjetoDS_11_Projeto_tipocontagem2 = AV51Projeto_TipoContagem2;
         AV76WWProjetoDS_12_Projeto_tecnicacontagem2 = AV52Projeto_TecnicaContagem2;
         AV77WWProjetoDS_13_Projeto_status2 = AV53Projeto_Status2;
         AV78WWProjetoDS_14_Dynamicfiltersenabled3 = AV54DynamicFiltersEnabled3;
         AV79WWProjetoDS_15_Dynamicfiltersselector3 = AV55DynamicFiltersSelector3;
         AV80WWProjetoDS_16_Projeto_sigla3 = AV56Projeto_Sigla3;
         AV81WWProjetoDS_17_Projeto_nome3 = AV57Projeto_Nome3;
         AV82WWProjetoDS_18_Projeto_tipocontagem3 = AV58Projeto_TipoContagem3;
         AV83WWProjetoDS_19_Projeto_tecnicacontagem3 = AV59Projeto_TecnicaContagem3;
         AV84WWProjetoDS_20_Projeto_status3 = AV60Projeto_Status3;
         AV85WWProjetoDS_21_Tfprojeto_nome = AV10TFProjeto_Nome;
         AV86WWProjetoDS_22_Tfprojeto_nome_sel = AV11TFProjeto_Nome_Sel;
         AV87WWProjetoDS_23_Tfprojeto_sigla = AV12TFProjeto_Sigla;
         AV88WWProjetoDS_24_Tfprojeto_sigla_sel = AV13TFProjeto_Sigla_Sel;
         AV89WWProjetoDS_25_Tfprojeto_prazo = AV14TFProjeto_Prazo;
         AV90WWProjetoDS_26_Tfprojeto_prazo_to = AV15TFProjeto_Prazo_To;
         AV91WWProjetoDS_27_Tfprojeto_custo = AV16TFProjeto_Custo;
         AV92WWProjetoDS_28_Tfprojeto_custo_to = AV17TFProjeto_Custo_To;
         AV93WWProjetoDS_29_Tfprojeto_incremental_sel = AV18TFProjeto_Incremental_Sel;
         AV94WWProjetoDS_30_Tfprojeto_esforco = AV19TFProjeto_Esforco;
         AV95WWProjetoDS_31_Tfprojeto_esforco_to = AV20TFProjeto_Esforco_To;
         AV96WWProjetoDS_32_Tfprojeto_status_sels = AV22TFProjeto_Status_Sels;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A658Projeto_Status ,
                                              AV96WWProjetoDS_32_Tfprojeto_status_sels ,
                                              AV65WWProjetoDS_1_Dynamicfiltersselector1 ,
                                              AV66WWProjetoDS_2_Projeto_sigla1 ,
                                              AV67WWProjetoDS_3_Projeto_nome1 ,
                                              AV68WWProjetoDS_4_Projeto_tipocontagem1 ,
                                              AV69WWProjetoDS_5_Projeto_tecnicacontagem1 ,
                                              AV70WWProjetoDS_6_Projeto_status1 ,
                                              AV71WWProjetoDS_7_Dynamicfiltersenabled2 ,
                                              AV72WWProjetoDS_8_Dynamicfiltersselector2 ,
                                              AV73WWProjetoDS_9_Projeto_sigla2 ,
                                              AV74WWProjetoDS_10_Projeto_nome2 ,
                                              AV75WWProjetoDS_11_Projeto_tipocontagem2 ,
                                              AV76WWProjetoDS_12_Projeto_tecnicacontagem2 ,
                                              AV77WWProjetoDS_13_Projeto_status2 ,
                                              AV78WWProjetoDS_14_Dynamicfiltersenabled3 ,
                                              AV79WWProjetoDS_15_Dynamicfiltersselector3 ,
                                              AV80WWProjetoDS_16_Projeto_sigla3 ,
                                              AV81WWProjetoDS_17_Projeto_nome3 ,
                                              AV82WWProjetoDS_18_Projeto_tipocontagem3 ,
                                              AV83WWProjetoDS_19_Projeto_tecnicacontagem3 ,
                                              AV84WWProjetoDS_20_Projeto_status3 ,
                                              AV86WWProjetoDS_22_Tfprojeto_nome_sel ,
                                              AV85WWProjetoDS_21_Tfprojeto_nome ,
                                              AV88WWProjetoDS_24_Tfprojeto_sigla_sel ,
                                              AV87WWProjetoDS_23_Tfprojeto_sigla ,
                                              AV89WWProjetoDS_25_Tfprojeto_prazo ,
                                              AV90WWProjetoDS_26_Tfprojeto_prazo_to ,
                                              AV91WWProjetoDS_27_Tfprojeto_custo ,
                                              AV92WWProjetoDS_28_Tfprojeto_custo_to ,
                                              AV93WWProjetoDS_29_Tfprojeto_incremental_sel ,
                                              AV94WWProjetoDS_30_Tfprojeto_esforco ,
                                              AV95WWProjetoDS_31_Tfprojeto_esforco_to ,
                                              AV96WWProjetoDS_32_Tfprojeto_status_sels.Count ,
                                              A650Projeto_Sigla ,
                                              A649Projeto_Nome ,
                                              A651Projeto_TipoContagem ,
                                              A652Projeto_TecnicaContagem ,
                                              A656Projeto_Prazo ,
                                              A655Projeto_Custo ,
                                              A1232Projeto_Incremental ,
                                              A657Projeto_Esforco },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         lV66WWProjetoDS_2_Projeto_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV66WWProjetoDS_2_Projeto_sigla1), 15, "%");
         lV67WWProjetoDS_3_Projeto_nome1 = StringUtil.PadR( StringUtil.RTrim( AV67WWProjetoDS_3_Projeto_nome1), 50, "%");
         lV73WWProjetoDS_9_Projeto_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV73WWProjetoDS_9_Projeto_sigla2), 15, "%");
         lV74WWProjetoDS_10_Projeto_nome2 = StringUtil.PadR( StringUtil.RTrim( AV74WWProjetoDS_10_Projeto_nome2), 50, "%");
         lV80WWProjetoDS_16_Projeto_sigla3 = StringUtil.PadR( StringUtil.RTrim( AV80WWProjetoDS_16_Projeto_sigla3), 15, "%");
         lV81WWProjetoDS_17_Projeto_nome3 = StringUtil.PadR( StringUtil.RTrim( AV81WWProjetoDS_17_Projeto_nome3), 50, "%");
         lV85WWProjetoDS_21_Tfprojeto_nome = StringUtil.PadR( StringUtil.RTrim( AV85WWProjetoDS_21_Tfprojeto_nome), 50, "%");
         lV87WWProjetoDS_23_Tfprojeto_sigla = StringUtil.PadR( StringUtil.RTrim( AV87WWProjetoDS_23_Tfprojeto_sigla), 15, "%");
         /* Using cursor P00O45 */
         pr_default.execute(1, new Object[] {lV66WWProjetoDS_2_Projeto_sigla1, lV67WWProjetoDS_3_Projeto_nome1, AV68WWProjetoDS_4_Projeto_tipocontagem1, AV69WWProjetoDS_5_Projeto_tecnicacontagem1, AV70WWProjetoDS_6_Projeto_status1, lV73WWProjetoDS_9_Projeto_sigla2, lV74WWProjetoDS_10_Projeto_nome2, AV75WWProjetoDS_11_Projeto_tipocontagem2, AV76WWProjetoDS_12_Projeto_tecnicacontagem2, AV77WWProjetoDS_13_Projeto_status2, lV80WWProjetoDS_16_Projeto_sigla3, lV81WWProjetoDS_17_Projeto_nome3, AV82WWProjetoDS_18_Projeto_tipocontagem3, AV83WWProjetoDS_19_Projeto_tecnicacontagem3, AV84WWProjetoDS_20_Projeto_status3, lV85WWProjetoDS_21_Tfprojeto_nome, AV86WWProjetoDS_22_Tfprojeto_nome_sel, lV87WWProjetoDS_23_Tfprojeto_sigla, AV88WWProjetoDS_24_Tfprojeto_sigla_sel, AV89WWProjetoDS_25_Tfprojeto_prazo, AV90WWProjetoDS_26_Tfprojeto_prazo_to, AV91WWProjetoDS_27_Tfprojeto_custo, AV92WWProjetoDS_28_Tfprojeto_custo_to, AV94WWProjetoDS_30_Tfprojeto_esforco, AV95WWProjetoDS_31_Tfprojeto_esforco_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKO44 = false;
            A650Projeto_Sigla = P00O45_A650Projeto_Sigla[0];
            A1232Projeto_Incremental = P00O45_A1232Projeto_Incremental[0];
            n1232Projeto_Incremental = P00O45_n1232Projeto_Incremental[0];
            A658Projeto_Status = P00O45_A658Projeto_Status[0];
            A652Projeto_TecnicaContagem = P00O45_A652Projeto_TecnicaContagem[0];
            A651Projeto_TipoContagem = P00O45_A651Projeto_TipoContagem[0];
            A649Projeto_Nome = P00O45_A649Projeto_Nome[0];
            A648Projeto_Codigo = P00O45_A648Projeto_Codigo[0];
            A657Projeto_Esforco = P00O45_A657Projeto_Esforco[0];
            A655Projeto_Custo = P00O45_A655Projeto_Custo[0];
            A656Projeto_Prazo = P00O45_A656Projeto_Prazo[0];
            A657Projeto_Esforco = P00O45_A657Projeto_Esforco[0];
            A655Projeto_Custo = P00O45_A655Projeto_Custo[0];
            A656Projeto_Prazo = P00O45_A656Projeto_Prazo[0];
            AV35count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00O45_A650Projeto_Sigla[0], A650Projeto_Sigla) == 0 ) )
            {
               BRKO44 = false;
               A648Projeto_Codigo = P00O45_A648Projeto_Codigo[0];
               AV35count = (long)(AV35count+1);
               BRKO44 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A650Projeto_Sigla)) )
            {
               AV27Option = A650Projeto_Sigla;
               AV28Options.Add(AV27Option, 0);
               AV33OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV35count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV28Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKO44 )
            {
               BRKO44 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV28Options = new GxSimpleCollection();
         AV31OptionsDesc = new GxSimpleCollection();
         AV33OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV36Session = context.GetSession();
         AV38GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV39GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFProjeto_Nome = "";
         AV11TFProjeto_Nome_Sel = "";
         AV12TFProjeto_Sigla = "";
         AV13TFProjeto_Sigla_Sel = "";
         AV21TFProjeto_Status_SelsJson = "";
         AV22TFProjeto_Status_Sels = new GxSimpleCollection();
         AV40GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV41DynamicFiltersSelector1 = "";
         AV42Projeto_Sigla1 = "";
         AV43Projeto_Nome1 = "";
         AV44Projeto_TipoContagem1 = "";
         AV45Projeto_TecnicaContagem1 = "";
         AV46Projeto_Status1 = "A";
         AV48DynamicFiltersSelector2 = "";
         AV49Projeto_Sigla2 = "";
         AV50Projeto_Nome2 = "";
         AV51Projeto_TipoContagem2 = "";
         AV52Projeto_TecnicaContagem2 = "";
         AV53Projeto_Status2 = "A";
         AV55DynamicFiltersSelector3 = "";
         AV56Projeto_Sigla3 = "";
         AV57Projeto_Nome3 = "";
         AV58Projeto_TipoContagem3 = "";
         AV59Projeto_TecnicaContagem3 = "";
         AV60Projeto_Status3 = "A";
         AV65WWProjetoDS_1_Dynamicfiltersselector1 = "";
         AV66WWProjetoDS_2_Projeto_sigla1 = "";
         AV67WWProjetoDS_3_Projeto_nome1 = "";
         AV68WWProjetoDS_4_Projeto_tipocontagem1 = "";
         AV69WWProjetoDS_5_Projeto_tecnicacontagem1 = "";
         AV70WWProjetoDS_6_Projeto_status1 = "";
         AV72WWProjetoDS_8_Dynamicfiltersselector2 = "";
         AV73WWProjetoDS_9_Projeto_sigla2 = "";
         AV74WWProjetoDS_10_Projeto_nome2 = "";
         AV75WWProjetoDS_11_Projeto_tipocontagem2 = "";
         AV76WWProjetoDS_12_Projeto_tecnicacontagem2 = "";
         AV77WWProjetoDS_13_Projeto_status2 = "";
         AV79WWProjetoDS_15_Dynamicfiltersselector3 = "";
         AV80WWProjetoDS_16_Projeto_sigla3 = "";
         AV81WWProjetoDS_17_Projeto_nome3 = "";
         AV82WWProjetoDS_18_Projeto_tipocontagem3 = "";
         AV83WWProjetoDS_19_Projeto_tecnicacontagem3 = "";
         AV84WWProjetoDS_20_Projeto_status3 = "";
         AV85WWProjetoDS_21_Tfprojeto_nome = "";
         AV86WWProjetoDS_22_Tfprojeto_nome_sel = "";
         AV87WWProjetoDS_23_Tfprojeto_sigla = "";
         AV88WWProjetoDS_24_Tfprojeto_sigla_sel = "";
         AV96WWProjetoDS_32_Tfprojeto_status_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV66WWProjetoDS_2_Projeto_sigla1 = "";
         lV67WWProjetoDS_3_Projeto_nome1 = "";
         lV73WWProjetoDS_9_Projeto_sigla2 = "";
         lV74WWProjetoDS_10_Projeto_nome2 = "";
         lV80WWProjetoDS_16_Projeto_sigla3 = "";
         lV81WWProjetoDS_17_Projeto_nome3 = "";
         lV85WWProjetoDS_21_Tfprojeto_nome = "";
         lV87WWProjetoDS_23_Tfprojeto_sigla = "";
         A658Projeto_Status = "";
         A650Projeto_Sigla = "";
         A649Projeto_Nome = "";
         A651Projeto_TipoContagem = "";
         A652Projeto_TecnicaContagem = "";
         P00O43_A649Projeto_Nome = new String[] {""} ;
         P00O43_A1232Projeto_Incremental = new bool[] {false} ;
         P00O43_n1232Projeto_Incremental = new bool[] {false} ;
         P00O43_A658Projeto_Status = new String[] {""} ;
         P00O43_A652Projeto_TecnicaContagem = new String[] {""} ;
         P00O43_A651Projeto_TipoContagem = new String[] {""} ;
         P00O43_A650Projeto_Sigla = new String[] {""} ;
         P00O43_A648Projeto_Codigo = new int[1] ;
         P00O43_A657Projeto_Esforco = new short[1] ;
         P00O43_A655Projeto_Custo = new decimal[1] ;
         P00O43_A656Projeto_Prazo = new short[1] ;
         AV27Option = "";
         P00O45_A650Projeto_Sigla = new String[] {""} ;
         P00O45_A1232Projeto_Incremental = new bool[] {false} ;
         P00O45_n1232Projeto_Incremental = new bool[] {false} ;
         P00O45_A658Projeto_Status = new String[] {""} ;
         P00O45_A652Projeto_TecnicaContagem = new String[] {""} ;
         P00O45_A651Projeto_TipoContagem = new String[] {""} ;
         P00O45_A649Projeto_Nome = new String[] {""} ;
         P00O45_A648Projeto_Codigo = new int[1] ;
         P00O45_A657Projeto_Esforco = new short[1] ;
         P00O45_A655Projeto_Custo = new decimal[1] ;
         P00O45_A656Projeto_Prazo = new short[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwprojetofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00O43_A649Projeto_Nome, P00O43_A1232Projeto_Incremental, P00O43_n1232Projeto_Incremental, P00O43_A658Projeto_Status, P00O43_A652Projeto_TecnicaContagem, P00O43_A651Projeto_TipoContagem, P00O43_A650Projeto_Sigla, P00O43_A648Projeto_Codigo, P00O43_A657Projeto_Esforco, P00O43_A655Projeto_Custo,
               P00O43_A656Projeto_Prazo
               }
               , new Object[] {
               P00O45_A650Projeto_Sigla, P00O45_A1232Projeto_Incremental, P00O45_n1232Projeto_Incremental, P00O45_A658Projeto_Status, P00O45_A652Projeto_TecnicaContagem, P00O45_A651Projeto_TipoContagem, P00O45_A649Projeto_Nome, P00O45_A648Projeto_Codigo, P00O45_A657Projeto_Esforco, P00O45_A655Projeto_Custo,
               P00O45_A656Projeto_Prazo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14TFProjeto_Prazo ;
      private short AV15TFProjeto_Prazo_To ;
      private short AV18TFProjeto_Incremental_Sel ;
      private short AV19TFProjeto_Esforco ;
      private short AV20TFProjeto_Esforco_To ;
      private short AV89WWProjetoDS_25_Tfprojeto_prazo ;
      private short AV90WWProjetoDS_26_Tfprojeto_prazo_to ;
      private short AV93WWProjetoDS_29_Tfprojeto_incremental_sel ;
      private short AV94WWProjetoDS_30_Tfprojeto_esforco ;
      private short AV95WWProjetoDS_31_Tfprojeto_esforco_to ;
      private short A656Projeto_Prazo ;
      private short A657Projeto_Esforco ;
      private int AV63GXV1 ;
      private int AV96WWProjetoDS_32_Tfprojeto_status_sels_Count ;
      private int A648Projeto_Codigo ;
      private long AV35count ;
      private decimal AV16TFProjeto_Custo ;
      private decimal AV17TFProjeto_Custo_To ;
      private decimal AV91WWProjetoDS_27_Tfprojeto_custo ;
      private decimal AV92WWProjetoDS_28_Tfprojeto_custo_to ;
      private decimal A655Projeto_Custo ;
      private String AV10TFProjeto_Nome ;
      private String AV11TFProjeto_Nome_Sel ;
      private String AV12TFProjeto_Sigla ;
      private String AV13TFProjeto_Sigla_Sel ;
      private String AV42Projeto_Sigla1 ;
      private String AV43Projeto_Nome1 ;
      private String AV44Projeto_TipoContagem1 ;
      private String AV45Projeto_TecnicaContagem1 ;
      private String AV46Projeto_Status1 ;
      private String AV49Projeto_Sigla2 ;
      private String AV50Projeto_Nome2 ;
      private String AV51Projeto_TipoContagem2 ;
      private String AV52Projeto_TecnicaContagem2 ;
      private String AV53Projeto_Status2 ;
      private String AV56Projeto_Sigla3 ;
      private String AV57Projeto_Nome3 ;
      private String AV58Projeto_TipoContagem3 ;
      private String AV59Projeto_TecnicaContagem3 ;
      private String AV60Projeto_Status3 ;
      private String AV66WWProjetoDS_2_Projeto_sigla1 ;
      private String AV67WWProjetoDS_3_Projeto_nome1 ;
      private String AV68WWProjetoDS_4_Projeto_tipocontagem1 ;
      private String AV69WWProjetoDS_5_Projeto_tecnicacontagem1 ;
      private String AV70WWProjetoDS_6_Projeto_status1 ;
      private String AV73WWProjetoDS_9_Projeto_sigla2 ;
      private String AV74WWProjetoDS_10_Projeto_nome2 ;
      private String AV75WWProjetoDS_11_Projeto_tipocontagem2 ;
      private String AV76WWProjetoDS_12_Projeto_tecnicacontagem2 ;
      private String AV77WWProjetoDS_13_Projeto_status2 ;
      private String AV80WWProjetoDS_16_Projeto_sigla3 ;
      private String AV81WWProjetoDS_17_Projeto_nome3 ;
      private String AV82WWProjetoDS_18_Projeto_tipocontagem3 ;
      private String AV83WWProjetoDS_19_Projeto_tecnicacontagem3 ;
      private String AV84WWProjetoDS_20_Projeto_status3 ;
      private String AV85WWProjetoDS_21_Tfprojeto_nome ;
      private String AV86WWProjetoDS_22_Tfprojeto_nome_sel ;
      private String AV87WWProjetoDS_23_Tfprojeto_sigla ;
      private String AV88WWProjetoDS_24_Tfprojeto_sigla_sel ;
      private String scmdbuf ;
      private String lV66WWProjetoDS_2_Projeto_sigla1 ;
      private String lV67WWProjetoDS_3_Projeto_nome1 ;
      private String lV73WWProjetoDS_9_Projeto_sigla2 ;
      private String lV74WWProjetoDS_10_Projeto_nome2 ;
      private String lV80WWProjetoDS_16_Projeto_sigla3 ;
      private String lV81WWProjetoDS_17_Projeto_nome3 ;
      private String lV85WWProjetoDS_21_Tfprojeto_nome ;
      private String lV87WWProjetoDS_23_Tfprojeto_sigla ;
      private String A658Projeto_Status ;
      private String A650Projeto_Sigla ;
      private String A649Projeto_Nome ;
      private String A651Projeto_TipoContagem ;
      private String A652Projeto_TecnicaContagem ;
      private bool returnInSub ;
      private bool AV47DynamicFiltersEnabled2 ;
      private bool AV54DynamicFiltersEnabled3 ;
      private bool AV71WWProjetoDS_7_Dynamicfiltersenabled2 ;
      private bool AV78WWProjetoDS_14_Dynamicfiltersenabled3 ;
      private bool A1232Projeto_Incremental ;
      private bool BRKO42 ;
      private bool n1232Projeto_Incremental ;
      private bool BRKO44 ;
      private String AV34OptionIndexesJson ;
      private String AV29OptionsJson ;
      private String AV32OptionsDescJson ;
      private String AV21TFProjeto_Status_SelsJson ;
      private String AV25DDOName ;
      private String AV23SearchTxt ;
      private String AV24SearchTxtTo ;
      private String AV41DynamicFiltersSelector1 ;
      private String AV48DynamicFiltersSelector2 ;
      private String AV55DynamicFiltersSelector3 ;
      private String AV65WWProjetoDS_1_Dynamicfiltersselector1 ;
      private String AV72WWProjetoDS_8_Dynamicfiltersselector2 ;
      private String AV79WWProjetoDS_15_Dynamicfiltersselector3 ;
      private String AV27Option ;
      private IGxSession AV36Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00O43_A649Projeto_Nome ;
      private bool[] P00O43_A1232Projeto_Incremental ;
      private bool[] P00O43_n1232Projeto_Incremental ;
      private String[] P00O43_A658Projeto_Status ;
      private String[] P00O43_A652Projeto_TecnicaContagem ;
      private String[] P00O43_A651Projeto_TipoContagem ;
      private String[] P00O43_A650Projeto_Sigla ;
      private int[] P00O43_A648Projeto_Codigo ;
      private short[] P00O43_A657Projeto_Esforco ;
      private decimal[] P00O43_A655Projeto_Custo ;
      private short[] P00O43_A656Projeto_Prazo ;
      private String[] P00O45_A650Projeto_Sigla ;
      private bool[] P00O45_A1232Projeto_Incremental ;
      private bool[] P00O45_n1232Projeto_Incremental ;
      private String[] P00O45_A658Projeto_Status ;
      private String[] P00O45_A652Projeto_TecnicaContagem ;
      private String[] P00O45_A651Projeto_TipoContagem ;
      private String[] P00O45_A649Projeto_Nome ;
      private int[] P00O45_A648Projeto_Codigo ;
      private short[] P00O45_A657Projeto_Esforco ;
      private decimal[] P00O45_A655Projeto_Custo ;
      private short[] P00O45_A656Projeto_Prazo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22TFProjeto_Status_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV96WWProjetoDS_32_Tfprojeto_status_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV31OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV33OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV38GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV39GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV40GridStateDynamicFilter ;
   }

   public class getwwprojetofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00O43( IGxContext context ,
                                             String A658Projeto_Status ,
                                             IGxCollection AV96WWProjetoDS_32_Tfprojeto_status_sels ,
                                             String AV65WWProjetoDS_1_Dynamicfiltersselector1 ,
                                             String AV66WWProjetoDS_2_Projeto_sigla1 ,
                                             String AV67WWProjetoDS_3_Projeto_nome1 ,
                                             String AV68WWProjetoDS_4_Projeto_tipocontagem1 ,
                                             String AV69WWProjetoDS_5_Projeto_tecnicacontagem1 ,
                                             String AV70WWProjetoDS_6_Projeto_status1 ,
                                             bool AV71WWProjetoDS_7_Dynamicfiltersenabled2 ,
                                             String AV72WWProjetoDS_8_Dynamicfiltersselector2 ,
                                             String AV73WWProjetoDS_9_Projeto_sigla2 ,
                                             String AV74WWProjetoDS_10_Projeto_nome2 ,
                                             String AV75WWProjetoDS_11_Projeto_tipocontagem2 ,
                                             String AV76WWProjetoDS_12_Projeto_tecnicacontagem2 ,
                                             String AV77WWProjetoDS_13_Projeto_status2 ,
                                             bool AV78WWProjetoDS_14_Dynamicfiltersenabled3 ,
                                             String AV79WWProjetoDS_15_Dynamicfiltersselector3 ,
                                             String AV80WWProjetoDS_16_Projeto_sigla3 ,
                                             String AV81WWProjetoDS_17_Projeto_nome3 ,
                                             String AV82WWProjetoDS_18_Projeto_tipocontagem3 ,
                                             String AV83WWProjetoDS_19_Projeto_tecnicacontagem3 ,
                                             String AV84WWProjetoDS_20_Projeto_status3 ,
                                             String AV86WWProjetoDS_22_Tfprojeto_nome_sel ,
                                             String AV85WWProjetoDS_21_Tfprojeto_nome ,
                                             String AV88WWProjetoDS_24_Tfprojeto_sigla_sel ,
                                             String AV87WWProjetoDS_23_Tfprojeto_sigla ,
                                             short AV89WWProjetoDS_25_Tfprojeto_prazo ,
                                             short AV90WWProjetoDS_26_Tfprojeto_prazo_to ,
                                             decimal AV91WWProjetoDS_27_Tfprojeto_custo ,
                                             decimal AV92WWProjetoDS_28_Tfprojeto_custo_to ,
                                             short AV93WWProjetoDS_29_Tfprojeto_incremental_sel ,
                                             short AV94WWProjetoDS_30_Tfprojeto_esforco ,
                                             short AV95WWProjetoDS_31_Tfprojeto_esforco_to ,
                                             int AV96WWProjetoDS_32_Tfprojeto_status_sels_Count ,
                                             String A650Projeto_Sigla ,
                                             String A649Projeto_Nome ,
                                             String A651Projeto_TipoContagem ,
                                             String A652Projeto_TecnicaContagem ,
                                             short A656Projeto_Prazo ,
                                             decimal A655Projeto_Custo ,
                                             bool A1232Projeto_Incremental ,
                                             short A657Projeto_Esforco )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [25] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Projeto_Nome], T1.[Projeto_Incremental], T1.[Projeto_Status], T1.[Projeto_TecnicaContagem], T1.[Projeto_TipoContagem], T1.[Projeto_Sigla], T1.[Projeto_Codigo], COALESCE( T2.[Projeto_Esforco], 0) AS Projeto_Esforco, COALESCE( T2.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T2.[Projeto_Prazo], 0) AS Projeto_Prazo FROM ([Projeto] T1 WITH (NOLOCK) LEFT JOIN (SELECT SUM([Sistema_Esforco]) AS Projeto_Esforco, [Sistema_ProjetoCod], SUM([Sistema_Custo]) AS Projeto_Custo, SUM([Sistema_Prazo]) AS Projeto_Prazo FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T2 ON T2.[Sistema_ProjetoCod] = T1.[Projeto_Codigo])";
         if ( ( StringUtil.StrCmp(AV65WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWProjetoDS_2_Projeto_sigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV66WWProjetoDS_2_Projeto_sigla1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like '%' + @lV66WWProjetoDS_2_Projeto_sigla1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV65WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWProjetoDS_3_Projeto_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV67WWProjetoDS_3_Projeto_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] like '%' + @lV67WWProjetoDS_3_Projeto_nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV65WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_TIPOCONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWProjetoDS_4_Projeto_tipocontagem1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TipoContagem] = @AV68WWProjetoDS_4_Projeto_tipocontagem1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TipoContagem] = @AV68WWProjetoDS_4_Projeto_tipocontagem1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV65WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWProjetoDS_5_Projeto_tecnicacontagem1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV69WWProjetoDS_5_Projeto_tecnicacontagem1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TecnicaContagem] = @AV69WWProjetoDS_5_Projeto_tecnicacontagem1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV65WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWProjetoDS_6_Projeto_status1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV70WWProjetoDS_6_Projeto_status1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Status] = @AV70WWProjetoDS_6_Projeto_status1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV71WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWProjetoDS_9_Projeto_sigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV73WWProjetoDS_9_Projeto_sigla2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like '%' + @lV73WWProjetoDS_9_Projeto_sigla2 + '%')";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV71WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWProjetoDS_10_Projeto_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV74WWProjetoDS_10_Projeto_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] like '%' + @lV74WWProjetoDS_10_Projeto_nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV71WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_TIPOCONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWProjetoDS_11_Projeto_tipocontagem2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TipoContagem] = @AV75WWProjetoDS_11_Projeto_tipocontagem2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TipoContagem] = @AV75WWProjetoDS_11_Projeto_tipocontagem2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV71WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWProjetoDS_12_Projeto_tecnicacontagem2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV76WWProjetoDS_12_Projeto_tecnicacontagem2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TecnicaContagem] = @AV76WWProjetoDS_12_Projeto_tecnicacontagem2)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV71WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWProjetoDS_13_Projeto_status2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV77WWProjetoDS_13_Projeto_status2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Status] = @AV77WWProjetoDS_13_Projeto_status2)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV78WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWProjetoDS_16_Projeto_sigla3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV80WWProjetoDS_16_Projeto_sigla3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like '%' + @lV80WWProjetoDS_16_Projeto_sigla3 + '%')";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV78WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWProjetoDS_17_Projeto_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV81WWProjetoDS_17_Projeto_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] like '%' + @lV81WWProjetoDS_17_Projeto_nome3 + '%')";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV78WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_TIPOCONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWProjetoDS_18_Projeto_tipocontagem3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TipoContagem] = @AV82WWProjetoDS_18_Projeto_tipocontagem3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TipoContagem] = @AV82WWProjetoDS_18_Projeto_tipocontagem3)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV78WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWProjetoDS_19_Projeto_tecnicacontagem3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV83WWProjetoDS_19_Projeto_tecnicacontagem3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TecnicaContagem] = @AV83WWProjetoDS_19_Projeto_tecnicacontagem3)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV78WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWProjetoDS_20_Projeto_status3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV84WWProjetoDS_20_Projeto_status3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Status] = @AV84WWProjetoDS_20_Projeto_status3)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV86WWProjetoDS_22_Tfprojeto_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWProjetoDS_21_Tfprojeto_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] like @lV85WWProjetoDS_21_Tfprojeto_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] like @lV85WWProjetoDS_21_Tfprojeto_nome)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWProjetoDS_22_Tfprojeto_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] = @AV86WWProjetoDS_22_Tfprojeto_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] = @AV86WWProjetoDS_22_Tfprojeto_nome_sel)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWProjetoDS_24_Tfprojeto_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWProjetoDS_23_Tfprojeto_sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like @lV87WWProjetoDS_23_Tfprojeto_sigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like @lV87WWProjetoDS_23_Tfprojeto_sigla)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWProjetoDS_24_Tfprojeto_sigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] = @AV88WWProjetoDS_24_Tfprojeto_sigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] = @AV88WWProjetoDS_24_Tfprojeto_sigla_sel)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (0==AV89WWProjetoDS_25_Tfprojeto_prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Prazo], 0) >= @AV89WWProjetoDS_25_Tfprojeto_prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Prazo], 0) >= @AV89WWProjetoDS_25_Tfprojeto_prazo)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (0==AV90WWProjetoDS_26_Tfprojeto_prazo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Prazo], 0) <= @AV90WWProjetoDS_26_Tfprojeto_prazo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Prazo], 0) <= @AV90WWProjetoDS_26_Tfprojeto_prazo_to)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV91WWProjetoDS_27_Tfprojeto_custo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Custo], 0) >= @AV91WWProjetoDS_27_Tfprojeto_custo)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Custo], 0) >= @AV91WWProjetoDS_27_Tfprojeto_custo)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV92WWProjetoDS_28_Tfprojeto_custo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Custo], 0) <= @AV92WWProjetoDS_28_Tfprojeto_custo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Custo], 0) <= @AV92WWProjetoDS_28_Tfprojeto_custo_to)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( AV93WWProjetoDS_29_Tfprojeto_incremental_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Incremental] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Incremental] = 1)";
            }
         }
         if ( AV93WWProjetoDS_29_Tfprojeto_incremental_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Incremental] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Incremental] = 0)";
            }
         }
         if ( ! (0==AV94WWProjetoDS_30_Tfprojeto_esforco) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Esforco], 0) >= @AV94WWProjetoDS_30_Tfprojeto_esforco)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Esforco], 0) >= @AV94WWProjetoDS_30_Tfprojeto_esforco)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! (0==AV95WWProjetoDS_31_Tfprojeto_esforco_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Esforco], 0) <= @AV95WWProjetoDS_31_Tfprojeto_esforco_to)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Esforco], 0) <= @AV95WWProjetoDS_31_Tfprojeto_esforco_to)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( AV96WWProjetoDS_32_Tfprojeto_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV96WWProjetoDS_32_Tfprojeto_status_sels, "T1.[Projeto_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV96WWProjetoDS_32_Tfprojeto_status_sels, "T1.[Projeto_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Projeto_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00O45( IGxContext context ,
                                             String A658Projeto_Status ,
                                             IGxCollection AV96WWProjetoDS_32_Tfprojeto_status_sels ,
                                             String AV65WWProjetoDS_1_Dynamicfiltersselector1 ,
                                             String AV66WWProjetoDS_2_Projeto_sigla1 ,
                                             String AV67WWProjetoDS_3_Projeto_nome1 ,
                                             String AV68WWProjetoDS_4_Projeto_tipocontagem1 ,
                                             String AV69WWProjetoDS_5_Projeto_tecnicacontagem1 ,
                                             String AV70WWProjetoDS_6_Projeto_status1 ,
                                             bool AV71WWProjetoDS_7_Dynamicfiltersenabled2 ,
                                             String AV72WWProjetoDS_8_Dynamicfiltersselector2 ,
                                             String AV73WWProjetoDS_9_Projeto_sigla2 ,
                                             String AV74WWProjetoDS_10_Projeto_nome2 ,
                                             String AV75WWProjetoDS_11_Projeto_tipocontagem2 ,
                                             String AV76WWProjetoDS_12_Projeto_tecnicacontagem2 ,
                                             String AV77WWProjetoDS_13_Projeto_status2 ,
                                             bool AV78WWProjetoDS_14_Dynamicfiltersenabled3 ,
                                             String AV79WWProjetoDS_15_Dynamicfiltersselector3 ,
                                             String AV80WWProjetoDS_16_Projeto_sigla3 ,
                                             String AV81WWProjetoDS_17_Projeto_nome3 ,
                                             String AV82WWProjetoDS_18_Projeto_tipocontagem3 ,
                                             String AV83WWProjetoDS_19_Projeto_tecnicacontagem3 ,
                                             String AV84WWProjetoDS_20_Projeto_status3 ,
                                             String AV86WWProjetoDS_22_Tfprojeto_nome_sel ,
                                             String AV85WWProjetoDS_21_Tfprojeto_nome ,
                                             String AV88WWProjetoDS_24_Tfprojeto_sigla_sel ,
                                             String AV87WWProjetoDS_23_Tfprojeto_sigla ,
                                             short AV89WWProjetoDS_25_Tfprojeto_prazo ,
                                             short AV90WWProjetoDS_26_Tfprojeto_prazo_to ,
                                             decimal AV91WWProjetoDS_27_Tfprojeto_custo ,
                                             decimal AV92WWProjetoDS_28_Tfprojeto_custo_to ,
                                             short AV93WWProjetoDS_29_Tfprojeto_incremental_sel ,
                                             short AV94WWProjetoDS_30_Tfprojeto_esforco ,
                                             short AV95WWProjetoDS_31_Tfprojeto_esforco_to ,
                                             int AV96WWProjetoDS_32_Tfprojeto_status_sels_Count ,
                                             String A650Projeto_Sigla ,
                                             String A649Projeto_Nome ,
                                             String A651Projeto_TipoContagem ,
                                             String A652Projeto_TecnicaContagem ,
                                             short A656Projeto_Prazo ,
                                             decimal A655Projeto_Custo ,
                                             bool A1232Projeto_Incremental ,
                                             short A657Projeto_Esforco )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [25] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Projeto_Sigla], T1.[Projeto_Incremental], T1.[Projeto_Status], T1.[Projeto_TecnicaContagem], T1.[Projeto_TipoContagem], T1.[Projeto_Nome], T1.[Projeto_Codigo], COALESCE( T2.[Projeto_Esforco], 0) AS Projeto_Esforco, COALESCE( T2.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T2.[Projeto_Prazo], 0) AS Projeto_Prazo FROM ([Projeto] T1 WITH (NOLOCK) LEFT JOIN (SELECT SUM([Sistema_Esforco]) AS Projeto_Esforco, [Sistema_ProjetoCod], SUM([Sistema_Custo]) AS Projeto_Custo, SUM([Sistema_Prazo]) AS Projeto_Prazo FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T2 ON T2.[Sistema_ProjetoCod] = T1.[Projeto_Codigo])";
         if ( ( StringUtil.StrCmp(AV65WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWProjetoDS_2_Projeto_sigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV66WWProjetoDS_2_Projeto_sigla1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like '%' + @lV66WWProjetoDS_2_Projeto_sigla1 + '%')";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV65WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWProjetoDS_3_Projeto_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV67WWProjetoDS_3_Projeto_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] like '%' + @lV67WWProjetoDS_3_Projeto_nome1 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV65WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_TIPOCONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWProjetoDS_4_Projeto_tipocontagem1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TipoContagem] = @AV68WWProjetoDS_4_Projeto_tipocontagem1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TipoContagem] = @AV68WWProjetoDS_4_Projeto_tipocontagem1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV65WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWProjetoDS_5_Projeto_tecnicacontagem1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV69WWProjetoDS_5_Projeto_tecnicacontagem1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TecnicaContagem] = @AV69WWProjetoDS_5_Projeto_tecnicacontagem1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV65WWProjetoDS_1_Dynamicfiltersselector1, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWProjetoDS_6_Projeto_status1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV70WWProjetoDS_6_Projeto_status1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Status] = @AV70WWProjetoDS_6_Projeto_status1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV71WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWProjetoDS_9_Projeto_sigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV73WWProjetoDS_9_Projeto_sigla2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like '%' + @lV73WWProjetoDS_9_Projeto_sigla2 + '%')";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV71WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWProjetoDS_10_Projeto_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV74WWProjetoDS_10_Projeto_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] like '%' + @lV74WWProjetoDS_10_Projeto_nome2 + '%')";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV71WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_TIPOCONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWProjetoDS_11_Projeto_tipocontagem2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TipoContagem] = @AV75WWProjetoDS_11_Projeto_tipocontagem2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TipoContagem] = @AV75WWProjetoDS_11_Projeto_tipocontagem2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV71WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWProjetoDS_12_Projeto_tecnicacontagem2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV76WWProjetoDS_12_Projeto_tecnicacontagem2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TecnicaContagem] = @AV76WWProjetoDS_12_Projeto_tecnicacontagem2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV71WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWProjetoDS_13_Projeto_status2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV77WWProjetoDS_13_Projeto_status2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Status] = @AV77WWProjetoDS_13_Projeto_status2)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV78WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWProjetoDS_16_Projeto_sigla3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV80WWProjetoDS_16_Projeto_sigla3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like '%' + @lV80WWProjetoDS_16_Projeto_sigla3 + '%')";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV78WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWProjetoDS_17_Projeto_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV81WWProjetoDS_17_Projeto_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] like '%' + @lV81WWProjetoDS_17_Projeto_nome3 + '%')";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV78WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_TIPOCONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWProjetoDS_18_Projeto_tipocontagem3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TipoContagem] = @AV82WWProjetoDS_18_Projeto_tipocontagem3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TipoContagem] = @AV82WWProjetoDS_18_Projeto_tipocontagem3)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV78WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWProjetoDS_19_Projeto_tecnicacontagem3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV83WWProjetoDS_19_Projeto_tecnicacontagem3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TecnicaContagem] = @AV83WWProjetoDS_19_Projeto_tecnicacontagem3)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV78WWProjetoDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWProjetoDS_15_Dynamicfiltersselector3, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWProjetoDS_20_Projeto_status3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV84WWProjetoDS_20_Projeto_status3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Status] = @AV84WWProjetoDS_20_Projeto_status3)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV86WWProjetoDS_22_Tfprojeto_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWProjetoDS_21_Tfprojeto_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] like @lV85WWProjetoDS_21_Tfprojeto_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] like @lV85WWProjetoDS_21_Tfprojeto_nome)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWProjetoDS_22_Tfprojeto_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Nome] = @AV86WWProjetoDS_22_Tfprojeto_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Nome] = @AV86WWProjetoDS_22_Tfprojeto_nome_sel)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWProjetoDS_24_Tfprojeto_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWProjetoDS_23_Tfprojeto_sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like @lV87WWProjetoDS_23_Tfprojeto_sigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like @lV87WWProjetoDS_23_Tfprojeto_sigla)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWProjetoDS_24_Tfprojeto_sigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] = @AV88WWProjetoDS_24_Tfprojeto_sigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] = @AV88WWProjetoDS_24_Tfprojeto_sigla_sel)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (0==AV89WWProjetoDS_25_Tfprojeto_prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Prazo], 0) >= @AV89WWProjetoDS_25_Tfprojeto_prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Prazo], 0) >= @AV89WWProjetoDS_25_Tfprojeto_prazo)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! (0==AV90WWProjetoDS_26_Tfprojeto_prazo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Prazo], 0) <= @AV90WWProjetoDS_26_Tfprojeto_prazo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Prazo], 0) <= @AV90WWProjetoDS_26_Tfprojeto_prazo_to)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV91WWProjetoDS_27_Tfprojeto_custo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Custo], 0) >= @AV91WWProjetoDS_27_Tfprojeto_custo)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Custo], 0) >= @AV91WWProjetoDS_27_Tfprojeto_custo)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV92WWProjetoDS_28_Tfprojeto_custo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Custo], 0) <= @AV92WWProjetoDS_28_Tfprojeto_custo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Custo], 0) <= @AV92WWProjetoDS_28_Tfprojeto_custo_to)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( AV93WWProjetoDS_29_Tfprojeto_incremental_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Incremental] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Incremental] = 1)";
            }
         }
         if ( AV93WWProjetoDS_29_Tfprojeto_incremental_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Incremental] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Incremental] = 0)";
            }
         }
         if ( ! (0==AV94WWProjetoDS_30_Tfprojeto_esforco) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Esforco], 0) >= @AV94WWProjetoDS_30_Tfprojeto_esforco)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Esforco], 0) >= @AV94WWProjetoDS_30_Tfprojeto_esforco)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ! (0==AV95WWProjetoDS_31_Tfprojeto_esforco_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Esforco], 0) <= @AV95WWProjetoDS_31_Tfprojeto_esforco_to)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Esforco], 0) <= @AV95WWProjetoDS_31_Tfprojeto_esforco_to)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( AV96WWProjetoDS_32_Tfprojeto_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV96WWProjetoDS_32_Tfprojeto_status_sels, "T1.[Projeto_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV96WWProjetoDS_32_Tfprojeto_status_sels, "T1.[Projeto_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Projeto_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00O43(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (short)dynConstraints[27] , (decimal)dynConstraints[28] , (decimal)dynConstraints[29] , (short)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (int)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (decimal)dynConstraints[39] , (bool)dynConstraints[40] , (short)dynConstraints[41] );
               case 1 :
                     return conditional_P00O45(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (short)dynConstraints[27] , (decimal)dynConstraints[28] , (decimal)dynConstraints[29] , (short)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (int)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (decimal)dynConstraints[39] , (bool)dynConstraints[40] , (short)dynConstraints[41] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00O43 ;
          prmP00O43 = new Object[] {
          new Object[] {"@lV66WWProjetoDS_2_Projeto_sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV67WWProjetoDS_3_Projeto_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV68WWProjetoDS_4_Projeto_tipocontagem1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV69WWProjetoDS_5_Projeto_tecnicacontagem1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV70WWProjetoDS_6_Projeto_status1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV73WWProjetoDS_9_Projeto_sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV74WWProjetoDS_10_Projeto_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV75WWProjetoDS_11_Projeto_tipocontagem2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV76WWProjetoDS_12_Projeto_tecnicacontagem2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV77WWProjetoDS_13_Projeto_status2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV80WWProjetoDS_16_Projeto_sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV81WWProjetoDS_17_Projeto_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV82WWProjetoDS_18_Projeto_tipocontagem3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV83WWProjetoDS_19_Projeto_tecnicacontagem3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV84WWProjetoDS_20_Projeto_status3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV85WWProjetoDS_21_Tfprojeto_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV86WWProjetoDS_22_Tfprojeto_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV87WWProjetoDS_23_Tfprojeto_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV88WWProjetoDS_24_Tfprojeto_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV89WWProjetoDS_25_Tfprojeto_prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV90WWProjetoDS_26_Tfprojeto_prazo_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV91WWProjetoDS_27_Tfprojeto_custo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV92WWProjetoDS_28_Tfprojeto_custo_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV94WWProjetoDS_30_Tfprojeto_esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV95WWProjetoDS_31_Tfprojeto_esforco_to",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00O45 ;
          prmP00O45 = new Object[] {
          new Object[] {"@lV66WWProjetoDS_2_Projeto_sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV67WWProjetoDS_3_Projeto_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV68WWProjetoDS_4_Projeto_tipocontagem1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV69WWProjetoDS_5_Projeto_tecnicacontagem1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV70WWProjetoDS_6_Projeto_status1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV73WWProjetoDS_9_Projeto_sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV74WWProjetoDS_10_Projeto_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV75WWProjetoDS_11_Projeto_tipocontagem2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV76WWProjetoDS_12_Projeto_tecnicacontagem2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV77WWProjetoDS_13_Projeto_status2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV80WWProjetoDS_16_Projeto_sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV81WWProjetoDS_17_Projeto_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV82WWProjetoDS_18_Projeto_tipocontagem3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV83WWProjetoDS_19_Projeto_tecnicacontagem3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV84WWProjetoDS_20_Projeto_status3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV85WWProjetoDS_21_Tfprojeto_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV86WWProjetoDS_22_Tfprojeto_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV87WWProjetoDS_23_Tfprojeto_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV88WWProjetoDS_24_Tfprojeto_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV89WWProjetoDS_25_Tfprojeto_prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV90WWProjetoDS_26_Tfprojeto_prazo_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV91WWProjetoDS_27_Tfprojeto_custo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV92WWProjetoDS_28_Tfprojeto_custo_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV94WWProjetoDS_30_Tfprojeto_esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV95WWProjetoDS_31_Tfprojeto_esforco_to",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00O43", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00O43,100,0,true,false )
             ,new CursorDef("P00O45", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00O45,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 1) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 15) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((short[]) buf[8])[0] = rslt.getShort(8) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(9) ;
                ((short[]) buf[10])[0] = rslt.getShort(10) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 1) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((short[]) buf[8])[0] = rslt.getShort(8) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(9) ;
                ((short[]) buf[10])[0] = rslt.getShort(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[49]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[49]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwprojetofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwprojetofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwprojetofilterdata") )
          {
             return  ;
          }
          getwwprojetofilterdata worker = new getwwprojetofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
