/*
               File: AlertaRead
        Description: Leitura
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:9:34.72
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class alertaread : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_5") == 0 )
         {
            A1881Alerta_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_5( A1881Alerta_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV9AlertaRead_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AlertaRead_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AlertaRead_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vALERTAREAD_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9AlertaRead_Codigo), "ZZZZZ9")));
               AV8Alerta_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Alerta_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vALERTA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Alerta_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Leitura", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtAlerta_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public alertaread( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public alertaread( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_AlertaRead_Codigo ,
                           int aP2_Alerta_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV9AlertaRead_Codigo = aP1_AlertaRead_Codigo;
         this.AV8Alerta_Codigo = aP2_Alerta_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4R212( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4R212e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4R212( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4R212( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4R212e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Leitura", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_AlertaRead.htm");
            wb_table3_28_4R212( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_4R212e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4R212e( true) ;
         }
         else
         {
            wb_table1_2_4R212e( false) ;
         }
      }

      protected void wb_table3_28_4R212( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_4R212( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_4R212e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_AlertaRead.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_AlertaRead.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_AlertaRead.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_4R212e( true) ;
         }
         else
         {
            wb_table3_28_4R212e( false) ;
         }
      }

      protected void wb_table4_34_4R212( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalerta_codigo_Internalname, "Codigo", "", "", lblTextblockalerta_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_AlertaRead.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAlerta_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1881Alerta_Codigo), 6, 0, ",", "")), ((edtAlerta_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1881Alerta_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1881Alerta_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAlerta_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtAlerta_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AlertaRead.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalertaread_codigo_Internalname, "Codigo", "", "", lblTextblockalertaread_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_AlertaRead.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAlertaRead_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1889AlertaRead_Codigo), 6, 0, ",", "")), ((edtAlertaRead_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1889AlertaRead_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1889AlertaRead_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAlertaRead_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtAlertaRead_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AlertaRead.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalertaread_usercod_Internalname, "Usu�rio", "", "", lblTextblockalertaread_usercod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_AlertaRead.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAlertaRead_UserCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1890AlertaRead_UserCod), 6, 0, ",", "")), ((edtAlertaRead_UserCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1890AlertaRead_UserCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1890AlertaRead_UserCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAlertaRead_UserCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtAlertaRead_UserCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AlertaRead.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalertaread_datahora_Internalname, "Em", "", "", lblTextblockalertaread_datahora_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_AlertaRead.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtAlertaRead_DataHora_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtAlertaRead_DataHora_Internalname, context.localUtil.TToC( A1891AlertaRead_DataHora, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1891AlertaRead_DataHora, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAlertaRead_DataHora_Jsonclick, 0, "Attribute", "", "", "", 1, edtAlertaRead_DataHora_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AlertaRead.htm");
            GxWebStd.gx_bitmap( context, edtAlertaRead_DataHora_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtAlertaRead_DataHora_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_AlertaRead.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_4R212e( true) ;
         }
         else
         {
            wb_table4_34_4R212e( false) ;
         }
      }

      protected void wb_table2_5_4R212( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AlertaRead.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AlertaRead.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AlertaRead.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AlertaRead.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AlertaRead.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AlertaRead.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AlertaRead.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AlertaRead.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AlertaRead.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AlertaRead.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AlertaRead.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AlertaRead.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AlertaRead.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AlertaRead.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AlertaRead.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AlertaRead.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4R212e( true) ;
         }
         else
         {
            wb_table2_5_4R212e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtAlerta_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAlerta_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "ALERTA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtAlerta_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1881Alerta_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
               }
               else
               {
                  A1881Alerta_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAlerta_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtAlertaRead_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAlertaRead_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "ALERTAREAD_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtAlertaRead_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1889AlertaRead_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1889AlertaRead_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1889AlertaRead_Codigo), 6, 0)));
               }
               else
               {
                  A1889AlertaRead_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAlertaRead_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1889AlertaRead_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1889AlertaRead_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtAlertaRead_UserCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAlertaRead_UserCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "ALERTAREAD_USERCOD");
                  AnyError = 1;
                  GX_FocusControl = edtAlertaRead_UserCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1890AlertaRead_UserCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1890AlertaRead_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1890AlertaRead_UserCod), 6, 0)));
               }
               else
               {
                  A1890AlertaRead_UserCod = (int)(context.localUtil.CToN( cgiGet( edtAlertaRead_UserCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1890AlertaRead_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1890AlertaRead_UserCod), 6, 0)));
               }
               if ( context.localUtil.VCDateTime( cgiGet( edtAlertaRead_DataHora_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Em"}), 1, "ALERTAREAD_DATAHORA");
                  AnyError = 1;
                  GX_FocusControl = edtAlertaRead_DataHora_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1891AlertaRead_DataHora = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1891AlertaRead_DataHora", context.localUtil.TToC( A1891AlertaRead_DataHora, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1891AlertaRead_DataHora = context.localUtil.CToT( cgiGet( edtAlertaRead_DataHora_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1891AlertaRead_DataHora", context.localUtil.TToC( A1891AlertaRead_DataHora, 8, 5, 0, 3, "/", ":", " "));
               }
               /* Read saved values. */
               Z1881Alerta_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1881Alerta_Codigo"), ",", "."));
               Z1889AlertaRead_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1889AlertaRead_Codigo"), ",", "."));
               Z1890AlertaRead_UserCod = (int)(context.localUtil.CToN( cgiGet( "Z1890AlertaRead_UserCod"), ",", "."));
               Z1891AlertaRead_DataHora = context.localUtil.CToT( cgiGet( "Z1891AlertaRead_DataHora"), 0);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV9AlertaRead_Codigo = (int)(context.localUtil.CToN( cgiGet( "vALERTAREAD_CODIGO"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "AlertaRead";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1881Alerta_Codigo != Z1881Alerta_Codigo ) || ( A1889AlertaRead_Codigo != Z1889AlertaRead_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("alertaread:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1881Alerta_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
                  A1889AlertaRead_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1889AlertaRead_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1889AlertaRead_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_delete( ) ;
                           }
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4R212( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            imgBtn_delete2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
            imgBtn_delete2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
            bttBtn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               imgBtn_enter2_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
               imgBtn_enter2_separator_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
               bttBtn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
            }
            DisableAttributes4R212( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_4R0( )
      {
         BeforeValidate4R212( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4R212( ) ;
            }
            else
            {
               CheckExtendedTable4R212( ) ;
               CloseExtendedTableCursors4R212( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption4R0( )
      {
      }

      protected void ZM4R212( short GX_JID )
      {
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1890AlertaRead_UserCod = T004R3_A1890AlertaRead_UserCod[0];
               Z1891AlertaRead_DataHora = T004R3_A1891AlertaRead_DataHora[0];
            }
            else
            {
               Z1890AlertaRead_UserCod = A1890AlertaRead_UserCod;
               Z1891AlertaRead_DataHora = A1891AlertaRead_DataHora;
            }
         }
         if ( GX_JID == -4 )
         {
            Z1889AlertaRead_Codigo = A1889AlertaRead_Codigo;
            Z1890AlertaRead_UserCod = A1890AlertaRead_UserCod;
            Z1891AlertaRead_DataHora = A1891AlertaRead_DataHora;
            Z1881Alerta_Codigo = A1881Alerta_Codigo;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         imgBtn_delete2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         if ( AV9AlertaRead_Codigo > 0 )
         {
            A1889AlertaRead_Codigo = AV9AlertaRead_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1889AlertaRead_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1889AlertaRead_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A1891AlertaRead_DataHora) && ( Gx_BScreen == 0 ) )
         {
            A1891AlertaRead_DataHora = DateTimeUtil.ServerNow( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1891AlertaRead_DataHora", context.localUtil.TToC( A1891AlertaRead_DataHora, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load4R212( )
      {
         /* Using cursor T004R5 */
         pr_default.execute(3, new Object[] {A1881Alerta_Codigo, A1889AlertaRead_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound212 = 1;
            A1890AlertaRead_UserCod = T004R5_A1890AlertaRead_UserCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1890AlertaRead_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1890AlertaRead_UserCod), 6, 0)));
            A1891AlertaRead_DataHora = T004R5_A1891AlertaRead_DataHora[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1891AlertaRead_DataHora", context.localUtil.TToC( A1891AlertaRead_DataHora, 8, 5, 0, 3, "/", ":", " "));
            ZM4R212( -4) ;
         }
         pr_default.close(3);
         OnLoadActions4R212( ) ;
      }

      protected void OnLoadActions4R212( )
      {
      }

      protected void CheckExtendedTable4R212( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T004R4 */
         pr_default.execute(2, new Object[] {A1881Alerta_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Alertas'.", "ForeignKeyNotFound", 1, "ALERTA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAlerta_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         if ( ! ( (DateTime.MinValue==A1891AlertaRead_DataHora) || ( A1891AlertaRead_DataHora >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Em fora do intervalo", "OutOfRange", 1, "ALERTAREAD_DATAHORA");
            AnyError = 1;
            GX_FocusControl = edtAlertaRead_DataHora_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors4R212( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_5( int A1881Alerta_Codigo )
      {
         /* Using cursor T004R6 */
         pr_default.execute(4, new Object[] {A1881Alerta_Codigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Alertas'.", "ForeignKeyNotFound", 1, "ALERTA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAlerta_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey4R212( )
      {
         /* Using cursor T004R7 */
         pr_default.execute(5, new Object[] {A1881Alerta_Codigo, A1889AlertaRead_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound212 = 1;
         }
         else
         {
            RcdFound212 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004R3 */
         pr_default.execute(1, new Object[] {A1881Alerta_Codigo, A1889AlertaRead_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4R212( 4) ;
            RcdFound212 = 1;
            A1889AlertaRead_Codigo = T004R3_A1889AlertaRead_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1889AlertaRead_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1889AlertaRead_Codigo), 6, 0)));
            A1890AlertaRead_UserCod = T004R3_A1890AlertaRead_UserCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1890AlertaRead_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1890AlertaRead_UserCod), 6, 0)));
            A1891AlertaRead_DataHora = T004R3_A1891AlertaRead_DataHora[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1891AlertaRead_DataHora", context.localUtil.TToC( A1891AlertaRead_DataHora, 8, 5, 0, 3, "/", ":", " "));
            A1881Alerta_Codigo = T004R3_A1881Alerta_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
            Z1881Alerta_Codigo = A1881Alerta_Codigo;
            Z1889AlertaRead_Codigo = A1889AlertaRead_Codigo;
            sMode212 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load4R212( ) ;
            if ( AnyError == 1 )
            {
               RcdFound212 = 0;
               InitializeNonKey4R212( ) ;
            }
            Gx_mode = sMode212;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound212 = 0;
            InitializeNonKey4R212( ) ;
            sMode212 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode212;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4R212( ) ;
         if ( RcdFound212 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound212 = 0;
         /* Using cursor T004R8 */
         pr_default.execute(6, new Object[] {A1881Alerta_Codigo, A1889AlertaRead_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T004R8_A1881Alerta_Codigo[0] < A1881Alerta_Codigo ) || ( T004R8_A1881Alerta_Codigo[0] == A1881Alerta_Codigo ) && ( T004R8_A1889AlertaRead_Codigo[0] < A1889AlertaRead_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T004R8_A1881Alerta_Codigo[0] > A1881Alerta_Codigo ) || ( T004R8_A1881Alerta_Codigo[0] == A1881Alerta_Codigo ) && ( T004R8_A1889AlertaRead_Codigo[0] > A1889AlertaRead_Codigo ) ) )
            {
               A1881Alerta_Codigo = T004R8_A1881Alerta_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
               A1889AlertaRead_Codigo = T004R8_A1889AlertaRead_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1889AlertaRead_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1889AlertaRead_Codigo), 6, 0)));
               RcdFound212 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound212 = 0;
         /* Using cursor T004R9 */
         pr_default.execute(7, new Object[] {A1881Alerta_Codigo, A1889AlertaRead_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T004R9_A1881Alerta_Codigo[0] > A1881Alerta_Codigo ) || ( T004R9_A1881Alerta_Codigo[0] == A1881Alerta_Codigo ) && ( T004R9_A1889AlertaRead_Codigo[0] > A1889AlertaRead_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T004R9_A1881Alerta_Codigo[0] < A1881Alerta_Codigo ) || ( T004R9_A1881Alerta_Codigo[0] == A1881Alerta_Codigo ) && ( T004R9_A1889AlertaRead_Codigo[0] < A1889AlertaRead_Codigo ) ) )
            {
               A1881Alerta_Codigo = T004R9_A1881Alerta_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
               A1889AlertaRead_Codigo = T004R9_A1889AlertaRead_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1889AlertaRead_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1889AlertaRead_Codigo), 6, 0)));
               RcdFound212 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4R212( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtAlerta_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4R212( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound212 == 1 )
            {
               if ( ( A1881Alerta_Codigo != Z1881Alerta_Codigo ) || ( A1889AlertaRead_Codigo != Z1889AlertaRead_Codigo ) )
               {
                  A1881Alerta_Codigo = Z1881Alerta_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
                  A1889AlertaRead_Codigo = Z1889AlertaRead_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1889AlertaRead_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1889AlertaRead_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "ALERTA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtAlerta_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtAlerta_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update4R212( ) ;
                  GX_FocusControl = edtAlerta_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A1881Alerta_Codigo != Z1881Alerta_Codigo ) || ( A1889AlertaRead_Codigo != Z1889AlertaRead_Codigo ) )
               {
                  /* Insert record */
                  GX_FocusControl = edtAlerta_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4R212( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "ALERTA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtAlerta_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtAlerta_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4R212( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A1881Alerta_Codigo != Z1881Alerta_Codigo ) || ( A1889AlertaRead_Codigo != Z1889AlertaRead_Codigo ) )
         {
            A1881Alerta_Codigo = Z1881Alerta_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
            A1889AlertaRead_Codigo = Z1889AlertaRead_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1889AlertaRead_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1889AlertaRead_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "ALERTA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAlerta_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtAlerta_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound212 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "ALERTA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAlerta_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtAlertaRead_UserCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart4R212( ) ;
         if ( RcdFound212 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
         }
         GX_FocusControl = edtAlertaRead_UserCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd4R212( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound212 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
         }
         GX_FocusControl = edtAlertaRead_UserCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound212 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
         }
         GX_FocusControl = edtAlertaRead_UserCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart4R212( ) ;
         if ( RcdFound212 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound212 != 0 )
            {
               ScanNext4R212( ) ;
            }
         }
         GX_FocusControl = edtAlertaRead_UserCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd4R212( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency4R212( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004R2 */
            pr_default.execute(0, new Object[] {A1881Alerta_Codigo, A1889AlertaRead_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AlertaRead"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1890AlertaRead_UserCod != T004R2_A1890AlertaRead_UserCod[0] ) || ( Z1891AlertaRead_DataHora != T004R2_A1891AlertaRead_DataHora[0] ) )
            {
               if ( Z1890AlertaRead_UserCod != T004R2_A1890AlertaRead_UserCod[0] )
               {
                  GXUtil.WriteLog("alertaread:[seudo value changed for attri]"+"AlertaRead_UserCod");
                  GXUtil.WriteLogRaw("Old: ",Z1890AlertaRead_UserCod);
                  GXUtil.WriteLogRaw("Current: ",T004R2_A1890AlertaRead_UserCod[0]);
               }
               if ( Z1891AlertaRead_DataHora != T004R2_A1891AlertaRead_DataHora[0] )
               {
                  GXUtil.WriteLog("alertaread:[seudo value changed for attri]"+"AlertaRead_DataHora");
                  GXUtil.WriteLogRaw("Old: ",Z1891AlertaRead_DataHora);
                  GXUtil.WriteLogRaw("Current: ",T004R2_A1891AlertaRead_DataHora[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"AlertaRead"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4R212( )
      {
         BeforeValidate4R212( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4R212( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4R212( 0) ;
            CheckOptimisticConcurrency4R212( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4R212( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4R212( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004R10 */
                     pr_default.execute(8, new Object[] {A1889AlertaRead_Codigo, A1890AlertaRead_UserCod, A1891AlertaRead_DataHora, A1881Alerta_Codigo});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("AlertaRead") ;
                     if ( (pr_default.getStatus(8) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4R0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4R212( ) ;
            }
            EndLevel4R212( ) ;
         }
         CloseExtendedTableCursors4R212( ) ;
      }

      protected void Update4R212( )
      {
         BeforeValidate4R212( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4R212( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4R212( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4R212( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4R212( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004R11 */
                     pr_default.execute(9, new Object[] {A1890AlertaRead_UserCod, A1891AlertaRead_DataHora, A1881Alerta_Codigo, A1889AlertaRead_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("AlertaRead") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AlertaRead"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4R212( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption4R0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4R212( ) ;
         }
         CloseExtendedTableCursors4R212( ) ;
      }

      protected void DeferredUpdate4R212( )
      {
      }

      protected void delete( )
      {
         BeforeValidate4R212( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4R212( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4R212( ) ;
            AfterConfirm4R212( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4R212( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004R12 */
                  pr_default.execute(10, new Object[] {A1881Alerta_Codigo, A1889AlertaRead_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("AlertaRead") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound212 == 0 )
                        {
                           InitAll4R212( ) ;
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption4R0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode212 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel4R212( ) ;
         Gx_mode = sMode212;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls4R212( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel4R212( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4R212( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "AlertaRead");
            if ( AnyError == 0 )
            {
               ConfirmValues4R0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "AlertaRead");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4R212( )
      {
         /* Scan By routine */
         /* Using cursor T004R13 */
         pr_default.execute(11);
         RcdFound212 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound212 = 1;
            A1881Alerta_Codigo = T004R13_A1881Alerta_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
            A1889AlertaRead_Codigo = T004R13_A1889AlertaRead_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1889AlertaRead_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1889AlertaRead_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4R212( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound212 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound212 = 1;
            A1881Alerta_Codigo = T004R13_A1881Alerta_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
            A1889AlertaRead_Codigo = T004R13_A1889AlertaRead_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1889AlertaRead_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1889AlertaRead_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd4R212( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm4R212( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4R212( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4R212( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4R212( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4R212( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4R212( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4R212( )
      {
         edtAlerta_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAlerta_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAlerta_Codigo_Enabled), 5, 0)));
         edtAlertaRead_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAlertaRead_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAlertaRead_Codigo_Enabled), 5, 0)));
         edtAlertaRead_UserCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAlertaRead_UserCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAlertaRead_UserCod_Enabled), 5, 0)));
         edtAlertaRead_DataHora_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAlertaRead_DataHora_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAlertaRead_DataHora_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4R0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282393549");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("alertaread.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV9AlertaRead_Codigo) + "," + UrlEncode("" +AV8Alerta_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1881Alerta_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1881Alerta_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1889AlertaRead_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1889AlertaRead_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1890AlertaRead_UserCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1890AlertaRead_UserCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1891AlertaRead_DataHora", context.localUtil.TToC( Z1891AlertaRead_DataHora, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vALERTA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Alerta_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vALERTAREAD_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9AlertaRead_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vALERTAREAD_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9AlertaRead_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vALERTA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Alerta_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "AlertaRead";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("alertaread:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("alertaread.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV9AlertaRead_Codigo) + "," + UrlEncode("" +AV8Alerta_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "AlertaRead" ;
      }

      public override String GetPgmdesc( )
      {
         return "Leitura" ;
      }

      protected void InitializeNonKey4R212( )
      {
         A1890AlertaRead_UserCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1890AlertaRead_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1890AlertaRead_UserCod), 6, 0)));
         A1891AlertaRead_DataHora = DateTimeUtil.ServerNow( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1891AlertaRead_DataHora", context.localUtil.TToC( A1891AlertaRead_DataHora, 8, 5, 0, 3, "/", ":", " "));
         Z1890AlertaRead_UserCod = 0;
         Z1891AlertaRead_DataHora = (DateTime)(DateTime.MinValue);
      }

      protected void InitAll4R212( )
      {
         A1881Alerta_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
         A1889AlertaRead_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1889AlertaRead_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1889AlertaRead_Codigo), 6, 0)));
         InitializeNonKey4R212( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1891AlertaRead_DataHora = i1891AlertaRead_DataHora;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1891AlertaRead_DataHora", context.localUtil.TToC( A1891AlertaRead_DataHora, 8, 5, 0, 3, "/", ":", " "));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282393553");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("alertaread.js", "?20204282393553");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockalerta_codigo_Internalname = "TEXTBLOCKALERTA_CODIGO";
         edtAlerta_Codigo_Internalname = "ALERTA_CODIGO";
         lblTextblockalertaread_codigo_Internalname = "TEXTBLOCKALERTAREAD_CODIGO";
         edtAlertaRead_Codigo_Internalname = "ALERTAREAD_CODIGO";
         lblTextblockalertaread_usercod_Internalname = "TEXTBLOCKALERTAREAD_USERCOD";
         edtAlertaRead_UserCod_Internalname = "ALERTAREAD_USERCOD";
         lblTextblockalertaread_datahora_Internalname = "TEXTBLOCKALERTAREAD_DATAHORA";
         edtAlertaRead_DataHora_Internalname = "ALERTAREAD_DATAHORA";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Leitura";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtAlertaRead_DataHora_Jsonclick = "";
         edtAlertaRead_DataHora_Enabled = 1;
         edtAlertaRead_UserCod_Jsonclick = "";
         edtAlertaRead_UserCod_Enabled = 1;
         edtAlertaRead_Codigo_Jsonclick = "";
         edtAlertaRead_Codigo_Enabled = 1;
         edtAlerta_Codigo_Jsonclick = "";
         edtAlerta_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Alerta_codigo( int GX_Parm1 )
      {
         A1881Alerta_Codigo = GX_Parm1;
         /* Using cursor T004R14 */
         pr_default.execute(12, new Object[] {A1881Alerta_Codigo});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Alertas'.", "ForeignKeyNotFound", 1, "ALERTA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAlerta_Codigo_Internalname;
         }
         pr_default.close(12);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9AlertaRead_Codigo',fld:'vALERTAREAD_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV8Alerta_Codigo',fld:'vALERTA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1891AlertaRead_DataHora = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockalerta_codigo_Jsonclick = "";
         lblTextblockalertaread_codigo_Jsonclick = "";
         lblTextblockalertaread_usercod_Jsonclick = "";
         lblTextblockalertaread_datahora_Jsonclick = "";
         A1891AlertaRead_DataHora = (DateTime)(DateTime.MinValue);
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         forbiddenHiddens = "";
         hsh = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T004R5_A1889AlertaRead_Codigo = new int[1] ;
         T004R5_A1890AlertaRead_UserCod = new int[1] ;
         T004R5_A1891AlertaRead_DataHora = new DateTime[] {DateTime.MinValue} ;
         T004R5_A1881Alerta_Codigo = new int[1] ;
         T004R4_A1881Alerta_Codigo = new int[1] ;
         T004R6_A1881Alerta_Codigo = new int[1] ;
         T004R7_A1881Alerta_Codigo = new int[1] ;
         T004R7_A1889AlertaRead_Codigo = new int[1] ;
         T004R3_A1889AlertaRead_Codigo = new int[1] ;
         T004R3_A1890AlertaRead_UserCod = new int[1] ;
         T004R3_A1891AlertaRead_DataHora = new DateTime[] {DateTime.MinValue} ;
         T004R3_A1881Alerta_Codigo = new int[1] ;
         sMode212 = "";
         T004R8_A1881Alerta_Codigo = new int[1] ;
         T004R8_A1889AlertaRead_Codigo = new int[1] ;
         T004R9_A1881Alerta_Codigo = new int[1] ;
         T004R9_A1889AlertaRead_Codigo = new int[1] ;
         T004R2_A1889AlertaRead_Codigo = new int[1] ;
         T004R2_A1890AlertaRead_UserCod = new int[1] ;
         T004R2_A1891AlertaRead_DataHora = new DateTime[] {DateTime.MinValue} ;
         T004R2_A1881Alerta_Codigo = new int[1] ;
         T004R13_A1881Alerta_Codigo = new int[1] ;
         T004R13_A1889AlertaRead_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i1891AlertaRead_DataHora = (DateTime)(DateTime.MinValue);
         T004R14_A1881Alerta_Codigo = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.alertaread__default(),
            new Object[][] {
                new Object[] {
               T004R2_A1889AlertaRead_Codigo, T004R2_A1890AlertaRead_UserCod, T004R2_A1891AlertaRead_DataHora, T004R2_A1881Alerta_Codigo
               }
               , new Object[] {
               T004R3_A1889AlertaRead_Codigo, T004R3_A1890AlertaRead_UserCod, T004R3_A1891AlertaRead_DataHora, T004R3_A1881Alerta_Codigo
               }
               , new Object[] {
               T004R4_A1881Alerta_Codigo
               }
               , new Object[] {
               T004R5_A1889AlertaRead_Codigo, T004R5_A1890AlertaRead_UserCod, T004R5_A1891AlertaRead_DataHora, T004R5_A1881Alerta_Codigo
               }
               , new Object[] {
               T004R6_A1881Alerta_Codigo
               }
               , new Object[] {
               T004R7_A1881Alerta_Codigo, T004R7_A1889AlertaRead_Codigo
               }
               , new Object[] {
               T004R8_A1881Alerta_Codigo, T004R8_A1889AlertaRead_Codigo
               }
               , new Object[] {
               T004R9_A1881Alerta_Codigo, T004R9_A1889AlertaRead_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004R13_A1881Alerta_Codigo, T004R13_A1889AlertaRead_Codigo
               }
               , new Object[] {
               T004R14_A1881Alerta_Codigo
               }
            }
         );
         Z1891AlertaRead_DataHora = DateTimeUtil.ServerNow( context, "DEFAULT");
         A1891AlertaRead_DataHora = DateTimeUtil.ServerNow( context, "DEFAULT");
         i1891AlertaRead_DataHora = DateTimeUtil.ServerNow( context, "DEFAULT");
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short GX_JID ;
      private short RcdFound212 ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV9AlertaRead_Codigo ;
      private int wcpOAV8Alerta_Codigo ;
      private int Z1881Alerta_Codigo ;
      private int Z1889AlertaRead_Codigo ;
      private int Z1890AlertaRead_UserCod ;
      private int A1881Alerta_Codigo ;
      private int AV9AlertaRead_Codigo ;
      private int AV8Alerta_Codigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtAlerta_Codigo_Enabled ;
      private int A1889AlertaRead_Codigo ;
      private int edtAlertaRead_Codigo_Enabled ;
      private int A1890AlertaRead_UserCod ;
      private int edtAlertaRead_UserCod_Enabled ;
      private int edtAlertaRead_DataHora_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtAlerta_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockalerta_codigo_Internalname ;
      private String lblTextblockalerta_codigo_Jsonclick ;
      private String edtAlerta_Codigo_Jsonclick ;
      private String lblTextblockalertaread_codigo_Internalname ;
      private String lblTextblockalertaread_codigo_Jsonclick ;
      private String edtAlertaRead_Codigo_Internalname ;
      private String edtAlertaRead_Codigo_Jsonclick ;
      private String lblTextblockalertaread_usercod_Internalname ;
      private String lblTextblockalertaread_usercod_Jsonclick ;
      private String edtAlertaRead_UserCod_Internalname ;
      private String edtAlertaRead_UserCod_Jsonclick ;
      private String lblTextblockalertaread_datahora_Internalname ;
      private String lblTextblockalertaread_datahora_Jsonclick ;
      private String edtAlertaRead_DataHora_Internalname ;
      private String edtAlertaRead_DataHora_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode212 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private DateTime Z1891AlertaRead_DataHora ;
      private DateTime A1891AlertaRead_DataHora ;
      private DateTime i1891AlertaRead_DataHora ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T004R5_A1889AlertaRead_Codigo ;
      private int[] T004R5_A1890AlertaRead_UserCod ;
      private DateTime[] T004R5_A1891AlertaRead_DataHora ;
      private int[] T004R5_A1881Alerta_Codigo ;
      private int[] T004R4_A1881Alerta_Codigo ;
      private int[] T004R6_A1881Alerta_Codigo ;
      private int[] T004R7_A1881Alerta_Codigo ;
      private int[] T004R7_A1889AlertaRead_Codigo ;
      private int[] T004R3_A1889AlertaRead_Codigo ;
      private int[] T004R3_A1890AlertaRead_UserCod ;
      private DateTime[] T004R3_A1891AlertaRead_DataHora ;
      private int[] T004R3_A1881Alerta_Codigo ;
      private int[] T004R8_A1881Alerta_Codigo ;
      private int[] T004R8_A1889AlertaRead_Codigo ;
      private int[] T004R9_A1881Alerta_Codigo ;
      private int[] T004R9_A1889AlertaRead_Codigo ;
      private int[] T004R2_A1889AlertaRead_Codigo ;
      private int[] T004R2_A1890AlertaRead_UserCod ;
      private DateTime[] T004R2_A1891AlertaRead_DataHora ;
      private int[] T004R2_A1881Alerta_Codigo ;
      private int[] T004R13_A1881Alerta_Codigo ;
      private int[] T004R13_A1889AlertaRead_Codigo ;
      private int[] T004R14_A1881Alerta_Codigo ;
      private GXWebForm Form ;
   }

   public class alertaread__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004R5 ;
          prmT004R5 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AlertaRead_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004R4 ;
          prmT004R4 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004R6 ;
          prmT004R6 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004R7 ;
          prmT004R7 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AlertaRead_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004R3 ;
          prmT004R3 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AlertaRead_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004R8 ;
          prmT004R8 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AlertaRead_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004R9 ;
          prmT004R9 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AlertaRead_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004R2 ;
          prmT004R2 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AlertaRead_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004R10 ;
          prmT004R10 = new Object[] {
          new Object[] {"@AlertaRead_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AlertaRead_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AlertaRead_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004R11 ;
          prmT004R11 = new Object[] {
          new Object[] {"@AlertaRead_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AlertaRead_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AlertaRead_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004R12 ;
          prmT004R12 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AlertaRead_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004R13 ;
          prmT004R13 = new Object[] {
          } ;
          Object[] prmT004R14 ;
          prmT004R14 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T004R2", "SELECT [AlertaRead_Codigo], [AlertaRead_UserCod], [AlertaRead_DataHora], [Alerta_Codigo] FROM [AlertaRead] WITH (UPDLOCK) WHERE [Alerta_Codigo] = @Alerta_Codigo AND [AlertaRead_Codigo] = @AlertaRead_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004R2,1,0,true,false )
             ,new CursorDef("T004R3", "SELECT [AlertaRead_Codigo], [AlertaRead_UserCod], [AlertaRead_DataHora], [Alerta_Codigo] FROM [AlertaRead] WITH (NOLOCK) WHERE [Alerta_Codigo] = @Alerta_Codigo AND [AlertaRead_Codigo] = @AlertaRead_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004R3,1,0,true,false )
             ,new CursorDef("T004R4", "SELECT [Alerta_Codigo] FROM [Alerta] WITH (NOLOCK) WHERE [Alerta_Codigo] = @Alerta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004R4,1,0,true,false )
             ,new CursorDef("T004R5", "SELECT TM1.[AlertaRead_Codigo], TM1.[AlertaRead_UserCod], TM1.[AlertaRead_DataHora], TM1.[Alerta_Codigo] FROM [AlertaRead] TM1 WITH (NOLOCK) WHERE TM1.[Alerta_Codigo] = @Alerta_Codigo and TM1.[AlertaRead_Codigo] = @AlertaRead_Codigo ORDER BY TM1.[Alerta_Codigo], TM1.[AlertaRead_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004R5,100,0,true,false )
             ,new CursorDef("T004R6", "SELECT [Alerta_Codigo] FROM [Alerta] WITH (NOLOCK) WHERE [Alerta_Codigo] = @Alerta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004R6,1,0,true,false )
             ,new CursorDef("T004R7", "SELECT [Alerta_Codigo], [AlertaRead_Codigo] FROM [AlertaRead] WITH (NOLOCK) WHERE [Alerta_Codigo] = @Alerta_Codigo AND [AlertaRead_Codigo] = @AlertaRead_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004R7,1,0,true,false )
             ,new CursorDef("T004R8", "SELECT TOP 1 [Alerta_Codigo], [AlertaRead_Codigo] FROM [AlertaRead] WITH (NOLOCK) WHERE ( [Alerta_Codigo] > @Alerta_Codigo or [Alerta_Codigo] = @Alerta_Codigo and [AlertaRead_Codigo] > @AlertaRead_Codigo) ORDER BY [Alerta_Codigo], [AlertaRead_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004R8,1,0,true,true )
             ,new CursorDef("T004R9", "SELECT TOP 1 [Alerta_Codigo], [AlertaRead_Codigo] FROM [AlertaRead] WITH (NOLOCK) WHERE ( [Alerta_Codigo] < @Alerta_Codigo or [Alerta_Codigo] = @Alerta_Codigo and [AlertaRead_Codigo] < @AlertaRead_Codigo) ORDER BY [Alerta_Codigo] DESC, [AlertaRead_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004R9,1,0,true,true )
             ,new CursorDef("T004R10", "INSERT INTO [AlertaRead]([AlertaRead_Codigo], [AlertaRead_UserCod], [AlertaRead_DataHora], [Alerta_Codigo]) VALUES(@AlertaRead_Codigo, @AlertaRead_UserCod, @AlertaRead_DataHora, @Alerta_Codigo)", GxErrorMask.GX_NOMASK,prmT004R10)
             ,new CursorDef("T004R11", "UPDATE [AlertaRead] SET [AlertaRead_UserCod]=@AlertaRead_UserCod, [AlertaRead_DataHora]=@AlertaRead_DataHora  WHERE [Alerta_Codigo] = @Alerta_Codigo AND [AlertaRead_Codigo] = @AlertaRead_Codigo", GxErrorMask.GX_NOMASK,prmT004R11)
             ,new CursorDef("T004R12", "DELETE FROM [AlertaRead]  WHERE [Alerta_Codigo] = @Alerta_Codigo AND [AlertaRead_Codigo] = @AlertaRead_Codigo", GxErrorMask.GX_NOMASK,prmT004R12)
             ,new CursorDef("T004R13", "SELECT [Alerta_Codigo], [AlertaRead_Codigo] FROM [AlertaRead] WITH (NOLOCK) ORDER BY [Alerta_Codigo], [AlertaRead_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004R13,100,0,true,false )
             ,new CursorDef("T004R14", "SELECT [Alerta_Codigo] FROM [Alerta] WITH (NOLOCK) WHERE [Alerta_Codigo] = @Alerta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004R14,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameterDatetime(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
