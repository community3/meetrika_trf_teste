/*
               File: WP_EstatisticasSistemas
        Description: Estatisticas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:22:3.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_estatisticassistemas : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_estatisticassistemas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_estatisticassistemas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavMes = new GXCombobox();
         cmbavAno = new GXCombobox();
         dynavContratada_codigo = new GXCombobox();
         cmbavGraficar = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADA_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV30WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADA_CODIGOEH2( AV30WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_30 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_30_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_30_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n52Contratada_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               A509ContagemrResultado_SistemaSigla = GetNextPar( );
               n509ContagemrResultado_SistemaSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A509ContagemrResultado_SistemaSigla", A509ContagemrResultado_SistemaSigla);
               A484ContagemResultado_StatusDmn = GetNextPar( );
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV30WWPContext);
               A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n490ContagemResultado_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
               AV68Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contratada_Codigo), 6, 0)));
               A489ContagemResultado_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n489ContagemResultado_SistemaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A489ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A489ContagemResultado_SistemaCod), 6, 0)));
               AV32FltSistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32FltSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32FltSistema_Codigo), 6, 0)));
               A471ContagemResultado_DataDmn = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A471ContagemResultado_DataDmn", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
               AV28Mes = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Mes), 10, 0)));
               AV27Ano = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Ano), 4, 0)));
               A574ContagemResultado_PFFinal = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
               A773ContagemResultadoItem_ContagemCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n773ContagemResultadoItem_ContagemCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A773ContagemResultadoItem_ContagemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A773ContagemResultadoItem_ContagemCod), 6, 0)));
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               A775ContagemResultadoItem_Tipo = GetNextPar( );
               n775ContagemResultadoItem_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A775ContagemResultadoItem_Tipo", A775ContagemResultadoItem_Tipo);
               A779ContagemResultadoItem_CP = GetNextPar( );
               n779ContagemResultadoItem_CP = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A779ContagemResultadoItem_CP", A779ContagemResultadoItem_CP);
               A780ContagemResultadoItem_PFB = NumberUtil.Val( GetNextPar( ), ".");
               n780ContagemResultadoItem_PFB = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A780ContagemResultadoItem_PFB", StringUtil.LTrim( StringUtil.Str( A780ContagemResultadoItem_PFB, 14, 5)));
               AV66DmnTtlD = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66DmnTtlD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66DmnTtlD), 12, 0)));
               AV40mDmn = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40mDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40mDmn), 4, 0)));
               AV65PFTtlD = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65PFTtlD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65PFTtlD), 12, 0)));
               AV41mPF = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41mPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41mPF), 4, 0)));
               AV59CpBttlD = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59CpBttlD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59CpBttlD), 12, 0)));
               AV42mCpB = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42mCpB", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42mCpB), 4, 0)));
               AV60CpMttlD = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60CpMttlD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60CpMttlD), 12, 0)));
               AV43mCpM = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43mCpM", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43mCpM), 4, 0)));
               AV61CpAttlD = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61CpAttlD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61CpAttlD), 12, 0)));
               AV44mCpA = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44mCpA", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44mCpA), 4, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( A52Contratada_AreaTrabalhoCod, A509ContagemrResultado_SistemaSigla, A484ContagemResultado_StatusDmn, AV30WWPContext, A490ContagemResultado_ContratadaCod, AV68Contratada_Codigo, A489ContagemResultado_SistemaCod, AV32FltSistema_Codigo, A471ContagemResultado_DataDmn, AV28Mes, AV27Ano, A574ContagemResultado_PFFinal, A773ContagemResultadoItem_ContagemCod, A456ContagemResultado_Codigo, A775ContagemResultadoItem_Tipo, A779ContagemResultadoItem_CP, A780ContagemResultadoItem_PFB, AV66DmnTtlD, AV40mDmn, AV65PFTtlD, AV41mPF, AV59CpBttlD, AV42mCpB, AV60CpMttlD, AV43mCpM, AV61CpAttlD, AV44mCpA) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAEH2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTEH2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020621622340");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_estatisticassistemas.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_30", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_30), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRRESULTADO_SISTEMASIGLA", StringUtil.RTrim( A509ContagemrResultado_SistemaSigla));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A489ContagemResultado_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATADMN", context.localUtil.DToC( A471ContagemResultado_DataDmn, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOITEM_CONTAGEMCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A773ContagemResultadoItem_ContagemCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOITEM_TIPO", A775ContagemResultadoItem_Tipo);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOITEM_CP", StringUtil.RTrim( A779ContagemResultadoItem_CP));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOITEM_PFB", StringUtil.LTrim( StringUtil.NToC( A780ContagemResultadoItem_PFB, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vDMNTTLD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66DmnTtlD), 12, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMDMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40mDmn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPFTTLD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65PFTtlD), 12, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMPF", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41mPF), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCPBTTLD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59CpBttlD), 12, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMCPB", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42mCpB), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCPMTTLD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60CpMttlD), 12, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMCPM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43mCpM), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCPATTLD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61CpAttlD), 12, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMCPA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44mCpA), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV30WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV30WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFFINAL", StringUtil.LTrim( StringUtil.NToC( A574ContagemResultado_PFFinal, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDDMNTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV29QtdDmnTtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7PFTtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCPBTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV47CpBttl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCPMTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV48CpMttl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCPATTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV49CpAttl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDALITTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV34QtdALITtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDAIETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV35QtdAIETtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDEETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV37QtdEETtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDCETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36QtdCETtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDSETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV38QtdSETtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDINMTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV39QtdINMTtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDDMNTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV29QtdDmnTtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7PFTtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCPBTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV47CpBttl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCPMTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV48CpMttl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCPATTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV49CpAttl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDALITTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV34QtdALITtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDAIETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV35QtdAIETtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDEETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV37QtdEETtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDCETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36QtdCETtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDSETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV38QtdSETtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDINMTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV39QtdINMTtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDDMNTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV29QtdDmnTtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7PFTtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCPBTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV47CpBttl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCPMTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV48CpMttl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCPATTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV49CpAttl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDALITTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV34QtdALITtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDAIETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV35QtdAIETtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDEETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV37QtdEETtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDCETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36QtdCETtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDSETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV38QtdSETtl), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDINMTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV39QtdINMTtl), "ZZZZZZZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEEH2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTEH2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_estatisticassistemas.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_EstatisticasSistemas" ;
      }

      public override String GetPgmdesc( )
      {
         return "Estatisticas" ;
      }

      protected void WBEH0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_EH2( true) ;
         }
         else
         {
            wb_table1_2_EH2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_EH2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTEH2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Estatisticas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPEH0( ) ;
      }

      protected void WSEH2( )
      {
         STARTEH2( ) ;
         EVTEH2( ) ;
      }

      protected void EVTEH2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11EH2 */
                              E11EH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_30_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
                              SubsflControlProps_302( ) ;
                              AV31Sistema_Sigla = StringUtil.Upper( cgiGet( edtavSistema_sigla_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSistema_sigla_Internalname, AV31Sistema_Sigla);
                              AV5QtdDmn = (long)(context.localUtil.CToN( cgiGet( edtavQtddmn_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtddmn_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5QtdDmn), 12, 0)));
                              AV6QtdDmnPrc = context.localUtil.CToN( cgiGet( edtavQtddmnprc_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtddmnprc_Internalname, StringUtil.LTrim( StringUtil.Str( AV6QtdDmnPrc, 6, 2)));
                              AV33PF = context.localUtil.CToN( cgiGet( edtavPf_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPf_Internalname, StringUtil.LTrim( StringUtil.Str( AV33PF, 14, 5)));
                              AV8PFPrc = context.localUtil.CToN( cgiGet( edtavPfprc_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfprc_Internalname, StringUtil.LTrim( StringUtil.Str( AV8PFPrc, 6, 2)));
                              AV9CpB = (long)(context.localUtil.CToN( cgiGet( edtavCpb_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpb_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9CpB), 12, 0)));
                              AV12CpBPrc = context.localUtil.CToN( cgiGet( edtavCpbprc_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpbprc_Internalname, StringUtil.LTrim( StringUtil.Str( AV12CpBPrc, 6, 2)));
                              AV10CpM = (long)(context.localUtil.CToN( cgiGet( edtavCpm_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpm_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10CpM), 12, 0)));
                              AV13CpMPrc = context.localUtil.CToN( cgiGet( edtavCpmprc_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpmprc_Internalname, StringUtil.LTrim( StringUtil.Str( AV13CpMPrc, 6, 2)));
                              AV11CpA = (long)(context.localUtil.CToN( cgiGet( edtavCpa_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpa_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11CpA), 12, 0)));
                              AV14CpAPrc = context.localUtil.CToN( cgiGet( edtavCpaprc_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpaprc_Internalname, StringUtil.LTrim( StringUtil.Str( AV14CpAPrc, 6, 2)));
                              AV15QtdALI = (long)(context.localUtil.CToN( cgiGet( edtavQtdali_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdali_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15QtdALI), 12, 0)));
                              AV16QtdAIE = (long)(context.localUtil.CToN( cgiGet( edtavQtdaie_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdaie_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16QtdAIE), 12, 0)));
                              AV17QtdEE = (long)(context.localUtil.CToN( cgiGet( edtavQtdee_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdee_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV17QtdEE), 12, 0)));
                              AV18QtdCE = (long)(context.localUtil.CToN( cgiGet( edtavQtdce_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdce_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV18QtdCE), 12, 0)));
                              AV19QtdSE = (long)(context.localUtil.CToN( cgiGet( edtavQtdse_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdse_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV19QtdSE), 12, 0)));
                              AV20QtdINM = (long)(context.localUtil.CToN( cgiGet( edtavQtdinm_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdinm_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV20QtdINM), 12, 0)));
                              AV21PFALI = context.localUtil.CToN( cgiGet( edtavPfali_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfali_Internalname, StringUtil.LTrim( StringUtil.Str( AV21PFALI, 14, 5)));
                              AV22PFAIE = context.localUtil.CToN( cgiGet( edtavPfaie_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfaie_Internalname, StringUtil.LTrim( StringUtil.Str( AV22PFAIE, 14, 5)));
                              AV23PFEE = context.localUtil.CToN( cgiGet( edtavPfee_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfee_Internalname, StringUtil.LTrim( StringUtil.Str( AV23PFEE, 14, 5)));
                              AV24PFCE = context.localUtil.CToN( cgiGet( edtavPfce_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfce_Internalname, StringUtil.LTrim( StringUtil.Str( AV24PFCE, 14, 5)));
                              AV25PFSE = context.localUtil.CToN( cgiGet( edtavPfse_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfse_Internalname, StringUtil.LTrim( StringUtil.Str( AV25PFSE, 14, 5)));
                              AV26PFINM = context.localUtil.CToN( cgiGet( edtavPfinm_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfinm_Internalname, StringUtil.LTrim( StringUtil.Str( AV26PFINM, 14, 5)));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E12EH2 */
                                    E12EH2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13EH2 */
                                    E13EH2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEEH2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAEH2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavMes.Name = "vMES";
            cmbavMes.WebTags = "";
            cmbavMes.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 10, 0)), "Todos", 0);
            cmbavMes.addItem("1", "Janeiro", 0);
            cmbavMes.addItem("2", "Fevereiro", 0);
            cmbavMes.addItem("3", "Mar�o", 0);
            cmbavMes.addItem("4", "Abril", 0);
            cmbavMes.addItem("5", "Maio", 0);
            cmbavMes.addItem("6", "Junho", 0);
            cmbavMes.addItem("7", "Julho", 0);
            cmbavMes.addItem("8", "Agosto", 0);
            cmbavMes.addItem("9", "Setembro", 0);
            cmbavMes.addItem("10", "Outubro", 0);
            cmbavMes.addItem("11", "Novembro", 0);
            cmbavMes.addItem("12", "Dezembro", 0);
            if ( cmbavMes.ItemCount > 0 )
            {
               AV28Mes = (long)(NumberUtil.Val( cmbavMes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28Mes), 10, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Mes), 10, 0)));
            }
            cmbavAno.Name = "vANO";
            cmbavAno.WebTags = "";
            cmbavAno.addItem("2011", "2011", 0);
            if ( cmbavAno.ItemCount > 0 )
            {
               AV27Ano = (short)(NumberUtil.Val( cmbavAno.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27Ano), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Ano), 4, 0)));
            }
            dynavContratada_codigo.Name = "vCONTRATADA_CODIGO";
            dynavContratada_codigo.WebTags = "";
            cmbavGraficar.Name = "vGRAFICAR";
            cmbavGraficar.WebTags = "";
            cmbavGraficar.addItem("DXS", "Demandas por Status", 0);
            cmbavGraficar.addItem("PFS", "PF por Status", 0);
            cmbavGraficar.addItem("PXS", "PF por Sistema", 0);
            cmbavGraficar.addItem("UDI", "Utiliza��o dos INM", 0);
            cmbavGraficar.addItem("UDC", "Utiliza��o do Contrato", 0);
            cmbavGraficar.addItem("PUC", "Previs�o de uso do Contrato", 0);
            cmbavGraficar.addItem("DMN", "Demandas por Sistema", 0);
            cmbavGraficar.addItem("FND", "Fun��es de Dados", 0);
            cmbavGraficar.addItem("FNT", "Fn Dados por Sistema", 0);
            cmbavGraficar.addItem("CP", "CP por sistema", 0);
            if ( cmbavGraficar.ItemCount > 0 )
            {
               AV67Graficar = cmbavGraficar.getValidValue(AV67Graficar);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Graficar", AV67Graficar);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavMes_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATADA_CODIGOEH2( wwpbaseobjects.SdtWWPContext AV30WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_CODIGO_dataEH2( AV30WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_CODIGO_htmlEH2( wwpbaseobjects.SdtWWPContext AV30WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_CODIGO_dataEH2( AV30WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratada_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV68Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV68Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contratada_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADA_CODIGO_dataEH2( wwpbaseobjects.SdtWWPContext AV30WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Todas");
         /* Using cursor H00EH2 */
         pr_default.execute(0, new Object[] {AV30WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00EH2_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00EH2_A41Contratada_PessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_302( ) ;
         while ( nGXsfl_30_idx <= nRC_GXsfl_30 )
         {
            sendrow_302( ) ;
            nGXsfl_30_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_30_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_30_idx+1));
            sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
            SubsflControlProps_302( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int A52Contratada_AreaTrabalhoCod ,
                                       String A509ContagemrResultado_SistemaSigla ,
                                       String A484ContagemResultado_StatusDmn ,
                                       wwpbaseobjects.SdtWWPContext AV30WWPContext ,
                                       int A490ContagemResultado_ContratadaCod ,
                                       int AV68Contratada_Codigo ,
                                       int A489ContagemResultado_SistemaCod ,
                                       int AV32FltSistema_Codigo ,
                                       DateTime A471ContagemResultado_DataDmn ,
                                       long AV28Mes ,
                                       short AV27Ano ,
                                       decimal A574ContagemResultado_PFFinal ,
                                       int A773ContagemResultadoItem_ContagemCod ,
                                       int A456ContagemResultado_Codigo ,
                                       String A775ContagemResultadoItem_Tipo ,
                                       String A779ContagemResultadoItem_CP ,
                                       decimal A780ContagemResultadoItem_PFB ,
                                       long AV66DmnTtlD ,
                                       short AV40mDmn ,
                                       long AV65PFTtlD ,
                                       short AV41mPF ,
                                       long AV59CpBttlD ,
                                       short AV42mCpB ,
                                       long AV60CpMttlD ,
                                       short AV43mCpM ,
                                       long AV61CpAttlD ,
                                       short AV44mCpA )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFEH2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavMes.ItemCount > 0 )
         {
            AV28Mes = (long)(NumberUtil.Val( cmbavMes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28Mes), 10, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Mes), 10, 0)));
         }
         if ( cmbavAno.ItemCount > 0 )
         {
            AV27Ano = (short)(NumberUtil.Val( cmbavAno.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27Ano), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Ano), 4, 0)));
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV68Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV68Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contratada_Codigo), 6, 0)));
         }
         if ( cmbavGraficar.ItemCount > 0 )
         {
            AV67Graficar = cmbavGraficar.getValidValue(AV67Graficar);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Graficar", AV67Graficar);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFEH2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavQtddmnttl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtddmnttl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtddmnttl_Enabled), 5, 0)));
         edtavPfttl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfttl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfttl_Enabled), 5, 0)));
         edtavCpbttl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCpbttl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCpbttl_Enabled), 5, 0)));
         edtavCpmttl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCpmttl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCpmttl_Enabled), 5, 0)));
         edtavCpattl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCpattl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCpattl_Enabled), 5, 0)));
         edtavQtdalittl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdalittl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdalittl_Enabled), 5, 0)));
         edtavQtdaiettl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdaiettl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdaiettl_Enabled), 5, 0)));
         edtavQtdeettl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdeettl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdeettl_Enabled), 5, 0)));
         edtavQtdcettl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdcettl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdcettl_Enabled), 5, 0)));
         edtavQtdsettl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdsettl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdsettl_Enabled), 5, 0)));
         edtavQtdinmttl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdinmttl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdinmttl_Enabled), 5, 0)));
      }

      protected void RFEH2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 30;
         /* Execute user event: E11EH2 */
         E11EH2 ();
         nGXsfl_30_idx = 1;
         sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
         SubsflControlProps_302( ) ;
         nGXsfl_30_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "Grid");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_302( ) ;
            /* Execute user event: E13EH2 */
            E13EH2 ();
            wbEnd = 30;
            WBEH0( ) ;
         }
         nGXsfl_30_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPEH0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavQtddmnttl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtddmnttl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtddmnttl_Enabled), 5, 0)));
         edtavPfttl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfttl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfttl_Enabled), 5, 0)));
         edtavCpbttl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCpbttl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCpbttl_Enabled), 5, 0)));
         edtavCpmttl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCpmttl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCpmttl_Enabled), 5, 0)));
         edtavCpattl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCpattl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCpattl_Enabled), 5, 0)));
         edtavQtdalittl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdalittl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdalittl_Enabled), 5, 0)));
         edtavQtdaiettl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdaiettl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdaiettl_Enabled), 5, 0)));
         edtavQtdeettl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdeettl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdeettl_Enabled), 5, 0)));
         edtavQtdcettl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdcettl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdcettl_Enabled), 5, 0)));
         edtavQtdsettl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdsettl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdsettl_Enabled), 5, 0)));
         edtavQtdinmttl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdinmttl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdinmttl_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12EH2 */
         E12EH2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTRATADA_CODIGO_htmlEH2( AV30WWPContext) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vWWPCONTEXT"), AV30WWPContext);
            /* Read variables values. */
            cmbavMes.Name = cmbavMes_Internalname;
            cmbavMes.CurrentValue = cgiGet( cmbavMes_Internalname);
            AV28Mes = (long)(NumberUtil.Val( cgiGet( cmbavMes_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Mes), 10, 0)));
            cmbavAno.Name = cmbavAno_Internalname;
            cmbavAno.CurrentValue = cgiGet( cmbavAno_Internalname);
            AV27Ano = (short)(NumberUtil.Val( cgiGet( cmbavAno_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Ano), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavFltsistema_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFltsistema_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFLTSISTEMA_CODIGO");
               GX_FocusControl = edtavFltsistema_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32FltSistema_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32FltSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32FltSistema_Codigo), 6, 0)));
            }
            else
            {
               AV32FltSistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavFltsistema_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32FltSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32FltSistema_Codigo), 6, 0)));
            }
            dynavContratada_codigo.Name = dynavContratada_codigo_Internalname;
            dynavContratada_codigo.CurrentValue = cgiGet( dynavContratada_codigo_Internalname);
            AV68Contratada_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratada_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contratada_Codigo), 6, 0)));
            cmbavGraficar.Name = cmbavGraficar_Internalname;
            cmbavGraficar.CurrentValue = cgiGet( cmbavGraficar_Internalname);
            AV67Graficar = cgiGet( cmbavGraficar_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Graficar", AV67Graficar);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQtddmnttl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtddmnttl_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDDMNTTL");
               GX_FocusControl = edtavQtddmnttl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29QtdDmnTtl = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29QtdDmnTtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29QtdDmnTtl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDDMNTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV29QtdDmnTtl), "ZZZZZZZZZZZ9")));
            }
            else
            {
               AV29QtdDmnTtl = (long)(context.localUtil.CToN( cgiGet( edtavQtddmnttl_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29QtdDmnTtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29QtdDmnTtl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDDMNTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV29QtdDmnTtl), "ZZZZZZZZZZZ9")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPfttl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPfttl_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFTTL");
               GX_FocusControl = edtavPfttl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7PFTtl = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7PFTtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7PFTtl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7PFTtl), "ZZZZZZZZZZZ9")));
            }
            else
            {
               AV7PFTtl = (long)(context.localUtil.CToN( cgiGet( edtavPfttl_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7PFTtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7PFTtl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7PFTtl), "ZZZZZZZZZZZ9")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCpbttl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCpbttl_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCPBTTL");
               GX_FocusControl = edtavCpbttl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47CpBttl = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47CpBttl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47CpBttl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCPBTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV47CpBttl), "ZZZZZZZZZZZ9")));
            }
            else
            {
               AV47CpBttl = (long)(context.localUtil.CToN( cgiGet( edtavCpbttl_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47CpBttl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47CpBttl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCPBTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV47CpBttl), "ZZZZZZZZZZZ9")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCpmttl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCpmttl_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCPMTTL");
               GX_FocusControl = edtavCpmttl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48CpMttl = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48CpMttl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48CpMttl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCPMTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV48CpMttl), "ZZZZZZZZZZZ9")));
            }
            else
            {
               AV48CpMttl = (long)(context.localUtil.CToN( cgiGet( edtavCpmttl_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48CpMttl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48CpMttl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCPMTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV48CpMttl), "ZZZZZZZZZZZ9")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCpattl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCpattl_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCPATTL");
               GX_FocusControl = edtavCpattl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49CpAttl = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49CpAttl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49CpAttl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCPATTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV49CpAttl), "ZZZZZZZZZZZ9")));
            }
            else
            {
               AV49CpAttl = (long)(context.localUtil.CToN( cgiGet( edtavCpattl_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49CpAttl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49CpAttl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCPATTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV49CpAttl), "ZZZZZZZZZZZ9")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdalittl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdalittl_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDALITTL");
               GX_FocusControl = edtavQtdalittl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34QtdALITtl = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34QtdALITtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34QtdALITtl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDALITTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV34QtdALITtl), "ZZZZZZZZZZZ9")));
            }
            else
            {
               AV34QtdALITtl = (long)(context.localUtil.CToN( cgiGet( edtavQtdalittl_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34QtdALITtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34QtdALITtl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDALITTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV34QtdALITtl), "ZZZZZZZZZZZ9")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdaiettl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdaiettl_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDAIETTL");
               GX_FocusControl = edtavQtdaiettl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35QtdAIETtl = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35QtdAIETtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35QtdAIETtl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDAIETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV35QtdAIETtl), "ZZZZZZZZZZZ9")));
            }
            else
            {
               AV35QtdAIETtl = (long)(context.localUtil.CToN( cgiGet( edtavQtdaiettl_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35QtdAIETtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35QtdAIETtl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDAIETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV35QtdAIETtl), "ZZZZZZZZZZZ9")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdeettl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdeettl_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDEETTL");
               GX_FocusControl = edtavQtdeettl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37QtdEETtl = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37QtdEETtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37QtdEETtl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDEETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV37QtdEETtl), "ZZZZZZZZZZZ9")));
            }
            else
            {
               AV37QtdEETtl = (long)(context.localUtil.CToN( cgiGet( edtavQtdeettl_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37QtdEETtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37QtdEETtl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDEETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV37QtdEETtl), "ZZZZZZZZZZZ9")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdcettl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdcettl_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDCETTL");
               GX_FocusControl = edtavQtdcettl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36QtdCETtl = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36QtdCETtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36QtdCETtl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDCETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36QtdCETtl), "ZZZZZZZZZZZ9")));
            }
            else
            {
               AV36QtdCETtl = (long)(context.localUtil.CToN( cgiGet( edtavQtdcettl_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36QtdCETtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36QtdCETtl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDCETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36QtdCETtl), "ZZZZZZZZZZZ9")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdsettl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdsettl_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDSETTL");
               GX_FocusControl = edtavQtdsettl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38QtdSETtl = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38QtdSETtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38QtdSETtl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDSETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV38QtdSETtl), "ZZZZZZZZZZZ9")));
            }
            else
            {
               AV38QtdSETtl = (long)(context.localUtil.CToN( cgiGet( edtavQtdsettl_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38QtdSETtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38QtdSETtl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDSETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV38QtdSETtl), "ZZZZZZZZZZZ9")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdinmttl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdinmttl_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDINMTTL");
               GX_FocusControl = edtavQtdinmttl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39QtdINMTtl = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39QtdINMTtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39QtdINMTtl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDINMTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV39QtdINMTtl), "ZZZZZZZZZZZ9")));
            }
            else
            {
               AV39QtdINMTtl = (long)(context.localUtil.CToN( cgiGet( edtavQtdinmttl_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39QtdINMTtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39QtdINMTtl), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDINMTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV39QtdINMTtl), "ZZZZZZZZZZZ9")));
            }
            /* Read saved values. */
            nRC_GXsfl_30 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_30"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12EH2 */
         E12EH2 ();
         if (returnInSub) return;
      }

      protected void E12EH2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV30WWPContext) ;
         AV57i = 2012;
         while ( AV57i <= DateTimeUtil.Year( Gx_date) )
         {
            cmbavAno.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV57i), 4, 0)), StringUtil.Str( (decimal)(AV57i), 4, 0), 0);
            AV57i = (short)(AV57i+1);
         }
         AV28Mes = DateTimeUtil.Month( Gx_date);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Mes), 10, 0)));
         AV27Ano = (short)(DateTimeUtil.Year( Gx_date));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Ano), 4, 0)));
         if ( AV30WWPContext.gxTpr_Userehadministradorgam )
         {
            /* Using cursor H00EH3 */
            pr_default.execute(1, new Object[] {AV30WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A516Contratada_TipoFabrica = H00EH3_A516Contratada_TipoFabrica[0];
               A52Contratada_AreaTrabalhoCod = H00EH3_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00EH3_n52Contratada_AreaTrabalhoCod[0];
               A39Contratada_Codigo = H00EH3_A39Contratada_Codigo[0];
               AV68Contratada_Codigo = A39Contratada_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contratada_Codigo), 6, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(1);
            }
            pr_default.close(1);
         }
         else
         {
            AV68Contratada_Codigo = AV30WWPContext.gxTpr_Contratada_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contratada_Codigo), 6, 0)));
         }
      }

      protected void E11EH2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV29QtdDmnTtl = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29QtdDmnTtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29QtdDmnTtl), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDDMNTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV29QtdDmnTtl), "ZZZZZZZZZZZ9")));
         AV7PFTtl = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7PFTtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7PFTtl), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7PFTtl), "ZZZZZZZZZZZ9")));
         AV34QtdALITtl = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34QtdALITtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34QtdALITtl), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDALITTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV34QtdALITtl), "ZZZZZZZZZZZ9")));
         AV35QtdAIETtl = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35QtdAIETtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35QtdAIETtl), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDAIETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV35QtdAIETtl), "ZZZZZZZZZZZ9")));
         AV36QtdCETtl = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36QtdCETtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36QtdCETtl), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDCETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36QtdCETtl), "ZZZZZZZZZZZ9")));
         AV37QtdEETtl = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37QtdEETtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37QtdEETtl), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDEETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV37QtdEETtl), "ZZZZZZZZZZZ9")));
         AV38QtdSETtl = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38QtdSETtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38QtdSETtl), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDSETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV38QtdSETtl), "ZZZZZZZZZZZ9")));
         AV39QtdINMTtl = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39QtdINMTtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39QtdINMTtl), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDINMTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV39QtdINMTtl), "ZZZZZZZZZZZ9")));
         AV47CpBttl = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47CpBttl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47CpBttl), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCPBTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV47CpBttl), "ZZZZZZZZZZZ9")));
         AV48CpMttl = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48CpMttl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48CpMttl), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCPMTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV48CpMttl), "ZZZZZZZZZZZ9")));
         AV49CpAttl = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49CpAttl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49CpAttl), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCPATTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV49CpAttl), "ZZZZZZZZZZZ9")));
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV68Contratada_Codigo ,
                                              AV32FltSistema_Codigo ,
                                              AV28Mes ,
                                              AV27Ano ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A489ContagemResultado_SistemaCod ,
                                              A471ContagemResultado_DataDmn ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV30WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A52Contratada_AreaTrabalhoCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.LONG, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00EH4 */
         pr_default.execute(2, new Object[] {AV30WWPContext.gxTpr_Areatrabalho_codigo, AV68Contratada_Codigo, AV32FltSistema_Codigo, AV28Mes, AV27Ano});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A52Contratada_AreaTrabalhoCod = H00EH4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EH4_n52Contratada_AreaTrabalhoCod[0];
            A471ContagemResultado_DataDmn = H00EH4_A471ContagemResultado_DataDmn[0];
            A489ContagemResultado_SistemaCod = H00EH4_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00EH4_n489ContagemResultado_SistemaCod[0];
            A490ContagemResultado_ContratadaCod = H00EH4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00EH4_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = H00EH4_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00EH4_n484ContagemResultado_StatusDmn[0];
            A509ContagemrResultado_SistemaSigla = H00EH4_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00EH4_n509ContagemrResultado_SistemaSigla[0];
            A456ContagemResultado_Codigo = H00EH4_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = H00EH4_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00EH4_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = H00EH4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EH4_n52Contratada_AreaTrabalhoCod[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A574ContagemResultado_PFFinal = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
            AV29QtdDmnTtl = (long)(AV29QtdDmnTtl+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29QtdDmnTtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29QtdDmnTtl), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDDMNTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV29QtdDmnTtl), "ZZZZZZZZZZZ9")));
            AV7PFTtl = (long)(AV7PFTtl+A574ContagemResultado_PFFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7PFTtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7PFTtl), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7PFTtl), "ZZZZZZZZZZZ9")));
            /* Using cursor H00EH5 */
            pr_default.execute(3, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A773ContagemResultadoItem_ContagemCod = H00EH5_A773ContagemResultadoItem_ContagemCod[0];
               n773ContagemResultadoItem_ContagemCod = H00EH5_n773ContagemResultadoItem_ContagemCod[0];
               A775ContagemResultadoItem_Tipo = H00EH5_A775ContagemResultadoItem_Tipo[0];
               n775ContagemResultadoItem_Tipo = H00EH5_n775ContagemResultadoItem_Tipo[0];
               A779ContagemResultadoItem_CP = H00EH5_A779ContagemResultadoItem_CP[0];
               n779ContagemResultadoItem_CP = H00EH5_n779ContagemResultadoItem_CP[0];
               if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "ALI") == 0 )
               {
                  AV34QtdALITtl = (long)(AV34QtdALITtl+1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34QtdALITtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34QtdALITtl), 12, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDALITTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV34QtdALITtl), "ZZZZZZZZZZZ9")));
               }
               else if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "AIE") == 0 )
               {
                  AV35QtdAIETtl = (long)(AV35QtdAIETtl+1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35QtdAIETtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35QtdAIETtl), 12, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDAIETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV35QtdAIETtl), "ZZZZZZZZZZZ9")));
               }
               else if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "CE") == 0 )
               {
                  AV36QtdCETtl = (long)(AV36QtdCETtl+1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36QtdCETtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36QtdCETtl), 12, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDCETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36QtdCETtl), "ZZZZZZZZZZZ9")));
               }
               else if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "EE") == 0 )
               {
                  AV37QtdEETtl = (long)(AV37QtdEETtl+1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37QtdEETtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37QtdEETtl), 12, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDEETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV37QtdEETtl), "ZZZZZZZZZZZ9")));
               }
               else if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "SE") == 0 )
               {
                  AV38QtdSETtl = (long)(AV38QtdSETtl+1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38QtdSETtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38QtdSETtl), 12, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDSETTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV38QtdSETtl), "ZZZZZZZZZZZ9")));
               }
               else if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "INM") == 0 )
               {
                  AV39QtdINMTtl = (long)(AV39QtdINMTtl+1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39QtdINMTtl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39QtdINMTtl), 12, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDINMTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV39QtdINMTtl), "ZZZZZZZZZZZ9")));
               }
               if ( StringUtil.StrCmp(A779ContagemResultadoItem_CP, "B") == 0 )
               {
                  AV47CpBttl = (long)(AV47CpBttl+1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47CpBttl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47CpBttl), 12, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCPBTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV47CpBttl), "ZZZZZZZZZZZ9")));
               }
               else if ( StringUtil.StrCmp(A779ContagemResultadoItem_CP, "M") == 0 )
               {
                  AV48CpMttl = (long)(AV48CpMttl+1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48CpMttl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48CpMttl), 12, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCPMTTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV48CpMttl), "ZZZZZZZZZZZ9")));
               }
               else if ( StringUtil.StrCmp(A779ContagemResultadoItem_CP, "A") == 0 )
               {
                  AV49CpAttl = (long)(AV49CpAttl+1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49CpAttl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49CpAttl), 12, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCPATTL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV49CpAttl), "ZZZZZZZZZZZ9")));
               }
               pr_default.readNext(3);
            }
            pr_default.close(3);
            pr_default.readNext(2);
         }
         pr_default.close(2);
         AV40mDmn = 100;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40mDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40mDmn), 4, 0)));
         AV41mPF = 100;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41mPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41mPF), 4, 0)));
         AV42mCpB = 100;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42mCpB", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42mCpB), 4, 0)));
         AV43mCpM = 100;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43mCpM", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43mCpM), 4, 0)));
         AV44mCpA = 100;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44mCpA", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44mCpA), 4, 0)));
         AV66DmnTtlD = AV29QtdDmnTtl;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66DmnTtlD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66DmnTtlD), 12, 0)));
         AV65PFTtlD = AV7PFTtl;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65PFTtlD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65PFTtlD), 12, 0)));
         AV59CpBttlD = AV47CpBttl;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59CpBttlD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59CpBttlD), 12, 0)));
         AV60CpMttlD = AV48CpMttl;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60CpMttlD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60CpMttlD), 12, 0)));
         AV61CpAttlD = AV49CpAttl;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61CpAttlD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61CpAttlD), 12, 0)));
         if ( (0==AV66DmnTtlD) )
         {
            AV40mDmn = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40mDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40mDmn), 4, 0)));
            AV66DmnTtlD = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66DmnTtlD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66DmnTtlD), 12, 0)));
         }
         if ( (0==AV65PFTtlD) )
         {
            AV41mPF = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41mPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41mPF), 4, 0)));
            AV65PFTtlD = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65PFTtlD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65PFTtlD), 12, 0)));
         }
         if ( (0==AV59CpBttlD) )
         {
            AV42mCpB = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42mCpB", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42mCpB), 4, 0)));
            AV59CpBttlD = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59CpBttlD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59CpBttlD), 12, 0)));
         }
         if ( (0==AV60CpMttlD) )
         {
            AV43mCpM = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43mCpM", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43mCpM), 4, 0)));
            AV60CpMttlD = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60CpMttlD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60CpMttlD), 12, 0)));
         }
         if ( (0==AV61CpAttlD) )
         {
            AV44mCpA = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44mCpA", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44mCpA), 4, 0)));
            AV61CpAttlD = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61CpAttlD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61CpAttlD), 12, 0)));
         }
      }

      private void E13EH2( )
      {
         /* Load Routine */
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              AV68Contratada_Codigo ,
                                              AV32FltSistema_Codigo ,
                                              AV28Mes ,
                                              AV27Ano ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A489ContagemResultado_SistemaCod ,
                                              A471ContagemResultado_DataDmn ,
                                              AV30WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A52Contratada_AreaTrabalhoCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.LONG, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00EH6 */
         pr_default.execute(4, new Object[] {AV30WWPContext.gxTpr_Areatrabalho_codigo, AV68Contratada_Codigo, AV32FltSistema_Codigo, AV28Mes, AV27Ano});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKEH6 = false;
            A52Contratada_AreaTrabalhoCod = H00EH6_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EH6_n52Contratada_AreaTrabalhoCod[0];
            A484ContagemResultado_StatusDmn = H00EH6_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00EH6_n484ContagemResultado_StatusDmn[0];
            A509ContagemrResultado_SistemaSigla = H00EH6_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00EH6_n509ContagemrResultado_SistemaSigla[0];
            A471ContagemResultado_DataDmn = H00EH6_A471ContagemResultado_DataDmn[0];
            A489ContagemResultado_SistemaCod = H00EH6_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00EH6_n489ContagemResultado_SistemaCod[0];
            A490ContagemResultado_ContratadaCod = H00EH6_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00EH6_n490ContagemResultado_ContratadaCod[0];
            A456ContagemResultado_Codigo = H00EH6_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = H00EH6_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00EH6_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = H00EH6_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EH6_n52Contratada_AreaTrabalhoCod[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A574ContagemResultado_PFFinal = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
            AV31Sistema_Sigla = A509ContagemrResultado_SistemaSigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSistema_sigla_Internalname, AV31Sistema_Sigla);
            AV5QtdDmn = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtddmn_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5QtdDmn), 12, 0)));
            AV6QtdDmnPrc = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtddmnprc_Internalname, StringUtil.LTrim( StringUtil.Str( AV6QtdDmnPrc, 6, 2)));
            AV33PF = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPf_Internalname, StringUtil.LTrim( StringUtil.Str( AV33PF, 14, 5)));
            AV8PFPrc = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfprc_Internalname, StringUtil.LTrim( StringUtil.Str( AV8PFPrc, 6, 2)));
            AV9CpB = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpb_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9CpB), 12, 0)));
            AV10CpM = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpm_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10CpM), 12, 0)));
            AV11CpA = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpa_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11CpA), 12, 0)));
            AV12CpBPrc = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpbprc_Internalname, StringUtil.LTrim( StringUtil.Str( AV12CpBPrc, 6, 2)));
            AV13CpMPrc = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpmprc_Internalname, StringUtil.LTrim( StringUtil.Str( AV13CpMPrc, 6, 2)));
            AV14CpAPrc = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpaprc_Internalname, StringUtil.LTrim( StringUtil.Str( AV14CpAPrc, 6, 2)));
            AV15QtdALI = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdali_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15QtdALI), 12, 0)));
            AV16QtdAIE = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdaie_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16QtdAIE), 12, 0)));
            AV17QtdEE = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdee_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV17QtdEE), 12, 0)));
            AV18QtdCE = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdce_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV18QtdCE), 12, 0)));
            AV19QtdSE = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdse_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV19QtdSE), 12, 0)));
            AV20QtdINM = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdinm_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV20QtdINM), 12, 0)));
            AV21PFALI = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfali_Internalname, StringUtil.LTrim( StringUtil.Str( AV21PFALI, 14, 5)));
            AV22PFAIE = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfaie_Internalname, StringUtil.LTrim( StringUtil.Str( AV22PFAIE, 14, 5)));
            AV23PFEE = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfee_Internalname, StringUtil.LTrim( StringUtil.Str( AV23PFEE, 14, 5)));
            AV24PFCE = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfce_Internalname, StringUtil.LTrim( StringUtil.Str( AV24PFCE, 14, 5)));
            AV25PFSE = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfse_Internalname, StringUtil.LTrim( StringUtil.Str( AV25PFSE, 14, 5)));
            AV26PFINM = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfinm_Internalname, StringUtil.LTrim( StringUtil.Str( AV26PFINM, 14, 5)));
            while ( (pr_default.getStatus(4) != 101) && ( H00EH6_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( StringUtil.StrCmp(H00EH6_A509ContagemrResultado_SistemaSigla[0], A509ContagemrResultado_SistemaSigla) == 0 ) )
            {
               BRKEH6 = false;
               A484ContagemResultado_StatusDmn = H00EH6_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00EH6_n484ContagemResultado_StatusDmn[0];
               A471ContagemResultado_DataDmn = H00EH6_A471ContagemResultado_DataDmn[0];
               A489ContagemResultado_SistemaCod = H00EH6_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00EH6_n489ContagemResultado_SistemaCod[0];
               A490ContagemResultado_ContratadaCod = H00EH6_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00EH6_n490ContagemResultado_ContratadaCod[0];
               A456ContagemResultado_Codigo = H00EH6_A456ContagemResultado_Codigo[0];
               if ( ! ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 ) )
               {
                  GXt_decimal1 = A574ContagemResultado_PFFinal;
                  new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  A574ContagemResultado_PFFinal = GXt_decimal1;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
                  AV5QtdDmn = (long)(AV5QtdDmn+1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtddmn_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5QtdDmn), 12, 0)));
                  AV33PF = (decimal)(AV33PF+A574ContagemResultado_PFFinal);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPf_Internalname, StringUtil.LTrim( StringUtil.Str( AV33PF, 14, 5)));
                  /* Using cursor H00EH7 */
                  pr_default.execute(5, new Object[] {A456ContagemResultado_Codigo});
                  while ( (pr_default.getStatus(5) != 101) )
                  {
                     A773ContagemResultadoItem_ContagemCod = H00EH7_A773ContagemResultadoItem_ContagemCod[0];
                     n773ContagemResultadoItem_ContagemCod = H00EH7_n773ContagemResultadoItem_ContagemCod[0];
                     A775ContagemResultadoItem_Tipo = H00EH7_A775ContagemResultadoItem_Tipo[0];
                     n775ContagemResultadoItem_Tipo = H00EH7_n775ContagemResultadoItem_Tipo[0];
                     A780ContagemResultadoItem_PFB = H00EH7_A780ContagemResultadoItem_PFB[0];
                     n780ContagemResultadoItem_PFB = H00EH7_n780ContagemResultadoItem_PFB[0];
                     A779ContagemResultadoItem_CP = H00EH7_A779ContagemResultadoItem_CP[0];
                     n779ContagemResultadoItem_CP = H00EH7_n779ContagemResultadoItem_CP[0];
                     if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "ALI") == 0 )
                     {
                        AV15QtdALI = (long)(AV15QtdALI+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdali_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15QtdALI), 12, 0)));
                        AV21PFALI = (decimal)(AV21PFALI+A780ContagemResultadoItem_PFB);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfali_Internalname, StringUtil.LTrim( StringUtil.Str( AV21PFALI, 14, 5)));
                     }
                     else if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "AIE") == 0 )
                     {
                        AV16QtdAIE = (long)(AV16QtdAIE+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdaie_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16QtdAIE), 12, 0)));
                        AV22PFAIE = (decimal)(AV22PFAIE+A780ContagemResultadoItem_PFB);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfaie_Internalname, StringUtil.LTrim( StringUtil.Str( AV22PFAIE, 14, 5)));
                     }
                     else if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "CE") == 0 )
                     {
                        AV18QtdCE = (long)(AV18QtdCE+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdce_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV18QtdCE), 12, 0)));
                        AV24PFCE = (decimal)(AV24PFCE+A780ContagemResultadoItem_PFB);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfce_Internalname, StringUtil.LTrim( StringUtil.Str( AV24PFCE, 14, 5)));
                     }
                     else if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "EE") == 0 )
                     {
                        AV17QtdEE = (long)(AV17QtdEE+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdee_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV17QtdEE), 12, 0)));
                        AV23PFEE = (decimal)(AV23PFEE+A780ContagemResultadoItem_PFB);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfee_Internalname, StringUtil.LTrim( StringUtil.Str( AV23PFEE, 14, 5)));
                     }
                     else if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "SE") == 0 )
                     {
                        AV19QtdSE = (long)(AV19QtdSE+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdse_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV19QtdSE), 12, 0)));
                        AV25PFSE = (decimal)(AV25PFSE+A780ContagemResultadoItem_PFB);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfse_Internalname, StringUtil.LTrim( StringUtil.Str( AV25PFSE, 14, 5)));
                     }
                     else if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "INM") == 0 )
                     {
                        AV20QtdINM = (long)(AV20QtdINM+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdinm_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV20QtdINM), 12, 0)));
                        AV26PFINM = (decimal)(AV26PFINM+A780ContagemResultadoItem_PFB);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfinm_Internalname, StringUtil.LTrim( StringUtil.Str( AV26PFINM, 14, 5)));
                     }
                     if ( StringUtil.StrCmp(A779ContagemResultadoItem_CP, "B") == 0 )
                     {
                        AV9CpB = (long)(AV9CpB+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpb_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9CpB), 12, 0)));
                     }
                     else if ( StringUtil.StrCmp(A779ContagemResultadoItem_CP, "M") == 0 )
                     {
                        AV10CpM = (long)(AV10CpM+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpm_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10CpM), 12, 0)));
                     }
                     else if ( StringUtil.StrCmp(A779ContagemResultadoItem_CP, "A") == 0 )
                     {
                        AV11CpA = (long)(AV11CpA+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpa_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11CpA), 12, 0)));
                     }
                     pr_default.readNext(5);
                  }
                  pr_default.close(5);
               }
               BRKEH6 = true;
               pr_default.readNext(4);
            }
            AV6QtdDmnPrc = (decimal)(AV5QtdDmn/ (decimal)(AV66DmnTtlD)*AV40mDmn);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtddmnprc_Internalname, StringUtil.LTrim( StringUtil.Str( AV6QtdDmnPrc, 6, 2)));
            AV8PFPrc = (decimal)(AV33PF/ (decimal)(AV65PFTtlD)*AV41mPF);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfprc_Internalname, StringUtil.LTrim( StringUtil.Str( AV8PFPrc, 6, 2)));
            AV12CpBPrc = (decimal)(AV9CpB/ (decimal)(AV59CpBttlD)*AV42mCpB);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpbprc_Internalname, StringUtil.LTrim( StringUtil.Str( AV12CpBPrc, 6, 2)));
            AV13CpMPrc = (decimal)(AV10CpM/ (decimal)(AV60CpMttlD)*AV43mCpM);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpmprc_Internalname, StringUtil.LTrim( StringUtil.Str( AV13CpMPrc, 6, 2)));
            AV14CpAPrc = (decimal)(AV11CpA/ (decimal)(AV61CpAttlD)*AV44mCpA);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCpaprc_Internalname, StringUtil.LTrim( StringUtil.Str( AV14CpAPrc, 6, 2)));
            sendrow_302( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_30_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(30, GridRow);
            }
            if ( ! BRKEH6 )
            {
               BRKEH6 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      protected void wb_table1_2_EH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_EH2( true) ;
         }
         else
         {
            wb_table2_5_EH2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_EH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; ") ;
            wb_table3_12_EH2( true) ;
         }
         else
         {
            wb_table3_12_EH2( false) ;
         }
         return  ;
      }

      protected void wb_table3_12_EH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"30\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "Grid", 0, "", "", 1, 1, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Qtd Dmn") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "% Dmn") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PF") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "% PF") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "CP B") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "% CP B") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "CP M") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "% CP M") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "CP A") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "% CP A") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Qtd ALI") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Qtd AIE") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Qtd EE") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Qtd CE") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Qtd SE") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Qtd INM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PFAIE") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PF AIE") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PF EE") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PF CE") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PFSE") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PF INM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "Grid");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV31Sistema_Sigla));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5QtdDmn), 12, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV6QtdDmnPrc, 6, 2, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV33PF, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV8PFPrc, 6, 2, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9CpB), 12, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV12CpBPrc, 6, 2, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10CpM), 12, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV13CpMPrc, 6, 2, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11CpA), 12, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV14CpAPrc, 6, 2, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15QtdALI), 12, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16QtdAIE), 12, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17QtdEE), 12, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18QtdCE), 12, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19QtdSE), 12, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20QtdINM), 12, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV21PFALI, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV22PFAIE, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV23PFEE, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV24PFCE, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV25PFSE, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV26PFINM, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 30 )
         {
            wbEnd = 0;
            nRC_GXsfl_30 = (short)(nGXsfl_30_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_56_EH2( true) ;
         }
         else
         {
            wb_table4_56_EH2( false) ;
         }
         return  ;
      }

      protected void wb_table4_56_EH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_EH2e( true) ;
         }
         else
         {
            wb_table1_2_EH2e( false) ;
         }
      }

      protected void wb_table4_56_EH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( StringUtil.StrCmp(context.BuildHTMLColor( (int)(0x000000))+";", "") != 0 )
            {
               sStyleString = sStyleString + " border-color: " + context.BuildHTMLColor( (int)(0x000000)) + ";";
            }
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "right", "", 1, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockquantidade_Internalname, "TOTAIS - Dmn:", "", "", lblTextblockquantidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQtddmnttl_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29QtdDmnTtl), 12, 0, ",", "")), ((edtavQtddmnttl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV29QtdDmnTtl), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV29QtdDmnTtl), "ZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQtddmnttl_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavQtddmnttl_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfbfstotal_Internalname, "PF: ", "", "", lblTextblockcontagemresultado_pfbfstotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPfttl_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7PFTtl), 12, 0, ",", "")), ((edtavPfttl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV7PFTtl), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV7PFTtl), "ZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPfttl_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavPfttl_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfstotal_Internalname, "CP B: ", "", "", lblTextblockcontagemresultado_pflfstotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCpbttl_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47CpBttl), 12, 0, ",", "")), ((edtavCpbttl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV47CpBttl), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV47CpBttl), "ZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCpbttl_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavCpbttl_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfbfmtotal_Internalname, "CP M: ", "", "", lblTextblockcontagemresultado_pfbfmtotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCpmttl_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48CpMttl), 12, 0, ",", "")), ((edtavCpmttl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV48CpMttl), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV48CpMttl), "ZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCpmttl_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavCpmttl_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfmtotal_Internalname, "CP A: ", "", "", lblTextblockcontagemresultado_pflfmtotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCpattl_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49CpAttl), 12, 0, ",", "")), ((edtavCpattl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV49CpAttl), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV49CpAttl), "ZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCpattl_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavCpattl_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfmtotal3_Internalname, "ALI: ", "", "", lblTextblockcontagemresultado_pflfmtotal3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQtdalittl_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34QtdALITtl), 12, 0, ",", "")), ((edtavQtdalittl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV34QtdALITtl), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV34QtdALITtl), "ZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQtdalittl_Jsonclick, 0, "Attribute", "", "", "", 1, edtavQtdalittl_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfmtotal2_Internalname, "AIE: ", "", "", lblTextblockcontagemresultado_pflfmtotal2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQtdaiettl_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35QtdAIETtl), 12, 0, ",", "")), ((edtavQtdaiettl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35QtdAIETtl), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV35QtdAIETtl), "ZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQtdaiettl_Jsonclick, 0, "Attribute", "", "", "", 1, edtavQtdaiettl_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfmtotal4_Internalname, "EE: ", "", "", lblTextblockcontagemresultado_pflfmtotal4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQtdeettl_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37QtdEETtl), 12, 0, ",", "")), ((edtavQtdeettl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV37QtdEETtl), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV37QtdEETtl), "ZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQtdeettl_Jsonclick, 0, "Attribute", "", "", "", 1, edtavQtdeettl_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfmtotal5_Internalname, "CE: ", "", "", lblTextblockcontagemresultado_pflfmtotal5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQtdcettl_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36QtdCETtl), 12, 0, ",", "")), ((edtavQtdcettl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36QtdCETtl), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV36QtdCETtl), "ZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQtdcettl_Jsonclick, 0, "Attribute", "", "", "", 1, edtavQtdcettl_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfmtotal6_Internalname, "SE: ", "", "", lblTextblockcontagemresultado_pflfmtotal6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQtdsettl_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38QtdSETtl), 12, 0, ",", "")), ((edtavQtdsettl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV38QtdSETtl), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV38QtdSETtl), "ZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQtdsettl_Jsonclick, 0, "Attribute", "", "", "", 1, edtavQtdsettl_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfmtotal7_Internalname, "INM: ", "", "", lblTextblockcontagemresultado_pflfmtotal7_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQtdinmttl_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39QtdINMTtl), 12, 0, ",", "")), ((edtavQtdinmttl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV39QtdINMTtl), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV39QtdINMTtl), "ZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQtdinmttl_Jsonclick, 0, "Attribute", "", "", "", 1, edtavQtdinmttl_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_56_EH2e( true) ;
         }
         else
         {
            wb_table4_56_EH2e( false) ;
         }
      }

      protected void wb_table3_12_EH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "M�s:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'" + sGXsfl_30_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavMes, cmbavMes_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV28Mes), 10, 0)), 1, cmbavMes_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "", true, "HLP_WP_EstatisticasSistemas.htm");
            cmbavMes.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28Mes), 10, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMes_Internalname, "Values", (String)(cmbavMes.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Ano:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'" + sGXsfl_30_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAno, cmbavAno_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV27Ano), 4, 0)), 1, cmbavAno_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WP_EstatisticasSistemas.htm");
            cmbavAno.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27Ano), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAno_Internalname, "Values", (String)(cmbavAno.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Sistema:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFltsistema_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32FltSistema_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32FltSistema_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFltsistema_codigo_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Contratada:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'" + sGXsfl_30_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_codigo, dynavContratada_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV68Contratada_Codigo), 6, 0)), 1, dynavContratada_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "", true, "HLP_WP_EstatisticasSistemas.htm");
            dynavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV68Contratada_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Values", (String)(dynavContratada_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "Graficar por:", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_30_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavGraficar, cmbavGraficar_Internalname, StringUtil.RTrim( AV67Graficar), 1, cmbavGraficar_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_WP_EstatisticasSistemas.htm");
            cmbavGraficar.CurrentValue = StringUtil.RTrim( AV67Graficar);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGraficar_Internalname, "Values", (String)(cmbavGraficar.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgExportgraphicdmn_Internalname, context.GetImagePath( "eb16c005-5a7d-48a8-a16c-d7dcc1909b6e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Gerar gr�fico", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgExportgraphicdmn_Jsonclick, "'"+""+"'"+",false,"+"'"+"e14eh1_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(30), 2, 0)+","+"null"+");", "Atualizar", bttButton1_Jsonclick, 5, "Atualizar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EREFRESH."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_12_EH2e( true) ;
         }
         else
         {
            wb_table3_12_EH2e( false) ;
         }
      }

      protected void wb_table2_5_EH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable4_Internalname, tblTable4_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Estat�stica dos Sistemas", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_EstatisticasSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_EH2e( true) ;
         }
         else
         {
            wb_table2_5_EH2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAEH2( ) ;
         WSEH2( ) ;
         WEEH2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020621622494");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_estatisticassistemas.js", "?2020621622494");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_302( )
      {
         edtavSistema_sigla_Internalname = "vSISTEMA_SIGLA_"+sGXsfl_30_idx;
         edtavQtddmn_Internalname = "vQTDDMN_"+sGXsfl_30_idx;
         edtavQtddmnprc_Internalname = "vQTDDMNPRC_"+sGXsfl_30_idx;
         edtavPf_Internalname = "vPF_"+sGXsfl_30_idx;
         edtavPfprc_Internalname = "vPFPRC_"+sGXsfl_30_idx;
         edtavCpb_Internalname = "vCPB_"+sGXsfl_30_idx;
         edtavCpbprc_Internalname = "vCPBPRC_"+sGXsfl_30_idx;
         edtavCpm_Internalname = "vCPM_"+sGXsfl_30_idx;
         edtavCpmprc_Internalname = "vCPMPRC_"+sGXsfl_30_idx;
         edtavCpa_Internalname = "vCPA_"+sGXsfl_30_idx;
         edtavCpaprc_Internalname = "vCPAPRC_"+sGXsfl_30_idx;
         edtavQtdali_Internalname = "vQTDALI_"+sGXsfl_30_idx;
         edtavQtdaie_Internalname = "vQTDAIE_"+sGXsfl_30_idx;
         edtavQtdee_Internalname = "vQTDEE_"+sGXsfl_30_idx;
         edtavQtdce_Internalname = "vQTDCE_"+sGXsfl_30_idx;
         edtavQtdse_Internalname = "vQTDSE_"+sGXsfl_30_idx;
         edtavQtdinm_Internalname = "vQTDINM_"+sGXsfl_30_idx;
         edtavPfali_Internalname = "vPFALI_"+sGXsfl_30_idx;
         edtavPfaie_Internalname = "vPFAIE_"+sGXsfl_30_idx;
         edtavPfee_Internalname = "vPFEE_"+sGXsfl_30_idx;
         edtavPfce_Internalname = "vPFCE_"+sGXsfl_30_idx;
         edtavPfse_Internalname = "vPFSE_"+sGXsfl_30_idx;
         edtavPfinm_Internalname = "vPFINM_"+sGXsfl_30_idx;
      }

      protected void SubsflControlProps_fel_302( )
      {
         edtavSistema_sigla_Internalname = "vSISTEMA_SIGLA_"+sGXsfl_30_fel_idx;
         edtavQtddmn_Internalname = "vQTDDMN_"+sGXsfl_30_fel_idx;
         edtavQtddmnprc_Internalname = "vQTDDMNPRC_"+sGXsfl_30_fel_idx;
         edtavPf_Internalname = "vPF_"+sGXsfl_30_fel_idx;
         edtavPfprc_Internalname = "vPFPRC_"+sGXsfl_30_fel_idx;
         edtavCpb_Internalname = "vCPB_"+sGXsfl_30_fel_idx;
         edtavCpbprc_Internalname = "vCPBPRC_"+sGXsfl_30_fel_idx;
         edtavCpm_Internalname = "vCPM_"+sGXsfl_30_fel_idx;
         edtavCpmprc_Internalname = "vCPMPRC_"+sGXsfl_30_fel_idx;
         edtavCpa_Internalname = "vCPA_"+sGXsfl_30_fel_idx;
         edtavCpaprc_Internalname = "vCPAPRC_"+sGXsfl_30_fel_idx;
         edtavQtdali_Internalname = "vQTDALI_"+sGXsfl_30_fel_idx;
         edtavQtdaie_Internalname = "vQTDAIE_"+sGXsfl_30_fel_idx;
         edtavQtdee_Internalname = "vQTDEE_"+sGXsfl_30_fel_idx;
         edtavQtdce_Internalname = "vQTDCE_"+sGXsfl_30_fel_idx;
         edtavQtdse_Internalname = "vQTDSE_"+sGXsfl_30_fel_idx;
         edtavQtdinm_Internalname = "vQTDINM_"+sGXsfl_30_fel_idx;
         edtavPfali_Internalname = "vPFALI_"+sGXsfl_30_fel_idx;
         edtavPfaie_Internalname = "vPFAIE_"+sGXsfl_30_fel_idx;
         edtavPfee_Internalname = "vPFEE_"+sGXsfl_30_fel_idx;
         edtavPfce_Internalname = "vPFCE_"+sGXsfl_30_fel_idx;
         edtavPfse_Internalname = "vPFSE_"+sGXsfl_30_fel_idx;
         edtavPfinm_Internalname = "vPFINM_"+sGXsfl_30_fel_idx;
      }

      protected void sendrow_302( )
      {
         SubsflControlProps_302( ) ;
         WBEH0( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xF0F0F0);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_30_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xE5E5E5);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xF0F0F0);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_30_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavSistema_sigla_Internalname,StringUtil.RTrim( AV31Sistema_Sigla),StringUtil.RTrim( context.localUtil.Format( AV31Sistema_Sigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"Sistema",(String)"",(String)edtavSistema_sigla_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)25,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtddmn_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5QtdDmn), 12, 0, ",", "")),context.localUtil.Format( (decimal)(AV5QtdDmn), "ZZZZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"Qtd Dmn",(String)"",(String)edtavQtddmn_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)12,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtddmnprc_Internalname,StringUtil.LTrim( StringUtil.NToC( AV6QtdDmnPrc, 6, 2, ",", "")),context.localUtil.Format( AV6QtdDmnPrc, "ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"% Dmn",(String)"",(String)edtavQtddmnprc_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPf_Internalname,StringUtil.LTrim( StringUtil.NToC( AV33PF, 14, 5, ",", "")),context.localUtil.Format( AV33PF, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"PF",(String)"",(String)edtavPf_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfprc_Internalname,StringUtil.LTrim( StringUtil.NToC( AV8PFPrc, 6, 2, ",", "")),context.localUtil.Format( AV8PFPrc, "ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"% PF",(String)"",(String)edtavPfprc_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCpb_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9CpB), 12, 0, ",", "")),context.localUtil.Format( (decimal)(AV9CpB), "ZZZZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"CP B",(String)"",(String)edtavCpb_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)12,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCpbprc_Internalname,StringUtil.LTrim( StringUtil.NToC( AV12CpBPrc, 6, 2, ",", "")),context.localUtil.Format( AV12CpBPrc, "ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"% CP B",(String)"",(String)edtavCpbprc_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCpm_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10CpM), 12, 0, ",", "")),context.localUtil.Format( (decimal)(AV10CpM), "ZZZZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"CP M",(String)"",(String)edtavCpm_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)12,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCpmprc_Internalname,StringUtil.LTrim( StringUtil.NToC( AV13CpMPrc, 6, 2, ",", "")),context.localUtil.Format( AV13CpMPrc, "ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"% CP M",(String)"",(String)edtavCpmprc_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCpa_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11CpA), 12, 0, ",", "")),context.localUtil.Format( (decimal)(AV11CpA), "ZZZZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"CP A",(String)"",(String)edtavCpa_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)12,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCpaprc_Internalname,StringUtil.LTrim( StringUtil.NToC( AV14CpAPrc, 6, 2, ",", "")),context.localUtil.Format( AV14CpAPrc, "ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"% CP A",(String)"",(String)edtavCpaprc_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtdali_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15QtdALI), 12, 0, ",", "")),context.localUtil.Format( (decimal)(AV15QtdALI), "ZZZZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"Qtd ALI",(String)"",(String)edtavQtdali_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)12,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtdaie_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16QtdAIE), 12, 0, ",", "")),context.localUtil.Format( (decimal)(AV16QtdAIE), "ZZZZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"Qtd AIE",(String)"",(String)edtavQtdaie_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)12,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtdee_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17QtdEE), 12, 0, ",", "")),context.localUtil.Format( (decimal)(AV17QtdEE), "ZZZZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"Qtd EE",(String)"",(String)edtavQtdee_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)12,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtdce_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18QtdCE), 12, 0, ",", "")),context.localUtil.Format( (decimal)(AV18QtdCE), "ZZZZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"Qtd CE",(String)"",(String)edtavQtdce_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)12,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtdse_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19QtdSE), 12, 0, ",", "")),context.localUtil.Format( (decimal)(AV19QtdSE), "ZZZZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"Qtd SE",(String)"",(String)edtavQtdse_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)12,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtdinm_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20QtdINM), 12, 0, ",", "")),context.localUtil.Format( (decimal)(AV20QtdINM), "ZZZZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"Qtd INM",(String)"",(String)edtavQtdinm_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)12,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfali_Internalname,StringUtil.LTrim( StringUtil.NToC( AV21PFALI, 14, 5, ",", "")),context.localUtil.Format( AV21PFALI, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"PF AIE",(String)"",(String)edtavPfali_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfaie_Internalname,StringUtil.LTrim( StringUtil.NToC( AV22PFAIE, 14, 5, ",", "")),context.localUtil.Format( AV22PFAIE, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"PF AIE",(String)"",(String)edtavPfaie_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfee_Internalname,StringUtil.LTrim( StringUtil.NToC( AV23PFEE, 14, 5, ",", "")),context.localUtil.Format( AV23PFEE, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"PF EE",(String)"",(String)edtavPfee_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfce_Internalname,StringUtil.LTrim( StringUtil.NToC( AV24PFCE, 14, 5, ",", "")),context.localUtil.Format( AV24PFCE, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"PF CE",(String)"",(String)edtavPfce_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfse_Internalname,StringUtil.LTrim( StringUtil.NToC( AV25PFSE, 14, 5, ",", "")),context.localUtil.Format( AV25PFSE, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"PF SE",(String)"",(String)edtavPfse_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfinm_Internalname,StringUtil.LTrim( StringUtil.NToC( AV26PFINM, 14, 5, ",", "")),context.localUtil.Format( AV26PFINM, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"PF INM",(String)"",(String)edtavPfinm_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         GridContainer.AddRow(GridRow);
         nGXsfl_30_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_30_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_30_idx+1));
         sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
         SubsflControlProps_302( ) ;
         /* End function sendrow_302 */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         tblTable4_Internalname = "TABLE4";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         cmbavMes_Internalname = "vMES";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         cmbavAno_Internalname = "vANO";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavFltsistema_codigo_Internalname = "vFLTSISTEMA_CODIGO";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         dynavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         cmbavGraficar_Internalname = "vGRAFICAR";
         imgExportgraphicdmn_Internalname = "EXPORTGRAPHICDMN";
         bttButton1_Internalname = "BUTTON1";
         tblTable2_Internalname = "TABLE2";
         edtavSistema_sigla_Internalname = "vSISTEMA_SIGLA";
         edtavQtddmn_Internalname = "vQTDDMN";
         edtavQtddmnprc_Internalname = "vQTDDMNPRC";
         edtavPf_Internalname = "vPF";
         edtavPfprc_Internalname = "vPFPRC";
         edtavCpb_Internalname = "vCPB";
         edtavCpbprc_Internalname = "vCPBPRC";
         edtavCpm_Internalname = "vCPM";
         edtavCpmprc_Internalname = "vCPMPRC";
         edtavCpa_Internalname = "vCPA";
         edtavCpaprc_Internalname = "vCPAPRC";
         edtavQtdali_Internalname = "vQTDALI";
         edtavQtdaie_Internalname = "vQTDAIE";
         edtavQtdee_Internalname = "vQTDEE";
         edtavQtdce_Internalname = "vQTDCE";
         edtavQtdse_Internalname = "vQTDSE";
         edtavQtdinm_Internalname = "vQTDINM";
         edtavPfali_Internalname = "vPFALI";
         edtavPfaie_Internalname = "vPFAIE";
         edtavPfee_Internalname = "vPFEE";
         edtavPfce_Internalname = "vPFCE";
         edtavPfse_Internalname = "vPFSE";
         edtavPfinm_Internalname = "vPFINM";
         lblTextblockquantidade_Internalname = "TEXTBLOCKQUANTIDADE";
         edtavQtddmnttl_Internalname = "vQTDDMNTTL";
         lblTextblockcontagemresultado_pfbfstotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFBFSTOTAL";
         edtavPfttl_Internalname = "vPFTTL";
         lblTextblockcontagemresultado_pflfstotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFSTOTAL";
         edtavCpbttl_Internalname = "vCPBTTL";
         lblTextblockcontagemresultado_pfbfmtotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFBFMTOTAL";
         edtavCpmttl_Internalname = "vCPMTTL";
         lblTextblockcontagemresultado_pflfmtotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFMTOTAL";
         edtavCpattl_Internalname = "vCPATTL";
         lblTextblockcontagemresultado_pflfmtotal3_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFMTOTAL3";
         edtavQtdalittl_Internalname = "vQTDALITTL";
         lblTextblockcontagemresultado_pflfmtotal2_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFMTOTAL2";
         edtavQtdaiettl_Internalname = "vQTDAIETTL";
         lblTextblockcontagemresultado_pflfmtotal4_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFMTOTAL4";
         edtavQtdeettl_Internalname = "vQTDEETTL";
         lblTextblockcontagemresultado_pflfmtotal5_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFMTOTAL5";
         edtavQtdcettl_Internalname = "vQTDCETTL";
         lblTextblockcontagemresultado_pflfmtotal6_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFMTOTAL6";
         edtavQtdsettl_Internalname = "vQTDSETTL";
         lblTextblockcontagemresultado_pflfmtotal7_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFMTOTAL7";
         edtavQtdinmttl_Internalname = "vQTDINMTTL";
         tblTable3_Internalname = "TABLE3";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavPfinm_Jsonclick = "";
         edtavPfse_Jsonclick = "";
         edtavPfce_Jsonclick = "";
         edtavPfee_Jsonclick = "";
         edtavPfaie_Jsonclick = "";
         edtavPfali_Jsonclick = "";
         edtavQtdinm_Jsonclick = "";
         edtavQtdse_Jsonclick = "";
         edtavQtdce_Jsonclick = "";
         edtavQtdee_Jsonclick = "";
         edtavQtdaie_Jsonclick = "";
         edtavQtdali_Jsonclick = "";
         edtavCpaprc_Jsonclick = "";
         edtavCpa_Jsonclick = "";
         edtavCpmprc_Jsonclick = "";
         edtavCpm_Jsonclick = "";
         edtavCpbprc_Jsonclick = "";
         edtavCpb_Jsonclick = "";
         edtavPfprc_Jsonclick = "";
         edtavPf_Jsonclick = "";
         edtavQtddmnprc_Jsonclick = "";
         edtavQtddmn_Jsonclick = "";
         edtavSistema_sigla_Jsonclick = "";
         cmbavGraficar_Jsonclick = "";
         dynavContratada_codigo_Jsonclick = "";
         edtavFltsistema_codigo_Jsonclick = "";
         cmbavAno_Jsonclick = "";
         cmbavMes_Jsonclick = "";
         edtavQtdinmttl_Jsonclick = "";
         edtavQtdinmttl_Enabled = 1;
         edtavQtdsettl_Jsonclick = "";
         edtavQtdsettl_Enabled = 1;
         edtavQtdcettl_Jsonclick = "";
         edtavQtdcettl_Enabled = 1;
         edtavQtdeettl_Jsonclick = "";
         edtavQtdeettl_Enabled = 1;
         edtavQtdaiettl_Jsonclick = "";
         edtavQtdaiettl_Enabled = 1;
         edtavQtdalittl_Jsonclick = "";
         edtavQtdalittl_Enabled = 1;
         edtavCpattl_Jsonclick = "";
         edtavCpattl_Enabled = 1;
         edtavCpmttl_Jsonclick = "";
         edtavCpmttl_Enabled = 1;
         edtavCpbttl_Jsonclick = "";
         edtavCpbttl_Enabled = 1;
         edtavPfttl_Jsonclick = "";
         edtavPfttl_Enabled = 1;
         edtavQtddmnttl_Jsonclick = "";
         edtavQtddmnttl_Enabled = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         subGrid_Class = "Grid";
         subGrid_Titleforecolor = (int)(0x000000);
         subGrid_Backcolorstyle = 3;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Estatisticas";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'A780ContagemResultadoItem_PFB',fld:'CONTAGEMRESULTADOITEM_PFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66DmnTtlD',fld:'vDMNTTLD',pic:'ZZZZZZZZZZZ9',nv:0},{av:'AV40mDmn',fld:'vMDMN',pic:'ZZZ9',nv:0},{av:'AV65PFTtlD',fld:'vPFTTLD',pic:'ZZZZZZZZZZZ9',nv:0},{av:'AV41mPF',fld:'vMPF',pic:'ZZZ9',nv:0},{av:'AV59CpBttlD',fld:'vCPBTTLD',pic:'ZZZZZZZZZZZ9',nv:0},{av:'AV42mCpB',fld:'vMCPB',pic:'ZZZ9',nv:0},{av:'AV60CpMttlD',fld:'vCPMTTLD',pic:'ZZZZZZZZZZZ9',nv:0},{av:'AV43mCpM',fld:'vMCPM',pic:'ZZZ9',nv:0},{av:'AV61CpAttlD',fld:'vCPATTLD',pic:'ZZZZZZZZZZZ9',nv:0},{av:'AV44mCpA',fld:'vMCPA',pic:'ZZZ9',nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A509ContagemrResultado_SistemaSigla',fld:'CONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV30WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV68Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV32FltSistema_Codigo',fld:'vFLTSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV28Mes',fld:'vMES',pic:'ZZZZZZZZZ9',nv:0},{av:'AV27Ano',fld:'vANO',pic:'ZZZ9',nv:0},{av:'A574ContagemResultado_PFFinal',fld:'CONTAGEMRESULTADO_PFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A773ContagemResultadoItem_ContagemCod',fld:'CONTAGEMRESULTADOITEM_CONTAGEMCOD',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A775ContagemResultadoItem_Tipo',fld:'CONTAGEMRESULTADOITEM_TIPO',pic:'',nv:''},{av:'A779ContagemResultadoItem_CP',fld:'CONTAGEMRESULTADOITEM_CP',pic:'',nv:''}],oparms:[{av:'AV29QtdDmnTtl',fld:'vQTDDMNTTL',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV7PFTtl',fld:'vPFTTL',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV34QtdALITtl',fld:'vQTDALITTL',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV35QtdAIETtl',fld:'vQTDAIETTL',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV36QtdCETtl',fld:'vQTDCETTL',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV37QtdEETtl',fld:'vQTDEETTL',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV38QtdSETtl',fld:'vQTDSETTL',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV39QtdINMTtl',fld:'vQTDINMTTL',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV47CpBttl',fld:'vCPBTTL',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV48CpMttl',fld:'vCPMTTL',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV49CpAttl',fld:'vCPATTL',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV40mDmn',fld:'vMDMN',pic:'ZZZ9',nv:0},{av:'AV41mPF',fld:'vMPF',pic:'ZZZ9',nv:0},{av:'AV42mCpB',fld:'vMCPB',pic:'ZZZ9',nv:0},{av:'AV43mCpM',fld:'vMCPM',pic:'ZZZ9',nv:0},{av:'AV44mCpA',fld:'vMCPA',pic:'ZZZ9',nv:0},{av:'AV66DmnTtlD',fld:'vDMNTTLD',pic:'ZZZZZZZZZZZ9',nv:0},{av:'AV65PFTtlD',fld:'vPFTTLD',pic:'ZZZZZZZZZZZ9',nv:0},{av:'AV59CpBttlD',fld:'vCPBTTLD',pic:'ZZZZZZZZZZZ9',nv:0},{av:'AV60CpMttlD',fld:'vCPMTTLD',pic:'ZZZZZZZZZZZ9',nv:0},{av:'AV61CpAttlD',fld:'vCPATTLD',pic:'ZZZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("'DOGRAFICOS'","{handler:'E14EH1',iparms:[{av:'AV30WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV68Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32FltSistema_Codigo',fld:'vFLTSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV27Ano',fld:'vANO',pic:'ZZZ9',nv:0},{av:'AV28Mes',fld:'vMES',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67Graficar',fld:'vGRAFICAR',pic:'',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV30WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         A509ContagemrResultado_SistemaSigla = "";
         A484ContagemResultado_StatusDmn = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A775ContagemResultadoItem_Tipo = "";
         A779ContagemResultadoItem_CP = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Sistema_Sigla = "";
         AV67Graficar = "DXS";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00EH2_A40Contratada_PessoaCod = new int[1] ;
         H00EH2_A39Contratada_Codigo = new int[1] ;
         H00EH2_A41Contratada_PessoaNom = new String[] {""} ;
         H00EH2_n41Contratada_PessoaNom = new bool[] {false} ;
         H00EH2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00EH2_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         Gx_date = DateTime.MinValue;
         H00EH3_A516Contratada_TipoFabrica = new String[] {""} ;
         H00EH3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00EH3_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00EH3_A39Contratada_Codigo = new int[1] ;
         A516Contratada_TipoFabrica = "";
         H00EH4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00EH4_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00EH4_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00EH4_A489ContagemResultado_SistemaCod = new int[1] ;
         H00EH4_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00EH4_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00EH4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00EH4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00EH4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00EH4_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00EH4_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00EH4_A456ContagemResultado_Codigo = new int[1] ;
         H00EH5_A772ContagemResultadoItem_Codigo = new int[1] ;
         H00EH5_A773ContagemResultadoItem_ContagemCod = new int[1] ;
         H00EH5_n773ContagemResultadoItem_ContagemCod = new bool[] {false} ;
         H00EH5_A775ContagemResultadoItem_Tipo = new String[] {""} ;
         H00EH5_n775ContagemResultadoItem_Tipo = new bool[] {false} ;
         H00EH5_A779ContagemResultadoItem_CP = new String[] {""} ;
         H00EH5_n779ContagemResultadoItem_CP = new bool[] {false} ;
         H00EH6_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00EH6_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00EH6_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00EH6_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00EH6_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00EH6_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00EH6_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00EH6_A489ContagemResultado_SistemaCod = new int[1] ;
         H00EH6_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00EH6_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00EH6_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00EH6_A456ContagemResultado_Codigo = new int[1] ;
         H00EH7_A772ContagemResultadoItem_Codigo = new int[1] ;
         H00EH7_A773ContagemResultadoItem_ContagemCod = new int[1] ;
         H00EH7_n773ContagemResultadoItem_ContagemCod = new bool[] {false} ;
         H00EH7_A775ContagemResultadoItem_Tipo = new String[] {""} ;
         H00EH7_n775ContagemResultadoItem_Tipo = new bool[] {false} ;
         H00EH7_A780ContagemResultadoItem_PFB = new decimal[1] ;
         H00EH7_n780ContagemResultadoItem_PFB = new bool[] {false} ;
         H00EH7_A779ContagemResultadoItem_CP = new String[] {""} ;
         H00EH7_n779ContagemResultadoItem_CP = new bool[] {false} ;
         GridRow = new GXWebRow();
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblTextblockquantidade_Jsonclick = "";
         TempTags = "";
         lblTextblockcontagemresultado_pfbfstotal_Jsonclick = "";
         lblTextblockcontagemresultado_pflfstotal_Jsonclick = "";
         lblTextblockcontagemresultado_pfbfmtotal_Jsonclick = "";
         lblTextblockcontagemresultado_pflfmtotal_Jsonclick = "";
         lblTextblockcontagemresultado_pflfmtotal3_Jsonclick = "";
         lblTextblockcontagemresultado_pflfmtotal2_Jsonclick = "";
         lblTextblockcontagemresultado_pflfmtotal4_Jsonclick = "";
         lblTextblockcontagemresultado_pflfmtotal5_Jsonclick = "";
         lblTextblockcontagemresultado_pflfmtotal6_Jsonclick = "";
         lblTextblockcontagemresultado_pflfmtotal7_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         imgExportgraphicdmn_Jsonclick = "";
         bttButton1_Jsonclick = "";
         lblTitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_estatisticassistemas__default(),
            new Object[][] {
                new Object[] {
               H00EH2_A40Contratada_PessoaCod, H00EH2_A39Contratada_Codigo, H00EH2_A41Contratada_PessoaNom, H00EH2_n41Contratada_PessoaNom, H00EH2_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00EH3_A516Contratada_TipoFabrica, H00EH3_A52Contratada_AreaTrabalhoCod, H00EH3_A39Contratada_Codigo
               }
               , new Object[] {
               H00EH4_A52Contratada_AreaTrabalhoCod, H00EH4_n52Contratada_AreaTrabalhoCod, H00EH4_A471ContagemResultado_DataDmn, H00EH4_A489ContagemResultado_SistemaCod, H00EH4_n489ContagemResultado_SistemaCod, H00EH4_A490ContagemResultado_ContratadaCod, H00EH4_n490ContagemResultado_ContratadaCod, H00EH4_A484ContagemResultado_StatusDmn, H00EH4_n484ContagemResultado_StatusDmn, H00EH4_A509ContagemrResultado_SistemaSigla,
               H00EH4_n509ContagemrResultado_SistemaSigla, H00EH4_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00EH5_A772ContagemResultadoItem_Codigo, H00EH5_A773ContagemResultadoItem_ContagemCod, H00EH5_n773ContagemResultadoItem_ContagemCod, H00EH5_A775ContagemResultadoItem_Tipo, H00EH5_n775ContagemResultadoItem_Tipo, H00EH5_A779ContagemResultadoItem_CP, H00EH5_n779ContagemResultadoItem_CP
               }
               , new Object[] {
               H00EH6_A52Contratada_AreaTrabalhoCod, H00EH6_n52Contratada_AreaTrabalhoCod, H00EH6_A484ContagemResultado_StatusDmn, H00EH6_n484ContagemResultado_StatusDmn, H00EH6_A509ContagemrResultado_SistemaSigla, H00EH6_n509ContagemrResultado_SistemaSigla, H00EH6_A471ContagemResultado_DataDmn, H00EH6_A489ContagemResultado_SistemaCod, H00EH6_n489ContagemResultado_SistemaCod, H00EH6_A490ContagemResultado_ContratadaCod,
               H00EH6_n490ContagemResultado_ContratadaCod, H00EH6_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00EH7_A772ContagemResultadoItem_Codigo, H00EH7_A773ContagemResultadoItem_ContagemCod, H00EH7_n773ContagemResultadoItem_ContagemCod, H00EH7_A775ContagemResultadoItem_Tipo, H00EH7_n775ContagemResultadoItem_Tipo, H00EH7_A780ContagemResultadoItem_PFB, H00EH7_n780ContagemResultadoItem_PFB, H00EH7_A779ContagemResultadoItem_CP, H00EH7_n779ContagemResultadoItem_CP
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavQtddmnttl_Enabled = 0;
         edtavPfttl_Enabled = 0;
         edtavCpbttl_Enabled = 0;
         edtavCpmttl_Enabled = 0;
         edtavCpattl_Enabled = 0;
         edtavQtdalittl_Enabled = 0;
         edtavQtdaiettl_Enabled = 0;
         edtavQtdeettl_Enabled = 0;
         edtavQtdcettl_Enabled = 0;
         edtavQtdsettl_Enabled = 0;
         edtavQtdinmttl_Enabled = 0;
      }

      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_8 ;
      private short nIsMod_8 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_30 ;
      private short nGXsfl_30_idx=1 ;
      private short AV27Ano ;
      private short AV40mDmn ;
      private short AV41mPF ;
      private short AV42mCpB ;
      private short AV43mCpM ;
      private short AV44mCpA ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_30_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short AV57i ;
      private short GRID_nEOF ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int AV68Contratada_Codigo ;
      private int A489ContagemResultado_SistemaCod ;
      private int AV32FltSistema_Codigo ;
      private int A773ContagemResultadoItem_ContagemCod ;
      private int A456ContagemResultado_Codigo ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int edtavQtddmnttl_Enabled ;
      private int edtavPfttl_Enabled ;
      private int edtavCpbttl_Enabled ;
      private int edtavCpmttl_Enabled ;
      private int edtavCpattl_Enabled ;
      private int edtavQtdalittl_Enabled ;
      private int edtavQtdaiettl_Enabled ;
      private int edtavQtdeettl_Enabled ;
      private int edtavQtdcettl_Enabled ;
      private int edtavQtdsettl_Enabled ;
      private int edtavQtdinmttl_Enabled ;
      private int subGrid_Titleforecolor ;
      private int A39Contratada_Codigo ;
      private int AV30WWPContext_gxTpr_Areatrabalho_codigo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long AV28Mes ;
      private long AV66DmnTtlD ;
      private long AV65PFTtlD ;
      private long AV59CpBttlD ;
      private long AV60CpMttlD ;
      private long AV61CpAttlD ;
      private long AV29QtdDmnTtl ;
      private long AV7PFTtl ;
      private long AV47CpBttl ;
      private long AV48CpMttl ;
      private long AV49CpAttl ;
      private long AV34QtdALITtl ;
      private long AV35QtdAIETtl ;
      private long AV37QtdEETtl ;
      private long AV36QtdCETtl ;
      private long AV38QtdSETtl ;
      private long AV39QtdINMTtl ;
      private long AV5QtdDmn ;
      private long AV9CpB ;
      private long AV10CpM ;
      private long AV11CpA ;
      private long AV15QtdALI ;
      private long AV16QtdAIE ;
      private long AV17QtdEE ;
      private long AV18QtdCE ;
      private long AV19QtdSE ;
      private long AV20QtdINM ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal A780ContagemResultadoItem_PFB ;
      private decimal AV6QtdDmnPrc ;
      private decimal AV33PF ;
      private decimal AV8PFPrc ;
      private decimal AV12CpBPrc ;
      private decimal AV13CpMPrc ;
      private decimal AV14CpAPrc ;
      private decimal AV21PFALI ;
      private decimal AV22PFAIE ;
      private decimal AV23PFEE ;
      private decimal AV24PFCE ;
      private decimal AV25PFSE ;
      private decimal AV26PFINM ;
      private decimal GXt_decimal1 ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_30_idx="0001" ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String A779ContagemResultadoItem_CP ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV31Sistema_Sigla ;
      private String edtavSistema_sigla_Internalname ;
      private String edtavQtddmn_Internalname ;
      private String edtavQtddmnprc_Internalname ;
      private String edtavPf_Internalname ;
      private String edtavPfprc_Internalname ;
      private String edtavCpb_Internalname ;
      private String edtavCpbprc_Internalname ;
      private String edtavCpm_Internalname ;
      private String edtavCpmprc_Internalname ;
      private String edtavCpa_Internalname ;
      private String edtavCpaprc_Internalname ;
      private String edtavQtdali_Internalname ;
      private String edtavQtdaie_Internalname ;
      private String edtavQtdee_Internalname ;
      private String edtavQtdce_Internalname ;
      private String edtavQtdse_Internalname ;
      private String edtavQtdinm_Internalname ;
      private String edtavPfali_Internalname ;
      private String edtavPfaie_Internalname ;
      private String edtavPfee_Internalname ;
      private String edtavPfce_Internalname ;
      private String edtavPfse_Internalname ;
      private String edtavPfinm_Internalname ;
      private String AV67Graficar ;
      private String cmbavMes_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavQtddmnttl_Internalname ;
      private String edtavPfttl_Internalname ;
      private String edtavCpbttl_Internalname ;
      private String edtavCpmttl_Internalname ;
      private String edtavCpattl_Internalname ;
      private String edtavQtdalittl_Internalname ;
      private String edtavQtdaiettl_Internalname ;
      private String edtavQtdeettl_Internalname ;
      private String edtavQtdcettl_Internalname ;
      private String edtavQtdsettl_Internalname ;
      private String edtavQtdinmttl_Internalname ;
      private String cmbavAno_Internalname ;
      private String edtavFltsistema_codigo_Internalname ;
      private String dynavContratada_codigo_Internalname ;
      private String cmbavGraficar_Internalname ;
      private String A516Contratada_TipoFabrica ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTable3_Internalname ;
      private String lblTextblockquantidade_Internalname ;
      private String lblTextblockquantidade_Jsonclick ;
      private String TempTags ;
      private String edtavQtddmnttl_Jsonclick ;
      private String lblTextblockcontagemresultado_pfbfstotal_Internalname ;
      private String lblTextblockcontagemresultado_pfbfstotal_Jsonclick ;
      private String edtavPfttl_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfstotal_Internalname ;
      private String lblTextblockcontagemresultado_pflfstotal_Jsonclick ;
      private String edtavCpbttl_Jsonclick ;
      private String lblTextblockcontagemresultado_pfbfmtotal_Internalname ;
      private String lblTextblockcontagemresultado_pfbfmtotal_Jsonclick ;
      private String edtavCpmttl_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfmtotal_Internalname ;
      private String lblTextblockcontagemresultado_pflfmtotal_Jsonclick ;
      private String edtavCpattl_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfmtotal3_Internalname ;
      private String lblTextblockcontagemresultado_pflfmtotal3_Jsonclick ;
      private String edtavQtdalittl_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfmtotal2_Internalname ;
      private String lblTextblockcontagemresultado_pflfmtotal2_Jsonclick ;
      private String edtavQtdaiettl_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfmtotal4_Internalname ;
      private String lblTextblockcontagemresultado_pflfmtotal4_Jsonclick ;
      private String edtavQtdeettl_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfmtotal5_Internalname ;
      private String lblTextblockcontagemresultado_pflfmtotal5_Jsonclick ;
      private String edtavQtdcettl_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfmtotal6_Internalname ;
      private String lblTextblockcontagemresultado_pflfmtotal6_Jsonclick ;
      private String edtavQtdsettl_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfmtotal7_Internalname ;
      private String lblTextblockcontagemresultado_pflfmtotal7_Jsonclick ;
      private String edtavQtdinmttl_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String cmbavMes_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String cmbavAno_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavFltsistema_codigo_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String dynavContratada_codigo_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String cmbavGraficar_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String imgExportgraphicdmn_Internalname ;
      private String imgExportgraphicdmn_Jsonclick ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String tblTable4_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String sGXsfl_30_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavSistema_sigla_Jsonclick ;
      private String edtavQtddmn_Jsonclick ;
      private String edtavQtddmnprc_Jsonclick ;
      private String edtavPf_Jsonclick ;
      private String edtavPfprc_Jsonclick ;
      private String edtavCpb_Jsonclick ;
      private String edtavCpbprc_Jsonclick ;
      private String edtavCpm_Jsonclick ;
      private String edtavCpmprc_Jsonclick ;
      private String edtavCpa_Jsonclick ;
      private String edtavCpaprc_Jsonclick ;
      private String edtavQtdali_Jsonclick ;
      private String edtavQtdaie_Jsonclick ;
      private String edtavQtdee_Jsonclick ;
      private String edtavQtdce_Jsonclick ;
      private String edtavQtdse_Jsonclick ;
      private String edtavQtdinm_Jsonclick ;
      private String edtavPfali_Jsonclick ;
      private String edtavPfaie_Jsonclick ;
      private String edtavPfee_Jsonclick ;
      private String edtavPfce_Jsonclick ;
      private String edtavPfse_Jsonclick ;
      private String edtavPfinm_Jsonclick ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n773ContagemResultadoItem_ContagemCod ;
      private bool n775ContagemResultadoItem_Tipo ;
      private bool n779ContagemResultadoItem_CP ;
      private bool n780ContagemResultadoItem_PFB ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool BRKEH6 ;
      private String A775ContagemResultadoItem_Tipo ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavMes ;
      private GXCombobox cmbavAno ;
      private GXCombobox dynavContratada_codigo ;
      private GXCombobox cmbavGraficar ;
      private IDataStoreProvider pr_default ;
      private int[] H00EH2_A40Contratada_PessoaCod ;
      private int[] H00EH2_A39Contratada_Codigo ;
      private String[] H00EH2_A41Contratada_PessoaNom ;
      private bool[] H00EH2_n41Contratada_PessoaNom ;
      private int[] H00EH2_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00EH2_n52Contratada_AreaTrabalhoCod ;
      private String[] H00EH3_A516Contratada_TipoFabrica ;
      private int[] H00EH3_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00EH3_n52Contratada_AreaTrabalhoCod ;
      private int[] H00EH3_A39Contratada_Codigo ;
      private int[] H00EH4_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00EH4_n52Contratada_AreaTrabalhoCod ;
      private DateTime[] H00EH4_A471ContagemResultado_DataDmn ;
      private int[] H00EH4_A489ContagemResultado_SistemaCod ;
      private bool[] H00EH4_n489ContagemResultado_SistemaCod ;
      private int[] H00EH4_A490ContagemResultado_ContratadaCod ;
      private bool[] H00EH4_n490ContagemResultado_ContratadaCod ;
      private String[] H00EH4_A484ContagemResultado_StatusDmn ;
      private bool[] H00EH4_n484ContagemResultado_StatusDmn ;
      private String[] H00EH4_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00EH4_n509ContagemrResultado_SistemaSigla ;
      private int[] H00EH4_A456ContagemResultado_Codigo ;
      private int[] H00EH5_A772ContagemResultadoItem_Codigo ;
      private int[] H00EH5_A773ContagemResultadoItem_ContagemCod ;
      private bool[] H00EH5_n773ContagemResultadoItem_ContagemCod ;
      private String[] H00EH5_A775ContagemResultadoItem_Tipo ;
      private bool[] H00EH5_n775ContagemResultadoItem_Tipo ;
      private String[] H00EH5_A779ContagemResultadoItem_CP ;
      private bool[] H00EH5_n779ContagemResultadoItem_CP ;
      private int[] H00EH6_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00EH6_n52Contratada_AreaTrabalhoCod ;
      private String[] H00EH6_A484ContagemResultado_StatusDmn ;
      private bool[] H00EH6_n484ContagemResultado_StatusDmn ;
      private String[] H00EH6_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00EH6_n509ContagemrResultado_SistemaSigla ;
      private DateTime[] H00EH6_A471ContagemResultado_DataDmn ;
      private int[] H00EH6_A489ContagemResultado_SistemaCod ;
      private bool[] H00EH6_n489ContagemResultado_SistemaCod ;
      private int[] H00EH6_A490ContagemResultado_ContratadaCod ;
      private bool[] H00EH6_n490ContagemResultado_ContratadaCod ;
      private int[] H00EH6_A456ContagemResultado_Codigo ;
      private int[] H00EH7_A772ContagemResultadoItem_Codigo ;
      private int[] H00EH7_A773ContagemResultadoItem_ContagemCod ;
      private bool[] H00EH7_n773ContagemResultadoItem_ContagemCod ;
      private String[] H00EH7_A775ContagemResultadoItem_Tipo ;
      private bool[] H00EH7_n775ContagemResultadoItem_Tipo ;
      private decimal[] H00EH7_A780ContagemResultadoItem_PFB ;
      private bool[] H00EH7_n780ContagemResultadoItem_PFB ;
      private String[] H00EH7_A779ContagemResultadoItem_CP ;
      private bool[] H00EH7_n779ContagemResultadoItem_CP ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV30WWPContext ;
   }

   public class wp_estatisticassistemas__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00EH4( IGxContext context ,
                                             int AV68Contratada_Codigo ,
                                             int AV32FltSistema_Codigo ,
                                             long AV28Mes ,
                                             short AV27Ano ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int A489ContagemResultado_SistemaCod ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             String A484ContagemResultado_StatusDmn ,
                                             int AV30WWPContext_gxTpr_Areatrabalho_codigo ,
                                             int A52Contratada_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [5] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T3.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_StatusDmn], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Codigo] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T3.[Contratada_AreaTrabalhoCod] = @AV30WWPC_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (Not T1.[ContagemResultado_StatusDmn] = 'X' or T1.[ContagemResultado_StatusDmn] IS NULL)";
         if ( ! (0==AV68Contratada_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV68Contratada_Codigo)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (0==AV32FltSistema_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV32FltSistema_Codigo)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (0==AV28Mes) )
         {
            sWhereString = sWhereString + " and (MONTH(T1.[ContagemResultado_DataDmn]) = @AV28Mes)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (0==AV27Ano) )
         {
            sWhereString = sWhereString + " and (YEAR(T1.[ContagemResultado_DataDmn]) = @AV27Ano)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Contratada_AreaTrabalhoCod], T2.[Sistema_Sigla], T1.[ContagemResultado_StatusDmn]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00EH6( IGxContext context ,
                                             int AV68Contratada_Codigo ,
                                             int AV32FltSistema_Codigo ,
                                             long AV28Mes ,
                                             short AV27Ano ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int A489ContagemResultado_SistemaCod ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             int AV30WWPContext_gxTpr_Areatrabalho_codigo ,
                                             int A52Contratada_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [5] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T3.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_StatusDmn], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T3.[Contratada_AreaTrabalhoCod] = @AV30WWPC_1Areatrabalho_codigo)";
         if ( ! (0==AV68Contratada_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV68Contratada_Codigo)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (0==AV32FltSistema_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV32FltSistema_Codigo)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (0==AV28Mes) )
         {
            sWhereString = sWhereString + " and (MONTH(T1.[ContagemResultado_DataDmn]) = @AV28Mes)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (0==AV27Ano) )
         {
            sWhereString = sWhereString + " and (YEAR(T1.[ContagemResultado_DataDmn]) = @AV27Ano)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Contratada_AreaTrabalhoCod], T2.[Sistema_Sigla], T1.[ContagemResultado_StatusDmn]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00EH4(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (long)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] );
               case 4 :
                     return conditional_H00EH6(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (long)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (DateTime)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00EH2 ;
          prmH00EH2 = new Object[] {
          new Object[] {"@AV30WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EH3 ;
          prmH00EH3 = new Object[] {
          new Object[] {"@AV30WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EH5 ;
          prmH00EH5 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EH7 ;
          prmH00EH7 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EH4 ;
          prmH00EH4 = new Object[] {
          new Object[] {"@AV30WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32FltSistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV27Ano",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00EH6 ;
          prmH00EH6 = new Object[] {
          new Object[] {"@AV30WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32FltSistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV27Ano",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00EH2", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV30WWPC_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EH2,0,0,true,false )
             ,new CursorDef("H00EH3", "SELECT TOP 1 [Contratada_TipoFabrica], [Contratada_AreaTrabalhoCod], [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE ([Contratada_AreaTrabalhoCod] = @AV30WWPC_1Areatrabalho_codigo) AND ([Contratada_TipoFabrica] = 'M') ORDER BY [Contratada_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EH3,1,0,false,true )
             ,new CursorDef("H00EH4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EH4,100,0,true,false )
             ,new CursorDef("H00EH5", "SELECT [ContagemResultadoItem_Codigo], [ContagemResultadoItem_ContagemCod], [ContagemResultadoItem_Tipo], [ContagemResultadoItem_CP] FROM [ContagemResultadoItem] WITH (NOLOCK) WHERE [ContagemResultadoItem_ContagemCod] = @ContagemResultado_Codigo ORDER BY [ContagemResultadoItem_ContagemCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EH5,100,0,false,false )
             ,new CursorDef("H00EH6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EH6,100,0,true,false )
             ,new CursorDef("H00EH7", "SELECT [ContagemResultadoItem_Codigo], [ContagemResultadoItem_ContagemCod], [ContagemResultadoItem_Tipo], [ContagemResultadoItem_PFB], [ContagemResultadoItem_CP] FROM [ContagemResultadoItem] WITH (NOLOCK) WHERE [ContagemResultadoItem_ContagemCod] = @ContagemResultado_Codigo ORDER BY [ContagemResultadoItem_ContagemCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EH7,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 25) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 25) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[9]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[9]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
