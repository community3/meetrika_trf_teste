/*
               File: type_SdtSDT_AuntenticacaoOUT_Usuario_Perfil
        Description: SDT_AuntenticacaoOUT
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:59.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_AuntenticacaoOUT.Usuario.Perfil" )]
   [XmlType(TypeName =  "SDT_AuntenticacaoOUT.Usuario.Perfil" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSDT_AuntenticacaoOUT_Usuario_Perfil : GxUserType
   {
      public SdtSDT_AuntenticacaoOUT_Usuario_Perfil( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_nome = "";
      }

      public SdtSDT_AuntenticacaoOUT_Usuario_Perfil( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_AuntenticacaoOUT_Usuario_Perfil deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_AuntenticacaoOUT_Usuario_Perfil)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_AuntenticacaoOUT_Usuario_Perfil obj ;
         obj = this;
         obj.gxTpr_Perfil_gamid = deserialized.gxTpr_Perfil_gamid;
         obj.gxTpr_Perfil_tipo = deserialized.gxTpr_Perfil_tipo;
         obj.gxTpr_Perfil_nome = deserialized.gxTpr_Perfil_nome;
         obj.gxTpr_Perfil_areatrabalhocod = deserialized.gxTpr_Perfil_areatrabalhocod;
         obj.gxTpr_Perfil_codigo = deserialized.gxTpr_Perfil_codigo;
         obj.gxTpr_Perfil_ativo = deserialized.gxTpr_Perfil_ativo;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_GamId") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_gamid = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Tipo") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_tipo = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Nome") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_AreaTrabalhoCod") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Codigo") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Ativo") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_AuntenticacaoOUT.Usuario.Perfil";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Perfil_GamId", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_gamid), 12, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_Tipo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_tipo), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_Nome", StringUtil.RTrim( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Perfil_GamId", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_gamid, false);
         AddObjectProperty("Perfil_Tipo", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_tipo, false);
         AddObjectProperty("Perfil_Nome", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_nome, false);
         AddObjectProperty("Perfil_AreaTrabalhoCod", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_areatrabalhocod, false);
         AddObjectProperty("Perfil_Codigo", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_codigo, false);
         AddObjectProperty("Perfil_Ativo", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_ativo, false);
         return  ;
      }

      [  SoapElement( ElementName = "Perfil_GamId" )]
      [  XmlElement( ElementName = "Perfil_GamId"   )]
      public long gxTpr_Perfil_gamid
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_gamid ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_gamid = (long)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_Tipo" )]
      [  XmlElement( ElementName = "Perfil_Tipo"   )]
      public short gxTpr_Perfil_tipo
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_tipo ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_tipo = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_Nome" )]
      [  XmlElement( ElementName = "Perfil_Nome"   )]
      public String gxTpr_Perfil_nome
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_nome ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "Perfil_AreaTrabalhoCod"   )]
      public int gxTpr_Perfil_areatrabalhocod
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_areatrabalhocod ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_areatrabalhocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_Codigo" )]
      [  XmlElement( ElementName = "Perfil_Codigo"   )]
      public int gxTpr_Perfil_codigo
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_codigo ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_Ativo" )]
      [  XmlElement( ElementName = "Perfil_Ativo"   )]
      public bool gxTpr_Perfil_ativo
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_ativo ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_ativo = value;
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_nome = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_ativo = true;
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_tipo ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_areatrabalhocod ;
      protected int gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_codigo ;
      protected long gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_gamid ;
      protected String gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_nome ;
      protected String sTagName ;
      protected bool gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_Perfil_ativo ;
   }

   [DataContract(Name = @"SDT_AuntenticacaoOUT.Usuario.Perfil", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_AuntenticacaoOUT_Usuario_Perfil_RESTInterface : GxGenericCollectionItem<SdtSDT_AuntenticacaoOUT_Usuario_Perfil>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_AuntenticacaoOUT_Usuario_Perfil_RESTInterface( ) : base()
      {
      }

      public SdtSDT_AuntenticacaoOUT_Usuario_Perfil_RESTInterface( SdtSDT_AuntenticacaoOUT_Usuario_Perfil psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Perfil_GamId" , Order = 0 )]
      public String gxTpr_Perfil_gamid
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Perfil_gamid), 12, 0)) ;
         }

         set {
            sdt.gxTpr_Perfil_gamid = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Perfil_Tipo" , Order = 1 )]
      public Nullable<short> gxTpr_Perfil_tipo
      {
         get {
            return sdt.gxTpr_Perfil_tipo ;
         }

         set {
            sdt.gxTpr_Perfil_tipo = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Perfil_Nome" , Order = 2 )]
      public String gxTpr_Perfil_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Perfil_nome) ;
         }

         set {
            sdt.gxTpr_Perfil_nome = (String)(value);
         }

      }

      [DataMember( Name = "Perfil_AreaTrabalhoCod" , Order = 3 )]
      public Nullable<int> gxTpr_Perfil_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Perfil_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Perfil_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Perfil_Codigo" , Order = 4 )]
      public Nullable<int> gxTpr_Perfil_codigo
      {
         get {
            return sdt.gxTpr_Perfil_codigo ;
         }

         set {
            sdt.gxTpr_Perfil_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Perfil_Ativo" , Order = 5 )]
      public bool gxTpr_Perfil_ativo
      {
         get {
            return sdt.gxTpr_Perfil_ativo ;
         }

         set {
            sdt.gxTpr_Perfil_ativo = value;
         }

      }

      public SdtSDT_AuntenticacaoOUT_Usuario_Perfil sdt
      {
         get {
            return (SdtSDT_AuntenticacaoOUT_Usuario_Perfil)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_AuntenticacaoOUT_Usuario_Perfil() ;
         }
      }

   }

}
