/*
               File: WP_AlterarServico
        Description: Alterar Servi�o da OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:24:16.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_alterarservico : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_alterarservico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_alterarservico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           String aP1_OS ,
                           ref String aP2_ServicoSigla ,
                           ref String aP3_Nome )
      {
         this.AV8Codigo = aP0_Codigo;
         this.AV35OS = aP1_OS;
         this.AV43ServicoSigla = aP2_ServicoSigla;
         this.AV23Nome = aP3_Nome;
         executePrivate();
         aP2_ServicoSigla=this.AV43ServicoSigla;
         aP3_Nome=this.AV23Nome;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavNovoservico = new GXCombobox();
         cmbavContratoservicos_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV8Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV35OS = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35OS", AV35OS);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vOS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV35OS, "@!"))));
                  AV43ServicoSigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ServicoSigla", AV43ServicoSigla);
                  AV23Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Nome", AV23Nome);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAKA2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTKA2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216241658");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_alterarservico.aspx") + "?" + UrlEncode("" +AV8Codigo) + "," + UrlEncode(StringUtil.RTrim(AV35OS)) + "," + UrlEncode(StringUtil.RTrim(AV43ServicoSigla)) + "," + UrlEncode(StringUtil.RTrim(AV23Nome))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_NUMERO", StringUtil.RTrim( A77Contrato_Numero));
         GxWebStd.gx_hidden_field( context, "vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV34WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV34WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vNOME", StringUtil.RTrim( AV23Nome));
         GxWebStd.gx_hidden_field( context, "vOS", AV35OS);
         GxWebStd.gx_hidden_field( context, "gxhash_vOS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV35OS, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vOS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV35OS, "@!"))));
         GxWebStd.gx_hidden_field( context, "vNOVOSERVICO_Text", StringUtil.RTrim( cmbavNovoservico.Description));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Wcanexos == null ) )
         {
            WebComp_Wcanexos.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEKA2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTKA2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_alterarservico.aspx") + "?" + UrlEncode("" +AV8Codigo) + "," + UrlEncode(StringUtil.RTrim(AV35OS)) + "," + UrlEncode(StringUtil.RTrim(AV43ServicoSigla)) + "," + UrlEncode(StringUtil.RTrim(AV23Nome)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_AlterarServico" ;
      }

      public override String GetPgmdesc( )
      {
         return "Alterar Servi�o da OS" ;
      }

      protected void WBKA0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_KA2( true) ;
         }
         else
         {
            wb_table1_2_KA2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_KA2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44Contratada_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44Contratada_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavContratada_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_AlterarServico.htm");
         }
         wbLoad = true;
      }

      protected void STARTKA2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Alterar Servi�o da OS", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPKA0( ) ;
      }

      protected void WSKA2( )
      {
         STARTKA2( ) ;
         EVTKA2( ) ;
      }

      protected void EVTKA2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11KA2 */
                              E11KA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VNOVOSERVICO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12KA2 */
                              E12KA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E13KA2 */
                                    E13KA2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14KA2 */
                              E14KA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15KA2 */
                              E15KA2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 33 )
                        {
                           WebComp_Wcanexos = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
                           WebComp_Wcanexos.ComponentInit();
                           WebComp_Wcanexos.Name = "WC_ContagemResultadoEvidencias";
                           WebComp_Wcanexos_Component = "WC_ContagemResultadoEvidencias";
                           WebComp_Wcanexos.componentprocess("W0033", "", sEvt);
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEKA2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAKA2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavNovoservico.Name = "vNOVOSERVICO";
            cmbavNovoservico.WebTags = "";
            cmbavNovoservico.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavNovoservico.ItemCount > 0 )
            {
               AV37NovoServico = (int)(NumberUtil.Val( cmbavNovoservico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV37NovoServico), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37NovoServico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37NovoServico), 6, 0)));
            }
            cmbavContratoservicos_codigo.Name = "vCONTRATOSERVICOS_CODIGO";
            cmbavContratoservicos_codigo.WebTags = "";
            cmbavContratoservicos_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavContratoservicos_codigo.ItemCount > 0 )
            {
               AV46ContratoServicos_Codigo = (int)(NumberUtil.Val( cmbavContratoservicos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV46ContratoServicos_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46ContratoServicos_Codigo), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavNovoservico_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavNovoservico.ItemCount > 0 )
         {
            AV37NovoServico = (int)(NumberUtil.Val( cmbavNovoservico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV37NovoServico), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37NovoServico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37NovoServico), 6, 0)));
         }
         if ( cmbavContratoservicos_codigo.ItemCount > 0 )
         {
            AV46ContratoServicos_Codigo = (int)(NumberUtil.Val( cmbavContratoservicos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV46ContratoServicos_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46ContratoServicos_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFKA2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavServicosigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicosigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicosigla_Enabled), 5, 0)));
      }

      protected void RFKA2( )
      {
         initialize_formulas( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( StringUtil.StrCmp(WebComp_Wcanexos_Component, "") == 0 )
            {
               WebComp_Wcanexos = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
               WebComp_Wcanexos.ComponentInit();
               WebComp_Wcanexos.Name = "WC_ContagemResultadoEvidencias";
               WebComp_Wcanexos_Component = "WC_ContagemResultadoEvidencias";
            }
            WebComp_Wcanexos.setjustcreated();
            WebComp_Wcanexos.componentprepare(new Object[] {(String)"W0033",(String)""});
            WebComp_Wcanexos.componentbind(new Object[] {});
            if ( isFullAjaxMode( ) )
            {
               context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0033"+"");
               WebComp_Wcanexos.componentdraw();
               context.httpAjaxContext.ajax_rspEndCmp();
            }
            if ( 1 != 0 )
            {
               WebComp_Wcanexos.componentstart();
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E15KA2 */
            E15KA2 ();
            WBKA0( ) ;
         }
      }

      protected void STRUPKA0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavServicosigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicosigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicosigla_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11KA2 */
         E11KA2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV43ServicoSigla = StringUtil.Upper( cgiGet( edtavServicosigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ServicoSigla", AV43ServicoSigla);
            cmbavNovoservico.CurrentValue = cgiGet( cmbavNovoservico_Internalname);
            AV37NovoServico = (int)(NumberUtil.Val( cgiGet( cmbavNovoservico_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37NovoServico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37NovoServico), 6, 0)));
            cmbavContratoservicos_codigo.CurrentValue = cgiGet( cmbavContratoservicos_codigo_Internalname);
            AV46ContratoServicos_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavContratoservicos_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46ContratoServicos_Codigo), 6, 0)));
            AV10Observacao = cgiGet( edtavObservacao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Observacao", AV10Observacao);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratada_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratada_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADA_CODIGO");
               GX_FocusControl = edtavContratada_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44Contratada_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Contratada_Codigo), 6, 0)));
            }
            else
            {
               AV44Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContratada_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Contratada_Codigo), 6, 0)));
            }
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11KA2 */
         E11KA2 ();
         if (returnInSub) return;
      }

      protected void E11KA2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV34WWPContext) ;
         Form.Caption = "Alterar Servi�o da OS "+StringUtil.Trim( AV35OS);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         bttBtnenter_Visible = (AV34WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
         edtavContratada_codigo_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_codigo_Visible), 5, 0)));
         /* Using cursor H00KA2 */
         pr_default.execute(0, new Object[] {AV8Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = H00KA2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00KA2_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = H00KA2_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = H00KA2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00KA2_n601ContagemResultado_Servico[0];
            A490ContagemResultado_ContratadaCod = H00KA2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00KA2_n490ContagemResultado_ContratadaCod[0];
            A601ContagemResultado_Servico = H00KA2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00KA2_n601ContagemResultado_Servico[0];
            AV9ServicoAtual = A601ContagemResultado_Servico;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ServicoAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ServicoAtual), 6, 0)));
            AV44Contratada_Codigo = A490ContagemResultado_ContratadaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Contratada_Codigo), 6, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Using cursor H00KA3 */
         pr_default.execute(1, new Object[] {AV44Contratada_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A39Contratada_Codigo = H00KA3_A39Contratada_Codigo[0];
            A74Contrato_Codigo = H00KA3_A74Contrato_Codigo[0];
            AV45Contratos.Add(A74Contrato_Codigo, 0);
            pr_default.readNext(1);
         }
         pr_default.close(1);
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A74Contrato_Codigo ,
                                              AV45Contratos ,
                                              A155Servico_Codigo ,
                                              AV9ServicoAtual },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00KA4 */
         pr_default.execute(2, new Object[] {AV9ServicoAtual});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A155Servico_Codigo = H00KA4_A155Servico_Codigo[0];
            A74Contrato_Codigo = H00KA4_A74Contrato_Codigo[0];
            A605Servico_Sigla = H00KA4_A605Servico_Sigla[0];
            A605Servico_Sigla = H00KA4_A605Servico_Sigla[0];
            cmbavNovoservico.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)), A605Servico_Sigla, 0);
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void E12KA2( )
      {
         /* Novoservico_Click Routine */
         cmbavContratoservicos_codigo.removeAllItems();
         /* Using cursor H00KA5 */
         pr_default.execute(3, new Object[] {AV37NovoServico, AV44Contratada_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A74Contrato_Codigo = H00KA5_A74Contrato_Codigo[0];
            A39Contratada_Codigo = H00KA5_A39Contratada_Codigo[0];
            A155Servico_Codigo = H00KA5_A155Servico_Codigo[0];
            A77Contrato_Numero = H00KA5_A77Contrato_Numero[0];
            A160ContratoServicos_Codigo = H00KA5_A160ContratoServicos_Codigo[0];
            A39Contratada_Codigo = H00KA5_A39Contratada_Codigo[0];
            A77Contrato_Numero = H00KA5_A77Contrato_Numero[0];
            cmbavContratoservicos_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)), A77Contrato_Numero, 0);
            AV46ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46ContratoServicos_Codigo), 6, 0)));
            pr_default.readNext(3);
         }
         pr_default.close(3);
         if ( ( cmbavContratoservicos_codigo.ItemCount == 0 ) || ( cmbavContratoservicos_codigo.ItemCount > 1 ) )
         {
            cmbavContratoservicos_codigo.addItem("0", "(Nenhum)", 1);
            AV46ContratoServicos_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46ContratoServicos_Codigo), 6, 0)));
         }
         cmbavContratoservicos_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV46ContratoServicos_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_codigo_Internalname, "Values", cmbavContratoservicos_codigo.ToJavascriptSource());
      }

      public void GXEnter( )
      {
         /* Execute user event: E13KA2 */
         E13KA2 ();
         if (returnInSub) return;
      }

      protected void E13KA2( )
      {
         /* Enter Routine */
         if ( (0==AV37NovoServico) )
         {
            GX_msglist.addItem("Selecione o servi�o correto da OS!");
            GX_FocusControl = cmbavNovoservico_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( (0==AV46ContratoServicos_Codigo) )
         {
            GX_msglist.addItem("Contrato � obrigat�rio!");
            GX_FocusControl = cmbavContratoservicos_codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV10Observacao)) )
         {
            GX_msglist.addItem("Observa��o � obrigat�rio!");
            GX_FocusControl = edtavObservacao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else
         {
            AV43ServicoSigla = cmbavNovoservico.Description;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ServicoSigla", AV43ServicoSigla);
            new prc_alterarservico(context ).execute( ref  AV8Codigo,  AV46ContratoServicos_Codigo, ref  AV10Observacao,  AV34WWPContext.gxTpr_Userid,  AV43ServicoSigla) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46ContratoServicos_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Observacao", AV10Observacao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ServicoSigla", AV43ServicoSigla);
            new prc_newevidenciademanda(context ).execute( ref  AV8Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Codigo), 6, 0)));
            context.setWebReturnParms(new Object[] {(String)AV43ServicoSigla,(String)AV23Nome});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E14KA2( )
      {
         /* 'DoFechar' Routine */
         context.setWebReturnParms(new Object[] {(String)AV43ServicoSigla,(String)AV23Nome});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E15KA2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_KA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_KA2( true) ;
         }
         else
         {
            wb_table2_8_KA2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_KA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table3_40_KA2( true) ;
         }
         else
         {
            wb_table3_40_KA2( false) ;
         }
         return  ;
      }

      protected void wb_table3_40_KA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_KA2e( true) ;
         }
         else
         {
            wb_table1_2_KA2e( false) ;
         }
      }

      protected void wb_table3_40_KA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 15, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "BtnDefault";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnenter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AlterarServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AlterarServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_40_KA2e( true) ;
         }
         else
         {
            wb_table3_40_KA2e( false) ;
         }
      }

      protected void wb_table2_8_KA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 1, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbservicoatual_Internalname, "Do servi�o", "", "", lblTbservicoatual_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_AlterarServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavServicosigla_Internalname, StringUtil.RTrim( AV43ServicoSigla), StringUtil.RTrim( context.localUtil.Format( AV43ServicoSigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicosigla_Jsonclick, 0, "Attribute", "", "", "", 1, edtavServicosigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_WP_AlterarServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbnovoservico_Internalname, "Para", "", "", lblTbnovoservico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_AlterarServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavNovoservico, cmbavNovoservico_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV37NovoServico), 6, 0)), 1, cmbavNovoservico_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVNOVOSERVICO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WP_AlterarServico.htm");
            cmbavNovoservico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37NovoServico), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNovoservico_Internalname, "Values", (String)(cmbavNovoservico.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "(Os prazos ser�o atualizados)", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_AlterarServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbnovoservico2_Internalname, "Contrato", "", "", lblTbnovoservico2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_AlterarServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratoservicos_codigo, cmbavContratoservicos_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV46ContratoServicos_Codigo), 6, 0)), 1, cmbavContratoservicos_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", true, "HLP_WP_AlterarServico.htm");
            cmbavContratoservicos_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV46ContratoServicos_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_codigo_Internalname, "Values", (String)(cmbavContratoservicos_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbobservacao_Internalname, "Observa��o", "", "", lblTbobservacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_AlterarServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='RequiredDataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavObservacao_Internalname, AV10Observacao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", 0, 1, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_WP_AlterarServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbanexos_Internalname, "Anexos", "", "", lblTbanexos_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_AlterarServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='DataContentCell'>") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0033"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( ! context.isAjaxRequest( ) )
               {
                  context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0033"+"");
               }
               WebComp_Wcanexos.componentdraw();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.httpAjaxContext.ajax_rspEndCmp();
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_KA2e( true) ;
         }
         else
         {
            wb_table2_8_KA2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV8Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Codigo), 6, 0)));
         AV35OS = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35OS", AV35OS);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vOS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV35OS, "@!"))));
         AV43ServicoSigla = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ServicoSigla", AV43ServicoSigla);
         AV23Nome = (String)getParm(obj,3);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Nome", AV23Nome);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAKA2( ) ;
         WSKA2( ) ;
         WEKA2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         if ( StringUtil.StrCmp(WebComp_Wcanexos_Component, "") == 0 )
         {
            WebComp_Wcanexos = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
            WebComp_Wcanexos.ComponentInit();
            WebComp_Wcanexos.Name = "WC_ContagemResultadoEvidencias";
            WebComp_Wcanexos_Component = "WC_ContagemResultadoEvidencias";
         }
         if ( ! ( WebComp_Wcanexos == null ) )
         {
            WebComp_Wcanexos.componentthemes();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216241690");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_alterarservico.js", "?20206216241690");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbservicoatual_Internalname = "TBSERVICOATUAL";
         edtavServicosigla_Internalname = "vSERVICOSIGLA";
         lblTbnovoservico_Internalname = "TBNOVOSERVICO";
         cmbavNovoservico_Internalname = "vNOVOSERVICO";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         lblTbnovoservico2_Internalname = "TBNOVOSERVICO2";
         cmbavContratoservicos_codigo_Internalname = "vCONTRATOSERVICOS_CODIGO";
         lblTbobservacao_Internalname = "TBOBSERVACAO";
         edtavObservacao_Internalname = "vOBSERVACAO";
         lblTbanexos_Internalname = "TBANEXOS";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtnfechar_Internalname = "BTNFECHAR";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavContratoservicos_codigo_Jsonclick = "";
         cmbavNovoservico_Jsonclick = "";
         edtavServicosigla_Jsonclick = "";
         edtavServicosigla_Enabled = 0;
         bttBtnenter_Visible = 1;
         edtavContratada_codigo_Jsonclick = "";
         edtavContratada_codigo_Visible = 1;
         cmbavNovoservico.Description = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Alterar Servi�o da OS";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("VNOVOSERVICO.CLICK","{handler:'E12KA2',iparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37NovoServico',fld:'vNOVOSERVICO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A77Contrato_Numero',fld:'CONTRATO_NUMERO',pic:'',nv:''}],oparms:[{av:'AV46ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("ENTER","{handler:'E13KA2',iparms:[{av:'AV37NovoServico',fld:'vNOVOSERVICO',pic:'ZZZZZ9',nv:0},{av:'AV46ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV10Observacao',fld:'vOBSERVACAO',pic:'',nv:''},{av:'cmbavNovoservico'},{av:'AV8Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV34WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV23Nome',fld:'vNOME',pic:'@!',nv:''}],oparms:[{av:'AV43ServicoSigla',fld:'vSERVICOSIGLA',pic:'@!',nv:''},{av:'AV10Observacao',fld:'vOBSERVACAO',pic:'',nv:''},{av:'AV8Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E14KA2',iparms:[{av:'AV23Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV43ServicoSigla',fld:'vSERVICOSIGLA',pic:'@!',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV35OS = "";
         wcpOAV43ServicoSigla = "";
         wcpOAV23Nome = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A77Contrato_Numero = "";
         AV34WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         WebComp_Wcanexos_Component = "";
         AV10Observacao = "";
         scmdbuf = "";
         H00KA2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00KA2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00KA2_A456ContagemResultado_Codigo = new int[1] ;
         H00KA2_A601ContagemResultado_Servico = new int[1] ;
         H00KA2_n601ContagemResultado_Servico = new bool[] {false} ;
         H00KA2_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00KA2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00KA3_A39Contratada_Codigo = new int[1] ;
         H00KA3_A74Contrato_Codigo = new int[1] ;
         AV45Contratos = new GxSimpleCollection();
         H00KA4_A155Servico_Codigo = new int[1] ;
         H00KA4_A74Contrato_Codigo = new int[1] ;
         H00KA4_A605Servico_Sigla = new String[] {""} ;
         A605Servico_Sigla = "";
         H00KA5_A74Contrato_Codigo = new int[1] ;
         H00KA5_A39Contratada_Codigo = new int[1] ;
         H00KA5_A155Servico_Codigo = new int[1] ;
         H00KA5_A77Contrato_Numero = new String[] {""} ;
         H00KA5_A160ContratoServicos_Codigo = new int[1] ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtnenter_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTbservicoatual_Jsonclick = "";
         lblTbnovoservico_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTbnovoservico2_Jsonclick = "";
         lblTbobservacao_Jsonclick = "";
         lblTbanexos_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_alterarservico__default(),
            new Object[][] {
                new Object[] {
               H00KA2_A1553ContagemResultado_CntSrvCod, H00KA2_n1553ContagemResultado_CntSrvCod, H00KA2_A456ContagemResultado_Codigo, H00KA2_A601ContagemResultado_Servico, H00KA2_n601ContagemResultado_Servico, H00KA2_A490ContagemResultado_ContratadaCod, H00KA2_n490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               H00KA3_A39Contratada_Codigo, H00KA3_A74Contrato_Codigo
               }
               , new Object[] {
               H00KA4_A155Servico_Codigo, H00KA4_A74Contrato_Codigo, H00KA4_A605Servico_Sigla
               }
               , new Object[] {
               H00KA5_A74Contrato_Codigo, H00KA5_A39Contratada_Codigo, H00KA5_A155Servico_Codigo, H00KA5_A77Contrato_Numero, H00KA5_A160ContratoServicos_Codigo
               }
            }
         );
         WebComp_Wcanexos = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavServicosigla_Enabled = 0;
      }

      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV8Codigo ;
      private int wcpOAV8Codigo ;
      private int A155Servico_Codigo ;
      private int A39Contratada_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int AV44Contratada_Codigo ;
      private int edtavContratada_codigo_Visible ;
      private int AV37NovoServico ;
      private int AV46ContratoServicos_Codigo ;
      private int edtavServicosigla_Enabled ;
      private int bttBtnenter_Visible ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int A601ContagemResultado_Servico ;
      private int A490ContagemResultado_ContratadaCod ;
      private int AV9ServicoAtual ;
      private int A74Contrato_Codigo ;
      private int idxLst ;
      private String AV43ServicoSigla ;
      private String AV23Nome ;
      private String wcpOAV43ServicoSigla ;
      private String wcpOAV23Nome ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A77Contrato_Numero ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavContratada_codigo_Internalname ;
      private String edtavContratada_codigo_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String WebComp_Wcanexos_Component ;
      private String cmbavNovoservico_Internalname ;
      private String edtavServicosigla_Internalname ;
      private String cmbavContratoservicos_codigo_Internalname ;
      private String edtavObservacao_Internalname ;
      private String bttBtnenter_Internalname ;
      private String scmdbuf ;
      private String A605Servico_Sigla ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String lblTbservicoatual_Internalname ;
      private String lblTbservicoatual_Jsonclick ;
      private String edtavServicosigla_Jsonclick ;
      private String lblTbnovoservico_Internalname ;
      private String lblTbnovoservico_Jsonclick ;
      private String cmbavNovoservico_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String lblTbnovoservico2_Internalname ;
      private String lblTbnovoservico2_Jsonclick ;
      private String cmbavContratoservicos_codigo_Jsonclick ;
      private String lblTbobservacao_Internalname ;
      private String lblTbobservacao_Jsonclick ;
      private String lblTbanexos_Internalname ;
      private String lblTbanexos_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n490ContagemResultado_ContratadaCod ;
      private String AV10Observacao ;
      private String AV35OS ;
      private String wcpOAV35OS ;
      private GXWebComponent WebComp_Wcanexos ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP2_ServicoSigla ;
      private String aP3_Nome ;
      private GXCombobox cmbavNovoservico ;
      private GXCombobox cmbavContratoservicos_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00KA2_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00KA2_n1553ContagemResultado_CntSrvCod ;
      private int[] H00KA2_A456ContagemResultado_Codigo ;
      private int[] H00KA2_A601ContagemResultado_Servico ;
      private bool[] H00KA2_n601ContagemResultado_Servico ;
      private int[] H00KA2_A490ContagemResultado_ContratadaCod ;
      private bool[] H00KA2_n490ContagemResultado_ContratadaCod ;
      private int[] H00KA3_A39Contratada_Codigo ;
      private int[] H00KA3_A74Contrato_Codigo ;
      private int[] H00KA4_A155Servico_Codigo ;
      private int[] H00KA4_A74Contrato_Codigo ;
      private String[] H00KA4_A605Servico_Sigla ;
      private int[] H00KA5_A74Contrato_Codigo ;
      private int[] H00KA5_A39Contratada_Codigo ;
      private int[] H00KA5_A155Servico_Codigo ;
      private String[] H00KA5_A77Contrato_Numero ;
      private int[] H00KA5_A160ContratoServicos_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV45Contratos ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV34WWPContext ;
   }

   public class wp_alterarservico__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00KA4( IGxContext context ,
                                             int A74Contrato_Codigo ,
                                             IGxCollection AV45Contratos ,
                                             int A155Servico_Codigo ,
                                             int AV9ServicoAtual )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [1] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT DISTINCT [Servico_Codigo], NULL AS [Contrato_Codigo], [Servico_Sigla] FROM ( SELECT TOP(100) PERCENT T1.[Servico_Codigo], T1.[Contrato_Codigo], T2.[Servico_Sigla] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV45Contratos, "T1.[Contrato_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T1.[Servico_Codigo] <> @AV9ServicoAtual)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Servico_Sigla]";
         scmdbuf = scmdbuf + ") DistinctT";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00KA4(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00KA2 ;
          prmH00KA2 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KA3 ;
          prmH00KA3 = new Object[] {
          new Object[] {"@AV44Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KA5 ;
          prmH00KA5 = new Object[] {
          new Object[] {"@AV37NovoServico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KA4 ;
          prmH00KA4 = new Object[] {
          new Object[] {"@AV9ServicoAtual",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00KA2", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_ContratadaCod] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV8Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KA2,1,0,false,true )
             ,new CursorDef("H00KA3", "SELECT [Contratada_Codigo], [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contratada_Codigo] = @AV44Contratada_Codigo ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KA3,100,0,false,false )
             ,new CursorDef("H00KA4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KA4,100,0,false,false )
             ,new CursorDef("H00KA5", "SELECT T1.[Contrato_Codigo], T2.[Contratada_Codigo], T1.[Servico_Codigo], T2.[Contrato_Numero], T1.[ContratoServicos_Codigo] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[Servico_Codigo] = @AV37NovoServico) AND (T2.[Contratada_Codigo] = @AV44Contratada_Codigo) ORDER BY T1.[Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KA5,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
