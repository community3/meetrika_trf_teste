/*
               File: GetPromptAtributosFilterData
        Description: Get Prompt Atributos Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:31.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptatributosfilterdata : GXProcedure
   {
      public getpromptatributosfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptatributosfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV23DDOName = aP0_DDOName;
         this.AV21SearchTxt = aP1_SearchTxt;
         this.AV22SearchTxtTo = aP2_SearchTxtTo;
         this.AV27OptionsJson = "" ;
         this.AV30OptionsDescJson = "" ;
         this.AV32OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV23DDOName = aP0_DDOName;
         this.AV21SearchTxt = aP1_SearchTxt;
         this.AV22SearchTxtTo = aP2_SearchTxtTo;
         this.AV27OptionsJson = "" ;
         this.AV30OptionsDescJson = "" ;
         this.AV32OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
         return AV32OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptatributosfilterdata objgetpromptatributosfilterdata;
         objgetpromptatributosfilterdata = new getpromptatributosfilterdata();
         objgetpromptatributosfilterdata.AV23DDOName = aP0_DDOName;
         objgetpromptatributosfilterdata.AV21SearchTxt = aP1_SearchTxt;
         objgetpromptatributosfilterdata.AV22SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptatributosfilterdata.AV27OptionsJson = "" ;
         objgetpromptatributosfilterdata.AV30OptionsDescJson = "" ;
         objgetpromptatributosfilterdata.AV32OptionIndexesJson = "" ;
         objgetpromptatributosfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptatributosfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptatributosfilterdata);
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptatributosfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV26Options = (IGxCollection)(new GxSimpleCollection());
         AV29OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV31OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_ATRIBUTOS_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADATRIBUTOS_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_ATRIBUTOS_DETALHES") == 0 )
         {
            /* Execute user subroutine: 'LOADATRIBUTOS_DETALHESOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_ATRIBUTOS_TABELANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADATRIBUTOS_TABELANOMOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV27OptionsJson = AV26Options.ToJSonString(false);
         AV30OptionsDescJson = AV29OptionsDesc.ToJSonString(false);
         AV32OptionIndexesJson = AV31OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV34Session.Get("PromptAtributosGridState"), "") == 0 )
         {
            AV36GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptAtributosGridState"), "");
         }
         else
         {
            AV36GridState.FromXml(AV34Session.Get("PromptAtributosGridState"), "");
         }
         AV50GXV1 = 1;
         while ( AV50GXV1 <= AV36GridState.gxTpr_Filtervalues.Count )
         {
            AV37GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV36GridState.gxTpr_Filtervalues.Item(AV50GXV1));
            if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_NOME") == 0 )
            {
               AV10TFAtributos_Nome = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_NOME_SEL") == 0 )
            {
               AV11TFAtributos_Nome_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_TIPODADOS_SEL") == 0 )
            {
               AV12TFAtributos_TipoDados_SelsJson = AV37GridStateFilterValue.gxTpr_Value;
               AV13TFAtributos_TipoDados_Sels.FromJSonString(AV12TFAtributos_TipoDados_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_DETALHES") == 0 )
            {
               AV14TFAtributos_Detalhes = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_DETALHES_SEL") == 0 )
            {
               AV15TFAtributos_Detalhes_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_TABELANOM") == 0 )
            {
               AV16TFAtributos_TabelaNom = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_TABELANOM_SEL") == 0 )
            {
               AV17TFAtributos_TabelaNom_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_PK_SEL") == 0 )
            {
               AV18TFAtributos_PK_Sel = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_FK_SEL") == 0 )
            {
               AV19TFAtributos_FK_Sel = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_ATIVO_SEL") == 0 )
            {
               AV20TFAtributos_Ativo_Sel = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
            }
            AV50GXV1 = (int)(AV50GXV1+1);
         }
         if ( AV36GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV38GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV36GridState.gxTpr_Dynamicfilters.Item(1));
            AV39DynamicFiltersSelector1 = AV38GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 )
            {
               AV40DynamicFiltersOperator1 = AV38GridStateDynamicFilter.gxTpr_Operator;
               AV41Atributos_Nome1 = AV38GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ATRIBUTOS_TABELANOM") == 0 )
            {
               AV40DynamicFiltersOperator1 = AV38GridStateDynamicFilter.gxTpr_Operator;
               AV42Atributos_TabelaNom1 = AV38GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV36GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV43DynamicFiltersEnabled2 = true;
               AV38GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV36GridState.gxTpr_Dynamicfilters.Item(2));
               AV44DynamicFiltersSelector2 = AV38GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 )
               {
                  AV45DynamicFiltersOperator2 = AV38GridStateDynamicFilter.gxTpr_Operator;
                  AV46Atributos_Nome2 = AV38GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "ATRIBUTOS_TABELANOM") == 0 )
               {
                  AV45DynamicFiltersOperator2 = AV38GridStateDynamicFilter.gxTpr_Operator;
                  AV47Atributos_TabelaNom2 = AV38GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADATRIBUTOS_NOMEOPTIONS' Routine */
         AV10TFAtributos_Nome = AV21SearchTxt;
         AV11TFAtributos_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A178Atributos_TipoDados ,
                                              AV13TFAtributos_TipoDados_Sels ,
                                              AV39DynamicFiltersSelector1 ,
                                              AV40DynamicFiltersOperator1 ,
                                              AV41Atributos_Nome1 ,
                                              AV42Atributos_TabelaNom1 ,
                                              AV43DynamicFiltersEnabled2 ,
                                              AV44DynamicFiltersSelector2 ,
                                              AV45DynamicFiltersOperator2 ,
                                              AV46Atributos_Nome2 ,
                                              AV47Atributos_TabelaNom2 ,
                                              AV11TFAtributos_Nome_Sel ,
                                              AV10TFAtributos_Nome ,
                                              AV13TFAtributos_TipoDados_Sels.Count ,
                                              AV15TFAtributos_Detalhes_Sel ,
                                              AV14TFAtributos_Detalhes ,
                                              AV17TFAtributos_TabelaNom_Sel ,
                                              AV16TFAtributos_TabelaNom ,
                                              AV18TFAtributos_PK_Sel ,
                                              AV19TFAtributos_FK_Sel ,
                                              AV20TFAtributos_Ativo_Sel ,
                                              A177Atributos_Nome ,
                                              A357Atributos_TabelaNom ,
                                              A390Atributos_Detalhes ,
                                              A400Atributos_PK ,
                                              A401Atributos_FK ,
                                              A180Atributos_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV41Atributos_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV41Atributos_Nome1), 50, "%");
         lV41Atributos_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV41Atributos_Nome1), 50, "%");
         lV42Atributos_TabelaNom1 = StringUtil.PadR( StringUtil.RTrim( AV42Atributos_TabelaNom1), 50, "%");
         lV42Atributos_TabelaNom1 = StringUtil.PadR( StringUtil.RTrim( AV42Atributos_TabelaNom1), 50, "%");
         lV46Atributos_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV46Atributos_Nome2), 50, "%");
         lV46Atributos_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV46Atributos_Nome2), 50, "%");
         lV47Atributos_TabelaNom2 = StringUtil.PadR( StringUtil.RTrim( AV47Atributos_TabelaNom2), 50, "%");
         lV47Atributos_TabelaNom2 = StringUtil.PadR( StringUtil.RTrim( AV47Atributos_TabelaNom2), 50, "%");
         lV10TFAtributos_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFAtributos_Nome), 50, "%");
         lV14TFAtributos_Detalhes = StringUtil.PadR( StringUtil.RTrim( AV14TFAtributos_Detalhes), 10, "%");
         lV16TFAtributos_TabelaNom = StringUtil.PadR( StringUtil.RTrim( AV16TFAtributos_TabelaNom), 50, "%");
         /* Using cursor P00UO2 */
         pr_default.execute(0, new Object[] {lV41Atributos_Nome1, lV41Atributos_Nome1, lV42Atributos_TabelaNom1, lV42Atributos_TabelaNom1, lV46Atributos_Nome2, lV46Atributos_Nome2, lV47Atributos_TabelaNom2, lV47Atributos_TabelaNom2, lV10TFAtributos_Nome, AV11TFAtributos_Nome_Sel, lV14TFAtributos_Detalhes, AV15TFAtributos_Detalhes_Sel, lV16TFAtributos_TabelaNom, AV17TFAtributos_TabelaNom_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUO2 = false;
            A356Atributos_TabelaCod = P00UO2_A356Atributos_TabelaCod[0];
            A177Atributos_Nome = P00UO2_A177Atributos_Nome[0];
            A180Atributos_Ativo = P00UO2_A180Atributos_Ativo[0];
            A401Atributos_FK = P00UO2_A401Atributos_FK[0];
            n401Atributos_FK = P00UO2_n401Atributos_FK[0];
            A400Atributos_PK = P00UO2_A400Atributos_PK[0];
            n400Atributos_PK = P00UO2_n400Atributos_PK[0];
            A390Atributos_Detalhes = P00UO2_A390Atributos_Detalhes[0];
            n390Atributos_Detalhes = P00UO2_n390Atributos_Detalhes[0];
            A178Atributos_TipoDados = P00UO2_A178Atributos_TipoDados[0];
            n178Atributos_TipoDados = P00UO2_n178Atributos_TipoDados[0];
            A357Atributos_TabelaNom = P00UO2_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = P00UO2_n357Atributos_TabelaNom[0];
            A176Atributos_Codigo = P00UO2_A176Atributos_Codigo[0];
            A357Atributos_TabelaNom = P00UO2_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = P00UO2_n357Atributos_TabelaNom[0];
            AV33count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00UO2_A177Atributos_Nome[0], A177Atributos_Nome) == 0 ) )
            {
               BRKUO2 = false;
               A176Atributos_Codigo = P00UO2_A176Atributos_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKUO2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A177Atributos_Nome)) )
            {
               AV25Option = A177Atributos_Nome;
               AV26Options.Add(AV25Option, 0);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUO2 )
            {
               BRKUO2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADATRIBUTOS_DETALHESOPTIONS' Routine */
         AV14TFAtributos_Detalhes = AV21SearchTxt;
         AV15TFAtributos_Detalhes_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A178Atributos_TipoDados ,
                                              AV13TFAtributos_TipoDados_Sels ,
                                              AV39DynamicFiltersSelector1 ,
                                              AV40DynamicFiltersOperator1 ,
                                              AV41Atributos_Nome1 ,
                                              AV42Atributos_TabelaNom1 ,
                                              AV43DynamicFiltersEnabled2 ,
                                              AV44DynamicFiltersSelector2 ,
                                              AV45DynamicFiltersOperator2 ,
                                              AV46Atributos_Nome2 ,
                                              AV47Atributos_TabelaNom2 ,
                                              AV11TFAtributos_Nome_Sel ,
                                              AV10TFAtributos_Nome ,
                                              AV13TFAtributos_TipoDados_Sels.Count ,
                                              AV15TFAtributos_Detalhes_Sel ,
                                              AV14TFAtributos_Detalhes ,
                                              AV17TFAtributos_TabelaNom_Sel ,
                                              AV16TFAtributos_TabelaNom ,
                                              AV18TFAtributos_PK_Sel ,
                                              AV19TFAtributos_FK_Sel ,
                                              AV20TFAtributos_Ativo_Sel ,
                                              A177Atributos_Nome ,
                                              A357Atributos_TabelaNom ,
                                              A390Atributos_Detalhes ,
                                              A400Atributos_PK ,
                                              A401Atributos_FK ,
                                              A180Atributos_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV41Atributos_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV41Atributos_Nome1), 50, "%");
         lV41Atributos_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV41Atributos_Nome1), 50, "%");
         lV42Atributos_TabelaNom1 = StringUtil.PadR( StringUtil.RTrim( AV42Atributos_TabelaNom1), 50, "%");
         lV42Atributos_TabelaNom1 = StringUtil.PadR( StringUtil.RTrim( AV42Atributos_TabelaNom1), 50, "%");
         lV46Atributos_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV46Atributos_Nome2), 50, "%");
         lV46Atributos_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV46Atributos_Nome2), 50, "%");
         lV47Atributos_TabelaNom2 = StringUtil.PadR( StringUtil.RTrim( AV47Atributos_TabelaNom2), 50, "%");
         lV47Atributos_TabelaNom2 = StringUtil.PadR( StringUtil.RTrim( AV47Atributos_TabelaNom2), 50, "%");
         lV10TFAtributos_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFAtributos_Nome), 50, "%");
         lV14TFAtributos_Detalhes = StringUtil.PadR( StringUtil.RTrim( AV14TFAtributos_Detalhes), 10, "%");
         lV16TFAtributos_TabelaNom = StringUtil.PadR( StringUtil.RTrim( AV16TFAtributos_TabelaNom), 50, "%");
         /* Using cursor P00UO3 */
         pr_default.execute(1, new Object[] {lV41Atributos_Nome1, lV41Atributos_Nome1, lV42Atributos_TabelaNom1, lV42Atributos_TabelaNom1, lV46Atributos_Nome2, lV46Atributos_Nome2, lV47Atributos_TabelaNom2, lV47Atributos_TabelaNom2, lV10TFAtributos_Nome, AV11TFAtributos_Nome_Sel, lV14TFAtributos_Detalhes, AV15TFAtributos_Detalhes_Sel, lV16TFAtributos_TabelaNom, AV17TFAtributos_TabelaNom_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKUO4 = false;
            A356Atributos_TabelaCod = P00UO3_A356Atributos_TabelaCod[0];
            A390Atributos_Detalhes = P00UO3_A390Atributos_Detalhes[0];
            n390Atributos_Detalhes = P00UO3_n390Atributos_Detalhes[0];
            A180Atributos_Ativo = P00UO3_A180Atributos_Ativo[0];
            A401Atributos_FK = P00UO3_A401Atributos_FK[0];
            n401Atributos_FK = P00UO3_n401Atributos_FK[0];
            A400Atributos_PK = P00UO3_A400Atributos_PK[0];
            n400Atributos_PK = P00UO3_n400Atributos_PK[0];
            A178Atributos_TipoDados = P00UO3_A178Atributos_TipoDados[0];
            n178Atributos_TipoDados = P00UO3_n178Atributos_TipoDados[0];
            A357Atributos_TabelaNom = P00UO3_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = P00UO3_n357Atributos_TabelaNom[0];
            A177Atributos_Nome = P00UO3_A177Atributos_Nome[0];
            A176Atributos_Codigo = P00UO3_A176Atributos_Codigo[0];
            A357Atributos_TabelaNom = P00UO3_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = P00UO3_n357Atributos_TabelaNom[0];
            AV33count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00UO3_A390Atributos_Detalhes[0], A390Atributos_Detalhes) == 0 ) )
            {
               BRKUO4 = false;
               A176Atributos_Codigo = P00UO3_A176Atributos_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKUO4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A390Atributos_Detalhes)) )
            {
               AV25Option = A390Atributos_Detalhes;
               AV26Options.Add(AV25Option, 0);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUO4 )
            {
               BRKUO4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADATRIBUTOS_TABELANOMOPTIONS' Routine */
         AV16TFAtributos_TabelaNom = AV21SearchTxt;
         AV17TFAtributos_TabelaNom_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A178Atributos_TipoDados ,
                                              AV13TFAtributos_TipoDados_Sels ,
                                              AV39DynamicFiltersSelector1 ,
                                              AV40DynamicFiltersOperator1 ,
                                              AV41Atributos_Nome1 ,
                                              AV42Atributos_TabelaNom1 ,
                                              AV43DynamicFiltersEnabled2 ,
                                              AV44DynamicFiltersSelector2 ,
                                              AV45DynamicFiltersOperator2 ,
                                              AV46Atributos_Nome2 ,
                                              AV47Atributos_TabelaNom2 ,
                                              AV11TFAtributos_Nome_Sel ,
                                              AV10TFAtributos_Nome ,
                                              AV13TFAtributos_TipoDados_Sels.Count ,
                                              AV15TFAtributos_Detalhes_Sel ,
                                              AV14TFAtributos_Detalhes ,
                                              AV17TFAtributos_TabelaNom_Sel ,
                                              AV16TFAtributos_TabelaNom ,
                                              AV18TFAtributos_PK_Sel ,
                                              AV19TFAtributos_FK_Sel ,
                                              AV20TFAtributos_Ativo_Sel ,
                                              A177Atributos_Nome ,
                                              A357Atributos_TabelaNom ,
                                              A390Atributos_Detalhes ,
                                              A400Atributos_PK ,
                                              A401Atributos_FK ,
                                              A180Atributos_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV41Atributos_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV41Atributos_Nome1), 50, "%");
         lV41Atributos_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV41Atributos_Nome1), 50, "%");
         lV42Atributos_TabelaNom1 = StringUtil.PadR( StringUtil.RTrim( AV42Atributos_TabelaNom1), 50, "%");
         lV42Atributos_TabelaNom1 = StringUtil.PadR( StringUtil.RTrim( AV42Atributos_TabelaNom1), 50, "%");
         lV46Atributos_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV46Atributos_Nome2), 50, "%");
         lV46Atributos_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV46Atributos_Nome2), 50, "%");
         lV47Atributos_TabelaNom2 = StringUtil.PadR( StringUtil.RTrim( AV47Atributos_TabelaNom2), 50, "%");
         lV47Atributos_TabelaNom2 = StringUtil.PadR( StringUtil.RTrim( AV47Atributos_TabelaNom2), 50, "%");
         lV10TFAtributos_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFAtributos_Nome), 50, "%");
         lV14TFAtributos_Detalhes = StringUtil.PadR( StringUtil.RTrim( AV14TFAtributos_Detalhes), 10, "%");
         lV16TFAtributos_TabelaNom = StringUtil.PadR( StringUtil.RTrim( AV16TFAtributos_TabelaNom), 50, "%");
         /* Using cursor P00UO4 */
         pr_default.execute(2, new Object[] {lV41Atributos_Nome1, lV41Atributos_Nome1, lV42Atributos_TabelaNom1, lV42Atributos_TabelaNom1, lV46Atributos_Nome2, lV46Atributos_Nome2, lV47Atributos_TabelaNom2, lV47Atributos_TabelaNom2, lV10TFAtributos_Nome, AV11TFAtributos_Nome_Sel, lV14TFAtributos_Detalhes, AV15TFAtributos_Detalhes_Sel, lV16TFAtributos_TabelaNom, AV17TFAtributos_TabelaNom_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKUO6 = false;
            A356Atributos_TabelaCod = P00UO4_A356Atributos_TabelaCod[0];
            A180Atributos_Ativo = P00UO4_A180Atributos_Ativo[0];
            A401Atributos_FK = P00UO4_A401Atributos_FK[0];
            n401Atributos_FK = P00UO4_n401Atributos_FK[0];
            A400Atributos_PK = P00UO4_A400Atributos_PK[0];
            n400Atributos_PK = P00UO4_n400Atributos_PK[0];
            A390Atributos_Detalhes = P00UO4_A390Atributos_Detalhes[0];
            n390Atributos_Detalhes = P00UO4_n390Atributos_Detalhes[0];
            A178Atributos_TipoDados = P00UO4_A178Atributos_TipoDados[0];
            n178Atributos_TipoDados = P00UO4_n178Atributos_TipoDados[0];
            A357Atributos_TabelaNom = P00UO4_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = P00UO4_n357Atributos_TabelaNom[0];
            A177Atributos_Nome = P00UO4_A177Atributos_Nome[0];
            A176Atributos_Codigo = P00UO4_A176Atributos_Codigo[0];
            A357Atributos_TabelaNom = P00UO4_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = P00UO4_n357Atributos_TabelaNom[0];
            AV33count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00UO4_A356Atributos_TabelaCod[0] == A356Atributos_TabelaCod ) )
            {
               BRKUO6 = false;
               A176Atributos_Codigo = P00UO4_A176Atributos_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKUO6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A357Atributos_TabelaNom)) )
            {
               AV25Option = A357Atributos_TabelaNom;
               AV24InsertIndex = 1;
               while ( ( AV24InsertIndex <= AV26Options.Count ) && ( StringUtil.StrCmp(((String)AV26Options.Item(AV24InsertIndex)), AV25Option) < 0 ) )
               {
                  AV24InsertIndex = (int)(AV24InsertIndex+1);
               }
               AV26Options.Add(AV25Option, AV24InsertIndex);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), AV24InsertIndex);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUO6 )
            {
               BRKUO6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV26Options = new GxSimpleCollection();
         AV29OptionsDesc = new GxSimpleCollection();
         AV31OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV34Session = context.GetSession();
         AV36GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV37GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFAtributos_Nome = "";
         AV11TFAtributos_Nome_Sel = "";
         AV12TFAtributos_TipoDados_SelsJson = "";
         AV13TFAtributos_TipoDados_Sels = new GxSimpleCollection();
         AV14TFAtributos_Detalhes = "";
         AV15TFAtributos_Detalhes_Sel = "";
         AV16TFAtributos_TabelaNom = "";
         AV17TFAtributos_TabelaNom_Sel = "";
         AV38GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV39DynamicFiltersSelector1 = "";
         AV41Atributos_Nome1 = "";
         AV42Atributos_TabelaNom1 = "";
         AV44DynamicFiltersSelector2 = "";
         AV46Atributos_Nome2 = "";
         AV47Atributos_TabelaNom2 = "";
         scmdbuf = "";
         lV41Atributos_Nome1 = "";
         lV42Atributos_TabelaNom1 = "";
         lV46Atributos_Nome2 = "";
         lV47Atributos_TabelaNom2 = "";
         lV10TFAtributos_Nome = "";
         lV14TFAtributos_Detalhes = "";
         lV16TFAtributos_TabelaNom = "";
         A178Atributos_TipoDados = "";
         A177Atributos_Nome = "";
         A357Atributos_TabelaNom = "";
         A390Atributos_Detalhes = "";
         P00UO2_A356Atributos_TabelaCod = new int[1] ;
         P00UO2_A177Atributos_Nome = new String[] {""} ;
         P00UO2_A180Atributos_Ativo = new bool[] {false} ;
         P00UO2_A401Atributos_FK = new bool[] {false} ;
         P00UO2_n401Atributos_FK = new bool[] {false} ;
         P00UO2_A400Atributos_PK = new bool[] {false} ;
         P00UO2_n400Atributos_PK = new bool[] {false} ;
         P00UO2_A390Atributos_Detalhes = new String[] {""} ;
         P00UO2_n390Atributos_Detalhes = new bool[] {false} ;
         P00UO2_A178Atributos_TipoDados = new String[] {""} ;
         P00UO2_n178Atributos_TipoDados = new bool[] {false} ;
         P00UO2_A357Atributos_TabelaNom = new String[] {""} ;
         P00UO2_n357Atributos_TabelaNom = new bool[] {false} ;
         P00UO2_A176Atributos_Codigo = new int[1] ;
         AV25Option = "";
         P00UO3_A356Atributos_TabelaCod = new int[1] ;
         P00UO3_A390Atributos_Detalhes = new String[] {""} ;
         P00UO3_n390Atributos_Detalhes = new bool[] {false} ;
         P00UO3_A180Atributos_Ativo = new bool[] {false} ;
         P00UO3_A401Atributos_FK = new bool[] {false} ;
         P00UO3_n401Atributos_FK = new bool[] {false} ;
         P00UO3_A400Atributos_PK = new bool[] {false} ;
         P00UO3_n400Atributos_PK = new bool[] {false} ;
         P00UO3_A178Atributos_TipoDados = new String[] {""} ;
         P00UO3_n178Atributos_TipoDados = new bool[] {false} ;
         P00UO3_A357Atributos_TabelaNom = new String[] {""} ;
         P00UO3_n357Atributos_TabelaNom = new bool[] {false} ;
         P00UO3_A177Atributos_Nome = new String[] {""} ;
         P00UO3_A176Atributos_Codigo = new int[1] ;
         P00UO4_A356Atributos_TabelaCod = new int[1] ;
         P00UO4_A180Atributos_Ativo = new bool[] {false} ;
         P00UO4_A401Atributos_FK = new bool[] {false} ;
         P00UO4_n401Atributos_FK = new bool[] {false} ;
         P00UO4_A400Atributos_PK = new bool[] {false} ;
         P00UO4_n400Atributos_PK = new bool[] {false} ;
         P00UO4_A390Atributos_Detalhes = new String[] {""} ;
         P00UO4_n390Atributos_Detalhes = new bool[] {false} ;
         P00UO4_A178Atributos_TipoDados = new String[] {""} ;
         P00UO4_n178Atributos_TipoDados = new bool[] {false} ;
         P00UO4_A357Atributos_TabelaNom = new String[] {""} ;
         P00UO4_n357Atributos_TabelaNom = new bool[] {false} ;
         P00UO4_A177Atributos_Nome = new String[] {""} ;
         P00UO4_A176Atributos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptatributosfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UO2_A356Atributos_TabelaCod, P00UO2_A177Atributos_Nome, P00UO2_A180Atributos_Ativo, P00UO2_A401Atributos_FK, P00UO2_n401Atributos_FK, P00UO2_A400Atributos_PK, P00UO2_n400Atributos_PK, P00UO2_A390Atributos_Detalhes, P00UO2_n390Atributos_Detalhes, P00UO2_A178Atributos_TipoDados,
               P00UO2_n178Atributos_TipoDados, P00UO2_A357Atributos_TabelaNom, P00UO2_n357Atributos_TabelaNom, P00UO2_A176Atributos_Codigo
               }
               , new Object[] {
               P00UO3_A356Atributos_TabelaCod, P00UO3_A390Atributos_Detalhes, P00UO3_n390Atributos_Detalhes, P00UO3_A180Atributos_Ativo, P00UO3_A401Atributos_FK, P00UO3_n401Atributos_FK, P00UO3_A400Atributos_PK, P00UO3_n400Atributos_PK, P00UO3_A178Atributos_TipoDados, P00UO3_n178Atributos_TipoDados,
               P00UO3_A357Atributos_TabelaNom, P00UO3_n357Atributos_TabelaNom, P00UO3_A177Atributos_Nome, P00UO3_A176Atributos_Codigo
               }
               , new Object[] {
               P00UO4_A356Atributos_TabelaCod, P00UO4_A180Atributos_Ativo, P00UO4_A401Atributos_FK, P00UO4_n401Atributos_FK, P00UO4_A400Atributos_PK, P00UO4_n400Atributos_PK, P00UO4_A390Atributos_Detalhes, P00UO4_n390Atributos_Detalhes, P00UO4_A178Atributos_TipoDados, P00UO4_n178Atributos_TipoDados,
               P00UO4_A357Atributos_TabelaNom, P00UO4_n357Atributos_TabelaNom, P00UO4_A177Atributos_Nome, P00UO4_A176Atributos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV18TFAtributos_PK_Sel ;
      private short AV19TFAtributos_FK_Sel ;
      private short AV20TFAtributos_Ativo_Sel ;
      private short AV40DynamicFiltersOperator1 ;
      private short AV45DynamicFiltersOperator2 ;
      private int AV50GXV1 ;
      private int AV13TFAtributos_TipoDados_Sels_Count ;
      private int A356Atributos_TabelaCod ;
      private int A176Atributos_Codigo ;
      private int AV24InsertIndex ;
      private long AV33count ;
      private String AV10TFAtributos_Nome ;
      private String AV11TFAtributos_Nome_Sel ;
      private String AV14TFAtributos_Detalhes ;
      private String AV15TFAtributos_Detalhes_Sel ;
      private String AV16TFAtributos_TabelaNom ;
      private String AV17TFAtributos_TabelaNom_Sel ;
      private String AV41Atributos_Nome1 ;
      private String AV42Atributos_TabelaNom1 ;
      private String AV46Atributos_Nome2 ;
      private String AV47Atributos_TabelaNom2 ;
      private String scmdbuf ;
      private String lV41Atributos_Nome1 ;
      private String lV42Atributos_TabelaNom1 ;
      private String lV46Atributos_Nome2 ;
      private String lV47Atributos_TabelaNom2 ;
      private String lV10TFAtributos_Nome ;
      private String lV14TFAtributos_Detalhes ;
      private String lV16TFAtributos_TabelaNom ;
      private String A178Atributos_TipoDados ;
      private String A177Atributos_Nome ;
      private String A357Atributos_TabelaNom ;
      private String A390Atributos_Detalhes ;
      private bool returnInSub ;
      private bool AV43DynamicFiltersEnabled2 ;
      private bool A400Atributos_PK ;
      private bool A401Atributos_FK ;
      private bool A180Atributos_Ativo ;
      private bool BRKUO2 ;
      private bool n401Atributos_FK ;
      private bool n400Atributos_PK ;
      private bool n390Atributos_Detalhes ;
      private bool n178Atributos_TipoDados ;
      private bool n357Atributos_TabelaNom ;
      private bool BRKUO4 ;
      private bool BRKUO6 ;
      private String AV32OptionIndexesJson ;
      private String AV27OptionsJson ;
      private String AV30OptionsDescJson ;
      private String AV12TFAtributos_TipoDados_SelsJson ;
      private String AV23DDOName ;
      private String AV21SearchTxt ;
      private String AV22SearchTxtTo ;
      private String AV39DynamicFiltersSelector1 ;
      private String AV44DynamicFiltersSelector2 ;
      private String AV25Option ;
      private IGxSession AV34Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00UO2_A356Atributos_TabelaCod ;
      private String[] P00UO2_A177Atributos_Nome ;
      private bool[] P00UO2_A180Atributos_Ativo ;
      private bool[] P00UO2_A401Atributos_FK ;
      private bool[] P00UO2_n401Atributos_FK ;
      private bool[] P00UO2_A400Atributos_PK ;
      private bool[] P00UO2_n400Atributos_PK ;
      private String[] P00UO2_A390Atributos_Detalhes ;
      private bool[] P00UO2_n390Atributos_Detalhes ;
      private String[] P00UO2_A178Atributos_TipoDados ;
      private bool[] P00UO2_n178Atributos_TipoDados ;
      private String[] P00UO2_A357Atributos_TabelaNom ;
      private bool[] P00UO2_n357Atributos_TabelaNom ;
      private int[] P00UO2_A176Atributos_Codigo ;
      private int[] P00UO3_A356Atributos_TabelaCod ;
      private String[] P00UO3_A390Atributos_Detalhes ;
      private bool[] P00UO3_n390Atributos_Detalhes ;
      private bool[] P00UO3_A180Atributos_Ativo ;
      private bool[] P00UO3_A401Atributos_FK ;
      private bool[] P00UO3_n401Atributos_FK ;
      private bool[] P00UO3_A400Atributos_PK ;
      private bool[] P00UO3_n400Atributos_PK ;
      private String[] P00UO3_A178Atributos_TipoDados ;
      private bool[] P00UO3_n178Atributos_TipoDados ;
      private String[] P00UO3_A357Atributos_TabelaNom ;
      private bool[] P00UO3_n357Atributos_TabelaNom ;
      private String[] P00UO3_A177Atributos_Nome ;
      private int[] P00UO3_A176Atributos_Codigo ;
      private int[] P00UO4_A356Atributos_TabelaCod ;
      private bool[] P00UO4_A180Atributos_Ativo ;
      private bool[] P00UO4_A401Atributos_FK ;
      private bool[] P00UO4_n401Atributos_FK ;
      private bool[] P00UO4_A400Atributos_PK ;
      private bool[] P00UO4_n400Atributos_PK ;
      private String[] P00UO4_A390Atributos_Detalhes ;
      private bool[] P00UO4_n390Atributos_Detalhes ;
      private String[] P00UO4_A178Atributos_TipoDados ;
      private bool[] P00UO4_n178Atributos_TipoDados ;
      private String[] P00UO4_A357Atributos_TabelaNom ;
      private bool[] P00UO4_n357Atributos_TabelaNom ;
      private String[] P00UO4_A177Atributos_Nome ;
      private int[] P00UO4_A176Atributos_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFAtributos_TipoDados_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV31OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV36GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV37GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV38GridStateDynamicFilter ;
   }

   public class getpromptatributosfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UO2( IGxContext context ,
                                             String A178Atributos_TipoDados ,
                                             IGxCollection AV13TFAtributos_TipoDados_Sels ,
                                             String AV39DynamicFiltersSelector1 ,
                                             short AV40DynamicFiltersOperator1 ,
                                             String AV41Atributos_Nome1 ,
                                             String AV42Atributos_TabelaNom1 ,
                                             bool AV43DynamicFiltersEnabled2 ,
                                             String AV44DynamicFiltersSelector2 ,
                                             short AV45DynamicFiltersOperator2 ,
                                             String AV46Atributos_Nome2 ,
                                             String AV47Atributos_TabelaNom2 ,
                                             String AV11TFAtributos_Nome_Sel ,
                                             String AV10TFAtributos_Nome ,
                                             int AV13TFAtributos_TipoDados_Sels_Count ,
                                             String AV15TFAtributos_Detalhes_Sel ,
                                             String AV14TFAtributos_Detalhes ,
                                             String AV17TFAtributos_TabelaNom_Sel ,
                                             String AV16TFAtributos_TabelaNom ,
                                             short AV18TFAtributos_PK_Sel ,
                                             short AV19TFAtributos_FK_Sel ,
                                             short AV20TFAtributos_Ativo_Sel ,
                                             String A177Atributos_Nome ,
                                             String A357Atributos_TabelaNom ,
                                             String A390Atributos_Detalhes ,
                                             bool A400Atributos_PK ,
                                             bool A401Atributos_FK ,
                                             bool A180Atributos_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [14] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Atributos_TabelaCod] AS Atributos_TabelaCod, T1.[Atributos_Nome], T1.[Atributos_Ativo], T1.[Atributos_FK], T1.[Atributos_PK], T1.[Atributos_Detalhes], T1.[Atributos_TipoDados], T2.[Tabela_Nome] AS Atributos_TabelaNom, T1.[Atributos_Codigo] FROM ([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod])";
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 ) && ( AV40DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Atributos_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV41Atributos_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV41Atributos_Nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 ) && ( AV40DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Atributos_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV41Atributos_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like '%' + @lV41Atributos_Nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV40DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Atributos_TabelaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV42Atributos_TabelaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV42Atributos_TabelaNom1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV40DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Atributos_TabelaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV42Atributos_TabelaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like '%' + @lV42Atributos_TabelaNom1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 ) && ( AV45DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Atributos_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV46Atributos_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV46Atributos_Nome2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 ) && ( AV45DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Atributos_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV46Atributos_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like '%' + @lV46Atributos_Nome2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV45DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Atributos_TabelaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV47Atributos_TabelaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV47Atributos_TabelaNom2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV45DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Atributos_TabelaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV47Atributos_TabelaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like '%' + @lV47Atributos_TabelaNom2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFAtributos_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFAtributos_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV10TFAtributos_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV10TFAtributos_Nome)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFAtributos_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] = @AV11TFAtributos_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] = @AV11TFAtributos_Nome_Sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV13TFAtributos_TipoDados_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFAtributos_TipoDados_Sels, "T1.[Atributos_TipoDados] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFAtributos_TipoDados_Sels, "T1.[Atributos_TipoDados] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAtributos_Detalhes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFAtributos_Detalhes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Detalhes] like @lV14TFAtributos_Detalhes)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Detalhes] like @lV14TFAtributos_Detalhes)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAtributos_Detalhes_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Detalhes] = @AV15TFAtributos_Detalhes_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Detalhes] = @AV15TFAtributos_Detalhes_Sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAtributos_TabelaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFAtributos_TabelaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV16TFAtributos_TabelaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV16TFAtributos_TabelaNom)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAtributos_TabelaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] = @AV17TFAtributos_TabelaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] = @AV17TFAtributos_TabelaNom_Sel)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV18TFAtributos_PK_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_PK] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_PK] = 1)";
            }
         }
         if ( AV18TFAtributos_PK_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_PK] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_PK] = 0)";
            }
         }
         if ( AV19TFAtributos_FK_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_FK] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_FK] = 1)";
            }
         }
         if ( AV19TFAtributos_FK_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_FK] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_FK] = 0)";
            }
         }
         if ( AV20TFAtributos_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Ativo] = 1)";
            }
         }
         if ( AV20TFAtributos_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Atributos_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00UO3( IGxContext context ,
                                             String A178Atributos_TipoDados ,
                                             IGxCollection AV13TFAtributos_TipoDados_Sels ,
                                             String AV39DynamicFiltersSelector1 ,
                                             short AV40DynamicFiltersOperator1 ,
                                             String AV41Atributos_Nome1 ,
                                             String AV42Atributos_TabelaNom1 ,
                                             bool AV43DynamicFiltersEnabled2 ,
                                             String AV44DynamicFiltersSelector2 ,
                                             short AV45DynamicFiltersOperator2 ,
                                             String AV46Atributos_Nome2 ,
                                             String AV47Atributos_TabelaNom2 ,
                                             String AV11TFAtributos_Nome_Sel ,
                                             String AV10TFAtributos_Nome ,
                                             int AV13TFAtributos_TipoDados_Sels_Count ,
                                             String AV15TFAtributos_Detalhes_Sel ,
                                             String AV14TFAtributos_Detalhes ,
                                             String AV17TFAtributos_TabelaNom_Sel ,
                                             String AV16TFAtributos_TabelaNom ,
                                             short AV18TFAtributos_PK_Sel ,
                                             short AV19TFAtributos_FK_Sel ,
                                             short AV20TFAtributos_Ativo_Sel ,
                                             String A177Atributos_Nome ,
                                             String A357Atributos_TabelaNom ,
                                             String A390Atributos_Detalhes ,
                                             bool A400Atributos_PK ,
                                             bool A401Atributos_FK ,
                                             bool A180Atributos_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [14] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Atributos_TabelaCod] AS Atributos_TabelaCod, T1.[Atributos_Detalhes], T1.[Atributos_Ativo], T1.[Atributos_FK], T1.[Atributos_PK], T1.[Atributos_TipoDados], T2.[Tabela_Nome] AS Atributos_TabelaNom, T1.[Atributos_Nome], T1.[Atributos_Codigo] FROM ([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod])";
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 ) && ( AV40DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Atributos_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV41Atributos_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV41Atributos_Nome1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 ) && ( AV40DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Atributos_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV41Atributos_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like '%' + @lV41Atributos_Nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV40DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Atributos_TabelaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV42Atributos_TabelaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV42Atributos_TabelaNom1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV40DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Atributos_TabelaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV42Atributos_TabelaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like '%' + @lV42Atributos_TabelaNom1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 ) && ( AV45DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Atributos_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV46Atributos_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV46Atributos_Nome2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 ) && ( AV45DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Atributos_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV46Atributos_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like '%' + @lV46Atributos_Nome2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV45DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Atributos_TabelaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV47Atributos_TabelaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV47Atributos_TabelaNom2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV45DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Atributos_TabelaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV47Atributos_TabelaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like '%' + @lV47Atributos_TabelaNom2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFAtributos_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFAtributos_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV10TFAtributos_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV10TFAtributos_Nome)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFAtributos_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] = @AV11TFAtributos_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] = @AV11TFAtributos_Nome_Sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV13TFAtributos_TipoDados_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFAtributos_TipoDados_Sels, "T1.[Atributos_TipoDados] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFAtributos_TipoDados_Sels, "T1.[Atributos_TipoDados] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAtributos_Detalhes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFAtributos_Detalhes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Detalhes] like @lV14TFAtributos_Detalhes)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Detalhes] like @lV14TFAtributos_Detalhes)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAtributos_Detalhes_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Detalhes] = @AV15TFAtributos_Detalhes_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Detalhes] = @AV15TFAtributos_Detalhes_Sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAtributos_TabelaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFAtributos_TabelaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV16TFAtributos_TabelaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV16TFAtributos_TabelaNom)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAtributos_TabelaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] = @AV17TFAtributos_TabelaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] = @AV17TFAtributos_TabelaNom_Sel)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV18TFAtributos_PK_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_PK] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_PK] = 1)";
            }
         }
         if ( AV18TFAtributos_PK_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_PK] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_PK] = 0)";
            }
         }
         if ( AV19TFAtributos_FK_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_FK] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_FK] = 1)";
            }
         }
         if ( AV19TFAtributos_FK_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_FK] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_FK] = 0)";
            }
         }
         if ( AV20TFAtributos_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Ativo] = 1)";
            }
         }
         if ( AV20TFAtributos_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Atributos_Detalhes]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00UO4( IGxContext context ,
                                             String A178Atributos_TipoDados ,
                                             IGxCollection AV13TFAtributos_TipoDados_Sels ,
                                             String AV39DynamicFiltersSelector1 ,
                                             short AV40DynamicFiltersOperator1 ,
                                             String AV41Atributos_Nome1 ,
                                             String AV42Atributos_TabelaNom1 ,
                                             bool AV43DynamicFiltersEnabled2 ,
                                             String AV44DynamicFiltersSelector2 ,
                                             short AV45DynamicFiltersOperator2 ,
                                             String AV46Atributos_Nome2 ,
                                             String AV47Atributos_TabelaNom2 ,
                                             String AV11TFAtributos_Nome_Sel ,
                                             String AV10TFAtributos_Nome ,
                                             int AV13TFAtributos_TipoDados_Sels_Count ,
                                             String AV15TFAtributos_Detalhes_Sel ,
                                             String AV14TFAtributos_Detalhes ,
                                             String AV17TFAtributos_TabelaNom_Sel ,
                                             String AV16TFAtributos_TabelaNom ,
                                             short AV18TFAtributos_PK_Sel ,
                                             short AV19TFAtributos_FK_Sel ,
                                             short AV20TFAtributos_Ativo_Sel ,
                                             String A177Atributos_Nome ,
                                             String A357Atributos_TabelaNom ,
                                             String A390Atributos_Detalhes ,
                                             bool A400Atributos_PK ,
                                             bool A401Atributos_FK ,
                                             bool A180Atributos_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [14] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Atributos_TabelaCod] AS Atributos_TabelaCod, T1.[Atributos_Ativo], T1.[Atributos_FK], T1.[Atributos_PK], T1.[Atributos_Detalhes], T1.[Atributos_TipoDados], T2.[Tabela_Nome] AS Atributos_TabelaNom, T1.[Atributos_Nome], T1.[Atributos_Codigo] FROM ([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod])";
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 ) && ( AV40DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Atributos_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV41Atributos_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV41Atributos_Nome1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 ) && ( AV40DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Atributos_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV41Atributos_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like '%' + @lV41Atributos_Nome1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV40DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Atributos_TabelaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV42Atributos_TabelaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV42Atributos_TabelaNom1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV40DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Atributos_TabelaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV42Atributos_TabelaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like '%' + @lV42Atributos_TabelaNom1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 ) && ( AV45DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Atributos_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV46Atributos_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV46Atributos_Nome2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 ) && ( AV45DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Atributos_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV46Atributos_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like '%' + @lV46Atributos_Nome2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV45DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Atributos_TabelaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV47Atributos_TabelaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV47Atributos_TabelaNom2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV45DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Atributos_TabelaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV47Atributos_TabelaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like '%' + @lV47Atributos_TabelaNom2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFAtributos_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFAtributos_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV10TFAtributos_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV10TFAtributos_Nome)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFAtributos_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] = @AV11TFAtributos_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] = @AV11TFAtributos_Nome_Sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV13TFAtributos_TipoDados_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFAtributos_TipoDados_Sels, "T1.[Atributos_TipoDados] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFAtributos_TipoDados_Sels, "T1.[Atributos_TipoDados] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAtributos_Detalhes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFAtributos_Detalhes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Detalhes] like @lV14TFAtributos_Detalhes)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Detalhes] like @lV14TFAtributos_Detalhes)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAtributos_Detalhes_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Detalhes] = @AV15TFAtributos_Detalhes_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Detalhes] = @AV15TFAtributos_Detalhes_Sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAtributos_TabelaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFAtributos_TabelaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV16TFAtributos_TabelaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV16TFAtributos_TabelaNom)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAtributos_TabelaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] = @AV17TFAtributos_TabelaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] = @AV17TFAtributos_TabelaNom_Sel)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV18TFAtributos_PK_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_PK] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_PK] = 1)";
            }
         }
         if ( AV18TFAtributos_PK_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_PK] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_PK] = 0)";
            }
         }
         if ( AV19TFAtributos_FK_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_FK] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_FK] = 1)";
            }
         }
         if ( AV19TFAtributos_FK_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_FK] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_FK] = 0)";
            }
         }
         if ( AV20TFAtributos_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Ativo] = 1)";
            }
         }
         if ( AV20TFAtributos_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Atributos_TabelaCod]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UO2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (bool)dynConstraints[24] , (bool)dynConstraints[25] , (bool)dynConstraints[26] );
               case 1 :
                     return conditional_P00UO3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (bool)dynConstraints[24] , (bool)dynConstraints[25] , (bool)dynConstraints[26] );
               case 2 :
                     return conditional_P00UO4(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (bool)dynConstraints[24] , (bool)dynConstraints[25] , (bool)dynConstraints[26] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UO2 ;
          prmP00UO2 = new Object[] {
          new Object[] {"@lV41Atributos_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV41Atributos_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Atributos_TabelaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Atributos_TabelaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46Atributos_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46Atributos_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47Atributos_TabelaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47Atributos_TabelaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFAtributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFAtributos_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14TFAtributos_Detalhes",SqlDbType.Char,10,0} ,
          new Object[] {"@AV15TFAtributos_Detalhes_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV16TFAtributos_TabelaNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV17TFAtributos_TabelaNom_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00UO3 ;
          prmP00UO3 = new Object[] {
          new Object[] {"@lV41Atributos_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV41Atributos_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Atributos_TabelaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Atributos_TabelaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46Atributos_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46Atributos_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47Atributos_TabelaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47Atributos_TabelaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFAtributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFAtributos_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14TFAtributos_Detalhes",SqlDbType.Char,10,0} ,
          new Object[] {"@AV15TFAtributos_Detalhes_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV16TFAtributos_TabelaNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV17TFAtributos_TabelaNom_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00UO4 ;
          prmP00UO4 = new Object[] {
          new Object[] {"@lV41Atributos_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV41Atributos_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Atributos_TabelaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Atributos_TabelaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46Atributos_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46Atributos_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47Atributos_TabelaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47Atributos_TabelaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFAtributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFAtributos_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14TFAtributos_Detalhes",SqlDbType.Char,10,0} ,
          new Object[] {"@AV15TFAtributos_Detalhes_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV16TFAtributos_TabelaNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV17TFAtributos_TabelaNom_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UO2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UO2,100,0,true,false )
             ,new CursorDef("P00UO3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UO3,100,0,true,false )
             ,new CursorDef("P00UO4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UO4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 4) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 4) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 4) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptatributosfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptatributosfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptatributosfilterdata") )
          {
             return  ;
          }
          getpromptatributosfilterdata worker = new getpromptatributosfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
