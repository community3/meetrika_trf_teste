/*
               File: WP_CopiarColarModulos
        Description: Copiar e Colar Modulos dos Sistemas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/3/2020 23:30:48.59
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_copiarcolarmodulos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_copiarcolarmodulos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_copiarcolarmodulos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridtabelas") == 0 )
            {
               nRC_GXsfl_12 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_12_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_12_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridtabelas_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridtabelas") == 0 )
            {
               AV8Filtro = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Filtro", AV8Filtro);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridtabelas_refresh( AV8Filtro) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PABD2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTBD2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205323304863");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_copiarcolarmodulos.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vFILTRO", StringUtil.RTrim( AV8Filtro));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_12", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_12), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vDADOSCOLADOS", AV5DadosColados);
         GxWebStd.gx_hidden_field( context, "MODULO_NOME", StringUtil.RTrim( A143Modulo_Nome));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUDPARG1", AV25Udparg1);
         GxWebStd.gx_hidden_field( context, "vMODULO_NOME", StringUtil.RTrim( AV15Modulo_Nome));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEBD2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTBD2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_copiarcolarmodulos.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_CopiarColarModulos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Copiar e Colar Modulos dos Sistemas" ;
      }

      protected void WBBD0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            wb_table1_3_BD2( true) ;
         }
         else
         {
            wb_table1_3_BD2( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_BD2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTBD2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Copiar e Colar Modulos dos Sistemas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPBD0( ) ;
      }

      protected void WSBD2( )
      {
         STARTBD2( ) ;
         EVTBD2( ) ;
      }

      protected void EVTBD2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'GRAVAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11BD2 */
                              E11BD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12BD2 */
                              E12BD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_12_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_12_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_12_idx), 4, 0)), 4, "0");
                              SubsflControlProps_122( ) ;
                              A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", "."));
                              A416Sistema_Nome = StringUtil.Upper( cgiGet( edtSistema_Nome_Internalname));
                              AV7Dados = cgiGet( edtavDados_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDados_Internalname, AV7Dados);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13BD2 */
                                    E13BD2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14BD2 */
                                    E14BD2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Filtro Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFILTRO"), AV8Filtro) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEBD2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PABD2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavFiltro_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridtabelas_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_122( ) ;
         while ( nGXsfl_12_idx <= nRC_GXsfl_12 )
         {
            sendrow_122( ) ;
            nGXsfl_12_idx = (short)(((subGridtabelas_Islastpage==1)&&(nGXsfl_12_idx+1>subGridtabelas_Recordsperpage( )) ? 1 : nGXsfl_12_idx+1));
            sGXsfl_12_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_12_idx), 4, 0)), 4, "0");
            SubsflControlProps_122( ) ;
         }
         context.GX_webresponse.AddString(GridtabelasContainer.ToJavascriptSource());
         /* End function gxnrGridtabelas_newrow */
      }

      protected void gxgrGridtabelas_refresh( String AV8Filtro )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRIDTABELAS_nCurrentRecord = 0;
         RFBD2( ) ;
         /* End function gxgrGridtabelas_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "SISTEMA_NOME", A416Sistema_Nome);
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFBD2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFBD2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridtabelasContainer.ClearRows();
         }
         wbStart = 12;
         nGXsfl_12_idx = 1;
         sGXsfl_12_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_12_idx), 4, 0)), 4, "0");
         SubsflControlProps_122( ) ;
         nGXsfl_12_Refreshing = 1;
         GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
         GridtabelasContainer.AddObjectProperty("CmpContext", "");
         GridtabelasContainer.AddObjectProperty("InMasterPage", "false");
         GridtabelasContainer.AddObjectProperty("Class", "WorkWith");
         GridtabelasContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridtabelasContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridtabelasContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Backcolorstyle), 1, 0, ".", "")));
         GridtabelasContainer.PageSize = subGridtabelas_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_122( ) ;
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV8Filtro ,
                                                 A416Sistema_Nome },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING
                                                 }
            });
            lV8Filtro = StringUtil.PadR( StringUtil.RTrim( AV8Filtro), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Filtro", AV8Filtro);
            /* Using cursor H00BD2 */
            pr_default.execute(0, new Object[] {lV8Filtro});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A416Sistema_Nome = H00BD2_A416Sistema_Nome[0];
               A127Sistema_Codigo = H00BD2_A127Sistema_Codigo[0];
               /* Execute user event: E14BD2 */
               E14BD2 ();
               pr_default.readNext(0);
            }
            pr_default.close(0);
            wbEnd = 12;
            WBBD0( ) ;
         }
         nGXsfl_12_Refreshing = 0;
      }

      protected int subGridtabelas_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGridtabelas_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridtabelas_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGridtabelas_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPBD0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13BD2 */
         E13BD2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV8Filtro = StringUtil.Upper( cgiGet( edtavFiltro_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Filtro", AV8Filtro);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavInseridas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavInseridas_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vINSERIDAS");
               GX_FocusControl = edtavInseridas_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV11Inseridas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Inseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Inseridas), 4, 0)));
            }
            else
            {
               AV11Inseridas = (short)(context.localUtil.CToN( cgiGet( edtavInseridas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Inseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Inseridas), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavJacadastradas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavJacadastradas_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vJACADASTRADAS");
               GX_FocusControl = edtavJacadastradas_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV12JaCadastradas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12JaCadastradas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12JaCadastradas), 4, 0)));
            }
            else
            {
               AV12JaCadastradas = (short)(context.localUtil.CToN( cgiGet( edtavJacadastradas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12JaCadastradas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12JaCadastradas), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavNaoinseridas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavNaoinseridas_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vNAOINSERIDAS");
               GX_FocusControl = edtavNaoinseridas_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16NaoInseridas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16NaoInseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16NaoInseridas), 4, 0)));
            }
            else
            {
               AV16NaoInseridas = (short)(context.localUtil.CToN( cgiGet( edtavNaoinseridas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16NaoInseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16NaoInseridas), 4, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_12 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_12"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13BD2 */
         E13BD2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13BD2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV20WWPContext) ;
         tblTblresultado_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblresultado_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblresultado_Visible), 5, 0)));
         AV9FiltroRecebido = AV19WebSession.Get("FiltroRecebido");
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9FiltroRecebido)) )
         {
            AV8Filtro = AV9FiltroRecebido;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Filtro", AV8Filtro);
         }
      }

      protected void E11BD2( )
      {
         /* 'Gravar' Routine */
         AV11Inseridas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Inseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Inseridas), 4, 0)));
         AV12JaCadastradas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12JaCadastradas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12JaCadastradas), 4, 0)));
         AV16NaoInseridas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16NaoInseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16NaoInseridas), 4, 0)));
         /* Start For Each Line */
         nRC_GXsfl_12 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_12"), ",", "."));
         nGXsfl_12_fel_idx = 0;
         while ( nGXsfl_12_fel_idx < nRC_GXsfl_12 )
         {
            nGXsfl_12_fel_idx = (short)(((subGridtabelas_Islastpage==1)&&(nGXsfl_12_fel_idx+1>subGridtabelas_Recordsperpage( )) ? 1 : nGXsfl_12_fel_idx+1));
            sGXsfl_12_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_12_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_122( ) ;
            A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", "."));
            A416Sistema_Nome = StringUtil.Upper( cgiGet( edtSistema_Nome_Internalname));
            AV7Dados = cgiGet( edtavDados_Internalname);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV7Dados)) )
            {
               AV18Sistema_Codigo = A127Sistema_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Sistema_Codigo), 6, 0)));
               AV5DadosColados = AV7Dados;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5DadosColados", AV5DadosColados);
               /* Execute user subroutine: 'GRAVARMODULOS' */
               S112 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
            /* End For Each Line */
         }
         if ( nGXsfl_12_fel_idx == 0 )
         {
            nGXsfl_12_idx = 1;
            sGXsfl_12_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_12_idx), 4, 0)), 4, "0");
            SubsflControlProps_122( ) ;
         }
         nGXsfl_12_fel_idx = 1;
         tblTblresultado_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblresultado_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblresultado_Visible), 5, 0)));
      }

      protected void S112( )
      {
         /* 'GRAVARMODULOS' Routine */
         AV10i = 0;
         AV15Modulo_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Modulo_Nome", AV15Modulo_Nome);
         AV10i = 1;
         while ( AV10i <= StringUtil.Len( StringUtil.Trim( AV5DadosColados)) )
         {
            AV17s = (short)(AV10i+1);
            AV6Char = StringUtil.Substring( AV5DadosColados, AV10i, 1);
            if ( StringUtil.StrCmp(AV6Char, " ") == 0 )
            {
               if ( StringUtil.Asc( StringUtil.Substring( AV5DadosColados, AV17s, 1)) > 32 )
               {
                  if ( StringUtil.Len( AV15Modulo_Nome) > 1 )
                  {
                     AV15Modulo_Nome = AV15Modulo_Nome + "_";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Modulo_Nome", AV15Modulo_Nome);
                  }
               }
               else
               {
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15Modulo_Nome)) )
                  {
                     /* Execute user subroutine: 'NEWATRIBUTO' */
                     S122 ();
                     if ( returnInSub )
                     {
                        returnInSub = true;
                        if (true) return;
                     }
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV6Char, "�") == 0 )
            {
               AV15Modulo_Nome = AV15Modulo_Nome + "o";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Modulo_Nome", AV15Modulo_Nome);
            }
            else if ( StringUtil.StrCmp(AV6Char, "�") == 0 )
            {
               AV15Modulo_Nome = AV15Modulo_Nome + "a";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Modulo_Nome", AV15Modulo_Nome);
            }
            else if ( ( StringUtil.StrCmp(AV6Char, ",") == 0 ) || ( StringUtil.StrCmp(AV6Char, ";") == 0 ) || ( StringUtil.StrCmp(AV6Char, StringUtil.Chr( 9)) == 0 ) || ( StringUtil.StrCmp(AV6Char, StringUtil.Chr( 10)) == 0 ) || ( StringUtil.StrCmp(AV6Char, StringUtil.Chr( 13)) == 0 ) )
            {
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15Modulo_Nome)) )
               {
                  /* Execute user subroutine: 'NEWATRIBUTO' */
                  S122 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
            }
            else if ( AV10i == StringUtil.Len( StringUtil.Trim( AV5DadosColados)) )
            {
               AV15Modulo_Nome = AV15Modulo_Nome + AV6Char;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Modulo_Nome", AV15Modulo_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15Modulo_Nome)) )
               {
                  /* Execute user subroutine: 'NEWATRIBUTO' */
                  S122 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
            }
            else
            {
               AV15Modulo_Nome = AV15Modulo_Nome + AV6Char;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Modulo_Nome", AV15Modulo_Nome);
            }
            AV10i = (short)(AV10i+1);
         }
      }

      protected void S122( )
      {
         /* 'NEWATRIBUTO' Routine */
         AV24GXLvl66 = 0;
         AV25Udparg1 = new prc_padronizastring(context).executeUdp(  AV15Modulo_Nome);
         /* Using cursor H00BD3 */
         pr_default.execute(1, new Object[] {AV18Sistema_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A127Sistema_Codigo = H00BD3_A127Sistema_Codigo[0];
            A143Modulo_Nome = H00BD3_A143Modulo_Nome[0];
            if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A143Modulo_Nome), AV25Udparg1) == 0 )
            {
               AV24GXLvl66 = 1;
               AV12JaCadastradas = (short)(AV12JaCadastradas+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12JaCadastradas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12JaCadastradas), 4, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( AV24GXLvl66 == 0 )
         {
            AV13Modulo = new SdtModulo(context);
            AV13Modulo.gxTpr_Modulo_nome = StringUtil.Upper( StringUtil.Trim( AV15Modulo_Nome));
            AV13Modulo.gxTpr_Modulo_sigla = StringUtil.Substring( AV13Modulo.gxTpr_Modulo_nome, 1, 15);
            AV13Modulo.gxTpr_Sistema_codigo = AV18Sistema_Codigo;
            AV13Modulo.Save();
            if ( AV13Modulo.Success() )
            {
               context.CommitDataStores( "WP_CopiarColarModulos");
               AV11Inseridas = (short)(AV11Inseridas+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Inseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Inseridas), 4, 0)));
            }
            else
            {
               AV16NaoInseridas = (short)(AV16NaoInseridas+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16NaoInseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16NaoInseridas), 4, 0)));
               context.RollbackDataStores( "WP_CopiarColarModulos");
            }
         }
         AV15Modulo_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Modulo_Nome", AV15Modulo_Nome);
      }

      protected void E12BD2( )
      {
         /* 'DoFechar' Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      private void E14BD2( )
      {
         /* Load Routine */
         sendrow_122( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_12_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(12, GridtabelasRow);
         }
      }

      protected void wb_table1_3_BD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"left\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left;height:50px")+"\" class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Filtro:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CopiarColarModulos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 7,'',false,'" + sGXsfl_12_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFiltro_Internalname, StringUtil.RTrim( AV8Filtro), StringUtil.RTrim( context.localUtil.Format( AV8Filtro, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,7);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFiltro_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_CopiarColarModulos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            /*  Grid Control  */
            GridtabelasContainer.SetWrapped(nGXWrapped);
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridtabelasContainer"+"DivS\" data-gxgridid=\"12\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridtabelas_Internalname, subGridtabelas_Internalname, "", "WorkWith", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridtabelas_Backcolorstyle == 0 )
               {
                  subGridtabelas_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                  {
                     subGridtabelas_Linesclass = subGridtabelas_Class+"Title";
                  }
               }
               else
               {
                  subGridtabelas_Titlebackstyle = 1;
                  if ( subGridtabelas_Backcolorstyle == 1 )
                  {
                     subGridtabelas_Titlebackcolor = subGridtabelas_Allbackcolor;
                     if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                     {
                        subGridtabelas_Linesclass = subGridtabelas_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                     {
                        subGridtabelas_Linesclass = subGridtabelas_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(570), 4, 0))+"px"+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(400), 4, 0))+"px"+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "M�dulos para inserir") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridtabelasContainer = new GXWebGrid( context);
               }
               else
               {
                  GridtabelasContainer.Clear();
               }
               GridtabelasContainer.SetWrapped(nGXWrapped);
               GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
               GridtabelasContainer.AddObjectProperty("Class", "WorkWith");
               GridtabelasContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Backcolorstyle), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("CmpContext", "");
               GridtabelasContainer.AddObjectProperty("InMasterPage", "false");
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ".", "")));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", A416Sistema_Nome);
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", AV7Dados);
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowselection), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Selectioncolor), 9, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowhovering), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Hoveringcolor), 9, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowcollapsing), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 12 )
         {
            wbEnd = 0;
            nRC_GXsfl_12 = (short)(nGXsfl_12_idx-1);
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridtabelasContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridtabelas", GridtabelasContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridtabelasContainerData", GridtabelasContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridtabelasContainerData"+"V", GridtabelasContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridtabelasContainerData"+"V"+"\" value='"+GridtabelasContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='Table'>") ;
            wb_table2_17_BD2( true) ;
         }
         else
         {
            wb_table2_17_BD2( false) ;
         }
         return  ;
      }

      protected void wb_table2_17_BD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttGravar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(12), 2, 0)+","+"null"+");", "Gravar", bttGravar_Jsonclick, 5, "Gravar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'GRAVAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_CopiarColarModulos.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(12), 2, 0)+","+"null"+");", "Fechar", bttBtn_trn_cancel_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_CopiarColarModulos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_BD2e( true) ;
         }
         else
         {
            wb_table1_3_BD2e( false) ;
         }
      }

      protected void wb_table2_17_BD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblresultado_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblresultado_Internalname, tblTblresultado_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Inseridas:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CopiarColarModulos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'" + sGXsfl_12_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavInseridas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Inseridas), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV11Inseridas), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavInseridas_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_CopiarColarModulos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Procuradas:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CopiarColarModulos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'" + sGXsfl_12_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavJacadastradas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12JaCadastradas), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12JaCadastradas), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavJacadastradas_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_CopiarColarModulos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "Erros:", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CopiarColarModulos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_12_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNaoinseridas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16NaoInseridas), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV16NaoInseridas), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNaoinseridas_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_CopiarColarModulos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_17_BD2e( true) ;
         }
         else
         {
            wb_table2_17_BD2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PABD2( ) ;
         WSBD2( ) ;
         WEBD2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205323304897");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_copiarcolarmodulos.js", "?20205323304897");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_122( )
      {
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO_"+sGXsfl_12_idx;
         edtSistema_Nome_Internalname = "SISTEMA_NOME_"+sGXsfl_12_idx;
         edtavDados_Internalname = "vDADOS_"+sGXsfl_12_idx;
      }

      protected void SubsflControlProps_fel_122( )
      {
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO_"+sGXsfl_12_fel_idx;
         edtSistema_Nome_Internalname = "SISTEMA_NOME_"+sGXsfl_12_fel_idx;
         edtavDados_Internalname = "vDADOS_"+sGXsfl_12_fel_idx;
      }

      protected void sendrow_122( )
      {
         SubsflControlProps_122( ) ;
         WBBD0( ) ;
         GridtabelasRow = GXWebRow.GetNew(context,GridtabelasContainer);
         if ( subGridtabelas_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridtabelas_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
            {
               subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
            }
         }
         else if ( subGridtabelas_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridtabelas_Backstyle = 0;
            subGridtabelas_Backcolor = subGridtabelas_Allbackcolor;
            if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
            {
               subGridtabelas_Linesclass = subGridtabelas_Class+"Uniform";
            }
         }
         else if ( subGridtabelas_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridtabelas_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
            {
               subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
            }
            subGridtabelas_Backcolor = (int)(0x0);
         }
         else if ( subGridtabelas_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridtabelas_Backstyle = 1;
            if ( ((int)((nGXsfl_12_idx) % (2))) == 0 )
            {
               subGridtabelas_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
               {
                  subGridtabelas_Linesclass = subGridtabelas_Class+"Even";
               }
            }
            else
            {
               subGridtabelas_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
               {
                  subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
               }
            }
         }
         if ( GridtabelasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGridtabelas_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_12_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridtabelasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistema_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistema_Codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)12,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridtabelasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistema_Nome_Internalname,(String)A416Sistema_Nome,StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistema_Nome_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)570,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)12,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridtabelasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Multiple line edit */
         TempTags = " " + ((edtavDados_Enabled!=0)&&(edtavDados_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 15,'',false,'"+sGXsfl_12_idx+"',12)\"" : " ");
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         GridtabelasRow.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavDados_Internalname,(String)AV7Dados,(String)"",TempTags+((edtavDados_Enabled!=0)&&(edtavDados_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavDados_Enabled!=0)&&(edtavDados_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,15);\"" : " "),(short)1,(short)-1,(short)1,(short)0,(short)400,(String)"px",(short)50,(String)"px",(String)StyleString,(String)ClassString,(String)"",(String)"1000",(short)1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_CODIGO"+"_"+sGXsfl_12_idx, GetSecureSignedToken( sGXsfl_12_idx, context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_NOME"+"_"+sGXsfl_12_idx, GetSecureSignedToken( sGXsfl_12_idx, StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!"))));
         GridtabelasContainer.AddRow(GridtabelasRow);
         nGXsfl_12_idx = (short)(((subGridtabelas_Islastpage==1)&&(nGXsfl_12_idx+1>subGridtabelas_Recordsperpage( )) ? 1 : nGXsfl_12_idx+1));
         sGXsfl_12_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_12_idx), 4, 0)), 4, "0");
         SubsflControlProps_122( ) ;
         /* End function sendrow_122 */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtavFiltro_Internalname = "vFILTRO";
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO";
         edtSistema_Nome_Internalname = "SISTEMA_NOME";
         edtavDados_Internalname = "vDADOS";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavInseridas_Internalname = "vINSERIDAS";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavJacadastradas_Internalname = "vJACADASTRADAS";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         edtavNaoinseridas_Internalname = "vNAOINSERIDAS";
         tblTblresultado_Internalname = "TBLRESULTADO";
         bttGravar_Internalname = "GRAVAR";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
         subGridtabelas_Internalname = "GRIDTABELAS";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavDados_Visible = -1;
         edtavDados_Enabled = 1;
         edtSistema_Nome_Jsonclick = "";
         edtSistema_Codigo_Jsonclick = "";
         edtavNaoinseridas_Jsonclick = "";
         edtavJacadastradas_Jsonclick = "";
         edtavInseridas_Jsonclick = "";
         subGridtabelas_Allowcollapsing = 0;
         subGridtabelas_Allowselection = 0;
         subGridtabelas_Class = "WorkWith";
         edtavFiltro_Jsonclick = "";
         tblTblresultado_Visible = 1;
         subGridtabelas_Backcolorstyle = 0;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Copiar e Colar Modulos dos Sistemas";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'AV8Filtro',fld:'vFILTRO',pic:'@!',nv:''}],oparms:[]}");
         setEventMetadata("'GRAVAR'","{handler:'E11BD2',iparms:[{av:'AV7Dados',fld:'vDADOS',grid:12,pic:'',nv:''},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',grid:12,pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV5DadosColados',fld:'vDADOSCOLADOS',pic:'',nv:''},{av:'A143Modulo_Nome',fld:'MODULO_NOME',pic:'@!',nv:''},{av:'AV18Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25Udparg1',fld:'vUDPARG1',pic:'',nv:''},{av:'AV12JaCadastradas',fld:'vJACADASTRADAS',pic:'ZZZ9',nv:0},{av:'AV15Modulo_Nome',fld:'vMODULO_NOME',pic:'@!',nv:''},{av:'AV11Inseridas',fld:'vINSERIDAS',pic:'ZZZ9',nv:0},{av:'AV16NaoInseridas',fld:'vNAOINSERIDAS',pic:'ZZZ9',nv:0}],oparms:[{av:'AV11Inseridas',fld:'vINSERIDAS',pic:'ZZZ9',nv:0},{av:'AV12JaCadastradas',fld:'vJACADASTRADAS',pic:'ZZZ9',nv:0},{av:'AV16NaoInseridas',fld:'vNAOINSERIDAS',pic:'ZZZ9',nv:0},{av:'AV18Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5DadosColados',fld:'vDADOSCOLADOS',pic:'',nv:''},{av:'tblTblresultado_Visible',ctrl:'TBLRESULTADO',prop:'Visible'},{av:'AV15Modulo_Nome',fld:'vMODULO_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E12BD2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV8Filtro = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV5DadosColados = "";
         A143Modulo_Nome = "";
         AV25Udparg1 = "";
         AV15Modulo_Nome = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A416Sistema_Nome = "";
         AV7Dados = "";
         GridtabelasContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV8Filtro = "";
         H00BD2_A416Sistema_Nome = new String[] {""} ;
         H00BD2_A127Sistema_Codigo = new int[1] ;
         AV20WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9FiltroRecebido = "";
         AV19WebSession = context.GetSession();
         AV6Char = "";
         H00BD3_A146Modulo_Codigo = new int[1] ;
         H00BD3_A127Sistema_Codigo = new int[1] ;
         H00BD3_A143Modulo_Nome = new String[] {""} ;
         AV13Modulo = new SdtModulo(context);
         GridtabelasRow = new GXWebRow();
         sStyleString = "";
         lblTextblock1_Jsonclick = "";
         TempTags = "";
         subGridtabelas_Linesclass = "";
         GridtabelasColumn = new GXWebColumn();
         bttGravar_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_copiarcolarmodulos__default(),
            new Object[][] {
                new Object[] {
               H00BD2_A416Sistema_Nome, H00BD2_A127Sistema_Codigo
               }
               , new Object[] {
               H00BD3_A146Modulo_Codigo, H00BD3_A127Sistema_Codigo, H00BD3_A143Modulo_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_12 ;
      private short nGXsfl_12_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_12_Refreshing=0 ;
      private short subGridtabelas_Backcolorstyle ;
      private short AV11Inseridas ;
      private short AV12JaCadastradas ;
      private short AV16NaoInseridas ;
      private short nGXsfl_12_fel_idx=1 ;
      private short AV10i ;
      private short AV17s ;
      private short AV24GXLvl66 ;
      private short subGridtabelas_Titlebackstyle ;
      private short subGridtabelas_Allowselection ;
      private short subGridtabelas_Allowhovering ;
      private short subGridtabelas_Allowcollapsing ;
      private short subGridtabelas_Collapsed ;
      private short nGXWrapped ;
      private short subGridtabelas_Backstyle ;
      private short GRIDTABELAS_nEOF ;
      private int AV18Sistema_Codigo ;
      private int A127Sistema_Codigo ;
      private int subGridtabelas_Islastpage ;
      private int tblTblresultado_Visible ;
      private int subGridtabelas_Titlebackcolor ;
      private int subGridtabelas_Allbackcolor ;
      private int subGridtabelas_Selectioncolor ;
      private int subGridtabelas_Hoveringcolor ;
      private int idxLst ;
      private int subGridtabelas_Backcolor ;
      private int edtavDados_Enabled ;
      private int edtavDados_Visible ;
      private long GRIDTABELAS_nCurrentRecord ;
      private long GRIDTABELAS_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_12_idx="0001" ;
      private String AV8Filtro ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A143Modulo_Nome ;
      private String AV15Modulo_Nome ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String ClassString ;
      private String StyleString ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtSistema_Codigo_Internalname ;
      private String edtSistema_Nome_Internalname ;
      private String edtavDados_Internalname ;
      private String edtavFiltro_Internalname ;
      private String scmdbuf ;
      private String lV8Filtro ;
      private String edtavInseridas_Internalname ;
      private String edtavJacadastradas_Internalname ;
      private String edtavNaoinseridas_Internalname ;
      private String tblTblresultado_Internalname ;
      private String AV9FiltroRecebido ;
      private String sGXsfl_12_fel_idx="0001" ;
      private String AV6Char ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String TempTags ;
      private String edtavFiltro_Jsonclick ;
      private String subGridtabelas_Internalname ;
      private String subGridtabelas_Class ;
      private String subGridtabelas_Linesclass ;
      private String bttGravar_Internalname ;
      private String bttGravar_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavInseridas_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String edtavJacadastradas_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String edtavNaoinseridas_Jsonclick ;
      private String ROClassString ;
      private String edtSistema_Codigo_Jsonclick ;
      private String edtSistema_Nome_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String AV5DadosColados ;
      private String AV25Udparg1 ;
      private String A416Sistema_Nome ;
      private String AV7Dados ;
      private IGxSession AV19WebSession ;
      private GXWebGrid GridtabelasContainer ;
      private GXWebRow GridtabelasRow ;
      private GXWebColumn GridtabelasColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] H00BD2_A416Sistema_Nome ;
      private int[] H00BD2_A127Sistema_Codigo ;
      private int[] H00BD3_A146Modulo_Codigo ;
      private int[] H00BD3_A127Sistema_Codigo ;
      private String[] H00BD3_A143Modulo_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private SdtModulo AV13Modulo ;
      private wwpbaseobjects.SdtWWPContext AV20WWPContext ;
   }

   public class wp_copiarcolarmodulos__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00BD2( IGxContext context ,
                                             String AV8Filtro ,
                                             String A416Sistema_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [1] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Sistema_Nome], [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8Filtro)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Sistema_Nome] like '%' + @lV8Filtro + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Sistema_Nome] like '%' + @lV8Filtro + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Sistema_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00BD2(context, (String)dynConstraints[0] , (String)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00BD3 ;
          prmH00BD3 = new Object[] {
          new Object[] {"@AV18Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BD2 ;
          prmH00BD2 = new Object[] {
          new Object[] {"@lV8Filtro",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00BD2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BD2,11,0,true,false )
             ,new CursorDef("H00BD3", "SELECT [Modulo_Codigo], [Sistema_Codigo], [Modulo_Nome] FROM [Modulo] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV18Sistema_Codigo ORDER BY [Modulo_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BD3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[1]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
