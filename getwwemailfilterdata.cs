/*
               File: GetWWEmailFilterData
        Description: Get WWEmail Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:46.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwemailfilterdata : GXProcedure
   {
      public getwwemailfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwemailfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwemailfilterdata objgetwwemailfilterdata;
         objgetwwemailfilterdata = new getwwemailfilterdata();
         objgetwwemailfilterdata.AV16DDOName = aP0_DDOName;
         objgetwwemailfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetwwemailfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetwwemailfilterdata.AV20OptionsJson = "" ;
         objgetwwemailfilterdata.AV23OptionsDescJson = "" ;
         objgetwwemailfilterdata.AV25OptionIndexesJson = "" ;
         objgetwwemailfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwemailfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwemailfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwemailfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_EMAIL_TITULO") == 0 )
         {
            /* Execute user subroutine: 'LOADEMAIL_TITULOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_EMAIL_KEY") == 0 )
         {
            /* Execute user subroutine: 'LOADEMAIL_KEYOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("WWEmailGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWEmailGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("WWEmailGridState"), "");
         }
         AV45GXV1 = 1;
         while ( AV45GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV45GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFEMAIL_TITULO") == 0 )
            {
               AV10TFEmail_Titulo = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFEMAIL_TITULO_SEL") == 0 )
            {
               AV11TFEmail_Titulo_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFEMAIL_KEY") == 0 )
            {
               AV12TFEmail_Key = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFEMAIL_KEY_SEL") == 0 )
            {
               AV13TFEmail_Key_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            AV45GXV1 = (int)(AV45GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "EMAIL_TITULO") == 0 )
            {
               AV33DynamicFiltersOperator1 = AV31GridStateDynamicFilter.gxTpr_Operator;
               AV34Email_Titulo1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV35DynamicFiltersEnabled2 = true;
               AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(2));
               AV36DynamicFiltersSelector2 = AV31GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "EMAIL_TITULO") == 0 )
               {
                  AV37DynamicFiltersOperator2 = AV31GridStateDynamicFilter.gxTpr_Operator;
                  AV38Email_Titulo2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV39DynamicFiltersEnabled3 = true;
                  AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(3));
                  AV40DynamicFiltersSelector3 = AV31GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "EMAIL_TITULO") == 0 )
                  {
                     AV41DynamicFiltersOperator3 = AV31GridStateDynamicFilter.gxTpr_Operator;
                     AV42Email_Titulo3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADEMAIL_TITULOOPTIONS' Routine */
         AV10TFEmail_Titulo = AV14SearchTxt;
         AV11TFEmail_Titulo_Sel = "";
         AV47WWEmailDS_1_Dynamicfiltersselector1 = AV32DynamicFiltersSelector1;
         AV48WWEmailDS_2_Dynamicfiltersoperator1 = AV33DynamicFiltersOperator1;
         AV49WWEmailDS_3_Email_titulo1 = AV34Email_Titulo1;
         AV50WWEmailDS_4_Dynamicfiltersenabled2 = AV35DynamicFiltersEnabled2;
         AV51WWEmailDS_5_Dynamicfiltersselector2 = AV36DynamicFiltersSelector2;
         AV52WWEmailDS_6_Dynamicfiltersoperator2 = AV37DynamicFiltersOperator2;
         AV53WWEmailDS_7_Email_titulo2 = AV38Email_Titulo2;
         AV54WWEmailDS_8_Dynamicfiltersenabled3 = AV39DynamicFiltersEnabled3;
         AV55WWEmailDS_9_Dynamicfiltersselector3 = AV40DynamicFiltersSelector3;
         AV56WWEmailDS_10_Dynamicfiltersoperator3 = AV41DynamicFiltersOperator3;
         AV57WWEmailDS_11_Email_titulo3 = AV42Email_Titulo3;
         AV58WWEmailDS_12_Tfemail_titulo = AV10TFEmail_Titulo;
         AV59WWEmailDS_13_Tfemail_titulo_sel = AV11TFEmail_Titulo_Sel;
         AV60WWEmailDS_14_Tfemail_key = AV12TFEmail_Key;
         AV61WWEmailDS_15_Tfemail_key_sel = AV13TFEmail_Key_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV47WWEmailDS_1_Dynamicfiltersselector1 ,
                                              AV48WWEmailDS_2_Dynamicfiltersoperator1 ,
                                              AV49WWEmailDS_3_Email_titulo1 ,
                                              AV50WWEmailDS_4_Dynamicfiltersenabled2 ,
                                              AV51WWEmailDS_5_Dynamicfiltersselector2 ,
                                              AV52WWEmailDS_6_Dynamicfiltersoperator2 ,
                                              AV53WWEmailDS_7_Email_titulo2 ,
                                              AV54WWEmailDS_8_Dynamicfiltersenabled3 ,
                                              AV55WWEmailDS_9_Dynamicfiltersselector3 ,
                                              AV56WWEmailDS_10_Dynamicfiltersoperator3 ,
                                              AV57WWEmailDS_11_Email_titulo3 ,
                                              AV59WWEmailDS_13_Tfemail_titulo_sel ,
                                              AV58WWEmailDS_12_Tfemail_titulo ,
                                              AV61WWEmailDS_15_Tfemail_key_sel ,
                                              AV60WWEmailDS_14_Tfemail_key ,
                                              A1669Email_Titulo ,
                                              A1672Email_Key },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV49WWEmailDS_3_Email_titulo1 = StringUtil.Concat( StringUtil.RTrim( AV49WWEmailDS_3_Email_titulo1), "%", "");
         lV49WWEmailDS_3_Email_titulo1 = StringUtil.Concat( StringUtil.RTrim( AV49WWEmailDS_3_Email_titulo1), "%", "");
         lV53WWEmailDS_7_Email_titulo2 = StringUtil.Concat( StringUtil.RTrim( AV53WWEmailDS_7_Email_titulo2), "%", "");
         lV53WWEmailDS_7_Email_titulo2 = StringUtil.Concat( StringUtil.RTrim( AV53WWEmailDS_7_Email_titulo2), "%", "");
         lV57WWEmailDS_11_Email_titulo3 = StringUtil.Concat( StringUtil.RTrim( AV57WWEmailDS_11_Email_titulo3), "%", "");
         lV57WWEmailDS_11_Email_titulo3 = StringUtil.Concat( StringUtil.RTrim( AV57WWEmailDS_11_Email_titulo3), "%", "");
         lV58WWEmailDS_12_Tfemail_titulo = StringUtil.Concat( StringUtil.RTrim( AV58WWEmailDS_12_Tfemail_titulo), "%", "");
         lV60WWEmailDS_14_Tfemail_key = StringUtil.Concat( StringUtil.RTrim( AV60WWEmailDS_14_Tfemail_key), "%", "");
         /* Using cursor P00SG2 */
         pr_default.execute(0, new Object[] {lV49WWEmailDS_3_Email_titulo1, lV49WWEmailDS_3_Email_titulo1, lV53WWEmailDS_7_Email_titulo2, lV53WWEmailDS_7_Email_titulo2, lV57WWEmailDS_11_Email_titulo3, lV57WWEmailDS_11_Email_titulo3, lV58WWEmailDS_12_Tfemail_titulo, AV59WWEmailDS_13_Tfemail_titulo_sel, lV60WWEmailDS_14_Tfemail_key, AV61WWEmailDS_15_Tfemail_key_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKSG2 = false;
            A1669Email_Titulo = P00SG2_A1669Email_Titulo[0];
            A1672Email_Key = P00SG2_A1672Email_Key[0];
            A1665Email_Guid = (Guid)((Guid)(P00SG2_A1665Email_Guid[0]));
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00SG2_A1669Email_Titulo[0], A1669Email_Titulo) == 0 ) )
            {
               BRKSG2 = false;
               A1665Email_Guid = (Guid)((Guid)(P00SG2_A1665Email_Guid[0]));
               AV26count = (long)(AV26count+1);
               BRKSG2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1669Email_Titulo)) )
            {
               AV18Option = A1669Email_Titulo;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSG2 )
            {
               BRKSG2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADEMAIL_KEYOPTIONS' Routine */
         AV12TFEmail_Key = AV14SearchTxt;
         AV13TFEmail_Key_Sel = "";
         AV47WWEmailDS_1_Dynamicfiltersselector1 = AV32DynamicFiltersSelector1;
         AV48WWEmailDS_2_Dynamicfiltersoperator1 = AV33DynamicFiltersOperator1;
         AV49WWEmailDS_3_Email_titulo1 = AV34Email_Titulo1;
         AV50WWEmailDS_4_Dynamicfiltersenabled2 = AV35DynamicFiltersEnabled2;
         AV51WWEmailDS_5_Dynamicfiltersselector2 = AV36DynamicFiltersSelector2;
         AV52WWEmailDS_6_Dynamicfiltersoperator2 = AV37DynamicFiltersOperator2;
         AV53WWEmailDS_7_Email_titulo2 = AV38Email_Titulo2;
         AV54WWEmailDS_8_Dynamicfiltersenabled3 = AV39DynamicFiltersEnabled3;
         AV55WWEmailDS_9_Dynamicfiltersselector3 = AV40DynamicFiltersSelector3;
         AV56WWEmailDS_10_Dynamicfiltersoperator3 = AV41DynamicFiltersOperator3;
         AV57WWEmailDS_11_Email_titulo3 = AV42Email_Titulo3;
         AV58WWEmailDS_12_Tfemail_titulo = AV10TFEmail_Titulo;
         AV59WWEmailDS_13_Tfemail_titulo_sel = AV11TFEmail_Titulo_Sel;
         AV60WWEmailDS_14_Tfemail_key = AV12TFEmail_Key;
         AV61WWEmailDS_15_Tfemail_key_sel = AV13TFEmail_Key_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV47WWEmailDS_1_Dynamicfiltersselector1 ,
                                              AV48WWEmailDS_2_Dynamicfiltersoperator1 ,
                                              AV49WWEmailDS_3_Email_titulo1 ,
                                              AV50WWEmailDS_4_Dynamicfiltersenabled2 ,
                                              AV51WWEmailDS_5_Dynamicfiltersselector2 ,
                                              AV52WWEmailDS_6_Dynamicfiltersoperator2 ,
                                              AV53WWEmailDS_7_Email_titulo2 ,
                                              AV54WWEmailDS_8_Dynamicfiltersenabled3 ,
                                              AV55WWEmailDS_9_Dynamicfiltersselector3 ,
                                              AV56WWEmailDS_10_Dynamicfiltersoperator3 ,
                                              AV57WWEmailDS_11_Email_titulo3 ,
                                              AV59WWEmailDS_13_Tfemail_titulo_sel ,
                                              AV58WWEmailDS_12_Tfemail_titulo ,
                                              AV61WWEmailDS_15_Tfemail_key_sel ,
                                              AV60WWEmailDS_14_Tfemail_key ,
                                              A1669Email_Titulo ,
                                              A1672Email_Key },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV49WWEmailDS_3_Email_titulo1 = StringUtil.Concat( StringUtil.RTrim( AV49WWEmailDS_3_Email_titulo1), "%", "");
         lV49WWEmailDS_3_Email_titulo1 = StringUtil.Concat( StringUtil.RTrim( AV49WWEmailDS_3_Email_titulo1), "%", "");
         lV53WWEmailDS_7_Email_titulo2 = StringUtil.Concat( StringUtil.RTrim( AV53WWEmailDS_7_Email_titulo2), "%", "");
         lV53WWEmailDS_7_Email_titulo2 = StringUtil.Concat( StringUtil.RTrim( AV53WWEmailDS_7_Email_titulo2), "%", "");
         lV57WWEmailDS_11_Email_titulo3 = StringUtil.Concat( StringUtil.RTrim( AV57WWEmailDS_11_Email_titulo3), "%", "");
         lV57WWEmailDS_11_Email_titulo3 = StringUtil.Concat( StringUtil.RTrim( AV57WWEmailDS_11_Email_titulo3), "%", "");
         lV58WWEmailDS_12_Tfemail_titulo = StringUtil.Concat( StringUtil.RTrim( AV58WWEmailDS_12_Tfemail_titulo), "%", "");
         lV60WWEmailDS_14_Tfemail_key = StringUtil.Concat( StringUtil.RTrim( AV60WWEmailDS_14_Tfemail_key), "%", "");
         /* Using cursor P00SG3 */
         pr_default.execute(1, new Object[] {lV49WWEmailDS_3_Email_titulo1, lV49WWEmailDS_3_Email_titulo1, lV53WWEmailDS_7_Email_titulo2, lV53WWEmailDS_7_Email_titulo2, lV57WWEmailDS_11_Email_titulo3, lV57WWEmailDS_11_Email_titulo3, lV58WWEmailDS_12_Tfemail_titulo, AV59WWEmailDS_13_Tfemail_titulo_sel, lV60WWEmailDS_14_Tfemail_key, AV61WWEmailDS_15_Tfemail_key_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKSG4 = false;
            A1672Email_Key = P00SG3_A1672Email_Key[0];
            A1669Email_Titulo = P00SG3_A1669Email_Titulo[0];
            A1665Email_Guid = (Guid)((Guid)(P00SG3_A1665Email_Guid[0]));
            AV26count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00SG3_A1672Email_Key[0], A1672Email_Key) == 0 ) )
            {
               BRKSG4 = false;
               A1665Email_Guid = (Guid)((Guid)(P00SG3_A1665Email_Guid[0]));
               AV26count = (long)(AV26count+1);
               BRKSG4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1672Email_Key)) )
            {
               AV18Option = A1672Email_Key;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSG4 )
            {
               BRKSG4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFEmail_Titulo = "";
         AV11TFEmail_Titulo_Sel = "";
         AV12TFEmail_Key = "";
         AV13TFEmail_Key_Sel = "";
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV34Email_Titulo1 = "";
         AV36DynamicFiltersSelector2 = "";
         AV38Email_Titulo2 = "";
         AV40DynamicFiltersSelector3 = "";
         AV42Email_Titulo3 = "";
         AV47WWEmailDS_1_Dynamicfiltersselector1 = "";
         AV49WWEmailDS_3_Email_titulo1 = "";
         AV51WWEmailDS_5_Dynamicfiltersselector2 = "";
         AV53WWEmailDS_7_Email_titulo2 = "";
         AV55WWEmailDS_9_Dynamicfiltersselector3 = "";
         AV57WWEmailDS_11_Email_titulo3 = "";
         AV58WWEmailDS_12_Tfemail_titulo = "";
         AV59WWEmailDS_13_Tfemail_titulo_sel = "";
         AV60WWEmailDS_14_Tfemail_key = "";
         AV61WWEmailDS_15_Tfemail_key_sel = "";
         scmdbuf = "";
         lV49WWEmailDS_3_Email_titulo1 = "";
         lV53WWEmailDS_7_Email_titulo2 = "";
         lV57WWEmailDS_11_Email_titulo3 = "";
         lV58WWEmailDS_12_Tfemail_titulo = "";
         lV60WWEmailDS_14_Tfemail_key = "";
         A1669Email_Titulo = "";
         A1672Email_Key = "";
         P00SG2_A1669Email_Titulo = new String[] {""} ;
         P00SG2_A1672Email_Key = new String[] {""} ;
         P00SG2_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         A1665Email_Guid = (Guid)(System.Guid.Empty);
         AV18Option = "";
         P00SG3_A1672Email_Key = new String[] {""} ;
         P00SG3_A1669Email_Titulo = new String[] {""} ;
         P00SG3_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwemailfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00SG2_A1669Email_Titulo, P00SG2_A1672Email_Key, P00SG2_A1665Email_Guid
               }
               , new Object[] {
               P00SG3_A1672Email_Key, P00SG3_A1669Email_Titulo, P00SG3_A1665Email_Guid
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV33DynamicFiltersOperator1 ;
      private short AV37DynamicFiltersOperator2 ;
      private short AV41DynamicFiltersOperator3 ;
      private short AV48WWEmailDS_2_Dynamicfiltersoperator1 ;
      private short AV52WWEmailDS_6_Dynamicfiltersoperator2 ;
      private short AV56WWEmailDS_10_Dynamicfiltersoperator3 ;
      private int AV45GXV1 ;
      private long AV26count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV35DynamicFiltersEnabled2 ;
      private bool AV39DynamicFiltersEnabled3 ;
      private bool AV50WWEmailDS_4_Dynamicfiltersenabled2 ;
      private bool AV54WWEmailDS_8_Dynamicfiltersenabled3 ;
      private bool BRKSG2 ;
      private bool BRKSG4 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV10TFEmail_Titulo ;
      private String AV11TFEmail_Titulo_Sel ;
      private String AV12TFEmail_Key ;
      private String AV13TFEmail_Key_Sel ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV34Email_Titulo1 ;
      private String AV36DynamicFiltersSelector2 ;
      private String AV38Email_Titulo2 ;
      private String AV40DynamicFiltersSelector3 ;
      private String AV42Email_Titulo3 ;
      private String AV47WWEmailDS_1_Dynamicfiltersselector1 ;
      private String AV49WWEmailDS_3_Email_titulo1 ;
      private String AV51WWEmailDS_5_Dynamicfiltersselector2 ;
      private String AV53WWEmailDS_7_Email_titulo2 ;
      private String AV55WWEmailDS_9_Dynamicfiltersselector3 ;
      private String AV57WWEmailDS_11_Email_titulo3 ;
      private String AV58WWEmailDS_12_Tfemail_titulo ;
      private String AV59WWEmailDS_13_Tfemail_titulo_sel ;
      private String AV60WWEmailDS_14_Tfemail_key ;
      private String AV61WWEmailDS_15_Tfemail_key_sel ;
      private String lV49WWEmailDS_3_Email_titulo1 ;
      private String lV53WWEmailDS_7_Email_titulo2 ;
      private String lV57WWEmailDS_11_Email_titulo3 ;
      private String lV58WWEmailDS_12_Tfemail_titulo ;
      private String lV60WWEmailDS_14_Tfemail_key ;
      private String A1669Email_Titulo ;
      private String A1672Email_Key ;
      private String AV18Option ;
      private Guid A1665Email_Guid ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00SG2_A1669Email_Titulo ;
      private String[] P00SG2_A1672Email_Key ;
      private Guid[] P00SG2_A1665Email_Guid ;
      private String[] P00SG3_A1672Email_Key ;
      private String[] P00SG3_A1669Email_Titulo ;
      private Guid[] P00SG3_A1665Email_Guid ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getwwemailfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00SG2( IGxContext context ,
                                             String AV47WWEmailDS_1_Dynamicfiltersselector1 ,
                                             short AV48WWEmailDS_2_Dynamicfiltersoperator1 ,
                                             String AV49WWEmailDS_3_Email_titulo1 ,
                                             bool AV50WWEmailDS_4_Dynamicfiltersenabled2 ,
                                             String AV51WWEmailDS_5_Dynamicfiltersselector2 ,
                                             short AV52WWEmailDS_6_Dynamicfiltersoperator2 ,
                                             String AV53WWEmailDS_7_Email_titulo2 ,
                                             bool AV54WWEmailDS_8_Dynamicfiltersenabled3 ,
                                             String AV55WWEmailDS_9_Dynamicfiltersselector3 ,
                                             short AV56WWEmailDS_10_Dynamicfiltersoperator3 ,
                                             String AV57WWEmailDS_11_Email_titulo3 ,
                                             String AV59WWEmailDS_13_Tfemail_titulo_sel ,
                                             String AV58WWEmailDS_12_Tfemail_titulo ,
                                             String AV61WWEmailDS_15_Tfemail_key_sel ,
                                             String AV60WWEmailDS_14_Tfemail_key ,
                                             String A1669Email_Titulo ,
                                             String A1672Email_Key )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [10] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Email_Titulo], [Email_Key], [Email_Guid] FROM [Email] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV47WWEmailDS_1_Dynamicfiltersselector1, "EMAIL_TITULO") == 0 ) && ( AV48WWEmailDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWEmailDS_3_Email_titulo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like @lV49WWEmailDS_3_Email_titulo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like @lV49WWEmailDS_3_Email_titulo1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWEmailDS_1_Dynamicfiltersselector1, "EMAIL_TITULO") == 0 ) && ( AV48WWEmailDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWEmailDS_3_Email_titulo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like '%' + @lV49WWEmailDS_3_Email_titulo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like '%' + @lV49WWEmailDS_3_Email_titulo1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV50WWEmailDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV51WWEmailDS_5_Dynamicfiltersselector2, "EMAIL_TITULO") == 0 ) && ( AV52WWEmailDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWEmailDS_7_Email_titulo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like @lV53WWEmailDS_7_Email_titulo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like @lV53WWEmailDS_7_Email_titulo2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV50WWEmailDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV51WWEmailDS_5_Dynamicfiltersselector2, "EMAIL_TITULO") == 0 ) && ( AV52WWEmailDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWEmailDS_7_Email_titulo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like '%' + @lV53WWEmailDS_7_Email_titulo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like '%' + @lV53WWEmailDS_7_Email_titulo2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV54WWEmailDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV55WWEmailDS_9_Dynamicfiltersselector3, "EMAIL_TITULO") == 0 ) && ( AV56WWEmailDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWEmailDS_11_Email_titulo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like @lV57WWEmailDS_11_Email_titulo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like @lV57WWEmailDS_11_Email_titulo3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV54WWEmailDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV55WWEmailDS_9_Dynamicfiltersselector3, "EMAIL_TITULO") == 0 ) && ( AV56WWEmailDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWEmailDS_11_Email_titulo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like '%' + @lV57WWEmailDS_11_Email_titulo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like '%' + @lV57WWEmailDS_11_Email_titulo3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59WWEmailDS_13_Tfemail_titulo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWEmailDS_12_Tfemail_titulo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like @lV58WWEmailDS_12_Tfemail_titulo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like @lV58WWEmailDS_12_Tfemail_titulo)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWEmailDS_13_Tfemail_titulo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] = @AV59WWEmailDS_13_Tfemail_titulo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] = @AV59WWEmailDS_13_Tfemail_titulo_sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWEmailDS_15_Tfemail_key_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWEmailDS_14_Tfemail_key)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Key] like @lV60WWEmailDS_14_Tfemail_key)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Key] like @lV60WWEmailDS_14_Tfemail_key)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWEmailDS_15_Tfemail_key_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Key] = @AV61WWEmailDS_15_Tfemail_key_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Key] = @AV61WWEmailDS_15_Tfemail_key_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Email_Titulo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00SG3( IGxContext context ,
                                             String AV47WWEmailDS_1_Dynamicfiltersselector1 ,
                                             short AV48WWEmailDS_2_Dynamicfiltersoperator1 ,
                                             String AV49WWEmailDS_3_Email_titulo1 ,
                                             bool AV50WWEmailDS_4_Dynamicfiltersenabled2 ,
                                             String AV51WWEmailDS_5_Dynamicfiltersselector2 ,
                                             short AV52WWEmailDS_6_Dynamicfiltersoperator2 ,
                                             String AV53WWEmailDS_7_Email_titulo2 ,
                                             bool AV54WWEmailDS_8_Dynamicfiltersenabled3 ,
                                             String AV55WWEmailDS_9_Dynamicfiltersselector3 ,
                                             short AV56WWEmailDS_10_Dynamicfiltersoperator3 ,
                                             String AV57WWEmailDS_11_Email_titulo3 ,
                                             String AV59WWEmailDS_13_Tfemail_titulo_sel ,
                                             String AV58WWEmailDS_12_Tfemail_titulo ,
                                             String AV61WWEmailDS_15_Tfemail_key_sel ,
                                             String AV60WWEmailDS_14_Tfemail_key ,
                                             String A1669Email_Titulo ,
                                             String A1672Email_Key )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [10] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [Email_Key], [Email_Titulo], [Email_Guid] FROM [Email] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV47WWEmailDS_1_Dynamicfiltersselector1, "EMAIL_TITULO") == 0 ) && ( AV48WWEmailDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWEmailDS_3_Email_titulo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like @lV49WWEmailDS_3_Email_titulo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like @lV49WWEmailDS_3_Email_titulo1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWEmailDS_1_Dynamicfiltersselector1, "EMAIL_TITULO") == 0 ) && ( AV48WWEmailDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWEmailDS_3_Email_titulo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like '%' + @lV49WWEmailDS_3_Email_titulo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like '%' + @lV49WWEmailDS_3_Email_titulo1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV50WWEmailDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV51WWEmailDS_5_Dynamicfiltersselector2, "EMAIL_TITULO") == 0 ) && ( AV52WWEmailDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWEmailDS_7_Email_titulo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like @lV53WWEmailDS_7_Email_titulo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like @lV53WWEmailDS_7_Email_titulo2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV50WWEmailDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV51WWEmailDS_5_Dynamicfiltersselector2, "EMAIL_TITULO") == 0 ) && ( AV52WWEmailDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWEmailDS_7_Email_titulo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like '%' + @lV53WWEmailDS_7_Email_titulo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like '%' + @lV53WWEmailDS_7_Email_titulo2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV54WWEmailDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV55WWEmailDS_9_Dynamicfiltersselector3, "EMAIL_TITULO") == 0 ) && ( AV56WWEmailDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWEmailDS_11_Email_titulo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like @lV57WWEmailDS_11_Email_titulo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like @lV57WWEmailDS_11_Email_titulo3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV54WWEmailDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV55WWEmailDS_9_Dynamicfiltersselector3, "EMAIL_TITULO") == 0 ) && ( AV56WWEmailDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWEmailDS_11_Email_titulo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like '%' + @lV57WWEmailDS_11_Email_titulo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like '%' + @lV57WWEmailDS_11_Email_titulo3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59WWEmailDS_13_Tfemail_titulo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWEmailDS_12_Tfemail_titulo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like @lV58WWEmailDS_12_Tfemail_titulo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like @lV58WWEmailDS_12_Tfemail_titulo)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWEmailDS_13_Tfemail_titulo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] = @AV59WWEmailDS_13_Tfemail_titulo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] = @AV59WWEmailDS_13_Tfemail_titulo_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWEmailDS_15_Tfemail_key_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWEmailDS_14_Tfemail_key)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Key] like @lV60WWEmailDS_14_Tfemail_key)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Key] like @lV60WWEmailDS_14_Tfemail_key)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWEmailDS_15_Tfemail_key_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Key] = @AV61WWEmailDS_15_Tfemail_key_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Key] = @AV61WWEmailDS_15_Tfemail_key_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Email_Key]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00SG2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] );
               case 1 :
                     return conditional_P00SG3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00SG2 ;
          prmP00SG2 = new Object[] {
          new Object[] {"@lV49WWEmailDS_3_Email_titulo1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV49WWEmailDS_3_Email_titulo1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV53WWEmailDS_7_Email_titulo2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV53WWEmailDS_7_Email_titulo2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV57WWEmailDS_11_Email_titulo3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV57WWEmailDS_11_Email_titulo3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV58WWEmailDS_12_Tfemail_titulo",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV59WWEmailDS_13_Tfemail_titulo_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV60WWEmailDS_14_Tfemail_key",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV61WWEmailDS_15_Tfemail_key_sel",SqlDbType.VarChar,40,0}
          } ;
          Object[] prmP00SG3 ;
          prmP00SG3 = new Object[] {
          new Object[] {"@lV49WWEmailDS_3_Email_titulo1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV49WWEmailDS_3_Email_titulo1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV53WWEmailDS_7_Email_titulo2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV53WWEmailDS_7_Email_titulo2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV57WWEmailDS_11_Email_titulo3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV57WWEmailDS_11_Email_titulo3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV58WWEmailDS_12_Tfemail_titulo",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV59WWEmailDS_13_Tfemail_titulo_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV60WWEmailDS_14_Tfemail_key",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV61WWEmailDS_15_Tfemail_key_sel",SqlDbType.VarChar,40,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00SG2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SG2,100,0,true,false )
             ,new CursorDef("P00SG3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SG3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((Guid[]) buf[2])[0] = rslt.getGuid(3) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((Guid[]) buf[2])[0] = rslt.getGuid(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwemailfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwemailfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwemailfilterdata") )
          {
             return  ;
          }
          getwwemailfilterdata worker = new getwwemailfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
