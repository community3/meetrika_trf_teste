/*
               File: PRC_UPDContagemResultado_Deflator
        Description: UPD Contagem Resultado_Deflator
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:50:38.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updcontagemresultado_deflator : GXProcedure
   {
      public prc_updcontagemresultado_deflator( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updcontagemresultado_deflator( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_Codigo ,
                           ref int aP1_ContagemResultado_Codigo ,
                           ref int aP2_ContagemResultado_Servico )
      {
         this.AV9Contratada_Codigo = aP0_Contratada_Codigo;
         this.A456ContagemResultado_Codigo = aP1_ContagemResultado_Codigo;
         this.A601ContagemResultado_Servico = aP2_ContagemResultado_Servico;
         initialize();
         executePrivate();
         aP1_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP2_ContagemResultado_Servico=this.A601ContagemResultado_Servico;
      }

      public int executeUdp( int aP0_Contratada_Codigo ,
                             ref int aP1_ContagemResultado_Codigo )
      {
         this.AV9Contratada_Codigo = aP0_Contratada_Codigo;
         this.A456ContagemResultado_Codigo = aP1_ContagemResultado_Codigo;
         this.A601ContagemResultado_Servico = aP2_ContagemResultado_Servico;
         initialize();
         executePrivate();
         aP1_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP2_ContagemResultado_Servico=this.A601ContagemResultado_Servico;
         return A601ContagemResultado_Servico ;
      }

      public void executeSubmit( int aP0_Contratada_Codigo ,
                                 ref int aP1_ContagemResultado_Codigo ,
                                 ref int aP2_ContagemResultado_Servico )
      {
         prc_updcontagemresultado_deflator objprc_updcontagemresultado_deflator;
         objprc_updcontagemresultado_deflator = new prc_updcontagemresultado_deflator();
         objprc_updcontagemresultado_deflator.AV9Contratada_Codigo = aP0_Contratada_Codigo;
         objprc_updcontagemresultado_deflator.A456ContagemResultado_Codigo = aP1_ContagemResultado_Codigo;
         objprc_updcontagemresultado_deflator.A601ContagemResultado_Servico = aP2_ContagemResultado_Servico;
         objprc_updcontagemresultado_deflator.context.SetSubmitInitialConfig(context);
         objprc_updcontagemresultado_deflator.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updcontagemresultado_deflator);
         aP1_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP2_ContagemResultado_Servico=this.A601ContagemResultado_Servico;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updcontagemresultado_deflator)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV14GXLvl2 = 0;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV9Contratada_Codigo ,
                                              A39Contratada_Codigo ,
                                              A516Contratada_TipoFabrica ,
                                              A601ContagemResultado_Servico ,
                                              A155Servico_Codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor P005R2 */
         pr_default.execute(0, new Object[] {A601ContagemResultado_Servico});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A74Contrato_Codigo = P005R2_A74Contrato_Codigo[0];
            A155Servico_Codigo = P005R2_A155Servico_Codigo[0];
            A558Servico_Percentual = P005R2_A558Servico_Percentual[0];
            n558Servico_Percentual = P005R2_n558Servico_Percentual[0];
            A160ContratoServicos_Codigo = P005R2_A160ContratoServicos_Codigo[0];
            /* Using cursor P005R3 */
            pr_default.execute(1, new Object[] {A74Contrato_Codigo});
            A39Contratada_Codigo = P005R3_A39Contratada_Codigo[0];
            pr_default.close(1);
            /* Using cursor P005R4 */
            pr_default.execute(2, new Object[] {A39Contratada_Codigo});
            A516Contratada_TipoFabrica = P005R4_A516Contratada_TipoFabrica[0];
            pr_default.close(2);
            AV14GXLvl2 = 1;
            AV11Servico_Percentual = A558Servico_Percentual;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         pr_default.close(1);
         pr_default.close(2);
         if ( AV14GXLvl2 == 0 )
         {
            AV15GXLvl9 = 0;
            /* Using cursor P005R5 */
            pr_default.execute(3, new Object[] {A601ContagemResultado_Servico});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A74Contrato_Codigo = P005R5_A74Contrato_Codigo[0];
               A155Servico_Codigo = P005R5_A155Servico_Codigo[0];
               A558Servico_Percentual = P005R5_A558Servico_Percentual[0];
               n558Servico_Percentual = P005R5_n558Servico_Percentual[0];
               A160ContratoServicos_Codigo = P005R5_A160ContratoServicos_Codigo[0];
               /* Using cursor P005R6 */
               pr_default.execute(4, new Object[] {A74Contrato_Codigo});
               A39Contratada_Codigo = P005R6_A39Contratada_Codigo[0];
               pr_default.close(4);
               /* Using cursor P005R7 */
               pr_default.execute(5, new Object[] {A39Contratada_Codigo});
               A516Contratada_TipoFabrica = P005R7_A516Contratada_TipoFabrica[0];
               pr_default.close(5);
               if ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "M") == 0 )
               {
                  AV15GXLvl9 = 1;
                  AV11Servico_Percentual = A558Servico_Percentual;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(3);
            }
            pr_default.close(3);
            pr_default.close(4);
            pr_default.close(5);
            if ( AV15GXLvl9 == 0 )
            {
               AV11Servico_Percentual = (decimal)(1);
            }
         }
         /* Using cursor P005R8 */
         pr_default.execute(6, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A800ContagemResultado_Deflator = P005R8_A800ContagemResultado_Deflator[0];
            n800ContagemResultado_Deflator = P005R8_n800ContagemResultado_Deflator[0];
            A511ContagemResultado_HoraCnt = P005R8_A511ContagemResultado_HoraCnt[0];
            A473ContagemResultado_DataCnt = P005R8_A473ContagemResultado_DataCnt[0];
            A800ContagemResultado_Deflator = AV11Servico_Percentual;
            n800ContagemResultado_Deflator = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P005R9 */
            pr_default.execute(7, new Object[] {n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(7);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            if (true) break;
            /* Using cursor P005R10 */
            pr_default.execute(8, new Object[] {n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(8);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            pr_default.readNext(6);
         }
         pr_default.close(6);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         A516Contratada_TipoFabrica = "";
         P005R2_A74Contrato_Codigo = new int[1] ;
         P005R2_A155Servico_Codigo = new int[1] ;
         P005R2_A558Servico_Percentual = new decimal[1] ;
         P005R2_n558Servico_Percentual = new bool[] {false} ;
         P005R2_A160ContratoServicos_Codigo = new int[1] ;
         P005R3_A39Contratada_Codigo = new int[1] ;
         P005R4_A516Contratada_TipoFabrica = new String[] {""} ;
         P005R5_A74Contrato_Codigo = new int[1] ;
         P005R5_A155Servico_Codigo = new int[1] ;
         P005R5_A558Servico_Percentual = new decimal[1] ;
         P005R5_n558Servico_Percentual = new bool[] {false} ;
         P005R5_A160ContratoServicos_Codigo = new int[1] ;
         P005R6_A39Contratada_Codigo = new int[1] ;
         P005R7_A516Contratada_TipoFabrica = new String[] {""} ;
         P005R8_A456ContagemResultado_Codigo = new int[1] ;
         P005R8_A800ContagemResultado_Deflator = new decimal[1] ;
         P005R8_n800ContagemResultado_Deflator = new bool[] {false} ;
         P005R8_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P005R8_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         A511ContagemResultado_HoraCnt = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updcontagemresultado_deflator__default(),
            new Object[][] {
                new Object[] {
               P005R2_A74Contrato_Codigo, P005R2_A155Servico_Codigo, P005R2_A558Servico_Percentual, P005R2_n558Servico_Percentual, P005R2_A160ContratoServicos_Codigo
               }
               , new Object[] {
               P005R3_A39Contratada_Codigo
               }
               , new Object[] {
               P005R4_A516Contratada_TipoFabrica
               }
               , new Object[] {
               P005R5_A74Contrato_Codigo, P005R5_A155Servico_Codigo, P005R5_A558Servico_Percentual, P005R5_n558Servico_Percentual, P005R5_A160ContratoServicos_Codigo
               }
               , new Object[] {
               P005R6_A39Contratada_Codigo
               }
               , new Object[] {
               P005R7_A516Contratada_TipoFabrica
               }
               , new Object[] {
               P005R8_A456ContagemResultado_Codigo, P005R8_A800ContagemResultado_Deflator, P005R8_n800ContagemResultado_Deflator, P005R8_A511ContagemResultado_HoraCnt, P005R8_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14GXLvl2 ;
      private short AV15GXLvl9 ;
      private int AV9Contratada_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A601ContagemResultado_Servico ;
      private int A39Contratada_Codigo ;
      private int A155Servico_Codigo ;
      private int A74Contrato_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private decimal A558Servico_Percentual ;
      private decimal AV11Servico_Percentual ;
      private decimal A800ContagemResultado_Deflator ;
      private String scmdbuf ;
      private String A516Contratada_TipoFabrica ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool n558Servico_Percentual ;
      private bool n800ContagemResultado_Deflator ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP1_ContagemResultado_Codigo ;
      private int aP2_ContagemResultado_Servico ;
      private IDataStoreProvider pr_default ;
      private int[] P005R2_A74Contrato_Codigo ;
      private int[] P005R2_A155Servico_Codigo ;
      private decimal[] P005R2_A558Servico_Percentual ;
      private bool[] P005R2_n558Servico_Percentual ;
      private int[] P005R2_A160ContratoServicos_Codigo ;
      private int[] P005R3_A39Contratada_Codigo ;
      private String[] P005R4_A516Contratada_TipoFabrica ;
      private int[] P005R5_A74Contrato_Codigo ;
      private int[] P005R5_A155Servico_Codigo ;
      private decimal[] P005R5_A558Servico_Percentual ;
      private bool[] P005R5_n558Servico_Percentual ;
      private int[] P005R5_A160ContratoServicos_Codigo ;
      private int[] P005R6_A39Contratada_Codigo ;
      private String[] P005R7_A516Contratada_TipoFabrica ;
      private int[] P005R8_A456ContagemResultado_Codigo ;
      private decimal[] P005R8_A800ContagemResultado_Deflator ;
      private bool[] P005R8_n800ContagemResultado_Deflator ;
      private String[] P005R8_A511ContagemResultado_HoraCnt ;
      private DateTime[] P005R8_A473ContagemResultado_DataCnt ;
   }

   public class prc_updcontagemresultado_deflator__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P005R2( IGxContext context ,
                                             int AV9Contratada_Codigo ,
                                             int A39Contratada_Codigo ,
                                             String A516Contratada_TipoFabrica ,
                                             int A601ContagemResultado_Servico ,
                                             int A155Servico_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [1] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 [Contrato_Codigo], [Servico_Codigo], [Servico_Percentual], [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Servico_Codigo] = @ContagemResultado_Servico)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Servico_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P005R2(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005R3 ;
          prmP005R3 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005R4 ;
          prmP005R4 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005R5 ;
          prmP005R5 = new Object[] {
          new Object[] {"@ContagemResultado_Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005R6 ;
          prmP005R6 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005R7 ;
          prmP005R7 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005R8 ;
          prmP005R8 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005R9 ;
          prmP005R9 = new Object[] {
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP005R10 ;
          prmP005R10 = new Object[] {
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP005R2 ;
          prmP005R2 = new Object[] {
          new Object[] {"@ContagemResultado_Servico",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005R2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005R2,1,0,true,true )
             ,new CursorDef("P005R3", "SELECT [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005R3,1,0,true,false )
             ,new CursorDef("P005R4", "SELECT [Contratada_TipoFabrica] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005R4,1,0,true,false )
             ,new CursorDef("P005R5", "SELECT TOP 1 [Contrato_Codigo], [Servico_Codigo], [Servico_Percentual], [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContagemResultado_Servico ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005R5,1,0,true,true )
             ,new CursorDef("P005R6", "SELECT [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005R6,1,0,true,false )
             ,new CursorDef("P005R7", "SELECT [Contratada_TipoFabrica] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005R7,1,0,true,false )
             ,new CursorDef("P005R8", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_Deflator], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] DESC, [ContagemResultado_DataCnt] DESC, [ContagemResultado_HoraCnt] DESC ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005R8,1,0,true,true )
             ,new CursorDef("P005R9", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Deflator]=@ContagemResultado_Deflator  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005R9)
             ,new CursorDef("P005R10", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Deflator]=@ContagemResultado_Deflator  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005R10)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 5) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (DateTime)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (DateTime)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                return;
       }
    }

 }

}
