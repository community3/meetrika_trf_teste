/*
               File: WP_AcompanhamentoFinanceiro
        Description: Acompanhamento Financeiro
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:26:2.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_acompanhamentofinanceiro : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_acompanhamentofinanceiro( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_acompanhamentofinanceiro( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavFiltrocontagemresultado_statusdmn = new GXCombobox();
         cmbavContagemresultado_statusdmn = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_35 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_35_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_35_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n490ContagemResultado_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
               A484ContagemResultado_StatusDmn = GetNextPar( );
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV17Contratadas);
               A803ContagemResultado_ContratadaSigla = GetNextPar( );
               n803ContagemResultado_ContratadaSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A803ContagemResultado_ContratadaSigla", A803ContagemResultado_ContratadaSigla);
               A601ContagemResultado_Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n601ContagemResultado_Servico = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A601ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(A601ContagemResultado_Servico), 6, 0)));
               A801ContagemResultado_ServicoSigla = GetNextPar( );
               n801ContagemResultado_ServicoSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A801ContagemResultado_ServicoSigla", A801ContagemResultado_ServicoSigla);
               A606ContagemResultado_ValorFinal = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A606ContagemResultado_ValorFinal", StringUtil.LTrim( StringUtil.Str( A606ContagemResultado_ValorFinal, 18, 5)));
               AV14ContagemResultado_GlsValor = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_GlsValor", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_GlsValor, 12, 2)));
               A512ContagemResultado_ValorPF = NumberUtil.Val( GetNextPar( ), ".");
               n512ContagemResultado_ValorPF = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A512ContagemResultado_ValorPF", StringUtil.LTrim( StringUtil.Str( A512ContagemResultado_ValorPF, 18, 5)));
               A1051ContagemResultado_GlsValor = NumberUtil.Val( GetNextPar( ), ".");
               n1051ContagemResultado_GlsValor = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1051ContagemResultado_GlsValor", StringUtil.LTrim( StringUtil.Str( A1051ContagemResultado_GlsValor, 12, 2)));
               A1602ContagemResultado_GlsIndValor = NumberUtil.Val( GetNextPar( ), ".");
               n1602ContagemResultado_GlsIndValor = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1602ContagemResultado_GlsIndValor", StringUtil.LTrim( StringUtil.Str( A1602ContagemResultado_GlsIndValor, 18, 5)));
               A574ContagemResultado_PFFinal = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
               AV11ContagemResultado_PFBFM = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV11ContagemResultado_PFBFM, 14, 5)));
               AV12ContagemResultado_PFLFM = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV12ContagemResultado_PFLFM, 14, 5)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV16WWPContext);
               A1446ContratoGestor_ContratadaAreaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1446ContratoGestor_ContratadaAreaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
               A1079ContratoGestor_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
               A1136ContratoGestor_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1136ContratoGestor_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1136ContratoGestor_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0)));
               AV6ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contratadacod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0)));
               AV8ContagemResultado_StatusDmn = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavContagemresultado_statusdmn_Internalname, AV8ContagemResultado_StatusDmn);
               A460ContagemResultado_PFBFM = NumberUtil.Val( GetNextPar( ), ".");
               n460ContagemResultado_PFBFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A460ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)));
               A461ContagemResultado_PFLFM = NumberUtil.Val( GetNextPar( ), ".");
               n461ContagemResultado_PFLFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A461ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( A461ContagemResultado_PFLFM, 14, 5)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, A490ContagemResultado_ContratadaCod, A484ContagemResultado_StatusDmn, AV17Contratadas, A803ContagemResultado_ContratadaSigla, A601ContagemResultado_Servico, A801ContagemResultado_ServicoSigla, A606ContagemResultado_ValorFinal, AV14ContagemResultado_GlsValor, A512ContagemResultado_ValorPF, A1051ContagemResultado_GlsValor, A1602ContagemResultado_GlsIndValor, A574ContagemResultado_PFFinal, AV11ContagemResultado_PFBFM, AV12ContagemResultado_PFLFM, AV16WWPContext, A1446ContratoGestor_ContratadaAreaCod, A1079ContratoGestor_UsuarioCod, A1136ContratoGestor_ContratadaCod, AV6ContagemResultado_ContratadaCod, AV8ContagemResultado_StatusDmn, A460ContagemResultado_PFBFM, A461ContagemResultado_PFLFM) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAMW2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTMW2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020621626220");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_acompanhamentofinanceiro.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_35", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_35), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADAS", AV17Contratadas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADAS", AV17Contratadas);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADASIGLA", StringUtil.RTrim( A803ContagemResultado_ContratadaSigla));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICOSIGLA", StringUtil.RTrim( A801ContagemResultado_ServicoSigla));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_GLSVALOR", StringUtil.LTrim( StringUtil.NToC( AV14ContagemResultado_GlsValor, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_GLSVALOR", StringUtil.LTrim( StringUtil.NToC( A1051ContagemResultado_GlsValor, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_GLSINDVALOR", StringUtil.LTrim( StringUtil.NToC( A1602ContagemResultado_GlsIndValor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_PFBFM", StringUtil.LTrim( StringUtil.NToC( AV11ContagemResultado_PFBFM, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_PFLFM", StringUtil.LTrim( StringUtil.NToC( AV12ContagemResultado_PFLFM, 14, 5, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV16WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV16WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATADAAREACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFM", StringUtil.LTrim( StringUtil.NToC( A460ContagemResultado_PFBFM, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFLFM", StringUtil.LTrim( StringUtil.NToC( A461ContagemResultado_PFLFM, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFFINAL", StringUtil.LTrim( StringUtil.NToC( A574ContagemResultado_PFFinal, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_VALORPF", StringUtil.LTrim( StringUtil.NToC( A512ContagemResultado_ValorPF, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_VALORFINAL", StringUtil.LTrim( StringUtil.NToC( A606ContagemResultado_ValorFinal, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "vWWPCONTEXT_Contratada_codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16WWPContext.gxTpr_Contratada_codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEMW2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTMW2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_acompanhamentofinanceiro.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_AcompanhamentoFinanceiro" ;
      }

      public override String GetPgmdesc( )
      {
         return "Acompanhamento Financeiro" ;
      }

      protected void WBMW0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_MW2( true) ;
         }
         else
         {
            wb_table1_2_MW2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MW2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTMW2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Acompanhamento Financeiro", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPMW0( ) ;
      }

      protected void WSMW2( )
      {
         STARTMW2( ) ;
         EVTMW2( ) ;
      }

      protected void EVTMW2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11MW2 */
                              E11MW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_35_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
                              SubsflControlProps_352( ) ;
                              AV6ContagemResultado_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_contratadacod_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contratadacod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0)));
                              AV7ContagemResultado_ContratadaSigla = StringUtil.Upper( cgiGet( edtavContagemresultado_contratadasigla_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contratadasigla_Internalname, AV7ContagemResultado_ContratadaSigla);
                              cmbavContagemresultado_statusdmn.Name = cmbavContagemresultado_statusdmn_Internalname;
                              cmbavContagemresultado_statusdmn.CurrentValue = cgiGet( cmbavContagemresultado_statusdmn_Internalname);
                              AV8ContagemResultado_StatusDmn = cgiGet( cmbavContagemresultado_statusdmn_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavContagemresultado_statusdmn_Internalname, AV8ContagemResultado_StatusDmn);
                              AV9ContagemResultado_Servico = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_servico_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_servico_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_Servico), 6, 0)));
                              AV10ContagemResultado_ServicoSigla = StringUtil.Upper( cgiGet( edtavContagemresultado_servicosigla_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_servicosigla_Internalname, AV10ContagemResultado_ServicoSigla);
                              AV22TotalContagemResultado_PFBFM = context.localUtil.CToN( cgiGet( edtavTotalcontagemresultado_pfbfm_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_pfbfm_Internalname, StringUtil.LTrim( StringUtil.Str( AV22TotalContagemResultado_PFBFM, 14, 5)));
                              AV24TotalContagemResultado_PFLFM = context.localUtil.CToN( cgiGet( edtavTotalcontagemresultado_pflfm_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_pflfm_Internalname, StringUtil.LTrim( StringUtil.Str( AV24TotalContagemResultado_PFLFM, 14, 5)));
                              AV27TotalContagemResultado_PFPagar = context.localUtil.CToN( cgiGet( edtavTotalcontagemresultado_pfpagar_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_pfpagar_Internalname, StringUtil.LTrim( StringUtil.Str( AV27TotalContagemResultado_PFPagar, 18, 5)));
                              AV26TotalContagemResultado_ValorPF = context.localUtil.CToN( cgiGet( edtavTotalcontagemresultado_valorpf_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_valorpf_Internalname, StringUtil.LTrim( StringUtil.Str( AV26TotalContagemResultado_ValorPF, 18, 5)));
                              AV25TotalContagemResultado_GlsValor = context.localUtil.CToN( cgiGet( edtavTotalcontagemresultado_glsvalor_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_glsvalor_Internalname, StringUtil.LTrim( StringUtil.Str( AV25TotalContagemResultado_GlsValor, 12, 2)));
                              AV23TotalContagemResultado_PFFinal = context.localUtil.CToN( cgiGet( edtavTotalcontagemresultado_pffinal_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_pffinal_Internalname, StringUtil.LTrim( StringUtil.Str( AV23TotalContagemResultado_PFFinal, 14, 5)));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E12MW2 */
                                    E12MW2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13MW2 */
                                    E13MW2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14MW2 */
                                    E14MW2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEMW2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAMW2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavFiltrocontagemresultado_statusdmn.Name = "vFILTROCONTAGEMRESULTADO_STATUSDMN";
            cmbavFiltrocontagemresultado_statusdmn.WebTags = "";
            cmbavFiltrocontagemresultado_statusdmn.addItem("B", "Stand by", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("S", "Solicitada", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("E", "Em An�lise", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("A", "Em execu��o", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("R", "Resolvida", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("C", "Conferida", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("D", "Retornada", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("H", "Homologada", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("O", "Aceite", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("P", "A Pagar", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("L", "Liquidada", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("X", "Cancelada", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("N", "N�o Faturada", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("J", "Planejamento", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("I", "An�lise Planejamento", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("T", "Validacao T�cnica", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("Q", "Validacao Qualidade", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("G", "Em Homologa��o", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbavFiltrocontagemresultado_statusdmn.addItem("U", "Rascunho", 0);
            if ( cmbavFiltrocontagemresultado_statusdmn.ItemCount > 0 )
            {
               AV19filtroContagemResultado_StatusDmn = cmbavFiltrocontagemresultado_statusdmn.getValidValue(AV19filtroContagemResultado_StatusDmn);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19filtroContagemResultado_StatusDmn", AV19filtroContagemResultado_StatusDmn);
            }
            GXCCtl = "vCONTAGEMRESULTADO_STATUSDMN_" + sGXsfl_35_idx;
            cmbavContagemresultado_statusdmn.Name = GXCCtl;
            cmbavContagemresultado_statusdmn.WebTags = "";
            cmbavContagemresultado_statusdmn.addItem("B", "Stand by", 0);
            cmbavContagemresultado_statusdmn.addItem("S", "Solicitada", 0);
            cmbavContagemresultado_statusdmn.addItem("E", "Em An�lise", 0);
            cmbavContagemresultado_statusdmn.addItem("A", "Em execu��o", 0);
            cmbavContagemresultado_statusdmn.addItem("R", "Resolvida", 0);
            cmbavContagemresultado_statusdmn.addItem("C", "Conferida", 0);
            cmbavContagemresultado_statusdmn.addItem("D", "Retornada", 0);
            cmbavContagemresultado_statusdmn.addItem("H", "Homologada", 0);
            cmbavContagemresultado_statusdmn.addItem("O", "Aceite", 0);
            cmbavContagemresultado_statusdmn.addItem("P", "A Pagar", 0);
            cmbavContagemresultado_statusdmn.addItem("L", "Liquidada", 0);
            cmbavContagemresultado_statusdmn.addItem("X", "Cancelada", 0);
            cmbavContagemresultado_statusdmn.addItem("N", "N�o Faturada", 0);
            cmbavContagemresultado_statusdmn.addItem("J", "Planejamento", 0);
            cmbavContagemresultado_statusdmn.addItem("I", "An�lise Planejamento", 0);
            cmbavContagemresultado_statusdmn.addItem("T", "Validacao T�cnica", 0);
            cmbavContagemresultado_statusdmn.addItem("Q", "Validacao Qualidade", 0);
            cmbavContagemresultado_statusdmn.addItem("G", "Em Homologa��o", 0);
            cmbavContagemresultado_statusdmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbavContagemresultado_statusdmn.addItem("U", "Rascunho", 0);
            if ( cmbavContagemresultado_statusdmn.ItemCount > 0 )
            {
               AV8ContagemResultado_StatusDmn = cmbavContagemresultado_statusdmn.getValidValue(AV8ContagemResultado_StatusDmn);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavContagemresultado_statusdmn_Internalname, AV8ContagemResultado_StatusDmn);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavFiltrocontagemresultado_statusdmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_352( ) ;
         while ( nGXsfl_35_idx <= nRC_GXsfl_35 )
         {
            sendrow_352( ) ;
            nGXsfl_35_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_35_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_35_idx+1));
            sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
            SubsflControlProps_352( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       int A490ContagemResultado_ContratadaCod ,
                                       String A484ContagemResultado_StatusDmn ,
                                       IGxCollection AV17Contratadas ,
                                       String A803ContagemResultado_ContratadaSigla ,
                                       int A601ContagemResultado_Servico ,
                                       String A801ContagemResultado_ServicoSigla ,
                                       decimal A606ContagemResultado_ValorFinal ,
                                       decimal AV14ContagemResultado_GlsValor ,
                                       decimal A512ContagemResultado_ValorPF ,
                                       decimal A1051ContagemResultado_GlsValor ,
                                       decimal A1602ContagemResultado_GlsIndValor ,
                                       decimal A574ContagemResultado_PFFinal ,
                                       decimal AV11ContagemResultado_PFBFM ,
                                       decimal AV12ContagemResultado_PFLFM ,
                                       wwpbaseobjects.SdtWWPContext AV16WWPContext ,
                                       int A1446ContratoGestor_ContratadaAreaCod ,
                                       int A1079ContratoGestor_UsuarioCod ,
                                       int A1136ContratoGestor_ContratadaCod ,
                                       int AV6ContagemResultado_ContratadaCod ,
                                       String AV8ContagemResultado_StatusDmn ,
                                       decimal A460ContagemResultado_PFBFM ,
                                       decimal A461ContagemResultado_PFLFM )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFMW2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavFiltrocontagemresultado_statusdmn.ItemCount > 0 )
         {
            AV19filtroContagemResultado_StatusDmn = cmbavFiltrocontagemresultado_statusdmn.getValidValue(AV19filtroContagemResultado_StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19filtroContagemResultado_StatusDmn", AV19filtroContagemResultado_StatusDmn);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFMW2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_contratadacod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contratadacod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contratadacod_Enabled), 5, 0)));
         edtavContagemresultado_contratadasigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contratadasigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contratadasigla_Enabled), 5, 0)));
         cmbavContagemresultado_statusdmn.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn.Enabled), 5, 0)));
         edtavContagemresultado_servico_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_servico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_servico_Enabled), 5, 0)));
         edtavContagemresultado_servicosigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_servicosigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_servicosigla_Enabled), 5, 0)));
         edtavTotalcontagemresultado_pfbfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalcontagemresultado_pfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalcontagemresultado_pfbfm_Enabled), 5, 0)));
         edtavTotalcontagemresultado_pflfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalcontagemresultado_pflfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalcontagemresultado_pflfm_Enabled), 5, 0)));
         edtavTotalcontagemresultado_pfpagar_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalcontagemresultado_pfpagar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalcontagemresultado_pfpagar_Enabled), 5, 0)));
         edtavTotalcontagemresultado_valorpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalcontagemresultado_valorpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalcontagemresultado_valorpf_Enabled), 5, 0)));
         edtavTotalcontagemresultado_glsvalor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalcontagemresultado_glsvalor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalcontagemresultado_glsvalor_Enabled), 5, 0)));
         edtavTotalcontagemresultado_pffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalcontagemresultado_pffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalcontagemresultado_pffinal_Enabled), 5, 0)));
      }

      protected void RFMW2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 35;
         /* Execute user event: E13MW2 */
         E13MW2 ();
         nGXsfl_35_idx = 1;
         sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
         SubsflControlProps_352( ) ;
         nGXsfl_35_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_352( ) ;
            /* Execute user event: E14MW2 */
            E14MW2 ();
            if ( ( GRID_nCurrentRecord > 0 ) && ( GRID_nGridOutOfScope == 0 ) && ( nGXsfl_35_idx == 1 ) )
            {
               GRID_nCurrentRecord = 0;
               GRID_nGridOutOfScope = 1;
               subgrid_firstpage( ) ;
               /* Execute user event: E14MW2 */
               E14MW2 ();
            }
            wbEnd = 35;
            WBMW0( ) ;
         }
         nGXsfl_35_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A490ContagemResultado_ContratadaCod, A484ContagemResultado_StatusDmn, AV17Contratadas, A803ContagemResultado_ContratadaSigla, A601ContagemResultado_Servico, A801ContagemResultado_ServicoSigla, A606ContagemResultado_ValorFinal, AV14ContagemResultado_GlsValor, A512ContagemResultado_ValorPF, A1051ContagemResultado_GlsValor, A1602ContagemResultado_GlsIndValor, A574ContagemResultado_PFFinal, AV11ContagemResultado_PFBFM, AV12ContagemResultado_PFLFM, AV16WWPContext, A1446ContratoGestor_ContratadaAreaCod, A1079ContratoGestor_UsuarioCod, A1136ContratoGestor_ContratadaCod, AV6ContagemResultado_ContratadaCod, AV8ContagemResultado_StatusDmn, A460ContagemResultado_PFBFM, A461ContagemResultado_PFLFM) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A490ContagemResultado_ContratadaCod, A484ContagemResultado_StatusDmn, AV17Contratadas, A803ContagemResultado_ContratadaSigla, A601ContagemResultado_Servico, A801ContagemResultado_ServicoSigla, A606ContagemResultado_ValorFinal, AV14ContagemResultado_GlsValor, A512ContagemResultado_ValorPF, A1051ContagemResultado_GlsValor, A1602ContagemResultado_GlsIndValor, A574ContagemResultado_PFFinal, AV11ContagemResultado_PFBFM, AV12ContagemResultado_PFLFM, AV16WWPContext, A1446ContratoGestor_ContratadaAreaCod, A1079ContratoGestor_UsuarioCod, A1136ContratoGestor_ContratadaCod, AV6ContagemResultado_ContratadaCod, AV8ContagemResultado_StatusDmn, A460ContagemResultado_PFBFM, A461ContagemResultado_PFLFM) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A490ContagemResultado_ContratadaCod, A484ContagemResultado_StatusDmn, AV17Contratadas, A803ContagemResultado_ContratadaSigla, A601ContagemResultado_Servico, A801ContagemResultado_ServicoSigla, A606ContagemResultado_ValorFinal, AV14ContagemResultado_GlsValor, A512ContagemResultado_ValorPF, A1051ContagemResultado_GlsValor, A1602ContagemResultado_GlsIndValor, A574ContagemResultado_PFFinal, AV11ContagemResultado_PFBFM, AV12ContagemResultado_PFLFM, AV16WWPContext, A1446ContratoGestor_ContratadaAreaCod, A1079ContratoGestor_UsuarioCod, A1136ContratoGestor_ContratadaCod, AV6ContagemResultado_ContratadaCod, AV8ContagemResultado_StatusDmn, A460ContagemResultado_PFBFM, A461ContagemResultado_PFLFM) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A490ContagemResultado_ContratadaCod, A484ContagemResultado_StatusDmn, AV17Contratadas, A803ContagemResultado_ContratadaSigla, A601ContagemResultado_Servico, A801ContagemResultado_ServicoSigla, A606ContagemResultado_ValorFinal, AV14ContagemResultado_GlsValor, A512ContagemResultado_ValorPF, A1051ContagemResultado_GlsValor, A1602ContagemResultado_GlsIndValor, A574ContagemResultado_PFFinal, AV11ContagemResultado_PFBFM, AV12ContagemResultado_PFLFM, AV16WWPContext, A1446ContratoGestor_ContratadaAreaCod, A1079ContratoGestor_UsuarioCod, A1136ContratoGestor_ContratadaCod, AV6ContagemResultado_ContratadaCod, AV8ContagemResultado_StatusDmn, A460ContagemResultado_PFBFM, A461ContagemResultado_PFLFM) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A490ContagemResultado_ContratadaCod, A484ContagemResultado_StatusDmn, AV17Contratadas, A803ContagemResultado_ContratadaSigla, A601ContagemResultado_Servico, A801ContagemResultado_ServicoSigla, A606ContagemResultado_ValorFinal, AV14ContagemResultado_GlsValor, A512ContagemResultado_ValorPF, A1051ContagemResultado_GlsValor, A1602ContagemResultado_GlsIndValor, A574ContagemResultado_PFFinal, AV11ContagemResultado_PFBFM, AV12ContagemResultado_PFLFM, AV16WWPContext, A1446ContratoGestor_ContratadaAreaCod, A1079ContratoGestor_UsuarioCod, A1136ContratoGestor_ContratadaCod, AV6ContagemResultado_ContratadaCod, AV8ContagemResultado_StatusDmn, A460ContagemResultado_PFBFM, A461ContagemResultado_PFLFM) ;
         }
         return (int)(0) ;
      }

      protected void STRUPMW0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_contratadacod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contratadacod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contratadacod_Enabled), 5, 0)));
         edtavContagemresultado_contratadasigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contratadasigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contratadasigla_Enabled), 5, 0)));
         cmbavContagemresultado_statusdmn.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn.Enabled), 5, 0)));
         edtavContagemresultado_servico_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_servico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_servico_Enabled), 5, 0)));
         edtavContagemresultado_servicosigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_servicosigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_servicosigla_Enabled), 5, 0)));
         edtavTotalcontagemresultado_pfbfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalcontagemresultado_pfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalcontagemresultado_pfbfm_Enabled), 5, 0)));
         edtavTotalcontagemresultado_pflfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalcontagemresultado_pflfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalcontagemresultado_pflfm_Enabled), 5, 0)));
         edtavTotalcontagemresultado_pfpagar_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalcontagemresultado_pfpagar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalcontagemresultado_pfpagar_Enabled), 5, 0)));
         edtavTotalcontagemresultado_valorpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalcontagemresultado_valorpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalcontagemresultado_valorpf_Enabled), 5, 0)));
         edtavTotalcontagemresultado_glsvalor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalcontagemresultado_glsvalor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalcontagemresultado_glsvalor_Enabled), 5, 0)));
         edtavTotalcontagemresultado_pffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalcontagemresultado_pffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalcontagemresultado_pffinal_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12MW2 */
         E12MW2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavFiltrocontagemresultado_statusdmn.Name = cmbavFiltrocontagemresultado_statusdmn_Internalname;
            cmbavFiltrocontagemresultado_statusdmn.CurrentValue = cgiGet( cmbavFiltrocontagemresultado_statusdmn_Internalname);
            AV19filtroContagemResultado_StatusDmn = cgiGet( cmbavFiltrocontagemresultado_statusdmn_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19filtroContagemResultado_StatusDmn", AV19filtroContagemResultado_StatusDmn);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavFiltrocontagemresultado_servico_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFiltrocontagemresultado_servico_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFILTROCONTAGEMRESULTADO_SERVICO");
               GX_FocusControl = edtavFiltrocontagemresultado_servico_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20filtroContagemResultado_Servico = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20filtroContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20filtroContagemResultado_Servico), 6, 0)));
            }
            else
            {
               AV20filtroContagemResultado_Servico = (int)(context.localUtil.CToN( cgiGet( edtavFiltrocontagemresultado_servico_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20filtroContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20filtroContagemResultado_Servico), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavGridcurrentpage_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavGridcurrentpage_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vGRIDCURRENTPAGE");
               GX_FocusControl = edtavGridcurrentpage_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29GridCurrentPage = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GridCurrentPage), 10, 0)));
            }
            else
            {
               AV29GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( edtavGridcurrentpage_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GridCurrentPage), 10, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_35 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_35"), ",", "."));
            AV30GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            nGXsfl_35_idx = (short)(context.localUtil.CToN( cgiGet( subGrid_Internalname+"_ROW"), ",", "."));
            sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
            SubsflControlProps_352( ) ;
            if ( nGXsfl_35_idx > 0 )
            {
               AV6ContagemResultado_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_contratadacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contratadacod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0)));
               AV7ContagemResultado_ContratadaSigla = StringUtil.Upper( cgiGet( edtavContagemresultado_contratadasigla_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contratadasigla_Internalname, AV7ContagemResultado_ContratadaSigla);
               cmbavContagemresultado_statusdmn.Name = cmbavContagemresultado_statusdmn_Internalname;
               cmbavContagemresultado_statusdmn.CurrentValue = cgiGet( cmbavContagemresultado_statusdmn_Internalname);
               AV8ContagemResultado_StatusDmn = cgiGet( cmbavContagemresultado_statusdmn_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavContagemresultado_statusdmn_Internalname, AV8ContagemResultado_StatusDmn);
               AV9ContagemResultado_Servico = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_servico_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_servico_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_Servico), 6, 0)));
               AV10ContagemResultado_ServicoSigla = StringUtil.Upper( cgiGet( edtavContagemresultado_servicosigla_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_servicosigla_Internalname, AV10ContagemResultado_ServicoSigla);
               AV22TotalContagemResultado_PFBFM = context.localUtil.CToN( cgiGet( edtavTotalcontagemresultado_pfbfm_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_pfbfm_Internalname, StringUtil.LTrim( StringUtil.Str( AV22TotalContagemResultado_PFBFM, 14, 5)));
               AV24TotalContagemResultado_PFLFM = context.localUtil.CToN( cgiGet( edtavTotalcontagemresultado_pflfm_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_pflfm_Internalname, StringUtil.LTrim( StringUtil.Str( AV24TotalContagemResultado_PFLFM, 14, 5)));
               AV27TotalContagemResultado_PFPagar = context.localUtil.CToN( cgiGet( edtavTotalcontagemresultado_pfpagar_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_pfpagar_Internalname, StringUtil.LTrim( StringUtil.Str( AV27TotalContagemResultado_PFPagar, 18, 5)));
               AV26TotalContagemResultado_ValorPF = context.localUtil.CToN( cgiGet( edtavTotalcontagemresultado_valorpf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_valorpf_Internalname, StringUtil.LTrim( StringUtil.Str( AV26TotalContagemResultado_ValorPF, 18, 5)));
               AV25TotalContagemResultado_GlsValor = context.localUtil.CToN( cgiGet( edtavTotalcontagemresultado_glsvalor_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_glsvalor_Internalname, StringUtil.LTrim( StringUtil.Str( AV25TotalContagemResultado_GlsValor, 12, 2)));
               AV23TotalContagemResultado_PFFinal = context.localUtil.CToN( cgiGet( edtavTotalcontagemresultado_pffinal_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_pffinal_Internalname, StringUtil.LTrim( StringUtil.Str( AV23TotalContagemResultado_PFFinal, 14, 5)));
            }
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12MW2 */
         E12MW2 ();
         if (returnInSub) return;
      }

      protected void E12MW2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV29GridCurrentPage = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GridCurrentPage), 10, 0)));
         edtavGridcurrentpage_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGridcurrentpage_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGridcurrentpage_Visible), 5, 0)));
         AV30GridPageCount = -1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30GridPageCount), 10, 0)));
      }

      protected void E13MW2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV5Context) ;
      }

      private void E14MW2( )
      {
         /* Grid_Load Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV16WWPContext) ;
         /* Execute user subroutine: 'ADDCONTRATADAS' */
         S112 ();
         if (returnInSub) return;
         AV22TotalContagemResultado_PFBFM = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_pfbfm_Internalname, StringUtil.LTrim( StringUtil.Str( AV22TotalContagemResultado_PFBFM, 14, 5)));
         AV24TotalContagemResultado_PFLFM = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_pflfm_Internalname, StringUtil.LTrim( StringUtil.Str( AV24TotalContagemResultado_PFLFM, 14, 5)));
         AV27TotalContagemResultado_PFPagar = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_pfpagar_Internalname, StringUtil.LTrim( StringUtil.Str( AV27TotalContagemResultado_PFPagar, 18, 5)));
         AV26TotalContagemResultado_ValorPF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_valorpf_Internalname, StringUtil.LTrim( StringUtil.Str( AV26TotalContagemResultado_ValorPF, 18, 5)));
         AV25TotalContagemResultado_GlsValor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_glsvalor_Internalname, StringUtil.LTrim( StringUtil.Str( AV25TotalContagemResultado_GlsValor, 12, 2)));
         AV23TotalContagemResultado_PFFinal = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_pffinal_Internalname, StringUtil.LTrim( StringUtil.Str( AV23TotalContagemResultado_PFFinal, 14, 5)));
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV17Contratadas },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00MW3 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKMW3 = false;
            A1553ContagemResultado_CntSrvCod = H00MW3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00MW3_n1553ContagemResultado_CntSrvCod[0];
            A484ContagemResultado_StatusDmn = H00MW3_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00MW3_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = H00MW3_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00MW3_n490ContagemResultado_ContratadaCod[0];
            A1051ContagemResultado_GlsValor = H00MW3_A1051ContagemResultado_GlsValor[0];
            n1051ContagemResultado_GlsValor = H00MW3_n1051ContagemResultado_GlsValor[0];
            A803ContagemResultado_ContratadaSigla = H00MW3_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = H00MW3_n803ContagemResultado_ContratadaSigla[0];
            A601ContagemResultado_Servico = H00MW3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00MW3_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = H00MW3_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = H00MW3_n801ContagemResultado_ServicoSigla[0];
            A1602ContagemResultado_GlsIndValor = H00MW3_A1602ContagemResultado_GlsIndValor[0];
            n1602ContagemResultado_GlsIndValor = H00MW3_n1602ContagemResultado_GlsIndValor[0];
            A456ContagemResultado_Codigo = H00MW3_A456ContagemResultado_Codigo[0];
            A512ContagemResultado_ValorPF = H00MW3_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = H00MW3_n512ContagemResultado_ValorPF[0];
            A601ContagemResultado_Servico = H00MW3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00MW3_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = H00MW3_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = H00MW3_n801ContagemResultado_ServicoSigla[0];
            A803ContagemResultado_ContratadaSigla = H00MW3_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = H00MW3_n803ContagemResultado_ContratadaSigla[0];
            A1602ContagemResultado_GlsIndValor = H00MW3_A1602ContagemResultado_GlsIndValor[0];
            n1602ContagemResultado_GlsIndValor = H00MW3_n1602ContagemResultado_GlsIndValor[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A574ContagemResultado_PFFinal = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A606ContagemResultado_ValorFinal", StringUtil.LTrim( StringUtil.Str( A606ContagemResultado_ValorFinal, 18, 5)));
            AV6ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contratadacod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0)));
            AV7ContagemResultado_ContratadaSigla = A803ContagemResultado_ContratadaSigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contratadasigla_Internalname, AV7ContagemResultado_ContratadaSigla);
            AV9ContagemResultado_Servico = A601ContagemResultado_Servico;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_servico_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_Servico), 6, 0)));
            AV10ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_servicosigla_Internalname, AV10ContagemResultado_ServicoSigla);
            AV8ContagemResultado_StatusDmn = A484ContagemResultado_StatusDmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavContagemresultado_statusdmn_Internalname, AV8ContagemResultado_StatusDmn);
            while ( (pr_default.getStatus(0) != 101) && ( H00MW3_A490ContagemResultado_ContratadaCod[0] == A490ContagemResultado_ContratadaCod ) && ( StringUtil.StrCmp(H00MW3_A484ContagemResultado_StatusDmn[0], A484ContagemResultado_StatusDmn) == 0 ) )
            {
               BRKMW3 = false;
               A1553ContagemResultado_CntSrvCod = H00MW3_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00MW3_n1553ContagemResultado_CntSrvCod[0];
               A1051ContagemResultado_GlsValor = H00MW3_A1051ContagemResultado_GlsValor[0];
               n1051ContagemResultado_GlsValor = H00MW3_n1051ContagemResultado_GlsValor[0];
               A1602ContagemResultado_GlsIndValor = H00MW3_A1602ContagemResultado_GlsIndValor[0];
               n1602ContagemResultado_GlsIndValor = H00MW3_n1602ContagemResultado_GlsIndValor[0];
               A456ContagemResultado_Codigo = H00MW3_A456ContagemResultado_Codigo[0];
               A512ContagemResultado_ValorPF = H00MW3_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = H00MW3_n512ContagemResultado_ValorPF[0];
               A1602ContagemResultado_GlsIndValor = H00MW3_A1602ContagemResultado_GlsIndValor[0];
               n1602ContagemResultado_GlsIndValor = H00MW3_n1602ContagemResultado_GlsIndValor[0];
               if ( A490ContagemResultado_ContratadaCod == AV6ContagemResultado_ContratadaCod )
               {
                  if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, AV8ContagemResultado_StatusDmn) == 0 )
                  {
                     if ( (AV17Contratadas.IndexOf(A490ContagemResultado_ContratadaCod)>0) )
                     {
                        GXt_decimal1 = A574ContagemResultado_PFFinal;
                        new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                        A574ContagemResultado_PFFinal = GXt_decimal1;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
                        A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A606ContagemResultado_ValorFinal", StringUtil.LTrim( StringUtil.Str( A606ContagemResultado_ValorFinal, 18, 5)));
                        /* Execute user subroutine: 'BUSCADADOS' */
                        S124 ();
                        if ( returnInSub )
                        {
                           pr_default.close(0);
                           returnInSub = true;
                           if (true) return;
                        }
                        AV15ContagemResultado_PFPagar = (decimal)(A606ContagemResultado_ValorFinal-AV14ContagemResultado_GlsValor);
                        AV21ContagemResultado_ValorPF = A512ContagemResultado_ValorPF;
                        AV14ContagemResultado_GlsValor = (decimal)(A1051ContagemResultado_GlsValor+A1602ContagemResultado_GlsIndValor);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_GlsValor", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_GlsValor, 12, 2)));
                        AV13ContagemResultado_PFFinal = A574ContagemResultado_PFFinal;
                        AV22TotalContagemResultado_PFBFM = (decimal)(AV22TotalContagemResultado_PFBFM+AV11ContagemResultado_PFBFM);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_pfbfm_Internalname, StringUtil.LTrim( StringUtil.Str( AV22TotalContagemResultado_PFBFM, 14, 5)));
                        AV24TotalContagemResultado_PFLFM = (decimal)(AV24TotalContagemResultado_PFLFM+AV12ContagemResultado_PFLFM);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_pflfm_Internalname, StringUtil.LTrim( StringUtil.Str( AV24TotalContagemResultado_PFLFM, 14, 5)));
                        AV27TotalContagemResultado_PFPagar = (decimal)(AV27TotalContagemResultado_PFPagar+AV15ContagemResultado_PFPagar);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_pfpagar_Internalname, StringUtil.LTrim( StringUtil.Str( AV27TotalContagemResultado_PFPagar, 18, 5)));
                        AV26TotalContagemResultado_ValorPF = (decimal)(AV26TotalContagemResultado_ValorPF+AV21ContagemResultado_ValorPF);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_valorpf_Internalname, StringUtil.LTrim( StringUtil.Str( AV26TotalContagemResultado_ValorPF, 18, 5)));
                        AV25TotalContagemResultado_GlsValor = (decimal)(AV25TotalContagemResultado_GlsValor+AV14ContagemResultado_GlsValor);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_glsvalor_Internalname, StringUtil.LTrim( StringUtil.Str( AV25TotalContagemResultado_GlsValor, 12, 2)));
                        AV23TotalContagemResultado_PFFinal = (decimal)(AV23TotalContagemResultado_PFFinal+AV13ContagemResultado_PFFinal);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotalcontagemresultado_pffinal_Internalname, StringUtil.LTrim( StringUtil.Str( AV23TotalContagemResultado_PFFinal, 14, 5)));
                     }
                  }
               }
               BRKMW3 = true;
               pr_default.readNext(0);
            }
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 35;
            }
            if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
            {
               sendrow_352( ) ;
               GRID_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
               if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
               {
                  GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
               }
            }
            if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
            {
               GRID_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            }
            GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_35_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(35, GridRow);
            }
            if ( ! BRKMW3 )
            {
               BRKMW3 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV16WWPContext", AV16WWPContext);
         cmbavContagemresultado_statusdmn.CurrentValue = StringUtil.RTrim( AV8ContagemResultado_StatusDmn);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV17Contratadas", AV17Contratadas);
      }

      protected void E11MW2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            AV29GridCurrentPage = (long)(AV29GridCurrentPage-1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GridCurrentPage), 10, 0)));
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            AV29GridCurrentPage = (long)(AV29GridCurrentPage+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GridCurrentPage), 10, 0)));
            subgrid_nextpage( ) ;
         }
         else
         {
            AV28PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            AV29GridCurrentPage = AV28PageToGo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GridCurrentPage), 10, 0)));
            subgrid_gotopage( AV28PageToGo) ;
         }
         context.DoAjaxRefresh();
      }

      protected void S112( )
      {
         /* 'ADDCONTRATADAS' Routine */
         AV17Contratadas.Clear();
         if ( AV16WWPContext.gxTpr_Userehcontratada )
         {
            AV17Contratadas.Add(AV16WWPContext.gxTpr_Contratada_codigo, 0);
         }
         else if ( AV16WWPContext.gxTpr_Userehcontratante )
         {
            /* Using cursor H00MW4 */
            pr_default.execute(1, new Object[] {AV16WWPContext.gxTpr_Userid, AV16WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1078ContratoGestor_ContratoCod = H00MW4_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = H00MW4_A1079ContratoGestor_UsuarioCod[0];
               A1446ContratoGestor_ContratadaAreaCod = H00MW4_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = H00MW4_n1446ContratoGestor_ContratadaAreaCod[0];
               A1136ContratoGestor_ContratadaCod = H00MW4_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = H00MW4_n1136ContratoGestor_ContratadaCod[0];
               A1446ContratoGestor_ContratadaAreaCod = H00MW4_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = H00MW4_n1446ContratoGestor_ContratadaAreaCod[0];
               A1136ContratoGestor_ContratadaCod = H00MW4_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = H00MW4_n1136ContratoGestor_ContratadaCod[0];
               AV17Contratadas.Add(A1136ContratoGestor_ContratadaCod, 0);
               pr_default.readNext(1);
            }
            pr_default.close(1);
         }
         AV18WebSession.Set("Contratadas", AV17Contratadas.ToXml(false, true, "Collection", ""));
      }

      protected void S124( )
      {
         /* 'BUSCADADOS' Routine */
         /* Optimized group. */
         /* Using cursor H00MW5 */
         pr_default.execute(2, new Object[] {AV6ContagemResultado_ContratadaCod, AV8ContagemResultado_StatusDmn});
         c460ContagemResultado_PFBFM = H00MW5_A460ContagemResultado_PFBFM[0];
         n460ContagemResultado_PFBFM = H00MW5_n460ContagemResultado_PFBFM[0];
         c461ContagemResultado_PFLFM = H00MW5_A461ContagemResultado_PFLFM[0];
         n461ContagemResultado_PFLFM = H00MW5_n461ContagemResultado_PFLFM[0];
         pr_default.close(2);
         AV11ContagemResultado_PFBFM = (decimal)(AV11ContagemResultado_PFBFM+c460ContagemResultado_PFBFM);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV11ContagemResultado_PFBFM, 14, 5)));
         AV12ContagemResultado_PFLFM = (decimal)(AV12ContagemResultado_PFLFM+c461ContagemResultado_PFLFM);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV12ContagemResultado_PFLFM, 14, 5)));
         /* End optimized group. */
      }

      protected void wb_table1_2_MW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_MW2( true) ;
         }
         else
         {
            wb_table2_8_MW2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_MW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_29_MW2( true) ;
         }
         else
         {
            wb_table3_29_MW2( false) ;
         }
         return  ;
      }

      protected void wb_table3_29_MW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MW2e( true) ;
         }
         else
         {
            wb_table1_2_MW2e( false) ;
         }
      }

      protected void wb_table3_29_MW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_32_MW2( true) ;
         }
         else
         {
            wb_table4_32_MW2( false) ;
         }
         return  ;
      }

      protected void wb_table4_32_MW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_29_MW2e( true) ;
         }
         else
         {
            wb_table3_29_MW2e( false) ;
         }
      }

      protected void wb_table4_32_MW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"35\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Contratada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Prestadora") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Status") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultado_contratadacod_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV7ContagemResultado_ContratadaSigla));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultado_contratadasigla_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV8ContagemResultado_StatusDmn));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavContagemresultado_statusdmn.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9ContagemResultado_Servico), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultado_servico_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV10ContagemResultado_ServicoSigla));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultado_servicosigla_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV22TotalContagemResultado_PFBFM, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavTotalcontagemresultado_pfbfm_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV24TotalContagemResultado_PFLFM, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavTotalcontagemresultado_pflfm_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV27TotalContagemResultado_PFPagar, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavTotalcontagemresultado_pfpagar_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV26TotalContagemResultado_ValorPF, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavTotalcontagemresultado_valorpf_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV25TotalContagemResultado_GlsValor, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavTotalcontagemresultado_glsvalor_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV23TotalContagemResultado_PFFinal, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavTotalcontagemresultado_pffinal_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 35 )
         {
            wbEnd = 0;
            nRC_GXsfl_35 = (short)(nGXsfl_35_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGridcurrentpage_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29GridCurrentPage), 10, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV29GridCurrentPage), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGridcurrentpage_Jsonclick, 0, "Attribute", "", "", "", edtavGridcurrentpage_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_AcompanhamentoFinanceiro.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_32_MW2e( true) ;
         }
         else
         {
            wb_table4_32_MW2e( false) ;
         }
      }

      protected void wb_table2_8_MW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktitle_Internalname, "Acompanhamento Financeiro", "", "", lblTextblocktitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_AcompanhamentoFinanceiro.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table5_13_MW2( true) ;
         }
         else
         {
            wb_table5_13_MW2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_MW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_17_MW2( true) ;
         }
         else
         {
            wb_table6_17_MW2( false) ;
         }
         return  ;
      }

      protected void wb_table6_17_MW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_MW2e( true) ;
         }
         else
         {
            wb_table2_8_MW2e( false) ;
         }
      }

      protected void wb_table6_17_MW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfiltrocontagemresultado_statusdmn_Internalname, "Status", "", "", lblTextblockfiltrocontagemresultado_statusdmn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_AcompanhamentoFinanceiro.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'" + sGXsfl_35_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFiltrocontagemresultado_statusdmn, cmbavFiltrocontagemresultado_statusdmn_Internalname, StringUtil.RTrim( AV19filtroContagemResultado_StatusDmn), 1, cmbavFiltrocontagemresultado_statusdmn_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "", true, "HLP_WP_AcompanhamentoFinanceiro.htm");
            cmbavFiltrocontagemresultado_statusdmn.CurrentValue = StringUtil.RTrim( AV19filtroContagemResultado_StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFiltrocontagemresultado_statusdmn_Internalname, "Values", (String)(cmbavFiltrocontagemresultado_statusdmn.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfiltrocontagemresultado_servico_Internalname, "Servi�o", "", "", lblTextblockfiltrocontagemresultado_servico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_AcompanhamentoFinanceiro.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFiltrocontagemresultado_servico_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20filtroContagemResultado_Servico), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV20filtroContagemResultado_Servico), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFiltrocontagemresultado_servico_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_AcompanhamentoFinanceiro.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_17_MW2e( true) ;
         }
         else
         {
            wb_table6_17_MW2e( false) ;
         }
      }

      protected void wb_table5_13_MW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_MW2e( true) ;
         }
         else
         {
            wb_table5_13_MW2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMW2( ) ;
         WSMW2( ) ;
         WEMW2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020621626345");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("wp_acompanhamentofinanceiro.js", "?2020621626346");
            context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_352( )
      {
         edtavContagemresultado_contratadacod_Internalname = "vCONTAGEMRESULTADO_CONTRATADACOD_"+sGXsfl_35_idx;
         edtavContagemresultado_contratadasigla_Internalname = "vCONTAGEMRESULTADO_CONTRATADASIGLA_"+sGXsfl_35_idx;
         cmbavContagemresultado_statusdmn_Internalname = "vCONTAGEMRESULTADO_STATUSDMN_"+sGXsfl_35_idx;
         edtavContagemresultado_servico_Internalname = "vCONTAGEMRESULTADO_SERVICO_"+sGXsfl_35_idx;
         edtavContagemresultado_servicosigla_Internalname = "vCONTAGEMRESULTADO_SERVICOSIGLA_"+sGXsfl_35_idx;
         edtavTotalcontagemresultado_pfbfm_Internalname = "vTOTALCONTAGEMRESULTADO_PFBFM_"+sGXsfl_35_idx;
         edtavTotalcontagemresultado_pflfm_Internalname = "vTOTALCONTAGEMRESULTADO_PFLFM_"+sGXsfl_35_idx;
         edtavTotalcontagemresultado_pfpagar_Internalname = "vTOTALCONTAGEMRESULTADO_PFPAGAR_"+sGXsfl_35_idx;
         edtavTotalcontagemresultado_valorpf_Internalname = "vTOTALCONTAGEMRESULTADO_VALORPF_"+sGXsfl_35_idx;
         edtavTotalcontagemresultado_glsvalor_Internalname = "vTOTALCONTAGEMRESULTADO_GLSVALOR_"+sGXsfl_35_idx;
         edtavTotalcontagemresultado_pffinal_Internalname = "vTOTALCONTAGEMRESULTADO_PFFINAL_"+sGXsfl_35_idx;
      }

      protected void SubsflControlProps_fel_352( )
      {
         edtavContagemresultado_contratadacod_Internalname = "vCONTAGEMRESULTADO_CONTRATADACOD_"+sGXsfl_35_fel_idx;
         edtavContagemresultado_contratadasigla_Internalname = "vCONTAGEMRESULTADO_CONTRATADASIGLA_"+sGXsfl_35_fel_idx;
         cmbavContagemresultado_statusdmn_Internalname = "vCONTAGEMRESULTADO_STATUSDMN_"+sGXsfl_35_fel_idx;
         edtavContagemresultado_servico_Internalname = "vCONTAGEMRESULTADO_SERVICO_"+sGXsfl_35_fel_idx;
         edtavContagemresultado_servicosigla_Internalname = "vCONTAGEMRESULTADO_SERVICOSIGLA_"+sGXsfl_35_fel_idx;
         edtavTotalcontagemresultado_pfbfm_Internalname = "vTOTALCONTAGEMRESULTADO_PFBFM_"+sGXsfl_35_fel_idx;
         edtavTotalcontagemresultado_pflfm_Internalname = "vTOTALCONTAGEMRESULTADO_PFLFM_"+sGXsfl_35_fel_idx;
         edtavTotalcontagemresultado_pfpagar_Internalname = "vTOTALCONTAGEMRESULTADO_PFPAGAR_"+sGXsfl_35_fel_idx;
         edtavTotalcontagemresultado_valorpf_Internalname = "vTOTALCONTAGEMRESULTADO_VALORPF_"+sGXsfl_35_fel_idx;
         edtavTotalcontagemresultado_glsvalor_Internalname = "vTOTALCONTAGEMRESULTADO_GLSVALOR_"+sGXsfl_35_fel_idx;
         edtavTotalcontagemresultado_pffinal_Internalname = "vTOTALCONTAGEMRESULTADO_PFFINAL_"+sGXsfl_35_fel_idx;
      }

      protected void sendrow_352( )
      {
         SubsflControlProps_352( ) ;
         WBMW0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_35_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_35_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_35_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_contratadacod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6ContagemResultado_ContratadaCod), 6, 0, ",", "")),((edtavContagemresultado_contratadacod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV6ContagemResultado_ContratadaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV6ContagemResultado_ContratadaCod), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_contratadacod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavContagemresultado_contratadacod_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_contratadasigla_Internalname,StringUtil.RTrim( AV7ContagemResultado_ContratadaSigla),StringUtil.RTrim( context.localUtil.Format( AV7ContagemResultado_ContratadaSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_contratadasigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavContagemresultado_contratadasigla_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_35_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vCONTAGEMRESULTADO_STATUSDMN_" + sGXsfl_35_idx;
               cmbavContagemresultado_statusdmn.Name = GXCCtl;
               cmbavContagemresultado_statusdmn.WebTags = "";
               cmbavContagemresultado_statusdmn.addItem("B", "Stand by", 0);
               cmbavContagemresultado_statusdmn.addItem("S", "Solicitada", 0);
               cmbavContagemresultado_statusdmn.addItem("E", "Em An�lise", 0);
               cmbavContagemresultado_statusdmn.addItem("A", "Em execu��o", 0);
               cmbavContagemresultado_statusdmn.addItem("R", "Resolvida", 0);
               cmbavContagemresultado_statusdmn.addItem("C", "Conferida", 0);
               cmbavContagemresultado_statusdmn.addItem("D", "Retornada", 0);
               cmbavContagemresultado_statusdmn.addItem("H", "Homologada", 0);
               cmbavContagemresultado_statusdmn.addItem("O", "Aceite", 0);
               cmbavContagemresultado_statusdmn.addItem("P", "A Pagar", 0);
               cmbavContagemresultado_statusdmn.addItem("L", "Liquidada", 0);
               cmbavContagemresultado_statusdmn.addItem("X", "Cancelada", 0);
               cmbavContagemresultado_statusdmn.addItem("N", "N�o Faturada", 0);
               cmbavContagemresultado_statusdmn.addItem("J", "Planejamento", 0);
               cmbavContagemresultado_statusdmn.addItem("I", "An�lise Planejamento", 0);
               cmbavContagemresultado_statusdmn.addItem("T", "Validacao T�cnica", 0);
               cmbavContagemresultado_statusdmn.addItem("Q", "Validacao Qualidade", 0);
               cmbavContagemresultado_statusdmn.addItem("G", "Em Homologa��o", 0);
               cmbavContagemresultado_statusdmn.addItem("M", "Valida��o Mensura��o", 0);
               cmbavContagemresultado_statusdmn.addItem("U", "Rascunho", 0);
               if ( cmbavContagemresultado_statusdmn.ItemCount > 0 )
               {
                  AV8ContagemResultado_StatusDmn = cmbavContagemresultado_statusdmn.getValidValue(AV8ContagemResultado_StatusDmn);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavContagemresultado_statusdmn_Internalname, AV8ContagemResultado_StatusDmn);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavContagemresultado_statusdmn,(String)cmbavContagemresultado_statusdmn_Internalname,StringUtil.RTrim( AV8ContagemResultado_StatusDmn),(short)1,(String)cmbavContagemresultado_statusdmn_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,cmbavContagemresultado_statusdmn.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbavContagemresultado_statusdmn.CurrentValue = StringUtil.RTrim( AV8ContagemResultado_StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn_Internalname, "Values", (String)(cmbavContagemresultado_statusdmn.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_servico_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9ContagemResultado_Servico), 6, 0, ",", "")),((edtavContagemresultado_servico_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9ContagemResultado_Servico), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV9ContagemResultado_Servico), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_servico_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavContagemresultado_servico_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_servicosigla_Internalname,StringUtil.RTrim( AV10ContagemResultado_ServicoSigla),StringUtil.RTrim( context.localUtil.Format( AV10ContagemResultado_ServicoSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_servicosigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavContagemresultado_servicosigla_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavTotalcontagemresultado_pfbfm_Internalname,StringUtil.LTrim( StringUtil.NToC( AV22TotalContagemResultado_PFBFM, 14, 5, ",", "")),((edtavTotalcontagemresultado_pfbfm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV22TotalContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV22TotalContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavTotalcontagemresultado_pfbfm_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavTotalcontagemresultado_pfbfm_Enabled,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavTotalcontagemresultado_pflfm_Internalname,StringUtil.LTrim( StringUtil.NToC( AV24TotalContagemResultado_PFLFM, 14, 5, ",", "")),((edtavTotalcontagemresultado_pflfm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV24TotalContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV24TotalContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavTotalcontagemresultado_pflfm_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavTotalcontagemresultado_pflfm_Enabled,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavTotalcontagemresultado_pfpagar_Internalname,StringUtil.LTrim( StringUtil.NToC( AV27TotalContagemResultado_PFPagar, 18, 5, ",", "")),((edtavTotalcontagemresultado_pfpagar_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV27TotalContagemResultado_PFPagar, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV27TotalContagemResultado_PFPagar, "ZZ,ZZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavTotalcontagemresultado_pfpagar_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavTotalcontagemresultado_pfpagar_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavTotalcontagemresultado_valorpf_Internalname,StringUtil.LTrim( StringUtil.NToC( AV26TotalContagemResultado_ValorPF, 18, 5, ",", "")),((edtavTotalcontagemresultado_valorpf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV26TotalContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV26TotalContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavTotalcontagemresultado_valorpf_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavTotalcontagemresultado_valorpf_Enabled,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavTotalcontagemresultado_glsvalor_Internalname,StringUtil.LTrim( StringUtil.NToC( AV25TotalContagemResultado_GlsValor, 18, 2, ",", "")),((edtavTotalcontagemresultado_glsvalor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV25TotalContagemResultado_GlsValor, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV25TotalContagemResultado_GlsValor, "ZZ,ZZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavTotalcontagemresultado_glsvalor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavTotalcontagemresultado_glsvalor_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavTotalcontagemresultado_pffinal_Internalname,StringUtil.LTrim( StringUtil.NToC( AV23TotalContagemResultado_PFFinal, 14, 5, ",", "")),((edtavTotalcontagemresultado_pffinal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV23TotalContagemResultado_PFFinal, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV23TotalContagemResultado_PFFinal, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavTotalcontagemresultado_pffinal_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavTotalcontagemresultado_pffinal_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GridContainer.AddRow(GridRow);
            nGXsfl_35_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_35_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_35_idx+1));
            sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
            SubsflControlProps_352( ) ;
         }
         /* End function sendrow_352 */
      }

      protected void init_default_properties( )
      {
         lblTextblocktitle_Internalname = "TEXTBLOCKTITLE";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblTextblockfiltrocontagemresultado_statusdmn_Internalname = "TEXTBLOCKFILTROCONTAGEMRESULTADO_STATUSDMN";
         cmbavFiltrocontagemresultado_statusdmn_Internalname = "vFILTROCONTAGEMRESULTADO_STATUSDMN";
         lblTextblockfiltrocontagemresultado_servico_Internalname = "TEXTBLOCKFILTROCONTAGEMRESULTADO_SERVICO";
         edtavFiltrocontagemresultado_servico_Internalname = "vFILTROCONTAGEMRESULTADO_SERVICO";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavContagemresultado_contratadacod_Internalname = "vCONTAGEMRESULTADO_CONTRATADACOD";
         edtavContagemresultado_contratadasigla_Internalname = "vCONTAGEMRESULTADO_CONTRATADASIGLA";
         cmbavContagemresultado_statusdmn_Internalname = "vCONTAGEMRESULTADO_STATUSDMN";
         edtavContagemresultado_servico_Internalname = "vCONTAGEMRESULTADO_SERVICO";
         edtavContagemresultado_servicosigla_Internalname = "vCONTAGEMRESULTADO_SERVICOSIGLA";
         edtavTotalcontagemresultado_pfbfm_Internalname = "vTOTALCONTAGEMRESULTADO_PFBFM";
         edtavTotalcontagemresultado_pflfm_Internalname = "vTOTALCONTAGEMRESULTADO_PFLFM";
         edtavTotalcontagemresultado_pfpagar_Internalname = "vTOTALCONTAGEMRESULTADO_PFPAGAR";
         edtavTotalcontagemresultado_valorpf_Internalname = "vTOTALCONTAGEMRESULTADO_VALORPF";
         edtavTotalcontagemresultado_glsvalor_Internalname = "vTOTALCONTAGEMRESULTADO_GLSVALOR";
         edtavTotalcontagemresultado_pffinal_Internalname = "vTOTALCONTAGEMRESULTADO_PFFINAL";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         edtavGridcurrentpage_Internalname = "vGRIDCURRENTPAGE";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavTotalcontagemresultado_pffinal_Jsonclick = "";
         edtavTotalcontagemresultado_glsvalor_Jsonclick = "";
         edtavTotalcontagemresultado_valorpf_Jsonclick = "";
         edtavTotalcontagemresultado_pfpagar_Jsonclick = "";
         edtavTotalcontagemresultado_pflfm_Jsonclick = "";
         edtavTotalcontagemresultado_pfbfm_Jsonclick = "";
         edtavContagemresultado_servicosigla_Jsonclick = "";
         edtavContagemresultado_servico_Jsonclick = "";
         cmbavContagemresultado_statusdmn_Jsonclick = "";
         edtavContagemresultado_contratadasigla_Jsonclick = "";
         edtavContagemresultado_contratadacod_Jsonclick = "";
         edtavFiltrocontagemresultado_servico_Jsonclick = "";
         cmbavFiltrocontagemresultado_statusdmn_Jsonclick = "";
         edtavGridcurrentpage_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowhovering = 0;
         subGrid_Selectioncolor = (int)(0xC4F0C4);
         subGrid_Allowselection = 1;
         edtavTotalcontagemresultado_pffinal_Enabled = 0;
         edtavTotalcontagemresultado_glsvalor_Enabled = 0;
         edtavTotalcontagemresultado_valorpf_Enabled = 0;
         edtavTotalcontagemresultado_pfpagar_Enabled = 0;
         edtavTotalcontagemresultado_pflfm_Enabled = 0;
         edtavTotalcontagemresultado_pfbfm_Enabled = 0;
         edtavContagemresultado_servicosigla_Enabled = 0;
         edtavContagemresultado_servico_Enabled = 0;
         cmbavContagemresultado_statusdmn.Enabled = 0;
         edtavContagemresultado_contratadasigla_Enabled = 0;
         edtavContagemresultado_contratadacod_Enabled = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavGridcurrentpage_Visible = 1;
         subGrid_Backcolorstyle = 3;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Acompanhamento Financeiro";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV17Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'A803ContagemResultado_ContratadaSigla',fld:'CONTAGEMRESULTADO_CONTRATADASIGLA',pic:'@!',nv:''},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A801ContagemResultado_ServicoSigla',fld:'CONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'A606ContagemResultado_ValorFinal',fld:'CONTAGEMRESULTADO_VALORFINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_GlsValor',fld:'vCONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A512ContagemResultado_ValorPF',fld:'CONTAGEMRESULTADO_VALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1051ContagemResultado_GlsValor',fld:'CONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1602ContagemResultado_GlsIndValor',fld:'CONTAGEMRESULTADO_GLSINDVALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A574ContagemResultado_PFFinal',fld:'CONTAGEMRESULTADO_PFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV11ContagemResultado_PFBFM',fld:'vCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV12ContagemResultado_PFLFM',fld:'vCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV16WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1446ContratoGestor_ContratadaAreaCod',fld:'CONTRATOGESTOR_CONTRATADAAREACOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV6ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_StatusDmn',fld:'vCONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A460ContagemResultado_PFBFM',fld:'CONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A461ContagemResultado_PFLFM',fld:'CONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E14MW2',iparms:[{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV17Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'A803ContagemResultado_ContratadaSigla',fld:'CONTAGEMRESULTADO_CONTRATADASIGLA',pic:'@!',nv:''},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A801ContagemResultado_ServicoSigla',fld:'CONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'A606ContagemResultado_ValorFinal',fld:'CONTAGEMRESULTADO_VALORFINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_GlsValor',fld:'vCONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A512ContagemResultado_ValorPF',fld:'CONTAGEMRESULTADO_VALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1051ContagemResultado_GlsValor',fld:'CONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1602ContagemResultado_GlsIndValor',fld:'CONTAGEMRESULTADO_GLSINDVALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A574ContagemResultado_PFFinal',fld:'CONTAGEMRESULTADO_PFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV11ContagemResultado_PFBFM',fld:'vCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV12ContagemResultado_PFLFM',fld:'vCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV16WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1446ContratoGestor_ContratadaAreaCod',fld:'CONTRATOGESTOR_CONTRATADAAREACOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV6ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_StatusDmn',fld:'vCONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A460ContagemResultado_PFBFM',fld:'CONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A461ContagemResultado_PFLFM',fld:'CONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV16WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV22TotalContagemResultado_PFBFM',fld:'vTOTALCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV24TotalContagemResultado_PFLFM',fld:'vTOTALCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV27TotalContagemResultado_PFPagar',fld:'vTOTALCONTAGEMRESULTADO_PFPAGAR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TotalContagemResultado_ValorPF',fld:'vTOTALCONTAGEMRESULTADO_VALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV25TotalContagemResultado_GlsValor',fld:'vTOTALCONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV23TotalContagemResultado_PFFinal',fld:'vTOTALCONTAGEMRESULTADO_PFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV7ContagemResultado_ContratadaSigla',fld:'vCONTAGEMRESULTADO_CONTRATADASIGLA',pic:'@!',nv:''},{av:'AV9ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV10ContagemResultado_ServicoSigla',fld:'vCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV8ContagemResultado_StatusDmn',fld:'vCONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV14ContagemResultado_GlsValor',fld:'vCONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV17Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV11ContagemResultado_PFBFM',fld:'vCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV12ContagemResultado_PFLFM',fld:'vCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11MW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV17Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'A803ContagemResultado_ContratadaSigla',fld:'CONTAGEMRESULTADO_CONTRATADASIGLA',pic:'@!',nv:''},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A801ContagemResultado_ServicoSigla',fld:'CONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'A606ContagemResultado_ValorFinal',fld:'CONTAGEMRESULTADO_VALORFINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_GlsValor',fld:'vCONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A512ContagemResultado_ValorPF',fld:'CONTAGEMRESULTADO_VALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1051ContagemResultado_GlsValor',fld:'CONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1602ContagemResultado_GlsIndValor',fld:'CONTAGEMRESULTADO_GLSINDVALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A574ContagemResultado_PFFinal',fld:'CONTAGEMRESULTADO_PFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV11ContagemResultado_PFBFM',fld:'vCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV12ContagemResultado_PFLFM',fld:'vCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV16WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1446ContratoGestor_ContratadaAreaCod',fld:'CONTRATOGESTOR_CONTRATADAAREACOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV6ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_StatusDmn',fld:'vCONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A460ContagemResultado_PFBFM',fld:'CONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A461ContagemResultado_PFLFM',fld:'CONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'},{av:'AV29GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}],oparms:[{av:'AV29GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV16WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A484ContagemResultado_StatusDmn = "";
         AV17Contratadas = new GxSimpleCollection();
         A803ContagemResultado_ContratadaSigla = "";
         A801ContagemResultado_ServicoSigla = "";
         AV8ContagemResultado_StatusDmn = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV7ContagemResultado_ContratadaSigla = "";
         AV10ContagemResultado_ServicoSigla = "";
         AV19filtroContagemResultado_StatusDmn = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV5Context = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         H00MW3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00MW3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00MW3_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00MW3_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00MW3_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00MW3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00MW3_A1051ContagemResultado_GlsValor = new decimal[1] ;
         H00MW3_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         H00MW3_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         H00MW3_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         H00MW3_A601ContagemResultado_Servico = new int[1] ;
         H00MW3_n601ContagemResultado_Servico = new bool[] {false} ;
         H00MW3_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00MW3_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00MW3_A1602ContagemResultado_GlsIndValor = new decimal[1] ;
         H00MW3_n1602ContagemResultado_GlsIndValor = new bool[] {false} ;
         H00MW3_A456ContagemResultado_Codigo = new int[1] ;
         H00MW3_A512ContagemResultado_ValorPF = new decimal[1] ;
         H00MW3_n512ContagemResultado_ValorPF = new bool[] {false} ;
         GridRow = new GXWebRow();
         H00MW4_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00MW4_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00MW4_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00MW4_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H00MW4_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H00MW4_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         AV18WebSession = context.GetSession();
         H00MW5_A460ContagemResultado_PFBFM = new decimal[1] ;
         H00MW5_n460ContagemResultado_PFBFM = new bool[] {false} ;
         H00MW5_A461ContagemResultado_PFLFM = new decimal[1] ;
         H00MW5_n461ContagemResultado_PFLFM = new bool[] {false} ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         TempTags = "";
         lblTextblocktitle_Jsonclick = "";
         lblTextblockfiltrocontagemresultado_statusdmn_Jsonclick = "";
         lblTextblockfiltrocontagemresultado_servico_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_acompanhamentofinanceiro__default(),
            new Object[][] {
                new Object[] {
               H00MW3_A1553ContagemResultado_CntSrvCod, H00MW3_n1553ContagemResultado_CntSrvCod, H00MW3_A484ContagemResultado_StatusDmn, H00MW3_n484ContagemResultado_StatusDmn, H00MW3_A490ContagemResultado_ContratadaCod, H00MW3_n490ContagemResultado_ContratadaCod, H00MW3_A1051ContagemResultado_GlsValor, H00MW3_n1051ContagemResultado_GlsValor, H00MW3_A803ContagemResultado_ContratadaSigla, H00MW3_n803ContagemResultado_ContratadaSigla,
               H00MW3_A601ContagemResultado_Servico, H00MW3_n601ContagemResultado_Servico, H00MW3_A801ContagemResultado_ServicoSigla, H00MW3_n801ContagemResultado_ServicoSigla, H00MW3_A1602ContagemResultado_GlsIndValor, H00MW3_n1602ContagemResultado_GlsIndValor, H00MW3_A456ContagemResultado_Codigo, H00MW3_A512ContagemResultado_ValorPF, H00MW3_n512ContagemResultado_ValorPF
               }
               , new Object[] {
               H00MW4_A1078ContratoGestor_ContratoCod, H00MW4_A1079ContratoGestor_UsuarioCod, H00MW4_A1446ContratoGestor_ContratadaAreaCod, H00MW4_n1446ContratoGestor_ContratadaAreaCod, H00MW4_A1136ContratoGestor_ContratadaCod, H00MW4_n1136ContratoGestor_ContratadaCod
               }
               , new Object[] {
               H00MW5_A460ContagemResultado_PFBFM, H00MW5_n460ContagemResultado_PFBFM, H00MW5_A461ContagemResultado_PFLFM, H00MW5_n461ContagemResultado_PFLFM
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_contratadacod_Enabled = 0;
         edtavContagemresultado_contratadasigla_Enabled = 0;
         cmbavContagemresultado_statusdmn.Enabled = 0;
         edtavContagemresultado_servico_Enabled = 0;
         edtavContagemresultado_servicosigla_Enabled = 0;
         edtavTotalcontagemresultado_pfbfm_Enabled = 0;
         edtavTotalcontagemresultado_pflfm_Enabled = 0;
         edtavTotalcontagemresultado_pfpagar_Enabled = 0;
         edtavTotalcontagemresultado_valorpf_Enabled = 0;
         edtavTotalcontagemresultado_glsvalor_Enabled = 0;
         edtavTotalcontagemresultado_pffinal_Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_35 ;
      private short nGXsfl_35_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_35_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int AV6ContagemResultado_ContratadaCod ;
      private int A456ContagemResultado_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int AV9ContagemResultado_Servico ;
      private int subGrid_Islastpage ;
      private int edtavContagemresultado_contratadacod_Enabled ;
      private int edtavContagemresultado_contratadasigla_Enabled ;
      private int edtavContagemresultado_servico_Enabled ;
      private int edtavContagemresultado_servicosigla_Enabled ;
      private int edtavTotalcontagemresultado_pfbfm_Enabled ;
      private int edtavTotalcontagemresultado_pflfm_Enabled ;
      private int edtavTotalcontagemresultado_pfpagar_Enabled ;
      private int edtavTotalcontagemresultado_valorpf_Enabled ;
      private int edtavTotalcontagemresultado_glsvalor_Enabled ;
      private int edtavTotalcontagemresultado_pffinal_Enabled ;
      private int GRID_nGridOutOfScope ;
      private int AV20filtroContagemResultado_Servico ;
      private int edtavGridcurrentpage_Visible ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int AV28PageToGo ;
      private int A1078ContratoGestor_ContratoCod ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV30GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long AV29GridCurrentPage ;
      private decimal A606ContagemResultado_ValorFinal ;
      private decimal AV14ContagemResultado_GlsValor ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A1051ContagemResultado_GlsValor ;
      private decimal A1602ContagemResultado_GlsIndValor ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal AV11ContagemResultado_PFBFM ;
      private decimal AV12ContagemResultado_PFLFM ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal AV22TotalContagemResultado_PFBFM ;
      private decimal AV24TotalContagemResultado_PFLFM ;
      private decimal AV27TotalContagemResultado_PFPagar ;
      private decimal AV26TotalContagemResultado_ValorPF ;
      private decimal AV25TotalContagemResultado_GlsValor ;
      private decimal AV23TotalContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private decimal AV15ContagemResultado_PFPagar ;
      private decimal AV21ContagemResultado_ValorPF ;
      private decimal AV13ContagemResultado_PFFinal ;
      private decimal c460ContagemResultado_PFBFM ;
      private decimal c461ContagemResultado_PFLFM ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_35_idx="0001" ;
      private String A484ContagemResultado_StatusDmn ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A801ContagemResultado_ServicoSigla ;
      private String edtavContagemresultado_contratadacod_Internalname ;
      private String AV8ContagemResultado_StatusDmn ;
      private String cmbavContagemresultado_statusdmn_Internalname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV7ContagemResultado_ContratadaSigla ;
      private String edtavContagemresultado_contratadasigla_Internalname ;
      private String edtavContagemresultado_servico_Internalname ;
      private String AV10ContagemResultado_ServicoSigla ;
      private String edtavContagemresultado_servicosigla_Internalname ;
      private String edtavTotalcontagemresultado_pfbfm_Internalname ;
      private String edtavTotalcontagemresultado_pflfm_Internalname ;
      private String edtavTotalcontagemresultado_pfpagar_Internalname ;
      private String edtavTotalcontagemresultado_valorpf_Internalname ;
      private String edtavTotalcontagemresultado_glsvalor_Internalname ;
      private String edtavTotalcontagemresultado_pffinal_Internalname ;
      private String AV19filtroContagemResultado_StatusDmn ;
      private String GXCCtl ;
      private String cmbavFiltrocontagemresultado_statusdmn_Internalname ;
      private String edtavFiltrocontagemresultado_servico_Internalname ;
      private String edtavGridcurrentpage_Internalname ;
      private String subGrid_Internalname ;
      private String scmdbuf ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String TempTags ;
      private String edtavGridcurrentpage_Jsonclick ;
      private String tblTableheader_Internalname ;
      private String lblTextblocktitle_Internalname ;
      private String lblTextblocktitle_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblTextblockfiltrocontagemresultado_statusdmn_Internalname ;
      private String lblTextblockfiltrocontagemresultado_statusdmn_Jsonclick ;
      private String cmbavFiltrocontagemresultado_statusdmn_Jsonclick ;
      private String lblTextblockfiltrocontagemresultado_servico_Internalname ;
      private String lblTextblockfiltrocontagemresultado_servico_Jsonclick ;
      private String edtavFiltrocontagemresultado_servico_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String sGXsfl_35_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavContagemresultado_contratadacod_Jsonclick ;
      private String edtavContagemresultado_contratadasigla_Jsonclick ;
      private String cmbavContagemresultado_statusdmn_Jsonclick ;
      private String edtavContagemresultado_servico_Jsonclick ;
      private String edtavContagemresultado_servicosigla_Jsonclick ;
      private String edtavTotalcontagemresultado_pfbfm_Jsonclick ;
      private String edtavTotalcontagemresultado_pflfm_Jsonclick ;
      private String edtavTotalcontagemresultado_pfpagar_Jsonclick ;
      private String edtavTotalcontagemresultado_valorpf_Jsonclick ;
      private String edtavTotalcontagemresultado_glsvalor_Jsonclick ;
      private String edtavTotalcontagemresultado_pffinal_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private bool entryPointCalled ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n601ContagemResultado_Servico ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n1051ContagemResultado_GlsValor ;
      private bool n1602ContagemResultado_GlsIndValor ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n461ContagemResultado_PFLFM ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool BRKMW3 ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavFiltrocontagemresultado_statusdmn ;
      private GXCombobox cmbavContagemresultado_statusdmn ;
      private IDataStoreProvider pr_default ;
      private int[] H00MW3_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00MW3_n1553ContagemResultado_CntSrvCod ;
      private String[] H00MW3_A484ContagemResultado_StatusDmn ;
      private bool[] H00MW3_n484ContagemResultado_StatusDmn ;
      private int[] H00MW3_A490ContagemResultado_ContratadaCod ;
      private bool[] H00MW3_n490ContagemResultado_ContratadaCod ;
      private decimal[] H00MW3_A1051ContagemResultado_GlsValor ;
      private bool[] H00MW3_n1051ContagemResultado_GlsValor ;
      private String[] H00MW3_A803ContagemResultado_ContratadaSigla ;
      private bool[] H00MW3_n803ContagemResultado_ContratadaSigla ;
      private int[] H00MW3_A601ContagemResultado_Servico ;
      private bool[] H00MW3_n601ContagemResultado_Servico ;
      private String[] H00MW3_A801ContagemResultado_ServicoSigla ;
      private bool[] H00MW3_n801ContagemResultado_ServicoSigla ;
      private decimal[] H00MW3_A1602ContagemResultado_GlsIndValor ;
      private bool[] H00MW3_n1602ContagemResultado_GlsIndValor ;
      private int[] H00MW3_A456ContagemResultado_Codigo ;
      private decimal[] H00MW3_A512ContagemResultado_ValorPF ;
      private bool[] H00MW3_n512ContagemResultado_ValorPF ;
      private int[] H00MW4_A1078ContratoGestor_ContratoCod ;
      private int[] H00MW4_A1079ContratoGestor_UsuarioCod ;
      private int[] H00MW4_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00MW4_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00MW4_A1136ContratoGestor_ContratadaCod ;
      private bool[] H00MW4_n1136ContratoGestor_ContratadaCod ;
      private decimal[] H00MW5_A460ContagemResultado_PFBFM ;
      private bool[] H00MW5_n460ContagemResultado_PFBFM ;
      private decimal[] H00MW5_A461ContagemResultado_PFLFM ;
      private bool[] H00MW5_n461ContagemResultado_PFLFM ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV18WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV17Contratadas ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV16WWPContext ;
      private wwpbaseobjects.SdtWWPContext AV5Context ;
   }

   public class wp_acompanhamentofinanceiro__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00MW3( IGxContext context ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             IGxCollection AV17Contratadas )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_GlsValor], T4.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T2.[Servico_Codigo] AS ContagemResultado_Servico, T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, COALESCE( T5.[ContagemResultado_GlsIndValor], 0) AS ContagemResultado_GlsIndValor, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ValorPF] FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT SUM(T6.[ContagemResultadoIndicadores_Valor]) AS ContagemResultado_GlsIndValor, T7.[ContagemResultado_Codigo] FROM [ContagemResultadoIndicadores] T6 WITH (NOLOCK),  [ContagemResultado] T7 WITH (NOLOCK) WHERE T6.[ContagemResultadoIndicadores_DemandaCod] = T7.[ContagemResultado_Codigo] GROUP BY T7.[ContagemResultado_Codigo] ) T5 ON T5.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17Contratadas, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_StatusDmn]";
         GXv_Object2[0] = scmdbuf;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00MW3(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00MW4 ;
          prmH00MW4 = new Object[] {
          new Object[] {"@AV16WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV16WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MW5 ;
          prmH00MW5 = new Object[] {
          new Object[] {"@AV6ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8ContagemResultado_StatusDmn",SqlDbType.Char,1,0}
          } ;
          Object[] prmH00MW3 ;
          prmH00MW3 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00MW3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MW3,100,0,true,false )
             ,new CursorDef("H00MW4", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE (T1.[ContratoGestor_UsuarioCod] = @AV16WWPContext__Userid) AND (T2.[Contrato_AreaTrabalhoCod] = @AV16WWPC_1Areatrabalho_codigo) ORDER BY T1.[ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MW4,100,0,false,false )
             ,new CursorDef("H00MW5", "SELECT SUM(T1.[ContagemResultado_PFBFM]), SUM(T1.[ContagemResultado_PFLFM]) FROM ([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T2.[ContagemResultado_ContratadaCod] = @AV6ContagemResultado_ContratadaCod) AND (T2.[ContagemResultado_StatusDmn] = @AV8ContagemResultado_StatusDmn) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MW5,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((decimal[]) buf[17])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 2 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
       }
    }

 }

}
