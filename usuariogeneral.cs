/*
               File: UsuarioGeneral
        Description: Usuario General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:26:16.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class usuariogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public usuariogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public usuariogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo )
      {
         this.A1Usuario_Codigo = aP0_Usuario_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbUsuario_PessoaTip = new GXCombobox();
         chkUsuario_EhContador = new GXCheckbox();
         chkUsuario_EhAuditorFM = new GXCheckbox();
         chkUsuario_EhFinanceiro = new GXCheckbox();
         chkUsuario_EhContratada = new GXCheckbox();
         chkUsuario_EhContratante = new GXCheckbox();
         chkavContratanteusuario_ehfiscal = new GXCheckbox();
         chkUsuario_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1Usuario_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA0K2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV43Pgmname = "UsuarioGeneral";
               context.Gx_err = 0;
               edtavEmail_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavEmail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmail_Enabled), 5, 0)));
               chkavContratanteusuario_ehfiscal.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavContratanteusuario_ehfiscal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavContratanteusuario_ehfiscal.Enabled), 5, 0)));
               edtavContratadausuario_cstuntprdnrm_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratadausuario_cstuntprdnrm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdnrm_Enabled), 5, 0)));
               edtavContratadausuario_cstuntprdext_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratadausuario_cstuntprdext_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdext_Enabled), 5, 0)));
               WS0K2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Usuario General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202053021261797");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("usuariogeneral.aspx") + "?" + UrlEncode("" +A1Usuario_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1Usuario_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vEHCONTRATADA_FLAG", AV15EhContratada_Flag);
         GxWebStd.gx_hidden_field( context, sPrefix+"vENTIDADE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Entidade_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_USUARIO_ENTIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1083Usuario_Entidade, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vEMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV13Email, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_USUARIO_EHCONTADOR", GetSecureSignedToken( sPrefix, A289Usuario_EhContador));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_USUARIO_EHAUDITORFM", GetSecureSignedToken( sPrefix, A290Usuario_EhAuditorFM));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_USUARIO_EHFINANCEIRO", GetSecureSignedToken( sPrefix, A293Usuario_EhFinanceiro));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_USUARIO_EHCONTRATADA", GetSecureSignedToken( sPrefix, A291Usuario_EhContratada));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_USUARIO_EHCONTRATANTE", GetSecureSignedToken( sPrefix, A292Usuario_EhContratante));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTRATANTEUSUARIO_EHFISCAL", GetSecureSignedToken( sPrefix, AV32ContratanteUsuario_EhFiscal));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_USUARIO_CRTFPATH", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1017Usuario_CrtfPath, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTRATADAUSUARIO_CSTUNTPRDNRM", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV19ContratadaUsuario_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTRATADAUSUARIO_CSTUNTPRDEXT", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV20ContratadaUsuario_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_USUARIO_ATIVO", GetSecureSignedToken( sPrefix, A54Usuario_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_USUARIO_CARGOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1073Usuario_CargoCod), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "UsuarioGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1073Usuario_CargoCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("usuariogeneral:[SendSecurityCheck value for]"+"Usuario_CargoCod:"+context.localUtil.Format( (decimal)(A1073Usuario_CargoCod), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm0K2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("usuariogeneral.js", "?202053021261824");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "UsuarioGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Usuario General" ;
      }

      protected void WB0K0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "usuariogeneral.aspx");
            }
            wb_table1_2_0K2( true) ;
         }
         else
         {
            wb_table1_2_0K2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_0K2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_CargoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1073Usuario_CargoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1073Usuario_CargoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_CargoCod_Jsonclick, 0, "Attribute", "", "", "", edtUsuario_CargoCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_UsuarioGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START0K2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Usuario General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP0K0( ) ;
            }
         }
      }

      protected void WS0K2( )
      {
         START0K2( ) ;
         EVT0K2( ) ;
      }

      protected void EVT0K2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0K0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0K0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E110K2 */
                                    E110K2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0K0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E120K2 */
                                    E120K2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0K0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E130K2 */
                                    E130K2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0K0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E140K2 */
                                    E140K2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0K0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E150K2 */
                                    E150K2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0K0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0K0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavEmail_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE0K2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm0K2( ) ;
            }
         }
      }

      protected void PA0K2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbUsuario_PessoaTip.Name = "USUARIO_PESSOATIP";
            cmbUsuario_PessoaTip.WebTags = "";
            cmbUsuario_PessoaTip.addItem("", "(Nenhum)", 0);
            cmbUsuario_PessoaTip.addItem("F", "F�sica", 0);
            cmbUsuario_PessoaTip.addItem("J", "Jur�dica", 0);
            if ( cmbUsuario_PessoaTip.ItemCount > 0 )
            {
               A59Usuario_PessoaTip = cmbUsuario_PessoaTip.getValidValue(A59Usuario_PessoaTip);
               n59Usuario_PessoaTip = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A59Usuario_PessoaTip", A59Usuario_PessoaTip);
            }
            chkUsuario_EhContador.Name = "USUARIO_EHCONTADOR";
            chkUsuario_EhContador.WebTags = "";
            chkUsuario_EhContador.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkUsuario_EhContador_Internalname, "TitleCaption", chkUsuario_EhContador.Caption);
            chkUsuario_EhContador.CheckedValue = "false";
            chkUsuario_EhAuditorFM.Name = "USUARIO_EHAUDITORFM";
            chkUsuario_EhAuditorFM.WebTags = "";
            chkUsuario_EhAuditorFM.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkUsuario_EhAuditorFM_Internalname, "TitleCaption", chkUsuario_EhAuditorFM.Caption);
            chkUsuario_EhAuditorFM.CheckedValue = "false";
            chkUsuario_EhFinanceiro.Name = "USUARIO_EHFINANCEIRO";
            chkUsuario_EhFinanceiro.WebTags = "";
            chkUsuario_EhFinanceiro.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkUsuario_EhFinanceiro_Internalname, "TitleCaption", chkUsuario_EhFinanceiro.Caption);
            chkUsuario_EhFinanceiro.CheckedValue = "false";
            chkUsuario_EhContratada.Name = "USUARIO_EHCONTRATADA";
            chkUsuario_EhContratada.WebTags = "";
            chkUsuario_EhContratada.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkUsuario_EhContratada_Internalname, "TitleCaption", chkUsuario_EhContratada.Caption);
            chkUsuario_EhContratada.CheckedValue = "false";
            chkUsuario_EhContratante.Name = "USUARIO_EHCONTRATANTE";
            chkUsuario_EhContratante.WebTags = "";
            chkUsuario_EhContratante.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkUsuario_EhContratante_Internalname, "TitleCaption", chkUsuario_EhContratante.Caption);
            chkUsuario_EhContratante.CheckedValue = "false";
            chkavContratanteusuario_ehfiscal.Name = "vCONTRATANTEUSUARIO_EHFISCAL";
            chkavContratanteusuario_ehfiscal.WebTags = "";
            chkavContratanteusuario_ehfiscal.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavContratanteusuario_ehfiscal_Internalname, "TitleCaption", chkavContratanteusuario_ehfiscal.Caption);
            chkavContratanteusuario_ehfiscal.CheckedValue = "false";
            chkUsuario_Ativo.Name = "USUARIO_ATIVO";
            chkUsuario_Ativo.WebTags = "";
            chkUsuario_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkUsuario_Ativo_Internalname, "TitleCaption", chkUsuario_Ativo.Caption);
            chkUsuario_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavEmail_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbUsuario_PessoaTip.ItemCount > 0 )
         {
            A59Usuario_PessoaTip = cmbUsuario_PessoaTip.getValidValue(A59Usuario_PessoaTip);
            n59Usuario_PessoaTip = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A59Usuario_PessoaTip", A59Usuario_PessoaTip);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0K2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV43Pgmname = "UsuarioGeneral";
         context.Gx_err = 0;
         edtavEmail_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavEmail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmail_Enabled), 5, 0)));
         chkavContratanteusuario_ehfiscal.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavContratanteusuario_ehfiscal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavContratanteusuario_ehfiscal.Enabled), 5, 0)));
         edtavContratadausuario_cstuntprdnrm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratadausuario_cstuntprdnrm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdnrm_Enabled), 5, 0)));
         edtavContratadausuario_cstuntprdext_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratadausuario_cstuntprdext_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdext_Enabled), 5, 0)));
      }

      protected void RF0K2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H000K2 */
            pr_default.execute(0, new Object[] {A1Usuario_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A57Usuario_PessoaCod = H000K2_A57Usuario_PessoaCod[0];
               A1075Usuario_CargoUOCod = H000K2_A1075Usuario_CargoUOCod[0];
               n1075Usuario_CargoUOCod = H000K2_n1075Usuario_CargoUOCod[0];
               A1073Usuario_CargoCod = H000K2_A1073Usuario_CargoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1073Usuario_CargoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1073Usuario_CargoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_CARGOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1073Usuario_CargoCod), "ZZZZZ9")));
               n1073Usuario_CargoCod = H000K2_n1073Usuario_CargoCod[0];
               A54Usuario_Ativo = H000K2_A54Usuario_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A54Usuario_Ativo", A54Usuario_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_ATIVO", GetSecureSignedToken( sPrefix, A54Usuario_Ativo));
               A341Usuario_UserGamGuid = H000K2_A341Usuario_UserGamGuid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
               n341Usuario_UserGamGuid = H000K2_n341Usuario_UserGamGuid[0];
               A1017Usuario_CrtfPath = H000K2_A1017Usuario_CrtfPath[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1017Usuario_CrtfPath", A1017Usuario_CrtfPath);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_CRTFPATH", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1017Usuario_CrtfPath, ""))));
               n1017Usuario_CrtfPath = H000K2_n1017Usuario_CrtfPath[0];
               A292Usuario_EhContratante = H000K2_A292Usuario_EhContratante[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A292Usuario_EhContratante", A292Usuario_EhContratante);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_EHCONTRATANTE", GetSecureSignedToken( sPrefix, A292Usuario_EhContratante));
               A291Usuario_EhContratada = H000K2_A291Usuario_EhContratada[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A291Usuario_EhContratada", A291Usuario_EhContratada);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_EHCONTRATADA", GetSecureSignedToken( sPrefix, A291Usuario_EhContratada));
               A293Usuario_EhFinanceiro = H000K2_A293Usuario_EhFinanceiro[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A293Usuario_EhFinanceiro", A293Usuario_EhFinanceiro);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_EHFINANCEIRO", GetSecureSignedToken( sPrefix, A293Usuario_EhFinanceiro));
               A289Usuario_EhContador = H000K2_A289Usuario_EhContador[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A289Usuario_EhContador", A289Usuario_EhContador);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_EHCONTADOR", GetSecureSignedToken( sPrefix, A289Usuario_EhContador));
               A325Usuario_PessoaDoc = H000K2_A325Usuario_PessoaDoc[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A325Usuario_PessoaDoc", A325Usuario_PessoaDoc);
               n325Usuario_PessoaDoc = H000K2_n325Usuario_PessoaDoc[0];
               A59Usuario_PessoaTip = H000K2_A59Usuario_PessoaTip[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A59Usuario_PessoaTip", A59Usuario_PessoaTip);
               n59Usuario_PessoaTip = H000K2_n59Usuario_PessoaTip[0];
               A58Usuario_PessoaNom = H000K2_A58Usuario_PessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
               n58Usuario_PessoaNom = H000K2_n58Usuario_PessoaNom[0];
               A40000Usuario_Foto_GXI = H000K2_A40000Usuario_Foto_GXI[0];
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgUsuario_Foto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)) ? A40000Usuario_Foto_GXI : context.convertURL( context.PathToRelativeUrl( A1716Usuario_Foto))));
               n40000Usuario_Foto_GXI = H000K2_n40000Usuario_Foto_GXI[0];
               A1074Usuario_CargoNom = H000K2_A1074Usuario_CargoNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1074Usuario_CargoNom", A1074Usuario_CargoNom);
               n1074Usuario_CargoNom = H000K2_n1074Usuario_CargoNom[0];
               A1076Usuario_CargoUONom = H000K2_A1076Usuario_CargoUONom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1076Usuario_CargoUONom", A1076Usuario_CargoUONom);
               n1076Usuario_CargoUONom = H000K2_n1076Usuario_CargoUONom[0];
               A1716Usuario_Foto = H000K2_A1716Usuario_Foto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1716Usuario_Foto", A1716Usuario_Foto);
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgUsuario_Foto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)) ? A40000Usuario_Foto_GXI : context.convertURL( context.PathToRelativeUrl( A1716Usuario_Foto))));
               n1716Usuario_Foto = H000K2_n1716Usuario_Foto[0];
               A325Usuario_PessoaDoc = H000K2_A325Usuario_PessoaDoc[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A325Usuario_PessoaDoc", A325Usuario_PessoaDoc);
               n325Usuario_PessoaDoc = H000K2_n325Usuario_PessoaDoc[0];
               A59Usuario_PessoaTip = H000K2_A59Usuario_PessoaTip[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A59Usuario_PessoaTip", A59Usuario_PessoaTip);
               n59Usuario_PessoaTip = H000K2_n59Usuario_PessoaTip[0];
               A58Usuario_PessoaNom = H000K2_A58Usuario_PessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
               n58Usuario_PessoaNom = H000K2_n58Usuario_PessoaNom[0];
               A1075Usuario_CargoUOCod = H000K2_A1075Usuario_CargoUOCod[0];
               n1075Usuario_CargoUOCod = H000K2_n1075Usuario_CargoUOCod[0];
               A1074Usuario_CargoNom = H000K2_A1074Usuario_CargoNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1074Usuario_CargoNom", A1074Usuario_CargoNom);
               n1074Usuario_CargoNom = H000K2_n1074Usuario_CargoNom[0];
               A1076Usuario_CargoUONom = H000K2_A1076Usuario_CargoUONom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1076Usuario_CargoUONom", A1076Usuario_CargoUONom);
               n1076Usuario_CargoUONom = H000K2_n1076Usuario_CargoUONom[0];
               /* Execute user event: E120K2 */
               E120K2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB0K0( ) ;
         }
      }

      protected void STRUP0K0( )
      {
         /* Before Start, stand alone formulas. */
         AV43Pgmname = "UsuarioGeneral";
         context.Gx_err = 0;
         edtavEmail_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavEmail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmail_Enabled), 5, 0)));
         chkavContratanteusuario_ehfiscal.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavContratanteusuario_ehfiscal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavContratanteusuario_ehfiscal.Enabled), 5, 0)));
         edtavContratadausuario_cstuntprdnrm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratadausuario_cstuntprdnrm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdnrm_Enabled), 5, 0)));
         edtavContratadausuario_cstuntprdext_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratadausuario_cstuntprdext_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdext_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110K2 */
         E110K2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXt_char1 = A1083Usuario_Entidade;
         new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         A1083Usuario_Entidade = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1083Usuario_Entidade", A1083Usuario_Entidade);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_ENTIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1083Usuario_Entidade, "@!"))));
         GXt_boolean2 = A290Usuario_EhAuditorFM;
         new prc_usuarioehauditorfm(context ).execute(  A1Usuario_Codigo, out  GXt_boolean2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         A290Usuario_EhAuditorFM = GXt_boolean2;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A290Usuario_EhAuditorFM", A290Usuario_EhAuditorFM);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_EHAUDITORFM", GetSecureSignedToken( sPrefix, A290Usuario_EhAuditorFM));
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1076Usuario_CargoUONom = StringUtil.Upper( cgiGet( edtUsuario_CargoUONom_Internalname));
            n1076Usuario_CargoUONom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1076Usuario_CargoUONom", A1076Usuario_CargoUONom);
            A1074Usuario_CargoNom = StringUtil.Upper( cgiGet( edtUsuario_CargoNom_Internalname));
            n1074Usuario_CargoNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1074Usuario_CargoNom", A1074Usuario_CargoNom);
            A1716Usuario_Foto = cgiGet( imgUsuario_Foto_Internalname);
            n1716Usuario_Foto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1716Usuario_Foto", A1716Usuario_Foto);
            A1083Usuario_Entidade = StringUtil.Upper( cgiGet( edtUsuario_Entidade_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1083Usuario_Entidade", A1083Usuario_Entidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_ENTIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1083Usuario_Entidade, "@!"))));
            AV13Email = StringUtil.Upper( cgiGet( edtavEmail_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Email", AV13Email);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vEMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV13Email, "@!"))));
            A58Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtUsuario_PessoaNom_Internalname));
            n58Usuario_PessoaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
            cmbUsuario_PessoaTip.CurrentValue = cgiGet( cmbUsuario_PessoaTip_Internalname);
            A59Usuario_PessoaTip = cgiGet( cmbUsuario_PessoaTip_Internalname);
            n59Usuario_PessoaTip = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A59Usuario_PessoaTip", A59Usuario_PessoaTip);
            A325Usuario_PessoaDoc = cgiGet( edtUsuario_PessoaDoc_Internalname);
            n325Usuario_PessoaDoc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A325Usuario_PessoaDoc", A325Usuario_PessoaDoc);
            A289Usuario_EhContador = StringUtil.StrToBool( cgiGet( chkUsuario_EhContador_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A289Usuario_EhContador", A289Usuario_EhContador);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_EHCONTADOR", GetSecureSignedToken( sPrefix, A289Usuario_EhContador));
            A290Usuario_EhAuditorFM = StringUtil.StrToBool( cgiGet( chkUsuario_EhAuditorFM_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A290Usuario_EhAuditorFM", A290Usuario_EhAuditorFM);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_EHAUDITORFM", GetSecureSignedToken( sPrefix, A290Usuario_EhAuditorFM));
            A293Usuario_EhFinanceiro = StringUtil.StrToBool( cgiGet( chkUsuario_EhFinanceiro_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A293Usuario_EhFinanceiro", A293Usuario_EhFinanceiro);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_EHFINANCEIRO", GetSecureSignedToken( sPrefix, A293Usuario_EhFinanceiro));
            A291Usuario_EhContratada = StringUtil.StrToBool( cgiGet( chkUsuario_EhContratada_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A291Usuario_EhContratada", A291Usuario_EhContratada);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_EHCONTRATADA", GetSecureSignedToken( sPrefix, A291Usuario_EhContratada));
            A292Usuario_EhContratante = StringUtil.StrToBool( cgiGet( chkUsuario_EhContratante_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A292Usuario_EhContratante", A292Usuario_EhContratante);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_EHCONTRATANTE", GetSecureSignedToken( sPrefix, A292Usuario_EhContratante));
            AV32ContratanteUsuario_EhFiscal = StringUtil.StrToBool( cgiGet( chkavContratanteusuario_ehfiscal_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ContratanteUsuario_EhFiscal", AV32ContratanteUsuario_EhFiscal);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTRATANTEUSUARIO_EHFISCAL", GetSecureSignedToken( sPrefix, AV32ContratanteUsuario_EhFiscal));
            A1017Usuario_CrtfPath = cgiGet( edtUsuario_CrtfPath_Internalname);
            n1017Usuario_CrtfPath = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1017Usuario_CrtfPath", A1017Usuario_CrtfPath);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_CRTFPATH", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1017Usuario_CrtfPath, ""))));
            A341Usuario_UserGamGuid = cgiGet( edtUsuario_UserGamGuid_Internalname);
            n341Usuario_UserGamGuid = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratadausuario_cstuntprdnrm_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratadausuario_cstuntprdnrm_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADAUSUARIO_CSTUNTPRDNRM");
               GX_FocusControl = edtavContratadausuario_cstuntprdnrm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19ContratadaUsuario_CstUntPrdNrm = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ContratadaUsuario_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV19ContratadaUsuario_CstUntPrdNrm, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTRATADAUSUARIO_CSTUNTPRDNRM", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV19ContratadaUsuario_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            }
            else
            {
               AV19ContratadaUsuario_CstUntPrdNrm = context.localUtil.CToN( cgiGet( edtavContratadausuario_cstuntprdnrm_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ContratadaUsuario_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV19ContratadaUsuario_CstUntPrdNrm, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTRATADAUSUARIO_CSTUNTPRDNRM", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV19ContratadaUsuario_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratadausuario_cstuntprdext_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratadausuario_cstuntprdext_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADAUSUARIO_CSTUNTPRDEXT");
               GX_FocusControl = edtavContratadausuario_cstuntprdext_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20ContratadaUsuario_CstUntPrdExt = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ContratadaUsuario_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV20ContratadaUsuario_CstUntPrdExt, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTRATADAUSUARIO_CSTUNTPRDEXT", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV20ContratadaUsuario_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            }
            else
            {
               AV20ContratadaUsuario_CstUntPrdExt = context.localUtil.CToN( cgiGet( edtavContratadausuario_cstuntprdext_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ContratadaUsuario_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV20ContratadaUsuario_CstUntPrdExt, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTRATADAUSUARIO_CSTUNTPRDEXT", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV20ContratadaUsuario_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            }
            A54Usuario_Ativo = StringUtil.StrToBool( cgiGet( chkUsuario_Ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A54Usuario_Ativo", A54Usuario_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_ATIVO", GetSecureSignedToken( sPrefix, A54Usuario_Ativo));
            A1073Usuario_CargoCod = (int)(context.localUtil.CToN( cgiGet( edtUsuario_CargoCod_Internalname), ",", "."));
            n1073Usuario_CargoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1073Usuario_CargoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1073Usuario_CargoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_CARGOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1073Usuario_CargoCod), "ZZZZZ9")));
            /* Read saved values. */
            wcpOA1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1Usuario_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "UsuarioGeneral";
            A1073Usuario_CargoCod = (int)(context.localUtil.CToN( cgiGet( edtUsuario_CargoCod_Internalname), ",", "."));
            n1073Usuario_CargoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1073Usuario_CargoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1073Usuario_CargoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_USUARIO_CARGOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1073Usuario_CargoCod), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1073Usuario_CargoCod), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("usuariogeneral:[SecurityCheckFailed value for]"+"Usuario_CargoCod:"+context.localUtil.Format( (decimal)(A1073Usuario_CargoCod), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E110K2 */
         E110K2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E110K2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         Gx_msg = AV16Websession.Get("Entidade");
         AV15EhContratada_Flag = (bool)(((StringUtil.StrCmp(StringUtil.Substring( Gx_msg, 1, 1), "A")==0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15EhContratada_Flag", AV15EhContratada_Flag);
         AV17Entidade_Codigo = (int)(NumberUtil.Val( StringUtil.Substring( Gx_msg, 2, 8), "."));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Entidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Entidade_Codigo), 6, 0)));
         if ( (0==AV17Entidade_Codigo) )
         {
            /* Using cursor H000K3 */
            pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A5AreaTrabalho_Codigo = H000K3_A5AreaTrabalho_Codigo[0];
               A29Contratante_Codigo = H000K3_A29Contratante_Codigo[0];
               n29Contratante_Codigo = H000K3_n29Contratante_Codigo[0];
               AV39GXLvl17 = 0;
               /* Using cursor H000K4 */
               pr_default.execute(2, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo, A1Usuario_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A63ContratanteUsuario_ContratanteCod = H000K4_A63ContratanteUsuario_ContratanteCod[0];
                  A60ContratanteUsuario_UsuarioCod = H000K4_A60ContratanteUsuario_UsuarioCod[0];
                  AV39GXLvl17 = 1;
                  AV15EhContratada_Flag = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15EhContratada_Flag", AV15EhContratada_Flag);
                  AV17Entidade_Codigo = A63ContratanteUsuario_ContratanteCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Entidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Entidade_Codigo), 6, 0)));
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               if ( AV39GXLvl17 == 0 )
               {
                  /* Using cursor H000K5 */
                  pr_default.execute(3, new Object[] {A5AreaTrabalho_Codigo, A1Usuario_Codigo});
                  while ( (pr_default.getStatus(3) != 101) )
                  {
                     A69ContratadaUsuario_UsuarioCod = H000K5_A69ContratadaUsuario_UsuarioCod[0];
                     A1228ContratadaUsuario_AreaTrabalhoCod = H000K5_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                     n1228ContratadaUsuario_AreaTrabalhoCod = H000K5_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                     A66ContratadaUsuario_ContratadaCod = H000K5_A66ContratadaUsuario_ContratadaCod[0];
                     A1228ContratadaUsuario_AreaTrabalhoCod = H000K5_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                     n1228ContratadaUsuario_AreaTrabalhoCod = H000K5_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                     AV15EhContratada_Flag = true;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15EhContratada_Flag", AV15EhContratada_Flag);
                     AV17Entidade_Codigo = A66ContratadaUsuario_ContratadaCod;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Entidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Entidade_Codigo), 6, 0)));
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(3);
                  }
                  pr_default.close(3);
               }
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
         }
         if ( AV15EhContratada_Flag )
         {
            /* Using cursor H000K6 */
            pr_default.execute(4, new Object[] {AV17Entidade_Codigo, A1Usuario_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A67ContratadaUsuario_ContratadaPessoaCod = H000K6_A67ContratadaUsuario_ContratadaPessoaCod[0];
               n67ContratadaUsuario_ContratadaPessoaCod = H000K6_n67ContratadaUsuario_ContratadaPessoaCod[0];
               A69ContratadaUsuario_UsuarioCod = H000K6_A69ContratadaUsuario_UsuarioCod[0];
               A66ContratadaUsuario_ContratadaCod = H000K6_A66ContratadaUsuario_ContratadaCod[0];
               A68ContratadaUsuario_ContratadaPessoaNom = H000K6_A68ContratadaUsuario_ContratadaPessoaNom[0];
               n68ContratadaUsuario_ContratadaPessoaNom = H000K6_n68ContratadaUsuario_ContratadaPessoaNom[0];
               A2Usuario_Nome = H000K6_A2Usuario_Nome[0];
               n2Usuario_Nome = H000K6_n2Usuario_Nome[0];
               A341Usuario_UserGamGuid = H000K6_A341Usuario_UserGamGuid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
               n341Usuario_UserGamGuid = H000K6_n341Usuario_UserGamGuid[0];
               A576ContratadaUsuario_CstUntPrdNrm = H000K6_A576ContratadaUsuario_CstUntPrdNrm[0];
               n576ContratadaUsuario_CstUntPrdNrm = H000K6_n576ContratadaUsuario_CstUntPrdNrm[0];
               A577ContratadaUsuario_CstUntPrdExt = H000K6_A577ContratadaUsuario_CstUntPrdExt[0];
               n577ContratadaUsuario_CstUntPrdExt = H000K6_n577ContratadaUsuario_CstUntPrdExt[0];
               A2Usuario_Nome = H000K6_A2Usuario_Nome[0];
               n2Usuario_Nome = H000K6_n2Usuario_Nome[0];
               A341Usuario_UserGamGuid = H000K6_A341Usuario_UserGamGuid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
               n341Usuario_UserGamGuid = H000K6_n341Usuario_UserGamGuid[0];
               A67ContratadaUsuario_ContratadaPessoaCod = H000K6_A67ContratadaUsuario_ContratadaPessoaCod[0];
               n67ContratadaUsuario_ContratadaPessoaCod = H000K6_n67ContratadaUsuario_ContratadaPessoaCod[0];
               A68ContratadaUsuario_ContratadaPessoaNom = H000K6_A68ContratadaUsuario_ContratadaPessoaNom[0];
               n68ContratadaUsuario_ContratadaPessoaNom = H000K6_n68ContratadaUsuario_ContratadaPessoaNom[0];
               AV12Entidade = A68ContratadaUsuario_ContratadaPessoaNom;
               AV26Usuario_Nome = A2Usuario_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Usuario_Nome", AV26Usuario_Nome);
               AV31Usuario_UserGamGuid = A341Usuario_UserGamGuid;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31Usuario_UserGamGuid", AV31Usuario_UserGamGuid);
               AV14GamUser.load( A341Usuario_UserGamGuid);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
               AV13Email = AV14GamUser.gxTpr_Email;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Email", AV13Email);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vEMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV13Email, "@!"))));
               AV19ContratadaUsuario_CstUntPrdNrm = A576ContratadaUsuario_CstUntPrdNrm;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ContratadaUsuario_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV19ContratadaUsuario_CstUntPrdNrm, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTRATADAUSUARIO_CSTUNTPRDNRM", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV19ContratadaUsuario_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               AV20ContratadaUsuario_CstUntPrdExt = A577ContratadaUsuario_CstUntPrdExt;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ContratadaUsuario_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV20ContratadaUsuario_CstUntPrdExt, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTRATADAUSUARIO_CSTUNTPRDEXT", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV20ContratadaUsuario_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(4);
         }
         else
         {
            /* Using cursor H000K7 */
            pr_default.execute(5, new Object[] {AV17Entidade_Codigo, A1Usuario_Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A340ContratanteUsuario_ContratantePesCod = H000K7_A340ContratanteUsuario_ContratantePesCod[0];
               n340ContratanteUsuario_ContratantePesCod = H000K7_n340ContratanteUsuario_ContratantePesCod[0];
               A60ContratanteUsuario_UsuarioCod = H000K7_A60ContratanteUsuario_UsuarioCod[0];
               A63ContratanteUsuario_ContratanteCod = H000K7_A63ContratanteUsuario_ContratanteCod[0];
               A64ContratanteUsuario_ContratanteRaz = H000K7_A64ContratanteUsuario_ContratanteRaz[0];
               n64ContratanteUsuario_ContratanteRaz = H000K7_n64ContratanteUsuario_ContratanteRaz[0];
               A2Usuario_Nome = H000K7_A2Usuario_Nome[0];
               n2Usuario_Nome = H000K7_n2Usuario_Nome[0];
               A341Usuario_UserGamGuid = H000K7_A341Usuario_UserGamGuid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
               n341Usuario_UserGamGuid = H000K7_n341Usuario_UserGamGuid[0];
               A1728ContratanteUsuario_EhFiscal = H000K7_A1728ContratanteUsuario_EhFiscal[0];
               n1728ContratanteUsuario_EhFiscal = H000K7_n1728ContratanteUsuario_EhFiscal[0];
               A2Usuario_Nome = H000K7_A2Usuario_Nome[0];
               n2Usuario_Nome = H000K7_n2Usuario_Nome[0];
               A341Usuario_UserGamGuid = H000K7_A341Usuario_UserGamGuid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
               n341Usuario_UserGamGuid = H000K7_n341Usuario_UserGamGuid[0];
               A340ContratanteUsuario_ContratantePesCod = H000K7_A340ContratanteUsuario_ContratantePesCod[0];
               n340ContratanteUsuario_ContratantePesCod = H000K7_n340ContratanteUsuario_ContratantePesCod[0];
               A64ContratanteUsuario_ContratanteRaz = H000K7_A64ContratanteUsuario_ContratanteRaz[0];
               n64ContratanteUsuario_ContratanteRaz = H000K7_n64ContratanteUsuario_ContratanteRaz[0];
               AV12Entidade = A64ContratanteUsuario_ContratanteRaz;
               AV26Usuario_Nome = A2Usuario_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Usuario_Nome", AV26Usuario_Nome);
               AV31Usuario_UserGamGuid = A341Usuario_UserGamGuid;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31Usuario_UserGamGuid", AV31Usuario_UserGamGuid);
               AV14GamUser.load( A341Usuario_UserGamGuid);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
               AV13Email = AV14GamUser.gxTpr_Email;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Email", AV13Email);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vEMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV13Email, "@!"))));
               AV32ContratanteUsuario_EhFiscal = A1728ContratanteUsuario_EhFiscal;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ContratanteUsuario_EhFiscal", AV32ContratanteUsuario_EhFiscal);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTRATANTEUSUARIO_EHFISCAL", GetSecureSignedToken( sPrefix, AV32ContratanteUsuario_EhFiscal));
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(5);
         }
         if ( AV14GamUser.fail() )
         {
            /* Execute user subroutine: 'CONFERIRNOGAM' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E120K2( )
      {
         /* Load Routine */
         edtUsuario_CargoUONom_Link = formatLink("viewgeral_unidadeorganizacional.aspx") + "?" + UrlEncode("" +A1075Usuario_CargoUOCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtUsuario_CargoUONom_Internalname, "Link", edtUsuario_CargoUONom_Link);
         edtUsuario_CargoNom_Link = formatLink("viewgeral_cargo.aspx") + "?" + UrlEncode("" +A1073Usuario_CargoCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtUsuario_CargoNom_Internalname, "Link", edtUsuario_CargoNom_Link);
         edtUsuario_CargoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtUsuario_CargoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_CargoCod_Visible), 5, 0)));
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
         if ( ! ( ( AV15EhContratada_Flag ) ) )
         {
            edtavContratadausuario_cstuntprdnrm_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratadausuario_cstuntprdnrm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdnrm_Visible), 5, 0)));
            cellContratadausuario_cstuntprdnrm_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContratadausuario_cstuntprdnrm_cell_Internalname, "Class", cellContratadausuario_cstuntprdnrm_cell_Class);
            cellTextblockcontratadausuario_cstuntprdnrm_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellTextblockcontratadausuario_cstuntprdnrm_cell_Internalname, "Class", cellTextblockcontratadausuario_cstuntprdnrm_cell_Class);
         }
         else
         {
            edtavContratadausuario_cstuntprdnrm_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratadausuario_cstuntprdnrm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdnrm_Visible), 5, 0)));
            cellContratadausuario_cstuntprdnrm_cell_Class = "DataContentCellView";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContratadausuario_cstuntprdnrm_cell_Internalname, "Class", cellContratadausuario_cstuntprdnrm_cell_Class);
            cellTextblockcontratadausuario_cstuntprdnrm_cell_Class = "DataDescriptionCell";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellTextblockcontratadausuario_cstuntprdnrm_cell_Internalname, "Class", cellTextblockcontratadausuario_cstuntprdnrm_cell_Class);
         }
         if ( ! ( ( AV15EhContratada_Flag ) ) )
         {
            edtavContratadausuario_cstuntprdext_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratadausuario_cstuntprdext_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdext_Visible), 5, 0)));
            cellContratadausuario_cstuntprdext_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContratadausuario_cstuntprdext_cell_Internalname, "Class", cellContratadausuario_cstuntprdext_cell_Class);
            cellTextblockcontratadausuario_cstuntprdext_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellTextblockcontratadausuario_cstuntprdext_cell_Internalname, "Class", cellTextblockcontratadausuario_cstuntprdext_cell_Class);
         }
         else
         {
            edtavContratadausuario_cstuntprdext_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratadausuario_cstuntprdext_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdext_Visible), 5, 0)));
            cellContratadausuario_cstuntprdext_cell_Class = "DataContentCellView";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContratadausuario_cstuntprdext_cell_Internalname, "Class", cellContratadausuario_cstuntprdext_cell_Class);
            cellTextblockcontratadausuario_cstuntprdext_cell_Class = "DataDescriptionCell";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellTextblockcontratadausuario_cstuntprdext_cell_Internalname, "Class", cellTextblockcontratadausuario_cstuntprdext_cell_Class);
         }
      }

      protected void E130K2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("usuario.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1Usuario_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E140K2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("usuario.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1Usuario_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E150K2( )
      {
         /* 'DoFechar' Routine */
         AV34Caller = AV16Websession.Get("Caller");
         if ( StringUtil.StrCmp(StringUtil.Substring( AV34Caller, 1, 3), "Ntf") == 0 )
         {
            AV33Codigo = (int)(NumberUtil.Val( AV16Websession.Get("CodigoNtf"), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Codigo), 6, 0)));
            AV16Websession.Remove("CodigoNtf");
            AV16Websession.Remove("Caller");
            if ( StringUtil.StrCmp(StringUtil.Substring( AV34Caller, 4, 3), "Dst") == 0 )
            {
               context.wjLoc = formatLink("viewcontagemresultadonotificacao.aspx") + "?" + UrlEncode("" +AV33Codigo) + "," + UrlEncode(StringUtil.RTrim("Destinatario"));
               context.wjLocDisableFrm = 1;
            }
            else if ( StringUtil.StrCmp(StringUtil.Substring( AV34Caller, 4, 3), "Dds") == 0 )
            {
               context.wjLoc = formatLink("viewcontagemresultadonotificacao.aspx") + "?" + UrlEncode("" +AV33Codigo) + "," + UrlEncode(StringUtil.RTrim("General"));
               context.wjLocDisableFrm = 1;
            }
            else if ( StringUtil.StrCmp(StringUtil.Substring( AV34Caller, 4, 3), "Dmn") == 0 )
            {
               context.wjLoc = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV33Codigo) + "," + UrlEncode(StringUtil.RTrim("Notificacoes"));
               context.wjLocDisableFrm = 1;
            }
            else
            {
               context.wjLoc = formatLink("wwcontagemresultadonotificacao.aspx") ;
               context.wjLocDisableFrm = 1;
            }
         }
         else if ( AV15EhContratada_Flag )
         {
            context.wjLoc = formatLink("viewcontratada.aspx") + "?" + UrlEncode("" +AV17Entidade_Codigo) + "," + UrlEncode(StringUtil.RTrim("ContratadaUsuario"));
            context.wjLocDisableFrm = 1;
         }
         else
         {
            context.wjLoc = formatLink("viewcontratante.aspx") + "?" + UrlEncode("" +AV17Entidade_Codigo) + "," + UrlEncode(StringUtil.RTrim("ContratanteUsuario"));
            context.wjLocDisableFrm = 1;
         }
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV43Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Usuario";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Usuario_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void S122( )
      {
         /* 'CONFERIRNOGAM' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV26Usuario_Nome)) )
         {
            /* Execute user subroutine: 'INSERTUSUARIONOGAM' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else
         {
            AV21GAMUserFilter.gxTpr_Name = AV26Usuario_Nome;
            AV30Usuarios = new SdtGAMRepository(context).getusers(AV21GAMUserFilter, out  AV28GamError);
            if ( ( AV30Usuarios.Count == 0 ) || ( StringUtil.StrCmp(((SdtGAMUser)AV30Usuarios.Item(1)).gxTpr_Name, AV26Usuario_Nome) != 0 ) )
            {
               /* Execute user subroutine: 'INSERTUSUARIONOGAM' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(AV31Usuario_UserGamGuid, ((SdtGAMUser)AV30Usuarios.Item(1)).gxTpr_Guid) != 0 )
               {
                  new prc_updusuario_usergamguid(context ).execute( ref  A1Usuario_Codigo,  ((SdtGAMUser)AV30Usuarios.Item(1)).gxTpr_Guid) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               }
            }
         }
      }

      protected void S132( )
      {
         /* 'INSERTUSUARIONOGAM' Routine */
         new prc_exportusertogam(context ).execute( ref  A1Usuario_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
      }

      protected void wb_table1_2_0K2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_0K2( true) ;
         }
         else
         {
            wb_table2_8_0K2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_0K2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:bottom")+"\">") ;
            wb_table3_111_0K2( true) ;
         }
         else
         {
            wb_table3_111_0K2( false) ;
         }
         return  ;
      }

      protected void wb_table3_111_0K2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0K2e( true) ;
         }
         else
         {
            wb_table1_2_0K2e( false) ;
         }
      }

      protected void wb_table3_111_0K2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_111_0K2e( true) ;
         }
         else
         {
            wb_table3_111_0K2e( false) ;
         }
      }

      protected void wb_table2_8_0K2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_cargouonom_Internalname, "U.Organizacional", "", "", lblTextblockusuario_cargouonom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_CargoUONom_Internalname, StringUtil.RTrim( A1076Usuario_CargoUONom), StringUtil.RTrim( context.localUtil.Format( A1076Usuario_CargoUONom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtUsuario_CargoUONom_Link, "", "", "", edtUsuario_CargoUONom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_cargonom_Internalname, "Cargo", "", "", lblTextblockusuario_cargonom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_CargoNom_Internalname, A1074Usuario_CargoNom, StringUtil.RTrim( context.localUtil.Format( A1074Usuario_CargoNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtUsuario_CargoNom_Link, "", "", "", edtUsuario_CargoNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"5\"  class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_foto_Internalname, "Foto", "", "", lblTextblockusuario_foto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"5\"  class='DataContentCellView'>") ;
            /* Static Bitmap Variable */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            A1716Usuario_Foto_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto))&&String.IsNullOrEmpty(StringUtil.RTrim( A40000Usuario_Foto_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)));
            GxWebStd.gx_bitmap( context, imgUsuario_Foto_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)) ? A40000Usuario_Foto_GXI : context.PathToRelativeUrl( A1716Usuario_Foto)), "", "", "", context.GetTheme( ), 1, 0, "", "", 1, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, A1716Usuario_Foto_IsBlob, true, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_entidade_Internalname, "Entidade", "", "", lblTextblockusuario_entidade_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_Entidade_Internalname, StringUtil.RTrim( A1083Usuario_Entidade), StringUtil.RTrim( context.localUtil.Format( A1083Usuario_Entidade, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_Entidade_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockemail_Internalname, "Email", "", "", lblTextblockemail_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEmail_Internalname, AV13Email, StringUtil.RTrim( context.localUtil.Format( AV13Email, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,30);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEmail_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, edtavEmail_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_pessoanom_Internalname, "Pessoa", "", "", lblTextblockusuario_pessoanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_PessoaNom_Internalname, StringUtil.RTrim( A58Usuario_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A58Usuario_PessoaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_PessoaNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_pessoatip_Internalname, "Tipo da Pessoa", "", "", lblTextblockusuario_pessoatip_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbUsuario_PessoaTip, cmbUsuario_PessoaTip_Internalname, StringUtil.RTrim( A59Usuario_PessoaTip), 1, cmbUsuario_PessoaTip_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_UsuarioGeneral.htm");
            cmbUsuario_PessoaTip.CurrentValue = StringUtil.RTrim( A59Usuario_PessoaTip);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbUsuario_PessoaTip_Internalname, "Values", (String)(cmbUsuario_PessoaTip.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_pessoadoc_Internalname, "CPF", "", "", lblTextblockusuario_pessoadoc_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_PessoaDoc_Internalname, A325Usuario_PessoaDoc, StringUtil.RTrim( context.localUtil.Format( A325Usuario_PessoaDoc, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_PessoaDoc_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_ehcontador_Internalname, "Contador?", "", "", lblTextblockusuario_ehcontador_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkUsuario_EhContador_Internalname, StringUtil.BoolToStr( A289Usuario_EhContador), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_ehauditorfm_Internalname, "Auditor FM?", "", "", lblTextblockusuario_ehauditorfm_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkUsuario_EhAuditorFM_Internalname, StringUtil.BoolToStr( A290Usuario_EhAuditorFM), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_ehfinanceiro_Internalname, "Financeiro?", "", "", lblTextblockusuario_ehfinanceiro_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkUsuario_EhFinanceiro_Internalname, StringUtil.BoolToStr( A293Usuario_EhFinanceiro), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_ehcontratada_Internalname, "Contratada?", "", "", lblTextblockusuario_ehcontratada_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkUsuario_EhContratada_Internalname, StringUtil.BoolToStr( A291Usuario_EhContratada), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_ehcontratante_Internalname, "Contratante?", "", "", lblTextblockusuario_ehcontratante_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkUsuario_EhContratante_Internalname, StringUtil.BoolToStr( A292Usuario_EhContratante), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratanteusuario_ehfiscal_Internalname, "Fiscal?", "", "", lblTextblockcontratanteusuario_ehfiscal_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavContratanteusuario_ehfiscal_Internalname, StringUtil.BoolToStr( AV32ContratanteUsuario_EhFiscal), "", "", 1, chkavContratanteusuario_ehfiscal.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(71, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_crtfpath_Internalname, "Certificado Digital", "", "", lblTextblockusuario_crtfpath_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_CrtfPath_Internalname, A1017Usuario_CrtfPath, StringUtil.RTrim( context.localUtil.Format( A1017Usuario_CrtfPath, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "(path + file name)", edtUsuario_CrtfPath_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_usergamguid_Internalname, "Guid", "", "", lblTextblockusuario_usergamguid_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_UserGamGuid_Internalname, StringUtil.RTrim( A341Usuario_UserGamGuid), StringUtil.RTrim( context.localUtil.Format( A341Usuario_UserGamGuid, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_UserGamGuid_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, 0, true, "GAMGUID", "left", true, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontratadausuario_cstuntprdnrm_cell_Internalname+"\"  class='"+cellTextblockcontratadausuario_cstuntprdnrm_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratadausuario_cstuntprdnrm_Internalname, "Custo Unit. (Normal)", "", "", lblTextblockcontratadausuario_cstuntprdnrm_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratadausuario_cstuntprdnrm_cell_Internalname+"\"  class='"+cellContratadausuario_cstuntprdnrm_cell_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratadausuario_cstuntprdnrm_Internalname, StringUtil.LTrim( StringUtil.NToC( AV19ContratadaUsuario_CstUntPrdNrm, 18, 5, ",", "")), ((edtavContratadausuario_cstuntprdnrm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV19ContratadaUsuario_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV19ContratadaUsuario_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,89);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratadausuario_cstuntprdnrm_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", edtavContratadausuario_cstuntprdnrm_Visible, edtavContratadausuario_cstuntprdnrm_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontratadausuario_cstuntprdext_cell_Internalname+"\"  class='"+cellTextblockcontratadausuario_cstuntprdext_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratadausuario_cstuntprdext_Internalname, "Custo Unit. (/extra)", "", "", lblTextblockcontratadausuario_cstuntprdext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratadausuario_cstuntprdext_cell_Internalname+"\"  class='"+cellContratadausuario_cstuntprdext_cell_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratadausuario_cstuntprdext_Internalname, StringUtil.LTrim( StringUtil.NToC( AV20ContratadaUsuario_CstUntPrdExt, 18, 5, ",", "")), ((edtavContratadausuario_cstuntprdext_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV20ContratadaUsuario_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV20ContratadaUsuario_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,93);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratadausuario_cstuntprdext_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", edtavContratadausuario_cstuntprdext_Visible, edtavContratadausuario_cstuntprdext_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_ativo_Internalname, "Ativo?", "", "", lblTextblockusuario_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkUsuario_Ativo_Internalname, StringUtil.BoolToStr( A54Usuario_Ativo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_0K2e( true) ;
         }
         else
         {
            wb_table2_8_0K2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1Usuario_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0K2( ) ;
         WS0K2( ) ;
         WE0K2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1Usuario_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA0K2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "usuariogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA0K2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1Usuario_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         }
         wcpOA1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1Usuario_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1Usuario_Codigo != wcpOA1Usuario_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1Usuario_Codigo = A1Usuario_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1Usuario_Codigo = cgiGet( sPrefix+"A1Usuario_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1Usuario_Codigo) > 0 )
         {
            A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA1Usuario_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         }
         else
         {
            A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1Usuario_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA0K2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS0K2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS0K2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1Usuario_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1Usuario_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1Usuario_Codigo_CTRL", StringUtil.RTrim( sCtrlA1Usuario_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE0K2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202053021262327");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("usuariogeneral.js", "?202053021262329");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockusuario_cargouonom_Internalname = sPrefix+"TEXTBLOCKUSUARIO_CARGOUONOM";
         edtUsuario_CargoUONom_Internalname = sPrefix+"USUARIO_CARGOUONOM";
         lblTextblockusuario_cargonom_Internalname = sPrefix+"TEXTBLOCKUSUARIO_CARGONOM";
         edtUsuario_CargoNom_Internalname = sPrefix+"USUARIO_CARGONOM";
         lblTextblockusuario_foto_Internalname = sPrefix+"TEXTBLOCKUSUARIO_FOTO";
         imgUsuario_Foto_Internalname = sPrefix+"USUARIO_FOTO";
         lblTextblockusuario_entidade_Internalname = sPrefix+"TEXTBLOCKUSUARIO_ENTIDADE";
         edtUsuario_Entidade_Internalname = sPrefix+"USUARIO_ENTIDADE";
         lblTextblockemail_Internalname = sPrefix+"TEXTBLOCKEMAIL";
         edtavEmail_Internalname = sPrefix+"vEMAIL";
         lblTextblockusuario_pessoanom_Internalname = sPrefix+"TEXTBLOCKUSUARIO_PESSOANOM";
         edtUsuario_PessoaNom_Internalname = sPrefix+"USUARIO_PESSOANOM";
         lblTextblockusuario_pessoatip_Internalname = sPrefix+"TEXTBLOCKUSUARIO_PESSOATIP";
         cmbUsuario_PessoaTip_Internalname = sPrefix+"USUARIO_PESSOATIP";
         lblTextblockusuario_pessoadoc_Internalname = sPrefix+"TEXTBLOCKUSUARIO_PESSOADOC";
         edtUsuario_PessoaDoc_Internalname = sPrefix+"USUARIO_PESSOADOC";
         lblTextblockusuario_ehcontador_Internalname = sPrefix+"TEXTBLOCKUSUARIO_EHCONTADOR";
         chkUsuario_EhContador_Internalname = sPrefix+"USUARIO_EHCONTADOR";
         lblTextblockusuario_ehauditorfm_Internalname = sPrefix+"TEXTBLOCKUSUARIO_EHAUDITORFM";
         chkUsuario_EhAuditorFM_Internalname = sPrefix+"USUARIO_EHAUDITORFM";
         lblTextblockusuario_ehfinanceiro_Internalname = sPrefix+"TEXTBLOCKUSUARIO_EHFINANCEIRO";
         chkUsuario_EhFinanceiro_Internalname = sPrefix+"USUARIO_EHFINANCEIRO";
         lblTextblockusuario_ehcontratada_Internalname = sPrefix+"TEXTBLOCKUSUARIO_EHCONTRATADA";
         chkUsuario_EhContratada_Internalname = sPrefix+"USUARIO_EHCONTRATADA";
         lblTextblockusuario_ehcontratante_Internalname = sPrefix+"TEXTBLOCKUSUARIO_EHCONTRATANTE";
         chkUsuario_EhContratante_Internalname = sPrefix+"USUARIO_EHCONTRATANTE";
         lblTextblockcontratanteusuario_ehfiscal_Internalname = sPrefix+"TEXTBLOCKCONTRATANTEUSUARIO_EHFISCAL";
         chkavContratanteusuario_ehfiscal_Internalname = sPrefix+"vCONTRATANTEUSUARIO_EHFISCAL";
         lblTextblockusuario_crtfpath_Internalname = sPrefix+"TEXTBLOCKUSUARIO_CRTFPATH";
         edtUsuario_CrtfPath_Internalname = sPrefix+"USUARIO_CRTFPATH";
         lblTextblockusuario_usergamguid_Internalname = sPrefix+"TEXTBLOCKUSUARIO_USERGAMGUID";
         edtUsuario_UserGamGuid_Internalname = sPrefix+"USUARIO_USERGAMGUID";
         lblTextblockcontratadausuario_cstuntprdnrm_Internalname = sPrefix+"TEXTBLOCKCONTRATADAUSUARIO_CSTUNTPRDNRM";
         cellTextblockcontratadausuario_cstuntprdnrm_cell_Internalname = sPrefix+"TEXTBLOCKCONTRATADAUSUARIO_CSTUNTPRDNRM_CELL";
         edtavContratadausuario_cstuntprdnrm_Internalname = sPrefix+"vCONTRATADAUSUARIO_CSTUNTPRDNRM";
         cellContratadausuario_cstuntprdnrm_cell_Internalname = sPrefix+"CONTRATADAUSUARIO_CSTUNTPRDNRM_CELL";
         lblTextblockcontratadausuario_cstuntprdext_Internalname = sPrefix+"TEXTBLOCKCONTRATADAUSUARIO_CSTUNTPRDEXT";
         cellTextblockcontratadausuario_cstuntprdext_cell_Internalname = sPrefix+"TEXTBLOCKCONTRATADAUSUARIO_CSTUNTPRDEXT_CELL";
         edtavContratadausuario_cstuntprdext_Internalname = sPrefix+"vCONTRATADAUSUARIO_CSTUNTPRDEXT";
         cellContratadausuario_cstuntprdext_cell_Internalname = sPrefix+"CONTRATADAUSUARIO_CSTUNTPRDEXT_CELL";
         lblTextblockusuario_ativo_Internalname = sPrefix+"TEXTBLOCKUSUARIO_ATIVO";
         chkUsuario_Ativo_Internalname = sPrefix+"USUARIO_ATIVO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtUsuario_CargoCod_Internalname = sPrefix+"USUARIO_CARGOCOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavContratadausuario_cstuntprdext_Jsonclick = "";
         edtavContratadausuario_cstuntprdext_Enabled = 1;
         cellContratadausuario_cstuntprdext_cell_Class = "";
         cellTextblockcontratadausuario_cstuntprdext_cell_Class = "";
         edtavContratadausuario_cstuntprdnrm_Jsonclick = "";
         edtavContratadausuario_cstuntprdnrm_Enabled = 1;
         cellContratadausuario_cstuntprdnrm_cell_Class = "";
         cellTextblockcontratadausuario_cstuntprdnrm_cell_Class = "";
         edtUsuario_UserGamGuid_Jsonclick = "";
         edtUsuario_CrtfPath_Jsonclick = "";
         chkavContratanteusuario_ehfiscal.Enabled = 1;
         edtUsuario_PessoaDoc_Jsonclick = "";
         cmbUsuario_PessoaTip_Jsonclick = "";
         edtUsuario_PessoaNom_Jsonclick = "";
         edtavEmail_Jsonclick = "";
         edtavEmail_Enabled = 1;
         edtUsuario_Entidade_Jsonclick = "";
         edtUsuario_CargoNom_Jsonclick = "";
         edtUsuario_CargoUONom_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         edtavContratadausuario_cstuntprdext_Visible = 1;
         edtavContratadausuario_cstuntprdnrm_Visible = 1;
         edtUsuario_CargoNom_Link = "";
         edtUsuario_CargoUONom_Link = "";
         chkUsuario_Ativo.Caption = "";
         chkavContratanteusuario_ehfiscal.Caption = "";
         chkUsuario_EhContratante.Caption = "";
         chkUsuario_EhContratada.Caption = "";
         chkUsuario_EhFinanceiro.Caption = "";
         chkUsuario_EhAuditorFM.Caption = "";
         chkUsuario_EhContador.Caption = "";
         edtUsuario_CargoCod_Jsonclick = "";
         edtUsuario_CargoCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E130K2',iparms:[{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E140K2',iparms:[{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E150K2',iparms:[{av:'AV33Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15EhContratada_Flag',fld:'vEHCONTRATADA_FLAG',pic:'',nv:false},{av:'AV17Entidade_Codigo',fld:'vENTIDADE_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV33Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV43Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1083Usuario_Entidade = "";
         AV13Email = "";
         A1017Usuario_CrtfPath = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A59Usuario_PessoaTip = "";
         scmdbuf = "";
         H000K2_A57Usuario_PessoaCod = new int[1] ;
         H000K2_A1075Usuario_CargoUOCod = new int[1] ;
         H000K2_n1075Usuario_CargoUOCod = new bool[] {false} ;
         H000K2_A1073Usuario_CargoCod = new int[1] ;
         H000K2_n1073Usuario_CargoCod = new bool[] {false} ;
         H000K2_A54Usuario_Ativo = new bool[] {false} ;
         H000K2_A341Usuario_UserGamGuid = new String[] {""} ;
         H000K2_n341Usuario_UserGamGuid = new bool[] {false} ;
         H000K2_A1017Usuario_CrtfPath = new String[] {""} ;
         H000K2_n1017Usuario_CrtfPath = new bool[] {false} ;
         H000K2_A292Usuario_EhContratante = new bool[] {false} ;
         H000K2_A291Usuario_EhContratada = new bool[] {false} ;
         H000K2_A293Usuario_EhFinanceiro = new bool[] {false} ;
         H000K2_A289Usuario_EhContador = new bool[] {false} ;
         H000K2_A325Usuario_PessoaDoc = new String[] {""} ;
         H000K2_n325Usuario_PessoaDoc = new bool[] {false} ;
         H000K2_A59Usuario_PessoaTip = new String[] {""} ;
         H000K2_n59Usuario_PessoaTip = new bool[] {false} ;
         H000K2_A58Usuario_PessoaNom = new String[] {""} ;
         H000K2_n58Usuario_PessoaNom = new bool[] {false} ;
         H000K2_A40000Usuario_Foto_GXI = new String[] {""} ;
         H000K2_n40000Usuario_Foto_GXI = new bool[] {false} ;
         H000K2_A1074Usuario_CargoNom = new String[] {""} ;
         H000K2_n1074Usuario_CargoNom = new bool[] {false} ;
         H000K2_A1076Usuario_CargoUONom = new String[] {""} ;
         H000K2_n1076Usuario_CargoUONom = new bool[] {false} ;
         H000K2_A1Usuario_Codigo = new int[1] ;
         H000K2_A1716Usuario_Foto = new String[] {""} ;
         H000K2_n1716Usuario_Foto = new bool[] {false} ;
         A341Usuario_UserGamGuid = "";
         A325Usuario_PessoaDoc = "";
         A58Usuario_PessoaNom = "";
         A40000Usuario_Foto_GXI = "";
         A1716Usuario_Foto = "";
         A1074Usuario_CargoNom = "";
         A1076Usuario_CargoUONom = "";
         GXt_char1 = "";
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         Gx_msg = "";
         AV16Websession = context.GetSession();
         H000K3_A5AreaTrabalho_Codigo = new int[1] ;
         H000K3_A29Contratante_Codigo = new int[1] ;
         H000K3_n29Contratante_Codigo = new bool[] {false} ;
         H000K4_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H000K4_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H000K5_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H000K5_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H000K5_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H000K5_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H000K6_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         H000K6_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         H000K6_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H000K6_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H000K6_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         H000K6_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         H000K6_A2Usuario_Nome = new String[] {""} ;
         H000K6_n2Usuario_Nome = new bool[] {false} ;
         H000K6_A341Usuario_UserGamGuid = new String[] {""} ;
         H000K6_n341Usuario_UserGamGuid = new bool[] {false} ;
         H000K6_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         H000K6_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         H000K6_A577ContratadaUsuario_CstUntPrdExt = new decimal[1] ;
         H000K6_n577ContratadaUsuario_CstUntPrdExt = new bool[] {false} ;
         A68ContratadaUsuario_ContratadaPessoaNom = "";
         A2Usuario_Nome = "";
         AV12Entidade = "";
         AV26Usuario_Nome = "";
         AV31Usuario_UserGamGuid = "";
         AV14GamUser = new SdtGAMUser(context);
         H000K7_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         H000K7_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         H000K7_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H000K7_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H000K7_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         H000K7_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         H000K7_A2Usuario_Nome = new String[] {""} ;
         H000K7_n2Usuario_Nome = new bool[] {false} ;
         H000K7_A341Usuario_UserGamGuid = new String[] {""} ;
         H000K7_n341Usuario_UserGamGuid = new bool[] {false} ;
         H000K7_A1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         H000K7_n1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         A64ContratanteUsuario_ContratanteRaz = "";
         AV34Caller = "";
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         AV21GAMUserFilter = new SdtGAMUserFilter(context);
         AV30Usuarios = new GxExternalCollection( context, "SdtGAMUser", "GeneXus.Programs");
         AV28GamError = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockusuario_cargouonom_Jsonclick = "";
         lblTextblockusuario_cargonom_Jsonclick = "";
         lblTextblockusuario_foto_Jsonclick = "";
         lblTextblockusuario_entidade_Jsonclick = "";
         lblTextblockemail_Jsonclick = "";
         lblTextblockusuario_pessoanom_Jsonclick = "";
         lblTextblockusuario_pessoatip_Jsonclick = "";
         lblTextblockusuario_pessoadoc_Jsonclick = "";
         lblTextblockusuario_ehcontador_Jsonclick = "";
         lblTextblockusuario_ehauditorfm_Jsonclick = "";
         lblTextblockusuario_ehfinanceiro_Jsonclick = "";
         lblTextblockusuario_ehcontratada_Jsonclick = "";
         lblTextblockusuario_ehcontratante_Jsonclick = "";
         lblTextblockcontratanteusuario_ehfiscal_Jsonclick = "";
         lblTextblockusuario_crtfpath_Jsonclick = "";
         lblTextblockusuario_usergamguid_Jsonclick = "";
         lblTextblockcontratadausuario_cstuntprdnrm_Jsonclick = "";
         lblTextblockcontratadausuario_cstuntprdext_Jsonclick = "";
         lblTextblockusuario_ativo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1Usuario_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.usuariogeneral__default(),
            new Object[][] {
                new Object[] {
               H000K2_A57Usuario_PessoaCod, H000K2_A1075Usuario_CargoUOCod, H000K2_n1075Usuario_CargoUOCod, H000K2_A1073Usuario_CargoCod, H000K2_n1073Usuario_CargoCod, H000K2_A54Usuario_Ativo, H000K2_A341Usuario_UserGamGuid, H000K2_A1017Usuario_CrtfPath, H000K2_n1017Usuario_CrtfPath, H000K2_A292Usuario_EhContratante,
               H000K2_A291Usuario_EhContratada, H000K2_A293Usuario_EhFinanceiro, H000K2_A289Usuario_EhContador, H000K2_A325Usuario_PessoaDoc, H000K2_n325Usuario_PessoaDoc, H000K2_A59Usuario_PessoaTip, H000K2_n59Usuario_PessoaTip, H000K2_A58Usuario_PessoaNom, H000K2_n58Usuario_PessoaNom, H000K2_A40000Usuario_Foto_GXI,
               H000K2_n40000Usuario_Foto_GXI, H000K2_A1074Usuario_CargoNom, H000K2_n1074Usuario_CargoNom, H000K2_A1076Usuario_CargoUONom, H000K2_n1076Usuario_CargoUONom, H000K2_A1Usuario_Codigo, H000K2_A1716Usuario_Foto, H000K2_n1716Usuario_Foto
               }
               , new Object[] {
               H000K3_A5AreaTrabalho_Codigo, H000K3_A29Contratante_Codigo, H000K3_n29Contratante_Codigo
               }
               , new Object[] {
               H000K4_A63ContratanteUsuario_ContratanteCod, H000K4_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               H000K5_A69ContratadaUsuario_UsuarioCod, H000K5_A1228ContratadaUsuario_AreaTrabalhoCod, H000K5_n1228ContratadaUsuario_AreaTrabalhoCod, H000K5_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H000K6_A67ContratadaUsuario_ContratadaPessoaCod, H000K6_n67ContratadaUsuario_ContratadaPessoaCod, H000K6_A69ContratadaUsuario_UsuarioCod, H000K6_A66ContratadaUsuario_ContratadaCod, H000K6_A68ContratadaUsuario_ContratadaPessoaNom, H000K6_n68ContratadaUsuario_ContratadaPessoaNom, H000K6_A2Usuario_Nome, H000K6_n2Usuario_Nome, H000K6_A341Usuario_UserGamGuid, H000K6_n341Usuario_UserGamGuid,
               H000K6_A576ContratadaUsuario_CstUntPrdNrm, H000K6_n576ContratadaUsuario_CstUntPrdNrm, H000K6_A577ContratadaUsuario_CstUntPrdExt, H000K6_n577ContratadaUsuario_CstUntPrdExt
               }
               , new Object[] {
               H000K7_A340ContratanteUsuario_ContratantePesCod, H000K7_n340ContratanteUsuario_ContratantePesCod, H000K7_A60ContratanteUsuario_UsuarioCod, H000K7_A63ContratanteUsuario_ContratanteCod, H000K7_A64ContratanteUsuario_ContratanteRaz, H000K7_n64ContratanteUsuario_ContratanteRaz, H000K7_A2Usuario_Nome, H000K7_n2Usuario_Nome, H000K7_A341Usuario_UserGamGuid, H000K7_n341Usuario_UserGamGuid,
               H000K7_A1728ContratanteUsuario_EhFiscal, H000K7_n1728ContratanteUsuario_EhFiscal
               }
            }
         );
         AV43Pgmname = "UsuarioGeneral";
         /* GeneXus formulas. */
         AV43Pgmname = "UsuarioGeneral";
         context.Gx_err = 0;
         edtavEmail_Enabled = 0;
         chkavContratanteusuario_ehfiscal.Enabled = 0;
         edtavContratadausuario_cstuntprdnrm_Enabled = 0;
         edtavContratadausuario_cstuntprdext_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV39GXLvl17 ;
      private short nGXWrapped ;
      private int A1Usuario_Codigo ;
      private int wcpOA1Usuario_Codigo ;
      private int edtavEmail_Enabled ;
      private int edtavContratadausuario_cstuntprdnrm_Enabled ;
      private int edtavContratadausuario_cstuntprdext_Enabled ;
      private int AV33Codigo ;
      private int AV17Entidade_Codigo ;
      private int A1073Usuario_CargoCod ;
      private int edtUsuario_CargoCod_Visible ;
      private int A57Usuario_PessoaCod ;
      private int A1075Usuario_CargoUOCod ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A67ContratadaUsuario_ContratadaPessoaCod ;
      private int A340ContratanteUsuario_ContratantePesCod ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int edtavContratadausuario_cstuntprdnrm_Visible ;
      private int edtavContratadausuario_cstuntprdext_Visible ;
      private int AV7Usuario_Codigo ;
      private int idxLst ;
      private decimal AV19ContratadaUsuario_CstUntPrdNrm ;
      private decimal AV20ContratadaUsuario_CstUntPrdExt ;
      private decimal A576ContratadaUsuario_CstUntPrdNrm ;
      private decimal A577ContratadaUsuario_CstUntPrdExt ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV43Pgmname ;
      private String edtavEmail_Internalname ;
      private String chkavContratanteusuario_ehfiscal_Internalname ;
      private String edtavContratadausuario_cstuntprdnrm_Internalname ;
      private String edtavContratadausuario_cstuntprdext_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A1083Usuario_Entidade ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String edtUsuario_CargoCod_Internalname ;
      private String edtUsuario_CargoCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String A59Usuario_PessoaTip ;
      private String chkUsuario_EhContador_Internalname ;
      private String chkUsuario_EhAuditorFM_Internalname ;
      private String chkUsuario_EhFinanceiro_Internalname ;
      private String chkUsuario_EhContratada_Internalname ;
      private String chkUsuario_EhContratante_Internalname ;
      private String chkUsuario_Ativo_Internalname ;
      private String scmdbuf ;
      private String A341Usuario_UserGamGuid ;
      private String A58Usuario_PessoaNom ;
      private String imgUsuario_Foto_Internalname ;
      private String A1076Usuario_CargoUONom ;
      private String GXt_char1 ;
      private String edtUsuario_CargoUONom_Internalname ;
      private String edtUsuario_CargoNom_Internalname ;
      private String edtUsuario_Entidade_Internalname ;
      private String edtUsuario_PessoaNom_Internalname ;
      private String cmbUsuario_PessoaTip_Internalname ;
      private String edtUsuario_PessoaDoc_Internalname ;
      private String edtUsuario_CrtfPath_Internalname ;
      private String edtUsuario_UserGamGuid_Internalname ;
      private String hsh ;
      private String Gx_msg ;
      private String A68ContratadaUsuario_ContratadaPessoaNom ;
      private String A2Usuario_Nome ;
      private String AV12Entidade ;
      private String AV26Usuario_Nome ;
      private String AV31Usuario_UserGamGuid ;
      private String A64ContratanteUsuario_ContratanteRaz ;
      private String edtUsuario_CargoUONom_Link ;
      private String edtUsuario_CargoNom_Link ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String cellContratadausuario_cstuntprdnrm_cell_Class ;
      private String cellContratadausuario_cstuntprdnrm_cell_Internalname ;
      private String cellTextblockcontratadausuario_cstuntprdnrm_cell_Class ;
      private String cellTextblockcontratadausuario_cstuntprdnrm_cell_Internalname ;
      private String cellContratadausuario_cstuntprdext_cell_Class ;
      private String cellContratadausuario_cstuntprdext_cell_Internalname ;
      private String cellTextblockcontratadausuario_cstuntprdext_cell_Class ;
      private String cellTextblockcontratadausuario_cstuntprdext_cell_Internalname ;
      private String AV34Caller ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockusuario_cargouonom_Internalname ;
      private String lblTextblockusuario_cargouonom_Jsonclick ;
      private String edtUsuario_CargoUONom_Jsonclick ;
      private String lblTextblockusuario_cargonom_Internalname ;
      private String lblTextblockusuario_cargonom_Jsonclick ;
      private String edtUsuario_CargoNom_Jsonclick ;
      private String lblTextblockusuario_foto_Internalname ;
      private String lblTextblockusuario_foto_Jsonclick ;
      private String lblTextblockusuario_entidade_Internalname ;
      private String lblTextblockusuario_entidade_Jsonclick ;
      private String edtUsuario_Entidade_Jsonclick ;
      private String lblTextblockemail_Internalname ;
      private String lblTextblockemail_Jsonclick ;
      private String edtavEmail_Jsonclick ;
      private String lblTextblockusuario_pessoanom_Internalname ;
      private String lblTextblockusuario_pessoanom_Jsonclick ;
      private String edtUsuario_PessoaNom_Jsonclick ;
      private String lblTextblockusuario_pessoatip_Internalname ;
      private String lblTextblockusuario_pessoatip_Jsonclick ;
      private String cmbUsuario_PessoaTip_Jsonclick ;
      private String lblTextblockusuario_pessoadoc_Internalname ;
      private String lblTextblockusuario_pessoadoc_Jsonclick ;
      private String edtUsuario_PessoaDoc_Jsonclick ;
      private String lblTextblockusuario_ehcontador_Internalname ;
      private String lblTextblockusuario_ehcontador_Jsonclick ;
      private String lblTextblockusuario_ehauditorfm_Internalname ;
      private String lblTextblockusuario_ehauditorfm_Jsonclick ;
      private String lblTextblockusuario_ehfinanceiro_Internalname ;
      private String lblTextblockusuario_ehfinanceiro_Jsonclick ;
      private String lblTextblockusuario_ehcontratada_Internalname ;
      private String lblTextblockusuario_ehcontratada_Jsonclick ;
      private String lblTextblockusuario_ehcontratante_Internalname ;
      private String lblTextblockusuario_ehcontratante_Jsonclick ;
      private String lblTextblockcontratanteusuario_ehfiscal_Internalname ;
      private String lblTextblockcontratanteusuario_ehfiscal_Jsonclick ;
      private String lblTextblockusuario_crtfpath_Internalname ;
      private String lblTextblockusuario_crtfpath_Jsonclick ;
      private String edtUsuario_CrtfPath_Jsonclick ;
      private String lblTextblockusuario_usergamguid_Internalname ;
      private String lblTextblockusuario_usergamguid_Jsonclick ;
      private String edtUsuario_UserGamGuid_Jsonclick ;
      private String lblTextblockcontratadausuario_cstuntprdnrm_Internalname ;
      private String lblTextblockcontratadausuario_cstuntprdnrm_Jsonclick ;
      private String edtavContratadausuario_cstuntprdnrm_Jsonclick ;
      private String lblTextblockcontratadausuario_cstuntprdext_Internalname ;
      private String lblTextblockcontratadausuario_cstuntprdext_Jsonclick ;
      private String edtavContratadausuario_cstuntprdext_Jsonclick ;
      private String lblTextblockusuario_ativo_Internalname ;
      private String lblTextblockusuario_ativo_Jsonclick ;
      private String sCtrlA1Usuario_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV15EhContratada_Flag ;
      private bool A289Usuario_EhContador ;
      private bool A290Usuario_EhAuditorFM ;
      private bool A293Usuario_EhFinanceiro ;
      private bool A291Usuario_EhContratada ;
      private bool A292Usuario_EhContratante ;
      private bool AV32ContratanteUsuario_EhFiscal ;
      private bool A54Usuario_Ativo ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n59Usuario_PessoaTip ;
      private bool n1075Usuario_CargoUOCod ;
      private bool n1073Usuario_CargoCod ;
      private bool n341Usuario_UserGamGuid ;
      private bool n1017Usuario_CrtfPath ;
      private bool n325Usuario_PessoaDoc ;
      private bool n58Usuario_PessoaNom ;
      private bool n40000Usuario_Foto_GXI ;
      private bool n1074Usuario_CargoNom ;
      private bool n1076Usuario_CargoUONom ;
      private bool n1716Usuario_Foto ;
      private bool GXt_boolean2 ;
      private bool returnInSub ;
      private bool n29Contratante_Codigo ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n67ContratadaUsuario_ContratadaPessoaCod ;
      private bool n68ContratadaUsuario_ContratadaPessoaNom ;
      private bool n2Usuario_Nome ;
      private bool n576ContratadaUsuario_CstUntPrdNrm ;
      private bool n577ContratadaUsuario_CstUntPrdExt ;
      private bool n340ContratanteUsuario_ContratantePesCod ;
      private bool n64ContratanteUsuario_ContratanteRaz ;
      private bool A1728ContratanteUsuario_EhFiscal ;
      private bool n1728ContratanteUsuario_EhFiscal ;
      private bool A1716Usuario_Foto_IsBlob ;
      private String AV13Email ;
      private String A1017Usuario_CrtfPath ;
      private String A325Usuario_PessoaDoc ;
      private String A40000Usuario_Foto_GXI ;
      private String A1074Usuario_CargoNom ;
      private String A1716Usuario_Foto ;
      private IGxSession AV16Websession ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbUsuario_PessoaTip ;
      private GXCheckbox chkUsuario_EhContador ;
      private GXCheckbox chkUsuario_EhAuditorFM ;
      private GXCheckbox chkUsuario_EhFinanceiro ;
      private GXCheckbox chkUsuario_EhContratada ;
      private GXCheckbox chkUsuario_EhContratante ;
      private GXCheckbox chkavContratanteusuario_ehfiscal ;
      private GXCheckbox chkUsuario_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H000K2_A57Usuario_PessoaCod ;
      private int[] H000K2_A1075Usuario_CargoUOCod ;
      private bool[] H000K2_n1075Usuario_CargoUOCod ;
      private int[] H000K2_A1073Usuario_CargoCod ;
      private bool[] H000K2_n1073Usuario_CargoCod ;
      private bool[] H000K2_A54Usuario_Ativo ;
      private String[] H000K2_A341Usuario_UserGamGuid ;
      private bool[] H000K2_n341Usuario_UserGamGuid ;
      private String[] H000K2_A1017Usuario_CrtfPath ;
      private bool[] H000K2_n1017Usuario_CrtfPath ;
      private bool[] H000K2_A292Usuario_EhContratante ;
      private bool[] H000K2_A291Usuario_EhContratada ;
      private bool[] H000K2_A293Usuario_EhFinanceiro ;
      private bool[] H000K2_A289Usuario_EhContador ;
      private String[] H000K2_A325Usuario_PessoaDoc ;
      private bool[] H000K2_n325Usuario_PessoaDoc ;
      private String[] H000K2_A59Usuario_PessoaTip ;
      private bool[] H000K2_n59Usuario_PessoaTip ;
      private String[] H000K2_A58Usuario_PessoaNom ;
      private bool[] H000K2_n58Usuario_PessoaNom ;
      private String[] H000K2_A40000Usuario_Foto_GXI ;
      private bool[] H000K2_n40000Usuario_Foto_GXI ;
      private String[] H000K2_A1074Usuario_CargoNom ;
      private bool[] H000K2_n1074Usuario_CargoNom ;
      private String[] H000K2_A1076Usuario_CargoUONom ;
      private bool[] H000K2_n1076Usuario_CargoUONom ;
      private int[] H000K2_A1Usuario_Codigo ;
      private String[] H000K2_A1716Usuario_Foto ;
      private bool[] H000K2_n1716Usuario_Foto ;
      private int[] H000K3_A5AreaTrabalho_Codigo ;
      private int[] H000K3_A29Contratante_Codigo ;
      private bool[] H000K3_n29Contratante_Codigo ;
      private int[] H000K4_A63ContratanteUsuario_ContratanteCod ;
      private int[] H000K4_A60ContratanteUsuario_UsuarioCod ;
      private int[] H000K5_A69ContratadaUsuario_UsuarioCod ;
      private int[] H000K5_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H000K5_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] H000K5_A66ContratadaUsuario_ContratadaCod ;
      private int[] H000K6_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] H000K6_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] H000K6_A69ContratadaUsuario_UsuarioCod ;
      private int[] H000K6_A66ContratadaUsuario_ContratadaCod ;
      private String[] H000K6_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] H000K6_n68ContratadaUsuario_ContratadaPessoaNom ;
      private String[] H000K6_A2Usuario_Nome ;
      private bool[] H000K6_n2Usuario_Nome ;
      private String[] H000K6_A341Usuario_UserGamGuid ;
      private bool[] H000K6_n341Usuario_UserGamGuid ;
      private decimal[] H000K6_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] H000K6_n576ContratadaUsuario_CstUntPrdNrm ;
      private decimal[] H000K6_A577ContratadaUsuario_CstUntPrdExt ;
      private bool[] H000K6_n577ContratadaUsuario_CstUntPrdExt ;
      private int[] H000K7_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] H000K7_n340ContratanteUsuario_ContratantePesCod ;
      private int[] H000K7_A60ContratanteUsuario_UsuarioCod ;
      private int[] H000K7_A63ContratanteUsuario_ContratanteCod ;
      private String[] H000K7_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] H000K7_n64ContratanteUsuario_ContratanteRaz ;
      private String[] H000K7_A2Usuario_Nome ;
      private bool[] H000K7_n2Usuario_Nome ;
      private String[] H000K7_A341Usuario_UserGamGuid ;
      private bool[] H000K7_n341Usuario_UserGamGuid ;
      private bool[] H000K7_A1728ContratanteUsuario_EhFiscal ;
      private bool[] H000K7_n1728ContratanteUsuario_EhFiscal ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV28GamError ;
      [ObjectCollection(ItemType=typeof( SdtGAMUser ))]
      private IGxCollection AV30Usuarios ;
      private SdtGAMUser AV14GamUser ;
      private SdtGAMUserFilter AV21GAMUserFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class usuariogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH000K2 ;
          prmH000K2 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000K3 ;
          prmH000K3 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000K4 ;
          prmH000K4 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000K5 ;
          prmH000K5 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000K6 ;
          prmH000K6 = new Object[] {
          new Object[] {"@AV17Entidade_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000K7 ;
          prmH000K7 = new Object[] {
          new Object[] {"@AV17Entidade_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H000K2", "SELECT T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T3.[Cargo_UOCod] AS Usuario_CargoUOCod, T1.[Usuario_CargoCod] AS Usuario_CargoCod, T1.[Usuario_Ativo], T1.[Usuario_UserGamGuid], T1.[Usuario_CrtfPath], T1.[Usuario_EhContratante], T1.[Usuario_EhContratada], T1.[Usuario_EhFinanceiro], T1.[Usuario_EhContador], T2.[Pessoa_Docto] AS Usuario_PessoaDoc, T2.[Pessoa_TipoPessoa] AS Usuario_PessoaTip, T2.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_Foto_GXI], T3.[Cargo_Nome] AS Usuario_CargoNom, T4.[UnidadeOrganizacional_Nome] AS Usuario_CargoUONom, T1.[Usuario_Codigo], T1.[Usuario_Foto] FROM ((([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) LEFT JOIN [Geral_Cargo] T3 WITH (NOLOCK) ON T3.[Cargo_Codigo] = T1.[Usuario_CargoCod]) LEFT JOIN [Geral_UnidadeOrganizacional] T4 WITH (NOLOCK) ON T4.[UnidadeOrganizacional_Codigo] = T3.[Cargo_UOCod]) WHERE T1.[Usuario_Codigo] = @Usuario_Codigo ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000K2,1,0,true,true )
             ,new CursorDef("H000K3", "SELECT [AreaTrabalho_Codigo], [Contratante_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000K3,1,0,true,true )
             ,new CursorDef("H000K4", "SELECT TOP 1 [ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @Contratante_Codigo and [ContratanteUsuario_UsuarioCod] = @Usuario_Codigo ORDER BY [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000K4,1,0,false,true )
             ,new CursorDef("H000K5", "SELECT TOP 1 T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) WHERE (T2.[Contratada_AreaTrabalhoCod] = @AreaTrabalho_Codigo) AND (T1.[ContratadaUsuario_UsuarioCod] = @Usuario_Codigo) ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000K5,1,0,false,true )
             ,new CursorDef("H000K6", "SELECT T3.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T4.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom, T2.[Usuario_Nome], T2.[Usuario_UserGamGuid], T1.[ContratadaUsuario_CstUntPrdNrm], T1.[ContratadaUsuario_CstUntPrdExt] FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) WHERE T1.[ContratadaUsuario_ContratadaCod] = @AV17Entidade_Codigo and T1.[ContratadaUsuario_UsuarioCod] = @Usuario_Codigo ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000K6,1,0,true,true )
             ,new CursorDef("H000K7", "SELECT T3.[Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, T4.[Pessoa_Nome] AS ContratanteUsuario_Contratante, T2.[Usuario_Nome], T2.[Usuario_UserGamGuid], T1.[ContratanteUsuario_EhFiscal] FROM ((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) INNER JOIN [Contratante] T3 WITH (NOLOCK) ON T3.[Contratante_Codigo] = T1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratante_PessoaCod]) WHERE T1.[ContratanteUsuario_ContratanteCod] = @AV17Entidade_Codigo and T1.[ContratanteUsuario_UsuarioCod] = @Usuario_Codigo ORDER BY T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000K7,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 40) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((bool[]) buf[9])[0] = rslt.getBool(7) ;
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((bool[]) buf[11])[0] = rslt.getBool(9) ;
                ((bool[]) buf[12])[0] = rslt.getBool(10) ;
                ((String[]) buf[13])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(11);
                ((String[]) buf[15])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(12);
                ((String[]) buf[17])[0] = rslt.getString(13, 100) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(13);
                ((String[]) buf[19])[0] = rslt.getMultimediaUri(14) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((String[]) buf[21])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((String[]) buf[23])[0] = rslt.getString(16, 50) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((int[]) buf[25])[0] = rslt.getInt(17) ;
                ((String[]) buf[26])[0] = rslt.getMultimediaFile(18, rslt.getVarchar(14)) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(18);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 40) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 40) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
