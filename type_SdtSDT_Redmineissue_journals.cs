/*
               File: type_SdtSDT_Redmineissue_journals
        Description: SDT_Redmineissue
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:5.4
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Redmineissue.journals" )]
   [XmlType(TypeName =  "SDT_Redmineissue.journals" , Namespace = "" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_Redmineissue_journals_journal ))]
   [Serializable]
   public class SdtSDT_Redmineissue_journals : GxUserType
   {
      public SdtSDT_Redmineissue_journals( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Redmineissue_journals_Type = "";
      }

      public SdtSDT_Redmineissue_journals( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Redmineissue_journals deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Redmineissue_journals)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Redmineissue_journals obj ;
         obj = this;
         obj.gxTpr_Type = deserialized.gxTpr_Type;
         obj.gxTpr_Journals = deserialized.gxTpr_Journals;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         if ( oReader.ExistsAttribute("type") == 1 )
         {
            gxTv_SdtSDT_Redmineissue_journals_Type = oReader.GetAttributeByName("type");
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            if ( gxTv_SdtSDT_Redmineissue_journals_Journals != null )
            {
               gxTv_SdtSDT_Redmineissue_journals_Journals.ClearCollection();
            }
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp(oReader.LocalName, "journal") == 0 )
               {
                  if ( gxTv_SdtSDT_Redmineissue_journals_Journals == null )
                  {
                     gxTv_SdtSDT_Redmineissue_journals_Journals = new GxObjectCollection( context, "SDT_Redmineissue.journals.journal", "", "SdtSDT_Redmineissue_journals_journal", "GeneXus.Programs");
                  }
                  GXSoapError = gxTv_SdtSDT_Redmineissue_journals_Journals.readxmlcollection(oReader, "", "journal");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Redmineissue.journals";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteAttribute("type", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_journals_Type));
         if ( gxTv_SdtSDT_Redmineissue_journals_Journals != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_Redmineissue_journals_Journals.writexmlcollection(oWriter, "", sNameSpace1, "journal", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("type", gxTv_SdtSDT_Redmineissue_journals_Type, false);
         if ( gxTv_SdtSDT_Redmineissue_journals_Journals != null )
         {
            AddObjectProperty("journals", gxTv_SdtSDT_Redmineissue_journals_Journals, false);
         }
         return  ;
      }

      [SoapAttribute( AttributeName = "type" )]
      [XmlAttribute( AttributeName = "type" )]
      public String gxTpr_Type
      {
         get {
            return gxTv_SdtSDT_Redmineissue_journals_Type ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_journals_Type = (String)(value);
         }

      }

      public class gxTv_SdtSDT_Redmineissue_journals_Journals_SdtSDT_Redmineissue_journals_journal_80compatibility:SdtSDT_Redmineissue_journals_journal {}
      [  SoapElement( ElementName = "journal" )]
      [  XmlElement( ElementName = "journal" , Namespace = "" , Type= typeof( SdtSDT_Redmineissue_journals_journal ))]
      public GxObjectCollection gxTpr_Journals_GxObjectCollection
      {
         get {
            if ( gxTv_SdtSDT_Redmineissue_journals_Journals == null )
            {
               gxTv_SdtSDT_Redmineissue_journals_Journals = new GxObjectCollection( context, "SDT_Redmineissue.journals.journal", "", "SdtSDT_Redmineissue_journals_journal", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtSDT_Redmineissue_journals_Journals ;
         }

         set {
            if ( gxTv_SdtSDT_Redmineissue_journals_Journals == null )
            {
               gxTv_SdtSDT_Redmineissue_journals_Journals = new GxObjectCollection( context, "SDT_Redmineissue.journals.journal", "", "SdtSDT_Redmineissue_journals_journal", "GeneXus.Programs");
            }
            gxTv_SdtSDT_Redmineissue_journals_Journals = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Journals
      {
         get {
            if ( gxTv_SdtSDT_Redmineissue_journals_Journals == null )
            {
               gxTv_SdtSDT_Redmineissue_journals_Journals = new GxObjectCollection( context, "SDT_Redmineissue.journals.journal", "", "SdtSDT_Redmineissue_journals_journal", "GeneXus.Programs");
            }
            return gxTv_SdtSDT_Redmineissue_journals_Journals ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_journals_Journals = value;
         }

      }

      public void gxTv_SdtSDT_Redmineissue_journals_Journals_SetNull( )
      {
         gxTv_SdtSDT_Redmineissue_journals_Journals = null;
         return  ;
      }

      public bool gxTv_SdtSDT_Redmineissue_journals_Journals_IsNull( )
      {
         if ( gxTv_SdtSDT_Redmineissue_journals_Journals == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_Redmineissue_journals_Type = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_Redmineissue_journals_Type ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Redmineissue_journals_journal ))]
      protected IGxCollection gxTv_SdtSDT_Redmineissue_journals_Journals=null ;
   }

   [DataContract(Name = @"SDT_Redmineissue.journals", Namespace = "")]
   public class SdtSDT_Redmineissue_journals_RESTInterface : GxGenericCollectionItem<SdtSDT_Redmineissue_journals>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Redmineissue_journals_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Redmineissue_journals_RESTInterface( SdtSDT_Redmineissue_journals psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "type" , Order = 0 )]
      public String gxTpr_Type
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Type) ;
         }

         set {
            sdt.gxTpr_Type = (String)(value);
         }

      }

      [DataMember( Name = "journals" , Order = 1 )]
      public GxGenericCollection<SdtSDT_Redmineissue_journals_journal_RESTInterface> gxTpr_Journals
      {
         get {
            return new GxGenericCollection<SdtSDT_Redmineissue_journals_journal_RESTInterface>(sdt.gxTpr_Journals) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Journals);
         }

      }

      public SdtSDT_Redmineissue_journals sdt
      {
         get {
            return (SdtSDT_Redmineissue_journals)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Redmineissue_journals() ;
         }
      }

   }

}
