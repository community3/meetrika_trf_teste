/*
               File: TipoPerfil
        Description: TipoPerfil
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 21:56:35.34
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomaintipoperfil
   {
      private static Hashtable domain = new Hashtable();
      static gxdomaintipoperfil ()
      {
         domain[(short)0] = "Desconhecido";
         domain[(short)1] = "Auditor";
         domain[(short)2] = "Contador";
         domain[(short)3] = "Contratada";
         domain[(short)4] = "Contratante";
         domain[(short)5] = "Financeiro";
         domain[(short)6] = "Usuario";
         domain[(short)10] = "Licensiado";
         domain[(short)99] = "Administrador GAM";
      }

      public static string getDescription( IGxContext context ,
                                           short key )
      {
         return (string)domain[key] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (short key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
