/*
               File: PRC_NewEvidenciaDemanda
        Description: Anexar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:11.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_newevidenciademanda : GXProcedure
   {
      public prc_newevidenciademanda( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_newevidenciademanda( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      public int executeUdp( )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         return A456ContagemResultado_Codigo ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo )
      {
         prc_newevidenciademanda objprc_newevidenciademanda;
         objprc_newevidenciademanda = new prc_newevidenciademanda();
         objprc_newevidenciademanda.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_newevidenciademanda.context.SetSubmitInitialConfig(context);
         objprc_newevidenciademanda.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_newevidenciademanda);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_newevidenciademanda)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( (0==A456ContagemResultado_Codigo) )
         {
            this.cleanup();
            if (true) return;
         }
         AV18Sdt_ContagemResultadoEvidencias.FromXml(AV19Websession.Get("ArquivosEvd"), "");
         AV27GXV1 = 1;
         while ( AV27GXV1 <= AV18Sdt_ContagemResultadoEvidencias.Count )
         {
            AV20Sdt_ArquivoEvidencia = ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV18Sdt_ContagemResultadoEvidencias.Item(AV27GXV1));
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_arquivo)) )
            {
               AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq = "";
            }
            else
            {
               if ( ( StringUtil.StringSearch( AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_arquivo, "http", 1) == 0 ) && ( StringUtil.StringSearch( AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_arquivo, "htts", 1) == 0 ) )
               {
                  AV22File.Source = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_arquivo;
               }
               else
               {
                  AV22File.Source = "";
               }
            }
            if ( AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_codigo != 0 )
            {
               AV21ContagemResultadoEvidencia_Codigo = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_codigo;
               if ( AV21ContagemResultadoEvidencia_Codigo > 0 )
               {
                  /* Using cursor P00492 */
                  pr_default.execute(0, new Object[] {AV21ContagemResultadoEvidencia_Codigo, A456ContagemResultado_Codigo});
                  while ( (pr_default.getStatus(0) != 101) )
                  {
                     A586ContagemResultadoEvidencia_Codigo = P00492_A586ContagemResultadoEvidencia_Codigo[0];
                     A589ContagemResultadoEvidencia_NomeArq = P00492_A589ContagemResultadoEvidencia_NomeArq[0];
                     n589ContagemResultadoEvidencia_NomeArq = P00492_n589ContagemResultadoEvidencia_NomeArq[0];
                     A590ContagemResultadoEvidencia_TipoArq = P00492_A590ContagemResultadoEvidencia_TipoArq[0];
                     n590ContagemResultadoEvidencia_TipoArq = P00492_n590ContagemResultadoEvidencia_TipoArq[0];
                     A591ContagemResultadoEvidencia_Data = P00492_A591ContagemResultadoEvidencia_Data[0];
                     n591ContagemResultadoEvidencia_Data = P00492_n591ContagemResultadoEvidencia_Data[0];
                     A587ContagemResultadoEvidencia_Descricao = P00492_A587ContagemResultadoEvidencia_Descricao[0];
                     n587ContagemResultadoEvidencia_Descricao = P00492_n587ContagemResultadoEvidencia_Descricao[0];
                     A1393ContagemResultadoEvidencia_RdmnCreated = P00492_A1393ContagemResultadoEvidencia_RdmnCreated[0];
                     n1393ContagemResultadoEvidencia_RdmnCreated = P00492_n1393ContagemResultadoEvidencia_RdmnCreated[0];
                     A1449ContagemResultadoEvidencia_Link = P00492_A1449ContagemResultadoEvidencia_Link[0];
                     n1449ContagemResultadoEvidencia_Link = P00492_n1449ContagemResultadoEvidencia_Link[0];
                     A1493ContagemResultadoEvidencia_Owner = P00492_A1493ContagemResultadoEvidencia_Owner[0];
                     n1493ContagemResultadoEvidencia_Owner = P00492_n1493ContagemResultadoEvidencia_Owner[0];
                     A645TipoDocumento_Codigo = P00492_A645TipoDocumento_Codigo[0];
                     n645TipoDocumento_Codigo = P00492_n645TipoDocumento_Codigo[0];
                     A588ContagemResultadoEvidencia_Arquivo = P00492_A588ContagemResultadoEvidencia_Arquivo[0];
                     n588ContagemResultadoEvidencia_Arquivo = P00492_n588ContagemResultadoEvidencia_Arquivo[0];
                     A588ContagemResultadoEvidencia_Arquivo = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_arquivo;
                     n588ContagemResultadoEvidencia_Arquivo = false;
                     A589ContagemResultadoEvidencia_NomeArq = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq;
                     n589ContagemResultadoEvidencia_NomeArq = false;
                     A590ContagemResultadoEvidencia_TipoArq = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_tipoarq;
                     n590ContagemResultadoEvidencia_TipoArq = false;
                     A591ContagemResultadoEvidencia_Data = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_data;
                     n591ContagemResultadoEvidencia_Data = false;
                     A587ContagemResultadoEvidencia_Descricao = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_descricao;
                     n587ContagemResultadoEvidencia_Descricao = false;
                     A1393ContagemResultadoEvidencia_RdmnCreated = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_rdmncreated;
                     n1393ContagemResultadoEvidencia_RdmnCreated = false;
                     A1449ContagemResultadoEvidencia_Link = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_link;
                     n1449ContagemResultadoEvidencia_Link = false;
                     A1493ContagemResultadoEvidencia_Owner = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_owner;
                     n1493ContagemResultadoEvidencia_Owner = false;
                     if ( (0==AV20Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_codigo) )
                     {
                        A645TipoDocumento_Codigo = 0;
                        n645TipoDocumento_Codigo = false;
                        n645TipoDocumento_Codigo = true;
                     }
                     else
                     {
                        A645TipoDocumento_Codigo = AV20Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_codigo;
                        n645TipoDocumento_Codigo = false;
                     }
                     /* Using cursor P00493 */
                     A590ContagemResultadoEvidencia_TipoArq = FileUtil.GetFileType( A588ContagemResultadoEvidencia_Arquivo);
                     n590ContagemResultadoEvidencia_TipoArq = false;
                     A589ContagemResultadoEvidencia_NomeArq = FileUtil.GetFileName( A588ContagemResultadoEvidencia_Arquivo);
                     n589ContagemResultadoEvidencia_NomeArq = false;
                     pr_default.execute(1, new Object[] {n588ContagemResultadoEvidencia_Arquivo, A588ContagemResultadoEvidencia_Arquivo, n591ContagemResultadoEvidencia_Data, A591ContagemResultadoEvidencia_Data, n587ContagemResultadoEvidencia_Descricao, A587ContagemResultadoEvidencia_Descricao, n1393ContagemResultadoEvidencia_RdmnCreated, A1393ContagemResultadoEvidencia_RdmnCreated, n1449ContagemResultadoEvidencia_Link, A1449ContagemResultadoEvidencia_Link, n1493ContagemResultadoEvidencia_Owner, A1493ContagemResultadoEvidencia_Owner, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, n590ContagemResultadoEvidencia_TipoArq, A590ContagemResultadoEvidencia_TipoArq, n589ContagemResultadoEvidencia_NomeArq, A589ContagemResultadoEvidencia_NomeArq, A586ContagemResultadoEvidencia_Codigo});
                     pr_default.close(1);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  pr_default.close(0);
               }
               else
               {
                  AV21ContagemResultadoEvidencia_Codigo = (int)(AV21ContagemResultadoEvidencia_Codigo*-1);
                  /* Using cursor P00494 */
                  pr_default.execute(2, new Object[] {AV21ContagemResultadoEvidencia_Codigo, A456ContagemResultado_Codigo});
                  while ( (pr_default.getStatus(2) != 101) )
                  {
                     A586ContagemResultadoEvidencia_Codigo = P00494_A586ContagemResultadoEvidencia_Codigo[0];
                     /* Using cursor P00495 */
                     pr_default.execute(3, new Object[] {A586ContagemResultadoEvidencia_Codigo});
                     pr_default.close(3);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  pr_default.close(2);
               }
            }
            else
            {
               /*
                  INSERT RECORD ON TABLE ContagemResultadoEvidencia

               */
               A588ContagemResultadoEvidencia_Arquivo = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_arquivo;
               n588ContagemResultadoEvidencia_Arquivo = false;
               A589ContagemResultadoEvidencia_NomeArq = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq;
               n589ContagemResultadoEvidencia_NomeArq = false;
               A590ContagemResultadoEvidencia_TipoArq = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_tipoarq;
               n590ContagemResultadoEvidencia_TipoArq = false;
               A591ContagemResultadoEvidencia_Data = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_data;
               n591ContagemResultadoEvidencia_Data = false;
               A587ContagemResultadoEvidencia_Descricao = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_descricao;
               n587ContagemResultadoEvidencia_Descricao = false;
               A1393ContagemResultadoEvidencia_RdmnCreated = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_rdmncreated;
               n1393ContagemResultadoEvidencia_RdmnCreated = false;
               A1449ContagemResultadoEvidencia_Link = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_link;
               n1449ContagemResultadoEvidencia_Link = false;
               A1493ContagemResultadoEvidencia_Owner = AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_owner;
               n1493ContagemResultadoEvidencia_Owner = false;
               if ( (0==AV20Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_codigo) )
               {
                  A645TipoDocumento_Codigo = 0;
                  n645TipoDocumento_Codigo = false;
                  n645TipoDocumento_Codigo = true;
               }
               else
               {
                  A645TipoDocumento_Codigo = AV20Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_codigo;
                  n645TipoDocumento_Codigo = false;
               }
               /* Using cursor P00496 */
               A589ContagemResultadoEvidencia_NomeArq = FileUtil.GetFileName( A588ContagemResultadoEvidencia_Arquivo);
               n589ContagemResultadoEvidencia_NomeArq = false;
               A590ContagemResultadoEvidencia_TipoArq = FileUtil.GetFileType( A588ContagemResultadoEvidencia_Arquivo);
               n590ContagemResultadoEvidencia_TipoArq = false;
               pr_default.execute(4, new Object[] {A456ContagemResultado_Codigo, n588ContagemResultadoEvidencia_Arquivo, A588ContagemResultadoEvidencia_Arquivo, n589ContagemResultadoEvidencia_NomeArq, A589ContagemResultadoEvidencia_NomeArq, n590ContagemResultadoEvidencia_TipoArq, A590ContagemResultadoEvidencia_TipoArq, n591ContagemResultadoEvidencia_Data, A591ContagemResultadoEvidencia_Data, n587ContagemResultadoEvidencia_Descricao, A587ContagemResultadoEvidencia_Descricao, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, n1393ContagemResultadoEvidencia_RdmnCreated, A1393ContagemResultadoEvidencia_RdmnCreated, n1449ContagemResultadoEvidencia_Link, A1449ContagemResultadoEvidencia_Link, n1493ContagemResultadoEvidencia_Owner, A1493ContagemResultadoEvidencia_Owner});
               A586ContagemResultadoEvidencia_Codigo = P00496_A586ContagemResultadoEvidencia_Codigo[0];
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
               if ( (pr_default.getStatus(4) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
               AV21ContagemResultadoEvidencia_Codigo = A586ContagemResultadoEvidencia_Codigo;
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_arquivo)) )
            {
               /* Optimized UPDATE. */
               /* Using cursor P00497 */
               A589ContagemResultadoEvidencia_NomeArq = FileUtil.GetFileName( A588ContagemResultadoEvidencia_Arquivo);
               n589ContagemResultadoEvidencia_NomeArq = false;
               A590ContagemResultadoEvidencia_TipoArq = FileUtil.GetFileType( A588ContagemResultadoEvidencia_Arquivo);
               n590ContagemResultadoEvidencia_TipoArq = false;
               pr_default.execute(5, new Object[] {n589ContagemResultadoEvidencia_NomeArq, AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq, n590ContagemResultadoEvidencia_TipoArq, AV20Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_tipoarq, AV21ContagemResultadoEvidencia_Codigo, A456ContagemResultado_Codigo});
               pr_default.close(5);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
               /* End optimized UPDATE. */
            }
            if ( AV20Sdt_ArquivoEvidencia.gxTpr_Artefatos_codigo > 0 )
            {
               /* Using cursor P00498 */
               pr_default.execute(6, new Object[] {AV20Sdt_ArquivoEvidencia.gxTpr_Artefatos_codigo, A456ContagemResultado_Codigo});
               while ( (pr_default.getStatus(6) != 101) )
               {
                  A1772ContagemResultadoArtefato_OSCod = P00498_A1772ContagemResultadoArtefato_OSCod[0];
                  A1771ContagemResultadoArtefato_ArtefatoCod = P00498_A1771ContagemResultadoArtefato_ArtefatoCod[0];
                  A1770ContagemResultadoArtefato_EvdCod = P00498_A1770ContagemResultadoArtefato_EvdCod[0];
                  n1770ContagemResultadoArtefato_EvdCod = P00498_n1770ContagemResultadoArtefato_EvdCod[0];
                  A1769ContagemResultadoArtefato_Codigo = P00498_A1769ContagemResultadoArtefato_Codigo[0];
                  A1770ContagemResultadoArtefato_EvdCod = AV21ContagemResultadoEvidencia_Codigo;
                  n1770ContagemResultadoArtefato_EvdCod = false;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  /* Using cursor P00499 */
                  pr_default.execute(7, new Object[] {n1770ContagemResultadoArtefato_EvdCod, A1770ContagemResultadoArtefato_EvdCod, A1769ContagemResultadoArtefato_Codigo});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoArtefato") ;
                  if (true) break;
                  /* Using cursor P004910 */
                  pr_default.execute(8, new Object[] {n1770ContagemResultadoArtefato_EvdCod, A1770ContagemResultadoArtefato_EvdCod, A1769ContagemResultadoArtefato_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoArtefato") ;
                  pr_default.readNext(6);
               }
               pr_default.close(6);
            }
            if ( AV22File.Exists() )
            {
               AV22File.Delete();
            }
            AV27GXV1 = (int)(AV27GXV1+1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_NewEvidenciaDemanda");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV18Sdt_ContagemResultadoEvidencias = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_MeetrikaVs3", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         AV19Websession = context.GetSession();
         AV20Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
         AV22File = new GxFile(context.GetPhysicalPath());
         scmdbuf = "";
         P00492_A456ContagemResultado_Codigo = new int[1] ;
         P00492_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         P00492_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         P00492_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         P00492_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         P00492_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         P00492_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00492_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         P00492_A587ContagemResultadoEvidencia_Descricao = new String[] {""} ;
         P00492_n587ContagemResultadoEvidencia_Descricao = new bool[] {false} ;
         P00492_A1393ContagemResultadoEvidencia_RdmnCreated = new DateTime[] {DateTime.MinValue} ;
         P00492_n1393ContagemResultadoEvidencia_RdmnCreated = new bool[] {false} ;
         P00492_A1449ContagemResultadoEvidencia_Link = new String[] {""} ;
         P00492_n1449ContagemResultadoEvidencia_Link = new bool[] {false} ;
         P00492_A1493ContagemResultadoEvidencia_Owner = new int[1] ;
         P00492_n1493ContagemResultadoEvidencia_Owner = new bool[] {false} ;
         P00492_A645TipoDocumento_Codigo = new int[1] ;
         P00492_n645TipoDocumento_Codigo = new bool[] {false} ;
         P00492_A588ContagemResultadoEvidencia_Arquivo = new String[] {""} ;
         P00492_n588ContagemResultadoEvidencia_Arquivo = new bool[] {false} ;
         A589ContagemResultadoEvidencia_NomeArq = "";
         A590ContagemResultadoEvidencia_TipoArq = "";
         A591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         A587ContagemResultadoEvidencia_Descricao = "";
         A1393ContagemResultadoEvidencia_RdmnCreated = (DateTime)(DateTime.MinValue);
         A1449ContagemResultadoEvidencia_Link = "";
         A588ContagemResultadoEvidencia_Arquivo = "";
         P00494_A456ContagemResultado_Codigo = new int[1] ;
         P00494_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         P00496_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         Gx_emsg = "";
         P00498_A1772ContagemResultadoArtefato_OSCod = new int[1] ;
         P00498_A1771ContagemResultadoArtefato_ArtefatoCod = new int[1] ;
         P00498_A1770ContagemResultadoArtefato_EvdCod = new int[1] ;
         P00498_n1770ContagemResultadoArtefato_EvdCod = new bool[] {false} ;
         P00498_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_newevidenciademanda__default(),
            new Object[][] {
                new Object[] {
               P00492_A456ContagemResultado_Codigo, P00492_A586ContagemResultadoEvidencia_Codigo, P00492_A589ContagemResultadoEvidencia_NomeArq, P00492_n589ContagemResultadoEvidencia_NomeArq, P00492_A590ContagemResultadoEvidencia_TipoArq, P00492_n590ContagemResultadoEvidencia_TipoArq, P00492_A591ContagemResultadoEvidencia_Data, P00492_n591ContagemResultadoEvidencia_Data, P00492_A587ContagemResultadoEvidencia_Descricao, P00492_n587ContagemResultadoEvidencia_Descricao,
               P00492_A1393ContagemResultadoEvidencia_RdmnCreated, P00492_n1393ContagemResultadoEvidencia_RdmnCreated, P00492_A1449ContagemResultadoEvidencia_Link, P00492_n1449ContagemResultadoEvidencia_Link, P00492_A1493ContagemResultadoEvidencia_Owner, P00492_n1493ContagemResultadoEvidencia_Owner, P00492_A645TipoDocumento_Codigo, P00492_n645TipoDocumento_Codigo, P00492_A588ContagemResultadoEvidencia_Arquivo, P00492_n588ContagemResultadoEvidencia_Arquivo
               }
               , new Object[] {
               }
               , new Object[] {
               P00494_A456ContagemResultado_Codigo, P00494_A586ContagemResultadoEvidencia_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               P00496_A586ContagemResultadoEvidencia_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               P00498_A1772ContagemResultadoArtefato_OSCod, P00498_A1771ContagemResultadoArtefato_ArtefatoCod, P00498_A1770ContagemResultadoArtefato_EvdCod, P00498_n1770ContagemResultadoArtefato_EvdCod, P00498_A1769ContagemResultadoArtefato_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private int AV27GXV1 ;
      private int AV21ContagemResultadoEvidencia_Codigo ;
      private int A586ContagemResultadoEvidencia_Codigo ;
      private int A1493ContagemResultadoEvidencia_Owner ;
      private int A645TipoDocumento_Codigo ;
      private int GX_INS76 ;
      private int A1772ContagemResultadoArtefato_OSCod ;
      private int A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int A1770ContagemResultadoArtefato_EvdCod ;
      private int A1769ContagemResultadoArtefato_Codigo ;
      private String scmdbuf ;
      private String A589ContagemResultadoEvidencia_NomeArq ;
      private String A590ContagemResultadoEvidencia_TipoArq ;
      private String Gx_emsg ;
      private DateTime A591ContagemResultadoEvidencia_Data ;
      private DateTime A1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool n589ContagemResultadoEvidencia_NomeArq ;
      private bool n590ContagemResultadoEvidencia_TipoArq ;
      private bool n591ContagemResultadoEvidencia_Data ;
      private bool n587ContagemResultadoEvidencia_Descricao ;
      private bool n1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool n1449ContagemResultadoEvidencia_Link ;
      private bool n1493ContagemResultadoEvidencia_Owner ;
      private bool n645TipoDocumento_Codigo ;
      private bool n588ContagemResultadoEvidencia_Arquivo ;
      private bool n1770ContagemResultadoArtefato_EvdCod ;
      private String A587ContagemResultadoEvidencia_Descricao ;
      private String A1449ContagemResultadoEvidencia_Link ;
      private String A588ContagemResultadoEvidencia_Arquivo ;
      private IGxSession AV19Websession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00492_A456ContagemResultado_Codigo ;
      private int[] P00492_A586ContagemResultadoEvidencia_Codigo ;
      private String[] P00492_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] P00492_n589ContagemResultadoEvidencia_NomeArq ;
      private String[] P00492_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] P00492_n590ContagemResultadoEvidencia_TipoArq ;
      private DateTime[] P00492_A591ContagemResultadoEvidencia_Data ;
      private bool[] P00492_n591ContagemResultadoEvidencia_Data ;
      private String[] P00492_A587ContagemResultadoEvidencia_Descricao ;
      private bool[] P00492_n587ContagemResultadoEvidencia_Descricao ;
      private DateTime[] P00492_A1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool[] P00492_n1393ContagemResultadoEvidencia_RdmnCreated ;
      private String[] P00492_A1449ContagemResultadoEvidencia_Link ;
      private bool[] P00492_n1449ContagemResultadoEvidencia_Link ;
      private int[] P00492_A1493ContagemResultadoEvidencia_Owner ;
      private bool[] P00492_n1493ContagemResultadoEvidencia_Owner ;
      private int[] P00492_A645TipoDocumento_Codigo ;
      private bool[] P00492_n645TipoDocumento_Codigo ;
      private String[] P00492_A588ContagemResultadoEvidencia_Arquivo ;
      private bool[] P00492_n588ContagemResultadoEvidencia_Arquivo ;
      private int[] P00494_A456ContagemResultado_Codigo ;
      private int[] P00494_A586ContagemResultadoEvidencia_Codigo ;
      private int[] P00496_A586ContagemResultadoEvidencia_Codigo ;
      private int[] P00498_A1772ContagemResultadoArtefato_OSCod ;
      private int[] P00498_A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int[] P00498_A1770ContagemResultadoArtefato_EvdCod ;
      private bool[] P00498_n1770ContagemResultadoArtefato_EvdCod ;
      private int[] P00498_A1769ContagemResultadoArtefato_Codigo ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV18Sdt_ContagemResultadoEvidencias ;
      private GxFile AV22File ;
      private SdtSDT_ContagemResultadoEvidencias_Arquivo AV20Sdt_ArquivoEvidencia ;
   }

   public class prc_newevidenciademanda__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00492 ;
          prmP00492 = new Object[] {
          new Object[] {"@AV21ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00493 ;
          prmP00493 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoEvidencia_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultadoEvidencia_RdmnCreated",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoEvidencia_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultadoEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00494 ;
          prmP00494 = new Object[] {
          new Object[] {"@AV21ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00495 ;
          prmP00495 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00496 ;
          prmP00496 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultadoEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoEvidencia_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoEvidencia_RdmnCreated",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoEvidencia_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Owner",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00497 ;
          prmP00497 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV21ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00498 ;
          prmP00498 = new Object[] {
          new Object[] {"@AV20Sdt__1Artefatos_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00499 ;
          prmP00499 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_EvdCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004910 ;
          prmP004910 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_EvdCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00492", "SELECT [ContagemResultado_Codigo], [ContagemResultadoEvidencia_Codigo], [ContagemResultadoEvidencia_NomeArq], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_Data], [ContagemResultadoEvidencia_Descricao], [ContagemResultadoEvidencia_RdmnCreated], [ContagemResultadoEvidencia_Link], [ContagemResultadoEvidencia_Owner], [TipoDocumento_Codigo], [ContagemResultadoEvidencia_Arquivo] FROM [ContagemResultadoEvidencia] WITH (UPDLOCK) WHERE ([ContagemResultadoEvidencia_Codigo] = @AV21ContagemResultadoEvidencia_Codigo) AND ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) ORDER BY [ContagemResultadoEvidencia_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00492,1,0,true,true )
             ,new CursorDef("P00493", "UPDATE [ContagemResultadoEvidencia] SET [ContagemResultadoEvidencia_Arquivo]=@ContagemResultadoEvidencia_Arquivo, [ContagemResultadoEvidencia_Data]=@ContagemResultadoEvidencia_Data, [ContagemResultadoEvidencia_Descricao]=@ContagemResultadoEvidencia_Descricao, [ContagemResultadoEvidencia_RdmnCreated]=@ContagemResultadoEvidencia_RdmnCreated, [ContagemResultadoEvidencia_Link]=@ContagemResultadoEvidencia_Link, [ContagemResultadoEvidencia_Owner]=@ContagemResultadoEvidencia_Owner, [TipoDocumento_Codigo]=@TipoDocumento_Codigo, [ContagemResultadoEvidencia_TipoArq]=@ContagemResultadoEvidencia_TipoArq, [ContagemResultadoEvidencia_NomeArq]=@ContagemResultadoEvidencia_NomeArq  WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00493)
             ,new CursorDef("P00494", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultadoEvidencia_Codigo] FROM [ContagemResultadoEvidencia] WITH (UPDLOCK) WHERE ([ContagemResultadoEvidencia_Codigo] = @AV21ContagemResultadoEvidencia_Codigo) AND ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) ORDER BY [ContagemResultadoEvidencia_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00494,1,0,true,true )
             ,new CursorDef("P00495", "DELETE FROM [ContagemResultadoEvidencia]  WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00495)
             ,new CursorDef("P00496", "INSERT INTO [ContagemResultadoEvidencia]([ContagemResultado_Codigo], [ContagemResultadoEvidencia_Arquivo], [ContagemResultadoEvidencia_NomeArq], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_Data], [ContagemResultadoEvidencia_Descricao], [TipoDocumento_Codigo], [ContagemResultadoEvidencia_RdmnCreated], [ContagemResultadoEvidencia_Link], [ContagemResultadoEvidencia_Owner]) VALUES(@ContagemResultado_Codigo, @ContagemResultadoEvidencia_Arquivo, @ContagemResultadoEvidencia_NomeArq, @ContagemResultadoEvidencia_TipoArq, @ContagemResultadoEvidencia_Data, @ContagemResultadoEvidencia_Descricao, @TipoDocumento_Codigo, @ContagemResultadoEvidencia_RdmnCreated, @ContagemResultadoEvidencia_Link, @ContagemResultadoEvidencia_Owner); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00496)
             ,new CursorDef("P00497", "UPDATE [ContagemResultadoEvidencia] SET [ContagemResultadoEvidencia_NomeArq]=@ContagemResultadoEvidencia_NomeArq, [ContagemResultadoEvidencia_TipoArq]=@ContagemResultadoEvidencia_TipoArq  WHERE ([ContagemResultadoEvidencia_Codigo] = @AV21ContagemResultadoEvidencia_Codigo) AND ([ContagemResultado_Codigo] = @ContagemResultado_Codigo)", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00497)
             ,new CursorDef("P00498", "SELECT TOP 1 [ContagemResultadoArtefato_OSCod], [ContagemResultadoArtefato_ArtefatoCod], [ContagemResultadoArtefato_EvdCod], [ContagemResultadoArtefato_Codigo] FROM [ContagemResultadoArtefato] WITH (UPDLOCK) WHERE ([ContagemResultadoArtefato_ArtefatoCod] = @AV20Sdt__1Artefatos_codigo) AND ([ContagemResultadoArtefato_OSCod] = @ContagemResultado_Codigo) ORDER BY [ContagemResultadoArtefato_ArtefatoCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00498,1,0,true,true )
             ,new CursorDef("P00499", "UPDATE [ContagemResultadoArtefato] SET [ContagemResultadoArtefato_EvdCod]=@ContagemResultadoArtefato_EvdCod  WHERE [ContagemResultadoArtefato_Codigo] = @ContagemResultadoArtefato_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00499)
             ,new CursorDef("P004910", "UPDATE [ContagemResultadoArtefato] SET [ContagemResultadoArtefato_EvdCod]=@ContagemResultadoArtefato_EvdCod  WHERE [ContagemResultadoArtefato_Codigo] = @ContagemResultadoArtefato_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004910)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getBLOBFile(11, rslt.getString(4, 10), rslt.getString(3, 50)) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 9 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[17]);
                }
                stmt.SetParameter(10, (int)parms[18]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(8, (DateTime)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[18]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                stmt.SetParameter(4, (int)parms[5]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
