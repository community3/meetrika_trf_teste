/*
               File: PRC_ValoresAnteresContratados
        Description: Valores Anteres Contratados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:54.83
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_valoresanterescontratados : GXProcedure
   {
      public prc_valoresanterescontratados( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_valoresanterescontratados( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contrato_Codigo ,
                           ref decimal aP1_ContratoTermoAditivo_VlrTtlPrvto ,
                           ref decimal aP2_ContratoTermoAditivo_QtdContratada ,
                           ref decimal aP3_ContratoTermoAditivo_VlrUntUndCnt )
      {
         this.A74Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV8ContratoTermoAditivo_VlrTtlPrvto = aP1_ContratoTermoAditivo_VlrTtlPrvto;
         this.AV9ContratoTermoAditivo_QtdContratada = aP2_ContratoTermoAditivo_QtdContratada;
         this.AV10ContratoTermoAditivo_VlrUntUndCnt = aP3_ContratoTermoAditivo_VlrUntUndCnt;
         initialize();
         executePrivate();
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
         aP1_ContratoTermoAditivo_VlrTtlPrvto=this.AV8ContratoTermoAditivo_VlrTtlPrvto;
         aP2_ContratoTermoAditivo_QtdContratada=this.AV9ContratoTermoAditivo_QtdContratada;
         aP3_ContratoTermoAditivo_VlrUntUndCnt=this.AV10ContratoTermoAditivo_VlrUntUndCnt;
      }

      public decimal executeUdp( ref int aP0_Contrato_Codigo ,
                                 ref decimal aP1_ContratoTermoAditivo_VlrTtlPrvto ,
                                 ref decimal aP2_ContratoTermoAditivo_QtdContratada )
      {
         this.A74Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV8ContratoTermoAditivo_VlrTtlPrvto = aP1_ContratoTermoAditivo_VlrTtlPrvto;
         this.AV9ContratoTermoAditivo_QtdContratada = aP2_ContratoTermoAditivo_QtdContratada;
         this.AV10ContratoTermoAditivo_VlrUntUndCnt = aP3_ContratoTermoAditivo_VlrUntUndCnt;
         initialize();
         executePrivate();
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
         aP1_ContratoTermoAditivo_VlrTtlPrvto=this.AV8ContratoTermoAditivo_VlrTtlPrvto;
         aP2_ContratoTermoAditivo_QtdContratada=this.AV9ContratoTermoAditivo_QtdContratada;
         aP3_ContratoTermoAditivo_VlrUntUndCnt=this.AV10ContratoTermoAditivo_VlrUntUndCnt;
         return AV10ContratoTermoAditivo_VlrUntUndCnt ;
      }

      public void executeSubmit( ref int aP0_Contrato_Codigo ,
                                 ref decimal aP1_ContratoTermoAditivo_VlrTtlPrvto ,
                                 ref decimal aP2_ContratoTermoAditivo_QtdContratada ,
                                 ref decimal aP3_ContratoTermoAditivo_VlrUntUndCnt )
      {
         prc_valoresanterescontratados objprc_valoresanterescontratados;
         objprc_valoresanterescontratados = new prc_valoresanterescontratados();
         objprc_valoresanterescontratados.A74Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_valoresanterescontratados.AV8ContratoTermoAditivo_VlrTtlPrvto = aP1_ContratoTermoAditivo_VlrTtlPrvto;
         objprc_valoresanterescontratados.AV9ContratoTermoAditivo_QtdContratada = aP2_ContratoTermoAditivo_QtdContratada;
         objprc_valoresanterescontratados.AV10ContratoTermoAditivo_VlrUntUndCnt = aP3_ContratoTermoAditivo_VlrUntUndCnt;
         objprc_valoresanterescontratados.context.SetSubmitInitialConfig(context);
         objprc_valoresanterescontratados.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_valoresanterescontratados);
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
         aP1_ContratoTermoAditivo_VlrTtlPrvto=this.AV8ContratoTermoAditivo_VlrTtlPrvto;
         aP2_ContratoTermoAditivo_QtdContratada=this.AV9ContratoTermoAditivo_QtdContratada;
         aP3_ContratoTermoAditivo_VlrUntUndCnt=this.AV10ContratoTermoAditivo_VlrUntUndCnt;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_valoresanterescontratados)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV13GXLvl2 = 0;
         /* Using cursor P008Z2 */
         pr_default.execute(0, new Object[] {A74Contrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1360ContratoTermoAditivo_VlrTtlPrvto = P008Z2_A1360ContratoTermoAditivo_VlrTtlPrvto[0];
            n1360ContratoTermoAditivo_VlrTtlPrvto = P008Z2_n1360ContratoTermoAditivo_VlrTtlPrvto[0];
            A1362ContratoTermoAditivo_QtdContratada = P008Z2_A1362ContratoTermoAditivo_QtdContratada[0];
            n1362ContratoTermoAditivo_QtdContratada = P008Z2_n1362ContratoTermoAditivo_QtdContratada[0];
            A1361ContratoTermoAditivo_VlrUntUndCnt = P008Z2_A1361ContratoTermoAditivo_VlrUntUndCnt[0];
            n1361ContratoTermoAditivo_VlrUntUndCnt = P008Z2_n1361ContratoTermoAditivo_VlrUntUndCnt[0];
            A315ContratoTermoAditivo_Codigo = P008Z2_A315ContratoTermoAditivo_Codigo[0];
            AV13GXLvl2 = 1;
            AV8ContratoTermoAditivo_VlrTtlPrvto = A1360ContratoTermoAditivo_VlrTtlPrvto;
            AV9ContratoTermoAditivo_QtdContratada = A1362ContratoTermoAditivo_QtdContratada;
            AV10ContratoTermoAditivo_VlrUntUndCnt = A1361ContratoTermoAditivo_VlrUntUndCnt;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV13GXLvl2 == 0 )
         {
            /* Using cursor P008Z3 */
            pr_default.execute(1, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A89Contrato_Valor = P008Z3_A89Contrato_Valor[0];
               A81Contrato_Quantidade = P008Z3_A81Contrato_Quantidade[0];
               A116Contrato_ValorUnidadeContratacao = P008Z3_A116Contrato_ValorUnidadeContratacao[0];
               AV8ContratoTermoAditivo_VlrTtlPrvto = A89Contrato_Valor;
               AV9ContratoTermoAditivo_QtdContratada = (decimal)(A81Contrato_Quantidade);
               AV10ContratoTermoAditivo_VlrUntUndCnt = A116Contrato_ValorUnidadeContratacao;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P008Z2_A74Contrato_Codigo = new int[1] ;
         P008Z2_A1360ContratoTermoAditivo_VlrTtlPrvto = new decimal[1] ;
         P008Z2_n1360ContratoTermoAditivo_VlrTtlPrvto = new bool[] {false} ;
         P008Z2_A1362ContratoTermoAditivo_QtdContratada = new decimal[1] ;
         P008Z2_n1362ContratoTermoAditivo_QtdContratada = new bool[] {false} ;
         P008Z2_A1361ContratoTermoAditivo_VlrUntUndCnt = new decimal[1] ;
         P008Z2_n1361ContratoTermoAditivo_VlrUntUndCnt = new bool[] {false} ;
         P008Z2_A315ContratoTermoAditivo_Codigo = new int[1] ;
         P008Z3_A74Contrato_Codigo = new int[1] ;
         P008Z3_A89Contrato_Valor = new decimal[1] ;
         P008Z3_A81Contrato_Quantidade = new int[1] ;
         P008Z3_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_valoresanterescontratados__default(),
            new Object[][] {
                new Object[] {
               P008Z2_A74Contrato_Codigo, P008Z2_A1360ContratoTermoAditivo_VlrTtlPrvto, P008Z2_n1360ContratoTermoAditivo_VlrTtlPrvto, P008Z2_A1362ContratoTermoAditivo_QtdContratada, P008Z2_n1362ContratoTermoAditivo_QtdContratada, P008Z2_A1361ContratoTermoAditivo_VlrUntUndCnt, P008Z2_n1361ContratoTermoAditivo_VlrUntUndCnt, P008Z2_A315ContratoTermoAditivo_Codigo
               }
               , new Object[] {
               P008Z3_A74Contrato_Codigo, P008Z3_A89Contrato_Valor, P008Z3_A81Contrato_Quantidade, P008Z3_A116Contrato_ValorUnidadeContratacao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV13GXLvl2 ;
      private int A74Contrato_Codigo ;
      private int A315ContratoTermoAditivo_Codigo ;
      private int A81Contrato_Quantidade ;
      private decimal AV8ContratoTermoAditivo_VlrTtlPrvto ;
      private decimal AV9ContratoTermoAditivo_QtdContratada ;
      private decimal AV10ContratoTermoAditivo_VlrUntUndCnt ;
      private decimal A1360ContratoTermoAditivo_VlrTtlPrvto ;
      private decimal A1362ContratoTermoAditivo_QtdContratada ;
      private decimal A1361ContratoTermoAditivo_VlrUntUndCnt ;
      private decimal A89Contrato_Valor ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private String scmdbuf ;
      private bool n1360ContratoTermoAditivo_VlrTtlPrvto ;
      private bool n1362ContratoTermoAditivo_QtdContratada ;
      private bool n1361ContratoTermoAditivo_VlrUntUndCnt ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contrato_Codigo ;
      private decimal aP1_ContratoTermoAditivo_VlrTtlPrvto ;
      private decimal aP2_ContratoTermoAditivo_QtdContratada ;
      private decimal aP3_ContratoTermoAditivo_VlrUntUndCnt ;
      private IDataStoreProvider pr_default ;
      private int[] P008Z2_A74Contrato_Codigo ;
      private decimal[] P008Z2_A1360ContratoTermoAditivo_VlrTtlPrvto ;
      private bool[] P008Z2_n1360ContratoTermoAditivo_VlrTtlPrvto ;
      private decimal[] P008Z2_A1362ContratoTermoAditivo_QtdContratada ;
      private bool[] P008Z2_n1362ContratoTermoAditivo_QtdContratada ;
      private decimal[] P008Z2_A1361ContratoTermoAditivo_VlrUntUndCnt ;
      private bool[] P008Z2_n1361ContratoTermoAditivo_VlrUntUndCnt ;
      private int[] P008Z2_A315ContratoTermoAditivo_Codigo ;
      private int[] P008Z3_A74Contrato_Codigo ;
      private decimal[] P008Z3_A89Contrato_Valor ;
      private int[] P008Z3_A81Contrato_Quantidade ;
      private decimal[] P008Z3_A116Contrato_ValorUnidadeContratacao ;
   }

   public class prc_valoresanterescontratados__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008Z2 ;
          prmP008Z2 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008Z3 ;
          prmP008Z3 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008Z2", "SELECT [Contrato_Codigo], [ContratoTermoAditivo_VlrTtlPrvto], [ContratoTermoAditivo_QtdContratada], [ContratoTermoAditivo_VlrUntUndCnt], [ContratoTermoAditivo_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008Z2,100,0,false,false )
             ,new CursorDef("P008Z3", "SELECT TOP 1 [Contrato_Codigo], [Contrato_Valor], [Contrato_Quantidade], [Contrato_ValorUnidadeContratacao] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008Z3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
