/*
               File: PRC_SetFimExc
        Description: Set Fim Exc
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:59.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_setfimexc : GXProcedure
   {
      public prc_setfimexc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_setfimexc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Codigo )
      {
         this.AV18Codigo = aP0_Codigo;
         initialize();
         executePrivate();
         aP0_Codigo=this.AV18Codigo;
      }

      public int executeUdp( )
      {
         this.AV18Codigo = aP0_Codigo;
         initialize();
         executePrivate();
         aP0_Codigo=this.AV18Codigo;
         return AV18Codigo ;
      }

      public void executeSubmit( ref int aP0_Codigo )
      {
         prc_setfimexc objprc_setfimexc;
         objprc_setfimexc = new prc_setfimexc();
         objprc_setfimexc.AV18Codigo = aP0_Codigo;
         objprc_setfimexc.context.SetSubmitInitialConfig(context);
         objprc_setfimexc.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_setfimexc);
         aP0_Codigo=this.AV18Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_setfimexc)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00AX3 */
         pr_default.execute(0, new Object[] {AV18Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A456ContagemResultado_Codigo = P00AX3_A456ContagemResultado_Codigo[0];
            A1510ContagemResultado_FimExc = P00AX3_A1510ContagemResultado_FimExc[0];
            n1510ContagemResultado_FimExc = P00AX3_n1510ContagemResultado_FimExc[0];
            A1509ContagemResultado_InicioExc = P00AX3_A1509ContagemResultado_InicioExc[0];
            n1509ContagemResultado_InicioExc = P00AX3_n1509ContagemResultado_InicioExc[0];
            A490ContagemResultado_ContratadaCod = P00AX3_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00AX3_n490ContagemResultado_ContratadaCod[0];
            A601ContagemResultado_Servico = P00AX3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00AX3_n601ContagemResultado_Servico[0];
            A1553ContagemResultado_CntSrvCod = P00AX3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00AX3_n1553ContagemResultado_CntSrvCod[0];
            A1505ContagemResultado_TmpEstExc = P00AX3_A1505ContagemResultado_TmpEstExc[0];
            n1505ContagemResultado_TmpEstExc = P00AX3_n1505ContagemResultado_TmpEstExc[0];
            A584ContagemResultado_ContadorFM = P00AX3_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = P00AX3_n584ContagemResultado_ContadorFM[0];
            A584ContagemResultado_ContadorFM = P00AX3_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = P00AX3_n584ContagemResultado_ContadorFM[0];
            A601ContagemResultado_Servico = P00AX3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00AX3_n601ContagemResultado_Servico[0];
            A1510ContagemResultado_FimExc = DateTimeUtil.ServerNow( context, "DEFAULT");
            n1510ContagemResultado_FimExc = false;
            if ( P00AX3_n1509ContagemResultado_InicioExc[0] )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00AX4 */
               pr_default.execute(1, new Object[] {n1510ContagemResultado_FimExc, A1510ContagemResultado_FimExc, n1505ContagemResultado_TmpEstExc, A1505ContagemResultado_TmpEstExc, A456ContagemResultado_Codigo});
               pr_default.close(1);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if (true) break;
            }
            AV10Contratada = A490ContagemResultado_ContratadaCod;
            AV11Servico = A601ContagemResultado_Servico;
            AV12Contador = A584ContagemResultado_ContadorFM;
            AV23ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
            GXt_int1 = AV9TmpEstExc;
            GXt_int2 = (int)(DateTimeUtil.TDiff( A1510ContagemResultado_FimExc, A1509ContagemResultado_InicioExc));
            new prc_sstohhmm(context ).execute( ref  GXt_int2, ref  AV13Horas, out  GXt_int1) ;
            AV9TmpEstExc = GXt_int1;
            A1505ContagemResultado_TmpEstExc = (int)(A1505ContagemResultado_TmpEstExc+AV9TmpEstExc);
            n1505ContagemResultado_TmpEstExc = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00AX5 */
            pr_default.execute(2, new Object[] {n1510ContagemResultado_FimExc, A1510ContagemResultado_FimExc, n1505ContagemResultado_TmpEstExc, A1505ContagemResultado_TmpEstExc, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P00AX6 */
            pr_default.execute(3, new Object[] {n1510ContagemResultado_FimExc, A1510ContagemResultado_FimExc, n1505ContagemResultado_TmpEstExc, A1505ContagemResultado_TmpEstExc, A456ContagemResultado_Codigo});
            pr_default.close(3);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Using cursor P00AX8 */
         pr_default.execute(4, new Object[] {AV23ContratoServicos_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A456ContagemResultado_Codigo = P00AX8_A456ContagemResultado_Codigo[0];
            A602ContagemResultado_OSVinculada = P00AX8_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00AX8_n602ContagemResultado_OSVinculada[0];
            A1505ContagemResultado_TmpEstExc = P00AX8_A1505ContagemResultado_TmpEstExc[0];
            n1505ContagemResultado_TmpEstExc = P00AX8_n1505ContagemResultado_TmpEstExc[0];
            A1553ContagemResultado_CntSrvCod = P00AX8_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00AX8_n1553ContagemResultado_CntSrvCod[0];
            A40000GXC1 = P00AX8_A40000GXC1[0];
            n40000GXC1 = P00AX8_n40000GXC1[0];
            A40001GXC2 = P00AX8_A40001GXC2[0];
            n40001GXC2 = P00AX8_n40001GXC2[0];
            A40000GXC1 = P00AX8_A40000GXC1[0];
            n40000GXC1 = P00AX8_n40000GXC1[0];
            A40001GXC2 = P00AX8_A40001GXC2[0];
            n40001GXC2 = P00AX8_n40001GXC2[0];
            AV17Count = A40000GXC1;
            AV19Soma = A40001GXC2;
            pr_default.readNext(4);
         }
         pr_default.close(4);
         if ( AV17Count > 0 )
         {
            AV15Media = (int)((AV19Soma+AV9TmpEstExc)/ (decimal)(AV17Count));
         }
         else
         {
            AV15Media = AV9TmpEstExc;
         }
         AV17Count = 0;
         /* Using cursor P00AX11 */
         pr_default.execute(5, new Object[] {AV23ContratoServicos_Codigo, AV12Contador});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A456ContagemResultado_Codigo = P00AX11_A456ContagemResultado_Codigo[0];
            A602ContagemResultado_OSVinculada = P00AX11_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00AX11_n602ContagemResultado_OSVinculada[0];
            A1505ContagemResultado_TmpEstExc = P00AX11_A1505ContagemResultado_TmpEstExc[0];
            n1505ContagemResultado_TmpEstExc = P00AX11_n1505ContagemResultado_TmpEstExc[0];
            A1553ContagemResultado_CntSrvCod = P00AX11_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00AX11_n1553ContagemResultado_CntSrvCod[0];
            A584ContagemResultado_ContadorFM = P00AX11_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = P00AX11_n584ContagemResultado_ContadorFM[0];
            A40000GXC1 = P00AX11_A40000GXC1[0];
            n40000GXC1 = P00AX11_n40000GXC1[0];
            A40001GXC2 = P00AX11_A40001GXC2[0];
            n40001GXC2 = P00AX11_n40001GXC2[0];
            A584ContagemResultado_ContadorFM = P00AX11_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = P00AX11_n584ContagemResultado_ContadorFM[0];
            A40000GXC1 = P00AX11_A40000GXC1[0];
            n40000GXC1 = P00AX11_n40000GXC1[0];
            A40001GXC2 = P00AX11_A40001GXC2[0];
            n40001GXC2 = P00AX11_n40001GXC2[0];
            AV17Count = A40000GXC1;
            AV19Soma = A40001GXC2;
            pr_default.readNext(5);
         }
         pr_default.close(5);
         if ( AV17Count > 0 )
         {
            AV16MediaCr = (int)((AV19Soma+AV9TmpEstExc)/ (decimal)(AV17Count));
         }
         else
         {
            AV16MediaCr = AV9TmpEstExc;
         }
         /* Using cursor P00AX12 */
         pr_default.execute(6, new Object[] {AV23ContratoServicos_Codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A160ContratoServicos_Codigo = P00AX12_A160ContratoServicos_Codigo[0];
            A1501ContratoServicos_TmpEstExc = P00AX12_A1501ContratoServicos_TmpEstExc[0];
            n1501ContratoServicos_TmpEstExc = P00AX12_n1501ContratoServicos_TmpEstExc[0];
            A1501ContratoServicos_TmpEstExc = AV15Media;
            n1501ContratoServicos_TmpEstExc = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00AX13 */
            pr_default.execute(7, new Object[] {n1501ContratoServicos_TmpEstExc, A1501ContratoServicos_TmpEstExc, A160ContratoServicos_Codigo});
            pr_default.close(7);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicos") ;
            if (true) break;
            /* Using cursor P00AX14 */
            pr_default.execute(8, new Object[] {n1501ContratoServicos_TmpEstExc, A1501ContratoServicos_TmpEstExc, A160ContratoServicos_Codigo});
            pr_default.close(8);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicos") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(6);
         /* Using cursor P00AX15 */
         pr_default.execute(9, new Object[] {AV10Contratada, AV12Contador});
         while ( (pr_default.getStatus(9) != 101) )
         {
            A69ContratadaUsuario_UsuarioCod = P00AX15_A69ContratadaUsuario_UsuarioCod[0];
            A66ContratadaUsuario_ContratadaCod = P00AX15_A66ContratadaUsuario_ContratadaCod[0];
            A1503ContratadaUsuario_TmpEstExc = P00AX15_A1503ContratadaUsuario_TmpEstExc[0];
            n1503ContratadaUsuario_TmpEstExc = P00AX15_n1503ContratadaUsuario_TmpEstExc[0];
            A1503ContratadaUsuario_TmpEstExc = AV16MediaCr;
            n1503ContratadaUsuario_TmpEstExc = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00AX16 */
            pr_default.execute(10, new Object[] {n1503ContratadaUsuario_TmpEstExc, A1503ContratadaUsuario_TmpEstExc, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
            pr_default.close(10);
            dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
            if (true) break;
            /* Using cursor P00AX17 */
            pr_default.execute(11, new Object[] {n1503ContratadaUsuario_TmpEstExc, A1503ContratadaUsuario_TmpEstExc, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
            pr_default.close(11);
            dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(9);
         /* Using cursor P00AX18 */
         pr_default.execute(12, new Object[] {AV12Contador, AV11Servico});
         while ( (pr_default.getStatus(12) != 101) )
         {
            A829UsuarioServicos_ServicoCod = P00AX18_A829UsuarioServicos_ServicoCod[0];
            A828UsuarioServicos_UsuarioCod = P00AX18_A828UsuarioServicos_UsuarioCod[0];
            A1513UsuarioServicos_TmpEstExc = P00AX18_A1513UsuarioServicos_TmpEstExc[0];
            n1513UsuarioServicos_TmpEstExc = P00AX18_n1513UsuarioServicos_TmpEstExc[0];
            A1513UsuarioServicos_TmpEstExc = AV16MediaCr;
            n1513UsuarioServicos_TmpEstExc = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00AX19 */
            pr_default.execute(13, new Object[] {n1513UsuarioServicos_TmpEstExc, A1513UsuarioServicos_TmpEstExc, A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
            pr_default.close(13);
            dsDefault.SmartCacheProvider.SetUpdated("UsuarioServicos") ;
            if (true) break;
            /* Using cursor P00AX20 */
            pr_default.execute(14, new Object[] {n1513UsuarioServicos_TmpEstExc, A1513UsuarioServicos_TmpEstExc, A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
            pr_default.close(14);
            dsDefault.SmartCacheProvider.SetUpdated("UsuarioServicos") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(12);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00AX3_A456ContagemResultado_Codigo = new int[1] ;
         P00AX3_A1510ContagemResultado_FimExc = new DateTime[] {DateTime.MinValue} ;
         P00AX3_n1510ContagemResultado_FimExc = new bool[] {false} ;
         P00AX3_A1509ContagemResultado_InicioExc = new DateTime[] {DateTime.MinValue} ;
         P00AX3_n1509ContagemResultado_InicioExc = new bool[] {false} ;
         P00AX3_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00AX3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00AX3_A601ContagemResultado_Servico = new int[1] ;
         P00AX3_n601ContagemResultado_Servico = new bool[] {false} ;
         P00AX3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00AX3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00AX3_A1505ContagemResultado_TmpEstExc = new int[1] ;
         P00AX3_n1505ContagemResultado_TmpEstExc = new bool[] {false} ;
         P00AX3_A584ContagemResultado_ContadorFM = new int[1] ;
         P00AX3_n584ContagemResultado_ContadorFM = new bool[] {false} ;
         A1510ContagemResultado_FimExc = (DateTime)(DateTime.MinValue);
         A1509ContagemResultado_InicioExc = (DateTime)(DateTime.MinValue);
         P00AX8_A456ContagemResultado_Codigo = new int[1] ;
         P00AX8_A602ContagemResultado_OSVinculada = new int[1] ;
         P00AX8_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00AX8_A1505ContagemResultado_TmpEstExc = new int[1] ;
         P00AX8_n1505ContagemResultado_TmpEstExc = new bool[] {false} ;
         P00AX8_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00AX8_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00AX8_A40000GXC1 = new int[1] ;
         P00AX8_n40000GXC1 = new bool[] {false} ;
         P00AX8_A40001GXC2 = new int[1] ;
         P00AX8_n40001GXC2 = new bool[] {false} ;
         P00AX11_A456ContagemResultado_Codigo = new int[1] ;
         P00AX11_A602ContagemResultado_OSVinculada = new int[1] ;
         P00AX11_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00AX11_A1505ContagemResultado_TmpEstExc = new int[1] ;
         P00AX11_n1505ContagemResultado_TmpEstExc = new bool[] {false} ;
         P00AX11_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00AX11_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00AX11_A584ContagemResultado_ContadorFM = new int[1] ;
         P00AX11_n584ContagemResultado_ContadorFM = new bool[] {false} ;
         P00AX11_A40000GXC1 = new int[1] ;
         P00AX11_n40000GXC1 = new bool[] {false} ;
         P00AX11_A40001GXC2 = new int[1] ;
         P00AX11_n40001GXC2 = new bool[] {false} ;
         P00AX12_A160ContratoServicos_Codigo = new int[1] ;
         P00AX12_A1501ContratoServicos_TmpEstExc = new int[1] ;
         P00AX12_n1501ContratoServicos_TmpEstExc = new bool[] {false} ;
         P00AX15_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00AX15_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00AX15_A1503ContratadaUsuario_TmpEstExc = new int[1] ;
         P00AX15_n1503ContratadaUsuario_TmpEstExc = new bool[] {false} ;
         P00AX18_A829UsuarioServicos_ServicoCod = new int[1] ;
         P00AX18_A828UsuarioServicos_UsuarioCod = new int[1] ;
         P00AX18_A1513UsuarioServicos_TmpEstExc = new int[1] ;
         P00AX18_n1513UsuarioServicos_TmpEstExc = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_setfimexc__default(),
            new Object[][] {
                new Object[] {
               P00AX3_A456ContagemResultado_Codigo, P00AX3_A1510ContagemResultado_FimExc, P00AX3_n1510ContagemResultado_FimExc, P00AX3_A1509ContagemResultado_InicioExc, P00AX3_n1509ContagemResultado_InicioExc, P00AX3_A490ContagemResultado_ContratadaCod, P00AX3_n490ContagemResultado_ContratadaCod, P00AX3_A601ContagemResultado_Servico, P00AX3_n601ContagemResultado_Servico, P00AX3_A1553ContagemResultado_CntSrvCod,
               P00AX3_n1553ContagemResultado_CntSrvCod, P00AX3_A1505ContagemResultado_TmpEstExc, P00AX3_n1505ContagemResultado_TmpEstExc, P00AX3_A584ContagemResultado_ContadorFM, P00AX3_n584ContagemResultado_ContadorFM
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00AX8_A456ContagemResultado_Codigo, P00AX8_A602ContagemResultado_OSVinculada, P00AX8_n602ContagemResultado_OSVinculada, P00AX8_A1505ContagemResultado_TmpEstExc, P00AX8_n1505ContagemResultado_TmpEstExc, P00AX8_A1553ContagemResultado_CntSrvCod, P00AX8_n1553ContagemResultado_CntSrvCod, P00AX8_A40000GXC1, P00AX8_n40000GXC1, P00AX8_A40001GXC2,
               P00AX8_n40001GXC2
               }
               , new Object[] {
               P00AX11_A456ContagemResultado_Codigo, P00AX11_A602ContagemResultado_OSVinculada, P00AX11_n602ContagemResultado_OSVinculada, P00AX11_A1505ContagemResultado_TmpEstExc, P00AX11_n1505ContagemResultado_TmpEstExc, P00AX11_A1553ContagemResultado_CntSrvCod, P00AX11_n1553ContagemResultado_CntSrvCod, P00AX11_A584ContagemResultado_ContadorFM, P00AX11_n584ContagemResultado_ContadorFM, P00AX11_A40000GXC1,
               P00AX11_n40000GXC1, P00AX11_A40001GXC2, P00AX11_n40001GXC2
               }
               , new Object[] {
               P00AX12_A160ContratoServicos_Codigo, P00AX12_A1501ContratoServicos_TmpEstExc, P00AX12_n1501ContratoServicos_TmpEstExc
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00AX15_A69ContratadaUsuario_UsuarioCod, P00AX15_A66ContratadaUsuario_ContratadaCod, P00AX15_A1503ContratadaUsuario_TmpEstExc, P00AX15_n1503ContratadaUsuario_TmpEstExc
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00AX18_A829UsuarioServicos_ServicoCod, P00AX18_A828UsuarioServicos_UsuarioCod, P00AX18_A1513UsuarioServicos_TmpEstExc, P00AX18_n1513UsuarioServicos_TmpEstExc
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV18Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1505ContagemResultado_TmpEstExc ;
      private int A584ContagemResultado_ContadorFM ;
      private int AV10Contratada ;
      private int AV11Servico ;
      private int AV12Contador ;
      private int AV23ContratoServicos_Codigo ;
      private int AV9TmpEstExc ;
      private int GXt_int1 ;
      private int GXt_int2 ;
      private int AV13Horas ;
      private int A602ContagemResultado_OSVinculada ;
      private int A40000GXC1 ;
      private int A40001GXC2 ;
      private int AV17Count ;
      private int AV19Soma ;
      private int AV15Media ;
      private int AV16MediaCr ;
      private int A160ContratoServicos_Codigo ;
      private int A1501ContratoServicos_TmpEstExc ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A1503ContratadaUsuario_TmpEstExc ;
      private int A829UsuarioServicos_ServicoCod ;
      private int A828UsuarioServicos_UsuarioCod ;
      private int A1513UsuarioServicos_TmpEstExc ;
      private String scmdbuf ;
      private DateTime A1510ContagemResultado_FimExc ;
      private DateTime A1509ContagemResultado_InicioExc ;
      private bool n1510ContagemResultado_FimExc ;
      private bool n1509ContagemResultado_InicioExc ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1505ContagemResultado_TmpEstExc ;
      private bool n584ContagemResultado_ContadorFM ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n40000GXC1 ;
      private bool n40001GXC2 ;
      private bool n1501ContratoServicos_TmpEstExc ;
      private bool n1503ContratadaUsuario_TmpEstExc ;
      private bool n1513UsuarioServicos_TmpEstExc ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00AX3_A456ContagemResultado_Codigo ;
      private DateTime[] P00AX3_A1510ContagemResultado_FimExc ;
      private bool[] P00AX3_n1510ContagemResultado_FimExc ;
      private DateTime[] P00AX3_A1509ContagemResultado_InicioExc ;
      private bool[] P00AX3_n1509ContagemResultado_InicioExc ;
      private int[] P00AX3_A490ContagemResultado_ContratadaCod ;
      private bool[] P00AX3_n490ContagemResultado_ContratadaCod ;
      private int[] P00AX3_A601ContagemResultado_Servico ;
      private bool[] P00AX3_n601ContagemResultado_Servico ;
      private int[] P00AX3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00AX3_n1553ContagemResultado_CntSrvCod ;
      private int[] P00AX3_A1505ContagemResultado_TmpEstExc ;
      private bool[] P00AX3_n1505ContagemResultado_TmpEstExc ;
      private int[] P00AX3_A584ContagemResultado_ContadorFM ;
      private bool[] P00AX3_n584ContagemResultado_ContadorFM ;
      private int[] P00AX8_A456ContagemResultado_Codigo ;
      private int[] P00AX8_A602ContagemResultado_OSVinculada ;
      private bool[] P00AX8_n602ContagemResultado_OSVinculada ;
      private int[] P00AX8_A1505ContagemResultado_TmpEstExc ;
      private bool[] P00AX8_n1505ContagemResultado_TmpEstExc ;
      private int[] P00AX8_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00AX8_n1553ContagemResultado_CntSrvCod ;
      private int[] P00AX8_A40000GXC1 ;
      private bool[] P00AX8_n40000GXC1 ;
      private int[] P00AX8_A40001GXC2 ;
      private bool[] P00AX8_n40001GXC2 ;
      private int[] P00AX11_A456ContagemResultado_Codigo ;
      private int[] P00AX11_A602ContagemResultado_OSVinculada ;
      private bool[] P00AX11_n602ContagemResultado_OSVinculada ;
      private int[] P00AX11_A1505ContagemResultado_TmpEstExc ;
      private bool[] P00AX11_n1505ContagemResultado_TmpEstExc ;
      private int[] P00AX11_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00AX11_n1553ContagemResultado_CntSrvCod ;
      private int[] P00AX11_A584ContagemResultado_ContadorFM ;
      private bool[] P00AX11_n584ContagemResultado_ContadorFM ;
      private int[] P00AX11_A40000GXC1 ;
      private bool[] P00AX11_n40000GXC1 ;
      private int[] P00AX11_A40001GXC2 ;
      private bool[] P00AX11_n40001GXC2 ;
      private int[] P00AX12_A160ContratoServicos_Codigo ;
      private int[] P00AX12_A1501ContratoServicos_TmpEstExc ;
      private bool[] P00AX12_n1501ContratoServicos_TmpEstExc ;
      private int[] P00AX15_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00AX15_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00AX15_A1503ContratadaUsuario_TmpEstExc ;
      private bool[] P00AX15_n1503ContratadaUsuario_TmpEstExc ;
      private int[] P00AX18_A829UsuarioServicos_ServicoCod ;
      private int[] P00AX18_A828UsuarioServicos_UsuarioCod ;
      private int[] P00AX18_A1513UsuarioServicos_TmpEstExc ;
      private bool[] P00AX18_n1513UsuarioServicos_TmpEstExc ;
   }

   public class prc_setfimexc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AX3 ;
          prmP00AX3 = new Object[] {
          new Object[] {"@AV18Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AX4 ;
          prmP00AX4 = new Object[] {
          new Object[] {"@ContagemResultado_FimExc",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AX5 ;
          prmP00AX5 = new Object[] {
          new Object[] {"@ContagemResultado_FimExc",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AX6 ;
          prmP00AX6 = new Object[] {
          new Object[] {"@ContagemResultado_FimExc",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AX8 ;
          prmP00AX8 = new Object[] {
          new Object[] {"@AV23ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AX11 ;
          prmP00AX11 = new Object[] {
          new Object[] {"@AV23ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12Contador",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AX12 ;
          prmP00AX12 = new Object[] {
          new Object[] {"@AV23ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AX13 ;
          prmP00AX13 = new Object[] {
          new Object[] {"@ContratoServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AX14 ;
          prmP00AX14 = new Object[] {
          new Object[] {"@ContratoServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AX15 ;
          prmP00AX15 = new Object[] {
          new Object[] {"@AV10Contratada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12Contador",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AX16 ;
          prmP00AX16 = new Object[] {
          new Object[] {"@ContratadaUsuario_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AX17 ;
          prmP00AX17 = new Object[] {
          new Object[] {"@ContratadaUsuario_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AX18 ;
          prmP00AX18 = new Object[] {
          new Object[] {"@AV12Contador",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AX19 ;
          prmP00AX19 = new Object[] {
          new Object[] {"@UsuarioServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AX20 ;
          prmP00AX20 = new Object[] {
          new Object[] {"@UsuarioServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AX3", "SELECT T1.[ContagemResultado_Codigo], T1.[ContagemResultado_FimExc], T1.[ContagemResultado_InicioExc], T1.[ContagemResultado_ContratadaCod], T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_TmpEstExc], COALESCE( T2.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM FROM (([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV18Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AX3,1,0,true,true )
             ,new CursorDef("P00AX4", "UPDATE [ContagemResultado] SET [ContagemResultado_FimExc]=@ContagemResultado_FimExc, [ContagemResultado_TmpEstExc]=@ContagemResultado_TmpEstExc  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AX4)
             ,new CursorDef("P00AX5", "UPDATE [ContagemResultado] SET [ContagemResultado_FimExc]=@ContagemResultado_FimExc, [ContagemResultado_TmpEstExc]=@ContagemResultado_TmpEstExc  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AX5)
             ,new CursorDef("P00AX6", "UPDATE [ContagemResultado] SET [ContagemResultado_FimExc]=@ContagemResultado_FimExc, [ContagemResultado_TmpEstExc]=@ContagemResultado_TmpEstExc  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AX6)
             ,new CursorDef("P00AX8", "SELECT T1.[ContagemResultado_Codigo], T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_TmpEstExc], T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, COALESCE( T2.[GXC1], 0) AS GXC1, COALESCE( T2.[GXC2], 0) AS GXC2 FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT COUNT(*) AS GXC1, [ContagemResultado_Codigo], [ContagemResultado_OSVinculada], SUM([ContagemResultado_TmpEstExc]) AS GXC2 FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo], [ContagemResultado_OSVinculada] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo] AND T2.[ContagemResultado_OSVinculada] = T1.[ContagemResultado_OSVinculada]) WHERE (T1.[ContagemResultado_CntSrvCod] = @AV23ContratoServicos_Codigo) AND (T1.[ContagemResultado_TmpEstExc] > 0) ORDER BY T1.[ContagemResultado_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AX8,100,0,false,false )
             ,new CursorDef("P00AX11", "SELECT T1.[ContagemResultado_Codigo], T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_TmpEstExc], T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, COALESCE( T2.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T3.[GXC1], 0) AS GXC1, COALESCE( T3.[GXC2], 0) AS GXC2 FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT COUNT(*) AS GXC1, [ContagemResultado_Codigo], [ContagemResultado_OSVinculada], SUM([ContagemResultado_TmpEstExc]) AS GXC2 FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo], [ContagemResultado_OSVinculada] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo] AND T3.[ContagemResultado_OSVinculada] = T1.[ContagemResultado_OSVinculada]) WHERE (T1.[ContagemResultado_CntSrvCod] = @AV23ContratoServicos_Codigo) AND (T1.[ContagemResultado_TmpEstExc] > 0) AND (COALESCE( T2.[ContagemResultado_ContadorFM], 0) = @AV12Contador) ORDER BY T1.[ContagemResultado_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AX11,100,0,false,false )
             ,new CursorDef("P00AX12", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicos_TmpEstExc] FROM [ContratoServicos] WITH (UPDLOCK) WHERE [ContratoServicos_Codigo] = @AV23ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AX12,1,0,true,true )
             ,new CursorDef("P00AX13", "UPDATE [ContratoServicos] SET [ContratoServicos_TmpEstExc]=@ContratoServicos_TmpEstExc  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AX13)
             ,new CursorDef("P00AX14", "UPDATE [ContratoServicos] SET [ContratoServicos_TmpEstExc]=@ContratoServicos_TmpEstExc  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AX14)
             ,new CursorDef("P00AX15", "SELECT TOP 1 [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_TmpEstExc] FROM [ContratadaUsuario] WITH (UPDLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @AV10Contratada and [ContratadaUsuario_UsuarioCod] = @AV12Contador ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AX15,1,0,true,true )
             ,new CursorDef("P00AX16", "UPDATE [ContratadaUsuario] SET [ContratadaUsuario_TmpEstExc]=@ContratadaUsuario_TmpEstExc  WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AX16)
             ,new CursorDef("P00AX17", "UPDATE [ContratadaUsuario] SET [ContratadaUsuario_TmpEstExc]=@ContratadaUsuario_TmpEstExc  WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AX17)
             ,new CursorDef("P00AX18", "SELECT TOP 1 [UsuarioServicos_ServicoCod], [UsuarioServicos_UsuarioCod], [UsuarioServicos_TmpEstExc] FROM [UsuarioServicos] WITH (UPDLOCK) WHERE [UsuarioServicos_UsuarioCod] = @AV12Contador and [UsuarioServicos_ServicoCod] = @AV11Servico ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AX18,1,0,true,true )
             ,new CursorDef("P00AX19", "UPDATE [UsuarioServicos] SET [UsuarioServicos_TmpEstExc]=@UsuarioServicos_TmpEstExc  WHERE [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod AND [UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AX19)
             ,new CursorDef("P00AX20", "UPDATE [UsuarioServicos] SET [UsuarioServicos_TmpEstExc]=@UsuarioServicos_TmpEstExc  WHERE [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod AND [UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AX20)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
       }
    }

 }

}
