/*
               File: PRC_Projeto_SistemaCod
        Description: Projeto_Sistema Cod
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:29.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_projeto_sistemacod : GXProcedure
   {
      public prc_projeto_sistemacod( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_projeto_sistemacod( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Projeto_Codigo ,
                           ref int aP1_Sistema_Codigo )
      {
         this.A648Projeto_Codigo = aP0_Projeto_Codigo;
         this.AV8Sistema_Codigo = aP1_Sistema_Codigo;
         initialize();
         executePrivate();
         aP0_Projeto_Codigo=this.A648Projeto_Codigo;
         aP1_Sistema_Codigo=this.AV8Sistema_Codigo;
      }

      public int executeUdp( ref int aP0_Projeto_Codigo )
      {
         this.A648Projeto_Codigo = aP0_Projeto_Codigo;
         this.AV8Sistema_Codigo = aP1_Sistema_Codigo;
         initialize();
         executePrivate();
         aP0_Projeto_Codigo=this.A648Projeto_Codigo;
         aP1_Sistema_Codigo=this.AV8Sistema_Codigo;
         return AV8Sistema_Codigo ;
      }

      public void executeSubmit( ref int aP0_Projeto_Codigo ,
                                 ref int aP1_Sistema_Codigo )
      {
         prc_projeto_sistemacod objprc_projeto_sistemacod;
         objprc_projeto_sistemacod = new prc_projeto_sistemacod();
         objprc_projeto_sistemacod.A648Projeto_Codigo = aP0_Projeto_Codigo;
         objprc_projeto_sistemacod.AV8Sistema_Codigo = aP1_Sistema_Codigo;
         objprc_projeto_sistemacod.context.SetSubmitInitialConfig(context);
         objprc_projeto_sistemacod.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_projeto_sistemacod);
         aP0_Projeto_Codigo=this.A648Projeto_Codigo;
         aP1_Sistema_Codigo=this.AV8Sistema_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_projeto_sistemacod)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P005D2 */
         pr_default.execute(0, new Object[] {A648Projeto_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A694ProjetoMelhoria_ProjetoCod = P005D2_A694ProjetoMelhoria_ProjetoCod[0];
            A695ProjetoMelhoria_SistemaCod = P005D2_A695ProjetoMelhoria_SistemaCod[0];
            A736ProjetoMelhoria_Codigo = P005D2_A736ProjetoMelhoria_Codigo[0];
            AV8Sistema_Codigo = A695ProjetoMelhoria_SistemaCod;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P005D2_A694ProjetoMelhoria_ProjetoCod = new int[1] ;
         P005D2_A695ProjetoMelhoria_SistemaCod = new int[1] ;
         P005D2_A736ProjetoMelhoria_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_projeto_sistemacod__default(),
            new Object[][] {
                new Object[] {
               P005D2_A694ProjetoMelhoria_ProjetoCod, P005D2_A695ProjetoMelhoria_SistemaCod, P005D2_A736ProjetoMelhoria_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A648Projeto_Codigo ;
      private int AV8Sistema_Codigo ;
      private int A694ProjetoMelhoria_ProjetoCod ;
      private int A695ProjetoMelhoria_SistemaCod ;
      private int A736ProjetoMelhoria_Codigo ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Projeto_Codigo ;
      private int aP1_Sistema_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P005D2_A694ProjetoMelhoria_ProjetoCod ;
      private int[] P005D2_A695ProjetoMelhoria_SistemaCod ;
      private int[] P005D2_A736ProjetoMelhoria_Codigo ;
   }

   public class prc_projeto_sistemacod__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005D2 ;
          prmP005D2 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005D2", "SELECT TOP 1 [ProjetoMelhoria_ProjetoCod], [ProjetoMelhoria_SistemaCod], [ProjetoMelhoria_Codigo] FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE [ProjetoMelhoria_ProjetoCod] = @Projeto_Codigo ORDER BY [ProjetoMelhoria_ProjetoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005D2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
