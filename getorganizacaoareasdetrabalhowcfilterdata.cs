/*
               File: GetOrganizacaoAreasDeTrabalhoWCFilterData
        Description: Get Organizacao Areas De Trabalho WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:24.68
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getorganizacaoareasdetrabalhowcfilterdata : GXProcedure
   {
      public getorganizacaoareasdetrabalhowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getorganizacaoareasdetrabalhowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
         return AV23OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getorganizacaoareasdetrabalhowcfilterdata objgetorganizacaoareasdetrabalhowcfilterdata;
         objgetorganizacaoareasdetrabalhowcfilterdata = new getorganizacaoareasdetrabalhowcfilterdata();
         objgetorganizacaoareasdetrabalhowcfilterdata.AV14DDOName = aP0_DDOName;
         objgetorganizacaoareasdetrabalhowcfilterdata.AV12SearchTxt = aP1_SearchTxt;
         objgetorganizacaoareasdetrabalhowcfilterdata.AV13SearchTxtTo = aP2_SearchTxtTo;
         objgetorganizacaoareasdetrabalhowcfilterdata.AV18OptionsJson = "" ;
         objgetorganizacaoareasdetrabalhowcfilterdata.AV21OptionsDescJson = "" ;
         objgetorganizacaoareasdetrabalhowcfilterdata.AV23OptionIndexesJson = "" ;
         objgetorganizacaoareasdetrabalhowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetorganizacaoareasdetrabalhowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetorganizacaoareasdetrabalhowcfilterdata);
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getorganizacaoareasdetrabalhowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Options = (IGxCollection)(new GxSimpleCollection());
         AV20OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV22OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV14DDOName), "DDO_AREATRABALHO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADAREATRABALHO_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV18OptionsJson = AV17Options.ToJSonString(false);
         AV21OptionsDescJson = AV20OptionsDesc.ToJSonString(false);
         AV23OptionIndexesJson = AV22OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get("OrganizacaoAreasDeTrabalhoWCGridState"), "") == 0 )
         {
            AV27GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "OrganizacaoAreasDeTrabalhoWCGridState"), "");
         }
         else
         {
            AV27GridState.FromXml(AV25Session.Get("OrganizacaoAreasDeTrabalhoWCGridState"), "");
         }
         AV33GXV1 = 1;
         while ( AV33GXV1 <= AV27GridState.gxTpr_Filtervalues.Count )
         {
            AV28GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV27GridState.gxTpr_Filtervalues.Item(AV33GXV1));
            if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_DESCRICAO") == 0 )
            {
               AV10TFAreaTrabalho_Descricao = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_DESCRICAO_SEL") == 0 )
            {
               AV11TFAreaTrabalho_Descricao_Sel = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "PARM_&AREATRABALHO_ORGANIZACAOCOD") == 0 )
            {
               AV30AreaTrabalho_OrganizacaoCod = (int)(NumberUtil.Val( AV28GridStateFilterValue.gxTpr_Value, "."));
            }
            AV33GXV1 = (int)(AV33GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADAREATRABALHO_DESCRICAOOPTIONS' Routine */
         AV10TFAreaTrabalho_Descricao = AV12SearchTxt;
         AV11TFAreaTrabalho_Descricao_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11TFAreaTrabalho_Descricao_Sel ,
                                              AV10TFAreaTrabalho_Descricao ,
                                              A6AreaTrabalho_Descricao ,
                                              AV30AreaTrabalho_OrganizacaoCod ,
                                              A1216AreaTrabalho_OrganizacaoCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV10TFAreaTrabalho_Descricao = StringUtil.Concat( StringUtil.RTrim( AV10TFAreaTrabalho_Descricao), "%", "");
         /* Using cursor P00QD2 */
         pr_default.execute(0, new Object[] {AV30AreaTrabalho_OrganizacaoCod, lV10TFAreaTrabalho_Descricao, AV11TFAreaTrabalho_Descricao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKQD2 = false;
            A1216AreaTrabalho_OrganizacaoCod = P00QD2_A1216AreaTrabalho_OrganizacaoCod[0];
            n1216AreaTrabalho_OrganizacaoCod = P00QD2_n1216AreaTrabalho_OrganizacaoCod[0];
            A6AreaTrabalho_Descricao = P00QD2_A6AreaTrabalho_Descricao[0];
            A5AreaTrabalho_Codigo = P00QD2_A5AreaTrabalho_Codigo[0];
            AV24count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00QD2_A1216AreaTrabalho_OrganizacaoCod[0] == A1216AreaTrabalho_OrganizacaoCod ) && ( StringUtil.StrCmp(P00QD2_A6AreaTrabalho_Descricao[0], A6AreaTrabalho_Descricao) == 0 ) )
            {
               BRKQD2 = false;
               A5AreaTrabalho_Codigo = P00QD2_A5AreaTrabalho_Codigo[0];
               AV24count = (long)(AV24count+1);
               BRKQD2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A6AreaTrabalho_Descricao)) )
            {
               AV16Option = A6AreaTrabalho_Descricao;
               AV17Options.Add(AV16Option, 0);
               AV22OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV24count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV17Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQD2 )
            {
               BRKQD2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Options = new GxSimpleCollection();
         AV20OptionsDesc = new GxSimpleCollection();
         AV22OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV25Session = context.GetSession();
         AV27GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV28GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFAreaTrabalho_Descricao = "";
         AV11TFAreaTrabalho_Descricao_Sel = "";
         scmdbuf = "";
         lV10TFAreaTrabalho_Descricao = "";
         A6AreaTrabalho_Descricao = "";
         P00QD2_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         P00QD2_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         P00QD2_A6AreaTrabalho_Descricao = new String[] {""} ;
         P00QD2_A5AreaTrabalho_Codigo = new int[1] ;
         AV16Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getorganizacaoareasdetrabalhowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00QD2_A1216AreaTrabalho_OrganizacaoCod, P00QD2_n1216AreaTrabalho_OrganizacaoCod, P00QD2_A6AreaTrabalho_Descricao, P00QD2_A5AreaTrabalho_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV33GXV1 ;
      private int AV30AreaTrabalho_OrganizacaoCod ;
      private int A1216AreaTrabalho_OrganizacaoCod ;
      private int A5AreaTrabalho_Codigo ;
      private long AV24count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool BRKQD2 ;
      private bool n1216AreaTrabalho_OrganizacaoCod ;
      private String AV23OptionIndexesJson ;
      private String AV18OptionsJson ;
      private String AV21OptionsDescJson ;
      private String AV14DDOName ;
      private String AV12SearchTxt ;
      private String AV13SearchTxtTo ;
      private String AV10TFAreaTrabalho_Descricao ;
      private String AV11TFAreaTrabalho_Descricao_Sel ;
      private String lV10TFAreaTrabalho_Descricao ;
      private String A6AreaTrabalho_Descricao ;
      private String AV16Option ;
      private IGxSession AV25Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00QD2_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] P00QD2_n1216AreaTrabalho_OrganizacaoCod ;
      private String[] P00QD2_A6AreaTrabalho_Descricao ;
      private int[] P00QD2_A5AreaTrabalho_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV27GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV28GridStateFilterValue ;
   }

   public class getorganizacaoareasdetrabalhowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00QD2( IGxContext context ,
                                             String AV11TFAreaTrabalho_Descricao_Sel ,
                                             String AV10TFAreaTrabalho_Descricao ,
                                             String A6AreaTrabalho_Descricao ,
                                             int AV30AreaTrabalho_OrganizacaoCod ,
                                             int A1216AreaTrabalho_OrganizacaoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [3] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [AreaTrabalho_OrganizacaoCod], [AreaTrabalho_Descricao], [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([AreaTrabalho_OrganizacaoCod] = @AV30AreaTrabalho_OrganizacaoCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFAreaTrabalho_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFAreaTrabalho_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([AreaTrabalho_Descricao] like @lV10TFAreaTrabalho_Descricao)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFAreaTrabalho_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([AreaTrabalho_Descricao] = @AV11TFAreaTrabalho_Descricao_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [AreaTrabalho_OrganizacaoCod], [AreaTrabalho_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00QD2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00QD2 ;
          prmP00QD2 = new Object[] {
          new Object[] {"@AV30AreaTrabalho_OrganizacaoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFAreaTrabalho_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV11TFAreaTrabalho_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00QD2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00QD2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getorganizacaoareasdetrabalhowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getorganizacaoareasdetrabalhowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getorganizacaoareasdetrabalhowcfilterdata") )
          {
             return  ;
          }
          getorganizacaoareasdetrabalhowcfilterdata worker = new getorganizacaoareasdetrabalhowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
