/*
               File: MasterPageMeetrika
        Description: Master Page Meetrika
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:26:38.0
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class masterpagemeetrika : GXMasterPage, System.Web.SessionState.IRequiresSessionState
   {
      public masterpagemeetrika( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public masterpagemeetrika( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavNewusuario_codigo = new GXCombobox();
         cmbavAreatrabalho_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         initialize_properties( ) ;
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            PAMO2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavQtderro_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavQtderro_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtderro_Enabled), 5, 0)));
               edtavQtdlrnj_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavQtdlrnj_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdlrnj_Enabled), 5, 0)));
               edtavQtdpend_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavQtdpend_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdpend_Enabled), 5, 0)));
               edtavQtdsolicitadas_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavQtdsolicitadas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdsolicitadas_Enabled), 5, 0)));
               WSMO2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEMO2( ) ;
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         if ( ! isFullAjaxMode( ) )
         {
            getDataAreaObject().RenderHtmlHeaders();
         }
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( ! isFullAjaxMode( ) )
         {
            getDataAreaObject().RenderHtmlOpenForm();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "vTABSMENUDATA_MPAGE", AV40TabsMenuData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTABSMENUDATA_MPAGE", AV40TabsMenuData);
         }
         GxWebStd.gx_hidden_field( context, "HELPSISTEMA_OBJETO_MPAGE", A1639HelpSistema_Objeto);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "vWWPCONTEXT_MPAGE", AV51WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT_MPAGE", AV51WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_CODIGO_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_CODIGO_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_DESCRICAO_MPAGE", A6AreaTrabalho_Descricao);
         GxWebStd.gx_boolean_hidden_field( context, "AREATRABALHO_ATIVO_MPAGE", A72AreaTrabalho_Ativo);
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_RAZAOSOCIAL_MPAGE", StringUtil.RTrim( A9Contratante_RazaoSocial));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_NOMEFANTASIA_MPAGE", StringUtil.RTrim( A10Contratante_NomeFantasia));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_TELEFONE_MPAGE", StringUtil.RTrim( A31Contratante_Telefone));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_SERVICOPADRAO_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_CALCULOPFINAL_MPAGE", StringUtil.RTrim( A642AreaTrabalho_CalculoPFinal));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTECOD_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATANTE_CODIGO_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_USUARIOCOD_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTERAZ_MPAGE", StringUtil.RTrim( A64ContratanteUsuario_ContratanteRaz));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTEFAN_MPAGE", StringUtil.RTrim( A65ContratanteUsuario_ContratanteFan));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_SIGLA_MPAGE", StringUtil.RTrim( A2208Contratante_Sigla));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_AREATRABALHOCOD_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOCOD_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADACOD_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADAPESSOACOD_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM_MPAGE", StringUtil.RTrim( A68ContratadaUsuario_ContratadaPessoaNom));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_SIGLA_MPAGE", StringUtil.RTrim( A438Contratada_Sigla));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATADACOD_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_USUARIOCOD_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATOCOD_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_CODIGO_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOAUXILIAR_USUARIOCOD_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1825ContratoAuxiliar_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_CODIGO_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A330ParametrosSistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_URLOTHERVER_MPAGE", A1952ParametrosSistema_URLOtherVer);
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_VALIDACAO_MPAGE", A1954ParametrosSistema_Validacao);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN_MPAGE", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_RESPONSAVEL_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A890ContagemResultado_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATAENTREGA_MPAGE", context.localUtil.DToC( A472ContagemResultado_DataEntrega, 0, "/"));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "vCONTRATADAS_MPAGE", AV9Contratadas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADAS_MPAGE", AV9Contratadas);
         }
         GxWebStd.gx_hidden_field( context, "vSERVERDATE_MPAGE", context.localUtil.DToC( AV5ServerDate, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSULTCNT_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A531ContagemResultado_StatusUltCnt), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OSVINCULADA_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADAPESSOACOD_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A499ContagemResultado_ContratadaPessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "USUARIO_ATIVO_MPAGE", A54Usuario_Ativo);
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_AREATRABALHOCOD_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1020ContratanteUsuario_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATADAUSUARIO_USUARIOATIVO_MPAGE", A1394ContratadaUsuario_UsuarioAtivo);
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATADA_ATIVO_MPAGE", A43Contratada_Ativo);
         GxWebStd.gx_hidden_field( context, "vLASTUSERAREATRABALHO_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20LastUserAreaTrabalho), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PERFIL_NOME_MPAGE", StringUtil.RTrim( A4Perfil_Nome));
         GxWebStd.gx_hidden_field( context, "USUARIO_EMAIL_MPAGE", A1647Usuario_Email);
         GxWebStd.gx_hidden_field( context, "USUARIO_PESSOANOM_MPAGE", StringUtil.RTrim( A58Usuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, "vENDIDADEUSUARIO_MPAGE", StringUtil.RTrim( AV73EndidadeUsuario));
         GxWebStd.gx_boolean_hidden_field( context, "USUARIOPERFIL_INSERT_MPAGE", A544UsuarioPerfil_Insert);
         GxWebStd.gx_boolean_hidden_field( context, "USUARIOPERFIL_UPDATE_MPAGE", A659UsuarioPerfil_Update);
         GxWebStd.gx_boolean_hidden_field( context, "USUARIOPERFIL_DELETE_MPAGE", A546UsuarioPerfil_Delete);
         GxWebStd.gx_boolean_hidden_field( context, "USUARIOPERFIL_DISPLAY_MPAGE", A543UsuarioPerfil_Display);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "vVERMELHAS_MPAGE", AV47Vermelhas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVERMELHAS_MPAGE", AV47Vermelhas);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "vSDT_FILTROCONSCONTADORFM_MPAGE", AV38SDT_FiltroConsContadorFM);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_FILTROCONSCONTADORFM_MPAGE", AV38SDT_FiltroConsContadorFM);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "vLARANJAS_MPAGE", AV52Laranjas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLARANJAS_MPAGE", AV52Laranjas);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "vAMARELAS_MPAGE", AV53Amarelas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAMARELAS_MPAGE", AV53Amarelas);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "vVERDES_MPAGE", AV54Verdes);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVERDES_MPAGE", AV54Verdes);
         }
         GxWebStd.gx_boolean_hidden_field( context, "USUARIO_EHCONTADOR_MPAGE", A289Usuario_EhContador);
         GxWebStd.gx_boolean_hidden_field( context, "USUARIO_EHFINANCEIRO_MPAGE", A293Usuario_EhFinanceiro);
         GxWebStd.gx_hidden_field( context, "USUARIO_USERGAMGUID_MPAGE", StringUtil.RTrim( A341Usuario_UserGamGuid));
         GxWebStd.gx_hidden_field( context, "USUARIO_ULTIMAAREA_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2016Usuario_UltimaArea), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO_MPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "USUARIO_EHAUDITORFM_MPAGE", A290Usuario_EhAuditorFM);
         GxWebStd.gx_hidden_field( context, "DROPDOWNTABSMENU1_MPAGE_Height", StringUtil.RTrim( Dropdowntabsmenu1_Height));
         GxWebStd.gx_hidden_field( context, "DROPDOWNTABSMENU1_MPAGE_Menustyle", StringUtil.RTrim( Dropdowntabsmenu1_Menustyle));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_MPAGE_Width", StringUtil.RTrim( Confirmpanel_Width));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_MPAGE_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_MPAGE_Confirmationtext", StringUtil.RTrim( Confirmpanel_Confirmationtext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_MPAGE_Yesbuttoncaption", StringUtil.RTrim( Confirmpanel_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_MPAGE_Nobuttoncaption", StringUtil.RTrim( Confirmpanel_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_MPAGE_Cancelbuttoncaption", StringUtil.RTrim( Confirmpanel_Cancelbuttoncaption));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_MPAGE_Yesbuttonposition", StringUtil.RTrim( Confirmpanel_Yesbuttonposition));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_MPAGE_Confirmtype", StringUtil.RTrim( Confirmpanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_MPAGE_Texttype", StringUtil.RTrim( Confirmpanel_Texttype));
         GxWebStd.gx_hidden_field( context, "FORM_MPAGE_Caption", StringUtil.RTrim( (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Caption));
         GxWebStd.gx_hidden_field( context, "vWWPCONTEXT_MPAGE_Areatrabalho_codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51WWPContext.gxTpr_Areatrabalho_codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vWWPCONTEXT_MPAGE_Areatrabalho_descricao", AV51WWPContext.gxTpr_Areatrabalho_descricao);
         GxWebStd.gx_hidden_field( context, "vNEWUSUARIO_CODIGO_MPAGE_Text", StringUtil.RTrim( cmbavNewusuario_codigo.Description));
         GxWebStd.gx_hidden_field( context, "SCREENDETECTOR_MPAGE_Screenwidth", StringUtil.RTrim( Screendetector_Screenwidth));
         GxWebStd.gx_hidden_field( context, "SCREENDETECTOR_MPAGE_Screenheight", StringUtil.RTrim( Screendetector_Screenheight));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormMO2( )
      {
         SendCloseFormHiddens( ) ;
         SendSecurityToken((String)(sPrefix));
         if ( ! isFullAjaxMode( ) )
         {
            getDataAreaObject().RenderHtmlCloseForm();
         }
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( ! ( WebComp_Wcrecentlinks == null ) )
         {
            WebComp_Wcrecentlinks.componentjscripts();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DropDownTabsMenu/dropdowntabs.js", "");
         context.AddJavascriptSource("DropDownTabsMenu/DropDownTabsMenuRender.js", "");
         context.AddJavascriptSource("ScreenDetector/ScreenDetectorRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("masterpagemeetrika.js", "?20206216263964");
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "MasterPageMeetrika" ;
      }

      public override String GetPgmdesc( )
      {
         return "Master Page Meetrika" ;
      }

      protected void WBMO0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            if ( ! ShowMPWhenPopUp( ) && context.isPopUpObject( ) )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableOutput();
               }
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               /* Content placeholder */
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gx-content-placeholder");
               context.WriteHtmlText( ">") ;
               if ( ! isFullAjaxMode( ) )
               {
                  getDataAreaObject().RenderHtmlContent();
               }
               context.WriteHtmlText( "</div>") ;
               if ( context.isSpaRequest( ) )
               {
                  disableOutput();
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
               wbLoad = true;
               return  ;
            }
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection1_Internalname, 1, 0, "px", 0, "px", "conteudo", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection2_Internalname, 1, 0, "px", 0, "px", "container-fluid", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection3_Internalname, 1, 0, "px", 0, "px", "row faixa", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection4_Internalname, 1, 0, "px", 0, "px", "col-lg-9 col-md-9 col-sm-7 col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection5_Internalname, 1, 0, "px", 0, "px", "menu", "left", "top", "", "", "div");
            wb_table1_7_MO2( true) ;
         }
         else
         {
            wb_table1_7_MO2( false) ;
         }
         return  ;
      }

      protected void wb_table1_7_MO2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection6_Internalname, 1, 0, "px", 0, "px", "col-lg-3 col-md-3 col-sm-5 col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection7_Internalname, 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection8_Internalname, 1, 0, "px", 0, "px", "col-lg-12 col-md-12 col-sm-12 col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection9_Internalname, 1, 0, "px", 0, "px", "menu-acoes", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection13_Internalname, 1, 0, "px", 0, "px", "acoes", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblControhelp_Internalname, "<i class=\"fa fa-question-circle\" aria-hidden=\"true\"></i>", "", "", lblControhelp_Jsonclick, "'"+""+"'"+",true,"+"'"+"EHELPSISTEMA_MPAGE."+"'", "", "TextBlock", 5, "", 1, 1, 2, "HLP_MasterPageMeetrika.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "acoes", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection17_Internalname, 1, 0, "px", 0, "px", "acoes", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "<i class=\"fa fa-plus\" aria-hidden=\"true\"></i>", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",true,"+"'"+"e11mo1_client"+"'", "", "TextBlock", 7, "", 1, 1, 2, "HLP_MasterPageMeetrika.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection12_Internalname, 1, 0, "px", 0, "px", "acoes", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divNotificacoes_Internalname, divNotificacoes_Visible, 0, "px", 0, "px", "", "left", "top", "", "", "div");
            wb_table2_34_MO2( true) ;
         }
         else
         {
            wb_table2_34_MO2( false) ;
         }
         return  ;
      }

      protected void wb_table2_34_MO2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTxbnotificacao_Internalname, "<i class=\"fa fa-bell-o\" aria-hidden=\"true\"></i>", "", "", lblTxbnotificacao_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "", "bt-notificacoes", 0, "", 1, 1, 1, "HLP_MasterPageMeetrika.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection11_Internalname, 1, 0, "px", 0, "px", "acoes", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTxbusuario_Internalname, "<i class=\"fa fa-user\"></i>", "", "", lblTxbusuario_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "", "bt-usuario", 0, "", 1, 1, 1, "HLP_MasterPageMeetrika.htm");
            /* Div Control */
            GxWebStd.gx_div_start( context, divUsuario_Internalname, 1, 0, "px", 0, "px", "", "left", "top", "", "", "div");
            wb_table3_50_MO2( true) ;
         }
         else
         {
            wb_table3_50_MO2( false) ;
         }
         return  ;
      }

      protected void wb_table3_50_MO2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection10_Internalname, 1, 0, "px", 0, "px", "acoes", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "<i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i>", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",true,"+"'"+"ELOG_OUT_MPAGE."+"'", "", "bt-sair", 5, "", 1, 1, 1, "HLP_MasterPageMeetrika.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection14_Internalname, 1, 0, "px", 0, "px", "container-fluid", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection23_Internalname, 1, 0, "px", 0, "px", "row sub-faixa", "left", "top", "", "", "div");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DROPDOWNTABSMENU1_MPAGEContainer"+"\"></div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection15_Internalname, 1, 0, "px", 0, "px", "container-fluid", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection19_Internalname, 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection20_Internalname, 1, 0, "px", 0, "px", "col-md-12", "left", "top", "", "", "div");
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "MPW0096"+"", StringUtil.RTrim( WebComp_Wcrecentlinks_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpMPW0096"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Wcrecentlinks_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWcrecentlinks), StringUtil.Lower( WebComp_Wcrecentlinks_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpMPW0096"+"");
                  }
                  WebComp_Wcrecentlinks.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWcrecentlinks), StringUtil.Lower( WebComp_Wcrecentlinks_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "container-fluid", "left", "top", "", "", "div");
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            /* Content placeholder */
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-content-placeholder");
            context.WriteHtmlText( ">") ;
            if ( ! isFullAjaxMode( ) )
            {
               getDataAreaObject().RenderHtmlContent();
            }
            context.WriteHtmlText( "</div>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "container-fluid", "left", "top", "", "", "div");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"SCREENDETECTOR_MPAGEContainer"+"\"></div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "container-fluid", "left", "top", "", "", "div");
            context.WriteHtmlText( "<p>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONFIRMPANEL_MPAGEContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"CONFIRMPANEL_MPAGEContainer"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</p>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',true,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavHelpsistema_objeto_Internalname, AV69HelpSistema_Objeto, StringUtil.RTrim( context.localUtil.Format( AV69HelpSistema_Objeto, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "", "", "", "", edtavHelpsistema_objeto_Jsonclick, 0, "Attribute", "", "", "", edtavHelpsistema_objeto_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_MasterPageMeetrika.htm");
         }
         wbLoad = true;
      }

      protected void STARTMO2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPMO0( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( getDataAreaObject().ExecuteStartEvent() != 0 )
            {
               setAjaxCallMode();
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      protected void WSMO2( )
      {
         STARTMO2( ) ;
         EVTMO2( ) ;
      }

      protected void EVTMO2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "HELPSISTEMAF1_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12MO2 */
                           E12MO2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SCREENDETECTOR_MPAGE.SCREENDETECTED_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13MO2 */
                           E13MO2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14MO2 */
                           E14MO2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "HELPSISTEMA_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15MO2 */
                           E15MO2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOG_OUT_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16MO2 */
                           E16MO2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VAREATRABALHO_CODIGO_MPAGE.CLICK_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17MO2 */
                           E17MO2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18MO2 */
                           E18MO2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ATUALIZARWARNINGS_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19MO2 */
                           E19MO2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VSIMULARUSUARIO_MPAGE.CLICK_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20MO2 */
                           E20MO2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VQTDERRO_MPAGE.CLICK_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21MO2 */
                           E21MO2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VQTDLRNJ_MPAGE.CLICK_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22MO2 */
                           E22MO2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VQTDPEND_MPAGE.CLICK_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23MO2 */
                           E23MO2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VQTDSOLICITADAS_MPAGE.CLICK_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24MO2 */
                           E24MO2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNEWUSUARIO_CODIGO_MPAGE.CLICK_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25MO2 */
                           E25MO2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E26MO2 */
                           E26MO2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                              }
                              dynload_actions( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  else if ( StringUtil.StrCmp(sEvtType, "M") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-2));
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-6));
                     nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                     if ( nCmpId == 96 )
                     {
                        OldWcrecentlinks = cgiGet( "MPW0096");
                        if ( ( StringUtil.Len( OldWcrecentlinks) == 0 ) || ( StringUtil.StrCmp(OldWcrecentlinks, WebComp_Wcrecentlinks_Component) != 0 ) )
                        {
                           WebComp_Wcrecentlinks = getWebComponent(GetType(), "GeneXus.Programs", OldWcrecentlinks, new Object[] {context} );
                           WebComp_Wcrecentlinks.ComponentInit();
                           WebComp_Wcrecentlinks.Name = "OldWcrecentlinks";
                           WebComp_Wcrecentlinks_Component = OldWcrecentlinks;
                        }
                        if ( StringUtil.Len( WebComp_Wcrecentlinks_Component) != 0 )
                        {
                           WebComp_Wcrecentlinks.componentprocess("MPW0096", "", sEvt);
                        }
                        WebComp_Wcrecentlinks_Component = OldWcrecentlinks;
                     }
                  }
                  if ( context.wbHandled == 0 )
                  {
                     getDataAreaObject().DispatchEvents();
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEMO2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormMO2( ) ;
            }
         }
      }

      protected void PAMO2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavNewusuario_codigo.Name = "vNEWUSUARIO_CODIGO_MPAGE";
            cmbavNewusuario_codigo.WebTags = "";
            cmbavNewusuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "<Selecione um usu�rio>", 0);
            if ( cmbavNewusuario_codigo.ItemCount > 0 )
            {
               AV27NewUsuario_Codigo = (int)(NumberUtil.Val( cmbavNewusuario_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27NewUsuario_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV27NewUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27NewUsuario_Codigo), 6, 0)));
            }
            cmbavAreatrabalho_codigo.Name = "vAREATRABALHO_CODIGO_MPAGE";
            cmbavAreatrabalho_codigo.WebTags = "";
            if ( cmbavAreatrabalho_codigo.ItemCount > 0 )
            {
               AV7AreaTrabalho_Codigo = (int)(NumberUtil.Val( cmbavAreatrabalho_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV7AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavQtderro_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavNewusuario_codigo.ItemCount > 0 )
         {
            AV27NewUsuario_Codigo = (int)(NumberUtil.Val( cmbavNewusuario_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27NewUsuario_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV27NewUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27NewUsuario_Codigo), 6, 0)));
         }
         if ( cmbavAreatrabalho_codigo.ItemCount > 0 )
         {
            AV7AreaTrabalho_Codigo = (int)(NumberUtil.Val( cmbavAreatrabalho_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV7AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFMO2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavQtderro_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavQtderro_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtderro_Enabled), 5, 0)));
         edtavQtdlrnj_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavQtdlrnj_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdlrnj_Enabled), 5, 0)));
         edtavQtdpend_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavQtdpend_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdpend_Enabled), 5, 0)));
         edtavQtdsolicitadas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavQtdsolicitadas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdsolicitadas_Enabled), 5, 0)));
      }

      protected void RFMO2( )
      {
         initialize_formulas( ) ;
         if ( ShowMPWhenPopUp( ) || ! context.isPopUpObject( ) )
         {
            /* Execute user event: E18MO2 */
            E18MO2 ();
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
            {
               if ( 1 != 0 )
               {
                  if ( StringUtil.Len( WebComp_Wcrecentlinks_Component) != 0 )
                  {
                     WebComp_Wcrecentlinks.componentstart();
                  }
               }
            }
            fix_multi_value_controls( ) ;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E26MO2 */
            E26MO2 ();
            WBMO0( ) ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
      }

      protected void STRUPMO0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavQtderro_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavQtderro_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtderro_Enabled), 5, 0)));
         edtavQtdlrnj_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavQtdlrnj_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdlrnj_Enabled), 5, 0)));
         edtavQtdpend_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavQtdpend_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdpend_Enabled), 5, 0)));
         edtavQtdsolicitadas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavQtdsolicitadas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdsolicitadas_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E14MO2 */
         E14MO2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vTABSMENUDATA_MPAGE"), AV40TabsMenuData);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQtderro_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtderro_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDERRO_MPAGE");
               GX_FocusControl = edtavQtderro_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32QtdErro = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV32QtdErro", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32QtdErro), 4, 0)));
            }
            else
            {
               AV32QtdErro = (short)(context.localUtil.CToN( cgiGet( edtavQtderro_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV32QtdErro", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32QtdErro), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdlrnj_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdlrnj_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDLRNJ_MPAGE");
               GX_FocusControl = edtavQtdlrnj_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33QtdLrnj = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV33QtdLrnj", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33QtdLrnj), 4, 0)));
            }
            else
            {
               AV33QtdLrnj = (short)(context.localUtil.CToN( cgiGet( edtavQtdlrnj_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV33QtdLrnj", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33QtdLrnj), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdpend_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdpend_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDPEND_MPAGE");
               GX_FocusControl = edtavQtdpend_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34QtdPend = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV34QtdPend", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34QtdPend), 4, 0)));
            }
            else
            {
               AV34QtdPend = (short)(context.localUtil.CToN( cgiGet( edtavQtdpend_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV34QtdPend", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34QtdPend), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdsolicitadas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdsolicitadas_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDSOLICITADAS_MPAGE");
               GX_FocusControl = edtavQtdsolicitadas_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35QtdSolicitadas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV35QtdSolicitadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35QtdSolicitadas), 4, 0)));
            }
            else
            {
               AV35QtdSolicitadas = (short)(context.localUtil.CToN( cgiGet( edtavQtdsolicitadas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV35QtdSolicitadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35QtdSolicitadas), 4, 0)));
            }
            cmbavNewusuario_codigo.CurrentValue = cgiGet( cmbavNewusuario_codigo_Internalname);
            AV27NewUsuario_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavNewusuario_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV27NewUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27NewUsuario_Codigo), 6, 0)));
            cmbavAreatrabalho_codigo.CurrentValue = cgiGet( cmbavAreatrabalho_codigo_Internalname);
            AV7AreaTrabalho_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavAreatrabalho_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV7AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0)));
            AV55SimularUsuario = cgiGet( imgavSimularusuario_Internalname);
            AV69HelpSistema_Objeto = cgiGet( edtavHelpsistema_objeto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV69HelpSistema_Objeto", AV69HelpSistema_Objeto);
            /* Read saved values. */
            Dropdowntabsmenu1_Height = cgiGet( "DROPDOWNTABSMENU1_MPAGE_Height");
            Dropdowntabsmenu1_Menustyle = cgiGet( "DROPDOWNTABSMENU1_MPAGE_Menustyle");
            Confirmpanel_Width = cgiGet( "CONFIRMPANEL_MPAGE_Width");
            Confirmpanel_Title = cgiGet( "CONFIRMPANEL_MPAGE_Title");
            Confirmpanel_Confirmationtext = cgiGet( "CONFIRMPANEL_MPAGE_Confirmationtext");
            Confirmpanel_Yesbuttoncaption = cgiGet( "CONFIRMPANEL_MPAGE_Yesbuttoncaption");
            Confirmpanel_Nobuttoncaption = cgiGet( "CONFIRMPANEL_MPAGE_Nobuttoncaption");
            Confirmpanel_Cancelbuttoncaption = cgiGet( "CONFIRMPANEL_MPAGE_Cancelbuttoncaption");
            Confirmpanel_Yesbuttonposition = cgiGet( "CONFIRMPANEL_MPAGE_Yesbuttonposition");
            Confirmpanel_Confirmtype = cgiGet( "CONFIRMPANEL_MPAGE_Confirmtype");
            Confirmpanel_Texttype = cgiGet( "CONFIRMPANEL_MPAGE_Texttype");
            (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Caption = cgiGet( "FORM_MPAGE_Caption");
            Screendetector_Screenwidth = cgiGet( "SCREENDETECTOR_MPAGE_Screenwidth");
            Screendetector_Screenheight = cgiGet( "SCREENDETECTOR_MPAGE_Screenheight");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E14MO2 */
         E14MO2 ();
         if (returnInSub) return;
      }

      protected void E14MO2( )
      {
         /* Start Routine */
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Meta.addItem("Versao", "1.1 - Data: 14/04/2020 15:07", 0) ;
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml = (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml+StringUtil.Format( "<link rel=\"SHORTCUT ICON\" href=\"%1\" /> ", context.convertURL( (String)(context.GetImagePath( "f4e7b3e0-b872-40d0-97e9-87e3199a4868", "", context.GetTheme( )))), "", "", "", "", "", "", "", "");
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml = (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml+"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">";
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml = (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml+"<link href=\"https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700italic,700,800,800italic\" rel=\"stylesheet\" type=\"text/css\">";
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml = (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml+"<link href=\"https://fonts.googleapis.com/css?family=Hammersmith+One\" rel=\"stylesheet\">";
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml = (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml+"<link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\" rel=\"stylesheet\">";
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml = (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml+"<link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/FontAwesome.otf\" rel=\"stylesheet\">";
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml = (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml+"<link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.eot\" rel=\"stylesheet\">";
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml = (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml+"<link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.svg\" rel=\"stylesheet\">";
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml = (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml+"<link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.ttf\" rel=\"stylesheet\">";
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml = (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml+"<link rel=\"stylesheet\" type=\"text/css\" href=\"extsrc/masterpage/css/style.css\">";
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml = (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml+"<link rel=\"stylesheet\" type=\"text/css\" href=\"extsrc/masterpage/css/menu.css\">";
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml = (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml+"<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>";
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml = (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Headerrawhtml+"<script src=\"extsrc/masterpage/js/main.js\"></script>";
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV51WWPContext) ;
         lblLblareatrabalho_Caption = AV51WWPContext.gxTpr_Areatrabalho_descricao;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblLblareatrabalho_Internalname, "Caption", lblLblareatrabalho_Caption);
         GXt_int1 = AV20LastUserAreaTrabalho;
         GXt_int2 = AV51WWPContext.gxTpr_Userid;
         new prc_getultimaarea(context ).execute( ref  GXt_int2, out  GXt_int1) ;
         AV51WWPContext.gxTpr_Userid = (short)(GXt_int2);
         AV20LastUserAreaTrabalho = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV20LastUserAreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20LastUserAreaTrabalho), 6, 0)));
         lblLblusuario_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblLblusuario_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblLblusuario_Visible), 5, 0)));
         cmbavNewusuario_codigo.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, cmbavNewusuario_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavNewusuario_codigo.Visible), 5, 0)));
         imgavSimularusuario_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, imgavSimularusuario_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavSimularusuario_Visible), 5, 0)));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         AV5ServerDate = DateTimeUtil.ServerDate( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV5ServerDate", context.localUtil.Format(AV5ServerDate, "99/99/99"));
         divNotificacoes_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, divNotificacoes_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divNotificacoes_Visible), 5, 0)));
         /* Execute user subroutine: 'INICIALIZAR' */
         S112 ();
         if (returnInSub) return;
         if ( new SdtGAMRepository(context).checkpermission("is_gam_administrator") )
         {
            cmbavNewusuario_codigo.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, cmbavNewusuario_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavNewusuario_codigo.Visible), 5, 0)));
            cmbavAreatrabalho_codigo.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, cmbavAreatrabalho_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAreatrabalho_codigo.Visible), 5, 0)));
            /* Using cursor H00MO2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               A57Usuario_PessoaCod = H00MO2_A57Usuario_PessoaCod[0];
               A54Usuario_Ativo = H00MO2_A54Usuario_Ativo[0];
               n54Usuario_Ativo = H00MO2_n54Usuario_Ativo[0];
               A1Usuario_Codigo = H00MO2_A1Usuario_Codigo[0];
               A58Usuario_PessoaNom = H00MO2_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H00MO2_n58Usuario_PessoaNom[0];
               A58Usuario_PessoaNom = H00MO2_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H00MO2_n58Usuario_PessoaNom[0];
               cmbavNewusuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)), A58Usuario_PessoaNom, 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            AV27NewUsuario_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV27NewUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27NewUsuario_Codigo), 6, 0)));
         }
         else
         {
            if ( AV51WWPContext.gxTpr_Userehgestor )
            {
               cmbavNewusuario_codigo.removeAllItems();
               cmbavNewusuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV51WWPContext.gxTpr_Userid), 6, 0)), StringUtil.Upper( AV51WWPContext.gxTpr_Username), 0);
               /* Using cursor H00MO3 */
               pr_default.execute(1, new Object[] {AV51WWPContext.gxTpr_Areatrabalho_codigo});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A29Contratante_Codigo = H00MO3_A29Contratante_Codigo[0];
                  n29Contratante_Codigo = H00MO3_n29Contratante_Codigo[0];
                  A1822Contratante_UsaOSistema = H00MO3_A1822Contratante_UsaOSistema[0];
                  n1822Contratante_UsaOSistema = H00MO3_n1822Contratante_UsaOSistema[0];
                  A5AreaTrabalho_Codigo = H00MO3_A5AreaTrabalho_Codigo[0];
                  A1822Contratante_UsaOSistema = H00MO3_A1822Contratante_UsaOSistema[0];
                  n1822Contratante_UsaOSistema = H00MO3_n1822Contratante_UsaOSistema[0];
                  /* Using cursor H00MO4 */
                  pr_default.execute(2, new Object[] {AV51WWPContext.gxTpr_Contratada_codigo});
                  while ( (pr_default.getStatus(2) != 101) )
                  {
                     A1078ContratoGestor_ContratoCod = H00MO4_A1078ContratoGestor_ContratoCod[0];
                     A1080ContratoGestor_UsuarioPesCod = H00MO4_A1080ContratoGestor_UsuarioPesCod[0];
                     n1080ContratoGestor_UsuarioPesCod = H00MO4_n1080ContratoGestor_UsuarioPesCod[0];
                     A2033ContratoGestor_UsuarioAtv = H00MO4_A2033ContratoGestor_UsuarioAtv[0];
                     n2033ContratoGestor_UsuarioAtv = H00MO4_n2033ContratoGestor_UsuarioAtv[0];
                     A1136ContratoGestor_ContratadaCod = H00MO4_A1136ContratoGestor_ContratadaCod[0];
                     n1136ContratoGestor_ContratadaCod = H00MO4_n1136ContratoGestor_ContratadaCod[0];
                     A1081ContratoGestor_UsuarioPesNom = H00MO4_A1081ContratoGestor_UsuarioPesNom[0];
                     n1081ContratoGestor_UsuarioPesNom = H00MO4_n1081ContratoGestor_UsuarioPesNom[0];
                     A1079ContratoGestor_UsuarioCod = H00MO4_A1079ContratoGestor_UsuarioCod[0];
                     A1446ContratoGestor_ContratadaAreaCod = H00MO4_A1446ContratoGestor_ContratadaAreaCod[0];
                     n1446ContratoGestor_ContratadaAreaCod = H00MO4_n1446ContratoGestor_ContratadaAreaCod[0];
                     A1136ContratoGestor_ContratadaCod = H00MO4_A1136ContratoGestor_ContratadaCod[0];
                     n1136ContratoGestor_ContratadaCod = H00MO4_n1136ContratoGestor_ContratadaCod[0];
                     A1446ContratoGestor_ContratadaAreaCod = H00MO4_A1446ContratoGestor_ContratadaAreaCod[0];
                     n1446ContratoGestor_ContratadaAreaCod = H00MO4_n1446ContratoGestor_ContratadaAreaCod[0];
                     A1080ContratoGestor_UsuarioPesCod = H00MO4_A1080ContratoGestor_UsuarioPesCod[0];
                     n1080ContratoGestor_UsuarioPesCod = H00MO4_n1080ContratoGestor_UsuarioPesCod[0];
                     A2033ContratoGestor_UsuarioAtv = H00MO4_A2033ContratoGestor_UsuarioAtv[0];
                     n2033ContratoGestor_UsuarioAtv = H00MO4_n2033ContratoGestor_UsuarioAtv[0];
                     A1081ContratoGestor_UsuarioPesNom = H00MO4_A1081ContratoGestor_UsuarioPesNom[0];
                     n1081ContratoGestor_UsuarioPesNom = H00MO4_n1081ContratoGestor_UsuarioPesNom[0];
                     GXt_boolean3 = A1135ContratoGestor_UsuarioEhContratante;
                     new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean3) ;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", true, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", true, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                     A1135ContratoGestor_UsuarioEhContratante = GXt_boolean3;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", true, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
                     if ( A1135ContratoGestor_UsuarioEhContratante )
                     {
                        cmbavNewusuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)), A1081ContratoGestor_UsuarioPesNom+" (Contratante)", 0);
                     }
                     pr_default.readNext(2);
                  }
                  pr_default.close(2);
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(1);
            }
            AV55SimularUsuario = context.GetImagePath( "c81edb6e-07c9-41ce-a90e-a70f1f7f1eba", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, imgavSimularusuario_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV55SimularUsuario)) ? AV79Simularusuario_GXI : context.convertURL( context.PathToRelativeUrl( AV55SimularUsuario))));
            AV79Simularusuario_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "c81edb6e-07c9-41ce-a90e-a70f1f7f1eba", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, imgavSimularusuario_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV55SimularUsuario)) ? AV79Simularusuario_GXI : context.convertURL( context.PathToRelativeUrl( AV55SimularUsuario))));
            imgavSimularusuario_Tooltiptext = "Trocar de Senha";
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, imgavSimularusuario_Internalname, "Tooltiptext", imgavSimularusuario_Tooltiptext);
            imgavSimularusuario_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, imgavSimularusuario_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavSimularusuario_Visible), 5, 0)));
         }
         if ( cmbavNewusuario_codigo.ItemCount > 1 )
         {
            lblLblusuario_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblLblusuario_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblLblusuario_Visible), 5, 0)));
            lblLblusuario_Caption = "Trocar para Usu�rio:";
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblLblusuario_Internalname, "Caption", lblLblusuario_Caption);
            cmbavNewusuario_codigo.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, cmbavNewusuario_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavNewusuario_codigo.Visible), 5, 0)));
         }
         edtavHelpsistema_objeto_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavHelpsistema_objeto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavHelpsistema_objeto_Visible), 5, 0)));
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Meta.addItem("Log_UserGam", StringUtil.BoolToStr( AV51WWPContext.gxTpr_Userehadministradorgam), 0) ;
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Meta.addItem("Log_EhContratada", StringUtil.BoolToStr( AV51WWPContext.gxTpr_Userehcontratada), 0) ;
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Meta.addItem("Log_EhContratante", StringUtil.BoolToStr( AV51WWPContext.gxTpr_Userehcontratante), 0) ;
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Meta.addItem("Log_EhGestor", StringUtil.BoolToStr( AV51WWPContext.gxTpr_Userehgestor), 0) ;
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Meta.addItem("Log_Area", context.localUtil.Format( (decimal)(AV51WWPContext.gxTpr_Areatrabalho_codigo), "ZZZZZ9"), 0) ;
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Meta.addItem("Log_Contratada", context.localUtil.Format( (decimal)(AV51WWPContext.gxTpr_Contratada_codigo), "ZZZZZ9"), 0) ;
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Meta.addItem("Log_Contratante", context.localUtil.Format( (decimal)(AV51WWPContext.gxTpr_Contratante_codigo), "ZZZZZ9"), 0) ;
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Meta.addItem("Log_User", context.localUtil.Format( (decimal)(AV51WWPContext.gxTpr_Userid), "ZZZ9"), 0) ;
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Meta.addItem("Log_Insert", StringUtil.BoolToStr( AV51WWPContext.gxTpr_Insert), 0) ;
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Meta.addItem("Log_Update", StringUtil.BoolToStr( AV51WWPContext.gxTpr_Update), 0) ;
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Meta.addItem("Log_Delete", StringUtil.BoolToStr( AV51WWPContext.gxTpr_Delete), 0) ;
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Meta.addItem("Log_Display", StringUtil.BoolToStr( AV51WWPContext.gxTpr_Display), 0) ;
      }

      protected void E15MO2( )
      {
         /* 'HelpSistema' Routine */
         /* Execute user subroutine: 'EXIBE.HELP' */
         S122 ();
         if (returnInSub) return;
      }

      protected void E12MO2( )
      {
         /* 'HelpSistemaF1' Routine */
         /* Execute user subroutine: 'EXIBE.HELP' */
         S122 ();
         if (returnInSub) return;
      }

      protected void S122( )
      {
         /* 'EXIBE.HELP' Routine */
         AV80GXLvl102 = 0;
         /* Using cursor H00MO5 */
         pr_default.execute(3, new Object[] {AV69HelpSistema_Objeto});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A1639HelpSistema_Objeto = H00MO5_A1639HelpSistema_Objeto[0];
            AV80GXLvl102 = 1;
            context.PopUp(formatLink("wp_sistemahelpvs2.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV69HelpSistema_Objeto)), new Object[] {});
            pr_default.readNext(3);
         }
         pr_default.close(3);
         if ( AV80GXLvl102 == 0 )
         {
            Confirmpanel_Title = "Aten��o!";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", true, Confirmpanel_Internalname, "Title", Confirmpanel_Title);
            Confirmpanel_Confirmationtext = "Nenhum help encontrado.";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", true, Confirmpanel_Internalname, "ConfirmationText", Confirmpanel_Confirmationtext);
            Confirmpanel_Yesbuttoncaption = "Fechar";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", true, Confirmpanel_Internalname, "YesButtonCaption", Confirmpanel_Yesbuttoncaption);
            Confirmpanel_Confirmtype = "YES";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", true, Confirmpanel_Internalname, "ConfirmType", Confirmpanel_Confirmtype);
            this.executeUsercontrolMethod("", true, "CONFIRMPANEL_MPAGEContainer", "Confirm", "", new Object[] {});
         }
      }

      protected void E16MO2( )
      {
         /* 'Log_Out' Routine */
         GXt_int2 = AV51WWPContext.gxTpr_Userid;
         new prc_setultimaarea(context ).execute( ref  GXt_int2,  AV7AreaTrabalho_Codigo) ;
         AV51WWPContext.gxTpr_Userid = (short)(GXt_int2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV7AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0)));
         AV29Ok = (short)(Convert.ToInt16(new SdtGAMRepository(context).logout(out  AV15Errors)));
         AV39Session.Destroy();
         context.wjLoc = formatLink("login.aspx") ;
         context.wjLocDisableFrm = 1;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV51WWPContext", AV51WWPContext);
      }

      protected void E17MO2( )
      {
         /* Areatrabalho_codigo_Click Routine */
         AV51WWPContext.gxTpr_Areatrabalho_codigo = AV7AreaTrabalho_Codigo;
         AV51WWPContext.gxTpr_Updcomboareatrabalho = true;
         /* Execute user subroutine: 'DADOSDAAREA' */
         S132 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'CONTRATANTEOUCONTRATADA' */
         S142 ();
         if (returnInSub) return;
         new wwpbaseobjects.setwwpcontext(context ).execute(  AV51WWPContext) ;
         AV49WebSession.Remove("Tabela_Codigo");
         AV49WebSession.Remove("FuncaoDados_Codigo");
         AV49WebSession.Remove("APFTabela_Codigo");
         AV49WebSession.Remove("SDTDemandas");
         GXt_int2 = AV51WWPContext.gxTpr_Userid;
         new prc_setultimaarea(context ).execute( ref  GXt_int2,  AV7AreaTrabalho_Codigo) ;
         AV51WWPContext.gxTpr_Userid = (short)(GXt_int2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV7AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0)));
         lblTbjava_Caption = "<script language=\"text/javascript\"> location.reload(true); </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV51WWPContext", AV51WWPContext);
      }

      protected void E18MO2( )
      {
         /* Refresh Routine */
         lblLblareatrabalho_Caption = AV51WWPContext.gxTpr_Areatrabalho_descricao;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblLblareatrabalho_Internalname, "Caption", lblLblareatrabalho_Caption);
         /* Using cursor H00MO6 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            A330ParametrosSistema_Codigo = H00MO6_A330ParametrosSistema_Codigo[0];
            A1952ParametrosSistema_URLOtherVer = H00MO6_A1952ParametrosSistema_URLOtherVer[0];
            n1952ParametrosSistema_URLOtherVer = H00MO6_n1952ParametrosSistema_URLOtherVer[0];
            A1954ParametrosSistema_Validacao = H00MO6_A1954ParametrosSistema_Validacao[0];
            n1954ParametrosSistema_Validacao = H00MO6_n1954ParametrosSistema_Validacao[0];
            AV63ParametrosSistema_URLOtherVer = A1952ParametrosSistema_URLOtherVer;
            AV64ParametrosSistema_Validacao = A1954ParametrosSistema_Validacao;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(4);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64ParametrosSistema_Validacao)) )
         {
            AV51WWPContext.gxTpr_Validationkey = "01010101010101010101010101010101010101010101010101010101010101010101010101";
         }
         else
         {
            AV51WWPContext.gxTpr_Validationkey = Crypto.Decrypt64( AV64ParametrosSistema_Validacao, "81982271FB56A23C045375B330BFF732");
         }
         lblTxbusernamer_Caption = AV51WWPContext.gxTpr_Username;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblTxbusernamer_Internalname, "Caption", lblTxbusernamer_Caption);
         if ( (0==AV27NewUsuario_Codigo) )
         {
            /* Object Property */
            if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Wcrecentlinks_Component), StringUtil.Lower( "RecentLinksNew")) != 0 )
            {
               WebComp_Wcrecentlinks = getWebComponent(GetType(), "GeneXus.Programs", "recentlinksnew", new Object[] {context} );
               WebComp_Wcrecentlinks.ComponentInit();
               WebComp_Wcrecentlinks.Name = "RecentLinksNew";
               WebComp_Wcrecentlinks_Component = "RecentLinksNew";
            }
            if ( StringUtil.Len( WebComp_Wcrecentlinks_Component) != 0 )
            {
               WebComp_Wcrecentlinks.setjustcreated();
               WebComp_Wcrecentlinks.componentprepare(new Object[] {(String)"MPW0096",(String)"",(getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Caption,Contentholder.Pgmname});
               WebComp_Wcrecentlinks.componentbind(new Object[] {(String)"",(String)""});
            }
            if ( isFullAjaxMode( ) )
            {
               context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpMPW0096"+"");
               WebComp_Wcrecentlinks.componentdraw();
               context.httpAjaxContext.ajax_rspEndCmp();
            }
         }
         /* Execute user subroutine: 'ATUALIZARWARNINGS' */
         S152 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'INICIALIZAR' */
         S112 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'DESATIVAINTERVALOS' */
         S162 ();
         if (returnInSub) return;
         if ( AV51WWPContext.gxTpr_Updcomboareatrabalho )
         {
            AV51WWPContext.gxTpr_Updcomboareatrabalho = false;
            new wwpbaseobjects.setwwpcontext(context ).execute(  AV51WWPContext) ;
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Request.QueryString)) )
            {
               /* Execute user subroutine: 'TELAINICIAL' */
               S172 ();
               if (returnInSub) return;
            }
         }
         else
         {
            AV42URL = Contentholder.Pgmname;
            AV69HelpSistema_Objeto = StringUtil.Format( "%1.aspx", StringUtil.Trim( AV42URL), "", "", "", "", "", "", "", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV69HelpSistema_Objeto", AV69HelpSistema_Objeto);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV51WWPContext", AV51WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV47Vermelhas", AV47Vermelhas);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV52Laranjas", AV52Laranjas);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV53Amarelas", AV53Amarelas);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV54Verdes", AV54Verdes);
         cmbavAreatrabalho_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, cmbavAreatrabalho_codigo_Internalname, "Values", cmbavAreatrabalho_codigo.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV9Contratadas", AV9Contratadas);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV40TabsMenuData", AV40TabsMenuData);
         cmbavNewusuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27NewUsuario_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, cmbavNewusuario_codigo_Internalname, "Values", cmbavNewusuario_codigo.ToJavascriptSource());
      }

      protected void E19MO2( )
      {
         /* 'AtualizarWarnings' Routine */
         /* Execute user subroutine: 'ATUALIZARWARNINGS' */
         S152 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV47Vermelhas", AV47Vermelhas);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV52Laranjas", AV52Laranjas);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV53Amarelas", AV53Amarelas);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV54Verdes", AV54Verdes);
      }

      protected void S172( )
      {
         /* 'TELAINICIAL' Routine */
         if ( AV51WWPContext.gxTpr_Userehcontratante )
         {
            context.wjLoc = formatLink("wp_monitordmn.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         else
         {
            AV45Usuario_EhGestor = false;
            /* Using cursor H00MO7 */
            pr_default.execute(5, new Object[] {AV51WWPContext.gxTpr_Userid});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A1079ContratoGestor_UsuarioCod = H00MO7_A1079ContratoGestor_UsuarioCod[0];
               AV45Usuario_EhGestor = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(5);
            }
            pr_default.close(5);
            if ( AV45Usuario_EhGestor )
            {
               context.wjLoc = formatLink("wp_gestao.aspx") ;
               context.wjLocDisableFrm = 1;
            }
            else
            {
               context.wjLoc = formatLink("wp_monitordmn.aspx") ;
               context.wjLocDisableFrm = 1;
            }
         }
      }

      protected void S152( )
      {
         /* 'ATUALIZARWARNINGS' Routine */
         AV32QtdErro = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV32QtdErro", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32QtdErro), 4, 0)));
         AV34QtdPend = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV34QtdPend", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34QtdPend), 4, 0)));
         AV33QtdLrnj = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV33QtdLrnj", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33QtdLrnj), 4, 0)));
         AV35QtdSolicitadas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV35QtdSolicitadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35QtdSolicitadas), 4, 0)));
         AV47Vermelhas.Clear();
         AV52Laranjas.Clear();
         AV53Amarelas.Clear();
         AV54Verdes.Clear();
         pr_default.dynParam(6, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV9Contratadas ,
                                              A484ContagemResultado_StatusDmn ,
                                              A472ContagemResultado_DataEntrega ,
                                              AV5ServerDate ,
                                              AV51WWPContext.gxTpr_Contratada_codigo ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A456ContagemResultado_Codigo ,
                                              A602ContagemResultado_OSVinculada ,
                                              A499ContagemResultado_ContratadaPessoaCod ,
                                              AV51WWPContext.gxTpr_Contratada_pessoacod ,
                                              A890ContagemResultado_Responsavel ,
                                              AV51WWPContext.gxTpr_Userid },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         /* Using cursor H00MO9 */
         pr_default.execute(6, new Object[] {AV5ServerDate, AV51WWPContext.gxTpr_Userid});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A499ContagemResultado_ContratadaPessoaCod = H00MO9_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00MO9_n499ContagemResultado_ContratadaPessoaCod[0];
            A602ContagemResultado_OSVinculada = H00MO9_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00MO9_n602ContagemResultado_OSVinculada[0];
            A456ContagemResultado_Codigo = H00MO9_A456ContagemResultado_Codigo[0];
            A472ContagemResultado_DataEntrega = H00MO9_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = H00MO9_n472ContagemResultado_DataEntrega[0];
            A890ContagemResultado_Responsavel = H00MO9_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = H00MO9_n890ContagemResultado_Responsavel[0];
            A484ContagemResultado_StatusDmn = H00MO9_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00MO9_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = H00MO9_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00MO9_n490ContagemResultado_ContratadaCod[0];
            A531ContagemResultado_StatusUltCnt = H00MO9_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00MO9_n531ContagemResultado_StatusUltCnt[0];
            A531ContagemResultado_StatusUltCnt = H00MO9_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00MO9_n531ContagemResultado_StatusUltCnt[0];
            A499ContagemResultado_ContratadaPessoaCod = H00MO9_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00MO9_n499ContagemResultado_ContratadaPessoaCod[0];
            if ( ( AV51WWPContext.gxTpr_Contratada_codigo <= 0 ) || ( H00MO9_n531ContagemResultado_StatusUltCnt[0] || ( A531ContagemResultado_StatusUltCnt != 7 ) || ( ( A531ContagemResultado_StatusUltCnt == 7 ) && ( new prc_qtdtrtmdvrg(context).executeUdp(  A456ContagemResultado_Codigo,  A602ContagemResultado_OSVinculada,  A499ContagemResultado_ContratadaPessoaCod,  AV51WWPContext.gxTpr_Contratada_pessoacod) == 0 ) ) ) )
            {
               AV32QtdErro = (short)(AV32QtdErro+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV32QtdErro", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32QtdErro), 4, 0)));
               AV47Vermelhas.Add(A456ContagemResultado_Codigo, 0);
            }
            pr_default.readNext(6);
         }
         pr_default.close(6);
         pr_default.dynParam(7, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV9Contratadas ,
                                              A484ContagemResultado_StatusDmn ,
                                              A890ContagemResultado_Responsavel ,
                                              AV51WWPContext.gxTpr_Userid ,
                                              AV51WWPContext.gxTpr_Contratada_codigo ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A456ContagemResultado_Codigo ,
                                              A602ContagemResultado_OSVinculada ,
                                              A499ContagemResultado_ContratadaPessoaCod ,
                                              AV51WWPContext.gxTpr_Contratada_pessoacod ,
                                              A472ContagemResultado_DataEntrega ,
                                              AV5ServerDate },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE
                                              }
         });
         /* Using cursor H00MO11 */
         pr_default.execute(7, new Object[] {AV51WWPContext.gxTpr_Userid, AV5ServerDate});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A499ContagemResultado_ContratadaPessoaCod = H00MO11_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00MO11_n499ContagemResultado_ContratadaPessoaCod[0];
            A602ContagemResultado_OSVinculada = H00MO11_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00MO11_n602ContagemResultado_OSVinculada[0];
            A456ContagemResultado_Codigo = H00MO11_A456ContagemResultado_Codigo[0];
            A472ContagemResultado_DataEntrega = H00MO11_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = H00MO11_n472ContagemResultado_DataEntrega[0];
            A890ContagemResultado_Responsavel = H00MO11_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = H00MO11_n890ContagemResultado_Responsavel[0];
            A484ContagemResultado_StatusDmn = H00MO11_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00MO11_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = H00MO11_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00MO11_n490ContagemResultado_ContratadaCod[0];
            A531ContagemResultado_StatusUltCnt = H00MO11_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00MO11_n531ContagemResultado_StatusUltCnt[0];
            A531ContagemResultado_StatusUltCnt = H00MO11_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00MO11_n531ContagemResultado_StatusUltCnt[0];
            A499ContagemResultado_ContratadaPessoaCod = H00MO11_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00MO11_n499ContagemResultado_ContratadaPessoaCod[0];
            if ( ( AV51WWPContext.gxTpr_Contratada_codigo <= 0 ) || ( H00MO11_n531ContagemResultado_StatusUltCnt[0] || ( A531ContagemResultado_StatusUltCnt != 7 ) || ( ( A531ContagemResultado_StatusUltCnt == 7 ) && ( new prc_qtdtrtmdvrg(context).executeUdp(  A456ContagemResultado_Codigo,  A602ContagemResultado_OSVinculada,  A499ContagemResultado_ContratadaPessoaCod,  AV51WWPContext.gxTpr_Contratada_pessoacod) == 0 ) ) ) )
            {
               AV33QtdLrnj = (short)(AV33QtdLrnj+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV33QtdLrnj", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33QtdLrnj), 4, 0)));
               AV52Laranjas.Add(A456ContagemResultado_Codigo, 0);
            }
            pr_default.readNext(7);
         }
         pr_default.close(7);
         pr_default.dynParam(8, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV9Contratadas ,
                                              A484ContagemResultado_StatusDmn ,
                                              A472ContagemResultado_DataEntrega ,
                                              AV5ServerDate ,
                                              AV51WWPContext.gxTpr_Contratada_codigo ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A456ContagemResultado_Codigo ,
                                              A602ContagemResultado_OSVinculada ,
                                              A499ContagemResultado_ContratadaPessoaCod ,
                                              AV51WWPContext.gxTpr_Contratada_pessoacod ,
                                              A890ContagemResultado_Responsavel ,
                                              AV51WWPContext.gxTpr_Userid },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         /* Using cursor H00MO13 */
         pr_default.execute(8, new Object[] {AV5ServerDate, AV5ServerDate, AV51WWPContext.gxTpr_Userid});
         while ( (pr_default.getStatus(8) != 101) )
         {
            A499ContagemResultado_ContratadaPessoaCod = H00MO13_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00MO13_n499ContagemResultado_ContratadaPessoaCod[0];
            A602ContagemResultado_OSVinculada = H00MO13_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00MO13_n602ContagemResultado_OSVinculada[0];
            A456ContagemResultado_Codigo = H00MO13_A456ContagemResultado_Codigo[0];
            A472ContagemResultado_DataEntrega = H00MO13_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = H00MO13_n472ContagemResultado_DataEntrega[0];
            A890ContagemResultado_Responsavel = H00MO13_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = H00MO13_n890ContagemResultado_Responsavel[0];
            A484ContagemResultado_StatusDmn = H00MO13_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00MO13_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = H00MO13_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00MO13_n490ContagemResultado_ContratadaCod[0];
            A531ContagemResultado_StatusUltCnt = H00MO13_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00MO13_n531ContagemResultado_StatusUltCnt[0];
            A531ContagemResultado_StatusUltCnt = H00MO13_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00MO13_n531ContagemResultado_StatusUltCnt[0];
            A499ContagemResultado_ContratadaPessoaCod = H00MO13_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00MO13_n499ContagemResultado_ContratadaPessoaCod[0];
            if ( ( AV51WWPContext.gxTpr_Contratada_codigo <= 0 ) || ( H00MO13_n531ContagemResultado_StatusUltCnt[0] || ( A531ContagemResultado_StatusUltCnt != 7 ) || ( ( A531ContagemResultado_StatusUltCnt == 7 ) && ( new prc_qtdtrtmdvrg(context).executeUdp(  A456ContagemResultado_Codigo,  A602ContagemResultado_OSVinculada,  A499ContagemResultado_ContratadaPessoaCod,  AV51WWPContext.gxTpr_Contratada_pessoacod) == 0 ) ) ) )
            {
               AV34QtdPend = (short)(AV34QtdPend+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV34QtdPend", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34QtdPend), 4, 0)));
               AV53Amarelas.Add(A456ContagemResultado_Codigo, 0);
            }
            pr_default.readNext(8);
         }
         pr_default.close(8);
         pr_default.dynParam(9, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV9Contratadas ,
                                              A484ContagemResultado_StatusDmn ,
                                              A890ContagemResultado_Responsavel ,
                                              AV51WWPContext.gxTpr_Userid },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         /* Using cursor H00MO14 */
         pr_default.execute(9, new Object[] {AV51WWPContext.gxTpr_Userid});
         while ( (pr_default.getStatus(9) != 101) )
         {
            A890ContagemResultado_Responsavel = H00MO14_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = H00MO14_n890ContagemResultado_Responsavel[0];
            A484ContagemResultado_StatusDmn = H00MO14_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00MO14_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = H00MO14_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00MO14_n490ContagemResultado_ContratadaCod[0];
            A456ContagemResultado_Codigo = H00MO14_A456ContagemResultado_Codigo[0];
            AV35QtdSolicitadas = (short)(AV35QtdSolicitadas+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV35QtdSolicitadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35QtdSolicitadas), 4, 0)));
            AV54Verdes.Add(A456ContagemResultado_Codigo, 0);
            pr_default.readNext(9);
         }
         pr_default.close(9);
         edtavQtderro_Tooltiptext = "Prazo vencido ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV32QtdErro), 4, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavQtderro_Internalname, "Tooltiptext", edtavQtderro_Tooltiptext);
         edtavQtdpend_Tooltiptext = "Prazo vencendo em 7 dias ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV34QtdPend), 4, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavQtdpend_Internalname, "Tooltiptext", edtavQtdpend_Tooltiptext);
         edtavQtdlrnj_Tooltiptext = "Prazo vencendo hoje ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV33QtdLrnj), 4, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavQtdlrnj_Internalname, "Tooltiptext", edtavQtdlrnj_Tooltiptext);
         edtavQtdsolicitadas_Tooltiptext = "Solicitadas ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV35QtdSolicitadas), 4, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, edtavQtdsolicitadas_Internalname, "Tooltiptext", edtavQtdsolicitadas_Tooltiptext);
      }

      protected void E20MO2( )
      {
         /* Simularusuario_Click Routine */
         context.wjLoc = formatLink("gamexamplechangeyourpassword.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void E21MO2( )
      {
         /* Qtderro_Click Routine */
         if ( AV32QtdErro > 0 )
         {
            AV49WebSession.Set("Codigos", AV47Vermelhas.ToXml(false, true, "Collection", ""));
         }
         AV38SDT_FiltroConsContadorFM.gxTpr_Nome = "#Demandas vencidas";
         AV49WebSession.Set("FiltroConsultaContador", AV38SDT_FiltroConsContadorFM.ToXml(false, true, "SDT_FiltroConsContadorFM", "GxEv3Up14_MeetrikaVs3"));
         context.wjLoc = formatLink("wwcontagemresultado.aspx") ;
         context.wjLocDisableFrm = 1;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV38SDT_FiltroConsContadorFM", AV38SDT_FiltroConsContadorFM);
      }

      protected void E22MO2( )
      {
         /* Qtdlrnj_Click Routine */
         if ( AV33QtdLrnj > 0 )
         {
            AV49WebSession.Set("Codigos", AV52Laranjas.ToXml(false, true, "Collection", ""));
         }
         AV38SDT_FiltroConsContadorFM.gxTpr_Nome = "#Demandas vencendo hoje";
         AV49WebSession.Set("FiltroConsultaContador", AV38SDT_FiltroConsContadorFM.ToXml(false, true, "SDT_FiltroConsContadorFM", "GxEv3Up14_MeetrikaVs3"));
         context.wjLoc = formatLink("wwcontagemresultado.aspx") ;
         context.wjLocDisableFrm = 1;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV38SDT_FiltroConsContadorFM", AV38SDT_FiltroConsContadorFM);
      }

      protected void E23MO2( )
      {
         /* Qtdpend_Click Routine */
         if ( AV34QtdPend > 0 )
         {
            AV49WebSession.Set("Codigos", AV53Amarelas.ToXml(false, true, "Collection", ""));
         }
         AV38SDT_FiltroConsContadorFM.gxTpr_Nome = "#Demandas a vencer na semana";
         AV49WebSession.Set("FiltroConsultaContador", AV38SDT_FiltroConsContadorFM.ToXml(false, true, "SDT_FiltroConsContadorFM", "GxEv3Up14_MeetrikaVs3"));
         context.wjLoc = formatLink("wwcontagemresultado.aspx") ;
         context.wjLocDisableFrm = 1;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV38SDT_FiltroConsContadorFM", AV38SDT_FiltroConsContadorFM);
      }

      protected void E24MO2( )
      {
         /* Qtdsolicitadas_Click Routine */
         if ( AV35QtdSolicitadas > 0 )
         {
            AV49WebSession.Set("Codigos", AV54Verdes.ToXml(false, true, "Collection", ""));
         }
         AV38SDT_FiltroConsContadorFM.gxTpr_Nome = "#Demandas solicitadas";
         AV49WebSession.Set("FiltroConsultaContador", AV38SDT_FiltroConsContadorFM.ToXml(false, true, "SDT_FiltroConsContadorFM", "GxEv3Up14_MeetrikaVs3"));
         context.wjLoc = formatLink("wwcontagemresultado.aspx") ;
         context.wjLocDisableFrm = 1;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV38SDT_FiltroConsContadorFM", AV38SDT_FiltroConsContadorFM);
      }

      protected void E25MO2( )
      {
         /* Newusuario_codigo_Click Routine */
         AV44Usuario_Codigo = AV27NewUsuario_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV44Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Usuario_Codigo), 6, 0)));
         AV51WWPContext.gxTpr_Userid = (short)(AV44Usuario_Codigo);
         /* Using cursor H00MO15 */
         pr_default.execute(10, new Object[] {AV44Usuario_Codigo});
         while ( (pr_default.getStatus(10) != 101) )
         {
            A289Usuario_EhContador = H00MO15_A289Usuario_EhContador[0];
            A293Usuario_EhFinanceiro = H00MO15_A293Usuario_EhFinanceiro[0];
            A341Usuario_UserGamGuid = H00MO15_A341Usuario_UserGamGuid[0];
            A2016Usuario_UltimaArea = H00MO15_A2016Usuario_UltimaArea[0];
            n2016Usuario_UltimaArea = H00MO15_n2016Usuario_UltimaArea[0];
            A1Usuario_Codigo = H00MO15_A1Usuario_Codigo[0];
            GXt_boolean3 = A290Usuario_EhAuditorFM;
            new prc_usuarioehauditorfm(context ).execute(  A1Usuario_Codigo, out  GXt_boolean3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            A290Usuario_EhAuditorFM = GXt_boolean3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "A290Usuario_EhAuditorFM", A290Usuario_EhAuditorFM);
            AV51WWPContext.gxTpr_Userehauditorfm = A290Usuario_EhAuditorFM;
            AV51WWPContext.gxTpr_Userehcontador = A289Usuario_EhContador;
            AV51WWPContext.gxTpr_Userehfinanceiro = A293Usuario_EhFinanceiro;
            AV46Usuario_UserGamGuid = A341Usuario_UserGamGuid;
            AV20LastUserAreaTrabalho = A2016Usuario_UltimaArea;
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV20LastUserAreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20LastUserAreaTrabalho), 6, 0)));
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(10);
         AV17GAMUser.load( AV46Usuario_UserGamGuid);
         AV51WWPContext.gxTpr_Userehadministradorgam = (bool)(((StringUtil.StrCmp(cmbavNewusuario_codigo.Description, "ADMINISTRADOR")==0)));
         AV51WWPContext.gxTpr_Contratada_codigo = 0;
         AV51WWPContext.gxTpr_Contratante_codigo = 0;
         AV51WWPContext.gxTpr_Userehgestor = false;
         if ( AV51WWPContext.gxTpr_Userehadministradorgam )
         {
            AV51WWPContext.gxTpr_Contratada_pessoacod = 0;
            AV51WWPContext.gxTpr_Contratada_pessoanom = "";
            AV51WWPContext.gxTpr_Contratante_cnpj = "";
            AV51WWPContext.gxTpr_Contratante_nomefantasia = "";
            AV51WWPContext.gxTpr_Contratante_razaosocial = "";
            AV51WWPContext.gxTpr_Contratante_telefone = "";
            AV51WWPContext.gxTpr_Userehgestor = true;
         }
         else
         {
            GXt_boolean3 = false;
            new prc_usuarioehlicensiado(context ).execute(  AV44Usuario_Codigo, out  GXt_boolean3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV44Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Usuario_Codigo), 6, 0)));
            AV51WWPContext.gxTpr_Userehlicenciado = GXt_boolean3;
         }
         AV51WWPContext.gxTpr_Username = StringUtil.Trim( AV17GAMUser.gxTpr_Firstname)+" "+StringUtil.Trim( AV17GAMUser.gxTpr_Lastname);
         /* Execute user subroutine: 'INICIALIZAR' */
         S112 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV51WWPContext", AV51WWPContext);
         cmbavAreatrabalho_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, cmbavAreatrabalho_codigo_Internalname, "Values", cmbavAreatrabalho_codigo.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV9Contratadas", AV9Contratadas);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV40TabsMenuData", AV40TabsMenuData);
         cmbavNewusuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27NewUsuario_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, cmbavNewusuario_codigo_Internalname, "Values", cmbavNewusuario_codigo.ToJavascriptSource());
      }

      protected void S112( )
      {
         /* 'INICIALIZAR' Routine */
         cmbavAreatrabalho_codigo.removeAllItems();
         AV7AreaTrabalho_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV7AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0)));
         AV14EntidadesDoUsuario.Clear();
         AV6Areas_Codigo.Clear();
         /* Using cursor H00MO17 */
         pr_default.execute(11, new Object[] {AV51WWPContext.gxTpr_Userid});
         while ( (pr_default.getStatus(11) != 101) )
         {
            A63ContratanteUsuario_ContratanteCod = H00MO17_A63ContratanteUsuario_ContratanteCod[0];
            A54Usuario_Ativo = H00MO17_A54Usuario_Ativo[0];
            n54Usuario_Ativo = H00MO17_n54Usuario_Ativo[0];
            A60ContratanteUsuario_UsuarioCod = H00MO17_A60ContratanteUsuario_UsuarioCod[0];
            A1020ContratanteUsuario_AreaTrabalhoCod = H00MO17_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = H00MO17_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            A54Usuario_Ativo = H00MO17_A54Usuario_Ativo[0];
            n54Usuario_Ativo = H00MO17_n54Usuario_Ativo[0];
            A1020ContratanteUsuario_AreaTrabalhoCod = H00MO17_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = H00MO17_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            AV14EntidadesDoUsuario.Add(A1020ContratanteUsuario_AreaTrabalhoCod, 0);
            pr_default.readNext(11);
         }
         pr_default.close(11);
         /* Using cursor H00MO18 */
         pr_default.execute(12, new Object[] {AV51WWPContext.gxTpr_Userid});
         while ( (pr_default.getStatus(12) != 101) )
         {
            A66ContratadaUsuario_ContratadaCod = H00MO18_A66ContratadaUsuario_ContratadaCod[0];
            A43Contratada_Ativo = H00MO18_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00MO18_n43Contratada_Ativo[0];
            A1394ContratadaUsuario_UsuarioAtivo = H00MO18_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = H00MO18_n1394ContratadaUsuario_UsuarioAtivo[0];
            A69ContratadaUsuario_UsuarioCod = H00MO18_A69ContratadaUsuario_UsuarioCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = H00MO18_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = H00MO18_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00MO18_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00MO18_n43Contratada_Ativo[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = H00MO18_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = H00MO18_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A1394ContratadaUsuario_UsuarioAtivo = H00MO18_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = H00MO18_n1394ContratadaUsuario_UsuarioAtivo[0];
            AV14EntidadesDoUsuario.Add(A1228ContratadaUsuario_AreaTrabalhoCod, 0);
            pr_default.readNext(12);
         }
         pr_default.close(12);
         AV90GXLvl383 = 0;
         pr_default.dynParam(13, new Object[]{ new Object[]{
                                              A5AreaTrabalho_Codigo ,
                                              AV14EntidadesDoUsuario ,
                                              A72AreaTrabalho_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00MO19 */
         pr_default.execute(13);
         while ( (pr_default.getStatus(13) != 101) )
         {
            A72AreaTrabalho_Ativo = H00MO19_A72AreaTrabalho_Ativo[0];
            A5AreaTrabalho_Codigo = H00MO19_A5AreaTrabalho_Codigo[0];
            A6AreaTrabalho_Descricao = H00MO19_A6AreaTrabalho_Descricao[0];
            AV90GXLvl383 = 1;
            cmbavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)), A6AreaTrabalho_Descricao, 0);
            AV6Areas_Codigo.Add(A5AreaTrabalho_Codigo, 0);
            pr_default.readNext(13);
         }
         pr_default.close(13);
         if ( AV90GXLvl383 == 0 )
         {
            AV91GXLvl390 = 0;
            pr_default.dynParam(14, new Object[]{ new Object[]{
                                                 A5AreaTrabalho_Codigo ,
                                                 AV14EntidadesDoUsuario },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor H00MO20 */
            pr_default.execute(14);
            while ( (pr_default.getStatus(14) != 101) )
            {
               A5AreaTrabalho_Codigo = H00MO20_A5AreaTrabalho_Codigo[0];
               A6AreaTrabalho_Descricao = H00MO20_A6AreaTrabalho_Descricao[0];
               AV91GXLvl390 = 1;
               cmbavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)), A6AreaTrabalho_Descricao, 0);
               AV6Areas_Codigo.Add(A5AreaTrabalho_Codigo, 0);
               pr_default.readNext(14);
            }
            pr_default.close(14);
            if ( AV91GXLvl390 == 0 )
            {
               if ( ( new SdtGAMRepository(context).checkpermission("is_gam_administrator") || AV51WWPContext.gxTpr_Userehlicenciado ) && (0==AV27NewUsuario_Codigo) )
               {
                  AV51WWPContext.gxTpr_Insert = true;
                  AV51WWPContext.gxTpr_Update = true;
                  AV51WWPContext.gxTpr_Delete = true;
                  AV51WWPContext.gxTpr_Display = true;
                  AV92GXLvl402 = 0;
                  /* Using cursor H00MO21 */
                  pr_default.execute(15);
                  while ( (pr_default.getStatus(15) != 101) )
                  {
                     A5AreaTrabalho_Codigo = H00MO21_A5AreaTrabalho_Codigo[0];
                     A6AreaTrabalho_Descricao = H00MO21_A6AreaTrabalho_Descricao[0];
                     AV92GXLvl402 = 1;
                     cmbavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)), A6AreaTrabalho_Descricao, 0);
                     AV6Areas_Codigo.Add(A5AreaTrabalho_Codigo, 0);
                     pr_default.readNext(15);
                  }
                  pr_default.close(15);
                  if ( AV92GXLvl402 == 0 )
                  {
                     cmbavAreatrabalho_codigo.addItem("0", "Nenhuma", 0);
                     AV7AreaTrabalho_Codigo = 0;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV7AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0)));
                  }
               }
               else
               {
                  GXt_objcol_int4 = AV93Udparg1;
                  new prc_areasdousuario(context ).execute(  AV51WWPContext.gxTpr_Userid, out  GXt_objcol_int4) ;
                  AV93Udparg1 = GXt_objcol_int4;
                  if ( ( AV51WWPContext.gxTpr_Areatrabalho_codigo > 0 ) && (AV93Udparg1.IndexOf(AV51WWPContext.gxTpr_Areatrabalho_codigo)>0) )
                  {
                     cmbavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV51WWPContext.gxTpr_Areatrabalho_codigo), 6, 0)), AV51WWPContext.gxTpr_Areatrabalho_descricao, 0);
                     AV6Areas_Codigo.Add(AV51WWPContext.gxTpr_Areatrabalho_codigo, 0);
                  }
                  else
                  {
                     cmbavAreatrabalho_codigo.addItem("0", "Nenhuma", 0);
                     AV7AreaTrabalho_Codigo = 0;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV7AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0)));
                  }
               }
            }
         }
         if ( AV6Areas_Codigo.Count > 0 )
         {
            if ( AV6Areas_Codigo.IndexOf(AV51WWPContext.gxTpr_Areatrabalho_codigo) > 0 )
            {
               AV7AreaTrabalho_Codigo = (int)(AV6Areas_Codigo.GetNumeric(AV6Areas_Codigo.IndexOf(AV51WWPContext.gxTpr_Areatrabalho_codigo)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV7AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0)));
            }
            else
            {
               if ( AV6Areas_Codigo.IndexOf(AV20LastUserAreaTrabalho) > 0 )
               {
                  AV7AreaTrabalho_Codigo = (int)(AV6Areas_Codigo.GetNumeric(AV6Areas_Codigo.IndexOf(AV20LastUserAreaTrabalho)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV7AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0)));
               }
               else
               {
                  AV7AreaTrabalho_Codigo = (int)(AV6Areas_Codigo.GetNumeric(1));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV7AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0)));
               }
            }
            /* Execute user subroutine: 'DADOSDAAREA' */
            S132 ();
            if (returnInSub) return;
            /* Execute user subroutine: 'CONTRATANTEOUCONTRATADA' */
            S142 ();
            if (returnInSub) return;
            AV9Contratadas.Clear();
            if ( AV51WWPContext.gxTpr_Userehcontratante )
            {
               /* Using cursor H00MO22 */
               pr_default.execute(16, new Object[] {AV51WWPContext.gxTpr_Areatrabalho_codigo});
               while ( (pr_default.getStatus(16) != 101) )
               {
                  A52Contratada_AreaTrabalhoCod = H00MO22_A52Contratada_AreaTrabalhoCod[0];
                  A39Contratada_Codigo = H00MO22_A39Contratada_Codigo[0];
                  n39Contratada_Codigo = H00MO22_n39Contratada_Codigo[0];
                  AV9Contratadas.Add(A39Contratada_Codigo, 0);
                  pr_default.readNext(16);
               }
               pr_default.close(16);
            }
            else
            {
               /* Using cursor H00MO23 */
               pr_default.execute(17, new Object[] {AV51WWPContext.gxTpr_Userid});
               while ( (pr_default.getStatus(17) != 101) )
               {
                  A69ContratadaUsuario_UsuarioCod = H00MO23_A69ContratadaUsuario_UsuarioCod[0];
                  A66ContratadaUsuario_ContratadaCod = H00MO23_A66ContratadaUsuario_ContratadaCod[0];
                  AV9Contratadas.Add(A66ContratadaUsuario_ContratadaCod, 0);
                  pr_default.readNext(17);
               }
               pr_default.close(17);
            }
         }
         /* Execute user subroutine: 'GET.PERFIL' */
         S192 ();
         if (returnInSub) return;
         new wwpbaseobjects.setwwpcontext(context ).execute(  AV51WWPContext) ;
         AV11Contratante_RazaoSocial = AV51WWPContext.gxTpr_Contratante_razaosocial;
         AV40TabsMenuData.Clear();
         AV44Usuario_Codigo = AV51WWPContext.gxTpr_Userid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV44Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Usuario_Codigo), 6, 0)));
         AV25MenuAcessoRapido.Clear();
         if ( AV51WWPContext.gxTpr_Userehadministradorgam || AV51WWPContext.gxTpr_Userehlicenciado )
         {
            GXt_objcol_int4 = (IGxCollection)(AV40TabsMenuData);
            GXt_objcol_SdtTabsMenuData_TabsMenuDataItem5 = (IGxCollection)(GXt_objcol_int4);
            new dp_tabsmenudataadm(context ).execute( out  GXt_objcol_SdtTabsMenuData_TabsMenuDataItem5) ;
            GXt_objcol_int4 = GXt_objcol_SdtTabsMenuData_TabsMenuDataItem5;
            AV40TabsMenuData = GXt_objcol_int4;
            GXt_objcol_SdtTabsMenuData_TabsMenuDataItem5 = (IGxCollection)(AV25MenuAcessoRapido);
            GXt_objcol_int4 = (IGxCollection)(GXt_objcol_SdtTabsMenuData_TabsMenuDataItem5);
            new dp_carrega_menuacessorapidoadm(context ).execute( out  GXt_objcol_int4) ;
            GXt_objcol_SdtTabsMenuData_TabsMenuDataItem5 = GXt_objcol_int4;
            AV25MenuAcessoRapido = GXt_objcol_SdtTabsMenuData_TabsMenuDataItem5;
         }
         else
         {
            GXt_objcol_SdtTabsMenuData_TabsMenuDataItem5 = AV40TabsMenuData;
            new dptabsmenudata(context ).execute(  AV44Usuario_Codigo, out  GXt_objcol_SdtTabsMenuData_TabsMenuDataItem5) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV44Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Usuario_Codigo), 6, 0)));
            AV40TabsMenuData = GXt_objcol_SdtTabsMenuData_TabsMenuDataItem5;
            GXt_objcol_SdtTabsMenuData_TabsMenuDataItem5 = (IGxCollection)(AV25MenuAcessoRapido);
            GXt_objcol_int4 = (IGxCollection)(GXt_objcol_SdtTabsMenuData_TabsMenuDataItem5);
            new dp_carrega_menuacessorapido(context ).execute(  AV44Usuario_Codigo, out  GXt_objcol_int4) ;
            GXt_objcol_SdtTabsMenuData_TabsMenuDataItem5 = GXt_objcol_int4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV44Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Usuario_Codigo), 6, 0)));
            AV25MenuAcessoRapido = GXt_objcol_SdtTabsMenuData_TabsMenuDataItem5;
         }
         AV40TabsMenuData.Sort("MenuOrder");
         AV25MenuAcessoRapido.Sort("Menu_Ordem");
         cmbavAreatrabalho_codigo.Enabled = (((cmbavAreatrabalho_codigo.ItemCount>1)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, cmbavAreatrabalho_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAreatrabalho_codigo.Enabled), 5, 0)));
         if ( AV51WWPContext.gxTpr_Userehlicenciado )
         {
            lblLblusuario_Caption = "LICENCIADO";
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblLblusuario_Internalname, "Caption", lblLblusuario_Caption);
         }
         if ( AV27NewUsuario_Codigo > 0 )
         {
            AV27NewUsuario_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV27NewUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27NewUsuario_Codigo), 6, 0)));
            AV51WWPContext.gxTpr_Updcomboareatrabalho = true;
            lblTbjava_Caption = "<script language=\"text/javascript\"> location.reload(true); </script>";
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         }
      }

      protected void S132( )
      {
         /* 'DADOSDAAREA' Routine */
         /* Using cursor H00MO24 */
         pr_default.execute(18, new Object[] {AV7AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(18) != 101) )
         {
            A335Contratante_PessoaCod = H00MO24_A335Contratante_PessoaCod[0];
            A5AreaTrabalho_Codigo = H00MO24_A5AreaTrabalho_Codigo[0];
            A29Contratante_Codigo = H00MO24_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H00MO24_n29Contratante_Codigo[0];
            A72AreaTrabalho_Ativo = H00MO24_A72AreaTrabalho_Ativo[0];
            A6AreaTrabalho_Descricao = H00MO24_A6AreaTrabalho_Descricao[0];
            A9Contratante_RazaoSocial = H00MO24_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = H00MO24_n9Contratante_RazaoSocial[0];
            A10Contratante_NomeFantasia = H00MO24_A10Contratante_NomeFantasia[0];
            A31Contratante_Telefone = H00MO24_A31Contratante_Telefone[0];
            n31Contratante_Telefone = H00MO24_n31Contratante_Telefone[0];
            A830AreaTrabalho_ServicoPadrao = H00MO24_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = H00MO24_n830AreaTrabalho_ServicoPadrao[0];
            A642AreaTrabalho_CalculoPFinal = H00MO24_A642AreaTrabalho_CalculoPFinal[0];
            A335Contratante_PessoaCod = H00MO24_A335Contratante_PessoaCod[0];
            A10Contratante_NomeFantasia = H00MO24_A10Contratante_NomeFantasia[0];
            A31Contratante_Telefone = H00MO24_A31Contratante_Telefone[0];
            n31Contratante_Telefone = H00MO24_n31Contratante_Telefone[0];
            A9Contratante_RazaoSocial = H00MO24_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = H00MO24_n9Contratante_RazaoSocial[0];
            AV10Contratante_Codigo = A29Contratante_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV10Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Contratante_Codigo), 6, 0)));
            AV51WWPContext.gxTpr_Areatrabalho_codigo = A5AreaTrabalho_Codigo;
            AV8AreaTrabalho_Descricao = A6AreaTrabalho_Descricao + (A72AreaTrabalho_Ativo ? "" : " (Inativa)");
            AV51WWPContext.gxTpr_Contratante_razaosocial = A9Contratante_RazaoSocial;
            AV51WWPContext.gxTpr_Contratante_nomefantasia = A10Contratante_NomeFantasia;
            AV51WWPContext.gxTpr_Contratante_telefone = A31Contratante_Telefone;
            AV51WWPContext.gxTpr_Servicopadrao = (short)(A830AreaTrabalho_ServicoPadrao);
            AV51WWPContext.gxTpr_Calculopfinal = A642AreaTrabalho_CalculoPFinal;
            AV51WWPContext.gxTpr_Insert = A72AreaTrabalho_Ativo;
            AV51WWPContext.gxTpr_Update = A72AreaTrabalho_Ativo;
            AV51WWPContext.gxTpr_Delete = A72AreaTrabalho_Ativo;
            AV51WWPContext.gxTpr_Display = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(18);
         AV51WWPContext.gxTpr_Areatrabalho_descricao = AV8AreaTrabalho_Descricao;
      }

      protected void S142( )
      {
         /* 'CONTRATANTEOUCONTRATADA' Routine */
         AV51WWPContext.gxTpr_Contratada_codigo = 0;
         AV51WWPContext.gxTpr_Contratada_pessoacod = 0;
         AV51WWPContext.gxTpr_Contratada_pessoanom = "";
         AV51WWPContext.gxTpr_Contratante_codigo = 0;
         AV51WWPContext.gxTpr_Contratante_razaosocial = "";
         AV51WWPContext.gxTpr_Userehcontratante = false;
         AV51WWPContext.gxTpr_Userehcontratada = false;
         AV73EndidadeUsuario = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV73EndidadeUsuario", AV73EndidadeUsuario);
         AV97GXLvl520 = 0;
         /* Using cursor H00MO25 */
         pr_default.execute(19, new Object[] {AV10Contratante_Codigo, AV51WWPContext.gxTpr_Userid});
         while ( (pr_default.getStatus(19) != 101) )
         {
            A340ContratanteUsuario_ContratantePesCod = H00MO25_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = H00MO25_n340ContratanteUsuario_ContratantePesCod[0];
            A60ContratanteUsuario_UsuarioCod = H00MO25_A60ContratanteUsuario_UsuarioCod[0];
            A63ContratanteUsuario_ContratanteCod = H00MO25_A63ContratanteUsuario_ContratanteCod[0];
            A64ContratanteUsuario_ContratanteRaz = H00MO25_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = H00MO25_n64ContratanteUsuario_ContratanteRaz[0];
            A65ContratanteUsuario_ContratanteFan = H00MO25_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = H00MO25_n65ContratanteUsuario_ContratanteFan[0];
            A31Contratante_Telefone = H00MO25_A31Contratante_Telefone[0];
            n31Contratante_Telefone = H00MO25_n31Contratante_Telefone[0];
            A2208Contratante_Sigla = H00MO25_A2208Contratante_Sigla[0];
            n2208Contratante_Sigla = H00MO25_n2208Contratante_Sigla[0];
            A340ContratanteUsuario_ContratantePesCod = H00MO25_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = H00MO25_n340ContratanteUsuario_ContratantePesCod[0];
            A65ContratanteUsuario_ContratanteFan = H00MO25_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = H00MO25_n65ContratanteUsuario_ContratanteFan[0];
            A31Contratante_Telefone = H00MO25_A31Contratante_Telefone[0];
            n31Contratante_Telefone = H00MO25_n31Contratante_Telefone[0];
            A2208Contratante_Sigla = H00MO25_A2208Contratante_Sigla[0];
            n2208Contratante_Sigla = H00MO25_n2208Contratante_Sigla[0];
            A64ContratanteUsuario_ContratanteRaz = H00MO25_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = H00MO25_n64ContratanteUsuario_ContratanteRaz[0];
            AV97GXLvl520 = 1;
            AV51WWPContext.gxTpr_Userehcontratante = true;
            AV51WWPContext.gxTpr_Contratante_codigo = A63ContratanteUsuario_ContratanteCod;
            AV51WWPContext.gxTpr_Contratante_razaosocial = A64ContratanteUsuario_ContratanteRaz;
            AV51WWPContext.gxTpr_Contratante_nomefantasia = A65ContratanteUsuario_ContratanteFan;
            AV51WWPContext.gxTpr_Contratante_telefone = A31Contratante_Telefone;
            AV51WWPContext.gxTpr_Userehgestor = true;
            AV73EndidadeUsuario = StringUtil.Format( "%1 %2", "/", StringUtil.Trim( A2208Contratante_Sigla), "", "", "", "", "", "", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV73EndidadeUsuario", AV73EndidadeUsuario);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(19);
         if ( AV97GXLvl520 == 0 )
         {
            /* Using cursor H00MO26 */
            pr_default.execute(20, new Object[] {AV51WWPContext.gxTpr_Userid, AV7AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(20) != 101) )
            {
               A69ContratadaUsuario_UsuarioCod = H00MO26_A69ContratadaUsuario_UsuarioCod[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = H00MO26_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = H00MO26_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A66ContratadaUsuario_ContratadaCod = H00MO26_A66ContratadaUsuario_ContratadaCod[0];
               A67ContratadaUsuario_ContratadaPessoaCod = H00MO26_A67ContratadaUsuario_ContratadaPessoaCod[0];
               n67ContratadaUsuario_ContratadaPessoaCod = H00MO26_n67ContratadaUsuario_ContratadaPessoaCod[0];
               A68ContratadaUsuario_ContratadaPessoaNom = H00MO26_A68ContratadaUsuario_ContratadaPessoaNom[0];
               n68ContratadaUsuario_ContratadaPessoaNom = H00MO26_n68ContratadaUsuario_ContratadaPessoaNom[0];
               A438Contratada_Sigla = H00MO26_A438Contratada_Sigla[0];
               n438Contratada_Sigla = H00MO26_n438Contratada_Sigla[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = H00MO26_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = H00MO26_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A67ContratadaUsuario_ContratadaPessoaCod = H00MO26_A67ContratadaUsuario_ContratadaPessoaCod[0];
               n67ContratadaUsuario_ContratadaPessoaCod = H00MO26_n67ContratadaUsuario_ContratadaPessoaCod[0];
               A438Contratada_Sigla = H00MO26_A438Contratada_Sigla[0];
               n438Contratada_Sigla = H00MO26_n438Contratada_Sigla[0];
               A68ContratadaUsuario_ContratadaPessoaNom = H00MO26_A68ContratadaUsuario_ContratadaPessoaNom[0];
               n68ContratadaUsuario_ContratadaPessoaNom = H00MO26_n68ContratadaUsuario_ContratadaPessoaNom[0];
               AV51WWPContext.gxTpr_Userehcontratada = true;
               AV51WWPContext.gxTpr_Contratada_codigo = A66ContratadaUsuario_ContratadaCod;
               AV51WWPContext.gxTpr_Contratada_pessoacod = A67ContratadaUsuario_ContratadaPessoaCod;
               AV51WWPContext.gxTpr_Contratada_pessoanom = A68ContratadaUsuario_ContratadaPessoaNom;
               AV73EndidadeUsuario = StringUtil.Format( "%1 %2", "/", StringUtil.Trim( A438Contratada_Sigla), "", "", "", "", "", "", "");
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV73EndidadeUsuario", AV73EndidadeUsuario);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(20);
            }
            pr_default.close(20);
         }
         /* Execute user subroutine: 'SERVICOSGERIDOS' */
         S202 ();
         if (returnInSub) return;
      }

      protected void S202( )
      {
         /* 'SERVICOSGERIDOS' Routine */
         AV51WWPContext.gxTpr_Servicosgeridos = "";
         if ( AV51WWPContext.gxTpr_Userehcontratada )
         {
            AV99GXLvl552 = 0;
            /* Using cursor H00MO27 */
            pr_default.execute(21, new Object[] {AV51WWPContext.gxTpr_Userid, AV51WWPContext.gxTpr_Contratada_codigo});
            while ( (pr_default.getStatus(21) != 101) )
            {
               A1078ContratoGestor_ContratoCod = H00MO27_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = H00MO27_A1079ContratoGestor_UsuarioCod[0];
               A1136ContratoGestor_ContratadaCod = H00MO27_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = H00MO27_n1136ContratoGestor_ContratadaCod[0];
               A1136ContratoGestor_ContratadaCod = H00MO27_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = H00MO27_n1136ContratoGestor_ContratadaCod[0];
               AV99GXLvl552 = 1;
               AV51WWPContext.gxTpr_Userehgestor = true;
               /* Using cursor H00MO28 */
               pr_default.execute(22, new Object[] {A1078ContratoGestor_ContratoCod});
               while ( (pr_default.getStatus(22) != 101) )
               {
                  A74Contrato_Codigo = H00MO28_A74Contrato_Codigo[0];
                  A155Servico_Codigo = H00MO28_A155Servico_Codigo[0];
                  AV51WWPContext.gxTpr_Servicosgeridos = AV51WWPContext.gxTpr_Servicosgeridos+StringUtil.Trim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0))+",";
                  pr_default.readNext(22);
               }
               pr_default.close(22);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(21);
            }
            pr_default.close(21);
            if ( AV99GXLvl552 == 0 )
            {
               /* Using cursor H00MO29 */
               pr_default.execute(23, new Object[] {AV51WWPContext.gxTpr_Userid, AV51WWPContext.gxTpr_Contratada_codigo});
               while ( (pr_default.getStatus(23) != 101) )
               {
                  A1824ContratoAuxiliar_ContratoCod = H00MO29_A1824ContratoAuxiliar_ContratoCod[0];
                  A1825ContratoAuxiliar_UsuarioCod = H00MO29_A1825ContratoAuxiliar_UsuarioCod[0];
                  A39Contratada_Codigo = H00MO29_A39Contratada_Codigo[0];
                  n39Contratada_Codigo = H00MO29_n39Contratada_Codigo[0];
                  A39Contratada_Codigo = H00MO29_A39Contratada_Codigo[0];
                  n39Contratada_Codigo = H00MO29_n39Contratada_Codigo[0];
                  AV51WWPContext.gxTpr_Userehgestor = true;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(23);
               }
               pr_default.close(23);
            }
         }
      }

      protected void S192( )
      {
         /* 'GET.PERFIL' Routine */
         AV51WWPContext.gxTpr_Insert = false;
         AV51WWPContext.gxTpr_Update = false;
         AV51WWPContext.gxTpr_Delete = false;
         AV51WWPContext.gxTpr_Display = true;
         lblTxbperfil_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblTxbperfil_Internalname, "Caption", lblTxbperfil_Caption);
         lblTxbemail_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblTxbemail_Internalname, "Caption", lblTxbemail_Caption);
         lblTxbusernamer_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblTxbusernamer_Internalname, "Caption", lblTxbusernamer_Caption);
         lblLbluser_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblLbluser_Internalname, "Caption", lblLbluser_Caption);
         /* Using cursor H00MO30 */
         pr_default.execute(24, new Object[] {AV51WWPContext.gxTpr_Userid});
         while ( (pr_default.getStatus(24) != 101) )
         {
            A57Usuario_PessoaCod = H00MO30_A57Usuario_PessoaCod[0];
            A3Perfil_Codigo = H00MO30_A3Perfil_Codigo[0];
            A1Usuario_Codigo = H00MO30_A1Usuario_Codigo[0];
            A4Perfil_Nome = H00MO30_A4Perfil_Nome[0];
            A1647Usuario_Email = H00MO30_A1647Usuario_Email[0];
            n1647Usuario_Email = H00MO30_n1647Usuario_Email[0];
            A58Usuario_PessoaNom = H00MO30_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00MO30_n58Usuario_PessoaNom[0];
            A544UsuarioPerfil_Insert = H00MO30_A544UsuarioPerfil_Insert[0];
            n544UsuarioPerfil_Insert = H00MO30_n544UsuarioPerfil_Insert[0];
            A659UsuarioPerfil_Update = H00MO30_A659UsuarioPerfil_Update[0];
            n659UsuarioPerfil_Update = H00MO30_n659UsuarioPerfil_Update[0];
            A546UsuarioPerfil_Delete = H00MO30_A546UsuarioPerfil_Delete[0];
            n546UsuarioPerfil_Delete = H00MO30_n546UsuarioPerfil_Delete[0];
            A543UsuarioPerfil_Display = H00MO30_A543UsuarioPerfil_Display[0];
            n543UsuarioPerfil_Display = H00MO30_n543UsuarioPerfil_Display[0];
            A4Perfil_Nome = H00MO30_A4Perfil_Nome[0];
            A57Usuario_PessoaCod = H00MO30_A57Usuario_PessoaCod[0];
            A1647Usuario_Email = H00MO30_A1647Usuario_Email[0];
            n1647Usuario_Email = H00MO30_n1647Usuario_Email[0];
            A58Usuario_PessoaNom = H00MO30_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00MO30_n58Usuario_PessoaNom[0];
            lblTxbperfil_Caption = StringUtil.Trim( A4Perfil_Nome);
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblTxbperfil_Internalname, "Caption", lblTxbperfil_Caption);
            lblTxbemail_Caption = StringUtil.Trim( A1647Usuario_Email);
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblTxbemail_Internalname, "Caption", lblTxbemail_Caption);
            lblTxbusernamer_Caption = StringUtil.Trim( A58Usuario_PessoaNom);
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblTxbusernamer_Internalname, "Caption", lblTxbusernamer_Caption);
            lblLbluser_Caption = StringUtil.Format( "%1(%2) %3", A58Usuario_PessoaNom, StringUtil.Trim( A4Perfil_Nome), AV73EndidadeUsuario, "", "", "", "", "", "");
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblLbluser_Internalname, "Caption", lblLbluser_Caption);
            AV51WWPContext.gxTpr_Insert = A544UsuarioPerfil_Insert;
            AV51WWPContext.gxTpr_Update = A659UsuarioPerfil_Update;
            AV51WWPContext.gxTpr_Delete = A546UsuarioPerfil_Delete;
            AV51WWPContext.gxTpr_Display = A543UsuarioPerfil_Display;
            tblTable14_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, tblTable14_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTable14_Visible), 5, 0)));
            tblTable15_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, tblTable15_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTable15_Visible), 5, 0)));
            GXt_char6 = "";
            new prc_iniciais(context ).execute(  A58Usuario_PessoaNom, out  GXt_char6) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
            lblTextblockiniciais_Caption = GXt_char6;
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblTextblockiniciais_Internalname, "Caption", lblTextblockiniciais_Caption);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(24);
         }
         pr_default.close(24);
      }

      protected void E13MO2( )
      {
         /* Screendetector_Screendetected Routine */
         /* Execute user subroutine: 'SCREENDETECTOR' */
         S182 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "AV51WWPContext", AV51WWPContext);
      }

      protected void S182( )
      {
         /* 'SCREENDETECTOR' Routine */
         AV51WWPContext.gxTpr_Screen_width = (short)(NumberUtil.Val( Screendetector_Screenwidth, "."));
         AV51WWPContext.gxTpr_Screen_height = (short)(NumberUtil.Val( Screendetector_Screenheight, "."));
         new wwpbaseobjects.setwwpcontext(context ).execute(  AV51WWPContext) ;
      }

      protected void S162( )
      {
         /* 'DESATIVAINTERVALOS' Routine */
         AV65Codigos.FromXml(AV49WebSession.Get("Intervalos"), "Collection");
         AV49WebSession.Remove("Intervalos");
         AV103GXV1 = 1;
         while ( AV103GXV1 <= AV65Codigos.Count )
         {
            AV66Codigo = (int)(AV65Codigos.GetNumeric(AV103GXV1));
            lblTbjava_Caption = "<script language=\"javascript\" type=\"text/javascript\"> window.clearInterval("+StringUtil.Trim( StringUtil.Str( (decimal)(AV66Codigo), 6, 0))+"); </script>";
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
            AV103GXV1 = (int)(AV103GXV1+1);
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E26MO2( )
      {
         /* Load Routine */
      }

      protected void wb_table3_50_MO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable9_Internalname, tblTable9_Internalname, "", "Table", 0, "", "", 1, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblusuario_Internalname, lblLblusuario_Caption, "", "", lblLblusuario_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "font-family:'Arial'; font-size:9.0pt; font-weight:bold; font-style:normal; color:#008000;", "TextBlock", 0, "", lblLblusuario_Visible, 1, 0, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',true,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavNewusuario_codigo, cmbavNewusuario_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV27NewUsuario_Codigo), 6, 0)), 1, cmbavNewusuario_codigo_Jsonclick, 5, "'"+""+"'"+",true,"+"'"+"EVNEWUSUARIO_CODIGO_MPAGE.CLICK_MPAGE."+"'", "int", "", cmbavNewusuario_codigo.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "", true, "HLP_MasterPageMeetrika.htm");
            cmbavNewusuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27NewUsuario_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, cmbavNewusuario_codigo_Internalname, "Values", (String)(cmbavNewusuario_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:10%")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "�rea de Trabalho:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "font-family:'Arial'; font-size:9.0pt; font-weight:bold; font-style:normal; color:#008000;", "TextBlock", 0, "", 1, 1, 0, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',true,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAreatrabalho_codigo, cmbavAreatrabalho_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0)), 1, cmbavAreatrabalho_codigo_Jsonclick, 5, "'"+""+"'"+",true,"+"'"+"EVAREATRABALHO_CODIGO_MPAGE.CLICK_MPAGE."+"'", "int", "", cmbavAreatrabalho_codigo.Visible, cmbavAreatrabalho_codigo.Enabled, 1, 0, 19, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "", true, "HLP_MasterPageMeetrika.htm");
            cmbavAreatrabalho_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, cmbavAreatrabalho_codigo_Internalname, "Values", (String)(cmbavAreatrabalho_codigo.ToJavascriptSource()));
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblusuario2_Internalname, "Usu�rio:", "", "", lblLblusuario2_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "font-family:'Arial'; font-size:9.0pt; font-weight:bold; font-style:normal; color:#008000;", "TextBlock", 0, "", 1, 1, 0, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTxbusernamer_Internalname, lblTxbusernamer_Caption, "", "", lblTxbusernamer_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "", "nome", 0, "", 1, 1, 0, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblemail_Internalname, "E-Mail:", "", "", lblLblemail_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "font-family:'Arial'; font-size:9.0pt; font-weight:bold; font-style:normal; color:#008000;", "TextBlock", 0, "", 1, 1, 0, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTxbemail_Internalname, lblTxbemail_Caption, "", "", lblTxbemail_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "", "nome", 0, "", 1, 1, 0, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblperfil_Internalname, "Perfil:", "", "", lblLblperfil_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "font-family:'Arial'; font-size:9.0pt; font-weight:bold; font-style:normal; color:#008000;", "TextBlock", 0, "", 1, 1, 0, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTxbperfil_Internalname, lblTxbperfil_Caption, "", "", lblTxbperfil_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "", "nome", 0, "", 1, 1, 0, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Active Bitmap Variable */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',true,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            AV55SimularUsuario_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV55SimularUsuario))&&String.IsNullOrEmpty(StringUtil.RTrim( AV79Simularusuario_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV55SimularUsuario)));
            GxWebStd.gx_bitmap( context, imgavSimularusuario_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV55SimularUsuario)) ? AV79Simularusuario_GXI : context.PathToRelativeUrl( AV55SimularUsuario)), "", "", "", context.GetTheme( ), imgavSimularusuario_Visible, 1, "", imgavSimularusuario_Tooltiptext, 0, -1, 0, "", 0, "", 0, 0, 5, imgavSimularusuario_Jsonclick, "'"+""+"'"+",true,"+"'"+"EVSIMULARUSUARIO_MPAGE.CLICK_MPAGE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, AV55SimularUsuario_IsBlob, false, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            wb_table4_81_MO2( true) ;
         }
         else
         {
            wb_table4_81_MO2( false) ;
         }
         return  ;
      }

      protected void wb_table4_81_MO2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table5_85_MO2( true) ;
         }
         else
         {
            wb_table5_85_MO2( false) ;
         }
         return  ;
      }

      protected void wb_table5_85_MO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_50_MO2e( true) ;
         }
         else
         {
            wb_table3_50_MO2e( false) ;
         }
      }

      protected void wb_table5_85_MO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTable14_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTable14_Internalname, tblTable14_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_85_MO2e( true) ;
         }
         else
         {
            wb_table5_85_MO2e( false) ;
         }
      }

      protected void wb_table4_81_MO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTable15_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTable15_Internalname, tblTable15_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockiniciais_Internalname, lblTextblockiniciais_Caption, "", "", lblTextblockiniciais_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_81_MO2e( true) ;
         }
         else
         {
            wb_table4_81_MO2e( false) ;
         }
      }

      protected void wb_table2_34_MO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblwarnings_Internalname, tblTblwarnings_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            context.WriteHtmlText( "<td data-align=\"center\" bgcolor=\"#FF0000\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;width:40px")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',true,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQtderro_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32QtdErro), 4, 0, ",", "")), ((edtavQtderro_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32QtdErro), "ZZZ9")) : context.localUtil.Format( (decimal)(AV32QtdErro), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",true,"+"'"+"EVQTDERRO_MPAGE.CLICK_MPAGE."+"'", "", "", edtavQtderro_Tooltiptext, "", edtavQtderro_Jsonclick, 5, "BootstrapAttributeBackRed", "font-family:'Arial'; font-size:10.0pt; font-weight:bold; font-style:normal; color:#FFFFFF;", "", "", 1, edtavQtderro_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\" bgcolor=\"#FFA500\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;width:40px")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',true,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQtdlrnj_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33QtdLrnj), 4, 0, ",", "")), ((edtavQtdlrnj_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV33QtdLrnj), "ZZZ9")) : context.localUtil.Format( (decimal)(AV33QtdLrnj), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",true,"+"'"+"EVQTDLRNJ_MPAGE.CLICK_MPAGE."+"'", "", "", edtavQtdlrnj_Tooltiptext, "", edtavQtdlrnj_Jsonclick, 5, "WarningAttribute", "font-family:'Arial'; font-size:10.0pt; font-weight:bold; font-style:normal; color:#000000;", "", "", 1, edtavQtdlrnj_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\" bgcolor=\"#FFFF00\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;width:40px")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',true,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQtdpend_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34QtdPend), 4, 0, ",", "")), ((edtavQtdpend_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV34QtdPend), "ZZZ9")) : context.localUtil.Format( (decimal)(AV34QtdPend), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",true,"+"'"+"EVQTDPEND_MPAGE.CLICK_MPAGE."+"'", "", "", edtavQtdpend_Tooltiptext, "", edtavQtdpend_Jsonclick, 5, "WarningAttribute", "font-family:'Arial'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavQtdpend_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\" bgcolor=\"#00FA9A\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;width:40px")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',true,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQtdsolicitadas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35QtdSolicitadas), 4, 0, ",", "")), ((edtavQtdsolicitadas_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35QtdSolicitadas), "ZZZ9")) : context.localUtil.Format( (decimal)(AV35QtdSolicitadas), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",true,"+"'"+"EVQTDSOLICITADAS_MPAGE.CLICK_MPAGE."+"'", "", "", edtavQtdsolicitadas_Tooltiptext, "", edtavQtdsolicitadas_Jsonclick, 5, "WarningAttribute", "font-family:'Arial'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavQtdsolicitadas_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellAtualizar_Internalname+"\" title=\"Atualizar quantidades\" data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class=''>") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',true,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImage1_Internalname, context.GetImagePath( "1021f89d-15e7-49bd-aa4e-7023dc926c54", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImage1_Jsonclick, "'"+""+"'"+",true,"+"'"+"EATUALIZARWARNINGS_MPAGE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_34_MO2e( true) ;
         }
         else
         {
            wb_table2_34_MO2e( false) ;
         }
      }

      protected void wb_table1_7_MO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagehome_Internalname, context.GetImagePath( "f8599651-11b4-4f31-9274-bbbbb08932e6", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_12_MO2( true) ;
         }
         else
         {
            wb_table6_12_MO2( false) ;
         }
         return  ;
      }

      protected void wb_table6_12_MO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_7_MO2e( true) ;
         }
         else
         {
            wb_table1_7_MO2e( false) ;
         }
      }

      protected void wb_table6_12_MO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:15px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "�rea de Trabalho", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblareatrabalho_Internalname, lblLblareatrabalho_Caption, "", "", lblLblareatrabalho_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "color:#93FFAF;", "TextBlock", 0, "", 1, 1, 0, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "Usu�rio/Perfil", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLbluser_Internalname, lblLbluser_Caption, "", "", lblLbluser_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "color:#93FFAF;", "TextBlock", 0, "", 1, 1, 0, "HLP_MasterPageMeetrika.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_12_MO2e( true) ;
         }
         else
         {
            wb_table6_12_MO2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMO2( ) ;
         WSMO2( ) ;
         WEMO2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void master_styles( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DropDownTabsMenu/css/DropDownTabsMenu.css", "?2256370");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         if ( ! ( WebComp_Wcrecentlinks == null ) )
         {
            if ( StringUtil.Len( WebComp_Wcrecentlinks_Component) != 0 )
            {
               WebComp_Wcrecentlinks.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)(getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Jscriptsrc.Item(idxLst))), "?20206216265931");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("masterpagemeetrika.js", "?20206216265933");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DropDownTabsMenu/dropdowntabs.js", "");
         context.AddJavascriptSource("DropDownTabsMenu/DropDownTabsMenuRender.js", "");
         context.AddJavascriptSource("ScreenDetector/ScreenDetectorRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgImagehome_Internalname = "IMAGEHOME_MPAGE";
         lblTextblock2_Internalname = "TEXTBLOCK2_MPAGE";
         lblLblareatrabalho_Internalname = "LBLAREATRABALHO_MPAGE";
         lblTextblock5_Internalname = "TEXTBLOCK5_MPAGE";
         lblLbluser_Internalname = "LBLUSER_MPAGE";
         tblTable2_Internalname = "TABLE2_MPAGE";
         tblTable1_Internalname = "TABLE1_MPAGE";
         divSection5_Internalname = "SECTION5_MPAGE";
         divSection4_Internalname = "SECTION4_MPAGE";
         lblControhelp_Internalname = "CONTROHELP_MPAGE";
         divSection13_Internalname = "SECTION13_MPAGE";
         lblTextblock6_Internalname = "TEXTBLOCK6_MPAGE";
         divSection17_Internalname = "SECTION17_MPAGE";
         div_Internalname = "_MPAGE";
         edtavQtderro_Internalname = "vQTDERRO_MPAGE";
         edtavQtdlrnj_Internalname = "vQTDLRNJ_MPAGE";
         edtavQtdpend_Internalname = "vQTDPEND_MPAGE";
         edtavQtdsolicitadas_Internalname = "vQTDSOLICITADAS_MPAGE";
         imgImage1_Internalname = "IMAGE1_MPAGE";
         cellAtualizar_Internalname = "ATUALIZAR_MPAGE";
         tblTblwarnings_Internalname = "TBLWARNINGS_MPAGE";
         divNotificacoes_Internalname = "NOTIFICACOES_MPAGE";
         lblTxbnotificacao_Internalname = "TXBNOTIFICACAO_MPAGE";
         divSection12_Internalname = "SECTION12_MPAGE";
         lblTxbusuario_Internalname = "TXBUSUARIO_MPAGE";
         lblLblusuario_Internalname = "LBLUSUARIO_MPAGE";
         cmbavNewusuario_codigo_Internalname = "vNEWUSUARIO_CODIGO_MPAGE";
         lblTextblock1_Internalname = "TEXTBLOCK1_MPAGE";
         cmbavAreatrabalho_codigo_Internalname = "vAREATRABALHO_CODIGO_MPAGE";
         lblTbjava_Internalname = "TBJAVA_MPAGE";
         lblLblusuario2_Internalname = "LBLUSUARIO2_MPAGE";
         lblTxbusernamer_Internalname = "TXBUSERNAMER_MPAGE";
         lblLblemail_Internalname = "LBLEMAIL_MPAGE";
         lblTxbemail_Internalname = "TXBEMAIL_MPAGE";
         lblLblperfil_Internalname = "LBLPERFIL_MPAGE";
         lblTxbperfil_Internalname = "TXBPERFIL_MPAGE";
         imgavSimularusuario_Internalname = "vSIMULARUSUARIO_MPAGE";
         lblTextblockiniciais_Internalname = "TEXTBLOCKINICIAIS_MPAGE";
         tblTable15_Internalname = "TABLE15_MPAGE";
         tblTable14_Internalname = "TABLE14_MPAGE";
         tblTable9_Internalname = "TABLE9_MPAGE";
         divUsuario_Internalname = "USUARIO_MPAGE";
         divSection11_Internalname = "SECTION11_MPAGE";
         lblTextblock4_Internalname = "TEXTBLOCK4_MPAGE";
         divSection10_Internalname = "SECTION10_MPAGE";
         divSection9_Internalname = "SECTION9_MPAGE";
         divSection8_Internalname = "SECTION8_MPAGE";
         divSection7_Internalname = "SECTION7_MPAGE";
         divSection6_Internalname = "SECTION6_MPAGE";
         divSection3_Internalname = "SECTION3_MPAGE";
         divSection2_Internalname = "SECTION2_MPAGE";
         Dropdowntabsmenu1_Internalname = "DROPDOWNTABSMENU1_MPAGE";
         divSection23_Internalname = "SECTION23_MPAGE";
         divSection14_Internalname = "SECTION14_MPAGE";
         divSection20_Internalname = "SECTION20_MPAGE";
         divSection19_Internalname = "SECTION19_MPAGE";
         divSection15_Internalname = "SECTION15_MPAGE";
         div_Internalname = "_MPAGE";
         Screendetector_Internalname = "SCREENDETECTOR_MPAGE";
         div_Internalname = "_MPAGE";
         Confirmpanel_Internalname = "CONFIRMPANEL_MPAGE";
         div_Internalname = "_MPAGE";
         divSection1_Internalname = "SECTION1_MPAGE";
         edtavHelpsistema_objeto_Internalname = "vHELPSISTEMA_OBJETO_MPAGE";
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Internalname = "FORM_MPAGE";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavQtdsolicitadas_Jsonclick = "";
         edtavQtdsolicitadas_Enabled = 1;
         edtavQtdpend_Jsonclick = "";
         edtavQtdpend_Enabled = 1;
         edtavQtdlrnj_Jsonclick = "";
         edtavQtdlrnj_Enabled = 1;
         edtavQtderro_Jsonclick = "";
         edtavQtderro_Enabled = 1;
         imgavSimularusuario_Jsonclick = "";
         lblTbjava_Visible = 1;
         cmbavAreatrabalho_codigo_Jsonclick = "";
         cmbavNewusuario_codigo_Jsonclick = "";
         lblLblusuario_Visible = 1;
         lblTextblockiniciais_Caption = "TextblockIniciais";
         tblTable15_Visible = 1;
         tblTable14_Visible = 1;
         lblLbluser_Caption = "Usu�rio";
         lblTxbemail_Caption = "Admin";
         lblTxbperfil_Caption = "Admin";
         cmbavAreatrabalho_codigo.Enabled = 1;
         edtavQtdsolicitadas_Tooltiptext = "";
         edtavQtdlrnj_Tooltiptext = "";
         edtavQtdpend_Tooltiptext = "";
         edtavQtderro_Tooltiptext = "";
         lblTxbusernamer_Caption = "Ricardo Vila Nova";
         lblTbjava_Caption = "Java";
         lblLblusuario_Caption = "Usu�rio:";
         imgavSimularusuario_Tooltiptext = "";
         cmbavAreatrabalho_codigo.Visible = 1;
         imgavSimularusuario_Visible = 1;
         cmbavNewusuario_codigo.Visible = 1;
         lblLblareatrabalho_Caption = "�rea de Trabalho";
         edtavHelpsistema_objeto_Jsonclick = "";
         edtavHelpsistema_objeto_Visible = 1;
         divNotificacoes_Visible = 1;
         cmbavNewusuario_codigo.Description = "";
         Confirmpanel_Texttype = "2";
         Confirmpanel_Confirmtype = "0";
         Confirmpanel_Yesbuttonposition = "right";
         Confirmpanel_Cancelbuttoncaption = "Cancelar";
         Confirmpanel_Nobuttoncaption = "Cancelar";
         Confirmpanel_Yesbuttoncaption = "Fechar";
         Confirmpanel_Confirmationtext = "Messagem";
         Confirmpanel_Title = "Aten��o!";
         Confirmpanel_Width = "1120px";
         Dropdowntabsmenu1_Menustyle = "bluetabs";
         Dropdowntabsmenu1_Height = "10px";
         Contentholder.setDataArea(getDataAreaObject());
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH_MPAGE","{handler:'Refresh',iparms:[{av:'AV51WWPContext',fld:'vWWPCONTEXT_MPAGE',pic:'',nv:null},{av:'A330ParametrosSistema_Codigo',fld:'PARAMETROSSISTEMA_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A1952ParametrosSistema_URLOtherVer',fld:'PARAMETROSSISTEMA_URLOTHERVER_MPAGE',pic:'',nv:''},{av:'A1954ParametrosSistema_Validacao',fld:'PARAMETROSSISTEMA_VALIDACAO_MPAGE',pic:'',nv:''},{av:'AV27NewUsuario_Codigo',fld:'vNEWUSUARIO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{ctrl:'FORM_MPAGE',prop:'Caption'},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN_MPAGE',pic:'',nv:''},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A472ContagemResultado_DataEntrega',fld:'CONTAGEMRESULTADO_DATAENTREGA_MPAGE',pic:'',nv:''},{av:'AV9Contratadas',fld:'vCONTRATADAS_MPAGE',pic:'',nv:null},{av:'AV5ServerDate',fld:'vSERVERDATE_MPAGE',pic:'',nv:''},{av:'A531ContagemResultado_StatusUltCnt',fld:'CONTAGEMRESULTADO_STATUSULTCNT_MPAGE',pic:'Z9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A499ContagemResultado_ContratadaPessoaCod',fld:'CONTAGEMRESULTADO_CONTRATADAPESSOACOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO_MPAGE',pic:'',nv:false},{av:'A1020ContratanteUsuario_AreaTrabalhoCod',fld:'CONTRATANTEUSUARIO_AREATRABALHOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO_MPAGE',pic:'',nv:false},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO_MPAGE',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO_MPAGE',pic:'@!',nv:''},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO_MPAGE',pic:'',nv:false},{av:'AV20LastUserAreaTrabalho',fld:'vLASTUSERAREATRABALHO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'AV7AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A9Contratante_RazaoSocial',fld:'CONTRATANTE_RAZAOSOCIAL_MPAGE',pic:'@!',nv:''},{av:'A10Contratante_NomeFantasia',fld:'CONTRATANTE_NOMEFANTASIA_MPAGE',pic:'@!',nv:''},{av:'A31Contratante_Telefone',fld:'CONTRATANTE_TELEFONE_MPAGE',pic:'',nv:''},{av:'A830AreaTrabalho_ServicoPadrao',fld:'AREATRABALHO_SERVICOPADRAO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A642AreaTrabalho_CalculoPFinal',fld:'AREATRABALHO_CALCULOPFINAL_MPAGE',pic:'',nv:''},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'AV10Contratante_Codigo',fld:'vCONTRATANTE_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ_MPAGE',pic:'@!',nv:''},{av:'A65ContratanteUsuario_ContratanteFan',fld:'CONTRATANTEUSUARIO_CONTRATANTEFAN_MPAGE',pic:'@!',nv:''},{av:'A2208Contratante_Sigla',fld:'CONTRATANTE_SIGLA_MPAGE',pic:'@!',nv:''},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM_MPAGE',pic:'@!',nv:''},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA_MPAGE',pic:'@!',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A4Perfil_Nome',fld:'PERFIL_NOME_MPAGE',pic:'@!',nv:''},{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL_MPAGE',pic:'',nv:''},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM_MPAGE',pic:'@!',nv:''},{av:'AV73EndidadeUsuario',fld:'vENDIDADEUSUARIO_MPAGE',pic:'@!',nv:''},{av:'A544UsuarioPerfil_Insert',fld:'USUARIOPERFIL_INSERT_MPAGE',pic:'',nv:false},{av:'A659UsuarioPerfil_Update',fld:'USUARIOPERFIL_UPDATE_MPAGE',pic:'',nv:false},{av:'A546UsuarioPerfil_Delete',fld:'USUARIOPERFIL_DELETE_MPAGE',pic:'',nv:false},{av:'A543UsuarioPerfil_Display',fld:'USUARIOPERFIL_DISPLAY_MPAGE',pic:'',nv:false},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD_MPAGE',pic:'ZZZZZ9',nv:0}],oparms:[{av:'lblLblareatrabalho_Caption',ctrl:'LBLAREATRABALHO_MPAGE',prop:'Caption'},{av:'AV51WWPContext',fld:'vWWPCONTEXT_MPAGE',pic:'',nv:null},{av:'lblTxbusernamer_Caption',ctrl:'TXBUSERNAMER_MPAGE',prop:'Caption'},{ctrl:'WCRECENTLINKS_MPAGE'},{av:'AV69HelpSistema_Objeto',fld:'vHELPSISTEMA_OBJETO_MPAGE',pic:'',nv:''},{av:'AV32QtdErro',fld:'vQTDERRO_MPAGE',pic:'ZZZ9',nv:0},{av:'AV34QtdPend',fld:'vQTDPEND_MPAGE',pic:'ZZZ9',nv:0},{av:'AV33QtdLrnj',fld:'vQTDLRNJ_MPAGE',pic:'ZZZ9',nv:0},{av:'AV35QtdSolicitadas',fld:'vQTDSOLICITADAS_MPAGE',pic:'ZZZ9',nv:0},{av:'AV47Vermelhas',fld:'vVERMELHAS_MPAGE',pic:'',nv:null},{av:'AV52Laranjas',fld:'vLARANJAS_MPAGE',pic:'',nv:null},{av:'AV53Amarelas',fld:'vAMARELAS_MPAGE',pic:'',nv:null},{av:'AV54Verdes',fld:'vVERDES_MPAGE',pic:'',nv:null},{av:'edtavQtderro_Tooltiptext',ctrl:'vQTDERRO_MPAGE',prop:'Tooltiptext'},{av:'edtavQtdpend_Tooltiptext',ctrl:'vQTDPEND_MPAGE',prop:'Tooltiptext'},{av:'edtavQtdlrnj_Tooltiptext',ctrl:'vQTDLRNJ_MPAGE',prop:'Tooltiptext'},{av:'edtavQtdsolicitadas_Tooltiptext',ctrl:'vQTDSOLICITADAS_MPAGE',prop:'Tooltiptext'},{av:'AV7AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'AV9Contratadas',fld:'vCONTRATADAS_MPAGE',pic:'',nv:null},{av:'AV40TabsMenuData',fld:'vTABSMENUDATA_MPAGE',pic:'',nv:null},{av:'AV44Usuario_Codigo',fld:'vUSUARIO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'cmbavAreatrabalho_codigo'},{av:'lblLblusuario_Caption',ctrl:'LBLUSUARIO_MPAGE',prop:'Caption'},{av:'AV27NewUsuario_Codigo',fld:'vNEWUSUARIO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'lblTbjava_Caption',ctrl:'TBJAVA_MPAGE',prop:'Caption'},{av:'AV10Contratante_Codigo',fld:'vCONTRATANTE_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'AV73EndidadeUsuario',fld:'vENDIDADEUSUARIO_MPAGE',pic:'@!',nv:''},{av:'lblTxbperfil_Caption',ctrl:'TXBPERFIL_MPAGE',prop:'Caption'},{av:'lblTxbemail_Caption',ctrl:'TXBEMAIL_MPAGE',prop:'Caption'},{av:'lblLbluser_Caption',ctrl:'LBLUSER_MPAGE',prop:'Caption'},{av:'tblTable14_Visible',ctrl:'TABLE14_MPAGE',prop:'Visible'},{av:'tblTable15_Visible',ctrl:'TABLE15_MPAGE',prop:'Visible'},{av:'lblTextblockiniciais_Caption',ctrl:'TEXTBLOCKINICIAIS_MPAGE',prop:'Caption'}]}");
         setEventMetadata("HELPSISTEMA_MPAGE","{handler:'E15MO2',iparms:[{av:'A1639HelpSistema_Objeto',fld:'HELPSISTEMA_OBJETO_MPAGE',pic:'',nv:''},{av:'AV69HelpSistema_Objeto',fld:'vHELPSISTEMA_OBJETO_MPAGE',pic:'',nv:''}],oparms:[{av:'Confirmpanel_Title',ctrl:'CONFIRMPANEL_MPAGE',prop:'Title'},{av:'Confirmpanel_Confirmationtext',ctrl:'CONFIRMPANEL_MPAGE',prop:'ConfirmationText'},{av:'Confirmpanel_Yesbuttoncaption',ctrl:'CONFIRMPANEL_MPAGE',prop:'YesButtonCaption'},{av:'Confirmpanel_Confirmtype',ctrl:'CONFIRMPANEL_MPAGE',prop:'ConfirmType'}]}");
         setEventMetadata("HELPSISTEMAF1_MPAGE","{handler:'E12MO2',iparms:[{av:'A1639HelpSistema_Objeto',fld:'HELPSISTEMA_OBJETO_MPAGE',pic:'',nv:''},{av:'AV69HelpSistema_Objeto',fld:'vHELPSISTEMA_OBJETO_MPAGE',pic:'',nv:''}],oparms:[{av:'Confirmpanel_Title',ctrl:'CONFIRMPANEL_MPAGE',prop:'Title'},{av:'Confirmpanel_Confirmationtext',ctrl:'CONFIRMPANEL_MPAGE',prop:'ConfirmationText'},{av:'Confirmpanel_Yesbuttoncaption',ctrl:'CONFIRMPANEL_MPAGE',prop:'YesButtonCaption'},{av:'Confirmpanel_Confirmtype',ctrl:'CONFIRMPANEL_MPAGE',prop:'ConfirmType'}]}");
         setEventMetadata("LOG_OUT_MPAGE","{handler:'E16MO2',iparms:[{av:'AV51WWPContext',fld:'vWWPCONTEXT_MPAGE',pic:'',nv:null},{av:'AV7AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV51WWPContext',fld:'vWWPCONTEXT_MPAGE',pic:'',nv:null}]}");
         setEventMetadata("VAREATRABALHO_CODIGO_MPAGE.CLICK_MPAGE","{handler:'E17MO2',iparms:[{av:'AV7AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'AV51WWPContext',fld:'vWWPCONTEXT_MPAGE',pic:'',nv:null},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO_MPAGE',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO_MPAGE',pic:'',nv:false},{av:'A9Contratante_RazaoSocial',fld:'CONTRATANTE_RAZAOSOCIAL_MPAGE',pic:'@!',nv:''},{av:'A10Contratante_NomeFantasia',fld:'CONTRATANTE_NOMEFANTASIA_MPAGE',pic:'@!',nv:''},{av:'A31Contratante_Telefone',fld:'CONTRATANTE_TELEFONE_MPAGE',pic:'',nv:''},{av:'A830AreaTrabalho_ServicoPadrao',fld:'AREATRABALHO_SERVICOPADRAO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A642AreaTrabalho_CalculoPFinal',fld:'AREATRABALHO_CALCULOPFINAL_MPAGE',pic:'',nv:''},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'AV10Contratante_Codigo',fld:'vCONTRATANTE_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ_MPAGE',pic:'@!',nv:''},{av:'A65ContratanteUsuario_ContratanteFan',fld:'CONTRATANTEUSUARIO_CONTRATANTEFAN_MPAGE',pic:'@!',nv:''},{av:'A2208Contratante_Sigla',fld:'CONTRATANTE_SIGLA_MPAGE',pic:'@!',nv:''},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM_MPAGE',pic:'@!',nv:''},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA_MPAGE',pic:'@!',nv:''},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD_MPAGE',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV51WWPContext',fld:'vWWPCONTEXT_MPAGE',pic:'',nv:null},{av:'lblTbjava_Caption',ctrl:'TBJAVA_MPAGE',prop:'Caption'},{av:'AV10Contratante_Codigo',fld:'vCONTRATANTE_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'AV73EndidadeUsuario',fld:'vENDIDADEUSUARIO_MPAGE',pic:'@!',nv:''}]}");
         setEventMetadata("ATUALIZARWARNINGS_MPAGE","{handler:'E19MO2',iparms:[{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN_MPAGE',pic:'',nv:''},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A472ContagemResultado_DataEntrega',fld:'CONTAGEMRESULTADO_DATAENTREGA_MPAGE',pic:'',nv:''},{av:'AV9Contratadas',fld:'vCONTRATADAS_MPAGE',pic:'',nv:null},{av:'AV51WWPContext',fld:'vWWPCONTEXT_MPAGE',pic:'',nv:null},{av:'AV5ServerDate',fld:'vSERVERDATE_MPAGE',pic:'',nv:''},{av:'A531ContagemResultado_StatusUltCnt',fld:'CONTAGEMRESULTADO_STATUSULTCNT_MPAGE',pic:'Z9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A499ContagemResultado_ContratadaPessoaCod',fld:'CONTAGEMRESULTADO_CONTRATADAPESSOACOD_MPAGE',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV32QtdErro',fld:'vQTDERRO_MPAGE',pic:'ZZZ9',nv:0},{av:'AV34QtdPend',fld:'vQTDPEND_MPAGE',pic:'ZZZ9',nv:0},{av:'AV33QtdLrnj',fld:'vQTDLRNJ_MPAGE',pic:'ZZZ9',nv:0},{av:'AV35QtdSolicitadas',fld:'vQTDSOLICITADAS_MPAGE',pic:'ZZZ9',nv:0},{av:'AV47Vermelhas',fld:'vVERMELHAS_MPAGE',pic:'',nv:null},{av:'AV52Laranjas',fld:'vLARANJAS_MPAGE',pic:'',nv:null},{av:'AV53Amarelas',fld:'vAMARELAS_MPAGE',pic:'',nv:null},{av:'AV54Verdes',fld:'vVERDES_MPAGE',pic:'',nv:null},{av:'edtavQtderro_Tooltiptext',ctrl:'vQTDERRO_MPAGE',prop:'Tooltiptext'},{av:'edtavQtdpend_Tooltiptext',ctrl:'vQTDPEND_MPAGE',prop:'Tooltiptext'},{av:'edtavQtdlrnj_Tooltiptext',ctrl:'vQTDLRNJ_MPAGE',prop:'Tooltiptext'},{av:'edtavQtdsolicitadas_Tooltiptext',ctrl:'vQTDSOLICITADAS_MPAGE',prop:'Tooltiptext'}]}");
         setEventMetadata("VSIMULARUSUARIO_MPAGE.CLICK_MPAGE","{handler:'E20MO2',iparms:[],oparms:[]}");
         setEventMetadata("VQTDERRO_MPAGE.CLICK_MPAGE","{handler:'E21MO2',iparms:[{av:'AV32QtdErro',fld:'vQTDERRO_MPAGE',pic:'ZZZ9',nv:0},{av:'AV47Vermelhas',fld:'vVERMELHAS_MPAGE',pic:'',nv:null},{av:'AV38SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM_MPAGE',pic:'',nv:null}],oparms:[{av:'AV38SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM_MPAGE',pic:'',nv:null}]}");
         setEventMetadata("VQTDLRNJ_MPAGE.CLICK_MPAGE","{handler:'E22MO2',iparms:[{av:'AV33QtdLrnj',fld:'vQTDLRNJ_MPAGE',pic:'ZZZ9',nv:0},{av:'AV52Laranjas',fld:'vLARANJAS_MPAGE',pic:'',nv:null},{av:'AV38SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM_MPAGE',pic:'',nv:null}],oparms:[{av:'AV38SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM_MPAGE',pic:'',nv:null}]}");
         setEventMetadata("VQTDPEND_MPAGE.CLICK_MPAGE","{handler:'E23MO2',iparms:[{av:'AV34QtdPend',fld:'vQTDPEND_MPAGE',pic:'ZZZ9',nv:0},{av:'AV53Amarelas',fld:'vAMARELAS_MPAGE',pic:'',nv:null},{av:'AV38SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM_MPAGE',pic:'',nv:null}],oparms:[{av:'AV38SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM_MPAGE',pic:'',nv:null}]}");
         setEventMetadata("VQTDSOLICITADAS_MPAGE.CLICK_MPAGE","{handler:'E24MO2',iparms:[{av:'AV35QtdSolicitadas',fld:'vQTDSOLICITADAS_MPAGE',pic:'ZZZ9',nv:0},{av:'AV54Verdes',fld:'vVERDES_MPAGE',pic:'',nv:null},{av:'AV38SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM_MPAGE',pic:'',nv:null}],oparms:[{av:'AV38SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM_MPAGE',pic:'',nv:null}]}");
         setEventMetadata("VNEWUSUARIO_CODIGO_MPAGE.CLICK_MPAGE","{handler:'E25MO2',iparms:[{av:'AV27NewUsuario_Codigo',fld:'vNEWUSUARIO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'AV51WWPContext',fld:'vWWPCONTEXT_MPAGE',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A290Usuario_EhAuditorFM',fld:'USUARIO_EHAUDITORFM_MPAGE',pic:'',nv:false},{av:'A289Usuario_EhContador',fld:'USUARIO_EHCONTADOR_MPAGE',pic:'',nv:false},{av:'A293Usuario_EhFinanceiro',fld:'USUARIO_EHFINANCEIRO_MPAGE',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID_MPAGE',pic:'',nv:''},{av:'A2016Usuario_UltimaArea',fld:'USUARIO_ULTIMAAREA_MPAGE',pic:'ZZZZZ9',nv:0},{av:'cmbavNewusuario_codigo'},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO_MPAGE',pic:'',nv:false},{av:'A1020ContratanteUsuario_AreaTrabalhoCod',fld:'CONTRATANTEUSUARIO_AREATRABALHOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO_MPAGE',pic:'',nv:false},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO_MPAGE',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO_MPAGE',pic:'@!',nv:''},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO_MPAGE',pic:'',nv:false},{av:'AV20LastUserAreaTrabalho',fld:'vLASTUSERAREATRABALHO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'AV7AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A9Contratante_RazaoSocial',fld:'CONTRATANTE_RAZAOSOCIAL_MPAGE',pic:'@!',nv:''},{av:'A10Contratante_NomeFantasia',fld:'CONTRATANTE_NOMEFANTASIA_MPAGE',pic:'@!',nv:''},{av:'A31Contratante_Telefone',fld:'CONTRATANTE_TELEFONE_MPAGE',pic:'',nv:''},{av:'A830AreaTrabalho_ServicoPadrao',fld:'AREATRABALHO_SERVICOPADRAO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A642AreaTrabalho_CalculoPFinal',fld:'AREATRABALHO_CALCULOPFINAL_MPAGE',pic:'',nv:''},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'AV10Contratante_Codigo',fld:'vCONTRATANTE_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ_MPAGE',pic:'@!',nv:''},{av:'A65ContratanteUsuario_ContratanteFan',fld:'CONTRATANTEUSUARIO_CONTRATANTEFAN_MPAGE',pic:'@!',nv:''},{av:'A2208Contratante_Sigla',fld:'CONTRATANTE_SIGLA_MPAGE',pic:'@!',nv:''},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM_MPAGE',pic:'@!',nv:''},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA_MPAGE',pic:'@!',nv:''},{av:'A4Perfil_Nome',fld:'PERFIL_NOME_MPAGE',pic:'@!',nv:''},{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL_MPAGE',pic:'',nv:''},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM_MPAGE',pic:'@!',nv:''},{av:'AV73EndidadeUsuario',fld:'vENDIDADEUSUARIO_MPAGE',pic:'@!',nv:''},{av:'A544UsuarioPerfil_Insert',fld:'USUARIOPERFIL_INSERT_MPAGE',pic:'',nv:false},{av:'A659UsuarioPerfil_Update',fld:'USUARIOPERFIL_UPDATE_MPAGE',pic:'',nv:false},{av:'A546UsuarioPerfil_Delete',fld:'USUARIOPERFIL_DELETE_MPAGE',pic:'',nv:false},{av:'A543UsuarioPerfil_Display',fld:'USUARIOPERFIL_DISPLAY_MPAGE',pic:'',nv:false},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD_MPAGE',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV44Usuario_Codigo',fld:'vUSUARIO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'AV51WWPContext',fld:'vWWPCONTEXT_MPAGE',pic:'',nv:null},{av:'AV20LastUserAreaTrabalho',fld:'vLASTUSERAREATRABALHO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'AV7AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'AV9Contratadas',fld:'vCONTRATADAS_MPAGE',pic:'',nv:null},{av:'AV40TabsMenuData',fld:'vTABSMENUDATA_MPAGE',pic:'',nv:null},{av:'cmbavAreatrabalho_codigo'},{av:'lblLblusuario_Caption',ctrl:'LBLUSUARIO_MPAGE',prop:'Caption'},{av:'AV27NewUsuario_Codigo',fld:'vNEWUSUARIO_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'lblTbjava_Caption',ctrl:'TBJAVA_MPAGE',prop:'Caption'},{av:'AV10Contratante_Codigo',fld:'vCONTRATANTE_CODIGO_MPAGE',pic:'ZZZZZ9',nv:0},{av:'AV73EndidadeUsuario',fld:'vENDIDADEUSUARIO_MPAGE',pic:'@!',nv:''},{av:'lblTxbperfil_Caption',ctrl:'TXBPERFIL_MPAGE',prop:'Caption'},{av:'lblTxbemail_Caption',ctrl:'TXBEMAIL_MPAGE',prop:'Caption'},{av:'lblTxbusernamer_Caption',ctrl:'TXBUSERNAMER_MPAGE',prop:'Caption'},{av:'lblLbluser_Caption',ctrl:'LBLUSER_MPAGE',prop:'Caption'},{av:'tblTable14_Visible',ctrl:'TABLE14_MPAGE',prop:'Visible'},{av:'tblTable15_Visible',ctrl:'TABLE15_MPAGE',prop:'Visible'},{av:'lblTextblockiniciais_Caption',ctrl:'TEXTBLOCKINICIAIS_MPAGE',prop:'Caption'}]}");
         setEventMetadata("CLIQUE_MPAGE","{handler:'E11MO1',iparms:[],oparms:[]}");
         setEventMetadata("SCREENDETECTOR_MPAGE.SCREENDETECTED_MPAGE","{handler:'E13MO2',iparms:[{av:'Screendetector_Screenwidth',ctrl:'SCREENDETECTOR_MPAGE',prop:'ScreenWidth'},{av:'AV51WWPContext',fld:'vWWPCONTEXT_MPAGE',pic:'',nv:null},{av:'Screendetector_Screenheight',ctrl:'SCREENDETECTOR_MPAGE',prop:'ScreenHeight'}],oparms:[{av:'AV51WWPContext',fld:'vWWPCONTEXT_MPAGE',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Contentholder = new GXDataAreaControl();
         AV51WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         Screendetector_Screenwidth = "";
         Screendetector_Screenheight = "";
         AV40TabsMenuData = new GxObjectCollection( context, "TabsMenuData.TabsMenuDataItem", "GxEv3Up14_MeetrikaVs3", "SdtTabsMenuData_TabsMenuDataItem", "GeneXus.Programs");
         A1639HelpSistema_Objeto = "";
         A6AreaTrabalho_Descricao = "";
         A9Contratante_RazaoSocial = "";
         A10Contratante_NomeFantasia = "";
         A31Contratante_Telefone = "";
         A642AreaTrabalho_CalculoPFinal = "";
         A64ContratanteUsuario_ContratanteRaz = "";
         A65ContratanteUsuario_ContratanteFan = "";
         A2208Contratante_Sigla = "";
         A68ContratadaUsuario_ContratadaPessoaNom = "";
         A438Contratada_Sigla = "";
         A1952ParametrosSistema_URLOtherVer = "";
         A1954ParametrosSistema_Validacao = "";
         A484ContagemResultado_StatusDmn = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         AV9Contratadas = new GxSimpleCollection();
         AV5ServerDate = DateTime.MinValue;
         A4Perfil_Nome = "";
         A1647Usuario_Email = "";
         A58Usuario_PessoaNom = "";
         AV73EndidadeUsuario = "";
         AV47Vermelhas = new GxSimpleCollection();
         AV38SDT_FiltroConsContadorFM = new SdtSDT_FiltroConsContadorFM(context);
         AV52Laranjas = new GxSimpleCollection();
         AV53Amarelas = new GxSimpleCollection();
         AV54Verdes = new GxSimpleCollection();
         A341Usuario_UserGamGuid = "";
         GXKey = "";
         sPrefix = "";
         lblControhelp_Jsonclick = "";
         lblTextblock6_Jsonclick = "";
         lblTxbnotificacao_Jsonclick = "";
         lblTxbusuario_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         WebComp_Wcrecentlinks_Component = "";
         OldWcrecentlinks = "";
         TempTags = "";
         AV69HelpSistema_Objeto = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GX_FocusControl = "";
         AV55SimularUsuario = "";
         scmdbuf = "";
         H00MO2_A57Usuario_PessoaCod = new int[1] ;
         H00MO2_A54Usuario_Ativo = new bool[] {false} ;
         H00MO2_n54Usuario_Ativo = new bool[] {false} ;
         H00MO2_A1Usuario_Codigo = new int[1] ;
         H00MO2_A58Usuario_PessoaNom = new String[] {""} ;
         H00MO2_n58Usuario_PessoaNom = new bool[] {false} ;
         H00MO3_A29Contratante_Codigo = new int[1] ;
         H00MO3_n29Contratante_Codigo = new bool[] {false} ;
         H00MO3_A1822Contratante_UsaOSistema = new bool[] {false} ;
         H00MO3_n1822Contratante_UsaOSistema = new bool[] {false} ;
         H00MO3_A5AreaTrabalho_Codigo = new int[1] ;
         H00MO4_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00MO4_A1080ContratoGestor_UsuarioPesCod = new int[1] ;
         H00MO4_n1080ContratoGestor_UsuarioPesCod = new bool[] {false} ;
         H00MO4_A2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         H00MO4_n2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         H00MO4_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H00MO4_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         H00MO4_A1081ContratoGestor_UsuarioPesNom = new String[] {""} ;
         H00MO4_n1081ContratoGestor_UsuarioPesNom = new bool[] {false} ;
         H00MO4_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00MO4_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00MO4_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         A1081ContratoGestor_UsuarioPesNom = "";
         AV79Simularusuario_GXI = "";
         H00MO5_A1638HelpSistema_Codigo = new int[1] ;
         H00MO5_A1639HelpSistema_Objeto = new String[] {""} ;
         AV15Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV39Session = context.GetSession();
         AV49WebSession = context.GetSession();
         H00MO6_A330ParametrosSistema_Codigo = new int[1] ;
         H00MO6_A1952ParametrosSistema_URLOtherVer = new String[] {""} ;
         H00MO6_n1952ParametrosSistema_URLOtherVer = new bool[] {false} ;
         H00MO6_A1954ParametrosSistema_Validacao = new String[] {""} ;
         H00MO6_n1954ParametrosSistema_Validacao = new bool[] {false} ;
         AV63ParametrosSistema_URLOtherVer = "";
         AV64ParametrosSistema_Validacao = "";
         AV36Request = new GxHttpRequest( context);
         AV42URL = "";
         AV45Usuario_EhGestor = false;
         H00MO7_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00MO7_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00MO9_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00MO9_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00MO9_A602ContagemResultado_OSVinculada = new int[1] ;
         H00MO9_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00MO9_A456ContagemResultado_Codigo = new int[1] ;
         H00MO9_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00MO9_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00MO9_A890ContagemResultado_Responsavel = new int[1] ;
         H00MO9_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00MO9_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00MO9_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00MO9_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00MO9_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00MO9_A531ContagemResultado_StatusUltCnt = new short[1] ;
         H00MO9_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         H00MO11_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00MO11_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00MO11_A602ContagemResultado_OSVinculada = new int[1] ;
         H00MO11_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00MO11_A456ContagemResultado_Codigo = new int[1] ;
         H00MO11_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00MO11_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00MO11_A890ContagemResultado_Responsavel = new int[1] ;
         H00MO11_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00MO11_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00MO11_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00MO11_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00MO11_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00MO11_A531ContagemResultado_StatusUltCnt = new short[1] ;
         H00MO11_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         H00MO13_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00MO13_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00MO13_A602ContagemResultado_OSVinculada = new int[1] ;
         H00MO13_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00MO13_A456ContagemResultado_Codigo = new int[1] ;
         H00MO13_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00MO13_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00MO13_A890ContagemResultado_Responsavel = new int[1] ;
         H00MO13_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00MO13_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00MO13_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00MO13_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00MO13_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00MO13_A531ContagemResultado_StatusUltCnt = new short[1] ;
         H00MO13_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         H00MO14_A890ContagemResultado_Responsavel = new int[1] ;
         H00MO14_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00MO14_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00MO14_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00MO14_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00MO14_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00MO14_A456ContagemResultado_Codigo = new int[1] ;
         H00MO15_A289Usuario_EhContador = new bool[] {false} ;
         H00MO15_A293Usuario_EhFinanceiro = new bool[] {false} ;
         H00MO15_A341Usuario_UserGamGuid = new String[] {""} ;
         H00MO15_A2016Usuario_UltimaArea = new int[1] ;
         H00MO15_n2016Usuario_UltimaArea = new bool[] {false} ;
         H00MO15_A1Usuario_Codigo = new int[1] ;
         AV46Usuario_UserGamGuid = "";
         AV17GAMUser = new SdtGAMUser(context);
         AV14EntidadesDoUsuario = new GxSimpleCollection();
         AV6Areas_Codigo = new GxSimpleCollection();
         H00MO17_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00MO17_A54Usuario_Ativo = new bool[] {false} ;
         H00MO17_n54Usuario_Ativo = new bool[] {false} ;
         H00MO17_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00MO17_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         H00MO17_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00MO18_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00MO18_A43Contratada_Ativo = new bool[] {false} ;
         H00MO18_n43Contratada_Ativo = new bool[] {false} ;
         H00MO18_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00MO18_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00MO18_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00MO18_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00MO18_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00MO19_A72AreaTrabalho_Ativo = new bool[] {false} ;
         H00MO19_A5AreaTrabalho_Codigo = new int[1] ;
         H00MO19_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00MO20_A5AreaTrabalho_Codigo = new int[1] ;
         H00MO20_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00MO21_A5AreaTrabalho_Codigo = new int[1] ;
         H00MO21_A6AreaTrabalho_Descricao = new String[] {""} ;
         AV93Udparg1 = new GxSimpleCollection();
         H00MO22_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00MO22_A39Contratada_Codigo = new int[1] ;
         H00MO22_n39Contratada_Codigo = new bool[] {false} ;
         H00MO23_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00MO23_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         AV11Contratante_RazaoSocial = "";
         AV25MenuAcessoRapido = new GxObjectCollection( context, "SDT_MenuAcessoRapido.SDT_MenuAcessoRapidoItem", "GxEv3Up14_MeetrikaVs3", "SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem", "GeneXus.Programs");
         GXt_objcol_SdtTabsMenuData_TabsMenuDataItem5 = new GxObjectCollection( context, "TabsMenuData.TabsMenuDataItem", "GxEv3Up14_MeetrikaVs3", "SdtTabsMenuData_TabsMenuDataItem", "GeneXus.Programs");
         GXt_objcol_int4 = new GxSimpleCollection();
         H00MO24_A335Contratante_PessoaCod = new int[1] ;
         H00MO24_A5AreaTrabalho_Codigo = new int[1] ;
         H00MO24_A29Contratante_Codigo = new int[1] ;
         H00MO24_n29Contratante_Codigo = new bool[] {false} ;
         H00MO24_A72AreaTrabalho_Ativo = new bool[] {false} ;
         H00MO24_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00MO24_A9Contratante_RazaoSocial = new String[] {""} ;
         H00MO24_n9Contratante_RazaoSocial = new bool[] {false} ;
         H00MO24_A10Contratante_NomeFantasia = new String[] {""} ;
         H00MO24_A31Contratante_Telefone = new String[] {""} ;
         H00MO24_n31Contratante_Telefone = new bool[] {false} ;
         H00MO24_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         H00MO24_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         H00MO24_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         AV8AreaTrabalho_Descricao = "";
         H00MO25_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         H00MO25_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         H00MO25_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00MO25_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00MO25_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         H00MO25_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         H00MO25_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         H00MO25_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         H00MO25_A31Contratante_Telefone = new String[] {""} ;
         H00MO25_n31Contratante_Telefone = new bool[] {false} ;
         H00MO25_A2208Contratante_Sigla = new String[] {""} ;
         H00MO25_n2208Contratante_Sigla = new bool[] {false} ;
         H00MO26_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00MO26_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00MO26_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00MO26_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00MO26_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         H00MO26_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         H00MO26_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         H00MO26_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         H00MO26_A438Contratada_Sigla = new String[] {""} ;
         H00MO26_n438Contratada_Sigla = new bool[] {false} ;
         H00MO27_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00MO27_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00MO27_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H00MO27_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         H00MO28_A160ContratoServicos_Codigo = new int[1] ;
         H00MO28_A74Contrato_Codigo = new int[1] ;
         H00MO28_A155Servico_Codigo = new int[1] ;
         H00MO29_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         H00MO29_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         H00MO29_A39Contratada_Codigo = new int[1] ;
         H00MO29_n39Contratada_Codigo = new bool[] {false} ;
         H00MO30_A57Usuario_PessoaCod = new int[1] ;
         H00MO30_A3Perfil_Codigo = new int[1] ;
         H00MO30_A1Usuario_Codigo = new int[1] ;
         H00MO30_A4Perfil_Nome = new String[] {""} ;
         H00MO30_A1647Usuario_Email = new String[] {""} ;
         H00MO30_n1647Usuario_Email = new bool[] {false} ;
         H00MO30_A58Usuario_PessoaNom = new String[] {""} ;
         H00MO30_n58Usuario_PessoaNom = new bool[] {false} ;
         H00MO30_A544UsuarioPerfil_Insert = new bool[] {false} ;
         H00MO30_n544UsuarioPerfil_Insert = new bool[] {false} ;
         H00MO30_A659UsuarioPerfil_Update = new bool[] {false} ;
         H00MO30_n659UsuarioPerfil_Update = new bool[] {false} ;
         H00MO30_A546UsuarioPerfil_Delete = new bool[] {false} ;
         H00MO30_n546UsuarioPerfil_Delete = new bool[] {false} ;
         H00MO30_A543UsuarioPerfil_Display = new bool[] {false} ;
         H00MO30_n543UsuarioPerfil_Display = new bool[] {false} ;
         GXt_char6 = "";
         AV65Codigos = new GxSimpleCollection();
         sStyleString = "";
         lblLblusuario_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTbjava_Jsonclick = "";
         lblLblusuario2_Jsonclick = "";
         lblTxbusernamer_Jsonclick = "";
         lblLblemail_Jsonclick = "";
         lblTxbemail_Jsonclick = "";
         lblLblperfil_Jsonclick = "";
         lblTxbperfil_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         lblTextblockiniciais_Jsonclick = "";
         imgImage1_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblLblareatrabalho_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         lblLbluser_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sDynURL = "";
         Form = new GXWebForm();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.masterpagemeetrika__default(),
            new Object[][] {
                new Object[] {
               H00MO2_A57Usuario_PessoaCod, H00MO2_A54Usuario_Ativo, H00MO2_A1Usuario_Codigo, H00MO2_A58Usuario_PessoaNom, H00MO2_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00MO3_A29Contratante_Codigo, H00MO3_n29Contratante_Codigo, H00MO3_A1822Contratante_UsaOSistema, H00MO3_n1822Contratante_UsaOSistema, H00MO3_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               H00MO4_A1078ContratoGestor_ContratoCod, H00MO4_A1080ContratoGestor_UsuarioPesCod, H00MO4_n1080ContratoGestor_UsuarioPesCod, H00MO4_A2033ContratoGestor_UsuarioAtv, H00MO4_n2033ContratoGestor_UsuarioAtv, H00MO4_A1136ContratoGestor_ContratadaCod, H00MO4_n1136ContratoGestor_ContratadaCod, H00MO4_A1081ContratoGestor_UsuarioPesNom, H00MO4_n1081ContratoGestor_UsuarioPesNom, H00MO4_A1079ContratoGestor_UsuarioCod,
               H00MO4_A1446ContratoGestor_ContratadaAreaCod, H00MO4_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00MO5_A1638HelpSistema_Codigo, H00MO5_A1639HelpSistema_Objeto
               }
               , new Object[] {
               H00MO6_A330ParametrosSistema_Codigo, H00MO6_A1952ParametrosSistema_URLOtherVer, H00MO6_n1952ParametrosSistema_URLOtherVer, H00MO6_A1954ParametrosSistema_Validacao, H00MO6_n1954ParametrosSistema_Validacao
               }
               , new Object[] {
               H00MO7_A1078ContratoGestor_ContratoCod, H00MO7_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               H00MO9_A499ContagemResultado_ContratadaPessoaCod, H00MO9_n499ContagemResultado_ContratadaPessoaCod, H00MO9_A602ContagemResultado_OSVinculada, H00MO9_n602ContagemResultado_OSVinculada, H00MO9_A456ContagemResultado_Codigo, H00MO9_A472ContagemResultado_DataEntrega, H00MO9_n472ContagemResultado_DataEntrega, H00MO9_A890ContagemResultado_Responsavel, H00MO9_n890ContagemResultado_Responsavel, H00MO9_A484ContagemResultado_StatusDmn,
               H00MO9_n484ContagemResultado_StatusDmn, H00MO9_A490ContagemResultado_ContratadaCod, H00MO9_n490ContagemResultado_ContratadaCod, H00MO9_A531ContagemResultado_StatusUltCnt, H00MO9_n531ContagemResultado_StatusUltCnt
               }
               , new Object[] {
               H00MO11_A499ContagemResultado_ContratadaPessoaCod, H00MO11_n499ContagemResultado_ContratadaPessoaCod, H00MO11_A602ContagemResultado_OSVinculada, H00MO11_n602ContagemResultado_OSVinculada, H00MO11_A456ContagemResultado_Codigo, H00MO11_A472ContagemResultado_DataEntrega, H00MO11_n472ContagemResultado_DataEntrega, H00MO11_A890ContagemResultado_Responsavel, H00MO11_n890ContagemResultado_Responsavel, H00MO11_A484ContagemResultado_StatusDmn,
               H00MO11_n484ContagemResultado_StatusDmn, H00MO11_A490ContagemResultado_ContratadaCod, H00MO11_n490ContagemResultado_ContratadaCod, H00MO11_A531ContagemResultado_StatusUltCnt, H00MO11_n531ContagemResultado_StatusUltCnt
               }
               , new Object[] {
               H00MO13_A499ContagemResultado_ContratadaPessoaCod, H00MO13_n499ContagemResultado_ContratadaPessoaCod, H00MO13_A602ContagemResultado_OSVinculada, H00MO13_n602ContagemResultado_OSVinculada, H00MO13_A456ContagemResultado_Codigo, H00MO13_A472ContagemResultado_DataEntrega, H00MO13_n472ContagemResultado_DataEntrega, H00MO13_A890ContagemResultado_Responsavel, H00MO13_n890ContagemResultado_Responsavel, H00MO13_A484ContagemResultado_StatusDmn,
               H00MO13_n484ContagemResultado_StatusDmn, H00MO13_A490ContagemResultado_ContratadaCod, H00MO13_n490ContagemResultado_ContratadaCod, H00MO13_A531ContagemResultado_StatusUltCnt, H00MO13_n531ContagemResultado_StatusUltCnt
               }
               , new Object[] {
               H00MO14_A890ContagemResultado_Responsavel, H00MO14_n890ContagemResultado_Responsavel, H00MO14_A484ContagemResultado_StatusDmn, H00MO14_n484ContagemResultado_StatusDmn, H00MO14_A490ContagemResultado_ContratadaCod, H00MO14_n490ContagemResultado_ContratadaCod, H00MO14_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00MO15_A289Usuario_EhContador, H00MO15_A293Usuario_EhFinanceiro, H00MO15_A341Usuario_UserGamGuid, H00MO15_A2016Usuario_UltimaArea, H00MO15_n2016Usuario_UltimaArea, H00MO15_A1Usuario_Codigo
               }
               , new Object[] {
               H00MO17_A63ContratanteUsuario_ContratanteCod, H00MO17_A54Usuario_Ativo, H00MO17_n54Usuario_Ativo, H00MO17_A60ContratanteUsuario_UsuarioCod, H00MO17_A1020ContratanteUsuario_AreaTrabalhoCod, H00MO17_n1020ContratanteUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               H00MO18_A66ContratadaUsuario_ContratadaCod, H00MO18_A43Contratada_Ativo, H00MO18_n43Contratada_Ativo, H00MO18_A1394ContratadaUsuario_UsuarioAtivo, H00MO18_n1394ContratadaUsuario_UsuarioAtivo, H00MO18_A69ContratadaUsuario_UsuarioCod, H00MO18_A1228ContratadaUsuario_AreaTrabalhoCod, H00MO18_n1228ContratadaUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               H00MO19_A72AreaTrabalho_Ativo, H00MO19_A5AreaTrabalho_Codigo, H00MO19_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00MO20_A5AreaTrabalho_Codigo, H00MO20_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00MO21_A5AreaTrabalho_Codigo, H00MO21_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00MO22_A52Contratada_AreaTrabalhoCod, H00MO22_A39Contratada_Codigo
               }
               , new Object[] {
               H00MO23_A69ContratadaUsuario_UsuarioCod, H00MO23_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H00MO24_A335Contratante_PessoaCod, H00MO24_A5AreaTrabalho_Codigo, H00MO24_A29Contratante_Codigo, H00MO24_n29Contratante_Codigo, H00MO24_A72AreaTrabalho_Ativo, H00MO24_A6AreaTrabalho_Descricao, H00MO24_A9Contratante_RazaoSocial, H00MO24_n9Contratante_RazaoSocial, H00MO24_A10Contratante_NomeFantasia, H00MO24_A31Contratante_Telefone,
               H00MO24_A830AreaTrabalho_ServicoPadrao, H00MO24_n830AreaTrabalho_ServicoPadrao, H00MO24_A642AreaTrabalho_CalculoPFinal
               }
               , new Object[] {
               H00MO25_A340ContratanteUsuario_ContratantePesCod, H00MO25_n340ContratanteUsuario_ContratantePesCod, H00MO25_A60ContratanteUsuario_UsuarioCod, H00MO25_A63ContratanteUsuario_ContratanteCod, H00MO25_A64ContratanteUsuario_ContratanteRaz, H00MO25_n64ContratanteUsuario_ContratanteRaz, H00MO25_A65ContratanteUsuario_ContratanteFan, H00MO25_n65ContratanteUsuario_ContratanteFan, H00MO25_A31Contratante_Telefone, H00MO25_n31Contratante_Telefone,
               H00MO25_A2208Contratante_Sigla, H00MO25_n2208Contratante_Sigla
               }
               , new Object[] {
               H00MO26_A69ContratadaUsuario_UsuarioCod, H00MO26_A1228ContratadaUsuario_AreaTrabalhoCod, H00MO26_n1228ContratadaUsuario_AreaTrabalhoCod, H00MO26_A66ContratadaUsuario_ContratadaCod, H00MO26_A67ContratadaUsuario_ContratadaPessoaCod, H00MO26_n67ContratadaUsuario_ContratadaPessoaCod, H00MO26_A68ContratadaUsuario_ContratadaPessoaNom, H00MO26_n68ContratadaUsuario_ContratadaPessoaNom, H00MO26_A438Contratada_Sigla, H00MO26_n438Contratada_Sigla
               }
               , new Object[] {
               H00MO27_A1078ContratoGestor_ContratoCod, H00MO27_A1079ContratoGestor_UsuarioCod, H00MO27_A1136ContratoGestor_ContratadaCod, H00MO27_n1136ContratoGestor_ContratadaCod
               }
               , new Object[] {
               H00MO28_A160ContratoServicos_Codigo, H00MO28_A74Contrato_Codigo, H00MO28_A155Servico_Codigo
               }
               , new Object[] {
               H00MO29_A1824ContratoAuxiliar_ContratoCod, H00MO29_A1825ContratoAuxiliar_UsuarioCod, H00MO29_A39Contratada_Codigo, H00MO29_n39Contratada_Codigo
               }
               , new Object[] {
               H00MO30_A57Usuario_PessoaCod, H00MO30_A3Perfil_Codigo, H00MO30_A1Usuario_Codigo, H00MO30_A4Perfil_Nome, H00MO30_A1647Usuario_Email, H00MO30_n1647Usuario_Email, H00MO30_A58Usuario_PessoaNom, H00MO30_n58Usuario_PessoaNom, H00MO30_A544UsuarioPerfil_Insert, H00MO30_n544UsuarioPerfil_Insert,
               H00MO30_A659UsuarioPerfil_Update, H00MO30_n659UsuarioPerfil_Update, H00MO30_A546UsuarioPerfil_Delete, H00MO30_n546UsuarioPerfil_Delete, H00MO30_A543UsuarioPerfil_Display, H00MO30_n543UsuarioPerfil_Display
               }
            }
         );
         WebComp_Wcrecentlinks = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavQtderro_Enabled = 0;
         edtavQtdlrnj_Enabled = 0;
         edtavQtdpend_Enabled = 0;
         edtavQtdsolicitadas_Enabled = 0;
      }

      private short nRcdExists_13 ;
      private short nIsMod_13 ;
      private short initialized ;
      private short GxWebError ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV32QtdErro ;
      private short AV33QtdLrnj ;
      private short AV34QtdPend ;
      private short AV35QtdSolicitadas ;
      private short AV80GXLvl102 ;
      private short AV29Ok ;
      private short AV51WWPContext_gxTpr_Userid ;
      private short AV90GXLvl383 ;
      private short AV91GXLvl390 ;
      private short AV92GXLvl402 ;
      private short AV97GXLvl520 ;
      private short AV99GXLvl552 ;
      private short nGotPars ;
      private short nGXWrapped ;
      private int edtavQtderro_Enabled ;
      private int edtavQtdlrnj_Enabled ;
      private int edtavQtdpend_Enabled ;
      private int edtavQtdsolicitadas_Enabled ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private int A830AreaTrabalho_ServicoPadrao ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int AV10Contratante_Codigo ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A67ContratadaUsuario_ContratadaPessoaCod ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A74Contrato_Codigo ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A155Servico_Codigo ;
      private int A39Contratada_Codigo ;
      private int A1825ContratoAuxiliar_UsuarioCod ;
      private int A330ParametrosSistema_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A890ContagemResultado_Responsavel ;
      private int A456ContagemResultado_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int A499ContagemResultado_ContratadaPessoaCod ;
      private int A1020ContratanteUsuario_AreaTrabalhoCod ;
      private int AV20LastUserAreaTrabalho ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A2016Usuario_UltimaArea ;
      private int A1Usuario_Codigo ;
      private int divNotificacoes_Visible ;
      private int edtavHelpsistema_objeto_Visible ;
      private int AV27NewUsuario_Codigo ;
      private int AV7AreaTrabalho_Codigo ;
      private int GXt_int1 ;
      private int lblLblusuario_Visible ;
      private int imgavSimularusuario_Visible ;
      private int lblTbjava_Visible ;
      private int A57Usuario_PessoaCod ;
      private int A1080ContratoGestor_UsuarioPesCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int GXt_int2 ;
      private int AV51WWPContext_gxTpr_Contratada_codigo ;
      private int AV51WWPContext_gxTpr_Contratada_pessoacod ;
      private int AV44Usuario_Codigo ;
      private int A335Contratante_PessoaCod ;
      private int A340ContratanteUsuario_ContratantePesCod ;
      private int A1824ContratoAuxiliar_ContratoCod ;
      private int A3Perfil_Codigo ;
      private int tblTable14_Visible ;
      private int tblTable15_Visible ;
      private int AV103GXV1 ;
      private int AV66Codigo ;
      private int idxLst ;
      private String Screendetector_Screenwidth ;
      private String Screendetector_Screenheight ;
      private String edtavQtderro_Internalname ;
      private String edtavQtdlrnj_Internalname ;
      private String edtavQtdpend_Internalname ;
      private String edtavQtdsolicitadas_Internalname ;
      private String A9Contratante_RazaoSocial ;
      private String A10Contratante_NomeFantasia ;
      private String A31Contratante_Telefone ;
      private String A642AreaTrabalho_CalculoPFinal ;
      private String A64ContratanteUsuario_ContratanteRaz ;
      private String A65ContratanteUsuario_ContratanteFan ;
      private String A2208Contratante_Sigla ;
      private String A68ContratadaUsuario_ContratadaPessoaNom ;
      private String A438Contratada_Sigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String A4Perfil_Nome ;
      private String A58Usuario_PessoaNom ;
      private String AV73EndidadeUsuario ;
      private String A341Usuario_UserGamGuid ;
      private String Dropdowntabsmenu1_Height ;
      private String Dropdowntabsmenu1_Menustyle ;
      private String Confirmpanel_Width ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Confirmationtext ;
      private String Confirmpanel_Yesbuttoncaption ;
      private String Confirmpanel_Nobuttoncaption ;
      private String Confirmpanel_Cancelbuttoncaption ;
      private String Confirmpanel_Yesbuttonposition ;
      private String Confirmpanel_Confirmtype ;
      private String Confirmpanel_Texttype ;
      private String GXKey ;
      private String sPrefix ;
      private String divSection1_Internalname ;
      private String divSection2_Internalname ;
      private String divSection3_Internalname ;
      private String divSection4_Internalname ;
      private String divSection5_Internalname ;
      private String divSection6_Internalname ;
      private String divSection7_Internalname ;
      private String divSection8_Internalname ;
      private String divSection9_Internalname ;
      private String divSection13_Internalname ;
      private String lblControhelp_Internalname ;
      private String lblControhelp_Jsonclick ;
      private String divSection17_Internalname ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String divSection12_Internalname ;
      private String divNotificacoes_Internalname ;
      private String lblTxbnotificacao_Internalname ;
      private String lblTxbnotificacao_Jsonclick ;
      private String divSection11_Internalname ;
      private String lblTxbusuario_Internalname ;
      private String lblTxbusuario_Jsonclick ;
      private String divUsuario_Internalname ;
      private String divSection10_Internalname ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String divSection14_Internalname ;
      private String divSection23_Internalname ;
      private String divSection15_Internalname ;
      private String divSection19_Internalname ;
      private String divSection20_Internalname ;
      private String WebComp_Wcrecentlinks_Component ;
      private String OldWcrecentlinks ;
      private String TempTags ;
      private String edtavHelpsistema_objeto_Internalname ;
      private String edtavHelpsistema_objeto_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GX_FocusControl ;
      private String cmbavNewusuario_codigo_Internalname ;
      private String cmbavAreatrabalho_codigo_Internalname ;
      private String imgavSimularusuario_Internalname ;
      private String lblLblareatrabalho_Caption ;
      private String lblLblareatrabalho_Internalname ;
      private String lblLblusuario_Internalname ;
      private String lblTbjava_Internalname ;
      private String scmdbuf ;
      private String A1081ContratoGestor_UsuarioPesNom ;
      private String imgavSimularusuario_Tooltiptext ;
      private String lblLblusuario_Caption ;
      private String Confirmpanel_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTxbusernamer_Caption ;
      private String lblTxbusernamer_Internalname ;
      private String edtavQtderro_Tooltiptext ;
      private String edtavQtdpend_Tooltiptext ;
      private String edtavQtdlrnj_Tooltiptext ;
      private String edtavQtdsolicitadas_Tooltiptext ;
      private String AV46Usuario_UserGamGuid ;
      private String AV11Contratante_RazaoSocial ;
      private String lblTxbperfil_Caption ;
      private String lblTxbperfil_Internalname ;
      private String lblTxbemail_Caption ;
      private String lblTxbemail_Internalname ;
      private String lblLbluser_Caption ;
      private String lblLbluser_Internalname ;
      private String tblTable14_Internalname ;
      private String tblTable15_Internalname ;
      private String lblTextblockiniciais_Caption ;
      private String GXt_char6 ;
      private String lblTextblockiniciais_Internalname ;
      private String sStyleString ;
      private String tblTable9_Internalname ;
      private String lblLblusuario_Jsonclick ;
      private String cmbavNewusuario_codigo_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String cmbavAreatrabalho_codigo_Jsonclick ;
      private String lblTbjava_Jsonclick ;
      private String lblLblusuario2_Internalname ;
      private String lblLblusuario2_Jsonclick ;
      private String lblTxbusernamer_Jsonclick ;
      private String lblLblemail_Internalname ;
      private String lblLblemail_Jsonclick ;
      private String lblTxbemail_Jsonclick ;
      private String lblLblperfil_Internalname ;
      private String lblLblperfil_Jsonclick ;
      private String lblTxbperfil_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String imgavSimularusuario_Jsonclick ;
      private String lblTextblockiniciais_Jsonclick ;
      private String tblTblwarnings_Internalname ;
      private String edtavQtderro_Jsonclick ;
      private String edtavQtdlrnj_Jsonclick ;
      private String edtavQtdpend_Jsonclick ;
      private String edtavQtdsolicitadas_Jsonclick ;
      private String cellAtualizar_Internalname ;
      private String imgImage1_Internalname ;
      private String imgImage1_Jsonclick ;
      private String tblTable1_Internalname ;
      private String imgImagehome_Internalname ;
      private String tblTable2_Internalname ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String lblLblareatrabalho_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String lblLbluser_Jsonclick ;
      private String sDynURL ;
      private String div_Internalname ;
      private String Dropdowntabsmenu1_Internalname ;
      private String Screendetector_Internalname ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime AV5ServerDate ;
      private bool A72AreaTrabalho_Ativo ;
      private bool A54Usuario_Ativo ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool A43Contratada_Ativo ;
      private bool A544UsuarioPerfil_Insert ;
      private bool A659UsuarioPerfil_Update ;
      private bool A546UsuarioPerfil_Delete ;
      private bool A543UsuarioPerfil_Display ;
      private bool A289Usuario_EhContador ;
      private bool A293Usuario_EhFinanceiro ;
      private bool A290Usuario_EhAuditorFM ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool toggleJsOutput ;
      private bool returnInSub ;
      private bool n54Usuario_Ativo ;
      private bool n58Usuario_PessoaNom ;
      private bool n29Contratante_Codigo ;
      private bool A1822Contratante_UsaOSistema ;
      private bool n1822Contratante_UsaOSistema ;
      private bool n1080ContratoGestor_UsuarioPesCod ;
      private bool A2033ContratoGestor_UsuarioAtv ;
      private bool n2033ContratoGestor_UsuarioAtv ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool n1081ContratoGestor_UsuarioPesNom ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool n1952ParametrosSistema_URLOtherVer ;
      private bool n1954ParametrosSistema_Validacao ;
      private bool AV45Usuario_EhGestor ;
      private bool n499ContagemResultado_ContratadaPessoaCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n531ContagemResultado_StatusUltCnt ;
      private bool n2016Usuario_UltimaArea ;
      private bool GXt_boolean3 ;
      private bool n1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool n43Contratada_Ativo ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n39Contratada_Codigo ;
      private bool n9Contratante_RazaoSocial ;
      private bool n31Contratante_Telefone ;
      private bool n830AreaTrabalho_ServicoPadrao ;
      private bool n340ContratanteUsuario_ContratantePesCod ;
      private bool n64ContratanteUsuario_ContratanteRaz ;
      private bool n65ContratanteUsuario_ContratanteFan ;
      private bool n2208Contratante_Sigla ;
      private bool n67ContratadaUsuario_ContratadaPessoaCod ;
      private bool n68ContratadaUsuario_ContratadaPessoaNom ;
      private bool n438Contratada_Sigla ;
      private bool n1647Usuario_Email ;
      private bool n544UsuarioPerfil_Insert ;
      private bool n659UsuarioPerfil_Update ;
      private bool n546UsuarioPerfil_Delete ;
      private bool n543UsuarioPerfil_Display ;
      private bool AV55SimularUsuario_IsBlob ;
      private String A1639HelpSistema_Objeto ;
      private String A6AreaTrabalho_Descricao ;
      private String A1952ParametrosSistema_URLOtherVer ;
      private String A1954ParametrosSistema_Validacao ;
      private String A1647Usuario_Email ;
      private String AV69HelpSistema_Objeto ;
      private String AV79Simularusuario_GXI ;
      private String AV63ParametrosSistema_URLOtherVer ;
      private String AV64ParametrosSistema_Validacao ;
      private String AV42URL ;
      private String AV8AreaTrabalho_Descricao ;
      private String AV55SimularUsuario ;
      private GXWebComponent WebComp_Wcrecentlinks ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavNewusuario_codigo ;
      private GXCombobox cmbavAreatrabalho_codigo ;
      private GXDataAreaControl Contentholder ;
      private IDataStoreProvider pr_default ;
      private int[] H00MO2_A57Usuario_PessoaCod ;
      private bool[] H00MO2_A54Usuario_Ativo ;
      private bool[] H00MO2_n54Usuario_Ativo ;
      private int[] H00MO2_A1Usuario_Codigo ;
      private String[] H00MO2_A58Usuario_PessoaNom ;
      private bool[] H00MO2_n58Usuario_PessoaNom ;
      private int[] H00MO3_A29Contratante_Codigo ;
      private bool[] H00MO3_n29Contratante_Codigo ;
      private bool[] H00MO3_A1822Contratante_UsaOSistema ;
      private bool[] H00MO3_n1822Contratante_UsaOSistema ;
      private int[] H00MO3_A5AreaTrabalho_Codigo ;
      private int[] H00MO4_A1078ContratoGestor_ContratoCod ;
      private int[] H00MO4_A1080ContratoGestor_UsuarioPesCod ;
      private bool[] H00MO4_n1080ContratoGestor_UsuarioPesCod ;
      private bool[] H00MO4_A2033ContratoGestor_UsuarioAtv ;
      private bool[] H00MO4_n2033ContratoGestor_UsuarioAtv ;
      private int[] H00MO4_A1136ContratoGestor_ContratadaCod ;
      private bool[] H00MO4_n1136ContratoGestor_ContratadaCod ;
      private String[] H00MO4_A1081ContratoGestor_UsuarioPesNom ;
      private bool[] H00MO4_n1081ContratoGestor_UsuarioPesNom ;
      private int[] H00MO4_A1079ContratoGestor_UsuarioCod ;
      private int[] H00MO4_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00MO4_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00MO5_A1638HelpSistema_Codigo ;
      private String[] H00MO5_A1639HelpSistema_Objeto ;
      private int[] H00MO6_A330ParametrosSistema_Codigo ;
      private String[] H00MO6_A1952ParametrosSistema_URLOtherVer ;
      private bool[] H00MO6_n1952ParametrosSistema_URLOtherVer ;
      private String[] H00MO6_A1954ParametrosSistema_Validacao ;
      private bool[] H00MO6_n1954ParametrosSistema_Validacao ;
      private int[] H00MO7_A1078ContratoGestor_ContratoCod ;
      private int[] H00MO7_A1079ContratoGestor_UsuarioCod ;
      private int[] H00MO9_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00MO9_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] H00MO9_A602ContagemResultado_OSVinculada ;
      private bool[] H00MO9_n602ContagemResultado_OSVinculada ;
      private int[] H00MO9_A456ContagemResultado_Codigo ;
      private DateTime[] H00MO9_A472ContagemResultado_DataEntrega ;
      private bool[] H00MO9_n472ContagemResultado_DataEntrega ;
      private int[] H00MO9_A890ContagemResultado_Responsavel ;
      private bool[] H00MO9_n890ContagemResultado_Responsavel ;
      private String[] H00MO9_A484ContagemResultado_StatusDmn ;
      private bool[] H00MO9_n484ContagemResultado_StatusDmn ;
      private int[] H00MO9_A490ContagemResultado_ContratadaCod ;
      private bool[] H00MO9_n490ContagemResultado_ContratadaCod ;
      private short[] H00MO9_A531ContagemResultado_StatusUltCnt ;
      private bool[] H00MO9_n531ContagemResultado_StatusUltCnt ;
      private int[] H00MO11_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00MO11_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] H00MO11_A602ContagemResultado_OSVinculada ;
      private bool[] H00MO11_n602ContagemResultado_OSVinculada ;
      private int[] H00MO11_A456ContagemResultado_Codigo ;
      private DateTime[] H00MO11_A472ContagemResultado_DataEntrega ;
      private bool[] H00MO11_n472ContagemResultado_DataEntrega ;
      private int[] H00MO11_A890ContagemResultado_Responsavel ;
      private bool[] H00MO11_n890ContagemResultado_Responsavel ;
      private String[] H00MO11_A484ContagemResultado_StatusDmn ;
      private bool[] H00MO11_n484ContagemResultado_StatusDmn ;
      private int[] H00MO11_A490ContagemResultado_ContratadaCod ;
      private bool[] H00MO11_n490ContagemResultado_ContratadaCod ;
      private short[] H00MO11_A531ContagemResultado_StatusUltCnt ;
      private bool[] H00MO11_n531ContagemResultado_StatusUltCnt ;
      private int[] H00MO13_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00MO13_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] H00MO13_A602ContagemResultado_OSVinculada ;
      private bool[] H00MO13_n602ContagemResultado_OSVinculada ;
      private int[] H00MO13_A456ContagemResultado_Codigo ;
      private DateTime[] H00MO13_A472ContagemResultado_DataEntrega ;
      private bool[] H00MO13_n472ContagemResultado_DataEntrega ;
      private int[] H00MO13_A890ContagemResultado_Responsavel ;
      private bool[] H00MO13_n890ContagemResultado_Responsavel ;
      private String[] H00MO13_A484ContagemResultado_StatusDmn ;
      private bool[] H00MO13_n484ContagemResultado_StatusDmn ;
      private int[] H00MO13_A490ContagemResultado_ContratadaCod ;
      private bool[] H00MO13_n490ContagemResultado_ContratadaCod ;
      private short[] H00MO13_A531ContagemResultado_StatusUltCnt ;
      private bool[] H00MO13_n531ContagemResultado_StatusUltCnt ;
      private int[] H00MO14_A890ContagemResultado_Responsavel ;
      private bool[] H00MO14_n890ContagemResultado_Responsavel ;
      private String[] H00MO14_A484ContagemResultado_StatusDmn ;
      private bool[] H00MO14_n484ContagemResultado_StatusDmn ;
      private int[] H00MO14_A490ContagemResultado_ContratadaCod ;
      private bool[] H00MO14_n490ContagemResultado_ContratadaCod ;
      private int[] H00MO14_A456ContagemResultado_Codigo ;
      private bool[] H00MO15_A289Usuario_EhContador ;
      private bool[] H00MO15_A293Usuario_EhFinanceiro ;
      private String[] H00MO15_A341Usuario_UserGamGuid ;
      private int[] H00MO15_A2016Usuario_UltimaArea ;
      private bool[] H00MO15_n2016Usuario_UltimaArea ;
      private int[] H00MO15_A1Usuario_Codigo ;
      private int[] H00MO17_A63ContratanteUsuario_ContratanteCod ;
      private bool[] H00MO17_A54Usuario_Ativo ;
      private bool[] H00MO17_n54Usuario_Ativo ;
      private int[] H00MO17_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00MO17_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] H00MO17_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private int[] H00MO18_A66ContratadaUsuario_ContratadaCod ;
      private bool[] H00MO18_A43Contratada_Ativo ;
      private bool[] H00MO18_n43Contratada_Ativo ;
      private bool[] H00MO18_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00MO18_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] H00MO18_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00MO18_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00MO18_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00MO19_A72AreaTrabalho_Ativo ;
      private int[] H00MO19_A5AreaTrabalho_Codigo ;
      private String[] H00MO19_A6AreaTrabalho_Descricao ;
      private int[] H00MO20_A5AreaTrabalho_Codigo ;
      private String[] H00MO20_A6AreaTrabalho_Descricao ;
      private int[] H00MO21_A5AreaTrabalho_Codigo ;
      private String[] H00MO21_A6AreaTrabalho_Descricao ;
      private int[] H00MO22_A52Contratada_AreaTrabalhoCod ;
      private int[] H00MO22_A39Contratada_Codigo ;
      private bool[] H00MO22_n39Contratada_Codigo ;
      private int[] H00MO23_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00MO23_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00MO24_A335Contratante_PessoaCod ;
      private int[] H00MO24_A5AreaTrabalho_Codigo ;
      private int[] H00MO24_A29Contratante_Codigo ;
      private bool[] H00MO24_n29Contratante_Codigo ;
      private bool[] H00MO24_A72AreaTrabalho_Ativo ;
      private String[] H00MO24_A6AreaTrabalho_Descricao ;
      private String[] H00MO24_A9Contratante_RazaoSocial ;
      private bool[] H00MO24_n9Contratante_RazaoSocial ;
      private String[] H00MO24_A10Contratante_NomeFantasia ;
      private String[] H00MO24_A31Contratante_Telefone ;
      private bool[] H00MO24_n31Contratante_Telefone ;
      private int[] H00MO24_A830AreaTrabalho_ServicoPadrao ;
      private bool[] H00MO24_n830AreaTrabalho_ServicoPadrao ;
      private String[] H00MO24_A642AreaTrabalho_CalculoPFinal ;
      private int[] H00MO25_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] H00MO25_n340ContratanteUsuario_ContratantePesCod ;
      private int[] H00MO25_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00MO25_A63ContratanteUsuario_ContratanteCod ;
      private String[] H00MO25_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] H00MO25_n64ContratanteUsuario_ContratanteRaz ;
      private String[] H00MO25_A65ContratanteUsuario_ContratanteFan ;
      private bool[] H00MO25_n65ContratanteUsuario_ContratanteFan ;
      private String[] H00MO25_A31Contratante_Telefone ;
      private bool[] H00MO25_n31Contratante_Telefone ;
      private String[] H00MO25_A2208Contratante_Sigla ;
      private bool[] H00MO25_n2208Contratante_Sigla ;
      private int[] H00MO26_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00MO26_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00MO26_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] H00MO26_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00MO26_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] H00MO26_n67ContratadaUsuario_ContratadaPessoaCod ;
      private String[] H00MO26_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] H00MO26_n68ContratadaUsuario_ContratadaPessoaNom ;
      private String[] H00MO26_A438Contratada_Sigla ;
      private bool[] H00MO26_n438Contratada_Sigla ;
      private int[] H00MO27_A1078ContratoGestor_ContratoCod ;
      private int[] H00MO27_A1079ContratoGestor_UsuarioCod ;
      private int[] H00MO27_A1136ContratoGestor_ContratadaCod ;
      private bool[] H00MO27_n1136ContratoGestor_ContratadaCod ;
      private int[] H00MO28_A160ContratoServicos_Codigo ;
      private int[] H00MO28_A74Contrato_Codigo ;
      private int[] H00MO28_A155Servico_Codigo ;
      private int[] H00MO29_A1824ContratoAuxiliar_ContratoCod ;
      private int[] H00MO29_A1825ContratoAuxiliar_UsuarioCod ;
      private int[] H00MO29_A39Contratada_Codigo ;
      private bool[] H00MO29_n39Contratada_Codigo ;
      private int[] H00MO30_A57Usuario_PessoaCod ;
      private int[] H00MO30_A3Perfil_Codigo ;
      private int[] H00MO30_A1Usuario_Codigo ;
      private String[] H00MO30_A4Perfil_Nome ;
      private String[] H00MO30_A1647Usuario_Email ;
      private bool[] H00MO30_n1647Usuario_Email ;
      private String[] H00MO30_A58Usuario_PessoaNom ;
      private bool[] H00MO30_n58Usuario_PessoaNom ;
      private bool[] H00MO30_A544UsuarioPerfil_Insert ;
      private bool[] H00MO30_n544UsuarioPerfil_Insert ;
      private bool[] H00MO30_A659UsuarioPerfil_Update ;
      private bool[] H00MO30_n659UsuarioPerfil_Update ;
      private bool[] H00MO30_A546UsuarioPerfil_Delete ;
      private bool[] H00MO30_n546UsuarioPerfil_Delete ;
      private bool[] H00MO30_A543UsuarioPerfil_Display ;
      private bool[] H00MO30_n543UsuarioPerfil_Display ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV36Request ;
      private IGxSession AV39Session ;
      private IGxSession AV49WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV9Contratadas ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV47Vermelhas ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV52Laranjas ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV53Amarelas ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV54Verdes ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV14EntidadesDoUsuario ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV6Areas_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV93Udparg1 ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection GXt_objcol_int4 ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV65Codigos ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV15Errors ;
      [ObjectCollection(ItemType=typeof( SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem ))]
      private IGxCollection AV25MenuAcessoRapido ;
      [ObjectCollection(ItemType=typeof( SdtTabsMenuData_TabsMenuDataItem ))]
      private IGxCollection AV40TabsMenuData ;
      [ObjectCollection(ItemType=typeof( SdtTabsMenuData_TabsMenuDataItem ))]
      private IGxCollection GXt_objcol_SdtTabsMenuData_TabsMenuDataItem5 ;
      private GXWebForm Form ;
      private SdtGAMUser AV17GAMUser ;
      private SdtSDT_FiltroConsContadorFM AV38SDT_FiltroConsContadorFM ;
      private wwpbaseobjects.SdtWWPContext AV51WWPContext ;
   }

   public class masterpagemeetrika__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00MO9( IGxContext context ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             IGxCollection AV9Contratadas ,
                                             String A484ContagemResultado_StatusDmn ,
                                             DateTime A472ContagemResultado_DataEntrega ,
                                             DateTime AV5ServerDate ,
                                             int AV51WWPContext_gxTpr_Contratada_codigo ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             int A456ContagemResultado_Codigo ,
                                             int A602ContagemResultado_OSVinculada ,
                                             int A499ContagemResultado_ContratadaPessoaCod ,
                                             int AV51WWPContext_gxTpr_Contratada_pessoacod ,
                                             int A890ContagemResultado_Responsavel ,
                                             short AV51WWPContext_gxTpr_Userid )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [2] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T3.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, COALESCE( T2.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV9Contratadas, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] = 'E' or T1.[ContagemResultado_StatusDmn] = 'A' or T1.[ContagemResultado_StatusDmn] = 'D')";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_DataEntrega] < @AV5ServerDate)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Responsavel] = @AV51WWPContext__Userid)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_DataEntrega]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_H00MO11( IGxContext context ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              IGxCollection AV9Contratadas ,
                                              String A484ContagemResultado_StatusDmn ,
                                              int A890ContagemResultado_Responsavel ,
                                              short AV51WWPContext_gxTpr_Userid ,
                                              int AV51WWPContext_gxTpr_Contratada_codigo ,
                                              short A531ContagemResultado_StatusUltCnt ,
                                              int A456ContagemResultado_Codigo ,
                                              int A602ContagemResultado_OSVinculada ,
                                              int A499ContagemResultado_ContratadaPessoaCod ,
                                              int AV51WWPContext_gxTpr_Contratada_pessoacod ,
                                              DateTime A472ContagemResultado_DataEntrega ,
                                              DateTime AV5ServerDate )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [2] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T3.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, COALESCE( T2.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV9Contratadas, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] = 'S' or T1.[ContagemResultado_StatusDmn] = 'E' or T1.[ContagemResultado_StatusDmn] = 'A' or T1.[ContagemResultado_StatusDmn] = 'D')";
         scmdbuf = scmdbuf + " and ((T1.[ContagemResultado_Responsavel] = convert(int, 0)) or T1.[ContagemResultado_Responsavel] = @AV51WWPContext__Userid)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_DataEntrega] = @AV5ServerDate)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_DataEntrega]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      protected Object[] conditional_H00MO13( IGxContext context ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              IGxCollection AV9Contratadas ,
                                              String A484ContagemResultado_StatusDmn ,
                                              DateTime A472ContagemResultado_DataEntrega ,
                                              DateTime AV5ServerDate ,
                                              int AV51WWPContext_gxTpr_Contratada_codigo ,
                                              short A531ContagemResultado_StatusUltCnt ,
                                              int A456ContagemResultado_Codigo ,
                                              int A602ContagemResultado_OSVinculada ,
                                              int A499ContagemResultado_ContratadaPessoaCod ,
                                              int AV51WWPContext_gxTpr_Contratada_pessoacod ,
                                              int A890ContagemResultado_Responsavel ,
                                              short AV51WWPContext_gxTpr_Userid )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int11 ;
         GXv_int11 = new short [3] ;
         Object[] GXv_Object12 ;
         GXv_Object12 = new Object [2] ;
         scmdbuf = "SELECT T3.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, COALESCE( T2.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV9Contratadas, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] = 'E' or T1.[ContagemResultado_StatusDmn] = 'A' or T1.[ContagemResultado_StatusDmn] = 'D')";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_DataEntrega] > @AV5ServerDate)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_DataEntrega] < DATEADD( ss,86400 * ( 7), @AV5ServerDate))";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Responsavel] = @AV51WWPContext__Userid)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_DataEntrega]";
         GXv_Object12[0] = scmdbuf;
         GXv_Object12[1] = GXv_int11;
         return GXv_Object12 ;
      }

      protected Object[] conditional_H00MO14( IGxContext context ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              IGxCollection AV9Contratadas ,
                                              String A484ContagemResultado_StatusDmn ,
                                              int A890ContagemResultado_Responsavel ,
                                              short AV51WWPContext_gxTpr_Userid )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int13 ;
         GXv_int13 = new short [1] ;
         Object[] GXv_Object14 ;
         GXv_Object14 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_Responsavel], [ContagemResultado_StatusDmn], [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV9Contratadas, "[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and ([ContagemResultado_StatusDmn] = 'S')";
         scmdbuf = scmdbuf + " and ([ContagemResultado_Responsavel] = @AV51WWPContext__Userid)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_ContratadaCod], [ContagemResultado_StatusDmn], [ContagemResultado_Responsavel]";
         GXv_Object14[0] = scmdbuf;
         GXv_Object14[1] = GXv_int13;
         return GXv_Object14 ;
      }

      protected Object[] conditional_H00MO19( IGxContext context ,
                                              int A5AreaTrabalho_Codigo ,
                                              IGxCollection AV14EntidadesDoUsuario ,
                                              bool A72AreaTrabalho_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object15 ;
         GXv_Object15 = new Object [2] ;
         scmdbuf = "SELECT [AreaTrabalho_Ativo], [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV14EntidadesDoUsuario, "[AreaTrabalho_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and ([AreaTrabalho_Ativo] = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [AreaTrabalho_Descricao]";
         GXv_Object15[0] = scmdbuf;
         return GXv_Object15 ;
      }

      protected Object[] conditional_H00MO20( IGxContext context ,
                                              int A5AreaTrabalho_Codigo ,
                                              IGxCollection AV14EntidadesDoUsuario )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object17 ;
         GXv_Object17 = new Object [2] ;
         scmdbuf = "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV14EntidadesDoUsuario, "[AreaTrabalho_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [AreaTrabalho_Descricao]";
         GXv_Object17[0] = scmdbuf;
         return GXv_Object17 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 6 :
                     return conditional_H00MO9(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (int)dynConstraints[5] , (short)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (short)dynConstraints[12] );
               case 7 :
                     return conditional_H00MO11(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (short)dynConstraints[4] , (int)dynConstraints[5] , (short)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] );
               case 8 :
                     return conditional_H00MO13(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (int)dynConstraints[5] , (short)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (short)dynConstraints[12] );
               case 9 :
                     return conditional_H00MO14(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (short)dynConstraints[4] );
               case 13 :
                     return conditional_H00MO19(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] );
               case 14 :
                     return conditional_H00MO20(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00MO2 ;
          prmH00MO2 = new Object[] {
          } ;
          Object[] prmH00MO3 ;
          prmH00MO3 = new Object[] {
          new Object[] {"@AV51WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MO4 ;
          prmH00MO4 = new Object[] {
          new Object[] {"@AV51WWPC_2Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MO5 ;
          prmH00MO5 = new Object[] {
          new Object[] {"@AV69HelpSistema_Objeto",SqlDbType.VarChar,80,0}
          } ;
          Object[] prmH00MO6 ;
          prmH00MO6 = new Object[] {
          } ;
          Object[] prmH00MO7 ;
          prmH00MO7 = new Object[] {
          new Object[] {"@AV51WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00MO15 ;
          prmH00MO15 = new Object[] {
          new Object[] {"@AV44Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MO17 ;
          prmH00MO17 = new Object[] {
          new Object[] {"@AV51WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00MO18 ;
          prmH00MO18 = new Object[] {
          new Object[] {"@AV51WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00MO21 ;
          prmH00MO21 = new Object[] {
          } ;
          Object[] prmH00MO22 ;
          prmH00MO22 = new Object[] {
          new Object[] {"@AV51WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MO23 ;
          prmH00MO23 = new Object[] {
          new Object[] {"@AV51WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00MO24 ;
          prmH00MO24 = new Object[] {
          new Object[] {"@AV7AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MO25 ;
          prmH00MO25 = new Object[] {
          new Object[] {"@AV10Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV51WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00MO26 ;
          prmH00MO26 = new Object[] {
          new Object[] {"@AV51WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV7AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MO27 ;
          prmH00MO27 = new Object[] {
          new Object[] {"@AV51WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV51WWPC_2Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MO28 ;
          prmH00MO28 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MO29 ;
          prmH00MO29 = new Object[] {
          new Object[] {"@AV51WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV51WWPC_2Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MO30 ;
          prmH00MO30 = new Object[] {
          new Object[] {"@AV51WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00MO9 ;
          prmH00MO9 = new Object[] {
          new Object[] {"@AV5ServerDate",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV51WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00MO11 ;
          prmH00MO11 = new Object[] {
          new Object[] {"@AV51WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV5ServerDate",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmH00MO13 ;
          prmH00MO13 = new Object[] {
          new Object[] {"@AV5ServerDate",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV5ServerDate",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV51WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00MO14 ;
          prmH00MO14 = new Object[] {
          new Object[] {"@AV51WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00MO19 ;
          prmH00MO19 = new Object[] {
          } ;
          Object[] prmH00MO20 ;
          prmH00MO20 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00MO2", "SELECT T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Ativo], T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Ativo] = 1 ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO2,100,0,false,false )
             ,new CursorDef("H00MO3", "SELECT T1.[Contratante_Codigo], T2.[Contratante_UsaOSistema], T1.[AreaTrabalho_Codigo] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE (T1.[AreaTrabalho_Codigo] = @AV51WWPC_1Areatrabalho_codigo) AND (Not T2.[Contratante_UsaOSistema] = 1 or T2.[Contratante_UsaOSistema] IS NULL) ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO3,1,0,true,true )
             ,new CursorDef("H00MO4", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T3.[Usuario_PessoaCod] AS ContratoGestor_UsuarioPesCod, T3.[Usuario_Ativo] AS ContratoGestor_UsuarioAtv, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T4.[Pessoa_Nome] AS ContratoGestor_UsuarioPesNom, T1.[ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod, T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ((([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratoGestor_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE (T3.[Usuario_Ativo] = 1) AND (T2.[Contratada_Codigo] = @AV51WWPC_2Contratada_codigo) ORDER BY T1.[ContratoGestor_ContratoCod], T1.[ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO4,100,0,true,false )
             ,new CursorDef("H00MO5", "SELECT [HelpSistema_Codigo], [HelpSistema_Objeto] FROM [HelpSistema] WITH (NOLOCK) WHERE UPPER([HelpSistema_Objeto]) = UPPER(@AV69HelpSistema_Objeto) ORDER BY [HelpSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO5,100,0,false,false )
             ,new CursorDef("H00MO6", "SELECT TOP 1 [ParametrosSistema_Codigo], [ParametrosSistema_URLOtherVer], [ParametrosSistema_Validacao] FROM [ParametrosSistema] WITH (NOLOCK) WHERE [ParametrosSistema_Codigo] = 1 ORDER BY [ParametrosSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO6,1,0,false,true )
             ,new CursorDef("H00MO7", "SELECT TOP 1 [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, [ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_UsuarioCod] = @AV51WWPContext__Userid ORDER BY [ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO7,1,0,false,true )
             ,new CursorDef("H00MO9", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO9,100,0,true,false )
             ,new CursorDef("H00MO11", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO11,100,0,true,false )
             ,new CursorDef("H00MO13", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO13,100,0,true,false )
             ,new CursorDef("H00MO14", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO14,100,0,false,false )
             ,new CursorDef("H00MO15", "SELECT [Usuario_EhContador], [Usuario_EhFinanceiro], [Usuario_UserGamGuid], [Usuario_UltimaArea], [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV44Usuario_Codigo ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO15,1,0,true,true )
             ,new CursorDef("H00MO17", "SELECT T1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, T2.[Usuario_Ativo], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, COALESCE( T3.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM (([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN (SELECT MIN(T4.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T5.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, T5.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [AreaTrabalho] T4 WITH (NOLOCK),  [ContratanteUsuario] T5 WITH (NOLOCK) WHERE T4.[Contratante_Codigo] = T5.[ContratanteUsuario_ContratanteCod] GROUP BY T5.[ContratanteUsuario_ContratanteCod], T5.[ContratanteUsuario_UsuarioCod] ) T3 ON T3.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T3.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE (T1.[ContratanteUsuario_UsuarioCod] = @AV51WWPContext__Userid) AND (T2.[Usuario_Ativo] = 1) ORDER BY T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO17,100,0,false,false )
             ,new CursorDef("H00MO18", "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo, T2.[Contratada_Ativo], T3.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV51WWPContext__Userid) AND (T3.[Usuario_Ativo] = 1) AND (T2.[Contratada_Ativo] = 1) ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO18,100,0,false,false )
             ,new CursorDef("H00MO19", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO19,100,0,false,false )
             ,new CursorDef("H00MO20", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO20,100,0,false,false )
             ,new CursorDef("H00MO21", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO21,100,0,false,false )
             ,new CursorDef("H00MO22", "SELECT [Contratada_AreaTrabalhoCod], [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_AreaTrabalhoCod] = @AV51WWPC_1Areatrabalho_codigo ORDER BY [Contratada_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO22,100,0,false,false )
             ,new CursorDef("H00MO23", "SELECT [ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_UsuarioCod] = @AV51WWPContext__Userid ORDER BY [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO23,100,0,false,false )
             ,new CursorDef("H00MO24", "SELECT TOP 1 T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_Codigo], T1.[Contratante_Codigo], T1.[AreaTrabalho_Ativo], T1.[AreaTrabalho_Descricao], T3.[Pessoa_Nome] AS Contratante_RazaoSocial, T2.[Contratante_NomeFantasia], T2.[Contratante_Telefone], T1.[AreaTrabalho_ServicoPadrao], T1.[AreaTrabalho_CalculoPFinal] FROM (([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) WHERE T1.[AreaTrabalho_Codigo] = @AV7AreaTrabalho_Codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO24,1,0,false,true )
             ,new CursorDef("H00MO25", "SELECT TOP 1 T2.[Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, T3.[Pessoa_Nome] AS ContratanteUsuario_Contratante, T2.[Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, T2.[Contratante_Telefone], T2.[Contratante_Sigla] FROM (([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) WHERE T1.[ContratanteUsuario_ContratanteCod] = @AV10Contratante_Codigo and T1.[ContratanteUsuario_UsuarioCod] = @AV51WWPContext__Userid ORDER BY T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO25,1,0,false,true )
             ,new CursorDef("H00MO26", "SELECT TOP 1 T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPe, T3.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPe, T2.[Contratada_Sigla] FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV51WWPContext__Userid) AND (T2.[Contratada_AreaTrabalhoCod] = @AV7AreaTrabalho_Codigo) ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO26,1,0,false,true )
             ,new CursorDef("H00MO27", "SELECT TOP 1 T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE (T1.[ContratoGestor_UsuarioCod] = @AV51WWPContext__Userid) AND (T2.[Contratada_Codigo] = @AV51WWPC_2Contratada_codigo) ORDER BY T1.[ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO27,1,0,true,true )
             ,new CursorDef("H00MO28", "SELECT [ContratoServicos_Codigo], [Contrato_Codigo], [Servico_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoGestor_ContratoCod ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO28,100,0,false,false )
             ,new CursorDef("H00MO29", "SELECT TOP 1 T1.[ContratoAuxiliar_ContratoCod] AS ContratoAuxiliar_ContratoCod, T1.[ContratoAuxiliar_UsuarioCod], T2.[Contratada_Codigo] FROM ([ContratoAuxiliar] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoAuxiliar_ContratoCod]) WHERE (T1.[ContratoAuxiliar_UsuarioCod] = @AV51WWPContext__Userid) AND (T2.[Contratada_Codigo] = @AV51WWPC_2Contratada_codigo) ORDER BY T1.[ContratoAuxiliar_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO29,1,0,false,true )
             ,new CursorDef("H00MO30", "SELECT TOP 1 T3.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Perfil_Codigo], T1.[Usuario_Codigo], T2.[Perfil_Nome], T3.[Usuario_Email], T4.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[UsuarioPerfil_Insert], T1.[UsuarioPerfil_Update], T1.[UsuarioPerfil_Delete], T1.[UsuarioPerfil_Display] FROM ((([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T2 WITH (NOLOCK) ON T2.[Perfil_Codigo] = T1.[Perfil_Codigo]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[Usuario_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV51WWPContext__Userid ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MO30,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((short[]) buf[13])[0] = rslt.getShort(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((short[]) buf[13])[0] = rslt.getShort(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((short[]) buf[13])[0] = rslt.getShort(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
             case 10 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 40) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 13 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 100) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((String[]) buf[12])[0] = rslt.getString(10, 2) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 20) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((bool[]) buf[12])[0] = rslt.getBool(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[3]);
                }
                return;
             case 7 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[3]);
                }
                return;
             case 8 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[5]);
                }
                return;
             case 9 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 20 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 21 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 24 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
       }
    }

 }

}
