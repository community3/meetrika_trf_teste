/*
               File: GetWWContagemResultadoNotificacaoFilterData
        Description: Get WWContagem Resultado Notificacao Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:6:25.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontagemresultadonotificacaofilterdata : GXProcedure
   {
      public getwwcontagemresultadonotificacaofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontagemresultadonotificacaofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV33DDOName = aP0_DDOName;
         this.AV31SearchTxt = aP1_SearchTxt;
         this.AV32SearchTxtTo = aP2_SearchTxtTo;
         this.AV37OptionsJson = "" ;
         this.AV40OptionsDescJson = "" ;
         this.AV42OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV37OptionsJson;
         aP4_OptionsDescJson=this.AV40OptionsDescJson;
         aP5_OptionIndexesJson=this.AV42OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV33DDOName = aP0_DDOName;
         this.AV31SearchTxt = aP1_SearchTxt;
         this.AV32SearchTxtTo = aP2_SearchTxtTo;
         this.AV37OptionsJson = "" ;
         this.AV40OptionsDescJson = "" ;
         this.AV42OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV37OptionsJson;
         aP4_OptionsDescJson=this.AV40OptionsDescJson;
         aP5_OptionIndexesJson=this.AV42OptionIndexesJson;
         return AV42OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontagemresultadonotificacaofilterdata objgetwwcontagemresultadonotificacaofilterdata;
         objgetwwcontagemresultadonotificacaofilterdata = new getwwcontagemresultadonotificacaofilterdata();
         objgetwwcontagemresultadonotificacaofilterdata.AV33DDOName = aP0_DDOName;
         objgetwwcontagemresultadonotificacaofilterdata.AV31SearchTxt = aP1_SearchTxt;
         objgetwwcontagemresultadonotificacaofilterdata.AV32SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontagemresultadonotificacaofilterdata.AV37OptionsJson = "" ;
         objgetwwcontagemresultadonotificacaofilterdata.AV40OptionsDescJson = "" ;
         objgetwwcontagemresultadonotificacaofilterdata.AV42OptionIndexesJson = "" ;
         objgetwwcontagemresultadonotificacaofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontagemresultadonotificacaofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontagemresultadonotificacaofilterdata);
         aP3_OptionsJson=this.AV37OptionsJson;
         aP4_OptionsDescJson=this.AV40OptionsDescJson;
         aP5_OptionIndexesJson=this.AV42OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontagemresultadonotificacaofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV36Options = (IGxCollection)(new GxSimpleCollection());
         AV39OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV41OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV33DDOName), "DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADONOTIFICACAO_USUNOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV33DDOName), "DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADONOTIFICACAO_ASSUNTOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV33DDOName), "DDO_CONTAGEMRESULTADONOTIFICACAO_HOST") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADONOTIFICACAO_HOSTOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV33DDOName), "DDO_CONTAGEMRESULTADONOTIFICACAO_USER") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADONOTIFICACAO_USEROPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV37OptionsJson = AV36Options.ToJSonString(false);
         AV40OptionsDescJson = AV39OptionsDesc.ToJSonString(false);
         AV42OptionIndexesJson = AV41OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV44Session.Get("WWContagemResultadoNotificacaoGridState"), "") == 0 )
         {
            AV46GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContagemResultadoNotificacaoGridState"), "");
         }
         else
         {
            AV46GridState.FromXml(AV44Session.Get("WWContagemResultadoNotificacaoGridState"), "");
         }
         AV84GXV1 = 1;
         while ( AV84GXV1 <= AV46GridState.gxTpr_Filtervalues.Count )
         {
            AV47GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV46GridState.gxTpr_Filtervalues.Item(AV84GXV1));
            if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADONOTIFICACAO_AREATRABALHOCOD") == 0 )
            {
               AV49ContagemResultadoNotificacao_AreaTrabalhoCod = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
            {
               AV10TFContagemResultadoNotificacao_DataHora = context.localUtil.CToT( AV47GridStateFilterValue.gxTpr_Value, 2);
               AV11TFContagemResultadoNotificacao_DataHora_To = context.localUtil.CToT( AV47GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
            {
               AV12TFContagemResultadoNotificacao_UsuNom = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL") == 0 )
            {
               AV13TFContagemResultadoNotificacao_UsuNom_Sel = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 )
            {
               AV14TFContagemResultadoNotificacao_Assunto = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL") == 0 )
            {
               AV15TFContagemResultadoNotificacao_Assunto_Sel = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS") == 0 )
            {
               AV16TFContagemResultadoNotificacao_Destinatarios = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Value, "."));
               AV17TFContagemResultadoNotificacao_Destinatarios_To = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS") == 0 )
            {
               AV18TFContagemResultadoNotificacao_Demandas = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Value, "."));
               AV19TFContagemResultadoNotificacao_Demandas_To = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_HOST") == 0 )
            {
               AV20TFContagemResultadoNotificacao_Host = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL") == 0 )
            {
               AV21TFContagemResultadoNotificacao_Host_Sel = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_USER") == 0 )
            {
               AV22TFContagemResultadoNotificacao_User = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_USER_SEL") == 0 )
            {
               AV23TFContagemResultadoNotificacao_User_Sel = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_PORT") == 0 )
            {
               AV24TFContagemResultadoNotificacao_Port = (short)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Value, "."));
               AV25TFContagemResultadoNotificacao_Port_To = (short)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL") == 0 )
            {
               AV26TFContagemResultadoNotificacao_Aut_Sel = (short)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_SEC_SEL") == 0 )
            {
               AV27TFContagemResultadoNotificacao_Sec_SelsJson = AV47GridStateFilterValue.gxTpr_Value;
               AV28TFContagemResultadoNotificacao_Sec_Sels.FromJSonString(AV27TFContagemResultadoNotificacao_Sec_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SEL") == 0 )
            {
               AV29TFContagemResultadoNotificacao_Logged_SelsJson = AV47GridStateFilterValue.gxTpr_Value;
               AV30TFContagemResultadoNotificacao_Logged_Sels.FromJSonString(AV29TFContagemResultadoNotificacao_Logged_SelsJson);
            }
            AV84GXV1 = (int)(AV84GXV1+1);
         }
         if ( AV46GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV48GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV46GridState.gxTpr_Dynamicfilters.Item(1));
            AV50DynamicFiltersSelector1 = AV48GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV50DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
            {
               AV52ContagemResultadoNotificacao_DataHora1 = context.localUtil.CToT( AV48GridStateDynamicFilter.gxTpr_Value, 2);
               AV53ContagemResultadoNotificacao_DataHora_To1 = context.localUtil.CToT( AV48GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 )
            {
               AV51DynamicFiltersOperator1 = AV48GridStateDynamicFilter.gxTpr_Operator;
               AV54ContagemResultadoNotificacao_Assunto1 = AV48GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
            {
               AV51DynamicFiltersOperator1 = AV48GridStateDynamicFilter.gxTpr_Operator;
               AV55ContagemResultadoNotificacao_UsuNom1 = AV48GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 )
            {
               AV51DynamicFiltersOperator1 = AV48GridStateDynamicFilter.gxTpr_Operator;
               AV56ContagemResultadoNotificacao_DestNome1 = AV48GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV46GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV57DynamicFiltersEnabled2 = true;
               AV48GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV46GridState.gxTpr_Dynamicfilters.Item(2));
               AV58DynamicFiltersSelector2 = AV48GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
               {
                  AV60ContagemResultadoNotificacao_DataHora2 = context.localUtil.CToT( AV48GridStateDynamicFilter.gxTpr_Value, 2);
                  AV61ContagemResultadoNotificacao_DataHora_To2 = context.localUtil.CToT( AV48GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 )
               {
                  AV59DynamicFiltersOperator2 = AV48GridStateDynamicFilter.gxTpr_Operator;
                  AV62ContagemResultadoNotificacao_Assunto2 = AV48GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
               {
                  AV59DynamicFiltersOperator2 = AV48GridStateDynamicFilter.gxTpr_Operator;
                  AV63ContagemResultadoNotificacao_UsuNom2 = AV48GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 )
               {
                  AV59DynamicFiltersOperator2 = AV48GridStateDynamicFilter.gxTpr_Operator;
                  AV64ContagemResultadoNotificacao_DestNome2 = AV48GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV46GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV65DynamicFiltersEnabled3 = true;
                  AV48GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV46GridState.gxTpr_Dynamicfilters.Item(3));
                  AV66DynamicFiltersSelector3 = AV48GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV66DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
                  {
                     AV68ContagemResultadoNotificacao_DataHora3 = context.localUtil.CToT( AV48GridStateDynamicFilter.gxTpr_Value, 2);
                     AV69ContagemResultadoNotificacao_DataHora_To3 = context.localUtil.CToT( AV48GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 )
                  {
                     AV67DynamicFiltersOperator3 = AV48GridStateDynamicFilter.gxTpr_Operator;
                     AV70ContagemResultadoNotificacao_Assunto3 = AV48GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
                  {
                     AV67DynamicFiltersOperator3 = AV48GridStateDynamicFilter.gxTpr_Operator;
                     AV71ContagemResultadoNotificacao_UsuNom3 = AV48GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 )
                  {
                     AV67DynamicFiltersOperator3 = AV48GridStateDynamicFilter.gxTpr_Operator;
                     AV72ContagemResultadoNotificacao_DestNome3 = AV48GridStateDynamicFilter.gxTpr_Value;
                  }
                  if ( AV46GridState.gxTpr_Dynamicfilters.Count >= 4 )
                  {
                     AV73DynamicFiltersEnabled4 = true;
                     AV48GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV46GridState.gxTpr_Dynamicfilters.Item(4));
                     AV74DynamicFiltersSelector4 = AV48GridStateDynamicFilter.gxTpr_Selected;
                     if ( StringUtil.StrCmp(AV74DynamicFiltersSelector4, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
                     {
                        AV76ContagemResultadoNotificacao_DataHora4 = context.localUtil.CToT( AV48GridStateDynamicFilter.gxTpr_Value, 2);
                        AV77ContagemResultadoNotificacao_DataHora_To4 = context.localUtil.CToT( AV48GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV74DynamicFiltersSelector4, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 )
                     {
                        AV75DynamicFiltersOperator4 = AV48GridStateDynamicFilter.gxTpr_Operator;
                        AV78ContagemResultadoNotificacao_Assunto4 = AV48GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV74DynamicFiltersSelector4, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
                     {
                        AV75DynamicFiltersOperator4 = AV48GridStateDynamicFilter.gxTpr_Operator;
                        AV79ContagemResultadoNotificacao_UsuNom4 = AV48GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV74DynamicFiltersSelector4, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 )
                     {
                        AV75DynamicFiltersOperator4 = AV48GridStateDynamicFilter.gxTpr_Operator;
                        AV80ContagemResultadoNotificacao_DestNome4 = AV48GridStateDynamicFilter.gxTpr_Value;
                     }
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMRESULTADONOTIFICACAO_USUNOMOPTIONS' Routine */
         AV12TFContagemResultadoNotificacao_UsuNom = AV31SearchTxt;
         AV13TFContagemResultadoNotificacao_UsuNom_Sel = "";
         AV86WWContagemResultadoNotificacaoDS_1_Contagemresultadonotificacao_areatrabalhocod = AV49ContagemResultadoNotificacao_AreaTrabalhoCod;
         AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1 = AV50DynamicFiltersSelector1;
         AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 = AV51DynamicFiltersOperator1;
         AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1 = AV52ContagemResultadoNotificacao_DataHora1;
         AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1 = AV53ContagemResultadoNotificacao_DataHora_To1;
         AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 = AV54ContagemResultadoNotificacao_Assunto1;
         AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 = AV55ContagemResultadoNotificacao_UsuNom1;
         AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 = AV56ContagemResultadoNotificacao_DestNome1;
         AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 = AV57DynamicFiltersEnabled2;
         AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2 = AV58DynamicFiltersSelector2;
         AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 = AV59DynamicFiltersOperator2;
         AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2 = AV60ContagemResultadoNotificacao_DataHora2;
         AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2 = AV61ContagemResultadoNotificacao_DataHora_To2;
         AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 = AV62ContagemResultadoNotificacao_Assunto2;
         AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 = AV63ContagemResultadoNotificacao_UsuNom2;
         AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 = AV64ContagemResultadoNotificacao_DestNome2;
         AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 = AV65DynamicFiltersEnabled3;
         AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3 = AV66DynamicFiltersSelector3;
         AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 = AV67DynamicFiltersOperator3;
         AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3 = AV68ContagemResultadoNotificacao_DataHora3;
         AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3 = AV69ContagemResultadoNotificacao_DataHora_To3;
         AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 = AV70ContagemResultadoNotificacao_Assunto3;
         AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 = AV71ContagemResultadoNotificacao_UsuNom3;
         AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 = AV72ContagemResultadoNotificacao_DestNome3;
         AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 = AV73DynamicFiltersEnabled4;
         AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4 = AV74DynamicFiltersSelector4;
         AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 = AV75DynamicFiltersOperator4;
         AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4 = AV76ContagemResultadoNotificacao_DataHora4;
         AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4 = AV77ContagemResultadoNotificacao_DataHora_To4;
         AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 = AV78ContagemResultadoNotificacao_Assunto4;
         AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 = AV79ContagemResultadoNotificacao_UsuNom4;
         AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 = AV80ContagemResultadoNotificacao_DestNome4;
         AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora = AV10TFContagemResultadoNotificacao_DataHora;
         AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to = AV11TFContagemResultadoNotificacao_DataHora_To;
         AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom = AV12TFContagemResultadoNotificacao_UsuNom;
         AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel = AV13TFContagemResultadoNotificacao_UsuNom_Sel;
         AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto = AV14TFContagemResultadoNotificacao_Assunto;
         AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel = AV15TFContagemResultadoNotificacao_Assunto_Sel;
         AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios = AV16TFContagemResultadoNotificacao_Destinatarios;
         AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to = AV17TFContagemResultadoNotificacao_Destinatarios_To;
         AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas = AV18TFContagemResultadoNotificacao_Demandas;
         AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to = AV19TFContagemResultadoNotificacao_Demandas_To;
         AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host = AV20TFContagemResultadoNotificacao_Host;
         AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel = AV21TFContagemResultadoNotificacao_Host_Sel;
         AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user = AV22TFContagemResultadoNotificacao_User;
         AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel = AV23TFContagemResultadoNotificacao_User_Sel;
         AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port = AV24TFContagemResultadoNotificacao_Port;
         AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to = AV25TFContagemResultadoNotificacao_Port_To;
         AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel = AV26TFContagemResultadoNotificacao_Aut_Sel;
         AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels = AV28TFContagemResultadoNotificacao_Sec_Sels;
         AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels = AV30TFContagemResultadoNotificacao_Logged_Sels;
         AV137Udparg52 = new prc_notificaoesparadestnome(context).executeUdp(  AV51DynamicFiltersOperator1,  AV56ContagemResultadoNotificacao_DestNome1,  AV59DynamicFiltersOperator2,  AV64ContagemResultadoNotificacao_DestNome2,  AV67DynamicFiltersOperator3,  AV72ContagemResultadoNotificacao_DestNome3,  AV75DynamicFiltersOperator4,  AV80ContagemResultadoNotificacao_DestNome4);
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A1960ContagemResultadoNotificacao_Sec ,
                                              AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels ,
                                              A1961ContagemResultadoNotificacao_Logged ,
                                              AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels ,
                                              A1412ContagemResultadoNotificacao_Codigo ,
                                              AV137Udparg52 ,
                                              AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1 ,
                                              AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1 ,
                                              AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1 ,
                                              AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 ,
                                              AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 ,
                                              AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 ,
                                              AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 ,
                                              AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2 ,
                                              AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2 ,
                                              AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2 ,
                                              AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 ,
                                              AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 ,
                                              AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 ,
                                              AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 ,
                                              AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3 ,
                                              AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3 ,
                                              AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3 ,
                                              AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 ,
                                              AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 ,
                                              AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 ,
                                              AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 ,
                                              AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4 ,
                                              AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4 ,
                                              AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4 ,
                                              AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 ,
                                              AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 ,
                                              AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 ,
                                              AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora ,
                                              AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to ,
                                              AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel ,
                                              AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom ,
                                              AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel ,
                                              AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto ,
                                              AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel ,
                                              AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host ,
                                              AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel ,
                                              AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user ,
                                              AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port ,
                                              AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to ,
                                              AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel ,
                                              AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels.Count ,
                                              AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels.Count ,
                                              AV50DynamicFiltersSelector1 ,
                                              AV56ContagemResultadoNotificacao_DestNome1 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV64ContagemResultadoNotificacao_DestNome2 ,
                                              AV66DynamicFiltersSelector3 ,
                                              AV72ContagemResultadoNotificacao_DestNome3 ,
                                              AV74DynamicFiltersSelector4 ,
                                              AV80ContagemResultadoNotificacao_DestNome4 ,
                                              A1416ContagemResultadoNotificacao_DataHora ,
                                              A1417ContagemResultadoNotificacao_Assunto ,
                                              A1422ContagemResultadoNotificacao_UsuNom ,
                                              A1421ContagemResultadoNotificacao_DestNome ,
                                              AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 ,
                                              AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 ,
                                              AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 ,
                                              AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 ,
                                              A1956ContagemResultadoNotificacao_Host ,
                                              A1957ContagemResultadoNotificacao_User ,
                                              A1958ContagemResultadoNotificacao_Port ,
                                              A1959ContagemResultadoNotificacao_Aut ,
                                              AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios ,
                                              A1962ContagemResultadoNotificacao_Destinatarios ,
                                              AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to ,
                                              AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas ,
                                              A1963ContagemResultadoNotificacao_Demandas ,
                                              AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to ,
                                              A1966ContagemResultadoNotificacao_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.LONG, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 = StringUtil.Concat( StringUtil.RTrim( AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1), "%", "");
         lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 = StringUtil.Concat( StringUtil.RTrim( AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1), "%", "");
         lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 = StringUtil.PadR( StringUtil.RTrim( AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1), 100, "%");
         lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 = StringUtil.PadR( StringUtil.RTrim( AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1), 100, "%");
         lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 = StringUtil.PadR( StringUtil.RTrim( AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1), 100, "%");
         lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 = StringUtil.PadR( StringUtil.RTrim( AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1), 100, "%");
         lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 = StringUtil.Concat( StringUtil.RTrim( AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2), "%", "");
         lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 = StringUtil.Concat( StringUtil.RTrim( AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2), "%", "");
         lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 = StringUtil.PadR( StringUtil.RTrim( AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2), 100, "%");
         lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 = StringUtil.PadR( StringUtil.RTrim( AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2), 100, "%");
         lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 = StringUtil.PadR( StringUtil.RTrim( AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2), 100, "%");
         lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 = StringUtil.PadR( StringUtil.RTrim( AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2), 100, "%");
         lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 = StringUtil.Concat( StringUtil.RTrim( AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3), "%", "");
         lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 = StringUtil.Concat( StringUtil.RTrim( AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3), "%", "");
         lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 = StringUtil.PadR( StringUtil.RTrim( AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3), 100, "%");
         lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 = StringUtil.PadR( StringUtil.RTrim( AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3), 100, "%");
         lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 = StringUtil.PadR( StringUtil.RTrim( AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3), 100, "%");
         lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 = StringUtil.PadR( StringUtil.RTrim( AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3), 100, "%");
         lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 = StringUtil.Concat( StringUtil.RTrim( AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4), "%", "");
         lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 = StringUtil.Concat( StringUtil.RTrim( AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4), "%", "");
         lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 = StringUtil.PadR( StringUtil.RTrim( AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4), 100, "%");
         lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 = StringUtil.PadR( StringUtil.RTrim( AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4), 100, "%");
         lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 = StringUtil.PadR( StringUtil.RTrim( AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4), 100, "%");
         lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 = StringUtil.PadR( StringUtil.RTrim( AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4), 100, "%");
         lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom = StringUtil.PadR( StringUtil.RTrim( AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom), 100, "%");
         lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto = StringUtil.Concat( StringUtil.RTrim( AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto), "%", "");
         lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host = StringUtil.PadR( StringUtil.RTrim( AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host), 50, "%");
         lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user = StringUtil.PadR( StringUtil.RTrim( AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user), 50, "%");
         /* Using cursor P00VG4 */
         pr_default.execute(0, new Object[] {AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios, AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios, AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to, AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to, AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas, AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas, AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to, AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to, AV9WWPContext.gxTpr_Areatrabalho_codigo, AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1, AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1, lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1, lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1, lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1, lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1, lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1, lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1, AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2, AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2, lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2, lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2, lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2, lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2, lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2, lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2, AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3, AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3, lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3, lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3, lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3, lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3, lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3, lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3, AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4, AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4, lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4, lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4, lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4, lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4, lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4, lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4, AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora, AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to, lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom, AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel, lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto, AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel, lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host, AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel, lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user, AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel, AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port, AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKVG2 = false;
            A1413ContagemResultadoNotificacao_UsuCod = P00VG4_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = P00VG4_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = P00VG4_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A1414ContagemResultadoNotificacao_DestCod = P00VG4_A1414ContagemResultadoNotificacao_DestCod[0];
            A1426ContagemResultadoNotificacao_DestPesCod = P00VG4_A1426ContagemResultadoNotificacao_DestPesCod[0];
            n1426ContagemResultadoNotificacao_DestPesCod = P00VG4_n1426ContagemResultadoNotificacao_DestPesCod[0];
            A1966ContagemResultadoNotificacao_AreaTrabalhoCod = P00VG4_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            n1966ContagemResultadoNotificacao_AreaTrabalhoCod = P00VG4_n1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            A1422ContagemResultadoNotificacao_UsuNom = P00VG4_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = P00VG4_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1412ContagemResultadoNotificacao_Codigo = P00VG4_A1412ContagemResultadoNotificacao_Codigo[0];
            A1961ContagemResultadoNotificacao_Logged = P00VG4_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = P00VG4_n1961ContagemResultadoNotificacao_Logged[0];
            A1960ContagemResultadoNotificacao_Sec = P00VG4_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = P00VG4_n1960ContagemResultadoNotificacao_Sec[0];
            A1959ContagemResultadoNotificacao_Aut = P00VG4_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = P00VG4_n1959ContagemResultadoNotificacao_Aut[0];
            A1958ContagemResultadoNotificacao_Port = P00VG4_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = P00VG4_n1958ContagemResultadoNotificacao_Port[0];
            A1957ContagemResultadoNotificacao_User = P00VG4_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = P00VG4_n1957ContagemResultadoNotificacao_User[0];
            A1956ContagemResultadoNotificacao_Host = P00VG4_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = P00VG4_n1956ContagemResultadoNotificacao_Host[0];
            A1421ContagemResultadoNotificacao_DestNome = P00VG4_A1421ContagemResultadoNotificacao_DestNome[0];
            n1421ContagemResultadoNotificacao_DestNome = P00VG4_n1421ContagemResultadoNotificacao_DestNome[0];
            A1417ContagemResultadoNotificacao_Assunto = P00VG4_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = P00VG4_n1417ContagemResultadoNotificacao_Assunto[0];
            A1416ContagemResultadoNotificacao_DataHora = P00VG4_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = P00VG4_n1416ContagemResultadoNotificacao_DataHora[0];
            A1963ContagemResultadoNotificacao_Demandas = P00VG4_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = P00VG4_n1963ContagemResultadoNotificacao_Demandas[0];
            A1962ContagemResultadoNotificacao_Destinatarios = P00VG4_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = P00VG4_n1962ContagemResultadoNotificacao_Destinatarios[0];
            A1426ContagemResultadoNotificacao_DestPesCod = P00VG4_A1426ContagemResultadoNotificacao_DestPesCod[0];
            n1426ContagemResultadoNotificacao_DestPesCod = P00VG4_n1426ContagemResultadoNotificacao_DestPesCod[0];
            A1421ContagemResultadoNotificacao_DestNome = P00VG4_A1421ContagemResultadoNotificacao_DestNome[0];
            n1421ContagemResultadoNotificacao_DestNome = P00VG4_n1421ContagemResultadoNotificacao_DestNome[0];
            A1413ContagemResultadoNotificacao_UsuCod = P00VG4_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1966ContagemResultadoNotificacao_AreaTrabalhoCod = P00VG4_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            n1966ContagemResultadoNotificacao_AreaTrabalhoCod = P00VG4_n1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            A1961ContagemResultadoNotificacao_Logged = P00VG4_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = P00VG4_n1961ContagemResultadoNotificacao_Logged[0];
            A1960ContagemResultadoNotificacao_Sec = P00VG4_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = P00VG4_n1960ContagemResultadoNotificacao_Sec[0];
            A1959ContagemResultadoNotificacao_Aut = P00VG4_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = P00VG4_n1959ContagemResultadoNotificacao_Aut[0];
            A1958ContagemResultadoNotificacao_Port = P00VG4_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = P00VG4_n1958ContagemResultadoNotificacao_Port[0];
            A1957ContagemResultadoNotificacao_User = P00VG4_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = P00VG4_n1957ContagemResultadoNotificacao_User[0];
            A1956ContagemResultadoNotificacao_Host = P00VG4_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = P00VG4_n1956ContagemResultadoNotificacao_Host[0];
            A1417ContagemResultadoNotificacao_Assunto = P00VG4_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = P00VG4_n1417ContagemResultadoNotificacao_Assunto[0];
            A1416ContagemResultadoNotificacao_DataHora = P00VG4_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = P00VG4_n1416ContagemResultadoNotificacao_DataHora[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = P00VG4_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = P00VG4_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A1422ContagemResultadoNotificacao_UsuNom = P00VG4_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = P00VG4_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1963ContagemResultadoNotificacao_Demandas = P00VG4_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = P00VG4_n1963ContagemResultadoNotificacao_Demandas[0];
            A1962ContagemResultadoNotificacao_Destinatarios = P00VG4_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = P00VG4_n1962ContagemResultadoNotificacao_Destinatarios[0];
            AV43count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00VG4_A1422ContagemResultadoNotificacao_UsuNom[0], A1422ContagemResultadoNotificacao_UsuNom) == 0 ) )
            {
               BRKVG2 = false;
               A1413ContagemResultadoNotificacao_UsuCod = P00VG4_A1413ContagemResultadoNotificacao_UsuCod[0];
               A1427ContagemResultadoNotificacao_UsuPesCod = P00VG4_A1427ContagemResultadoNotificacao_UsuPesCod[0];
               n1427ContagemResultadoNotificacao_UsuPesCod = P00VG4_n1427ContagemResultadoNotificacao_UsuPesCod[0];
               A1414ContagemResultadoNotificacao_DestCod = P00VG4_A1414ContagemResultadoNotificacao_DestCod[0];
               A1412ContagemResultadoNotificacao_Codigo = P00VG4_A1412ContagemResultadoNotificacao_Codigo[0];
               A1413ContagemResultadoNotificacao_UsuCod = P00VG4_A1413ContagemResultadoNotificacao_UsuCod[0];
               A1427ContagemResultadoNotificacao_UsuPesCod = P00VG4_A1427ContagemResultadoNotificacao_UsuPesCod[0];
               n1427ContagemResultadoNotificacao_UsuPesCod = P00VG4_n1427ContagemResultadoNotificacao_UsuPesCod[0];
               AV43count = (long)(AV43count+1);
               BRKVG2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1422ContagemResultadoNotificacao_UsuNom)) )
            {
               AV35Option = A1422ContagemResultadoNotificacao_UsuNom;
               AV36Options.Add(AV35Option, 0);
               AV41OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV43count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV36Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKVG2 )
            {
               BRKVG2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTAGEMRESULTADONOTIFICACAO_ASSUNTOOPTIONS' Routine */
         AV14TFContagemResultadoNotificacao_Assunto = AV31SearchTxt;
         AV15TFContagemResultadoNotificacao_Assunto_Sel = "";
         AV86WWContagemResultadoNotificacaoDS_1_Contagemresultadonotificacao_areatrabalhocod = AV49ContagemResultadoNotificacao_AreaTrabalhoCod;
         AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1 = AV50DynamicFiltersSelector1;
         AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 = AV51DynamicFiltersOperator1;
         AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1 = AV52ContagemResultadoNotificacao_DataHora1;
         AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1 = AV53ContagemResultadoNotificacao_DataHora_To1;
         AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 = AV54ContagemResultadoNotificacao_Assunto1;
         AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 = AV55ContagemResultadoNotificacao_UsuNom1;
         AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 = AV56ContagemResultadoNotificacao_DestNome1;
         AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 = AV57DynamicFiltersEnabled2;
         AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2 = AV58DynamicFiltersSelector2;
         AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 = AV59DynamicFiltersOperator2;
         AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2 = AV60ContagemResultadoNotificacao_DataHora2;
         AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2 = AV61ContagemResultadoNotificacao_DataHora_To2;
         AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 = AV62ContagemResultadoNotificacao_Assunto2;
         AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 = AV63ContagemResultadoNotificacao_UsuNom2;
         AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 = AV64ContagemResultadoNotificacao_DestNome2;
         AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 = AV65DynamicFiltersEnabled3;
         AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3 = AV66DynamicFiltersSelector3;
         AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 = AV67DynamicFiltersOperator3;
         AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3 = AV68ContagemResultadoNotificacao_DataHora3;
         AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3 = AV69ContagemResultadoNotificacao_DataHora_To3;
         AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 = AV70ContagemResultadoNotificacao_Assunto3;
         AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 = AV71ContagemResultadoNotificacao_UsuNom3;
         AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 = AV72ContagemResultadoNotificacao_DestNome3;
         AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 = AV73DynamicFiltersEnabled4;
         AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4 = AV74DynamicFiltersSelector4;
         AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 = AV75DynamicFiltersOperator4;
         AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4 = AV76ContagemResultadoNotificacao_DataHora4;
         AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4 = AV77ContagemResultadoNotificacao_DataHora_To4;
         AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 = AV78ContagemResultadoNotificacao_Assunto4;
         AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 = AV79ContagemResultadoNotificacao_UsuNom4;
         AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 = AV80ContagemResultadoNotificacao_DestNome4;
         AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora = AV10TFContagemResultadoNotificacao_DataHora;
         AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to = AV11TFContagemResultadoNotificacao_DataHora_To;
         AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom = AV12TFContagemResultadoNotificacao_UsuNom;
         AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel = AV13TFContagemResultadoNotificacao_UsuNom_Sel;
         AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto = AV14TFContagemResultadoNotificacao_Assunto;
         AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel = AV15TFContagemResultadoNotificacao_Assunto_Sel;
         AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios = AV16TFContagemResultadoNotificacao_Destinatarios;
         AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to = AV17TFContagemResultadoNotificacao_Destinatarios_To;
         AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas = AV18TFContagemResultadoNotificacao_Demandas;
         AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to = AV19TFContagemResultadoNotificacao_Demandas_To;
         AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host = AV20TFContagemResultadoNotificacao_Host;
         AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel = AV21TFContagemResultadoNotificacao_Host_Sel;
         AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user = AV22TFContagemResultadoNotificacao_User;
         AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel = AV23TFContagemResultadoNotificacao_User_Sel;
         AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port = AV24TFContagemResultadoNotificacao_Port;
         AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to = AV25TFContagemResultadoNotificacao_Port_To;
         AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel = AV26TFContagemResultadoNotificacao_Aut_Sel;
         AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels = AV28TFContagemResultadoNotificacao_Sec_Sels;
         AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels = AV30TFContagemResultadoNotificacao_Logged_Sels;
         AV137Udparg52 = new prc_notificaoesparadestnome(context).executeUdp(  AV51DynamicFiltersOperator1,  AV56ContagemResultadoNotificacao_DestNome1,  AV59DynamicFiltersOperator2,  AV64ContagemResultadoNotificacao_DestNome2,  AV67DynamicFiltersOperator3,  AV72ContagemResultadoNotificacao_DestNome3,  AV75DynamicFiltersOperator4,  AV80ContagemResultadoNotificacao_DestNome4);
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1960ContagemResultadoNotificacao_Sec ,
                                              AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels ,
                                              A1961ContagemResultadoNotificacao_Logged ,
                                              AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels ,
                                              A1412ContagemResultadoNotificacao_Codigo ,
                                              AV137Udparg52 ,
                                              AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1 ,
                                              AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1 ,
                                              AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1 ,
                                              AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 ,
                                              AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 ,
                                              AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 ,
                                              AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 ,
                                              AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2 ,
                                              AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2 ,
                                              AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2 ,
                                              AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 ,
                                              AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 ,
                                              AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 ,
                                              AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 ,
                                              AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3 ,
                                              AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3 ,
                                              AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3 ,
                                              AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 ,
                                              AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 ,
                                              AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 ,
                                              AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 ,
                                              AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4 ,
                                              AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4 ,
                                              AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4 ,
                                              AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 ,
                                              AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 ,
                                              AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 ,
                                              AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora ,
                                              AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to ,
                                              AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel ,
                                              AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom ,
                                              AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel ,
                                              AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto ,
                                              AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel ,
                                              AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host ,
                                              AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel ,
                                              AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user ,
                                              AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port ,
                                              AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to ,
                                              AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel ,
                                              AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels.Count ,
                                              AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels.Count ,
                                              AV50DynamicFiltersSelector1 ,
                                              AV56ContagemResultadoNotificacao_DestNome1 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV64ContagemResultadoNotificacao_DestNome2 ,
                                              AV66DynamicFiltersSelector3 ,
                                              AV72ContagemResultadoNotificacao_DestNome3 ,
                                              AV74DynamicFiltersSelector4 ,
                                              AV80ContagemResultadoNotificacao_DestNome4 ,
                                              A1416ContagemResultadoNotificacao_DataHora ,
                                              A1417ContagemResultadoNotificacao_Assunto ,
                                              A1422ContagemResultadoNotificacao_UsuNom ,
                                              A1421ContagemResultadoNotificacao_DestNome ,
                                              AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 ,
                                              AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 ,
                                              AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 ,
                                              AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 ,
                                              A1956ContagemResultadoNotificacao_Host ,
                                              A1957ContagemResultadoNotificacao_User ,
                                              A1958ContagemResultadoNotificacao_Port ,
                                              A1959ContagemResultadoNotificacao_Aut ,
                                              AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios ,
                                              A1962ContagemResultadoNotificacao_Destinatarios ,
                                              AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to ,
                                              AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas ,
                                              A1963ContagemResultadoNotificacao_Demandas ,
                                              AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to ,
                                              A1966ContagemResultadoNotificacao_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.LONG, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 = StringUtil.Concat( StringUtil.RTrim( AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1), "%", "");
         lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 = StringUtil.Concat( StringUtil.RTrim( AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1), "%", "");
         lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 = StringUtil.PadR( StringUtil.RTrim( AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1), 100, "%");
         lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 = StringUtil.PadR( StringUtil.RTrim( AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1), 100, "%");
         lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 = StringUtil.PadR( StringUtil.RTrim( AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1), 100, "%");
         lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 = StringUtil.PadR( StringUtil.RTrim( AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1), 100, "%");
         lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 = StringUtil.Concat( StringUtil.RTrim( AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2), "%", "");
         lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 = StringUtil.Concat( StringUtil.RTrim( AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2), "%", "");
         lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 = StringUtil.PadR( StringUtil.RTrim( AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2), 100, "%");
         lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 = StringUtil.PadR( StringUtil.RTrim( AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2), 100, "%");
         lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 = StringUtil.PadR( StringUtil.RTrim( AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2), 100, "%");
         lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 = StringUtil.PadR( StringUtil.RTrim( AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2), 100, "%");
         lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 = StringUtil.Concat( StringUtil.RTrim( AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3), "%", "");
         lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 = StringUtil.Concat( StringUtil.RTrim( AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3), "%", "");
         lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 = StringUtil.PadR( StringUtil.RTrim( AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3), 100, "%");
         lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 = StringUtil.PadR( StringUtil.RTrim( AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3), 100, "%");
         lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 = StringUtil.PadR( StringUtil.RTrim( AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3), 100, "%");
         lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 = StringUtil.PadR( StringUtil.RTrim( AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3), 100, "%");
         lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 = StringUtil.Concat( StringUtil.RTrim( AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4), "%", "");
         lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 = StringUtil.Concat( StringUtil.RTrim( AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4), "%", "");
         lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 = StringUtil.PadR( StringUtil.RTrim( AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4), 100, "%");
         lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 = StringUtil.PadR( StringUtil.RTrim( AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4), 100, "%");
         lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 = StringUtil.PadR( StringUtil.RTrim( AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4), 100, "%");
         lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 = StringUtil.PadR( StringUtil.RTrim( AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4), 100, "%");
         lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom = StringUtil.PadR( StringUtil.RTrim( AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom), 100, "%");
         lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto = StringUtil.Concat( StringUtil.RTrim( AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto), "%", "");
         lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host = StringUtil.PadR( StringUtil.RTrim( AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host), 50, "%");
         lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user = StringUtil.PadR( StringUtil.RTrim( AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user), 50, "%");
         /* Using cursor P00VG7 */
         pr_default.execute(1, new Object[] {AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios, AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios, AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to, AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to, AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas, AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas, AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to, AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to, AV9WWPContext.gxTpr_Areatrabalho_codigo, AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1, AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1, lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1, lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1, lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1, lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1, lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1, lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1, AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2, AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2, lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2, lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2, lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2, lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2, lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2, lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2, AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3, AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3, lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3, lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3, lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3, lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3, lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3, lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3, AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4, AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4, lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4, lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4, lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4, lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4, lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4, lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4, AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora, AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to, lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom, AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel, lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto, AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel, lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host, AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel, lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user, AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel, AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port, AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKVG4 = false;
            A1413ContagemResultadoNotificacao_UsuCod = P00VG7_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = P00VG7_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = P00VG7_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A1414ContagemResultadoNotificacao_DestCod = P00VG7_A1414ContagemResultadoNotificacao_DestCod[0];
            A1426ContagemResultadoNotificacao_DestPesCod = P00VG7_A1426ContagemResultadoNotificacao_DestPesCod[0];
            n1426ContagemResultadoNotificacao_DestPesCod = P00VG7_n1426ContagemResultadoNotificacao_DestPesCod[0];
            A1966ContagemResultadoNotificacao_AreaTrabalhoCod = P00VG7_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            n1966ContagemResultadoNotificacao_AreaTrabalhoCod = P00VG7_n1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            A1417ContagemResultadoNotificacao_Assunto = P00VG7_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = P00VG7_n1417ContagemResultadoNotificacao_Assunto[0];
            A1412ContagemResultadoNotificacao_Codigo = P00VG7_A1412ContagemResultadoNotificacao_Codigo[0];
            A1961ContagemResultadoNotificacao_Logged = P00VG7_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = P00VG7_n1961ContagemResultadoNotificacao_Logged[0];
            A1960ContagemResultadoNotificacao_Sec = P00VG7_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = P00VG7_n1960ContagemResultadoNotificacao_Sec[0];
            A1959ContagemResultadoNotificacao_Aut = P00VG7_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = P00VG7_n1959ContagemResultadoNotificacao_Aut[0];
            A1958ContagemResultadoNotificacao_Port = P00VG7_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = P00VG7_n1958ContagemResultadoNotificacao_Port[0];
            A1957ContagemResultadoNotificacao_User = P00VG7_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = P00VG7_n1957ContagemResultadoNotificacao_User[0];
            A1956ContagemResultadoNotificacao_Host = P00VG7_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = P00VG7_n1956ContagemResultadoNotificacao_Host[0];
            A1421ContagemResultadoNotificacao_DestNome = P00VG7_A1421ContagemResultadoNotificacao_DestNome[0];
            n1421ContagemResultadoNotificacao_DestNome = P00VG7_n1421ContagemResultadoNotificacao_DestNome[0];
            A1422ContagemResultadoNotificacao_UsuNom = P00VG7_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = P00VG7_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1416ContagemResultadoNotificacao_DataHora = P00VG7_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = P00VG7_n1416ContagemResultadoNotificacao_DataHora[0];
            A1963ContagemResultadoNotificacao_Demandas = P00VG7_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = P00VG7_n1963ContagemResultadoNotificacao_Demandas[0];
            A1962ContagemResultadoNotificacao_Destinatarios = P00VG7_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = P00VG7_n1962ContagemResultadoNotificacao_Destinatarios[0];
            A1426ContagemResultadoNotificacao_DestPesCod = P00VG7_A1426ContagemResultadoNotificacao_DestPesCod[0];
            n1426ContagemResultadoNotificacao_DestPesCod = P00VG7_n1426ContagemResultadoNotificacao_DestPesCod[0];
            A1421ContagemResultadoNotificacao_DestNome = P00VG7_A1421ContagemResultadoNotificacao_DestNome[0];
            n1421ContagemResultadoNotificacao_DestNome = P00VG7_n1421ContagemResultadoNotificacao_DestNome[0];
            A1413ContagemResultadoNotificacao_UsuCod = P00VG7_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1966ContagemResultadoNotificacao_AreaTrabalhoCod = P00VG7_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            n1966ContagemResultadoNotificacao_AreaTrabalhoCod = P00VG7_n1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            A1417ContagemResultadoNotificacao_Assunto = P00VG7_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = P00VG7_n1417ContagemResultadoNotificacao_Assunto[0];
            A1961ContagemResultadoNotificacao_Logged = P00VG7_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = P00VG7_n1961ContagemResultadoNotificacao_Logged[0];
            A1960ContagemResultadoNotificacao_Sec = P00VG7_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = P00VG7_n1960ContagemResultadoNotificacao_Sec[0];
            A1959ContagemResultadoNotificacao_Aut = P00VG7_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = P00VG7_n1959ContagemResultadoNotificacao_Aut[0];
            A1958ContagemResultadoNotificacao_Port = P00VG7_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = P00VG7_n1958ContagemResultadoNotificacao_Port[0];
            A1957ContagemResultadoNotificacao_User = P00VG7_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = P00VG7_n1957ContagemResultadoNotificacao_User[0];
            A1956ContagemResultadoNotificacao_Host = P00VG7_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = P00VG7_n1956ContagemResultadoNotificacao_Host[0];
            A1416ContagemResultadoNotificacao_DataHora = P00VG7_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = P00VG7_n1416ContagemResultadoNotificacao_DataHora[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = P00VG7_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = P00VG7_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A1422ContagemResultadoNotificacao_UsuNom = P00VG7_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = P00VG7_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1963ContagemResultadoNotificacao_Demandas = P00VG7_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = P00VG7_n1963ContagemResultadoNotificacao_Demandas[0];
            A1962ContagemResultadoNotificacao_Destinatarios = P00VG7_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = P00VG7_n1962ContagemResultadoNotificacao_Destinatarios[0];
            AV43count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00VG7_A1417ContagemResultadoNotificacao_Assunto[0], A1417ContagemResultadoNotificacao_Assunto) == 0 ) )
            {
               BRKVG4 = false;
               A1414ContagemResultadoNotificacao_DestCod = P00VG7_A1414ContagemResultadoNotificacao_DestCod[0];
               A1412ContagemResultadoNotificacao_Codigo = P00VG7_A1412ContagemResultadoNotificacao_Codigo[0];
               AV43count = (long)(AV43count+1);
               BRKVG4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1417ContagemResultadoNotificacao_Assunto)) )
            {
               AV35Option = A1417ContagemResultadoNotificacao_Assunto;
               AV36Options.Add(AV35Option, 0);
               AV41OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV43count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV36Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKVG4 )
            {
               BRKVG4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTAGEMRESULTADONOTIFICACAO_HOSTOPTIONS' Routine */
         AV20TFContagemResultadoNotificacao_Host = AV31SearchTxt;
         AV21TFContagemResultadoNotificacao_Host_Sel = "";
         AV86WWContagemResultadoNotificacaoDS_1_Contagemresultadonotificacao_areatrabalhocod = AV49ContagemResultadoNotificacao_AreaTrabalhoCod;
         AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1 = AV50DynamicFiltersSelector1;
         AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 = AV51DynamicFiltersOperator1;
         AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1 = AV52ContagemResultadoNotificacao_DataHora1;
         AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1 = AV53ContagemResultadoNotificacao_DataHora_To1;
         AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 = AV54ContagemResultadoNotificacao_Assunto1;
         AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 = AV55ContagemResultadoNotificacao_UsuNom1;
         AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 = AV56ContagemResultadoNotificacao_DestNome1;
         AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 = AV57DynamicFiltersEnabled2;
         AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2 = AV58DynamicFiltersSelector2;
         AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 = AV59DynamicFiltersOperator2;
         AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2 = AV60ContagemResultadoNotificacao_DataHora2;
         AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2 = AV61ContagemResultadoNotificacao_DataHora_To2;
         AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 = AV62ContagemResultadoNotificacao_Assunto2;
         AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 = AV63ContagemResultadoNotificacao_UsuNom2;
         AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 = AV64ContagemResultadoNotificacao_DestNome2;
         AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 = AV65DynamicFiltersEnabled3;
         AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3 = AV66DynamicFiltersSelector3;
         AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 = AV67DynamicFiltersOperator3;
         AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3 = AV68ContagemResultadoNotificacao_DataHora3;
         AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3 = AV69ContagemResultadoNotificacao_DataHora_To3;
         AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 = AV70ContagemResultadoNotificacao_Assunto3;
         AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 = AV71ContagemResultadoNotificacao_UsuNom3;
         AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 = AV72ContagemResultadoNotificacao_DestNome3;
         AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 = AV73DynamicFiltersEnabled4;
         AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4 = AV74DynamicFiltersSelector4;
         AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 = AV75DynamicFiltersOperator4;
         AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4 = AV76ContagemResultadoNotificacao_DataHora4;
         AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4 = AV77ContagemResultadoNotificacao_DataHora_To4;
         AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 = AV78ContagemResultadoNotificacao_Assunto4;
         AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 = AV79ContagemResultadoNotificacao_UsuNom4;
         AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 = AV80ContagemResultadoNotificacao_DestNome4;
         AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora = AV10TFContagemResultadoNotificacao_DataHora;
         AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to = AV11TFContagemResultadoNotificacao_DataHora_To;
         AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom = AV12TFContagemResultadoNotificacao_UsuNom;
         AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel = AV13TFContagemResultadoNotificacao_UsuNom_Sel;
         AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto = AV14TFContagemResultadoNotificacao_Assunto;
         AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel = AV15TFContagemResultadoNotificacao_Assunto_Sel;
         AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios = AV16TFContagemResultadoNotificacao_Destinatarios;
         AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to = AV17TFContagemResultadoNotificacao_Destinatarios_To;
         AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas = AV18TFContagemResultadoNotificacao_Demandas;
         AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to = AV19TFContagemResultadoNotificacao_Demandas_To;
         AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host = AV20TFContagemResultadoNotificacao_Host;
         AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel = AV21TFContagemResultadoNotificacao_Host_Sel;
         AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user = AV22TFContagemResultadoNotificacao_User;
         AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel = AV23TFContagemResultadoNotificacao_User_Sel;
         AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port = AV24TFContagemResultadoNotificacao_Port;
         AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to = AV25TFContagemResultadoNotificacao_Port_To;
         AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel = AV26TFContagemResultadoNotificacao_Aut_Sel;
         AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels = AV28TFContagemResultadoNotificacao_Sec_Sels;
         AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels = AV30TFContagemResultadoNotificacao_Logged_Sels;
         AV137Udparg52 = new prc_notificaoesparadestnome(context).executeUdp(  AV51DynamicFiltersOperator1,  AV56ContagemResultadoNotificacao_DestNome1,  AV59DynamicFiltersOperator2,  AV64ContagemResultadoNotificacao_DestNome2,  AV67DynamicFiltersOperator3,  AV72ContagemResultadoNotificacao_DestNome3,  AV75DynamicFiltersOperator4,  AV80ContagemResultadoNotificacao_DestNome4);
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A1960ContagemResultadoNotificacao_Sec ,
                                              AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels ,
                                              A1961ContagemResultadoNotificacao_Logged ,
                                              AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels ,
                                              A1412ContagemResultadoNotificacao_Codigo ,
                                              AV137Udparg52 ,
                                              AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1 ,
                                              AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1 ,
                                              AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1 ,
                                              AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 ,
                                              AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 ,
                                              AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 ,
                                              AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 ,
                                              AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2 ,
                                              AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2 ,
                                              AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2 ,
                                              AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 ,
                                              AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 ,
                                              AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 ,
                                              AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 ,
                                              AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3 ,
                                              AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3 ,
                                              AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3 ,
                                              AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 ,
                                              AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 ,
                                              AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 ,
                                              AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 ,
                                              AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4 ,
                                              AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4 ,
                                              AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4 ,
                                              AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 ,
                                              AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 ,
                                              AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 ,
                                              AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora ,
                                              AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to ,
                                              AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel ,
                                              AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom ,
                                              AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel ,
                                              AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto ,
                                              AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel ,
                                              AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host ,
                                              AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel ,
                                              AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user ,
                                              AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port ,
                                              AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to ,
                                              AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel ,
                                              AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels.Count ,
                                              AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels.Count ,
                                              AV50DynamicFiltersSelector1 ,
                                              AV56ContagemResultadoNotificacao_DestNome1 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV64ContagemResultadoNotificacao_DestNome2 ,
                                              AV66DynamicFiltersSelector3 ,
                                              AV72ContagemResultadoNotificacao_DestNome3 ,
                                              AV74DynamicFiltersSelector4 ,
                                              AV80ContagemResultadoNotificacao_DestNome4 ,
                                              A1416ContagemResultadoNotificacao_DataHora ,
                                              A1417ContagemResultadoNotificacao_Assunto ,
                                              A1422ContagemResultadoNotificacao_UsuNom ,
                                              A1421ContagemResultadoNotificacao_DestNome ,
                                              AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 ,
                                              AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 ,
                                              AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 ,
                                              AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 ,
                                              A1956ContagemResultadoNotificacao_Host ,
                                              A1957ContagemResultadoNotificacao_User ,
                                              A1958ContagemResultadoNotificacao_Port ,
                                              A1959ContagemResultadoNotificacao_Aut ,
                                              AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios ,
                                              A1962ContagemResultadoNotificacao_Destinatarios ,
                                              AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to ,
                                              AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas ,
                                              A1963ContagemResultadoNotificacao_Demandas ,
                                              AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to ,
                                              A1966ContagemResultadoNotificacao_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.LONG, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 = StringUtil.Concat( StringUtil.RTrim( AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1), "%", "");
         lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 = StringUtil.Concat( StringUtil.RTrim( AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1), "%", "");
         lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 = StringUtil.PadR( StringUtil.RTrim( AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1), 100, "%");
         lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 = StringUtil.PadR( StringUtil.RTrim( AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1), 100, "%");
         lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 = StringUtil.PadR( StringUtil.RTrim( AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1), 100, "%");
         lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 = StringUtil.PadR( StringUtil.RTrim( AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1), 100, "%");
         lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 = StringUtil.Concat( StringUtil.RTrim( AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2), "%", "");
         lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 = StringUtil.Concat( StringUtil.RTrim( AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2), "%", "");
         lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 = StringUtil.PadR( StringUtil.RTrim( AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2), 100, "%");
         lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 = StringUtil.PadR( StringUtil.RTrim( AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2), 100, "%");
         lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 = StringUtil.PadR( StringUtil.RTrim( AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2), 100, "%");
         lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 = StringUtil.PadR( StringUtil.RTrim( AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2), 100, "%");
         lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 = StringUtil.Concat( StringUtil.RTrim( AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3), "%", "");
         lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 = StringUtil.Concat( StringUtil.RTrim( AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3), "%", "");
         lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 = StringUtil.PadR( StringUtil.RTrim( AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3), 100, "%");
         lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 = StringUtil.PadR( StringUtil.RTrim( AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3), 100, "%");
         lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 = StringUtil.PadR( StringUtil.RTrim( AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3), 100, "%");
         lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 = StringUtil.PadR( StringUtil.RTrim( AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3), 100, "%");
         lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 = StringUtil.Concat( StringUtil.RTrim( AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4), "%", "");
         lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 = StringUtil.Concat( StringUtil.RTrim( AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4), "%", "");
         lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 = StringUtil.PadR( StringUtil.RTrim( AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4), 100, "%");
         lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 = StringUtil.PadR( StringUtil.RTrim( AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4), 100, "%");
         lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 = StringUtil.PadR( StringUtil.RTrim( AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4), 100, "%");
         lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 = StringUtil.PadR( StringUtil.RTrim( AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4), 100, "%");
         lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom = StringUtil.PadR( StringUtil.RTrim( AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom), 100, "%");
         lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto = StringUtil.Concat( StringUtil.RTrim( AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto), "%", "");
         lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host = StringUtil.PadR( StringUtil.RTrim( AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host), 50, "%");
         lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user = StringUtil.PadR( StringUtil.RTrim( AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user), 50, "%");
         /* Using cursor P00VG10 */
         pr_default.execute(2, new Object[] {AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios, AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios, AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to, AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to, AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas, AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas, AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to, AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to, AV9WWPContext.gxTpr_Areatrabalho_codigo, AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1, AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1, lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1, lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1, lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1, lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1, lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1, lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1, AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2, AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2, lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2, lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2, lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2, lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2, lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2, lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2, AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3, AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3, lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3, lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3, lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3, lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3, lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3, lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3, AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4, AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4, lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4, lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4, lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4, lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4, lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4, lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4, AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora, AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to, lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom, AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel, lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto, AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel, lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host, AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel, lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user, AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel, AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port, AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKVG6 = false;
            A1413ContagemResultadoNotificacao_UsuCod = P00VG10_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = P00VG10_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = P00VG10_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A1414ContagemResultadoNotificacao_DestCod = P00VG10_A1414ContagemResultadoNotificacao_DestCod[0];
            A1426ContagemResultadoNotificacao_DestPesCod = P00VG10_A1426ContagemResultadoNotificacao_DestPesCod[0];
            n1426ContagemResultadoNotificacao_DestPesCod = P00VG10_n1426ContagemResultadoNotificacao_DestPesCod[0];
            A1966ContagemResultadoNotificacao_AreaTrabalhoCod = P00VG10_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            n1966ContagemResultadoNotificacao_AreaTrabalhoCod = P00VG10_n1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            A1956ContagemResultadoNotificacao_Host = P00VG10_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = P00VG10_n1956ContagemResultadoNotificacao_Host[0];
            A1412ContagemResultadoNotificacao_Codigo = P00VG10_A1412ContagemResultadoNotificacao_Codigo[0];
            A1961ContagemResultadoNotificacao_Logged = P00VG10_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = P00VG10_n1961ContagemResultadoNotificacao_Logged[0];
            A1960ContagemResultadoNotificacao_Sec = P00VG10_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = P00VG10_n1960ContagemResultadoNotificacao_Sec[0];
            A1959ContagemResultadoNotificacao_Aut = P00VG10_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = P00VG10_n1959ContagemResultadoNotificacao_Aut[0];
            A1958ContagemResultadoNotificacao_Port = P00VG10_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = P00VG10_n1958ContagemResultadoNotificacao_Port[0];
            A1957ContagemResultadoNotificacao_User = P00VG10_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = P00VG10_n1957ContagemResultadoNotificacao_User[0];
            A1421ContagemResultadoNotificacao_DestNome = P00VG10_A1421ContagemResultadoNotificacao_DestNome[0];
            n1421ContagemResultadoNotificacao_DestNome = P00VG10_n1421ContagemResultadoNotificacao_DestNome[0];
            A1422ContagemResultadoNotificacao_UsuNom = P00VG10_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = P00VG10_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1417ContagemResultadoNotificacao_Assunto = P00VG10_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = P00VG10_n1417ContagemResultadoNotificacao_Assunto[0];
            A1416ContagemResultadoNotificacao_DataHora = P00VG10_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = P00VG10_n1416ContagemResultadoNotificacao_DataHora[0];
            A1963ContagemResultadoNotificacao_Demandas = P00VG10_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = P00VG10_n1963ContagemResultadoNotificacao_Demandas[0];
            A1962ContagemResultadoNotificacao_Destinatarios = P00VG10_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = P00VG10_n1962ContagemResultadoNotificacao_Destinatarios[0];
            A1426ContagemResultadoNotificacao_DestPesCod = P00VG10_A1426ContagemResultadoNotificacao_DestPesCod[0];
            n1426ContagemResultadoNotificacao_DestPesCod = P00VG10_n1426ContagemResultadoNotificacao_DestPesCod[0];
            A1421ContagemResultadoNotificacao_DestNome = P00VG10_A1421ContagemResultadoNotificacao_DestNome[0];
            n1421ContagemResultadoNotificacao_DestNome = P00VG10_n1421ContagemResultadoNotificacao_DestNome[0];
            A1413ContagemResultadoNotificacao_UsuCod = P00VG10_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1966ContagemResultadoNotificacao_AreaTrabalhoCod = P00VG10_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            n1966ContagemResultadoNotificacao_AreaTrabalhoCod = P00VG10_n1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            A1956ContagemResultadoNotificacao_Host = P00VG10_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = P00VG10_n1956ContagemResultadoNotificacao_Host[0];
            A1961ContagemResultadoNotificacao_Logged = P00VG10_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = P00VG10_n1961ContagemResultadoNotificacao_Logged[0];
            A1960ContagemResultadoNotificacao_Sec = P00VG10_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = P00VG10_n1960ContagemResultadoNotificacao_Sec[0];
            A1959ContagemResultadoNotificacao_Aut = P00VG10_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = P00VG10_n1959ContagemResultadoNotificacao_Aut[0];
            A1958ContagemResultadoNotificacao_Port = P00VG10_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = P00VG10_n1958ContagemResultadoNotificacao_Port[0];
            A1957ContagemResultadoNotificacao_User = P00VG10_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = P00VG10_n1957ContagemResultadoNotificacao_User[0];
            A1417ContagemResultadoNotificacao_Assunto = P00VG10_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = P00VG10_n1417ContagemResultadoNotificacao_Assunto[0];
            A1416ContagemResultadoNotificacao_DataHora = P00VG10_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = P00VG10_n1416ContagemResultadoNotificacao_DataHora[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = P00VG10_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = P00VG10_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A1422ContagemResultadoNotificacao_UsuNom = P00VG10_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = P00VG10_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1963ContagemResultadoNotificacao_Demandas = P00VG10_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = P00VG10_n1963ContagemResultadoNotificacao_Demandas[0];
            A1962ContagemResultadoNotificacao_Destinatarios = P00VG10_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = P00VG10_n1962ContagemResultadoNotificacao_Destinatarios[0];
            AV43count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00VG10_A1956ContagemResultadoNotificacao_Host[0], A1956ContagemResultadoNotificacao_Host) == 0 ) )
            {
               BRKVG6 = false;
               A1414ContagemResultadoNotificacao_DestCod = P00VG10_A1414ContagemResultadoNotificacao_DestCod[0];
               A1412ContagemResultadoNotificacao_Codigo = P00VG10_A1412ContagemResultadoNotificacao_Codigo[0];
               AV43count = (long)(AV43count+1);
               BRKVG6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1956ContagemResultadoNotificacao_Host)) )
            {
               AV35Option = A1956ContagemResultadoNotificacao_Host;
               AV36Options.Add(AV35Option, 0);
               AV41OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV43count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV36Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKVG6 )
            {
               BRKVG6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTAGEMRESULTADONOTIFICACAO_USEROPTIONS' Routine */
         AV22TFContagemResultadoNotificacao_User = AV31SearchTxt;
         AV23TFContagemResultadoNotificacao_User_Sel = "";
         AV86WWContagemResultadoNotificacaoDS_1_Contagemresultadonotificacao_areatrabalhocod = AV49ContagemResultadoNotificacao_AreaTrabalhoCod;
         AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1 = AV50DynamicFiltersSelector1;
         AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 = AV51DynamicFiltersOperator1;
         AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1 = AV52ContagemResultadoNotificacao_DataHora1;
         AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1 = AV53ContagemResultadoNotificacao_DataHora_To1;
         AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 = AV54ContagemResultadoNotificacao_Assunto1;
         AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 = AV55ContagemResultadoNotificacao_UsuNom1;
         AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 = AV56ContagemResultadoNotificacao_DestNome1;
         AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 = AV57DynamicFiltersEnabled2;
         AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2 = AV58DynamicFiltersSelector2;
         AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 = AV59DynamicFiltersOperator2;
         AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2 = AV60ContagemResultadoNotificacao_DataHora2;
         AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2 = AV61ContagemResultadoNotificacao_DataHora_To2;
         AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 = AV62ContagemResultadoNotificacao_Assunto2;
         AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 = AV63ContagemResultadoNotificacao_UsuNom2;
         AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 = AV64ContagemResultadoNotificacao_DestNome2;
         AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 = AV65DynamicFiltersEnabled3;
         AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3 = AV66DynamicFiltersSelector3;
         AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 = AV67DynamicFiltersOperator3;
         AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3 = AV68ContagemResultadoNotificacao_DataHora3;
         AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3 = AV69ContagemResultadoNotificacao_DataHora_To3;
         AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 = AV70ContagemResultadoNotificacao_Assunto3;
         AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 = AV71ContagemResultadoNotificacao_UsuNom3;
         AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 = AV72ContagemResultadoNotificacao_DestNome3;
         AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 = AV73DynamicFiltersEnabled4;
         AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4 = AV74DynamicFiltersSelector4;
         AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 = AV75DynamicFiltersOperator4;
         AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4 = AV76ContagemResultadoNotificacao_DataHora4;
         AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4 = AV77ContagemResultadoNotificacao_DataHora_To4;
         AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 = AV78ContagemResultadoNotificacao_Assunto4;
         AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 = AV79ContagemResultadoNotificacao_UsuNom4;
         AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 = AV80ContagemResultadoNotificacao_DestNome4;
         AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora = AV10TFContagemResultadoNotificacao_DataHora;
         AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to = AV11TFContagemResultadoNotificacao_DataHora_To;
         AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom = AV12TFContagemResultadoNotificacao_UsuNom;
         AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel = AV13TFContagemResultadoNotificacao_UsuNom_Sel;
         AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto = AV14TFContagemResultadoNotificacao_Assunto;
         AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel = AV15TFContagemResultadoNotificacao_Assunto_Sel;
         AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios = AV16TFContagemResultadoNotificacao_Destinatarios;
         AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to = AV17TFContagemResultadoNotificacao_Destinatarios_To;
         AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas = AV18TFContagemResultadoNotificacao_Demandas;
         AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to = AV19TFContagemResultadoNotificacao_Demandas_To;
         AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host = AV20TFContagemResultadoNotificacao_Host;
         AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel = AV21TFContagemResultadoNotificacao_Host_Sel;
         AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user = AV22TFContagemResultadoNotificacao_User;
         AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel = AV23TFContagemResultadoNotificacao_User_Sel;
         AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port = AV24TFContagemResultadoNotificacao_Port;
         AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to = AV25TFContagemResultadoNotificacao_Port_To;
         AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel = AV26TFContagemResultadoNotificacao_Aut_Sel;
         AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels = AV28TFContagemResultadoNotificacao_Sec_Sels;
         AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels = AV30TFContagemResultadoNotificacao_Logged_Sels;
         AV137Udparg52 = new prc_notificaoesparadestnome(context).executeUdp(  AV51DynamicFiltersOperator1,  AV56ContagemResultadoNotificacao_DestNome1,  AV59DynamicFiltersOperator2,  AV64ContagemResultadoNotificacao_DestNome2,  AV67DynamicFiltersOperator3,  AV72ContagemResultadoNotificacao_DestNome3,  AV75DynamicFiltersOperator4,  AV80ContagemResultadoNotificacao_DestNome4);
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A1960ContagemResultadoNotificacao_Sec ,
                                              AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels ,
                                              A1961ContagemResultadoNotificacao_Logged ,
                                              AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels ,
                                              A1412ContagemResultadoNotificacao_Codigo ,
                                              AV137Udparg52 ,
                                              AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1 ,
                                              AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1 ,
                                              AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1 ,
                                              AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 ,
                                              AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 ,
                                              AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 ,
                                              AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 ,
                                              AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2 ,
                                              AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2 ,
                                              AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2 ,
                                              AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 ,
                                              AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 ,
                                              AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 ,
                                              AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 ,
                                              AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3 ,
                                              AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3 ,
                                              AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3 ,
                                              AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 ,
                                              AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 ,
                                              AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 ,
                                              AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 ,
                                              AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4 ,
                                              AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4 ,
                                              AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4 ,
                                              AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 ,
                                              AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 ,
                                              AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 ,
                                              AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora ,
                                              AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to ,
                                              AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel ,
                                              AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom ,
                                              AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel ,
                                              AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto ,
                                              AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel ,
                                              AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host ,
                                              AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel ,
                                              AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user ,
                                              AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port ,
                                              AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to ,
                                              AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel ,
                                              AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels.Count ,
                                              AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels.Count ,
                                              AV50DynamicFiltersSelector1 ,
                                              AV56ContagemResultadoNotificacao_DestNome1 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV64ContagemResultadoNotificacao_DestNome2 ,
                                              AV66DynamicFiltersSelector3 ,
                                              AV72ContagemResultadoNotificacao_DestNome3 ,
                                              AV74DynamicFiltersSelector4 ,
                                              AV80ContagemResultadoNotificacao_DestNome4 ,
                                              A1416ContagemResultadoNotificacao_DataHora ,
                                              A1417ContagemResultadoNotificacao_Assunto ,
                                              A1422ContagemResultadoNotificacao_UsuNom ,
                                              A1421ContagemResultadoNotificacao_DestNome ,
                                              AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 ,
                                              AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 ,
                                              AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 ,
                                              AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 ,
                                              A1956ContagemResultadoNotificacao_Host ,
                                              A1957ContagemResultadoNotificacao_User ,
                                              A1958ContagemResultadoNotificacao_Port ,
                                              A1959ContagemResultadoNotificacao_Aut ,
                                              AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios ,
                                              A1962ContagemResultadoNotificacao_Destinatarios ,
                                              AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to ,
                                              AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas ,
                                              A1963ContagemResultadoNotificacao_Demandas ,
                                              AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to ,
                                              A1966ContagemResultadoNotificacao_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.LONG, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 = StringUtil.Concat( StringUtil.RTrim( AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1), "%", "");
         lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 = StringUtil.Concat( StringUtil.RTrim( AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1), "%", "");
         lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 = StringUtil.PadR( StringUtil.RTrim( AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1), 100, "%");
         lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 = StringUtil.PadR( StringUtil.RTrim( AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1), 100, "%");
         lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 = StringUtil.PadR( StringUtil.RTrim( AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1), 100, "%");
         lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 = StringUtil.PadR( StringUtil.RTrim( AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1), 100, "%");
         lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 = StringUtil.Concat( StringUtil.RTrim( AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2), "%", "");
         lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 = StringUtil.Concat( StringUtil.RTrim( AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2), "%", "");
         lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 = StringUtil.PadR( StringUtil.RTrim( AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2), 100, "%");
         lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 = StringUtil.PadR( StringUtil.RTrim( AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2), 100, "%");
         lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 = StringUtil.PadR( StringUtil.RTrim( AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2), 100, "%");
         lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 = StringUtil.PadR( StringUtil.RTrim( AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2), 100, "%");
         lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 = StringUtil.Concat( StringUtil.RTrim( AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3), "%", "");
         lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 = StringUtil.Concat( StringUtil.RTrim( AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3), "%", "");
         lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 = StringUtil.PadR( StringUtil.RTrim( AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3), 100, "%");
         lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 = StringUtil.PadR( StringUtil.RTrim( AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3), 100, "%");
         lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 = StringUtil.PadR( StringUtil.RTrim( AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3), 100, "%");
         lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 = StringUtil.PadR( StringUtil.RTrim( AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3), 100, "%");
         lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 = StringUtil.Concat( StringUtil.RTrim( AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4), "%", "");
         lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 = StringUtil.Concat( StringUtil.RTrim( AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4), "%", "");
         lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 = StringUtil.PadR( StringUtil.RTrim( AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4), 100, "%");
         lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 = StringUtil.PadR( StringUtil.RTrim( AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4), 100, "%");
         lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 = StringUtil.PadR( StringUtil.RTrim( AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4), 100, "%");
         lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 = StringUtil.PadR( StringUtil.RTrim( AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4), 100, "%");
         lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom = StringUtil.PadR( StringUtil.RTrim( AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom), 100, "%");
         lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto = StringUtil.Concat( StringUtil.RTrim( AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto), "%", "");
         lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host = StringUtil.PadR( StringUtil.RTrim( AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host), 50, "%");
         lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user = StringUtil.PadR( StringUtil.RTrim( AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user), 50, "%");
         /* Using cursor P00VG13 */
         pr_default.execute(3, new Object[] {AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios, AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios, AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to, AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to, AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas, AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas, AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to, AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to, AV9WWPContext.gxTpr_Areatrabalho_codigo, AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1, AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1, lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1, lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1, lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1, lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1, lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1, lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1, AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2, AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2, lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2, lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2, lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2, lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2, lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2, lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2, AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3, AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3, lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3, lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3, lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3, lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3, lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3, lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3, AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4, AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4, lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4, lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4, lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4, lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4, lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4, lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4, AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora, AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to, lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom, AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel, lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto, AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel, lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host, AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel, lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user, AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel, AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port, AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKVG8 = false;
            A1413ContagemResultadoNotificacao_UsuCod = P00VG13_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = P00VG13_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = P00VG13_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A1414ContagemResultadoNotificacao_DestCod = P00VG13_A1414ContagemResultadoNotificacao_DestCod[0];
            A1426ContagemResultadoNotificacao_DestPesCod = P00VG13_A1426ContagemResultadoNotificacao_DestPesCod[0];
            n1426ContagemResultadoNotificacao_DestPesCod = P00VG13_n1426ContagemResultadoNotificacao_DestPesCod[0];
            A1966ContagemResultadoNotificacao_AreaTrabalhoCod = P00VG13_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            n1966ContagemResultadoNotificacao_AreaTrabalhoCod = P00VG13_n1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            A1957ContagemResultadoNotificacao_User = P00VG13_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = P00VG13_n1957ContagemResultadoNotificacao_User[0];
            A1412ContagemResultadoNotificacao_Codigo = P00VG13_A1412ContagemResultadoNotificacao_Codigo[0];
            A1961ContagemResultadoNotificacao_Logged = P00VG13_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = P00VG13_n1961ContagemResultadoNotificacao_Logged[0];
            A1960ContagemResultadoNotificacao_Sec = P00VG13_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = P00VG13_n1960ContagemResultadoNotificacao_Sec[0];
            A1959ContagemResultadoNotificacao_Aut = P00VG13_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = P00VG13_n1959ContagemResultadoNotificacao_Aut[0];
            A1958ContagemResultadoNotificacao_Port = P00VG13_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = P00VG13_n1958ContagemResultadoNotificacao_Port[0];
            A1956ContagemResultadoNotificacao_Host = P00VG13_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = P00VG13_n1956ContagemResultadoNotificacao_Host[0];
            A1421ContagemResultadoNotificacao_DestNome = P00VG13_A1421ContagemResultadoNotificacao_DestNome[0];
            n1421ContagemResultadoNotificacao_DestNome = P00VG13_n1421ContagemResultadoNotificacao_DestNome[0];
            A1422ContagemResultadoNotificacao_UsuNom = P00VG13_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = P00VG13_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1417ContagemResultadoNotificacao_Assunto = P00VG13_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = P00VG13_n1417ContagemResultadoNotificacao_Assunto[0];
            A1416ContagemResultadoNotificacao_DataHora = P00VG13_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = P00VG13_n1416ContagemResultadoNotificacao_DataHora[0];
            A1963ContagemResultadoNotificacao_Demandas = P00VG13_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = P00VG13_n1963ContagemResultadoNotificacao_Demandas[0];
            A1962ContagemResultadoNotificacao_Destinatarios = P00VG13_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = P00VG13_n1962ContagemResultadoNotificacao_Destinatarios[0];
            A1426ContagemResultadoNotificacao_DestPesCod = P00VG13_A1426ContagemResultadoNotificacao_DestPesCod[0];
            n1426ContagemResultadoNotificacao_DestPesCod = P00VG13_n1426ContagemResultadoNotificacao_DestPesCod[0];
            A1421ContagemResultadoNotificacao_DestNome = P00VG13_A1421ContagemResultadoNotificacao_DestNome[0];
            n1421ContagemResultadoNotificacao_DestNome = P00VG13_n1421ContagemResultadoNotificacao_DestNome[0];
            A1413ContagemResultadoNotificacao_UsuCod = P00VG13_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1966ContagemResultadoNotificacao_AreaTrabalhoCod = P00VG13_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            n1966ContagemResultadoNotificacao_AreaTrabalhoCod = P00VG13_n1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
            A1957ContagemResultadoNotificacao_User = P00VG13_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = P00VG13_n1957ContagemResultadoNotificacao_User[0];
            A1961ContagemResultadoNotificacao_Logged = P00VG13_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = P00VG13_n1961ContagemResultadoNotificacao_Logged[0];
            A1960ContagemResultadoNotificacao_Sec = P00VG13_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = P00VG13_n1960ContagemResultadoNotificacao_Sec[0];
            A1959ContagemResultadoNotificacao_Aut = P00VG13_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = P00VG13_n1959ContagemResultadoNotificacao_Aut[0];
            A1958ContagemResultadoNotificacao_Port = P00VG13_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = P00VG13_n1958ContagemResultadoNotificacao_Port[0];
            A1956ContagemResultadoNotificacao_Host = P00VG13_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = P00VG13_n1956ContagemResultadoNotificacao_Host[0];
            A1417ContagemResultadoNotificacao_Assunto = P00VG13_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = P00VG13_n1417ContagemResultadoNotificacao_Assunto[0];
            A1416ContagemResultadoNotificacao_DataHora = P00VG13_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = P00VG13_n1416ContagemResultadoNotificacao_DataHora[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = P00VG13_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = P00VG13_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A1422ContagemResultadoNotificacao_UsuNom = P00VG13_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = P00VG13_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1963ContagemResultadoNotificacao_Demandas = P00VG13_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = P00VG13_n1963ContagemResultadoNotificacao_Demandas[0];
            A1962ContagemResultadoNotificacao_Destinatarios = P00VG13_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = P00VG13_n1962ContagemResultadoNotificacao_Destinatarios[0];
            AV43count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00VG13_A1957ContagemResultadoNotificacao_User[0], A1957ContagemResultadoNotificacao_User) == 0 ) )
            {
               BRKVG8 = false;
               A1414ContagemResultadoNotificacao_DestCod = P00VG13_A1414ContagemResultadoNotificacao_DestCod[0];
               A1412ContagemResultadoNotificacao_Codigo = P00VG13_A1412ContagemResultadoNotificacao_Codigo[0];
               AV43count = (long)(AV43count+1);
               BRKVG8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1957ContagemResultadoNotificacao_User)) )
            {
               AV35Option = A1957ContagemResultadoNotificacao_User;
               AV36Options.Add(AV35Option, 0);
               AV41OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV43count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV36Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKVG8 )
            {
               BRKVG8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV36Options = new GxSimpleCollection();
         AV39OptionsDesc = new GxSimpleCollection();
         AV41OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV44Session = context.GetSession();
         AV46GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV47GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         AV11TFContagemResultadoNotificacao_DataHora_To = (DateTime)(DateTime.MinValue);
         AV12TFContagemResultadoNotificacao_UsuNom = "";
         AV13TFContagemResultadoNotificacao_UsuNom_Sel = "";
         AV14TFContagemResultadoNotificacao_Assunto = "";
         AV15TFContagemResultadoNotificacao_Assunto_Sel = "";
         AV20TFContagemResultadoNotificacao_Host = "";
         AV21TFContagemResultadoNotificacao_Host_Sel = "";
         AV22TFContagemResultadoNotificacao_User = "";
         AV23TFContagemResultadoNotificacao_User_Sel = "";
         AV27TFContagemResultadoNotificacao_Sec_SelsJson = "";
         AV28TFContagemResultadoNotificacao_Sec_Sels = new GxSimpleCollection();
         AV29TFContagemResultadoNotificacao_Logged_SelsJson = "";
         AV30TFContagemResultadoNotificacao_Logged_Sels = new GxSimpleCollection();
         AV48GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV50DynamicFiltersSelector1 = "";
         AV52ContagemResultadoNotificacao_DataHora1 = (DateTime)(DateTime.MinValue);
         AV53ContagemResultadoNotificacao_DataHora_To1 = (DateTime)(DateTime.MinValue);
         AV54ContagemResultadoNotificacao_Assunto1 = "";
         AV55ContagemResultadoNotificacao_UsuNom1 = "";
         AV56ContagemResultadoNotificacao_DestNome1 = "";
         AV58DynamicFiltersSelector2 = "";
         AV60ContagemResultadoNotificacao_DataHora2 = (DateTime)(DateTime.MinValue);
         AV61ContagemResultadoNotificacao_DataHora_To2 = (DateTime)(DateTime.MinValue);
         AV62ContagemResultadoNotificacao_Assunto2 = "";
         AV63ContagemResultadoNotificacao_UsuNom2 = "";
         AV64ContagemResultadoNotificacao_DestNome2 = "";
         AV66DynamicFiltersSelector3 = "";
         AV68ContagemResultadoNotificacao_DataHora3 = (DateTime)(DateTime.MinValue);
         AV69ContagemResultadoNotificacao_DataHora_To3 = (DateTime)(DateTime.MinValue);
         AV70ContagemResultadoNotificacao_Assunto3 = "";
         AV71ContagemResultadoNotificacao_UsuNom3 = "";
         AV72ContagemResultadoNotificacao_DestNome3 = "";
         AV74DynamicFiltersSelector4 = "";
         AV76ContagemResultadoNotificacao_DataHora4 = (DateTime)(DateTime.MinValue);
         AV77ContagemResultadoNotificacao_DataHora_To4 = (DateTime)(DateTime.MinValue);
         AV78ContagemResultadoNotificacao_Assunto4 = "";
         AV79ContagemResultadoNotificacao_UsuNom4 = "";
         AV80ContagemResultadoNotificacao_DestNome4 = "";
         AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1 = "";
         AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1 = (DateTime)(DateTime.MinValue);
         AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1 = (DateTime)(DateTime.MinValue);
         AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 = "";
         AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 = "";
         AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 = "";
         AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2 = "";
         AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2 = (DateTime)(DateTime.MinValue);
         AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2 = (DateTime)(DateTime.MinValue);
         AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 = "";
         AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 = "";
         AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 = "";
         AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3 = "";
         AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3 = (DateTime)(DateTime.MinValue);
         AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3 = (DateTime)(DateTime.MinValue);
         AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 = "";
         AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 = "";
         AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 = "";
         AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4 = "";
         AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4 = (DateTime)(DateTime.MinValue);
         AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4 = (DateTime)(DateTime.MinValue);
         AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 = "";
         AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 = "";
         AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 = "";
         AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora = (DateTime)(DateTime.MinValue);
         AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to = (DateTime)(DateTime.MinValue);
         AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom = "";
         AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel = "";
         AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto = "";
         AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel = "";
         AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host = "";
         AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel = "";
         AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user = "";
         AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel = "";
         AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels = new GxSimpleCollection();
         AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels = new GxSimpleCollection();
         AV137Udparg52 = new GxSimpleCollection();
         scmdbuf = "";
         lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 = "";
         lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 = "";
         lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 = "";
         lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 = "";
         lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 = "";
         lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 = "";
         lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 = "";
         lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 = "";
         lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 = "";
         lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 = "";
         lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 = "";
         lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 = "";
         lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom = "";
         lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto = "";
         lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host = "";
         lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user = "";
         A1416ContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         A1417ContagemResultadoNotificacao_Assunto = "";
         A1422ContagemResultadoNotificacao_UsuNom = "";
         A1421ContagemResultadoNotificacao_DestNome = "";
         A1956ContagemResultadoNotificacao_Host = "";
         A1957ContagemResultadoNotificacao_User = "";
         P00VG4_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         P00VG4_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         P00VG4_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         P00VG4_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         P00VG4_A1426ContagemResultadoNotificacao_DestPesCod = new int[1] ;
         P00VG4_n1426ContagemResultadoNotificacao_DestPesCod = new bool[] {false} ;
         P00VG4_A1966ContagemResultadoNotificacao_AreaTrabalhoCod = new int[1] ;
         P00VG4_n1966ContagemResultadoNotificacao_AreaTrabalhoCod = new bool[] {false} ;
         P00VG4_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         P00VG4_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         P00VG4_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         P00VG4_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         P00VG4_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         P00VG4_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         P00VG4_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         P00VG4_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         P00VG4_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         P00VG4_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         P00VG4_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         P00VG4_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         P00VG4_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         P00VG4_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         P00VG4_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         P00VG4_A1421ContagemResultadoNotificacao_DestNome = new String[] {""} ;
         P00VG4_n1421ContagemResultadoNotificacao_DestNome = new bool[] {false} ;
         P00VG4_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         P00VG4_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         P00VG4_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00VG4_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         P00VG4_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         P00VG4_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         P00VG4_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         P00VG4_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         AV35Option = "";
         P00VG7_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         P00VG7_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         P00VG7_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         P00VG7_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         P00VG7_A1426ContagemResultadoNotificacao_DestPesCod = new int[1] ;
         P00VG7_n1426ContagemResultadoNotificacao_DestPesCod = new bool[] {false} ;
         P00VG7_A1966ContagemResultadoNotificacao_AreaTrabalhoCod = new int[1] ;
         P00VG7_n1966ContagemResultadoNotificacao_AreaTrabalhoCod = new bool[] {false} ;
         P00VG7_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         P00VG7_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         P00VG7_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         P00VG7_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         P00VG7_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         P00VG7_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         P00VG7_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         P00VG7_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         P00VG7_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         P00VG7_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         P00VG7_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         P00VG7_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         P00VG7_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         P00VG7_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         P00VG7_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         P00VG7_A1421ContagemResultadoNotificacao_DestNome = new String[] {""} ;
         P00VG7_n1421ContagemResultadoNotificacao_DestNome = new bool[] {false} ;
         P00VG7_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         P00VG7_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         P00VG7_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00VG7_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         P00VG7_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         P00VG7_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         P00VG7_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         P00VG7_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         P00VG10_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         P00VG10_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         P00VG10_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         P00VG10_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         P00VG10_A1426ContagemResultadoNotificacao_DestPesCod = new int[1] ;
         P00VG10_n1426ContagemResultadoNotificacao_DestPesCod = new bool[] {false} ;
         P00VG10_A1966ContagemResultadoNotificacao_AreaTrabalhoCod = new int[1] ;
         P00VG10_n1966ContagemResultadoNotificacao_AreaTrabalhoCod = new bool[] {false} ;
         P00VG10_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         P00VG10_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         P00VG10_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         P00VG10_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         P00VG10_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         P00VG10_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         P00VG10_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         P00VG10_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         P00VG10_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         P00VG10_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         P00VG10_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         P00VG10_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         P00VG10_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         P00VG10_A1421ContagemResultadoNotificacao_DestNome = new String[] {""} ;
         P00VG10_n1421ContagemResultadoNotificacao_DestNome = new bool[] {false} ;
         P00VG10_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         P00VG10_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         P00VG10_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         P00VG10_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         P00VG10_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00VG10_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         P00VG10_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         P00VG10_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         P00VG10_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         P00VG10_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         P00VG13_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         P00VG13_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         P00VG13_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         P00VG13_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         P00VG13_A1426ContagemResultadoNotificacao_DestPesCod = new int[1] ;
         P00VG13_n1426ContagemResultadoNotificacao_DestPesCod = new bool[] {false} ;
         P00VG13_A1966ContagemResultadoNotificacao_AreaTrabalhoCod = new int[1] ;
         P00VG13_n1966ContagemResultadoNotificacao_AreaTrabalhoCod = new bool[] {false} ;
         P00VG13_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         P00VG13_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         P00VG13_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         P00VG13_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         P00VG13_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         P00VG13_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         P00VG13_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         P00VG13_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         P00VG13_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         P00VG13_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         P00VG13_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         P00VG13_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         P00VG13_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         P00VG13_A1421ContagemResultadoNotificacao_DestNome = new String[] {""} ;
         P00VG13_n1421ContagemResultadoNotificacao_DestNome = new bool[] {false} ;
         P00VG13_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         P00VG13_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         P00VG13_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         P00VG13_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         P00VG13_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00VG13_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         P00VG13_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         P00VG13_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         P00VG13_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         P00VG13_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontagemresultadonotificacaofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00VG4_A1413ContagemResultadoNotificacao_UsuCod, P00VG4_A1427ContagemResultadoNotificacao_UsuPesCod, P00VG4_n1427ContagemResultadoNotificacao_UsuPesCod, P00VG4_A1414ContagemResultadoNotificacao_DestCod, P00VG4_A1426ContagemResultadoNotificacao_DestPesCod, P00VG4_n1426ContagemResultadoNotificacao_DestPesCod, P00VG4_A1966ContagemResultadoNotificacao_AreaTrabalhoCod, P00VG4_n1966ContagemResultadoNotificacao_AreaTrabalhoCod, P00VG4_A1422ContagemResultadoNotificacao_UsuNom, P00VG4_n1422ContagemResultadoNotificacao_UsuNom,
               P00VG4_A1412ContagemResultadoNotificacao_Codigo, P00VG4_A1961ContagemResultadoNotificacao_Logged, P00VG4_n1961ContagemResultadoNotificacao_Logged, P00VG4_A1960ContagemResultadoNotificacao_Sec, P00VG4_n1960ContagemResultadoNotificacao_Sec, P00VG4_A1959ContagemResultadoNotificacao_Aut, P00VG4_n1959ContagemResultadoNotificacao_Aut, P00VG4_A1958ContagemResultadoNotificacao_Port, P00VG4_n1958ContagemResultadoNotificacao_Port, P00VG4_A1957ContagemResultadoNotificacao_User,
               P00VG4_n1957ContagemResultadoNotificacao_User, P00VG4_A1956ContagemResultadoNotificacao_Host, P00VG4_n1956ContagemResultadoNotificacao_Host, P00VG4_A1421ContagemResultadoNotificacao_DestNome, P00VG4_n1421ContagemResultadoNotificacao_DestNome, P00VG4_A1417ContagemResultadoNotificacao_Assunto, P00VG4_n1417ContagemResultadoNotificacao_Assunto, P00VG4_A1416ContagemResultadoNotificacao_DataHora, P00VG4_n1416ContagemResultadoNotificacao_DataHora, P00VG4_A1963ContagemResultadoNotificacao_Demandas,
               P00VG4_n1963ContagemResultadoNotificacao_Demandas, P00VG4_A1962ContagemResultadoNotificacao_Destinatarios, P00VG4_n1962ContagemResultadoNotificacao_Destinatarios
               }
               , new Object[] {
               P00VG7_A1413ContagemResultadoNotificacao_UsuCod, P00VG7_A1427ContagemResultadoNotificacao_UsuPesCod, P00VG7_n1427ContagemResultadoNotificacao_UsuPesCod, P00VG7_A1414ContagemResultadoNotificacao_DestCod, P00VG7_A1426ContagemResultadoNotificacao_DestPesCod, P00VG7_n1426ContagemResultadoNotificacao_DestPesCod, P00VG7_A1966ContagemResultadoNotificacao_AreaTrabalhoCod, P00VG7_n1966ContagemResultadoNotificacao_AreaTrabalhoCod, P00VG7_A1417ContagemResultadoNotificacao_Assunto, P00VG7_n1417ContagemResultadoNotificacao_Assunto,
               P00VG7_A1412ContagemResultadoNotificacao_Codigo, P00VG7_A1961ContagemResultadoNotificacao_Logged, P00VG7_n1961ContagemResultadoNotificacao_Logged, P00VG7_A1960ContagemResultadoNotificacao_Sec, P00VG7_n1960ContagemResultadoNotificacao_Sec, P00VG7_A1959ContagemResultadoNotificacao_Aut, P00VG7_n1959ContagemResultadoNotificacao_Aut, P00VG7_A1958ContagemResultadoNotificacao_Port, P00VG7_n1958ContagemResultadoNotificacao_Port, P00VG7_A1957ContagemResultadoNotificacao_User,
               P00VG7_n1957ContagemResultadoNotificacao_User, P00VG7_A1956ContagemResultadoNotificacao_Host, P00VG7_n1956ContagemResultadoNotificacao_Host, P00VG7_A1421ContagemResultadoNotificacao_DestNome, P00VG7_n1421ContagemResultadoNotificacao_DestNome, P00VG7_A1422ContagemResultadoNotificacao_UsuNom, P00VG7_n1422ContagemResultadoNotificacao_UsuNom, P00VG7_A1416ContagemResultadoNotificacao_DataHora, P00VG7_n1416ContagemResultadoNotificacao_DataHora, P00VG7_A1963ContagemResultadoNotificacao_Demandas,
               P00VG7_n1963ContagemResultadoNotificacao_Demandas, P00VG7_A1962ContagemResultadoNotificacao_Destinatarios, P00VG7_n1962ContagemResultadoNotificacao_Destinatarios
               }
               , new Object[] {
               P00VG10_A1413ContagemResultadoNotificacao_UsuCod, P00VG10_A1427ContagemResultadoNotificacao_UsuPesCod, P00VG10_n1427ContagemResultadoNotificacao_UsuPesCod, P00VG10_A1414ContagemResultadoNotificacao_DestCod, P00VG10_A1426ContagemResultadoNotificacao_DestPesCod, P00VG10_n1426ContagemResultadoNotificacao_DestPesCod, P00VG10_A1966ContagemResultadoNotificacao_AreaTrabalhoCod, P00VG10_n1966ContagemResultadoNotificacao_AreaTrabalhoCod, P00VG10_A1956ContagemResultadoNotificacao_Host, P00VG10_n1956ContagemResultadoNotificacao_Host,
               P00VG10_A1412ContagemResultadoNotificacao_Codigo, P00VG10_A1961ContagemResultadoNotificacao_Logged, P00VG10_n1961ContagemResultadoNotificacao_Logged, P00VG10_A1960ContagemResultadoNotificacao_Sec, P00VG10_n1960ContagemResultadoNotificacao_Sec, P00VG10_A1959ContagemResultadoNotificacao_Aut, P00VG10_n1959ContagemResultadoNotificacao_Aut, P00VG10_A1958ContagemResultadoNotificacao_Port, P00VG10_n1958ContagemResultadoNotificacao_Port, P00VG10_A1957ContagemResultadoNotificacao_User,
               P00VG10_n1957ContagemResultadoNotificacao_User, P00VG10_A1421ContagemResultadoNotificacao_DestNome, P00VG10_n1421ContagemResultadoNotificacao_DestNome, P00VG10_A1422ContagemResultadoNotificacao_UsuNom, P00VG10_n1422ContagemResultadoNotificacao_UsuNom, P00VG10_A1417ContagemResultadoNotificacao_Assunto, P00VG10_n1417ContagemResultadoNotificacao_Assunto, P00VG10_A1416ContagemResultadoNotificacao_DataHora, P00VG10_n1416ContagemResultadoNotificacao_DataHora, P00VG10_A1963ContagemResultadoNotificacao_Demandas,
               P00VG10_n1963ContagemResultadoNotificacao_Demandas, P00VG10_A1962ContagemResultadoNotificacao_Destinatarios, P00VG10_n1962ContagemResultadoNotificacao_Destinatarios
               }
               , new Object[] {
               P00VG13_A1413ContagemResultadoNotificacao_UsuCod, P00VG13_A1427ContagemResultadoNotificacao_UsuPesCod, P00VG13_n1427ContagemResultadoNotificacao_UsuPesCod, P00VG13_A1414ContagemResultadoNotificacao_DestCod, P00VG13_A1426ContagemResultadoNotificacao_DestPesCod, P00VG13_n1426ContagemResultadoNotificacao_DestPesCod, P00VG13_A1966ContagemResultadoNotificacao_AreaTrabalhoCod, P00VG13_n1966ContagemResultadoNotificacao_AreaTrabalhoCod, P00VG13_A1957ContagemResultadoNotificacao_User, P00VG13_n1957ContagemResultadoNotificacao_User,
               P00VG13_A1412ContagemResultadoNotificacao_Codigo, P00VG13_A1961ContagemResultadoNotificacao_Logged, P00VG13_n1961ContagemResultadoNotificacao_Logged, P00VG13_A1960ContagemResultadoNotificacao_Sec, P00VG13_n1960ContagemResultadoNotificacao_Sec, P00VG13_A1959ContagemResultadoNotificacao_Aut, P00VG13_n1959ContagemResultadoNotificacao_Aut, P00VG13_A1958ContagemResultadoNotificacao_Port, P00VG13_n1958ContagemResultadoNotificacao_Port, P00VG13_A1956ContagemResultadoNotificacao_Host,
               P00VG13_n1956ContagemResultadoNotificacao_Host, P00VG13_A1421ContagemResultadoNotificacao_DestNome, P00VG13_n1421ContagemResultadoNotificacao_DestNome, P00VG13_A1422ContagemResultadoNotificacao_UsuNom, P00VG13_n1422ContagemResultadoNotificacao_UsuNom, P00VG13_A1417ContagemResultadoNotificacao_Assunto, P00VG13_n1417ContagemResultadoNotificacao_Assunto, P00VG13_A1416ContagemResultadoNotificacao_DataHora, P00VG13_n1416ContagemResultadoNotificacao_DataHora, P00VG13_A1963ContagemResultadoNotificacao_Demandas,
               P00VG13_n1963ContagemResultadoNotificacao_Demandas, P00VG13_A1962ContagemResultadoNotificacao_Destinatarios, P00VG13_n1962ContagemResultadoNotificacao_Destinatarios
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV24TFContagemResultadoNotificacao_Port ;
      private short AV25TFContagemResultadoNotificacao_Port_To ;
      private short AV26TFContagemResultadoNotificacao_Aut_Sel ;
      private short AV51DynamicFiltersOperator1 ;
      private short AV59DynamicFiltersOperator2 ;
      private short AV67DynamicFiltersOperator3 ;
      private short AV75DynamicFiltersOperator4 ;
      private short AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 ;
      private short AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 ;
      private short AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 ;
      private short AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 ;
      private short AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port ;
      private short AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to ;
      private short AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel ;
      private short A1960ContagemResultadoNotificacao_Sec ;
      private short A1961ContagemResultadoNotificacao_Logged ;
      private short A1958ContagemResultadoNotificacao_Port ;
      private int AV84GXV1 ;
      private int AV49ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private int AV16TFContagemResultadoNotificacao_Destinatarios ;
      private int AV17TFContagemResultadoNotificacao_Destinatarios_To ;
      private int AV18TFContagemResultadoNotificacao_Demandas ;
      private int AV19TFContagemResultadoNotificacao_Demandas_To ;
      private int AV86WWContagemResultadoNotificacaoDS_1_Contagemresultadonotificacao_areatrabalhocod ;
      private int AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios ;
      private int AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to ;
      private int AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas ;
      private int AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to ;
      private int AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels_Count ;
      private int AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels_Count ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A1962ContagemResultadoNotificacao_Destinatarios ;
      private int A1963ContagemResultadoNotificacao_Demandas ;
      private int A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private int A1413ContagemResultadoNotificacao_UsuCod ;
      private int A1427ContagemResultadoNotificacao_UsuPesCod ;
      private int A1414ContagemResultadoNotificacao_DestCod ;
      private int A1426ContagemResultadoNotificacao_DestPesCod ;
      private long A1412ContagemResultadoNotificacao_Codigo ;
      private long AV43count ;
      private String AV12TFContagemResultadoNotificacao_UsuNom ;
      private String AV13TFContagemResultadoNotificacao_UsuNom_Sel ;
      private String AV20TFContagemResultadoNotificacao_Host ;
      private String AV21TFContagemResultadoNotificacao_Host_Sel ;
      private String AV22TFContagemResultadoNotificacao_User ;
      private String AV23TFContagemResultadoNotificacao_User_Sel ;
      private String AV55ContagemResultadoNotificacao_UsuNom1 ;
      private String AV56ContagemResultadoNotificacao_DestNome1 ;
      private String AV63ContagemResultadoNotificacao_UsuNom2 ;
      private String AV64ContagemResultadoNotificacao_DestNome2 ;
      private String AV71ContagemResultadoNotificacao_UsuNom3 ;
      private String AV72ContagemResultadoNotificacao_DestNome3 ;
      private String AV79ContagemResultadoNotificacao_UsuNom4 ;
      private String AV80ContagemResultadoNotificacao_DestNome4 ;
      private String AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 ;
      private String AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 ;
      private String AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 ;
      private String AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 ;
      private String AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 ;
      private String AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 ;
      private String AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 ;
      private String AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 ;
      private String AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom ;
      private String AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel ;
      private String AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host ;
      private String AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel ;
      private String AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user ;
      private String AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel ;
      private String scmdbuf ;
      private String lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 ;
      private String lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 ;
      private String lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 ;
      private String lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 ;
      private String lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 ;
      private String lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 ;
      private String lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 ;
      private String lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 ;
      private String lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom ;
      private String lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host ;
      private String lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user ;
      private String A1422ContagemResultadoNotificacao_UsuNom ;
      private String A1421ContagemResultadoNotificacao_DestNome ;
      private String A1956ContagemResultadoNotificacao_Host ;
      private String A1957ContagemResultadoNotificacao_User ;
      private DateTime AV10TFContagemResultadoNotificacao_DataHora ;
      private DateTime AV11TFContagemResultadoNotificacao_DataHora_To ;
      private DateTime AV52ContagemResultadoNotificacao_DataHora1 ;
      private DateTime AV53ContagemResultadoNotificacao_DataHora_To1 ;
      private DateTime AV60ContagemResultadoNotificacao_DataHora2 ;
      private DateTime AV61ContagemResultadoNotificacao_DataHora_To2 ;
      private DateTime AV68ContagemResultadoNotificacao_DataHora3 ;
      private DateTime AV69ContagemResultadoNotificacao_DataHora_To3 ;
      private DateTime AV76ContagemResultadoNotificacao_DataHora4 ;
      private DateTime AV77ContagemResultadoNotificacao_DataHora_To4 ;
      private DateTime AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1 ;
      private DateTime AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1 ;
      private DateTime AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2 ;
      private DateTime AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2 ;
      private DateTime AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3 ;
      private DateTime AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3 ;
      private DateTime AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4 ;
      private DateTime AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4 ;
      private DateTime AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora ;
      private DateTime AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to ;
      private DateTime A1416ContagemResultadoNotificacao_DataHora ;
      private bool returnInSub ;
      private bool AV57DynamicFiltersEnabled2 ;
      private bool AV65DynamicFiltersEnabled3 ;
      private bool AV73DynamicFiltersEnabled4 ;
      private bool AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 ;
      private bool AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 ;
      private bool AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 ;
      private bool A1959ContagemResultadoNotificacao_Aut ;
      private bool BRKVG2 ;
      private bool n1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool n1426ContagemResultadoNotificacao_DestPesCod ;
      private bool n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool n1422ContagemResultadoNotificacao_UsuNom ;
      private bool n1961ContagemResultadoNotificacao_Logged ;
      private bool n1960ContagemResultadoNotificacao_Sec ;
      private bool n1959ContagemResultadoNotificacao_Aut ;
      private bool n1958ContagemResultadoNotificacao_Port ;
      private bool n1957ContagemResultadoNotificacao_User ;
      private bool n1956ContagemResultadoNotificacao_Host ;
      private bool n1421ContagemResultadoNotificacao_DestNome ;
      private bool n1417ContagemResultadoNotificacao_Assunto ;
      private bool n1416ContagemResultadoNotificacao_DataHora ;
      private bool n1963ContagemResultadoNotificacao_Demandas ;
      private bool n1962ContagemResultadoNotificacao_Destinatarios ;
      private bool BRKVG4 ;
      private bool BRKVG6 ;
      private bool BRKVG8 ;
      private String AV42OptionIndexesJson ;
      private String AV37OptionsJson ;
      private String AV40OptionsDescJson ;
      private String AV27TFContagemResultadoNotificacao_Sec_SelsJson ;
      private String AV29TFContagemResultadoNotificacao_Logged_SelsJson ;
      private String AV54ContagemResultadoNotificacao_Assunto1 ;
      private String AV62ContagemResultadoNotificacao_Assunto2 ;
      private String AV70ContagemResultadoNotificacao_Assunto3 ;
      private String AV78ContagemResultadoNotificacao_Assunto4 ;
      private String AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 ;
      private String AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 ;
      private String AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 ;
      private String AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 ;
      private String lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 ;
      private String lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 ;
      private String lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 ;
      private String lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 ;
      private String A1417ContagemResultadoNotificacao_Assunto ;
      private String AV33DDOName ;
      private String AV31SearchTxt ;
      private String AV32SearchTxtTo ;
      private String AV14TFContagemResultadoNotificacao_Assunto ;
      private String AV15TFContagemResultadoNotificacao_Assunto_Sel ;
      private String AV50DynamicFiltersSelector1 ;
      private String AV58DynamicFiltersSelector2 ;
      private String AV66DynamicFiltersSelector3 ;
      private String AV74DynamicFiltersSelector4 ;
      private String AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1 ;
      private String AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2 ;
      private String AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3 ;
      private String AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4 ;
      private String AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto ;
      private String AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel ;
      private String lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto ;
      private String AV35Option ;
      private IGxSession AV44Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00VG4_A1413ContagemResultadoNotificacao_UsuCod ;
      private int[] P00VG4_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] P00VG4_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private int[] P00VG4_A1414ContagemResultadoNotificacao_DestCod ;
      private int[] P00VG4_A1426ContagemResultadoNotificacao_DestPesCod ;
      private bool[] P00VG4_n1426ContagemResultadoNotificacao_DestPesCod ;
      private int[] P00VG4_A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool[] P00VG4_n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private String[] P00VG4_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] P00VG4_n1422ContagemResultadoNotificacao_UsuNom ;
      private long[] P00VG4_A1412ContagemResultadoNotificacao_Codigo ;
      private short[] P00VG4_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] P00VG4_n1961ContagemResultadoNotificacao_Logged ;
      private short[] P00VG4_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] P00VG4_n1960ContagemResultadoNotificacao_Sec ;
      private bool[] P00VG4_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] P00VG4_n1959ContagemResultadoNotificacao_Aut ;
      private short[] P00VG4_A1958ContagemResultadoNotificacao_Port ;
      private bool[] P00VG4_n1958ContagemResultadoNotificacao_Port ;
      private String[] P00VG4_A1957ContagemResultadoNotificacao_User ;
      private bool[] P00VG4_n1957ContagemResultadoNotificacao_User ;
      private String[] P00VG4_A1956ContagemResultadoNotificacao_Host ;
      private bool[] P00VG4_n1956ContagemResultadoNotificacao_Host ;
      private String[] P00VG4_A1421ContagemResultadoNotificacao_DestNome ;
      private bool[] P00VG4_n1421ContagemResultadoNotificacao_DestNome ;
      private String[] P00VG4_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] P00VG4_n1417ContagemResultadoNotificacao_Assunto ;
      private DateTime[] P00VG4_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] P00VG4_n1416ContagemResultadoNotificacao_DataHora ;
      private int[] P00VG4_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] P00VG4_n1963ContagemResultadoNotificacao_Demandas ;
      private int[] P00VG4_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] P00VG4_n1962ContagemResultadoNotificacao_Destinatarios ;
      private int[] P00VG7_A1413ContagemResultadoNotificacao_UsuCod ;
      private int[] P00VG7_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] P00VG7_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private int[] P00VG7_A1414ContagemResultadoNotificacao_DestCod ;
      private int[] P00VG7_A1426ContagemResultadoNotificacao_DestPesCod ;
      private bool[] P00VG7_n1426ContagemResultadoNotificacao_DestPesCod ;
      private int[] P00VG7_A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool[] P00VG7_n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private String[] P00VG7_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] P00VG7_n1417ContagemResultadoNotificacao_Assunto ;
      private long[] P00VG7_A1412ContagemResultadoNotificacao_Codigo ;
      private short[] P00VG7_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] P00VG7_n1961ContagemResultadoNotificacao_Logged ;
      private short[] P00VG7_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] P00VG7_n1960ContagemResultadoNotificacao_Sec ;
      private bool[] P00VG7_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] P00VG7_n1959ContagemResultadoNotificacao_Aut ;
      private short[] P00VG7_A1958ContagemResultadoNotificacao_Port ;
      private bool[] P00VG7_n1958ContagemResultadoNotificacao_Port ;
      private String[] P00VG7_A1957ContagemResultadoNotificacao_User ;
      private bool[] P00VG7_n1957ContagemResultadoNotificacao_User ;
      private String[] P00VG7_A1956ContagemResultadoNotificacao_Host ;
      private bool[] P00VG7_n1956ContagemResultadoNotificacao_Host ;
      private String[] P00VG7_A1421ContagemResultadoNotificacao_DestNome ;
      private bool[] P00VG7_n1421ContagemResultadoNotificacao_DestNome ;
      private String[] P00VG7_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] P00VG7_n1422ContagemResultadoNotificacao_UsuNom ;
      private DateTime[] P00VG7_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] P00VG7_n1416ContagemResultadoNotificacao_DataHora ;
      private int[] P00VG7_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] P00VG7_n1963ContagemResultadoNotificacao_Demandas ;
      private int[] P00VG7_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] P00VG7_n1962ContagemResultadoNotificacao_Destinatarios ;
      private int[] P00VG10_A1413ContagemResultadoNotificacao_UsuCod ;
      private int[] P00VG10_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] P00VG10_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private int[] P00VG10_A1414ContagemResultadoNotificacao_DestCod ;
      private int[] P00VG10_A1426ContagemResultadoNotificacao_DestPesCod ;
      private bool[] P00VG10_n1426ContagemResultadoNotificacao_DestPesCod ;
      private int[] P00VG10_A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool[] P00VG10_n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private String[] P00VG10_A1956ContagemResultadoNotificacao_Host ;
      private bool[] P00VG10_n1956ContagemResultadoNotificacao_Host ;
      private long[] P00VG10_A1412ContagemResultadoNotificacao_Codigo ;
      private short[] P00VG10_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] P00VG10_n1961ContagemResultadoNotificacao_Logged ;
      private short[] P00VG10_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] P00VG10_n1960ContagemResultadoNotificacao_Sec ;
      private bool[] P00VG10_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] P00VG10_n1959ContagemResultadoNotificacao_Aut ;
      private short[] P00VG10_A1958ContagemResultadoNotificacao_Port ;
      private bool[] P00VG10_n1958ContagemResultadoNotificacao_Port ;
      private String[] P00VG10_A1957ContagemResultadoNotificacao_User ;
      private bool[] P00VG10_n1957ContagemResultadoNotificacao_User ;
      private String[] P00VG10_A1421ContagemResultadoNotificacao_DestNome ;
      private bool[] P00VG10_n1421ContagemResultadoNotificacao_DestNome ;
      private String[] P00VG10_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] P00VG10_n1422ContagemResultadoNotificacao_UsuNom ;
      private String[] P00VG10_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] P00VG10_n1417ContagemResultadoNotificacao_Assunto ;
      private DateTime[] P00VG10_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] P00VG10_n1416ContagemResultadoNotificacao_DataHora ;
      private int[] P00VG10_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] P00VG10_n1963ContagemResultadoNotificacao_Demandas ;
      private int[] P00VG10_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] P00VG10_n1962ContagemResultadoNotificacao_Destinatarios ;
      private int[] P00VG13_A1413ContagemResultadoNotificacao_UsuCod ;
      private int[] P00VG13_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] P00VG13_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private int[] P00VG13_A1414ContagemResultadoNotificacao_DestCod ;
      private int[] P00VG13_A1426ContagemResultadoNotificacao_DestPesCod ;
      private bool[] P00VG13_n1426ContagemResultadoNotificacao_DestPesCod ;
      private int[] P00VG13_A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool[] P00VG13_n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private String[] P00VG13_A1957ContagemResultadoNotificacao_User ;
      private bool[] P00VG13_n1957ContagemResultadoNotificacao_User ;
      private long[] P00VG13_A1412ContagemResultadoNotificacao_Codigo ;
      private short[] P00VG13_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] P00VG13_n1961ContagemResultadoNotificacao_Logged ;
      private short[] P00VG13_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] P00VG13_n1960ContagemResultadoNotificacao_Sec ;
      private bool[] P00VG13_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] P00VG13_n1959ContagemResultadoNotificacao_Aut ;
      private short[] P00VG13_A1958ContagemResultadoNotificacao_Port ;
      private bool[] P00VG13_n1958ContagemResultadoNotificacao_Port ;
      private String[] P00VG13_A1956ContagemResultadoNotificacao_Host ;
      private bool[] P00VG13_n1956ContagemResultadoNotificacao_Host ;
      private String[] P00VG13_A1421ContagemResultadoNotificacao_DestNome ;
      private bool[] P00VG13_n1421ContagemResultadoNotificacao_DestNome ;
      private String[] P00VG13_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] P00VG13_n1422ContagemResultadoNotificacao_UsuNom ;
      private String[] P00VG13_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] P00VG13_n1417ContagemResultadoNotificacao_Assunto ;
      private DateTime[] P00VG13_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] P00VG13_n1416ContagemResultadoNotificacao_DataHora ;
      private int[] P00VG13_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] P00VG13_n1963ContagemResultadoNotificacao_Demandas ;
      private int[] P00VG13_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] P00VG13_n1962ContagemResultadoNotificacao_Destinatarios ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV28TFContagemResultadoNotificacao_Sec_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV30TFContagemResultadoNotificacao_Logged_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV137Udparg52 ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV36Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV39OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV41OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV46GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV47GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV48GridStateDynamicFilter ;
   }

   public class getwwcontagemresultadonotificacaofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00VG4( IGxContext context ,
                                             short A1960ContagemResultadoNotificacao_Sec ,
                                             IGxCollection AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels ,
                                             short A1961ContagemResultadoNotificacao_Logged ,
                                             IGxCollection AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels ,
                                             long A1412ContagemResultadoNotificacao_Codigo ,
                                             IGxCollection AV137Udparg52 ,
                                             String AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1 ,
                                             DateTime AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1 ,
                                             DateTime AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1 ,
                                             short AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 ,
                                             String AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 ,
                                             String AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 ,
                                             bool AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 ,
                                             String AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2 ,
                                             DateTime AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2 ,
                                             DateTime AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2 ,
                                             short AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 ,
                                             String AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 ,
                                             String AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 ,
                                             bool AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 ,
                                             String AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3 ,
                                             DateTime AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3 ,
                                             DateTime AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3 ,
                                             short AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 ,
                                             String AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 ,
                                             String AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 ,
                                             bool AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 ,
                                             String AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4 ,
                                             DateTime AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4 ,
                                             DateTime AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4 ,
                                             short AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 ,
                                             String AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 ,
                                             String AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 ,
                                             DateTime AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora ,
                                             DateTime AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to ,
                                             String AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel ,
                                             String AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom ,
                                             String AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel ,
                                             String AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto ,
                                             String AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel ,
                                             String AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host ,
                                             String AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel ,
                                             String AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user ,
                                             short AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port ,
                                             short AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to ,
                                             short AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel ,
                                             int AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels_Count ,
                                             int AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels_Count ,
                                             String AV50DynamicFiltersSelector1 ,
                                             String AV56ContagemResultadoNotificacao_DestNome1 ,
                                             String AV58DynamicFiltersSelector2 ,
                                             String AV64ContagemResultadoNotificacao_DestNome2 ,
                                             String AV66DynamicFiltersSelector3 ,
                                             String AV72ContagemResultadoNotificacao_DestNome3 ,
                                             String AV74DynamicFiltersSelector4 ,
                                             String AV80ContagemResultadoNotificacao_DestNome4 ,
                                             DateTime A1416ContagemResultadoNotificacao_DataHora ,
                                             String A1417ContagemResultadoNotificacao_Assunto ,
                                             String A1422ContagemResultadoNotificacao_UsuNom ,
                                             String A1421ContagemResultadoNotificacao_DestNome ,
                                             String AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 ,
                                             String AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 ,
                                             String AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 ,
                                             String AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 ,
                                             String A1956ContagemResultadoNotificacao_Host ,
                                             String A1957ContagemResultadoNotificacao_User ,
                                             short A1958ContagemResultadoNotificacao_Port ,
                                             bool A1959ContagemResultadoNotificacao_Aut ,
                                             int AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios ,
                                             int A1962ContagemResultadoNotificacao_Destinatarios ,
                                             int AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to ,
                                             int AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas ,
                                             int A1963ContagemResultadoNotificacao_Demandas ,
                                             int AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to ,
                                             int A1966ContagemResultadoNotificacao_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [53] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T4.[ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, T5.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod, T1.[ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_D, T2.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_D, T4.[ContagemResultadoNotificacao_AreaTrabalhoCod], T6.[Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom, T1.[ContagemResultadoNotificacao_Codigo], T4.[ContagemResultadoNotificacao_Logged], T4.[ContagemResultadoNotificacao_Sec], T4.[ContagemResultadoNotificacao_Aut], T4.[ContagemResultadoNotificacao_Port], T4.[ContagemResultadoNotificacao_User], T4.[ContagemResultadoNotificacao_Host], T3.[Pessoa_Nome] AS ContagemResultadoNotificacao_D, T4.[ContagemResultadoNotificacao_Assunto], T4.[ContagemResultadoNotificacao_DataHora], COALESCE( T7.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas, COALESCE( T8.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios FROM ((((((([ContagemResultadoNotificacaoDestinatario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoNotificacao_DestCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [ContagemResultadoNotificacao] T4 WITH (NOLOCK) ON T4.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) INNER JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T4.[ContagemResultadoNotificacao_UsuCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo]";
         scmdbuf = scmdbuf + " ) T7 ON T7.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T8 ON T8.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo])";
         scmdbuf = scmdbuf + " WHERE ((@AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios = convert(int, 0)) or ( COALESCE( T8.[ContagemResultadoNotificacao_Destinatarios], 0) >= @AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios))";
         scmdbuf = scmdbuf + " and ((@AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to = convert(int, 0)) or ( COALESCE( T8.[ContagemResultadoNotificacao_Destinatarios], 0) <= @AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to))";
         scmdbuf = scmdbuf + " and ((@AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas = convert(int, 0)) or ( COALESCE( T7.[ContagemResultadoNotificacao_Demandas], 0) >= @AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas))";
         scmdbuf = scmdbuf + " and ((@AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultadoNotificacao_Demandas], 0) <= @AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to))";
         scmdbuf = scmdbuf + " and (T4.[ContagemResultadoNotificacao_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like '%' + @lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like '%' + @lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 0 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 1 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like '%' + @lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2)";
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like '%' + @lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2)";
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 0 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2)";
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 1 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2)";
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3)";
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3)";
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3)";
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like '%' + @lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3)";
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3)";
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like '%' + @lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3)";
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 0 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3)";
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 1 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3)";
         }
         else
         {
            GXv_int1[32] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4)";
         }
         else
         {
            GXv_int1[33] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4)";
         }
         else
         {
            GXv_int1[34] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4)";
         }
         else
         {
            GXv_int1[35] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like '%' + @lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4)";
         }
         else
         {
            GXv_int1[36] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4)";
         }
         else
         {
            GXv_int1[37] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like '%' + @lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4)";
         }
         else
         {
            GXv_int1[38] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 0 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4)";
         }
         else
         {
            GXv_int1[39] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 1 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4)";
         }
         else
         {
            GXv_int1[40] = 1;
         }
         if ( ! (DateTime.MinValue==AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora)";
         }
         else
         {
            GXv_int1[41] = 1;
         }
         if ( ! (DateTime.MinValue==AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to)";
         }
         else
         {
            GXv_int1[42] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom)";
         }
         else
         {
            GXv_int1[43] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] = @AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel)";
         }
         else
         {
            GXv_int1[44] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto)";
         }
         else
         {
            GXv_int1[45] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] = @AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel)";
         }
         else
         {
            GXv_int1[46] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Host] like @lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host)";
         }
         else
         {
            GXv_int1[47] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Host] = @AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel)";
         }
         else
         {
            GXv_int1[48] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_User] like @lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user)";
         }
         else
         {
            GXv_int1[49] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_User] = @AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel)";
         }
         else
         {
            GXv_int1[50] = 1;
         }
         if ( ! (0==AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Port] >= @AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port)";
         }
         else
         {
            GXv_int1[51] = 1;
         }
         if ( ! (0==AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Port] <= @AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to)";
         }
         else
         {
            GXv_int1[52] = 1;
         }
         if ( AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel == 1 )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Aut] = 1)";
         }
         if ( AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel == 2 )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Aut] = 0)";
         }
         if ( AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels, "T4.[ContagemResultadoNotificacao_Sec] IN (", ")") + ")";
         }
         if ( AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels, "T4.[ContagemResultadoNotificacao_Logged] IN (", ")") + ")";
         }
         if ( ( ( StringUtil.StrCmp(AV50DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultadoNotificacao_DestNome1)) ) || ( ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemResultadoNotificacao_DestNome2)) ) || ( ( StringUtil.StrCmp(AV66DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV72ContagemResultadoNotificacao_DestNome3)) ) || ( ( StringUtil.StrCmp(AV74DynamicFiltersSelector4, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV80ContagemResultadoNotificacao_DestNome4)) ) )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV137Udparg52, "T1.[ContagemResultadoNotificacao_Codigo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T6.[Pessoa_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00VG7( IGxContext context ,
                                             short A1960ContagemResultadoNotificacao_Sec ,
                                             IGxCollection AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels ,
                                             short A1961ContagemResultadoNotificacao_Logged ,
                                             IGxCollection AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels ,
                                             long A1412ContagemResultadoNotificacao_Codigo ,
                                             IGxCollection AV137Udparg52 ,
                                             String AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1 ,
                                             DateTime AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1 ,
                                             DateTime AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1 ,
                                             short AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 ,
                                             String AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 ,
                                             String AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 ,
                                             bool AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 ,
                                             String AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2 ,
                                             DateTime AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2 ,
                                             DateTime AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2 ,
                                             short AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 ,
                                             String AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 ,
                                             String AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 ,
                                             bool AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 ,
                                             String AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3 ,
                                             DateTime AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3 ,
                                             DateTime AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3 ,
                                             short AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 ,
                                             String AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 ,
                                             String AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 ,
                                             bool AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 ,
                                             String AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4 ,
                                             DateTime AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4 ,
                                             DateTime AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4 ,
                                             short AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 ,
                                             String AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 ,
                                             String AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 ,
                                             DateTime AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora ,
                                             DateTime AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to ,
                                             String AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel ,
                                             String AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom ,
                                             String AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel ,
                                             String AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto ,
                                             String AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel ,
                                             String AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host ,
                                             String AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel ,
                                             String AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user ,
                                             short AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port ,
                                             short AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to ,
                                             short AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel ,
                                             int AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels_Count ,
                                             int AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels_Count ,
                                             String AV50DynamicFiltersSelector1 ,
                                             String AV56ContagemResultadoNotificacao_DestNome1 ,
                                             String AV58DynamicFiltersSelector2 ,
                                             String AV64ContagemResultadoNotificacao_DestNome2 ,
                                             String AV66DynamicFiltersSelector3 ,
                                             String AV72ContagemResultadoNotificacao_DestNome3 ,
                                             String AV74DynamicFiltersSelector4 ,
                                             String AV80ContagemResultadoNotificacao_DestNome4 ,
                                             DateTime A1416ContagemResultadoNotificacao_DataHora ,
                                             String A1417ContagemResultadoNotificacao_Assunto ,
                                             String A1422ContagemResultadoNotificacao_UsuNom ,
                                             String A1421ContagemResultadoNotificacao_DestNome ,
                                             String AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 ,
                                             String AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 ,
                                             String AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 ,
                                             String AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 ,
                                             String A1956ContagemResultadoNotificacao_Host ,
                                             String A1957ContagemResultadoNotificacao_User ,
                                             short A1958ContagemResultadoNotificacao_Port ,
                                             bool A1959ContagemResultadoNotificacao_Aut ,
                                             int AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios ,
                                             int A1962ContagemResultadoNotificacao_Destinatarios ,
                                             int AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to ,
                                             int AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas ,
                                             int A1963ContagemResultadoNotificacao_Demandas ,
                                             int AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to ,
                                             int A1966ContagemResultadoNotificacao_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [53] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T4.[ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, T5.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod, T1.[ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_D, T2.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_D, T4.[ContagemResultadoNotificacao_AreaTrabalhoCod], T4.[ContagemResultadoNotificacao_Assunto], T1.[ContagemResultadoNotificacao_Codigo], T4.[ContagemResultadoNotificacao_Logged], T4.[ContagemResultadoNotificacao_Sec], T4.[ContagemResultadoNotificacao_Aut], T4.[ContagemResultadoNotificacao_Port], T4.[ContagemResultadoNotificacao_User], T4.[ContagemResultadoNotificacao_Host], T3.[Pessoa_Nome] AS ContagemResultadoNotificacao_D, T6.[Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom, T4.[ContagemResultadoNotificacao_DataHora], COALESCE( T7.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas, COALESCE( T8.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios FROM ((((((([ContagemResultadoNotificacaoDestinatario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoNotificacao_DestCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [ContagemResultadoNotificacao] T4 WITH (NOLOCK) ON T4.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) INNER JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T4.[ContagemResultadoNotificacao_UsuCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo]";
         scmdbuf = scmdbuf + " ) T7 ON T7.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T8 ON T8.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo])";
         scmdbuf = scmdbuf + " WHERE ((@AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios = convert(int, 0)) or ( COALESCE( T8.[ContagemResultadoNotificacao_Destinatarios], 0) >= @AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios))";
         scmdbuf = scmdbuf + " and ((@AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to = convert(int, 0)) or ( COALESCE( T8.[ContagemResultadoNotificacao_Destinatarios], 0) <= @AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to))";
         scmdbuf = scmdbuf + " and ((@AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas = convert(int, 0)) or ( COALESCE( T7.[ContagemResultadoNotificacao_Demandas], 0) >= @AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas))";
         scmdbuf = scmdbuf + " and ((@AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultadoNotificacao_Demandas], 0) <= @AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to))";
         scmdbuf = scmdbuf + " and (T4.[ContagemResultadoNotificacao_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like '%' + @lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like '%' + @lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 0 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 1 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like '%' + @lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2)";
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like '%' + @lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2)";
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 0 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2)";
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 1 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2)";
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3)";
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3)";
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3)";
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like '%' + @lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3)";
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3)";
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like '%' + @lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3)";
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 0 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3)";
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 1 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3)";
         }
         else
         {
            GXv_int3[32] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4)";
         }
         else
         {
            GXv_int3[33] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4)";
         }
         else
         {
            GXv_int3[34] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4)";
         }
         else
         {
            GXv_int3[35] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like '%' + @lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4)";
         }
         else
         {
            GXv_int3[36] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4)";
         }
         else
         {
            GXv_int3[37] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like '%' + @lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4)";
         }
         else
         {
            GXv_int3[38] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 0 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4)";
         }
         else
         {
            GXv_int3[39] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 1 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4)";
         }
         else
         {
            GXv_int3[40] = 1;
         }
         if ( ! (DateTime.MinValue==AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora)";
         }
         else
         {
            GXv_int3[41] = 1;
         }
         if ( ! (DateTime.MinValue==AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to)";
         }
         else
         {
            GXv_int3[42] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom)";
         }
         else
         {
            GXv_int3[43] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] = @AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel)";
         }
         else
         {
            GXv_int3[44] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto)";
         }
         else
         {
            GXv_int3[45] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] = @AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel)";
         }
         else
         {
            GXv_int3[46] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Host] like @lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host)";
         }
         else
         {
            GXv_int3[47] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Host] = @AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel)";
         }
         else
         {
            GXv_int3[48] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_User] like @lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user)";
         }
         else
         {
            GXv_int3[49] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_User] = @AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel)";
         }
         else
         {
            GXv_int3[50] = 1;
         }
         if ( ! (0==AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Port] >= @AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port)";
         }
         else
         {
            GXv_int3[51] = 1;
         }
         if ( ! (0==AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Port] <= @AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to)";
         }
         else
         {
            GXv_int3[52] = 1;
         }
         if ( AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel == 1 )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Aut] = 1)";
         }
         if ( AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel == 2 )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Aut] = 0)";
         }
         if ( AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels, "T4.[ContagemResultadoNotificacao_Sec] IN (", ")") + ")";
         }
         if ( AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels, "T4.[ContagemResultadoNotificacao_Logged] IN (", ")") + ")";
         }
         if ( ( ( StringUtil.StrCmp(AV50DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultadoNotificacao_DestNome1)) ) || ( ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemResultadoNotificacao_DestNome2)) ) || ( ( StringUtil.StrCmp(AV66DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV72ContagemResultadoNotificacao_DestNome3)) ) || ( ( StringUtil.StrCmp(AV74DynamicFiltersSelector4, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV80ContagemResultadoNotificacao_DestNome4)) ) )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV137Udparg52, "T1.[ContagemResultadoNotificacao_Codigo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T4.[ContagemResultadoNotificacao_Assunto]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00VG10( IGxContext context ,
                                              short A1960ContagemResultadoNotificacao_Sec ,
                                              IGxCollection AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels ,
                                              short A1961ContagemResultadoNotificacao_Logged ,
                                              IGxCollection AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels ,
                                              long A1412ContagemResultadoNotificacao_Codigo ,
                                              IGxCollection AV137Udparg52 ,
                                              String AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1 ,
                                              DateTime AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1 ,
                                              DateTime AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1 ,
                                              short AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 ,
                                              String AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 ,
                                              String AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 ,
                                              bool AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 ,
                                              String AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2 ,
                                              DateTime AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2 ,
                                              DateTime AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2 ,
                                              short AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 ,
                                              String AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 ,
                                              String AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 ,
                                              bool AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 ,
                                              String AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3 ,
                                              DateTime AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3 ,
                                              DateTime AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3 ,
                                              short AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 ,
                                              String AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 ,
                                              String AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 ,
                                              bool AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 ,
                                              String AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4 ,
                                              DateTime AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4 ,
                                              DateTime AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4 ,
                                              short AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 ,
                                              String AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 ,
                                              String AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 ,
                                              DateTime AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora ,
                                              DateTime AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to ,
                                              String AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel ,
                                              String AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom ,
                                              String AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel ,
                                              String AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto ,
                                              String AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel ,
                                              String AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host ,
                                              String AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel ,
                                              String AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user ,
                                              short AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port ,
                                              short AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to ,
                                              short AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel ,
                                              int AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels_Count ,
                                              int AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels_Count ,
                                              String AV50DynamicFiltersSelector1 ,
                                              String AV56ContagemResultadoNotificacao_DestNome1 ,
                                              String AV58DynamicFiltersSelector2 ,
                                              String AV64ContagemResultadoNotificacao_DestNome2 ,
                                              String AV66DynamicFiltersSelector3 ,
                                              String AV72ContagemResultadoNotificacao_DestNome3 ,
                                              String AV74DynamicFiltersSelector4 ,
                                              String AV80ContagemResultadoNotificacao_DestNome4 ,
                                              DateTime A1416ContagemResultadoNotificacao_DataHora ,
                                              String A1417ContagemResultadoNotificacao_Assunto ,
                                              String A1422ContagemResultadoNotificacao_UsuNom ,
                                              String A1421ContagemResultadoNotificacao_DestNome ,
                                              String AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 ,
                                              String AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 ,
                                              String AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 ,
                                              String AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 ,
                                              String A1956ContagemResultadoNotificacao_Host ,
                                              String A1957ContagemResultadoNotificacao_User ,
                                              short A1958ContagemResultadoNotificacao_Port ,
                                              bool A1959ContagemResultadoNotificacao_Aut ,
                                              int AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios ,
                                              int A1962ContagemResultadoNotificacao_Destinatarios ,
                                              int AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to ,
                                              int AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas ,
                                              int A1963ContagemResultadoNotificacao_Demandas ,
                                              int AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to ,
                                              int A1966ContagemResultadoNotificacao_AreaTrabalhoCod ,
                                              int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [53] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T4.[ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, T5.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod, T1.[ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_D, T2.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_D, T4.[ContagemResultadoNotificacao_AreaTrabalhoCod], T4.[ContagemResultadoNotificacao_Host], T1.[ContagemResultadoNotificacao_Codigo], T4.[ContagemResultadoNotificacao_Logged], T4.[ContagemResultadoNotificacao_Sec], T4.[ContagemResultadoNotificacao_Aut], T4.[ContagemResultadoNotificacao_Port], T4.[ContagemResultadoNotificacao_User], T3.[Pessoa_Nome] AS ContagemResultadoNotificacao_D, T6.[Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom, T4.[ContagemResultadoNotificacao_Assunto], T4.[ContagemResultadoNotificacao_DataHora], COALESCE( T7.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas, COALESCE( T8.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios FROM ((((((([ContagemResultadoNotificacaoDestinatario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoNotificacao_DestCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [ContagemResultadoNotificacao] T4 WITH (NOLOCK) ON T4.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) INNER JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T4.[ContagemResultadoNotificacao_UsuCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo]";
         scmdbuf = scmdbuf + " ) T7 ON T7.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T8 ON T8.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo])";
         scmdbuf = scmdbuf + " WHERE ((@AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios = convert(int, 0)) or ( COALESCE( T8.[ContagemResultadoNotificacao_Destinatarios], 0) >= @AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios))";
         scmdbuf = scmdbuf + " and ((@AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to = convert(int, 0)) or ( COALESCE( T8.[ContagemResultadoNotificacao_Destinatarios], 0) <= @AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to))";
         scmdbuf = scmdbuf + " and ((@AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas = convert(int, 0)) or ( COALESCE( T7.[ContagemResultadoNotificacao_Demandas], 0) >= @AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas))";
         scmdbuf = scmdbuf + " and ((@AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultadoNotificacao_Demandas], 0) <= @AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to))";
         scmdbuf = scmdbuf + " and (T4.[ContagemResultadoNotificacao_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like '%' + @lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like '%' + @lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 0 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1)";
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 1 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1)";
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2)";
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2)";
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2)";
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like '%' + @lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2)";
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2)";
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like '%' + @lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2)";
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 0 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2)";
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 1 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2)";
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3)";
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3)";
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3)";
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like '%' + @lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3)";
         }
         else
         {
            GXv_int5[28] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3)";
         }
         else
         {
            GXv_int5[29] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like '%' + @lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3)";
         }
         else
         {
            GXv_int5[30] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 0 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3)";
         }
         else
         {
            GXv_int5[31] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 1 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3)";
         }
         else
         {
            GXv_int5[32] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4)";
         }
         else
         {
            GXv_int5[33] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4)";
         }
         else
         {
            GXv_int5[34] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4)";
         }
         else
         {
            GXv_int5[35] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like '%' + @lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4)";
         }
         else
         {
            GXv_int5[36] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4)";
         }
         else
         {
            GXv_int5[37] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like '%' + @lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4)";
         }
         else
         {
            GXv_int5[38] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 0 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4)";
         }
         else
         {
            GXv_int5[39] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 1 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4)";
         }
         else
         {
            GXv_int5[40] = 1;
         }
         if ( ! (DateTime.MinValue==AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora)";
         }
         else
         {
            GXv_int5[41] = 1;
         }
         if ( ! (DateTime.MinValue==AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to)";
         }
         else
         {
            GXv_int5[42] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom)";
         }
         else
         {
            GXv_int5[43] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] = @AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel)";
         }
         else
         {
            GXv_int5[44] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto)";
         }
         else
         {
            GXv_int5[45] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] = @AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel)";
         }
         else
         {
            GXv_int5[46] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Host] like @lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host)";
         }
         else
         {
            GXv_int5[47] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Host] = @AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel)";
         }
         else
         {
            GXv_int5[48] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_User] like @lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user)";
         }
         else
         {
            GXv_int5[49] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_User] = @AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel)";
         }
         else
         {
            GXv_int5[50] = 1;
         }
         if ( ! (0==AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Port] >= @AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port)";
         }
         else
         {
            GXv_int5[51] = 1;
         }
         if ( ! (0==AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Port] <= @AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to)";
         }
         else
         {
            GXv_int5[52] = 1;
         }
         if ( AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel == 1 )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Aut] = 1)";
         }
         if ( AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel == 2 )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Aut] = 0)";
         }
         if ( AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels, "T4.[ContagemResultadoNotificacao_Sec] IN (", ")") + ")";
         }
         if ( AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels, "T4.[ContagemResultadoNotificacao_Logged] IN (", ")") + ")";
         }
         if ( ( ( StringUtil.StrCmp(AV50DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultadoNotificacao_DestNome1)) ) || ( ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemResultadoNotificacao_DestNome2)) ) || ( ( StringUtil.StrCmp(AV66DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV72ContagemResultadoNotificacao_DestNome3)) ) || ( ( StringUtil.StrCmp(AV74DynamicFiltersSelector4, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV80ContagemResultadoNotificacao_DestNome4)) ) )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV137Udparg52, "T1.[ContagemResultadoNotificacao_Codigo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T4.[ContagemResultadoNotificacao_Host]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00VG13( IGxContext context ,
                                              short A1960ContagemResultadoNotificacao_Sec ,
                                              IGxCollection AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels ,
                                              short A1961ContagemResultadoNotificacao_Logged ,
                                              IGxCollection AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels ,
                                              long A1412ContagemResultadoNotificacao_Codigo ,
                                              IGxCollection AV137Udparg52 ,
                                              String AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1 ,
                                              DateTime AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1 ,
                                              DateTime AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1 ,
                                              short AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 ,
                                              String AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1 ,
                                              String AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1 ,
                                              bool AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 ,
                                              String AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2 ,
                                              DateTime AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2 ,
                                              DateTime AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2 ,
                                              short AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 ,
                                              String AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2 ,
                                              String AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2 ,
                                              bool AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 ,
                                              String AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3 ,
                                              DateTime AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3 ,
                                              DateTime AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3 ,
                                              short AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 ,
                                              String AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3 ,
                                              String AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3 ,
                                              bool AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 ,
                                              String AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4 ,
                                              DateTime AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4 ,
                                              DateTime AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4 ,
                                              short AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 ,
                                              String AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4 ,
                                              String AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4 ,
                                              DateTime AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora ,
                                              DateTime AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to ,
                                              String AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel ,
                                              String AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom ,
                                              String AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel ,
                                              String AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto ,
                                              String AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel ,
                                              String AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host ,
                                              String AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel ,
                                              String AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user ,
                                              short AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port ,
                                              short AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to ,
                                              short AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel ,
                                              int AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels_Count ,
                                              int AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels_Count ,
                                              String AV50DynamicFiltersSelector1 ,
                                              String AV56ContagemResultadoNotificacao_DestNome1 ,
                                              String AV58DynamicFiltersSelector2 ,
                                              String AV64ContagemResultadoNotificacao_DestNome2 ,
                                              String AV66DynamicFiltersSelector3 ,
                                              String AV72ContagemResultadoNotificacao_DestNome3 ,
                                              String AV74DynamicFiltersSelector4 ,
                                              String AV80ContagemResultadoNotificacao_DestNome4 ,
                                              DateTime A1416ContagemResultadoNotificacao_DataHora ,
                                              String A1417ContagemResultadoNotificacao_Assunto ,
                                              String A1422ContagemResultadoNotificacao_UsuNom ,
                                              String A1421ContagemResultadoNotificacao_DestNome ,
                                              String AV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1 ,
                                              String AV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2 ,
                                              String AV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3 ,
                                              String AV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4 ,
                                              String A1956ContagemResultadoNotificacao_Host ,
                                              String A1957ContagemResultadoNotificacao_User ,
                                              short A1958ContagemResultadoNotificacao_Port ,
                                              bool A1959ContagemResultadoNotificacao_Aut ,
                                              int AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios ,
                                              int A1962ContagemResultadoNotificacao_Destinatarios ,
                                              int AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to ,
                                              int AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas ,
                                              int A1963ContagemResultadoNotificacao_Demandas ,
                                              int AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to ,
                                              int A1966ContagemResultadoNotificacao_AreaTrabalhoCod ,
                                              int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [53] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T4.[ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, T5.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod, T1.[ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_D, T2.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_D, T4.[ContagemResultadoNotificacao_AreaTrabalhoCod], T4.[ContagemResultadoNotificacao_User], T1.[ContagemResultadoNotificacao_Codigo], T4.[ContagemResultadoNotificacao_Logged], T4.[ContagemResultadoNotificacao_Sec], T4.[ContagemResultadoNotificacao_Aut], T4.[ContagemResultadoNotificacao_Port], T4.[ContagemResultadoNotificacao_Host], T3.[Pessoa_Nome] AS ContagemResultadoNotificacao_D, T6.[Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom, T4.[ContagemResultadoNotificacao_Assunto], T4.[ContagemResultadoNotificacao_DataHora], COALESCE( T7.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas, COALESCE( T8.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios FROM ((((((([ContagemResultadoNotificacaoDestinatario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoNotificacao_DestCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [ContagemResultadoNotificacao] T4 WITH (NOLOCK) ON T4.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) INNER JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T4.[ContagemResultadoNotificacao_UsuCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo]";
         scmdbuf = scmdbuf + " ) T7 ON T7.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T8 ON T8.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo])";
         scmdbuf = scmdbuf + " WHERE ((@AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios = convert(int, 0)) or ( COALESCE( T8.[ContagemResultadoNotificacao_Destinatarios], 0) >= @AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios))";
         scmdbuf = scmdbuf + " and ((@AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to = convert(int, 0)) or ( COALESCE( T8.[ContagemResultadoNotificacao_Destinatarios], 0) <= @AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to))";
         scmdbuf = scmdbuf + " and ((@AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas = convert(int, 0)) or ( COALESCE( T7.[ContagemResultadoNotificacao_Demandas], 0) >= @AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas))";
         scmdbuf = scmdbuf + " and ((@AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultadoNotificacao_Demandas], 0) <= @AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to))";
         scmdbuf = scmdbuf + " and (T4.[ContagemResultadoNotificacao_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1)";
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1)";
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1)";
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like '%' + @lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1)";
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1)";
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like '%' + @lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1)";
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 0 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1)";
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWContagemResultadoNotificacaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV88WWContagemResultadoNotificacaoDS_3_Dynamicfiltersoperator1 == 1 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1)";
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2)";
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2)";
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2)";
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like '%' + @lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2)";
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2)";
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like '%' + @lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2)";
         }
         else
         {
            GXv_int7[22] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 0 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2)";
         }
         else
         {
            GXv_int7[23] = 1;
         }
         if ( AV94WWContagemResultadoNotificacaoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWContagemResultadoNotificacaoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV96WWContagemResultadoNotificacaoDS_11_Dynamicfiltersoperator2 == 1 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2)";
         }
         else
         {
            GXv_int7[24] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3)";
         }
         else
         {
            GXv_int7[25] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3)";
         }
         else
         {
            GXv_int7[26] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3)";
         }
         else
         {
            GXv_int7[27] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like '%' + @lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3)";
         }
         else
         {
            GXv_int7[28] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3)";
         }
         else
         {
            GXv_int7[29] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like '%' + @lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3)";
         }
         else
         {
            GXv_int7[30] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 0 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3)";
         }
         else
         {
            GXv_int7[31] = 1;
         }
         if ( AV102WWContagemResultadoNotificacaoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWContagemResultadoNotificacaoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV104WWContagemResultadoNotificacaoDS_19_Dynamicfiltersoperator3 == 1 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3)";
         }
         else
         {
            GXv_int7[32] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4)";
         }
         else
         {
            GXv_int7[33] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 ) && ( ! (DateTime.MinValue==AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4)";
         }
         else
         {
            GXv_int7[34] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4)";
         }
         else
         {
            GXv_int7[35] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like '%' + @lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4)";
         }
         else
         {
            GXv_int7[36] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4)";
         }
         else
         {
            GXv_int7[37] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like '%' + @lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4)";
         }
         else
         {
            GXv_int7[38] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 0 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4)";
         }
         else
         {
            GXv_int7[39] = 1;
         }
         if ( AV110WWContagemResultadoNotificacaoDS_25_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV111WWContagemResultadoNotificacaoDS_26_Dynamicfiltersselector4, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ( AV112WWContagemResultadoNotificacaoDS_27_Dynamicfiltersoperator4 == 1 ) && ( false ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4)";
         }
         else
         {
            GXv_int7[40] = 1;
         }
         if ( ! (DateTime.MinValue==AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] >= @AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora)";
         }
         else
         {
            GXv_int7[41] = 1;
         }
         if ( ! (DateTime.MinValue==AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_DataHora] <= @AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to)";
         }
         else
         {
            GXv_int7[42] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] like @lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom)";
         }
         else
         {
            GXv_int7[43] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Pessoa_Nome] = @AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel)";
         }
         else
         {
            GXv_int7[44] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] like @lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto)";
         }
         else
         {
            GXv_int7[45] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Assunto] = @AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel)";
         }
         else
         {
            GXv_int7[46] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Host] like @lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host)";
         }
         else
         {
            GXv_int7[47] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Host] = @AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel)";
         }
         else
         {
            GXv_int7[48] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_User] like @lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user)";
         }
         else
         {
            GXv_int7[49] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_User] = @AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel)";
         }
         else
         {
            GXv_int7[50] = 1;
         }
         if ( ! (0==AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Port] >= @AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port)";
         }
         else
         {
            GXv_int7[51] = 1;
         }
         if ( ! (0==AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Port] <= @AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to)";
         }
         else
         {
            GXv_int7[52] = 1;
         }
         if ( AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel == 1 )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Aut] = 1)";
         }
         if ( AV134WWContagemResultadoNotificacaoDS_49_Tfcontagemresultadonotificacao_aut_sel == 2 )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultadoNotificacao_Aut] = 0)";
         }
         if ( AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV135WWContagemResultadoNotificacaoDS_50_Tfcontagemresultadonotificacao_sec_sels, "T4.[ContagemResultadoNotificacao_Sec] IN (", ")") + ")";
         }
         if ( AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV136WWContagemResultadoNotificacaoDS_51_Tfcontagemresultadonotificacao_logged_sels, "T4.[ContagemResultadoNotificacao_Logged] IN (", ")") + ")";
         }
         if ( ( ( StringUtil.StrCmp(AV50DynamicFiltersSelector1, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultadoNotificacao_DestNome1)) ) || ( ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemResultadoNotificacao_DestNome2)) ) || ( ( StringUtil.StrCmp(AV66DynamicFiltersSelector3, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV72ContagemResultadoNotificacao_DestNome3)) ) || ( ( StringUtil.StrCmp(AV74DynamicFiltersSelector4, "CONTAGEMRESULTADONOTIFICACAO_DESTNOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV80ContagemResultadoNotificacao_DestNome4)) ) )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV137Udparg52, "T1.[ContagemResultadoNotificacao_Codigo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T4.[ContagemResultadoNotificacao_User]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00VG4(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (long)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (short)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (bool)dynConstraints[19] , (String)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (short)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (bool)dynConstraints[26] , (String)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (short)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (short)dynConstraints[43] , (short)dynConstraints[44] , (short)dynConstraints[45] , (int)dynConstraints[46] , (int)dynConstraints[47] , (String)dynConstraints[48] , (String)dynConstraints[49] , (String)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (String)dynConstraints[53] , (String)dynConstraints[54] , (String)dynConstraints[55] , (DateTime)dynConstraints[56] , (String)dynConstraints[57] , (String)dynConstraints[58] , (String)dynConstraints[59] , (String)dynConstraints[60] , (String)dynConstraints[61] , (String)dynConstraints[62] , (String)dynConstraints[63] , (String)dynConstraints[64] , (String)dynConstraints[65] , (short)dynConstraints[66] , (bool)dynConstraints[67] , (int)dynConstraints[68] , (int)dynConstraints[69] , (int)dynConstraints[70] , (int)dynConstraints[71] , (int)dynConstraints[72] , (int)dynConstraints[73] , (int)dynConstraints[74] , (int)dynConstraints[75] );
               case 1 :
                     return conditional_P00VG7(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (long)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (short)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (bool)dynConstraints[19] , (String)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (short)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (bool)dynConstraints[26] , (String)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (short)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (short)dynConstraints[43] , (short)dynConstraints[44] , (short)dynConstraints[45] , (int)dynConstraints[46] , (int)dynConstraints[47] , (String)dynConstraints[48] , (String)dynConstraints[49] , (String)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (String)dynConstraints[53] , (String)dynConstraints[54] , (String)dynConstraints[55] , (DateTime)dynConstraints[56] , (String)dynConstraints[57] , (String)dynConstraints[58] , (String)dynConstraints[59] , (String)dynConstraints[60] , (String)dynConstraints[61] , (String)dynConstraints[62] , (String)dynConstraints[63] , (String)dynConstraints[64] , (String)dynConstraints[65] , (short)dynConstraints[66] , (bool)dynConstraints[67] , (int)dynConstraints[68] , (int)dynConstraints[69] , (int)dynConstraints[70] , (int)dynConstraints[71] , (int)dynConstraints[72] , (int)dynConstraints[73] , (int)dynConstraints[74] , (int)dynConstraints[75] );
               case 2 :
                     return conditional_P00VG10(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (long)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (short)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (bool)dynConstraints[19] , (String)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (short)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (bool)dynConstraints[26] , (String)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (short)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (short)dynConstraints[43] , (short)dynConstraints[44] , (short)dynConstraints[45] , (int)dynConstraints[46] , (int)dynConstraints[47] , (String)dynConstraints[48] , (String)dynConstraints[49] , (String)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (String)dynConstraints[53] , (String)dynConstraints[54] , (String)dynConstraints[55] , (DateTime)dynConstraints[56] , (String)dynConstraints[57] , (String)dynConstraints[58] , (String)dynConstraints[59] , (String)dynConstraints[60] , (String)dynConstraints[61] , (String)dynConstraints[62] , (String)dynConstraints[63] , (String)dynConstraints[64] , (String)dynConstraints[65] , (short)dynConstraints[66] , (bool)dynConstraints[67] , (int)dynConstraints[68] , (int)dynConstraints[69] , (int)dynConstraints[70] , (int)dynConstraints[71] , (int)dynConstraints[72] , (int)dynConstraints[73] , (int)dynConstraints[74] , (int)dynConstraints[75] );
               case 3 :
                     return conditional_P00VG13(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (long)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (short)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (bool)dynConstraints[19] , (String)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (short)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (bool)dynConstraints[26] , (String)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (short)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (short)dynConstraints[43] , (short)dynConstraints[44] , (short)dynConstraints[45] , (int)dynConstraints[46] , (int)dynConstraints[47] , (String)dynConstraints[48] , (String)dynConstraints[49] , (String)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (String)dynConstraints[53] , (String)dynConstraints[54] , (String)dynConstraints[55] , (DateTime)dynConstraints[56] , (String)dynConstraints[57] , (String)dynConstraints[58] , (String)dynConstraints[59] , (String)dynConstraints[60] , (String)dynConstraints[61] , (String)dynConstraints[62] , (String)dynConstraints[63] , (String)dynConstraints[64] , (String)dynConstraints[65] , (short)dynConstraints[66] , (bool)dynConstraints[67] , (int)dynConstraints[68] , (int)dynConstraints[69] , (int)dynConstraints[70] , (int)dynConstraints[71] , (int)dynConstraints[72] , (int)dynConstraints[73] , (int)dynConstraints[74] , (int)dynConstraints[75] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VG4 ;
          prmP00VG4 = new Object[] {
          new Object[] {"@AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4",SqlDbType.Char,100,0} ,
          new Object[] {"@lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4",SqlDbType.Char,100,0} ,
          new Object[] {"@lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4",SqlDbType.Char,100,0} ,
          new Object[] {"@lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4",SqlDbType.Char,100,0} ,
          new Object[] {"@AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host",SqlDbType.Char,50,0} ,
          new Object[] {"@AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user",SqlDbType.Char,50,0} ,
          new Object[] {"@AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00VG7 ;
          prmP00VG7 = new Object[] {
          new Object[] {"@AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4",SqlDbType.Char,100,0} ,
          new Object[] {"@lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4",SqlDbType.Char,100,0} ,
          new Object[] {"@lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4",SqlDbType.Char,100,0} ,
          new Object[] {"@lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4",SqlDbType.Char,100,0} ,
          new Object[] {"@AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host",SqlDbType.Char,50,0} ,
          new Object[] {"@AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user",SqlDbType.Char,50,0} ,
          new Object[] {"@AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00VG10 ;
          prmP00VG10 = new Object[] {
          new Object[] {"@AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4",SqlDbType.Char,100,0} ,
          new Object[] {"@lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4",SqlDbType.Char,100,0} ,
          new Object[] {"@lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4",SqlDbType.Char,100,0} ,
          new Object[] {"@lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4",SqlDbType.Char,100,0} ,
          new Object[] {"@AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host",SqlDbType.Char,50,0} ,
          new Object[] {"@AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user",SqlDbType.Char,50,0} ,
          new Object[] {"@AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00VG13 ;
          prmP00VG13 = new Object[] {
          new Object[] {"@AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV124WWContagemResultadoNotificacaoDS_39_Tfcontagemresultadonotificacao_destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV125WWContagemResultadoNotificacaoDS_40_Tfcontagemresultadonotificacao_destinatarios_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126WWContagemResultadoNotificacaoDS_41_Tfcontagemresultadonotificacao_demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV127WWContagemResultadoNotificacaoDS_42_Tfcontagemresultadonotificacao_demandas_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV89WWContagemResultadoNotificacaoDS_4_Contagemresultadonotificacao_datahora1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV90WWContagemResultadoNotificacaoDS_5_Contagemresultadonotificacao_datahora_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV91WWContagemResultadoNotificacaoDS_6_Contagemresultadonotificacao_assunto1",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV92WWContagemResultadoNotificacaoDS_7_Contagemresultadonotificacao_usunom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV93WWContagemResultadoNotificacaoDS_8_Contagemresultadonotificacao_destnome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV97WWContagemResultadoNotificacaoDS_12_Contagemresultadonotificacao_datahora2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV98WWContagemResultadoNotificacaoDS_13_Contagemresultadonotificacao_datahora_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV99WWContagemResultadoNotificacaoDS_14_Contagemresultadonotificacao_assunto2",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV100WWContagemResultadoNotificacaoDS_15_Contagemresultadonotificacao_usunom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV101WWContagemResultadoNotificacaoDS_16_Contagemresultadonotificacao_destnome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV105WWContagemResultadoNotificacaoDS_20_Contagemresultadonotificacao_datahora3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV106WWContagemResultadoNotificacaoDS_21_Contagemresultadonotificacao_datahora_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV107WWContagemResultadoNotificacaoDS_22_Contagemresultadonotificacao_assunto3",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV108WWContagemResultadoNotificacaoDS_23_Contagemresultadonotificacao_usunom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV109WWContagemResultadoNotificacaoDS_24_Contagemresultadonotificacao_destnome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV113WWContagemResultadoNotificacaoDS_28_Contagemresultadonotificacao_datahora4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV114WWContagemResultadoNotificacaoDS_29_Contagemresultadonotificacao_datahora_to4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV115WWContagemResultadoNotificacaoDS_30_Contagemresultadonotificacao_assunto4",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4",SqlDbType.Char,100,0} ,
          new Object[] {"@lV116WWContagemResultadoNotificacaoDS_31_Contagemresultadonotificacao_usunom4",SqlDbType.Char,100,0} ,
          new Object[] {"@lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4",SqlDbType.Char,100,0} ,
          new Object[] {"@lV117WWContagemResultadoNotificacaoDS_32_Contagemresultadonotificacao_destnome4",SqlDbType.Char,100,0} ,
          new Object[] {"@AV118WWContagemResultadoNotificacaoDS_33_Tfcontagemresultadonotificacao_datahora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV119WWContagemResultadoNotificacaoDS_34_Tfcontagemresultadonotificacao_datahora_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV120WWContagemResultadoNotificacaoDS_35_Tfcontagemresultadonotificacao_usunom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV121WWContagemResultadoNotificacaoDS_36_Tfcontagemresultadonotificacao_usunom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV122WWContagemResultadoNotificacaoDS_37_Tfcontagemresultadonotificacao_assunto",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV123WWContagemResultadoNotificacaoDS_38_Tfcontagemresultadonotificacao_assunto_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV128WWContagemResultadoNotificacaoDS_43_Tfcontagemresultadonotificacao_host",SqlDbType.Char,50,0} ,
          new Object[] {"@AV129WWContagemResultadoNotificacaoDS_44_Tfcontagemresultadonotificacao_host_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV130WWContagemResultadoNotificacaoDS_45_Tfcontagemresultadonotificacao_user",SqlDbType.Char,50,0} ,
          new Object[] {"@AV131WWContagemResultadoNotificacaoDS_46_Tfcontagemresultadonotificacao_user_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV132WWContagemResultadoNotificacaoDS_47_Tfcontagemresultadonotificacao_port",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV133WWContagemResultadoNotificacaoDS_48_Tfcontagemresultadonotificacao_port_to",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VG4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VG4,100,0,true,false )
             ,new CursorDef("P00VG7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VG7,100,0,true,false )
             ,new CursorDef("P00VG10", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VG10,100,0,true,false )
             ,new CursorDef("P00VG13", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VG13,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((long[]) buf[10])[0] = rslt.getLong(7) ;
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((short[]) buf[13])[0] = rslt.getShort(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((bool[]) buf[15])[0] = rslt.getBool(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((short[]) buf[17])[0] = rslt.getShort(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 50) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getLongVarchar(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[27])[0] = rslt.getGXDateTime(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((int[]) buf[29])[0] = rslt.getInt(17) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                ((int[]) buf[31])[0] = rslt.getInt(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((long[]) buf[10])[0] = rslt.getLong(7) ;
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((short[]) buf[13])[0] = rslt.getShort(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((bool[]) buf[15])[0] = rslt.getBool(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((short[]) buf[17])[0] = rslt.getShort(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 50) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getString(15, 100) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[27])[0] = rslt.getGXDateTime(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((int[]) buf[29])[0] = rslt.getInt(17) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                ((int[]) buf[31])[0] = rslt.getInt(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((long[]) buf[10])[0] = rslt.getLong(7) ;
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((short[]) buf[13])[0] = rslt.getShort(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((bool[]) buf[15])[0] = rslt.getBool(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((short[]) buf[17])[0] = rslt.getShort(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 50) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 100) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getLongVarchar(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[27])[0] = rslt.getGXDateTime(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((int[]) buf[29])[0] = rslt.getInt(17) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                ((int[]) buf[31])[0] = rslt.getInt(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((long[]) buf[10])[0] = rslt.getLong(7) ;
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((short[]) buf[13])[0] = rslt.getShort(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((bool[]) buf[15])[0] = rslt.getBool(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((short[]) buf[17])[0] = rslt.getShort(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 50) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 100) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getLongVarchar(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[27])[0] = rslt.getGXDateTime(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((int[]) buf[29])[0] = rslt.getInt(17) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                ((int[]) buf[31])[0] = rslt.getInt(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[60]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[70]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[71]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[78]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[79]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[81]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[82]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[83]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[84]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[85]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[86]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[87]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[90]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[91]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[92]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[93]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[94]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[95]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[96]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[97]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[98]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[99]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[100]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[104]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[105]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[60]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[70]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[71]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[78]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[79]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[81]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[82]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[83]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[84]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[85]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[86]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[87]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[90]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[91]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[92]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[93]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[94]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[95]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[96]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[97]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[98]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[99]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[100]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[104]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[105]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[60]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[70]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[71]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[78]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[79]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[81]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[82]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[83]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[84]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[85]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[86]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[87]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[90]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[91]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[92]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[93]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[94]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[95]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[96]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[97]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[98]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[99]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[100]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[104]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[105]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[60]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[70]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[71]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[78]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[79]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[81]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[82]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[83]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[84]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[85]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[86]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[87]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[90]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[91]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[92]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[93]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[94]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[95]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[96]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[97]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[98]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[99]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[100]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[104]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[105]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontagemresultadonotificacaofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontagemresultadonotificacaofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontagemresultadonotificacaofilterdata") )
          {
             return  ;
          }
          getwwcontagemresultadonotificacaofilterdata worker = new getwwcontagemresultadonotificacaofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
