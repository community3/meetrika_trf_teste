/*
               File: type_SdtReferenciaINM_Anexos
        Description: Referencia INM
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:55:32.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ReferenciaINM.Anexos" )]
   [XmlType(TypeName =  "ReferenciaINM.Anexos" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtReferenciaINM_Anexos : GxSilentTrnSdt, IGxSilentTrnGridItem
   {
      public SdtReferenciaINM_Anexos( )
      {
         /* Constructor for serialization */
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_descricao = "";
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo = "";
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome = "";
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo = "";
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link = "";
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data = (DateTime)(DateTime.MinValue);
         gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome = "";
         gxTv_SdtReferenciaINM_Anexos_Mode = "";
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_Z = "";
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_Z = "";
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome_Z = "";
      }

      public SdtReferenciaINM_Anexos( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Anexos");
         metadata.Set("BT", "ReferenciaINMAnexos");
         metadata.Set("PK", "[ \"ReferenciaINMAnexos_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ReferenciaINM_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"TipoDocumento_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Modified" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Referenciainmanexos_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Referenciainmanexos_arquivonome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Referenciainmanexos_arquivotipo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Referenciainmanexos_data_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tipodocumento_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tipodocumento_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Referenciainmanexos_arquivo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Referenciainmanexos_arquivonome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Referenciainmanexos_arquivotipo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Referenciainmanexos_link_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Referenciainmanexos_data_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tipodocumento_codigo_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtReferenciaINM_Anexos deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtReferenciaINM_Anexos)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtReferenciaINM_Anexos obj ;
         obj = this;
         obj.gxTpr_Referenciainmanexos_codigo = deserialized.gxTpr_Referenciainmanexos_codigo;
         obj.gxTpr_Referenciainmanexos_descricao = deserialized.gxTpr_Referenciainmanexos_descricao;
         obj.gxTpr_Referenciainmanexos_arquivo = deserialized.gxTpr_Referenciainmanexos_arquivo;
         obj.gxTpr_Referenciainmanexos_arquivonome = deserialized.gxTpr_Referenciainmanexos_arquivonome;
         obj.gxTpr_Referenciainmanexos_arquivotipo = deserialized.gxTpr_Referenciainmanexos_arquivotipo;
         obj.gxTpr_Referenciainmanexos_link = deserialized.gxTpr_Referenciainmanexos_link;
         obj.gxTpr_Referenciainmanexos_data = deserialized.gxTpr_Referenciainmanexos_data;
         obj.gxTpr_Tipodocumento_codigo = deserialized.gxTpr_Tipodocumento_codigo;
         obj.gxTpr_Tipodocumento_nome = deserialized.gxTpr_Tipodocumento_nome;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Modified = deserialized.gxTpr_Modified;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Referenciainmanexos_codigo_Z = deserialized.gxTpr_Referenciainmanexos_codigo_Z;
         obj.gxTpr_Referenciainmanexos_arquivonome_Z = deserialized.gxTpr_Referenciainmanexos_arquivonome_Z;
         obj.gxTpr_Referenciainmanexos_arquivotipo_Z = deserialized.gxTpr_Referenciainmanexos_arquivotipo_Z;
         obj.gxTpr_Referenciainmanexos_data_Z = deserialized.gxTpr_Referenciainmanexos_data_Z;
         obj.gxTpr_Tipodocumento_codigo_Z = deserialized.gxTpr_Tipodocumento_codigo_Z;
         obj.gxTpr_Tipodocumento_nome_Z = deserialized.gxTpr_Tipodocumento_nome_Z;
         obj.gxTpr_Referenciainmanexos_arquivo_N = deserialized.gxTpr_Referenciainmanexos_arquivo_N;
         obj.gxTpr_Referenciainmanexos_arquivonome_N = deserialized.gxTpr_Referenciainmanexos_arquivonome_N;
         obj.gxTpr_Referenciainmanexos_arquivotipo_N = deserialized.gxTpr_Referenciainmanexos_arquivotipo_N;
         obj.gxTpr_Referenciainmanexos_link_N = deserialized.gxTpr_Referenciainmanexos_link_N;
         obj.gxTpr_Referenciainmanexos_data_N = deserialized.gxTpr_Referenciainmanexos_data_N;
         obj.gxTpr_Tipodocumento_codigo_N = deserialized.gxTpr_Tipodocumento_codigo_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINMAnexos_Codigo") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINMAnexos_Descricao") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINMAnexos_Arquivo") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo=context.FileFromBase64( oReader.Value) ;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINMAnexos_ArquivoNome") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINMAnexos_ArquivoTipo") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINMAnexos_Link") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINMAnexos_Data") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Codigo") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Nome") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modified") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Modified = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINMAnexos_Codigo_Z") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINMAnexos_ArquivoNome_Z") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINMAnexos_ArquivoTipo_Z") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINMAnexos_Data_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Codigo_Z") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Nome_Z") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINMAnexos_Arquivo_N") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINMAnexos_ArquivoNome_N") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINMAnexos_ArquivoTipo_N") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINMAnexos_Link_N") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINMAnexos_Data_N") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Codigo_N") )
               {
                  gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ReferenciaINM.Anexos";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ReferenciaINMAnexos_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ReferenciaINMAnexos_Descricao", StringUtil.RTrim( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ReferenciaINMAnexos_Arquivo", context.FileToBase64( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ReferenciaINMAnexos_ArquivoNome", StringUtil.RTrim( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ReferenciaINMAnexos_ArquivoTipo", StringUtil.RTrim( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ReferenciaINMAnexos_Link", StringUtil.RTrim( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data) )
         {
            oWriter.WriteStartElement("ReferenciaINMAnexos_Data");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ReferenciaINMAnexos_Data", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("TipoDocumento_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("TipoDocumento_Nome", StringUtil.RTrim( gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtReferenciaINM_Anexos_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Modified", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Anexos_Modified), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Anexos_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ReferenciaINMAnexos_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ReferenciaINMAnexos_ArquivoNome_Z", StringUtil.RTrim( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ReferenciaINMAnexos_ArquivoTipo_Z", StringUtil.RTrim( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z) )
            {
               oWriter.WriteStartElement("ReferenciaINMAnexos_Data_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("ReferenciaINMAnexos_Data_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("TipoDocumento_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("TipoDocumento_Nome_Z", StringUtil.RTrim( gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ReferenciaINMAnexos_Arquivo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ReferenciaINMAnexos_ArquivoNome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ReferenciaINMAnexos_ArquivoTipo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ReferenciaINMAnexos_Link_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ReferenciaINMAnexos_Data_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("TipoDocumento_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ReferenciaINMAnexos_Codigo", gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_codigo, false);
         AddObjectProperty("ReferenciaINMAnexos_Descricao", gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_descricao, false);
         AddObjectProperty("ReferenciaINMAnexos_Arquivo", gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo, false);
         AddObjectProperty("ReferenciaINMAnexos_ArquivoNome", gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome, false);
         AddObjectProperty("ReferenciaINMAnexos_ArquivoTipo", gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo, false);
         AddObjectProperty("ReferenciaINMAnexos_Link", gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link, false);
         datetime_STZ = gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ReferenciaINMAnexos_Data", sDateCnv, false);
         AddObjectProperty("TipoDocumento_Codigo", gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo, false);
         AddObjectProperty("TipoDocumento_Nome", gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtReferenciaINM_Anexos_Mode, false);
            AddObjectProperty("Modified", gxTv_SdtReferenciaINM_Anexos_Modified, false);
            AddObjectProperty("Initialized", gxTv_SdtReferenciaINM_Anexos_Initialized, false);
            AddObjectProperty("ReferenciaINMAnexos_Codigo_Z", gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_codigo_Z, false);
            AddObjectProperty("ReferenciaINMAnexos_ArquivoNome_Z", gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_Z, false);
            AddObjectProperty("ReferenciaINMAnexos_ArquivoTipo_Z", gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_Z, false);
            datetime_STZ = gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("ReferenciaINMAnexos_Data_Z", sDateCnv, false);
            AddObjectProperty("TipoDocumento_Codigo_Z", gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_Z, false);
            AddObjectProperty("TipoDocumento_Nome_Z", gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome_Z, false);
            AddObjectProperty("ReferenciaINMAnexos_Arquivo_N", gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo_N, false);
            AddObjectProperty("ReferenciaINMAnexos_ArquivoNome_N", gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_N, false);
            AddObjectProperty("ReferenciaINMAnexos_ArquivoTipo_N", gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_N, false);
            AddObjectProperty("ReferenciaINMAnexos_Link_N", gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link_N, false);
            AddObjectProperty("ReferenciaINMAnexos_Data_N", gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_N, false);
            AddObjectProperty("TipoDocumento_Codigo_N", gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ReferenciaINMAnexos_Codigo" )]
      [  XmlElement( ElementName = "ReferenciaINMAnexos_Codigo"   )]
      public int gxTpr_Referenciainmanexos_codigo
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_codigo ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_codigo = (int)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      [  SoapElement( ElementName = "ReferenciaINMAnexos_Descricao" )]
      [  XmlElement( ElementName = "ReferenciaINMAnexos_Descricao"   )]
      public String gxTpr_Referenciainmanexos_descricao
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_descricao ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_descricao = (String)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      [  SoapElement( ElementName = "ReferenciaINMAnexos_Arquivo" )]
      [  XmlElement( ElementName = "ReferenciaINMAnexos_Arquivo"   )]
      [GxUpload()]
      public byte[] gxTpr_Referenciainmanexos_arquivo_Blob
      {
         get {
            IGxContext context = this.context == null ? new GxContext() : this.context;
            return context.FileToByteArray( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo) ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo_N = 0;
            IGxContext context = this.context == null ? new GxContext() : this.context;
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo=context.FileFromByteArray( value) ;
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      [GxUpload()]
      public String gxTpr_Referenciainmanexos_arquivo
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo_N = 0;
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo = value;
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo_SetBlob( String blob ,
                                                                                    String fileName ,
                                                                                    String fileType )
      {
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo = blob;
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome = fileName;
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo = fileType;
         return  ;
      }

      public void gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo_N = 1;
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo = "";
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINMAnexos_ArquivoNome" )]
      [  XmlElement( ElementName = "ReferenciaINMAnexos_ArquivoNome"   )]
      public String gxTpr_Referenciainmanexos_arquivonome
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_N = 0;
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome = (String)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_N = 1;
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome = "";
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINMAnexos_ArquivoTipo" )]
      [  XmlElement( ElementName = "ReferenciaINMAnexos_ArquivoTipo"   )]
      public String gxTpr_Referenciainmanexos_arquivotipo
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_N = 0;
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo = (String)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_N = 1;
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo = "";
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINMAnexos_Link" )]
      [  XmlElement( ElementName = "ReferenciaINMAnexos_Link"   )]
      public String gxTpr_Referenciainmanexos_link
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link_N = 0;
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link = (String)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link_N = 1;
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link = "";
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINMAnexos_Data" )]
      [  XmlElement( ElementName = "ReferenciaINMAnexos_Data"  , IsNullable=true )]
      public string gxTpr_Referenciainmanexos_data_Nullable
      {
         get {
            if ( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data).value ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data = DateTime.MinValue;
            else
               gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data = DateTime.Parse( value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Referenciainmanexos_data
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_N = 0;
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data = (DateTime)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_N = 1;
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Codigo" )]
      [  XmlElement( ElementName = "TipoDocumento_Codigo"   )]
      public int gxTpr_Tipodocumento_codigo
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_N = 0;
            gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo = (int)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_N = 1;
         gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Nome" )]
      [  XmlElement( ElementName = "TipoDocumento_Nome"   )]
      public String gxTpr_Tipodocumento_nome
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome = (String)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Mode ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Mode = (String)(value);
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Mode_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Mode = "";
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modified" )]
      [  XmlElement( ElementName = "Modified"   )]
      public short gxTpr_Modified
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Modified ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Modified = (short)(value);
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Modified_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Modified = 0;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Modified_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Initialized ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Initialized = (short)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Initialized_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINMAnexos_Codigo_Z" )]
      [  XmlElement( ElementName = "ReferenciaINMAnexos_Codigo_Z"   )]
      public int gxTpr_Referenciainmanexos_codigo_Z
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_codigo_Z ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_codigo_Z = (int)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_codigo_Z_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINMAnexos_ArquivoNome_Z" )]
      [  XmlElement( ElementName = "ReferenciaINMAnexos_ArquivoNome_Z"   )]
      public String gxTpr_Referenciainmanexos_arquivonome_Z
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_Z ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_Z = (String)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_Z_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_Z = "";
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINMAnexos_ArquivoTipo_Z" )]
      [  XmlElement( ElementName = "ReferenciaINMAnexos_ArquivoTipo_Z"   )]
      public String gxTpr_Referenciainmanexos_arquivotipo_Z
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_Z ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_Z = (String)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_Z_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_Z = "";
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINMAnexos_Data_Z" )]
      [  XmlElement( ElementName = "ReferenciaINMAnexos_Data_Z"  , IsNullable=true )]
      public string gxTpr_Referenciainmanexos_data_Z_Nullable
      {
         get {
            if ( gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z = DateTime.MinValue;
            else
               gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z = DateTime.Parse( value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Referenciainmanexos_data_Z
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z = (DateTime)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Codigo_Z" )]
      [  XmlElement( ElementName = "TipoDocumento_Codigo_Z"   )]
      public int gxTpr_Tipodocumento_codigo_Z
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_Z ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_Z = (int)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_Z_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Nome_Z" )]
      [  XmlElement( ElementName = "TipoDocumento_Nome_Z"   )]
      public String gxTpr_Tipodocumento_nome_Z
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome_Z ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome_Z = (String)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome_Z_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINMAnexos_Arquivo_N" )]
      [  XmlElement( ElementName = "ReferenciaINMAnexos_Arquivo_N"   )]
      public short gxTpr_Referenciainmanexos_arquivo_N
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo_N ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo_N = (short)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo_N_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo_N = 0;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINMAnexos_ArquivoNome_N" )]
      [  XmlElement( ElementName = "ReferenciaINMAnexos_ArquivoNome_N"   )]
      public short gxTpr_Referenciainmanexos_arquivonome_N
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_N ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_N = (short)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_N_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_N = 0;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINMAnexos_ArquivoTipo_N" )]
      [  XmlElement( ElementName = "ReferenciaINMAnexos_ArquivoTipo_N"   )]
      public short gxTpr_Referenciainmanexos_arquivotipo_N
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_N ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_N = (short)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_N_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_N = 0;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINMAnexos_Link_N" )]
      [  XmlElement( ElementName = "ReferenciaINMAnexos_Link_N"   )]
      public short gxTpr_Referenciainmanexos_link_N
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link_N ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link_N = (short)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link_N_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link_N = 0;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINMAnexos_Data_N" )]
      [  XmlElement( ElementName = "ReferenciaINMAnexos_Data_N"   )]
      public short gxTpr_Referenciainmanexos_data_N
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_N ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_N = (short)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_N_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_N = 0;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Codigo_N" )]
      [  XmlElement( ElementName = "TipoDocumento_Codigo_N"   )]
      public short gxTpr_Tipodocumento_codigo_N
      {
         get {
            return gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_N ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_N = (short)(value);
            gxTv_SdtReferenciaINM_Anexos_Modified = 1;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_N_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_descricao = "";
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo = "";
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome = "";
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo = "";
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link = "";
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data = (DateTime)(DateTime.MinValue);
         gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome = "";
         gxTv_SdtReferenciaINM_Anexos_Mode = "";
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_Z = "";
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_Z = "";
         gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome_Z = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         return  ;
      }

      private short gxTv_SdtReferenciaINM_Anexos_Modified ;
      private short gxTv_SdtReferenciaINM_Anexos_Initialized ;
      private short gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo_N ;
      private short gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_N ;
      private short gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_N ;
      private short gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link_N ;
      private short gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_N ;
      private short gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_codigo ;
      private int gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo ;
      private int gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_codigo_Z ;
      private int gxTv_SdtReferenciaINM_Anexos_Tipodocumento_codigo_Z ;
      private String gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome ;
      private String gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo ;
      private String gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome ;
      private String gxTv_SdtReferenciaINM_Anexos_Mode ;
      private String gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivonome_Z ;
      private String gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivotipo_Z ;
      private String gxTv_SdtReferenciaINM_Anexos_Tipodocumento_nome_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data ;
      private DateTime gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_data_Z ;
      private DateTime datetime_STZ ;
      private String gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_descricao ;
      private String gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_link ;
      private String gxTv_SdtReferenciaINM_Anexos_Referenciainmanexos_arquivo ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ReferenciaINM.Anexos", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtReferenciaINM_Anexos_RESTInterface : GxGenericCollectionItem<SdtReferenciaINM_Anexos>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtReferenciaINM_Anexos_RESTInterface( ) : base()
      {
      }

      public SdtReferenciaINM_Anexos_RESTInterface( SdtReferenciaINM_Anexos psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ReferenciaINMAnexos_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Referenciainmanexos_codigo
      {
         get {
            return sdt.gxTpr_Referenciainmanexos_codigo ;
         }

         set {
            sdt.gxTpr_Referenciainmanexos_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ReferenciaINMAnexos_Descricao" , Order = 1 )]
      public String gxTpr_Referenciainmanexos_descricao
      {
         get {
            return sdt.gxTpr_Referenciainmanexos_descricao ;
         }

         set {
            sdt.gxTpr_Referenciainmanexos_descricao = (String)(value);
         }

      }

      [DataMember( Name = "ReferenciaINMAnexos_Arquivo" , Order = 2 )]
      [GxUpload()]
      public String gxTpr_Referenciainmanexos_arquivo
      {
         get {
            return PathUtil.RelativePath( sdt.gxTpr_Referenciainmanexos_arquivo) ;
         }

         set {
            sdt.gxTpr_Referenciainmanexos_arquivo = value;
         }

      }

      [DataMember( Name = "ReferenciaINMAnexos_ArquivoNome" , Order = 3 )]
      public String gxTpr_Referenciainmanexos_arquivonome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Referenciainmanexos_arquivonome) ;
         }

         set {
            sdt.gxTpr_Referenciainmanexos_arquivonome = (String)(value);
         }

      }

      [DataMember( Name = "ReferenciaINMAnexos_ArquivoTipo" , Order = 4 )]
      public String gxTpr_Referenciainmanexos_arquivotipo
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Referenciainmanexos_arquivotipo) ;
         }

         set {
            sdt.gxTpr_Referenciainmanexos_arquivotipo = (String)(value);
         }

      }

      [DataMember( Name = "ReferenciaINMAnexos_Link" , Order = 5 )]
      public String gxTpr_Referenciainmanexos_link
      {
         get {
            return sdt.gxTpr_Referenciainmanexos_link ;
         }

         set {
            sdt.gxTpr_Referenciainmanexos_link = (String)(value);
         }

      }

      [DataMember( Name = "ReferenciaINMAnexos_Data" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Referenciainmanexos_data
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Referenciainmanexos_data) ;
         }

         set {
            sdt.gxTpr_Referenciainmanexos_data = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "TipoDocumento_Codigo" , Order = 7 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Tipodocumento_codigo
      {
         get {
            return sdt.gxTpr_Tipodocumento_codigo ;
         }

         set {
            sdt.gxTpr_Tipodocumento_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "TipoDocumento_Nome" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Tipodocumento_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tipodocumento_nome) ;
         }

         set {
            sdt.gxTpr_Tipodocumento_nome = (String)(value);
         }

      }

      public SdtReferenciaINM_Anexos sdt
      {
         get {
            return (SdtReferenciaINM_Anexos)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtReferenciaINM_Anexos() ;
         }
      }

   }

}
