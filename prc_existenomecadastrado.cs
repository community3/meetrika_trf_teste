/*
               File: PRC_ExisteNomeCadastrado
        Description: Existe Nome Cadastrado
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:50.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_existenomecadastrado : GXProcedure
   {
      public prc_existenomecadastrado( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_existenomecadastrado( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Tabela ,
                           String aP1_String ,
                           out bool aP2_Existe )
      {
         this.AV10Tabela = aP0_Tabela;
         this.AV13String = aP1_String;
         this.AV8Existe = false ;
         initialize();
         executePrivate();
         aP2_Existe=this.AV8Existe;
      }

      public bool executeUdp( String aP0_Tabela ,
                              String aP1_String )
      {
         this.AV10Tabela = aP0_Tabela;
         this.AV13String = aP1_String;
         this.AV8Existe = false ;
         initialize();
         executePrivate();
         aP2_Existe=this.AV8Existe;
         return AV8Existe ;
      }

      public void executeSubmit( String aP0_Tabela ,
                                 String aP1_String ,
                                 out bool aP2_Existe )
      {
         prc_existenomecadastrado objprc_existenomecadastrado;
         objprc_existenomecadastrado = new prc_existenomecadastrado();
         objprc_existenomecadastrado.AV10Tabela = aP0_Tabela;
         objprc_existenomecadastrado.AV13String = aP1_String;
         objprc_existenomecadastrado.AV8Existe = false ;
         objprc_existenomecadastrado.context.SetSubmitInitialConfig(context);
         objprc_existenomecadastrado.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_existenomecadastrado);
         aP2_Existe=this.AV8Existe;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_existenomecadastrado)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV11WWPContext) ;
         GXt_char1 = AV9Nome;
         new prc_padronizastring(context ).execute(  AV13String, out  GXt_char1) ;
         AV9Nome = GXt_char1;
         AV8Existe = false;
         if ( StringUtil.StrCmp(AV10Tabela, "Tec") == 0 )
         {
            /* Using cursor P00292 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               A132Tecnologia_Nome = P00292_A132Tecnologia_Nome[0];
               A131Tecnologia_Codigo = P00292_A131Tecnologia_Codigo[0];
               if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A132Tecnologia_Nome), AV9Nome) == 0 )
               {
                  AV8Existe = true;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(0);
            }
            pr_default.close(0);
         }
         else if ( StringUtil.StrCmp(AV10Tabela, "Met") == 0 )
         {
            /* Using cursor P00293 */
            pr_default.execute(1);
            while ( (pr_default.getStatus(1) != 101) )
            {
               A138Metodologia_Descricao = P00293_A138Metodologia_Descricao[0];
               A137Metodologia_Codigo = P00293_A137Metodologia_Codigo[0];
               if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A138Metodologia_Descricao), AV9Nome) == 0 )
               {
                  AV8Existe = true;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
         }
         else if ( StringUtil.StrCmp(AV10Tabela, "GrpSrv") == 0 )
         {
            /* Using cursor P00294 */
            pr_default.execute(2);
            while ( (pr_default.getStatus(2) != 101) )
            {
               A158ServicoGrupo_Descricao = P00294_A158ServicoGrupo_Descricao[0];
               A157ServicoGrupo_Codigo = P00294_A157ServicoGrupo_Codigo[0];
               if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A158ServicoGrupo_Descricao), AV9Nome) == 0 )
               {
                  AV8Existe = true;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(2);
            }
            pr_default.close(2);
         }
         else if ( StringUtil.StrCmp(AV10Tabela, "AmbTec") == 0 )
         {
            /* Using cursor P00295 */
            pr_default.execute(3, new Object[] {AV11WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A728AmbienteTecnologico_AreaTrabalhoCod = P00295_A728AmbienteTecnologico_AreaTrabalhoCod[0];
               A352AmbienteTecnologico_Descricao = P00295_A352AmbienteTecnologico_Descricao[0];
               A351AmbienteTecnologico_Codigo = P00295_A351AmbienteTecnologico_Codigo[0];
               if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A352AmbienteTecnologico_Descricao), AV9Nome) == 0 )
               {
                  AV8Existe = true;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(3);
            }
            pr_default.close(3);
         }
         else if ( StringUtil.StrCmp(AV10Tabela, "Sis") == 0 )
         {
            /* Using cursor P00296 */
            pr_default.execute(4, new Object[] {AV11WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A135Sistema_AreaTrabalhoCod = P00296_A135Sistema_AreaTrabalhoCod[0];
               A416Sistema_Nome = P00296_A416Sistema_Nome[0];
               A127Sistema_Codigo = P00296_A127Sistema_Codigo[0];
               if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A416Sistema_Nome), AV9Nome) == 0 )
               {
                  AV8Existe = true;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(4);
            }
            pr_default.close(4);
         }
         else if ( StringUtil.StrCmp(AV10Tabela, "Sig") == 0 )
         {
            /* Using cursor P00297 */
            pr_default.execute(5, new Object[] {AV11WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A129Sistema_Sigla = P00297_A129Sistema_Sigla[0];
               A135Sistema_AreaTrabalhoCod = P00297_A135Sistema_AreaTrabalhoCod[0];
               A127Sistema_Codigo = P00297_A127Sistema_Codigo[0];
               if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A129Sistema_Sigla), AV9Nome) == 0 )
               {
                  AV8Existe = true;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(5);
            }
            pr_default.close(5);
         }
         else if ( StringUtil.StrCmp(AV10Tabela, "Mod") == 0 )
         {
            /* Using cursor P00298 */
            pr_default.execute(6, new Object[] {AV11WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A127Sistema_Codigo = P00298_A127Sistema_Codigo[0];
               A135Sistema_AreaTrabalhoCod = P00298_A135Sistema_AreaTrabalhoCod[0];
               A143Modulo_Nome = P00298_A143Modulo_Nome[0];
               A146Modulo_Codigo = P00298_A146Modulo_Codigo[0];
               A135Sistema_AreaTrabalhoCod = P00298_A135Sistema_AreaTrabalhoCod[0];
               if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A143Modulo_Nome), AV9Nome) == 0 )
               {
                  AV8Existe = true;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(6);
            }
            pr_default.close(6);
         }
         else if ( StringUtil.StrCmp(AV10Tabela, "Atr") == 0 )
         {
            /* Using cursor P00299 */
            pr_default.execute(7);
            while ( (pr_default.getStatus(7) != 101) )
            {
               A177Atributos_Nome = P00299_A177Atributos_Nome[0];
               A356Atributos_TabelaCod = P00299_A356Atributos_TabelaCod[0];
               A176Atributos_Codigo = P00299_A176Atributos_Codigo[0];
               if ( (Convert.ToDecimal( A356Atributos_TabelaCod ) == NumberUtil.Val( AV12WebSession.Get("Tabela_Codigo"), ".") ) )
               {
                  if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A177Atributos_Nome), AV9Nome) == 0 )
                  {
                     AV8Existe = true;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
               pr_default.readNext(7);
            }
            pr_default.close(7);
         }
         else if ( StringUtil.StrCmp(AV10Tabela, "Tab") == 0 )
         {
            /* Using cursor P002910 */
            pr_default.execute(8);
            while ( (pr_default.getStatus(8) != 101) )
            {
               A173Tabela_Nome = P002910_A173Tabela_Nome[0];
               A190Tabela_SistemaCod = P002910_A190Tabela_SistemaCod[0];
               A172Tabela_Codigo = P002910_A172Tabela_Codigo[0];
               if ( (Convert.ToDecimal( A190Tabela_SistemaCod ) == NumberUtil.Val( AV12WebSession.Get("Sistema_Codigo"), ".") ) )
               {
                  if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A173Tabela_Nome), AV9Nome) == 0 )
                  {
                     AV8Existe = true;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
               pr_default.readNext(8);
            }
            pr_default.close(8);
         }
         else if ( StringUtil.StrCmp(AV10Tabela, "FnD") == 0 )
         {
            /* Using cursor P002911 */
            pr_default.execute(9);
            while ( (pr_default.getStatus(9) != 101) )
            {
               A369FuncaoDados_Nome = P002911_A369FuncaoDados_Nome[0];
               A370FuncaoDados_SistemaCod = P002911_A370FuncaoDados_SistemaCod[0];
               A368FuncaoDados_Codigo = P002911_A368FuncaoDados_Codigo[0];
               if ( (Convert.ToDecimal( A370FuncaoDados_SistemaCod ) == NumberUtil.Val( AV12WebSession.Get("Sistema_Codigo"), ".") ) )
               {
                  if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A369FuncaoDados_Nome), AV9Nome) == 0 )
                  {
                     AV8Existe = true;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
               pr_default.readNext(9);
            }
            pr_default.close(9);
         }
         else if ( StringUtil.StrCmp(AV10Tabela, "FnT") == 0 )
         {
            /* Using cursor P002912 */
            pr_default.execute(10);
            while ( (pr_default.getStatus(10) != 101) )
            {
               A166FuncaoAPF_Nome = P002912_A166FuncaoAPF_Nome[0];
               A360FuncaoAPF_SistemaCod = P002912_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = P002912_n360FuncaoAPF_SistemaCod[0];
               A165FuncaoAPF_Codigo = P002912_A165FuncaoAPF_Codigo[0];
               if ( (Convert.ToDecimal( A360FuncaoAPF_SistemaCod ) == NumberUtil.Val( AV12WebSession.Get("Sistema_Codigo"), ".") ) )
               {
                  if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A166FuncaoAPF_Nome), AV9Nome) == 0 )
                  {
                     AV8Existe = true;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
               pr_default.readNext(10);
            }
            pr_default.close(10);
         }
         AV12WebSession.Remove("Tabela_Codigo");
         AV12WebSession.Remove("Sistema_Codigo");
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV11WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9Nome = "";
         GXt_char1 = "";
         scmdbuf = "";
         P00292_A132Tecnologia_Nome = new String[] {""} ;
         P00292_A131Tecnologia_Codigo = new int[1] ;
         A132Tecnologia_Nome = "";
         P00293_A138Metodologia_Descricao = new String[] {""} ;
         P00293_A137Metodologia_Codigo = new int[1] ;
         A138Metodologia_Descricao = "";
         P00294_A158ServicoGrupo_Descricao = new String[] {""} ;
         P00294_A157ServicoGrupo_Codigo = new int[1] ;
         A158ServicoGrupo_Descricao = "";
         P00295_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         P00295_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P00295_A351AmbienteTecnologico_Codigo = new int[1] ;
         A352AmbienteTecnologico_Descricao = "";
         P00296_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00296_A416Sistema_Nome = new String[] {""} ;
         P00296_A127Sistema_Codigo = new int[1] ;
         A416Sistema_Nome = "";
         P00297_A129Sistema_Sigla = new String[] {""} ;
         P00297_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00297_A127Sistema_Codigo = new int[1] ;
         A129Sistema_Sigla = "";
         P00298_A127Sistema_Codigo = new int[1] ;
         P00298_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00298_A143Modulo_Nome = new String[] {""} ;
         P00298_A146Modulo_Codigo = new int[1] ;
         A143Modulo_Nome = "";
         P00299_A177Atributos_Nome = new String[] {""} ;
         P00299_A356Atributos_TabelaCod = new int[1] ;
         P00299_A176Atributos_Codigo = new int[1] ;
         A177Atributos_Nome = "";
         AV12WebSession = context.GetSession();
         P002910_A173Tabela_Nome = new String[] {""} ;
         P002910_A190Tabela_SistemaCod = new int[1] ;
         P002910_A172Tabela_Codigo = new int[1] ;
         A173Tabela_Nome = "";
         P002911_A369FuncaoDados_Nome = new String[] {""} ;
         P002911_A370FuncaoDados_SistemaCod = new int[1] ;
         P002911_A368FuncaoDados_Codigo = new int[1] ;
         A369FuncaoDados_Nome = "";
         P002912_A166FuncaoAPF_Nome = new String[] {""} ;
         P002912_A360FuncaoAPF_SistemaCod = new int[1] ;
         P002912_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         P002912_A165FuncaoAPF_Codigo = new int[1] ;
         A166FuncaoAPF_Nome = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_existenomecadastrado__default(),
            new Object[][] {
                new Object[] {
               P00292_A132Tecnologia_Nome, P00292_A131Tecnologia_Codigo
               }
               , new Object[] {
               P00293_A138Metodologia_Descricao, P00293_A137Metodologia_Codigo
               }
               , new Object[] {
               P00294_A158ServicoGrupo_Descricao, P00294_A157ServicoGrupo_Codigo
               }
               , new Object[] {
               P00295_A728AmbienteTecnologico_AreaTrabalhoCod, P00295_A352AmbienteTecnologico_Descricao, P00295_A351AmbienteTecnologico_Codigo
               }
               , new Object[] {
               P00296_A135Sistema_AreaTrabalhoCod, P00296_A416Sistema_Nome, P00296_A127Sistema_Codigo
               }
               , new Object[] {
               P00297_A129Sistema_Sigla, P00297_A135Sistema_AreaTrabalhoCod, P00297_A127Sistema_Codigo
               }
               , new Object[] {
               P00298_A127Sistema_Codigo, P00298_A135Sistema_AreaTrabalhoCod, P00298_A143Modulo_Nome, P00298_A146Modulo_Codigo
               }
               , new Object[] {
               P00299_A177Atributos_Nome, P00299_A356Atributos_TabelaCod, P00299_A176Atributos_Codigo
               }
               , new Object[] {
               P002910_A173Tabela_Nome, P002910_A190Tabela_SistemaCod, P002910_A172Tabela_Codigo
               }
               , new Object[] {
               P002911_A369FuncaoDados_Nome, P002911_A370FuncaoDados_SistemaCod, P002911_A368FuncaoDados_Codigo
               }
               , new Object[] {
               P002912_A166FuncaoAPF_Nome, P002912_A360FuncaoAPF_SistemaCod, P002912_n360FuncaoAPF_SistemaCod, P002912_A165FuncaoAPF_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A131Tecnologia_Codigo ;
      private int A137Metodologia_Codigo ;
      private int A157ServicoGrupo_Codigo ;
      private int A728AmbienteTecnologico_AreaTrabalhoCod ;
      private int A351AmbienteTecnologico_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A127Sistema_Codigo ;
      private int A146Modulo_Codigo ;
      private int A356Atributos_TabelaCod ;
      private int A176Atributos_Codigo ;
      private int A190Tabela_SistemaCod ;
      private int A172Tabela_Codigo ;
      private int A370FuncaoDados_SistemaCod ;
      private int A368FuncaoDados_Codigo ;
      private int A360FuncaoAPF_SistemaCod ;
      private int A165FuncaoAPF_Codigo ;
      private String AV10Tabela ;
      private String AV13String ;
      private String AV9Nome ;
      private String GXt_char1 ;
      private String scmdbuf ;
      private String A132Tecnologia_Nome ;
      private String A129Sistema_Sigla ;
      private String A143Modulo_Nome ;
      private String A177Atributos_Nome ;
      private String A173Tabela_Nome ;
      private bool AV8Existe ;
      private bool n360FuncaoAPF_SistemaCod ;
      private String A138Metodologia_Descricao ;
      private String A158ServicoGrupo_Descricao ;
      private String A352AmbienteTecnologico_Descricao ;
      private String A416Sistema_Nome ;
      private String A369FuncaoDados_Nome ;
      private String A166FuncaoAPF_Nome ;
      private IGxSession AV12WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00292_A132Tecnologia_Nome ;
      private int[] P00292_A131Tecnologia_Codigo ;
      private String[] P00293_A138Metodologia_Descricao ;
      private int[] P00293_A137Metodologia_Codigo ;
      private String[] P00294_A158ServicoGrupo_Descricao ;
      private int[] P00294_A157ServicoGrupo_Codigo ;
      private int[] P00295_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private String[] P00295_A352AmbienteTecnologico_Descricao ;
      private int[] P00295_A351AmbienteTecnologico_Codigo ;
      private int[] P00296_A135Sistema_AreaTrabalhoCod ;
      private String[] P00296_A416Sistema_Nome ;
      private int[] P00296_A127Sistema_Codigo ;
      private String[] P00297_A129Sistema_Sigla ;
      private int[] P00297_A135Sistema_AreaTrabalhoCod ;
      private int[] P00297_A127Sistema_Codigo ;
      private int[] P00298_A127Sistema_Codigo ;
      private int[] P00298_A135Sistema_AreaTrabalhoCod ;
      private String[] P00298_A143Modulo_Nome ;
      private int[] P00298_A146Modulo_Codigo ;
      private String[] P00299_A177Atributos_Nome ;
      private int[] P00299_A356Atributos_TabelaCod ;
      private int[] P00299_A176Atributos_Codigo ;
      private String[] P002910_A173Tabela_Nome ;
      private int[] P002910_A190Tabela_SistemaCod ;
      private int[] P002910_A172Tabela_Codigo ;
      private String[] P002911_A369FuncaoDados_Nome ;
      private int[] P002911_A370FuncaoDados_SistemaCod ;
      private int[] P002911_A368FuncaoDados_Codigo ;
      private String[] P002912_A166FuncaoAPF_Nome ;
      private int[] P002912_A360FuncaoAPF_SistemaCod ;
      private bool[] P002912_n360FuncaoAPF_SistemaCod ;
      private int[] P002912_A165FuncaoAPF_Codigo ;
      private bool aP2_Existe ;
      private wwpbaseobjects.SdtWWPContext AV11WWPContext ;
   }

   public class prc_existenomecadastrado__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00292 ;
          prmP00292 = new Object[] {
          } ;
          Object[] prmP00293 ;
          prmP00293 = new Object[] {
          } ;
          Object[] prmP00294 ;
          prmP00294 = new Object[] {
          } ;
          Object[] prmP00295 ;
          prmP00295 = new Object[] {
          new Object[] {"@AV11WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00296 ;
          prmP00296 = new Object[] {
          new Object[] {"@AV11WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00297 ;
          prmP00297 = new Object[] {
          new Object[] {"@AV11WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00298 ;
          prmP00298 = new Object[] {
          new Object[] {"@AV11WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00299 ;
          prmP00299 = new Object[] {
          } ;
          Object[] prmP002910 ;
          prmP002910 = new Object[] {
          } ;
          Object[] prmP002911 ;
          prmP002911 = new Object[] {
          } ;
          Object[] prmP002912 ;
          prmP002912 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00292", "SELECT [Tecnologia_Nome], [Tecnologia_Codigo] FROM [Tecnologia] WITH (NOLOCK) ORDER BY [Tecnologia_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00292,100,0,true,false )
             ,new CursorDef("P00293", "SELECT [Metodologia_Descricao], [Metodologia_Codigo] FROM [Metodologia] WITH (NOLOCK) ORDER BY [Metodologia_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00293,100,0,true,false )
             ,new CursorDef("P00294", "SELECT [ServicoGrupo_Descricao], [ServicoGrupo_Codigo] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00294,100,0,true,false )
             ,new CursorDef("P00295", "SELECT [AmbienteTecnologico_AreaTrabalhoCod], [AmbienteTecnologico_Descricao], [AmbienteTecnologico_Codigo] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_AreaTrabalhoCod] = @AV11WWPC_1Areatrabalho_codigo ORDER BY [AmbienteTecnologico_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00295,100,0,true,false )
             ,new CursorDef("P00296", "SELECT [Sistema_AreaTrabalhoCod], [Sistema_Nome], [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_AreaTrabalhoCod] = @AV11WWPC_1Areatrabalho_codigo ORDER BY [Sistema_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00296,100,0,true,false )
             ,new CursorDef("P00297", "SELECT [Sistema_Sigla], [Sistema_AreaTrabalhoCod], [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_AreaTrabalhoCod] = @AV11WWPC_1Areatrabalho_codigo ORDER BY [Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00297,100,0,true,false )
             ,new CursorDef("P00298", "SELECT T1.[Sistema_Codigo], T2.[Sistema_AreaTrabalhoCod], T1.[Modulo_Nome], T1.[Modulo_Codigo] FROM ([Modulo] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Sistema_Codigo]) WHERE T2.[Sistema_AreaTrabalhoCod] = @AV11WWPC_1Areatrabalho_codigo ORDER BY T1.[Modulo_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00298,100,0,true,false )
             ,new CursorDef("P00299", "SELECT [Atributos_Nome], [Atributos_TabelaCod], [Atributos_Codigo] FROM [Atributos] WITH (NOLOCK) ORDER BY [Atributos_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00299,100,0,true,false )
             ,new CursorDef("P002910", "SELECT [Tabela_Nome], [Tabela_SistemaCod], [Tabela_Codigo] FROM [Tabela] WITH (NOLOCK) ORDER BY [Tabela_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002910,100,0,true,false )
             ,new CursorDef("P002911", "SELECT [FuncaoDados_Nome], [FuncaoDados_SistemaCod], [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) ORDER BY [FuncaoDados_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002911,100,0,true,false )
             ,new CursorDef("P002912", "SELECT [FuncaoAPF_Nome], [FuncaoAPF_SistemaCod], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) ORDER BY [FuncaoAPF_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002912,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 25) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
